
Partial Class PropertyManagement_Reports_property_reports
    Inherits System.Web.UI.Page

    Protected Sub lnkBtnPSD_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmLease_Agreement_Details.aspx")
    End Sub

    Protected Sub lnkBtnANUM_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmPremise_Details.aspx")
    End Sub

    Protected Sub lnkBtnPCD2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmLease_Expiry_Details.aspx")
    End Sub

    Protected Sub lnkBtnPCD3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmLessorDetails.aspx")
    End Sub

    Protected Sub lnkBtnPCD4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmPropertyDetails.aspx")
    End Sub
End Class
