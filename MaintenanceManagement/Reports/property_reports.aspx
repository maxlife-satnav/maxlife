<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="property_reports.aspx.vb" Inherits="PropertyManagement_Reports_property_reports" title="Property Reports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Property Management Reports
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Property Management Reports</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="center">
                <br />
                <table id="Table1" cellspacing="1" cellpadding="1" width="95%" border="1">
                    <tr>
                        <td class="clsLabel" width="75%">
                           Lease Agreement Details </td>
                        <td width="25%" align="center" class="leftlinksgrid">
                            <asp:LinkButton id="lnkBtnPSD" runat="server" Font-Size="Small" OnClick="lnkBtnPSD_Click">View</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td class="clsLabel" width="75%">
                            Premise Details </td>
                        <td width="25%" align="center" class="leftlinksgrid">
                            <asp:LinkButton id="lnkBtnANUM" runat="server" Font-Size="Small" OnClick="lnkBtnANUM_Click">View</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td class="clsLabel" width="75%">
                            Lease Expiry Details</td>
                        <td width="25%" align="center" class="leftlinksgrid">
                            <asp:LinkButton id="lnkBtnPCD2" runat="server" Font-Size="Small" OnClick="lnkBtnPCD2_Click">View</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td class="clsLabel" width="75%">
                           Lessor Details</td>
                        <td width="25%" align="center" class="leftlinksgrid">
                            <asp:LinkButton id="lnkBtnPCD3" runat="server" Font-Size="Small" OnClick="lnkBtnPCD3_Click">View</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td class="clsLabel" width="75%">
                           Property Details</td>
                        <td width="25%" align="center" class="leftlinksgrid">
                            <asp:LinkButton id="lnkBtnPCD4" runat="server" Font-Size="Small" OnClick="lnkBtnPCD4_Click">View</asp:LinkButton></td>
                    </tr>
                      <%--<tr>
                        <td class="clsLabel" width="75%">
                           Property Documents</td>
                        <td width="25%" align="center" class="leftlinksgrid">
                            <asp:LinkButton id="lnkBtnPCD5" runat="server" Font-Size="Small">View</asp:LinkButton></td>
                    </tr>--%>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" />&nbsp;</td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</asp:Content>

