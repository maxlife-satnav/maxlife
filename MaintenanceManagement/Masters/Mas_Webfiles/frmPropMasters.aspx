<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmPropMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmPropMasters"
    Title="Property Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap-select.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/amantra.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/font-awesome/css/font-awesome.min.css")%>' rel="stylesheet" />
    <link rel="stylesheet" href='<%= Page.ResolveUrl("~/BootStrapCSS/datepicker.css")%>' />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <style>
        .btn {
            border-radius: 4px;
            background-color: #3A618F;
        }
    </style>
</head>
<body>


    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="box box-primary">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Property Masters</legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="box-body">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="col-md-12 control-label"
                                    ShowMessageBox="true" DisplayMode="List" ForeColor="" ValidationGroup="Val1" />
                                <div class="clearfix">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/WorkSpace/SMS_Webfiles/frmAddPropertyType.aspx">Property Type</asp:HyperLink>
                                    </div>
                                    <%-- <div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="HyperLink5" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/ExpenseHead.aspx">Expense Head</asp:HyperLink>
                                    </div>--%>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/LeaseEscalation.aspx">Lease Escalation</asp:HyperLink>
                                    </div>


                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/ServiceType.aspx">Service Type</asp:HyperLink>
                                    </div>


                                </div>
                                </br>
                                <div class="clearfix">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/SystemPreferences.aspx">System Preferences</asp:HyperLink>
                                    </div>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-datepicker.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/ObjectKeys.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/wz_tooltip.js")%>' type="text/javascript"></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" temp_src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" temp_src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" temp_src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>
</html>

