Imports System.Data

Partial Class Masters_Mas_Webfiles_frmMasVendor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then
            BindCity()
            BindGrid()
            rbActions.Checked = True
            BindVendor()
            TRVENDOR.Visible = False
        End If
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_VENDOR_DETAILS")
        Dim dummy As String = sp.Command.CommandSql
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY")
        sp.Command.AddParameter("@dummy", "")
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        cmbloc.DataSource = sp.GetDataSet()
        cmbloc.DataTextField = "CTY_NAME"
        cmbloc.DataValueField = "CTY_CODE"
        cmbloc.DataBind()
        cmbloc.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindVendor()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_VENDORS")
        Dim dummy As String = sp.Command.CommandSql
        cmbPKeys.DataSource = sp.GetDataSet()
        cmbPKeys.DataTextField = "AVR_NAME"
        cmbPKeys.DataValueField = "AVR_CODE"
        cmbPKeys.DataBind()
        cmbPKeys.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If rbActions.Checked = True Then
            Dim Validate As Integer
            Validate = ValidateVendorCode()
            If Validate = 1 Then
                LblFinalStatus.Text = ""
                AddVendor()
                BindVendor()
                BindGrid()
                Cleardata()
                ' Response.Redirect("~/MaintenanceManagement/WorkSpace/frmThanks.aspx?id=19")
                LblFinalStatus.Text = "New Vendor Added Successfully..."
            Else
                LblFinalStatus.Text = "Vendor Code Already Exists..."
            End If
        Else
            ModifyVendor()
            BindGrid()
            BindVendor()
            Cleardata()
            LblFinalStatus.Text = "Vendor Modified Successfully..."
            ' Response.Redirect("~/MaintenanceManagement/WorkSpace/frmThanks.aspx?id=20")
        End If
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            btnSubmit.Text = "Add"
            Cleardata()
            TRVENDOR.Visible = False
            txtcode.ReadOnly = False
            txtcode.Enabled = True
        Else
            btnSubmit.Text = "Modify"
            Cleardata()
            TRVENDOR.Visible = True
            'txtcode.ReadOnly = True
        End If
    End Sub

    Private Sub Cleardata()
        txtcode.Text = ""
        txtName.Text = ""
        cmbgrade.SelectedIndex = -1
        cmbconst.SelectedIndex = -1
        cmbloc.SelectedIndex = -1
        txtpincode.Text = ""
        txtoffphno.Text = ""
        txtmobilephno.Text = ""
        txtresiphno.Text = ""
        txtemail.Text = ""
        txtPANno.Text = ""
        txtGIRno.Text = ""
        txtTANno.Text = ""
        txtICICIACno.Text = ""
        txtlstno.Text = ""
        txtwctno.Text = ""
        txtcstno.Text = ""
        txtstcircle.Text = ""
        txtaddress.Text = ""
        txtrem.Text = ""
        ddlstatus.SelectedIndex = -1
        LblFinalStatus.Text = ""
        cmbPKeys.SelectedIndex = 0
    End Sub

    Private Sub AddVendor()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_VENDOR")
        sp.Command.AddParameter("@VENDOR_CODE", txtcode.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_NAME", txtName.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_GRADE", cmbgrade.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VENDOR_CONSTITUTION", cmbconst.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VENDOR_CITY", cmbloc.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VENDOR_PINCODE", txtpincode.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_OPHN", txtoffphno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_MPHN", txtmobilephno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_RPHN", txtresiphno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_EMAIL", txtemail.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_PAN", txtPANno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_GIR", txtGIRno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_TAT", txtTANno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_BANK", txtICICIACno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_LST", txtlstno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_WCT", txtwctno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_CST", txtcstno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_STCIRCLE", txtstcircle.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_ADDRESS", txtaddress.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_REM", txtrem.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_STAT", ddlstatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Function ValidateVendorCode()
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_VENDOR")
        sp.Command.AddParameter("@VENDOR_CODE", txtcode.Text, DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function

    Private Sub ModifyVendor()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MODIFY_VENDOR")
        sp.Command.AddParameter("@VENDOR_CODE", cmbPKeys.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VENDOR_NAME", txtName.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_GRADE", cmbgrade.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VENDOR_CONSTITUTION", cmbconst.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VENDOR_CITY", cmbloc.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VENDOR_PINCODE", txtpincode.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_OPHN", txtoffphno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_MPHN", txtmobilephno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_RPHN", txtresiphno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_EMAIL", txtemail.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_PAN", txtPANno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_GIR", txtGIRno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_TAT", txtTANno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_BANK", txtICICIACno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_LST", txtlstno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_WCT", txtwctno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_CST", txtcstno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_STCIRCLE", txtstcircle.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_ADDRESS", txtaddress.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_REM", txtrem.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_STAT", ddlstatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub cmbPKeys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPKeys.SelectedIndexChanged
        If cmbPKeys.SelectedIndex > 0 Then
            LblFinalStatus.Text = ""
            BindDetails()
            txtcode.Enabled = False
        Else
            Cleardata()
        End If
    End Sub

    Private Sub BindDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_DETAILS_VENDOR")
        sp.Command.AddParameter("@VENDOR_CODE", cmbPKeys.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtcode.Text = ds.Tables(0).Rows(0).Item("AVR_CODE")
            txtName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME")
            cmbgrade.ClearSelection()
            cmbgrade.Items.FindByValue(ds.Tables(0).Rows(0).Item("AVR_GRADE")).Selected = True
            cmbconst.ClearSelection()
            cmbconst.Items.FindByValue(ds.Tables(0).Rows(0).Item("AVR_CONSTITUTION")).Selected = True
            cmbloc.ClearSelection()
            cmbloc.Items.FindByValue(ds.Tables(0).Rows(0).Item("AVR_CITY")).Selected = True
            txtpincode.Text = ds.Tables(0).Rows(0).Item("AVR_PINCODE")
            txtoffphno.Text = ds.Tables(0).Rows(0).Item("AVR_OFFICE_PHNO")
            txtmobilephno.Text = ds.Tables(0).Rows(0).Item("AVR_MOBILE_PHNO")
            txtresiphno.Text = ds.Tables(0).Rows(0).Item("AVR_RESIDENCE_PHNO")
            txtemail.Text = ds.Tables(0).Rows(0).Item("AVR_EMAIL")
            txtPANno.Text = ds.Tables(0).Rows(0).Item("AVR_PAN_NO")
            txtGIRno.Text = ds.Tables(0).Rows(0).Item("AVR_GIR_NO")
            txtTANno.Text = ds.Tables(0).Rows(0).Item("AVR_TAN_NO")
            txtICICIACno.Text = ds.Tables(0).Rows(0).Item("AVR_BANKAC_NO")
            txtlstno.Text = ds.Tables(0).Rows(0).Item("AVR_LSTNO")
            txtwctno.Text = ds.Tables(0).Rows(0).Item("AVR_WSTNO")
            txtcstno.Text = ds.Tables(0).Rows(0).Item("AVR_CSTNO")
            txtstcircle.Text = ds.Tables(0).Rows(0).Item("AVR_STCIRCLE")
            txtaddress.Text = ds.Tables(0).Rows(0).Item("AVR_ADDR")
            txtrem.Text = ds.Tables(0).Rows(0).Item("AVR_REMARKS")
            ddlstatus.ClearSelection()
            ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AVR_STA_ID")).Selected = True

        End If
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

End Class
