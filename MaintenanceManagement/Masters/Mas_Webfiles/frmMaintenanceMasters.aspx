<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMaintenanceMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMaintenanceMasters" Title="Maintenance Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap-select.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/amantra.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/font-awesome/css/font-awesome.min.css")%>' rel="stylesheet" />
    <link rel="stylesheet" href='<%= Page.ResolveUrl("~/BootStrapCSS/datepicker.css")%>' />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <script language="javascript" type="text/javascript">
        function noway(go) {
            if (document.all) {
                if (event.button == 2) {
                    alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                    return false;
                }
            }
            if (document.layers) {
                if (go.which == 3) {
                    alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = noway;
    </script>

</head>
<body>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Maintenance Master 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/MaintenanceManagement/MaintenanceMaster/frmAssetGroups.aspx">Asset Group Master</asp:HyperLink>
                                </div>

                            </div>

                            <div class="col-md-6 ">
                                <div class="form-group">

                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/MaintenanceManagement/MaintenanceMaster/frmAssetGroupsType.aspx">Asset Group Type Master</asp:HyperLink>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/MaintenanceManagement/MaintenanceMaster/frmAssetBrands.aspx"> Asset Brand Master</asp:HyperLink>
                                </div>

                            </div>

                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/MaintenanceManagement/MaintenanceMaster/frmAssetModels.aspx">Asset Model Master</asp:HyperLink>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmMasVendor.aspx">Vendor Master</asp:HyperLink></td>
                                </div>

                            </div>

                            <div class="col-md-6 ">
                                <div class="form-group">

                                    <asp:HyperLink ID="hlkFloor" runat="server" NavigateUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmAssetMaster.aspx">Asset Master</asp:HyperLink>
                                </div>

                            </div>
                        </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-datepicker.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/ObjectKeys.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/wz_tooltip.js")%>' type="text/javascript"></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" temp_src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" temp_src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" temp_src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>
</html>

