Imports System.Data
Imports System.Data.SqlClient
Imports System

Partial Class Masters_Mas_Webfiles_frmAssetMaster
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim strAstID As String

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_ASSETS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvAsset.DataSource = ds
        gvAsset.DataBind()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtDOP.Attributes.Add("readonly", "readonly")
        txtWaranty.Attributes.Add("readonly", "readonly")
        'Put user code to initialize the page here
        'txtDOP.Attributes.Add("onClick", "displayDatePicker('" + txtDOP.ClientID + "')")
        'txtDOP.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txtWaranty.Attributes.Add("onClick", "displayDatePicker('" + txtWaranty.ClientID + "')")
        'txtWaranty.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        If Not IsPostBack Then
            txtHidTodayDt.Text = getoffsetdate(Date.Today)
            LblFinalStatus.Visible = False
            rbActions.Checked = True
            LoadData("Group", cmbAstGroups)
            LoadData("VENDOR", cmbAstVendor)
            LoadData("COUNTRY", cmbCountry)
            BindGrid()
        End If
        'If Not IsPostBack Then
        'txtHidTodayDt.Text =  getoffsetdate(Date.Today)
        'LblFinalStatus.Visible = False
        'rbActions.Items(0).Selected = True
        'LoadData("Group", cmbAstGroups)
        'LoadData("VENDOR", cmbAstVendor)
        'LoadData("COUNTRY", cmbCountry)
        'End If
    End Sub

    Private Sub LoadData(ByVal STR As String, ByVal cmbDestination As DropDownList, Optional ByVal cmbSource As DropDownList = Nothing)
        Dim TextField As String = ""
        Dim ValueField As String = ""
        Dim ds As New DataSet
        Select Case STR
            Case "Group"
                ds = New DataSet
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_GRP")
                ValueField = "AAG_CODE"
                TextField = "AAG_NAME"
            Case "GroupType"
                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@AGT_AGPM_ID", SqlDbType.NVarChar)
                param(0).Value = cmbSource.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_GRPTYPE_ASSET", param)
                ValueField = "AGT_CODE"
                TextField = "AGT_NAME"
            Case "BRAND"
                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@AAB_AGTM_ID", SqlDbType.NVarChar)
                param(0).Value = cmbSource.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_BRAND_ASSET", param)
                ValueField = "AAB_CODE"
                TextField = "AAB_NAME"

            Case "MODEL"
                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@AAM_ABM_ID", SqlDbType.NVarChar)
                param(0).Value = cmbSource.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_MODEL_ASSET", param)
                'If ds.Tables(0).Rows.Count > 0 Then
                '    cmbDestination.Items.Insert(0, "--Select--")
                '    cmbDestination.Items.Insert(1, "--All--")
                'End If
                ValueField = "AAM_CODE"
                TextField = "AAM_NAME"
                'cmbAstModels.Items.Insert(1, "--All--")
            Case "VENDOR"
                ds = New DataSet
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_VENDOR_ASSET")
                ValueField = "AVR_CODE"
                TextField = "AVR_NAME"
            Case "COUNTRY"
                ds = New DataSet
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_COUNTRY_ASSET")
                ValueField = "CNY_CODE"
                TextField = "CNY_NAME"
                'Case "REGION"
                '    ds = New DataSet
                '    Dim param() As SqlParameter = New SqlParameter(0) {}
                '    param(0) = New SqlParameter("@RGN_CNY_ID", SqlDbType.NVarChar)
                '    param(0).Value = cmbSource.SelectedItem.Value
                '    ds = objsubsonic.GetSubSonicDataSet("MAS_GET_REGION_BY_CNTRY", param)
                '    ValueField = "RGN_CODE"
                '    TextField = "RGN_NAME"

                'Case "STATE"
                '    ds = New DataSet
                '    Dim param() As SqlParameter = New SqlParameter(0) {}
                '    param(0) = New SqlParameter("@STE_RGN_CODE", SqlDbType.NVarChar)
                '    param(0).Value = cmbSource.SelectedItem.Value
                '    ds = objsubsonic.GetSubSonicDataSet("MAS_GET_STATE_BY_REGION", param)
                '    ValueField = "STE_CODE"
                '    TextField = "STE_NAME"

            Case "CITY"
                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@CTY_CNY_CODE", SqlDbType.NVarChar)
                param(0).Value = cmbCountry.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_CITY_BY_COUNTRY", param)
                ValueField = "CTY_CODE"
                TextField = "CTY_NAME"
                'Case "GSC"
                '    ds = New DataSet
                '    Dim param() As SqlParameter = New SqlParameter(0) {}
                '    param(0) = New SqlParameter("@LCM_CTY_ID", SqlDbType.NVarChar)
                '    param(0).Value = cmbCity.SelectedItem.Value
                '    ds = objsubsonic.GetSubSonicDataSet("MAS_GET_GSC_ASSET", param)
                '    ValueField = "GSC_CODE"
                '    TextField = "GSC_NAME"
            Case "BUILDING"

                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@BDG_CTY_ID", SqlDbType.NVarChar)
                param(0).Value = cmbCity.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_BDG_ASSET", param)
                ValueField = "BDG_ADM_CODE"
                TextField = "BDG_NAME"

            Case "LOCATION"

                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@BDG_CTY_ID", SqlDbType.NVarChar)
                param(0).Value = cmbCity.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MN_GET_ALL_LOCATIONS_BY_CITY", param)
                ValueField = "LCM_CODE"
                TextField = "LCM_NAME"

            Case "TOWER"

                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@TWR_BDG_ID", SqlDbType.NVarChar)
                param(0).Value = cmbBldg.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_TWR_ASSET", param)
                ValueField = "TWR_ID"
                TextField = "TWR_NAME"

            Case "FLOOR"

                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@FLR_BDG_ID", SqlDbType.NVarChar)
                param(0).Value = cmbBldg.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_FLR_ASSET", param)
                ValueField = "FLR_CODE"
                TextField = "FLR_NAME"

            Case "WING"

                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(1) {}
                param(0) = New SqlParameter("@WNG_FLR_ID", SqlDbType.NVarChar)
                param(0).Value = cmbFloor.SelectedItem.Value
                param(1) = New SqlParameter("@WNG_BDG_ID", SqlDbType.NVarChar)
                param(1).Value = cmbBldg.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_WNG_ASSET", param)
                ValueField = "WNG_CODE"
                TextField = "WNG_NAME"

            Case "SPACE"

                ds = New DataSet
                Dim param() As SqlParameter = New SqlParameter(0) {}
                param(0) = New SqlParameter("@bdg_ID", SqlDbType.NVarChar)
                param(0).Value = cmbBldg.SelectedItem.Value
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_SPC_ASSET", param)
                ValueField = "SPC_ID"
                TextField = "SPC_NAME"

            Case "APC_ASSET_PROCUREMENTS"

                ds = New DataSet
                ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_ASSET_DES")
                ValueField = "AAP_SNO"
                TextField = "AAP_DESCRIPTION"

        End Select

        cmbDestination.DataSource = ds
        cmbDestination.DataValueField = ValueField
        cmbDestination.DataTextField = TextField
        cmbDestination.DataBind()
        Lblmsg.Visible = False
        If cmbDestination.Items.Count = 0 Then
            cmbDestination.Items.Clear()
            cmbDestination.Items.Insert(0, "--Select--")
        Else
            cmbDestination.Items.Insert(0, "--Select--")
            'If ds.Tables(0).Rows.Count > 0 Then
            'cmbAstModels.Items.Insert(1, "--All--")
            'End If
        End If
        Try
            cmbDestination.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub

    Private Sub InitialiseCombos(ByVal ParamArray lst() As DropDownList)
        Dim i As DropDownList
        For Each i In lst
            i.Items.Clear()
            i.Items.Insert(0, "--Select--")
        Next
    End Sub

    Public Sub ClearData()
        txtAstTitle.Text = ""
        txtrem.Text = ""
        txtAstCost.Text = ""
        txtAstPO.Text = ""
        txtAstSNO.Text = ""
        txtDOP.Text = ""
        txtWaranty.Text = ""
        txtInvoice.Text = ""
        Try
            Me.cmbCountry.Enabled = True
            'Me.cmbState.Enabled = True
            'Me.cmbReg.Enabled = True
            Me.cmbCity.Enabled = True
            'Me.cmbGSC.Enabled = True
            Me.cmbBldg.Enabled = True
            Me.cmbFloor.Enabled = True
            Me.cmbWing.Enabled = True
            'Me.cmbSpace.Enabled = True
            Me.cmbAstGroups.Enabled = True
            Me.cmbAstGroupsTypes.Enabled = True
            Me.cmbAstBrands.Enabled = True
            Me.cmbAstModels.Enabled = True
            Me.cmbAstStatus.Enabled = True
            Me.cmbAstVendor.Enabled = True
            'Me.cmbassets.SelectedIndex = 0
            Me.cmbAstGroups.SelectedIndex = 0
            Me.cmbAstGroupsTypes.SelectedIndex = 0
            Me.cmbAstBrands.SelectedIndex = 0
            Me.cmbAstModels.SelectedIndex = 0
            Me.cmbAstVendor.SelectedIndex = 0
            Me.cmbAstStatus.SelectedIndex = 0
            Me.cmbCountry.SelectedIndex = 0
            ' Me.cmbState.SelectedIndex = 0
            ' Me.cmbReg.SelectedIndex = 0
            Me.cmbCity.SelectedIndex = 0
            'Me.cmbGSC.SelectedIndex = 0
            Me.cmbBldg.SelectedIndex = 0
            Me.cmbFloor.SelectedIndex = 0
            Me.cmbWing.SelectedIndex = 0
            'Me.cmbSpace.SelectedIndex = 0
            Me.cmbCountry.SelectedIndex = 0
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmbAstGroups_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAstGroups.SelectedIndexChanged
        lblMessage.Text = ""
        If cmbAstGroups.SelectedItem.Value = "--Select--" Then
            InitialiseCombos(cmbAstGroupsTypes, cmbAstBrands, cmbAstModels)
        Else
            LoadData("GroupType", cmbAstGroupsTypes, cmbAstGroups)
            InitialiseCombos(cmbAstBrands, cmbAstModels)
        End If
    End Sub

    Private Sub cmbAstGroupsTypes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAstGroupsTypes.SelectedIndexChanged
        If cmbAstGroupsTypes.SelectedItem.Text = "--Select--" Then
            InitialiseCombos(cmbAstBrands, cmbAstModels)
        Else
            LoadData("BRAND", cmbAstBrands, cmbAstGroupsTypes)
            InitialiseCombos(cmbAstModels)
        End If
    End Sub

    Private Sub cmbAstBrands_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAstBrands.SelectedIndexChanged
        If cmbAstBrands.SelectedItem.Text = "--Select--" Then
            InitialiseCombos(cmbAstModels)
        Else
            LoadData("MODEL", cmbAstModels, cmbAstBrands)
            If cmbAstModels.Items.Count > 1 Then
                cmbAstModels.Items.Insert(1, "--All--")
            End If
        End If
    End Sub

    Private Sub cmbCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCountry.SelectedIndexChanged
        If cmbCountry.SelectedItem.Value = "--Select--" Then
            'InitialiseCombos(cmbReg, cmbCity, cmbGSC, cmbBldg, cmbFloor, cmbWing, cmbSpace)
            InitialiseCombos(cmbCity, cmbBldg, cmbFloor, cmbWing) ', cmbSpace)
        Else
            'LoadData("REGION", cmbReg, )
            LoadData("CITY", cmbCity, cmbCountry)
            InitialiseCombos(cmbBldg, cmbFloor, cmbWing) ', cmbSpace)
        End If
    End Sub

    'Private Sub cmbReg_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbReg.SelectedIndexChanged

    '    If cmbReg.SelectedItem.Value = "--Select--" Then
    '        InitialiseCombos(cmbState, cmbCity, cmbGSC, cmbBldg, cmbFloor, cmbWing, cmbSpace)
    '    Else
    '        LoadData("STATE", cmbState, cmbReg)
    '        InitialiseCombos(cmbCity, cmbGSC, cmbBldg, cmbFloor, cmbWing, cmbSpace)
    '    End If
    'End Sub

    'Private Sub cmbState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbState.SelectedIndexChanged

    '    If cmbState.SelectedItem.Value = "--Select--" Then
    '        InitialiseCombos(cmbCity, cmbGSC, cmbBldg, cmbFloor, cmbWing, cmbSpace)
    '    Else
    '        LoadData("CITY", cmbCity, cmbState)
    '        InitialiseCombos(cmbGSC, cmbBldg, cmbFloor, cmbWing, cmbSpace)
    '    End If


    'End Sub

    Private Sub cmbCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCity.SelectedIndexChanged

        If cmbCity.SelectedItem.Value = "--Select--" Then
            InitialiseCombos(cmbBldg, cmbFloor, cmbWing) ', cmbSpace)
        Else
            'LoadData("GSC", cmbGSC, cmbCity)
            'InitialiseCombos(cmbBldg, cmbFloor, cmbWing, cmbSpace)
            ' LoadData("BUILDING", cmbBldg, cmbCity)
            LoadData("LOCATION", cmbBldg, cmbCity)
            InitialiseCombos(cmbFloor, cmbWing) ', cmbSpace)
        End If

    End Sub

    'Private Sub cmbGSC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGSC.SelectedIndexChanged

    '    If cmbGSC.SelectedItem.Value = "--Select--" Then
    '        InitialiseCombos(cmbBldg, cmbFloor, cmbWing, cmbSpace)
    '    Else
    '        LoadData("BUILDING", cmbBldg, cmbGSC)
    '        InitialiseCombos(cmbFloor, cmbWing, cmbSpace)
    '    End If

    'End Sub

    Private Sub cmbBldg_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBldg.SelectedIndexChanged

        If cmbBldg.SelectedItem.Value = "--Select--" Then
            InitialiseCombos(cmbFloor, cmbWing) ', cmbSpace)
        Else
            LoadData("FLOOR", cmbFloor, cmbBldg)
            InitialiseCombos(cmbWing) ', cmbSpace)
        End If
    End Sub

    Private Sub cmbFloor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFloor.SelectedIndexChanged

        If cmbFloor.SelectedItem.Value = "--Select--" Then
            InitialiseCombos(cmbWing) ', cmbSpace)
        Else
            LoadData("WING", cmbWing, cmbFloor)
            ' InitialiseCombos(cmbSpace)
        End If
    End Sub

    Private Sub cmbWing_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWing.SelectedIndexChanged

        If cmbWing.SelectedItem.Value = "--Select--" Then
            'InitialiseCombos(cmbSpace)
        Else
            ' LoadData("SPACE", cmbSpace, cmbWing)
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Try
        If Not IsValid Then
            Exit Sub
        End If
        If rbActions.Checked = True Then
            Dim valid As Integer
            valid = ValidateAsset()
            If valid = 1 Then
                InsertAssets()
            Else
                lblMessage.Text = "Asset Already Exists..."
                'Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=21")
                'Response.Write("<script language='javascript'>alert('Asset with same Serial Number is already exists in the database.Please Change it.');</script>")
                Exit Sub
            End If
        Else
            ModifyAssets()
            ' Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=22")
        End If
    End Sub

    Private Function ValidateAsset()
        Dim Valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_ASSET")
        sp.Command.AddParameter("@ASSET_CODE", txtHiddenSNO.Text, DbType.String)
        Valid = sp.ExecuteScalar()
        Return Valid
    End Function

    Sub InsertAssets()

        'Checking Duplicates
        Dim intRunNo As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "asset_procurements")

        'strSQL = "SELECT ISNULL(max(AAP_RUNNO),0) as RunNumber FROM APC_ASSET_PROCUREMENTS"
        ObjDR = sp.GetReader()

        While ObjDR.Read
            intRunNo = ObjDR("RunNumber") + 1
        End While

        ObjDR.Close()

        'Generating AssetID for Asset

        If cmbAstGroups.SelectedItem.Value <> "--Select--" And cmbAstGroupsTypes.SelectedItem.Value <> "--Select--" And cmbAstBrands.SelectedItem.Value <> "--Select--" And cmbAstModels.SelectedItem.Value <> "--Select--" Then
            strAstID = Trim(cmbAstGroups.SelectedItem.Text & "/" & Trim(cmbAstGroupsTypes.SelectedItem.Text) & "/" & Trim(cmbAstBrands.SelectedItem.Text)) & "/" & Trim(cmbAstModels.SelectedItem.Text) & "/" & CStr(intRunNo).PadLeft(5, "0")
        Else
            LblFinalStatus.Text = "Select Asset Group/GroupType/Brand/Model !"
            LblFinalStatus.Visible = True
        End If
        Dim ivendor As String = ""
        If (cmbAstVendor.SelectedIndex = 0) Then
            ivendor = 0
        Else
            ivendor = cmbAstVendor.SelectedValue

        End If
        Dim sSpcid As String
        'If (cmbSpace.SelectedIndex = 0) Then
        sSpcid = "NA"
        'Else
        'sSpcid = cmbSpace.SelectedValue
        'End If
        'When new Asset is added---AMC_STA_ID=0 and PVM_STA_ID=0'
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_INSRT_ASEET_ALL")             'MAS_INSRT_ASEET
        sp1.Command.AddParameter("@AAP_DESCRIPTION", Trim(Replace(txtAstTitle.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_SNO", strAstID, DbType.String)
        sp1.Command.AddParameter("@AAP_CODE", strAstID, DbType.String)
        sp1.Command.AddParameter("@AAP_GROUP_ID", cmbAstGroups.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_GROUPTYPE_ID", cmbAstGroupsTypes.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_BRAND_ID", cmbAstBrands.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_MODEL_ID", cmbAstModels.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_PO_ID", Trim(Replace(txtAstPO.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_INVOICE_NO", Trim(Replace(txtInvoice.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_INS_COST", Trim(Replace(txtAstCost.Text, "'", "''")), DbType.String)
        'sp1.Command.AddParameter("@AAP_INS_COST", Trim(Replace(txtAstCost.Text, "'", "''")), DbType.Decimal)
        sp1.Command.AddParameter("@AAP_VENDOR", ivendor, DbType.String)
        sp1.Command.AddParameter("@AAP_COUNTRY", cmbCountry.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_REGION", "NA", DbType.String)
        sp1.Command.AddParameter("@AAP_STATE", "NA", DbType.String)
        sp1.Command.AddParameter("@AAP_CITY", cmbCity.SelectedValue, DbType.String)
        sp1.Command.AddParameter("@AAP_GSC", "NA", DbType.String)
        sp1.Command.AddParameter("@AAP_BDG_ID", cmbBldg.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_FLR_ID", cmbFloor.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_WNG_ID", cmbWing.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_SPC_ID", sSpcid, DbType.String)
        sp1.Command.AddParameter("@APP_DT_PURCHASE", txtDOP.Text, DbType.String)
        'sp1.Command.AddParameter("@APP_DT_PURCHASE", txtDOP.Text, DbType.DateTime)
        sp1.Command.AddParameter("@AAP_DT_ENTRY", getoffsetdatetime(DateTime.Now).Date, DbType.DateTime)
        sp1.Command.AddParameter("@AAP_ENTERED_BY", Session("UID"), DbType.String)
        sp1.Command.AddParameter("@AAP_AST_STATUS", cmbAstStatus.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_REM", Trim(Replace(txtrem.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_AMC_STA", 0, DbType.Int32)
        sp1.Command.AddParameter("@AAP_PVM_STA", 0, DbType.Int32)
        sp1.Command.AddParameter("@AAP_WRNT_DATE", txtWaranty.Text, DbType.String)
        sp1.ExecuteScalar()
        BindGrid()
        'strSQL = " exec MAS_INSRT_ASEET '" & Trim(Replace(txtAstTitle.Text, "'", "''")) & "','" & strAstID & "','" & strAstID & "'," & cmbAstGroups.SelectedItem.Value & "," & cmbAstGroupsTypes.SelectedItem.Value & "," & cmbAstBrands.SelectedItem.Value & "," & cmbAstModels.SelectedItem.Value & ",'" & Trim(Replace(txtAstPO.Text, "'", "''")) & "','" & Trim(Replace(txtInvoice.Text, "'", "''")) & "','" & Trim(Replace(txtAstCost.Text, "'", "''")) & "'," & ivendor & "," & cmbCountry.SelectedItem.Value & "," & cmbReg.SelectedItem.Value & "," & cmbState.SelectedItem.Value & "," & cmbCity.SelectedItem.Value & "," & cmbGSC.SelectedItem.Value & "," & cmbBldg.SelectedItem.Value & "," & cmbFloor.SelectedItem.Value & "," & cmbWing.SelectedItem.Value & ",'" & sSpcid & "','" & Trim(Replace(txtDOP.Text, "'", "''")) & "','" & getoffsetdatetime(DateTime.Now) & "','" & Session("uid") & "','" & cmbAstStatus.SelectedItem.Text & "','" & Trim(Replace(txtrem.Text, "'", "''")) & "',0,0"
        'SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=69", False)
        ClearData()
        lblMessage.Visible = True
        lblMessage.Text = "New Asset Added Successfully..."

    End Sub

    Private Sub ModifyAssets()

        Dim intRunNo As Integer
        Dim strAssetSNO As String

        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_GET_RUNNO_ASSET")
        SP.Command.AddParameter("@AAP_SNO", cmbassets.SelectedItem.Value, DbType.String)

        ObjDR = SP.GetReader()

        While ObjDR.Read
            intRunNo = ObjDR("RunNumber")
            strAssetSNO = ObjDR("AAP_SNO")
        End While

        ObjDR.Close()
        Lblmsg.Visible = False

        Dim ivendor As String = ""
        If (cmbAstVendor.SelectedIndex = 0) Then
            ivendor = 0
        Else
            ivendor = cmbAstVendor.SelectedValue

        End If
        Dim sSpcid As String
        'If (cmbSpace.SelectedIndex = 0) Then
        sSpcid = "NA"
        'Else
        'sSpcid = cmbSpace.SelectedValue
        'End If
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_UPDATE_ASEET")

        sp1.Command.AddParameter("@AAP_SNO", Trim(Replace(txtAstSNO.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_PO_ID", Trim(Replace(txtAstPO.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_INVOICE_NO", Trim(Replace(txtInvoice.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_INS_COST", Trim(Replace(txtAstCost.Text, "'", "''")), DbType.Decimal)
        sp1.Command.AddParameter("@AAP_VENDOR", ivendor, DbType.String)
        sp1.Command.AddParameter("@AAP_COUNTRY", cmbCountry.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_REGION", "NA", DbType.String)
        sp1.Command.AddParameter("@AAP_STATE", "NA", DbType.String)
        sp1.Command.AddParameter("@AAP_CITY", cmbCity.SelectedValue, DbType.String)
        'sp1.Command.AddParameter("@AAP_CITY", cmbCity.SelectedValue, DbType.Decimal)
        sp1.Command.AddParameter("@AAP_GSC", "NA", DbType.String)
        sp1.Command.AddParameter("@AAP_BDG_ID", cmbBldg.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_FLR_ID", cmbFloor.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_WNG_ID", cmbWing.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_SPC_ID", sSpcid, DbType.String)
        sp1.Command.AddParameter("@APP_DT_PURCHASE", txtDOP.Text, DbType.String)
        'sp1.Command.AddParameter("@APP_DT_PURCHASE", txtDOP.Text, DbType.DateTime)
        sp1.Command.AddParameter("@AAP_DT_ENTRY", getoffsetdatetime(DateTime.Now).Date, DbType.DateTime)
        sp1.Command.AddParameter("@AAP_ENTERED_BY", Session("UID"), DbType.String)
        sp1.Command.AddParameter("@AAP_AST_STATUS", cmbAstStatus.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AAP_REM", Trim(Replace(txtrem.Text, "'", "''")), DbType.String)
        sp1.Command.AddParameter("@AAP_RUNNO", intRunNo, DbType.Int32)
        sp1.Command.AddParameter("@AAP_WRNT_DATE", txtWaranty.Text, DbType.String)
        sp1.ExecuteScalar()
        BindGrid()
        'Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=70", False)
        ClearData()
        cmbassets.SelectedIndex = 0
        lblMessage.Visible = True
        lblMessage.Text = "Asset Modified Successfully..."
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            btnSubmit.Text = "Add"
            LblPageStatus.Text = "Add"
            ClearData()
            lblMessage.Text = ""
            LblFinalStatus.Text = ""
            'lblast.Visible = False
            'cmbassets.Visible = False
            trgroup.Visible = False
            txtAstTitle.ReadOnly = False
        Else
            lblMessage.Text = ""
            btnSubmit.Text = "Modify"
            LblPageStatus.Text = "Modify"
            ClearData()
            'lblast.Visible = True
            'cmbassets.Visible = True
            trgroup.Visible = True
            LoadData("APC_ASSET_PROCUREMENTS", cmbassets)
            LblFinalStatus.Text = ""
            txtAstTitle.ReadOnly = True
        End If
          

    End Sub

    Protected Sub cmbassets_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbassets.SelectedIndexChanged
        lblMessage.Text = ""
        Try
            Dim i As Integer
            If cmbassets.SelectedItem.Value = "--Select--" Or cmbassets.SelectedItem.Value = "NA" Then
                txtrem.Text = ""
                txtAstTitle.Text = ""
                ClearData()
            Else
                Dim asgrp As String = ""
                Dim asgrptype As String = ""
                Dim asbrnd As String = ""
                Dim asmodel As String = ""
                Dim vnm As String = ""
                Dim astCountry As String = ""
                Dim astReg As String = ""
                Dim astState As String = ""
                Dim astCity As String = ""
                Dim astGSC As String = ""
                Dim astBldg As String = ""
                Dim astFloor As String = ""
                Dim astWing As String = ""
                Dim astSpc As String = ""
                Dim astStatus As String = ""
                Dim astVendor As String = ""

                Dim asset_id As SqlParameter = New SqlParameter("@asset_id", SqlDbType.VarChar, 50)
                asset_id.Value = cmbassets.SelectedItem.Value
                strSQL = "MAS_GET_ASEET_DTLS"
                ObjDR = DataReader(strSQL, asset_id)

                While ObjDR.Read
                    txtAstTitle.Text = ObjDR("AAP_DESCRIPTION").ToString
                    txtAstCost.Text = ObjDR("AAP_INS_COST")
                    txtAstPO.Text = ObjDR("AAP_PO_ID")
                    txtAstSNO.Text = ObjDR("AAP_SNO")
                    txtHiddenSNO.Text = ObjDR("AAP_SNO") 'Purpose of this is to validate the duplicate records while Modifying the Asset details
                    txtDOP.Text = ObjDR("APP_DT_PURCHASE")
                    txtWaranty.Text = ObjDR("AAP_WRNT_DATE")
                    txtInvoice.Text = ObjDR("AAP_INVOICE_NO")
                    txtrem.Text = ObjDR("AAP_REM")

                    astStatus = ObjDR("AAP_AST_STATUS")
                    astVendor = ObjDR("AAP_VENDOR")

                    asgrp = ObjDR("AAP_GROUP_ID").ToString
                    asgrptype = ObjDR("AAP_GROUPTYPE_ID").ToString
                    asbrnd = ObjDR("AAP_BRAND_ID").ToString
                    asmodel = ObjDR("AAP_MODEL_ID").ToString

                    astCountry = ObjDR("AAP_COUNTRY")
                    'astReg = ObjDR("AAP_REGION")
                    'astState = ObjDR("AAP_STATE")
                    astCity = ObjDR("AAP_CITY")
                    'astGSC = ObjDR("AAP_GSC")
                    astBldg = ObjDR("AAP_BDG_ID")
                    'astTower = ObjDR("AAP_TWR_ID")
                    astFloor = ObjDR("AAP_FLR_ID")
                    astWing = ObjDR("AAP_WNG_ID")
                    'astSpc = ObjDR("AAP_SPC_ID").ToString
                    'cmbAstStatus.Items.FindByValue(ObjDR("AAP_AST_STATUS")).Selected = True

                End While
                ObjDR.Close()

                LoadData("Group", cmbAstGroups)
                cmbAstGroups.ClearSelection()
                cmbAstGroups.Items.FindByValue(asgrp).Selected = True

                'LoadData("Group", cmbAstGroups)
                'For i = 0 To cmbAstGroups.Items.Count - 1
                '    If Trim(cmbAstGroups.Items(i).Value) = asgrp Then
                '        cmbAstGroups.SelectedIndex = i
                '    End If
                'Next

                LoadData("GroupType", cmbAstGroupsTypes, cmbAstGroups)
                cmbAstGroupsTypes.ClearSelection()
                cmbAstGroupsTypes.Items.FindByValue(asgrptype).Selected = True



                LoadData("BRAND", cmbAstBrands, cmbAstGroupsTypes)
                cmbAstBrands.ClearSelection()
                cmbAstBrands.Items.FindByValue(asbrnd).Selected = True


                LoadData("MODEL", cmbAstModels, cmbAstBrands)
                cmbAstModels.ClearSelection()
                cmbAstModels.Items.FindByValue(asmodel).Selected = True


                LoadData("COUNTRY", cmbCountry)
                cmbCountry.ClearSelection()
                cmbCountry.Items.FindByValue(astCountry).Selected = True

                'LoadData("REGION", cmbReg, cmbCountry)
                'cmbReg.ClearSelection()
                'cmbReg.Items.FindByValue(astReg).Selected = True


                'LoadData("STATE", cmbState, cmbReg)
                'cmbState.ClearSelection()
                'cmbState.Items.FindByValue(astState).Selected = True

                LoadData("CITY", cmbCity, cmbCountry)
                cmbCity.ClearSelection()
                cmbCity.Items.FindByValue(astCity).Selected = True

                'LoadData("GSC", cmbGSC, cmbCity)
                'cmbGSC.ClearSelection()
                'cmbGSC.Items.FindByValue(astGSC).Selected = True

                ' LoadData("BUILDING", cmbBldg, cmbCity)
                LoadData("LOCATION", cmbBldg, cmbCity)
                cmbBldg.ClearSelection()
                cmbBldg.Items.FindByValue(astBldg).Selected = True


                LoadData("FLOOR", cmbFloor, cmbBldg)
                cmbFloor.ClearSelection()
                cmbFloor.Items.FindByValue(astFloor).Selected = True

                Try
                    LoadData("WING", cmbWing, cmbFloor)
                    cmbWing.ClearSelection()
                    cmbWing.Items.FindByValue(astWing).Selected = True

                Catch ex As Exception

                End Try

                'LoadData("SPACE", cmbSpace, cmbWing)
                'cmbSpace.ClearSelection()
                'cmbSpace.Items.FindByValue(astSpc).Selected = True

                LoadData("VENDOR", cmbAstVendor)
                cmbAstVendor.ClearSelection()
                cmbAstVendor.Items.FindByValue(astVendor).Selected = True

                cmbAstStatus.ClearSelection()
                cmbAstStatus.Items.FindByValue(astStatus).Selected = True

                cmbAstGroups.Enabled = False
                cmbAstGroupsTypes.Enabled = False
                cmbAstBrands.Enabled = False
                cmbAstModels.Enabled = False
            End If

        Catch ex As Exception
            'lblMessage.Text = "Asset Brand & Model Doesnot Exist For The Selected Asset! Please Contact Your Administrator!"
            lblMessage.Text = "Asset Details Are Not Correct... Please Contact Your Administrator..."
        End Try

    End Sub

    Protected Sub gvAsset_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvAsset.PageIndexChanging
        gvAsset.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub txtAstTitle_TextChanged(sender As Object, e As EventArgs) Handles txtAstTitle.TextChanged
        lblMessage.Text = ""
    End Sub

End Class
