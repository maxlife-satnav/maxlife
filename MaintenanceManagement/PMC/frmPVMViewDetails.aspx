<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPVMViewDetails.aspx.vb" Inherits="MaintenanceManagement_PMC_frmPVMViewDetails" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row" style="margin-left: -60px">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>View Plan Details
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                             <div class="col-md-12">
                                    <asp:DataGrid ID="PM_REQ_DATA" runat="server" AutoGenerateColumns="False" PageSize="10" AllowPaging="True"
                                        CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:BoundColumn DataField="PVD_ID" HeaderText="S.No.">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AST_ID" HeaderText="Asset Description">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PVD_PLANSCHD_DT" HeaderText="Schedule Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="STA_TITLE" HeaderText="Status">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                                    </asp:DataGrid>
                                </div>
                            </div>

                            <div id="pnlbutton">

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="cmdExcel" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export to Excel"></asp:Button>
                                            <asp:Button ID="btnprint" runat="server" CssClass="btn btn-primary custom-button-color" Text="Print"></asp:Button>
                                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="false" PostBackUrl="~/MaintenanceManagement/PMC/frmPVMViewPlan.aspx" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        $(document).ready(function () {
            $("#btnprint").click(function (e) {
                e.preventDefault();
                $("#pnlbutton").hide();
                window.print();
                $("#pnlbutton").show();
                return false;
            })
        });
    </script>
</body>
</html>
