<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPVMUpdateDetails.aspx.vb" Inherits="MaintenanceManagement_PMC_frmPVMUpdateDetails" %>

<%@ Register Src="Controls/PVMUpdateDetails.ascx" TagName="PVMUpdateDetails" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row" style="margin-right: -70px !important; margin-left: -70px !important;" > <%--style="margin-left: -85px"--%>
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Update Plan Details
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ScriptManager ID="ScriptManager1" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="True" CssClass="alert alert-danger"
                                ForeColor="Red" ShowMessageBox="false"></asp:ValidationSummary>
                            <uc1:PVMUpdateDetails ID="PVMUpdateDetails1" runat="server"></uc1:PVMUpdateDetails>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
