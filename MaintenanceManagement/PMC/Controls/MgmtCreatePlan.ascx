<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MgmtCreatePlan.ascx.vb"
    Inherits="MaintenanceManagement_PMC_Controls_MgmtCreatePlan" %>

<div class ="row">
    <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                      <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                    </div>
                </div>
            </div>
</div>
 


<asp:TextBox ID="txtHiddenDate" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>

<div id="pnlContainer" runat="server">

    <div id="PanPremise" runat="server">

        <div class="row">  
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="cboBuilding" Display="None" ErrorMessage="Please Select Location" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="cboBuilding" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Asset Group<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None" ErrorMessage="Please Select Asset Group " Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Asset Group Type<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Asset Brand<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlbrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Vendor<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="Comparevalidator10" runat="server" ControlToValidate="cboVendor11" Display="None" ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="cboVendor11" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>


              <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Contract<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="Comparevalidator1" runat="server" ControlToValidate="cboContract" Display="None" ErrorMessage="Please Select Contract" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="cboContract" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="row" id="PanSelAssets" runat="server">

        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <%--<div class="col-md-5">--%>
                    <label class="col-md-6 control-label">
                        <asp:ListBox ID="lstDisAssets" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                    </label>
                    <%-- </div>--%>
                    <div class="col-md-1">
                        <asp:Button ID="btnRight" runat="server" Text=">>" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnLeft" runat="server" Text="<<" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                    </div>
                    <div class="col-md-5">
                        <asp:ListBox ID="lstSelAssets" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="panAssetService" runat="server">

        <div id="pnlCon" runat="server" visible="false">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Vendor Name<span style="color: red;">*</span></label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Vendor Name"
                                ControlToValidate="txtConName" Display="None"></asp:RequiredFieldValidator>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtConName" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">No of Years<span style="color: red;">*</span></label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Enter Vendor Name"
                                ControlToValidate="txtNoofYears" Display="None"></asp:RequiredFieldValidator>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtNoofYears" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Contract Start Date<span style="color: red;">*</span></label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Contractor Start Date"
                                ControlToValidate="txtConStart" Display="None"></asp:RequiredFieldValidator>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtConStart" CssClass="form-control" ReadOnly="True" runat="server" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Contract End Date<span style="color: red;">*</span></label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please Select Contractor End Date"
                                ControlToValidate="txtConTo" Display="None"></asp:RequiredFieldValidator>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtConTo" CssClass="form-control" ReadOnly="True" runat="server" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="panPeriod" runat="server">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radDaily" runat="server" CssClass="clsRadioButton" Text="Daily"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">

                                <asp:RadioButton ID="radWeekly" runat="server" CssClass="clsRadioButton" Text="Weekly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radMonthly" runat="server" CssClass="clsRadioButton" Text="Monthly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radBiMonthly" runat="server" CssClass="clsRadioButton" Text="Bimonthly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radQuarterly" runat="server" CssClass="clsRadioButton" Text="Quarterly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                              <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radHalfYearly" runat="server" CssClass="clsRadioButton" Text="HalfYearly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radYearly" runat="server" CssClass="clsRadioButton" Text="Yearly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                             
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-md-3 control-label">
                    Update Before No of Days
                </label>
                <asp:TextBox ID="txtPriorDays" runat="server" ></asp:TextBox>
            </div>
            <br />

        </div>

        <div id="panAllPeriods" runat="server">

            <div id="panWeekly" runat="server">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Day of the Week<span style="color: red;"></span></label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="cboWWeek" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="2">Monday</asp:ListItem>
                                        <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                        <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                        <asp:ListItem Value="5">Thursday</asp:ListItem>
                                        <asp:ListItem Value="6">Friday</asp:ListItem>
                                        <asp:ListItem Value="7">Saturday</asp:ListItem>
                                        <asp:ListItem Value="1">Sunday</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div id="panMonthly" runat="server">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Day of the Month<span style="color: red;"></span></label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="cboMDate" runat="server" CssClass="selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div id="panBiMonthly" runat="server">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Month<span style="color: red;"></span></label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="cboBMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">Febuary</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Day<span style="color: red;"></span></label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="cboBday" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div id="panQuarterly" runat="server">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">First Quarter </label>
                                <label class="col-md-3 control-label">Second Quarter</label>
                                <label class="col-md-3 control-label">Third Quarter</label>
                                <label class="col-md-3 control-label">Fourth Quarter</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ1Month" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                    </asp:DropDownList>
                                </label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ2Month" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ3Month" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ4Month" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">Febuary</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                    </asp:DropDownList></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ1Date" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ2Date" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ3Date" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboQ4Date" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            
            <div id="panHalfyearly" runat="server">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">First Half </label>
                                <label class="col-md-3 control-label">Second Half</label>
                             </div>
                        </div>
                    </div>
                </div>
                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                 <div class="row">
                                     <label class="col-md-3 control-label">
                                         <asp:DropDownList ID="cboFirstHalfMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                             <asp:ListItem Value="4">April</asp:ListItem>
                                             <asp:ListItem Value="5">May</asp:ListItem>
                                             <asp:ListItem Value="6">June</asp:ListItem>
                                             <asp:ListItem Value="7">July</asp:ListItem>
                                             <asp:ListItem Value="8">August</asp:ListItem>
                                             <asp:ListItem Value="9">September</asp:ListItem>
                                         </asp:DropDownList>
                                     </label>

                                     <label class="col-md-3 control-label">
                                         <asp:DropDownList ID="cboSecondHalfMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                             <asp:ListItem Value="10">October</asp:ListItem>
                                             <asp:ListItem Value="11">November</asp:ListItem>
                                             <asp:ListItem Value="12">December</asp:ListItem>
                                             <asp:ListItem Value="1">January</asp:ListItem>
                                             <asp:ListItem Value="2">Febuary</asp:ListItem>
                                             <asp:ListItem Value="3">March</asp:ListItem>
                                         </asp:DropDownList></label>

                                 </div>
                             </div>
                         </div>
                     </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboFHDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboSHDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                           
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div id="panYearly" runat="server">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Month<span style="color: red;"></span></label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="cboYMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">Febuary</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Day<span style="color: red;"></span></label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="cboYDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div id="panDaily" runat="server">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Time<span style="color: red;"></span></label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="cboHour" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                            <asp:RequiredFieldValidator ID="rfvFromDt" runat="server" ErrorMessage="Please Select The From Date"
                                ControlToValidate="txtFromDate" Display="None"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvDates" runat="server" ErrorMessage="Please Select To Date Greater Than From Date"
                                ControlToValidate="txtFromDate" Display="None" Operator="LessThan" ControlToCompare="txtToDate"
                                Type="Date"></asp:CompareValidator>
                            <div class="col-md-7">

                                <div class='input-group date' id='fromdate'>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                            <asp:RequiredFieldValidator ID="rfvToDt" runat="server" ErrorMessage="Please Select The To Date"
                                ControlToValidate="txtToDate" Display="None"></asp:RequiredFieldValidator>
                            <div class="col-md-7">

                                <div class='input-group date' id='todate'>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"></asp:Button>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>



