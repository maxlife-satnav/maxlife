<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PVMAmenDetails.ascx.vb"
    Inherits="MaintenanceManagement_PMC_Controls_PVMAmenDetails" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" ForeColor="Red" class="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>

 <div id="PanPremise" runat="server">

        <div class="row">  
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select Location<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="cboBuilding" Display="None" ErrorMessage="Please Select Location" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="cboBuilding" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" >
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Asset Group<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None" ErrorMessage="Please Select Asset Group " Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Asset Group Type<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Asset Brand<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlbrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select Vendor<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="Comparevalidator10" runat="server" ControlToValidate="cboVendor11" Display="None" ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="cboVendor11" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>


              <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select Contract<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="Comparevalidator1" runat="server" ControlToValidate="cboContract" Display="None" ErrorMessage="Please Select Contract" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="cboContract" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

  <div class="row" id="PanSelAssets" runat="server">

        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <%--<div class="col-md-5">--%>
                    <label class="col-md-6 control-label">
                        <asp:ListBox ID="lstDisAssets" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                    </label>
                    <%-- </div>--%>
                    <div class="col-md-1">
                        <asp:Button ID="btnRight" runat="server" Text=">>" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnLeft" runat="server" Text="<<" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                    </div>
                    <div class="col-md-5">
                        <asp:ListBox ID="lstSelAssets" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contract Start Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Contractor Start Date"
                    ControlToValidate="txtConStart" Display="None"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtConStart" CssClass="form-control" ReadOnly="True" runat="server" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contract End Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please Select Contractor End Date"
                    ControlToValidate="txtConTo" Display="None"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtConTo" CssClass="form-control" ReadOnly="True" runat="server" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <label class="col-md-2 control-label">
                    <asp:RadioButton ID="radDaily" runat="server" CssClass="clsRadioButton" Text="Daily" AutoPostBack="True"></asp:RadioButton>
                </label>
                <label class="col-md-2 control-label">
                    <asp:RadioButton ID="radWeekly" runat="server" CssClass="clsRadioButton" Text="Weekly" AutoPostBack="True"></asp:RadioButton>
                </label>
                <label class="col-md-2 control-label">
                    <asp:RadioButton ID="radMonthly" runat="server" CssClass="clsRadioButton" Text="Monthly" AutoPostBack="True"></asp:RadioButton>
                </label>
                <label class="col-md-2 control-label">
                    <asp:RadioButton ID="radBiMonthly" runat="server" CssClass="clsRadioButton" Text="Bimonthly" AutoPostBack="True"></asp:RadioButton>
                </label>
                <label class="col-md-2 control-label">
                    <asp:RadioButton ID="radQuarterly" runat="server" CssClass="clsRadioButton" Text="Quarterly" AutoPostBack="True"></asp:RadioButton>
                </label>


                 <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radHalfYearly" runat="server" CssClass="clsRadioButton" Text="HalfYearly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>

                <label class="col-md-2 control-label">
                    <asp:RadioButton ID="radYearly" runat="server" CssClass="clsRadioButton" Text="Yearly" AutoPostBack="True"></asp:RadioButton>
                </label>
            </div>
        </div>
    </div>
</div>

   <div class="row">
                <label class="col-md-3 control-label">
                    Update Before No of Days
                </label>
                <asp:TextBox ID="txtPriorDays" runat="server" ></asp:TextBox>
            </div>
            <br />

<div id="panWeekly" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Day of the Week<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="W_DrpDwnWeek" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="2">Monday</asp:ListItem>
                            <asp:ListItem Value="3">Tuesday</asp:ListItem>
                            <asp:ListItem Value="4">Wednesday</asp:ListItem>
                            <asp:ListItem Value="5">Thursday</asp:ListItem>
                            <asp:ListItem Value="6">Friday</asp:ListItem>
                            <asp:ListItem Value="7">Saturday</asp:ListItem>
                            <asp:ListItem Value="1">Sunday</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="panMonthly" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Day of the Month<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="M_DrpDwnDate" runat="server" CssClass="selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="panBiMonthly" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Month<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="B_DrpDwnMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">Febuary</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Day<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="B_DrpDwnDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="panYearly" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Month<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="Y_DrpDwnMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">Febuary</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Day<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="Y_DrpDwnDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="panQuarterly" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3 control-label">First Quarter </label>
                    <label class="col-md-3 control-label">Second Quarter</label>
                    <label class="col-md-3 control-label">Third Quarter</label>
                    <label class="col-md-3 control-label">Fourth Quarter</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q1_DrpDwnMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q2_DrpDwnMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q3_DrpDwnMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q4_DrpDwnMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">Febuary</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q1_DrpDwnDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </label>
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q2_DrpDwnDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </label>
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q3_DrpDwnDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </label>
                    <label class="col-md-3 control-label">
                        <asp:DropDownList ID="Q4_DrpDwnDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

  <div id="panHalfyearly" runat="server">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">First Half </label>
                                <label class="col-md-3 control-label">Second Half</label>
                             </div>
                        </div>
                    </div>
                </div>
                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                 <div class="row">
                                     <label class="col-md-3 control-label">
                                         <asp:DropDownList ID="cboFirstHalfMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                             <asp:ListItem Value="4">April</asp:ListItem>
                                             <asp:ListItem Value="5">May</asp:ListItem>
                                             <asp:ListItem Value="6">June</asp:ListItem>
                                             <asp:ListItem Value="7">July</asp:ListItem>
                                             <asp:ListItem Value="8">August</asp:ListItem>
                                             <asp:ListItem Value="9">September</asp:ListItem>
                                         </asp:DropDownList>
                                     </label>

                                     <label class="col-md-3 control-label">
                                         <asp:DropDownList ID="cboSecondHalfMonth" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                             <asp:ListItem Value="10">October</asp:ListItem>
                                             <asp:ListItem Value="11">November</asp:ListItem>
                                             <asp:ListItem Value="12">December</asp:ListItem>
                                             <asp:ListItem Value="1">January</asp:ListItem>
                                             <asp:ListItem Value="2">Febuary</asp:ListItem>
                                             <asp:ListItem Value="3">March</asp:ListItem>
                                         </asp:DropDownList></label>

                                 </div>
                             </div>
                         </div>
                     </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboFHDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="cboSHDate" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                           
                            </div>
                        </div>
                    </div>
                </div>

            </div>

<div id="panDaily" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Time<span style="color: red;"></span></label>

                    <div class="col-md-7">
                        <asp:DropDownList ID="D_DrpDwnHour" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvFromDt" runat="server" ErrorMessage="Please Select The From Date"
                    ControlToValidate="txtFromDate" Display="None"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvDates" runat="server" ErrorMessage="Please Select To Date Greater Than From Date"
                    Display="None" ControlToCompare="txtToDate" ControlToValidate="txtFromDate" Operator="LessThan"
                    Type="Date"></asp:CompareValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvToDt" runat="server" ErrorMessage="Please Select The To Date"
                    ControlToValidate="txtToDate" Display="None"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='todate'>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"></asp:Button>
            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false"
                PostBackUrl="~/MaintenanceManagement/PMC/frmPVM_ChangePlan.aspx" Text="Back" />
        </div>
    </div>
</div>
