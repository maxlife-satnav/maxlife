Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_PMC_Controls_PVMUpdateDetails
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblWarn.Text = ""

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        txtExeDate.Attributes.Add("readonly", "readonly")
        txtBRKStartDt.Attributes.Add("readonly", "readonly")
        txtBRKEndDt.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            If Session("ViewOrModify") = "1" Then
                cmdSubmit.Enabled = False
            End If
            BindData()
            pnlBrkDwn.Visible = False
            txtDate.Text = getoffsetdate(Date.Today)
        End If
    End Sub

    Sub BindData()

        Dim flag
        flag = 1

        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@rid", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("rid")

        ds = New DataSet

        ds = ObjSubSonic.GetSubSonicDataSet("PVM_PMASSETFULLDETAILS_SP", param)
        Dim priordays As Integer
        priordays = ds.Tables(0).Rows(0).Item("PVM_UPDATE_PRIOR_DAYS")
        If ds.Tables(0).Rows.Count > 0 Then
            flag = 2
        Else
            flag = 1
        End If
        If flag = 1 Then
            'lblNote.Visible = True
            lblError.Visible = True
            lblError.Text = "No PVM Details To Update Today"
            'lblhead.Visible = True
            cmdSubmit.Visible = False
            pnlButton.Visible = False
            Exit Sub
        End If

        'Dim i As Int16
        'For i = 0 To ds.Tables(0).Rows.Count - 2
        '    Dim f, n As String
        '    f = ds.Tables(0).Rows(i).Item("PVD_PLANSCHD_DT")
        '    n = ds.Tables(0).Rows(i + 1).Item("PVD_PLANSCHD_DT")
        '    If f = n Then
        '        ds.Tables(0).Rows(i).Delete()
        '    End If
        'Next
        PM_REQ_DATA.DataSource = ds
        PM_REQ_DATA.DataBind()

        Dim cnt As Integer
        For cnt = 0 To PM_REQ_DATA.Items.Count() - 1
            If PM_REQ_DATA.Items(cnt).Cells(5).Text = "Closed" Or (CDate(PM_REQ_DATA.Items(cnt).Cells(4).Text).AddDays(-priordays)) > getoffsetdatetime(DateTime.Now) Then
                ' If PM_REQ_DATA.Items(cnt).Cells(5).Text = "Closed" Then
                Dim btn As New LinkButton
                btn = CType(PM_REQ_DATA.Items(cnt).Cells(0).Controls(0), LinkButton)
                btn.Enabled = False

            End If
        Next

    End Sub

    Sub PM_REQ_DATA_Paged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

        PM_REQ_DATA.CurrentPageIndex = e.NewPageIndex
        BindData()

    End Sub

    Private Sub PM_REQ_DATA_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles PM_REQ_DATA.ItemCommand

        Dim btn As LinkButton = CType(e.CommandSource, LinkButton)



        Dim gdSno, LnkVal, Referralcode, txtRef
        Dim icnt As Integer

        Dim gdLnkbtn As New LinkButton
        gdLnkbtn = CType(PM_REQ_DATA.Items(icnt).Cells(0).Controls(0), LinkButton)
        LnkVal = gdLnkbtn.Text


        Select Case Trim(btn.Text)
            Case "Update"

                Dim n As Integer
                Dim i As Integer
                Dim exeDate As String = e.Item.Cells(7).Text
                Dim exeTime As String = e.Item.Cells(8).Text
                Dim sprCost As String = e.Item.Cells(9).Text
                Dim lbrCost As String = e.Item.Cells(10).Text
                Dim rmks As String = e.Item.Cells(11).Text

                e.Item.BackColor = System.Drawing.Color.AliceBlue
                e.Item.ForeColor = System.Drawing.Color.Blue
                pnlButton.Visible = True
                txtExeDate.Text = ""
                txtExeTime.Text = ""
                txtSpr.Text = ""
                txtLbr.Text = ""
                txtRmks.Text = ""
                gdSno = PM_REQ_DATA.Items(icnt).Cells(1).Text

                sus(gdSno)

                btn.Text = "Done"
                chkBrk.Checked = False

            Case "Done"
                e.Item.BackColor = System.Drawing.Color.Empty
                e.Item.ForeColor = System.Drawing.Color.Empty
                pnlButton.Visible = False
                btn.Text = "Update"
            Case Else
        End Select

    End Sub

    Private Function sus(ByVal i)
        Dim j As String = i
        Return j
    End Function

    Private Sub cmdSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim rid = Request.QueryString("rid")
        If Val(txtSpr.Text) >= 0 Then
            If Val(txtLbr.Text) >= 0 Then
                If Val(txtExeTime.Text) >= 0 Then
                    Dim icnt As Integer
                    Dim a As Integer
                    Dim LnkVal As String
                    Dim gdLnkbtn As New LinkButton
                    For icnt = 0 To PM_REQ_DATA.Items.Count - 1

                        'COMMENT LINE IN GRID VIEW asp:EditCommandColumn
                        gdLnkbtn = CType(PM_REQ_DATA.Items(icnt).Cells(0).Controls(0), LinkButton)
                        LnkVal = gdLnkbtn.Text

                        If LnkVal = "Done" Then
                            a = PM_REQ_DATA.Items(icnt).Cells(1).Text
                            If txtExeDate.Text = String.Empty Then
                                txtExeDate.Text = getoffsetdate(Date.Today)
                            End If
                            If txtExeTime.Text = String.Empty Then
                                txtExeTime.Text = "00:00"
                            End If
                            If txtSpr.Text = String.Empty Then
                                txtSpr.Text = "0"
                            End If
                            If txtLbr.Text = String.Empty Then
                                txtLbr.Text = "0"
                            End If
                            If txtRmks.Text = String.Empty Then
                                txtRmks.Text = "0"
                            End If


                            Dim orgfilenameINC As String = Replace(Replace(fpBrowseIncDoc.FileName, " ", "_"), "&", "_")
                            Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
                            Dim repdocdatetimeINC As String = ""

                            If (fpBrowseIncDoc.HasFile) Then
                                Dim fileExt As String
                                fileExt = System.IO.Path.GetExtension(fpBrowseIncDoc.FileName)
                                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_INC_" & orgfilenameINC
                                fpBrowseIncDoc.PostedFile.SaveAs(filePath)
                                repdocdatetimeINC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "INC_" & orgfilenameINC
                            End If


                            If chkBrk.Checked = False Then
                                param = New SqlParameter(8) {}
                                param(0) = New SqlParameter("@PVD_PLANEND_TIME", SqlDbType.NVarChar, 200)
                                param(0).Value = Replace(Trim(txtExeTime.Text), "'", "''")
                                param(1) = New SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.DateTime)
                                param(1).Value = Replace(Trim(txtExeDate.Text), "'", "''")
                                param(2) = New SqlParameter("@PVD_PLANSPARES_COST", SqlDbType.NVarChar, 200)
                                param(2).Value = Replace(Trim(txtSpr.Text), "'", "''")
                                param(3) = New SqlParameter("@PVD_PLANLABOUR_COST", SqlDbType.NVarChar, 200)
                                param(3).Value = Replace(Trim(txtLbr.Text), "'", "''")
                                param(4) = New SqlParameter("@PVD_PLAN_REMARKS", SqlDbType.NVarChar, 500)
                                param(4).Value = Replace(Trim(txtRmks.Text), "'", "''")
                                param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                param(5).Value = 2 'in status table 3 indicates extended status, so modified to 2(Closed)
                                'param(5).Value = 3
                                param(6) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                param(6).Value = Request.QueryString("rid")
                                param(7) = New SqlParameter("@PVD_ID", SqlDbType.Int)
                                param(7).Value = a

                                param(8) = New SqlParameter("@PVD_UPLOADED_DOC", DbType.String)
                                param(8).Value = repdocdatetimeINC

                                'ObjSubSonic.GetSubSonicExecute("PVM_UPDATE_PLAN_DTLS", param)
                                ObjSubSonic.GetSubSonicExecute("MN_PVM_UPDATE_PLAN_DTLS", param)
                            Else
                                Dim strStTimeDt, strEndTimeDt As String

                                strStTimeDt = cboBRKStHr.SelectedItem.Text & ":" & cboBRKStMin.SelectedItem.Text & " " & txtBRKStartDt.Text
                                strEndTimeDt = cboBRKEndHr.SelectedItem.Text & ":" & cboBRKEndMin.SelectedItem.Text & " " & txtBRKEndDt.Text

                                param = New SqlParameter(12) {}
                                param(0) = New SqlParameter("@PVD_PLANEND_TIME", SqlDbType.NVarChar, 200)
                                param(0).Value = Replace(Trim(txtExeTime.Text), "'", "''")
                                param(1) = New SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.NVarChar, 200)
                                param(1).Value = Replace(Trim(txtExeDate.Text), "'", "''")
                                param(2) = New SqlParameter("@PVD_PLANSPARES_COST", SqlDbType.NVarChar, 200)
                                param(2).Value = Replace(Trim(txtSpr.Text), "'", "''")
                                param(3) = New SqlParameter("@PVD_PLANLABOUR_COST", SqlDbType.NVarChar, 200)
                                param(3).Value = Replace(Trim(txtLbr.Text), "'", "''")
                                param(4) = New SqlParameter("@PVD_PLAN_REMARKS", SqlDbType.NVarChar, 200)
                                param(4).Value = Replace(Trim(txtRmks.Text), "'", "''")
                                param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.NVarChar, 200)
                                param(5).Value = 2 'in status table 3 indicates extended status, so modified to 2(Closed)
                                'param(5).Value = 3
                                param(6) = New SqlParameter("@PVD_BRK_EXP", SqlDbType.NVarChar, 200)
                                param(6).Value = Replace(Trim(txtBrkExp.Text), "'", "''")
                                param(7) = New SqlParameter("@PVD_BRK_REM", SqlDbType.NVarChar, 200)
                                param(7).Value = Replace(Trim(txtBRKRem.Text), "'", "''")
                                param(8) = New SqlParameter("@PVD_BRK_ONDT", SqlDbType.NVarChar, 200)
                                param(8).Value = strStTimeDt
                                param(9) = New SqlParameter("@PVD_BRK_ENDDT", SqlDbType.NVarChar, 200)
                                param(9).Value = strEndTimeDt
                                param(10) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                param(10).Value = Request.QueryString("rid")
                                param(11) = New SqlParameter("@PVD_ID", SqlDbType.NVarChar, 200)
                                param(11).Value = a

                                param(12) = New SqlParameter("@PVD_UPLOADED_DOC", DbType.String)
                                param(12).Value = repdocdatetimeINC
                                'ObjSubSonic.GetSubSonicExecute("PVM_UPDATE_PLAN_DTLS_WTH_BREAKGE", param)
                                ObjSubSonic.GetSubSonicExecute("MN_PVM_UPDATE_PLAN_DTLS_WTH_BREAKGE", param)

                            End If
                            gdLnkbtn.Enabled = False

                        End If
                    Next

                    lblWarn.Text = ""
                    pnlButton.Visible = False

                    BindData()
                    Response.Redirect("frmPVMFinalpage.aspx?rid=" & rid & "&staid=Updated")
                Else
                    lblWarn.Text = "Enter Only Digits"
                End If
            Else
                lblWarn.Text = "Enter Only Digits"
            End If
        Else
            lblWarn.Text = "Enter Only Digits"
        End If
    End Sub

    Private Sub chkBrk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBrk.CheckedChanged

        If chkBrk.Checked = True Then
            pnlBrkDwn.Visible = True
            'For Each ctrl As Control In pnlBrkDwn.Controls
            '    ctrl.Visible = True
            'Next

        Else
            pnlBrkDwn.Visible = False
            pnlBrkDwn.Controls.Clear()
        End If

    End Sub

    Private Sub PM_REQ_DATA_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles PM_REQ_DATA.PageIndexChanged
        PM_REQ_DATA.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub


    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim filePath As String = CType(sender, LinkButton).CommandArgument
        If Not filePath = "NA" Then
            filePath = "~\UploadFiles\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\UploadFiles", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If

    End Sub

End Class
