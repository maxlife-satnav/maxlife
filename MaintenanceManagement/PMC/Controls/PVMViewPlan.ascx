<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PVMViewPlan.ascx.vb"
    Inherits="MaintenanceManagement_PMC_Controls_PVMViewPlan" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-7 control-label">Search by PM Plan ID/Location Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfAssetName" runat="server" ControlToValidate="txtAssetName"
                    Display="none" ErrorMessage="Please Enter PM Plan ID/Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-5">
                    <asp:TextBox ID="txtAssetName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                CausesValidation="true" TabIndex="2" />
        </div>
    </div>
</div>

<div class="row">
   <div class="col-md-12">
        
        <asp:DataGrid ID="PM_REQ_DATA" runat="server" PageSize="20" AutoGenerateColumns="False" EmptyDataText="No View Plan Found."
            AllowSorting="True" AllowPaging="True" CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:BoundColumn DataField="PVM_PLAN_ID" HeaderText="PM Plan Id" Visible="False">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="MPVM_PLAN_ID" HeaderText="PM Plan Id">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="BDG_NAME" HeaderText="Location">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="NAME" HeaderText="Asset Category">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>

                 <asp:BoundColumn DataField="AST_SUBCAT_NAME" HeaderText="Asset Sub Category">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>

                <asp:BoundColumn DataField="PVM_PLAN_FREQ" HeaderText="Plan Type">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                 <asp:HyperLinkColumn DataNavigateUrlField="MPVM_PLAN_ID" DataNavigateUrlFormatString="../frmPVMViewDetails.aspx?rid={0}" Text="View"
                    HeaderText="Details">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:HyperLinkColumn>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
        </asp:DataGrid>
    </div>
</div>

