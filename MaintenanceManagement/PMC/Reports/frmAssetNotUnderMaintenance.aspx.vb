Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class MaintenanceManagement_PMC_Reports_frmAssetNotUnderMaintenance
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        If Not Page.IsPostBack Then

            Dim flag As Integer

            Dim uid As String
            uid = Session("uid")
            flag = 1
            BindLocation()
            getassetcategory()

            getsubcategorybycat(ddlastCat.SelectedItem.Value)
            ddlastsubCat.SelectedIndex = 0

            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
            ddlAstBrand.SelectedIndex = 0

            getModelbycatsubcat()
            ddlAstModel.SelectedIndex = 0
            'ddlLocation.Items.Insert(0, New ListItem("--All--", ""))
            'ddlastCat.Items.Insert(0, New ListItem("--All--", ""))
            'ddlastsubCat.Items.Insert(0, New ListItem("--All--", ""))
            'ddlAstBrand.Items.Insert(0, New ListItem("--All--", ""))
            'ddlAstModel.Items.Insert(0, New ListItem("--All--", ""))

            'ddlastsubCat.SelectedIndex = If(ddlastsubCat.Items.Count > 1, 1, 0)
            ' ddlAstBrand.SelectedIndex = If(ddlAstBrand.Items.Count > 1, 1, 0)
            ' ddlAstModel.SelectedIndex = If(ddlAstModel.Items.Count > 1, 1, 0)
            'ddlastCat.Items.Insert("0", "--All--")
            'ddlastsubCat.Items.Insert("0", "--All--")
            'ddlAstBrand.Items.Insert("0", "--All--")
            'ddlAstModel.Items.Insert("0", "--All--")
            BindGrid()
        End If
    End Sub

    'Private Sub BindLocation()
    '    'ObjSubSonic.Binddropdown(ddlLocation, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
    '    ObjSubSonic.Binddropdown(ddlLocation, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE")
    '    ddlLocation.Items.Insert("0", "--All--")
    '    ddlLocation.Items.RemoveAt(1)
    'End Sub


    'Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged

    '    ReportViewer1.Visible = False
    '    lblMsg.Visible = False
    '    ddlastCat.Enabled = True
    '    ddlastCat.Items.Clear()
    '    ddlastCat.Items.Insert("0", "--All--")

    '    'gdAsset.Visible = False
    '    If ddlLocation.SelectedItem.Value <> "--All--" Then
    '        'If ddlLocation.SelectedItem.Text = "--All--" Then
    '        '    ObjSubSonic.Binddropdown(ddlastCat, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
    '        '    ddlastCat.Items.Insert("1", "--All--")
    '        '    'BindGrid()
    '        '    'gdAsset.Visible = False
    '        '    'ddlastCat.Enabled = False
    '        '    'ddlastsubCat.Enabled = False
    '        '    'ddlAstBrand.Enabled = False
    '        '    'ddlAstModel.Enabled = False
    '        '    'ddlastCat.Enabled = False
    '        'Else
    '        Dim param() As SqlParameter = New SqlParameter(0) {}
    '        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '        param(0).Value = ddlLocation.SelectedItem.Value
    '        'ObjSubSonic.Binddropdown(ddlastCat, "PVM_ANUM_BDG", "FLR_NAME", "FLR_CODE", param)
    '        'ObjSubSonic.Binddropdown(ddlastCat, "PVM_ANUM_FLOOR", "GP", "GP_ID", param)
    '        ObjSubSonic.Binddropdown(ddlastCat, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION_REPORT", "GROUP_NAME", "GROUP_ID", param)
    '        ddlastCat.Items.Insert("0", "--All--")
    '        ddlastCat.Items.RemoveAt(1)
    '        'End If
    '    End If

    '    'ddlastCat.Items.Clear()
    '    'ddlastCat.Items.Insert("0", "--Select--")

    '    ddlastsubCat.Items.Clear()
    '    ddlastsubCat.Items.Insert("0", "--All--")


    '    ddlAstBrand.Items.Clear()
    '    ddlAstBrand.Items.Insert("0", "--All--")


    '    ddlAstModel.Items.Clear()
    '    ddlAstModel.Items.Insert("0", "--All--")


    '    lblNo.Visible = False
    'End Sub

    Private Sub BindGrid()

        Dim strBuilding As String = ""
        Dim strFloor As String = ""
        Dim strAstGrp As String = ""
        Dim strAstGrpType As String = ""
        Dim strAstBrand As String = ""
        Dim strAstMdl As String = ""

        Try
            If ddlLocation.SelectedItem.Value = "" Then
                strBuilding = ""
            Else
                strBuilding = ddlLocation.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try

            If ddlastCat.SelectedItem.Value = "" Then
                strFloor = ""
            Else
                strFloor = ddlastCat.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try
            If ddlastCat.SelectedItem.Value = "" Then
                strAstGrp = ""
            Else
                strAstGrp = ddlastCat.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try
            If ddlastsubCat.SelectedItem.Value = "" Then
                strAstGrpType = ""
            Else
                strAstGrpType = ddlastsubCat.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try


        Try
            If ddlAstBrand.SelectedItem.Value = "" Then
                strAstBrand = ""
            Else
                strAstBrand = ddlAstBrand.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try
            If ddlAstModel.SelectedItem.Value = "" Then
                strAstMdl = ""
            Else
                strAstMdl = ddlAstModel.SelectedItem.Value
            End If

        Catch ex As Exception

        End Try

        Dim ds As New DataSet
        Dim param() As SqlParameter = New SqlParameter(5) {}
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = strBuilding
        param(1) = New SqlParameter("@VT_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = strAstGrp
        param(2) = New SqlParameter("@AST_SUBCAT_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = strAstGrpType
        param(3) = New SqlParameter("@MANUFACTUER_CODE", SqlDbType.NVarChar, 200)
        param(3).Value = strAstBrand
        param(4) = New SqlParameter("@AST_MD_CODE", SqlDbType.NVarChar, 200)
        param(4).Value = strAstMdl
        param(5) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(5).Value = Session("uid")
        ds = ObjSubSonic.GetSubSonicDataSet("MN_GET_ASSETNOT_MAINT_REPORT", param)
        Dim rds As New ReportDataSource()
        rds.Name = "AssetNotUnderMaintenanceReport"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/AssetNotUnderMaintenanceReport.rdlc")

        'Setting Header Column value dynamically

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        'gdAsset.DataSource = ds
        'gdAsset.DataBind()

        If (ds.Tables(0).Rows.Count <= 0) Then

            lblMsg.Visible = True
            'lblMsg.Text = "No Records Found With Selected Criteria"
        Else
            lblMsg.Visible = True
            lblMsg.Text = ""
            'gdAsset.Visible = True
        End If

    End Sub


    'Protected Sub ddlastCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastCat.SelectedIndexChanged
    '    ReportViewer1.Visible = False
    '    lblMsg.Visible = False
    '    'gdAsset.Visible = False
    '    ddlastsubCat.Enabled = True
    '    ddlastsubCat.Items.Clear()
    '    ddlastsubCat.Items.Insert("0", "--All--")
    '    If ddlastCat.SelectedItem.Value <> "--All--" Then
    '        'If ddlastCat.SelectedItem.Text = "--All--" Then
    '        '    BindGrid()
    '        '    gdAsset.Visible = False
    '        '    ddlastsubCat.Enabled = False
    '        '    ddlAstBrand.Enabled = False
    '        '    ddlAstModel.Enabled = False
    '        'Else
    '        'Dim param() As SqlParameter = New SqlParameter(2) {}
    '        'param(1) = New SqlParameter("@FLRID", SqlDbType.NVarChar, 200)
    '        'param(1).Value = ddlastCat.SelectedItem.Value
    '        'param(1) = New SqlParameter("@AGRPID", SqlDbType.NVarChar, 200)
    '        'ObjSubSonic.Binddropdown(ddlastsubCat, "PVM_ANUM_AGRP", "GPT", "AGT_CODE", param)

    '        Dim param(1) As SqlParameter
    '        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '        param(0).Value = ddlLocation.SelectedItem.Value
    '        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
    '        param(1).Value = ddlastCat.SelectedItem.Value
    '        ObjSubSonic.Binddropdown(ddlastsubCat, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP_REPORT", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
    '        ddlastsubCat.Items.Insert("0", "--All--")
    '        ddlastsubCat.Items.RemoveAt(1)

    '        'End If
    '    End If

    '    ddlAstBrand.Items.Clear()
    '    ddlAstBrand.Items.Insert("0", "--All--")

    '    ddlAstModel.Items.Clear()
    '    ddlAstModel.Items.Insert("0", "--All--")
    '    lblNo.Visible = False
    'End Sub
    Protected Sub ddlastCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastCat.SelectedIndexChanged

        getsubcategorybycat(ddlastCat.SelectedItem.Value)
    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getModelbycatsubcat()
        End If

    End Sub

    'Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
    '    ReportViewer1.Visible = False
    '    lblMsg.Visible = False
    '    'gdAsset.Visible = False
    '    ddlAstModel.Enabled = True
    '    ddlAstModel.Items.Clear()
    '    ddlAstModel.Items.Insert("0", "--All--")
    '    If ddlAstBrand.SelectedItem.Value <> "--All--" Then
    '        'If ddlAstBrand.SelectedItem.Text = "--All--" Then
    '        '    BindGrid()
    '        '    gdAsset.Visible = False
    '        '    ddlAstModel.Enabled = False
    '        'Else
    '        'BindGrid()

    '        '-----Replaced with SP PVM_ANUM_ABRND
    '        'Dim param() As SqlParameter = New SqlParameter(4) {}
    '        'param(1) = New SqlParameter("@FLRID", SqlDbType.NVarChar, 200)
    '        'param(1).Value = ddlastCat.SelectedItem.Value
    '        'param(2) = New SqlParameter("@AGRPID", SqlDbType.NVarChar, 200)
    '        'param(2).Value = ddlastCat.SelectedItem.Value
    '        'param(3) = New SqlParameter("@AGRPTYP", SqlDbType.NVarChar, 200)
    '        'param(3).Value = ddlastsubCat.SelectedItem.Value

    '        Dim param(3) As SqlParameter
    '        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '        param(0).Value = ddlLocation.SelectedItem.Value
    '        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
    '        param(1).Value = ddlastCat.SelectedItem.Value
    '        param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
    '        param(2).Value = ddlastsubCat.SelectedItem.Value
    '        param(3) = New SqlParameter("@BRD_ID", SqlDbType.NVarChar, 200)
    '        param(3).Value = ddlAstBrand.SelectedItem.Value
    '        'ObjSubSonic.Binddropdown(ddlAstModel, "PVM_ANUM_ABRND", "AAM_NAME", "AAM_CODE", param)
    '        ObjSubSonic.Binddropdown(ddlAstModel, "MN_GET_ASSET_MODEL_BY_LOCGRUPBRND_REPORT", "MODEL_NAME", "MODEL_CODE", param)
    '        ddlAstModel.Items.Insert("0", "--All--")
    '        ddlAstModel.Items.RemoveAt(1)
    '    End If
    '    'End If
    '    lblNo.Visible = False
    'End Sub

    'Protected Sub ddlastsubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastsubCat.SelectedIndexChanged
    '    ReportViewer1.Visible = False
    '    lblMsg.Visible = False
    '    'gdAsset.Visible = False
    '    ddlAstBrand.Enabled = True
    '    ddlAstBrand.Items.Clear()
    '    ddlAstBrand.Items.Insert("0", "--All--")
    '    If ddlastsubCat.SelectedItem.Value <> "--All--" Then
    '        'If ddlastsubCat.SelectedItem.Text = "--All--" Then
    '        '    BindGrid()
    '        '    gdAsset.Visible = False
    '        '    ddlAstModel.Enabled = False
    '        'Else

    '        'BindGrid()
    '        '------Replaced with SP PVM_ANUM_AGRPTYP
    '        'Dim param() As SqlParameter = New SqlParameter(3) {}
    '        'param(1) = New SqlParameter("@FLRID", SqlDbType.NVarChar, 200)
    '        'param(1).Value = ddlastCat.SelectedItem.Value
    '        'param(2) = New SqlParameter("@AGRPID", SqlDbType.NVarChar, 200)
    '        'param(2).Value = ddlastCat.SelectedItem.Value

    '        Dim param(2) As SqlParameter
    '        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '        param(0).Value = ddlLocation.SelectedItem.Value
    '        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
    '        param(1).Value = ddlastCat.SelectedItem.Value
    '        param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
    '        param(2).Value = ddlastsubCat.SelectedItem.Value
    '        'ObjSubSonic.Binddropdown(ddlAstBrand, "PVM_ANUM_AGRPTYP", "AAB_NAME", "AAB_CODE", param)
    '        ObjSubSonic.Binddropdown(ddlAstBrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE_REPORT", "BRAND_NAME", "BRAND_ID", param)
    '        ddlAstBrand.Items.Insert("0", "--All--")
    '        ddlAstBrand.Items.RemoveAt(1)
    '    End If
    '    'End If
    '    lblNo.Visible = False
    '    ddlAstModel.Items.Clear()
    '    ddlAstModel.Items.Insert("0", "--All--")
    'End Sub
    Protected Sub ddlastsubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlastsubCat.SelectedIndexChanged
        If ddlastsubCat.SelectedIndex > 0 Then
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
        End If

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        lblNo.Visible = False
        BindGrid()
    End Sub

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlastsubCat.DataSource = sp.GetDataSet()
        ddlastsubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlastsubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlastsubCat.DataBind()
        ' ddlastsubCat.Items.Insert(0, "--Select--")
        ddlastsubCat.Items.Insert(0, New ListItem("--All--", ""))

    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        'ddlastCat.Items.Insert(0, "--Select--")
        ddlastCat.Items.Remove("--Select--")
        ddlastCat.Items.Insert(0, New ListItem("--All--", ""))
        ' ddlastCat.SelectedIndex = If(ddlastCat.Items.Count > 1, 1, 0)

    End Sub
    Private Sub getbrandbycatsubcat(ByVal astcatcode As String, ByVal astsubcatcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlastsubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", ""))


    End Sub
    Private Sub getModelbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastsubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", ""))

    End Sub

    Private Sub BindLocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"


        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", ""))
        'ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)

        'ddlLocation.Items.Remove("--All--")
    End Sub
    
End Class
