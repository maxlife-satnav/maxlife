Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class MaintenanceManagement_PMC_Reports_frmViewPVMCostDtls
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        txtFromDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            Dim flag
            Dim uid
            uid = Session("uid")
            flag = 1
            'cmdExcel.Enabled = False
            BindBuilding()
            cboType.Items.Insert("0", "--All--")
            txtFromDate.Text = getoffsetdatetime(DateTime.Now).AddYears(-1).Date
            txtToDate.Text = getoffsetdatetime(DateTime.Now).Date
            bindgrid()
        End If
    End Sub

    Private Sub BindBuilding()
        'ObjSubSonic.Binddropdown(cboBuilding, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
        'cboBuilding.SelectedIndex = 0
        cboBuilding.Items.Insert("0", "--All--")
        cboBuilding.Items.RemoveAt(1)
    End Sub

    Private Sub cboBuilding_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged
        ReportViewer1.Visible = False
        '    If cboBuilding.SelectedItem.Text = "--Select--" Then
        '        'cboType.SelectedIndex = 0
        '        cboType.Items.Clear()
        '        'gdCost.Visible = False
        '        txtFromDate.Text = ""
        '        txtToDate.Text = ""
        '    Else
        '        cboType.Items.Clear()
        '        cboType.Items.Insert("0", "--Select--")
        '        Dim param() As SqlParameter = New SqlParameter(0) {}
        '        param(0) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
        '        param(0).Value = cboBuilding.SelectedItem.Value
        '        'ObjSubSonic.Binddropdown(cboType, "PVM_COSTDTLS_FLR", "PVM_PLAN_TYPE", "PVM_PLAN_TYPE", param)
        '        ObjSubSonic.Binddropdown(cboType, "MN_PVM_COSTDTLS_FLR_REPORT", "PVM_PLAN_TYPE", "PVM_PLAN_TYPE", param)
        '        cboType.Items.Insert("0", "--All--")
        '        cboType.Items.RemoveAt(1)
        '    End If
    End Sub

    Private Sub cboType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboType.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub

    Private Sub bindgrid()
        Dim ds As New DataSet
        Dim param() As SqlParameter = New SqlParameter(4) {}
        param(0) = New SqlParameter("@PLAN_TYPE", SqlDbType.NVarChar, 200)
        param(0).Value = cboType.SelectedItem.Text
        param(1) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
        param(1).Value = cboBuilding.SelectedItem.Value
        param(2) = New SqlParameter("@FRMDT", SqlDbType.NVarChar, 200)
        param(2).Value = txtFromDate.Text
        param(3) = New SqlParameter("@TODT", SqlDbType.NVarChar, 200)
        param(3).Value = txtToDate.Text
        param(4) = New SqlParameter("@USR_ID", SqlDbType.NVarChar, 200)
        param(4).Value = Session("uid")
        ds = ObjSubSonic.GetSubSonicDataSet("PVM_COSTDTLS_BIND_CTYPE", param)
        Dim rds As New ReportDataSource()
        rds.Name = "PPMCostDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PPMCostReport.rdlc")
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("Currencyparam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblWarn.Text = ""
        bindgrid()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("prvnt_maintenance_report.aspx")
    End Sub

End Class
