Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class MaintenanceManagement_PMC_Reports_frmViewPVMDates
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        txtFromDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")

        If Not Page.IsPostBack Then
            'cmdExcel.Visible = False
            ReportViewer1.Visible = False
            Dim flag
            Dim uid
            uid = Session("uid")
            flag = 1

            BindLocation()
            BindData()
            'PM_REQ_DATA.Visible = False
        End If
        'txtFromDate.Attributes.Add("onClick", "displayDatePicker('" + txtFromDate.ClientID + "')")
        'txtFromDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    End Sub

    Private Sub BindLocation()
        'ObjSubSonic.Binddropdown(DrpDwnPremise, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        ObjSubSonic.Binddropdown(DrpDwnPremise, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
        DrpDwnPremise.Items.Insert("0", "--All--")
        DrpDwnPremise.Items.RemoveAt(1)
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindData()
    End Sub

    Sub BindData()
        Dim ds As New DataSet
        Dim param() As SqlParameter = New SqlParameter(3) {}
        param(0) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
        param(0).Value = DrpDwnPremise.SelectedItem.Value
        param(1) = New SqlParameter("@FRMDT", SqlDbType.DateTime)
        param(1).Value = txtFromDate.Text
        param(2) = New SqlParameter("@TODT", SqlDbType.DateTime)
        param(2).Value = txtToDate.Text
        param(3) = New SqlParameter("@USR_ID", SqlDbType.NVarChar, 200)
        param(3).Value = Session("uid")

        ds = ObjSubSonic.GetSubSonicDataSet("BIND_GROLE_PVM_STATUS_DETAILS_ALL", param)
        Dim rds As New ReportDataSource()
        rds.Name = "PPMStatusDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PPMStatusReport.rdlc")

        'Setting Header Column value dynamically

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        'PM_REQ_DATA.DataSource = ds
        'PM_REQ_DATA.DataBind()
        Dim dtScheduled As DateTime = getoffsetdate(Date.Today)

        'If PM_REQ_DATA.Items.Count > 0 Then
        '    For i As Int32 = 0 To PM_REQ_DATA.Items.Count - 1
        '        If PM_REQ_DATA.Items(i).Cells(3).Text <> String.Empty Then
        '            dtScheduled = Convert.ToDateTime(PM_REQ_DATA.Items(i).Cells(3).Text)
        '        End If
        '        If PM_REQ_DATA.Items(i).Cells(4).Text.StartsWith("P") Then
        '            If dtScheduled > getoffsetdate(Date.Today) Then
        '                PM_REQ_DATA.Items(i).Cells(4).Text = "Due"
        '            End If
        '        End If
        '    Next
        'End If
        ' If PM_REQ_DATA.Items.Count > 0 Then
        'pnlbutton.Visible = True
        pnlPremise.Visible = True
        'PM_REQ_DATA.Visible = True
        lblError.Visible = False
        'cmdExcel.Visible = True
        'Else
        'PM_REQ_DATA.Visible = False
        'lblError.Visible = True
        'pnlbutton.Visible = False
        'lblError.Text = "No Records Found"
        Exit Sub
        'End If

    End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Response.Redirect("prvnt_maintenance_report.aspx")
    'End Sub

End Class
