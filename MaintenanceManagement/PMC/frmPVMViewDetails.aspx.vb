Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_PMC_frmPVMViewDetails
    Inherits System.Web.UI.Page

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    ' ------------------- Json ------------------------------------


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/JSon_Source/Source/themes/flick/jquery-ui.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/JSon_Source/Source/css/ui.jqgrid.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/JSon_Source/Source/js/jquery.min.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/JSon_Source/Source/js/i18n/grid.locale-en.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/JSon_Source/Source/js/jquery.jqGrid.min.js")))
    '    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js_source/TowerwiseUtilization.js")))





    '    '--------------------------------------------------------------


    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))


    'End Sub

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim strUid As String
    Dim rid As String

    Dim param() As SqlParameter
    Dim ds As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim flag
            flag = 1
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@rid", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("rid")
            ds = New DataSet
            'ds = ObjSubSonic.GetSubSonicDataSet("PVM_PMASSETDETAILS_SP", param)
            ds = ObjSubSonic.GetSubSonicDataSet("MN_PVM_PMASSETDETAILS_SP", param)
            If ds.Tables(0).Rows.Count > 0 Then
                flag = 2
            End If
            If flag = 1 Then
                Response.Redirect("frmPVMFinalpage.aspx?rid=1")
            End If
            BindData()
        End If
    End Sub

    Sub BindData()
        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@rid", SqlDbType.NVarChar, 50)
        param(0).Value = Request.QueryString("rid")
        Dim ds As New DataSet

        'ds = ObjSubSonic.GetSubSonicDataSet("PVM_PMASSETDETAILS_SP", param)
        ds = ObjSubSonic.GetSubSonicDataSet("MN_PVM_PMASSETDETAILS_SP", param)
        Dim i As Int16
        For i = 0 To ds.Tables(0).Rows.Count - 2
            Dim f, n As String
            f = ds.Tables(0).Rows(i).Item("PVD_PLANSCHD_DT")
            n = ds.Tables(0).Rows(i + 1).Item("PVD_PLANSCHD_DT")
            'If f = n Then
            '    ds.Tables(0).Rows(i).Delete()
            'End If
        Next
        PM_REQ_DATA.DataSource = ds
        PM_REQ_DATA.DataBind()

        Dim dtScheduled As DateTime = getoffsetdate(Date.Today)

        'If PM_REQ_DATA.Items.Count > 0 Then
        '    For i1 As Int32 = 0 To PM_REQ_DATA.Items.Count - 1
        '        If PM_REQ_DATA.Items(i1).Cells(2).Text <> String.Empty Then
        '            dtScheduled = Convert.ToDateTime(PM_REQ_DATA.Items(i1).Cells(2).Text)
        '        End If
        '        If PM_REQ_DATA.Items(i1).Cells(3).Text.StartsWith("P") Then
        '            If dtScheduled > getoffsetdate(Date.Today) Then
        '                PM_REQ_DATA.Items(i1).Cells(3).Text = "Due"
        '            End If
        '        End If
        '    Next
        'End If
    End Sub

    Protected Sub PM_REQ_DATA_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles PM_REQ_DATA.PageIndexChanged
        PM_REQ_DATA.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub

    'Protected Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
    '    pnlbutton.Visible = False
    '    Response.Write("<script>javascript:window.print();</script> ")
    '    'If PM_REQ_DATA.Visible = True Then
    '    '    PM_REQ_DATA.Visible = False
    '    '    Datagrid1.Visible = True

    '    'End If
    'End Sub

    Protected Sub cmdExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExcel.Click
        ExportToExcelEmployeeAllocationReport()
    End Sub

    Private Sub ExportToExcelEmployeeAllocationReport()

        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@rid", SqlDbType.NVarChar, 50)
        param(0).Value = Request.QueryString("rid")
        Dim ds As New DataSet

        'ds = ObjSubSonic.GetSubSonicDataSet("PVM_PMASSETDETAILS_SP", param)
        ds = ObjSubSonic.GetSubSonicDataSet("MN_PVM_PMASSETDETAILS_SP", param)
        Dim i As Int16
        For i = 0 To ds.Tables(0).Rows.Count - 2
            Dim f, n As String
            f = ds.Tables(0).Rows(i).Item("PVD_PLANSCHD_DT")
            n = ds.Tables(0).Rows(i + 1).Item("PVD_PLANSCHD_DT")
            'If f = n Then
            '    ds.Tables(0).Rows(i).Delete()
            'End If
        Next
        'PM_REQ_DATA.DataSource = ds
        'PM_REQ_DATA.DataBind()

        'Dim dtScheduled As DateTime = getoffsetdate(Date.Today)

        'If PM_REQ_DATA.Items.Count > 0 Then
        '    For i1 As Int32 = 0 To PM_REQ_DATA.Items.Count - 1
        '        If PM_REQ_DATA.Items(i1).Cells(2).Text <> String.Empty Then
        '            dtScheduled = Convert.ToDateTime(PM_REQ_DATA.Items(i1).Cells(2).Text)
        '        End If
        '        If PM_REQ_DATA.Items(i1).Cells(3).Text.StartsWith("P") Then
        '            If dtScheduled > getoffsetdate(Date.Today) Then
        '                PM_REQ_DATA.Items(i1).Cells(3).Text = "Due"
        '            End If
        '        End If
        '    Next
        'End If

        'Dim param(3) As SqlParameter
        'param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        'param(0).Value = ddlCity.SelectedItem.Value
        'param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        'param(1).Value = ddlLocation.SelectedItem.Value
        'param(2) = New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 200)
        'param(2).Value = "AUR_ID"
        'param(3) = New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 200)
        'param(3).Value = "ASC"

        'Dim dsEmployeeAllocationReport As New DataSet
        'dsEmployeeAllocationReport = objsubsonic.GetSubSonicDataSet("GET_ALLAMANTRA_USERBYLOC_CODE", param)

        'dsEmployeeAllocationReport.Tables(0).Columns(0).ColumnName = "Employee Id"
        'dsEmployeeAllocationReport.Tables(0).Columns(1).ColumnName = "Name"
        'dsEmployeeAllocationReport.Tables(0).Columns(2).ColumnName = "Space Id"
        'dsEmployeeAllocationReport.Tables(0).Columns(3).ColumnName = "Department"

        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("PVMDetailSummary.xls", gv)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
End Class
