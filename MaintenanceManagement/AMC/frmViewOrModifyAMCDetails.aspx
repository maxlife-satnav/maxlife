﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewOrModifyAMCDetails.aspx.vb" Inherits="MaintenanceManagement_AMC_frmViewOrModifyAMCDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

      <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../Dashboard/CSS/ProfileStyle.css" rel="stylesheet" />
</head>
<body>
   
        <div id="wrapper">
            <div id="page-wrapper" class="row">
                <div class="row form-wrapper">
                    <div class="row" id="PanSelAssets" runat="server">
                        <div class="col-md-12">
                               <fieldset>
                            <legend>View Or Modify Maintenance Contract
                            </legend>
                        </fieldset>

                            <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" ValidationGroup="Val1" />
                                     <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-7 control-label">Search by Vendor Name / Location<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                Display="none" ErrorMessage="Please Enter Vendor Name / Location" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <div class="col-md-12">
                                        <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                            CausesValidation="true" TabIndex="2" />
                                    </div>
                                </div>
                            </div>
                               
                   
                                <br />
                       
                            <div class="form-group">
                                
                                    <asp:GridView ID="gvItems" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                        EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>


                                            <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AMN_PLAN_ID", "~/MaintenanceManagement/AMC/frmViewOrModifyAMC.aspx?RID={0}")%>'
                                                        Text='<%#Eval("AMN_PLAN_ID")%>'></asp:HyperLink>
                                                    <asp:Label ID="lblPlanid" runat="server" Text='<%#Eval("AMN_PLAN_ID")%>' Visible="false"></asp:Label>
                                                </ItemTemplate>

                                                 

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLocation" Text='<%#Eval("LOCATION")%>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Vendor" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVendor" Text='<%#Eval("VENDORNAME")%>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                               <asp:TemplateField HeaderText="AMC Years" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAMCYears" Text='<%#Eval("AMN_NO_OF_YEARS")%>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="AMC Start Date" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAMNFromDate" Text='<%#Eval("AMN_FROM_DATE") %>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="AMC End Date" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAMNToDate" Text='<%#Eval("AMN_TO_DATE") %>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="AMC Created Date" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAMCCreateddate" Text='<%#Eval("CREATED_DATE")%>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                             <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                
                                     
                              
                            </div>
                                  </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    

</body>
</html>
