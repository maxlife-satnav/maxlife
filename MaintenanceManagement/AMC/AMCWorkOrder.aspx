<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AMCWorkOrder.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_AMCWorkOrder"
    Title="VIew Work Order" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>View Work Order
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" ValidationGroup="Val1" />
                            <div><br /><br /></div>
                            <div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-7 control-label">Search by AMC Plan ID / Location<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                    Display="none" ErrorMessage="Please Enter AMC Plan Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-5">
                    <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row col-md-6">
        <div class="col-md-12">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                CausesValidation="true" TabIndex="2" />
        </div>
    </div>
</div>

                            <div id="Panel1" runat="server" class="row" style="margin-top: 10px">
                              <div class="col-md-12">
                                    <asp:DataGrid ID="grdViewAssets" runat="server" AllowSorting="True" HorizontalAlign="Center" AutoGenerateColumns="False" OnPageIndexChanged="grdViewAssets_Paged"
                                        AllowPaging="True" PageSize="6" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                                    <asp:TemplateColumn HeaderText="Plan Id">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <%--<a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAMCWorkOrder.aspx")%>?rid=<%# Eval("AMN_PLAN_ID") %>',850,338,'')">View </a>--%>
                                                    <a href="#" onclick="showPopWin('<%# Eval("AMN_PLAN_ID") %>')">
                                                        <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("AMN_PLAN_ID") %>'></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <%--    <asp:BoundColumn DataField="AMN_PLAN_ID" HeaderText="Plan Id">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="AMN_PLAN_FOR" HeaderText="Plan For">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AVR_NAME" HeaderText="Vendor">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                               <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AWO_AMC_COST" HeaderText="Cost" DataFormatString="{0:c2}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AMN_FROM_DATE" HeaderText="From Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AMN_TO_DATE" HeaderText="To Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn Text="View" Visible="false" HeaderText="Details" CommandName="ShowDetails">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:ButtonColumn>
                                         <%--   <asp:TemplateColumn HeaderText="Details">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                <ItemTemplate>--%>
                                                    <%-- <a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAMCWorkOrder.aspx")%>?rid=<%# Eval("AMN_PLAN_ID") %>',850,338,'')">View </a>--%>
                                   <%--                 <a href="#" onclick="showPopWin('<%# Eval("AMN_PLAN_ID") %>')">View</a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>--%>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <%--<PagerStyle CssClass="pagination-ys" />--%>
                                        <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                                    </asp:DataGrid>

                                    <div id="pnlbutton" runat="server">

                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <div class="form-group">
                                                    <asp:Button ID="cmdExcel" OnClick="cmdExcel_Click" Text="Export to Excel" runat="server" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                                    <asp:Button ID="btnBack" OnClick="btnBack_Click" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false"></asp:Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Work Order Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none; width:840px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {

            $("#modalcontentframe").attr("src", "frmAMCWorkOrder.aspx?rid=" + id);
            $("#myModal").modal().fadeIn();

            return false;

            //$("#modelcontainer").load("frmAMCWorkOrder.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal().fadeIn();
            //});
        }
    </script>
</body>
</html>
