﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms
Partial Class MaintenanceManagement_AMC_Reports_frmAMCWorkOrder
    Inherits System.Web.UI.Page
    Dim digit(19) As String
    Dim tys(9) As String
    Dim other(4) As String
    Dim Words As String
    Dim mNos(99) As String
    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Function GetUrl(ByVal imagepath As String) As String

        Dim splits As String() = Request.Url.AbsoluteUri.Split("/"c)
        If splits.Length >= 2 Then
            Dim url As String = splits(0) & "//"
            For i As Integer = 2 To i = 6
                url += splits(i)
                url += "/"
            Next
            Return url + imagepath
        End If
        Return imagepath
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            ' AWC.Src = GetUrl(AWC.Src)
            Dim rid
            rid = Request.QueryString("rid")
            param = New SqlParameter(0) {}
            'param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
            param(0) = New SqlParameter("@AWO_MAMPLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = rid
            Dim ds As New DataSet
            'ds = ObjSubSonic.GetSubSonicDataSet("GET_AMC_WO_RDLC_REPORT", param)
            ds = ObjSubSonic.GetSubSonicDataSet("WORK_ORDER_INTEGRATION", param)

            Label1.Text = ds.Tables(0).Rows(0).Item(0).ToString()
            Label1.Visible = True

            'Dim rds As New ReportDataSource()
            'rds.Name = "AMCWorkOrderReport"
            'rds.Value = ds.Tables(0)

            ''This refers to the dataset name in the RDLC file
            'ReportViewer1.Reset()
            'ReportViewer1.LocalReport.DataSources.Add(rds)
            'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/AMCWorkOrderReport.rdlc")
            'ReportViewer1.LocalReport.Refresh()
            'ReportViewer1.SizeToReportContent = True
            'ReportViewer1.Visible = True
        End If
    End Sub

    Private newPropertyValue As String
    Public Property NewProperty() As String
        Get
            Return newPropertyValue
        End Get
        Set(ByVal value As String)
            newPropertyValue = value
        End Set
    End Property

End Class
