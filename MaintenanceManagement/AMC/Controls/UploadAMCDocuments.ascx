<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UploadAMCDocuments.ascx.vb"
    Inherits="MaintenanceManagement_AMC_Controls_UploadAMCDocuments" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:TextBox ID="txtEditDocNo" runat="server" Width="0px" Visible="False"></asp:TextBox>
                <asp:TextBox ID="txtIndex" runat="server" Width="0px" Visible="False"></asp:TextBox>

                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
                <asp:Label ID="lblErrorlink" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>

            </div>
        </div>
    </div>
</div>

<div id="tblPremise" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator9" runat="server" ErrorMessage="Please Select Location"
                        Display="None" ControlToValidate="cboBuilding" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cboBuilding" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None"
                        ErrorMessage="Please Select Asset Group" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group Type<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None"
                        ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Brand <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None"
                        ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlbrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Vendor <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="cmbVen" Display="None"
                        ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbVen" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Agreement Number<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="DrpDwnAMCId" Display="None"
                        ErrorMessage="Please Select Agreement Number" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="DrpDwnAMCId" CssClass="selectpicker" data-live-search="true" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Assets<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:ListBox ID="lstasset" runat="server" CssClass="form-control"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add Document"></asp:Button><br>
            </div>
        </div>
    </div>

</div>

<div id="tblUploadFile" runat="server" visible="false">

    <div class="row">
        <div class="col-md-12 text-center">
            <div class="form-group">
                <div class="row">
                    <strong>UPLOAD DOCUMENTS</strong>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Type<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txttype" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvType" runat="server" ErrorMessage="Please Enter Document Type"
                            ControlToValidate="txttype" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbstatus" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                            <asp:ListItem Value="RBL">Receivable</asp:ListItem>
                            <asp:ListItem Value="RVD">Received</asp:ListItem>
                        </asp:DropDownList>
                        <asp:CompareValidator ID="cfvStatus" runat="server" ErrorMessage="Please Select Document Status"
                            ControlToValidate="cmbstatus" Display="None" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6" id="TDtxtDT" runat="server">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Receivable/Received Date<span style="color: red;">*</span></label>
                    <div class="col-md-7">

                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                        <asp:RequiredFieldValidator ID="rfvTxtDt" runat="server" ErrorMessage="Please Enter Receivable/Received Date"
                            ControlToValidate="txtDate" Display="None"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" id="tdDocLink" runat="server">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Link for Scanned Documents <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvFile" runat="server" ErrorMessage="Click On Choose File And Provide Upload Document File"
                        ControlToValidate="fu1" Display="None"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <div class="btn btn-default">
                            <i class="fa fa-folder-open-o fa-lg"></i>
                            <asp:FileUpload ID="fu1" runat="Server" Width="90%" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="cmdsavedoc" runat="server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup=""></asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary custom-button-color" Text="Cancel" CausesValidation="False"></asp:Button>
            </div>
        </div>
    </div>
</div>
<div id="tblGridDocs" cellspacing="0" cellpadding="0" width="100%" border="1" runat="server">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="form-group">
                <div class="row">
                    <strong>DOCUMENTS</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <asp:DataGrid ID="grdDocs" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" DataKeyField="AMD_ID" 
                EmptyDataText="No AMC Document Found." AutoGenerateColumns="False" PageSize="15">
                <Columns>
                    <asp:BoundColumn Visible="False" DataField="AMD_ID" HeaderText="ID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="AMD_DOC_TYPE" HeaderText="Document Type">
                        <HeaderStyle></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="AMD_Doc_FILE" HeaderText="Document">
                        <HeaderStyle></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="AMD_DOC_STATUS" HeaderText="Status">
                        <HeaderStyle></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="AMD_DOC_RECDT" HeaderText="Document Date">
                        <HeaderStyle></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:ButtonColumn Text="Edit" CommandName="Select">
                        <HeaderStyle></HeaderStyle>
                    </asp:ButtonColumn>
                    <asp:ButtonColumn Text="Delete" CommandName="Delete">
                        <HeaderStyle></HeaderStyle>
                    </asp:ButtonColumn>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
            </asp:DataGrid>
        </div>
    </div>
</div>

