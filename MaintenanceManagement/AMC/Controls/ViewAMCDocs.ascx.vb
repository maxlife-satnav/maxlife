Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_AMC_Controls_ViewAMCDocs
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Public param() As SqlParameter


    Public Sub clearitems()

        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        cmbAgreement.Items.Clear()
        lstasset.Items.Clear()
        lblMsg.Visible = False

    End Sub

    Public Sub clearitems1()

        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        cmbAgreement.Items.Clear()
        lstasset.Items.Clear()
        lblMsg.Visible = False

    End Sub

    Public Sub clearitems2()

        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        cmbAgreement.Items.Clear()
        lstasset.Items.Clear()
        lblMsg.Visible = False

    End Sub

    Public Sub clearitems3()

        cmbVen.Items.Clear()
        cmbAgreement.Items.Clear()
        lstasset.Items.Clear()
        lblMsg.Visible = False

    End Sub

    Public Sub clearitems4()

        cmbAgreement.Items.Clear()
        lstasset.Items.Clear()
        lblMsg.Visible = False

    End Sub

    Private Sub BindBuilding()
        'ObjSubSonic.Binddropdown(cboBuilding, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        ' ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE")
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindDocuments(ByVal Agrno As String)
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@AMD_AGR_NO", SqlDbType.NVarChar, 200)
        param(0).Value = cmbAgreement.SelectedItem.Text
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("AMC_VIEW_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            'BindGrid(strSQL, grdDocs)
            ObjSubSonic.BindDataGrid(grdDocs, "AMC_VIEW_DOCS", param)

            tblDocs.Visible = True
            lblMsg.Text = ""
        Else
            tblDocs.Visible = False
            lblMsg.Visible = True
            lblMsg.Text = "No Documents Available"
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindBuilding()
        End If
        tblDocs.Visible = False
    End Sub

    Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged
        If cboBuilding.SelectedItem.Value <> "--Select--" Then
            If cboBuilding.SelectedItem.Value <> "--All--" Then
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
                param(0).Value = cboBuilding.SelectedItem.Value
                clearitems()
                ddlGroup.Items.Insert(0, "--Select--")
                ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
            End If
        Else
            clearitems()
        End If
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            clearitems1()
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            clearitems1()
        End If
    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged
        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            clearitems2()
            'ObjSubSonic.Binddropdown(ddlbrand, "GET_ASSET_BRAND_BLDGGROOUP", "BRAND_NAME", "BRAND_ID", param)
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            clearitems2()
        End If
    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged
        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            clearitems3()
            'ObjSubSonic.Binddropdown(cmbVen, "GET_ASSET_VENDOR_BLDGGROOUPBR", "VENDOR_NAME", "VENDOR_ID", param)
            ObjSubSonic.Binddropdown(cmbVen, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
            If cmbVen.Items.Count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "No vendors for selected Asset Group."
                cmbVen.Items.Insert(0, "--Select--")
            End If
        Else
            clearitems3()
        End If
    End Sub

    Protected Sub cmbVen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVen.SelectedIndexChanged

        If cmbVen.SelectedItem.Value <> "--Select--" Then
            Dim param1(4) As SqlParameter
            param1(0) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 200)
            param1(0).Value = cmbVen.SelectedItem.Value
            param1(1) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
            param1(1).Value = cboBuilding.SelectedItem.Value
            param1(2) = New SqlParameter("@GROUP_ID", SqlDbType.NVarChar, 200)
            param1(2).Value = ddlGroup.SelectedItem.Value
            param1(3) = New SqlParameter("@GROUP_TYPE_ID", SqlDbType.NVarChar, 200)
            param1(3).Value = ddlgrouptype.SelectedItem.Value
            param1(4) = New SqlParameter("@BRAND", SqlDbType.NVarChar, 200)
            param1(4).Value = ddlbrand.SelectedItem.Value
            clearitems4()
            cmbAgreement.Items.Insert(0, "--Select--")
            ObjSubSonic.Binddropdown(cmbAgreement, "MN_AMC_GET_WORKORDER_BY_AMNSTA_ID", "AMN_PLAN_ID", "AMN_PLAN_ID", param1)
        Else
            clearitems4()
        End If

    End Sub

    Protected Sub cmbAgreement_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAgreement.SelectedIndexChanged
        If cmbAgreement.SelectedItem.Text <> "--Select--" Then

            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = cmbAgreement.SelectedItem.Text
            ObjSubSonic.BindListBox(lstasset, "AMC_GET_ASSET_VIEW_DOCS", "AAT_NAME", "AMN_PLAN_ID", param)
            BindDocuments(cmbAgreement.SelectedItem.Text)
        Else
            lstasset.Items.Clear()
        End If
    End Sub

    Private Sub grdDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDocs.ItemDataBound
        Dim objalinkCal As HtmlAnchor
        objalinkCal = CType(e.Item.FindControl("hrfDoc"), HtmlAnchor)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(2).Text.Trim = "Received" Then
                    Dim row As DataRowView = e.Item.DataItem
                    Dim DocName As String = CType(row("AMD_DOC_LINK"), String)
                    DocName = DocName.Substring(DocName.LastIndexOf("\") + 1)
                    If Not objalinkCal Is Nothing Then
                        objalinkCal.HRef = "../documents/" & DocName
                        objalinkCal.InnerHtml = DocName
                    Else
                        objalinkCal.InnerHtml = "'Link Not availble'"
                    End If
                Else
                    objalinkCal.Visible = False
                End If
            End If
        Catch ex As Exception
            objalinkCal.InnerHtml = "Link Not availble"
            tblDocs.Visible = False
            lblMsg.Text = "No Documents Available"
        End Try
    End Sub

End Class
