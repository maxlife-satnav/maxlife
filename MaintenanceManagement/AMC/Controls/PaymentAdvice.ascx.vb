Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_AMC_Controls_PaymentAdvice
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim rid, uid, strsql As String

    Public Sub clearitems()

        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        cmbWorkOrder.Items.Clear()
        lstasset.Items.Clear()
        lstPendingMemo.Items.Clear()
        lstSelectedMemo.Items.Clear()
        cmbPayMode.SelectedIndex = 0
        txtPayAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems1()

        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        cmbWorkOrder.Items.Clear()
        lstasset.Items.Clear()
        lstPendingMemo.Items.Clear()
        lstSelectedMemo.Items.Clear()
        cmbPayMode.SelectedIndex = 0
        txtPayAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems2()

        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        cmbWorkOrder.Items.Clear()
        lstasset.Items.Clear()
        lstPendingMemo.Items.Clear()
        lstSelectedMemo.Items.Clear()
        cmbPayMode.SelectedIndex = 0
        txtPayAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems3()

        cmbVen.Items.Clear()
        cmbWorkOrder.Items.Clear()
        lstasset.Items.Clear()
        lstPendingMemo.Items.Clear()
        lstSelectedMemo.Items.Clear()
        cmbPayMode.SelectedIndex = 0
        txtPayAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems4()

        cmbWorkOrder.Items.Clear()
        lstasset.Items.Clear()
        lstPendingMemo.Items.Clear()
        lstSelectedMemo.Items.Clear()
        cmbPayMode.SelectedIndex = 0
        txtPayAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems5()

        'lstasset.Items.Clear()
        lstPendingMemo.Items.Clear()
        lstSelectedMemo.Items.Clear()
        cmbPayMode.SelectedIndex = 0
        txtPayAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Private Sub BindBuilding()

        'ObjSubSonic.Binddropdown(cboBuilding, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        ' ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE")
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        rid = Session("rid")
        uid = Session("uid")
        If Not IsPostBack Then
            If Session("ViewOrModify") = "1" Then
                btnSub.Enabled = False
            End If
            BindBuilding()
            'ObjSubSonic.Binddropdown(cmbVen, "AMC_GET_VENDOR_ADVICE", "AVR_NAME", "AVR_ID")
            'cmbWorkOrder.Items.Insert(0, "--Select--")
            txtListCount.Text = 0
            lblMsg.Visible = False
            If cmbVen.Items.Count = 1 Then
                lblMsg.Visible = True
                lblMsg.Text = "No Pending Payment Memos are Available for Payment Advice Generation !"
                Panel1.Visible = False
            End If
        End If

    End Sub

    Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged

        If cboBuilding.SelectedItem.Value <> "--Select--" Then
            If cboBuilding.SelectedItem.Value <> "--All--" Then
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
                param(0).Value = cboBuilding.SelectedItem.Value
                clearitems()
                'ddlGroup.Items.Insert(0, "--Select--")
                'ObjSubSonic.Binddropdown(ddlGroup, "GET_ASSET_GROUP_BLDG", "GROUP_NAME", "GROUP_ID", param)
                ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
            End If
        Else
            clearitems()
            'ddlGroup.Items.Clear()
            'ddlgrouptype.Items.Clear()
            'ddlbrand.Items.Clear()
            'cmbVen.Items.Clear()
            'cmbVen.Items.Insert("0", "--Select--")
            ' lblMsg.Visible = False
        End If

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged

        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            clearitems1()
            'lblMsg.Visible = False
            'ddlbrand.Items.Clear()
            'ddlgrouptype.Items.Clear()
            'cmbVen.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlgrouptype, "GET_ASSET_GROUPTYPE_BLDGGROOUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            clearitems1()
            'cmbVen.Items.Clear()
            'ddlgrouptype.Items.Clear()
            'ddlbrand.Items.Clear()
        End If

    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged

        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            clearitems2()
            'lblMsg.Visible = False
            'ddlbrand.Items.Clear()
            'cmbVen.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlbrand, "GET_ASSET_BRAND_BLDGGROOUP", "BRAND_NAME", "BRAND_ID", param)
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            clearitems2()
            'ddlbrand.ClearSelection()
            'cmbVen.ClearSelection()
        End If

    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged

        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            clearitems3()
            'lblMsg.Visible = False
            'cmbVen.Items.Clear()
            'ObjSubSonic.Binddropdown(cmbVen, "GET_ASSET_VENDOR_BLDGGROOUPBR", "VENDOR_NAME", "VENDOR_ID", param)
            ObjSubSonic.Binddropdown(cmbVen, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
            If cmbVen.Items.Count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "No vendors for selected Asset Group."
                cmbVen.Items.Insert(0, "--Select--")
            End If
        Else
            clearitems3()
            'cmbVen.ClearSelection()
        End If

    End Sub

    Protected Sub cmbVen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVen.SelectedIndexChanged

        lstPendingMemo.Items.Clear()
        lstSelectedMemo.Items.Clear()
        txtPayAmt.Text = ""
        If cmbVen.SelectedItem.Value <> "--Select--" Then
            clearitems4()
            cmbWorkOrder.Items.Clear()
            Dim GRole, MRole, SqlStr
            Dim uid
            uid = Session("uid")

            'Dim par(0) As SqlParameter
            'par(0) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 200)
            'par(0).Value = cmbVen.SelectedItem.Value
            'ObjSubSonic.Binddropdown(cmbWorkOrder, "AMC_GET_MADMIN_PAY_ADVICE", "AMN_PLAN_ID", "AMN_PLAN_ID", par)
            Dim param1(4) As SqlParameter
            param1(0) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 200)
            param1(0).Value = cmbVen.SelectedItem.Value
            param1(1) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
            param1(1).Value = cboBuilding.SelectedItem.Value
            param1(2) = New SqlParameter("@GROUP_ID", SqlDbType.NVarChar, 200)
            param1(2).Value = ddlGroup.SelectedItem.Value
            param1(3) = New SqlParameter("@GROUP_TYPE_ID", SqlDbType.NVarChar, 200)
            param1(3).Value = ddlgrouptype.SelectedItem.Value
            param1(4) = New SqlParameter("@BRAND", SqlDbType.NVarChar, 200)
            param1(4).Value = ddlbrand.SelectedItem.Value
            ObjSubSonic.Binddropdown(cmbWorkOrder, "MN_AMC_GET_WORKORDER_BY_VENDOR", "AMN_PLAN_ID", "AMN_PLAN_ID", param1)
            If cmbWorkOrder.Items.Count > 1 Then

            Else
                lblMsg.Visible = True
                lblMsg.Text = " Please Create Work Order And Then Proceed..."
                'cmbWorkOrder.Items.Insert(0, "--Select--")
            End If
        Else
            clearitems4()
            'cmbWorkOrder.Items.Clear()
            'cmbWorkOrder.Items.Insert(0, "--Select--")
            'lstPendingMemo.Items.Clear()
            'lstSelectedMemo.Items.Clear()
            'txtPayAmt.Text = ""
        End If

    End Sub

    Protected Sub cmbWorkOrder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWorkOrder.SelectedIndexChanged

        Dim param() As SqlParameter
        If cmbWorkOrder.SelectedItem.Value <> "--Select--" Then
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@amn_plan_id", SqlDbType.NVarChar, 50)
            param(0).Value = cmbWorkOrder.SelectedItem.Text
            ObjSubSonic.BindListBox(lstasset, "AMC_GET_ASSETS_ADVISE", "ASSETNAME", "AMN_PLAN_ID", param)

            lstPendingMemo.Items.Clear()
            clearitems5()
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APM_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
            param(0).Value = cmbWorkOrder.SelectedItem.Value
            ObjSubSonic.BindListBox(lstPendingMemo, "AMC_GET_PAYMEMO_ID", "APM_PAYMEMO_ID", "APM_PAYMEMO_ID", param)
            If lstPendingMemo.Items.Count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = " Please Create Payment Memo and Then Proceed..."
                'btnCalc.Visible = False
                Exit Sub
            End If
        Else
            clearitems5()
            lstasset.Items.Clear()
            'lstPendingMemo.Items.Clear()
            'lstSelectedMemo.Items.Clear()
            'txtPayAmt.Text = ""
        End If

    End Sub

    Protected Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight.Click

        Dim param() As SqlParameter
        If lstPendingMemo.SelectedIndex > -1 Then
            Dim i
            For i = 0 To lstPendingMemo.Items.Count() - 1
                If lstPendingMemo.Items(i).Selected = True Then
                    lstSelectedMemo.Items.Add(lstPendingMemo.Items(i))
                End If
            Next i
            For i = 0 To lstSelectedMemo.Items.Count() - 1
                If lstSelectedMemo.Items(i).Selected = True Then
                    lstPendingMemo.Items.Remove(lstSelectedMemo.Items(i))
                End If
            Next i
            Dim Total
            Total = 0
            For i = 0 To lstSelectedMemo.Items.Count() - 1
                param = New SqlParameter(0) {}
                param(0) = New SqlParameter("@APM_PAYMEMO_ID", SqlDbType.NVarChar, 50)
                param(0).Value = lstSelectedMemo.Items(i).Value

                Dim ds As New DataSet
                ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_NET_PAYABLE", param)
                If ds.Tables(0).Rows.Count > 0 Then
                    Total = Total + CInt(ds.Tables(0).Rows(0).Item(0).ToString)
                End If

                txtPayAmt.Text = Total
            Next i
            txtListCount.Text = CInt(lstSelectedMemo.Items.Count())
        End If

    End Sub

    Protected Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft.Click

        Dim param() As SqlParameter
        If lstSelectedMemo.SelectedIndex > -1 Then
            Dim i
            For i = 0 To lstSelectedMemo.Items.Count() - 1
                If lstSelectedMemo.Items(i).Selected = True Then
                    lstPendingMemo.Items.Add(lstSelectedMemo.Items(i))
                End If
            Next i
            For i = 0 To lstPendingMemo.Items.Count() - 1
                If lstPendingMemo.Items(i).Selected = True Then
                    lstSelectedMemo.Items.Remove(lstPendingMemo.Items(i))
                End If
            Next i
            Dim Total
            Total = txtPayAmt.Text

            For i = 0 To lstSelectedMemo.Items.Count() - 1

                param = New SqlParameter(0) {}
                param(0) = New SqlParameter("@APM_PAYMEMO_ID", SqlDbType.NVarChar, 50)
                param(0).Value = lstSelectedMemo.Items(i).Value

                Dim ds As New DataSet
                ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_NET_PAYABLE", param)
                If ds.Tables(0).Rows.Count > 0 Then
                    Total = Total - CInt(ds.Tables(0).Rows(0).Item(0).ToString)
                End If

                txtPayAmt.Text = Total
            Next i

            If CInt(lstSelectedMemo.Items.Count()) = 0 Then
                txtPayAmt.Text = 0
            End If
            txtListCount.Text = CInt(lstSelectedMemo.Items.Count())
        End If

    End Sub

    Protected Sub btnSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSub.Click

        Dim param() As SqlParameter
        'If Page.IsValid Then
        Dim flag = 0
        Dim rid As String = ""
        Dim uid As String = ""
        rid = Session("rid")
        uid = Session("uid")
        Dim ds As New DataSet
        'param = New SqlParameter(0) {}
        'param(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
        'param(0).Value = rid
        'ds = objsubsonic.GetSubSonicDataSet("AMC_GET_PAYADVICE_NO", param)
        'If ds.Tables(0).Rows.Count > 0 Then
        '    flag = 1
        'End If
        If flag = 0 Then
            Dim Reqseqid, TmpReqseqid As String
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("GET_AMC_PAYMENT_ADVICE_COUNT")
            If ds.Tables(0).Rows.Count > 0 Then
                Reqseqid = ds.Tables(0).Rows(0).Item(0).ToString
            End If

            If Reqseqid < 10 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/000000" & Reqseqid
            ElseIf Reqseqid < 100 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/00000" & Reqseqid
            ElseIf Reqseqid < 1000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/0000" & Reqseqid
            ElseIf Reqseqid < 10000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/000" & Reqseqid
            ElseIf Reqseqid < 100000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/00" & Reqseqid
            ElseIf Reqseqid < 1000000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/0" & Reqseqid
            ElseIf Reqseqid < 10000000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/" & Reqseqid
            End If

            'REPLACED BY SP
            If lstSelectedMemo.Items.Count > 0 Then

                param = New SqlParameter(6) {}
                param(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
                param(0).Value = TmpReqseqid
                param(1) = New SqlParameter("@APA_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
                param(1).Value = cmbWorkOrder.SelectedItem.Value
                param(2) = New SqlParameter("@APA_PAYMENT_MODE", SqlDbType.NVarChar, 50)
                param(2).Value = cmbPayMode.SelectedItem.Text
                param(3) = New SqlParameter("@APA_PAYMENT_AMOUNT", SqlDbType.Float)
                param(3).Value = txtPayAmt.Text
                param(4) = New SqlParameter("@APA_UPDATED_DT", SqlDbType.DateTime)
                param(4).Value = getoffsetdatetime(DateTime.Now)
                param(5) = New SqlParameter("@APA_UPDATED_BY", SqlDbType.NVarChar, 50)
                param(5).Value = uid
                param(6) = New SqlParameter("@APA_STATUS", SqlDbType.Bit)
                param(6).Value = 0
                ObjSubSonic.GetSubSonicExecute("AMC_INSRT_PAY_ADVICE", param)
            Else
                'lblMsg.Visible = True
                'lblMsg.Text = " Please..."
                ''btnCalc.Visible = False
                Exit Sub
            End If
            For i As Integer = 0 To lstSelectedMemo.Items.Count() - 1
                param = New SqlParameter(2) {}
                param(0) = New SqlParameter("@APM_MPAPAYADVICE_NO", SqlDbType.NVarChar, 55)
                param(0).Value = TmpReqseqid
                param(1) = New SqlParameter("@apm_pay_mode", SqlDbType.NVarChar, 50)
                param(1).Value = cmbPayMode.SelectedItem.Text
                param(2) = New SqlParameter("@APM_PAYMEMO_ID", SqlDbType.NVarChar, 50)
                param(2).Value = lstSelectedMemo.Items(i).Value
                ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_PAY_MEMO", param)
            Next i

            Response.Redirect("frmAMCfinalpage.aspx?staid=updated&rid=" & TmpReqseqid)
        Else
            Response.Redirect("frmbackpage.aspx?flag=6")
        End If
        'End If
    End Sub

End Class
