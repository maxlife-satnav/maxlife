<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PaymentAdvice.ascx.vb"
    Inherits="MaintenanceManagement_AMC_Controls_PaymentAdvice" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                <asp:TextBox ID="txtListCount" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>

                <asp:CompareValidator ID="cvList" runat="server" ErrorMessage="Please Select Payment Memos..."
                    Display="None" ControlToValidate="txtListCount" Operator="NotEqual" Type="Integer"
                    ValueToCompare="0"></asp:CompareValidator>
            </div>
        </div>
    </div>
</div>

<div id="Panel1" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Location <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator9" runat="server" ErrorMessage="Please Select Location"
                        Display="None" ControlToValidate="cboBuilding" ValueToCompare="--Select--" Operator="NotEqual">cboBuilding</asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cboBuilding" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None"
                        ErrorMessage="Please Select Asset Group" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group Type <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None"
                        ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Brand <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None"
                        ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlbrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Vendor<span style="color: red;">*</span></label>

                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="cmbVen" Display="None"
                        ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbVen" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Work Order<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="cmbWorkOrder" Display="None"
                        ErrorMessage="Please Select Work Order" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbWorkOrder" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Name<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:ListBox ID="lstasset" runat="server" CssClass="form-control"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-6 control-label">Pending Payment Memos <span style="color: red;">*</span></label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-2 control-label"> </label>
                    <div class="col-md-10">
                        Selected Payment Memos<span style="color: red;">*</span>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row" id="PanSelAssets" runat="server">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <%--<div class="col-md-5">--%>
                    <label class="col-md-6 control-label">
                        <asp:ListBox ID="lstPendingMemo" runat="server" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox></label>
                    <%--</div>--%>
                    <div class="col-md-1">
                        <asp:Button ID="btnRight" runat="server" CssClass="btn btn-primary custom-button-color" Text=">>" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnLeft" runat="server" CssClass="btn btn-primary custom-button-color" Text="<<" CausesValidation="False"></asp:Button>
                    </div>
                    <div class="col-md-5">
                        <asp:ListBox ID="lstSelectedMemo" runat="server" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Mode Of Payment<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="cfvcmbPayMode" runat="server" ValueToCompare="--Select--"
                        Operator="NotEqual" ControlToValidate="cmbPayMode" Display="None" ErrorMessage="Please Select Mode Of Payment"> </asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbPayMode" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                            <asp:ListItem Value="1">Cheque</asp:ListItem>
                            <asp:ListItem Value="2">Demand Draft</asp:ListItem>
                            <asp:ListItem Value="3">Cash</asp:ListItem>
                            <asp:ListItem Value="4">NEFT</asp:ListItem>
                            <asp:ListItem Value="5">RTGS</asp:ListItem>
                            <asp:ListItem Value="6">IMPS</asp:ListItem>
                            <asp:ListItem Value="NA">NA</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Payment Amount<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtPayAmt" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSub" CssClass="btn btn-primary custom-button-color" Text="Submit" runat="server" Font-Bold="True"></asp:Button>
            </div>
        </div>
    </div>


</div>
