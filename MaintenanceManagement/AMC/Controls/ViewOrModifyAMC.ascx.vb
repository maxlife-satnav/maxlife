﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class MaintenanceManagement_AMC_Controls_ViewOrModifyAMC

    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("rid") <> "" Then
            Session("rid") = Request.QueryString("rid")
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("fmglogout"))
        End If


        lblmsg.Visible = False
        txtSDate.Attributes.Add("readonly", "readonly")
        txtEDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            txtAmcYears.Text = "1"
            If Session("ViewOrModify") = "1" Then
                btnSubmit.Enabled = False
            End If
            lblmsg.Visible = False
            lblmsgCanUpdateOrNot.Visible = False
            txtHidTodayDt.Text = getoffsetdate(Date.Today)


            BindBuilding()
            dispdata()

            Check_WorkOrder_Or_PrvntPlan_CreatedOrNot()

            If ddlBuilding.Items.Count = 1 Then
                lblmsg.Visible = True
                lblmsg.Text = "Assets Not Available For AMC !"
                Exit Sub
            End If

        End If
    End Sub


    Public Sub Check_WorkOrder_Or_PrvntPlan_CreatedOrNot()
        Dim dt As DataTable
        Dim Status As String
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Check_WorkOrder_Or_PrvntPlan_CreatedOrNot")
        sp.Command.AddParameter("@AMN_PLAN_ID", Request("RID"), DbType.String)
        ds = sp.GetDataSet
        dt = ds.Tables(0)
        Status = ds.Tables(0).Rows(0).Item("MSG")

        If Status = "YES" Then
            lblmsgCanUpdateOrNot.Visible = True
            lblmsgCanUpdateOrNot.Text = "Work Order / Preventive Plan has been already generated for this Maintenance Contract."

            btnSubmit.Enabled = False
        Else
            btnSubmit.Enabled = True
        End If


    End Sub

    Private Sub dispdata()
        Dim ReqId As String = Request("RID")
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMC_DETAILS_BY_REQ_ID")
        SP.Command.AddParameter("@AMN_PLAN_ID", Request.QueryString("RID"), DbType.String)
        SP.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim dr As SqlDataReader = SP.GetReader()
        If dr.Read() Then
            lblReqId.Text = ReqId

            Dim LOC_ID As String
            LOC_ID = dr("AMN_BDG_ID")
            Dim li As ListItem = ddlBuilding.Items.FindByValue(CStr(LOC_ID))
            If Not li Is Nothing Then
                li.Selected = True
            End If
            ddlBuilding.Enabled = True

            bindAssetGroupByLocation(LOC_ID)
            Dim CatId As String
            CatId = dr("AMN_AST_GROUP")
            Dim li1 As ListItem = ddlGroup.Items.FindByValue(CStr(CatId))
            If Not li1 Is Nothing Then
                li1.Selected = True
            End If
            ddlGroup.Enabled = True


            bindAssetGroupTypeByAssetGroup(LOC_ID, CatId)
            Dim subcatID As String
            subcatID = dr("AMN_AST_GROUP_TYPE")
            Dim li2 As ListItem = ddlgrouptype.Items.FindByValue(CStr(subcatID))
            If Not li2 Is Nothing Then
                li2.Selected = True
            End If
            ddlgrouptype.Enabled = True


            bindAssetBrandByLocNGroupNGroupType(LOC_ID, CatId, subcatID)
            Dim brandId As String
            brandId = dr("AMN_AST_BRAND")
            Dim li3 As ListItem = ddlbrand.Items.FindByValue(CStr(brandId))
            If Not li3 Is Nothing Then
                li3.Selected = True
            End If
            ddlbrand.Enabled = True


            bindVendorByAssetgroupNgrouTypeNbrandNLoc(LOC_ID, CatId, subcatID, brandId)
            Dim vendorID As String
            vendorID = dr("AMN_CTM_ID")
            Dim li4 As ListItem = ddlVendor.Items.FindByValue(CStr(vendorID))
            If Not li4 Is Nothing Then
                li4.Selected = True
            End If
            ddlVendor.Enabled = True


            txtAMCDate.Text = dr("AMN_EXPIRING_ON_OR_BEFORE")

            fillgrid()

            txtSDate.Text = dr("AMN_FROM_DATE")

            txtEDate.Text = dr("AMN_TO_DATE")

            txtComments.Text = dr("AMN_COMMENTS")

            txtAmcYears.Text = dr("AMN_NO_OF_YEARS")
          
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        '     ObjSubSonic.BindGridView(gvItems, "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR", param)
        
    End Sub

    Private Sub BindBuilding()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ObjSubSonic.Binddropdown(ddlBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub ddlBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBuilding.SelectedIndexChanged
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
        param(0).Value = ddlBuilding.SelectedItem.Value
        clearitems()

        ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
        ddlGroup.Focus()
    End Sub

    Protected Sub bindAssetGroupByLocation(ByVal LocID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
        param(0).Value = LocID
        clearitems()
        ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
    End Sub


    Protected Sub bindAssetGroupTypeByAssetGroup(ByVal LocID As String, ByVal group_id As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
        param(0).Value = LocID
        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
        param(1).Value = group_id
        ddlbrand.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlVendor.Items.Clear()
        ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
    End Sub



    Protected Sub bindAssetBrandByLocNGroupNGroupType(ByVal LocID As String, ByVal group_id As String, ByVal grouptype_id As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
        param(0).Value = LocID
        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
        param(1).Value = group_id
        param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
        param(2).Value = grouptype_id
        ddlbrand.Items.Clear()
        ddlVendor.Items.Clear()
        ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
    End Sub

    Protected Sub bindVendorByAssetgroupNgrouTypeNbrandNLoc(ByVal LocID As String, ByVal group_id As String, ByVal grouptype_id As String, ByVal brandId As String)
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
        param(0).Value = LocID
        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
        param(1).Value = group_id
        param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
        param(2).Value = grouptype_id
        param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
        param(3).Value = brandId
        ddlVendor.Items.Clear()
        ObjSubSonic.Binddropdown(ddlVendor, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
    End Sub


    Public Sub clearitems()
        ddlbrand.Items.Clear()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlVendor.Items.Clear()
    End Sub

    Public Sub cleardata()
        ddlbrand.ClearSelection()
        ddlGroup.ClearSelection()
        ddlgrouptype.ClearSelection()
        ddlVendor.ClearSelection()
    End Sub


    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETS_BY_EXPIRY_AMC_EXPIRY_WRNT_DATE_VIEW_OR_MODIFY")

        sp.Command.AddParameter("@BDG_ID", ddlBuilding.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_VENDOR", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_GROUP_ID", ddlGroup.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_GROUPTYPE_ID", ddlgrouptype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_BRAND_ID", ddlbrand.SelectedItem.Value, DbType.String)

        sp.Command.AddParameter("@AMC_DATE", CDate(txtAMCDate.Text), DbType.Date)

        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()

        If gvItems.Rows.Count = 0 Then
            lblmsg.Text = "No Assets To Create/Update AMC..."
            lblmsg.Visible = True
            PanSelAssets.Visible = False
            Panel2.Visible = False
        Else
            lblmsg.Visible = False
            PanSelAssets.Visible = True
            Panel2.Visible = True
        End If


    End Sub


    Protected Sub btnGetData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetData.Click
        fillgrid()

        PanSelAssets.Visible = True

        txtSDate.Text = txtAMCDate.Text
        Dim toDate As DateTime
        toDate = CDate(txtSDate.Text).AddYears(txtAmcYears.Text)

        txtEDate.Text = toDate

    End Sub



    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        If txtEDate.Text < getoffsetdate(Date.Today) Then
            lblmsg.Visible = True
            lblmsg.Text = "Please Select AMC End Date Greater Than Todays Date"
            Exit Sub
        End If

        Dim arparam(), param(), param1() As SqlParameter
        If gvItems.Rows.Count > 0 Then

            Dim plan As String ''This is not use full updated in final page
            plan = Session("uid") & "REQ" & Year(getoffsetdatetime(DateTime.Now)) & Month(getoffsetdatetime(DateTime.Now)) & Day(getoffsetdatetime(DateTime.Now)) & Hour(getoffsetdatetime(DateTime.Now)) & Minute(getoffsetdatetime(DateTime.Now)) & Second(getoffsetdatetime(DateTime.Now))
            ''''**Plan code updated in  final page using stored procedure UPDATE_AMC_MAIN_DETAILS
            lblmsg.Visible = False
            If Len(txtComments.Text) > 200 Then
                lblmsg.Visible = True
                lblmsg.Text = "Please Enter The Comments Less Than 200 Characters"
                Exit Sub
            ElseIf txtComments.Text = String.Empty Then
                txtComments.Text = "NA"
            Else
                lblmsg.Visible = False
            End If
            Dim Mainflag, Checkflag
            Mainflag = 1
            Checkflag = 1

            Dim i As Integer
            Dim icnt As Integer = 0



            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Request("RID")
            ObjSubSonic.GetSubSonicExecute("DELETE_AMC_DTLS_AND_INSERT_HISTORY", param)


            param = New SqlParameter(16) {}
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Request("RID")
            param(1) = New SqlParameter("@AMN_PLAN_FOR", SqlDbType.NVarChar, 50)
            param(1).Value = "Office"
            param(2) = New SqlParameter("@AMN_BDG_ID", SqlDbType.NVarChar, 50)
            param(2).Value = ddlBuilding.SelectedItem.Value
            param(3) = New SqlParameter("@AMN_TWR_ID", SqlDbType.Int)
            param(3).Value = 0
            param(4) = New SqlParameter("@AMN_TYPE_ID ", SqlDbType.NVarChar, 50)
            param(4).Value = ""
            param(5) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 50)
            param(5).Value = ddlVendor.SelectedItem.Value
            param(6) = New SqlParameter("@AMN_FROM_DATE", SqlDbType.DateTime)
            param(6).Value = CDate(txtSDate.Text)
            param(7) = New SqlParameter("@AMN_TO_DATE", SqlDbType.DateTime)
            param(7).Value = CDate(txtEDate.Text)
            param(8) = New SqlParameter("@AMN_COMMENTS", SqlDbType.NVarChar, 250)
            param(8).Value = Replace(Trim(txtComments.Text), "'", "''")
            param(9) = New SqlParameter("@AMN_UPD_BY", SqlDbType.NVarChar, 10)
            param(9).Value = Session("uid")
            param(10) = New SqlParameter("@AMN_UPD_DT", SqlDbType.DateTime)
            param(10).Value = getoffsetdatetime(DateTime.Now)
            param(11) = New SqlParameter("@AMN_CNTR_NAME", SqlDbType.NVarChar, 250)
            param(11).Value = ddlVendor.SelectedItem.Text



            param(12) = New SqlParameter("@AMN_AST_GROUP", SqlDbType.NVarChar, 250)
            param(12).Value = ddlGroup.SelectedItem.Value

            param(13) = New SqlParameter("@AMN_AST_GROUP_TYPE", SqlDbType.NVarChar, 250)
            param(13).Value = ddlgrouptype.SelectedItem.Value

            param(14) = New SqlParameter("@AMN_AST_BRAND", SqlDbType.NVarChar, 250)
            param(14).Value = ddlbrand.SelectedItem.Value

            param(15) = New SqlParameter("@AMN_EXPIRING_ON_OR_BEFORE", SqlDbType.DateTime)
            param(15).Value = CDate(txtAMCDate.Text)

            param(16) = New SqlParameter("@AMN_NO_OF_YEARS", SqlDbType.Int)
            param(16).Value = CInt(txtAmcYears.Text)

            ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_AMC_MAIN", param)

            Dim chkCount As Integer = 0

            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblwarrantyDate As Label = DirectCast(row.FindControl("lblwarrantyDate"), Label)

                If chkSelect.Checked And Not lblwarrantyDate.Text = "" Then
                    If CDate(lblwarrantyDate.Text) > CDate(txtSDate.Text) Then
                        chkCount = chkCount + 1
                        lblmsg.Visible = True
                        lblmsg.Text = "Warranty Date should be less than Contract Start Date !"
                        Exit Sub
                    End If
                End If
            Next

         



            If chkCount = 0 Then
                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lbAAP_CODE As Label = DirectCast(row.FindControl("lbAAP_CODE"), Label)

                    Dim lblAssetName As Label = DirectCast(row.FindControl("lblAssetName"), Label)

                    Dim lblAAP_RUNNO As Label = DirectCast(row.FindControl("lblAAP_RUNNO"), Label)
                    Dim lblwarrantyDate As Label = DirectCast(row.FindControl("lblwarrantyDate"), Label)
                    If chkSelect.Checked Then
                        arparam = New SqlParameter(2) {}
                        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
                        arparam(0).Value = Request("RID")
                        arparam(1) = New SqlParameter("@AAT_CODE", SqlDbType.NVarChar, 35)
                        arparam(1).Value = lbAAP_CODE.Text
                        arparam(2) = New SqlParameter("@AAT_NAME", SqlDbType.NVarChar, 100)
                        arparam(2).Value = lblAssetName.Text

                        ObjSubSonic.GetSubSonicExecute("INSRT_AMC_DTLS", arparam)
                    End If
                Next
            End If

            SendAMCMail()

            If Mainflag = 2 Then
                lblmsg.Visible = True
                lblmsg.Text = "AMC Already Submitted !"
                Exit Sub
            Else
                lblmsg.Visible = False
                If Len(Trim(txtSDate.Text)) = 0 And Len(Trim(txtEDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select Start Date And End Date !"
                    Exit Sub
                ElseIf Len(Trim(txtSDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select Start Date !"
                    Exit Sub
                ElseIf Len(Trim(txtEDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select End Date !"
                    Exit Sub
                End If

                If Checkflag = 2 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "AMC Already Submitted For This Dates."
                    Exit Sub
                Else
                    lblmsg.Visible = False
                    Dim strAst As String = String.Empty
                    strAst = Trim(ddlVendor.SelectedItem.Value)

                    ''''**Plan generated code updated in  final page using stored procedure UPDATE_AMC_MAIN_DETAILS
                    Response.Redirect("frmAMCfinalpage.aspx?rid=" & Request("RID") & "&asset=" & strAst & "&staid=AMCupdated")
                End If
            End If
        Else
            lblmsg.Visible = True
            lblmsg.Text = "Select At Least one Asset"
        End If
    End Sub

    Protected Sub SendAMCMail()
        Dim arparam() As SqlParameter
        arparam = New SqlParameter(1) {}
        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
        arparam(0).Value = Request("RID")
        arparam(1) = New SqlParameter("@MAIL_ID", SqlDbType.NVarChar, 50)
        arparam(1).Value = 14
        ObjSubSonic.GetSubSonicExecute("SEND_MAIL_MN_CREATE_AMC", arparam)
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            ddlbrand.Items.Clear()
            ddlgrouptype.Items.Clear()
            ddlVendor.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlgrouptype, "GET_ASSET_GROUPTYPE_BLDGGROOUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            cleardata()
        End If
    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged
        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            ddlbrand.Items.Clear()
            ddlVendor.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlbrand, "GET_ASSET_BRAND_BLDGGROOUP", "BRAND_NAME", "BRAND_ID", param)
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            ddlbrand.ClearSelection()
            ddlVendor.ClearSelection()
        End If
    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged
        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            ddlVendor.Items.Clear()
            ' ObjSubSonic.Binddropdown(ddlVendor, "GET_ASSET_VENDOR_BLDGGROOUPBR", "VENDOR_NAME", "VENDOR_ID", param)
            ObjSubSonic.Binddropdown(ddlVendor, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
        Else
            ddlVendor.ClearSelection()
        End If
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmViewOrModifyAMCDetails.aspx")
    End Sub

  

    Protected Sub txtAmcYears_TextChanged(sender As Object, e As EventArgs)
        txtSDate.Text = txtAMCDate.Text
        Dim toDate As DateTime
        toDate = CDate(txtSDate.Text).AddYears(txtAmcYears.Text)

        txtEDate.Text = toDate
    End Sub
End Class
