<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMC_Work_Order.ascx.vb"
    Inherits="MaintenanceManagement_AMC_Controls_AMC_Work_Order" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblerror" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:RangeValidator ID="rvAdvance" runat="server" MinimumValue="0" MaximumValue="999999999"
                    Type="Double" ControlToValidate="txtAdvance" Display="None" ErrorMessage="Advance Must be greater than or Equal to 0"></asp:RangeValidator>

                <asp:TextBox ID="TxtAssetCtr" Visible="false" ReadOnly="True" CssClass="form-control"
                    runat="server" Text="0" EnableViewState="False">0</asp:TextBox>
                <asp:TextBox ID="txtval" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator9" runat="server" ErrorMessage="Please Select Location"
                    Display="None" ControlToValidate="cboBuilding" ValueToCompare="--Select--" Operator="NotEqual">cboBuilding</asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="cboBuilding" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Group<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None" ErrorMessage="Please Select Asset Group" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Group Type<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlbrand" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Vendor<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="Comparevalidator10" runat="server" ErrorMessage="Please Select Vendor"
                    Display="None" ControlToValidate="cboVendor" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="cboVendor" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Maint. Contract ID<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="Comparevalidator1" runat="server" ErrorMessage="Please Select  Maint. Contract ID"
                    Display="None" ControlToValidate="DrpDwnAMCId" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="DrpDwnAMCId" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Name<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:ListBox ID="lstasset" runat="server" CssClass="form-control"></asp:ListBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Maint. Contract Start Date<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAMCSDate" runat="server" CssClass="form-control"
                        AutoPostBack="True" Enabled="false"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Maint. Contract End Date<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAMCEDate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Start Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvTD" runat="server" ErrorMessage="Please Select Start Date"
                    ControlToValidate="txtDate" Display="None"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtDate"
                    Display="None" ErrorMessage="Please Select Start Date" Operator="DataTypeCheck"
                    Type="Date"></asp:CompareValidator>
                <div class="col-md-7">


                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"
                            EnableViewState="False" MaxLength="10"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:Label ID="lblAssets" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>

<div id="Panel1" visible="false" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="regXTot" runat="server" ErrorMessage="Please Enter A Valid Total"
                        Display="None" ControlToValidate="txtGrandTot" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtGrandTot" CssClass="form-control" runat="server"
                            AutoPostBack="True" OnTextChanged="txtGrandTot_TextChanged" Text="0">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Taxes<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="regXTax" runat="server" ErrorMessage="Please Enter A Valid Tax Details "
                        Display="None" ControlToValidate="txtTax" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtTax" CssClass="form-control" runat="server" AutoPostBack="True"
                            OnTextChanged="txtTax_TextChanged"></asp:TextBox>
                        <asp:Button Style="z-index: 0" Visible="false" ID="btnCalc" CssClass="clsButton" runat="server" Text="Re-Calculate"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Grand Total<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="regXGT" runat="server" ErrorMessage="Please Enter A Valid Grand total"
                        Display="None" ControlToValidate="TxtTotCost" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="TxtTotCost" CssClass="form-control" EnableViewState="False"
                            runat="server" ReadOnly="True" value="0"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Vendor Name</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtConName" CssClass="form-control" EnableViewState="False"
                            runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Vendor Address</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtConAdd" CssClass="form-control" Height="50px" EnableViewState="False"
                            runat="server" ReadOnly="True" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Phone No.</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtConPh" CssClass="form-control" runat="server" ReadOnly="True"
                            MaxLength="15"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Email</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtConPer" CssClass="form-control" runat="server" ReadOnly="True"
                            Visible="True"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Advance</label>
                    <asp:RegularExpressionValidator ID="regXAdv" runat="server" ErrorMessage="Please Enter A Valid Advance Amount !"
                        Display="None" ControlToValidate="txtAdvance" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtAdvance" runat="server" ControlToValidate="txtAdvance"
                        Display="None" ErrorMessage="Please Enter Advance Amount!"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAdvance" CssClass="form-control" EnableViewState="False"
                            runat="server" MaxLength="10">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Half Yearly</label>
                    <asp:RegularExpressionValidator ID="regXHy" runat="server" ErrorMessage="Please Enter A Valid Half-Year Amount !"
                        Display="None" ControlToValidate="txtHalfYear" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtHalfYear" CssClass="form-control" EnableViewState="False"
                            runat="server" MaxLength="10">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Monthly</label>
                    <asp:RegularExpressionValidator ID="regXMnt" runat="server" ErrorMessage="Please Enter A Valid Monthly Advance Amount !"
                        Display="None" ControlToValidate="txtMonthly" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtMonthly" CssClass="form-control" EnableViewState="False"
                            runat="server" MaxLength="10">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Annual</label>
                    <asp:RegularExpressionValidator ID="regXAnn" runat="server" ErrorMessage="Please Enter A Valid Annual Amount !"
                        Display="None" ControlToValidate="txtAnnual" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAnnual" CssClass="form-control" EnableViewState="False"
                            runat="server" MaxLength="10">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Quarterly</label>
                    <asp:RegularExpressionValidator ID="regxQtly" runat="server" ErrorMessage="Please Enter A Valid Quarter Amount !"
                        Display="None" ControlToValidate="txtQuarter" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtQuarter" CssClass="form-control" EnableViewState="False"
                            runat="server" MaxLength="10">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Others</label>
                    <asp:RegularExpressionValidator ID="regXOth" runat="server" ErrorMessage="Please Enter A Valid Others Amount !"
                        Display="None" ControlToValidate="TxtOthers" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="TxtOthers" CssClass="form-control" EnableViewState="False"
                            runat="server" MaxLength="10">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSub" CssClass="btn btn-primary custom-button-color" EnableViewState="False" runat="server"
                Text="Submit"></asp:Button>
        </div>
    </div>
</div>




