Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class MaintenanceManagement_AMC_Controls_UpdateAMCDetails
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("rid") <> "" Then
            Session("rid") = Request.QueryString("rid")
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("fmglogout"))
        End If
        'txtSDate.Attributes.Add("onClick", "displayDatePicker('" + txtSDate.ClientID + "')")
        'txtSDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txtEDate.Attributes.Add("onClick", "displayDatePicker('" + txtEDate.ClientID + "')")
        'txtEDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        lblmsg.Visible = False
        txtSDate.Attributes.Add("readonly", "readonly")
        txtEDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then

            If Session("ViewOrModify") = "1" Then
                btnSubmit.Enabled = False
            End If

            txtAmcYears.Text = "1"
            lblmsg.Visible = False
            txtHidTodayDt.Text = getoffsetdate(Date.Today)
            FillBFT_Panel()
            Disable_All()
            bindDefaultValueAfterClear()
            'ddlBuilding.Focus()
            If ddlBuilding.Items.Count = 1 Then
                lblmsg.Visible = True
                lblmsg.Text = "Assets Not Available For AMC !"
                Exit Sub
            End If

        End If
    End Sub

    Sub FillBFT_Panel()
        Disable_All()
        'lblerror.Visible = False
        ddlGroup.Items.Insert(0, "--Select--")
        ddlBuilding.Items.Clear()
        BindBuilding()
    End Sub

    Private Sub Disable_All()
        PanSelAssets.Visible = False
    End Sub

    Private Sub BindBuilding()
        'ObjSubSonic.Binddropdown(ddlBuilding, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ObjSubSonic.Binddropdown(ddlBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub ddlBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBuilding.SelectedIndexChanged
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
        param(0).Value = ddlBuilding.SelectedItem.Value
        clearitems()
        'ObjSubSonic.Binddropdown(ddlGroup, "GET_ASSET_GROUP_BLDG", "GROUP_NAME", "GROUP_ID", param)
        'ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
        ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
        'ddlGroup.Focus()
        bindDefaultValueAfterClear()
        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub

    Public Sub clearitems()
        ddlbrand.Items.Clear()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlVendor.Items.Clear()
    End Sub
    Public Sub bindDefaultValueAfterClear()
        'ddlGroup.Items.Insert(0, "--Select--")
        ddlgrouptype.Items.Insert(0, "--Select--")
        ddlbrand.Items.Insert(0, "--Select--")
        ddlVendor.Items.Insert(0, "--Select--")
    End Sub

    Public Sub cleardata()
        ddlbrand.ClearSelection()
        ddlGroup.ClearSelection()
        ddlgrouptype.ClearSelection()
        ddlVendor.ClearSelection()
    End Sub


    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETS_BY_EXPIRY_AMC_EXPIRY_WRNT_DATE")

        sp.Command.AddParameter("@BDG_ID", ddlBuilding.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_VENDOR", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_GROUP_ID", ddlGroup.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_GROUPTYPE_ID", ddlgrouptype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_BRAND_ID", ddlbrand.SelectedItem.Value, DbType.String)

        sp.Command.AddParameter("@AMC_DATE", CDate(txtAMCDate.Text), DbType.Date)

        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()

        If gvItems.Rows.Count = 0 Then
            lblmsg.Text = "No Assets To Create"
            lblmsg.Visible = True
            PanSelAssets.Visible = False
            Panel2.Visible = False
        Else
            lblmsg.Visible = False
            PanSelAssets.Visible = True
            Panel2.Visible = True
        End If


    End Sub

    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged

        'If ddlVendor.SelectedItem.Value <> "--Select--" Then

        '    fillgrid()

        '    PanSelAssets.Visible = True
        'Else
        '    PanSelAssets.Visible = False
        'End If
        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub

    Protected Sub btnGetData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetData.Click
        fillgrid()

        PanSelAssets.Visible = True

        txtSDate.Text = txtAMCDate.Text
        Dim toDate As DateTime
        toDate = CDate(txtSDate.Text).AddYears(txtAmcYears.Text)

        txtEDate.Text = toDate

    End Sub

    Dim plan As String ''This is not use full updated in final page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        If txtEDate.Text < getoffsetdate(Date.Today) Then
            lblmsg.Visible = True
            lblmsg.Text = "Please Select AMC End Date Greater Than Todays Date"
            Exit Sub
        End If

        Dim arparam(), param(), param1() As SqlParameter
        If gvItems.Rows.Count > 0 Then


            plan = Session("uid") & "REQ" & Year(getoffsetdatetime(DateTime.Now)) & Month(getoffsetdatetime(DateTime.Now)) & Day(getoffsetdatetime(DateTime.Now)) & Hour(getoffsetdatetime(DateTime.Now)) & Minute(getoffsetdatetime(DateTime.Now)) & Second(getoffsetdatetime(DateTime.Now))
            ''''**Plan code updated in  final page using stored procedure UPDATE_AMC_MAIN_DETAILS
            lblmsg.Visible = False
            If Len(txtComments.Text) > 200 Then
                lblmsg.Visible = True
                lblmsg.Text = "Please Enter The Comments Less Than 200 Characters"
                Exit Sub
            ElseIf txtComments.Text = String.Empty Then
                txtComments.Text = "NA"
            Else
                lblmsg.Visible = False
            End If
            Dim Mainflag, Checkflag
            Mainflag = 1
            Checkflag = 1

            Dim i As Integer
            Dim icnt As Integer = 0

            param = New SqlParameter(16) {}
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
            param(0).Value = plan
            param(1) = New SqlParameter("@AMN_PLAN_FOR", SqlDbType.NVarChar, 50)
            param(1).Value = "Office"
            param(2) = New SqlParameter("@AMN_BDG_ID", SqlDbType.NVarChar, 50)
            param(2).Value = ddlBuilding.SelectedItem.Value
            param(3) = New SqlParameter("@AMN_TWR_ID", SqlDbType.Int)
            param(3).Value = 0
            param(4) = New SqlParameter("@AMN_TYPE_ID ", SqlDbType.NVarChar, 50)
            param(4).Value = ""
            param(5) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 50)
            param(5).Value = ddlVendor.SelectedItem.Value
            param(6) = New SqlParameter("@AMN_FROM_DATE", SqlDbType.DateTime)
            param(6).Value = CDate(txtSDate.Text)
            param(7) = New SqlParameter("@AMN_TO_DATE", SqlDbType.DateTime)
            param(7).Value = CDate(txtEDate.Text)
            param(8) = New SqlParameter("@AMN_COMMENTS", SqlDbType.NVarChar, 250)
            param(8).Value = Replace(Trim(txtComments.Text), "'", "''")
            param(9) = New SqlParameter("@AMN_UPD_BY", SqlDbType.NVarChar, 10)
            param(9).Value = Session("uid")
            param(10) = New SqlParameter("@AMN_UPD_DT", SqlDbType.DateTime)
            param(10).Value = getoffsetdatetime(DateTime.Now)
            param(11) = New SqlParameter("@AMN_CNTR_NAME", SqlDbType.NVarChar, 250)
            param(11).Value = ddlVendor.SelectedItem.Text


            param(12) = New SqlParameter("@AMN_AST_GROUP", SqlDbType.NVarChar, 250)
            param(12).Value = ddlGroup.SelectedItem.Value

            param(13) = New SqlParameter("@AMN_AST_GROUP_TYPE", SqlDbType.NVarChar, 250)
            param(13).Value = ddlgrouptype.SelectedItem.Value

            param(14) = New SqlParameter("@AMN_AST_BRAND", SqlDbType.NVarChar, 250)
            param(14).Value = ddlbrand.SelectedItem.Value

            param(15) = New SqlParameter("@AMN_EXPIRING_ON_OR_BEFORE", SqlDbType.DateTime)
            param(15).Value = CDate(txtAMCDate.Text)

            param(16) = New SqlParameter("@AMN_NO_OF_YEARS", SqlDbType.Int)
            param(16).Value = CInt(txtAmcYears.Text)

            ObjSubSonic.GetSubSonicExecute("AMC_INSRT_AMC_DTLS", param)

            Dim chkCount As Integer = 0

            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblwarrantyDate As Label = DirectCast(row.FindControl("lblwarrantyDate"), Label)

                If chkSelect.Checked And Not lblwarrantyDate.Text = "" Then
                    If CDate(lblwarrantyDate.Text) > CDate(txtSDate.Text) Then
                        chkCount = chkCount + 1
                        lblmsg.Visible = True
                        lblmsg.Text = "Warranty Date should be less than Contract Start Date !"
                        Exit Sub
                    End If
                End If
            Next

            If chkCount = 0 Then
                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lbAAP_CODE As Label = DirectCast(row.FindControl("lbAAP_CODE"), Label)


                    Dim lblAssetName As Label = DirectCast(row.FindControl("lblAssetName"), Label)

                    Dim lblAAP_RUNNO As Label = DirectCast(row.FindControl("lblAAP_RUNNO"), Label)
                    Dim lblwarrantyDate As Label = DirectCast(row.FindControl("lblwarrantyDate"), Label)
                    If chkSelect.Checked Then
                        arparam = New SqlParameter(2) {}
                        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
                        arparam(0).Value = plan
                        arparam(1) = New SqlParameter("@AAT_CODE", SqlDbType.NVarChar, 200)
                        arparam(1).Value = lbAAP_CODE.Text
                        arparam(2) = New SqlParameter("@AAT_NAME", SqlDbType.NVarChar, 200)
                        arparam(2).Value = lblAssetName.Text

                        ObjSubSonic.GetSubSonicExecute("INSRT_AMC_DTLS", arparam)
                    End If
                Next
            End If

            SendAMCMail()

            If Mainflag = 2 Then
                lblmsg.Visible = True
                lblmsg.Text = "AMC Already Submitted !"
                Exit Sub
            Else
                lblmsg.Visible = False
                If Len(Trim(txtSDate.Text)) = 0 And Len(Trim(txtEDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select Start Date And End Date !"
                    Exit Sub
                ElseIf Len(Trim(txtSDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select Start Date !"
                    Exit Sub
                ElseIf Len(Trim(txtEDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select End Date !"
                    Exit Sub
                End If

                If Checkflag = 2 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "AMC Already Submitted For This Dates."
                    Exit Sub
                Else
                    lblmsg.Visible = False
                    Dim strAst As String = String.Empty
                    strAst = Trim(ddlVendor.SelectedItem.Value)

                    ''''**Plan generated code updated in  final page using stored procedure UPDATE_AMC_MAIN_DETAILS
                    Response.Redirect("frmAMCfinalpage.aspx?rid=" & plan & "&asset=" & strAst & "&staid=submitted")
                End If
            End If
        Else
            lblmsg.Visible = True
            lblmsg.Text = "Select At Least one Asset"
        End If
    End Sub

    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim arparam(), param(), param1() As SqlParameter

    'If lstRight.Items.Count > 0 Then
    '    Dim aid, plan, MainSql
    '    Dim planid As String
    '    aid = Sanitize.SanitizeInput(Session("uid"))
    '    plan = Sanitize.SanitizeInput(Session("rid"))
    '    lblmsg.Visible = False

    '    If Len(txtComments.Text) > 100 Then
    '        lblmsg.Visible = True
    '        lblmsg.Text = "Please Enter The Comments Less Than 100 Characters"
    '        Exit Sub
    '    ElseIf txtComments.Text = String.Empty Then
    '        txtComments.Text = "NA"
    '    Else
    '        lblmsg.Visible = False
    '    End If


    '    MainSql = "exec AMC_MAIN_PLAN '" & plan & "'"
    '    Dim Mainflag, Checkflag
    '    Mainflag = 1
    '    Checkflag = 1
    '    strSQL = MainSql


    '    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    '    If ObjDR.Read() Then
    '        Mainflag = 2
    '        planid = ObjDR("amn_plan_id")
    '    End If
    '    ObjDR.Close()


    '    Dim i As Integer
    '    Dim icnt As Integer = 0
    '    lstRight.DataValueField = lstLeft.DataValueField
    '    If Mainflag = 1 Then
    '        For icnt = 0 To lstRight.Items.Count - 1
    '            '----------Replaced By SP ------------------
    '            param = New SqlParameter(11) {}
    '            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
    '            param(0).Value = plan
    '            param(1) = New SqlParameter("@AMN_PLAN_FOR", SqlDbType.NVarChar, 50)
    '            param(1).Value = "Office"
    '            param(2) = New SqlParameter("@AMN_BDG_ID", SqlDbType.Int)
    '            param(2).Value = ddlBuilding.SelectedItem.Value
    '            param(3) = New SqlParameter("@AMN_TWR_ID", SqlDbType.Int)
    '            param(3).Value = 0
    '            param(4) = New SqlParameter("@AMN_TYPE_ID ", SqlDbType.NVarChar, 50)
    '            param(4).Value = lstRight.Items(icnt).Text
    '            param(5) = New SqlParameter("@AMN_CTM_ID", SqlDbType.Int)
    '            param(5).Value = ddlVendor.SelectedItem.Value
    '            param(6) = New SqlParameter("@AMN_FROM_DATE", SqlDbType.DateTime)
    '            param(6).Value = CDate(txtSDate.Text)
    '            param(7) = New SqlParameter("@AMN_TO_DATE", SqlDbType.DateTime)
    '            param(7).Value = CDate(txtEDate.Text)
    '            param(8) = New SqlParameter("@AMN_COMMENTS", SqlDbType.NVarChar, 250)
    '            param(8).Value = Replace(Trim(txtComments.Text), "'", "''")
    '            param(9) = New SqlParameter("@AMN_UPD_BY", SqlDbType.NVarChar, 10)
    '            param(9).Value = aid
    '            param(10) = New SqlParameter("@AMN_UPD_DT", SqlDbType.DateTime)
    '            param(10).Value = getoffsetdatetime(DateTime.Now)

    '            param(11) = New SqlParameter("@AMN_CNTR_NAME", SqlDbType.NVarChar, 250)
    '            param(11).Value = ddlVendor.SelectedItem.Text
    '            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "AMC_INSRT_AMC_DTLS", param)



    '            '----------replaced By SP ------------------
    '            param1 = New SqlParameter(5) {}
    '            param1(0) = New SqlParameter("@AAP_AMC_VENDOR", SqlDbType.Int)
    '            param1(0).Value = ddlVendor.SelectedItem.Value
    '            param1(1) = New SqlParameter("@AAP_AMC_FROMDT", SqlDbType.DateTime)
    '            param1(1).Value = CDate(txtSDate.Text)

    '            param1(2) = New SqlParameter("@AAP_AMC_TODT", SqlDbType.DateTime)
    '            param1(2).Value = CDate(txtEDate.Text)

    '            param1(3) = New SqlParameter("@AAP_AMC_STA", SqlDbType.Int)
    '            param1(3).Value = 1
    '            param1(4) = New SqlParameter("@AAP_PVM_STA", SqlDbType.Int)
    '            param1(4).Value = 0
    '            param1(5) = New SqlParameter("@AAP_SNO", SqlDbType.VarChar, 50)
    '            param1(5).Value = lstRight.Items(icnt).Value
    '            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "AMC_UPDATE_AMC_DTLS", param1)


    '        Next


    '    End If




    '    i = 0
    '    icnt = 0
    '    lstRight.DataValueField = lstLeft.DataValueField
    '    For icnt = 0 To lstRight.Items.Count - 1
    '        Dim temp As String
    '        arparam = New SqlParameter(2) {}
    '        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
    '        arparam(0).Value = plan
    '        arparam(1) = New SqlParameter("@AAT_CODE", SqlDbType.NVarChar, 35)
    '        arparam(1).Value = lstRight.Items(icnt).Value
    '        arparam(2) = New SqlParameter("@AAT_NAME", SqlDbType.NVarChar, 100)
    '        arparam(2).Value = lstRight.Items(icnt).Text
    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSRT_AMC_DTLS", arparam)
    '    Next

    '    If Mainflag = 2 Then
    '        lblmsg.Visible = True
    '        lblmsg.Text = "AMC Already Submitted !"
    '        Exit Sub
    '    Else
    '        lblmsg.Visible = False
    '        If Len(Trim(txtSDate.Text)) = 0 And Len(Trim(txtEDate.Text)) = 0 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "Select StartDate And EndDate !"
    '            Exit Sub
    '        ElseIf Len(Trim(txtSDate.Text)) = 0 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "Select Start Date !"
    '            Exit Sub
    '        ElseIf Len(Trim(txtEDate.Text)) = 0 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "Select End Date !"
    '            Exit Sub
    '        End If

    '        If Checkflag = 2 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "AMC Already Submited For This Dates."
    '            Exit Sub
    '        Else
    '            lblmsg.Visible = False
    '            Dim strAst As String = String.Empty
    '            strAst = Trim(ddlVendor.SelectedItem.Text)
    '            Response.Redirect("frmAMCfinalpage.aspx?rid=" & plan & "&asset=" & strAst & "&staid=submitted")
    '        End If
    '    End If
    'Else
    '    lblmsg.Visible = True
    '    lblmsg.Text = "Select At Least one Asset"
    'End If
    'End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            ddlbrand.Items.Clear()
            ddlbrand.Items.Insert(0, "--Select--")
            ddlgrouptype.Items.Clear()
            'ddlgrouptype.Items.Insert(0, "--Select--")
            ddlVendor.Items.Clear()
            ddlVendor.Items.Insert(0, "--Select--")
            'ObjSubSonic.Binddropdown(ddlgrouptype, "GET_ASSET_GROUPTYPE_BLDGGROOUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            cleardata()
        End If

        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged
        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            ddlbrand.Items.Clear()
            'ddlbrand.Items.Insert(0, "--Select--")
            ddlVendor.Items.Clear()
            ddlVendor.Items.Insert(0, "--Select--")
            'ObjSubSonic.Binddropdown(ddlbrand, "GET_ASSET_BRAND_BLDGGROOUP", "BRAND_NAME", "BRAND_ID", param)
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            ddlbrand.ClearSelection()
            ddlVendor.ClearSelection()
        End If
        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub


    Protected Sub SendAMCMail()
        Dim arparam() As SqlParameter
        arparam = New SqlParameter(1) {}
        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
        arparam(0).Value = plan
        arparam(1) = New SqlParameter("@MAIL_ID", SqlDbType.NVarChar, 50)
        arparam(1).Value = 1
        ObjSubSonic.GetSubSonicExecute("SEND_MAIL_MN_CREATE_AMC", arparam)
    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged
        lblmsg.Visible = False
        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            ddlVendor.Items.Clear()
            ' ObjSubSonic.Binddropdown(ddlVendor, "GET_ASSET_VENDOR_BLDGGROOUPBR", "VENDOR_NAME", "VENDOR_ID", param)
            ObjSubSonic.Binddropdown(ddlVendor, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
            If ddlVendor.Items.Count = 0 Then
                lblmsg.Visible = True
                lblmsg.Text = "No vendors for selected Asset Group."
                ddlVendor.Items.Insert(0, "--Select--")
            End If
        Else
            ddlVendor.ClearSelection()
        End If
        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub


    Protected Sub txtAmcYears_TextChanged(sender As Object, e As EventArgs)
        txtSDate.Text = txtAMCDate.Text
        Dim toDate As DateTime
        toDate = CDate(txtSDate.Text).AddYears(txtAmcYears.Text)

        txtEDate.Text = toDate
    End Sub

End Class
