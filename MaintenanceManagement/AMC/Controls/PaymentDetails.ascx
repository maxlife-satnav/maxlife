<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PaymentDetails.ascx.vb"
    Inherits="MaintenanceManagement_AMC_Controls_PaymentDetails" %>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div id="Panel1" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Location <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator9" runat="server" ErrorMessage="Please Select Location "
                        Display="None" ControlToValidate="cboBuilding" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cboBuilding" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None"
                        ErrorMessage="Please Select Asset Group" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group Type <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None"
                        ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Brand <span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None"
                        ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlbrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Vendor<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="cmbVendor" Display="None"
                        ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbVendor" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Work Order<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="cmbWorkOrder" Display="None"
                        ErrorMessage="Please Select Work Order" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbWorkOrder" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Name<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:ListBox ID="lstasset" runat="server" CssClass="form-control" data-live-search="true"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Payment Advice No<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="cmbPayment" Display="None"
                        ErrorMessage="Please Select Payment Advice No" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="cmbPayment" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Payment Advice Generation Date<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtcheckdate" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">DD/Cheque/Acc No/Ref. No.<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtChqNo" Display="None"
                        ErrorMessage="Please Enter Cheque/DD/Acc No/Ref. No."></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtChqNo" CssClass="form-control" runat="server" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Dated/Credited On<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtChqDate" Display="None"
                        ErrorMessage="Please Enter Dated/Credited On"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtChqDate" CssClass="form-control" runat="server"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Amount<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtChqAmt" CssClass="form-control" AutoPostBack="True" ReadOnly="True" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSub" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit"></asp:Button>
            </div>
        </div>
    </div>

</div>
