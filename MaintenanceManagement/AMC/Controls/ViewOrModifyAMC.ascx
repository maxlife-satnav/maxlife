﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewOrModifyAMC.ascx.vb" Inherits="MaintenanceManagement_AMC_Controls_ViewOrModifyAMC" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../Dashboard/CSS/ProfileStyle.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
        
    <![endif]-->
   
    <style>
        #ddlAssignStatus1 {
            height: 600px;
            max-height: 600px;
            overflow-y: scroll;
        }

        #ddlAssignStatus {
            height: auto;
            max-height: 200px;
            overflow-y: scroll;
        }
    </style>
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }


            function ChildClick(CheckBox) {
                //Get target base & child control.
                var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
                var TargetChildControl = "chkSelect";
                //Get all the control of the type INPUT in the base control.
                var Inputs = TargetBaseControl.getElementsByTagName("input");
                // check to see if all other checkboxes are checked
                for (var n = 0; n < Inputs.length; ++n)
                    if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                        // Whoops, there is an unchecked checkbox, make sure
                        // that the header checkbox is unchecked
                        if (!Inputs[n].checked) {
                            Inputs[0].checked = false;
                            return;
                        }
                    }
                // If we reach here, ALL GridView checkboxes are checked
                Inputs[0].checked = true;
            }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }




   

    </script>
</head>
<body>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblmsg" ForeColor="Red" CssClass="col-md-12 control-label" runat="server"></asp:Label>
                     <asp:Label ID="lblmsgCanUpdateOrNot" ForeColor="Red" CssClass="col-md-12 control-label" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Requisition Id</label>
                <div class="col-md-7">
                    <asp:Label ID="lblReqId" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                    <asp:TextBox ID="txtHidTodayDt" runat="server" Visible="False"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="ddlBuilding" Display="None" ErrorMessage="Please Select Location" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None" ErrorMessage="Please Select Asset Group" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Group Type<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Brand<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlbrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Vendor<span style="color: red;">*</span></label>
                    <asp:CompareValidator ID="Comparevalidator10" runat="server" ControlToValidate="ddlVendor" Display="None" ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Expiring On Or Before<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="ReqStartAMCDate" runat="server" ControlToValidate="txtAMCDate"
                        Display="None" ErrorMessage="Please Select  Start Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <div class='input-group date' id='txtAMCDate1'>
                            <asp:TextBox ID="txtAMCDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('txtAMCDate1')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
  

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnGetData" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"></asp:Button>
                 <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation ="false" CssClass="btn btn-primary custom-button-color" />
            </div>
        </div>
    </div>



    <div class="row" id="PanSelAssets" runat="server">
        <div class="col-md-12">
            <div class="form-group">
                <%--  <div class="row">
                <label class="col-md-6 control-label">
                    <asp:ListBox ID="lstLeft" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox></label>
                <div class="col-md-1">
                    <asp:Button ID="btnRight" runat="server" Text=">>" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                    <asp:Button ID="btnLeft" runat="server" Text="<<" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                </div>
                <div class="col-md-5">
                    <asp:ListBox ID="lstRight" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                </div>
            </div>--%>
                <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                    <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        EmptyDataText="No Asset Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                        <Columns>

                            <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetName" Text='<%#Eval("AssetName")%>' runat="server"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lbAAP_CODE" Text='<%#Eval("AAP_CODE") %>' runat="server"></asp:Label>
                                    <asp:Label ID="lblAAP_RUNNO" Text='<%#Eval("AAP_RUNNO") %>' runat="server" Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Warranty Date / AMC End Date" ItemStyle-HorizontalAlign="left" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblwarrantyDate" Text='<%#Eval("AAT_WRNTY_DATE") %>'  runat="server"></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>

                                    <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkSelect', this.checked)">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" onclick="javascript:ChildClick(this);" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />

                    </asp:GridView>
                </div>

            </div>
        </div>
    </div>

    <div id="Panel2" runat="server" visible="false">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">No.of Years<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                           <%-- <input type="number" runat="server" id="txtAmcYears" cssclass="form-control" value="1" min="1"  />--%>
                            <asp:TextBox ID="txtAmcYears" runat="server" CssClass="form-control" AutoPostBack="true" onkeypress="return validateText(event)" OnTextChanged="txtAmcYears_TextChanged"></asp:TextBox>
                            

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Maint. Contract Start Date<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="cmpStartDt" runat="server" Operator="LessThan" ErrorMessage="Maint. Contract Start Date should be less than Maint. Contract End Date"
                            Display="None" ControlToValidate="txtSDate" Type="Date" ControlToCompare="txtEDate"></asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="ReqStartDate" runat="server" ControlToValidate="txtSDate"
                            Display="None" ErrorMessage="Please Select Maint. Contract Start Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator5" runat="server" Display="None" ControlToValidate="txtSDate"
                            ErrorMessage="Maint. Contract Start Date Cannot Be Less Than Today's Date" Operator="GreaterThanEqual"
                            Type="Date" ControlToCompare="txtHidTodayDt">
                        </asp:CompareValidator>
                        <div class="col-md-7">
                            <div class='input-group date' id='fromdate'>
                                <asp:TextBox ID="txtSDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Maint. Contract End Date<span style="color: red;">*</span></label>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" Operator="GreaterThan"
                            ErrorMessage="Maint. Contract End Date should be greater than Maint. Contract Start Date" Display="None" ControlToValidate="txtEDate"
                            Type="Date" ControlToCompare="txtSDate"></asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="ReqEndDate" runat="server" ControlToValidate="txtEDate"
                            Display="None" ErrorMessage="Please Select Maint. Contract End Date" SetFocusOnError="True"></asp:RequiredFieldValidator>

                        <div class="col-md-7">
                            <div class='input-group date' id='todate'>
                                <asp:TextBox ID="txtEDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Remarks<span style="color: red;"></span></label>
                        <asp:RegularExpressionValidator ID="regXAC" runat="server" Visible="True" ErrorMessage="Special characters (',-,.,+,<,>) not allowed in Admin Comments!"
                            Display="None" ControlToValidate="txtComments" ValidationExpression="^[a-zA-Z0-9\s-()]+"></asp:RegularExpressionValidator>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <div class="form-group">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Update" OnClientClick="javascript:return validateCheckBoxesMyReq();"></asp:Button>
                </div>
            </div>
        </div>

    </div>
   
    <script type = "text/javascript">
        function validateText(evt) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Enter Only Numbers");
                return false;
            }
        }
     </script>

</body>
    
</html>

