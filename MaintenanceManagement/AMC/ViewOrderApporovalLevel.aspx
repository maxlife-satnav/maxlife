﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewOrderApporovalLevel.aspx.vb" Inherits="MaintenanceManagement_AMC_ViewOrderApporovalLevel" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms"
    TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.grdViewAssets.Items.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }
        <%--function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.grdViewAssets.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }--%>

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.grdViewAssets.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=grdViewAssets.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Work Order Approval
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server" defaultfocus="DrpDwnPremise">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <br />
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-7 control-label">Search by AMC Plan ID / Location<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                Display="none" ErrorMessage="Please Enter AMC Plan Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-6">
                                    <div class="col-md-12">
                                        <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                            CausesValidation="true" TabIndex="2" />
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <asp:DataGrid ID="grdViewAssets" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanged="grdViewAssets_Paged"
                                        CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Work Order Details Found.">
                                        <Columns>
                                            <%--                               <asp:BoundColumn DataField="AMN_PLAN_ID" HeaderText="Plan Id">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>

                                            </asp:BoundColumn>--%>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAll" runat="server" TextAlign="Right" onclick="javascript:CheckAllDataGridCheckBoxes('chkItem', this.checked);"
                                                        ToolTip="Click to check all" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkItem" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn HeaderText="Plan Id">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <%--<a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAMCWorkOrder.aspx")%>?rid=<%# Eval("AMN_PLAN_ID") %>',850,338,'')">View </a>--%>
                                                    <a href="#" onclick="showPopWin('<%# Eval("AMN_PLAN_ID") %>')">
                                                        <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("AMN_PLAN_ID") %>'></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:BoundColumn DataField="AMN_PLAN_FOR" HeaderText="Plan For">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AVR_NAME" HeaderText="Vendor">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AWO_AMC_COST" HeaderText="Cost" DataFormatString="{0:c2}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AMN_FROM_DATE" HeaderText="From Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AMN_TO_DATE" HeaderText="To Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn Text="View" Visible="false" HeaderText="Details" CommandName="ShowDetails">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:ButtonColumn>

                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                                    </asp:DataGrid>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <label class="col-md-4 control-label" id="lblrem" runat="server">Remarks </label>
                                            <%--<asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                        Display="None" ErrorMessage="Please Enter the Remarks" ValidationGroup="Val2"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"
                                                    Rows="3" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <div class="form-group">
                                        <%--asp:Button ID="cmdExcel" OnClick="cmdExcel_Click" runat="server" CssClass="btn btn-primary custom-button-color" Text="Approve"></asp:Button>
                                        <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reject"></asp:Button>--%>
                                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" Text="Approve" OnClientClick="javascript:return validateCheckBoxesMyReq();"/>
                                        <asp:Button ID="Button2" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reject" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">AMC - Work Order Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none; width:840px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {

            $("#modalcontentframe").attr("src", "frmAMCWorkOrder.aspx?rid=" + id);
            $("#myModal").modal().fadeIn();

            return false;

            //$("#modelcontainer").load("frmAMCWorkOrder.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal().fadeIn();
            //});
        }
    </script>
</body>
</html>
