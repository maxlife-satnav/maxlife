﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class MaintenanceManagement_AMC_frmViewOrModifyAMCDetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("rid") <> "" Then
            Session("rid") = Request.QueryString("rid")
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("fmglogout"))
        End If
        If Not IsPostBack Then
            fillgrid()
        End If
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMC_PLANS_TO_VIEW_OR_MODIFY")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            PanSelAssets.Visible = False
        Else
            PanSelAssets.Visible = True
        End If
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_AMC_REQUISITION_BY_PLAN_ID_LOCATION_VENDOR")
        sp.Command.AddParameter("@PLAN_ID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
       
    End Sub

End Class


