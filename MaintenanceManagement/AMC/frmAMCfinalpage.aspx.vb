Imports System.Data.SqlClient
Imports System.Data
Partial Class MaintenanceManagement_AMC_frmAMCfinalpage
    Inherits System.Web.UI.Page
     Dim lid, rid, uid As String
    Dim uprm_id As String
    Dim TmpReqseqid As String
    Dim Reqseqid As Integer

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim param() As SqlParameter
    Dim ds As New DataSet


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        rid = Request.QueryString("rid")
        uid = Session("uid")
        Dim strAsset As String = String.Empty
        strAsset = Request.QueryString("asset")


        If Request.QueryString("staid") = "submitted" Then

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = rid
            Dim ds As New DataSet
            ds = objsubsonic.GetSubSonicDataSet("GET_AMC_MAINID", param)
            If ds.Tables(0).Rows.Count > 0 Then
                Reqseqid = ds.Tables(0).Rows(0).Item("AMN_ID")
            End If


            
            If Reqseqid < 10 Then
                TmpReqseqid = strAsset & "/AMC" & "/000000" & Reqseqid
            ElseIf Reqseqid < 100 Then
                TmpReqseqid = strAsset & "/AMC" & "/00000" & Reqseqid
            ElseIf Reqseqid < 1000 Then
                TmpReqseqid = strAsset & "/AMC" & "/0000" & Reqseqid
            ElseIf Reqseqid < 10000 Then
                TmpReqseqid = strAsset & "/AMC" & "/000" & Reqseqid
            ElseIf Reqseqid < 100000 Then
                TmpReqseqid = strAsset & "/AMC" & "/00" & Reqseqid
            ElseIf Reqseqid < 1000000 Then
                TmpReqseqid = strAsset & "/AMC" & "/0" & Reqseqid
            ElseIf Reqseqid < 10000000 Then
                TmpReqseqid = strAsset & "/AMC" & "/" & Reqseqid
            End If


            If Reqseqid Then
                'For HSBC requirement Creating Single AMC ID for more then ONE ASSET

                Dim sparam(1) As SqlParameter
                sparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
                sparam(0).Value = TmpReqseqid
                sparam(1) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                sparam(1).Value = rid
                ObjSubsonic.GetSubSonicExecute("UPDATE_AMC_MAIN_DETAILS", sparam)


                lblmsg.Text = lblmsg.Text & "<b><center><br><br><font >"
                lblmsg.Text = lblmsg.Text & "Requisition Id : </font>"
                lblmsg.Text = lblmsg.Text & "<font >" & TmpReqseqid & "</font> "
                lblmsg.Text = lblmsg.Text & "<font ><br> AMC Created Successfully !"
                lblmsg.Text = lblmsg.Text & "<br><br>Thank You For Using The System.</font></center>"
            Else

            End If
        ElseIf Request.QueryString("staid") = "updated" Then

            lblmsg.Text = lblmsg.Text & "<b><center><br><br><font >"
            lblmsg.Text = lblmsg.Text & "Requisition Id : </font>"
            lblmsg.Text = lblmsg.Text & "<font >" & Request.QueryString("rid") & "</font> "
            lblmsg.Text = lblmsg.Text & "<font ><br> Payment Advice Created Successfully !"
            lblmsg.Text = lblmsg.Text & "<br><br>Thank You For Using The System.</font></center>"

        ElseIf Request.QueryString("staid") = "AMCupdated" Then

            lblmsg.Text = lblmsg.Text & "<b><center><br><br><font >"
            lblmsg.Text = lblmsg.Text & "Requisition Id : </font>"
            lblmsg.Text = lblmsg.Text & "<font >" & Request.QueryString("rid") & "</font> "
            lblmsg.Text = lblmsg.Text & "<font ><br> AMC Updated Successfully !"
            lblmsg.Text = lblmsg.Text & "<br><br>Thank You For Using The System.</font></center>"

        ElseIf Request.QueryString("staid") = "chequedetailsupdated" Then

            ds = New DataSet
            ds = objsubsonic.GetSubSonicDataSet("GETAMC_CHQCOUNT")
            If ds.Tables(0).Rows.Count > 0 Then
                Reqseqid = ds.Tables(0).Rows(0).Item("ACD_ID")
            End If
 

            If Reqseqid < 10 Then
                TmpReqseqid = Session("uid") & "/ACD" & "/000000" & Reqseqid
            ElseIf Reqseqid < 100 Then
                TmpReqseqid = Session("uid") & "/ACD" & "/00000" & Reqseqid
            ElseIf Reqseqid < 1000 Then
                TmpReqseqid = Session("uid") & "/ACD" & "/0000" & Reqseqid
            ElseIf Reqseqid < 10000 Then
                TmpReqseqid = Session("uid") & "/ACD" & "/000" & Reqseqid
            ElseIf Reqseqid < 100000 Then
                TmpReqseqid = Session("uid") & "/ACD" & "/00" & Reqseqid
            ElseIf Reqseqid < 1000000 Then
                TmpReqseqid = Session("uid") & "/ACD" & "/0" & Reqseqid
            ElseIf Reqseqid < 10000000 Then
                TmpReqseqid = Session("uid") & "/ACD" & "/" & Reqseqid
            End If


            If Reqseqid Then
                param = New SqlParameter(1) {}
                param(0) = New SqlParameter("@ACD_ID", SqlDbType.NVarChar, 200)
                param(0).Value = TmpReqseqid
                param(1) = New SqlParameter("@REQ", SqlDbType.NVarChar, 200)
                param(1).Value = Session("rid")
                ObjSubsonic.GetSubSonicExecute("UPDATE_CHQ_REQ_DETAILS", param)

                lblmsg.Text = lblmsg.Text & "<b><center><br><br><font >"
                lblmsg.Text = lblmsg.Text & "Requisition Id : </font>"
                lblmsg.Text = lblmsg.Text & "<font >" & TmpReqseqid & "</font> "
                lblmsg.Text = lblmsg.Text & "<font ><br> Payment Details Created Successfully !"
                lblmsg.Text = lblmsg.Text & "<br><br>Thank You For Using The System.</font></center>"
            Else
            End If

        ElseIf Request.QueryString("val") <> "" Then
            If Request.QueryString("val") = 1 Then
                lblmsg.Text = lblmsg.Text & "<b><center><br><br><br><br><font f>No Records Found !</font></center>"

            ElseIf Request.QueryString("val") = 2 Then
                lblmsg.Text = lblmsg.Text & "<b><center><br><br><br><br><font >No Records Found !</font></center>"

            End If

        ElseIf Request.QueryString("staid") = "WorkOrder" Then
            lblmsg.Text = lblmsg.Text & "<b><center><br><br><font> Requisition Id : </font><font >" & Request.QueryString("rid") & "</font><font ><br> Work Order Created Successfully ! <br><br>Thank You For Using The System.</font></center>"

        ElseIf Request.QueryString("staid") = "PaymentMemo" Then
            lblmsg.Text = lblmsg.Text & "<b><center><br><br><font> Requisition Id : </font><font >" & Request.QueryString("rid") & "</font><font ><br> Payment Memo Created Successfully ! <br><br>Thank You For Using The System.</font></center>"

        Else
            lblmsg.Text = lblmsg.Text & "<b><center><br><br><font> Requisition Id : </font><font >" & Request.QueryString("rid") & "</font><font ><br> Details Has Been Updated ! <br><br>Thank You For Using The System.</font></center>"
        End If



    End Sub
End Class
