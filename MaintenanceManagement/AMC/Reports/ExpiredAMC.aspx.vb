Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class MaintenanceManagement_AMC_Reports_ExpiredAMC
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Private Sub BindData()

        param = New SqlParameter(2) {}
        param(0) = New SqlParameter("@AMN_BDG_ID", SqlDbType.NVarChar, 200)
        param(0).Value = DrpDwnPremise.SelectedItem.Value
        param(1) = New SqlParameter("@AMN_TO_DATE", SqlDbType.NVarChar, 200)
        param(1).Value = txtDate.Text
        param(2) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(2).Value = Session("uid")


        Dim ds As New DataSet
        'ds = ObjSubSonic.GetSubSonicDataSet("AMC_RPT_EXPIRY_DTLS_BY_BDG", param)
        ds = ObjSubSonic.GetSubSonicDataSet("MN_AMC_RPT_EXPIRY_DTLS_BY_BDG", param)
        Dim i As Int16
        For i = 0 To ds.Tables(0).Rows.Count - 2
            Dim f, n As String
            f = ds.Tables(0).Rows(i).Item("AMN_PLAN_ID")
            n = ds.Tables(0).Rows(i + 1).Item("AMN_PLAN_ID")
            If f = n Then
                ds.Tables(0).Rows(i).Delete()
            End If
        Next

        'If ds.Tables(0).Rows.Count = 0 Then
        '    'PopUpMessage("No records found!")
        '    'lblmsg.Text = "No Records Found"
        '    'pnlbutton.Visible = False
        '    Panel1.Visible = True
        '    ReportViewer1.Visible = True
        'Else

        Dim rds As New ReportDataSource()
        rds.Name = "ExpiredAMCDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/ExpiredAMCReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        pnlbutton.Visible = True
        Panel1.Visible = True
        'End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblmsg.Text = ""
        txtDate.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            BindLocation()
            txtDate.Text = getoffsetdatetime(DateTime.Now).Date
            BindData()
        End If
    End Sub

    Private Sub BindLocation()
        'ObjSubSonic.Binddropdown(DrpDwnPremise, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        ObjSubSonic.Binddropdown(DrpDwnPremise, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
        DrpDwnPremise.Items.Insert("0", "--All--")
        DrpDwnPremise.Items.RemoveAt(1)
    End Sub
    Private val As Integer = 0

    'Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click
    '    If Request.QueryString("rurl") <> Nothing Then
    '        Response.Redirect(Request.QueryString("rurl") + "?back=maintenance")
    '        val = 1
    '    Else
    '        Response.Redirect("ViewAMCReports.aspx")
    '    End If
    'End Sub

    Protected Sub cmdView_Click(sender As Object, e As EventArgs) Handles cmdView.Click
        BindData()
    End Sub
  
End Class
