<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AssetUnderAMCDetails.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_AssetUnderAMCDetails" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Assets Under Maintenance Contract
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row" style="margin-top: 10px">
                              <div class="col-md-12">
                                    <asp:GridView ID="grdViewAssets" runat="server" AutoGenerateColumns="False" EmptyDataText="No Asset Maint. Contract Details Found."
                                        AllowSorting="True" PageSize="5" AllowPaging="True" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:BoundField DataField="AMN_PLAN_ID" HeaderText="Plan ID">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                            <%--<asp:BoundField DataField="ASSET_VENDOR" HeaderText="Asset Vendor">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>--%>
                                             <asp:BoundField DataField="VT_CODE" HeaderText="Asset Type">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                             
                                             <asp:BoundField DataField="AST_MD_NAME" HeaderText="Asset Model">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AVR_NAME" HeaderText="Maint. Contract Vendor">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_NAME" HeaderText="Location">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AMN_FROM_DATE" HeaderText="Maint. Contract From Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AMN_TO_DATE" HeaderText="Maint. Contract To Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                             <asp:BoundField DataField="AMN_NO_OF_YEARS" HeaderText="No Of years" >
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>
                                            <%-- <asp:BoundField DataField="AMN_STA_ID" HeaderText="Status" >
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>--%>
                                               <asp:TemplateField HeaderText="Status"> 
                                                  <ItemTemplate>
                                                    <asp:Label ID="staid" Text='<%#Eval("AMN_STA_ID")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                               </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Assets">
                                                <HeaderStyle Width="50%" CssClass="clsTblHead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:ListBox ID="lstAssets" runat="server" Width="100%"></asp:ListBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="AMN_COMMENTS" HeaderText="AMC Generated By">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundField>--%>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />  
                                        <PagerStyle CssClass="pagination-ys" />                                      
                                    </asp:GridView>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="cmdExcel" OnClick="cmdExcel_Click" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export to Excel"></asp:Button>
                                        <%--<asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>--%>
                                        <asp:Button ID="btnprint" runat="server" CssClass="btn btn-primary custom-button-color" Text="Print" Visible="False"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>




