
Partial Class MaintenanceManagement_AMC_Reports_ViewAMCReports
    Inherits System.Web.UI.Page
 
    Protected Sub lnkBtnAUAMC_Click2(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnAUAMC.Click
        Response.Redirect("AssetUnderAMCDetails.aspx")
    End Sub

    Protected Sub lnkBtnEAMC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnEAMC.Click
        Response.Redirect("ExpiredAMC.aspx")
    End Sub

    Protected Sub lnkBtnWOAMC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnWOAMC.Click
        Response.Redirect("AMCWorkOrder.aspx")
    End Sub

    Protected Sub lnkBtnPMAMC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnPMAMC.Click
        Response.Redirect("AMC_PaymentMemos.aspx")
    End Sub

    Protected Sub lnkBtnPAAMC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBtnPAAMC.Click
        Response.Redirect("AMC_PaymentAdvice.aspx")
    End Sub

    Protected Sub lnkBtnANUM_Click(sender As Object, e As EventArgs) Handles lnkBtnANUM.Click
        Response.Redirect("~/MaintenanceManagement/PMC/Reports/frmAssetNotUnderMaintenance.aspx")
    End Sub
End Class
