<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AMC_PaymentMemos.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_AMC_PaymentMemos"
    Title="AMC Report : AMC Payment Memos" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Maintenance Contract- Payment Memos
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="Export to excel" CssClass="btn btn-primary custom-button-color" />
<%--                                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" CssClass="btn btn-primary custom-button-color" />--%>
                                    </div>
                                </div>
                            </div>
                            <asp:DataGrid ID="grdViewAssets" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Payment Memo Details Found."
                                CssClass="table table-condensed table-bordered table-hover table-striped" OnPageIndexChanged="grdViewAssets_Paged">
                                <Columns>
                                    <asp:BoundColumn DataField="AMN_PLAN_ID" HeaderText="Plan Id">
                                        <HeaderStyle CssClass="clsTblHead" />
                                    </asp:BoundColumn>
                                  <%--  <asp:BoundColumn DataField="AMN_PLAN_FOR" HeaderText="Plan For">
                                        <HeaderStyle CssClass="clsTblHead" />
                                    </asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="AVR_NAME" HeaderText="Vendor">
                                        <HeaderStyle CssClass="clsTblHead" />
                                    </asp:BoundColumn>
                                     <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                        <HeaderStyle CssClass="clsTblHead" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AMN_FROM_DATE" DataFormatString="{0:d}" HeaderText="From Date">
                                        <HeaderStyle CssClass="clsTblHead" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AMN_TO_DATE" DataFormatString="{0:d}" HeaderText="To Date">
                                        <HeaderStyle CssClass="clsTblHead" />
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Show Details" HeaderText="Details" Text="View" Visible="false">
                                        <HeaderStyle CssClass="clsTblHead" />
                                    </asp:ButtonColumn>
                                    <asp:TemplateColumn HeaderText="Details">
                                        <HeaderStyle CssClass="clsTblHead" />
                                        <ItemTemplate>
                                            <%--<a href="#" onclick='showPopWin(&#039;<%=Page.ResolveUrl("frmViewPODetails.aspx")%>?rid=<%# Eval("AMN_PLAN_ID") %>&#039;,850,338,&#039;&#039;)' title="Name">View </a>--%>
                                            <a href="#" onclick="showPopWin('<%# Eval("AMN_PLAN_ID") %>')">View</a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                <PagerStyle NextPageText="Next" Position="Top" PrevPageText="Previous" />
                            </asp:DataGrid>
                            

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Maintenance Contract- Payment Memos</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="300px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "frmViewPODetails.aspx?rid=" + id);
            $("#myModal").modal().fadeIn();
            return false;
        }
    </script>
</body>
</html>
