Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class MaintenanceManagement_AMC_Reports_AMCWorkOrder
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()

        Dim ds As New DataSet
        'ds = ObjSubSonic.GetSubSonicDataSet("AMC_RPT_WO_MAINADMIN")
        'ds = ObjSubSonic.GetSubSonicDataSet("MN_VIEW_WORKORDERS")
        param = New SqlParameter(0) {}
        'param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("Uid").ToString()

        ds = ObjSubSonic.GetSubSonicDataSet("MN_AMC_RPT_WO_MAINADMIN", param)

        Dim i As Int16
        For i = 0 To ds.Tables(0).Rows.Count - 2
            Dim f, n As String
            f = ds.Tables(0).Rows(i).Item("AMN_PLAN_ID")
            n = ds.Tables(0).Rows(i + 1).Item("AMN_PLAN_ID")
            If f = n Then
                ds.Tables(0).Rows(i).Delete()
            End If
        Next
        grdViewAssets.DataSource = ds
        grdViewAssets.DataBind()
        If grdViewAssets.Items.Count = 0 Then
            cmdExcel.Visible = False
        End If
    End Sub

    Sub grdViewAssets_Paged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

        grdViewAssets.CurrentPageIndex = e.NewPageIndex
        BindData()

    End Sub

    Private Sub grdViewAssets_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdViewAssets.PageIndexChanged

        grdViewAssets.CurrentPageIndex = e.NewPageIndex
        BindData()

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Response.Redirect("ViewAMCReports.aspx")
    End Sub

    Protected Sub cmdExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExcel.Click
        ExportToExcelEmployeeAllocationReport()
    End Sub

    Private Sub ExportToExcelEmployeeAllocationReport()

        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("MN_AMC_RPT_WO_MAINADMIN")
        ' ds = ObjSubSonic.GetSubSonicDataSet("AMC_RPT_WO_MAINADMIN")
        'ds = ObjSubSonic.GetSubSonicDataSet("MN_VIEW_WORKORDERS")

        'Dim i As Int16
        'For i = 0 To ds.Tables(0).Rows.Count - 2
        '    Dim f, n As String
        '    f = ds.Tables(0).Rows(i).Item("AMN_PLAN_ID")
        '    n = ds.Tables(0).Rows(i + 1).Item("AMN_PLAN_ID")
        '    If f = n Then
        '        ds.Tables(0).Rows(i).Delete()
        '    End If
        'Next
        'Dim dsEmployeeAllocationReport As New DataSet
        'dsEmployeeAllocationReport = objsubsonic.GetSubSonicDataSet("GET_ALLAMANTRA_USERBYLOC_CODE", param)

        'ds.Tables(0).Columns(0).ColumnName = "AMC Request ID (Plan Id)"
        'ds.Tables(0).Columns(1).ColumnName = "Asset Name"
        'ds.Tables(0).Columns(2).ColumnName = "Vendor"
        'ds.Tables(0).Columns(3).ColumnName = "From Date"
        'ds.Tables(0).Columns(4).ColumnName = "To Date"
        'ds.Tables(0).Columns(5).ColumnName = "Building"


        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("AMCWorkOrder.xls", gv)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_AMC_WORK_ORDER_BY_PLAN_ID_LOCATION")
        sp.Command.AddParameter("@PLAN_ID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        grdViewAssets.DataSource = sp.GetDataSet
        grdViewAssets.DataBind()
        'If grdViewAssets.Items.Count > 0 Then
        '    For i As Integer = 0 To grdViewAssets.Items.Count - 1
        '        Dim lblstatus As Label = CType(grdViewAssets.Items(i).FindControl("lblStatusId"), Label)
        '        Dim hLinkFeedBack As HyperLink = CType(grdViewAssets.Items(i).FindControl("hLinkFeedBack"), HyperLink)
        '        Dim hLinkDetails As HyperLink = CType(grdViewAssets.Items(i).FindControl("hLinkDetails"), HyperLink)
        '        If lblstatus.Text = "8" Then
        '            hLinkFeedBack.Visible = True
        '            hLinkDetails.Visible = False
        '        Else
        '            hLinkFeedBack.Visible = False
        '            hLinkDetails.Visible = True
        '        End If
        '    Next
        'Else
        '    grdViewAssets.DataSource = Nothing
        '    grdViewAssets.DataBind()
        'End If
    End Sub

End Class
