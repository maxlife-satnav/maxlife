Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_MaintenanceMaster_frmAssetGroupsType
    Inherits System.Web.UI.Page

    Dim intstatus As Integer
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strname As String
    Dim strdesc As String

    Public Sub cleardata()
        Try
            txtcode.Text = ""
            txtName.Text = ""
            txtrem.Text = ""
            cmbPKeys.SelectedIndex = 0
            cmbFKeys.SelectedIndex = 0
            cmbastype.SelectedIndex = 0
            cmbspaces.SelectedIndex = 0
            ddlStatus.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            binddata()
            rbActions.Checked = True
            LblPageStatus.Text = "Add"
            'cmbFKeys.Items.Clear()
            txtcode.Enabled = True
            cleardata()
            BindGrid()
            LblFinalStatus.Text = ""
        End If
    End Sub

    Private Sub binddata()
        ObjSubsonic.Binddropdown(cmbPKeys, "MAS_GET_GRPTYPES", "AGT_NAME", "AGT_CODE")
        ObjSubsonic.Binddropdown(cmbFKeys, "MAS_GET_ALLGRP_DTLS", "AAG_NAME", "AAG_CODE")
    End Sub

    Private Sub Insertdata()

        'chk for length of remarks  > 500
        'If ((Len(Trim(txtrem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
        '    LblFinalStatus.Text = "Please Enter Name and Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtrem.Text)) >= 500 Then
        '    LblFinalStatus.Text = "Please Enter Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtName.Text)) >= 500 Then
        '    LblFinalStatus.Text = "Please Enter Name in not more than 500 Characters!"
        'Else
        'chk for duplicate entries

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_GRP_TYPE_CODE")
        sp.Command.AddParameter("@GRP_TYPE_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)

        'strSQL = "select count(*) from  AMG_ASSET_GROUPTYPE where AGT_CODE='" & Replace(Trim(txtcode.Text), "'", "''") & "'"
        'strSQL1 = "select count(*) from  AMG_ASSET_GROUPTYPE where AGT_NAME='" & Replace(Trim(txtName.Text), "'", "''") & "'"
        Dim iVar1, iVar2 As Integer
        ObjDR = sp.GetReader()
        ObjDR.Read()
        iVar1 = ObjDR(0)
        ObjDR.Close()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_GRP_TYPE_CODE1")
        sp1.Command.AddParameter("@GRP_TYPE_CODE1", Replace(Trim(txtName.Text), "'", "''"), DbType.String)
        ObjDR = sp1.GetReader()
        ObjDR.Read()
        iVar2 = ObjDR(0)
        ObjDR.Close()
        If iVar1 > 0 Then
            LblFinalStatus.Text = "Asset Group Type With The Same Code Already Exists..."
            'ElseIf iVar2 > 0 Then
            '    LblFinalStatus.Text = "Asset Group Type With The Same Name already exists.Please modify !"
        Else
            'intstatus = 1

            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_INSRT_GRPTYPES")
            sp2.Command.AddParameter("@AGT_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
            sp2.Command.AddParameter("@AGT_NAME", Replace(UCase(Trim(txtName.Text)), "'", "''"), DbType.String)
            sp2.Command.AddParameter("@AGT_STA_ID", ddlStatus.SelectedValue, DbType.Int32)
            sp2.Command.AddParameter("@AGT_UPT_BY", Session("UID"), DbType.String)
            sp2.Command.AddParameter("@AGT_UPT_DT", getoffsetdate(Date.Today), DbType.DateTime)
            sp2.Command.AddParameter("@AGT_REM", Replace(Trim(txtrem.Text), "'", "''"), DbType.String)
            sp2.Command.AddParameter("@AGT_AGPM_ID", cmbFKeys.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@AGT_MFTYPE", cmbastype.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@AGT_STYPE", cmbspaces.SelectedItem.Value, DbType.String)
            sp2.ExecuteScalar()

            LblFinalStatus.Text = " Asset Group Type Added Successfully..."
            'Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=56")
            cleardata()
            BindGrid()
            'binddata()
            'End If
        End If
    End Sub

    Private Sub modifydata()
        'chk for length of remarks  > 500
        'If ((Len(Trim(txtrem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
        '    LblFinalStatus.Text = "Please Enter Name and Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtrem.Text)) >= 500 Then
        '    LblFinalStatus.Text = "Please Enter Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtName.Text)) >= 500 Then
        '    LblFinalStatus.Text = "Please Enter Name in not more than 500 Characters!"
        'Else
        '    intstatus = 1
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_UPDT_GRPTYPES")
        sp2.Command.AddParameter("@AGT_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
        sp2.Command.AddParameter("@AGT_NAME", Replace(UCase(Trim(txtName.Text)), "'", "''"), DbType.String)
        sp2.Command.AddParameter("@AGT_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp2.Command.AddParameter("@AGT_REM", Replace(Trim(txtrem.Text), "'", "''"), DbType.String)
        sp2.Command.AddParameter("@AGT_AGPM_ID", cmbFKeys.SelectedItem.Value, DbType.String)
        sp2.Command.AddParameter("@AGT_MFTYPE", cmbastype.SelectedItem.Value, DbType.String)
        sp2.Command.AddParameter("@AGT_STYPE", cmbspaces.SelectedItem.Value, DbType.String)
        sp2.ExecuteScalar()


        LblFinalStatus.Text = " Asset Group Type Modified Successfully..."
        'Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=24")
        cleardata()
        BindGrid()
        'binddata()
        'End If
    End Sub

    Private Sub cmbPKeys_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPKeys.SelectedIndexChanged

        Dim i As Integer
        If cmbPKeys.SelectedItem.Value <> "--Select--" Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_GRP_TYPE_GET_dETAILS")
            sp.Command.AddParameter("@GRP_TYPE_CODE", cmbPKeys.SelectedItem.Value, DbType.String)
            'strSQL = "select * from AMG_ASSET_GROUPTYPE where AGT_CODE =" & cmbPKeys.SelectedItem.Value
            ObjDR = sp.GetReader()
            'Do While ObjDR.Read
            While ObjDR.Read


                txtcode.Text = ObjDR("AGT_CODE").ToString
                txtcd.Text = ObjDR("AGT_CODE").ToString
                txtName.Text = ObjDR("AGT_NAME").ToString
                txtrem.Text = ObjDR("AGT_REM").ToString
                intstatus = ObjDR("AGT_STA_ID").ToString
                cmbFKeys.ClearSelection()
                cmbFKeys.Items.FindByValue(ObjDR("AGT_AGPM_ID")).Selected = True
                cmbastype.ClearSelection()
                cmbastype.Items.FindByValue(ObjDR("AGT_MFTYPE")).Selected = True
                cmbspaces.ClearSelection()
                cmbspaces.Items.FindByValue(ObjDR("AGT_STYPE")).Selected = True
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ObjDR("AGT_STA_ID")).Selected = True
                'For i = 0 To cmbFKeys.Items.Count - 1
                '    If cmbFKeys.Items(i).Value = ObjDR("AGT_AGPM_ID").ToString Then
                '        cmbFKeys.SelectedIndex = i
                '        Exit For
                '    End If
                'Next
                'For i = 0 To cmbastype.Items.Count - 1
                '    If cmbastype.Items(i).Value = ObjDR("AGT_MFTYPE").ToString Then
                '        cmbastype.SelectedIndex = i
                '    End If
                'Next
                'For i = 0 To cmbspaces.Items.Count - 1
                '    If cmbspaces.Items(i).Text = ObjDR("AGT_STYPE").ToString Then
                '        cmbspaces.SelectedIndex = i
                '    End If
                'Next
                'Loop
            End While
            ObjDR.Close()
            LblFinalStatus.Text = ""
        Else
            cleardata()
            binddata()
        End If

    End Sub

    Private Sub rbActions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            LblPageStatus.Text = "Add"
            cmbFKeys.Items.Clear()
            btnSubmit.Text = "Add"
            'lblasgrp.Visible = False
            'cmbPKeys.Visible = False
            trgroup.Visible = False
            txtcode.Enabled = True
            txtName.Enabled = True
            binddata()
            cleardata()
            LblFinalStatus.Text = ""
        Else
            LblPageStatus.Text = "Modify"
            btnSubmit.Text = "Modify"
            cmbPKeys.Items.Clear()
            cmbFKeys.Items.Clear()
            'lblasgrp.Visible = True
            'cmbPKeys.Visible = True
            trgroup.Visible = True
            txtcode.Enabled = False
            txtName.Enabled = True
            binddata()
            cleardata()
            LblFinalStatus.Text = ""
        End If

          

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If Not IsValid Then
            Exit Sub
        End If
        If sender.Text = "Add" Then
            Insertdata()
        ElseIf sender.Text = "Modify" Then
            'chk for duplicate entries
            If txtcode.Text <> txtcd.Text Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_GRP_TYPE_CODE")
                sp.Command.AddParameter("@GRP_TYPE_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
                'StrSQL = "select count(*) from AMG_ASSET_GROUPTYPE where AGT_CODE='" _
                '& Replace(Trim(txtcode.Text), "'", "''") & "'"
                Dim iVar As Integer
                ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                ObjDR.Read()
                iVar = ObjDR(0)
                ObjDR.Close()

                If iVar > 0 Then
                    LblFinalStatus.Text = "Asset Group Type With The Same Code Already Exists..."
                    Exit Sub
                Else
                    modifydata()
                End If
            Else
                modifydata()
            End If
        End If

    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_ASSET_GROUPS_TYPES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvAssetGroupType.DataSource = ds
        gvAssetGroupType.DataBind()
        For i As Integer = 0 To gvAssetGroupType.Rows.Count - 1
            Dim lblstatus As Label = CType(gvAssetGroupType.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
    End Sub

    Protected Sub gvAssetGroupType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvAssetGroupType.PageIndexChanging
        gvAssetGroupType.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

End Class
