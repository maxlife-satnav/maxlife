<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmAssetModels.aspx.vb" Inherits="MaintenanceManagement_MaintenanceMaster_frmAssetModels"
    Title="Asset Models" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Asset Model Master 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="true" CssClass="alert alert-danger" ForeColor="Red"
                            ShowMessageBox="false"></asp:ValidationSummary>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="LblFinalStatus" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                        <asp:Label ID="LblPageStatus" runat="server" Visible="False" CssClass="col-md-12 control-label"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Asset Model and Select Modify to modify the existing Asset Model" />
                                        Add
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Asset Model and Select Modify to modify the existing Asset Model" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trgroup" runat="server" visible="False">
                                        <label class="col-md-5 control-label">
                                            Select Asset Models <span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmpPkeys" runat="server" Operator="NotEqual" ValueToCompare="--Select--"
                                            ErrorMessage="Please Select Asset Model" ControlToValidate="cmbPKeys" Display="None"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbPKeys" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true" ToolTip="Select Asset Model">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Model Code <span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="regXCode" runat="server" ErrorMessage="Enter Valid Code!!"
                                            ControlToValidate="txtcode" Display="None" ValidationExpression="^[a-z/0-9A-Z- ]+"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvcode" runat="server" Width="48px" ErrorMessage="Please Enter Asset Model Code"
                                            ControlToValidate="txtcode" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtcode" TabIndex="1" runat="server" CssClass="form-control"
                                                MaxLength="30" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Model Name <span style="color: red;">*</span></label>
                                        <asp:TextBox ID="txtcd" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" Width="48px" ErrorMessage="Please Enter Asset Model Name"
                                            ControlToValidate="txtName" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegXName" runat="server" ErrorMessage="Enter Only Alphabets and Numbers "
                                            ControlToValidate="txtName" Display="None" ValidationExpression="^[a-z&amp;0-9/A-Z ]+"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtName" TabIndex="2" runat="server" CssClass="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Brand <span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CmpFkeys" runat="server" Operator="NotEqual" ValueToCompare="--Select--"
                                            ErrorMessage="Please Select Asset Brand" ControlToValidate="cmbFKeys" Display="None"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbFKeys" TabIndex="3" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true" ToolTip="Select Asset Brand">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Status<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlStatus"
                                            ErrorMessage="Please Select Status" ValueToCompare="--Select--"
                                            Operator="NotEqual"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status">
                                                <asp:ListItem>--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>

                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRem"
                                            Display="None" ErrorMessage="Please Enter Remarks  "></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtRem" TabIndex="5" runat="server" CssClass="form-control"
                                                MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" TabIndex="5" runat="server" CssClass="btn btn-primary custom-button-color"
                                        Text="Add"></asp:Button>
                                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmMaintenanceMasters.aspx"
                                        Text="Back" />

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    Width="100%" PageSize="10" EmptyDataText="No Asset Model found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsno" runat="server" Text='<%#Eval("AAM_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Model Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllcode" runat="server" Text='<%#Eval("AAM_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Model Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("AAM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Brand">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAGT" runat="server" Text='<%#Eval("AAM_ABM_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("AAM_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
