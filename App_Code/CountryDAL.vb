Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports Amantra.CountryDTON

Namespace Amantra.CountryDALN

    Public Class CountryDAL

#Region "BLL and DTO Instance declarations"

        Private m_Classname As String = "CountryDAL"
        Private cDTO As New CountryDTO()

#End Region

#Region "Private Variable Declaration"
        Private insertdata As Integer = 0
        Private countryId As Integer = 0

#End Region

#Region " insert country details"
        Public Function insertCountry(ByVal cdto As CountryDTO) As Integer

            Dim m_Methodname As String = "insertCountry"
            Dim CountryList As IList(Of CountryDTO) = New List(Of CountryDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@Country_id", SqlDbType.Int), New SqlParameter("@vc_countryname", SqlDbType.NVarChar, 50), New SqlParameter("@vc_countrycode", SqlDbType.NVarChar, 50), New SqlParameter("@vc_countryrem", SqlDbType.NVarChar, 500), New SqlParameter("@vc_cntstaid", SqlDbType.Int), New SqlParameter("@vc_cntusrid", SqlDbType.NVarChar, 10)}

            parms(0).Direction = ParameterDirection.Output
            parms(1).Value = cdto.CountryName
            parms(2).Value = cdto.CountryCode
            parms(3).Value = cdto.CountryRemarks
            parms(4).Value = cdto.CountryStatus
            parms(5).Value = cdto.CountryUser


            Try
                insertdata = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_AmantraAxis_Country_InsertCountry", parms)
                'insertdata = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionString, CommandType.StoredProcedure, " usp_AmantraAxis_Country_InsertCountry", parms)
                If insertdata <> -1 Then
                    If Convert.ToString(parms(0).Value) = String.Empty Then
                        'If TravelId is Null then TravelId returns 0 
                        countryId = 0
                    Else
                        ' If TravelId is Not Null then TravelId returns Value 
                        countryId = Convert.ToInt32(parms(0).Value)

                    End If
                End If
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in DAL", m_Classname, m_Methodname, ex)
            End Try
            Return insertdata
        End Function
        '' ----------------------------------------------------------------------------------------------- 
#End Region
    End Class

End Namespace
