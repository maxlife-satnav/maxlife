﻿Imports Microsoft.VisualBasic

Public Class LeaseExpenses
    Public Property PM_EXP_HEAD() As String
        Get
            Return m_PM_EXP_HEAD
        End Get
        Set(value As String)
            m_PM_EXP_HEAD = value
        End Set
    End Property
    Private m_PM_EXP_HEAD As String
    Public Property PM_EXP_SERV_PROVIDER() As String
        Get
            Return m_PM_EXP_SERV_PROVIDER
        End Get
        Set(value As String)
            m_PM_EXP_SERV_PROVIDER = value
        End Set
    End Property
    Private m_PM_EXP_SERV_PROVIDER As String
    Public Property PM_EXP_INP_TYPE() As String
        Get
            Return m_PM_EXP_INP_TYPE
        End Get
        Set(value As String)
            m_PM_EXP_INP_TYPE = value
        End Set
    End Property
    Private m_PM_EXP_INP_TYPE As String
    Public Property PM_EXP_LES_VAL() As String
        Get
            Return m_PM_EXP_LES_VAL
        End Get
        Set(value As String)
            m_PM_EXP_LES_VAL = value
        End Set
    End Property
    Private m_PM_EXP_LES_VAL As Decimal
    Public Property PM_EXP_PAID_BY() As String
        Get
            Return m_PM_EXP_PAID_BY
        End Get
        Set(value As String)
            m_PM_EXP_PAID_BY = value
        End Set
    End Property
    Private m_PM_EXP_PAID_BY As String
End Class
