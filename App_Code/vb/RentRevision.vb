﻿Imports Microsoft.VisualBasic

Public Class RentRevision
    Public Property RR_Year() As String
        Get
            Return m_RR_Year
        End Get
        Set(value As String)
            m_RR_Year = value
        End Set
    End Property
    Private m_RR_Year As String
    Public Property RR_Percentage() As String
        Get
            Return m_RR_Percentage
        End Get
        Set(value As String)
            m_RR_Percentage = value
        End Set
    End Property
    Private m_RR_Percentage As String
End Class


Public Class ImageClas
    Private _fn As String

    Public Property Filename() As String
        Get
            Return _fn
        End Get
        Set(ByVal value As String)
            _fn = value
        End Set
    End Property

    Private _Fpath As String

    Public Property FilePath() As String
        Get
            Return _Fpath
        End Get
        Set(ByVal value As String)
            _Fpath = value
        End Set
    End Property

End Class

Public Class L1Approval

    Public Property Lease_ID() As String
        Get
            Return m_Lease_ID
        End Get
        Set(value As String)
            m_Lease_ID = value
        End Set
    End Property
    Private m_Lease_ID As String

    Public Property Lease_SNO() As Integer
        Get
            Return m_Lease_SNO
        End Get
        Set(value As Integer)
            m_Lease_SNO = value
        End Set
    End Property
    Private m_Lease_SNO As Integer

    Public Property L1_REMARKS() As String
        Get
            Return m_L1_REMARKS
        End Get
        Set(value As String)
            m_L1_REMARKS = value
        End Set
    End Property
    Private m_L1_REMARKS As String

End Class

