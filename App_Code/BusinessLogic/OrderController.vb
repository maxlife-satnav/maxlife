#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common
Imports Commerce.Providers
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports System.Collections
Imports SubSonic

''' <summary>
''' Summary description for OrderController
''' </summary>
Public Class OrderController
  ''' <summary>
  ''' Uses the tax provider to calculate sales tax based on zipcode and subtotal. Returns 
  ''' the rate as a decimal
  ''' </summary>
  ''' <param name="zip"></param>
  ''' <param name="subtotal"></param>
  ''' <returns>System.Decimal</returns>
  Private Sub New()
  End Sub
  Public Shared Function CalculateTax(ByVal zip As String, ByVal subtotal As Decimal) As Decimal

	Return TaxService.CalculateAmountByZIP(zip, subtotal)

  End Function

  ''' <summary>
  ''' Cancel's a pending order
  ''' </summary>
  ''' <param name="order"></param>
  ''' <param name="cancellationReason"></param>
  Public Shared Sub CancelOrder(ByVal order As Order, ByVal cancellationReason As String)

	'first, update the status
	order.OrderStatus = OrderStatus.OrderCancelledPriorToShipping
	order.Save(Utility.GetUserName())

	'next, add a note to say it was cancelled
	Dim note As OrderNote = New OrderNote()
	note.OrderID = order.OrderID
	note.OrderStatus = System.Enum.GetName(GetType(OrderStatus), OrderStatus.OrderCancelledPriorToShipping)
	note.Note = "Order Cancelled by " & Utility.GetUserName() & ": " & cancellationReason
	note.Save(Utility.GetUserName())


  End Sub


  Public Shared Sub AddNote(ByVal note As String, ByVal order As Order)

	Dim orderStatus As String = order.OrderStatus.ToString()
	If orderStatus = "NotProcessed" Then
	  orderStatus = "Not checked out"
	End If

	OrderNote.Insert(order.OrderID, note, orderStatus)
  End Sub

  #Region "Getters"

  ''' <summary>
  ''' Returns a filtered order set, based on the passed-in information
  ''' </summary>
  ''' <param name="dtStart">Beginning of date range</param>
  ''' <param name="dtEnd">End of date range</param>
  ''' <param name="userName">User's name</param>
  ''' <param name="orderNumber">Order number (partial)</param>
  ''' <returns>System.Data.IDataReader</returns>
  Public Shared Function GetAllByFilter(ByVal dtStart As DateTime, ByVal dtEnd As DateTime, ByVal userName As String, ByVal orderNumber As String) As IDataReader

	Return SPs.StoreOrderQuery(dtStart, dtEnd, userName, orderNumber).GetReader()

    End Function
    ''' <summary>
    ''' Returns a filtered order set, based on the passed-in information
    ''' </summary>
    ''' <param name="dtStart">Beginning of date range</param>
    ''' <param name="dtEnd">End of date range</param>
    ''' <param name="userName">User's name</param>
    ''' <param name="orderNumber">Order number (partial)</param>
    ''' <returns>System.Data.Dataset</returns>
    Public Shared Function GetAllByFilter_DataSet(ByVal dtStart As DateTime, ByVal dtEnd As DateTime, ByVal userName As String, ByVal orderNumber As String) As DataSet
        Return SPs.StoreOrderQuery(dtStart, dtEnd, userName, orderNumber).GetDataSet
    End Function
  ''' <summary>
  ''' Get's all orders for the logged-in user
  ''' </summary>
  ''' <returns>OrderCollection</returns>
  Public Shared Function GetByUser() As OrderCollection
	Return GetByUser(Utility.GetUserName())
  End Function
  ''' <summary>
  ''' Get's all orders for the specified user
  ''' </summary>
  ''' <returns>OrderCollection</returns>

  Public Shared Function GetByUser(ByVal userName As String) As OrderCollection

	Return New OrderCollection().Where("userName", userName).Where("orderStatusID", Comparison.NotEquals, CInt(Fix(OrderStatus.NotProcessed))).OrderByDesc("orderDate").Load()

  End Function
  ''' <summary>
  ''' Returns any unchecked out order (aka "The Cart") for the currently logged-in user
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GetCurrentOrder() As Order
	Dim orderID As Integer = GetCartOrderID()
	Return GetOrder(orderID)

  End Function
  ''' <summary>
  ''' Returns any unchecked out order (aka "The Cart") for a user
  ''' </summary>
  ''' <returns></returns>
    Public Shared Function GetCurrentOrder(ByVal userName As String) As Order
        'LJD 10/17/2007 Added Commerec.Common in front of Order.GetTableSchema
        Dim rdr As IDataReader = New Query(Commerce.Common.Order.GetTableSchema()).AddWhere("OrderStatusID", OrderStatus.NotProcessed).AddWhere("userName", userName).ExecuteReader()
        Dim order As Order = New Order()
        order.Load(rdr)
        rdr.Close()

        Return order

    End Function
  ''' <summary>
  ''' Returns an order by it's GUID ID.
  ''' </summary>
  ''' <param name="orderID"></param>
  ''' <returns></returns>
  Public Shared Function GetOrder(ByVal orderID As Integer) As Order

	Dim order As Order = New Order()
	Dim ds As DataSet = New DataSet()
	'set to false since this is not a new order
	order.IsNew = False
	Dim sql As String = ""

	'use a multiple return set to load up the order info

        '0 - the order itself
        'LJD 10/17/2007 Added Commerce.Common in front of order
        Dim q As Query = New Query(Commerce.Common.Order.GetTableSchema())
	q.AddWhere("orderID", orderID)

	'append the sql
	sql &= DataService.GetSql(q) & Constants.vbCrLf

	'1 - items
	q = New Query(OrderItem.GetTableSchema())
	q.AddWhere("orderID", orderID)
	q.OrderBy = OrderBy.Asc("createdOn")

	'append the sql
	sql &= DataService.GetSql(q) & Constants.vbCrLf

	'2 - notes
	q = New Query(OrderNote.GetTableSchema())
	q.AddWhere("orderID", orderID)

	'append the sql
	sql &= DataService.GetSql(q) & Constants.vbCrLf

	'3 - transactions
	q = New Query(Transaction.GetTableSchema())
	q.AddWhere("orderID", orderID)

	'append the sql
	sql &= DataService.GetSql(q) & Constants.vbCrLf

	'Build the Command
	Dim cmd As QueryCommand = New QueryCommand(sql)
	cmd.AddParameter("@orderID", orderID, DbType.Int32)

	'load the dataset
	ds = DataService.GetDataSet(cmd)

	'load up the bits - order first
	order.Load(ds.Tables(0))

	'then items
	order.Items = New OrderItemCollection()
	order.Items.Load(ds.Tables(1))
	If order.Items.Count > 0 Then
	  order.LastAdded = order.Items(order.Items.Count - 1)
	End If

	'notes
	order.Notes = New OrderNoteCollection()
	order.Notes.Load(ds.Tables(2))

	'transactions
	order.Transactions = New TransactionCollection()
	order.Transactions.Load(ds.Tables(3))


	Return order
  End Function

  ''' <summary>
  ''' Get's an order based on it's unique ID
  ''' </summary>
  ''' <param name="orderGuid"></param>
  ''' <returns></returns>
  Public Shared Function GetOrder(ByVal orderGuid As String) As Order
	Dim orderID As Integer = 0
	Dim q As Query = New Query(Order.GetTableSchema())
	q.AddWhere("orderGuid", orderGuid)
	q.SelectList = "orderID"
	Dim result As Object = q.ExecuteScalar()

	If Not result Is Nothing Then
	  orderID = CInt(Fix(result))
	End If

	Return GetOrder(orderID)
  End Function

    ''' <summary>
    ''' Get's an order based on it's order no
    ''' </summary>
    ''' <param name="orderNumber"></param>
    ''' <returns></returns>
    Public Shared Function GetOrderByOrderNumber(ByVal orderNumber As String) As Order
        Dim orderID As Integer = 0
        Dim q As Query = New Query(Order.GetTableSchema())
        q.AddWhere("orderNumber", orderNumber)
        q.SelectList = "orderID"
        Dim result As Object = q.ExecuteScalar()

        If Not result Is Nothing Then
            orderID = CInt(Fix(result))
        End If

        Return GetOrder(orderID)
    End Function
#End Region


  #Region "Address Methods"
  ''' <summary>
  ''' Get's a count of a user's addresses
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GetAddressBookCount() As Integer
	Dim where As Where = New Where()
	where.ColumnName = "userName"
	where.ParameterValue = Utility.GetUserName()

	Dim iOut As Integer = Query.GetCount("CSK_Store_Address", "addressID", where)
	Return iOut

  End Function

  ''' <summary>
  ''' Returns all addresses stored for a user
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GetUserAddresses() As AddressCollection
	Return New AddressCollection().Where("userName", Utility.GetUserName()).Load()
  End Function
  ''' <summary>
  ''' Saves the address to the DB
  ''' </summary>
  ''' <param name="address"></param>
  ''' <param name="isBilling"></param>
    Public Shared Sub SaveAddress(ByVal address1 As Address)

        'check to see if this address exists
        Dim addCheck As Address = New Address()
        addCheck.Address1 = address1.Address1
        addCheck.City = address1.City
        addCheck.StateOrRegion = address1.StateOrRegion
        addCheck.Country = address1.Country
        addCheck.UserName = address1.UserName

        'use the Find() method to see if this address exists
        Dim list As AddressCollection = New AddressCollection()
        Dim rdr As IDataReader = address.Find(addCheck)
        list.Load(rdr)
        rdr.Close()

        If list.Count > 0 Then
            'this is an old address
            'set the address ID and mark it as old
            'this will tell the base class to run an Update
            address1.AddressID = list(0).AddressID
            address1.IsNew = False

        End If
        'save it back
        address1.Save(Utility.GetUserName())

    End Sub
  #End Region

  #Region "Permission Sets"
  ''' <summary>
  ''' Defines if an order can be refunded. Used on the Admin_Orders page
  ''' </summary>
  ''' <param name="order"></param>
  ''' <returns></returns>
  Public Shared Function CanRefund(ByVal order As Order) As Boolean
	Dim bOut As Boolean = False

	'if the order is not set to shipped
	'or if it hasn't already been refunded
	'we're good to go

	'your rules might be different, alter as needed
	Select Case order.OrderStatus
	  Case OrderStatus.NotProcessed
	  Case OrderStatus.ReceivedAwaitingPayment
		bOut = True
	  Case OrderStatus.ReceivedPaymentProcessingOrder
		bOut = True
	  Case OrderStatus.GatheringItemsFromInventory
		bOut = True
	  Case OrderStatus.AwatingShipmentToCustomer
		bOut = True
	  Case OrderStatus.DelayedItemsNotAvailable
	  Case OrderStatus.ShippedToCustomer
	  Case OrderStatus.DelayedReroutingShipping
	  Case OrderStatus.DelayedCustomerRequest
		bOut = True
	  Case OrderStatus.DelayedOrderUnderReview
		bOut = True
	  Case OrderStatus.OrderCancelledPriorToShipping
	  Case OrderStatus.OrderRefunded
	  Case Else
	End Select

	'make sure the order total is valid
	If order.CalculateSubTotal() <= 0 Then
	  bOut = False
	End If

	Return bOut
  End Function

  ''' <summary>
  ''' Defines if an order can be shipped
  ''' </summary>
  ''' <param name="order"></param>
  ''' <returns></returns>
  Public Shared Function CanShip(ByVal order As Order) As Boolean
	Dim bOut As Boolean = False
	'if the order is not set to shipped
	'or if it hasn't already been refunded
	'we're good to go

	'your rules might be different, alter as needed
	Select Case order.OrderStatus
	  Case OrderStatus.NotProcessed
	  Case OrderStatus.ReceivedAwaitingPayment
		bOut = True
	  Case OrderStatus.ReceivedPaymentProcessingOrder
		bOut = True
	  Case OrderStatus.GatheringItemsFromInventory
		bOut = True
	  Case OrderStatus.AwatingShipmentToCustomer
		bOut = True
	  Case OrderStatus.DelayedItemsNotAvailable
	  Case OrderStatus.ShippedToCustomer
	  Case OrderStatus.DelayedReroutingShipping
	  Case OrderStatus.DelayedCustomerRequest
		bOut = True
	  Case OrderStatus.DelayedOrderUnderReview
		bOut = True
	  Case OrderStatus.OrderCancelledPriorToShipping
	  Case OrderStatus.OrderRefunded
	  Case Else
	End Select

	Return bOut

  End Function

  ''' <summary>
  ''' Defines if an order can be cancelled
  ''' </summary>
  ''' <param name="order"></param>
  ''' <returns></returns>
  Public Shared Function CanCancel(ByVal order As Order) As Boolean
	Dim bOut As Boolean = True
	Select Case order.OrderStatus
	  Case OrderStatus.NotProcessed
	  Case OrderStatus.ReceivedAwaitingPayment
	  Case OrderStatus.ReceivedPaymentProcessingOrder
	  Case OrderStatus.GatheringItemsFromInventory
	  Case OrderStatus.AwatingShipmentToCustomer
	  Case OrderStatus.DelayedItemsNotAvailable
	  Case OrderStatus.ShippedToCustomer
		bOut = False
	  Case OrderStatus.DelayedReroutingShipping
	  Case OrderStatus.DelayedCustomerRequest
	  Case OrderStatus.DelayedOrderUnderReview
	  Case OrderStatus.OrderCancelledPriorToShipping
		bOut = False
	  Case OrderStatus.OrderRefunded
		bOut = False
	  Case Else
	End Select
	Return bOut

  End Function
  #End Region

  #Region "Transaction Methods"

  ''' <summary>
  ''' Validates an order before the transaction tabkes place
  ''' </summary>
  ''' <param name="order">The Commerce.Common.Order</param>
  ''' <param name="validateAddress">Whether to validate billing/shipping</param>
  Private Shared Sub ValidateOrder(ByVal order As Order, ByVal validateAddress As Boolean)

	If validateAddress Then
	  'need to have shipping and billing
	  TestCondition.IsNotNull(order.ShippingAddress, "Need a shipping address")
	  TestCondition.IsNotNull(order.BillingAddress, "Need a billing address")

	End If

	'need to have the IP, email, first and last set
	TestCondition.IsNotNull(order.UserIP, "Need a valid IP address for this order")
	TestCondition.IsNotNull(order.Email, "Need a valid Email for this order")
	TestCondition.IsNotNull(order.FirstName, "Need a valid First Name for this order")
	TestCondition.IsNotNull(order.LastName, "Need a valid Last Name for this order")

	'validation
	'uncomment the following line if you don't give out freebies
	'TestCondition.IsGreaterThanZero(order.CalculateSubTotal(), "Invalid Subtotal");
	TestCondition.IsNotEmptyString(order.UserName, "Invalid User Name for the Order. Please set Order.UserName accordingly.")
	TestCondition.IsGreaterThanZero(order.Items.Count, "No items have been added to the order")


	'finally, for good measure, go through each monetary figure
	'and make sure it's decimal setting is appropriate
	'the subtotal is always rounded
	Dim currencyDecimals As Integer = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits
	order.ShippingAmount = Math.Round(order.ShippingAmount, currencyDecimals)
	order.TaxAmount = Math.Round(order.TaxAmount, currencyDecimals)
	For Each item As OrderItem In order.Items
	  item.PricePaid = Math.Round(item.PricePaid, currencyDecimals)
	  item.OriginalPrice = Math.Round(item.OriginalPrice, currencyDecimals)
	Next item


  End Sub

  #Region "Chargers"
  ''' <summary>
  ''' Executes the charge using the gateway of choice. If you are going to accept
  ''' Different types of payments (like POs or Offline Cards), then this is 
  ''' the place to put your routines.
  ''' </summary>
  ''' <param name="transType">Enum to define type of purchase</param>
  ''' <returns>Commerce.Common.Transaction</returns>
    ' Private Shared Function ExecuteCharge(ByVal order As Order, ByVal transType As TransactionType) As Transaction
    'Dim trans As Transaction = Nothing
    'If transType = TransactionType.CreditCardPayment Then
    '  'if this is successful, there's no going back
    '  'NO errors after this!
    '  trans = Commerce.Providers.PaymentService.RunCharge(order)
    '  trans.TransactionNotes = "Credit card transaction completed " & DateTime.UtcNow.ToString()

    'ElseIf transType = TransactionType.PayPalPayment Then
    '  'this is an express checkout, use the PayPal API
    '  'string certPath = HttpContext.Current.Server.MapPath("~/App_Data/" + SiteConfig.PayPalAPICertificate);
    '           Dim wrapper As Commerce.PayPal.APIWrapper = New Commerce.PayPal.APIWrapper(SiteConfig.PayPalAPIUserName, SiteConfig.PayPalAPIPassword, SiteConfig.PayPalAPISignature, SiteConfig.CurrencyCode, SiteConfig.PayPalAPIIsLive)

    '  'the token should be in the BillingAddress of the order
    '  'check to be sure
    '  TestCondition.IsNotEmptyString(order.BillingAddress.PayPalToken, "Invalid PayPal Token")

    '  trans = wrapper.DoExpressCheckout(order, SiteConfig.AuthorizeSaleOnly)
    '  If Not trans Is Nothing Then
    '	trans.TransactionNotes = "PayPal ExpressCheckout transaction completed " & DateTime.UtcNow.ToString()

    '	'an acknowledgement is received from PayPal
    '	'and it says "Success" if, well, it was a success
    '	If trans.GatewayResponse <> "Success" Then
    '	  Throw New Exception("PayPal ExpressCheckout Error: " & trans.GatewayResponse)
    '	End If

    '  End If

    'End If
    'If Not trans Is Nothing Then
    '  'add the baseline information
    '  trans.OrderID = order.OrderID
    '  trans.Amount = order.OrderTotal
    '  trans.TransactionType = transType
    '  trans.TransactionDate = DateTime.UtcNow

    'Else
    '  'if the transaction's null, an error occurred along the way
    '  'throw an exception, as the order is not valid
    '  Throw New Exception("Invalid transaction, the transaction did not record properly")
    'End If
    'Return trans

    ' End Function
#End Region

  ''' <summary>
  ''' Transacts an order
  ''' </summary>
  ''' <param name="order">The order to be transacted</param>
  ''' <param name="transType">The type of order (Credit Card, PO, etc)</param>
  ''' <returns>Commerce.Common.Transaction</returns>
  Public Shared Function TransactOrder(ByVal order As Order, ByVal transType As TransactionType) As Transaction
	Dim db As Database = DatabaseFactory.CreateDatabase()

	'Validate the Order
	ValidateOrder(order, True)

	'update the order info
	order.OrderDate = getoffsetdatetime(DateTime.Now)
        order.SubTotalAmount = order.CalculateSubTotal()
	order.UserIP = HttpContext.Current.Request.UserHostAddress

	'set the status to processing
	order.OrderStatus = OrderStatus.ReceivedPaymentProcessingOrder

	'setting the final order number. This is for internal reference
	'to your business. Update as needed to reflect your companies
	'ordering system
	Dim companyOrderIdentifier As String = ConfigurationManager.AppSettings("companyOrderIdentifier")
	Select Case transType
	  Case TransactionType.PayPalPayment
		order.OrderNumber = companyOrderIdentifier & "-PP-" & Utility.GetRandomString()
	  Case TransactionType.PurchaseOrder
		order.OrderNumber = companyOrderIdentifier & "-PO-" & Utility.GetRandomString()
	  Case TransactionType.CreditCardPayment, TransactionType.OfflineCreditCardPayment
		order.OrderNumber = companyOrderIdentifier & "-CC-" & Utility.GetRandomString()
	End Select
	Dim orderCommand As DbCommand = order.GetUpdateCommand("Order System").ToDbCommand()

	'create a note for the order
	Dim note As OrderNote = New OrderNote()
	note.Note = "Order Created"
	note.OrderID = order.OrderID
	note.OrderStatus = OrderStatus.ReceivedAwaitingPayment.ToString()


	Dim noteCommand As DbCommand = note.GetInsertCommand("Order System").ToDbCommand()

	'the transaction to hold the charge info
	Dim trans As Transaction = Nothing



	Using connection As DbConnection = db.CreateConnection()
	  connection.Open()
	  Dim transaction As DbTransaction = connection.BeginTransaction()

	  Try
		'Execute the order update
		db.ExecuteNonQuery(orderCommand, transaction)

		'add in the order note
		db.ExecuteNonQuery(noteCommand, transaction)

		'execute the charge - this charges the user
		'NO ERRORS after this, cannot roll back
                'trans = ExecuteCharge(order, transType)

		'add the transaction to the order
		If Not trans Is Nothing Then
		  order.Transactions.Add(trans)
		End If

		Try
		  'record the transaction
		  'since the order is paid, the order record MUST be comitted
		  'if this call fails, enter it manually
		  Dim transRecordCommand As DbCommand = trans.GetInsertCommand("Order System").ToDbCommand()

		  'finally add in the transaction
		  db.ExecuteNonQuery(transRecordCommand, transaction)

		  Dim successNote As OrderNote = New OrderNote()
		  successNote.Note = "Transaction for order " & order.OrderNumber & " successfully transacted"
		  successNote.OrderID = order.OrderID
		  successNote.OrderStatus = OrderStatus.ReceivedPaymentProcessingOrder.ToString()
		  db.ExecuteNonQuery(successNote.GetInsertCommand("Order System").ToDbCommand(), transaction)
		Catch x As Exception
		  'do nothing
		  'if the transaction record does not record in the DB for some reason
		  'we do NOT want to fail the overall transaction because they have already been
		  'charged. Log this failure....

		  'TODO: CMC - Log failure - Try to write to FileSystem - XML?

		End Try

		' Commit the transaction
		transaction.Commit()
		'close the connection ... we are done with it.  
		connection.Close()

		'send off notification *after* the transaction commits ...
		'that way, we close the transacation ASAP.  
		'send off the notifications
		'NO ERRORS 
		Try
		  'to user saying thank you
		  MessagingController.SendOrderReceived_Customer(order)

		  'to merchant
		  MessagingController.SendOrderReceived_Merchant(order)

		Catch
		  'TODO - CMC - Log Errror
		End Try

	  Catch x As Exception
		' Rollback transaction 
		transaction.Rollback()
		Throw x

	  End Try

	End Using

	Return trans

  End Function

  ''' <summary>
  ''' Refunds an order
  ''' </summary>
  ''' <param name="order"></param>
  Public Shared Sub Refund(ByVal order As Order)

	'add a transaction to the order, indicating it's been refunded
	Dim trans As Transaction = New Transaction()
	trans.Amount = 0 - order.OrderTotal
	trans.TransactionNotes = "Order Refunded on " & DateTime.UtcNow.ToString()
	trans.TransactionType = TransactionType.Refund
	trans.OrderID = order.OrderID
	trans.TransactionDate = DateTime.UtcNow
	Dim cmdTrans As DbCommand = trans.GetInsertCommand(Utility.GetUserName()).ToDbCommand()

	'add a note as well
	Dim note As OrderNote = New OrderNote()
	note.OrderID = order.OrderID
	note.Note = "Order Refunded"
	note.OrderStatus = System.Enum.GetName(GetType(OrderStatus), order.OrderStatus)
	Dim cmdNote As DbCommand = note.GetInsertCommand(Utility.GetUserName()).ToDbCommand()

	'set the status
	order.OrderStatus = OrderStatus.OrderRefunded
	Dim cmdOrder As DbCommand = order.GetUpdateCommand(Utility.GetUserName()).ToDbCommand()
	'commit
	Dim db As Database = DatabaseFactory.CreateDatabase()
	Using connection As DbConnection = db.CreateConnection()
	  connection.Open()
	  Dim transaction As DbTransaction = connection.BeginTransaction()

	  Try
		'get the commands

		'run the queries
		db.ExecuteNonQuery(cmdTrans, transaction)
		db.ExecuteNonQuery(cmdNote, transaction)
		db.ExecuteNonQuery(cmdOrder, transaction)

		'call the provider to refund the order
		'if this fails, toss the DB bits
		'NO ERRORS after this
		PaymentService.Refund(order)

		' Commit the transaction
		transaction.Commit()
		'close the connection ... we are done with it.  
		connection.Close()


	  Catch x As Exception
		' Rollback transaction 
		transaction.Rollback()
		Throw x

	  End Try


	End Using
  End Sub
  #Region "PP Standard Methods"
  ''' <summary>
  ''' This method creates an order in the database, without applying a transaction to it.
  ''' The order id is created for sending to PayPal in the "Custom" field. When the IPN/PDT 
  ''' come back to the site, this value is used to Reconcile the transaction.
  ''' </summary>
  Public Shared Sub CreateStandardOrder()
	Dim companyOrderIdentifier As String = ConfigurationManager.AppSettings("companyOrderIdentifier")
	Dim order As Order = GetCurrentOrder()

	'validate the order without address info since we won't know the billing address
	ValidateOrder(order, False)

	order.OrderGUID = System.Guid.NewGuid().ToString()

	'use your own logic for this one :)
	order.OrderNumber = companyOrderIdentifier & "-PPS-" & Utility.GetRandomString()

	'flag the status as awaiting payment
	order.OrderStatus = OrderStatus.ReceivedAwaitingPayment
	order.OrderDate = getoffsetdatetime(DateTime.Now)
	order.SubTotalAmount = order.CalculateSubTotal() - order.DiscountAmount

	'update the order status to "paid"
	Dim q As Query = New Query(Order.GetTableSchema())
	q.AddUpdateSetting("orderStatusID", OrderStatus.ReceivedPaymentProcessingOrder)
	q.AddWhere("orderID", order.OrderID)
	Dim orderCommand As DbCommand = q.BuildUpdateCommand().ToDbCommand()

	'create a note for the order
	Dim note As OrderNote = New OrderNote()
	note.Note = "Order Created"
	note.OrderID = order.OrderID
	note.OrderStatus = OrderStatus.ReceivedAwaitingPayment.ToString()

	'get the command to transact in the transaction
	Dim noteCommand As DbCommand = note.GetInsertCommand("Order System").ToDbCommand()


	'open the default db from the EL factory
	Dim db As Database = DatabaseFactory.CreateDatabase()
	Using connection As DbConnection = db.CreateConnection()
	  connection.Open()
	  Dim transaction As DbTransaction = connection.BeginTransaction()

	  Try
		'input the order
		db.ExecuteNonQuery(orderCommand, transaction)


		'add in the order note
		db.ExecuteNonQuery(noteCommand, transaction)


		' Commit the transaction
		transaction.Commit()
		'close the connection ... we are done with it.  
		connection.Close()


	  Catch x As Exception
		' Rollback transaction 
		transaction.Rollback()
		Throw x

	  End Try
	End Using
  End Sub

  ''' <summary>
  ''' This method commits the PayPal Standard order once received by the PDT or IPN.
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function CommitStandardOrder(ByVal order As Order, ByVal transactionID As String, ByVal reportedAmount As Decimal) As Transaction
	'the IPN and PDT return a transactionID as well as the amount paid.
	'make sure the amount is >= the amount of the transaction

	'create a transaction for this order
	Dim trans As Commerce.Common.Transaction = New Transaction()

	'if there are transactions for this order, that means that the PDT or IPN has already 
	'been received, so ignore it
	If order.Transactions.Count = 0 Then

	  'no transactions
	  'validate the transactionID
	  TestCondition.IsNotEmptyString(transactionID, "Invalid PayPal TransactionID")

	  'now check the amount of the order versus that paid for
            TestCondition.IsTrue(order.OrderTotal <= reportedAmount, "The amount returned does match that of the order")

	  'add the baseline information
	  trans.OrderID = order.OrderID
	  trans.Amount = order.OrderTotal

            If order.ShippingMethod = "COD" Then
                trans.TransactionType = TransactionType.CashOnDelivery
            Else
                trans.TransactionType = TransactionType.CreditCardPayment
            End If

	  trans.TransactionDate = DateTime.UtcNow
	  trans.AuthorizationCode = transactionID
            trans.TransactionNotes = "Standard Payment Received on " & DateTime.UtcNow.ToString() & ": ID=" & transactionID

	  'add a note to the Order
	  Dim note As OrderNote = New OrderNote()
            note.Note = "Payment received and applied"
	  note.OrderID = order.OrderID
	  note.OrderStatus = Utility.ParseCamelToProper(OrderStatus.ReceivedPaymentProcessingOrder.ToString())


	  'finally, update the order status
	  order.OrderStatus = OrderStatus.ReceivedPaymentProcessingOrder
	  order.OrderDate = getoffsetdatetime(DateTime.Now)
	  order.SubTotalAmount = order.CalculateSubTotal()

	  'save em down
	  'open the default db from the EL factory
	  Dim db As Database = DatabaseFactory.CreateDatabase()
	  Using connection As DbConnection = db.CreateConnection()
		connection.Open()
		Dim transaction As DbTransaction = connection.BeginTransaction()

		Try
		  'get the commands
		  Dim orderCommand As DbCommand = order.GetUpdateCommand("Order System").ToDbCommand()
		  Dim noteCommand As DbCommand = note.GetInsertCommand("Order System").ToDbCommand()
		  Dim transCommand As DbCommand = trans.GetInsertCommand("Order System").ToDbCommand()

		  'run the queries
		  db.ExecuteNonQuery(orderCommand, transaction)
		  db.ExecuteNonQuery(noteCommand, transaction)
		  db.ExecuteNonQuery(transCommand, transaction)

		  ' Commit the transaction
		  transaction.Commit()
		  'close the connection ... we are done with it.  
		  connection.Close()


		Catch x As Exception
		  ' Rollback transaction 
		  transaction.Rollback()
		  Throw x

		End Try

		'notifications
		Try
		  'send off notification *after* the transaction commits ...
		  MessagingController.SendOrderReceived_Customer(order)

		  'to merchant; this is redundant a bit, as the standard bits
		  'will tell merchant anyway.
		  MessagingController.SendOrderReceived_Merchant(order)
		Catch
		  'Do Nothing
		End Try
	  End Using
	Else
	  'set it to the first transaction for this order
	  trans = order.Transactions(0)
	End If

	Return trans

  End Function

  #End Region


  #End Region

  #Region "Cart Methods"
  ''' <summary>
  ''' Executes the CSK_Store_Order_MigrateOrder Stored Procedure, which merges the cart items 
  ''' from an anonymous account to a known account transactionally.
  ''' </summary>
  ''' <param name="oldUserName">Anonymous User</param>
  ''' <param name="newUserName">Known User</param>
  Public Shared Sub MigrateCart(ByVal oldUserName As String, ByVal newUserName As String)


	Dim fromOrder As Order = GetOrder(GetCartOrderID(oldUserName))
	Dim toOrder As Order = GetOrder(GetCartOrderID(newUserName))

	'first see if there is an order for the now-recognized user
	If toOrder.OrderNumber = String.Empty Then
	  'if not, just update the old order with the new user's username
	  fromOrder.UserName = newUserName
	  fromOrder.Save(Utility.GetUserName())
	Else
	  'the logged-in user has an existing basket. 
	  'if there is no basket from their anon session, 
	  'we don't need to do anything
	  If fromOrder.OrderNumber <> String.Empty Then

		'in this case, there is an order (cart) from their anon session
		'and an order that they had open from their last session
		'need to marry the items.

		'this part is up to your business needs - some merchants
		'will want to replace the cart contents
		'others will want to synch them. We're going to assume that
		'this scenario will synch the existing items.

		'############### Synch the Cart Items
		If toOrder.Items.Count > 0 AndAlso fromOrder.Items.Count > 0 Then
		  'there are items in both carts, move the old to the new
		  'when synching, find matching items in both carts
		  'update the quantities of the matching items in the logged-in cart
		  'removing them from the anon cart

		  'a switch to tell us if we need to update the from orders


		  '1.) Find items that are the same between the two carts and add the anon quantity to the registered user quantity
		  '2.) Mark found items as items to be removed from the anon cart.
		  Dim toBeRemoved As ArrayList = New ArrayList(fromOrder.Items.Count)
		  Dim i As Integer = 0
		  Do While i < fromOrder.Items.Count
			Dim foundItem As OrderItem = toOrder.Items.FindItem(fromOrder.Items(i))
			If Not foundItem Is Nothing Then
			  foundItem.Quantity &= fromOrder.Items(i).Quantity
			  toBeRemoved.Add(i)
			End If
			  i += 1
		  Loop
		  '3.) Now remove any foundItems from the anon cart, but trim it up first
		  toBeRemoved.TrimToSize()
		  i = 0
		  Do While i < toBeRemoved.Count
			fromOrder.Items.RemoveAt(CInt(Fix(toBeRemoved(i))))
			  i += 1
		  Loop

		  '4.) Move over to the registered user cart any remaining items in the anon cart.
		  For Each anonItem As OrderItem In fromOrder.Items
			'reset the orderID
			anonItem.OrderID = toOrder.OrderID
			toOrder.Items.Add(anonItem)
		  Next anonItem

		  '5.) Finally, save it down to the DB
		  ' (Since we know toOrder.Items.Count > 0 && fromOrder.Items.Count > 0, we know a Save needs to occur)
		  toOrder.SaveItems()
		ElseIf toOrder.Items.Count = 0 Then
		  'items exist only in the anon cart
		  'move the anon items to the new cart
		  'then save the order and the order items.
		  toOrder.IsNew = True
		  toOrder.OrderGUID = fromOrder.OrderGUID
		  toOrder.UserName = newUserName
		  toOrder.OrderNumber = fromOrder.OrderNumber
		  toOrder.OrderDate = fromOrder.OrderDate
		  toOrder.Save(newUserName)
		  For Each item As OrderItem In fromOrder.Items
			'reset the orderID on each item
			item.OrderID = toOrder.OrderID
			toOrder.Items.Add(item)
		  Next item
		  toOrder.SaveItems()

		ElseIf fromOrder.Items.Count = 0 Then
		  'no items in the old cart, do nothing
		End If


		'finally, drop the anon order from the DB, we don't want to 
		'keep it
		fromOrder.DeletePermanent()
	  End If
	End If

  End Sub

  ''' <summary>
  ''' Returns the number of items in the current cart
  ''' </summary>
  ''' <returns>System.Int32</returns>
  Public Shared Function GetCartItemCount() As Integer
	Dim orderID As Integer = GetCartOrderID()
	Dim where As Where = New Where()
	where.ColumnName = "orderID"
	where.ParameterValue = orderID

	Dim iOut As Integer = Query.GetCount("CSK_Store_OrderItem", "orderItemID", where)

	Return iOut
  End Function

  ''' <summary>
  ''' Returns the items in the current cart
  ''' </summary>
  ''' <returns>OrderItemCollection</returns>
  Public Shared Function GetCartItems() As OrderItemCollection
	'get the cart items for the current user's cart
	Dim orderID As Integer = GetCartOrderID()
	Dim list As OrderItemCollection = New OrderItemCollection()

	If orderID <> 0 Then
	  list = list.Where("orderID", orderID).Load()

	End If
	Return list
  End Function

  ''' <summary>
  ''' Adds an item to the cart
  ''' </summary>
  ''' <param name="product">Commerce.Common.Product</param>
  Public Shared Sub AddItem(ByVal product As Product)

	'handle the exceptions in the page itself
	'let bubble up

	'get the current orderID
	Dim orderID As Integer = ProvisionOrder(Utility.GetUserName())

	Dim selectedAttrbutes As String = String.Empty
	If Not product.SelectedAttributes Is Nothing Then
	  selectedAttrbutes = product.SelectedAttributes.ToString().Trim()
	End If

	'make sure that the discount, if any, is applied
	product.OurPrice -= product.DiscountAmount

	'add the order using the CSK_Store_AddItemToCart SP
	SPs.StoreAddItemToCart(Utility.GetUserName(), product.ProductID, selectedAttrbutes, product.OurPrice, product.PromoCode, product.Quantity).Execute()


	'get the current quantity
	Dim oldQuantity As Integer = GetExistingQuantity(orderID, product)


  End Sub

  ''' <summary>
  ''' Adjusts the quantity of an item in the cart
  ''' </summary>
  ''' <param name="orderID"></param>
  ''' <param name="productID"></param>
  ''' <param name="selectedAttributes"></param>
  ''' <param name="newQuantity"></param>
  Public Shared Sub AdjustQuantity(ByVal orderID As Integer, ByVal productID As Integer, ByVal selectedAttributes As String, ByVal newQuantity As Integer)
	'check to see if this product is already added, if not, add it. If it's there, adjust the quantity
	Dim q As Query = New Query(OrderItem.GetTableSchema())
	q.AddWhere("orderID", orderID)
	q.AddWhere("productID", productID)
	q.AddWhere("attributes", selectedAttributes)

	q.AddUpdateSetting("quantity", newQuantity)
	q.AddUpdateSetting("modifiedOn", DateTime.UtcNow.ToString())
	q.AddUpdateSetting("modifiedBy", Utility.GetUserName())

	'check the rows updated, if it's more than one the item has already been set and we're done
	q.Execute()
  End Sub

  ''' <summary>
  ''' Removes an item from the cart with specified attributes
  ''' </summary>
  ''' <param name="productID"></param>
  ''' <param name="selectedAttributes"></param>
  Public Shared Sub RemoveItem(ByVal productID As Integer, ByVal selectedAttributes As String)


	Dim isPartOfBundle As Boolean = False
	Dim bundleName As String = ""
	Dim bundledProducts As Hashtable = New Hashtable()

	'check to see if the product to be removed
	'is part of a bundle
	Dim items As OrderItemCollection = GetCartItems()

	For Each item As OrderItem In items
	  If item.ProductID.Equals(productID) AndAlso item.Attributes.Equals(selectedAttributes) Then
		If item.PromoCode.StartsWith("BUNDLE:") Then
		  bundleName = item.PromoCode
		  isPartOfBundle = True
		  Exit For

		End If
	  End If
	Next item

	'remove the item from the order items table
	Dim q As Query = New Query(OrderItem.GetTableSchema())
	Dim orderID As Integer = items(0).OrderID
	q.QueryType = QueryType.Delete
	q.AddWhere("orderID", orderID)
	q.AddWhere("productID", productID)

	If selectedAttributes <> String.Empty Then
	  q.AddWhere("attributes", selectedAttributes)
	End If

	q.Execute()

	'remove it from the collection
'INSTANT VB NOTE: The local variable removeItem was renamed since Visual Basic will not allow local variables with the same name as their method:
	Dim removeItem_Renamed As OrderItem = items.Find(productID)
	If Not removeItem_Renamed Is Nothing Then
	  items.Remove(removeItem_Renamed)
	End If

	'if this was a bundle, loop the remaining items
	'and remove the BUNDLE tag, as well as the discount
	If isPartOfBundle Then
	  'need to loop through the items
	  'find those that are part of this bundle
	  For Each item As OrderItem In items
		If item.PromoCode.Equals(bundleName) Then
		  item.PromoCode = ""

		  'pull the product
		  Dim thisProduct As Product = ProductController.GetProduct(item.ProductID)

		  'make sure to apply any other discounts back to the product
		  Commerce.Promotions.PromotionService.SetProductPricing(thisProduct)

		  'reset the pricing
		  item.OriginalPrice = thisProduct.RetailPrice
		  item.PricePaid = thisProduct.OurPrice

		  'save it down to the order
		  item.Save(Utility.GetUserName())
		End If

	  Next item
	End If



	'if the current order items are 0, make sure to reset the coupons
	'use the EnsureOrderCoupon SP to do this
	SPs.PromoEnsureOrderCoupon(orderID).Execute()

  End Sub

  ''' <summary>
  ''' Removes an item from the cart
  ''' </summary>
  ''' <param name="productID"></param>
  Public Shared Sub RemoveItem(ByVal productID As Integer)
	RemoveItem(productID, "")
  End Sub

  ''' <summary>
  ''' Returns the current quantity of an item
  ''' </summary>
  ''' <param name="orderID"></param>
  ''' <param name="product"></param>
  ''' <returns></returns>
  Private Shared Function GetExistingQuantity(ByVal orderID As Integer, ByVal product As Product) As Integer
	Dim iOut As Integer = 0
	Dim q As Query = New Query(OrderItem.GetTableSchema())
	q.AddWhere("orderID", orderID)
	q.AddWhere("productID", product.ProductID)
	If Not product.SelectedAttributes Is Nothing Then
	  q.AddWhere("attributes", product.SelectedAttributes.ToString())
	End If
	q.SelectList = "quantity"

	Dim result As Object = q.ExecuteScalar()
	If Not result Is Nothing Then
	  Integer.TryParse(result.ToString(), iOut)
	End If
	Return iOut

  End Function

  ''' <summary>
  ''' Gets the order ID of a cart for the specified user
  ''' </summary>
  ''' <param name="userName"></param>
  ''' <returns></returns>
  Public Shared Function GetCartOrderID(ByVal userName As String) As Integer
	Dim iOut As Integer = 0
	'get the current order for the user. if there isn't one, make one
	Dim q As Query = New Query(Order.GetTableSchema())
	q.AddWhere("userName", userName)
	q.AddWhere("orderStatusID", CInt(Fix(OrderStatus.NotProcessed)))
	q.SelectList = "orderID"

	Dim oResult As Object = q.ExecuteScalar()
	If Not oResult Is Nothing Then
	  iOut = CInt(Fix(oResult))
	End If
	Return iOut

  End Function

  ''' <summary>
  ''' Gets the order ID of a cart for the current user
  ''' </summary>
  ''' <param name="userName"></param>
  ''' <returns></returns>
  Public Shared Function GetCartOrderID() As Integer
	Return GetCartOrderID(Utility.GetUserName())

  End Function

  ''' <summary>
  ''' Get's the GUID of the current user's order
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GetCartOrderGUID() As String
	Dim sOut As String = ""
	'get the current order for the user. if there isn't one, make one
	Dim q As Query = New Query(Order.GetTableSchema())

	q.AddWhere("userName", Utility.GetUserName())
	q.AddWhere("orderStatusID", OrderStatus.NotProcessed)
	q.SelectList = "orderGUIID"

	Dim oResult As Object = q.ExecuteScalar()
	If Not oResult Is Nothing Then
	  sOut = oResult.ToString()
	End If
	Return sOut

  End Function

  ''' <summary>
  ''' Creates an order for a user when first-adding an item to the basket
  ''' </summary>
  ''' <param name="userName"></param>
  ''' <returns></returns>
  Private Shared Function ProvisionOrder(ByVal userName As String) As Integer
	Dim orderID As Integer = GetCartOrderID()

	If orderID = 0 Then
	  'create an order
	  Dim companyOrderIdentifier As String = ConfigurationManager.AppSettings("companyOrderIdentifier")
	  Dim newOrder As Order = New Order()
	  newOrder.OrderGUID = System.Guid.NewGuid().ToString()
	  newOrder.OrderStatus = OrderStatus.NotProcessed
	  newOrder.SubTotalAmount = 0
	  newOrder.UserIP = HttpContext.Current.Request.UserHostAddress
	  newOrder.UserName = userName
	  newOrder.OrderNumber = companyOrderIdentifier & "-XX-" & Utility.GetRandomString()
	  newOrder.Save(userName)
	  orderID = newOrder.OrderID
	End If

	Return orderID
  End Function

  ''' <summary>
  ''' Saves an order item to the DB
  ''' </summary>
  ''' <param name="product"></param>
  ''' <param name="orderID"></param>
  ''' <param name="userName"></param>
  Private Shared Sub SaveOrderItem(ByVal product As Product, ByVal orderID As Integer, ByVal userName As String)
	Dim adjustedPrice As Decimal = product.OurPrice

	If product.DiscountAmount <> 0 Then
	  adjustedPrice = product.OurPrice - product.DiscountAmount
	End If
	Dim item As OrderItem = New OrderItem()
	item.OrderID = orderID
	item.ProductID = product.ProductID
	item.Sku = product.Sku
	item.PromoCode = product.PromoCode
	item.PricePaid = adjustedPrice
	item.OriginalPrice = product.RetailPrice
	item.ProductName = product.ProductName
	item.ProductDescription = product.ShortDescription
	item.ImageFile = product.ImageFile
	item.ShippingEstimate = product.ShippingEstimate

	'save them as a list - no need for the XML structure here.
	'especially since the structure of the Attributes might change
	'making this hard to inflate properly
	If Not product.SelectedAttributes Is Nothing Then
	  item.Attributes = product.SelectedAttributes.ToString()
	End If

	item.Weight = product.Weight
	item.Quantity = product.Quantity
	item.Width = product.Width
	item.Height = product.Height
	item.Length = product.Length
	item.DateAdded = DateTime.UtcNow
	item.ImageFile = product.ImageFile
	item.Rating = product.Rating
	item.Save(userName)
  End Sub

  #End Region

End Class
