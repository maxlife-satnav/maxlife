'6--
'147--said by Phani
'163--said by Phani
'

Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Web.UI.WebControls
Imports System
Imports System.Web

Public Class clsEmpMapping

    Public Sub BindData(ByVal uid As String, ByVal grdReqList As GridView, ByVal strTowerName As String, ByVal strLoc As String)
        Dim spLocation As New SqlParameter("@VC_LOC_ID", SqlDbType.NVarChar, 50)
        Dim spTower As New SqlParameter("@VC_TOWER_ID", SqlDbType.NVarChar, 50)
        spLocation.Value = strLoc
        spTower.Value = strTowerName

        grdReqList.DataSource = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_SPACES_TO_MAP_EMPLOYEE", spLocation, spTower)
        grdReqList.DataBind()
    End Sub
    Public Sub bindDetails(ByVal gvEmp As GridView, ByVal ddlProj As String)

        Dim sp1 As New SqlParameter("@vc_proj_id", SqlDbType.VarChar, 50)
        sp1.Value = ddlProj
        Dim dtUsers As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_USERS_TO_MAP", sp1)

        For i As Integer = 0 To gvEmp.Rows.Count - 1
            Dim spReqId As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
            spReqId.Value = CType(gvEmp.Rows(i).FindControl("lblReqID"), Label).Text
            ObjDR = SqlHelper.ExecuteReader(Data.CommandType.StoredProcedure, "USP_GET_FLRID_WINGID", spReqId)
            If ObjDR.Read() Then
                CType(gvEmp.Rows(i).FindControl("lblFloor"), Label).Text = ObjDR(0).ToString()
                CType(gvEmp.Rows(i).FindControl("lblWing"), Label).Text = ObjDR(1).ToString()
            End If
            Dim ddl As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
            ddl.DataSource = dtUsers
            ddl.DataTextField = "NAME"
            ddl.DataValueField = "AUR_ID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select--")
        Next
    End Sub
    Public Sub updateSpace(ByVal strReqID As String, ByVal intEmpId As String, ByVal strSpaceID As String, ByVal intStatusID As Integer)
        Dim sp1 As New SqlParameter("@strReqID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = strReqID
        Dim sp2 As New SqlParameter("@intEmpID", SqlDbType.NVarChar, 50)
        sp2.Value = intEmpId
        Dim sp3 As New SqlParameter("@strSpaceID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = strSpaceID
        Dim sp4 As New SqlParameter("@intStatusID", SqlDbType.Int)
        sp4.Value = intStatusID
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UPDATE_SPACE1", sp1, sp2, sp3, sp4)
    End Sub
    '*********space allocation*****
    Public Sub BindData(ByVal ddl As DropDownList)
        'strSQL = "SELECT SRN_REQ_ID,SRN_SNO FROM  " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACEREQUISITION WHERE SRN_STA_ID IN (147, 163) order by SRN_SNO DESC"
        strSQL = "usp_bindSpaceData"
        BindCombo(strSQL, ddl, "SRN_REQ_ID1", "SRN_REQ_ID")
    End Sub
    Public Function getReqDetails(ByVal strReqID As String, ByVal uid As String) As SqlDataReader
        'strSQL = "SELECT (CASE WHEN SRN_STA_ID ='163' THEN 'BFM' ELSE 'RM' END) as 'ApprovedBy' FROM " & HttpContext.Current.Session("TENANT") & "."  & "sms_spacerequisition where srn_req_id='" & strReqID & "'"
        Dim sp1 As New SqlParameter("@vc_Reqid", SqlDbType.NVarChar, 50)
        sp1.Value = strReqID
        Dim strApprovedBy As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "usp_getApproveduserforRequest", sp1).ToString()

        '        strSQL = "select distinct srn_req_id,srn_branch_id, (select twr_name from tower where twr_code=srn_bdg_one) as 'srn_bdg_one', " & _
        '"(select srd_qty from SMS_SPACEREQUISITION_DETAILS where srd_srnreq_id=ssr.SRN_REQ_ID and srd_type='Cubicals') as 'Cubicals', " & _
        '"(select srd_qty from SMS_SPACEREQUISITION_DETAILS where srd_srnreq_id=ssr.SRN_REQ_ID and srd_type='cabins') as 'cabins', " & _
        '"(select srd_qty from SMS_SPACEREQUISITION_DETAILS where srd_srnreq_id=ssr.SRN_REQ_ID and srd_type='Work stations') as 'Work_stations', " & _
        '"convert(nvarchar,srn_frm_dt,101) as 'srn_frm_dt', convert(nvarchar,srn_to_dt,101) as 'srn_to_dt',  " & _
        '"(select usr_name from [user] where usr_id='" & uid & "') as 'usr_name', (select dep_name from department where dep_code=srn_dep_id) as 'dep_name',  " & _
        '"srn_rem, (select usr_name from [user] where usr_id = (select aur_reporting_to from amantra_user where aur_no='" & uid & "')) as 'RptMgr_Name',  " & _
        '"srn_sta_id,isnull(srn_rm_rem,'NA') as 'srn_rm_rem',SSAD.SMS_WST_ALLOC as 'Work_Stations_A', SSAD.SMS_CAB_ALLOC as 'Cabins_A', SSAD.SMS_CUB_ALLOC as 'Cubicals_A' " & _
        '"from sms_spacerequisition ssr INNER JOIN SMS_SPACE_APPROVAL_DTLS SSAD on  " & _
        '"ssr.SRN_REQ_ID = ssad.SMS_ALLOC_REQ , SMS_SPACEREQUISITION_DETAILS SSRD where srn_req_id='1/SPCREQ/0000011'  " & _
        '"and ssad.SMS_APPROVED_BY='" & strApprovedBy & "' "

        '        strSQL = "select distinct srn_req_id,srn_branch_id, (select twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code=srn_bdg_one) as 'srn_bdg_one',  " & _
        '" (select srd_qty from " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACEREQUISITION_DETAILS where srd_srnreq_id=ssr.SRN_REQ_ID and srd_type='Cubicals') as 'Cubicals',  " & _
        '" (select srd_qty from " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACEREQUISITION_DETAILS where srd_srnreq_id=ssr.SRN_REQ_ID and srd_type='cabins') as 'cabins',  " & _
        '" (select srd_qty from " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACEREQUISITION_DETAILS where srd_srnreq_id=ssr.SRN_REQ_ID and srd_type='Work stations') as 'Work_stations',  " & _
        '" convert(nvarchar,srn_frm_dt,101) as 'srn_frm_dt', convert(nvarchar,srn_to_dt,101) as 'srn_to_dt',   " & _
        '" (select usr_name from " & HttpContext.Current.Session("TENANT") & "."  & "[user] where usr_id=ssr.SRN_AUR_ID) as 'usr_name', (select dep_name from " & HttpContext.Current.Session("TENANT") & "."  & "department where dep_code=srn_dep_id) as 'dep_name',   " & _
        '" srn_rem, (select usr_name from " & HttpContext.Current.Session("TENANT") & "."  & "[user] where usr_id = (select aur_reporting_to from " & HttpContext.Current.Session("TENANT") & "."  & "amantra_user where aur_no='" & uid & "')) as 'RptMgr_Name',   " & _
        '" srn_sta_id,isnull(srn_rm_rem,'NA') as 'srn_rm_rem'," & _
        '" (SSAD.SMS_WST_ALLOC-(SELECT ISNULL(SUM(SMS_WST_ALLOC), 0) FROM " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION_FOR_PROJECT_DTLS WHERE SMS_ALLOC_REQ = '" & strReqID & "')) as 'Work_Stations_A', " & _
        '" (SSAD.SMS_CAB_ALLOC-(SELECT ISNULL(SUM(SMS_CAB_ALLOC), 0) FROM " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION_FOR_PROJECT_DTLS WHERE SMS_ALLOC_REQ = '" & strReqID & "')) as 'Cabins_A', " & _
        '" (SSAD.SMS_CUB_ALLOC-(SELECT ISNULL(SUM(SMS_CUB_ALLOC), 0) FROM " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION_FOR_PROJECT_DTLS WHERE SMS_ALLOC_REQ = '" & strReqID & "')) as 'Cubicals_A' " & _
        '" from " & HttpContext.Current.Session("TENANT") & "."  & "sms_spacerequisition ssr INNER JOIN " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACE_APPROVAL_DTLS SSAD on   " & _
        '" ssr.SRN_REQ_ID = ssad.SMS_ALLOC_REQ , SMS_SPACEREQUISITION_DETAILS SSRD where srn_req_id='" & strReqID & "'   " & _
        '" and ssad.SMS_APPROVED_BY='" & strApprovedBy & "' order by srn_req_id"
        Dim sp2 As New SqlParameter("@vc_reqId", SqlDbType.NVarChar, 50)
        sp2.Value = strReqID
        Dim sp3 As New SqlParameter("@vc_user", SqlDbType.NVarChar, 50)
        sp3.Value = uid
        Dim sp4 As New SqlParameter("@vc_Apporved_by", SqlDbType.NVarChar, 50)
        sp4.Value = strApprovedBy
        ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getRequestDetails", sp2, sp3, sp4)
        Return ObjDR
    End Function

    Public Sub getReqSpaces(ByVal strVertical As String, ByVal strType As String, ByVal lst As ListBox, ByVal frmdt As String, ByVal todt As String)
        Dim sp1 As New SqlParameter("@vc_spc_type", SqlDbType.NVarChar, 50)
        Dim sp2 As New SqlParameter("@vc_vertical", SqlDbType.NVarChar, 50)
        Dim sp3 As New SqlParameter("@vc_fromdate", SqlDbType.DateTime)
        Dim sp4 As New SqlParameter("@vc_todate", SqlDbType.DateTime)
        sp1.Value = strType
        sp2.Value = strVertical
        sp3.Value = frmdt
        sp4.Value = todt

        strSQL = "usp_getRequestSpaces"
        BindList(strSQL, lst, "SSA_SPC_ID", "SSA_SPC_ID", sp1, sp2, sp3, sp4)
        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    End Sub
    Public Sub getReqSpaces(ByVal strVertical As String, ByVal strType As String, ByVal gv As GridView, ByVal frmdt As String, ByVal todt As String, ByVal strreq As String)
        Dim paramstrType As SqlParameter = New SqlParameter("@vc_spc_type", SqlDbType.NVarChar, 100)
        Dim paramstrVertical As SqlParameter = New SqlParameter("@vc_vertical", SqlDbType.NVarChar, 100)
        Dim paramfrmdt As SqlParameter = New SqlParameter("@vc_fromdate", SqlDbType.NVarChar, 100)
        Dim paramtodt As SqlParameter = New SqlParameter("@vc_todate", SqlDbType.NVarChar, 100)
        Dim paramstrreq As SqlParameter = New SqlParameter("@strreq", SqlDbType.NVarChar, 100)

        paramstrType.Value = strType
        paramstrVertical.Value = strVertical
        paramfrmdt.Value = frmdt
        paramtodt.Value = todt
        paramstrreq.Value = strreq

        'strSQL = "usp_getRequestSpaces '" & strType & "','" & strVertical & "','" & frmdt & "','" & todt & "','" & strreq & "'"
        strSQL = "usp_getRequestSpaces"
        'strSQL = "SELECT SSA_SPC_ID, SSA_SRNREQ_ID FROM " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE SSA_SPC_TYPE = '" & strType & "' AND SSA_VERTICAL = '" & strVertical & "' and SSA_SPC_ID not in (SELECT DISTINCT SSA_SPC_ID FROM SMS_SPACE_ALLOCATION_FOR_PROJECT where  SSA_STA_ID <> 4)"
        BindGrid(strSQL, gv, paramstrType, paramstrVertical, paramfrmdt, paramtodt, paramstrreq)
        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    End Sub
    Public Function insertSpacesForProject(ByVal strReqID As String, ByVal strSpaceID As String, ByVal uid As String, ByVal dtAllocDate As Date, ByVal dtFromDate As Date, ByVal strVertical As String, ByVal strType As String, ByVal dtToDate As Date, ByVal strVerticalReqId As String, ByVal strBdgCode As String, ByVal strTwrCode As String, ByVal strFlrCode As String, ByVal strWngCode As String, ByVal strRequisitionId As String, lblreqid1 As String) As Int16
        'strSQL = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION_FOR_PROJECT (SSA_SRNREQ_ID, SSA_SPC_ID, SSA_AUR_ID, SSA_ALC_DT, SSA_UPT_DT, SSA_STA_ID,  SSA_FROM_DATE,  SSA_VERTICAL,  SSA_SPC_TYPE, SSA_TO_DT,SSA_VER_REQ_ID, SSA_EMP_MAP,SSA_BDG_ID , SSA_TWR_ID , SSA_FLR_ID , SSA_WNG_ID  ) " & _
        '         "VALUES('" & strReqID & "','" & strSpaceID & "','" & uid & "','" & dtAllocDate & "','" & getoffsetdatetime(DateTime.Now) & "',6,'" & dtFromDate & "','" & strVertical & "','" & strType & "','" & dtToDate & "','" & strVerticalReqId & "','-1','" & strBdgCode & "','" & strTwrCode & "','" & strFlrCode & "','" & strWngCode & "')"

        Dim spSSA_SRNREQ_ID As SqlParameter = New SqlParameter("@SSA_SRNREQ_ID", SqlDbType.NVarChar, 100)
        Dim spSSA_SPC_ID As SqlParameter = New SqlParameter("@SSA_SPC_ID", SqlDbType.NVarChar, 100)
        Dim spSSA_AUR_ID As SqlParameter = New SqlParameter("@SSA_AUR_ID", SqlDbType.NVarChar, 50)
        Dim spSSA_ALC_DT As SqlParameter = New SqlParameter("@SSA_ALC_DT", SqlDbType.DateTime)
        Dim spSSA_UPT_DT As SqlParameter = New SqlParameter("@SSA_UPT_DT", SqlDbType.DateTime)
        Dim spSSA_STA_ID As SqlParameter = New SqlParameter("@SSA_STA_ID", SqlDbType.NVarChar, 10)
        Dim spSSA_FROM_DATE As SqlParameter = New SqlParameter("@SSA_FROM_DATE", SqlDbType.DateTime)
        Dim spSSA_VERTICAL As SqlParameter = New SqlParameter("@SSA_VERTICAL", SqlDbType.NVarChar, 50)
        Dim spSSA_SPC_TYPE As SqlParameter = New SqlParameter("@SSA_SPC_TYPE", SqlDbType.NVarChar, 50)
        Dim spSSA_TO_DT As SqlParameter = New SqlParameter("@SSA_TO_DT", SqlDbType.DateTime)
        Dim spSSA_VER_REQ_ID As SqlParameter = New SqlParameter("@SSA_VER_REQ_ID", SqlDbType.NVarChar, 100)
        Dim spSSA_EMP_MAP As SqlParameter = New SqlParameter("@SSA_EMP_MAP", SqlDbType.NVarChar, 50)
        Dim spSSA_BDG_ID As SqlParameter = New SqlParameter("@SSA_BDG_ID", SqlDbType.NVarChar, 50)
        Dim spSSA_TWR_ID As SqlParameter = New SqlParameter("@SSA_TWR_ID", SqlDbType.NVarChar, 50)
        Dim spSSA_FLR_ID As SqlParameter = New SqlParameter("@SSA_FLR_ID", SqlDbType.NVarChar, 50)
        Dim spSSA_WNG_ID As SqlParameter = New SqlParameter("@SSA_WNG_ID", SqlDbType.NVarChar, 50)
        Dim spREQ_ID As SqlParameter = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 50)
        Dim splblreqid As SqlParameter = New SqlParameter("@SRNREQID", SqlDbType.NVarChar, 50)
        spSSA_SRNREQ_ID.Value = strReqID
        spSSA_SPC_ID.Value = strSpaceID
        spSSA_AUR_ID.Value = uid
        spSSA_ALC_DT.Value = dtAllocDate
        spSSA_UPT_DT.Value = getoffsetdatetime(DateTime.Now)
        spSSA_STA_ID.Value = 6
        spSSA_FROM_DATE.Value = dtFromDate
        spSSA_VERTICAL.Value = strVertical
        spSSA_SPC_TYPE.Value = strType
        spSSA_TO_DT.Value = dtToDate
        spSSA_VER_REQ_ID.Value = strVerticalReqId
        spSSA_EMP_MAP.Value = "-1"
        spSSA_BDG_ID.Value = strBdgCode
        spSSA_TWR_ID.Value = strTwrCode
        spSSA_FLR_ID.Value = strFlrCode
        spSSA_WNG_ID.Value = strWngCode
        spREQ_ID.Value = strRequisitionId
        splblreqid.Value = lblreqid1
        Dim i As Integer = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_ALLOC_PROJECT", spSSA_SRNREQ_ID, spSSA_SPC_ID, spSSA_AUR_ID, spSSA_ALC_DT, spSSA_UPT_DT, spSSA_STA_ID, spSSA_FROM_DATE, spSSA_VERTICAL, spSSA_SPC_TYPE, spSSA_TO_DT, spSSA_VER_REQ_ID, spSSA_EMP_MAP, spSSA_BDG_ID, spSSA_TWR_ID, spSSA_FLR_ID, spSSA_WNG_ID, spREQ_ID, splblreqid)

        If i = 1 Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Public Function insertSpacesForProjectDetails(ByVal strReqID As String, ByVal intWST_NUM As Integer, ByVal intCAB_NUM As Integer, ByVal intCUB_NUM As Integer, ByVal intWST_ALLOC As Integer, ByVal intCAB_ALLOC As Integer, ByVal intCUB_ALLOC As Integer, ByVal strRemarks As String, ByVal uid As String) As Int16
        strSQL = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACE_ALLOCATION_FOR_PROJECT_DTLS (SMS_ALLOC_REQ, SMS_WST_NUM, SMS_CAB_NUM, SMS_CUB_NUM, SMS_WST_ALLOC, SMS_CAB_ALLOC, SMS_CUB_ALLOC,  SMS_UPDATED_ON, SMS_REM, SMS_UPDATED_BY)" & _
        "VALUES('" & strReqID & "','" & intWST_NUM & "','" & intCAB_NUM & "','" & intCUB_NUM & "','" & intWST_ALLOC & "','" & intCAB_ALLOC & "','" & intCUB_ALLOC & "','" & getoffsetdatetime(DateTime.Now) & "','" & strRemarks & "','" & uid & "')"
        Dim i As Integer = SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        If i = 1 Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Public Function UPDATESpacesForProjectDetails(ByVal strReqID As String, ByVal intWST_ALLOC As Integer, ByVal intCAB_ALLOC As Integer, ByVal intCUB_ALLOC As Integer, ByVal strRemarks As String, ByVal uid As String) As Int16
        strSQL = "UPDATE " & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACE_ALLOCATION_FOR_PROJECT_DTLS set  SMS_WST_ALLOC=SMS_WST_ALLOC+" & intWST_ALLOC & ", SMS_CAB_ALLOC=SMS_CAB_ALLOC+" & intCAB_ALLOC & ", SMS_CUB_ALLOC=SMS_CUB_ALLOC+" & intCUB_ALLOC & ",  SMS_UPDATED_ON=" & getoffsetdatetime(DateTime.Now) & ", SMS_REM='" & strRemarks & "', SMS_UPDATED_BY='" & uid & "' where SMS_ALLOC_REQ='" & strReqID & "'"
        Dim i As Integer = SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        If i = 1 Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Public Function updateSpaceReq(ByVal strreqID As String, ByVal intStatuID As Int16) As Integer
        strSQL = "UPDATE " & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACEREQUISITION SET SRN_STA_ID ='" & intStatuID & "' where SRN_REQ_ID ='" & strreqID & "'"
        Dim i As Integer = SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        If i = 1 Then
            Return 1
        Else
            Return 0
        End If
    End Function
End Class