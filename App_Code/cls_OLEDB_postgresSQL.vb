Imports System.Data.OleDb
Imports System.Data
Imports System.Data.SqlClient
Imports Npgsql
Imports System.Configuration



Public Class cls_OLEDB_postgresSQL
    ''' Function to retrieve the connection from the app.config
    ''' </summary>
    ''' <param name="conName">Name of the connectionString to retrieve</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetConnectionString(ByVal conName As String) As String
        'variable to hold our connection string for returning it
        Dim strReturn As New String("")
        'check to see if the user provided a connection string name
        'this is for if your application has more than one connection string
        If Not String.IsNullOrEmpty(conName) Then
            'a connection string name was provided
            'get the connection string by the name provided
            strReturn = ConfigurationManager.ConnectionStrings(conName).ConnectionString
        Else
            'no connection string name was provided
            'get the default connection string
            strReturn = ConfigurationManager.ConnectionStrings("OLEDBConnectionString").ConnectionString
        End If
        'return the connection string to the calling method
        Return strReturn
    End Function


    Public Function PostGresGetDataSet(ByVal SP_Name As String) As DataSet
        'Create the objects we need to insert a new record
        Dim con As New NpgsqlConnection(GetConnectionString("OLEDBConnectionString"))
        Dim cmdUpdate As New NpgsqlCommand
        con.Open()
        '****************** Dataset with Stored Procedure in Postgresql **********************************
        'Start a transaction as it is required to work with result sets (cursors) in PostgreSQL
        Dim tran As NpgsqlTransaction = con.BeginTransaction()
        ' Define a command to call show_cities() procedure
        Dim command As NpgsqlCommand = New NpgsqlCommand(SP_Name, con)
        command.CommandType = CommandType.StoredProcedure

        Dim nda As New NpgsqlDataAdapter
        nda.SelectCommand = command
        Dim ds As New DataSet
        nda.Fill(ds)
        tran.Commit()
        con.Close()
        '*********************************************************************************
        Return ds
    End Function


    'Public Shared Function InsertNewRecord(ByVal item1 As String, ByVal item2 As String, ByVal item3 As String) As Boolean
    '    'Create the objects we need to insert a new record
    '    Dim cnInsert As New OleDbConnection(GetConnectionString("OLEDBConnectionString"))
    '    Dim cmdInsert As New OleDbCommand
    '    Dim query As String = "INSERT INTO YourTable(column1,column2,column3) VALUES(@item1,@item2,@item3)"
    '    Dim iSqlStatus As Integer

    '    'Clear any parameters
    '    cmdInsert.Parameters.Clear()
    '    Try
    '        'Set the OleDbCommand Object Properties
    '        With cmdInsert
    '            'Tell it what to execute
    '            .CommandText = query
    '            'Tell it its a text query
    '            .CommandType = CommandType.Text
    '            'Now add the parameters to our query
    '            'NOTE: Replace @value1.... with your parameter names in your query
    '            'and add all your parameters in this fashion
    '            .Parameters.AddWithValue("@value1", item1)
    '            .Parameters.AddWithValue("@value2", item2)
    '            .Parameters.AddWithValue("@value3", item3)
    '            'Set the connection of the object
    '            .Connection = cnInsert
    '        End With

    '        'Now take care of the connection
    '        HandleConnection(cnInsert)

    '        'Set the iSqlStatus to the ExecuteNonQuery 
    '        'status of the insert (0 = failed, 1 = success)
    '        iSqlStatus = cmdInsert.ExecuteNonQuery

    '        'Now check the status
    '        If Not iSqlStatus = 0 Then
    '            'DO your failed messaging here
    '            Return False
    '        Else
    '            'Do your success work here
    '            Return True
    '        End If
    '    Catch ex As Exception
    '        MsgBox(ex.Message, "Error")
    '    Finally
    '        'Now close the connection
    '        HandleConnection(cnInsert)
    '    End Try
    'End Function
    Public Function PostGresGetDataSet(ByVal SP_Name As String, ByVal param As NpgsqlParameter()) As DataSet
        'Create the objects we need to insert a new record
        Dim con As New NpgsqlConnection(GetConnectionString("OLEDBConnectionString"))
        Dim cmdUpdate As New NpgsqlCommand
        con.Open()
        '****************** Dataset with Stored Procedure in Postgresql **********************************
        'Start a transaction as it is required to work with result sets (cursors) in PostgreSQL
        Dim tran As NpgsqlTransaction = con.BeginTransaction()
        ' Define a command to call show_cities() procedure
        Dim command As NpgsqlCommand = New NpgsqlCommand(SP_Name, con)
        command.CommandType = CommandType.StoredProcedure
        For i As Integer = 0 To param.Length - 1
            command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Dim nda As New NpgsqlDataAdapter
        nda.SelectCommand = command
        Dim ds As New DataSet
        nda.Fill(ds)
        tran.Commit()
        con.Close()
        '*********************************************************************************
        Return ds
    End Function
    Public Shared Function PostGresExecute(ByVal SP_Name As String, ByVal param As NpgsqlParameter())
        'Create the objects we need to insert a new record
        Dim con As New NpgsqlConnection(GetConnectionString("OLEDBConnectionString"))
        Dim cmdUpdate As New NpgsqlCommand
        con.Open()
        '****************** Dataset with Stored Procedure in Postgresql **********************************
        'Start a transaction as it is required to work with result sets (cursors) in PostgreSQL
        Dim tran As NpgsqlTransaction = con.BeginTransaction()
        ' Define a command to call show_cities() procedure
        Dim command As NpgsqlCommand = New NpgsqlCommand(SP_Name, con)
        command.CommandType = CommandType.StoredProcedure
        For i As Integer = 0 To param.Length - 1
            command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next


        command.ExecuteNonQuery()
        'Dim nda As New NpgsqlDataAdapter
        'nda.SelectCommand = command
        'Dim ds As New DataSet
        'nda.Fill(ds)
        tran.Commit()
        con.Close()
        '*********************************************************************************
        Return 0
    End Function


    Public Shared Function UpdateSpacestatus(ByVal spc_id As String, ByVal status As String, ByVal vertical As String)
        Dim param(3) As Npgsql.NpgsqlParameter
        param(0) = New Npgsql.NpgsqlParameter("@SPC_ID", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(0).Value = spc_id
        param(1) = New Npgsql.NpgsqlParameter("@STATUS", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(1).Value = status
        param(2) = New Npgsql.NpgsqlParameter("@VERTICAL", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(2).Value = vertical
        param(3) = New Npgsql.NpgsqlParameter("@tenantid", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(3).Value = System.Web.HttpContext.Current.Session("Tenant")
        PostGresExecute("update_space_status", param)
    End Function
    Public Shared Function UpdateSpaceType(ByVal spc_id As String, ByVal status As String)
        Dim param(2) As Npgsql.NpgsqlParameter
        param(0) = New Npgsql.NpgsqlParameter("@SPC_ID", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(0).Value = spc_id
        param(1) = New Npgsql.NpgsqlParameter("@STATUS", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(1).Value = status
        param(2) = New Npgsql.NpgsqlParameter("@tenantid", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(2).Value = System.Web.HttpContext.Current.Session("Tenant")
        'param(2).Value = "T9"
        PostGresExecute("update_space_type", param)
    End Function
    Public Shared Function UpdateRecord(ByVal SPC_ID As String, ByVal STATUS As String, ByVal UserNameToolTip As String) As Boolean
        Dim param(3) As Npgsql.NpgsqlParameter
        param(0) = New Npgsql.NpgsqlParameter("@SPC_ID", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(0).Value = SPC_ID
        param(1) = New Npgsql.NpgsqlParameter("@STATUS", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(1).Value = STATUS
        param(2) = New Npgsql.NpgsqlParameter("@displayname", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(2).Value = UserNameToolTip
        param(3) = New Npgsql.NpgsqlParameter("@tenantid", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param(3).Value = System.Web.HttpContext.Current.Session("Tenant")
        PostGresExecute("update_floor_maps", param)
        'Create the objects we need to insert a new record
        '' '' '' ''Dim cnUpdate As New NpgsqlConnection(GetConnectionString("OLEDBConnectionString"))
        '' '' '' ''Dim cmdUpdate As New NpgsqlCommand

        '        ' WHERE FLR_ID='AX0021-NA-GF' AND SPACE_ID='AX0021/NA/GF/NA/FCB-001'
        'Dim query As String = "UPDATE FloorMaps SET status= @status  WHERE oid= @oid"

        'Dim sp As New SubSonic.StoredProcedure("update_floor_maps")
        'sp.Command.AddParameter("@SPC_ID", SPC_ID, DbType.String)
        'sp.Command.AddParameter("@STATUS", STATUS, DbType.Int32)
        'sp.Command.AddParameter("@displayname", UserNameToolTip, DbType.String)
        'sp.ExecuteScalar()


        '''''Dim query As String = "UPDATE FloorMaps SET status= @STATUS,asset_id=@UserNameToolTip,displayname=@UserNameToolTip   WHERE  SPACE_ID=@SPC_ID"

        ' '' ''Dim iSqlStatus As Integer

        'Clear any parameters
        '' '' ''cmdUpdate.Parameters.Clear()
        '' '' ''Try
        'Set the OleDbCommand Object Properties
        '' '' ''With cmdUpdate
        'Tell it what to execute
        '' '' ''.CommandText = query
        'Tell it its a text query
        '' '' ''.CommandType = CommandType.Text
        'Now add the parameters to our query
        'NOTE: Replace @value1.... with your parameter names in your query
        'and add all your parameters in this fashion

        '' '' ''.Parameters.Add("@STATUS", STATUS)
        '' '' ''.Parameters.Add("@SPC_ID", SPC_ID)
        '' '' ''.Parameters.Add("@UserNameToolTip", UserNameToolTip)
        'UserNameToolTip
        'Set the connection of the object
        ' '' '' ''.Connection = cnUpdate
        ' '' '' ''End With

        'Now take care of the connection
        '' '' ''HandleConnection(cnUpdate)

        'Set the iSqlStatus to the ExecuteNonQuery 
        'status of the insert (0 = success, 1 = failed)
        '' '' ''iSqlStatus = cmdUpdate.ExecuteNonQuery

        'Now check the status
        '' '' ''If Not iSqlStatus = 0 Then
        'DO your failed messaging here
        '' '' ''Select Case cnUpdate.State
        '' '' ''    Case ConnectionState.Open
        'the connection is open
        'close then re-open
        '' '' ''cnUpdate.Close()
        ' .Open()
        '  Exit Select
        '' '' ''End Select
        '' '' ''Return False
        '' '' ''Else
        'Do your success work here
        '' '' ''Select Case cnUpdate.State
        '' '' ''    Case ConnectionState.Open
        'the connection is open
        'close then re-open
        '' '' ''cnUpdate.Close()
        ' .Open()
        '  Exit Select
        '' '' ''End Select
        '' '' ''Return True
        '' '' ''End If
        '' '' ''Catch ex As Exception
        '' '' ''    MsgBox(ex.Message, "Error")
        '' '' ''Finally
        'Now close the connection
        '' '' ''Select Case cnUpdate.State
        '' '' ''    Case ConnectionState.Open
        'the connection is open
        'close then re-open
        '' '' ''cnUpdate.Close()
        ' .Open()
        '  Exit Select
        '' '' ''End Select
        'HandleConnection(cnUpdate)
        '' '' ''End Try
    End Function

    Public Shared Sub HandleConnection(ByVal conn As NpgsqlConnection)
        With conn
            'do a switch on the state of the connection
            Select Case .State
                Case ConnectionState.Open
                    'the connection is open
                    'close then re-open
                    .Close()
                    .Open()
                    Exit Select
                Case ConnectionState.Closed
                    'connection is open
                    'open the connection
                    .Open()
                    Exit Select
                    'Case Else
                    '    .Close()
                    '    .Open()
                    '    Exit Select
            End Select
        End With
    End Sub


End Class
