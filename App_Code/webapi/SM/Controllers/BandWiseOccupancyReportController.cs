﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for BandWiseOccupancyReportController
/// </summary>
public class BandWiseOccupancyReportController : ApiController
{
   
    ReportViewData RptData = new ReportViewData();
    BandWiseOccupancyReportService bandService = new BandWiseOccupancyReportService();

    [HttpPost]
    public HttpResponseMessage GetRptGrid([FromBody]BandList BandAlloc)
    {
        var obj = bandService.GetRptGrid(BandAlloc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetSpaceAllocationReport([FromBody]BandList BandAlloc)
    {
        ReportGenerator<BandWiseOccupancy> GenAllocReport = new ReportGenerator<BandWiseOccupancy>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/BandWiseOccupancyReport.rdlc"),
            DataSetName = "BandOccupancyReport",
            ReportType = "Band Wise Occupancy Report"
        };
        bandService = new BandWiseOccupancyReportService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/BandWiseOccupancyReport." + BandAlloc.Type);
        List<BandWiseOccupancy> reportdata = bandService.GetAllocationDetails(BandAlloc);
        await GenAllocReport.GenerateReport(reportdata, filePath, BandAlloc.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "BandWiseOccupancyReport." + BandAlloc.Type;
        return result;
    }
    //Grid Data
    [HttpPost]
    public HttpResponseMessage GetGriddata(BandList BandAlloc)
    {
        var obj = bandService.GetAllocationDetails(BandAlloc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}