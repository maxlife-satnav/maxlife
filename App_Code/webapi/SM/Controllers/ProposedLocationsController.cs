﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;

public class ProposedLocationsController : ApiController
{
    ProposedLocationsService servc = new ProposedLocationsService();
    
    [HttpGet]
    public Object GetStatewiseLoc()
    {
        return servc.GetStatewiseLoc();
    }
    [HttpGet]
    public Object GetMarkers()  
    {
        return servc.GetMarkers();
    }

}
