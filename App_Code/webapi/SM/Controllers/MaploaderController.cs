﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class MaploaderAPIController : ApiController
{
    MaploaderService mspsrvc = new MaploaderService();

    public Object GetFloorLst()
    {
        return mspsrvc.GetFloorLst();
    }

    [HttpPost]
    public Object GetMapItems([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetMapItems(svm);
    }

    [HttpPost]
    public Object GetMarkers([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetMarkers(svm);
    }

    [HttpPost]
    public Object GetLegendsCount([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetLegendsCount(svm);
    }

    [HttpPost]
    public Object GetLegendsSummary([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetLegendsSummary(svm);
    }

    [HttpPost]
    public Object GetAllocDetails([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetAllocDetails(svm);
    }

    [HttpPost]
    public object GetSpaceDetailsBySPCID([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsBySPCID(svm);
    }

    [HttpPost]
    public object GetSpaceDetailsBySUBITEM([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsBySUBITEM(svm);
    }

    [HttpPost]
    public object GetSpaceDetailsByREQID([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsByREQID(svm);
    }

    [HttpPost]
    public object GetEmpDetails([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetEmpDetails(svm);
    }

    [HttpGet]
    public object GetallFilterbyItem()
    {
        return mspsrvc.GetallFilterbyItem();
    }

    [HttpPost]
    public object GetallFilterbySubItem([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetallFilterbySubItem(svm);
    }

    [HttpPost]
    public object GetAllocEmpDetails([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetAllocEmpDetails(svm);
    }

    [HttpPost]
    public object ReleaseSelectedseat([FromBody] SPACE_REL_DETAILS sad)
    {
        return mspsrvc.ReleaseSelectedseat(sad);
    }

    [HttpPost]
    public object AllocateSeats(List<SPACE_ALLOC_DETAILS> allocDetLst)
    {
        return mspsrvc.AllocateSeats(allocDetLst);
    }

    [HttpPost]
    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS data)
    {
        return mspsrvc.SpcAvailabilityByShift(data);
    }

    [HttpPost]
    public object GetTotalAreaDetails(Space_mapVM data)
    {
        return mspsrvc.GetTotalAreaDetails(data);
    }
    [HttpPost]
    public object GetSeatingCapacity(Space_mapVM data)
    {
        return mspsrvc.GetSeatingCapacity(data);
    }
    [HttpPost]
    public object Validate([FromUri] string id)
    {
        return mspsrvc.Validate(id);
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetViewMaps()
    {
        ReportGenerator<DownloadMap> reportgen = new ReportGenerator<DownloadMap>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Space_Mgmt/ViewAllMaps.rdlc"),
            DataSetName = "ViewAllMaps",
            ReportType = ""
        };
        mspsrvc = new MaploaderService();
        List<DownloadMap> reportdata = mspsrvc.DownloadAllMapsLst();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ViewAllMaps.xls");
        await reportgen.GenerateReport(reportdata, filePath, "xls");
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ViewAllMaps." + "xls";
        return result;
    }
    //[HttpPost]
    //public DataTable Post(MaploaderVM mapVm)
    //{
    //    return Ok(mspsrvc.Post(mapVm));
    //}

    //[HttpPut]
    //public IHttpActionResult Put(MaploaderVM mapVm)
    //{
    //    return Ok(mspsrvc.Put(mapVm));
    //}

    //[HttpDelete]
    //public void Delete(MaploaderVM mapVm)
    //{
    //    mspsrvc.Delete(mapVm);
    //}

    //[HttpGet]
    //public IHttpActionResult GetLayerDet()
    //{
    //    return Ok(mspsrvc.GetLayerDet());
    //}

    //[HttpPost]
    //public IHttpActionResult GetMarkerDet(MaploaderVM mapvm)
    //{
    //    return Ok(mspsrvc.GetMarkerDet(mapvm));
    //}

    //[HttpPost]
    //public IHttpActionResult InsertMarkerDet(MaploaderVM mapvm)
    //{
    //    return Ok(mspsrvc.InsertMarkerDet(mapvm));
    //}
}
