﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

public class UploadSpaceAllocationController : ApiController
{
    UploadSpaceAllocationService servc = new UploadSpaceAllocationService();
    [HttpPost]
    public HttpResponseMessage DownloadTemplate([FromBody] UploadSpacesVM spcdet)
    {
        var obj = servc.DownloadTemplate(spcdet);
        if (obj.Tables[0].Rows.Count != 0)
        {
            var stream = CreateExcelFile.CreateExcelDocumentAsStream(obj);
            // processing the stream.

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.GetBuffer())
            };
            result.Content.Headers.ContentDisposition =
                new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "DownloadTemplate.xlsx"
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            return result;
        }
        else
        {
            var retval = new { Message = "No Data Found" };
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retval);
            return response;
        }
    }

    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = servc.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
