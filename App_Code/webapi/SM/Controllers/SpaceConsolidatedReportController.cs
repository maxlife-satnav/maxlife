﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class SpaceConsolidatedReportController : ApiController
{
    SpaceConsolidatedReportService SPC_CON_SER = new SpaceConsolidatedReportService();

    [HttpPost]
    public HttpResponseMessage BindGrid()
    {
        var obj = SPC_CON_SER.BindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SpaceConsolidatedChart()
    {
        var obj = SPC_CON_SER.SpaceConsolidatedChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportVM> reportgen = new ReportGenerator<SpaceConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedReport.rdlc"),
            DataSetName = "SpaceConsolidatedReport",
            ReportType = "Space Consolidated Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedReport." + rptCost.DocType);
        List<SpaceConsolidatedReportVM> reportdata = SPC_CON_SER.GetReportList();
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedReport." + rptCost.DocType;
        return result;

    }

}
