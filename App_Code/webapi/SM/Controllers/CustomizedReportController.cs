﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class CustomizedReportController : ApiController
{
    CustomizedReportService Csvc = new CustomizedReportService();
    CustomizedReportView report = new CustomizedReportView();

    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(CustomizedDetails Custdata)
    {
        var obj = Csvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedData([FromBody]CustomizedDetails data)
    {
        ReportGenerator<CustomizedData> reportgen = new ReportGenerator<CustomizedData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/CustomizedReport.rdlc"),
            DataSetName = "CustomizedReport",
            ReportType = "Customized Report"
        };

        Csvc = new CustomizedReportService();
        List<CustomizedData> reportdata = Csvc.GetCustomizedDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CustomizedReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CustomizedReport." + data.Type;
        return result;
    }
}
