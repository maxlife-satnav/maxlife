﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class OverloadReportController : ApiController
{
    OverloadService service = new OverloadService();
    [HttpPost]
    public HttpResponseMessage GetOverloadReportBindGrid()
    {
        var obj = service.GetOverloadObject();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Chart Data

    [HttpPost]
    public HttpResponseMessage GetOverloadChartData()
    {
        var obj = service.GetOverloadChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
   
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> OverloadReportData([FromBody]OverloadParams rptArea)
    {
        OverloadParams over = new OverloadParams();
       
        ReportGenerator<OverloadReportModel> reportgen = new ReportGenerator<OverloadReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/OverloadReport.rdlc"),
            DataSetName = "OverloadReportSPC",
            ReportType= "Overload Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/OverloadReport." + rptArea.DocType);
        List<OverloadReportModel> reportdata = service.GetOverloadReportList();
        await reportgen.GenerateReport(reportdata, filePath, rptArea.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "OverloadReport." + rptArea.DocType;
        return result;

    }
}
