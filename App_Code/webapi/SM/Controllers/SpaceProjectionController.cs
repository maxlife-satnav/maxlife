﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for SpaceProjectionController
/// </summary>
public class SpaceProjectionController : ApiController
{
    SpaceProjectionService sps = new SpaceProjectionService();
   
    [HttpGet]
    public HttpResponseMessage GetYears()
    {
        IEnumerable<Years> yearlist = sps.GetYears();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, yearlist);
        return response;
    }

    //verticals
    [HttpGet]
    public HttpResponseMessage GetVerticals()
    {
        IEnumerable<VerticalVM> verticallist = sps.GetVerticals();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, verticallist);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveDetails(SpaceProjectionVM dataobject)
    {
        object obj = sps.SaveDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        
    }

    //sprj details by country, vertical code, sprj year, projection type
    [HttpPost]
    public HttpResponseMessage GetSPRJ_Details(SMS_SPACE_PROJECTION ssp)
    {
        object obj = sps.GetSPRJDetails(ssp);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
	
}