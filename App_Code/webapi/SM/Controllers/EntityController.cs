﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;


public class EntityController: ApiController
{
    EntityService service = new EntityService();
    [HttpPost]

    public HttpResponseMessage Insert(EntityModel entycategory)
    {
        if (service.Save(entycategory) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, entycategory);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
    }
    [HttpGet]
    public HttpResponseMessage GetParentEntity()
    {
        IEnumerable<EntityAdminModel> ParentEntityList = service.GetParentEntity();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ParentEntityList);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage UpdateChildEntity(EntityModel update)
    {

        if (service.UpdateChildEntity(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage BindGridEntity()
    {
        IEnumerable<EntityModel> ChildEntityList = service.BindGridEntity();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildEntityList);
        return response;
    }

}