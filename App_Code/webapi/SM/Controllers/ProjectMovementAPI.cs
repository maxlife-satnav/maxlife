﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net;

/// <summary>
/// Summary description for ProjectMovementAPI
/// </summary>
public class ProjectMovementAPIController : ApiController
{
    ProjectMovementService service = new ProjectMovementService();
    
    [HttpPost]
    public HttpResponseMessage Search([FromBody] ProjectMovementModel spcdet)
    {
        var obj = service.Search(spcdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ViewPossibilities([FromBody] ProjectMovementModel spcdet)
    {
        var obj = service.ViewPossibilities(spcdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}