﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// Summary description for ViewSpaceExtensionRequisitionController
/// </summary>
public class ViewSpaceExtReqController : ApiController
{
    ViewSpaceExtReqService spcextreqService = new ViewSpaceExtReqService();
    [HttpGet]
    public HttpResponseMessage GetMyReqList()
    {
        var SpcExtReqList = spcextreqService.GetMyReqList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpcExtReqList);
        return response;
    }

    [HttpGet]
    public object GetPendingReqList()
    {
        var Verlist = spcextreqService.GetPendingReqList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Verlist);
        return response;
    }

    [HttpPost]
    public object GetDetailsOnSelection(SMS_SPACE_EXTENSION_VM selectedid)
    {
        var Verlist = spcextreqService.GetDetailsOnSelection(selectedid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Verlist);
        return response;
    }
}