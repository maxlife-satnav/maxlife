﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;

public class MapMarkerController : ApiController
{
    MapMarkerService servc = new MapMarkerService();
    [HttpGet]
    public Object GetZonewiseLoc()
    {
        return servc.GetZonewiseLoc();
    }
    [HttpGet]
    public Object GetStatewiseLoc()
    {
        return servc.GetStatewiseLoc();
    }
    [HttpGet]
    public Object GetMarkers()  
    {
        return servc.GetMarkers();
    }
    
    [HttpPost]
    public HttpResponseMessage GoogleOrigins([FromBody]Origins Orglst)
    {
        var user = servc.GetLatLongs(Orglst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response; 
    }

    [HttpPost]
    public HttpResponseMessage GetLocations([FromBody]City cty)
    {
        var user = servc.GetLocations(cty);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
}
