﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class SpaceRequisitionReportController : ApiController
{
    SpaceRequisitionReportService service = new SpaceRequisitionReportService();
    ReportView view = new ReportView();

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetSpaceRequisitionReportdata([FromBody]SpaceReportDetails Spcdata)
    {
        ReportGenerator<SpaceData> reportgen = new ReportGenerator<SpaceData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceRequisitionReport.rdlc"),
            DataSetName = "SpaceRequisitionData",
            ReportType="Space Requisition Report"
        };

        service = new SpaceRequisitionReportService();
        List<SpaceData> reportdata = service.GetSpaceReportDetails(Spcdata);
       
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceRequisitionReport." + Spcdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, Spcdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceRequisitionReport." + Spcdata.Type;
        return result;
    }

    //Getting Grid Data
    [HttpPost]
    public HttpResponseMessage GetGrid(SpaceReportDetails Spcdata)
    {
        var obj = service.GetSpaceReportDetails(Spcdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Getting Requisition Data On Popup
    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(SpaceData Spcdata)
    {
        var obj = service.GetReqDetails(Spcdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Chart Data

    [HttpPost]
    public HttpResponseMessage GetSpaceChartData(SpaceReportDetails Spcdata)
    {
        var obj = service.GetSpaceChart(Spcdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
