﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class SpaceAreaReportController : ApiController
{

    SpaceAreaReportService areaService = new SpaceAreaReportService();
    [HttpPost]
    public HttpResponseMessage GetAreaReportBindGrid()
    {
        var obj = areaService.GetReportByUserObject();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Chart Data

    [HttpPost]
    public HttpResponseMessage GetAreaChartData()
    {
        var obj = areaService.GetAreaChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetAreaReportdata([FromBody]AreaParams rptArea)
    {
       
        ReportGenerator<AreaReport> reportgen = new ReportGenerator<AreaReport>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/AreaReportSpace.rdlc"),
            DataSetName = "AreaReportSPC",
            ReportType = "Area Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/AreaReportSpace." + rptArea.DocType);
        List<AreaReport> reportdata = areaService.GetAreaReportByUserList();
        await reportgen.GenerateReport(reportdata, filePath, rptArea.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AreaReportSpace." + rptArea.DocType;
        return result;

    }
  
}
