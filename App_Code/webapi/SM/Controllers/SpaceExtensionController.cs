﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for SpaceExtensionController
/// </summary>
public class SpaceExtensionController : ApiController
{
    SpaceExtensionService SES = new SpaceExtensionService();

    [HttpPost]
    public HttpResponseMessage GetSpaceExtensionDetails([FromBody] SpaceExtensionVM sevm)
    {
        var obj = SES.GetSpaceExtensionDetails(sevm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage RaiseRequest([FromBody] SpaceExtensionVM SPC_EXTN_DETLS)
    {
        var obj = SES.RaiseRequest(SPC_EXTN_DETLS);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CheckIfSpaceisAllocatedOrNot([FromBody] SMS_SPACE_EXTENSION SPM)
    {
        var obj = SES.CheckIfSpaceisAllocatedOrNot(SPM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ApproveAndRejectRequests([FromBody] SpaceExtensionVM spcextn_det)
    {
        var obj = SES.ApproveAndRejectRequests(spcextn_det);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
	
}