﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BandController : ApiController
{
    BandService service = new BandService();
    [HttpPost]
    public HttpResponseMessage Create(BandModel band)
    {

        if (service.Save(band) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, band);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
        }

    }

    [HttpPost]
    public HttpResponseMessage UpdateBandData(BandModel updt)
    {

        if (service.UpdateBandData(updt))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, updt);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest);
    }

    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<BandModel> bandlist = service.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, bandlist);
        return response;
    }
}
