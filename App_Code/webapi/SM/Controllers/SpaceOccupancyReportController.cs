﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class SpaceOccupancyReportController : ApiController
{
    SpaceOccupancyReportService Occupsvc = new SpaceOccupancyReportService();
    OccupancyReport OccupRpt = new OccupancyReport();

    [HttpPost]
    public async Task<HttpResponseMessage> GetSpaceOccupancyReportdata([FromBody]SpaceOccupancyDetials Occupdata)
    {
        ReportGenerator<SpaceOccupancyData> reportgen = new ReportGenerator<SpaceOccupancyData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceOccupancyReport.rdlc"),
            DataSetName = "SpaceOccupancyReport",
            ReportType = "Space Occupancy Report"
        };
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceOccupancyReport." + Occupdata.Type);
        List<SpaceOccupancyData> reportdata = Occupsvc.GetOccupancyDetails(Occupdata);
        await reportgen.GenerateReport(reportdata, filePath, Occupdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceOccupancyReport." + Occupdata.Type;
        return result;
    }
    [HttpPost]
    public HttpResponseMessage GetOccupGrid(SpaceOccupancyDetials SpcOccup)
    {
        var obj = Occupsvc.GetOccupancyDetails(SpcOccup);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetDetailsCount(SpaceOccupancyDetials CountData)
    {
        var obj = Occupsvc.GetChartCountData(CountData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
