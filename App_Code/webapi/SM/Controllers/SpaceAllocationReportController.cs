﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class SpaceAllocationReportController : ApiController
{
    SpaceAllocationReportService AllocSvc = new SpaceAllocationReportService();
    ReportViewData RptData = new ReportViewData();

    //Exporting Report
    [HttpPost]
    public async Task<HttpResponseMessage> GetSpaceAllocationReport([FromBody]SpaceAllocationDetials SpcAlloc)
    {
        ReportGenerator<SpaceAllocationData> GenAllocReport = new ReportGenerator<SpaceAllocationData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceAllocationReport.rdlc"),
            DataSetName = "SpaceAllocationReport",
            ReportType = "Space Allocation Report"
        };
        AllocSvc = new SpaceAllocationReportService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceAllocationReport." + SpcAlloc.Type);
        List<SpaceAllocationData> reportdata = AllocSvc.GetAllocationDetails(SpcAlloc);
        await GenAllocReport.GenerateReport(reportdata, filePath, SpcAlloc.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceAllocationReport." + SpcAlloc.Type;
        return result;
    }
    //Grid Data
    [HttpPost]
    public HttpResponseMessage GetAllocGrid(SpaceAllocationDetials SpcAlloc)
    {
         var obj = AllocSvc.GetAllocationDetails(SpcAlloc);
         HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
         return response;
    }
    //[HttpPost]
    //public HttpResponseMessage GetDetailsCount(SpaceReportDetails CountData)
    //{
    //    var obj = AllocSvc.GetChartCountData(CountData);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    [HttpPost]
    public object GetDetailsCount(SpaceReportDetails CountData)
    {
        return AllocSvc.GetChartCountData(CountData);
    }

}
