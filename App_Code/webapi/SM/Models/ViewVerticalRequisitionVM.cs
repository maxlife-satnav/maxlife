﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ViewVerticalRequisition
/// </summary>
public class ViewVerticalRequisitionVM
{
    public List<SMS_VERTICAL_REQUISITION> VerticalRequsitionList { get; set; }
      public SMS_VERTICAL_REQUISITION VerticalRequsition { get; set; }
      public VerticalDetails VerticalDetails { get; set; }

}

public class SMS_VERTICAL_REQUISITION
{
    public string SVR_REQ_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }   
    public Nullable<System.DateTime> SVR_REQ_DATE { get; set; }
    public string SVR_VER_CODE { get; set; }
    public Nullable<System.DateTime> SVR_FROM_DATE { get; set; }
    public Nullable<System.DateTime> SVR_TO_DATE { get; set; }
    public int SVR_STA_ID { get; set; }
    public string STA_DESC { get; set; }
    public int SVR_ID { get; set; }
    public string SVR_REQ_BY { get; set; }
    public Boolean ticked { get; set; }
    public string SVR_APPR_REM { get; set; }
    public string SVR_REQ_REM { get; set; }
    public string VER_NAME { get; set; }

}
public class SMS_VERTICAL_REQUISITION_SUB_DTLS
{ 
      public string SVRSD_REQ_ID { get; set; }
      public string SVRSD_CNY_CODE { get; set; }
      public string SVRSD_CTY_CODE { get; set; }
      public string SVRSD_LOC_CODE { get; set; }
      public string SVRSD_TWR_CODE { get; set; }
      public string SVRSD_FLR_CODE { get; set; }
      public string SVRSD_REM { get; set; }
}

public class VerticalDetails
{
      
        public List<Countrylst> selectedCountries { get; set; }
        public List<Zonelst> SelectedZone { get; set; }
        public List<Statelst> SelectedState { get; set; }
        public List<Citylst> selectedCities { get; set; }
        public List<Locationlst> selectedLocations { get; set; }
        public List<Towerlst> selectedTowers { get; set; }
        public List<Floorlst> selectedFloors { get; set; }
        public List<Verticallst> selectedVerticals { get; set; }
        public List<VerticalReq_details> selectedSeats { get; set; }

}
