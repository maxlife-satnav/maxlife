﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class SpaceTypeModel
{
    public string SPC_Code { get; set; }
    public string SPC_Name { get; set; }
    public string SPC_Status_Id { get; set; }
    public string SPC_REM { get; set; }
    public string SPC_COLOR { get; set; }
}