﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ProjectMovementModel
/// </summary>
public class ProjectMovementModel
{
    public List<GetSpaceDetails> GetSpaceDetailsList { get; set; }
    public List<Floorlst> Floorlst { get; set; }
    public List<Costcenterlst> Costcenterlst { get; set; }
    public List<Locationlst> Locationlst { get; set; }
}
public class GetSpaceDetails
{
    public string SSA_SPC_ID { get; set; }
    public string SSA_VER_CODE { get; set; }
    public string VER_NAME { get; set; }
    public string SSAD_COST_CENTER { get; set; }
    public string Cost_Center_Name { get; set; }
    public string SSAD_AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string SSA_LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string SSA_TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public string SSA_FLR_CODE { get; set; }
    public string SSA_SPC_SUB_TYPE { get; set; }
    public string SST_NAME { get; set; }
}
public class CountData
{
    public string SPC_FLR_ID { get; set; }
    public string VER_NAME { get; set; }
    public string COST_CENTER_NAME { get; set; }
    public string WORK_STATION { get; set; }
    public string CUBICLE { get; set; }
    public string CABIN { get; set; }
}
