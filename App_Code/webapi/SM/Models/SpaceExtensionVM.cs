﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceExtensionVM
/// </summary>
public class SpaceExtensionVM
{
    public SMS_SPACE_EXTENSION_VM spcextn { get; set; }
    public SPC_EXTN_FLOOR_VERTICAL_LIST spceextn_flr_ver { get; set; }
    public List<SMS_SPACE_EXTENSION_VM> spcextn_lst { get; set; }
    public int FLAG { get; set; }
    public string Remarks { get; set; }

}
public class SMS_SPACE_EXTENSION
{
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SSED_SPC_ID { get; set; }
    public string SPC_NAME { get; set; }
    public string SPC_FLR_ID { get; set; }
    public string lat { get; set; }
    public string lon { get; set; }
    public Nullable<DateTime> SSAD_FROM_DATE { get; set; }
    public Nullable<DateTime> SSAD_TO_DATE { get; set; }
    public Nullable<DateTime>  SSAD_FROM_TIME { get; set; }
    public Nullable<DateTime> SSAD_TO_TIME { get; set; }
    public string SSA_SPC_TYPE { get; set; }
    public string SSA_SPC_SUB_TYPE { get; set; }
    public string SH_NAME { get; set; }
    public string Cost_Center_Name { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string SSED_AUR_ID { get; set; }
    public string SSED_VER_NAME { get; set; }
    public string EMP_DESIGNATION { get; set; }
    public string STATUS { get; set; }
    public Nullable<DateTime> SSED_EXTN_DT { get; set; }
    public Boolean ticked { get; set; }
    public int SSAD_ID { get; set; }
    public int STACHECK { get; set; }
    public string SSED_REQ_ID { get; set; }
 }
public class SMS_SPACE_EXTENSION_VM
{
    public string SSE_REQ_ID { get; set; }
    public int SSE_STA_ID { get; set; }
    public string SSE_REQ_BY { get; set; }
    public string SSE_APPR_BY { get; set; }
    public string SSE_UPDATED_BY { get; set; }
    public string SSE_REQ_REM { get; set; }
    public string SSE_APPR_REM { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string STA_DESC { get; set; }
    //public string SSED_SSE_REQ_ID { get; set; }
    public Nullable<DateTime> SSE_REQ_DT { get; set; }
    public Nullable<DateTime> SSE_FROM_DATE { get; set; }
    public Nullable<DateTime> SSE_TO_DATE { get; set; }
    //public DateTime SSED_EXTN_DT { get; set; }
    public int STACHECK { get; set; }
}
public class SMS_SPACE_EXTENSION_DETAILS
{
    public string SSED_REQ_ID { get; set; }
    public string SSED_SSE_REQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public DateTime SSED_EXTN_DT { get; set; }
    public string SSED_SPC_ID { get; set; }
    public int SSED_STA_ID { get; set; }
    public Boolean ticked { get; set; }
    public int STACHECK { get; set; }
    public Nullable<DateTime> SSAD_FROM_DATE { get; set; }
    public Nullable<DateTime> SSAD_TO_DATE { get; set; }
    public Nullable<DateTime> SSAD_FROM_TIME { get; set; }
    public Nullable<DateTime> SSAD_TO_TIME { get; set; }
}

public class SPC_EXTN_FLOOR_VERTICAL_LIST
{
    public List<Countrylst> selectedCountries { get; set; }
    public List<Citylst> selectedCities { get; set; }
    public List<Locationlst> selectedLocations { get; set; }
    public List<Towerlst> selectedTowers { get; set; }
    public List<Floorlst> selectedFloors { get; set; }
    public List<Verticallst> selectedVerticals { get; set; }
    public List<Costcenterlst> selectedCostcenters { get; set; }
    public List<SMS_SPACE_EXTENSION_DETAILS> selectedSpaces { get; set; }
    public List<Statelst> selectedStates { get; set; }
    public List<Zonelst> selectedZones { get; set; }
    
}