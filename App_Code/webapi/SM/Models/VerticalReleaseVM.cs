﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for VerticalReleaseVM
/// </summary>
public class VerticalReleaseVM
{
    public VerticalReleaseDetails VerticalReleaseDetails { get; set; }
    public VerticalReleaseMaster VRM { get; set; }
    public List<VerticalReleaseMaster> VR { get; set; }
    public string SVRL_REL_REM { get; set; }
}

public class VerticalReleaseDetails
{
    public List<Countrylst> selectedCountries { get; set; }
    public List<Citylst> selectedCities { get; set; }
    public List<Locationlst> selectedLocations { get; set; }
    public List<Towerlst> selectedTowers { get; set; }
    public List<Verticallst> selectedVerticals { get; set; }
    public List<Floorlst> selectedFloors { get; set; }
}

public class VerticalReleaseMaster
{
    public string SSA_SRNREQ_ID { get; set; }
    public string SSA_SRNCC_ID { get; set; }
    public string SSA_VER_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string SSA_SPC_ID { get; set; }
    public Nullable<DateTime> SSA_FROM_DATE { get; set; }
    public Nullable<DateTime> SSA_TO_DATE { get; set; }
    public string SSA_STA_ID { get; set; }
    public Boolean ticked { get; set; }
    public string SSA_SPC_TYPE { get; set; }
    public string SSA_SPC_SUB_TYPE { get; set; }
    public string SPC_NAME { get; set; }
    public string SPC_FLR_ID { get; set; }
    public string lat { get; set; }
    public string lon { get; set; }
    public string VER_NAME { get; set; }
    
}

