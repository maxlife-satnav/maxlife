﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for BandWiseOccupancyReportVM
/// </summary>
public class BandWiseOccupancyReportVM
{
    public string SUB_TYPE { get; set; }
    public int SPC_BAND { get; set; }
    public int EMP_BAND { get; set; }
    public int CNT { get; set; }
}

public class Band
{
    public string BAND_NAME { get; set; }
    public int BAND_ORDER { get; set; }
}

public class Subtype_Band
{
    public string SST_CODE { get; set; }
    public string SST_NAME { get; set; }
    public int BAND_ORDER { get; set; }
}
public class BandWiseOccupancy
{
    public string SUB_TYPE { get; set; }
    public string SPC_ID { get; set; }
    public string EMP_BAND { get; set; }
    public string EMP_ID { get; set; }
    public string SPC_BAND { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string VER_NAME { get; set; }
    public string Cost_Center_Name { get; set; }
    public string STATUS { get; set; }
    public string AUR_TYPE { get; set; }
   
}
public class BandList
{
 public List<Floorlst> flrlst { get; set; }
 public string Type { get; set; }
 public int mode { get; set; }
}
