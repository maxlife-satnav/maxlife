﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for SpaceConsolidatedReportVM
/// </summary>
public class SpaceConsolidatedReportVM
{
    public string CNY_NAME { get; set; }
    public string ZN_NAME { get; set; }
    public string STE_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public Int32 ALLOCATED { get; set; }
    public Int32 OCCUPIED { get; set; }
    public Int32 VACANT { get; set; }
    public Int32 TOTAL { get; set; }
}

public class SpaceConsolidatedReportVM_Chart
{
    public string ALLOCATED_SEATS { get; set; }
    public string OCCUPIED_SEATS { get; set; }
    public string ALLOCATED_VACANT { get; set; }
    public string VACANT_SEATS { get; set; }
    public string TOTAL_SEATS { get; set; }
}

public class SpaceConsolidatedParameters
{
    public string DocType { get; set; }
}

public class SpaceSonsolidatedReportView
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public SpaceSonsolidatedReportView()
    {
        CostData = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> CostData { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case SpaceSonsolidatedReportView.ReportFormat.Word: return ".doc";
                case SpaceSonsolidatedReportView.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.CostData)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}