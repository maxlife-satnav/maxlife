﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceExtensionRequisitionVM
/// </summary>
/// 


public class ViewSpaceExtnRequisitionVM
{
    public List<SpaceExtReqVM> SpaceExtnRequsitionList { get; set; }
    public SpaceExtReqVM spaceExtnRequsition { get; set; }
    public SpaceExtnMasterDetails VerticalDetails { get; set; }

}

public class SpaceExtReqVM
{
    public int SSE_ID { get; set; }
    public string SSE_REQ_ID { get; set; }
    public string SSE_SSA_SRN_REQID { get; set; }
    public int SSE_STA_ID { get; set; }
    public string SSE_REQ_BY { get; set; }
    public DateTime SSE_REQ_DT { get; set; }
    public string SSE_REQ_REM { get; set; }
    public string SSE_APPR_BY { get; set; }
    public DateTime SSE_APPR_DT { get; set; }
    public string SSE_APPR_REM { get; set; }
    public string AUR_KNOWN_AS { get; set; }

    public DateTime SSA_FROM_DATE { get; set; }

    public DateTime SSA_TO_DATE { get; set; }
    public string STA_DESC { get; set; }
    public string SSED_VER_CODE { get; set; }

    public int STACHECK { get; set; }
    public bool ticked { get; set; }
}

public class SpaceExtnMasterDetails
{

    public List<Countrylst> selectedCountries { get; set; }
    public List<Citylst> selectedCities { get; set; }
    public List<Locationlst> selectedLocations { get; set; }
    public List<Towerlst> selectedTowers { get; set; }
    public List<Floorlst> selectedFloors { get; set; }
    public List<Verticallst> selectedVerticals { get; set; }
    public List<Costcenterlst> selectedCostcenters { get; set; }
    public List<SpaceExtReqVM> selectedSeats { get; set; }

}