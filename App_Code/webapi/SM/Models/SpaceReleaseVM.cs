﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceReleaseVM
/// </summary>
public class SpaceReleaseVM
{
    public List<SMS_SPACE_RELEASE> SSR { get; set; }
    public string Remarks { get; set; }
    public int RELEASE_STA_ID { get; set; }
}

public class SMS_SPACE_RELEASE
{
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SSED_SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_AUR_ID { get; set; }
}

public class SMS_SPACE_RELEASE_VM
{
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_AUR_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SSAD_SRN_CC_ID { get; set; }
    public string SSED_SPC_ID { get; set; }
    public Nullable<DateTime> SSAD_FROM_DATE { get; set; }
    public Nullable<DateTime> SSAD_TO_DATE { get; set; }
    public Nullable<DateTime> SSAD_FROM_TIME { get; set; }
    public Nullable<DateTime> SSAD_TO_TIME { get; set; }
    public string SSA_SPC_TYPE { get; set; }
    public string SSA_SPC_SUB_TYPE { get; set; }
    public string SH_NAME { get; set; }
    public string Cost_Center_Name { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string SSED_AUR_ID { get; set; }
    public string SSED_VER_NAME { get; set; }
    public string EMP_DESIGNATION { get; set; }
    public string STATUS { get; set; }
    public Nullable<DateTime> SSED_EXTN_DT { get; set; }
    public Boolean ticked { get; set; }
    public int SSAD_ID { get; set; }
    public string SPC_NAME { get; set; }
    public string SPC_FLR_ID { get; set; }
    public string lat { get; set; }
    public string lon { get; set; }
}

public class SPC_RELEASE_FLOOR_VERTICAL_SPACE_LIST
{
    public List<Countrylst> selectedCountries { get; set; }
    public List<Zonelst> selectedZones { get; set; }
    public List<Statelst> selectedStates { get; set; }
    public List<Citylst> selectedCities { get; set; }
    public List<Locationlst> selectedLocations { get; set; }
    public List<Towerlst> selectedTowers { get; set; }
    public List<Floorlst> selectedFloors { get; set; }
    public List<Verticallst> selectedVerticals { get; set; }
    public List<Costcenterlst> selectedCostcenters { get; set; }
    public Nullable<DateTime> FromDate { get; set; }
    public Nullable<DateTime> ToDate { get; set; }
    public int RELEASE_STA_ID { get; set; }
    public string EMP_ID { get; set; }
}