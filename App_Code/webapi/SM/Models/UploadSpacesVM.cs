﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UploadSpacesVM
/// </summary>
public class UploadSpacesVM
{
    public List<Floorlst> flrlst { get; set; }
    public string ALLOCSTA { get; set; }
}

public class UploadAllocationDataVM
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SpaceID { get; set; }
    public string SeatType { get; set; }
    public string Vertical { get; set; }
    public string Costcenter { get; set; }
    public string EmployeeID { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
}

public class UPLTYPEVM
{
    public string UplAllocType { get; set; }
    public string UplOptions { get; set; }
}