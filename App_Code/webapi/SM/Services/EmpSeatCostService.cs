﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

/// <summary>
/// Summary description for EmpSeatCostService
/// </summary>
public class EmpSeatCostService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object GetEmpSeatCost([FromBody] EmpSeatCostVM ObjGridSummary)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "OCCUPIED_UNOCCUPIED_SEAT_COST_CHART");
        sp.Command.Parameters.Add("@LOC_CODE", string.IsNullOrEmpty(ObjGridSummary.LOCATION) ? "--All--" : ObjGridSummary.LOCATION, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", string.IsNullOrEmpty(ObjGridSummary.FLOOR) ? "--All--" : ObjGridSummary.FLOOR, DbType.String);
        ds = sp.GetDataSet();

        return new { ChartData = new dynamic[] { new { TotalSeats = ds.Tables[0].Rows[0]["CNT"] }, new { OccSeats = ds.Tables[0].Rows[1]["CNT"] }, new { UnoccSeats = ds.Tables[0].Rows[2]["CNT"] } } };
    }
    public object GetEmpSeatCount([FromBody] EmpSeatCostVM ObjGridSummary)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "OCCUPIED_UNOCCUPIED_SEAT_COUNT_CHART");
        sp.Command.Parameters.Add("@LOC_CODE", string.IsNullOrEmpty(ObjGridSummary.LOCATION) ? "--All--" : ObjGridSummary.LOCATION, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", string.IsNullOrEmpty(ObjGridSummary.FLOOR) ? "--All--" : ObjGridSummary.FLOOR, DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }
}