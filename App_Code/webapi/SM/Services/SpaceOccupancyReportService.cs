﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpaceOccupancyReportService
{
    SubSonic.StoredProcedure sp;
    SpaceOccupancyData SpcOccup;
    List<SpaceOccupancyData> OccupList;
    DataSet ds;

    public List<SpaceOccupancyData> GetOccupancyDetails(SpaceOccupancyDetials Occup)
    {
        try
        {
            OccupList = new List<SpaceOccupancyData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_OCCUPANCY_REPORT");
            sp.Command.AddParameter("@FROM_DATE", Occup.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", Occup.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    SpcOccup = new SpaceOccupancyData();
                    SpcOccup.CNY_NAME = reader["CNY_NAME"].ToString();
                    SpcOccup.ZN_NAME = reader["ZN_NAME"].ToString();
                    SpcOccup.STE_NAME = reader["STE_NAME"].ToString();
                    SpcOccup.CTY_NAME = reader["CTY_NAME"].ToString();
                    SpcOccup.LCM_NAME = reader["LCM_NAME"].ToString();
                    SpcOccup.TWR_NAME = reader["TWR_NAME"].ToString();
                    SpcOccup.FLR_NAME = reader["FLR_NAME"].ToString();
                    SpcOccup.VER_NAME = reader["VER_NAME"].ToString();
                    SpcOccup.DEP_NAME = reader["DEP_NAME"].ToString();
                    SpcOccup.SHIFT = reader["SHIFT"].ToString();
                    SpcOccup.SPACE_TYPE = reader["SPACE_TYPE"].ToString();
                    SpcOccup.SPACE_SUB_TYPE = reader["SPACE_SUB_TYPE"].ToString();
                    SpcOccup.EMP_ID = reader["EMP_ID"].ToString();
                    SpcOccup.AUR_ID = reader["AUR_ID"].ToString();
                    SpcOccup.SPC_ID = reader["SPC_ID"].ToString();
                    SpcOccup.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                    SpcOccup.AUR_TYPE = reader["AUR_TYPE"].ToString();
                    SpcOccup.AUR_REPORTING_TO = reader["AUR_REPORTING_TO"].ToString();
                    SpcOccup.AUR_GENDER = reader["AUR_GENDER"].ToString();
                    SpcOccup.AUR_GRADE = reader["AUR_GRADE"].ToString();
                    SpcOccup.DSN_AMT_TITLE = reader["DSN_AMT_TITLE"].ToString();
                    OccupList.Add(SpcOccup);
                }
                reader.Close();
            }
            if (OccupList.Count != 0)
                return OccupList;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }

    public object GetChartCountData(SpaceOccupancyDetials OccupDetails)
    {
        try
        {
            List<SpaceOccupancyData> Spcdata = new List<SpaceOccupancyData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_OCCUPANCY_COUNT");
            sp.Command.AddParameter("@FROM_DATE", OccupDetails.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", OccupDetails.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch
        {
            throw;
        }
    }
}