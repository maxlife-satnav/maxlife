﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for L2ApprovalService
/// </summary>
public class L2ApprovalService
{
    SubSonic.StoredProcedure sp;
    public object GetPendingSpaceRequisitions()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "View_L2_Pending_Space_Requisition_Details");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                List<SpaceReq> spclist = new List<SpaceReq>();
                while (reader.Read())
                {
                    spclist.Add(new SpaceReq()
                    {
                        SRN_REQ_ID = reader["SRN_REQ_ID"].ToString(),
                        VER_NAME = reader["VER_NAME"].ToString(),
                        COST_CENTER_NAME = reader["COST_CENTER_NAME"].ToString(),
                        STA_DESC = reader["STA_DESC"].ToString(),
                        SRN_FROM_DATE = (DateTime)reader["SRN_FROM_DATE"],
                        SRN_TO_DATE = (DateTime)reader["SRN_TO_DATE"],
                        AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                        SRN_REQ_BY = reader["SRN_REQ_BY"].ToString(),
                        SRN_REQ_REM = reader["SRN_REQ_REM"].ToString(),
                        SRN_STA_ID = (int)reader["SRN_STA_ID"],
                        SRN_REQ_DT = (DateTime)reader["SRN_REQ_DT"],
                        SRN_SYS_PRF_CODE = reader["SRN_SYS_PRF_CODE"].ToString(),
                        SRN_VERTICAL = reader["SRN_VERTICAL"].ToString(),
                        SRN_COST_CENTER = reader["SRN_COST_CENTER"].ToString(),
                    });
                }
                reader.Close();
                if (spclist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = spclist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

   
    public object ApproveRequisitions(SpaceReqDetails spcreqDetails)
    {
        try
        {
            List<SpaceRequistion> spcreqLst = new List<SpaceRequistion>();
            spcreqLst.Add(spcreqDetails.spcreq);
            if (spcreqLst.Count != 0)
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@SPC_REQ", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(spcreqLst);
                param[1] = new SqlParameter("@SPC_REQ_DET", SqlDbType.Structured);
                if (spcreqDetails.spcreqdet == null)
                {
                    param[1].Value = null;
                }
                else
                {
                    param[1].Value = UtilityService.ConvertToDataTable(spcreqDetails.spcreqdet);
                }

                param[2] = new SqlParameter("@SPC_REQ_DET_COUNT", SqlDbType.Structured);
                if (spcreqDetails.spcreqcount==null)
                {
                    param[2].Value = null;
                }
                else
                {
                    param[2].Value = UtilityService.ConvertToDataTable(spcreqDetails.spcreqcount);
                }
                param[3] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
                param[3].Value = spcreqDetails.ALLOCSTA;
                param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[4].Value = HttpContext.Current.Session["UID"];

                using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_L2_SPACEREQ_TRAN", param))
                {
                    if (dr.Read())
                    {
                        int FLAG = (int)dr["FLAG"];
                        string MSG = dr["MSG"].ToString();

                        if (FLAG == 1)
                        {
                            string RetMessage = string.Empty;
                            RequestState sta = (RequestState)spcreqDetails.ALLOCSTA;
                            switch (sta)
                            {
                                case RequestState.Approved: RetMessage = MessagesVM.SPC_REQ_APPROVED;
                                    break;
                                case RequestState.Rejected: RetMessage = MessagesVM.SPC_REQ_REJECTED;
                                    break;
                            }
                            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_REQUISITION_L2_APPR_ACTION");
                            sp.Command.AddParameter("@REQID", MSG, DbType.String);
                            sp.Command.AddParameter("@STATUS", spcreqDetails.ALLOCSTA, DbType.String);
                            sp.ExecuteScalar();
                            return new { Message = RetMessage + " For : " + MSG, data = MSG };
                        }
                        else
                            return new { Message = MSG, data = (object)null };
                    }
                }
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }

            else
                return new { Message = MessagesVM.L1_NOSPC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object ApproveAllReq(SpaceReqDetails spcreqDetails)
    {
        try
        {
            if (spcreqDetails.spcreqList.Count != 0)
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@SPC_REQ", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(spcreqDetails.spcreqList);
                param[1] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
                param[1].Value = spcreqDetails.ALLOCSTA;
                param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[2].Value = HttpContext.Current.Session["UID"];
                param[3] = new SqlParameter("@L2_REMARKS", SqlDbType.NVarChar);
                param[3].Value = spcreqDetails.SRN_L2_REM;

                using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_L2_SPACEREQ_APPROVEALL", param))
                {
                    if (dr.Read())
                    {
                        int FLAG = (int)dr["FLAG"];
                        string MSG = dr["MSG"].ToString();

                        if (FLAG == 1)
                        {
                            string RetMessage = string.Empty;
                            RequestState sta = (RequestState)spcreqDetails.ALLOCSTA;
                            switch (sta)
                            {
                                case RequestState.Approved: RetMessage = MessagesVM.SPC_REQ_APPROVED;
                                    break;
                                case RequestState.Rejected: RetMessage = MessagesVM.SPC_REQ_REJECTED;
                                    break;
                            }
                            foreach (SpaceRequistion spc in spcreqDetails.spcreqList)
                            {
                                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_REQUISITION_L2_APPR_ACTION");
                                sp.Command.AddParameter("@REQID", spc.SRN_REQ_ID, DbType.String);
                                sp.Command.AddParameter("@STATUS", spcreqDetails.ALLOCSTA, DbType.Int32);
                                sp.ExecuteScalar();
                            }
                            return new { Message = RetMessage, data = MSG };
                        }
                        else
                            return new { Message = MSG, data = (object)null };
                    }
                }
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
            else
                return new { Message = MessagesVM.L1_NOREQ, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
}