﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BandWiseOccupancyReportService
/// </summary>
public class BandWiseOccupancyReportService
{
    SubSonic.StoredProcedure sp;
    BandWiseOccupancy BandOccu;
    DataSet ds;
    public object GetRptGrid(BandList bandlist)
    {
        List<BandWiseOccupancyReportVM> BandOccdet = new List<BandWiseOccupancyReportVM>();

        //SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_BAND_OCCUPANCY_REPORT");       
        //using (IDataReader reader = sp.GetReader())
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@FLRLIST", SqlDbType.Structured);
      
        if (bandlist.flrlst == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = UtilityService.ConvertToDataTable(bandlist.flrlst);
        }
        param[1] = new SqlParameter("@MODE", SqlDbType.Int);
        param[1].Value = bandlist.mode;
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_BAND_OCCUPANCY_REPORT", param))
        {

            while (reader.Read())
            {
                BandOccdet.Add(new BandWiseOccupancyReportVM()
                {
                    SUB_TYPE = reader["SUB_TYPE"].ToString(),
                    SPC_BAND = Convert.ToInt32(reader["SPC_BAND"]),
                    EMP_BAND = Convert.ToInt32(reader["EMP_BAND"]),
                    CNT = Convert.ToInt32(reader["CNT"]),
                });
            }
            reader.Close();
        }
        List<DataRow> BandRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_EMP_BANDS").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
        List<DataRow> SubtypeBandRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_SUBTYPE_BANDS").GetDataSet().Tables[0].Select().AsEnumerable().ToList();

        List<Band> Bands = new List<Band>();
        List<Subtype_Band> SubtypeBands = new List<Subtype_Band>();

        Band bnd;
        Subtype_Band sbnd;

        foreach (DataRow dr in BandRows)
        {
            bnd = new Band();
            bnd.BAND_ORDER = Convert.ToInt32(dr["BAND_ORDER"]);
            bnd.BAND_NAME = dr["BAND_NAME"].ToString();
            Bands.Add(bnd);
        }

        foreach (DataRow dr in SubtypeBandRows)
        {
            sbnd = new Subtype_Band();
            sbnd.BAND_ORDER = Convert.ToInt32(dr["BAND_ORDER"]);
            sbnd.SST_CODE = dr["SST_CODE"].ToString();
            sbnd.SST_NAME = dr["SST_NAME"].ToString();
            SubtypeBands.Add(sbnd);
        }

        Dictionary<object, object> dictBand = new Dictionary<object, object>();

        Dictionary<object, object> dictSubband;
        foreach (Band band in Bands)
        {
            dictSubband = new Dictionary<object, object>();
            foreach (Band empband in Bands)
            {
                var Banddet = BandOccdet.Where(det => det.SPC_BAND == band.BAND_ORDER && det.EMP_BAND == empband.BAND_ORDER)
                            .Select(slad => new { SUB_TYPE = slad.SUB_TYPE, SPC_BAND = slad.SPC_BAND, EMP_BAND = slad.EMP_BAND, CNT = slad.CNT }).FirstOrDefault();
                if (Banddet != null)
                {
                    dictSubband.Add(new { BAND_NAME = empband.BAND_NAME, BAND_CODE = empband.BAND_ORDER }, Banddet);
                }

                else
                    dictSubband.Add(new { BAND_NAME = empband.BAND_NAME, BAND_CODE = empband.BAND_ORDER }, new
                    {
                        SUB_TYPE = "",
                        SPC_BAND = band.BAND_ORDER,
                        EMP_BAND = empband.BAND_ORDER,
                        CNT = 0
                    });
            }
            dictBand.Add(new { BAND_NAME = band.BAND_NAME, BAND_CODE = band.BAND_ORDER }, dictSubband.ToList());

        }
        return new { BANDDET = dictBand.ToList(), BANDS = Bands, SUBTYPEBAND = SubtypeBands };
    }

    public List<BandWiseOccupancy> GetAllocationDetails(BandList bandlist)
    {
        try
        {
            List<BandWiseOccupancy> BandOccuList = new List<BandWiseOccupancy>();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@FLRLIST", SqlDbType.Structured);
            if (bandlist.flrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(bandlist.flrlst);
            }

            param[1] = new SqlParameter("@MODE", SqlDbType.Int);
            param[1].Value = bandlist.mode;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_BAND_OCCUPANCY_REPORT_GRID", param))
            {
                while (reader.Read())
                {
                    BandOccu = new BandWiseOccupancy();
                    BandOccu.SUB_TYPE = reader["SUB_TYPE"].ToString();
                    BandOccu.SPC_ID = reader["SPC_ID"].ToString();
                    BandOccu.EMP_BAND = reader["EMP_BAND"].ToString();
                    BandOccu.EMP_ID = reader["EMP_ID"].ToString();
                    BandOccu.SPC_BAND = reader["SPC_BAND"].ToString();
                    BandOccu.AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString();
                    BandOccu.VER_NAME = reader["VER_NAME"].ToString();
                    BandOccu.Cost_Center_Name = reader["Cost_Center_Name"].ToString();
                    BandOccu.STATUS = reader["STATUS"].ToString();
                    BandOccu.AUR_TYPE = reader["AUR_TYPE"].ToString();
                    BandOccuList.Add(BandOccu);
                }
                reader.Close();
            }
            if (BandOccuList.Count != 0)
                return BandOccuList;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }

}