﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;


/// <summary>
/// Summary description for OverloadService
/// </summary>
public class OverloadService
{
    SubSonic.StoredProcedure sp;
    List<OverloadReportModel> rptByUserlst;
    HDMReport_Bar_Graph rptBarGph;
    DataSet ds;
    public object GetOverloadObject()
    {
        try
        {
            rptByUserlst = GetOverloadReportList();

            if (rptByUserlst == null)
            {
                return new { Message = MessagesVM.UM_NO_REC, data = rptByUserlst };
            }

            else if (rptByUserlst.Count != 0)
            {
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }

            else
                return new { Message = MessagesVM.UM_NO_REC, data = rptByUserlst };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<OverloadReportModel> GetOverloadReportList()
    {

        try
        {
            List<OverloadReportModel> rptByUserlst = new List<OverloadReportModel>();
            OverloadReportModel rptByUser;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_OVER_LOAD_REPORT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    rptByUser = new OverloadReportModel();
                    rptByUser.CNY_NAME = sdr["CNY_NAME"].ToString();
                    rptByUser.ZN_NAME = sdr["ZN_NAME"].ToString();
                    rptByUser.STE_NAME = sdr["STE_NAME"].ToString();
                    rptByUser.CTY_NAME = sdr["CTY_NAME"].ToString();
                    rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                    rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                    rptByUser.FLR_NAME = sdr["FLR_NAME"].ToString();
                    rptByUser.SPACE_ID = sdr["SPACE_ID"].ToString();
                    rptByUser.OVERLOAD_COUNT = (int)sdr["OVERLOAD_COUNT"];
                    rptByUserlst.Add(rptByUser);
                }
                sdr.Close();
            }
            if (rptByUserlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
                return rptByUserlst;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }


    public object GetOverloadChart()
    {
        try
        {
            List<OverloadReportModel> area = new List<OverloadReportModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_OVER_LODAD_GRAPH");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }


}