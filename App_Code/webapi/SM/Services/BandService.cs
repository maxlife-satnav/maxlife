﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BandService
/// </summary>
public class BandService
{
    SubSonic.StoredProcedure sp;
    //Saving Data//
    public int Save(BandModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SAVE_BAND_DATA");
            sp.Command.AddParameter("@BAND_CODE", model.Code, DbType.String);
            sp.Command.AddParameter("@BAND_NAME", model.Name, DbType.String);
            sp.Command.AddParameter("@BAND_STA_ID", model.Status, DbType.String);
            sp.Command.AddParameter("@BAND_REMARKS", model.Remarks, DbType.String);
           
        }
        catch
        {
            throw;
        }
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }

    //Updating Data//
    public Boolean UpdateBandData(BandModel updt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_UPDATE_BAND_MASTER");
            sp.Command.AddParameter("@BAND_CODE", updt.Code, DbType.String);
            sp.Command.AddParameter("@BAND_NAME", updt.Name, DbType.String);
            sp.Command.AddParameter("@BAND_STA_ID", updt.Status, DbType.String);
            sp.Command.AddParameter("@BAND_REMARKS", updt.Remarks, DbType.String);
           
            sp.Execute();
        }
        catch
        {
            throw;
        }
        return true;
    }
    //Binding Data to Grid//
    public IEnumerable<BandModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_BIND_BAND_GRIDDATA").GetReader())
        {
            try
            {
                List<BandModel> Bandlist = new List<BandModel>();
                while (reader.Read())
                {
                    Bandlist.Add(new BandModel()
                    {
                        Code = reader.GetValue(0).ToString(),
                        Name = reader.GetValue(1).ToString(),
                        Status = reader.GetValue(2).ToString(),
                        Remarks = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return Bandlist;
            }
            catch
            {
                throw;
            }
        }

    }
}