﻿//#define INCLUDE_WEB_FUNCTIONS

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data;
using System.Reflection;
using System.Drawing;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;


//
//  November 2013
//  http://www.mikesknowledgebase.com
//
//  Note: if you plan to use this in an ASP.Net application, remember to add a reference to "System.Web", and to uncomment
//  the "INCLUDE_WEB_FUNCTIONS" definition at the top of this file.
//
//  Release history
//   - Nov 2013: 
//        Changed "CreateExcelDocument(DataTable dt, string xlsxFilePath)" to remove the DataTable from the DataSet after creating the Excel file.
//        You can now create an Excel file via a Stream (making it more ASP.Net friendly)
//   - Jan 2013: Fix: Couldn't open .xlsx files using OLEDB  (was missing "WorkbookStylesPart" part)
//   - Nov 2012: 
//        List<>s with Nullable columns weren't be handled properly.
//        If a value in a numeric column doesn't have any data, don't write anything to the Excel file (previously, it'd write a '0')
//   - Jul 2012: Fix: Some worksheets weren't exporting their numeric data properly, causing "Excel found unreadable content in '___.xslx'" errors.
//   - Mar 2012: Fixed issue, where Microsoft.ACE.OLEDB.12.0 wasn't able to connect to the Excel files created using this class.
//

public class CreateExcelFile
{
    public enum HeaderType : int
    {
        AllHeader,
        AllFooter,
        OddHeader,
        OddFooter,
        EvenHeader,
        EvenFooter,
        FirstHeader,
        FirstFooter
    }

    public static bool CreateExcelDocument<T>(List<T> list, string xlsxFilePath)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(ListToDataTable(list));

        return CreateExcelDocument(ds, xlsxFilePath);
    }
    #region HELPER_FUNCTIONS
    //  This function is adapated from: http://www.codeguru.com/forum/showthread.php?t=450171
    //  My thanks to Carl Quirion, for making it "nullable-friendly".
    public static DataTable ListToDataTable<T>(List<T> list)
    {
        DataTable dt = new DataTable();

        foreach (PropertyInfo info in typeof(T).GetProperties())
        {
            dt.Columns.Add(new DataColumn(info.Name, GetNullableType(info.PropertyType)));
        }
        foreach (T t in list)
        {
            DataRow row = dt.NewRow();
            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                if (!IsNullableType(info.PropertyType))
                    row[info.Name] = info.GetValue(t, null);
                else
                    row[info.Name] = (info.GetValue(t, null) ?? DBNull.Value);
            }
            dt.Rows.Add(row);
        }
        return dt;
    }
    private static Type GetNullableType(Type t)
    {
        Type returnType = t;
        if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
        {
            returnType = Nullable.GetUnderlyingType(t);
        }
        return returnType;
    }
    private static bool IsNullableType(Type type)
    {
        return (type == typeof(string) ||
                type.IsArray ||
                (type.IsGenericType &&
                 type.GetGenericTypeDefinition().Equals(typeof(Nullable<>))));
    }

    public static bool CreateExcelDocument(DataTable dt, string xlsxFilePath)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        bool result = CreateExcelDocument(ds, xlsxFilePath);
        ds.Tables.Remove(dt);
        return result;
    }
    #endregion


    /// <summary>
    /// Create an Excel file, and write it out to a MemoryStream (rather than directly to a file)
    /// </summary>
    /// <param name="dt">DataTable containing the data to be written to the Excel.</param>
    /// <param name="filename">The filename (without a path) to call the new Excel file.</param>
    /// <param name="Response">HttpResponse of the current page.</param>
    /// <returns>True if it was created succesfully, otherwise false.</returns>
    public static bool CreateExcelDocument(DataTable dt, string filename, System.Web.HttpResponse Response)
    {
        try
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            CreateExcelDocumentAsStream(ds, filename, Response);
            ds.Tables.Remove(dt);
            return true;
        }
        catch (Exception ex)
        {
            //ErrorSignal.FromCurrentContext().Raise(ex);
            Trace.WriteLine("Failed, exception thrown: " + ex.Message);
            return false;
        }
    }

    public static bool CreateExcelDocument<T>(List<T> list, string filename, System.Web.HttpResponse Response)
    {
        try
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(ListToDataTable(list));
            CreateExcelDocumentAsStream(ds, filename, Response);
            return true;
        }
        catch (Exception ex)
        {
            //ErrorSignal.FromCurrentContext().Raise(ex);
            Trace.WriteLine("Failed, exception thrown: " + ex.Message);
            return false;
        }
    }

    public static bool CreateExcelDocumentAndSave<T>(List<T> list, string filename)
    {
        try
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(ListToDataTable(list));
            CreateExcelDocument(ds, filename);
            return true;
        }
        catch (Exception ex)
        {
            //ErrorSignal.FromCurrentContext().Raise(ex);
            Trace.WriteLine("Failed, exception thrown: " + ex.Message);
            return false;
        }
    }



    /// <summary>
    /// Create an Excel file, and write it out to a MemoryStream (rather than directly to a file)
    /// </summary>
    /// <param name="ds">DataSet containing the data to be written to the Excel.</param>
    /// <param name="filename">The filename (without a path) to call the new Excel file.</param>
    /// <param name="Response">HttpResponse of the current page.</param>
    /// <returns>Either a MemoryStream, or NULL if something goes wrong.</returns>
    public static bool CreateExcelDocumentAsStream(DataSet ds, string filename, System.Web.HttpResponse Response)
    {
        try
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook, true))
            {
                WriteExcelFile(ds, document);
            }
            stream.Flush();
            stream.Position = 0;

            Response.ClearContent();
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";

            //  NOTE: If you get an "HttpCacheability does not exist" error on the following line, make sure you have
            //  manually added System.Web to this project's References.

            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            byte[] data1 = new byte[stream.Length];
            stream.Read(data1, 0, data1.Length);
            stream.Close();
            Response.BinaryWrite(data1);
            Response.Flush();
            Response.End();

            return true;
        }
        catch (Exception ex)
        {
            //ErrorSignal.FromCurrentContext().Raise(ex);
            Trace.WriteLine("Failed, exception thrown: " + ex.Message);
            return false;
        }
    }

    public static MemoryStream CreateExcelDocumentAsStream(DataSet ds)
    {
        System.IO.MemoryStream stream = new System.IO.MemoryStream();
        try
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook, true))
            {
                WriteExcelFile(ds, document);
            }
            stream.Flush();
            stream.Position = 0;

            //  NOTE: If you get an "HttpCacheability does not exist" error on the following line, make sure you have
            //  manually added System.Web to this project's References.

            byte[] data1 = new byte[stream.Length];
            stream.Read(data1, 0, data1.Length);
            stream.Close();
            return stream;
        }
        catch (Exception ex)
        {
            //ErrorSignal.FromCurrentContext().Raise(ex);
            Trace.WriteLine("Failed, exception thrown: " + ex.Message);
            return stream;
        }
    }

    //  End of "INCLUDE_WEB_FUNCTIONS" section

    /// <summary>
    /// Create an Excel file, and write it to a file.
    /// </summary>
    /// <param name="ds">DataSet containing the data to be written to the Excel.</param>
    /// <param name="excelFilename">Name of file to be written.</param>
    /// <returns>True if successful, false if something went wrong.</returns>
    public static bool CreateExcelDocument(DataSet ds, string excelFilename)
    {
        try
        {
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(excelFilename, SpreadsheetDocumentType.Workbook))
            {
                WriteExcelFile(ds, document);
            }
            //XLInsertHeaderFooter(excelFilename, ds.Tables[0].TableName, "This is my header", HeaderType.FirstHeader);
            //XLInsertHeaderFooter(excelFilename, ds.Tables[0].TableName, "This is my footer", HeaderType.FirstFooter);
            Trace.WriteLine("Successfully created: " + excelFilename);
            return true;
        }
        catch (Exception ex)
        {
            //ErrorSignal.FromCurrentContext().Raise(ex);
            Trace.WriteLine("Failed, exception thrown: " + ex.Message);
            return false;
        }
    }

    private static void WriteExcelFile(DataSet ds, SpreadsheetDocument spreadsheet)
    {
        //  Create the Excel file contents.  This function is used when creating an Excel file either writing 
        //  to a file, or writing to a MemoryStream.
        spreadsheet.AddWorkbookPart();
        spreadsheet.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

        //  My thanks to James Miera for the following line of code (which prevents crashes in Excel 2010)
        spreadsheet.WorkbookPart.Workbook.Append(new BookViews(new WorkbookView()));

        //  If we don't add a "WorkbookStylesPart", OLEDB will refuse to connect to this .xlsx file !
        WorkbookStylesPart workbookStylesPart = spreadsheet.WorkbookPart.AddNewPart<WorkbookStylesPart>("rIdStyles");
        Stylesheet stylesheet = new Stylesheet();
        workbookStylesPart.Stylesheet = CreateStylesheet();

        //  Loop through each of the DataTables in our DataSet, and create a new Excel Worksheet for each.
        uint worksheetNumber = 1;
        foreach (DataTable dt in ds.Tables)
        {
            //  For each worksheet you want to create
            string workSheetID = "rId" + worksheetNumber.ToString();
            string worksheetName = dt.TableName;

            WorksheetPart newWorksheetPart = spreadsheet.WorkbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet();

            // create sheet data

            SheetViews sheetViews = new SheetViews();

            SheetView sheetView = new SheetView();
            //Selection selection = new Selection() { ActiveCell = "A1", SequenceOfReferences = new ListValue<StringValue>() { InnerText = "A1" } };
            sheetView.ShowGridLines = new BooleanValue(true);
            sheetView.WorkbookViewId = 0;
            sheetViews.Append(sheetView);
            //sheetView.Append(selection);
            newWorksheetPart.Worksheet.Append(sheetViews);
            newWorksheetPart.Worksheet.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.SheetData());

            // save worksheet
            WriteDataTableToExcelWorksheet(dt, newWorksheetPart);

            //AddHeader(newWorksheetPart.Worksheet);

            newWorksheetPart.Worksheet.Save();
            // create the worksheet to workbook relation
            if (worksheetNumber == 1)
                spreadsheet.WorkbookPart.Workbook.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheets());

            spreadsheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>().AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheet()
            {
                Id = spreadsheet.WorkbookPart.GetIdOfPart(newWorksheetPart),
                SheetId = (uint)worksheetNumber,
                Name = dt.TableName
            });

            worksheetNumber++;
        }
        spreadsheet.WorkbookPart.Workbook.Save();
        //AddHeader(spreadsheet);
        //spreadsheet.WorkbookPart.Workbook.Save();
    }

    private static Stylesheet CreateStylesheet()
    {
        Stylesheet ss = new Stylesheet();

        Fonts fts = new Fonts();
        DocumentFormat.OpenXml.Spreadsheet.Font ft = new DocumentFormat.OpenXml.Spreadsheet.Font();
        FontName ftn = new FontName();
        ftn.Val = StringValue.FromString("Calibri");
        FontSize ftsz = new FontSize();
        ftsz.Val = DoubleValue.FromDouble(11);
        ft.FontName = ftn;
        ft.FontSize = ftsz;
        fts.Append(ft);

        ft = new DocumentFormat.OpenXml.Spreadsheet.Font();
        ftn = new FontName();
        ftn.Val = StringValue.FromString("Palatino Linotype");
        ftsz = new FontSize();
        ftsz.Val = DoubleValue.FromDouble(18);
        ft.FontName = ftn;
        ft.FontSize = ftsz;
        fts.Append(ft);

        fts.Count = UInt32Value.FromUInt32((uint)fts.ChildElements.Count);

        Fills fills = new Fills();
        Fill fill;
        PatternFill patternFill;
        fill = new Fill();
        patternFill = new PatternFill();
        patternFill.PatternType = PatternValues.None;
        fill.PatternFill = patternFill;
        fills.Append(fill);

        fill = new Fill();
        patternFill = new PatternFill();
        patternFill.PatternType = PatternValues.Gray125;
        fill.PatternFill = patternFill;
        fills.Append(fill);

        fill = new Fill();
        patternFill = new PatternFill();
        patternFill.PatternType = PatternValues.Solid;
        patternFill.ForegroundColor = new ForegroundColor();
        patternFill.ForegroundColor.Rgb = HexBinaryValue.FromString("00ff9728");
        patternFill.BackgroundColor = new BackgroundColor();
        patternFill.BackgroundColor.Rgb = patternFill.ForegroundColor.Rgb;
        fill.PatternFill = patternFill;
        fills.Append(fill);

        fills.Count = UInt32Value.FromUInt32((uint)fills.ChildElements.Count);

        Borders borders = new Borders();
        Border border = new Border();
        border.LeftBorder = new LeftBorder();
        border.RightBorder = new RightBorder();
        border.TopBorder = new TopBorder();
        border.BottomBorder = new BottomBorder();
        border.DiagonalBorder = new DiagonalBorder();
        borders.Append(border);

        border = new Border();
        border.LeftBorder = new LeftBorder();
        border.LeftBorder.Style = BorderStyleValues.Thin;
        border.RightBorder = new RightBorder();
        border.RightBorder.Style = BorderStyleValues.Thin;
        border.TopBorder = new TopBorder();
        border.TopBorder.Style = BorderStyleValues.Thin;
        border.BottomBorder = new BottomBorder();
        border.BottomBorder.Style = BorderStyleValues.Thin;
        border.DiagonalBorder = new DiagonalBorder();
        borders.Append(border);
        borders.Count = UInt32Value.FromUInt32((uint)borders.ChildElements.Count);

        CellStyleFormats csfs = new CellStyleFormats();
        CellFormat cf = new CellFormat();
        cf.NumberFormatId = 0;
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 0;
        csfs.Append(cf);
        csfs.Count = UInt32Value.FromUInt32((uint)csfs.ChildElements.Count);

        uint iExcelIndex = 164;
        CellFormats cfs = new CellFormats();

        cf = new CellFormat();
        cf.NumberFormatId = 0;
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 0;
        cf.FormatId = 0;
        cfs.Append(cf);

        // index 1
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 0;
        cf.FormatId = 0;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        // index 2
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 0;
        cf.FormatId = 0;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        // index 3
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 0;
        cf.FormatId = 0;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        // index 4
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 0;
        cf.FormatId = 0;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        // index 5
        // Header text
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 1;
        cf.FormatId = 1;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        // index 6
        // column text
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 0;
        cf.BorderId = 1;
        cf.FormatId = 0;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        // index 7
        // coloured 2 decimal text
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 2;
        cf.BorderId = 0;
        cf.FormatId = 0;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        // index 8
        // coloured column text
        cf = new CellFormat();
        cf.FontId = 0;
        cf.FillId = 2;
        cf.BorderId = 1;
        cf.FormatId = 0;
        cf.ApplyNumberFormat = BooleanValue.FromBoolean(true);
        cfs.Append(cf);

        cfs.Count = UInt32Value.FromUInt32((uint)cfs.ChildElements.Count);

        ss.Append(fts);
        ss.Append(fills);
        ss.Append(borders);
        ss.Append(csfs);
        ss.Append(cfs);

        CellStyles css = new CellStyles();
        CellStyle cs = new CellStyle();
        cs.Name = StringValue.FromString("Normal");
        cs.FormatId = 0;
        cs.BuiltinId = 0;
        css.Append(cs);
        css.Count = UInt32Value.FromUInt32((uint)css.ChildElements.Count);
        ss.Append(css);

        DifferentialFormats dfs = new DifferentialFormats();
        dfs.Count = 0;
        ss.Append(dfs);

        TableStyles tss = new TableStyles();
        tss.Count = 0;
        tss.DefaultTableStyle = StringValue.FromString("TableStyleMedium9");
        tss.DefaultPivotStyle = StringValue.FromString("PivotStyleLight16");
        ss.Append(tss);

        return ss;
    }


    public static void AddHeader(SpreadsheetDocument excelDoc, String filename)
    {
        foreach (var wsp in excelDoc.WorkbookPart.WorksheetParts)
            AddHeader(wsp.Worksheet, filename);
    }

    private static void AddHeader(Worksheet sheet, string Header)
    {
        HeaderFooter headerFooter = new HeaderFooter();
        OddHeader oddHeader = new OddHeader();
        oddHeader.Text = "&C&\"Verdana,Bold\"&10 " + Header;
        OddFooter oddFooter = new OddFooter();
        oddFooter.Text = "&C&\"Algerian,Bold\"&9www.yesbank.in";

        headerFooter.Append(oddHeader);
        headerFooter.Append(oddFooter);

        sheet.Append(headerFooter);
    }

    public static void XLInsertHeaderFooter(string fileName, string sheetName, string textToInsert, HeaderType type)
    {
        using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, true))
        {
            WorkbookPart wbPart = document.WorkbookPart;

            // Find the sheet with the supplied name, and then use 
            // that Sheet object to retrieve a reference to 
            // the appropriate worksheet.
            Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
              Where(s => s.Name == sheetName).FirstOrDefault();
            if (theSheet == null)
            {
                return;
            }
            WorksheetPart wsPart =
              (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
            Worksheet ws = wsPart.Worksheet;

            // Worksheet is nothing? You have a damaged workbook!
            if (ws == null)
            {
                return;
            }

            // Retrieve a reference to the header/footer node, if it exists.
            HeaderFooter hf = ws.Descendants<HeaderFooter>().FirstOrDefault();
            if (hf == null)
            {
                hf = new HeaderFooter();
                ws.AppendChild<HeaderFooter>(hf);
            }

            // The HeaderFooter node should be there, at this point!
            if (hf != null)
            {
                // You've found the node. Now add the header or footer.
                // Deal with the attributes first:
                switch (type)
                {
                    case HeaderType.EvenHeader:
                    case HeaderType.EvenFooter:
                    case HeaderType.OddHeader:
                    case HeaderType.OddFooter:
                        // Even or odd only? Add a differentOddEven attribute and set 
                        // it to "1".
                        hf.DifferentOddEven = true;
                        break;

                    case HeaderType.FirstFooter:
                    case HeaderType.FirstHeader:
                        hf.DifferentFirst = true;
                        break;
                }

                switch (type)
                {
                    // This code creates new header elements, even if they
                    // already exist. Either way, you end up with a 
                    // "fresh" element.
                    case HeaderType.AllHeader:
                        hf.EvenHeader = new EvenHeader();
                        hf.EvenHeader.Text = textToInsert;

                        hf.OddHeader = new OddHeader();
                        hf.OddHeader.Text = textToInsert;
                        break;

                    case HeaderType.AllFooter:
                        hf.EvenFooter = new EvenFooter();
                        hf.EvenFooter.Text = textToInsert;

                        hf.OddFooter = new OddFooter();
                        hf.OddFooter.Text = textToInsert;
                        break;

                    case HeaderType.EvenFooter:
                        hf.EvenFooter = new EvenFooter();
                        hf.EvenFooter.Text = textToInsert;
                        break;

                    case HeaderType.EvenHeader:
                        hf.EvenHeader = new EvenHeader();
                        hf.EvenHeader.Text = textToInsert;
                        break;

                    case HeaderType.OddFooter:
                        hf.OddFooter = new OddFooter();
                        hf.OddFooter.Text = textToInsert;
                        break;

                    case HeaderType.OddHeader:
                        hf.OddHeader = new OddHeader();
                        hf.OddHeader.Text = textToInsert;
                        break;

                    case HeaderType.FirstHeader:
                        hf.FirstHeader = new FirstHeader();
                        hf.FirstHeader.Text = textToInsert;
                        break;

                    case HeaderType.FirstFooter:
                        hf.FirstFooter = new FirstFooter();
                        hf.FirstFooter.Text = textToInsert;
                        break;
                }
            }
            ws.Save();
        }
    }


    private static SheetData WriteDataTableToExcelWorksheet(DataTable dt, WorksheetPart worksheetPart)
    {
        var worksheet = worksheetPart.Worksheet;
        var sheetData = worksheet.GetFirstChild<SheetData>();

        worksheet.MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x14ac" } ;

        worksheet.AddNamespaceDeclaration("r", "schemas.openxmlformats.org/.../relationships");

        worksheet.AddNamespaceDeclaration("mc", "schemas.openxmlformats.org/.../2006");

        worksheet.AddNamespaceDeclaration("x14ac", "schemas.microsoft.com/.../ac");

        string cellValue = "";

        //  Create a Header Row in our Excel file, containing one header for each Column of data in our DataTable.
        //
        //  We'll also create an array, showing which type each column of data is (Text or Numeric), so when we come to write the actual
        //  cells of data, we'll know if to write Text values or Numeric cell values.
        int numberOfColumns = dt.Columns.Count;
        bool[] IsNumericColumn = new bool[numberOfColumns];

        string[] excelColumnNames = new string[numberOfColumns];
        for (int n = 0; n < numberOfColumns; n++)
            excelColumnNames[n] = GetExcelColumnName(n);

        //
        //  Create the Header row in our Excel Worksheet
        //
        uint rowIndex = 1;

        var headerRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
        sheetData.Append(headerRow);

        for (int colInx = 0; colInx < numberOfColumns; colInx++)
        {
            DataColumn col = dt.Columns[colInx];
            AppendTextCell(excelColumnNames[colInx] + "1", col.ColumnName, headerRow);
            IsNumericColumn[colInx] = (col.DataType.FullName == "System.Decimal") || (col.DataType.FullName == "System.Int32");
        }

        //
        //  Now, step through each row of data in our DataTable...
        //
        double cellNumericValue = 0;
        foreach (DataRow dr in dt.Rows)
        {
            // ...create a new row, and append a set of this row's data to it.
            ++rowIndex;
            var newExcelRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
            sheetData.Append(newExcelRow);

            for (int colInx = 0; colInx < numberOfColumns; colInx++)
            {
                cellValue = dr.ItemArray[colInx].ToString();

                // Create cell with data
                if (IsNumericColumn[colInx])
                {
                    //  For numeric cells, make sure our input data IS a number, then write it out to the Excel file.
                    //  If this numeric value is NULL, then don't write anything to the Excel file.
                    cellNumericValue = 0;
                    if (double.TryParse(cellValue, out cellNumericValue))
                    {
                        cellValue = cellNumericValue.ToString();
                        AppendNumericCell(excelColumnNames[colInx] + rowIndex.ToString(), cellValue, newExcelRow);
                    }
                }
                else
                {
                    //  For text cells, just write the input data straight out to the Excel file.
                    AppendTextCell(excelColumnNames[colInx] + rowIndex.ToString(), cellValue, newExcelRow);
                }
            }
        }
        Columns columns = AutoSize(sheetData);
        sheetData.Append(columns);
        return sheetData;
    }

    private static void AppendTextCell(string cellReference, string cellStringValue, Row excelRow)
    {
        //  Add a new Excel Cell to our Row 
        Cell cell = new Cell() { CellReference = cellReference, DataType = CellValues.InlineString };
        cell.InlineString = new InlineString() { Text = new Text(cellStringValue) }; 

        if (excelRow.RowIndex == 1)
            cell.StyleIndex = 5;
        else
            cell.StyleIndex = 6;
        excelRow.Append(cell);
    }

    private static void AppendNumericCell(string cellReference, string cellStringValue, Row excelRow)
    {
        //  Add a new Excel Cell to our Row 
        Cell cell = new Cell() { CellReference = cellReference };
        cell.DataType = CellValues.InlineString;
        cell.InlineString = new InlineString() { Text = new Text(cellStringValue) }; 
        cell.StyleIndex = 6;
        excelRow.Append(cell);
    }

    private static string GetExcelColumnName(int columnIndex)
    {
        //  Convert a zero-based column index into an Excel column reference  (A, B, C.. Y, Y, AA, AB, AC... AY, AZ, B1, B2..)
        //
        //  eg  GetExcelColumnName(0) should return "A"
        //      GetExcelColumnName(1) should return "B"
        //      GetExcelColumnName(25) should return "Z"
        //      GetExcelColumnName(26) should return "AA"
        //      GetExcelColumnName(27) should return "AB"
        //      ..etc..
        //
        if (columnIndex < 26)
            return ((char)('A' + columnIndex)).ToString();

        char firstChar = (char)('A' + (columnIndex / 26) - 1);
        char secondChar = (char)('A' + (columnIndex % 26));

        return string.Format("{0}{1}", firstChar, secondChar);
    }

    /// <summary>
    /// //////////////     Setting auto column width
    /// </summary>
    /// <param name="sheetData"></param>
    /// <returns></returns>
    private static Columns AutoSize(SheetData sheetData)
    {
        var maxColWidth = GetMaxCharacterWidth(sheetData);

        Columns columns = new Columns();
        //this is the width of my font - yours may be different
        double maxWidth = 7;
        foreach (var item in maxColWidth)
        {
            
            //width = Truncate([{Number of Characters} * {Maximum Digit Width} + {5 pixel padding}]/{Maximum Digit Width}*256)/256
            double width = Math.Truncate((item.Value * maxWidth + 5) / maxWidth * 256) / 256;

            //pixels=Truncate(((256 * {width} + Truncate(128/{Maximum Digit Width}))/256)*{Maximum Digit Width})
            double pixels = Math.Truncate(((256 * width + Math.Truncate(128 / maxWidth)) / 256) * maxWidth);

            //character width=Truncate(({pixels}-5)/{Maximum Digit Width} * 100+0.5)/100
            double charWidth = Math.Truncate((pixels - 5) / maxWidth * 100 + 0.5) / 100;

            Column col = new Column() { BestFit = true, Min = (UInt32)(item.Key + 1), Max = (UInt32)(item.Key + 1), CustomWidth = true, Width = (DoubleValue)width };
            //columnlst[item.Key] = new Column() { BestFit = true, Min = (UInt32)(item.Key + 1), Max = (UInt32)(item.Key + 1), CustomWidth = true, Width = (DoubleValue)width };
            columns.Append(col);
        }

        return columns;
    }

    private static Dictionary<int, int> GetMaxCharacterWidth(SheetData sheetData)
    {
        //iterate over all cells getting a max char value for each column
        Dictionary<int, int> maxColWidth = new Dictionary<int, int>();
        var rows = sheetData.Elements<Row>();
        UInt32[] numberStyles = new UInt32[] { 5, 6, 7, 8 }; //styles that will add extra chars
        UInt32[] boldStyles = new UInt32[] { 1, 2, 3, 4, 6, 7, 8 }; //styles that will bold
        foreach (var r in rows)
        {
            var cells = r.Elements<Cell>().ToArray();

            //using cell index as my column
            for (int i = 0; i < cells.Length; i++)
            {
                var cell = cells[i];
                var cellValue = cell.CellValue == null ? string.Empty : cell.CellValue.InnerText;
                var cellTextLength = cellValue.Length;

                if (cell.StyleIndex != null && numberStyles.Contains(cell.StyleIndex))
                {
                    int thousandCount = (int)Math.Truncate((double)cellTextLength / 4);

                    //add 3 for '.00' 
                    cellTextLength += (3 + thousandCount);
                }

                if (cell.StyleIndex != null && boldStyles.Contains(cell.StyleIndex))
                {
                    //add an extra char for bold - not 100% acurate but good enough for what i need.
                    cellTextLength += 1;
                }

                if (maxColWidth.ContainsKey(i))
                {
                    var current = maxColWidth[i];
                    if (cellTextLength > current)
                    {
                        maxColWidth[i] = cellTextLength;
                    }
                }
                else
                {
                    maxColWidth.Add(i, cellTextLength);
                }
            }
        }

        return maxColWidth;
    }

    /// <summary>
    /// end here
    /// </summary>
    /// <param name="Path"></param>
    /// <returns></returns>
    /// 
    public static DataTable ReadAsDataTable(string Path)
    {
        DataTable dataTable = new DataTable();
        string fileName = Path;
        SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false);

        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        string relationshipId = sheets.First().Id.Value;
        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        Worksheet workSheet = worksheetPart.Worksheet;
        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        IEnumerable<Row> rows = sheetData.Descendants<Row>();

        foreach (Cell cell in rows.ElementAt(0))
        {
            dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
        }

        foreach (Row row in rows)
        {
            DataRow dataRow = dataTable.NewRow();
            for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
            {
                dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
            }

            dataTable.Rows.Add(dataRow);
        }

        spreadSheetDocument.Close();
        dataTable.Rows.RemoveAt(0);

        return dataTable;
    }

    public static List<object> ReadAsList(string Path, string all)
    {
        List<object> uplst = new List<object>();

        string fileName = Path;
        SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false);

        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        string relationshipId = sheets.First().Id.Value;
        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        Worksheet workSheet = worksheetPart.Worksheet;
        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        IEnumerable<Row> rows = sheetData.Descendants<Row>();
        JObject jobj = new JObject();
        List<string> Vertical = new List<string> { "BU", "Vertical", "Function", "Bussiness Entity" };
        List<string> Department = new List<string> { "Department", "Costcenter", "Project" };
        
        foreach (Cell cell in rows.ElementAt(0))
        {
            string str = GetCellValue(spreadSheetDocument, cell);
            switch (str) {
                case "FromDate (in MM/dd/yyyy)": str = "FromDate";
                    break;
                case "ToDate (in MM/dd/yyyy)": str = "ToDate";
                    break;
                case "FromTime (in HH:MM)": str = "FromTime";
                    break;
                case "ToTime (in HH:MM)": str = "ToTime";
                    break;
                case "BU": str = "Vertical";
                    break;
                case "Vertical": str = "Vertical";
                    break;
                case "Function": str = "Vertical";
                    break;
                case "Bussiness Entity": str = "Vertical";
                    break;
                case "Department": str = "Department";
                    break;
                case "Costcenter": str = "Costcenter";
                    break;
                case "Project": str = "Costcenter";
                    break;
            }
            jobj.Add(str,str);
        }

        for (int i = 1; i < rows.Count(); i++)
        {
            JObject tempobj = JObject.FromObject(jobj);
            int j = 0;
            var celvalchkr = GetCellValue(spreadSheetDocument, rows.ElementAt(i).Descendants<Cell>().ElementAt(0));
            if (celvalchkr == null || celvalchkr == "")
                continue;

            foreach (JProperty jprp in tempobj.Properties())
            {
                jprp.Value = GetCellValue(spreadSheetDocument, rows.ElementAt(i).Descendants<Cell>().ElementAt(j));
                j++;
            }
            uplst.Add(tempobj);
        }
        spreadSheetDocument.Close();
        return uplst;
    }

    public static List<UploadAllocationDataVM> ReadAsList(string Path)
    {
        List<UploadAllocationDataVM> uplst = new List<UploadAllocationDataVM>();

        string fileName = Path;
        SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false);

        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        string relationshipId = sheets.First().Id.Value;
        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        Worksheet workSheet = worksheetPart.Worksheet;
        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        IEnumerable<Row> rows = sheetData.Descendants<Row>();
        UploadAllocationDataVM uadv;
        int MainCount = 1;
        foreach (Row row in rows)
        {
            if (MainCount != 1)
            {
                uadv = new UploadAllocationDataVM();
                uadv.City = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(0));
                uadv.Location = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(1));
                uadv.Tower = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(2));
                uadv.Floor = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(3));
                uadv.SpaceID = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(4));
                uadv.SeatType = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(5));
                uadv.Vertical = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(6));
                uadv.Costcenter = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(7));
                uadv.EmployeeID = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(8));
                uadv.FromDate = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(9));
                uadv.ToDate = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(10));
                uadv.FromTime = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(11));
                uadv.ToTime = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(12));

                DateTime dateNow = DateTime.Now;
                if (uadv.FromDate != "")
                {
                    DateTime fromDate = DateTime.TryParse(uadv.FromDate, out dateNow)
                        ? Convert.ToDateTime(uadv.FromDate)
                        : DateTime.FromOADate(Convert.ToDouble(uadv.FromDate));
                    uadv.FromDate = fromDate.ToString();
                }
                if (uadv.ToDate != "")
                {
                    DateTime ToDate = DateTime.TryParse(uadv.ToDate, out dateNow)
                        ? Convert.ToDateTime(uadv.ToDate)
                        : DateTime.FromOADate(Convert.ToDouble(uadv.ToDate));
                    uadv.ToDate = ToDate.ToString();
                }
                if (uadv.FromTime != "")
                {
                    if (uadv.FromTime != "")
                    {
                        bool IsDecimal = true;
                        decimal decimalValue = 0;

                        if (uadv.FromTime.Contains("E-"))
                        {
                            IsDecimal = Decimal.TryParse(uadv.FromTime, NumberStyles.Float, CultureInfo.InvariantCulture, out decimalValue);
                        }
                        else
                        {
                            IsDecimal = Decimal.TryParse(uadv.FromTime, out decimalValue);
                        }

                        if (IsDecimal)
                        {
                            DateTime FromTime = DateTime.FromOADate(Convert.ToDouble(decimalValue));
                            uadv.FromTime = FromTime.ToString("HH:mm");
                        }
                        else
                        {
                            uadv.FromTime = uadv.FromTime;
                        }
                    }

                    if (uadv.ToTime != "")
                    {
                        bool IsDecimal = true;
                        decimal decimalValue = 0;

                        if (uadv.ToTime.Contains("E-"))
                        {
                            IsDecimal = Decimal.TryParse(uadv.ToTime, NumberStyles.Float, CultureInfo.InvariantCulture, out decimalValue);
                        }
                        else
                        {
                            IsDecimal = Decimal.TryParse(uadv.ToTime, out decimalValue);
                        }

                        if (IsDecimal)
                        {
                            DateTime ToTime = DateTime.FromOADate(Convert.ToDouble(decimalValue));
                            uadv.ToTime = ToTime.ToString("HH:mm");
                        }
                        else
                        {
                            uadv.ToTime = uadv.ToTime;
                        }
                    }
                }
                uplst.Add(uadv);
            }
            MainCount++;
        }
        spreadSheetDocument.Close();
        return uplst;
    }

    private static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = "";
        if (cell.CellValue != null)
            value = Convert.ToString(cell.CellValue.InnerXml);
        else
            value = "";

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }
}
