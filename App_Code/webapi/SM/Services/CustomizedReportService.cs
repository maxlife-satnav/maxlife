﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class CustomizedReportService
{
    SubSonic.StoredProcedure sp;
    List<CustomizedData> Cust;
    CustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(CustomizedDetails Det)
    {
        try
        {
            Cust = GetCustomizedDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<CustomizedData> GetCustomizedDetails(CustomizedDetails Details)
    {
        try
        {
            List<CustomizedData> CData = new List<CustomizedData>();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Details.Request_Type;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.flrlst);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_CUSTOMIZED_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new CustomizedData();
                    Custm.COUNTRY = Convert.ToString(reader["COUNTRY"]);
                    Custm.ZONE = Convert.ToString(reader["ZONE"]);
                    Custm.STATE = Convert.ToString(reader["STATE"]);
                    Custm.CITY = Convert.ToString(reader["CITY"]);
                    Custm.LOCATION = Convert.ToString(reader["LOCATION"]);
                    Custm.TOWER = Convert.ToString(reader["TOWER"]);
                    Custm.FLOOR = Convert.ToString(reader["FLOOR"]);
                    Custm.SPACE = Convert.ToString(reader["SPACE"]);
                    Custm.AREA = Convert.ToDouble(reader["AREA"]);
                    Custm.STATUS = Convert.ToString(reader["STATUS"]);
                    Custm.VERTICAL = Convert.ToString(reader["VERTICAL"]);
                    Custm.COSTCENTER = Convert.ToString(reader["COSTCENTER"]);
                    Custm.EMP_ID = Convert.ToString(reader["EMP_ID"]);
                    Custm.EMP_NAME = Convert.ToString(reader["EMP_NAME"]);
                    Custm.MAIL = Convert.ToString(reader["MAIL"]);
                    Custm.REPORTING = Convert.ToString(reader["REPORTING"]);
                    Custm.REPORTING_NAME = Convert.ToString(reader["REPORTING_NAME"]);
                    Custm.AUR_TYPE = Convert.ToString(reader["AUR_TYPE"]);
                    Custm.AUR_GRADE = Convert.ToString(reader["AUR_GRADE"]);
                    Custm.SPC_SEATING_CAPACITY = Convert.ToString(reader["SPC_SEATING_CAPACITY"]);
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}