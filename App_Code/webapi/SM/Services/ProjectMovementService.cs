﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using UtiltiyVM;

/// <summary>
/// Summary description for ProjectMovementService
/// </summary>
public class ProjectMovementService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    GetSpaceDetails GetSpaceDetails;
    CountData CountData;
    List<GetSpaceDetails> GetSpaceDetailslst;
    List<CountData> CountDatalst;

    public object Search(ProjectMovementModel spcdet)
    {
        GetSpaceDetailslst = GetVacantSpacesList(spcdet);
        if (GetSpaceDetailslst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = GetSpaceDetailslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<GetSpaceDetails> GetVacantSpacesList(ProjectMovementModel Prjspcdet)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@COST_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Prjspcdet.Costcenterlst);
        param[1] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(Prjspcdet.Locationlst);
        GetSpaceDetailslst = new List<GetSpaceDetails>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_SPACES_BY_ALLOCATED_COSTCENTER_AND_LOCATION", param))
        {
            while (sdr.Read())
            {
                GetSpaceDetails = new GetSpaceDetails();
                GetSpaceDetails.SSA_SPC_ID = sdr["SSA_SPC_ID"].ToString();
                GetSpaceDetails.SSA_VER_CODE = sdr["SSA_VER_CODE"].ToString();
                GetSpaceDetails.VER_NAME = sdr["VER_NAME"].ToString();
                GetSpaceDetails.SSAD_COST_CENTER = sdr["SSAD_COST_CENTER"].ToString();
                GetSpaceDetails.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                GetSpaceDetails.SSAD_AUR_ID = sdr["SSAD_AUR_ID"].ToString();
                GetSpaceDetails.SSA_LCM_CODE = sdr["SSA_LCM_CODE"].ToString();
                GetSpaceDetails.LCM_NAME = sdr["LCM_NAME"].ToString();
                GetSpaceDetails.SSA_TWR_CODE = sdr["SSA_TWR_CODE"].ToString();
                GetSpaceDetails.TWR_NAME = sdr["TWR_NAME"].ToString();
                GetSpaceDetails.SSA_FLR_CODE = sdr["SSA_FLR_CODE"].ToString();
                GetSpaceDetails.SSA_SPC_SUB_TYPE = sdr["SSA_SPC_SUB_TYPE"].ToString();
                GetSpaceDetails.SST_NAME = sdr["SST_NAME"].ToString();
                GetSpaceDetailslst.Add(GetSpaceDetails);
            }
        }
        return GetSpaceDetailslst;
    }
    public List<CountData> ViewPossibilities(ProjectMovementModel spcdet)
    {
        List<CountData> verlst = new List<CountData>();


        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        if (spcdet.Floorlst == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = UtilityService.ConvertToDataTable(spcdet.Floorlst);
        }

        param[1] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(spcdet.Locationlst);
        param[2] = new SqlParameter("@COSTLST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(spcdet.Costcenterlst);
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];

        CountDatalst = new List<CountData>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_POSSIBLE_SEATS_BY_COSTCENTER", param))
        {
            while (sdr.Read())
            {
                CountData = new CountData();
                CountData.SPC_FLR_ID = sdr["SPC_FLR_ID"].ToString();
                CountData.VER_NAME = sdr["VER_NAME"].ToString();
                CountData.COST_CENTER_NAME = sdr["COST_CENTER_NAME"].ToString();
                CountData.WORK_STATION = sdr["WORKSTATION"].ToString();
                CountData.CUBICLE = sdr["CUBICLE"].ToString();
                CountData.CABIN = sdr["CABIN"].ToString();
                CountDatalst.Add(CountData);
            }
        }
        return CountDatalst;
    }
}