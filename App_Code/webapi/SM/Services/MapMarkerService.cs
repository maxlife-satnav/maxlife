﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for MapMarkerService
/// </summary>
public class MapMarkerService
{
    LTLNG LNT;
    public object GetZonewiseLoc()
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ZONE_WISE_LCM");
        ds = sp.GetDataSet();
        return ds.Tables[0];

    }
    public object GetStatewiseLoc()
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ZONE_WISE_LCM");
        ds = sp.GetDataSet();
        return ds.Tables[0];

    }

    public object GetMarkers()
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_GOOGLE_MAP_MARKERS");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public List<LTLNG> GetLatLongs(Origins Org)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Org.GoogleLocationList);
        List<LTLNG> GoogleOrg = new List<LTLNG>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GOOGLE_MAP_ORIGINS", param))
        {
            while (sdr.Read())
            {
                LNT = new LTLNG();
                LNT.LAT = sdr["LAT"].ToString();
                LNT.LONG = sdr["LONG"].ToString();
                LNT.LCM_CODE = sdr["LCM_CODE"].ToString();
                GoogleOrg.Add(LNT);
            }
            sdr.Close();
        }
        return GoogleOrg;
    }

    public object GetLocations(City cty)
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_TOWERBY_CITY");
        sp.Command.AddParameter("@LCM_CODE", cty.CTY_CODE, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
}