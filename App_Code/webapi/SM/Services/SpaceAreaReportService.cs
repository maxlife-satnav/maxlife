﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class SpaceAreaReportService
{
    SubSonic.StoredProcedure sp;
    List<AreaReport> rptByUserlst;

    DataSet ds;
    public object GetReportByUserObject()
    {
        try
        {
            rptByUserlst = GetAreaReportByUserList();

            if (rptByUserlst.Count!=0)

                return new { Message = MessagesVM.UM_NO_REC, data = rptByUserlst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<AreaReport> GetAreaReportByUserList()
    {
        try
        {
            List<AreaReport> rptByUserlst = new List<AreaReport>();
            AreaReport rptByUser;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPC_AREA_REPORT");
            sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["UID"], DbType.String);


            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    rptByUser = new AreaReport();
                    rptByUser.CNY_NAME = sdr["CNY_NAME"].ToString();
                    rptByUser.CTY_NAME = sdr["CTY_NAME"].ToString();
                    rptByUser.ZN_NAME = sdr["ZN_NAME"].ToString();
                    rptByUser.STE_NAME = sdr["STE_NAME"].ToString();
                    rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                    rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                    rptByUser.FLR_NAME = sdr["FLR_NAME"].ToString();
                    rptByUser.FLR_AREA = (int)sdr["FLR_AREA"];
                    rptByUser.CARPET_AREA = (int)sdr["CARPET_AREA"];
                    rptByUser.TILE_AREA = (int)sdr["TILE_AREA"];

                    rptByUser.WORK_AREA = (int)sdr["WORK_AREA"];
                    rptByUser.FLR_CIRCULATION_AREA = (int)sdr["FLR_CIRCULATION_AREA"];
                    rptByUser.SUPPORT_AREA = (int)sdr["SUPPORT_AREA"];
                    rptByUser.UTILITY_AREA = (int)sdr["UTILITY_AREA"];
                    rptByUser.FLR_COMMON_AREA = (int)sdr["FLR_COMMON_AREA"];
                    rptByUser.FLR_LEASABLE_AREA = (int)sdr["FLR_LEASABLE_AREA"];

                    rptByUser.TOTAL_SPACES = (int)sdr["TOTAL_SPACES"];
                    rptByUserlst.Add(rptByUser);
                }
                sdr.Close();
            }
            if (rptByUserlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
                return rptByUserlst;
            else
                return null;
        }
        catch
        {
            throw;
        }

    }


    public object GetAreaChart()
    {
        try
        {
            List<SpaceData> area = new List<SpaceData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_AREA_BY_LOCATIONS");
            //sp.Command.AddParameter("@FROM_DATE", Spcdetails.FromDate, DbType.DateTime);
            //sp.Command.AddParameter("@TO_DATE", Spcdetails.ToDate, DbType.DateTime);
            //sp.Command.AddParameter("@REQTYP", Spcdetails.Request_Type, DbType.String);
            sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }

        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}