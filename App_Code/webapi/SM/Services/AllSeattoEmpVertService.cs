﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AllSeattoEmpVertService
/// </summary>
public class AllSeattoEmpVertService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    string REQID;

    //Vertical/Cost center labels
    public string[] GetlblVerticalCC()
    {
        string[] sessvals = new string[2];
        sessvals[0] = Convert.ToString(HttpContext.Current.Session["Parent"]);
        sessvals[1] = Convert.ToString(HttpContext.Current.Session["Child"]);
        return sessvals;
    }

    //ddlshift binding
    public DataTable GetURL(Param parameter)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "USP_GETACTIVESHIFTSBYFLR_ID");
        sp.Command.Parameters.Add("@FLR_ID", parameter.URL, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    // get shift timings
    public DataTable GetShiftTimings(AllSeatToEmpVertModel.GetShifttimes shiftparam)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "USP_GETACTIVESHIFTTIMINGS_BYFLR_ID_SM");
        sp.Command.Parameters.Add("@FLR_ID", shiftparam.flr_id, DbType.String);
        sp.Command.Parameters.Add("@SHIFT_ID", shiftparam.shift_id, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //get spacetype and Binding gvSharedEmployee/gvAllocatedSeats
    int spacetypeval;
    public DataSet GetSpaceType(string parameter)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETSPACETYPE_SHARED_SHAREDALLOCATED_API");
        sp.Command.Parameters.Add("@SPC_ID", parameter, DbType.String);
        ds = sp.GetDataSet();
        spacetypeval = Convert.ToInt32(ds.Tables[0].Rows[0]["SPACE_TYPE"]);

        return ds;
    }

    //load() functinality in aspx.cs, Binding gdavail 
    public DataTable GetSeattoAllocateOccupied(AllSeatToEmpVertModel.GetSeattoAllOcc gstao)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "USP_GET_LOCATION_SEATS_PART1_UPDATED_SM");
        sp.Command.Parameters.Add("@LCM_CODE", gstao.location == null ? "" : gstao.location, DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", gstao.floor == null ? "" : gstao.floor, DbType.String);
        sp.Command.Parameters.Add("@FDATE", gstao.fromdate == null ? DateTime.Now : gstao.fromdate, DbType.DateTime);
        sp.Command.Parameters.Add("@TDATE", gstao.todate == null ? DateTime.Now : gstao.todate, DbType.DateTime);
        sp.Command.Parameters.Add("@FTIME", gstao.fromtime == null ? DateTime.Now : gstao.fromtime, DbType.DateTime);
        sp.Command.Parameters.Add("@TTIME", gstao.totime == null ? DateTime.Now : gstao.totime, DbType.DateTime);
        sp.Command.Parameters.Add("@SHARED", gstao.spacetype == null ? "" : gstao.spacetype, DbType.String);
        sp.Command.Parameters.Add("@MODE", gstao.mode, DbType.Int16);
        sp.Command.Parameters.Add("@SPC_ID", gstao.spaceid, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //ddlvertical binding
    public DataTable GetVerticals()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_ALL_VERTICALS");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //btnallocate check emp details tab - 2 Binding
    public DataTable GetEmpDetails(Employee emp)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETEMPLOYEEDETAILS");
        sp.Command.Parameters.Add("@AUR_ID", emp.EmployeeID, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //Allocate to Vertical btn functionality
    public string SubmitAllocationOccupied(string Strfield)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GENERATE_VERTCAL_REQID");
        ds = sp.GetDataSet();

        string TmpReqseqid;
        string tmpSSAREQID;
        int Reqseqid = Convert.ToInt32(ds.Tables[0].Rows[0]["REQID"]);
        int SSAREQID = Convert.ToInt32(ds.Tables[1].Rows[0]["SSAREQID"]);
        //REQID 384
            TmpReqseqid = SetDatetimeOffset.GetOffSetDateTime(DateTime.Now).Year + "/" + Strfield +"/" +Reqseqid.ToString().PadLeft(7, '0');
            tmpSSAREQID = SetDatetimeOffset.GetOffSetDateTime(DateTime.Now).Year + "/" + Strfield + "/" + SSAREQID.ToString().PadLeft(7, '0');
        //verticalreqid 527

        return TmpReqseqid + "@" + SetDatetimeOffset.GetOffSetDateTime(DateTime.Now) + "@" + Convert.ToString(HttpContext.Current.Session["uid"] + "@" + tmpSSAREQID);
    }

    //SubmitAllocationOccupied('','','') in aspx.cs
    public int BlockSeatsRequestPart1_Updated(AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "ALLOCATE_SEAT_EMP_VERTICAL_API");
            sp.Command.Parameters.Add("@REQID", obj.reqid, DbType.String);
            sp.Command.Parameters.Add("@VERTICAL", obj.VerticalCode, DbType.String);
            sp.Command.Parameters.Add("@WSTREQCOUNT", obj.cntWst, DbType.Int32);
            sp.Command.Parameters.Add("@FCREQCOUNT", obj.cntFCB, DbType.Int32);
            sp.Command.Parameters.Add("@HCREQCOUNT", obj.cntHCB, DbType.Int32);
            sp.Command.Parameters.Add("@FROMDATE", obj.fromdate, DbType.DateTime);
            sp.Command.Parameters.Add("@TODATE", obj.todate, DbType.DateTime);
            sp.Command.Parameters.Add("@FROMTIME", obj.fromtime, DbType.DateTime);
            sp.Command.Parameters.Add("@TOTIME", obj.totime, DbType.DateTime);
            sp.Command.Parameters.Add("@EMPID", obj.AurId, DbType.String);
            sp.Command.Parameters.Add("@BDGID", obj.BDGID, DbType.String);
            sp.Command.Parameters.Add("@CITY", obj.city == null ? "" : obj.city, DbType.String);
            sp.Command.Parameters.Add("@SPACETYPE", obj.spacetype, DbType.String);
            sp.Command.Parameters.Add("@BCPTYPE", "2", DbType.Int32);
            sp.Command.Parameters.Add("@STATUSID", obj.Statusid, DbType.String);
            sp.Command.Parameters.Add("@spc_id", obj.spaceid, DbType.String);
            sp.Command.Parameters.Add("@costcenter", obj.Statusid == "6" ? "" : obj.costcenter, DbType.String);
            sp.Command.Parameters.Add("@TEMPREQID", obj.TmpReqseqid, DbType.String);
            sp.Command.Parameters.Add("@SHIFTID", obj.shift, DbType.String);

            int cnt = Convert.ToInt32(sp.ExecuteScalar());

            if (obj.spacetype != "2")
            {
                UpdateRecord(obj.spaceid, obj.Statusid, obj.AurId + "/" + obj.DeptName);
            }
            else
            {
                if (cnt == 1)
                {
                    UpdateRecord(obj.spaceid, "11", obj.AurId + "/" + obj.DeptName);           //completely allocated        
                }
                else
                {
                    UpdateRecord(obj.spaceid, "10", obj.AurId + "/" + obj.DeptName);              //partially allocated     
                }
            }
        }
        catch (SqlException ex)
        {
            Console.WriteLine("SQL Error" + ex.Message.ToString());
        }
        return 0;
    }


    

    ////Allocate to employee btn
    public void VacantEmpToAllocateNewSeat(AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "VACANTSEAT_BYEMPIDExisting");
        sp.Command.AddParameter("@AUR_ID", obj.AurId, DbType.String);
        sp.ExecuteScalar();

        UpdateRecord(obj.spaceid == null ? "" : obj.spaceid, "1", obj.AurId + "/" + obj.DeptName);
    }

    //shared Employee Release
    public void SharedEmployeeSpaceRelease(AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "VACANTSEAT_BYEMPID");
        sp.Command.Parameters.Add("@VC_SPACEID", obj.spaceid, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", obj.AurId, DbType.String);
        sp.ExecuteScalar();

        releasecommon(obj.spacetype, obj.spaceid, obj.AurId, obj.DeptName);

    }

    public void releasecommon(string spctype, string spaceid, string aurid, string dept)
    {
        int spcstatus = 1;
        if (spctype == "1")
        {
            if (GETALLOCATIONCOUNT(spaceid) == 0)
            {
                spcstatus = 1;
            }
        }
        else if (spctype == "2")
        {
            if (GETALLOCATIONCOUNT(spaceid) > 0)
            {

                int count = Shift_SeatRequsitioncnt(spaceid);
                if (count == 1)
                {
                    spcstatus = 11;  // completely allocated
                }
                else if (count == -1)
                {
                    spcstatus = 1; //vacant
                }
                else
                {
                    spcstatus = 10; // partially allocated
                }
            }
            else
            {
                spcstatus = 1;
            }
        }
        string Aurcode = aurid == null ? "" : aurid;
        string deptcode = dept == null ? "" : dept;

        UpdateRecord(spaceid, spcstatus.ToString(), Aurcode + "/" + deptcode);

        //Trim(empcode) & "/" & lblDepartment.Text
        //Response.Redirect("AllocateSeatToEmployeeVertical.aspx?id=" & Request.QueryString("id"))
    }

    //Allocated Seats Release
    public void AllocatedSeatsRelease(AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {

        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "RELEASE_VERTICAL_SPACEID");
        sp.Command.Parameters.Add("@VERTICAL", obj.VerticalCode, DbType.String);
        sp.Command.Parameters.Add("@SPCID", obj.spaceid, DbType.String);
        sp.Command.Parameters.Add("@FROMDATE", obj.fromdate, DbType.DateTime);
        sp.Command.Parameters.Add("@TODATE", obj.todate, DbType.DateTime);
        sp.Command.Parameters.Add("@FROMTIME", obj.fromtime, DbType.DateTime);
        sp.Command.Parameters.Add("@TOTIME", obj.totime, DbType.DateTime);
        sp.Command.Parameters.Add("@REQ_ID", obj.reqid, DbType.String);
        sp.ExecuteScalar();

        releasecommon(obj.spacetype, obj.spaceid, obj.AurId, obj.DeptName);
    }

    public int Shift_SeatRequsitioncnt(string spid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "Shift_SeatRequsition");
        sp.Command.Parameters.Add("@spc_id", spid, DbType.String);
        ds = sp.GetDataSet();
        return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
    }

    public int GETALLOCATIONCOUNT(string spid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETALLOCATIONCOUNT");
        sp.Command.Parameters.Add("@SPC_ID", spid, DbType.String);
        ds = sp.GetDataSet();

        if (ds.Tables[0].Rows.Count > 0)
        {
            return Convert.ToInt32(ds.Tables[0].Rows[0]["cnt"]);
        }
        return 0;
    }

    public DataTable GetgvAssets(string id)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_ASSET_DETAILS");
        sp.Command.Parameters.Add("@EMP_ID", id, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //Auto complete
    public List<string> SearchCustomers(AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "getsearchEmpName");
        sp.Command.Parameters.Add("@EmpName", obj.AurId, DbType.String);
        sp.Command.Parameters.Add("@VERTICAL", obj.VerticalCode == null ? "" : obj.VerticalCode, DbType.String);
        ds = sp.GetDataSet();
        List<string> customers = new List<string>();        
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            customers.Add(dr["EMP_NAME"].ToString());
        }
        return customers;        
        //return ds.Tables[0];
    }

    public void UpdateRecord(string SPC_ID, string STATUS, string UserNameToolTip)
    {
        Npgsql.NpgsqlParameter[] param = new Npgsql.NpgsqlParameter[4];
        param[0] = new Npgsql.NpgsqlParameter("@SPC_ID", NpgsqlTypes.NpgsqlDbType.Text, 200);
        param[0].Value = SPC_ID;
        param[1] = new Npgsql.NpgsqlParameter("@STATUS", NpgsqlTypes.NpgsqlDbType.Text, 200);
        param[1].Value = STATUS;
        param[2] = new Npgsql.NpgsqlParameter("@displayname", NpgsqlTypes.NpgsqlDbType.Text, 200);
        param[2].Value = UserNameToolTip;
        param[3] = new Npgsql.NpgsqlParameter("@tenantid", NpgsqlTypes.NpgsqlDbType.Text, 200);
        param[3].Value = System.Web.HttpContext.Current.Session["Tenant"];
        PostGresExecute("update_floor_maps", param);
    }

    public static object PostGresExecute(string SP_Name, NpgsqlParameter[] param)
    {
        //Create the objects we need to insert a new record
        NpgsqlConnection con = new NpgsqlConnection(GetConnectionString("OLEDBConnectionString"));
        NpgsqlCommand cmdUpdate = new NpgsqlCommand();
        con.Open();
        //****************** Dataset with Stored Procedure in Postgresql **********************************
        //Start a transaction as it is required to work with result sets (cursors) in PostgreSQL
        NpgsqlTransaction tran = con.BeginTransaction();
        // Define a command to call show_cities() procedure
        NpgsqlCommand command = new NpgsqlCommand(SP_Name, con);
        command.CommandType = CommandType.StoredProcedure;
        for (int i = 0; i <= param.Length - 1; i++)
        {
            command.Parameters.Add(param[i].ParameterName, param[i].Value);
        }
        command.ExecuteNonQuery();
        tran.Commit();
        con.Close();
        return 0;
    }

    public static string GetConnectionString(string conName)
    {
        //variable to hold our connection string for returning it
        string strReturn = "";
        //check to see if the user provided a connection string name
        //this is for if your application has more than one connection string
        if (!string.IsNullOrEmpty(conName))
        {
            //a connection string name was provided
            //get the connection string by the name provided
            strReturn = ConfigurationManager.ConnectionStrings[conName].ConnectionString;
        }
        else
        {
            //no connection string name was provided
            //get the default connection string
            strReturn = ConfigurationManager.ConnectionStrings["OLEDBConnectionString"].ConnectionString;
        }
        //return the connection string to the calling method
        return strReturn;
    }

    
}