﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpaceAllocationReportService
{
    SubSonic.StoredProcedure sp;
    SpaceAllocationData SpcAlloc;
    DataSet ds;

    public List<SpaceAllocationData> GetAllocationDetails(SpaceAllocationDetials spd)
    {
        try
        {
            List<SpaceAllocationData> AllocList = new List<SpaceAllocationData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_ALLOCATION_REPORT");
            sp.Command.AddParameter("@FROM_DATE", spd.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", spd.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    SpcAlloc = new SpaceAllocationData();
                    SpcAlloc.CNY_NAME = reader["CNY_NAME"].ToString();
                    SpcAlloc.ZN_NAME = reader["ZN_NAME"].ToString();
                    SpcAlloc.STE_NAME = reader["STE_NAME"].ToString();
                    SpcAlloc.CTY_NAME = reader["CTY_NAME"].ToString();
                    SpcAlloc.LCM_NAME = reader["LCM_NAME"].ToString();
                    SpcAlloc.TWR_NAME = reader["TWR_NAME"].ToString();
                    SpcAlloc.ENTITY = reader["ENTITY"].ToString();
                    SpcAlloc.CHILD_ENTITY = reader["CHILD_ENTITY"].ToString();
                    SpcAlloc.FLR_NAME = reader["FLR_NAME"].ToString();
                    SpcAlloc.VER_NAME = reader["VER_NAME"].ToString();
                    SpcAlloc.DEP_NAME = reader["DEP_NAME"].ToString();
                    SpcAlloc.SHIFT = reader["SHIFT"].ToString();
                    SpcAlloc.ALLOC_COUNT = (int)reader["ALLOC_COUNT"];
                    SpcAlloc.OCCUP_COUNT = (int)reader["OCCUP_COUNT"];
                    SpcAlloc.VAC_COUNT = (int)reader["VAC_COUNT"];
                    SpcAlloc.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                    AllocList.Add(SpcAlloc);
                }
                reader.Close();
            }
            if (AllocList.Count != 0)
                return AllocList;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }

    //Vertical & Dept Wise Allocated Count
    public object GetChartCountData(SpaceReportDetails AllocDetails)
    {
        try
        {
            List<SpaceAllocationData> Spcdata = new List<SpaceAllocationData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_ALLOCATION_COUNT");
            sp.Command.AddParameter("@FROM_DATE", AllocDetails.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", AllocDetails.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            UtilityService userv = new UtilityService();
            DataTable dt = userv.GetInversedDataTable(ds.Tables[0], "LCM_NAME");
            var columns = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
            return new { Message = "", data = new { Details = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), Columnnames = columns } };

        }
        catch {
            throw;
        }
    }
    

}