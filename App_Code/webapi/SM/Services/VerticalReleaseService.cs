﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;


/// <summary>
/// Summary description for VerticalReleaseService
/// </summary>
public class VerticalReleaseService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object GetSpaceAllocationsForVerticalRelease(VerticalReleaseDetails vrvm)
    {
        try 
        { 
        List<VerticalReleaseMaster> VRMDETLST = new List<VerticalReleaseMaster>(); 
        VerticalReleaseMaster VRMDET;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(vrvm.selectedFloors);
        param[1] = new SqlParameter("@VERLST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(vrvm.selectedVerticals);
        param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["UID"];
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SPACE_TO_RELEASE_VERTICAL", param))
        {
            while (sdr.Read())
            {
                VRMDET = new VerticalReleaseMaster();
                VRMDET.SSA_STA_ID = sdr["SSA_STA_ID"].ToString();
                VRMDET.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                VRMDET.SSA_SRNCC_ID = sdr["SSA_SRNCC_ID"].ToString();
                VRMDET.SSA_VER_CODE = sdr["SSA_VER_CODE"].ToString();
                VRMDET.TWR_NAME = sdr["TWR_NAME"].ToString();
                VRMDET.FLR_NAME = sdr["FLR_NAME"].ToString();
                VRMDET.VER_NAME = sdr["VER_NAME"].ToString();
                VRMDET.SSA_SPC_ID = sdr["SSA_SPC_ID"].ToString();
                VRMDET.SSA_FROM_DATE = Convert.ToDateTime(sdr["SSA_FROM_DATE"]);
                VRMDET.SSA_TO_DATE = Convert.ToDateTime(sdr["SSA_TO_DATE"]);
                VRMDET.ticked = false;
                VRMDET.SSA_SPC_TYPE = sdr["SSA_SPC_TYPE"].ToString();
                VRMDET.SSA_SPC_SUB_TYPE = sdr["SSA_SPC_SUB_TYPE"].ToString();
                VRMDET.SPC_NAME = sdr["SPC_NAME"].ToString();
                VRMDET.SPC_FLR_ID = sdr["SPC_FLR_ID"].ToString();
                VRMDET.lat = sdr["lat"].ToString();
                VRMDET.lon = sdr["lon"].ToString();
                VRMDETLST.Add(VRMDET);
            }
            sdr.Close();
        }
   
        if (VRMDETLST.Count != 0)
            return new { Message = MessagesVM.VR_OK, data = VRMDETLST };
        else
            return new { Message = MessagesVM.VR_NO_REC, data = (object)null };
        }
        catch(Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
       
    }

    public object UpdateSSAAndInsertVerticalRelease(VerticalReleaseVM updtSSAAndVR)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = UtilityService.ConvertToDataTable(updtSSAAndVR.VR);
            List<VerticalReleaseMaster> VRMDETLST = updtSSAAndVR.VR.ToList();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@VRTRLSLST", SqlDbType.Structured);
            param[0].Value = dt;
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@SVRL_REL_REM", SqlDbType.NVarChar);
            param[2].Value = updtSSAAndVR.SVRL_REL_REM;
            dt = CheckSpaceIDsBeforeRelease(dt);
            if (dt.Rows.Count == 0)
            {
                using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_UPDATE_SSA_AND_INSERT_SVRL", param))
                {
                    if (dr.Read())
                    {
                        SendMail(dr["SPC_COUNT"].ToString(), dr["SVRL_REQ_ID"].ToString(), dr["SVRL_VER_CODE"].ToString());
                    }

                    return new { Message = MessagesVM.VR_SUCCESS + " For : " + dr["SVRL_REQ_ID"].ToString(), data = VRMDETLST, STATUS = "SUCCESS" };
                }
            }
            else
                return new { Message = MessagesVM.VR_FAILURE, data = dt, STATUS = "FAIL" };
        }
        catch(Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
      
    }

    public DataTable CheckSpaceIDsBeforeRelease(DataTable dtTable)
    {
        try
        {
            DataTable dt = dtTable;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@VRTRLSLST", SqlDbType.Structured);
            param[0].Value = dtTable;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_CHECK_SPACE_IDS_BEFORE_RELEASE", param);
            dt = ds.Tables[0];
            return dt;
        }
        catch 
        {
            throw;
        }
    }

    public void SendMail(string SPC_COUNT,string REQUEST_ID, string VER_NAME)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_VERTICAL_RELEASE");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@VER_NAME", VER_NAME, DbType.String);
            sp.Command.AddParameter("@SPC_COUNT", SPC_COUNT, DbType.Int32);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }
}

