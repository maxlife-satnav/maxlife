﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;


public class MaploaderService
{
    SubSonic.StoredProcedure sp;

    DownloadMap DM;
    List<DownloadMap> DML;
    
    public List<DownloadMap> DownloadAllMapsLst() {
        List<DownloadMap> DMLst = new List<DownloadMap>();

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GETUSERFLOORLIST", param))
        {
            while (reader.Read())
            {
                DM = new DownloadMap();
                DM.CTY_NAME = reader["CTY_NAME"].ToString();
                DM.LCM_NAME = reader["LCM_NAME"].ToString();
                DM.TWR_NAME = reader["TWR_NAME"].ToString();
                DM.FLR_NAME = reader["FLR_NAME"].ToString();
                DM.STE_NAME = reader["STE_NAME"].ToString();
                DM.ZN_NAME = reader["ZN_NAME"].ToString();
                DM.FLR_VAL_BY = Convert.ToString(reader["FLR_VAL_BY"]);
                DM.FLR_VAL_DT = Convert.ToString(reader["FLR_VAL_DT"]);
                DMLst.Add(DM);
            }
            reader.Close();
        }
        return DMLst;
    }
    public object GetFloorLst()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GETUSERFLOORLIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetMapItems(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        ds = sp.GetDataSet();
        Hashtable sendData = new Hashtable();

        foreach (DataRow drIn in ds.Tables[1].Rows)
        {
            sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
        }
        return new { mapDetails = ds.Tables[0], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
    }

    public object GetMarkers(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_MARKER_ALL");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    // built up area details
    public object GetLegendsCount(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@FLAG", svm.Item, DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }

    public object GetAllocDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ALLOC_REPORT_MAP");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }

    public object GetLegendsSummary(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP_LEGEND");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        IDataReader dr = sp.GetReader();
        LEGEND_SUMMARY ls = new LEGEND_SUMMARY();
        if (dr.Read())
        {
            ls.ALLOCATEDVER = (int)dr["ALLOCATEDVER"];
            ls.ALLOCATEDCST = (int)dr["ALLOCATEDCST"];
            ls.TOTAL = (int)dr["TOTAL"];
            ls.VACANT = (int)dr["VACANT"];
            ls.OCCUPIED = (int)dr["OCCUPIED"];
            ls.OVERLOAD = (int)dr["OVERLOAD"];
        }
        return ls;
    }

    public object GetSpaceDetailsBySPCID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYSPCID");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.CatValue, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbyItem()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_ITEM_SM");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbySubItem(Space_mapVM param)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_SUBITEM_SM");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEMID", param.Item, DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetSpaceDetailsBySUBITEM(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DATA_MAP_SM");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@SUB_ITEM", svm.subitem, DbType.String);
        ds = sp.GetDataSet();

        if (svm.Item == "login")
        {

            //ActiveDirectoryHelper adhelper = new ActiveDirectoryHelper();
            //List<ADUserDetail> useradlist = adhelper.GetUserslist();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AD_DATA_LIVE");
            DataSet DS1 = sp.GetDataSet();

            List<AD_DATA_VM> useradlist = DS1.Tables[0].AsEnumerable()
                  .Select(row => new AD_DATA_VM
                  {
                      USR_ID = row.Field<string>(0),
                      LOG_IN_TIME = row.Field<string>(1),
                      LOG_OUT_TIME = row.Field<string>(2)
                  }).ToList();

            List<Space_MapEmployeeVM> empmaplist = ds.Tables[0].AsEnumerable()
              .Select(row => new Space_MapEmployeeVM
              {
                  SPC_ID = row.Field<string>(0),
                  x = row.Field<double>(1).ToString(),
                  y = row.Field<double>(2).ToString(),
                  SPC_FLR_ID = row.Field<string>(3),
                  SPC_LAYER = row.Field<string>(4),
                  SPC_DESC = row.Field<string>(5),
                  STATUS = row.Field<int>(6).ToString(),
                  AD_ID = row.Field<string>(7)
              }).ToList();

            var list3 = (from Item1 in empmaplist
                         join Item2 in useradlist
                         on Item1.AD_ID equals Item2.USR_ID // join on some property
                         into grouping
                         from Item2 in grouping.DefaultIfEmpty()
                         //where Item2 != null
                         select new { Item1, Item2 }).ToList();
            return list3;
        }

        return ds;
    }

    public object GetSpaceDetailsByREQID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYREQID");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.category, DbType.String);
        sp.Command.Parameters.Add("@REQ_ID", svm.subitem, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object ReleaseSelectedseat(SPACE_REL_DETAILS reldet)
    {
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@REL_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(reldet.sad);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@RELTYPE", SqlDbType.NVarChar);
        param[2].Value = reldet.reltype;

        int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_SPACE_REL_MAP", param);

        if (RETVAL == 1)
            return new { Message = MessagesVM.UM_OK, data = reldet.reltype };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };


    }

    public object GetEmpDetails(Space_mapVM svm)
    {
        if (svm.subitem != null && svm.subitem != "")
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_EMP_DETAILS_FLRID");
            sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
            sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
            sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
            sp.Command.Parameters.Add("@VER_CODE", svm.Item, DbType.String);
            sp.Command.Parameters.Add("@COST_CENTER_CODE", svm.subitem, DbType.String);
            ds = sp.GetDataSet();
            return new { Message = MessagesVM.SelectCST, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.SelectCST, data = (object)null };
    }

    public object GetAllocEmpDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_ALLOC_EMP_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object AllocateSeats(List<SPACE_ALLOC_DETAILS> allocDetLst)
    {
        var details = allocDetLst.Where(x => x.STACHECK == (int)RequestState.Modified).ToList();

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];

        List<SPACE_ALLOC_DETAILS> spcdet = new List<SPACE_ALLOC_DETAILS>();
        SPACE_ALLOC_DETAILS spc;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP", param))
        {
            while (sdr.Read())
            {
                spc = new SPACE_ALLOC_DETAILS();
                spc.AUR_ID = sdr["AUR_ID"].ToString();
                spc.VERTICAL = sdr["VERTICAL"].ToString();
                spc.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                spc.FROM_DATE = Convert.ToDateTime(sdr["FROM_DATE"]);
                spc.TO_DATE = Convert.ToDateTime(sdr["TO_DATE"]);
                spc.SH_CODE = sdr["SH_CODE"].ToString();
                spc.SPC_ID = sdr["SPC_ID"].ToString();
                spc.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                spc.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                spc.STACHECK = (int)RequestState.Modified;
                spc.STATUS = Convert.ToInt16(sdr["STATUS"]);
                spc.ticked = true;
                spcdet.Add(spc);
            }
        }

        foreach (SPACE_ALLOC_DETAILS obj in allocDetLst.Where(x => x.STACHECK == (int)RequestState.Deleted).ToList())
        {
            spc = new SPACE_ALLOC_DETAILS();
            spc.SPC_ID = obj.SPC_ID;
            spc.STATUS = obj.STATUS;
            spc.ticked = true;
            spcdet.Add(spc);
        }
        if (spcdet.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcdet };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS sad)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_BY_SHIFT");
        sp.Command.Parameters.Add("@SPC_ID", sad.SPC_ID);
        sp.Command.Parameters.Add("@FDATE", sad.FROM_DATE);
        sp.Command.Parameters.Add("@TDATE", sad.TO_DATE);
        sp.Command.Parameters.Add("@SH_CODE", sad.SH_CODE);

        using (IDataReader dr = sp.GetReader())
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = dr["MSG"].ToString(), data = 1 };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //office area details
    public object GetTotalAreaDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_GET_AREA_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetSeatingCapacity(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_GET_SEATING_CAPACITY_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object Validate(string id)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "VALIDATE_FLOOR_LAYOOUT");
        sp.Command.Parameters.Add("@FLR_CODE", id, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader dr = sp.GetReader())
        {

            if (dr.Read())
            {
                return new { Message = dr["MSG"].ToString(), data = 1 };
                
            }
        }
        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    //public object Post(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = new FLOORMAP();
    //    var geom = MakeValidGeographyFromText(mapVm.Wkt);
    //    flr.geom = geom;
    //    flr.AREA_SQFT = geom.Area;
    //    flr.LATITUDE = geom.XCoordinate;
    //    flr.LONGITUDE = geom.YCoordinate;
    //    flr.FLR_ID = mapVm.flr_id;
    //    quickfmsentities.FLOORMAPS.Add(flr);
    //    quickfmsentities.SaveChanges();
    //    mapVm.ID = flr.ID;
    //    return mapVm;
    //}

    //public object Put(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = quickfmsentities.FLOORMAPS.Find(mapVm.ID);
    //    flr.geom = MakeValidGeographyFromText(mapVm.Wkt);
    //    flr.AREA_SQFT = MakeValidGeographyFromText(mapVm.Wkt).Area;
    //    quickfmsentities.Entry(flr).State = System.Data.EntityState.Modified;
    //    quickfmsentities.SaveChanges();
    //    mapVm.ID = flr.ID;
    //    return mapVm;
    //}

    //public void Delete(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = quickfmsentities.FLOORMAPS.Find(mapVm.ID);
    //    quickfmsentities.Entry(flr).State = System.Data.EntityState.Deleted;
    //    quickfmsentities.SaveChanges();
    //}

    //private DbGeometry MakeValidGeographyFromText(string inputWkt)
    //{
    //    SqlGeography sqlPolygon = SQLSpatialTools.Functions.MakeValidGeographyFromText(inputWkt, 4326);
    //    return DbGeometry.FromBinary(sqlPolygon.STAsBinary().Value);
    //}

    //public object GetMarkerDet(MaploaderVM mapVm)
    //{
    //    try
    //    {
    //        FLOORMAPS_MARKERS flrmarker = quickfmsentities.FLOORMAPS_MARKERS.Where(mrk => mrk.FM_SPC_ID == mapVm.SPACE_ID).SingleOrDefault();
    //        return new { Message = MessagesVM.UM_OK, data = flrmarker };
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Message = ex.Message, data = (object)null };
    //    }
    //}

    //public object InsertMarkerDet(MaploaderVM mapVm)
    //{
    //    return new { Message = MessagesVM.UM_OK, data = (object)null };
    //}
}
