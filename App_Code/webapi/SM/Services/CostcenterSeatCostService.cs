﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CostcenterSeatCostService
/// </summary>
public class CostcenterSeatCostService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public Object GetCostcenterDetails()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_PROJECT");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public static dynamic GetDynamicObject(Dictionary<string, object> properties)
    {
        var dynamicObject = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
        foreach (var property in properties)
        {
            dynamicObject.Add(property.Key, property.Value);
        }
        return dynamicObject;
    }
    public object GetCostcenterChart(string costcenterseatcost)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_GRAPH_COSTCENTER_SFT_COST");
        sp.Command.AddParameter("@PROJECT", string.IsNullOrEmpty(costcenterseatcost) ? "" : costcenterseatcost, DbType.String);
        ds = sp.GetDataSet();

        //Locations
        //object Locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[0].ToString()) ? r.ItemArray[1].ToString().Insert(0, "'").Insert(r.ItemArray[1].ToString().Length + 1, "'") : r.ItemArray[0].ToString().Insert(0, "'").Insert(r.ItemArray[0].ToString().Length + 1, "'"))).ToArray();        
        List<object> costcenter = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (r.ItemArray[1])).ToList();
        costcenter.Insert(0, "x");
        //AMCS
        List<object> seats = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (r.ItemArray[0])).ToList();
        seats.Insert(0, "Seat Count");
        //NONAMCS
        List<object> cost = ds.Tables[0].Rows.Cast<DataRow>().Select(r => ( r.ItemArray[2])).ToList();
        cost.Insert(0, "Total Seat Cost");

        List<Object> list = new List<object>();
        list.Add(costcenter);
        list.Add(seats);
        list.Add(cost);
        return list;
    }
}