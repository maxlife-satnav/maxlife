﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


public class EntityAdminService
{
    SubSonic.StoredProcedure sp;
    //Insert
    public int Save(EntityAdminVM model)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ENTITY_ADMIN_INSERT");
        sp.Command.AddParameter("@ADMN_CODE", model.ADM_Code, DbType.String);
        sp.Command.AddParameter("@ADMN_NAME", model.ADM_Name, DbType.String);
        sp.Command.AddParameter("@ADMN_STA_ID", model.ADM_Status_Id, DbType.String);
        sp.Command.AddParameter("@ADMN_REM", model.ADM_REM, DbType.String);
        sp.Command.Parameters.Add("@ADMN_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }
    //Update
    public Boolean UpdateEntityAdminData(EntityAdminVM update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_ENTITY_ADMIN_DATA");
        sp.Command.AddParameter("@ADMN_CODE", update.ADM_Code, DbType.String);
        sp.Command.AddParameter("@ADMN_NAME", update.ADM_Name, DbType.String);
        sp.Command.AddParameter("@ADMN_STA_ID", update.ADM_Status_Id, DbType.Int32);
        sp.Command.AddParameter("@ADMN_REM", update.ADM_REM, DbType.String);
        sp.Command.Parameters.Add("@ADMN_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Execute();
        return true;
    }
    //Bind Grid
    public IEnumerable<EntityAdminVM> BindEntityGrid()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_ENTITY_ADMIN_GRID").GetReader();
        List<EntityAdminVM> EntyAdminList = new List<EntityAdminVM>();
        while (reader.Read())
        {
            EntyAdminList.Add(new EntityAdminVM()
            {
                ADM_Code = reader.GetValue(0).ToString(),
                ADM_Name = reader.GetValue(1).ToString(),
                ADM_Status_Id = reader.GetValue(2).ToString(),
                ADM_REM = reader.GetValue(3).ToString()
            });
        }
        reader.Close();
        return EntyAdminList;
    }
}