﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ViewSpaceExtensionRequisitionService
/// </summary>
public class ViewSpaceExtReqService
{
    SubSonic.StoredProcedure sp;
    SMS_SPACE_EXTENSION_VM SpcExtnObj;
    List<SMS_SPACE_EXTENSION_VM> SSEVMLST;
    public object GetMyReqList()
    {
        try
        {

            SSEVMLST = new List<SMS_SPACE_EXTENSION_VM>();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@USER_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "VIEW_SPACE_EXTENSION_REQUISITION_DETAILS", param))
            {

                while (sdr.Read())
                {
                    SpcExtnObj = new SMS_SPACE_EXTENSION_VM();
                    SpcExtnObj.SSE_REQ_ID = sdr["SSE_REQ_ID"].ToString();
                    SpcExtnObj.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                    SpcExtnObj.SSE_REQ_BY = sdr["SSE_REQ_BY"].ToString();
                    SpcExtnObj.SSE_REQ_DT = Convert.ToDateTime(sdr["SSE_REQ_DT"]);
                    SpcExtnObj.SSE_STA_ID = Convert.ToInt32(sdr["SSE_STA_ID"]);
                    SpcExtnObj.STA_DESC = sdr["STA_DESC"].ToString();
                    SpcExtnObj.SSE_REQ_REM = sdr["SSE_REQ_REM"].ToString();
                    SpcExtnObj.SSE_APPR_REM = sdr["SSE_APPR_REM"].ToString();
                    SpcExtnObj.SSE_FROM_DATE = string.IsNullOrEmpty(Convert.ToString(sdr["SSE_FROM_DATE"])) ? (DateTime?)null : Convert.ToDateTime(sdr["SSE_FROM_DATE"]);
                    SpcExtnObj.SSE_TO_DATE = string.IsNullOrEmpty(Convert.ToString(sdr["SSE_TO_DATE"])) ? (DateTime?)null : Convert.ToDateTime(sdr["SSE_TO_DATE"]);
                    SSEVMLST.Add(SpcExtnObj);
                }
                sdr.Close();
            }
            if (SSEVMLST.Count != 0)
                return new { Message = MessagesVM.SER_OK, data = SSEVMLST };
            else
                return new { Message = MessagesVM.SER_OK, data = (object)null };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object GetPendingReqList()
    {
        SSEVMLST = new List<SMS_SPACE_EXTENSION_VM>();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@USER_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "VIEW_PENDING_SPACE_EXTN_REQUISITION_DETAILS", param))
        {
            while (sdr.Read())
            {
                SpcExtnObj = new SMS_SPACE_EXTENSION_VM();
                SpcExtnObj.SSE_REQ_ID = sdr["SSE_REQ_ID"].ToString();
                SpcExtnObj.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                SpcExtnObj.SSE_REQ_BY = sdr["SSE_REQ_BY"].ToString();
                SpcExtnObj.SSE_REQ_DT = Convert.ToDateTime(sdr["SSE_REQ_DT"]);
                SpcExtnObj.SSE_STA_ID = Convert.ToInt32(sdr["SSE_STA_ID"]);
                SpcExtnObj.STA_DESC = sdr["STA_DESC"].ToString();
                SpcExtnObj.SSE_REQ_REM = sdr["SSE_REQ_REM"].ToString();
                SpcExtnObj.SSE_APPR_REM = sdr["SSE_APPR_REM"].ToString();
                SpcExtnObj.SSE_FROM_DATE = string.IsNullOrEmpty(Convert.ToString(sdr["SSE_FROM_DATE"])) ? (DateTime?)null : Convert.ToDateTime(sdr["SSE_FROM_DATE"]);
                SpcExtnObj.SSE_TO_DATE = string.IsNullOrEmpty(Convert.ToString(sdr["SSE_TO_DATE"])) ? (DateTime?)null : Convert.ToDateTime(sdr["SSE_TO_DATE"]);
                SSEVMLST.Add(SpcExtnObj);
            }
            sdr.Close();
        }
        if (SSEVMLST.Count != 0)
            return new { Message = MessagesVM.SER_OK, data = SSEVMLST };
        else
            return new { Message = MessagesVM.SER_OK, data = (object)null };
    }

    public object GetDetailsOnSelection(SMS_SPACE_EXTENSION_VM selectedid)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "EDIT_VIEW_SPACE_EXTENSION_REQUISITION_DETAILS");
            sp.Command.AddParameter("@SSE_REQ_ID", selectedid.SSE_REQ_ID, DbType.String);

            SPC_EXTN_FLOOR_VERTICAL_LIST flr_ver_detials = new SPC_EXTN_FLOOR_VERTICAL_LIST();
            flr_ver_detials.selectedSpaces = new List<SMS_SPACE_EXTENSION_DETAILS>();
            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    flr_ver_detials.selectedCountries = new List<Countrylst>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        flr_ver_detials.selectedCountries.Add(new Countrylst { CNY_CODE = Convert.ToString(dr["SSESD_CNY_CODE"]), CNY_NAME = Convert.ToString(dr["CNY_NAME"]), ticked = true });
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    flr_ver_detials.selectedCities = new List<Citylst>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        flr_ver_detials.selectedCities.Add(new Citylst { CTY_CODE = Convert.ToString(dr["SSESD_CTY_CODE"]), ZN_CODE = Convert.ToString(dr["SSESD_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SSESD_STE_CODE"]), CTY_NAME = Convert.ToString(dr["CTY_NAME"]), CNY_CODE = Convert.ToString(dr["SSESD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    flr_ver_detials.selectedLocations = new List<Locationlst>();
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        flr_ver_detials.selectedLocations.Add(new Locationlst { LCM_CODE = Convert.ToString(dr["SSESD_LCM_CODE"]), ZN_CODE = Convert.ToString(dr["SSESD_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SSESD_STE_CODE"]), LCM_NAME = Convert.ToString(dr["LCM_NAME"]), CTY_CODE = Convert.ToString(dr["SSESD_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SSESD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    flr_ver_detials.selectedTowers = new List<Towerlst>();
                    foreach (DataRow dr in ds.Tables[3].Rows)
                    {
                        flr_ver_detials.selectedTowers.Add(new Towerlst { TWR_CODE = Convert.ToString(dr["SSESD_TWR_CODE"]), ZN_CODE = Convert.ToString(dr["SSESD_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SSESD_STE_CODE"]), TWR_NAME = Convert.ToString(dr["TWR_NAME"]), LCM_CODE = Convert.ToString(dr["SSESD_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["SSESD_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SSESD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[4].Rows.Count > 0)
                {
                    flr_ver_detials.selectedFloors = new List<Floorlst>();
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        flr_ver_detials.selectedFloors.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["SSESD_FLR_CODE"]), ZN_CODE = Convert.ToString(dr["SSESD_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SSESD_STE_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["SSESD_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["SSESD_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["SSESD_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SSESD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[5].Rows.Count > 0)
                {
                    flr_ver_detials.selectedVerticals = new List<Verticallst>();
                    foreach (DataRow dr in ds.Tables[5].Rows)
                    {
                        flr_ver_detials.selectedVerticals.Add(new Verticallst { VER_CODE = Convert.ToString(dr["SSECD_VER_CODE"]), VER_NAME = Convert.ToString(dr["VER_NAME"]), ticked = true });
                    }
                }

                if (ds.Tables[6].Rows.Count > 0)
                {
                    flr_ver_detials.selectedCostcenters = new List<Costcenterlst>();
                    foreach (DataRow dr in ds.Tables[6].Rows)
                    {
                        flr_ver_detials.selectedCostcenters.Add(new Costcenterlst { Cost_Center_Code = Convert.ToString(dr["SSECD_COST_CENTER"]), Cost_Center_Name = Convert.ToString(dr["Cost_Center_Name"]), Vertical_Code = Convert.ToString(dr["Vertical_Code"]), ticked = true });
                    }
                }
                if (ds.Tables[7].Rows.Count > 0)
                {
                    flr_ver_detials.selectedZones = new List<Zonelst>();
                    foreach (DataRow dr in ds.Tables[7].Rows)
                    {
                        flr_ver_detials.selectedZones.Add(new Zonelst {  ZN_CODE = Convert.ToString(dr["SSESD_ZN_CODE"]),  ZN_NAME = Convert.ToString(dr["ZN_NAME"]), CNY_CODE = Convert.ToString(dr["SSESD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[8].Rows.Count > 0)
                {
                    flr_ver_detials.selectedStates = new List<Statelst>();
                    foreach (DataRow dr in ds.Tables[8].Rows)
                    {
                        flr_ver_detials.selectedStates.Add(new Statelst { ZN_CODE = Convert.ToString(dr["SSESD_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SSESD_STE_CODE"]), STE_NAME = Convert.ToString(dr["STE_NAME"]), CNY_CODE = Convert.ToString(dr["SSESD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[9].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[9].Rows)
                    {
                        flr_ver_detials.selectedSpaces.Add(new SMS_SPACE_EXTENSION_DETAILS { SSED_REQ_ID = Convert.ToString(dr["SSED_REQ_ID"]), SSAD_FROM_DATE = Convert.ToDateTime(dr["SSAD_FROM_DATE"]), SSAD_TO_DATE = Convert.ToDateTime(dr["SSAD_TO_DATE"]), SSAD_FROM_TIME = Convert.ToDateTime(dr["SSAD_FROM_TIME"]), SSAD_TO_TIME = Convert.ToDateTime(dr["SSAD_TO_TIME"]), SSED_STA_ID = Convert.ToInt32(dr["SSED_STA_ID"]), SSED_SSE_REQ_ID = Convert.ToString(dr["SSED_SSE_REQ_ID"]), SSAD_SRN_REQ_ID = Convert.ToString(dr["SSED_SSAD_SRN_REQ_ID"]), SSED_EXTN_DT = Convert.ToDateTime(dr["SSED_EXTN_DT"]), SSED_SPC_ID = Convert.ToString(dr["SSED_SPC_ID"]), STACHECK = (int)RequestState.Unchanged });
                    }
                }

            }
            SpaceExtensionService seservc = new SpaceExtensionService();
            SpaceExtensionVM spcdet = new SpaceExtensionVM();
            SPC_EXTN_FLOOR_VERTICAL_LIST sefvl = new SPC_EXTN_FLOOR_VERTICAL_LIST();
            sefvl.selectedFloors = flr_ver_detials.selectedFloors;
            sefvl.selectedCostcenters = flr_ver_detials.selectedCostcenters;
            spcdet.spceextn_flr_ver = sefvl;
            spcdet.spcextn = new SMS_SPACE_EXTENSION_VM()
            {
                SSE_FROM_DATE = selectedid.SSE_FROM_DATE,
                SSE_TO_DATE = selectedid.SSE_TO_DATE
            };
            List<SMS_SPACE_EXTENSION> spcextdet = seservc.GetSpaceExtensionDetailsList(spcdet);
            var common = from a1 in spcextdet
                         join a2 in flr_ver_detials.selectedSpaces on a1.SSAD_SRN_REQ_ID equals a2.SSAD_SRN_REQ_ID into temp
                         from t1 in temp.DefaultIfEmpty()
                         select new { A1 = a1, A2 = t1 };

            foreach (var c in common)
            {
                if (c.A2 != null)
                {
                    c.A1.SSAD_SRN_REQ_ID = c.A2.SSAD_SRN_REQ_ID;
                    c.A1.ticked = true;
                    c.A1.SSED_EXTN_DT = c.A2.SSED_EXTN_DT;
                    c.A1.SSED_REQ_ID = c.A2.SSED_REQ_ID;
                    c.A1.STACHECK = (int)RequestState.Unchanged;
                }

            }

            return new { Message = MessagesVM.UM_OK, data = new { spceextn_flr_ver = flr_ver_detials, spcextn = selectedid, UpdateGridDETAILS = common.Select(cmn => cmn.A1).OrderByDescending(x => x.ticked).ToList() } };

        }

        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

}