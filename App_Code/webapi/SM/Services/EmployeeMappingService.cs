﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for EmployeeMappingService
/// </summary>
public class EmployeeMappingService
{
    SubSonic.StoredProcedure sp;
    SpaceRequistion_details spcreqdet;
    List<SpaceRequistion_details> spcreqdetlst;

    public object GetPendingEmpMapList()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SPACE_EMP_MAP_REQUESTS_SM");
        sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader reader = sp.GetReader())
        {
            List<SpaceReq> spclist = new List<SpaceReq>();
            while (reader.Read())
            {
                spclist.Add(new SpaceReq()
                {
                    SRN_REQ_ID = reader["SRN_REQ_ID"].ToString(),
                    VER_NAME = reader["VER_NAME"].ToString(),
                    COST_CENTER_NAME = reader["COST_CENTER_NAME"].ToString(),
                    STA_DESC = reader["STA_DESC"].ToString(),
                    SRN_FROM_DATE = (DateTime)reader["SRN_FROM_DATE"],
                    SRN_TO_DATE = (DateTime)reader["SRN_TO_DATE"],
                    AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                    SRN_REQ_BY = reader["SRN_REQ_BY"].ToString(),
                    SRN_REQ_REM = reader["SRN_REQ_REM"].ToString(),
                    SRN_STA_ID = (int)reader["SRN_STA_ID"],
                    SRN_REQ_DT = (DateTime)reader["SRN_REQ_DT"],
                    SRN_SYS_PRF_CODE = reader["SRN_SYS_PRF_CODE"].ToString(),
                    SRN_VERTICAL = reader["SRN_VERTICAL"].ToString(),
                    SRN_COST_CENTER = reader["SRN_COST_CENTER"].ToString()
                });
            }
            reader.Close();
            if (spclist.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = spclist };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GetDetailsOnSelection(SpaceRequistion selectedid)
    {
        SpaceReqDetails SpaceReqDet = new SpaceReqDetails();

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "EDIT_EMP_MAP_REQUISITIONS_SM");
        sp.Command.AddParameter("@SRN_REQ_ID", selectedid.SRN_REQ_ID, DbType.String);

        DataSet ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                SpaceReqDet.cnylst = new List<Countrylst>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    SpaceReqDet.cnylst.Add(new Countrylst { CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), CNY_NAME = Convert.ToString(dr["CNY_NAME"]), ticked = true });
                }
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                SpaceReqDet.ctylst = new List<Citylst>();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    SpaceReqDet.ctylst.Add(new Citylst { CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CTY_NAME = Convert.ToString(dr["CTY_NAME"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["SRNB_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SRNB_STE_CODE"]), ticked = true });
                }
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                SpaceReqDet.loclst = new List<Locationlst>();
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    SpaceReqDet.loclst.Add(new Locationlst { LCM_CODE = Convert.ToString(dr["SRNB_LOC_CODE"]), LCM_NAME = Convert.ToString(dr["LCM_NAME"]), CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["SRNB_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SRNB_STE_CODE"]), ticked = true });
                }
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                SpaceReqDet.twrlst = new List<Towerlst>();
                foreach (DataRow dr in ds.Tables[3].Rows)
                {
                    SpaceReqDet.twrlst.Add(new Towerlst { TWR_NAME = Convert.ToString(dr["TWR_NAME"]), TWR_CODE = Convert.ToString(dr["SRNB_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["SRNB_LOC_CODE"]), CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["SRNB_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SRNB_STE_CODE"]), ticked = true });
                }
            }

            if (ds.Tables[4].Rows.Count > 0)
            {
                SpaceReqDet.flrlst = new List<Floorlst>();
                foreach (DataRow dr in ds.Tables[4].Rows)
                {
                    SpaceReqDet.flrlst.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["SRNB_FLR_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["SRNB_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["SRNB_LOC_CODE"]), CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["SRNB_ZN_CODE"]), STE_CODE = Convert.ToString(dr["SRNB_STE_CODE"]), ticked = true });
                }
            }
            if (ds.Tables[5].Rows.Count > 0)
            {
                SpaceReqDet.verlst = new List<Verticallst>();
                foreach (DataRow dr in ds.Tables[5].Rows)
                {
                    SpaceReqDet.verlst.Add(new Verticallst { VER_CODE = Convert.ToString(dr["SRN_VERTICAL"]), VER_NAME = Convert.ToString(dr["VER_NAME"]), ticked = true });

                }

            }
            if (ds.Tables[6].Rows.Count > 0)
            {
                SpaceReqDet.cstlst = new List<Costcenterlst>();
                foreach (DataRow dr in ds.Tables[6].Rows)
                {
                    SpaceReqDet.cstlst.Add(new Costcenterlst { Cost_Center_Code = Convert.ToString(dr["SRN_COST_CENTER"]), Cost_Center_Name = Convert.ToString(dr["Cost_Center_Name"]), Vertical_Code = Convert.ToString(dr["SRN_VERTICAL"]), ticked = true });
                }

            }
            if (ds.Tables[7].Rows.Count > 0)
            {
                SpaceReqDet.spcreqcount = new List<SpaceRequistionCount>();
                foreach (DataRow dr in ds.Tables[7].Rows)
                {
                    SpaceReqDet.spcreqcount.Add(new SpaceRequistionCount
                    {
                        SRC_SRN_REQ_ID = Convert.ToString(dr["SRC_SRN_REQ_ID"]),
                        SRC_REQ_CNT = Convert.ToInt32(dr["SRC_REQ_CNT"]),
                        SRC_FLR_CODE = Convert.ToString(dr["SRC_FLR_CODE"]),
                        SRC_STA_ID = Convert.ToInt32(dr["SRC_STA_ID"]),
                        SRC_REQ_SEL_TYPE = Convert.ToString(dr["SRC_REQ_SEL_TYPE"]),
                        SRC_REQ_TYPE = Convert.ToString(dr["SRC_REQ_TYPE"])
                    });
                }

            }

            if (ds.Tables[8].Rows.Count > 0)
            {
                SpaceReqDet.spcreqdet = new List<SpaceRequistion_details>();
                foreach (DataRow dr in ds.Tables[8].Rows)
                {
                    SpaceReqDet.spcreqdet.Add(new SpaceRequistion_details
                    {
                        SRD_SPC_ID = Convert.ToString(dr["SRD_SPC_ID"]),
                        SRD_REQ_ID = Convert.ToString(dr["SRD_REQ_ID"]),
                        SRD_SH_CODE = Convert.ToString(dr["SRD_SH_CODE"]),
                        SRD_SPC_NAME = Convert.ToString(dr["SPC_NAME"]),
                        SRD_SPC_TYPE = Convert.ToString(dr["SPC_TYPE_CODE"]),
                        SRD_SPC_TYPE_NAME = Convert.ToString(dr["SPC_TYPE_NAME"]),
                        SRD_SPC_SUB_TYPE = Convert.ToString(dr["SST_CODE"]),
                        SRD_SPC_SUB_TYPE_NAME = Convert.ToString(dr["SST_NAME"]),
                        SRD_AUR_ID = Convert.ToString(dr["SRD_AUR_ID"]),
                        SRD_SSA_SRNREQ_ID = Convert.ToString(dr["SRD_SSA_SRNREQ_ID"]),
                        SRD_STA_ID = (int)(dr["SRD_STA_ID"]),
                        SRD_SRNREQ_ID = Convert.ToString(dr["SRD_SRNREQ_ID"]),
                        lat = Convert.ToString(dr["SPC_X_VALUE"]),
                        lon = Convert.ToString(dr["SPC_Y_VALUE"]),
                        SSA_FLR_CODE = Convert.ToString(dr["SRNB_FLR_CODE"]),
                        STACHECK = (int)RequestState.Unchanged
                    });

                }
            }
        }
        //SpaceRequisitionService spcreq = new SpaceRequisitionService();
        //if (selectedid.SRN_SYS_PRF_CODE == "1039" || selectedid.SRN_SYS_PRF_CODE == "1040")
        //{
        //    SpaceReqDet.spcreq = new SpaceRequistion() { SRN_FROM_DATE = (DateTime)selectedid.SRN_FROM_DATE, SRN_TO_DATE = (DateTime)selectedid.SRN_TO_DATE };
        //    List<SpaceRequistion_details> spc = spcreq.GetSpaceLst(SpaceReqDet);

        //    var common = from a1 in spc
        //                 join a2 in SpaceReqDet.spcreqdet on a1.SRD_SPC_ID equals a2.SRD_SPC_ID into temp
        //                 from t1 in temp.DefaultIfEmpty()
        //                 select new { A1 = a1, A2 = t1 };

        //    foreach (var c in common)
        //    {
        //        if (c.A2 != null)
        //        {
        //            c.A1.SRD_REQ_ID = c.A2.SRD_REQ_ID;
        //            c.A1.ticked = true;
        //            c.A1.SRD_SH_CODE = c.A2.SRD_SH_CODE;
        //            c.A1.SRD_AUR_ID = c.A2.SRD_AUR_ID;
        //            c.A1.STACHECK = (int)RequestState.Unchanged;
        //        }
        //        else
        //            c.A1.SRD_REQ_ID = "";
        //    }

        //    return new { Message = MessagesVM.UM_OK, data = new { SELSPACES = SpaceReqDet, DETAILS = common.Select(cmn => cmn.A1).OrderByDescending(x => x.ticked).ToList() } };
        //}
        //else if (selectedid.SRN_SYS_PRF_CODE == "1041")
        //{
        //    return new { Message = MessagesVM.UM_OK, data = new { SELSPACES = SpaceReqDet } };
        //}
        return new { Message = MessagesVM.UM_OK, data = new { SELSPACES = SpaceReqDet } };
    }

    public object SaveSelectedSpaces(SpaceReqDetails spcreqDetails)
    {
        List<SpaceRequistion> spcreqLst = new List<SpaceRequistion>();
        spcreqLst.Add(spcreqDetails.spcreq);
        if (spcreqDetails.spcreqdet.Count != 0)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@SPC_REQ", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(spcreqLst);
            param[1] = new SqlParameter("@SPC_REQ_DET", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(spcreqDetails.spcreqdet);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SAVE_SPACE_EMP_MAPPING_SM", param))
            {
                if (dr.Read())
                {
                    int FLAG = (int)dr["FLAG"];
                    string REQID = dr["REQID"].ToString();

                    if (FLAG == 1)
                    {
                        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_EMPLOYEE_MAPPING_ACTION_SM");
                        sp.Command.AddParameter("@REQID", REQID, DbType.String);
                        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"], DbType.String);
                        sp.ExecuteScalar();

                        return new { Message = MessagesVM.EMP_ALLOC_SUC + " For : " + REQID, data = REQID };

                    }
                    else
                        return new { Message = REQID, data = (object)null };
                }
            }
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        else
            return new { Message = MessagesVM.L1_NOREQ, data = (object)null };
    }

    public object GetRepeatColumns(EmployeeMappingVM spcreqDetails)
    {
        //List<SpaceRequistion_details> spcreqLst = new List<SpaceRequistion_details>();
        //if (spcreqDetails.spcreqdet.Count != 0)
        //{
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[0].Value = spcreqDetails.SRN_FROM_DATE;
        param[1] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[1].Value = spcreqDetails.SRN_TO_DATE;
        param[2] = new SqlParameter("@SPC_ID", SqlDbType.NVarChar);
        param[2].Value = spcreqDetails.SRD_SPC_ID;
        param[3] = new SqlParameter("@SH_CODE", SqlDbType.NVarChar);
        param[3].Value = spcreqDetails.SRD_SH_CODE;

        List<Shiftlst> RptSh_lst = new List<Shiftlst>();
        Shiftlst shift;
        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_REPT_COLUMNS_SHIFT_SM", param))
        {
            while (dr.Read())
            {
                shift = new Shiftlst();
                shift.SH_CODE = dr["SH_CODE"].ToString();
                shift.SH_NAME = dr["SH_NAME"].ToString();
                RptSh_lst.Add(shift);
            }
            return new { Message = MessagesVM.UM_OK, data = RptSh_lst };
        }
    }

    public object GetRepeatColumns_FreeSeat(EmployeeMappingVM spcreqDetails)
    {
        //List<SpaceRequistion_details> spcreqLst = new List<SpaceRequistion_details>();
        //if (spcreqDetails.spcreqdet.Count != 0)
        //{
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@SPC_ID", SqlDbType.NVarChar);
        param[0].Value = spcreqDetails.SRD_SPC_ID;
        param[1] = new SqlParameter("@SH_CODE", SqlDbType.NVarChar);
        param[1].Value = spcreqDetails.SRD_SH_CODE;

        List<Shiftlst> RptSh_lst = new List<Shiftlst>();
        Shiftlst shift;
        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_REPT_COLUMNS_SHIFT_SM_FREE_SEAT", param))
        {
            while (dr.Read())
            {
                shift = new Shiftlst();
                shift.SH_CODE = dr["SH_CODE"].ToString();
                shift.SH_NAME = dr["SH_NAME"].ToString();
                RptSh_lst.Add(shift);
            }
            return new { Message = MessagesVM.UM_OK, data = RptSh_lst };
        }
    }
}