﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

public class SpaceSubTypeService
{
    SubSonic.StoredProcedure sp;
    public int Save(SpaceSubTypeVM SST_VM)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_INSERT_SPACE_SUB_TYPE");
            sp.Command.AddParameter("@SST_CODE", SST_VM.SST_CODE, DbType.String);
            sp.Command.AddParameter("@SST_NAME", SST_VM.SST_NAME, DbType.String);
            sp.Command.AddParameter("@SST_SPC_TYPE", SST_VM.SST_SPC_TYPE, DbType.String);
            sp.Command.AddParameter("@SST_STA_ID", SST_VM.SST_STA_ID, DbType.String);
            sp.Command.AddParameter("@SST_REM", SST_VM.SST_REM, DbType.String);
            sp.Command.AddParameter("@SST_COLOR", SST_VM.SST_COLOR, DbType.String);
            sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }

    public Boolean Update(SpaceSubTypeVM upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_UPDATE_SPACE_SUB_TYPE");
            sp.Command.AddParameter("@SST_CODE", upd.SST_CODE, DbType.String);
            sp.Command.AddParameter("@SST_NAME", upd.SST_NAME, DbType.String);
            sp.Command.AddParameter("@SST_SPC_TYPE", upd.SST_SPC_TYPE, DbType.String);
            sp.Command.AddParameter("@SST_STA_ID", upd.SST_STA_ID, DbType.String);
            sp.Command.AddParameter("@SST_REM", upd.SST_REM, DbType.String);
            sp.Command.AddParameter("@SST_COLOR", upd.SST_COLOR, DbType.String);
            sp.Command.Parameters.Add("@UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Execute();
            return true;
        }
        catch
        {
            throw;
        }
    }

    public IEnumerable<SpaceSubTypeVM> BindGrid()
    {
        try
        {
            IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_ALL_SPACE_SUBTYPE_GRID]").GetReader();
            List<SpaceSubTypeVM> SpcSubTypeList = new List<SpaceSubTypeVM>();
            while (reader.Read())
            {
                SpcSubTypeList.Add(new SpaceSubTypeVM()
                {
                    SST_CODE = reader.GetValue(0).ToString(),
                    SST_NAME = reader.GetValue(1).ToString(),
                    SST_STA_ID = reader.GetValue(2).ToString(),
                    SST_REM = reader.GetValue(3).ToString(),
                    SST_SPC_TYPE = reader.GetValue(4).ToString(),
                    SST_COLOR = reader.GetValue(5).ToString()
                });
            }
            reader.Close();
            return SpcSubTypeList;
        }
        catch
        {
            throw;
        }
    }

    public IEnumerable<SpaceTypeVM> GetSpaceTypes()
    {
        try
        {
            IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_ACTIVE_SPACE_TYPES").GetReader();
            List<SpaceTypeVM> SpaceTypeList = new List<SpaceTypeVM>();
            while (reader.Read())
            {
                SpaceTypeList.Add(new SpaceTypeVM()
                {
                    SPC_TYPE_CODE = reader.GetValue(0).ToString(),
                    SPC_TYPE_NAME = reader.GetValue(1).ToString()
                });
            }
            reader.Close();
            return SpaceTypeList;
        }

        catch
        {
            throw;
        }
    }
}