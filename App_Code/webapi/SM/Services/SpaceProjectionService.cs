﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceProjectionService
/// </summary>
public class SpaceProjectionService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public IEnumerable<Years> GetYears()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_PROJECTION_Years").GetReader();
        List<Years> yearlist = new List<Years>();
        while (reader.Read())
        {
            yearlist.Add(new Years()
            {
                Year = reader.GetValue(0).ToString(),
            });
        }
        reader.Close();
        return yearlist;
    }

  
    public IEnumerable<VerticalVM> GetVerticals()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GETVERTICALS").GetReader();
        List<VerticalVM> verticallist = new List<VerticalVM>();
        while (reader.Read())
        {
            verticallist.Add(new VerticalVM()
            {
                VER_CODE = reader.GetValue(0).ToString(),
                VER_NAME = reader.GetValue(1).ToString()
            });
        }
        reader.Close();
        return verticallist;
    }

    public object SaveDetails(SpaceProjectionVM sprjVm)
    {
        try
        {
            DeleteExistingSPDetails(sprjVm);
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_REFERENCE_ID_SPACE_PROJECTION");
            sp.Command.AddParameter("@SSP_CNY_CODE", sprjVm.ssp.SSP_CNY_CODE);
            sp.Command.AddParameter("@SSP_VER_CODE", sprjVm.ssp.SSP_VER_CODE, DbType.String);
            sp.Command.AddParameter("@SSP_PROJECTION_TYPE", sprjVm.ssp.SSP_PROJECTION_TYPE, DbType.String);
            sp.Command.AddParameter("@SSP_YEAR", sprjVm.ssp.SSP_YEAR, DbType.String);
            IDataReader dr = sp.GetReader();
            while (dr.Read())
            {
                sprjVm.ssp.SSP_ID = Convert.ToInt32(dr["SSP_ID"]);
                sprjVm.ssp.SSP_REQ_ID = dr["SSP_REQ_ID"].ToString();
                sprjVm.ssp.SSP_REQ_REM = dr["SSP_REQ_REM"].ToString();
           
            }
            if (sprjVm.ssp.SSP_REQ_ID == null)
            {
                sprjVm.ssp.SSP_REQ_ID = "SPRJ_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            }


            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_INSERT_SPACE_PROJECTION");
            sp.Command.AddParameter("@SSP_CNY_CODE", sprjVm.ssp.SSP_CNY_CODE);
            sp.Command.AddParameter("@SSP_VER_CODE", sprjVm.ssp.SSP_VER_CODE, DbType.String);
            sp.Command.AddParameter("@SSP_PROJECTION_TYPE", sprjVm.ssp.SSP_PROJECTION_TYPE, DbType.String);
            sp.Command.AddParameter("@SSP_YEAR", sprjVm.ssp.SSP_YEAR, DbType.String);
            sp.Command.AddParameter("@SSP_REQ_ID", sprjVm.ssp.SSP_REQ_ID, DbType.String);
            sp.Command.AddParameter("@SSP_REQ_REM", sprjVm.ssp.SSP_REQ_REM, DbType.String);
            sp.Command.AddParameter("@SSP_STA_ID", sprjVm.ssp.SSP_STA_ID, DbType.String);
            sp.Command.AddParameter("@SSP_ID", sprjVm.ssp.SSP_ID, DbType.String);
            sp.Command.AddParameter("@SSP_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            //int SSP_ID = (int)sp.ExecuteScalar();
            sp.Execute();
            Insert_SP_Details(sprjVm);

            if (sprjVm.ssp.SSP_STA_ID == "1027")
            {
                SendMail(sprjVm);
                return new { Message = MessagesVM.PRJ_SUBMITTED_MAIL, data = sprjVm.ssp };
            }

            return new { Message = MessagesVM.PRJ_INSERTED, data = sprjVm.ssp };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public void Insert_SP_Details(SpaceProjectionVM SprjVM)
    {
        try
        {
            List<SP_Main> Mainlst = SprjVM.SP_Main;
            List<SPACE_PROJECTION_DETAILS_VM> spdetaillst = new List<SPACE_PROJECTION_DETAILS_VM>();
            SPACE_PROJECTION_DETAILS_VM spdt;
            foreach (var lis in Mainlst)
            {
                List<SP_Sub> Sublst = lis.Value;
                foreach (var detlis in Sublst)
                {
                    spdt = new SPACE_PROJECTION_DETAILS_VM();
                    spdt.SSPD_CTY_CODE = lis.Key.CTY_CODE;
                    spdt.SSPD_MONTH = (int)detlis.Key.monthNumber;
                    spdt.SSPD_PRJ_SPC_TYPE = detlis.Value.SSPD_PRJ_SPC_TYPE;
                    spdt.SSPD_PRJ_VALUE = detlis.Value.SSPD_PRJ_VALUE;
                    spdt.SSPD_PROJECTION_TYPE = SprjVM.ssp.SSP_PROJECTION_TYPE;
                    spdt.SSPD_STA_ID = SprjVM.ssp.SSP_STA_ID;
                    if (spdt.SSPD_PRJ_VALUE != 0)
                    {
                        spdetaillst.Add(spdt);
                    }
                }
            }
            foreach (var insrtList in spdetaillst)
            {
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_INSERT_SPACE_PROJECTION_DETAILS");
                sp.Command.AddParameter("@SSPD_SSP_REQ_ID", SprjVM.ssp.SSP_REQ_ID, DbType.String);
                sp.Command.AddParameter("@SSPD_CTY_CODE", insrtList.SSPD_CTY_CODE, DbType.String);
                sp.Command.AddParameter("@SSPD_PRJ_SPC_TYPE", insrtList.SSPD_PRJ_SPC_TYPE, DbType.String);
                sp.Command.AddParameter("@SSPD_MONTH", insrtList.SSPD_MONTH, DbType.String);
                sp.Command.AddParameter("@SSPD_PRJ_VALUE", insrtList.SSPD_PRJ_VALUE, DbType.String);
                sp.Command.AddParameter("@SSPD_PROJECTION_TYPE", insrtList.SSPD_PROJECTION_TYPE, DbType.String);
                sp.Command.AddParameter("@SSPD_STA_ID", insrtList.SSPD_STA_ID, DbType.String);
                sp.Command.AddParameter("@User_id", HttpContext.Current.Session["UID"], DbType.String);
                sp.Execute();
            }
        }
        catch
        {
            throw;
        }
    }

    public object GetSPRJDetails(SMS_SPACE_PROJECTION ssp)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_SPRJ_DETAILS");
            sp.Command.AddParameter("@REQ_CNY_CODE", ssp.SSP_CNY_CODE);
            sp.Command.AddParameter("@REQ_SPRJ_YEAR", ssp.SSP_YEAR);
            sp.Command.AddParameter("@REQ_SPRJ_VER_CODE", ssp.SSP_VER_CODE);
            IDataReader dr = sp.GetReader();
            List<SPACE_PROJECTION_DETAILS_VM> ssplst = new List<SPACE_PROJECTION_DETAILS_VM>();
            SPACE_PROJECTION_DETAILS_VM SPDVM;
            while (dr.Read())
            {
                SPDVM = new SPACE_PROJECTION_DETAILS_VM();
                SPDVM.SSPD_PRJ_VALUE = Convert.ToInt32(dr["SSPD_PRJ_VALUE"]);
                SPDVM.SSPD_MONTH = Convert.ToInt32(dr["SSPD_MONTH"]);
                SPDVM.SSPD_CTY_CODE = dr["SSPD_CTY_CODE"].ToString();
                SPDVM.SSPD_STA_ID = dr["SSPD_STA_ID"].ToString();
                SPDVM.SSPD_PROJECTION_TYPE = ssp.SSP_PROJECTION_TYPE;
                ssplst.Add(SPDVM);
            }

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_City_By_Country");
            sp.Command.AddParameter("@CTY_CNY_ID", ssp.SSP_CNY_CODE);
            dr = sp.GetReader();
            List<City> cityList = new List<City>();
            City cty;
            while (dr.Read())
            {
                cty = new City();
                cty.CTY_CODE = dr["CTY_CODE"].ToString();
                cty.CTY_NAME = dr["CTY_NAME"].ToString();
                cityList.Add(cty);
            }
            List<MonthNames> monthList = new List<MonthNames>();
            MonthNames mon;
            Dictionary<Int32, double> totals = new Dictionary<Int32, double>();
            for (int i = 1; i <= 12; i++)
            {
                mon = new MonthNames();
                string monName = DateTimeFormatInfo.CurrentInfo.GetMonthName(i).Substring(0, 3);
                mon.monthName = monName;
                mon.monthNumber = i;
                monthList.Add(mon);
                totals.Add(i, 0);
            }

            Dictionary<object, object> dictCities = new Dictionary<object, object>();
            Dictionary<object, object> dictMon;

            foreach (City ctyVm in cityList)
            {
                dictMon = new Dictionary<object, object>();
                foreach (MonthNames month in monthList)
                {
                    var query = from firstItem in ssplst
                                join secondItem in cityList
                                on firstItem.SSPD_CTY_CODE equals secondItem.CTY_CODE
                                select firstItem;
                    var monthNum = query.FirstOrDefault(x => x.SSPD_MONTH == month.monthNumber && x.SSPD_PRJ_VALUE != 0 && x.SSPD_CTY_CODE == ctyVm.CTY_CODE);

                    if (monthNum != null && monthNum.SSPD_CTY_CODE == ctyVm.CTY_CODE)
                    {
                        dictMon.Add(new { monthNumber = month.monthNumber }, new { SSPD_PRJ_VALUE = monthNum.SSPD_PRJ_VALUE, SSP_STA_ID = monthNum.SSPD_STA_ID, SSPD_PROJECTION_TYPE = monthNum.SSPD_PROJECTION_TYPE, StateOfObj = EntityState.Modified });
                        totals[month.monthNumber] += monthNum.SSPD_PRJ_VALUE;
                    }
                    else
                    {
                        dictMon.Add(new { monthNumber = month.monthNumber }, new { SSPD_PRJ_VALUE = 0, SSP_STA_ID = ssp.SSP_STA_ID });
                    }
                }
                dictCities.Add(new { CTY_CODE = ctyVm.CTY_CODE, CTY_NAME = ctyVm.CTY_NAME }, dictMon.ToArray());
            }

            if(dictCities.Count != 0)
            return new { SP_Master = dictCities.ToList(), MonthList = monthList, totals = totals.ToList() };
            else
                return new { Message = MessagesVM.PRJ_REC, SP_Master = (object)null, MonthList = (object)null, totals = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, SP_Master = (object)null, MonthList = (object)null, totals = (object)null };
        }
    }

    public void DeleteExistingSPDetails(SpaceProjectionVM SPVM)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_DELETE_SPRJ_DETAILS");
            sp.Command.AddParameter("@SSP_CNY_CODE", SPVM.ssp.SSP_CNY_CODE);
            sp.Command.AddParameter("@SSP_VER_CODE", SPVM.ssp.SSP_VER_CODE, DbType.String);
            sp.Command.AddParameter("@SSP_PROJECTION_TYPE", SPVM.ssp.SSP_PROJECTION_TYPE, DbType.String);
            sp.Command.AddParameter("@SSP_YEAR", SPVM.ssp.SSP_YEAR, DbType.String);
            sp.Execute();
        }
        catch
        {
            throw;
        }
    }

    public void SendMail(SpaceProjectionVM SPVMMMAIL)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_PROJECTION");
            sp.Command.AddParameter("@AMT_REQ_ID", SPVMMMAIL.ssp.SSP_REQ_ID, DbType.String);
            sp.Command.AddParameter("@CNY_CODE", SPVMMMAIL.ssp.SSP_CNY_CODE);
            sp.Command.AddParameter("@YEAR", SPVMMMAIL.ssp.SSP_YEAR, DbType.String);
            sp.Command.AddParameter("@VER_CODE", SPVMMMAIL.ssp.SSP_VER_CODE, DbType.String);
            sp.Command.AddParameter("@SSP_REQ_REM", SPVMMMAIL.ssp.SSP_REQ_REM, DbType.String);
            sp.Execute();
        }
        catch
        {
            throw;
        }
    }
 }