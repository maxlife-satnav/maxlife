﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;


/// <summary>
/// Summary description for SpaceConsolidatedReportService
/// </summary>
public class SpaceConsolidatedReportService
{
    SubSonic.StoredProcedure sp;
    List<SpaceConsolidatedReportVM> rptByUserlst;
    //HDMReport_Bar_Graph rptBarGph;
    DataSet ds;

    public object BindGrid()
    {
        rptByUserlst = GetReportList();

        if (rptByUserlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = rptByUserlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<SpaceConsolidatedReportVM> GetReportList()
    {

        List<SpaceConsolidatedReportVM> rptByUserlst = new List<SpaceConsolidatedReportVM>();
        SpaceConsolidatedReportVM rptByUser;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_SM");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByUser = new SpaceConsolidatedReportVM();
                rptByUser.CNY_NAME = sdr["CNY_NAME"].ToString();
                rptByUser.ZN_NAME = sdr["ZN_NAME"].ToString();
                rptByUser.STE_NAME = sdr["STE_NAME"].ToString();
                rptByUser.CTY_NAME = sdr["CTY_NAME"].ToString();
                rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                rptByUser.FLR_NAME = sdr["FLR_NAME"].ToString();
                rptByUser.VACANT = Convert.ToInt32(sdr["VACANT"]);
                rptByUser.ALLOCATED = Convert.ToInt32(sdr["ALLOCATED"]);
                rptByUser.OCCUPIED = Convert.ToInt32(sdr["OCCUPIED"]);
                rptByUser.TOTAL = Convert.ToInt32(sdr["TOTAL"]);
                rptByUserlst.Add(rptByUser);
            }
            sdr.Close();
        }
        if (rptByUserlst.Count != 0)
            return rptByUserlst;
        else
            return null;
    }

    public object SpaceConsolidatedChart()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_CHART_SM");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

        if (arr.Length != 0)
            return new { Message = MessagesVM.UM_OK, data = arr };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}