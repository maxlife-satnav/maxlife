﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UploadSpaceAllocationService
/// </summary>
public class UploadSpaceAllocationService
{
    public DataSet DownloadTemplate(UploadSpacesVM spcdet)
    {
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@DWNTYPE", SqlDbType.VarChar);
        param[2].Value = spcdet.ALLOCSTA;

        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_DWNLD_ALLOC_TEMPLATE", param);
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds;
    }

    public object UploadTemplate(HttpRequest httpRequest)
    {
        try
        {
            List<UploadAllocationDataVM> uadmlst = GetDataTableFrmReq(httpRequest);

            String jsonstr = httpRequest.Params["CurrObj"];

            UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);

            string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_SPACEALLOCATION_DATA (CITY,LOCATION,TOWER,FLOOR,SPACE_ID,SEATTYPE,VERTICAL,COSTCENTER,EMP_ID,FROM_DATE,TO_DATE,FROM_TIME,TO_TIME,UPLOADEDBY) VALUES ";
            foreach (UploadAllocationDataVM uadm in uadmlst)
            {
                str = str + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}'),",
                          uadm.City,
                          uadm.Location,
                          uadm.Tower,
                          uadm.Floor,
                          uadm.SpaceID,
                          uadm.SeatType,
                          uadm.Vertical.Replace("'","''"),
                          uadm.Costcenter.Replace("'", "''"),
                          uadm.EmployeeID,
                          uadm.FromDate,
                          uadm.ToDate,
                          uadm.FromTime,
                          uadm.ToTime,
                          HttpContext.Current.Session["Uid"]
                        );
            }
            str = str.Remove(str.Length - 1, 1);
            int retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str);
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@UPLTYPE", SqlDbType.VarChar);
                param[0].Value = UVM.UplAllocType;
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[1].Value = HttpContext.Current.Session["UID"];
                param[2] = new SqlParameter("@UPLOPTIONS", SqlDbType.VarChar);
                param[2].Value = UVM.UplOptions;

                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_UPLOAD_SPACE_ALLOC", param);
                return new { Message = MessagesVM.UAD_UPLOK, data = dt };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public List<UploadAllocationDataVM> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
        var uplst = CreateExcelFile.ReadAsList(filePath);
        //if (uplst.Count != 0)
            //uplst.RemoveAt(0);
        return uplst;

    }
}