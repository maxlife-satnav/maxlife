﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for ProposedLocationsService
/// </summary>
public class ProposedLocationsService
{
    LTLNG LNT;
   
    public object GetStatewiseLoc()
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_PROPOSED_EXISTING_COUNT");
        ds = sp.GetDataSet();
        return ds.Tables[0];

    }

    public object GetMarkers()
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PROPOSED_EXISTING_LOCATIONS_MARKERS");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    
}