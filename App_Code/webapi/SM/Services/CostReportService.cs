﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class CostReportService
{
    SubSonic.StoredProcedure sp;
    List<CostReportModel> rptByUserlst;
    //HDMReport_Bar_Graph rptBarGph;
    DataSet ds;
    public object GetCostReportObject()
    {
        try
        {
            rptByUserlst = GetCostReportList();

            if (rptByUserlst.Count != 0)
                return new { Message = MessagesVM.UM_NO_REC, data = rptByUserlst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<CostReportModel> GetCostReportList()
    {
        try
        {
            List<CostReportModel> rptByUserlst = new List<CostReportModel>();
            CostReportModel rptByUser;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_COST_REPORT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    rptByUser = new CostReportModel();
                    rptByUser.CNY_NAME = sdr["CNY_NAME"].ToString();
                    rptByUser.ZN_NAME = sdr["ZN_NAME"].ToString();
                    rptByUser.STE_NAME = sdr["STE_NAME"].ToString();
                    rptByUser.CTY_NAME = sdr["CTY_NAME"].ToString();
                    rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                    rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                    rptByUser.FLR_NAME = sdr["FLR_NAME"].ToString();
                    rptByUser.PE_NAME = sdr["PE_NAME"].ToString();
                    rptByUser.CHE_NAME = sdr["CHE_NAME"].ToString();
                    rptByUser.VER_NAME = sdr["VER_NAME"].ToString();
                    rptByUser.DEP_NAME = sdr["DEP_NAME"].ToString();
                    rptByUser.SHIFT = sdr["SHIFT"].ToString();
                    rptByUser.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                    rptByUser.ALLOC_SEATS_COUNT = sdr["ALLOC_SEATS_COUNT"].ToString();
                    rptByUser.UNITCOST = sdr["UNITCOST"].ToString();
                    rptByUser.TOTAL_BUSS_UNITCOST = Convert.ToDouble(sdr["TOTAL_BUSS_UNITCOST"]);
                    rptByUserlst.Add(rptByUser);
                }
                sdr.Close();
            }
            if (rptByUserlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
                return rptByUserlst;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }


    public object GetCostChart()
    {
        try
        {
            List<CostReportModel> area = new List<CostReportModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_COST_CHART_REPORT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public object GetCostVerticalChart()
    {
        try
        {
            List<CostReportModel> area = new List<CostReportModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_COST_VERTICAL_CHART_REPORT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}