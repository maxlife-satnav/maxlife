﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for SummaryReportService
/// </summary>
public class SummaryReportService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public string GetlblVerticalCC()
    {
        string sessvals;
        sessvals = Convert.ToString(HttpContext.Current.Session["Parent"]);       
        return sessvals;
    }

    public Object GetCityDetails()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GETACTIVECITIES");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public Object Getalllocations([FromBody] SummaryReportVM City)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATIONBY_CTY");
        sp.Command.Parameters.Add("@CTY_ID", City.CityId, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public Object Getalltowers([FromBody] SummaryReportVM Citytower)
    {
        
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_TOWER_LOCATION_CITY_NEW");
        sp.Command.Parameters.Add("@CITY", Citytower.CITY, DbType.String);
        sp.Command.Parameters.Add("@LOCATION", string.IsNullOrEmpty(Citytower.LOCATION) ? "--All--" : Citytower.LOCATION, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public Object Getallfloors([FromBody] SummaryReportVM Cityfloor)
    {

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_FLOOR_API");
        sp.Command.Parameters.Add("@CITY", Cityfloor.CITY, DbType.String);
        sp.Command.Parameters.Add("@LOCATION", string.IsNullOrEmpty(Cityfloor.LOCATION) ? "--All--" : Cityfloor.LOCATION, DbType.String);
        sp.Command.Parameters.Add("@TOWER", string.IsNullOrEmpty(Cityfloor.TOWER) ? "--All--" : Cityfloor.TOWER, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public Object Getverticals()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VERTICAL");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public Object Getconsolidatedseatdetails([FromBody] SummaryReportVM Consolidateddetails)
    {
        if (Consolidateddetails != null)
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CONSOLIDATED_SEAT_REPORT");
            sp.Command.Parameters.Add("@CITY", Consolidateddetails.CITY, DbType.String);
            sp.Command.Parameters.Add("@LCM_CODE", Consolidateddetails.LOCATION, DbType.String);
            sp.Command.Parameters.Add("@TWR_CODE", Consolidateddetails.TOWER, DbType.String);
            sp.Command.Parameters.Add("@FLR_CODE", Consolidateddetails.FLOOR, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables[0];
        }
        return null;
    }

    public Object GetSummaryDetails([FromBody] SummaryReportVM ObjGridSummary)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SPACE_SUMMARY_REPORT");
        sp.Command.Parameters.Add("@CITY", ObjGridSummary.CITY, DbType.String);
        sp.Command.Parameters.Add("@LCM_CODE", ObjGridSummary.LOCATION, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", string.IsNullOrEmpty(ObjGridSummary.TOWER) ? "--All--" : ObjGridSummary.TOWER, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", string.IsNullOrEmpty(ObjGridSummary.FLOOR) ? "--All--" : ObjGridSummary.FLOOR, DbType.String);
        ds = sp.GetDataSet();
        var e = ds.Tables[0].AsEnumerable();
        int OS = e.Sum(x => x.Field<int>("Occupied_Seats"));
        int ASeats = e.Sum(x => x.Field<int>("Allocated_Seats"));
        int VT = e.Sum(x => x.Field<int>("Allocated_but_not_occupied_Seats"));
        //string jsonstring = "['Allocated Total'," + Convert.ToString(ASeats) + "],['Occupied Total '," + Convert.ToString(OS) + "],['Allocated Vacant'," + Convert.ToString(VT) + "]";
        return new { Table = ds.Tables[0], ChartData = new dynamic[] { new { AllocatedTotal = ASeats }, new { OccupiedTotal = OS }, new { AllocatedVacant = VT } } };
    }

    public Object GetVerticalDetails([FromBody] SummaryReportVM ObjGridVertical)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VERTICAL_REPORT");
        sp.Command.Parameters.Add("@VER_CODE", ObjGridVertical.VERTICAL, DbType.String); 
        ds = sp.GetDataSet();
        var e = ds.Tables[0].AsEnumerable();
        int AS = e.Sum(x => x.Field<int>("ALLOCATED_SEATS"));
        //return new { Table = ds.Tables[0], VertChartData = new dynamic[] { new { verticalname = ds.Tables[0].Rows[0]["COST_CENTER_NAME"] }, new { allocatedseats = ds.Tables[0].Rows[0]["ALLOCATED_SEATS"] } } };
        return new { Table = ds.Tables[0], VertChartData = new dynamic[] { new { allocatedseats = AS } } };

    }
	
}