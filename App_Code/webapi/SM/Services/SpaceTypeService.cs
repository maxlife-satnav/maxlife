﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


public class SpaceTypeService
{
    SubSonic.StoredProcedure sp;
    //Insert
    public int Save(SpaceTypeModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACETYPE_INSERT");
            sp.Command.AddParameter("@SPC_CODE", model.SPC_Code, DbType.String);
            sp.Command.AddParameter("@SPC_NAME", model.SPC_Name, DbType.String);
            sp.Command.AddParameter("@SPC_STA_ID", model.SPC_Status_Id, DbType.String);
            sp.Command.AddParameter("@SPC_REM", model.SPC_REM, DbType.String);
            sp.Command.AddParameter("@SPC_COLOR", model.SPC_COLOR, DbType.String);
            sp.Command.Parameters.Add("@SPC_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch {
            throw;
        }
    }
    //Update
    public Boolean UpdateSpaceTypeData(SpaceTypeModel update)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_SPACE_TYPE_MASTER");
            sp.Command.AddParameter("@SPC_CODE", update.SPC_Code, DbType.String);
            sp.Command.AddParameter("@SPC_NAME", update.SPC_Name, DbType.String);
            sp.Command.AddParameter("@SPC_STA_ID", update.SPC_Status_Id, DbType.Int32);
            sp.Command.AddParameter("@SPC_REM", update.SPC_REM, DbType.String);
            sp.Command.AddParameter("@SPC_COLOR", update.SPC_COLOR, DbType.String);
            sp.Command.Parameters.Add("@SPC_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Execute();
            return true;
        }
        catch {
            throw;
        }
    }    
    //Bind Grid
    public IEnumerable<SpaceTypeModel> BindGridData()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_SPACE_TYPE_GRID").GetReader())
            {
                List<SpaceTypeModel> SPCcatlist = new List<SpaceTypeModel>();
                while (reader.Read())
                {
                    SPCcatlist.Add(new SpaceTypeModel()
                    {
                        SPC_Code = reader.GetValue(0).ToString(),
                        SPC_Name = reader.GetValue(1).ToString(),
                        SPC_Status_Id = reader.GetValue(2).ToString(),
                        SPC_REM = reader.GetValue(3).ToString(),
                        SPC_COLOR = reader.GetValue(4).ToString()
                    });
                }
                reader.Close();
                return SPCcatlist;
            }
        }
        catch {
            throw;
        }
    }
}