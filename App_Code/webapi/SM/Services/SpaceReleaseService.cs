﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceReleaseService
/// </summary>
public class SpaceReleaseService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public object GetSpacesToRelease(SPC_RELEASE_FLOOR_VERTICAL_SPACE_LIST srvm)
    {
        try
        {
            List<SMS_SPACE_RELEASE_VM> SEDETLST = new List<SMS_SPACE_RELEASE_VM>();
            SMS_SPACE_RELEASE_VM SEDET;

            SqlParameter[] param = new SqlParameter[14];

            param[0] = new SqlParameter("@CNYLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable((srvm.selectedCountries == null) ? new List<Countrylst>() : srvm.selectedCountries);
            param[1] = new SqlParameter("@CTYLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable((srvm.selectedCities == null) ? new List<Citylst>() : srvm.selectedCities);
            param[2] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable((srvm.selectedLocations == null) ? new List<Locationlst>() : srvm.selectedLocations);
            param[3] = new SqlParameter("@TWRLST", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable((srvm.selectedTowers == null) ? new List<Towerlst>() : srvm.selectedTowers);
            param[4] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable((srvm.selectedFloors==null)? new List<Floorlst>():srvm.selectedFloors);
            param[5] = new SqlParameter("@VERLST", SqlDbType.Structured);
            param[5].Value = UtilityService.ConvertToDataTable((srvm.selectedVerticals==null)?new List<Verticallst>():srvm.selectedVerticals);
            param[6] = new SqlParameter("@CCLST", SqlDbType.Structured);
            param[6].Value = UtilityService.ConvertToDataTable((srvm.selectedCostcenters==null)?new List<Costcenterlst>():srvm.selectedCostcenters);

            param[7] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["UID"];

            param[8] = new SqlParameter("@From_Date", SqlDbType.DateTime);
            param[8].Value = srvm.FromDate;

            param[9] = new SqlParameter("@To_Date", SqlDbType.DateTime);
            param[9].Value = srvm.ToDate;
            
            if (srvm.RELEASE_STA_ID == 1050)
                srvm.RELEASE_STA_ID = 1003;
            else
                srvm.RELEASE_STA_ID = 1004;

            param[10] = new SqlParameter("@RELEASE_STA_ID", SqlDbType.Int);
            param[10].Value = srvm.RELEASE_STA_ID;
            param[11] = new SqlParameter("@EMP_ID", SqlDbType.NVarChar);
            param[11].Value = srvm.EMP_ID;

            param[12] = new SqlParameter("@ZNLST", SqlDbType.Structured);
            param[12].Value = UtilityService.ConvertToDataTable((srvm.selectedZones == null) ? new List<Zonelst>() : srvm.selectedZones);
            param[13] = new SqlParameter("@STELST", SqlDbType.Structured);
            param[13].Value = UtilityService.ConvertToDataTable((srvm.selectedStates == null) ? new List<Statelst>() : srvm.selectedStates);

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SPACES_TO_RELEASE", param))
            {
                while (sdr.Read())
                {
                    SEDET = new SMS_SPACE_RELEASE_VM();
                    SEDET.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                    SEDET.SSED_SPC_ID = sdr["SSA_SPC_ID"].ToString();
                    SEDET.SSAD_FROM_DATE = Convert.ToDateTime(sdr["SSAD_FROM_DATE"]);
                    SEDET.SSAD_TO_DATE = Convert.ToDateTime(sdr["SSAD_TO_DATE"]);
                    SEDET.SSAD_FROM_TIME = Convert.ToDateTime(sdr["SSAD_FROM_TIME"]);
                    SEDET.SSAD_TO_TIME = Convert.ToDateTime(sdr["SSAD_TO_TIME"]);
                    SEDET.SSA_SPC_TYPE = sdr["SSA_SPC_TYPE"].ToString();
                    SEDET.SSA_SPC_SUB_TYPE = sdr["SSA_SPC_SUB_TYPE"].ToString();
                    SEDET.SH_NAME = sdr["SH_NAME"].ToString();
                    SEDET.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                    SEDET.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                    SEDET.SSED_AUR_ID = sdr["AUR_ID"].ToString();
                    SEDET.SSED_VER_NAME = sdr["VER_NAME"].ToString();
                    SEDET.EMP_DESIGNATION = sdr["DSN_AMT_TITLE"].ToString();
                    SEDET.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                    SEDET.SSAD_AUR_ID = sdr["SSAD_AUR_ID"].ToString();
                    SEDET.STATUS = "Default";
                    SEDET.SSED_EXTN_DT = null;
                    SEDET.ticked = false;
                    SEDET.SSAD_ID = Convert.ToInt32(sdr["SSAD_ID"]);
                    SEDET.SPC_NAME = sdr["SPC_NAME"].ToString();
                    SEDET.SPC_FLR_ID = sdr["SPC_FLR_ID"].ToString();
                    SEDET.lat = sdr["lat"].ToString();
                    SEDET.lon = sdr["lon"].ToString();
                    SEDETLST.Add(SEDET);
                }
                sdr.Close();
            }

            if (SEDETLST.Count != 0)
                return new { Message = MessagesVM.SER_OK, data = SEDETLST };
            else
                return new { Message = MessagesVM.SER_OK, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public object ReleaseSelectedSpaces(SpaceReleaseVM srvm)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@SPACE_RELEASE_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(srvm.SSR);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@REMARKS", SqlDbType.NVarChar);
            param[2].Value = srvm.Remarks;
            param[3] = new SqlParameter("@RELEASE_STA_ID", SqlDbType.Int);
            param[3].Value = srvm.RELEASE_STA_ID;

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_UPDATE_SSA_AND_INSERT_SPACE_RELEASE", param))
            {

                if (dr.Read())
                {
                    return new { Message = MessagesVM.SR_SUCCESS + " For : " + dr["REQID"].ToString(), data = srvm };
                }
                return new { Message = dr["REQID"].ToString(), data = (object)null };
            }

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

}