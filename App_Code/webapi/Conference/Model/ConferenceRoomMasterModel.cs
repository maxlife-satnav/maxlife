﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class ConferenceRoomMasterModel
{
    public string CONFERENCE_ROOM_CODE { get; set; }
    public string CONFERENCE_ROOM_NAME { get; set; }
    public string CONFERENCE_CITY_CODE { get; set; }
    public string CONFERENCE_CITY { get; set; }
    public string CONFERENCE_LOCATION_CODE { get; set; }
    public string CONFERENCE_LOCATION { get; set; }
    public string CONFERENCE_TOWER_CODE { get; set; }
    public string CONFERENCE_TOWER { get; set; }
    public string CONFERENCE_FLOOR_CODE { get; set; }
    public string CONFERENCE_FLOOR { get; set; }
    public int CONFERENCE_CAPACITY { get; set; }
    public string CONFERENCE_TYPE { get; set; }
    public string CONFERENCE_TYPE_NAME { get; set; }
    public string CONFERENCE_STATUS { get; set; }
}

public class ConferenceType
{
    public string CONF_CODE { get; set; }
    public string CONF_NAME { get; set; }   
}

public class ConferenceLocationStructure
{
    public List<Floorlst> Structure { get; set; }
    public ConferenceRoomMasterModel Model { get; set; }
}