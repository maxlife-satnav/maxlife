﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class FacilityDetails 
{ 
    public List<Locationlst> LocatinoList { get; set; }
    public FacilityMasterModel FMastr { get; set; }
}

public class FacilityMasterModel
{
    public string FACILITY_CODE {get; set;}
    public string FACILITY_NAME {get; set;}
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public int FACILITY_UNIT_COST {get; set;}
    public string FACILITY_REMARKS {get; set;}
    public string FACILITY_STATUS { get; set; }
}