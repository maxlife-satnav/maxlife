﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class GuestHouseDBAPIController : ApiController
{

    GuestHouseDBModuleService service = new GuestHouseDBModuleService();

    [HttpGet]
    public object GetFacilitiesCountbyLocation()
    {
        return service.GetLocationwiseFacilitiesCount();
    }

    [HttpGet]
    public object GetTotalRequestsCount()
    {
        return service.GetTotalReqCount();
    }
    [HttpGet]
    public object GetTotalRequestsDetails()
    {
        return service.GetTotalReqDetails();
    }
    [HttpGet]
    public object GetWithHoldRequestsCount()
    {
        return service.GetWithHoldCount();
    }
    [HttpGet]
    public object GetWithHoldRequestsDetails()
    {
        return service.GetWithHoldDetails();
    }
    [HttpGet]
    public object GHUtilization()
    {
        return service.GetGHUtilization();
    }

    [HttpGet]
    public HttpResponseMessage GetFacilitiesforPie([FromUri] string lcmname)
    {
        var obj = service.GetFaciilitiesBylcmName(lcmname);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetGHDetailsForBarChart([FromUri] string category, [FromUri] string location)
    {
        var obj = service.GetRequestDetailsforBarGraph(category, location);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
