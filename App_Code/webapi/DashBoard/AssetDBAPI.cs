﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class AssetDBAPIController : ApiController
{
    AssetDBModuleService service = new AssetDBModuleService();

    [HttpGet]
    public object GetAssetTypes()
    {
        return service.GetAssetTypes();
    }

    [HttpGet]
    public object GetMapunmapassets()
    {
        return service.GetMapunmapassets();
    }

}
