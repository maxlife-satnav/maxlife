﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class HelpDeskDBAPIController : ApiController
{
    HelpDeskDBModuleService service = new HelpDeskDBModuleService();
    // GET api/<controller>
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }

    //Req raised for last 7 days
    [HttpGet]
    public object GetHDreqWeekly()
    {
       return service.GetHDreqWeekly();
    }
    
    [HttpGet]
    public HelpDeskDBModel.SLADetails GetSLACount()
    {
        return service.GetSLACount();
    }

    //Location-Wise Pending Services
    [HttpGet]
    public object BindHDCategories()
    {
        return service.BindHDCategories();
    }

}
