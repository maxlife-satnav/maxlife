﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ConferenceDBAPIController : ApiController
{
    ConferenceDBModuleService service = new ConferenceDBModuleService();

    [HttpGet]
    public object GetcitywiseconfCount()
    {
        return service.GetcitywiseconfCount();
    }

    [HttpGet]
    public object GetcitywiseUtil()
    {
        return service.GetcitywiseUtil();
    }

    [HttpGet]
    public object GetConfUtil()
    {
        return service.GetConfUtil();
    }
}
