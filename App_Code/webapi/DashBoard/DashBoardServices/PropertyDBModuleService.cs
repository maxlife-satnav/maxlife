﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyDBModuleService
/// </summary>
public class PropertyDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public PropertyDBModuleService()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Properties Count - pie
    public object[] BindProperties(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_PROPERTIES_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
     
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }

    public object BindAmounts(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_PAID_PENDING_AMOUNT_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
       
        ds = sp.GetDataSet();

        return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] }, new { WRExpenses = ds.Tables[0].Rows[2]["Total"] } } };
    }

    public object GetWorkRequests()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_WORKREQUESTS_COUNT_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        return ds;
        //return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] }, new { WRExpenses = ds.Tables[0].Rows[2]["Total"] } } };
    }

    public DataTable GetExpLeasesDetails(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_Expiry_Leases_Dashboard");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
       
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //Next 

    //Received and Pending Amounts (Current Year) -bar
    public object GetLeaseDueDetails(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_LEASE_DUES_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
      
        ds = sp.GetDataSet();
        return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] } } };

    }

    public DataTable GetPropDetailsByPropName(string PropName, int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_Prop_Details_By_Prop_Name");
        sp.Command.AddParameter("@PROP_NAME", PropName, DbType.String);
     
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //Tenant View Details
    public DataTable GetPendingAndReceivedAmounts(string category, int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "Get_Pending_And_Received_Amounts");
        sp.Command.AddParameter("@CATEGORY", category, DbType.Int32);
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
       
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    //Lease bar graph View Details
    public DataTable GetDueLeaseReceivedAmounts(int category)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "VIEW_LEASE_DUES_DETAILS_DB");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@CATEGORY", category, DbType.Int32);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //public DataSet GetProperty_Details()
    //{
    //    sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_USER_PROPERTIES_DASHBOARD");
    //    sp.Command.AddParameter("@USER_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
    //    ds = sp.GetDataSet();
    //    return ds;
    //}

    public object[] BindCityWise_Properties()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_CITYWISE_PROPERTIES");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }

    public DataTable GetProperties_City(string cityName)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_PROEPRTIES_BY_CITY");
        sp.Command.AddParameter("@CITY_NAME", cityName, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataSet GetTenant_Vacancies(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TENANT_VACANCIES");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
       
        ds = sp.GetDataSet();
        return ds;
    }

    public DataSet Get_Vacant_Properties(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_VACANT_PROPERTIES");
       
        ds = sp.GetDataSet();
        return ds;
    }

    public static dynamic GetDynamicObject(Dictionary<string, object> properties)
    {
        var dynamicObject = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
        foreach (var property in properties)
        {
            dynamicObject.Add(property.Key, property.Value);
        }
        return dynamicObject;
    }

    public object ExpenseUtilityChartByLoc(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "EXPENSE_UTILITY_DASHBOARD");
      
        ds = sp.GetDataSet();

        List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[2]).Distinct().ToList();
        List<dynamic> list = new List<dynamic>();

        foreach (string location in locs)
        {
            Dictionary<String, Object> properties = new Dictionary<string, object>();
            properties.Add("name", location);

            var strCategory = ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("LOCATION") == location))
                       .Select(r => new { ExpenseHead = r.Field<string>("EXP_NAME"), Amount = r.Field<decimal>("AMOUNT") }).ToList();
            for (int i = 0; i < strCategory.Count; i++)
            {
                properties.Add(strCategory[i].ExpenseHead, strCategory[i].Amount);
            }
            dynamic obj = GetDynamicObject(properties);
            list.Add(obj);
        }

        List<object> serviceslist = ds.Tables[1].Rows.Cast<DataRow>().Select(r => (r.ItemArray[0])).ToList();
        //  return new { shift = listParent, ttlshift = totalcount };
        return new { locVal = Newtonsoft.Json.JsonConvert.SerializeObject(list), services = Newtonsoft.Json.JsonConvert.SerializeObject(serviceslist) };
    }

    public DataTable GetExpenseUtilityDetailsByLoc(string location, string expense)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_EXPENSE_UTILITY_DETAILS");
        sp.Command.AddParameter("@LOCATION", location, DbType.String);
        sp.Command.AddParameter("@EXPENSE", expense, DbType.String);
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
}