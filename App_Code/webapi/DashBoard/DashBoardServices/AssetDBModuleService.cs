﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssetDBModuleService
/// </summary>
public class AssetDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
	
    public object[] GetAssetTypes()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_ASSETS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();        
        return arr;
    }

    public object[] GetMapunmapassets()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_ASSETS_BYLOCATION_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }
}