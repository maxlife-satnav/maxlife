﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceDBModuleService
/// </summary>
public class SpaceDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object bindWorkstation()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_WS_REPORT");
        ds = sp.GetDataSet();

        //Total WS Count
        int totalcount = ds.Tables[0].AsEnumerable().Sum(x => x.Field<int>("cnt"));

        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();
        
        //Distinct Countries
        List<object> countries = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();

        foreach (string country in countries)
        {
            Loclist.Add(country);

            List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[2])).ToList();
            listLocCount.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CNY_NAME") == country)).Sum(x => x.Field<int>("cnt")));
            
            //Adding Locations 
            foreach (string location in locs)
            {
                Loclist.Add(location);
            }

            //Adding WS
            foreach (int loccount in ReqCount)
            {
                listLocCount.Add(loccount);
            }
        }
        Loclist.Insert(0, "x");
        listLocCount.Insert(0, "Total Seats");

        //Adding Locations/WS-Count to parent List
        listParent.Add(Loclist);
        listParent.Add(listLocCount);
        return new {ws= listParent, ttlcount= totalcount };
    }

    public object bindCabin()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_FC_REPORT");
        ds = sp.GetDataSet();

        //Total WS Count
        int totalcount = ds.Tables[0].AsEnumerable().Sum(x => x.Field<int>("Cabins"));

        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();

        //Distinct Countries
        List<object> countries = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[2]).Distinct().ToList();

        foreach (string country in countries)
        {
            //Loclist.Add(country);
            Loclist.Add(".");

            List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[0])).ToList();
            List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            listLocCount.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CNY_NAME") == country)).Sum(x => x.Field<int>("Cabins")));

            //Adding Locations 
            foreach (string location in locs)
            {
                //Loclist.Add(location);
                Loclist.Add(".");
            }

            //Adding Cabins
            foreach (int loccount in ReqCount)
            {
                listLocCount.Add(loccount);
            }
        }
        Loclist.Insert(0, "x");
        listLocCount.Insert(0, "Total Cabins");

        //Adding Locations/Cabins-Count to parent List
        listParent.Add(Loclist);
        listParent.Add(listLocCount);
        return new { cabins = listParent, ttlcabinscount = totalcount };
    }

    public object bindFTE()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_FTE_REPORT");
        ds = sp.GetDataSet();

        //Total WS Count
        int totalcount = ds.Tables[0].AsEnumerable().Sum(x => int.Parse(x.Field<string>("FTE")));        
        
        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();

        //Distinct Countries
        List<object> countries = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[2]).Distinct().ToList();

        foreach (string country in countries)
        {
            //Loclist.Add(country);
            Loclist.Add(country);

            List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[0])).ToList();
            //List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            listLocCount.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CNY_NAME") == country)).Sum(x => int.Parse(x.Field<string>("FTE"))));

            //Adding Locations 
            foreach (string location in locs)
            {
                //Loclist.Add(location);
                Loclist.Add(location);
            }

            //Adding FTE
            foreach (string loccount in ReqCount)
            {
                listLocCount.Add(loccount);
            }
        }
        Loclist.Insert(0, "x");
          //listLocCount.Insert(0, "Seat Utilization(Occupied/Total Seats) ");
        listLocCount.Insert(0, "Occupied by Function/Employee(s) ");

        //Adding Locations/FTE-Count to parent List
        listParent.Add(Loclist);
        listParent.Add(listLocCount);
        return new { FTE = listParent, ttlFTE = totalcount };
    }
    public object bindShift()
    {
      
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_VACANT_COUNT");//for vacant count 1002 status
      
        ds = sp.GetDataSet();
       
        int totalcount = ds.Tables[0].AsEnumerable().Sum(x => int.Parse(x.Field<string>("FTE")));

        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();

     
        List<object> countries = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[2]).Distinct().ToList();
        foreach (string country in countries)
        {
            Loclist.Add(country);

            List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[0])).ToList();         
            List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            listLocCount.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CNY_NAME") == country)).Sum(x => int.Parse(x.Field<string>("FTE"))));

          
            foreach (string location in locs)
            {

                Loclist.Add(location);
            }

        
            foreach (string loccount in ReqCount)
            {
                listLocCount.Add(loccount);
            }
        }
        Loclist.Insert(0, "x");
    
        listLocCount.Insert(0, "Occupied by BU");

     
        listParent.Add(Loclist);
        listParent.Add(listLocCount);
        return new { shift = listParent, ttlshift = totalcount };
    }

    //public object bindShift()
    //{
    //    //sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_SHIFT_REPORT");
  
    //    ds = sp.GetDataSet();

    //    //Total WS Count
    //    string totalcount = Convert.ToString(ds.Tables[0].Rows[0]["TOT"]);

    //    List<object> Loclist = new List<object>();
    //    List<object> listLocCount = new List<object>();
    //    List<object> listParent = new List<object>();

       
    //    Loclist = ds.Tables[1].AsEnumerable().Select(r => r.ItemArray[0]).ToList();

    //    var resultList = Loclist.Select(x => string.Format("{0}", ".")).ToList();

    //    listLocCount = ds.Tables[1].AsEnumerable().Select(r => r.ItemArray[1]).ToList();

    //    resultList.Insert(0, "x");
    //    //listLocCount.Insert(0, "Seat Utilization(Occupied/Total Seats) ");
    //    listLocCount.Insert(0, "Vacant Seats) ");

    //    listParent.Add(resultList);
    //    listParent.Add(listLocCount);

    //    return new { shift = listParent, ttlshift = totalcount };
    //}
}
