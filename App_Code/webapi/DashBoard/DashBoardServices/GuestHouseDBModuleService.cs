﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GuestHouseDBModuleService
/// </summary>
public class GuestHouseDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public object[] GetLocationwiseFacilitiesCount()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GH_GET_FACILITIES_COUNT_BY_LOCATION_DASHBOARD");
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }


    public object[] GetTotalReqCount()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_REQUESTS_COUNT_GH_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }


    public DataSet GetTotalReqDetails()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_GH_REQUESTS_DETAILS_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();

        return ds;
    }

    public object[] GetWithHoldCount()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_WITHHOLD_REQUESTS_COUNT_GH_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }

    public DataSet GetWithHoldDetails()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_GH_WITHHOLD_REQUESTS_DETAILS_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();

        
        return ds;
    }
    public static dynamic GetDynamicObject(Dictionary<string, object> properties)
    {
        var dynamicObject = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
        foreach (var property in properties)
        {
            dynamicObject.Add(property.Key, property.Value);
        }
        return dynamicObject;
    }
    public object GetGHUtilization()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_GH_UTILIZATION_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();

        List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[2]).Distinct().ToList();
        List<dynamic> list = new List<dynamic>();

        foreach (string location in locs)
        {
            Dictionary<String, Object> properties = new Dictionary<string, object>();
            properties.Add("name", location);

            var strCategory = ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("LCM_NAME") == location))
                       .Select(r => new { ServiceName = r.Field<string>("TOT"), PendingCount = r.Field<int>("REQ_COUNT") }).ToList();
            for (int i = 0; i < strCategory.Count; i++)
            {
                properties.Add(strCategory[i].ServiceName, strCategory[i].PendingCount);
            }
            dynamic obj = GetDynamicObject(properties);
            list.Add(obj);
        }

        List<object> serviceslist = ds.Tables[1].Rows.Cast<DataRow>().Select(r => (r.ItemArray[0])).ToList();
        //  return new { shift = listParent, ttlshift = totalcount };
        return new { locVal = Newtonsoft.Json.JsonConvert.SerializeObject(list), services = Newtonsoft.Json.JsonConvert.SerializeObject(serviceslist) };  
    }


    public DataTable GetFaciilitiesBylcmName(string lcmName)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_FACILITIES_GH_PIE_CHART");
        sp.Command.AddParameter("@RF_NAME", lcmName, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetRequestDetailsforBarGraph( string category, string location)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_BOOKED_DETIAILS_FOR_BAR_CHART");
        sp.Command.AddParameter("@BOOKED_TYPE", category, DbType.String);
        sp.Command.AddParameter("@LCM_NAME", location, DbType.String);  
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

}