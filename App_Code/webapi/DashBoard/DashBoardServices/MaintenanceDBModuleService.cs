﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MaintenanceDBModuleService
/// </summary>
public class MaintenanceDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object[] BindAMCTypes()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETAMCTYPES_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
       //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => new { Name = r.ItemArray[1], total = r.ItemArray[0] }).ToArray();
        return arr;       
    }

    public DataTable GetAMCAmounts()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETPAYMENT_ADVICE");
        sp.Command.AddParameter("@TYPE", "1", DbType.String);
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetAMCAssetsExp()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_AMC_EXPIRYASSETS_DASHBOARD");        
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object BindAMCNonAMCBarCh()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_AMC_NONAMC_LOC_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();               
        
        //Locations
        //object Locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[0].ToString()) ? r.ItemArray[1].ToString().Insert(0, "'").Insert(r.ItemArray[1].ToString().Length + 1, "'") : r.ItemArray[0].ToString().Insert(0, "'").Insert(r.ItemArray[0].ToString().Length + 1, "'"))).ToArray();        
        List<object> Locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[0].ToString()) ? r.ItemArray[1] : r.ItemArray[0])).ToList();
        Locs.Insert(0, "x");
        //AMCS
        List<object> AMCs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[2].ToString()) ? 0 : r.ItemArray[2])).ToList();
        AMCs.Insert(0, "AMCS");
        //NONAMCS
        List<object> NonAMCs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[3].ToString()) ? 0 : r.ItemArray[3])).ToList();
        NonAMCs.Insert(0, "NONAMCS");

        List<Object> list = new List<object>();
        list.Add(Locs);
        list.Add(AMCs);
        list.Add(NonAMCs);
        return list;
    }
}