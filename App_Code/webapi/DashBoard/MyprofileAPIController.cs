﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Http;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/// <summary>
/// Summary description for MyprofileAPIController
/// </summary>
public class MyprofileAPIController: ApiController
{
    [HttpPost]
    public string SaveImagetoDB()
    {
        bool filesfound=HttpContext.Current.Request.Files.AllKeys.Any();
        if (filesfound)
        {
            for (int i = 0; i < HttpContext.Current.Request.Files.Count;i++)
            {
                HttpPostedFile file = HttpContext.Current.Request.Files[i];
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/userprofiles"), file.FileName);
                file.SaveAs(fileSavePath);                
                SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Update_Get_ProfileImage");
                sp1.Command.AddParameter("@aur_id", HttpContext.Current.Session["UID"], DbType.String);
                sp1.Command.AddParameter("@path", file.FileName, DbType.String);
                sp1.ExecuteScalar();                
            }
            return "Profile Picture Uploaded Successfully.";
        }
        return "File Not Selected";
    }
}

public class MyporfileVM
{
    public object image { get; set; }
}