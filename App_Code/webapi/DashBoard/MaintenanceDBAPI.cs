﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class MaintenanceDBAPIController : ApiController
{
    MaintenanceDBModuleService service=new MaintenanceDBModuleService();
    // GET api/<controller>
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }

    //AMC Types PIE
    [HttpGet]    
    public object[] BindAMCTypes()
    {
        return service.BindAMCTypes();
    }

    //Assets Loc Wise Bar Chart
    [HttpGet]
    public object BindAMCNonAMCBarCh()
    {
        return service.BindAMCNonAMCBarCh();
    }

    // AMC AMOUNT RECEIVED CURR MONTH
    [HttpGet]
    public DataTable GetAMCAmounts()
    {
        return service.GetAMCAmounts();
    }

    [HttpGet]
    public DataTable GetAMCAssetsExp()
    {
        return service.GetAMCAssetsExp();
    }
}
