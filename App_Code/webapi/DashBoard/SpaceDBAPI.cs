﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SpaceDBAPIController : ApiController
{
    SpaceDBModuleService service = new SpaceDBModuleService();
    // GET api/<controller>
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
    //bindWorkstation
    [HttpGet]
    public object bindWorkstation()
    {
        return service.bindWorkstation();
    }

    //Bind Cabins
    [HttpGet]
    public object bindCabin()
    {
        return service.bindCabin();
    }

    //Bind Occupied Employees
    [HttpGet]    
    public object bindFTE()
    {
        return service.bindFTE();
    }

    //Bind Seat Utlization
    [HttpGet]
    public object bindShift()
    {
        return service.bindShift();
    }

}
