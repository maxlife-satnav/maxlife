﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for TenantReportService
/// </summary>
public class TenantPaymentReportService
{
    SubSonic.StoredProcedure sp;
    List<TenantPaymentReportModel> Tenlist;
    TenantPaymentReportModel Tenant;
    DataSet ds;
    public object GetLeaseData(TenantPaymentReportVM Propdetails)
    {
        try
        {
            Tenlist = TenantReportDetails(Propdetails);
            if (Tenlist.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Tenlist };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<TenantPaymentReportModel> TenantReportDetails(TenantPaymentReportVM Propdetails)
    {
        try
        {
            List<TenantPaymentReportModel> TenData = new List<TenantPaymentReportModel>();

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@FRMDT", SqlDbType.DateTime);
            param[0].Value = Propdetails.FromDate;
            param[1] = new SqlParameter("@TODT", SqlDbType.DateTime);
            param[1].Value = Propdetails.ToDate;
            param[2] = new SqlParameter("@AURID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_GET_TEN_PAYMENT_RPT", param))
            {
                while (reader.Read())
                {
                    Tenant = new TenantPaymentReportModel();
                    Tenant.TENANT = reader["TENANT"].ToString();
                    Tenant.PRP_NAME = reader["PM_PPT_NAME"].ToString();
                    Tenant.RENT = Convert.ToDecimal(reader["PM_RP_TEN_RENT"]);
                    Tenant.MAIN_FEE = Convert.ToDecimal(reader["PM_RP_TEN_MAINT_FEES"]);
                    Tenant.TOT_RENT = Convert.ToDecimal(reader["PM_RP_OUST_AMOUNT"]);
                    Tenant.PAIDDATE = reader["PAID_DATE"].ToString();
                    Tenant.FROMDATE = reader["PM_RP_FROMDATE"].ToString();
                    Tenant.TODATE = reader["PM_RP_TODATE"].ToString();
                    Tenant.TDS = reader["PM_RP_TDS_VALUE"].ToString();
                    Tenant.PAY_MODE = reader["PM_RP_PAY_MODE"].ToString();
                    Tenant.CHEQUENO = reader["PM_RP_CHEQUENO"].ToString();
                    Tenant.CHEQUEDATE = reader["PM_RP_CHEQUEDATE"].ToString();
                    Tenant.ISS_BANK = reader["PM_RP_ISSUINGBANK"].ToString();
                    Tenant.IFSC = reader["PM_RP_IFCB"].ToString();
                    Tenant.ACCNUM = reader["PM_RP_ACCOUNTNUM"].ToString();
                    TenData.Add(Tenant);
                }
                reader.Close();
            }
            return TenData;
        }
        catch
        {
            throw;
        }
    }
	
}