﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for Prop_Ins_ValidityService
/// </summary>
public class Prop_Ins_ValidityService
{
    public object GetGriddata(Prop_Ins_Validity_Details InsDetails)
    {
        List<Prop_Ins_ValidityModel> inslst = GetInsList(InsDetails);
        if (inslst.Count != 0)
        {
            return new { Message = MessagesVM.SER_OK, data = inslst };
        }
        else
        {
            return new { Message = MessagesVM.SER_OK, data = (object)null };
        }

    }

    public List<Prop_Ins_ValidityModel> GetInsList(Prop_Ins_Validity_Details InsDetails)
    {
        List<Prop_Ins_ValidityModel> insdet = new List<Prop_Ins_ValidityModel>();
        Prop_Ins_ValidityModel insobj;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@FROM_DATE", SqlDbType.NVarChar);
        param[1].Value = InsDetails.FROM_DATE;
        param[2] = new SqlParameter("@TO_DATE", SqlDbType.NVarChar);
        param[2].Value = InsDetails.TO_DATE;

        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_INSURACE_VALIDITY", param))
        {
            while (reader.Read())
            {
                insobj = new Prop_Ins_ValidityModel();
                insobj.INS_AMOUNT = Convert.ToDouble(reader["INS_AMOUNT"]);
                insobj.INS_END_DT = string.IsNullOrEmpty(Convert.ToString(reader["INS_END_DT"])) ? (DateTime?)null : Convert.ToDateTime(reader["INS_END_DT"]);
                insobj.INS_PNO = Convert.ToString(reader["INS_PNO"]);
                insobj.INS_START_DT = string.IsNullOrEmpty(Convert.ToString(reader["INS_START_DT"])) ? (DateTime?)null : Convert.ToDateTime(reader["INS_START_DT"]);
                insobj.INS_TYPE = Convert.ToString(reader["INS_TYPE"]);
                insobj.INS_VENDOR = Convert.ToString(reader["INS_VENDOR"]);
                insobj.OWNER = Convert.ToString(reader["OWNER"]);
                insobj.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                insobj.PPT_ADDR = Convert.ToString(reader["PPT_ADDR"]);
                insobj.PPT_NAME = Convert.ToString(reader["PPT_NAME"]);
                insobj.PPT_TYPE = Convert.ToString(reader["PPT_TYPE"]);
                insdet.Add(insobj);
            }
        }
        return insdet;
    }
}