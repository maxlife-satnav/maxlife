﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class LeaseRentalReportService
{

    SubSonic.StoredProcedure sp;
    List<LeaseRentalData> Cust;
    LeaseRentalData Rent;
    DataSet ds;

    public object GetRentalObject(LeaseRentalDetails Det)
    {
        try
        {
            Cust = GetRentalDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<LeaseRentalData> GetRentalDetails(LeaseRentalDetails Details)
    {
        try
        {
            List<LeaseRentalData> CData = new List<LeaseRentalData>();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@ZONELST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Details.zonlst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[2].Value = Details.FromDate;
            param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[3].Value = Details.ToDate;


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_RENTAL_REPORT", param))
            {
                while (reader.Read())
                {
                    Rent = new LeaseRentalData();
                    Rent.LEASE_NAME = Convert.ToString(reader["LEASE_NAME"]);
                    Rent.ZN_CODE = Convert.ToString(reader["ZN_CODE"]);
                    Rent.ZN_NAME = Convert.ToString(reader["ZN_NAME"]);
                    Rent.LOCATION_CODE = Convert.ToString(reader["LOCATION_CODE"]);
                    Rent.LOCATION_NAME = Convert.ToString(reader["LOCATION_NAME"]);
                    Rent.LANDLORD_CODE = Convert.ToString(reader["LANDLORD_CODE"]);
                    Rent.LANDLORD_NAME = Convert.ToString(reader["LANDLORD_NAME"]);
                    Rent.LEASE_START_DATE = (DateTime)reader["LEASE_START_DATE"];
                    Rent.LEASE_END_DATE = (DateTime)(reader["LEASE_END_DATE"]);
                    //Rent.TYPE_OF_AGREEMENT = Convert.ToString(reader["TYPE_OF_AGREEMENT"]);
                    Rent.RENT = Convert.ToDouble(reader["RENT"]);
                    Rent.MAINTENANCE = Convert.ToDouble(reader["MAINTENANCE"]);
                    Rent.OTHER = Convert.ToDouble(reader["OTHER"]);
                    Rent.GROSS_MONTHLY_RENT = Convert.ToDouble(reader["GROSS_MONTHLY_RENT"]);
                    Rent.SECURITY_DEPOSIT = Convert.ToDouble(reader["SECURITY_DEPOSIT"]);

                    Rent.REMARKS = Convert.ToString(reader["REMARKS"]);
                    Rent.RENTAL_PAYMENT_VALIDATION = Convert.ToString(reader["RENTAL_PAYMENT_VALIDATION"]);

                    Rent.CARRY_FORWARD_RENT = Convert.ToDouble(reader["CARRY_FORWARD_RENT"]);
                    //Nullable<DateTime> dt = null;
                    //Rent.PAYMENT_DATE = reader["PAYMENT_DATE"].ToString() == "" ? dt : (DateTime)reader["PAYMENT_DATE"];
                    Rent.PAYMENT_DATE = Convert.ToString(reader["PAYMENT_DATE"]); 
                    Rent.VALIDATION_DATE = Convert.ToString(reader["VALIDATION_DATE"]); 
                    
                    
                    Rent.RENTAL_VALIDATOR = Convert.ToString(reader["RENTAL_VALIDATOR"]);
                    
                   

                    CData.Add(Rent);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}