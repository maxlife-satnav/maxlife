﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ServiceTypeService
/// </summary>
public class ServiceTypeService
{
    SubSonic.StoredProcedure sp;

    public object InsertNUpdate(ServiceTypeVM model)
    {
        try
        {
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_INSERT_UPDATE_SERVICE_TYPE");
            //sp.Command.AddParameter("@FLAG", model.Flag, DbType.String);
            //sp.Command.AddParameter("@PM_ST_SNO", model.PM_ST_SNO, DbType.String);
            //sp.Command.AddParameter("@PM_ST_NAME", model.PM_ST_NAME, DbType.String);
            //sp.Command.AddParameter("@PM_STATUS", model.PM_STATUS, DbType.String);
            //sp.Command.AddParameter("@PM_ST_REMARKS", model.PM_ST_REMARKS, DbType.String);
            //sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            //int flag = (int)sp.ExecuteScalar();
            //return flag;
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@FLAG", model.Flag );
            param[1] = new SqlParameter("@PM_ST_SNO", model.PM_ST_SNO);
            param[2] = new SqlParameter("@PM_ST_NAME", model.PM_ST_NAME);
            param[3] = new SqlParameter("@PM_STATUS", model.PM_STATUS);
            param[4] = new SqlParameter("@AUR_ID", HttpContext.Current.Session["UID"]);
            param[5] = new SqlParameter("@PM_ST_REMARKS", model.PM_ST_REMARKS);

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_INSERT_UPDATE_SERVICE_TYPE", param))
            {

                if (dr.Read())
                {
                    if (dr["FLAG"] == "Success")
                    {
                        return new { Message = MessagesVM.PM_SER_TYPE_OK, data = (object)null };
                    }
                    else
                        return new { Message = MessagesVM.PM_EXISTS, data = (object)null };
                }
                else
                    return new { Message = MessagesVM.PM_NO_REC, data = (object)null };
            }

        }
        catch
        {
            throw;
        }
    }


    public Boolean Update(ServiceTypeVM upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UPDATE_SERVICE_TYPE");
            sp.Command.AddParameter("@PM_ST_SNO", upd.PM_ST_SNO, DbType.String);
            sp.Command.AddParameter("@PM_ST_NAME", upd.PM_ST_NAME, DbType.String);
            sp.Command.AddParameter("@PM_STATUS", upd.PM_STATUS, DbType.String);
            sp.Command.AddParameter("@PM_ST_REMARKS", upd.PM_ST_REMARKS, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Execute();
            return true;
        }
        catch
        {
            throw;
        }
    }



    public IEnumerable<ServiceTypeVM> GetServiceTypeBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_SERVICE_TYPE_GRID").GetReader())
            {
                List<ServiceTypeVM> RTList = new List<ServiceTypeVM>();
                while (reader.Read())
                {
                    RTList.Add(new ServiceTypeVM()
                    {
                        PM_ST_SNO = Convert.ToInt32(reader["PM_ST_SNO"]),
                        PM_ST_NAME = reader["PM_ST_NAME"].ToString(),
                        PM_STATUS = reader["PM_ST_STA_ID"].ToString(),
                        PM_ST_REMARKS = reader["PM_ST_REMARKS"].ToString(),
                   
                    });
                }
                reader.Close();
                return RTList;
            }
        }
        catch
        {
            throw;
        }
    }

}