﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for LeaseReportService
/// </summary>
public class LeaseReportService
{
    SubSonic.StoredProcedure sp;
    List<LeaseReportModel> Leaselist;
    LeaseReportModel Lease;
    DataSet ds;
    public object GetLeaseData(LeaseReportVM Propdetails)
    {
        try
        {
            Leaselist = LeaseReportDetails(Propdetails);
            if (Leaselist.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Leaselist };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<LeaseReportModel> LeaseReportDetails(LeaseReportVM Propdetails)
    {
        try
        {
            List<LeaseReportModel> LeaseData = new List<LeaseReportModel>();

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@FRMDT", SqlDbType.DateTime);
            param[0].Value = Propdetails.FromDate;
            param[1] = new SqlParameter("@TODT", SqlDbType.DateTime);
            param[1].Value = Propdetails.ToDate;
            param[2] = new SqlParameter("@AURID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_REPORT", param))
            {
                while (reader.Read())
                {
                    Lease = new LeaseReportModel();
                    Lease.PROP_TYPE = reader["PROP_TYPE"].ToString();
                    Lease.PRP_NAME = reader["PRP_NAME"].ToString();
                    Lease.LEASE_SDATE = reader["LEASE_SDATE"].ToString();
                    Lease.LEASE_EDATE = reader["LEASE_EDATE"].ToString();
                    Lease.MON_RENT = reader["MONTHLY_RENT"].ToString();
                    Lease.SEC_DEPOSIT = reader["SECURITY_DEPOSIT"].ToString();
                    Lease.BRO_NAME = reader["BROKER_NAME"].ToString();
                    Lease.BRO_FEE = reader["BROKER_FEE"].ToString();
                    Lease.LAND_NAME = reader["LANDLORD_NAME"].ToString();
                    Lease.LAND_ADDR = reader["LANDLORD_ADDRESS"].ToString();
                    Lease.LAND_RENT = reader["LANDLORD_RENT"].ToString();
                    Lease.LAND_SEC_DEP = reader["LANDLORD_DEPOSIT"].ToString();
                    LeaseData.Add(Lease);
                }
                reader.Close();
            }
            return LeaseData;
        }
        catch
        {
            throw;
        }
    }
	
}