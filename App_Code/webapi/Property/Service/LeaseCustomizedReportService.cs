﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class LeaseCustomizedReportService
{
    SubSonic.StoredProcedure sp;
    List<LeaseCustomizedData> Cust;
    LeaseCustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(LeaseCustomizedDetails Det)
    {
        try
        {
            Cust = GetCustomizedDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<LeaseCustomizedData> GetCustomizedDetails(LeaseCustomizedDetails Details)
    {
        try
        {
            List<LeaseCustomizedData> CData = new List<LeaseCustomizedData>();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Details.loclst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[2].Value = Details.FromDate;
            param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[3].Value = Details.ToDate;
           

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_CUSTOMIZED_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new LeaseCustomizedData();
                    Custm.LEASE_NAME = Convert.ToString(reader["LEASE_NAME"]);
                    Custm.LEASE_START_DATE = (DateTime)reader["LEASE_START_DATE"];
                    Custm.LEASE_END_DATE = (DateTime)(reader["LEASE_END_DATE"]);
                    Custm.EXTESNION_TODATE = (DateTime)(reader["EXTENSION_TODATE"]);

                    Custm.COUNTRY = Convert.ToString(reader["CNY_NAME"]);
                    Custm.ZONE = Convert.ToString(reader["ZN_NAME"]);
                    Custm.CITY = Convert.ToString(reader["CTY_NAME"]);
                    Custm.LOCATION = Convert.ToString(reader["LCM_NAME"]);
                    Custm.LANDLORD_NAME = Convert.ToString(reader["LANDLORD_NAME"]);
                    Custm.LANDLORD_ADDRESS = Convert.ToString(reader["LANDLORD_ADDRESS"]);
                    Custm.MONTHLY_RENT = Convert.ToDouble(reader["MONTHLY_RENT"]);
                    Custm.PN_PROPERTYTYPE = Convert.ToString(reader["PN_PROPERTYTYPE"]);
                    Custm.PM_LAD_LOCK_INPERIOD = Convert.ToString(reader["PM_LAD_LOCK_INPERIOD"]);
                    Custm.SECURITY_DEPOSIT = Convert.ToDouble(reader["SECURITY_DEPOSIT"]);
                    Custm.MAINTENANCE_CHARGES = Convert.ToDouble(reader["MAINTENANCE_CHARGES"]);
                    Custm.SERVICE_TAX = Convert.ToDouble(reader["SERVICE_TAX"]);
                    Custm.LEASE_ESCALATION = Convert.ToString(reader["LEASE_ESCALATION"]);
                   
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}