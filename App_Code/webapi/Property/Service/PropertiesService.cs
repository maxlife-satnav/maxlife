﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class PropertiesService
{
    SubSonic.StoredProcedure sp;
    PropertiesModel Prop;
    List<PropertiesModel> PropList;
    DataSet ds;

    public object ViewProperties(ReportType Type)
    {
        try
        {
            PropList = GetProperties(Type);
            if (PropList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = PropList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }


    public object GetMarkers()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ACTIVE_PROPERTIES");
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return new { Message = MessagesVM.SER_OK, data = ds };
                }
                else
                {
                    return new { Message = MessagesVM.SER_OK, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.SER_OK, data = (object)null };

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<PropertiesModel> GetProperties(ReportType Type)
    {
        try
        {
            DateTime? dt = null;
            List<PropertiesModel> VMlist = new List<PropertiesModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_VIEW_ALL_PROPERTIES");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    Prop = new PropertiesModel();
                    Prop.PN_PROPERTYTYPE = reader["PN_PROPERTYTYPE"].ToString();
                    Prop.PM_PPT_NAME = reader["PM_PPT_NAME"].ToString();
                    Prop.LCM_NAME = reader["LCM_NAME"].ToString();
                    Prop.PM_PPT_ADDRESS = reader["PM_PPT_ADDRESS"].ToString();
                    Prop.PM_AR_CARPET_AREA = reader["PM_AR_CARPET_AREA"].ToString();
                    Prop.PM_AR_BUA_AREA = reader["PM_AR_BUA_AREA"].ToString();
                    Prop.PM_OWN_NAME = reader["PM_OWN_NAME"].ToString();
                    Prop.AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString();
                    Prop.PM_INS_START_DT = reader["PM_INS_START_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PM_INS_START_DT"]);
                    Prop.PM_INS_END_DT = reader["PM_INS_END_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PM_INS_END_DT"]);
                    Prop.AUR_RES_NUMBER = reader["AUR_RES_NUMBER"].ToString();
                    Prop.AUR_EMAIL = reader["AUR_EMAIL"].ToString();
                    Prop.PM_IT_NAME = reader["PM_IT_NAME"].ToString();
                    Prop.PM_INS_VENDOR = reader["PM_INS_VENDOR"].ToString();
                    Prop.PM_INS_AMOUNT = reader["PM_INS_AMOUNT"].ToString();
                    Prop.PM_INS_PNO = reader["PM_INS_PNO"].ToString();
                    VMlist.Add(Prop);
                }
                reader.Close();
            }
            return VMlist;
        }
        catch
        {
            throw;
        }
    }
}