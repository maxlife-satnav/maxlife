﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for TenantReportService
/// </summary>
public class TenantReportService
{
    SubSonic.StoredProcedure sp;
    List<TenantModels> Tenantlst;
    TenantModels Tenant;
    DataSet ds;

    public object GetTenantObject(TenantReportModel TRM)
    {
        try
        {
            Tenantlst = GetTenantDetails(TRM);
            if (Tenantlst.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Tenantlst };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<TenantModels> GetTenantDetails(TenantReportModel TRM)
    {

        List<TenantModels> TData = new List<TenantModels>();
        try
        {
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            if (TRM.selectedLoc == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(TRM.selectedLoc);
            }
            param[1] = new SqlParameter("@PT_LST", SqlDbType.Structured);
            if (TRM.selectedPrpType == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(TRM.selectedPrpType);
            }
            param[2] = new SqlParameter("@PRP_NAMES_LST", SqlDbType.Structured);
            if (TRM.selectedPrpName == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(TRM.selectedPrpName);
            }
            param[3] = new SqlParameter("@AURID", SqlDbType.VarChar);
            param[3].Value = HttpContext.Current.Session["Uid"].ToString();

          
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_GET_TENANT_REPORT", param))
            {
                while (reader.Read())
                {
                    Tenant = new TenantModels();
                    Tenant.TEN_PRPTYPE = reader["TEN_PRPTYPE"].ToString();
                    Tenant.TEN_CODE = reader["TEN_CODE"].ToString();
                    Tenant.TEN_NAME = reader["TEN_NAME"].ToString();

                    Tenant.TEN_EMAIL = reader["TEN_EMAIL"].ToString();
                    Tenant.OCC_AREA = reader["OCC_AREA"].ToString();
                    Tenant.TEN_RENT = reader["TEN_RENT"].ToString();
                    Tenant.MAINT_FEE = reader["MAINT_FEE"].ToString();
                    Tenant.TOT_RENT_AMT = reader["TOT_RENT_AMT"].ToString();

                    TData.Add(Tenant);
                }
                reader.Close();
            }
            return TData;
        }
        catch { return TData; }
    }
}