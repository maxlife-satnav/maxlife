﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SystemPreferencesService
/// </summary>
public class SystemPreferencesService
{
    SubSonic.StoredProcedure sp;

    ////bind months
    public object BindMonths()
    {
        List<MonthsList> mntlst = new List<MonthsList>();
        MonthsList mon;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_MONTHS");       
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                mon = new MonthsList();
                mon.AST_SYSP_CODE = sdr["AST_SYSP_CODE"].ToString();
                mon.AST_SYSP_VAL1 = sdr["AST_SYSP_VAL1"].ToString();
                mon.ticked = false;
                mntlst.Add(mon);
            }
        }
        if (mntlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = mntlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public IEnumerable<MonthsBind> BindGrid()
    {
        try
        {
            IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_ALL_MONTHS_DATA]").GetReader();
            List<MonthsBind> BindMonths = new List<MonthsBind>();
            while (reader.Read())
            {
                BindMonths.Add(new MonthsBind()
                {
                    AST_SYSP_CODE = reader.GetValue(0).ToString(),
                    SYSP_FROM_DATE = (DateTime)reader.GetValue(1),
                    SYSP_TO_DATE = (DateTime)reader.GetValue(2),                    
                });
            }
            reader.Close();
            return BindMonths;
        }
        catch
        {
            throw;
        }
    }

    //update
    public object UpdateDetails(SystemPreferencesModel update)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@MONTH", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(update.MonLst);
            param[1] = new SqlParameter("@SYSP_FROM_DATE", SqlDbType.DateTime);
            param[1].Value = update.SYSP_FROM_DATE;
            param[2] = new SqlParameter("@SYSP_TO_DATE", SqlDbType.DateTime);
            param[2].Value = update.SYSP_TO_DATE;

            object value = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_INSERT_MONTHS", param);
            return update;
        }
        catch
        {
            throw;
        }
    }






}