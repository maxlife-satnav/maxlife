﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ExpenseHeadService
/// </summary>
public class ExpenseHeadService
{
    SubSonic.StoredProcedure sp;
    //Insert
    public int SaveData(ExpHeadModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_ADD_EXPENSES_HEAD");
            sp.Command.AddParameter("@EXP_CODE", model.EXP_CODE, DbType.String);
            sp.Command.AddParameter("@EXP_NAME", model.EXP_NAME, DbType.String);
            sp.Command.AddParameter("@EXP_STATUS", (model.EXP_STATUS == null) ? "1" : model.EXP_STATUS, DbType.String);
            sp.Command.AddParameter("@EXP_REM", (model.EXP_REM == null) ? "NA" : model.EXP_REM, DbType.String);
            sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);          
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }
    //Update
    public Boolean UpdateExpenseHeadData(ExpHeadModel update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UPDATE_EXPENSES_HEAD");
        sp.Command.AddParameter("@EXP_CODE", update.EXP_CODE, DbType.String);
        sp.Command.AddParameter("@EXP_NAME", update.EXP_NAME, DbType.String);
        sp.Command.AddParameter("@EXP_STATUS", update.EXP_STATUS, DbType.Int32);
        sp.Command.AddParameter("@EXP_REM", (update.EXP_REM == null) ? "NA" : update.EXP_REM, DbType.String);
        sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);        
        sp.Execute();
        return true;
    }
    //Bind Grid
    public IEnumerable<ExpHeadModel> BindExpenseHeadGrid()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_BIND_EXPENSE_HEAD_GRID").GetReader();
        List<ExpHeadModel> ExpHeadList = new List<ExpHeadModel>();
        while (reader.Read())
        {
            ExpHeadList.Add(new ExpHeadModel()
            {
                EXP_CODE = reader.GetValue(0).ToString(),
                EXP_NAME = reader.GetValue(1).ToString(),
                EXP_STATUS = reader.GetValue(2).ToString(),
                EXP_REM = reader.GetValue(3).ToString()
            });
        }
        reader.Close();
        return ExpHeadList;
    }
}