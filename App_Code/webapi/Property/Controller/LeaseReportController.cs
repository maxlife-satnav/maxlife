﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

/// <summary>
/// Summary description for LeaseReportController
/// </summary>
public class LeaseReportController: ApiController
{
    LeaseReportService LRS = new LeaseReportService();
    [HttpPost]
    public HttpResponseMessage GetGriddata(LeaseReportVM Propdetails)
    {
        var obj = LRS.GetLeaseData(Propdetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(LeaseReportVM data)
    {
        //CultureInfo ci = new CultureInfo(HttpContext.Current.Session["userculture"].ToString());
        //NumberFormatInfo nfi = ci.NumberFormat;
        //ReportParameter p1 = new ReportParameter("Currencyparam", nfi.CurrencySymbol);

        ReportGenerator<LeaseReportModel> reportgen = new ReportGenerator<LeaseReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/ViewLeaseReport.rdlc"),
            DataSetName = "ViewLeaseReportDS",
            ReportType = "Lease Report"
        };
        LRS = new LeaseReportService();
        List<LeaseReportModel> reportdata = LRS.LeaseReportDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ViewLeaseReport.rdlc" + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "LeaseReport." + data.Type;
        return result;
    }
	
}