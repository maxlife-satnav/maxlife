﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SystemPreferencesController : ApiController
{
    SystemPreferencesService SystemPreferencesService = new SystemPreferencesService();

    [HttpGet]


    // Bind Months
    public HttpResponseMessage BindMonths()
    {
        var Monthlist = SystemPreferencesService.BindMonths();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Monthlist);
        return response;
    }

    //Bind Grid
    [HttpGet]
    public HttpResponseMessage BindGrid()
    {
        IEnumerable<MonthsBind> BindMonths = SystemPreferencesService.BindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BindMonths);
        return response;
    }

//update 
      


    [HttpPost]
    public HttpResponseMessage UpdateDetails(SystemPreferencesModel update)
    {
        var Userlist = SystemPreferencesService.UpdateDetails(update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Userlist);
        return response;
    }

    
}
