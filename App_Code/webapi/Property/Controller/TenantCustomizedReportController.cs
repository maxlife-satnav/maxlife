﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


public class TenantCustomizedReportController : ApiController
{
    TenantCustomizedReportService Csvc = new TenantCustomizedReportService();
    TenantCustomizedReportView report = new TenantCustomizedReportView();

    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(TenantCustomizedDetails Custdata)
    {
        var obj = Csvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedData([FromBody]TenantCustomizedDetails data)
    {
        ReportGenerator<TenantCustomizedData> reportgen = new ReportGenerator<TenantCustomizedData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/TenantCustomizedReport.rdlc"),
            DataSetName = "CustomizedReport",
            ReportType = "Tenant Customizable Report"
        };

        Csvc = new TenantCustomizedReportService();
        List<TenantCustomizedData> reportdata = Csvc.GetCustomizedDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/TenantCustomizedReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "TenantCustomizableReport." + data.Type;
        return result;
    }
}