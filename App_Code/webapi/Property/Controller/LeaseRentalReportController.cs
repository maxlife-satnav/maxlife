﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class LeaseRentalReportController : ApiController
{

    LeaseRentalReportService Csvc = new LeaseRentalReportService();
    LeaseCustomizedReportView report = new LeaseCustomizedReportView();

    [HttpPost]
    public HttpResponseMessage GetRentalDetails(LeaseRentalDetails Rentdata)
    {
        var obj = Csvc.GetRentalObject(Rentdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetRentalData([FromBody]LeaseRentalDetails data)
    {
        try
        {
            ReportGenerator<LeaseRentalData> reportgen = new ReportGenerator<LeaseRentalData>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/LeaseRentalReport.rdlc"),
                DataSetName = "LeaseRentalReport",
                ReportType = "Lease Rental Report"
            };

            Csvc = new LeaseRentalReportService();
            List<LeaseRentalData> reportdata = Csvc.GetRentalDetails(data);
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/LeaseRentalReport." + data.Type);
            await reportgen.GenerateReport(reportdata, filePath, data.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "LeaseRentalReport." + data.Type;
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }
}