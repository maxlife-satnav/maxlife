﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ServiceTypeController : ApiController
{
    ServiceTypeService service = new ServiceTypeService();

    [HttpPost]
    public HttpResponseMessage InsertNUpdate(ServiceTypeVM objVM)
    {
        var obj = service.InsertNUpdate(objVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage Update(ServiceTypeVM update)
    {

        if (service.Update(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Facility Type Updated Successfully");
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage ServiceTypeBindGrid()
    {
        IEnumerable<ServiceTypeVM> mncCatlist = service.GetServiceTypeBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, mncCatlist);
        return response;
    }

}
