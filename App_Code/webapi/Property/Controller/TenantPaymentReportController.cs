﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

/// <summary>
/// Summary description for TenantReportController
/// </summary>
public class TenantPaymentReportController : ApiController
{
    TenantPaymentReportService LRS = new TenantPaymentReportService();
    [HttpPost]
    public HttpResponseMessage GetGriddata(TenantPaymentReportVM Propdetails)
    {
        var obj = LRS.GetLeaseData(Propdetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(TenantPaymentReportVM data)
    {
        //CultureInfo ci = new CultureInfo(HttpContext.Current.Session["userculture"].ToString());
        //NumberFormatInfo nfi = ci.NumberFormat;
        //ReportParameter p1 = new ReportParameter("Currencyparam", nfi.CurrencySymbol);

        ReportGenerator<TenantPaymentReportModel> reportgen = new ReportGenerator<TenantPaymentReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/TenantPaymentReport.rdlc"),
            DataSetName = "TenantPaymentReportDS",
            ReportType = "Tenant Payment Report"
        };
        LRS = new TenantPaymentReportService();
        List<TenantPaymentReportModel> reportdata = LRS.TenantReportDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/TenantPaymentReport.rdlc" + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "TenantReport." + data.Type;
        return result;
    }
	
}