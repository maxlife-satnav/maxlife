﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using LocWiseUtilityVModel;


public class ExpenseUtilityReportVM
{
    
    
    public List<ExpLocationVM> loclst { get; set; }
    public List<ExpHeadUtilityModel> explst { get; set; }
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string CNP_NAME { get; set; }
    public string CNP_ID { get; set; }
}

public class ExpenseCustomizedData
{
    public string LCM_CODE { get; set; }
    public string EXP_CODE { get; set; }
    public string BILL_NO { get; set; }
    public string BILL_INVOICE { get; set; }
    public double AMT { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public string VENDOR { get; set; }
    public string VEN_MAIL { get; set; }
    public string VEN_PHNO { get; set; }
    public string AUR_ID { get; set; }
    public string REM { get; set; }
    public string EXP_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string AUR_NAME { get; set; }
    public string STATUS { get; set; }
    public string DEP_CODE { get; set; }
    public string DEP_NAME { get; set; }
    
}

public class ExpenseUtilityReportView
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public ExpenseUtilityReportView()
    {
        SpaceDatas = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> SpaceDatas { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case ExpenseUtilityReportView.ReportFormat.Word: return ".doc";
                case ExpenseUtilityReportView.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.SpaceDatas)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}