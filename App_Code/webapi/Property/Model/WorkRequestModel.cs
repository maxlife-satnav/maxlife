﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class WorkRequestModel
{
    public string PM_CODE { get; set; }
    public string PM_NAME { get; set; }
    public int TOTAL_WR { get; set; }
    public string PM_WR_REQ_ID { get; set; }
    public string PM_WR_CREATED_BY { get; set; }
    public string STA_TITLE { get; set; }
    public Nullable<System.DateTime> PM_WR_CREATED_DT { get; set; }
    public Nullable<System.DateTime> PM_UWR_CREATED_DT { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string PN_PROPERTYTYPE { get; set; }
    public string AVR_NAME { get; set; }
    public string PM_WR_VEN_ADDRESS { get; set; }
    public string PM_WR_VEN_PH_NO { get; set; }
    public string PM_PPT_SNO { get; set; }
}

public class WorkRequestDetails
{
    public string Type { get; set; }
    public string Property { get; set; }
    public string RequestId { get; set; }
    
}