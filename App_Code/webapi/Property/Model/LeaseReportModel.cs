﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaseReportModel
/// </summary>
//public class Company
//{
//    public string CNP_NAME { get; set; }
//    public string PLAN_ID { get; set; }
//    public string Type { get; set; }
//}
public class LeaseReportModel
{   
    public string PROP_TYPE { get; set; }
    public string PRP_NAME { get; set; }
    public string LEASE_SDATE { get; set; }
    public string LEASE_EDATE { get; set; }
    public string MON_RENT { get; set; }
    public string SEC_DEPOSIT { get; set; }
    public string BRO_NAME { get; set; }
    public string BRO_FEE { get; set; }
    public string LAND_NAME { get; set; }
    public string LAND_ADDR { get; set; }
    public string LAND_RENT { get; set; }
    public string LAND_SEC_DEP { get; set; }   
}


public class LeaseReportVM
{
    public string PLAN_ID { get; set; }
    public string Type { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
}