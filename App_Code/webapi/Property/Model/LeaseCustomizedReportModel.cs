﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class LeaseCustomizedDetails
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }  
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }   
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
}

public class LeaseCustomizedData
{
    public string COUNTRY { get; set; }
    public string ZONE { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string EMPLOYEE_ID { get; set; }
    public string EMPLOYEE_NAME { get; set; }
    public double SECURITY_DEPOSIT { get; set; }
    public string LEASE_NAME { get; set; }
    public string LANDLORD_NAME { get; set; }
    public double MONTHLY_RENT { get; set; }
    public double RECOVERY_AMOUNT { get; set; }
    public double BUILTUP_AREA { get; set; }
    public double MAINTENANCE_CHARGES { get; set; }
    public string LANDLORD_ADDRESS { get; set; }
    public double SERVICE_TAX { get; set; }
    public string RENT_REVISION { get; set; }
    public string PN_LEASE_TYPE { get; set; }
    public string PM_LAD_LOCK_INPERIOD { get; set; }
    public string PN_PROPERTYTYPE { get; set; }
    public string COMPLETE_ADDRESS { get; set; }
    public double PAID_AMOUNT { get; set; }
    public string RENEWAL_STATUS { get; set; }
    public string LEASE_ESCALATION { get; set; }
    public Nullable<DateTime> LEASE_START_DATE { get; set; }
    public Nullable<System.DateTime> EXTESNION_TODATE { get; set; }
    public Nullable<System.DateTime> LEASE_END_DATE { get; set; }
    public Nullable<System.DateTime> EXPIRY_AGREEMENT_DATE { get; set; }
}

public class LeaseCustomizedReportView
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public LeaseCustomizedReportView()
    {
        SpaceDatas = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> SpaceDatas { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case LeaseCustomizedReportView.ReportFormat.Word: return ".doc";
                case LeaseCustomizedReportView.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.SpaceDatas)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}