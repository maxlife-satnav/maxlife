﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaseReportModel
/// </summary>
//public class Company
//{
//    public string CNP_NAME { get; set; }
//    public string PLAN_ID { get; set; }
//    public string Type { get; set; }
//}
public class TenantPaymentReportModel
{
    public string TENANT { get; set; }
    public string PRP_NAME { get; set; }
    public decimal RENT { get; set; }
    public decimal MAIN_FEE { get; set; }
    public decimal TOT_RENT { get; set; }
    public string PAIDDATE { get; set; }
    public string FROMDATE { get; set; }
    public string TODATE { get; set; }
    public string TDS { get; set; }
    public string PAY_MODE{ get; set; }
    public string CHEQUENO{ get; set; }
    public string CHEQUEDATE{ get; set; }
    public string ISS_BANK{ get; set; }
    public string IFSC { get; set; }
    public string ACCNUM{ get; set; }   
}

public class TenantPaymentReportVM
{
    public string PLAN_ID { get; set; }
    public string Type { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
}