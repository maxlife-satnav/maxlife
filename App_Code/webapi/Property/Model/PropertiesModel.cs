﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class PropertiesModel
{
    public string PN_PROPERTYTYPE { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string PM_PPT_ADDRESS { get; set; }
    public string PM_AR_CARPET_AREA { get; set; }
    public string PM_AR_BUA_AREA { get; set; }
    public string PM_OWN_NAME { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public Nullable<System.DateTime> PM_INS_START_DT { get; set; }
    public Nullable<System.DateTime> PM_INS_END_DT { get; set; }
    public string AUR_RES_NUMBER { get; set; }
    public string AUR_EMAIL { get; set; }
    public string PM_IT_NAME { get; set; }
    public string PM_INS_VENDOR { get; set; }
    public string PM_INS_AMOUNT { get; set; }
    public string PM_INS_PNO { get; set; }
    
}
public class ReportType
{
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
}