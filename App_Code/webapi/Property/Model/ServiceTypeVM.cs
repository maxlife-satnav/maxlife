﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReservationTypeVM
/// </summary>
public class ServiceTypeVM
{
    public string Flag { get; set; }
    public int PM_ST_SNO { get; set; }
    public string PM_ST_NAME { get; set; }
    public string PM_STATUS { get; set; }
    public string PM_ST_REMARKS { get; set; }
    public string PM_ST_CREATED_BY { get; set; }
    public string PM_ST_UPDATED_BY { get; set; }
}