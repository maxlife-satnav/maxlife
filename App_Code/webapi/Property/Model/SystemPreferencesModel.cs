﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SystemPreferencesModel
/// </summary>
public class SystemPreferencesModel
{
    public List<MonthsList> MonLst { get; set; }
    public DateTime SYSP_FROM_DATE { get; set; }
    public DateTime SYSP_TO_DATE { get; set; }
}

public class MonthsList
{
    public string AST_SYSP_CODE { get; set; }
    public string AST_SYSP_VAL1 { get; set; }
    public bool ticked { get; set; }
}
public class MonthsBind
{
    public string AST_SYSP_CODE { get; set; }
    public DateTime SYSP_FROM_DATE { get; set; }
    public DateTime SYSP_TO_DATE { get; set; }
}