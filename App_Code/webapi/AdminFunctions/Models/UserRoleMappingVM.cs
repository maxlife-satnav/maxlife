﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UserRoleMappingVM
/// </summary>
public class UserRoleMappingVM
{
    public UserRoleMapping UserRoleMapping { get; set; }
    public List<UserRoleVM> UserRoleVM { get; set; }
    public USERMAPPINGDETAILS USERMAPPINGDETAILS { get; set; }
    public List<Module> modulelist { get; set; }
    public string MapLevelAccess { get; set; }
    public string Type { get; set; }
}
public class UserRoleVM
{
    
    public int ROL_ID { get; set; }  
    public string ROL_ACRONYM { get; set; }
    public string ROL_DESCRIPTION { get; set; }
    public Boolean isChecked { get; set; }
}

public class UserRoleMapping
{
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string AUR_EMAIL { get; set; }
    public string ROL_DESCRIPTION { get; set; }
    public string AUR_DESGN_ID { get; set; }
    public string AUR_DEP_ID { get; set; }
    public string AUR_PRJ_CODE { get; set; }
    public string AUR_LOCATION { get; set; }
    public string AUR_RES_NUMBER { get; set; }
    public Nullable<System.DateTime> AUR_DOB { get; set; }
    public Nullable<System.DateTime> AUR_DOJ { get; set; }
    public string AUR_COUNTRY { get; set; }
    public string AUR_TIME_ZONE { get; set; }
    public string AUR_REPORTING_TO { get; set; }
    public string AUR_EXTENSION { get; set; }
    public int AUR_STA_ID { get; set; }
    public string AUR_PASSWORD { get; set; }
    public string AUR_OLD_PASSWORD { get; set; }
    public string AUR_ADDRESS { get; set; }
    public string Remarks { get; set; }
}
public class UserCompanyModules
{
    public string AurId { get; set; }
    public string ModuleId { get; set; }
}
public class Module
{
    public string MOD_CODE { get; set; }
    public string MOD_NAME { get; set; }
}
public class USERMAPPINGDETAILS
{
    public List<Countrylst> selectedCountries { get; set; }
    public List<Zonelst> selectedZone { get; set; }
    public List<Statelst> selectedState { get; set; }
    public List<Citylst> selectedCities { get; set; }
    public List<Locationlst> selectedLocations { get; set; }
    public List<Towerlst> selectedTowers { get; set; }
    public List<Floorlst> selectedFloors { get; set; }
    public List<Verticallst> selectedVerticals { get; set; }
    public List<Costcenterlst> selectedCostcenter { get; set; }
    public List<ParentEntityLst> selectedParentEntity { get; set; }
    public List<ChildEntityLst> selectedChildEntity { get; set; }
}



