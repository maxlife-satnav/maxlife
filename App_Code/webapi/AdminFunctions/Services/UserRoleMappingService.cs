﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.OleDb;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;

/// <summary>
/// Summary description for UserRoleMappingService
/// </summary>
public class UserRoleMappingService
{
    SubSonic.StoredProcedure sp;


    //Grid View
    public object GetUserRoleDetails()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_GET_USER_ROLE_MAPPING_DETAILS").GetReader())
            {
                List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
                while (reader.Read())
                {
                    URMlist.Add(new UserRoleMapping()
                    {
                        AUR_ID = reader["AUR_ID"].ToString(),
                        AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                        AUR_EMAIL = reader["AUR_EMAIL"].ToString(),
                        ROL_DESCRIPTION = reader["ROL_DESCRIPTION"].ToString(),
                        AUR_DESGN_ID = reader["AUR_DESGN_ID"].ToString(),
                        AUR_DEP_ID = reader["AUR_DEP_ID"].ToString(),

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.UM_NO_REC, data = URMlist };
            }
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetUserOnSelection(string Id)
    {
        try
        {
            UserRoleMappingVM URMVMlist = new UserRoleMappingVM();

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_Get_Amantra_User_Details");
            sp.Command.AddParameter("@AUR_ID", Id, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                UserRoleMapping URMlist = new UserRoleMapping();
                while (reader.Read())
                {

                    URMlist.AUR_ID = reader["AUR_ID"].ToString();
                    URMlist.AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString();
                    URMlist.AUR_EMAIL = reader["AUR_EMAIL"].ToString();
                    URMlist.AUR_RES_NUMBER = reader["AUR_RES_NUMBER"].ToString();
                    URMlist.AUR_DESGN_ID = reader["AUR_DESGN_ID"].ToString();
                    URMlist.AUR_DEP_ID = reader["AUR_DEP_ID"].ToString();
                    URMlist.AUR_PRJ_CODE = reader["AUR_PRJ_CODE"].ToString();
                    URMlist.AUR_COUNTRY = reader["AUR_COUNTRY"].ToString();
                    URMlist.AUR_TIME_ZONE = reader["AUR_TIME_ZONE"].ToString();
                    URMlist.AUR_REPORTING_TO = reader["AUR_REPORTING_TO"].ToString();
                    URMlist.AUR_EXTENSION = reader["AUR_EXTENSION"].ToString();
                    URMlist.AUR_STA_ID = (int)reader["AUR_STA_ID"];
                    URMlist.AUR_PASSWORD = reader["USR_LOGIN_PASSWORD"].ToString();
                    URMlist.AUR_LOCATION = reader["AUR_LOCATION"].ToString();
                    Nullable<DateTime> dt = null;
                    URMlist.AUR_DOB = reader["AUR_DOB"].ToString() == "" ? dt : (DateTime)reader["AUR_DOB"];
                    URMlist.AUR_DOJ = reader["AUR_DOJ"].ToString() == "" ? dt : (DateTime)reader["AUR_DOJ"];
                    URMlist.AUR_ADDRESS = reader["AUR_ADDRESS"].ToString();
                }
                reader.Close();
                URMVMlist.UserRoleMapping = URMlist;
            }
            //ROLE
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Selected_Roles_By_User");
            sp.Command.AddParameter("@URL_USR_ID", Id, DbType.String);

            List<UserRoleVM> Rolelist = new List<UserRoleVM>();
            using (IDataReader Rreader = sp.GetReader())
            {
                while (Rreader.Read())
                {
                    Rolelist.Add(new UserRoleVM()
                    {
                        ROL_ID = (int)Rreader["URL_ROL_ID"],

                    });
                }
                URMVMlist.UserRoleVM = Rolelist;
                Rreader.Close();
            }

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Edit_User_Mapping_Details_ADM");
            sp.Command.AddParameter("@UDM_AUR_ID", Id, DbType.String);

            USERMAPPINGDETAILS Userlist = new USERMAPPINGDETAILS();
            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Userlist.selectedCountries = new List<Countrylst>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Userlist.selectedCountries.Add(new Countrylst { CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), CNY_NAME = Convert.ToString(dr["CNY_NAME"]), ticked = true });
                    }
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    Userlist.selectedCities = new List<Citylst>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        Userlist.selectedCities.Add(new Citylst { CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CTY_NAME = Convert.ToString(dr["CTY_NAME"]), ZN_CODE = Convert.ToString(dr["UDM_ZN_CODE"]), STE_CODE = Convert.ToString(dr["UDM_STE_CODE"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ticked = true });
                    }
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    Userlist.selectedLocations = new List<Locationlst>();
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        Userlist.selectedLocations.Add(new Locationlst { LCM_CODE = Convert.ToString(dr["UDM_LCM_CODE"]), LCM_NAME = Convert.ToString(dr["LCM_NAME"]), CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["UDM_ZN_CODE"]), STE_CODE = Convert.ToString(dr["UDM_STE_CODE"]), ticked = true });
                    }
                }

                if (ds.Tables[3].Rows.Count > 0)
                {
                    Userlist.selectedTowers = new List<Towerlst>();
                    foreach (DataRow dr in ds.Tables[3].Rows)
                    {
                        Userlist.selectedTowers.Add(new Towerlst { TWR_CODE = Convert.ToString(dr["UDM_TWR_CODE"]), TWR_NAME = Convert.ToString(dr["TWR_NAME"]), LCM_CODE = Convert.ToString(dr["UDM_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["UDM_ZN_CODE"]), STE_CODE = Convert.ToString(dr["UDM_STE_CODE"]), ticked = true });
                    }
                }

                if (ds.Tables[4].Rows.Count > 0)
                {
                    Userlist.selectedFloors = new List<Floorlst>();
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        Userlist.selectedFloors.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["UFM_FLR_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["UDM_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["UDM_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["UDM_ZN_CODE"]), STE_CODE = Convert.ToString(dr["UDM_STE_CODE"]), ticked = true });
                    }
                }

                if (ds.Tables[5].Rows.Count > 0)
                {
                    Userlist.selectedVerticals = new List<Verticallst>();
                    foreach (DataRow dr in ds.Tables[5].Rows)
                    {
                        Userlist.selectedVerticals.Add(new Verticallst { VER_CODE = Convert.ToString(dr["UVM_VERT_CODE"]), VER_NAME = Convert.ToString(dr["VER_NAME"]), ticked = true });
                    }
                }

                if (ds.Tables[6].Rows.Count > 0)
                {
                    Userlist.selectedCostcenter = new List<Costcenterlst>();
                    foreach (DataRow dr in ds.Tables[6].Rows)
                    {
                        Userlist.selectedCostcenter.Add(new Costcenterlst { Cost_Center_Code = Convert.ToString(dr["UCM_COST_CENTER_CODE"]), Cost_Center_Name = Convert.ToString(dr["Cost_Center_Name"]), Vertical_Code = Convert.ToString(dr["UCM_VERT_CODE"]), ticked = true });
                    }

                }

                if (ds.Tables[7].Rows.Count > 0)
                {
                    Userlist.selectedParentEntity = new List<ParentEntityLst>();
                    foreach (DataRow dr in ds.Tables[7].Rows)
                    {
                        Userlist.selectedParentEntity.Add(new ParentEntityLst { PE_CODE = Convert.ToString(dr["UPE_PE_CODE"]), PE_NAME = Convert.ToString(dr["PE_NAME"]), ticked = true });
                    }

                }

                if (ds.Tables[8].Rows.Count > 0)
                {
                    Userlist.selectedChildEntity = new List<ChildEntityLst>();
                    foreach (DataRow dr in ds.Tables[8].Rows)
                    {
                        Userlist.selectedChildEntity.Add(new ChildEntityLst { CHE_CODE = Convert.ToString(dr["UCHE_CHE_CODE"]), CHE_NAME = Convert.ToString(dr["CHE_NAME"]), PE_CODE = Convert.ToString(dr["UCHE_PE_CODE"]), ticked = true });
                    }

                }
                if (ds.Tables[9].Rows.Count > 0)
                {
                    Userlist.selectedZone = new List<Zonelst>();
                    foreach (DataRow dr in ds.Tables[9].Rows)
                    {
                        Userlist.selectedZone.Add(new Zonelst { ZN_CODE = Convert.ToString(dr["UDM_ZN_CODE"]), ZN_NAME = Convert.ToString(dr["ZN_NAME"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ticked = true });
                    }

                }
                if (ds.Tables[10].Rows.Count > 0)
                {
                    Userlist.selectedState = new List<Statelst>();
                    foreach (DataRow dr in ds.Tables[10].Rows)
                    {
                        Userlist.selectedState.Add(new Statelst { CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ZN_CODE = Convert.ToString(dr["UDM_ZN_CODE"]), STE_CODE = Convert.ToString(dr["UDM_STE_CODE"]), STE_NAME = Convert.ToString(dr["STE_NAME"]), ticked = true });
                    }

                }


            }

            URMVMlist.USERMAPPINGDETAILS = Userlist;

            return URMVMlist;
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    //for module Dropdown

    public object Getmodules()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_GET_MODULES").GetReader())
            {
                List<Module> modulelist = new List<Module>();
                while (reader.Read())
                {
                    modulelist.Add(new Module()
                    {
                        MOD_CODE = reader["MOD_CODE"].ToString(),
                        MOD_NAME = reader["MOD_NAME"].ToString(),
                    });
                }
                reader.Close();
                return modulelist;
            }
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }


    public object GetAccessByModule(UserCompanyModules ucm)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_Get_Roles_By_Module");
            sp.Command.AddParameter("@MOD_NAME", ucm.ModuleId, DbType.String);
            sp.Command.AddParameter("@AUR_NAME", ucm.AurId, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                List<UserRoleVM> Rolelist = new List<UserRoleVM>();
                while (reader.Read())
                {
                    Rolelist.Add(new UserRoleVM()
                    {
                        ROL_ID = (int)reader["ROL_ID"],
                        ROL_ACRONYM = reader["ROL_ACRONYM"].ToString(),
                        ROL_DESCRIPTION = reader["ROL_DESCRIPTION"].ToString(),
                        isChecked = false
                    });
                }
                reader.Close();
                return Rolelist;
            }
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    //Save Emp Details
    public object SaveEmpDetails(UserRoleMappingVM user)
    {
        try
        {
            //update user details
            SqlParameter[] udParams = new SqlParameter[17];
            udParams[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            udParams[0].Value = user.UserRoleMapping.AUR_ID;
            udParams[1] = new SqlParameter("@AUR_EMAIL", SqlDbType.VarChar);
            udParams[1].Value = user.UserRoleMapping.AUR_EMAIL;
            udParams[2] = new SqlParameter("@AUR_PHONE", SqlDbType.VarChar);
            udParams[2].Value = user.UserRoleMapping.AUR_RES_NUMBER;
            udParams[3] = new SqlParameter("@AUR_DOB", SqlDbType.Date);
            udParams[3].Value = user.UserRoleMapping.AUR_DOB;
            udParams[4] = new SqlParameter("@AUR_DOJ", SqlDbType.Date);
            udParams[4].Value = user.UserRoleMapping.AUR_DOJ;
            udParams[5] = new SqlParameter("@AUR_CNY", SqlDbType.VarChar);
            udParams[5].Value = user.USERMAPPINGDETAILS.selectedLocations[0].CNY_CODE;
            udParams[6] = new SqlParameter("@AUR_REPTO", SqlDbType.VarChar);
            udParams[6].Value = user.UserRoleMapping.AUR_REPORTING_TO;
            udParams[7] = new SqlParameter("@AUR_EXTN", SqlDbType.VarChar);
            udParams[7].Value = user.UserRoleMapping.AUR_EXTENSION;
            udParams[8] = new SqlParameter("@AUR_STATUS", SqlDbType.VarChar);
            udParams[8].Value = user.UserRoleMapping.AUR_STA_ID;
            udParams[9] = new SqlParameter("@AUR_PASSWORD", SqlDbType.NVarChar);
            udParams[9].Value = user.UserRoleMapping.AUR_PASSWORD; //AUR_OLD_PASSWORD 
            udParams[10] = new SqlParameter("@AUR_VERTICAL", SqlDbType.VarChar);
            udParams[10].Value = user.UserRoleMapping.AUR_DEP_ID;
            udParams[11] = new SqlParameter("@AUR_COSTCENTER", SqlDbType.NVarChar);
            udParams[11].Value = user.UserRoleMapping.AUR_PRJ_CODE;
            udParams[12] = new SqlParameter("@AUR_LOC", SqlDbType.VarChar);
            udParams[12].Value = user.USERMAPPINGDETAILS.selectedLocations[0].LCM_CODE;
            udParams[13] = new SqlParameter("@AUR_CITY", SqlDbType.NVarChar);
            udParams[13].Value = user.USERMAPPINGDETAILS.selectedLocations[0].CTY_CODE; //AUR_OLD_PASSWORD 
            udParams[14] = new SqlParameter("@AUR_STATE", SqlDbType.VarChar);
            udParams[14].Value = user.USERMAPPINGDETAILS.selectedLocations[0].STE_CODE;
            udParams[15] = new SqlParameter("@AUR_ZONE", SqlDbType.NVarChar);
            udParams[15].Value = user.USERMAPPINGDETAILS.selectedLocations[0].ZN_CODE;
            udParams[16] = new SqlParameter("@AUR_ADDRESS", SqlDbType.NVarChar);
            udParams[16].Value = user.UserRoleMapping.AUR_ADDRESS;
            int returnvalue = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UPDATE_USERDETAILS", udParams);

            if (returnvalue == 1)
            {
                return new { Message = "EmployeeId already Exists" };
            }
            else
            {
                // PASSWORD CHANGE EMAIL NOTIFICATION
                if (user.UserRoleMapping.AUR_OLD_PASSWORD != null)
                {
                    if (user.UserRoleMapping.AUR_PASSWORD.ToLower() != user.UserRoleMapping.AUR_OLD_PASSWORD.ToLower())
                    {
                        SqlParameter[] ParamsEmail = new SqlParameter[3];
                        ParamsEmail[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                        ParamsEmail[0].Value = user.UserRoleMapping.AUR_ID;
                        ParamsEmail[1] = new SqlParameter("@ADMIN_ID", SqlDbType.VarChar);
                        ParamsEmail[1].Value = HttpContext.Current.Session["UID"].ToString();
                        ParamsEmail[2] = new SqlParameter("@NEW_PASSWORD", SqlDbType.VarChar);
                        ParamsEmail[2].Value = user.UserRoleMapping.AUR_PASSWORD;
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PASSWORD_CHANGE_EMAIL_NOTIFICATION", ParamsEmail);
                    }
                }
                Object MRdetails = GetUserModRolesDetails(user.UserRoleMapping.AUR_ID);
                return new { Message = MessagesVM.UM_OK, data = new { usr = user, MRD = MRdetails } };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        } 
    }

    //Save Role/Module details
    public object SaveRoleModDetls(UserRoleMappingVM user)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@ROLLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(user.UserRoleVM);
            param[1] = new SqlParameter("@UDM_CREATED_BY", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@UDM_AUR_ID", SqlDbType.NVarChar);
            param[2].Value = user.UserRoleMapping.AUR_ID;
            param[3] = new SqlParameter("@MAP_ACCESS", SqlDbType.NVarChar);
            param[3].Value = user.MapLevelAccess;
            param[4] = new SqlParameter("@COMPANY", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["COMPANYID"];
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_ROLE_DETAILS", param);

            // mapping details
            Object mappingDtls = GetUserMappingDetails(user.UserRoleMapping.AUR_ID);
            return new { Message = MessagesVM.UM_OK, data = mappingDtls };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object SaveMappingDetails(UserRoleMappingVM user)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@TWRLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedTowers);
            param[1] = new SqlParameter("@PARLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedParentEntity);
            param[2] = new SqlParameter("@CHDLST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedChildEntity);
            param[3] = new SqlParameter("@VERLST", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedVerticals);
            param[4] = new SqlParameter("@COSTLIST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedCostcenter);
            param[5] = new SqlParameter("@UDM_CREATED_BY", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["UID"];
            param[6] = new SqlParameter("@UDM_AUR_ID", SqlDbType.NVarChar);
            param[6].Value = user.UserRoleMapping.AUR_ID;
            param[7] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[7].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedLocations);
            param[8] = new SqlParameter("@MAP_ACCESS", SqlDbType.NVarChar);
            param[8].Value = user.MapLevelAccess;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_MAPPING_DETAILS", param);


            SqlParameter[] flrparam = new SqlParameter[3];
            flrparam[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            flrparam[0].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedFloors);
            flrparam[1] = new SqlParameter("@UFM_CREATED_BY", SqlDbType.NVarChar);
            flrparam[1].Value = HttpContext.Current.Session["UID"];
            flrparam[2] = new SqlParameter("@UFM_AUR_ID", SqlDbType.NVarChar);
            flrparam[2].Value = user.UserRoleMapping.AUR_ID;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_FLOOR_MAPPING_ADM", flrparam);
            return new { Message = MessagesVM.UM_OK, data = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    //for module Dropdown
    public object GetUserModRolesDetails(string Id)
    {
        try
        {
            UserRoleMappingVM URMVMlist = new UserRoleMappingVM();
            //ROLE
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Selected_Roles_By_User");
            sp.Command.AddParameter("@URL_USR_ID", Id, DbType.String);

            List<UserRoleVM> Rolelist = new List<UserRoleVM>();
            using (IDataReader Rreader = sp.GetReader())
            {
                while (Rreader.Read())
                {
                    Rolelist.Add(new UserRoleVM()
                    {
                        ROL_ID = (int)Rreader["URL_ROL_ID"],
                        ROL_ACRONYM = (string)Rreader["URL_MAP_ACCESS"],
                    });
                }
                URMVMlist.UserRoleVM = Rolelist;
                Rreader.Close();
            }

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_GET_COMPANY_MODULES_BY_USER_COMPANY");
            //sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", Id, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                List<Module> modulelist = new List<Module>();
                while (reader.Read())
                {
                    modulelist.Add(new Module()
                    {
                        MOD_CODE = reader["MOD_CODE"].ToString(),
                        MOD_NAME = reader["MOD_NAME"].ToString(),
                    });
                }
                reader.Close();
                URMVMlist.modulelist = modulelist;
            }
            return URMVMlist;
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object SaveDetails(UserRoleMappingVM user)
{
    try
    {

        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedTowers);
        param[1] = new SqlParameter("@PARLST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedParentEntity);
        param[2] = new SqlParameter("@CHDLST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedChildEntity);
        param[3] = new SqlParameter("@VERLST", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedVerticals);
        param[4] = new SqlParameter("@COSTLIST", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedCostcenter);
        param[5] = new SqlParameter("@ROLLST", SqlDbType.Structured);
        param[5].Value = UtilityService.ConvertToDataTable(user.UserRoleVM);
        param[6] = new SqlParameter("@UDM_CREATED_BY", SqlDbType.NVarChar);
        param[6].Value = HttpContext.Current.Session["UID"];
        param[7] = new SqlParameter("@UDM_AUR_ID", SqlDbType.NVarChar);
        param[7].Value = user.UserRoleMapping.AUR_ID;

        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_MAPPING_DETAILS", param);

        SqlParameter[] flrparam = new SqlParameter[3];
        flrparam[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        flrparam[0].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedFloors);
        flrparam[1] = new SqlParameter("@UFM_CREATED_BY", SqlDbType.NVarChar);
        flrparam[1].Value = HttpContext.Current.Session["UID"];
        flrparam[2] = new SqlParameter("@UFM_AUR_ID", SqlDbType.NVarChar);
        flrparam[2].Value = user.UserRoleMapping.AUR_ID;
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_FLOOR_MAPPING_ADM", flrparam);

        //update user details
        SqlParameter[] udParams = new SqlParameter[12];
        udParams[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        udParams[0].Value = user.UserRoleMapping.AUR_ID;
        udParams[1] = new SqlParameter("@AUR_EMAIL", SqlDbType.VarChar);
        udParams[1].Value = user.UserRoleMapping.AUR_EMAIL;
        udParams[2] = new SqlParameter("@AUR_PHONE", SqlDbType.VarChar);
        udParams[2].Value = user.UserRoleMapping.AUR_RES_NUMBER;
        udParams[3] = new SqlParameter("@AUR_DOB", SqlDbType.Date);
        udParams[3].Value = user.UserRoleMapping.AUR_DOB;
        udParams[4] = new SqlParameter("@AUR_DOJ", SqlDbType.Date);
        udParams[4].Value = user.UserRoleMapping.AUR_DOJ;
        udParams[5] = new SqlParameter("@AUR_CNY", SqlDbType.VarChar);
        udParams[5].Value = user.UserRoleMapping.AUR_COUNTRY;
        udParams[6] = new SqlParameter("@AUR_REPTO", SqlDbType.VarChar);
        udParams[6].Value = user.UserRoleMapping.AUR_REPORTING_TO;
        udParams[7] = new SqlParameter("@AUR_EXTN", SqlDbType.VarChar);
        udParams[7].Value = user.UserRoleMapping.AUR_EXTENSION;
        udParams[8] = new SqlParameter("@AUR_STATUS", SqlDbType.VarChar);
        udParams[8].Value = user.UserRoleMapping.AUR_STA_ID;
        udParams[9] = new SqlParameter("@AUR_PASSWORD", SqlDbType.NVarChar);
        udParams[9].Value = user.UserRoleMapping.AUR_PASSWORD; //AUR_OLD_PASSWORD 

        udParams[10] = new SqlParameter("@AUR_VERTICAL", SqlDbType.VarChar);
        udParams[10].Value = user.UserRoleMapping.AUR_DEP_ID;
        udParams[11] = new SqlParameter("@AUR_COSTCENTER", SqlDbType.NVarChar);
        udParams[11].Value = user.UserRoleMapping.AUR_PRJ_CODE;
        int returnvalue = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UPDATE_USERDETAILS", udParams);


        if (returnvalue == 1)
        {
            return new { Message = "EmployeeId already Exists" };
        }
        else
        {
            // PASSWORD CHANGE EMAIL NOTIFICATION
            if (user.UserRoleMapping.AUR_OLD_PASSWORD != null)
            {
                if (user.UserRoleMapping.AUR_PASSWORD.ToLower() != user.UserRoleMapping.AUR_OLD_PASSWORD.ToLower())
                {
                    SqlParameter[] ParamsEmail = new SqlParameter[3];
                    ParamsEmail[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                    ParamsEmail[0].Value = user.UserRoleMapping.AUR_ID;
                    ParamsEmail[1] = new SqlParameter("@ADMIN_ID", SqlDbType.VarChar);
                    ParamsEmail[1].Value = HttpContext.Current.Session["UID"].ToString();
                    ParamsEmail[2] = new SqlParameter("@NEW_PASSWORD", SqlDbType.VarChar);
                    ParamsEmail[2].Value = user.UserRoleMapping.AUR_PASSWORD;
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PASSWORD_CHANGE_EMAIL_NOTIFICATION", ParamsEmail);
                }
            }
        }
        return new { Message = MessagesVM.UM_OK, data = user };
    }
    catch (Exception)
    {
        return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }
}

    public object GetRMDetails_Step2(string user)
    {
        try
        {
            string UID = user.Replace("_", "/");
            Object MRdetails = GetUserModRolesDetails(user);
            return new { Message = MessagesVM.UM_OK, data = MRdetails };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetMappingDetails_Step3(string user)
    {
        try
        {
            string UID = user.Replace("__", "/");
            Object Mappingdetails = GetUserMappingDetails(UID);
            return new { Message = MessagesVM.UM_OK, data = Mappingdetails };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    //Emp mapping details
    public object GetUserMappingDetails(string Id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Edit_User_Mapping_Details_ADM");
        sp.Command.AddParameter("@UDM_AUR_ID", Id, DbType.String);
        USERMAPPINGDETAILS Userlist = new USERMAPPINGDETAILS();
        DataSet ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                Userlist.selectedCountries = new List<Countrylst>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Userlist.selectedCountries.Add(new Countrylst { CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), CNY_NAME = Convert.ToString(dr["CNY_NAME"]), ticked = true });
                }
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                Userlist.selectedCities = new List<Citylst>();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    Userlist.selectedCities.Add(new Citylst { CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CTY_NAME = Convert.ToString(dr["CTY_NAME"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ticked = true });
                }
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                Userlist.selectedLocations = new List<Locationlst>();
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    Userlist.selectedLocations.Add(new Locationlst { LCM_CODE = Convert.ToString(dr["UDM_LCM_CODE"]), LCM_NAME = Convert.ToString(dr["LCM_NAME"]), CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ticked = true });
                }
            }

            if (ds.Tables[3].Rows.Count > 0)
            {
                Userlist.selectedTowers = new List<Towerlst>();
                foreach (DataRow dr in ds.Tables[3].Rows)
                {
                    Userlist.selectedTowers.Add(new Towerlst { TWR_CODE = Convert.ToString(dr["UDM_TWR_CODE"]), TWR_NAME = Convert.ToString(dr["TWR_NAME"]), LCM_CODE = Convert.ToString(dr["UDM_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ticked = true });
                }
            }

            if (ds.Tables[4].Rows.Count > 0)
            {
                Userlist.selectedFloors = new List<Floorlst>();
                foreach (DataRow dr in ds.Tables[4].Rows)
                {
                    Userlist.selectedFloors.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["UFM_FLR_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["UDM_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["UDM_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["UDM_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["UDM_CNY_CODE"]), ticked = true });
                }
            }

            if (ds.Tables[5].Rows.Count > 0)
            {
                Userlist.selectedVerticals = new List<Verticallst>();
                foreach (DataRow dr in ds.Tables[5].Rows)
                {
                    Userlist.selectedVerticals.Add(new Verticallst { VER_CODE = Convert.ToString(dr["UVM_VERT_CODE"]), VER_NAME = Convert.ToString(dr["VER_NAME"]), ticked = true });
                }
            }

            if (ds.Tables[6].Rows.Count > 0)
            {
                Userlist.selectedCostcenter = new List<Costcenterlst>();
                foreach (DataRow dr in ds.Tables[6].Rows)
                {
                    Userlist.selectedCostcenter.Add(new Costcenterlst { Cost_Center_Code = Convert.ToString(dr["UCM_COST_CENTER_CODE"]), Cost_Center_Name = Convert.ToString(dr["Cost_Center_Name"]), Vertical_Code = Convert.ToString(dr["UCM_VERT_CODE"]), ticked = true });
                }
            }

            if (ds.Tables[7].Rows.Count > 0)
            {
                Userlist.selectedParentEntity = new List<ParentEntityLst>();
                foreach (DataRow dr in ds.Tables[7].Rows)
                {
                    Userlist.selectedParentEntity.Add(new ParentEntityLst { PE_CODE = Convert.ToString(dr["UPE_PE_CODE"]), PE_NAME = Convert.ToString(dr["PE_NAME"]), ticked = true });
                }
            }

            if (ds.Tables[8].Rows.Count > 0)
            {
                Userlist.selectedChildEntity = new List<ChildEntityLst>();
                foreach (DataRow dr in ds.Tables[8].Rows)
                {
                    Userlist.selectedChildEntity.Add(new ChildEntityLst { CHE_CODE = Convert.ToString(dr["UCHE_CHE_CODE"]), CHE_NAME = Convert.ToString(dr["CHE_NAME"]), PE_CODE = Convert.ToString(dr["UCHE_PE_CODE"]), ticked = true });
                }
            }
        }
        return Userlist;
    }


    //EXCEL UPLOAD
    public dynamic UploadTemplate(HttpRequest httpRequest)
{
    try
    {
        if (httpRequest.Files.Count > 0)
        {
            SqlParameter[] udParams = new SqlParameter[1];
            String guid = System.Guid.NewGuid().ToString();

            DataTable UsrDtl = GetDataTableFrmReq(httpRequest);
            string strSQL;
            if (UsrDtl == null)
                return new { Message = MessagesVM.UAD_UPLVALDAT, data = (object)null };
            if (UsrDtl.Rows.Count > 0)
            {
                foreach (DataRow x in UsrDtl.Select())
                {
                    strSQL = "INSERT INTO " + HttpContext.Current.Session["TENANT"].ToString() + "." + "UDM_MAPPING_DETAILS_TEMP (HST_REQ_ID, HST_CNY_NAME,HST_CTY_NAME,HST_ZN_NAME,HST_STE_NAME,HST_LCM_NAME, HST_TWR_NAME, HST_FLR_NAME, HST_VERT_NAME, HST_CST_NAME, HST_PAR_ENTY_NAME, HST_CHL_ENTY_NAME, HST_USR_ID,HST_USR_ROLE_NAME, HST_CREATED_DT, HST_CREATED_BY) VALUES('" + guid + "','" + x["COUNTRY_NAME"] + "','" + x["ZONE_NAME"] + "','" + x["STATE_NAME"] + "','" + x["CITY_NAME"] + "','" + x["LOCATION_NAME"] + "','" + x["TOWER_NAME"] + "','" + x["FLOOR_NAME"] + "','" + x["VERTICAL_NAME"] + "','" + x["COSTCENTER_NAME"] + "','" + x["PARENT_ENTITY"] + "','" + x["CHILD_ENTITY"] + "','" + x["USER_ID"] + "','" + x["USER_ROLE"] + "','" + System.DateTime.Now + "','" + HttpContext.Current.Session["UID"].ToString() + "')";
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL);
                }

                udParams[0] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
                udParams[0].Value = guid;
                DataSet DS = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UDM_EXCEL_UPLOAD_CHECKER", udParams);
                return new { Message = MessagesVM.UAD_UPLNOREC, data = DS };
            }
            else
                return new { Message = MessagesVM.UAD_UPLNOREC, data = (object)null };
        }
        else
            return new { Message = MessagesVM.UAD_UPLNO, data = (object)null };
    }
    catch (Exception ex)
    {
        return new { Message = ex.ToString(), data = (object)null };
    }
}

public DataTable GetDataTableFrmReq(HttpRequest httpRequest)
{
    DataTable uplst = new DataTable();
    foreach (string file in httpRequest.Files)
    {
        var postedFile = httpRequest.Files[file];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);
        uplst = ReadAsList(filePath);
    }
    if (uplst != null)
        if (uplst.Rows.Count != 0)
            uplst.Rows.RemoveAt(0);
    return uplst;
}

public DataTable ReadAsList(string Path)
{
    string fileName = Path;
    SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false);

    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
    string relationshipId = sheets.First().Id.Value;
    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
    Worksheet workSheet = worksheetPart.Worksheet;
    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
    IEnumerable<Row> rows = sheetData.Descendants<Row>();

    DataTable dt = new DataTable();
    foreach (Cell cell in rows.ElementAt(0))
    {
        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
    }
    foreach (Row row in rows) //this will also include your header row...
    {
        DataRow tempRow = dt.NewRow();
        int columnIndex = 0;
        foreach (Cell cell in row.Descendants<Cell>())
        {
            // Gets the column index of the cell with data
            int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
            cellColumnIndex--; //zero based index
            if (columnIndex < cellColumnIndex)
            {
                do
                {
                    tempRow[columnIndex] = ""; //Insert blank data here;
                    columnIndex++;
                }
                while (columnIndex < cellColumnIndex);
            }
            tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

            columnIndex++;
        }
        dt.Rows.Add(tempRow);
    }
    spreadSheetDocument.Close();
    return dt;
}

public static string GetColumnName(string cellReference)
{
    Regex regex = new Regex("[A-Za-z]+");
    Match match = regex.Match(cellReference);
    return match.Value;
}

public static int? GetColumnIndexFromName(string columnName)
{
    //return columnIndex;
    string name = columnName;
    int number = 0;
    int pow = 1;
    for (int i = name.Length - 1; i >= 0; i--)
    {
        number += (name[i] - 'A' + 1) * pow;
        pow *= 26;
    }
    return number;
}

public static string GetCellValue(SpreadsheetDocument document, Cell cell)
{
    SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
    if (cell.CellValue == null)
    {
        return "";
    }
    string value = cell.CellValue.InnerXml;
    if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
    {
        return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
    }
    else
    {
        return value;
    }
}

public List<UserRoleMapping> EmployeeData()
{
    List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
    try
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_GET_USER_ROLE_MAPPING_DETAILS").GetReader())
        {

            while (reader.Read())
            {
                URMlist.Add(new UserRoleMapping()
                {
                    AUR_ID = reader["AUR_ID"].ToString(),
                    AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                    AUR_EMAIL = reader["AUR_EMAIL"].ToString(),
                    ROL_DESCRIPTION = reader["ROL_DESCRIPTION"].ToString(),
                    AUR_DESGN_ID = reader["AUR_DESGN_ID"].ToString(),
                    AUR_DEP_ID = reader["AUR_DEP_ID"].ToString(),

                });
            }
            reader.Close();
            return URMlist;
        }
    }
    catch (Exception)
    {
        return URMlist;
    }
}

}