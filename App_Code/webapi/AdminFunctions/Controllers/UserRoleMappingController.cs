﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;

public class UserRoleMappingController : ApiController
{
    UserRoleMappingService UserRoleService = new UserRoleMappingService();
   

    //Grid View
    [HttpGet]
    public HttpResponseMessage GetUserRoleDetails()
    {
        var URMlist = UserRoleService.GetUserRoleDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetUserOnSelection(String Id)
    {
       var URMlist = UserRoleService.GetUserOnSelection(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    //for  Module Dropdown
    [HttpGet]
    public HttpResponseMessage Getmodules()
    {
        var modulelist = UserRoleService.Getmodules();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, modulelist);
        return response;
    }

    //For Access CheckBox By Module change
    [HttpPost]
    public HttpResponseMessage GetAccessByModule(UserCompanyModules ucm)
    // public HttpResponseMessage GetAccessByModule(string id)
    {
        var Rolelist = UserRoleService.GetAccessByModule(ucm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Rolelist);
        return response;
    }
    //save Emp Details
    [HttpPost]
    public HttpResponseMessage SaveEmpDetails(UserRoleMappingVM dataobject)
    {
        var user = UserRoleService.SaveEmpDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
    //Save Role/Module details
    [HttpPost]
    public HttpResponseMessage SaveRoleModDetls(UserRoleMappingVM dataobject)
    {
        var user = UserRoleService.SaveRoleModDetls(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    //Save Mapping details
    [HttpPost]
    public HttpResponseMessage SaveMappingDetails(UserRoleMappingVM dataobject)
    {
        var URMlist = UserRoleService.SaveMappingDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveDetails(UserRoleMappingVM dataobject)
    {
        var user=UserRoleService.SaveDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response; 
    }

    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        dynamic retobj = UserRoleService.UploadTemplate(httpRequest);
        string retContent = "";
        if (retobj.data != null)
            retContent = JsonConvert.SerializeObject(retobj);
        else
            retContent = JsonConvert.SerializeObject(retobj);

        var response = Request.CreateResponse(HttpStatusCode.OK);
        response.Content = new StringContent(retContent, Encoding.UTF8, "text/plain");
        response.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> GetEmployeeData([FromBody]UserRoleMappingVM EmpData)
    {
        ReportGenerator<UserRoleMapping> GenEmployeeData = new ReportGenerator<UserRoleMapping>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/EmployeeData.rdlc"),
            DataSetName = "EmployeeData",
            ReportType = "Employee Data"
        };
        UserRoleService = new UserRoleMappingService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/EmployeeData." + EmpData.Type);
        List<UserRoleMapping> reportdata = UserRoleService.EmployeeData();
        await GenEmployeeData.GenerateReport(reportdata, filePath, EmpData.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "EmployeeData." + EmpData.Type;
        return result;
    }
    //Step2 -> get role/module details
    [HttpGet]
    public HttpResponseMessage GetRMDetails_Step2(String Id)
    {
        var URMlist = UserRoleService.GetRMDetails_Step2(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    //Step3 -> get User Mapping details
    [HttpGet]
    public HttpResponseMessage GetMappingDetails_Step3(String Id)
    {
        var URMlist = UserRoleService.GetMappingDetails_Step3(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }
}
