﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// Summary description for ItemIssuanceVM
/// </summary>
public class ItemIssuanceVM
{
    public String Category { get; set; }
    public String SubCategory { get; set; }
    public String Brand { get; set; }
    public String Location { get; set; }
    public String Model { get; set; }
    public int jtStartIndex { get; set; }
    public int jtPageSize { get; set; }
    public String jtSorting { get; set; }
    public String[] modelarray { get; set; }
    public List<IsseuedItemsVM> Jsondata { get; set; }
}

public class IsseuedItemsVM
{
    public int Id { get; set; }
    public string Item { get; set; }
    public string Subcat { get; set; }
    public decimal Price { get; set; }
    public string totavbl { get; set; }
    public int Quantity { get; set; }
    public string Discount { get; set; }
    public string location { get; set; }
    public string userid { get; set; }
    public int status { get; set; }
    public int IssueStat { get; set; }
}