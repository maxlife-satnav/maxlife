﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ItemIssuanceService
/// </summary>
public class ItemIssuanceService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public DataTable GetallEmployeeIDDS()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AMT_BINDUSERS_SP");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetallLocations()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCTION_ISSUANCES");
        sp.Command.Parameters.Add("@dummy", 1, DbType.Int16);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetallCategories()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_ASSETCATEGORIESSCON_CON");
        sp.Command.Parameters.Add("@dummy", 1, DbType.Int16);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public Object Getallsubcatbycat(ItemIssuanceVM category)
    {
        if (category != null)
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AST_GET_SUBCATBYVENDORS");
            sp.Command.Parameters.Add("@VT_CODE", category.Category, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables[0];
        }
        return null;
    }

    public DataTable GetallBrandbysubcat(ItemIssuanceVM category)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AST_GET_MAKEBYCATSUBCAT");
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category.Category, DbType.String);
        sp.Command.AddParameter("@manufacturer_type_subcode", category.SubCategory, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public Object GetallItemstotbl(ItemIssuanceVM category, int? jtStartIndex, int? jtPageSize, string jtSorting)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AST_GET_MODEL_LISTFORGRID");
        sp.Command.AddParameter("@AST_MD_CATID", category.Category, DbType.String);
        sp.Command.AddParameter("@AST_MD_SUBCATID", category.SubCategory, DbType.String);
        sp.Command.AddParameter("@AST_MD_BRDID", category.Brand, DbType.String);
        ds = sp.GetDataSet();
        var list = ds.Tables[0].AsEnumerable().ToList();
        list = list.Skip((int)jtStartIndex).Take((int)jtPageSize).ToList();
        var finallist = from rec in list
                        select new
                        {
                            AST_MD_ID = rec.Field<int>("AST_MD_ID"),
                            BRAND = rec.Field<string>("BRAND"),
                            MDCODE = rec.Field<string>("MDCODE"),
                            MDNAME = rec.Field<string>("MDNAME"),
                            SUBCAT = rec.Field<string>("SUBCAT"),
                            SUBCATID = rec.Field<string>("SUBCATID"),
                            UNITS = rec.Field<string>("UNITS")
                        };

        return new { Result = "OK", Records = finallist, TotalRecordCount = ds.Tables[0].Rows.Count };
    }

    public object Getallselectedmodels(ItemIssuanceVM category)
    {
        string[] arr = category.modelarray;
        string models = String.Join("/", category.modelarray);
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AST_GET_CONSUMABLES_FOR_ASSET_ISSUANCE");
        sp.Command.AddParameter("@MDID", models, DbType.String);
        ds = sp.GetDataSet();
        string json = JsonConvert.SerializeObject(ds.Tables[0]);
        return json;

    }

    public string Submitdetails(ItemIssuanceVM category)
    {
        List<IsseuedItemsVM> arr = category.Jsondata;
        //DataTable dt = ConvertToTable.ToDataTable<IsseuedItemsVM>(arr);
        object flag = 0;
        bool check = true;
        string reqid = "6666/AST/ISSUE/00001";
        do
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_ITEMS_ISSUED");
            sp.Command.AddParameter("@LOCATION", arr[0].location, DbType.String);
            sp.Command.AddParameter("@USERID", arr[0].userid, DbType.String);
            //reqid = sp.ExecuteScalar().ToString();
            DataSet ds = sp.GetDataSet();
            reqid = ds.Tables[0].Rows[0][0].ToString();
            check = false;
        } while (check);

        if (reqid != "")
        {
            foreach (var asset in arr)
            {
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_ITEM_ISSUED_DETAILS");
                sp.Command.AddParameter("@MODID", asset.Id, DbType.String);
                sp.Command.AddParameter("@PRICE", asset.Price, DbType.String);
                sp.Command.AddParameter("@QTY", asset.Quantity, DbType.String);
                sp.Command.AddParameter("@TOTAVBL", asset.totavbl, DbType.String);
                sp.Command.AddParameter("@DISCOUNT", asset.Discount, DbType.String);
                sp.Command.AddParameter("@LOCATION", asset.location, DbType.String);
                sp.Command.AddParameter("@USERID", asset.userid, DbType.String);
                sp.Command.AddParameter("@REQID", reqid, DbType.String);
                sp.Command.AddParameter("@ISSUESTAT", asset.IssueStat, DbType.String);
                //flag = sp.ExecuteScalar();
                DataSet ds = sp.GetDataSet();
                flag = ds.Tables[0].Rows[0][0].ToString();
                asset.status = Convert.ToInt32(flag);

            }
        }
        return reqid;
    }

}


//public static class ConvertToTable
//{
//    public static DataTable ToDataTable<IsseuedItemsVM>(this IList<IsseuedItemsVM> data)
//    {
//        DataTable dataTable = new DataTable(typeof(IsseuedItemsVM).Name);
//        PropertyInfo[] props = typeof(IsseuedItemsVM).GetProperties(BindingFlags.Public | BindingFlags.Instance);
//        foreach (PropertyInfo prop in props)
//        {
//            dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ??
//                prop.PropertyType);
//        }

//        foreach (IsseuedItemsVM item in data)
//        {
//            var values = new object[props.Length];
//            for (int i = 0; i < props.Length; i++)
//            {
//                values[i] = props[i].GetValue(item, null);
//            }
//            dataTable.Rows.Add(values);
//        }
//        return dataTable;
//    }


//}
