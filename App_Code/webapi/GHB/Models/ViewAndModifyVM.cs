﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CheckInAndOutVM
/// </summary>
public class ViewAndModifyVM
{
    public string BM_REQ_ID { get; set; }
    public DateTime BM_FROM_DATE { get; set; }
    public DateTime BM_TO_DATE { get; set; }
    public string BM_FRM_TIME { get; set; }
    public string BM_TO_TIME { get; set; }
    public string BM_REFERENCE_ID { get; set; }
    public string RESERVED_FOR_EMAIL { get; set; }
    public string RESERVED_FOR { get; set; }
    public string RESERVED_BY_EMAIL { get; set; }
    public string RESERVED_BY { get; set; }
    public DateTime RESERVED_DT { get; set; }
    public string RR_NAME { get; set; }
    public string BM_LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string RF_NAME { get; set; }

}

public class ModifyABVM
{
    public bool ticked { get; set; }

    public string RR_RT_SNO { get; set; }
    public string RR_RF_SNO { get; set; }
    public string RoomName { get; set; }

    public string COUNTRY { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }

    public string RF_CNY_CODE { get; set; }
    public string RF_CTY_CODE { get; set; }
    public string RF_LOC_CODE { get; set; }

    public string RT_NAME { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }

    public string RR_SNO { get; set; }
    public string WD_REQ_ID { get; set; }



}
