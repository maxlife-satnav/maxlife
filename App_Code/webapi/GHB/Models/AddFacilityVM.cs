﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class AddFacilityVM
{
   
    public int RF_SNO { get; set; }
    public string RT_SNO { get; set; }
    public string RF_NAME { get; set; }
    public string RF_EMAIL { get; set; }
    public string RF_CNY_CODE { get; set; }
    public string RF_CTY_CODE { get; set; }
    public string RF_LOC_CODE { get; set; }
    public string RF_LONG { get; set; }
    public string RF_LAT { get; set; }
    public string RF_NO_OF_ROOMS { get; set; }
    public string RF_REMARKS { get; set; }


    public int RR_SNO { get; set; }
    public string RT_NAME { get; set; }
    public string RR_NAME { get; set; }

    public string CNY_NAME { get; set; }
    public string CTY_NAME {get;set;}
    public string LCM_NAME { get; set; }

    public string RF_STATUS { get; set; }

    public string RRDT_FILE_NAME { get; set; }

    public int Flag { get; set; }

    public List<AddRoomVM> addroom { get; set; }
}


public class AddRoomVM
{

    public int RR_RT_SNO { get; set; }
    public int RR_RF_SNO { get; set; }
    public string RR_NAME { get; set; }
    public int RR_CAPCITY { get; set; }
    public string RR_FACILITIES { get; set; }
    public string dataImg { get; set; }
    public string ImageName { get; set; }
    public int IsActive { get; set; }
    public int RR_SNO { get; set; }
    public string FileName { get; set; }

}

//public class UpldResrvType
//{
//    public string UplAllocType { get; set; }
//    public string UplOptions { get; set; }
//}