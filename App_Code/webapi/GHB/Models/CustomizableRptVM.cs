﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for CustomizableRptVM
/// </summary>
public class CustomizableRptVM
{	
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public List<Locationlst> lcmlst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Countrylst> cnylst { get; set; }
    public string Status { get; set; }
    public string Type { get; set; }	
}



public class Lcmlst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class Custcnylst
{
    public string CNY_CODE { get; set; }
    public string CNY_NAME { get; set; }
    public bool ticked { get; set; }
}

public class Ctylst
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class CustomizableRpteDetails
{
    public string RB_REQ_ID { get; set; }
    public string RT_NAME { get; set; }
    public DateTime RB_FROM_DATE { get; set; }
    public DateTime RB_TO_DATE { get; set; }

    public string RB_FRM_TIME { get; set; }
    public string RB_TO_TIME { get; set; }

    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RR_CAPCITY { get; set; }
    public string RBS_CNY_CODE { get; set; }
    public string RBS_CTY_CODE { get; set; }
    public string RBS_LOC_CODE { get; set; }
    public string DEP_NAME { get; set; }
    public string RB_CREATEDBY { get; set; }
    public string RB_CREATEDON { get; set; }
    public string RB_RESERVED_FOR { get; set; }

    public string STA_TITLE { get; set; }
    public string RESVERED_FOR_EMAIL { get; set; }
    public string RESVERED_BY_EMAIL { get; set; }

    public string RB_CHK_IN_TIME { get; set; }
    public string RB_CHK_OUT_TIME { get; set; }

    public string RB_REFERENCE_ID { get; set; }
    public string Cost_Center_Name { get; set; }

}

