﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for AddReservationController
/// </summary>
public class AddFacilityController : ApiController
{

    AddFacilityService service = new AddFacilityService();


    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<AddFacilityVM> griddata = service.GetGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetRoomDetails(AddFacilityVM obj)
    {
        IEnumerable<AddRoomVM> griddata = service.GetRoomDetails(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Create(AddFacilityVM obj)
    {
        var retobj = service.Create(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retobj);
        return response;

    }
   

    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = service.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

   
}