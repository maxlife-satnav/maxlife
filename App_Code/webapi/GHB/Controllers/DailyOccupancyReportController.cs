﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;


public class DailyOccupancyReportController : ApiController
{
    DailyOccupancyService userguthoudt = new DailyOccupancyService();

    [HttpPost]
    public async Task<HttpResponseMessage> GetDailyOccupancyDetailsReport([FromBody]DailyOccupancyVM UserGHdata)
    {
        ReportGenerator<DailyOccupancyDetailsGrid> reportgen = new ReportGenerator<DailyOccupancyDetailsGrid>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/GuestHouse_Mgmt/DailyOccupancyReport.rdlc"),
            DataSetName = "DailyOccupancyRpt",
            ReportType = "Daily Occupancy Report"
        };

        List<DailyOccupancyDetailsGrid> reportdata = userguthoudt.GetDailyOccupancyDetails(UserGHdata);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/DailyOccupancyReport." + UserGHdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, UserGHdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "DailyOccupancyReport." + UserGHdata.Type;
        return result;
    }

    [HttpPost]
    public HttpResponseMessage GetDailyOccupancyDetails(DailyOccupancyVM UGHU)
    {
        var obj = userguthoudt.GetDailyOccupancyDetailsObj(UGHU);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

 
}
