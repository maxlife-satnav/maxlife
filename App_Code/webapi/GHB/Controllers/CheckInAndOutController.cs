﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class CheckInAndOutController : ApiController
{
    CheckInAndOutService chkinoutservice = new CheckInAndOutService();

    [HttpGet]
    public HttpResponseMessage GetChkInOutDetails()
    {
        var obj = chkinoutservice.GetDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetDetailsByReqId(string reqId)
    {
        var obj = chkinoutservice.GetDetailsByReqId(reqId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SubmitCheckinoutData(SaveCheckInOutData chkobj)
    {
        var obj = chkinoutservice.SubmitCheckinoutData(chkobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
