﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;



public class AdminBookingController : ApiController
{
    AdminBookingService abServc = new AdminBookingService();
    
    [HttpPost]
    public HttpResponseMessage GetViewAvailabilityDetails(AdminBookingDetails vabData)
    {
        var obj = abServc.GetViewAvailableObject(vabData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    
    [HttpPost]
    public HttpResponseMessage SaveBookingRequest(AdminBookingVM obj)
    {
        var retMsg = abServc.SaveBookingRequest(obj);
        if ( retMsg != null)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retMsg);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Guest House is Already Booked between your selected dates.");
        }

    }
    
    [HttpPost]
    public HttpResponseMessage GetBookingDetails(AdminBookingVM rrno)
    {
       var resp = abServc.GetAllEvents(rrno);
        var eventList = from e in resp
                        select new
                        {
                            
                            title = e.title,
                            start = e.start,
                            end = e.end,
                            allDay = true,
                            className = e.className,
                            referenceID = e.ReferenceID,
                            reservedFor = e.ReservedFor,
                            remarks = e.Remarks,
                            AUR_NAME = e.AUR_NAME,
                            //CNY_NAME = e.CNY_NAME,
                            //CTY_NAME = e.CTY_NAME,
                            //LCM_NAME = e.LCM_NAME,
                            //BOOKED_RR_NAME = e.BOOKED_RR_NAME,
                            RB_TYPE = e.RB_TYPE,
                            RoomName = e.RoomNames,
                            LocationName = e.LCM_NAME,
                            RF_NAME = e.RF_NAME
                        };
        var rows = eventList.ToArray();

        var response = Request.CreateResponse(HttpStatusCode.OK, resp);
        return response;
    }

   
}
