﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;


public class UserGuestHouseUltReportController : ApiController
{
    UserGuestHouseUltReportService userguthoudt = new UserGuestHouseUltReportService();

    [HttpPost]
    public async Task<HttpResponseMessage> GetUserGHDetailsReportdata([FromBody]UserGuestHouseUltVM UserGHdata)
    {
        ReportGenerator<UserGuestHouseDetails> reportgen = new ReportGenerator<UserGuestHouseDetails>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/GuestHouse_Mgmt/UserGuestHouseUtilizationRpt.rdlc"),
            DataSetName = "UserGuestHouseRptDS",
            ReportType = "User Guest House Utilization Report"
        };

        List<UserGuestHouseDetails> reportdata = userguthoudt.Getdetails(UserGHdata);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/UserGuestHouseUtlRpt." + UserGHdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, UserGHdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "UserGuestHouseUtilizationReport." + UserGHdata.Type;
        return result;
    }

    [HttpPost]
    public HttpResponseMessage GetGuesthousedetails(UserGuestHouseUltVM UGHU)
    {
        var obj = userguthoudt.Getdetails(UGHU);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetGHStatusDetails()
    {
        var obj = userguthoudt.GetGHStatusDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
