﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;



public class WithholdController : ApiController
{
    WithholdService abServc = new WithholdService();
    
    [HttpPost]
    public HttpResponseMessage GetViewAvailabilityDetails(WithholdDetails vabData)
    {
        var obj = abServc.GetViewAvailableObject(vabData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

   
    [HttpPost]
    public HttpResponseMessage SaveBookingRequest(WithholdVM obj)
    {
        var retMsg = abServc.SaveBookingRequest(obj);
        if ( retMsg != null)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retMsg);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Guest House is Already Booked between your selected dates.");
        }

    }
  [HttpGet]
    public HttpResponseMessage GetBookingDetails(string ScreenType, string LocationCode, string start, string end)
    {
       var resp = abServc.GetAllEvents(ScreenType, LocationCode, start, end);
        var eventList = from e in resp
                        select new
                        {
                            
                            title = e.title,
                            start = e.start,
                            end = e.end,
                            allDay = true,
                            className = e.className,
                            referenceID = e.ReferenceID,
                            reservedFor = e.ReservedFor,
                            remarks = e.Remarks,
                            RB_TYPE = e.RB_TYPE,
                            RoomName = e.RoomNames,
                            LocationName = e.LCM_NAME
                        };
        var rows = eventList.ToArray();

        var response = Request.CreateResponse(HttpStatusCode.OK, resp);
        return response;
    }
   
}
