﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class ViewAvailabilityService
{
    SubSonic.StoredProcedure sp;
    List<ViewAvailabilityData> vadlst;
    ViewAvailabilityData vad;
    DataSet ds;

    public object GetViewAvailableObject(ViewAvailabilityDetails Det)
    {
        try
        {
            vadlst = GetViewAvailableDetails(Det);
            if (vadlst.Count != 0) { return new { Message = MessagesVM.SER_OK, data = vadlst }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<ViewAvailabilityData> GetViewAvailableDetails(ViewAvailabilityDetails Details)
    {
        try
        {
            List<ViewAvailabilityData> CData = new List<ViewAvailabilityData>();
            SqlParameter[] param = new SqlParameter[8];

            param[0] = new SqlParameter("@LOCLIST", SqlDbType.Structured);

            if (Details.loclst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Details.loclst);
            }

            param[1] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
            param[1].Value = Details.FromDate.ToString();
            param[2] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
            param[2].Value = Details.ToDate.ToString();
            param[3] = new SqlParameter("@STAT", SqlDbType.Int);
            param[3].Value = Details.STAT;
            param[4] = new SqlParameter("@RF_LIST", SqlDbType.Structured);

            if (Details.rflist == null)
            {
                param[4].Value = null;
            }
            else
            {
                param[4].Value = UtilityService.ConvertToDataTable(Details.rflist);
            }
            param[5] = new SqlParameter("@FROMTIME", SqlDbType.NVarChar);
            param[5].Value = Details.FromTime.ToString();
            param[6] = new SqlParameter("@TOTIME", SqlDbType.NVarChar);
            param[6].Value = Details.ToTime.ToString();
            param[7] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["UID"].ToString();

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_AVAILABILITY_DATA_BINDGRID", param))
            {
                while (reader.Read())
                {
                    vad = new ViewAvailabilityData();
                    vad.COUNTRY = Convert.ToString(reader["CNY_NAME"]);
                    vad.CITY = Convert.ToString(reader["CTY_NAME"]);
                    vad.LOCATION = Convert.ToString(reader["LCM_NAME"]);
                    vad.RT_NAME = Convert.ToString(reader["RT_NAME"]);
                    //vad.RR_CODE = Convert.ToString(reader["RR_CODE"]);
                    vad.RR_NAME = Convert.ToString(reader["RR_NAME"]);               
                    vad.RRDT_FILE_NAME = Convert.ToString(reader["RRDT_FILE_NAME"]);
                    vad.RF_NAME = Convert.ToString(reader["RF_NAME"]);

                    CData.Add(vad);
                }
                reader.Close();
            }
            return CData;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //public object GetReservationTypes(int id)
    //{
    //    List<ReservationTypeslist> rtplst = new List<ReservationTypeslist>();
    //    ReservationTypeslist rtp;
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Reservation_Types");
    //    sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
    //    sp.Command.AddParameter("@MODE", id, DbType.Int32);
    //    using (IDataReader sdr = sp.GetReader())
    //    {
    //        while (sdr.Read())
    //        {
    //            rtp = new ReservationTypeslist();
    //            rtp.RT_CODE = sdr["RT_CODE"].ToString();
    //            rtp.RT_NAME = sdr["RT_NAME"].ToString();
    //            rtp.ticked = false;
    //            rtplst.Add(rtp);
    //        }
    //    }
    //    if (rtplst.Count != 0)
    //        return new { Message = MessagesVM.UM_OK, data = rtplst };
    //    else
    //        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    //}
}