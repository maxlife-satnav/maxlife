﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;


/// <summary>
/// Summary description for CheckInAndOutService
/// </summary>
public class CheckInAndOutService
{
    SubSonic.StoredProcedure sp;
    UserGuestHouseDetails UsrHosdt;
    DataSet ds;

    public object GetDetails()
    {

        ChkInOutDetails chkdetails;
        List<ChkInOutDetails> chkdetailslist = new List<ChkInOutDetails>();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_DETAILS_TO_CHECK_IN_OUT");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            using (IDataReader reader = sp.GetReader())
            {                
                while (reader.Read())
                {
                    chkdetails = new ChkInOutDetails();
                    chkdetails.RB_REQ_ID = reader["RB_REQ_ID"].ToString();
                    chkdetails.RT_NAME = reader["RT_NAME"].ToString();
                    chkdetails.RF_NAME = reader["RF_NAME"].ToString();
                    chkdetails.RR_NAME = reader["RR_NAME"].ToString();


                    chkdetails.CNY_NAME = reader["CNY_NAME"].ToString();
                    chkdetails.CTY_NAME = reader["CTY_NAME"].ToString();
                    chkdetails.LCM_NAME = reader["LCM_NAME"].ToString();

                    chkdetails.RF_CNY_CODE = reader["RF_CNY_CODE"].ToString();
                    chkdetails.RF_CTY_CODE = reader["RF_CTY_CODE"].ToString();
                    chkdetails.RF_LOC_CODE = reader["RF_LOC_CODE"].ToString();

                    chkdetails.RB_FROM_DATE = Convert.ToDateTime(reader["RB_FROM_DATE"]);
                    chkdetails.RB_TO_DATE = Convert.ToDateTime(reader["RB_TO_DATE"].ToString());
                    chkdetails.RB_FRM_TIME = reader["RB_FRM_TIME"].ToString();
                    chkdetails.RB_TO_TIME = reader["RB_TO_TIME"].ToString();



                    chkdetails.RESERVED_FOR_EMAIL = reader["RESERVED_FOR_EMAIL"].ToString();
                    chkdetails.RESERVED_FOR = reader["RESERVED_FOR"].ToString();
                    chkdetails.RESERVED_BY_EMAIL = reader["RESERVED_BY_EMAIL"].ToString();
                    chkdetails.RESERVED_BY = reader["RESERVED_BY"].ToString();
                    chkdetails.RESERVED_DT = Convert.ToDateTime(reader["RESERVED_DT"]);

                    chkdetails.RB_REFERENCE_ID = reader["RB_REFERENCE_ID"].ToString();

                    chkdetails.CHKSTATUS = reader["CHKSTATUS"].ToString();

                    chkdetailslist.Add(chkdetails);
                }
                reader.Close();
                if (chkdetailslist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = chkdetailslist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object GetDetailsByReqId(string reqid)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_CHKINOUT_DETAILS_BY_REQID");
        sp.Command.AddParameter("@REQID", reqid, DbType.String);
        DataSet ds = sp.GetDataSet();
        return ds;
    }
    public object SubmitCheckinoutData(SaveCheckInOutData ab)
    {
        SqlParameter[] param = new SqlParameter[6];

        param[0] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        param[0].Value = ab.RB_REQ_ID;

        param[1] = new SqlParameter("@DATE", SqlDbType.Date);
        param[1].Value = ab.checkinout;

        param[2] = new SqlParameter("@TIME", SqlDbType.VarChar);
        param[2].Value = ab.Time;

        param[3] = new SqlParameter("@REMARKS", SqlDbType.NVarChar);
        param[3].Value = ab.Remarks;

        param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[4].Value = HttpContext.Current.Session["UID"];

        param[5] = new SqlParameter("@TYPE", SqlDbType.NVarChar);
        param[5].Value = ab.Type;

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_INSERT_CHECK_IN_OUT_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    SendMailCheckInOut(dr["REQID"].ToString(), 1);
                    return new { Message = dr["MSG"] + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    SendMailCheckInOut(dr["REQID"].ToString(), 2);
                    return new { Message = dr["MSG"] + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else
                    return new { Message = dr["REQID"].ToString(), data = (object)null };
            }
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public void SendMailCheckInOut(string REQUEST_ID, int FLAG)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_SEND_MAIL_CHECK_IN_OUT");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }
}