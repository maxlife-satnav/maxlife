﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;


/// <summary>
/// Summary description for PerVSOfficalBookingReportService
/// </summary>
public class PerVSOfficalBookingReportService
{
    SubSonic.StoredProcedure sp;
    PervsoffDetails peroffdt;
    DataSet ds;

    public List<PervsoffDetails> Getdetails(PerVSOfficalBookingVM PVSOB)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(PVSOB.lcmlst);
        param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[1].Value = PVSOB.FromDate;
        param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[2].Value = PVSOB.ToDate;
        param[3] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
        param[3].Value = PVSOB.Status;

        List<PervsoffDetails> peroffdtDt = new List<PervsoffDetails>();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_PERSONAL_VS_OFFICAL_BOOKING_DT", param))
        {
            while (sdr.Read())
            {
                peroffdt = new PervsoffDetails();
                peroffdt.RT_NAME = sdr["RT_NAME"].ToString();
                peroffdt.RF_NAME = sdr["RF_NAME"].ToString();
                peroffdt.RR_NAME = sdr["RR_NAME"].ToString();
                peroffdt.RR_CAPCITY = sdr["RR_CAPCITY"].ToString();
                peroffdt.RF_CTY_CODE = sdr["RF_CTY_CODE"].ToString();
                peroffdt.RF_LOC_CODE = sdr["RF_LOC_CODE"].ToString();
                peroffdt.PERSONAL = sdr["PERSONAL"].ToString();
                peroffdt.OFFICIAL = sdr["OFFICIAL"].ToString();
                peroffdtDt.Add(peroffdt);
            }
        }
        return peroffdtDt;
    }
    public object GetPersonalVSOfficialChart(PerVSOfficalBookingVM Avail)
    {
        try
        {
            List<SpaceAvailData> Spcdata = new List<SpaceAvailData>();
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_PERSONAL_VS_OFFICAL_CHART");
            //sp.Command.AddParameter("@FDATE", Avail.FromDate, DbType.DateTime);
            //sp.Command.AddParameter("@TDATE", Avail.ToDate, DbType.DateTime);
            //sp.Command.AddParameter("@STATUS", Avail.Type, DbType.String);
            //sp.Command.AddParameter("@LCMLST", SqlDbType.Structured);

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Avail.lcmlst);
            param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
            param[1].Value = Avail.FromDate;
            param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
            param[2].Value = Avail.ToDate;
            param[3] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
            param[3].Value = Avail.Status;
            param[4] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["Uid"].ToString();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GH_GET_PERSONAL_VS_OFFICAL_CHART", param);
            //ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}