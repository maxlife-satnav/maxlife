﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class WithholdService
{
    SubSonic.StoredProcedure sp;
    List<WithholdGrid> vadlst;
    WithholdGrid vad;
    DataSet ds;

    public object GetViewAvailableObject(WithholdDetails Det)
    {
        try
        {
            vadlst = GetViewAvailableDetails(Det);
            if (vadlst.Count != 0) { return new { Message = MessagesVM.SER_OK, data = vadlst }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<WithholdGrid> GetViewAvailableDetails(WithholdDetails Details)
    {
        try
        {
            List<WithholdGrid> CData = new List<WithholdGrid>();
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@LOCLIST", SqlDbType.Structured);

            if (Details.loclst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Details.loclst);
            }

           
            param[1] = new SqlParameter("@STAT", SqlDbType.Int);
            param[1].Value = Details.STAT;
            param[2] = new SqlParameter("@RF_LIST", SqlDbType.Structured);

            if (Details.rflist == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(Details.rflist);
            }

            param[3] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[3].Value = HttpContext.Current.Session["UID"];

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_DATA_ADMIN_BOOKING", param))
            {
                while (reader.Read())
                {
                    vad = new WithholdGrid();
                    vad.COUNTRY = Convert.ToString(reader["CNY_NAME"]);
                    vad.CITY = Convert.ToString(reader["CTY_NAME"]);
                    vad.LOCATION = Convert.ToString(reader["LCM_NAME"]);
                    vad.RT_NAME = Convert.ToString(reader["RT_NAME"]);
                    
                    vad.RR_NAME = Convert.ToString(reader["RR_NAME"]);
                    vad.RRDT_FILE_NAME = Convert.ToString(reader["RRDT_FILE_NAME"]);
                    vad.RF_NAME = Convert.ToString(reader["RF_NAME"]);

                    vad.RR_SNO = Convert.ToString(reader["RR_SNO"]);
                    vad.RR_RT_SNO = Convert.ToString(reader["RR_RT_SNO"]);
                    vad.RR_RF_SNO = Convert.ToString(reader["RR_RF_SNO"]);
       

                    CData.Add(vad);
                }
                reader.Close();
            }
            return CData;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   
    public object SaveBookingRequest(WithholdVM wobj)
    {
        SqlParameter[] param = new SqlParameter[15];

        param[0] = new SqlParameter("@WM_TYPE", SqlDbType.Int);
        param[0].Value = wobj.obj.WM_TYPE;

        param[1] = new SqlParameter("@WM_TITLE", SqlDbType.NVarChar);
        param[1].Value = wobj.obj.WM_TITLE;

        param[2] = new SqlParameter("@WM_FROM_DATE", SqlDbType.DateTime);
        param[2].Value = wobj.obj.WM_FROM_DATE;

        param[3] = new SqlParameter("@WM_TO_DATE", SqlDbType.DateTime);
        param[3].Value = wobj.obj.WM_TO_DATE;

        param[4] = new SqlParameter("@WM_FROM_TIME", SqlDbType.VarChar);
        param[4].Value = wobj.obj.WM_FROM_TIME;

        param[5] = new SqlParameter("@WM_TO_TIME", SqlDbType.VarChar);
        param[5].Value = wobj.obj.WM_TO_TIME;

        param[6] = new SqlParameter("@WM_RESERVED_FOR", SqlDbType.NVarChar);
        param[6].Value = wobj.obj.WM_RESERVED_FOR;

        param[7] = new SqlParameter("@WM_REFERENCE_ID", SqlDbType.NVarChar);
        param[7].Value = wobj.obj.WM_REFRERENCE_ID;

        param[8] = new SqlParameter("@WM_STA_ID", SqlDbType.Int);
        param[8].Value = wobj.obj.WM_STA_ID;

        param[9] = new SqlParameter("@WM_REMARKS", SqlDbType.NVarChar);
        param[9].Value = wobj.obj.WM_REMARKS;

        param[10] = new SqlParameter("@WM_CREATEDBY", SqlDbType.NVarChar);
        param[10].Value = HttpContext.Current.Session["UID"];

        param[11] = new SqlParameter("@WD_LIST", SqlDbType.Structured);
        param[11].Value = UtilityService.ConvertToDataTable(wobj.objList);

        param[12] = new SqlParameter("@WM_LCM_CODE", SqlDbType.NVarChar);
        param[12].Value = wobj.obj.WM_LCM_CODE;

        param[13] = new SqlParameter("@WM_CTY_CODE", SqlDbType.NVarChar);
        param[13].Value = wobj.obj.WM_CTY_CODE;

        param[14] = new SqlParameter("@WM_CNY_CODE", SqlDbType.NVarChar);
        param[14].Value = wobj.obj.WM_CNY_CODE;

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_ADMIN_DIRECT_BOOKING_WITHHOLD", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 2)
                {
                    string RetMessage = string.Empty;

                    if (wobj.obj.WM_STA_ID == 5)
                        wobj.obj.WM_STA_ID = 4;
                    RequestState sta = (RequestState)wobj.obj.WM_STA_ID;
                    switch (sta)
                    {
                        case RequestState.Unchanged: RetMessage = MessagesVM.GH_BOOKED;
                        break;
                        case RequestState.Added: RetMessage = MessagesVM.GH_WH_Added;
                        break;
                       
                    }

                    SendMailAdminBooking(dr["REQID"].ToString(), wobj.obj.WM_STA_ID);
                    return new { Message = RetMessage + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                  //  SendMailVerticalAllocation(dr["REQID"].ToString());
                    return new { Message = MessagesVM.GH_FAIL, data = dr["REQID"].ToString() };
                }
                else
                    return new { Message = dr["REQID"].ToString(), data = (object)null };
            }
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        // }
        //else
        //    return new { Message = MessagesVM.BSR_NODET, data = (object)null };
    }

    // get gh bookings by date

    public List<WithholdEvents> GetAllEvents(string ScreenType, string LocationCode, string startdate, string enddate)
    {
        List<WithholdEvents> CData = new List<WithholdEvents>();
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        param[0].Value = startdate;
        param[1] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        param[1].Value = enddate;

        param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["UID"];

        param[3] = new SqlParameter("@ScreenType", SqlDbType.NVarChar);
        param[3].Value = ScreenType;

        param[4] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
        param[4].Value = LocationCode;

        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_CALENDAR_EVENTS_WITHHOLD", param))
        {
            while (reader.Read())
            {
                WithholdEvents e  = new WithholdEvents();
                e.title = Convert.ToString(reader["Title"]);
                e.start = Convert.ToString(reader["StartDateString"]);
                e.end = Convert.ToString(reader["EndDateString"]);
                e.ReferenceID = Convert.ToString(reader["WM_REFERENCE_ID"]);
                e.ReservedFor = Convert.ToString(reader["WM_RESERVED_FOR"]);
                e.Remarks = Convert.ToString(reader["WM_REMARKS"]);
                e.className = "#FF0000";
                e.RoomNames = Convert.ToString(reader["RR_NAME"]);
                e.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                e.RB_TYPE = Convert.ToString(reader["WM_TYPE"]);
                CData.Add(e);
            }
            reader.Close();
        }
        return CData;
    }

    public void SendMailAdminBooking(string REQUEST_ID, int FLAG)
    {
        try
        {

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_SEND_MAIL_WITHHOLD_BOOKING");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }

 }

public class WithholdEvents
{
    public int id;
    public string title;
    public int someKey;
    public string start;
    public string end;
    public string StatusString;
    public string color;
    public string className;
    public bool allDay;
    public string ReferenceID;
    public string ReservedFor;
    public string Remarks;
    public string CNY_NAME;
    public string CTY_NAME;
    public string LCM_NAME;
    public string BOOKED_RR_NAME;
    public string RB_TYPE;
    public string RoomNames;


}



