﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReservationTypeService
/// </summary>
public class ReservationTypeService
{
    SubSonic.StoredProcedure sp;

    public int Save(ReservationTypeVM model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_RESERVATION_TYPE");
            sp.Command.AddParameter("@RT_NAME", model.RT_NAME, DbType.String);
            sp.Command.AddParameter("@RT_STATUS", model.RT_STATUS, DbType.String);
            sp.Command.AddParameter("@RT_REMARKS", model.RT_REMARKS, DbType.String);
            sp.Command.Parameters.Add("@RT_USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }


    public Boolean Update(ReservationTypeVM upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_RESERVATION_TYPE");
            sp.Command.AddParameter("@RT_SNO", upd.RT_SNO, DbType.String);
            sp.Command.AddParameter("@RT_NAME", upd.RT_NAME, DbType.String);
            sp.Command.AddParameter("@RT_STATUS", upd.RT_STATUS, DbType.Int32);
            sp.Command.AddParameter("@RT_REMARKS", upd.RT_REMARKS, DbType.String);
            sp.Command.Parameters.Add("@RT_USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Execute();
            return true;
        }
        catch
        {
            throw;
        }
    }



    public IEnumerable<ReservationTypeVM> GetReservationTypeBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_RESERVATION_TYPE_BINDGRID").GetReader())
            {
                List<ReservationTypeVM> RTList = new List<ReservationTypeVM>();
                while (reader.Read())
                {
                    RTList.Add(new ReservationTypeVM()
                    {
                        RT_SNO = Convert.ToInt32(reader["RT_SNO"]),
                        RT_NAME = reader["RT_NAME"].ToString(),
                        RT_STATUS = reader["RT_STATUS"].ToString(),
                        RT_REMARKS = reader["RT_REMARKS"].ToString(),
                        RT_CREATEDON = reader["RT_CREATEDON"].ToString(),
                        RT_MODIFIEDON = reader["RT_MODIFIEDON"].ToString()
                    });
                }
                reader.Close();
                return RTList;
            }
        }
        catch
        {
            throw;
        }
    }

}