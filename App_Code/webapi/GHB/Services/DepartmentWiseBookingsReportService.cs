﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;


/// <summary>
/// Summary description for DepartmentWiseBookingsReportService
/// </summary>
public class DepartmentWiseBookingsReportService
{
    SubSonic.StoredProcedure sp;
    DeptWiseBookingDetails deptDetails;
    List<DeptWiseBookingDetails> Cust;
    DeptWiseBookingDetails Custm;
    DataSet ds;
    public object GetDepartementWiseBookingsObject(DeptWiseBookingVM Det)
    {
        try
        {
            Cust = Getdetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<DeptWiseBookingDetails> Getdetails(DeptWiseBookingVM deptbk)
    {
        SqlParameter[] param = new SqlParameter[5];
        //param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        //param[0].Value = UtilityService.ConvertToDataTable(deptbk.lcmlst);

        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);

        if (deptbk.lcmlst == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = UtilityService.ConvertToDataTable(deptbk.lcmlst);
        }

        param[1] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
        param[1].Value = deptbk.Status;
        param[2] = new SqlParameter("@MONTH", SqlDbType.NVarChar);
        param[2].Value = deptbk.Month;
        //param[2].Value = 10;
        param[3] = new SqlParameter("@YEAR", SqlDbType.NVarChar);
        param[3].Value = deptbk.Year; 
        param[4] = new SqlParameter("@BOOKING_TYPE", SqlDbType.NVarChar);
        param[4].Value = deptbk.Booking_Type;

        List<DeptWiseBookingDetails> deptDetailsDt = new List<DeptWiseBookingDetails>();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_DEPT_WISE_BOOKINGS_REPORT", param))
        {
            while (sdr.Read())
            {
                deptDetails = new DeptWiseBookingDetails();
                deptDetails.RT_NAME = sdr["RT_NAME"].ToString();
                deptDetails.RF_NAME = sdr["RF_NAME"].ToString();
                deptDetails.RR_NAME = sdr["RR_NAME"].ToString();
                deptDetails.RR_CAPCITY = sdr["RR_CAPCITY"].ToString();
                deptDetails.DEP_NAME = sdr["DEP_NAME"].ToString();
                deptDetails.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                deptDetails.CTY_NAME = sdr["CTY_NAME"].ToString();
                deptDetails.LCM_NAME = sdr["LCM_NAME"].ToString();
                deptDetails.WEEK_1 = sdr["WEEK_1"].ToString();  
                deptDetails.WEEK_2 = sdr["WEEK_2"].ToString();
                deptDetails.WEEK_3 = sdr["WEEK_3"].ToString();
                deptDetails.WEEK_4 = sdr["WEEK_4"].ToString();
                deptDetails.WEEK_5 = sdr["WEEK_5"].ToString();
                deptDetails.WEEK_6 = sdr["WEEK_6"].ToString();
               
                deptDetailsDt.Add(deptDetails);
            }
        }
        return deptDetailsDt;
    }
}