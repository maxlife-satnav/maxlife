﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for UserGuestHouseUltReportService
/// </summary>
public class UserGuestHouseUltReportService
{
    SubSonic.StoredProcedure sp;
    UserGuestHouseDetails UsrHosdt;
    DataSet ds;

    public List<UserGuestHouseDetails> Getdetails(UserGuestHouseUltVM UGHU)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(UGHU.lcmlst);
        param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[1].Value = UGHU.FromDate;
        param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[2].Value = UGHU.ToDate;
        param[3] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
        param[3].Value = UGHU.Status;

        List<UserGuestHouseDetails> UserGustDt = new List<UserGuestHouseDetails>();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_USER_GUEST_HOUSE_UTIL_RPT", param))
        {
            while (sdr.Read())
            {
                UsrHosdt = new UserGuestHouseDetails();
                UsrHosdt.RB_REQ_ID = sdr["RB_REQ_ID"].ToString();
                UsrHosdt.RT_NAME = sdr["RT_NAME"].ToString();
                UsrHosdt.RF_NAME = sdr["RF_NAME"].ToString();
                UsrHosdt.RR_NAME = sdr["RR_NAME"].ToString();
                UsrHosdt.RR_CAPCITY = sdr["RR_CAPCITY"].ToString();
                UsrHosdt.RBS_CTY_CODE = sdr["RBS_CTY_CODE"].ToString();
                UsrHosdt.RBS_LOC_CODE = sdr["RBS_LOC_CODE"].ToString();
                UsrHosdt.RB_FROM_DATE = Convert.ToDateTime(sdr["RB_FROM_DATE"]);
                UsrHosdt.RB_TO_DATE = Convert.ToDateTime(sdr["RB_TO_DATE"].ToString());
                UsrHosdt.DEP_NAME = sdr["DEP_NAME"].ToString();
                UsrHosdt.RB_CREATEDBY = sdr["RB_CREATEDBY"].ToString();
                UsrHosdt.RB_CREATEDON = Convert.ToDateTime(sdr["RB_CREATEDON"]);
                UsrHosdt.RB_RESERVED_FOR = sdr["RB_RESERVED_FOR"].ToString();
                UsrHosdt.RESVERED_FOR_EMAIL = sdr["RESVERED_FOR_EMAIL"].ToString();
                UsrHosdt.RESVERED_BY_EMAIL = sdr["RESVERED_BY_EMAIL"].ToString();
                UsrHosdt.RB_CHK_IN_DATE = Convert.ToDateTime(sdr["RB_CHK_IN_DATE"]);
                UsrHosdt.RB_CHK_OUT_DATE = Convert.ToDateTime(sdr["RB_CHK_OUT_DATE"]);
                UsrHosdt.RB_CHK_IN_TIME = sdr["RB_CHK_IN_TIME"].ToString();
                UsrHosdt.RB_CHK_OUT_TIME = sdr["RB_CHK_OUT_TIME"].ToString();
                UsrHosdt.STA_TITLE = sdr["STA_TITLE"].ToString();
                UsrHosdt.UTILIZATION = sdr["UTILIZATION"].ToString();
                UsrHosdt.RB_REFERENCE_ID = sdr["RB_REFERENCE_ID"].ToString();
                UsrHosdt.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                UserGustDt.Add(UsrHosdt);
            }
        }
        return UserGustDt;
    }
    
    public object GetGHStatusDetails()
    {
        List<GHStatusDT> stalst = new List<GHStatusDT>();
        GHStatusDT sta;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_GH_STATUS_DETAILS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                sta = new GHStatusDT();
                sta.STA_ID = sdr["STA_ID"].ToString();
                sta.STA_TITLE = sdr["STA_TITLE"].ToString();
                sta.STA_STA_ID = Convert.ToInt32(sdr["STA_STA_ID"]);
                stalst.Add(sta);
            }
        }
        if (stalst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = stalst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }


}