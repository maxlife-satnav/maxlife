﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;


/// <summary>
/// Summary description for CheckInAndOutService
/// </summary>
public class ViewAndModifyWithholdService
{
    SubSonic.StoredProcedure sp;
    UserGuestHouseDetails UsrHosdt;
    DataSet ds;

    public object GetBookedRequests(string ScreenType)
    {

        ViewAndModifyWithholdVM vmdetails;
        List<ViewAndModifyWithholdVM> vmdetailslist = new List<ViewAndModifyWithholdVM>();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_BOOKED_REQUEST_VIEW_MODIFY_WITHHOLD");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@ScreenType", ScreenType, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {                
                while (reader.Read())
                {
                    vmdetails = new ViewAndModifyWithholdVM();
                    vmdetails.WM_REQ_ID = reader["WM_REQ_ID"].ToString();
                    vmdetails.WM_FROM_DATE = Convert.ToDateTime(reader["WM_FROM_DATE"]);
                    vmdetails.WM_TO_DATE = Convert.ToDateTime(reader["WM_TO_DATE"].ToString());
                    vmdetails.WM_FRM_TIME = reader["WM_FRM_TIME"].ToString();
                    vmdetails.WM_TO_TIME = reader["WM_TO_TIME"].ToString();
                    vmdetails.WM_REFERENCE_ID = reader["WM_REFERENCE_ID"].ToString();
                    vmdetails.RESERVED_FOR_EMAIL = reader["RESERVED_FOR_EMAIL"].ToString();
                    vmdetails.RESERVED_FOR = reader["RESERVED_FOR"].ToString();
                    vmdetails.RESERVED_BY_EMAIL = reader["RESERVED_BY_EMAIL"].ToString();
                    vmdetails.RESERVED_BY = reader["RESERVED_BY"].ToString();
                    vmdetails.RESERVED_DT = Convert.ToDateTime(reader["RESERVED_DT"]);
                    vmdetails.RR_NAME = reader["RR_NAME"].ToString();
                    vmdetails.LCM_NAME = reader["LCM_NAME"].ToString();
                    vmdetails.WM_LCM_CODE = reader["WM_LCM_CODE"].ToString();
                    vmdetails.RF_NAME = reader["RF_NAME"].ToString();
                    vmdetailslist.Add(vmdetails);
                }
                reader.Close();
                if (vmdetailslist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = vmdetailslist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public List<Events> GetBookedEvent(string RB_REQ_ID, string LocationCode, string screenType, string startdate, string enddate)
    {
        List<Events> CData = new List<Events>(); 
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        param[0].Value = startdate;
        param[1] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        param[1].Value = enddate;
        param[2] = new SqlParameter("@RB_REQ_ID", SqlDbType.NVarChar);
        param[2].Value = RB_REQ_ID;
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        param[4] = new SqlParameter("@ScreenType", SqlDbType.NVarChar);
        param[4].Value = screenType;
        param[5] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
        param[5].Value = LocationCode;
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_CALENDAR_EVENTS", param))
        {
            while (reader.Read())
            {
                Events e = new Events();
                e.title = Convert.ToString(reader["Title"]);
                e.start = Convert.ToString(reader["StartDateString"]);
                e.end = Convert.ToString(reader["EndDateString"]);
                e.ReferenceID = Convert.ToString(reader["WM_REFERENCE_ID"]);
                e.ReservedFor = Convert.ToString(reader["WM_RESERVED_FOR"]);
                e.Remarks = Convert.ToString(reader["WM_REMARKS"]);
                e.AUR_NAME = Convert.ToString(reader["AUR_NAME"]);
                e.RoomNames = Convert.ToString(reader["RR_NAME"]);
                e.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                e.RF_NAME = Convert.ToString(reader["RF_NAME"]);
                e.className = "#FF0000";
                e.RB_TYPE = Convert.ToString(reader["WM_TYPE"]);
                CData.Add(e);
            }
            reader.Close();
        }
        return CData;
    }
    
    public void SendMailAdminBooking(string REQUEST_ID, int FLAG, string screenType)
    {
        try
        {
            
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_SEND_MAIL_ADMIN_BOOKING");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@REQID", screenType, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }

    public object GetRequestDetails(string reqID)
    {

        ModifyWithholdVM vmdetails;
        List<ModifyWithholdVM> vmdetailslist = new List<ModifyWithholdVM>();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_REQ_DETAILS_WITHHOLD");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@REQID", reqID, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    vmdetails = new ModifyWithholdVM();
                    vmdetails.RT_NAME = reader["RT_NAME"].ToString();
                    vmdetails.RF_NAME = reader["RF_NAME"].ToString();
                    vmdetails.RR_NAME = reader["RR_NAME"].ToString();

                    vmdetails.COUNTRY = reader["CNY_NAME"].ToString();
                    vmdetails.CITY = reader["CTY_NAME"].ToString();
                    vmdetails.LOCATION = reader["LCM_NAME"].ToString();

                    vmdetails.RF_CNY_CODE = reader["RF_CNY_CODE"].ToString();
                    vmdetails.RF_CTY_CODE = reader["RF_CTY_CODE"].ToString();
                    vmdetails.RF_LOC_CODE = reader["RF_LOC_CODE"].ToString();

                    vmdetails.ticked = Convert.ToBoolean(reader["ticked"]);

                    vmdetails.RR_RT_SNO = reader["RT_SNO"].ToString();
                    vmdetails.RR_RF_SNO = reader["RF_SNO"].ToString();
                    vmdetails.RR_SNO = reader["RR_SNO"].ToString();

                    vmdetails.WD_REQ_ID = reader["RB_REQ_ID_CC"].ToString();

                    vmdetailslist.Add(vmdetails);
                }
                reader.Close();
                if (vmdetailslist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = vmdetailslist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object UpdateBookedRequest(WithholdVM wobj)
    {
        SqlParameter[] param = new SqlParameter[16];

        param[0] = new SqlParameter("@WM_TYPE", SqlDbType.Int);
        param[0].Value = wobj.obj.WM_TYPE;

        param[1] = new SqlParameter("@WM_TITLE", SqlDbType.NVarChar);
        param[1].Value = wobj.obj.WM_TITLE;

        param[2] = new SqlParameter("@WM_FROM_DATE", SqlDbType.DateTime);
        param[2].Value = wobj.obj.WM_FROM_DATE;

        param[3] = new SqlParameter("@WM_TO_DATE", SqlDbType.DateTime);
        param[3].Value = wobj.obj.WM_TO_DATE;

        param[4] = new SqlParameter("@WM_FROM_TIME", SqlDbType.VarChar);
        param[4].Value = wobj.obj.WM_FROM_TIME;

        param[5] = new SqlParameter("@WM_TO_TIME", SqlDbType.VarChar);
        param[5].Value = wobj.obj.WM_TO_TIME;

        param[6] = new SqlParameter("@WM_RESERVED_FOR", SqlDbType.NVarChar);
        param[6].Value = wobj.obj.WM_RESERVED_FOR;

        param[7] = new SqlParameter("@WM_REFERENCE_ID", SqlDbType.NVarChar);
        param[7].Value = wobj.obj.WM_REFRERENCE_ID;

        param[8] = new SqlParameter("@WM_STA_ID", SqlDbType.Int);
        param[8].Value = wobj.obj.WM_STA_ID;

        param[9] = new SqlParameter("@WM_REMARKS", SqlDbType.NVarChar);
        param[9].Value = wobj.obj.WM_REMARKS;

        param[10] = new SqlParameter("@WM_CREATEDBY", SqlDbType.NVarChar);
        param[10].Value = HttpContext.Current.Session["UID"];

        param[11] = new SqlParameter("@WD_LIST", SqlDbType.Structured);
        param[11].Value = UtilityService.ConvertToDataTable(wobj.objList);

        param[12] = new SqlParameter("@WM_REQ_ID", SqlDbType.NVarChar);
        param[12].Value = wobj.obj.WM_REQ_ID;

        param[13] = new SqlParameter("@WM_LCM_CODE", SqlDbType.NVarChar);
        param[13].Value = wobj.obj.WM_LCM_CODE;

        param[14] = new SqlParameter("@WM_CTY_CODE", SqlDbType.NVarChar);
        param[14].Value = wobj.obj.WM_CTY_CODE;

        param[15] = new SqlParameter("@WM_CNY_CODE", SqlDbType.NVarChar);
        param[15].Value = wobj.obj.WM_CNY_CODE;

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_ADMIN_DIRECT_BOOKING_WITHHOLD_UPDATE", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 2)
                {
                    string RetMessage = string.Empty;

                    if (wobj.obj.WM_STA_ID == 6)
                        wobj.obj.WM_STA_ID = 16;
                    else
                        wobj.obj.WM_STA_ID = 1;
                    RequestState sta = (RequestState)wobj.obj.WM_STA_ID;
                    switch (sta)
                    {
                        case RequestState.Canceled: RetMessage = MessagesVM.GH_WH_Cancelled;
                            break;
                        case RequestState.Modified: RetMessage = MessagesVM.GH_WH_Modified;
                            break;

                    }

                    SendMailAdminBooking(dr["REQID"].ToString(), wobj.obj.WM_STA_ID);
                    return new { Message = RetMessage + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                   return new { Message = MessagesVM.GH_FAIL, data = dr["REQID"].ToString() };
                }
                else
                    return new { Message = dr["REQID"].ToString(), data = (object)null };
            }
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
      }

    public void SendMailAdminBooking(string REQUEST_ID, int FLAG)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_SEND_MAIL_WITHHOLD_BOOKING");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }
}