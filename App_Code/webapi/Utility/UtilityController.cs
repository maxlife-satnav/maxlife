﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;
using System.Web;

public class UtilityController : ApiController
{
    UtilityService uservc = new UtilityService();

    [HttpPost]
    public HttpResponseMessage ValidatePagePath([FromBody] PagePathURLParam path)
    {
        var obj = uservc.PagePathValidator(path);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetSysPreferences()
    {
        var obj = uservc.GetSysPreferences();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetCountries([FromUri] int id)
    {
        var obj = uservc.GetCountries(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetZone([FromUri] int id)
    {
        var obj = uservc.GetZone(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetState([FromUri] int id)
    {
        var obj = uservc.GetState(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetCities([FromUri] int id)
    {
        var obj = uservc.GetCities(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getCitiesByCountry([FromUri] string id)
    {
        var obj = uservc.getCitiesByCountry(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getCitiesByLocations([FromUri] string id)
    {
        var obj = uservc.getCitiesByLocations(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetLocations([FromUri] int id)
    {
        var obj = uservc.GetLocations(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetToLocations([FromUri] int id)
    {
        var obj = uservc.GetToLocations(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetTowers([FromUri] int id)
    {
        var obj = uservc.GetTowers(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetFloors([FromUri] int id)
    {
        var obj = uservc.GetFloors(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetVerticals([FromUri] int id)
    {
        var obj = uservc.GetVerticals(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetVerticalsByFlrid([FromUri] string id)
    {
        var obj = uservc.GetVerticalsByFlrid(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpGet]
    public HttpResponseMessage GetCostCenters([FromUri] int id)
    {
        var obj = uservc.GetCostCenters(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetCostCentersByFlrid([FromUri] string id)
    {
        var obj = uservc.GetCostCentersByFlrid(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCountriesById([FromBody] Countrylst cny)
    {
        var obj = uservc.GetCountriesById(cny);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCitiesBycountry([FromUri] int id, [FromBody] List<Countrylst> cny)
    {
        var obj = uservc.GetCitiesBycountry(cny, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetZoneByCny([FromUri] int id, [FromBody] List<Countrylst> cny)
    {
        var obj = uservc.GetZoneByCny(cny, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetStateByZone([FromUri] int id, [FromBody] List<Zonelst> Zne)
    {
        var obj = uservc.GetStateByZone(Zne, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetCityByState([FromUri] int id, [FromBody] List<Statelst> Zne)
    {
        var obj = uservc.GetCityByState(Zne, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetLocationsByCity([FromUri] int id, [FromBody] List<Citylst> city)
    {
        var obj = uservc.GetLocationsByCity(city, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetLocationsByCostcenters([FromUri] int id, [FromBody] List<Costcenterlst> cost)
    {
        var obj = uservc.GetLocationsByCostcenters(cost, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetTowersByCity([FromBody] List<Citylst> city)
    {
        var obj = uservc.GetTowersByCity(city);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetTowerByLocation([FromUri] int id, [FromBody] List<Locationlst> Location)
    {
        var obj = uservc.GetTowerByLocation(Location, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetFloorByTower([FromUri] int id, [FromBody] List<Towerlst> tower)
    {
        var obj = uservc.GetFloorByTower(tower, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetVerticalByChildEntity([FromUri] int id, [FromBody] List<ChildEntityLst> child)
    {
        var obj = uservc.GetVerticalByChildEntity(child, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage GetCostcenterByVertical([FromUri] int id, [FromBody] List<Verticallst> vertical)
    {
        var obj = uservc.GetCostcenterByVertical(vertical, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCostcenterByVerticalCode([FromUri] int id, [FromBody] Verticallst vertical)
    {
        var obj = uservc.GetCostcenterByVerticalCode(vertical, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetCityByStateCode([FromUri] int id, [FromBody] Statelst state)
    {
        var obj = uservc.GetCityByStateCode(state);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetSpacetypeByFloor([FromUri] int id, [FromBody] List<Floorlst> floors)
    {
        var obj = uservc.GetSpacetypeByFloor(floors, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetParentEntity([FromUri] int id)
    {
        var obj = uservc.GetParentEntity(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetChildEntity([FromUri] int id)
    {
        var obj = uservc.GetChildEntity(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetChildEntityByParent([FromUri] int id, [FromBody] List<ParentEntityLst> pel)
    {
        var obj = uservc.GetChildEntityByParent(pel, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetBussHeirarchy()
    {
        var obj = uservc.GetBussHeirarchy();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetRoleByUserId()
    {
        var obj = uservc.GetRoleByUserId();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetRoles([FromUri] int id)
    {
        var obj = uservc.GetRoles(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // guest house booking

    [HttpGet]
    public HttpResponseMessage GetReservationTypes([FromUri] int id)
    {
        var obj = uservc.GetReservationTypes(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // get employee name and id for autocomplete

    [HttpGet]
    public HttpResponseMessage GetEmployeeNameAndID(string q)
    {
        var obj = uservc.GetEmployeeNameAndID(q);
        
        var response = Request.CreateResponse(HttpStatusCode.OK,obj);
        return response;

    }

 

    //get all users
    [HttpGet]
    public HttpResponseMessage GetAllUsers()
    {
        var user = uservc.GetAllUsers();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }


    // get facility names by types

    [HttpPost]
    public HttpResponseMessage GetFacilityNamesbyType([FromUri] int id, [FromBody] List<ReservationType> rt)
    {
        var obj = uservc.GetFacilityNamesbyType(rt, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetPropTypes()
    {
        var user = uservc.GetPropTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetProperties([FromBody] TenantReportModel objVal)
    {
        var obj = uservc.GetPropertyNames(objVal);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetDesignation()
    {
        var obj = uservc.GetDesignation();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}
