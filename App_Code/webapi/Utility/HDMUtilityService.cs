﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using HDMUtilityVM;
using UtiltiyVM;
/// <summary>
/// Summary description for HDMUtilityService
/// </summary>
public class HDMUtilityService
{
    SubSonic.StoredProcedure sp;

    public object GetMainCategories()
    {
        List<MainCatlst> maincatlst = new List<MainCatlst>();
        MainCatlst main;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORIES");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                main = new MainCatlst();
                main.MNC_CODE = sdr["MNC_CODE"].ToString();
                main.MNC_NAME = sdr["MNC_NAME"].ToString();
                main.ticked = false;
                maincatlst.Add(main);
            }
            sdr.Close();
        }
        if (maincatlst.Count != 0)
            return new { Message = MessagesVM.HDM_UM_OK, data = maincatlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSubCatByMainCategories(List<MainCatlst> main)
    {
        List<SubCatlst> sublst = new List<SubCatlst>();
        SubCatlst sub;

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@HDM_MAIN_CAT", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(main);
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_SUBCAT_BY_MAINCATEGORIES", param))
        {

            while (sdr.Read())
            {
                sub = new SubCatlst();
                sub.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                sub.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                sub.SUBC_MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                sub.ticked = false;
                sublst.Add(sub);
            }
            sdr.Close();
        }
 
        if (sublst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = sublst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetChildCatBySubCategories(List<SubCatlst> sub)
    {
        List<ChildCatlst> subcatlst = new List<ChildCatlst>();
        ChildCatlst child;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@HDM_SUB_CAT", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(sub);
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_CHILD_BY_SUBCATEGORIES", param))
        {
            while (sdr.Read())
            {
                child = new ChildCatlst();
                child.CHC_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                child.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                child.CHC_TYPE_SUBC_CODE = sdr["CHC_TYPE_SUBC_CODE"].ToString();
                child.CHC_TYPE_MNC_CODE = sdr["CHC_TYPE_MNC_CODE"].ToString();
                child.ticked = false;
                subcatlst.Add(child);
            }
            sdr.Close();
        }
        if (subcatlst.Count != 0)
            return new { Message = MessagesVM.HDM_UM_OK, data = subcatlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetStatus()
    {
        List<Status> statuslst = new List<Status>();
        Status status;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Status");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                status = new Status();
                status.STA_ID = Convert.ToInt32(sdr["STA_ID"]);
                status.STA_DESC = sdr["STA_DESC"].ToString();
                status.ticked = false;
                statuslst.Add(status);
            }
            sdr.Close();
        }
        if (statuslst.Count != 0)
            return new { Message = MessagesVM.HDM_UM_OK, data = statuslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetRequestedUsers()
    {
        List<RequestedUsers> reqUserslst = new List<RequestedUsers>();
        RequestedUsers requsr;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQUESTED_USERS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                requsr = new RequestedUsers();
                requsr.AUR_ID = (sdr["AUR_ID"]).ToString();
                requsr.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                requsr.ticked = false;
                reqUserslst.Add(requsr);
            }
            sdr.Close();
        }
        if (reqUserslst.Count != 0)
            return new { Message = MessagesVM.HDM_UM_OK, data = reqUserslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}