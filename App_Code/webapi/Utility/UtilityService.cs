﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for UtilityService
/// </summary>
public class UtilityService
{
    SubSonic.StoredProcedure sp;

    public object PagePathValidator(UtiltiyVM.PagePathURLParam pagepath) 
    {
        int count;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 10);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = pagepath.URL;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
        {
            if (sdr.HasRows) { 
                return new { Message = MessagesVM.UM_OK, data = 0 }; 
            }
            else
            {
                return new { Message = MessagesVM.UM_OK, data = 1 }; 
            }
        }
    }

    public object GetSysPreferences()
    {
        List<SysPreference> SysPrflst = new List<SysPreference>();
        SysPreference SysPrf;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SYSTEM_PREFERENCE_ADM");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SysPrf = new SysPreference();
                SysPrf.SYSP_CODE = sdr["SYSP_CODE"].ToString();
                SysPrf.SYSP_VAL1 = sdr["SYSP_VAL1"].ToString();
                SysPrf.SYSP_VAL2 = sdr["SYSP_VAL2"].ToString();
                SysPrf.SYSP_OPR = sdr["SYSP_OPR"].ToString();
                SysPrf.SYSP_STA_ID = Convert.ToInt32(sdr["SYSP_STA_ID"]);
                SysPrflst.Add(SysPrf);
            }
        }
        if (SysPrflst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SysPrflst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetCountries(int id)
    {
        List<Countrylst> cnylst = new List<Countrylst>();
        Countrylst cny;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Countries_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cny = new Countrylst();
                cny.CNY_CODE = sdr["CNY_CODE"].ToString();
                cny.CNY_NAME = sdr["CNY_NAME"].ToString();
                cny.ticked = false;
                cnylst.Add(cny);
            }
        }
        if (cnylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = cnylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetZone(int id)
    {
        List<Zonelst> Znlst = new List<Zonelst>();
        Zonelst Zn;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ZONE");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Zn = new Zonelst();
                Zn.ZN_CODE = sdr["ZN_CODE"].ToString();
                Zn.ZN_NAME = sdr["ZN_NAME"].ToString();
                Zn.CNY_CODE = sdr["CNY_CODE"].ToString();
                Zn.ticked = false;
                Znlst.Add(Zn);
            }
        }
        if (Znlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Znlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetState(int id)
    {
        List<Statelst> stelst = new List<Statelst>();
        Statelst ste;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_STATE");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                ste = new Statelst();
                ste.STE_CODE = sdr["STE_CODE"].ToString();
                ste.STE_NAME = sdr["STE_NAME"].ToString();
                ste.ZN_CODE = sdr["ZN_CODE"].ToString();
                ste.CNY_CODE = sdr["CNY_CODE"].ToString();
                ste.ticked = false;
                stelst.Add(ste);
            }
        }
        if (stelst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = stelst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetCities(int id)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CITY_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.STE_CODE = sdr["STE_CODE"].ToString();
                cty.ZN_CODE = sdr["ZN_CODE"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }


    public object getCitiesByCountry(string id)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CITY_ADM_BY_COUNTRY");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COUNTRY", id, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.STE_CODE = sdr["STE_CODE"].ToString();
                cty.ZN_CODE = sdr["ZN_CODE"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object getCitiesByLocations(string id)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CITY_ADM_BY_LOCATION");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@LOCATION", id, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.STE_CODE = sdr["STE_CODE"].ToString();
                cty.ZN_CODE = sdr["ZN_CODE"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetLocations(int id)
    {
        List<Locationlst> Loc_lst = new List<Locationlst>();
        Locationlst Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new Locationlst();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.STE_CODE = sdr["STE_CODE"].ToString();
                Loc.ZN_CODE = sdr["ZN_CODE"].ToString();
                Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetToLocations(int id)
    {
        List<Towerlst> Loc_lst = new List<Towerlst>();
        Towerlst Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_TO_LOC");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@OTC2", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new Towerlst();
                Loc.TWR_CODE = sdr["TWR_CODE"].ToString();
                Loc.TWR_NAME = sdr["TWR_NAME"].ToString();
                Loc.TWR_PCN_CODE = sdr["TWR_PCN_CODE"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetTowers(int id)
    {
        List<Towerlst> Tow_lst = new List<Towerlst>();
        Towerlst Tow;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_TOWER_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Tow = new Towerlst();
                Tow.TWR_CODE = sdr["TWR_CODE"].ToString();
                Tow.TWR_NAME = sdr["TWR_NAME"].ToString();
                Tow.LCM_CODE = sdr["LCM_CODE"].ToString();
                Tow.CTY_CODE = sdr["CTY_CODE"].ToString();
                Tow.STE_CODE = sdr["STE_CODE"].ToString();
                Tow.ZN_CODE = sdr["ZN_CODE"].ToString();
                Tow.CNY_CODE = sdr["CNY_CODE"].ToString();
                Tow.ticked = false;
                Tow_lst.Add(Tow);
            }
        }
        if (Tow_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Tow_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetFloors(int id)
    {
        List<Floorlst> Flr_lst = new List<Floorlst>();
        Floorlst Flr;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_FLOOR_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Flr = new Floorlst();
                Flr.FLR_CODE = sdr["FLR_CODE"].ToString();
                Flr.FLR_NAME = sdr["FLR_NAME"].ToString();
                Flr.TWR_CODE = sdr["TWR_CODE"].ToString();
                Flr.LCM_CODE = sdr["LCM_CODE"].ToString();
                Flr.CTY_CODE = sdr["CTY_CODE"].ToString();
                Flr.STE_CODE = sdr["STE_CODE"].ToString();
                Flr.ZN_CODE = sdr["ZN_CODE"].ToString();
                Flr.CNY_CODE = sdr["CNY_CODE"].ToString();
                Flr.ticked = false;
                Flr_lst.Add(Flr);
            }
        }
        if (Flr_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Flr_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetVerticals(int id)
    {
        List<Verticallst> verlst = new List<Verticallst>();
        Verticallst ver;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Vertical_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                ver = new Verticallst();
                ver.VER_CODE = sdr["VER_CODE"].ToString();
                ver.VER_NAME = sdr["VER_NAME"].ToString();
                ver.ticked = false;
                verlst.Add(ver);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetDesignation()
    {
        List<Designation> dsglst = new List<Designation>();
        Designation dsg;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_DESIGNATION");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                dsg = new Designation();
                dsg.DSG_CODE = sdr["DSN_CODE"].ToString();
                dsg.DSG_NAME = sdr["DSN_AMT_TITLE"].ToString();
                dsg.ticked = false;
                dsglst.Add(dsg);
            }
        }
        if (dsglst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = dsglst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetVerticalsByFlrid(string id)
    {
        List<Verticallst> verlst = new List<Verticallst>();
        Verticallst ver;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VERTICAL_BY_FLRID");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FLR_ID", id, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                ver = new Verticallst();
                ver.VER_CODE = sdr["VER_CODE"].ToString();
                ver.VER_NAME = sdr["VER_NAME"].ToString();
                ver.ticked = false;
                verlst.Add(ver);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCostCenters(int id)
    {
        List<Costcenterlst> Cst_lst = new List<Costcenterlst>();
        Costcenterlst CST;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_COSTCENTERS_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CST = new Costcenterlst();
                CST.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                CST.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                CST.Vertical_Code = sdr["Vertical_Code"].ToString();
                CST.ticked = false;
                Cst_lst.Add(CST);
            }
        }
        if (Cst_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Cst_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCostCentersByFlrid(string id)
    {
        List<Costcenterlst> Cst_lst = new List<Costcenterlst>();
        Costcenterlst CST;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_COSTCENTERS_BY_FLRID");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FLR_ID", id, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CST = new Costcenterlst();
                CST.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                CST.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                CST.Vertical_Code = sdr["Vertical_Code"].ToString();
                CST.ticked = false;
                Cst_lst.Add(CST);
            }
        }
        if (Cst_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Cst_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCountriesById(UtiltiyVM.Countrylst cny)
    {
        return new { Message = "", data = (object)null };
    }

    public object GetCitiesBycountry(List<Countrylst> cny, int id)
    {
        List<Citylst> citylst = new List<Citylst>();
        Citylst city;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CNYLST", SqlDbType.Structured);
        if (cny == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(cny);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CITY_BY_CNYLST_ADM", param))
        {
            while (sdr.Read())
            {
                city = new Citylst();
                city.CTY_CODE = sdr["CTY_CODE"].ToString();
                city.CTY_NAME = sdr["CTY_NAME"].ToString();
                city.STE_CODE = sdr["STE_CODE"].ToString();
                city.ZN_CODE = sdr["ZN_CODE"].ToString();
                city.CNY_CODE = sdr["CNY_CODE"].ToString();
                city.ticked = false;
                citylst.Add(city);
            }
        }
        if (citylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = citylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetZoneByCny(List<Countrylst> cny, int id)
    {
        List<Zonelst> znlst = new List<Zonelst>();
        Zonelst zne;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CNYLST", SqlDbType.Structured);
        if (cny == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(cny);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_ZONE_BY_CNYLST", param))
        {
            while (sdr.Read())
            {
                zne = new Zonelst();
                zne.ZN_NAME = sdr["ZN_NAME"].ToString();
                zne.ZN_CODE = sdr["ZN_CODE"].ToString();
                zne.CNY_CODE = sdr["CNY_CODE"].ToString();
                zne.ticked = false;
                znlst.Add(zne);
            }
        }
        if (znlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = znlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetStateByZone(List<Zonelst> zone, int id)
    {
        List<Statelst> stelst = new List<Statelst>();
        Statelst ste;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ZNLST", SqlDbType.Structured);
        if (zone == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(zone);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_STATE_BY_ZNLST", param))
        {
            while (sdr.Read())
            {
                ste = new Statelst();
                ste.STE_NAME = sdr["STE_NAME"].ToString();
                ste.STE_CODE = sdr["STE_CODE"].ToString();
                ste.ZN_CODE = sdr["STE_ZN_ID"].ToString();
                ste.CNY_CODE = sdr["STE_CNY_ID"].ToString();
                ste.ticked = false;
                stelst.Add(ste);
            }
        }
        if (stelst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = stelst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCityByState(List<Statelst> state, int id)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@STELST", SqlDbType.Structured);
        if (state == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(state);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CITY_BY_STATELST", param))
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.STE_CODE = sdr["CTY_STATE_ID"].ToString();
                cty.ZN_CODE = sdr["CTY_ZN_ID"].ToString();
                cty.CNY_CODE = sdr["CTY_CNY_ID"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetCityByStateCode(Statelst State)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@STELST", SqlDbType.NVarChar);
        param[0].Value = State.STE_CODE;
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = 1;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CITY_BY_STATE_CODE", param))
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.STE_CODE = sdr["CTY_STATE_ID"].ToString();
                cty.ZN_CODE = sdr["CTY_ZN_ID"].ToString();
                cty.CNY_CODE = sdr["CTY_CNY_ID"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetLocationsByCity(List<Citylst> city, int id)
    {
        List<Locationlst> loclst = new List<Locationlst>();
        Locationlst loc;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CITYLST", SqlDbType.Structured);
        if (city == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(city);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_LOCATION_BY_CITYLST_ADM", param))
        {
            while (sdr.Read())
            {
                loc = new Locationlst();
                loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                loc.STE_CODE = sdr["STE_CODE"].ToString();
                loc.ZN_CODE = sdr["ZN_CODE"].ToString();
                loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                loc.TWR_PCN_CODE = sdr["TWR_PCN_CODE"].ToString();
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetLocationsByCostcenters(List<Costcenterlst> cost, int id)
    {
        List<Locationlst> loclst = new List<Locationlst>();
        Locationlst loc;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@COST_LIST", SqlDbType.Structured);
        if (cost == null)
        {
            param[1].Value = null;
        }
        else
        {
            param[1].Value = ConvertToDataTable(cost);
        }
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_LOCATIONS_BY_OCCUPIED_COSTCENTERS", param))
        {
            while (sdr.Read())
            {
                loc = new Locationlst();
                loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                loc.STE_CODE = sdr["STE_CODE"].ToString();
                loc.ZN_CODE = sdr["ZN_CODE"].ToString();
                loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                loc.TWR_PCN_CODE = sdr["TWR_PCN_CODE"].ToString();
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object GetTowersByCity(List<Citylst> city)
    {
        List<Towerlst> loclst = new List<Towerlst>();
        Towerlst loc;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CITYLST", SqlDbType.Structured);
        if (city == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(city);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_TWR_BY_CITYLST_ADM", param))
        {
            while (sdr.Read())
            {
                loc = new Towerlst();
                loc.TWR_CODE = sdr["TWR_CODE"].ToString();
                loc.TWR_NAME = sdr["TWR_NAME"].ToString();
                loc.LAT = sdr["LAT"].ToString();
                loc.LONG = sdr["LONG"].ToString();
                loc.TWR_PCN_CODE = sdr["TWR_PCN_CODE"].ToString();
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetTowerByLocation(List<Locationlst> Loc, int id)
    {
        List<Towerlst> towerlst = new List<Towerlst>();
        Towerlst twr;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        if (Loc == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(Loc);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_TOWER_BY_LOCATIONLST_ADM", param))
        {
            while (sdr.Read())
            {
                twr = new Towerlst();
                twr.TWR_CODE = sdr["TWR_CODE"].ToString();
                twr.TWR_NAME = sdr["TWR_NAME"].ToString();
                twr.LCM_CODE = sdr["LCM_CODE"].ToString();
                twr.CTY_CODE = sdr["CTY_CODE"].ToString();
                twr.STE_CODE = sdr["STE_CODE"].ToString();
                twr.ZN_CODE = sdr["ZN_CODE"].ToString();
                twr.CNY_CODE = sdr["CNY_CODE"].ToString();
                twr.ticked = false;
                towerlst.Add(twr);
            }
        }
        if (towerlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = towerlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetFloorByTower(List<Towerlst> tower, int id)
    {
        List<Floorlst> floorlst = new List<Floorlst>();
        Floorlst flr;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@TWRLST", SqlDbType.Structured);

        if (tower == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(tower);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_FLOOR_BY_TOWERLST_ADM", param))
        {
            while (sdr.Read())
            {
                flr = new Floorlst();
                flr.FLR_CODE = sdr["FLR_CODE"].ToString();
                flr.FLR_NAME = sdr["FLR_NAME"].ToString();
                flr.TWR_CODE = sdr["TWR_CODE"].ToString();
                flr.LCM_CODE = sdr["LCM_CODE"].ToString();
                flr.CTY_CODE = sdr["CTY_CODE"].ToString();
                flr.STE_CODE = sdr["STE_CODE"].ToString();
                flr.ZN_CODE = sdr["ZN_CODE"].ToString();
                flr.CNY_CODE = sdr["CNY_CODE"].ToString();
                flr.ticked = false;
                floorlst.Add(flr);
            }
        }
        if (floorlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = floorlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetVerticalByChildEntity(List<ChildEntityLst> child, int id)
    {
        List<Verticallst> verlst = new List<Verticallst>();
        Verticallst vert;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CHILD", SqlDbType.Structured);

        if (child == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(child);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_VERTICAL_BY_CHILD_ENTITY_ADM", param))
        {
            while (sdr.Read())
            {
                vert = new Verticallst();
                vert.VER_CODE = sdr["VER_CODE"].ToString();
                vert.VER_NAME = sdr["VER_NAME"].ToString();
                vert.ticked = false;
                verlst.Add(vert);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCostcenterByVertical(List<Verticallst> Vertcal, int id)
    {
        List<Costcenterlst> costlst = new List<Costcenterlst>();
        Costcenterlst cost;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@VERLST", SqlDbType.Structured);

        if (Vertcal == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(Vertcal);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_COSTCENTER_BY_VERTICAL_ADM", param))
        {
            while (sdr.Read())
            {
                cost = new Costcenterlst();
                cost.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                cost.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                cost.Vertical_Code = sdr["Vertical_Code"].ToString();
                cost.ticked = false;
                costlst.Add(cost);
            }
        }
        if (costlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = costlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCostcenterByVerticalCode(Verticallst Vertcal, int id)
    {
        List<Costcenterlst> costlst = new List<Costcenterlst>();
        Costcenterlst cost;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@VER_CODE", SqlDbType.NVarChar);
        param[0].Value = Vertcal.VER_CODE;
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_COSTCENTER_BY_VERTICALCODE", param))
        {
            while (sdr.Read())
            {
                cost = new Costcenterlst();
                cost.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                cost.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                cost.Vertical_Code = sdr["Vertical_Code"].ToString();
                cost.ticked = false;
                costlst.Add(cost);
            }
        }
        if (costlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = costlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSpacetypeByFloor(List<Floorlst> floors, int mode)
    {
        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetParentEntity(int id)
    {
        List<ParentEntityLst> pelst = new List<ParentEntityLst>();
        ParentEntityLst pe;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_PARENTENTITY_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                pe = new ParentEntityLst();
                pe.PE_CODE = sdr["PE_CODE"].ToString();
                pe.PE_NAME = sdr["PE_NAME"].ToString();
                pe.ticked = false;
                pelst.Add(pe);
            }
        }
        if (pelst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = pelst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetChildEntity(int id)
    {
        List<ChildEntityLst> chlst = new List<ChildEntityLst>();
        ChildEntityLst che;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILDENTITY_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                che = new ChildEntityLst();
                che.CHE_CODE = sdr["CHE_CODE"].ToString();
                che.CHE_NAME = sdr["CHE_NAME"].ToString();
                che.PE_CODE = sdr["CHE_PE_CODE"].ToString();
                che.ticked = false;
                chlst.Add(che);
            }
        }
        if (chlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = chlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetChildEntityByParent(List<ParentEntityLst> pel, int id)
    {
        List<ChildEntityLst> chdlst = new List<ChildEntityLst>();
        ChildEntityLst chd;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@PARENTLST", SqlDbType.Structured);
        if (pel == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(pel);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CHILDENTITY_BY_PARENT_ADM", param))
        {
            while (sdr.Read())
            {
                chd = new ChildEntityLst();
                chd.CHE_CODE = sdr["CHE_CODE"].ToString();
                chd.CHE_NAME = sdr["CHE_NAME"].ToString();
                chd.PE_CODE = sdr["PE_CODE"].ToString();
                chd.ticked = false;
                chdlst.Add(chd);
            }
        }
        if (chdlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = chdlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetBussHeirarchy()
    {
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AMT_BSM_GETALL"))
        {
            if (sdr.Read())
            {
                return new { Message = MessagesVM.UM_OK, data = new { Parent = sdr["AMT_BSM_PARENT"], Child = sdr["AMT_BSM_CHILD"] } };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }
    public object GetRoleByUserId()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_MAP_GETROLENOFORUSER");
        sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            if (sdr.Read())
            {
                return new { Message = MessagesVM.UM_OK, data = new { Rol_id = sdr["NAME"] } };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }
    public object GetRoles(int id)
    {
        List<Rolelst> Rollst = new List<Rolelst>();
        Rolelst role;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GETROLES");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                role = new Rolelst();
                role.ROL_ID = sdr["ROL_ID"].ToString();
                role.ROL_ACRONYM = sdr["ROL_ACRONYM"].ToString();
                role.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                role.isChecked = false;
                Rollst.Add(role);
            }
        }
        if (Rollst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Rollst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }

    public DataTable GetInversedDataTable(DataTable table, string columnX,
                                             params string[] columnsToIgnore)
    {
        //Create a DataTable to Return
        DataTable returnTable = new DataTable();

        if (columnX == "")
            columnX = table.Columns[0].ColumnName;

        //Add a Column at the beginning of the table

        returnTable.Columns.Add(columnX);

        //Read all DISTINCT values from columnX Column in the provided DataTale
        List<string> columnXValues = new List<string>();

        //Creates list of columns to ignore
        List<string> listColumnsToIgnore = new List<string>();
        if (columnsToIgnore.Length > 0)
            listColumnsToIgnore.AddRange(columnsToIgnore);

        if (!listColumnsToIgnore.Contains(columnX))
            listColumnsToIgnore.Add(columnX);

        foreach (DataRow dr in table.Rows)
        {
            string columnXTemp = dr[columnX].ToString();
            //Verify if the value was already listed
            if (!columnXValues.Contains(columnXTemp))
            {
                //if the value id different from others provided, add to the list of 
                //values and creates a new Column with its value.
                columnXValues.Add(columnXTemp);
                returnTable.Columns.Add(columnXTemp);
            }
            else
            {
                //Throw exception for a repeated value
                throw new Exception("The inversion used must have " +
                                    "unique values for column " + columnX);
            }
        }

        //Add a line for each column of the DataTable

        foreach (DataColumn dc in table.Columns)
        {
            if (!columnXValues.Contains(dc.ColumnName) &&
                !listColumnsToIgnore.Contains(dc.ColumnName))
            {
                DataRow dr = returnTable.NewRow();
                dr[0] = dc.ColumnName;
                returnTable.Rows.Add(dr);
            }
        }

        //Complete the datatable with the values
        for (int i = 0; i < returnTable.Rows.Count; i++)
        {
            for (int j = 1; j < returnTable.Columns.Count; j++)
            {
                returnTable.Rows[i][j] =
                  table.Rows[j - 1][returnTable.Rows[i][0].ToString()].ToString();
            }
        }

        return returnTable;
    }

    // guest house booking

    public object GetReservationTypes(int id)
    {
        List<ReservationType> rt_lst = new List<ReservationType>();
        ReservationType RT;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_RESERVATION_TYPES");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                RT = new ReservationType();
                RT.RT_SNO = sdr["RT_SNO"].ToString();
                RT.RT_NAME = sdr["RT_NAME"].ToString();
                RT.ticked = false;
                rt_lst.Add(RT);
            }
        }
        if (rt_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = rt_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetEmployeeNameAndID(string name)
    {
        List<EmployeeNameID> emplst = new List<EmployeeNameID>();
        EmployeeNameID emp;
        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];

        param[1] = new SqlParameter("@NAME", SqlDbType.NVarChar);
        param[1].Value = name;


        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_EMP_NAMES_AUTO_COMPLETE", param))
        {
            while (sdr.Read())
            {
                emp = new EmployeeNameID();
                emp.AUR_ID = sdr["id"].ToString();
                emp.NAME = sdr["name"].ToString();
                emp.ticked = false;
                emplst.Add(emp);
            }
        }
        return new { items = emplst, total_count = emplst.Count, incomplete_results = false };
    }


    public object GetAllUsers()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_USER_REPORTING_TO");
            DataSet ds = sp.GetDataSet();
            return new { Message = MessagesVM.UM_OK, data = ds }; ;
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }


    public object GetFacilityNamesbyType(List<ReservationType> rt, int id)
    {
        List<FacilityNamelst> fclst = new List<FacilityNamelst>();
        FacilityNamelst city;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@RT_LST", SqlDbType.Structured);
        if (rt == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(rt);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_FACILITY_NAMES_BY_TYPE", param))
        {
            while (sdr.Read())
            {
                city = new FacilityNamelst();
                city.RF_NAME = sdr["RF_NAME"].ToString();
                city.RF_SNO = sdr["RF_SNO"].ToString();
                city.RT_SNO = sdr["RT_SNO"].ToString();
                city.ticked = false;
                fclst.Add(city);
            }
        }
        if (fclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = fclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetPropTypes()
    {
        try
        {
            List<GetPropertyType> prplst = new List<GetPropertyType>();
            GetPropertyType PrpType;
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_GET_ACTPROPTYPE"))
            {
                while (sdr.Read())
                {
                    PrpType = new GetPropertyType();
                    PrpType.PN_PROPERTYTYPE = sdr["PN_PROPERTYTYPE"].ToString();
                    PrpType.PN_TYPEID = sdr["PN_TYPEID"].ToString();
                    prplst.Add(PrpType);
                }
            }
            if (prplst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = prplst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetPropertyNames(TenantReportModel obj)
    {
        try
        {
            List<GetPropertyName> fclst = new List<GetPropertyName>();
            GetPropertyName pn;

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            if (obj.selectedLoc == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = ConvertToDataTable(obj.selectedLoc);
            }
            param[1] = new SqlParameter("@PT_LST", SqlDbType.Structured);
            if (obj.selectedPrpType == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = ConvertToDataTable(obj.selectedPrpType);
            }
            //param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            //param[2].Value = HttpContext.Current.Session["UID"];


            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_GET_TENANT_PROPERTIES", param))
            {
                while (sdr.Read())
                {
                    pn = new GetPropertyName();
                    pn.PM_PPT_NAME = sdr["PM_PPT_NAME"].ToString();
                    pn.PM_PPT_CODE = sdr["PM_PPT_CODE"].ToString();
                    pn.PM_PPT_TYPE = sdr["PM_PPT_SNO"].ToString();
                    pn.ticked = false;
                    fclst.Add(pn);
                }
            }
            if (fclst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = fclst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }


    }

}
public class ReportGenerator<T>
{
    public string ReportPath { get; set; }
    public string DataSetName { get; set; }
    public string ReportType { get; set; }
    public string CurrencyParam { get; set; }

    //Report Format//
    public Task GenerateReport(List<T> datasource, string filePath, string Type)
    {
        return Task.Run(() =>
        {
            var viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = ReportPath;

            ReportDataSource rds = new ReportDataSource();
            rds.Name = DataSetName;
            rds.Value = datasource;


            Microsoft.Reporting.WebForms.Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            viewer.LocalReport.DataSources.Add(rds);

            if (!string.IsNullOrEmpty(ReportType))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("ReportType", ReportType));
            if (!string.IsNullOrEmpty(CurrencyParam))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("CurrencyParam", CurrencyParam));

            viewer.LocalReport.Refresh();

            String apptype = GetApplicationName(Type);
            byte[] bytes = viewer.LocalReport.Render(
                apptype, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        });
    }

    public String GetApplicationName(String ext)
    {
        switch (ext)
        {
            case "xls": return "Excel";
            case "doc": return "Word";
            case "pdf": return "PDF";
        }
        return "PDF";
    }

    internal Task GenerateReport(List<ZFMReportMdel> reportdata, string filePath, string type)
    {
        throw new NotImplementedException();
    }
}