﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UtiltiyVM
/// </summary>
namespace UtiltiyVM
{
    public class SysPreference
    {
        public string SYSP_CODE { get; set; }
        public string SYSP_VAL1 { get; set; }
        public string SYSP_VAL2 { get; set; }
        public string SYSP_OPR { get; set; }
        public int SYSP_STA_ID { get; set; }
    }

    public class Aurlst
    {
        public string AUR_ID { get; set; }
        public string AUR_KNOWN_AS { get; set; }
        public string AUR_DES_CODE { get; set; }
        public string AUR_DES_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class Type_Data
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class PagePathURLParam
    {
        public string URL { get; set; }
    }

    public class Countrylst
    {
        public string CNY_CODE { get; set; }
        public string CNY_NAME { get; set; }
        public bool ticked { get; set; }
    }
    public class Zonelst
    {
        public string ZN_CODE { get; set; }
        public string ZN_NAME { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }
    public class Statelst
    {      
        public string STE_CODE { get; set; }
        public string STE_NAME { get; set; }
        public string ZN_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Citylst
    {
        public string CTY_CODE { get; set; }
        public string CTY_NAME { get; set; }
        public string STE_CODE { get; set; }
        public string ZN_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Locationlst
    {
        public string LCM_CODE { get; set; }
        public string LCM_NAME { get; set; }
        public string CTY_CODE { get; set; }
        public string STE_CODE { get; set; }
        public string ZN_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public string TWR_PCN_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Towerlst
    {
        public string TWR_CODE { get; set; }
        public string TWR_NAME { get; set; }
        public string LCM_CODE { get; set; }
        public string CTY_CODE { get; set; }
        public string STE_CODE { get; set; }
        public string ZN_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public string LAT { get; set; }
        public string LONG { get; set; }
        public string TWR_PCN_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Floorlst
    {
        public string FLR_CODE { get; set; }
        public string FLR_NAME { get; set; }
        public string TWR_CODE { get; set; }
        public string LCM_CODE { get; set; }
        public string CTY_CODE { get; set; }
        public string STE_CODE { get; set; }
        public string ZN_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }
    public class Verticallst
    {
        public string VER_CODE { get; set; }
        public string VER_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class Costcenterlst
    {
        public string Cost_Center_Code { get; set; }
        public string Cost_Center_Name { get; set; }
        public string Vertical_Code { get; set; }
        public bool ticked { get; set; }
    }

    public class Shiftlst
    {
        public string SH_CODE { get; set; }
        public string SH_NAME { get; set; }
        public string SH_LOC_ID { get; set; }
        public string SH_SEAT_TYPE { get; set; }
        public string REP_COL { get; set; } 
        public bool ticked { get; set; }
    }
    public class ParentEntityLst
    {
        public string PE_CODE { get; set; }
        public string PE_NAME { get; set; }
        public bool ticked { get; set; }
    }
    public class ChildEntityLst
    {
        public string CHE_CODE { get; set; }
        public string CHE_NAME { get; set; }
        public string PE_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Rolelst
    {
        public string ROL_ID { get; set; }
        public string ROL_ACRONYM { get; set; }
        public string ROL_DESCRIPTION { get; set; }
        public bool isChecked { get; set; }
    }

    [BindableType(IsBindable = false)]
    [Flags]
    public enum RequestState
    {
        Canceled = 1,
        Unchanged = 2,
        Added = 4,
        Deleted = 8,
        Modified = 16,
        Approved = 32,
        Rejected = 64
    }
    //property reports

    public class GetPropertyType
    {
        public string PN_TYPEID { get; set; }
        public string PN_PROPERTYTYPE { get; set; }
    }
    public class GetPropertyName
    {
        public string PM_PPT_TYPE { get; set; }
        public string PM_PPT_CODE { get; set; }
        public string PM_PPT_NAME { get; set; }
        public bool ticked { get; set; }
    }

    // guest house booking

    public class ReservationType
    {
        public string RT_SNO { get; set; }
        public string RT_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class EmployeeNameID
    {
        public string AUR_ID { get; set; }
        public string NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class FacilityNamelst
    {
        public string RF_NAME { get; set; }
        public string RF_SNO { get; set; }
        public string RT_SNO { get; set; }
        public bool ticked { get; set; }
    }

    public class Designation
    {
        public string DSG_CODE { get; set; }
        public string DSG_NAME { get; set; }
        public bool ticked { get; set; }
    }

}