﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HDMUtilityVM;

/// <summary>
/// Summary description for HDMUtilityController
/// </summary>
public class HDMUtilityController : ApiController
{
    HDMUtilityService HUsrvc = new HDMUtilityService();

    [HttpGet]
    public HttpResponseMessage GetMainCategories()
    {
        var obj = HUsrvc.GetMainCategories();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetSubCatbyMainCategories([FromBody] List<MainCatlst> main)
    {
        var obj = HUsrvc.GetSubCatByMainCategories(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetChildCatBySubCategories([FromBody] List<SubCatlst> sub)
    {
        var obj = HUsrvc.GetChildCatBySubCategories(sub);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetStatus()
    {
        var obj = HUsrvc.GetStatus();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetRequestedUsers()
    {
        var obj = HUsrvc.GetRequestedUsers();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}