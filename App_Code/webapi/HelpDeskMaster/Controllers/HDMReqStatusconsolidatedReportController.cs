﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for HDMconsolidatedReportController
/// </summary>
public class HDMReqStatusconsolidatedReportController : ApiController
{
    HDMReqStatusconsolidatedReportService hdmCR = new HDMReqStatusconsolidatedReportService();

    [HttpPost]
    public HttpResponseMessage BindGrid()
    {
        var obj = hdmCR.BindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage HDMconsolidatedChart()
    {
        var obj = hdmCR.HDMconsolidatedChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> Export_HDMconsolidatedRpt([FromBody]HDMconsolidatedParameters exType)
    {

        ReportGenerator<HDMReqStatusconsolidatedReportVM> reportgen = new ReportGenerator<HDMReqStatusconsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/HDMRequestStatusReport.rdlc"),
            DataSetName = "HDMconsolidatedReport",
            ReportType = "Help Desk Request Status Summary Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/HDMRequestStatusReport." + exType.DocType);
        List<HDMReqStatusconsolidatedReportVM> reportdata = hdmCR.GetReportList();
        await reportgen.GenerateReport(reportdata, filePath, exType.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedReport." + exType.DocType;
        return result;

    }
}