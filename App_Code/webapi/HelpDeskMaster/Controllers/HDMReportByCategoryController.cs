﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class HDMReportByCategoryController : ApiController
{
    HDMReportByCategoryService rptbyCatSer = new HDMReportByCategoryService();
    [HttpPost]
    public HttpResponseMessage GetHDMReportByCategoryGrid([FromBody] HDMReportByCatParams hdm)
    {
        var obj = rptbyCatSer.GetReportByCategory(hdm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpPost]
    //public HttpResponseMessage GetHistory([FromBody] HDMReportByCategory hdm)
    ////{
    ////    var obj = rptbyCatSer.GetHistory(hdm);
    ////    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    ////    return response;
    ////}

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetReportByCategory([FromBody]HDMReportByCatParams rptByCat)
    {

        ReportGenerator<HDMReportByCategory> reportgen = new ReportGenerator<HDMReportByCategory>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/ReportByCategory.rdlc"),
            DataSetName = "ReportByCategory",
            ReportType = "Report By Category"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ReportByCategory." + rptByCat.DocType);
        List<HDMReportByCategory> reportdata = rptbyCatSer.GetHDMReportByCategoryData(rptByCat);
        await reportgen.GenerateReport(reportdata, filePath, rptByCat.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ReportByCategory." + rptByCat.DocType;
        return result;

    }

    [HttpPost]
    public HttpResponseMessage GetCategoryWiseChart(HDMReportByCatParams data)
    {
        var obj = rptbyCatSer.GetCategoryWiseChart(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCategoryWisePieChartData(HDMReportByCatParams data)
    {
        var obj = rptbyCatSer.GetCategoryWisePieChart(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
