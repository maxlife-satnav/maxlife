﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class HDMcustomizedReportController : ApiController
{
    HDMcustomizedReportService Csvc = new HDMcustomizedReportService();
    CustomizedReportView report = new CustomizedReportView();

    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(HDMcustomizedDetails Custdata)
    {
         var obj = Csvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}