﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class HolidayController : ApiController
{
    HolidayService service = new HolidayService();

    [HttpPost]
    public HttpResponseMessage Create(HolidayModel subcat)
    {

        if (service.Save(subcat) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcat);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Holiday Already Exists");
        }

    }
    [HttpPost]
    public HttpResponseMessage UpdateHolidays(HolidayModel updt)
    {

        if (service.Update(updt))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, updt);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage GetCities()
    {
        IEnumerable<City> citylist = service.GetActiveCities();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, citylist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<HolidayModel> griddata = service.HolidaysBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }
}
