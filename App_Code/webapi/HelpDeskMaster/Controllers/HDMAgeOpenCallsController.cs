﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for AgeOpenCallsController
/// </summary>
public class HDMAgeOpenCallsController : ApiController
{
    HDMAgeOpenCallsService openclsReport = new HDMAgeOpenCallsService();
    [HttpPost]
    public object GetHDMOpenCallsRpt([FromBody] HDMOpenAgeCallsFiltersVM VMobj)
    {
        var response = openclsReport.GetHDMopenclsRpt(VMobj);
        //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Gen report 13.5 
    [HttpPost]
    public async Task<HttpResponseMessage> GetOpenCallsAgeReportdata([FromBody] HDMOpenAgeCallsFiltersVM opencallsdata)
    {
        ReportGenerator<HDMOpenAgeCallsVM> reportgen = new ReportGenerator<HDMOpenAgeCallsVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/AgofOpenCallsReport.rdlc"),
            DataSetName = "AgofOpenCallsReport",
            ReportType = "Age of OpenCalls Report"
        };
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/AgofOpenCallsReport." + opencallsdata.Type);
        SubSonic.StoredProcedure sp;
        List<HDMOpenAgeCallsVM> VMlist;
        HDMOpenAgeCallsVM rptVMobj;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_AGE_OPENCALLS_RPT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", opencallsdata.FromDate.ToString(), DbType.String);
        sp.Command.AddParameter("@TODATE", opencallsdata.ToDate.ToString(), DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<HDMOpenAgeCallsVM>();
            while (sdr.Read())
            {
                rptVMobj = new HDMOpenAgeCallsVM();
                rptVMobj.REQCOUNT = Convert.ToInt32(sdr["REQCOUNT"]);
                rptVMobj.COL3 = sdr["COL3"].ToString();
                VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }
        await reportgen.GenerateReport(VMlist, filePath, opencallsdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AgofOpenCallsReport." + opencallsdata.Type;
        return result;
    }
}