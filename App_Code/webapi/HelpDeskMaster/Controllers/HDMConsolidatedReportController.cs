﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class HDMConsolidatedReportController : ApiController
{
    HDMConsolidatedReportService consSer = new HDMConsolidatedReportService();

    [HttpPost]
    public HttpResponseMessage GetHDMConsolidateDetails(HDMcustomizedDetails Custdata)
    {
        var obj = consSer.GetHDMConsolidatedData(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetConsolidatedReportDoc([FromBody]HDMcustomizedDetails rptByCat)
    {        
        ReportGenerator<HDMConsolidatedReportVM> reportgen = new ReportGenerator<HDMConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/ConsolidatedReport.rdlc"),
            DataSetName = "ConsolidatedReport_DT",
            ReportType = "Consolidated Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ConsolidatedReport." + rptByCat.Type);
        List<HDMConsolidatedReportVM> reportdata = consSer.GethdmConsReportData(rptByCat);
        await reportgen.GenerateReport(reportdata, filePath, rptByCat.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ConsolidatedReport." + rptByCat.Type;
        return result;
    }
}