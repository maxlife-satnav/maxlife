﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ServiceEscalationMappingController : ApiController
{
    ServiceEscalationMappingService SEMService = new ServiceEscalationMappingService();
    //main cat dropdowm
    [HttpGet]
    public HttpResponseMessage GetMaincategory()
    {
        IEnumerable<MainCategoryModel> maincategorylist = SEMService.GetActiveMainCategories();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }
    //sub dropdown by main
    [HttpGet]
    public HttpResponseMessage GetSubCategoryByMain(String id)
    {
        IEnumerable<SubCategoryModel> subcategorylist = SEMService.GetActiveSubCategoriesByMain(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcategorylist);
        return response;
    }
    //Child dropdown by sub
    [HttpGet]
    public HttpResponseMessage GetChildCategoryBySubCat(String id)
    {
        IEnumerable<ChildCategoryModel> childcategorylist = SEMService.GetActiveChildCategoriesBySub(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, childcategorylist);
        return response;
    }


    //country
    [HttpGet]
    public HttpResponseMessage GetCountries()
    {
        IEnumerable<Country> countrylist = SEMService.GetActiveCountries();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, countrylist);
        return response;
    }
    //city by country
    [HttpGet]
    public HttpResponseMessage GetCityByCountry(String id)
    {
        IEnumerable<City> citylist = SEMService.GetActiveCityByCountry(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, citylist);
        return response;
    }
    //location by city
    [HttpGet]
    public HttpResponseMessage GetLocationByCity(String id)
    {
        IEnumerable<Location> locationlist = SEMService.GetActiveLocationByCity(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, locationlist);
        return response;
    }

    //tower by location
    [HttpGet]
    public HttpResponseMessage GetTowerByLocation(String id)
    {
        IEnumerable<Tower> locationlist = SEMService.GetActiveTowerByLocation(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, locationlist);
        return response;
    }


    //Grid View
    [HttpGet]
    public HttpResponseMessage GetSEMDetails()
    {
        IEnumerable<ServiceEscalationMappingModel> SEMlist = SEMService.GetSEMDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SEMlist);
        return response;
    }
    //to get role user data
    [HttpGet]
    public HttpResponseMessage GetRoleUserDetails()
    {
        IEnumerable<ServiceEscalationMappingDetails> SEMlist = SEMService.GetRoleUserDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SEMlist);
        return response;
    }
   // edit
    [HttpGet]
    public HttpResponseMessage EditSEMDetails(int id)
    {
        IEnumerable<ServiceEscalationMappingDetails> SEMlist = SEMService.EditSEMDetails(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SEMlist);
        return response;
    }

  

    [HttpPost]
    public HttpResponseMessage SaveDetails(ServiceEscalationMappingVM dataobject)
    {
        ServiceEscalationMappingModel MODEL = SEMService.Save(dataobject);      
        if (MODEL.SEM_ID== 0)
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Data Already Exists With Same Details");
        }
        else
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, MODEL);
            return response;
        }
    }

    [HttpPost]
    public HttpResponseMessage UpdateDetails(ServiceEscalationMappingVM dataobject)
    {
       
        if (SEMService.UpdateSEMDetails(dataobject).SEM_ID == 0)
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Data Already Exists With Same Details");
        }
        else
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, dataobject);
            return response;
        }
       
    }

}


