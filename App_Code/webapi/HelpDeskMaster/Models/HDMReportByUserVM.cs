﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMReportByUserVM
/// </summary>
public class HDMReportByUserVM
{
    public string SER_REQ_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string NoOfEscalations { get; set; }
    public string DEFINED_TAT { get; set; }
    public string TAT { get; set; }
    public string SLA { get; set; }
    public string TAT_SLA { get; set; }
    public string DELAYED_TIME { get; set; }
    public string DELAYED_SLA { get; set; }
    public string TOTAL { get; set; }
    public string SER_SUB_CAT_CODE { get; set; }
    public string STATUS { get; set; }
    
}
public class HDMReportByUser
{
    public Nullable<DateTime> FromDate { get; set; }
    public Nullable<DateTime> ToDate { get; set; }
    public string DocType { get; set; }
}
public class HDMReport_Req_History
{
    public string SERH_SER_ID { get; set; }
    public string SERH_STA_TITLE { get; set; }
    public string SERH_COMMENTS { get; set; }
    public Nullable<DateTime> CREATEDON { get; set; }
    public string CREATEDBY { get; set; }
}
public class HDMReport_Bar_Graph
{
    public int REQ_COUNT { get; set; }
    public string LCM_NAME { get; set; }
}
public class LocationwiseCount
{
    public List<HDMReportByUserVM> griddata { get; set; }
    public List<HDMReport_Bar_Graph> graphdata { get; set; }
}