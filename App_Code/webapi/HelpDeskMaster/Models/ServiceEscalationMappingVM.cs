﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ServiceEscalationMappingVM
/// </summary>
public class ServiceEscalationMappingVM
{
  public  ServiceEscalationMappingModel SEM { get; set; }
  public  List<ServiceEscalationMappingDetails> SEMD { get; set; }
}
public class ServiceEscalationMappingModel
{
    public int SEM_ID { get; set; }
    public string SEM_CNY_CODE { get; set; }
    public string SEM_CTY_CODE { get; set; }
    public string SEM_LOC_CODE { get; set; }
    public string SEM_TWR_CODE { get; set; }
    public string SEM_MNC_CODE { get; set; }
    public string SEM_SUBC_CODE { get; set; }
    public string SEM_CHC_CODE { get; set; }
    public int SEM_STA_ID { get; set; }
    public string SEM_MAP_TYPE { get; set; }

    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }

    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }

}

public class ServiceEscalationMappingDetails
{
    public string ROL_ACRONYM { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public int ROL_ID { get; set; }
    public string AD_ID { get; set; }
    public string AUR_EMAIL { get; set; }
    public int ROL_ORDER { get; set; }
}



