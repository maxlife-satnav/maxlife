﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class HDMReportByCategory
{

    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string CHILD_CATEGORY { get; set; }
    public string LOCATION { get; set; }
    public string REQUESTED_BY { get; set; }
    public string REQUESTED_DATE { get; set; }

    public string SER_PROB_DESC { get; set; }
    public string ASSIGNED_TO { get; set; }
    public string REQ_STATUS { get; set; }
    public string TOTAL_TIME { get; set; }
    public string CLOSED_TIME { get; set; }
    public string DEFINED_TAT { get; set; }
    public string SER_REQ_ID { get; set; }
    //public string TAT { get; set; }
    public string DELAYED_TIME { get; set; }
	
}
public class HDMReportByCatParams
{
    public Nullable<DateTime> FromDate { get; set; }
    public Nullable<DateTime> ToDate { get; set; }
    public string DocType { get; set; }
}

//public class HDMReport_Req_History
//{
//    public string SERH_SER_ID { get; set; }
//    public string SERH_STA_TITLE { get; set; }
//    public string SERH_COMMENTS { get; set; }
//    public Nullable<DateTime> CREATEDON { get; set; }
//    public string CREATEDBY { get; set; }
//}
//public class HDMReport_Bar_Graph
//{
//    public int REQ_COUNT { get; set; }
//    public string LCM_NAME { get; set; }
//}
//public class LocationwiseCount
//{
//    public List<HDMReportByUserVM> griddata { get; set; }
//    public List<HDMReport_Bar_Graph> graphdata { get; set; }
//}