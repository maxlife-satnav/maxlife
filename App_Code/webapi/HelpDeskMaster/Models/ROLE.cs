﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ROLE
/// </summary>
public class ROLE
{
    public int ROL_ID { get; set; }
    public string ROL_ACRONYM { get; set; }
    public string ROL_DESCRIPTION { get; set; }
    public string ROL_ACTIVE { get; set; }
    public string ROL_UPDATED_BY { get; set; }
    public Nullable<System.DateTime> ROL_UPDATED_ON { get; set; }
    public Nullable<int> ROL_STA_ID { get; set; }
    public string ROL_MODULE { get; set; }
    public Nullable<int> ROL_IS_SLA { get; set; }
    public string ROL_REMARKS { get; set; }
    public Nullable<int> ROL_ORDER { get; set; }
}