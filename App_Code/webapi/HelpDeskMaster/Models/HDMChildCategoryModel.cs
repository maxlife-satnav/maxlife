﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMChildCategoryModel
/// </summary>
public class HDMChildCategoryModel
{
	
    public int Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public string SubCode {get; set;}
    public string SubName { get; set; }
    public string MainCode { get; set; }
    public string MainName { get; set; }
    public string Status { get; set; }
    public string Remarks { get; set; }
}