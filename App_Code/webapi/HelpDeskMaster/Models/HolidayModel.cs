﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class HolidayModel
{
    public string HOL_REASON { get; set; }
    public Nullable<System.DateTime> HOL_DATE { get; set; }
    public string HOL_CITY_CODE { get; set; }
    public string HOL_CITY_NAME { get; set; }
    public string HOL_STA_ID { get; set; }
    public string HOL_REM { get; set; }
    public Nullable<System.DateTime> HOL_PREVDATE { get; set; }
}