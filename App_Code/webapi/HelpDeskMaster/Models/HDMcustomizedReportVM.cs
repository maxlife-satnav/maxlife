﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMcustomizedReportVM
/// </summary>
public class HDMcustomizedReportVM
{
    public string COL { get; set; }
    public string value { get; set; }
    public bool ticked { get; set; }
}

public class HDMcustomizedData
{
    public string SER_REQ_ID { get; set; }
    public string hdIncharge { get; set; }
    public string L1Manager { get; set; }
    public string L2Manager { get; set; }
    public string L3Manager { get; set; }
    public string MAIN { get; set; }
    public string SUB { get; set; }
    public string CHILD { get; set; }
    public DateTime SER_CAL_LOG_DT { get; set; }
    public string REQUESTEDBY { get; set; }
    public string SER_PROB_DESC { get; set; }
    public string LCM_NAME { get; set; }
    public string SERH_COMMENTS { get; set; }
    public string CLOSEDDATE { get; set; }
    public string STA_TITLE { get; set; }
    public string AUR_TYPE { get; set; }
}

public class HDMcustomizedDetails
{
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
}

public class CustomizedGridCols
{
    public string headerName { get; set; }
    public string field { get; set; }
    public int width { get; set; }
    public string cellClass{ get; set; }
    public bool suppressMenu { get; set; }
}

public class ExportClass
{
    public string title { get; set; }
    public string key { get; set; }
}