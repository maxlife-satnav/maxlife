﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Location
/// </summary>
public class Location
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public Boolean ticked { get; set; }
}