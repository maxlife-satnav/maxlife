﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class RepeatCallsModel
{
    public string RPT_Code { get; set; }
    public string RPT_Name { get; set; }
    public string RPT_Status_Id { get; set; }
    public string RPT_REM { get; set; }
    public int RPT_ID { get; set; }
  
}

