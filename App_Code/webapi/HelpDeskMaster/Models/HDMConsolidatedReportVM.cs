﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMConsolidatedReportVM
/// </summary>
public class HDMConsolidatedReportVM
{
    public string SER_REQ_ID { get; set; }
    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string SER_PROB_DESC { get; set; }
    public string REQUESTED_BY { get; set; }
    public string SER_CREATED_DT { get; set; }
    public string SER_UPDATED_DT { get; set; }
    public string ASSIGNED_DATE { get; set; }
    public string SERH_ASSIGN_TO { get; set; }
    public string STA_TITLE { get; set; }
    public string LABOUR_COST { get; set; }
    public string SPARE_COST { get; set; }
    public string ADDITIONAL_COST { get; set; }
    public string CLOSED_TIME { get; set; }
    public string DEFINED_TAT { get; set; }   
    public string Response_TAT { get; set; }   
    public string DELAYED_TAT { get; set; }
    public string FEEBACK { get; set; }

}