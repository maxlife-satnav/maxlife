﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class FeedBackModel
{
    public string FBD_CODE { get; set; }
    public string FBD_NAME { get; set; }
    public string FBD_STA_ID { get; set; }
    public string FBD_REM { get; set; }
}