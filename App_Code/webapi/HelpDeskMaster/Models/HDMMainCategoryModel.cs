﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class HDMMainCategoryModel
{
    public string MNC_Code { get; set; }
    public string MNC_Name { get; set; }
    public string MNC_Status_Id { get; set; }
    public string MNC_REM { get; set; }
    public string MNC_CREATED_DT { get; set; }
    public string MNC_UPDATED_DT { get; set; }

}