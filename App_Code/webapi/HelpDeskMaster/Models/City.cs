﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for City
/// </summary>
public class City
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public Boolean ticked { get; set; }
}