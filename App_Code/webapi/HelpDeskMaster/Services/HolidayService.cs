﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class HolidayService
{
    SubSonic.StoredProcedure sp;

    public int Save(HolidayModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_HOLIDAY");
            sp.Command.AddParameter("@HOL_REASON", model.HOL_REASON, DbType.String);
            sp.Command.AddParameter("@HOL_DATE", model.HOL_DATE, DbType.DateTime);
            sp.Command.AddParameter("@HOL_CITY_CODE", model.HOL_CITY_CODE, DbType.String);
            sp.Command.AddParameter("@HOL_STA_ID", model.HOL_STA_ID, DbType.String);
            sp.Command.AddParameter("@HOL_REM", model.HOL_REM, DbType.String);
            sp.Command.Parameters.Add("@HOL_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch {
            throw;
        }
    }

    public Boolean Update(HolidayModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_HOLIDAYS");
            sp.Command.AddParameter("@HOL_REASON", upd.HOL_REASON, DbType.String);
            sp.Command.AddParameter("@HOL_DATE", upd.HOL_DATE, DbType.DateTime);
            sp.Command.AddParameter("@HOL_CITY_CODE", upd.HOL_CITY_CODE, DbType.String);
            sp.Command.AddParameter("@HOL_STA_ID", upd.HOL_STA_ID, DbType.String);
            sp.Command.AddParameter("@HOL_REM", upd.HOL_REM, DbType.String);
            sp.Command.Parameters.Add("@HOL_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@HOL_PREVDT", upd.HOL_PREVDATE, DbType.DateTime);
            sp.Execute();
            return true;
        }
        catch {
            throw;
        }
    }

    public IEnumerable<City> GetActiveCities()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_HOLIDAY_CITY").GetReader())
            {
                List<City> Citylist = new List<City>();
                while (reader.Read())
                {
                    Citylist.Add(new City()
                    {
                        CTY_CODE = reader.GetValue(0).ToString(),
                        CTY_NAME = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
                return Citylist;
            }
        }
        catch {
            throw;
        }
    }


    public IEnumerable<HolidayModel> HolidaysBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_HOLIDAYS_BINDGRID").GetReader())
            {
                List<HolidayModel> hldlist = new List<HolidayModel>();
                while (reader.Read())
                {
                    hldlist.Add(new HolidayModel()
                    {

                        HOL_REASON = reader.GetValue(0).ToString(),
                        HOL_DATE = (DateTime)reader.GetValue(1),
                        HOL_CITY_CODE = reader.GetValue(2).ToString(),
                        HOL_CITY_NAME = reader.GetValue(3).ToString(),
                        HOL_STA_ID = reader.GetValue(4).ToString(),
                        HOL_REM = reader.GetValue(5).ToString()
                    });
                }
                reader.Close();
                return hldlist;
            }
        }
        catch {
            throw;
        }
    }
}