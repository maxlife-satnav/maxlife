﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
public class UrgencyService
{
    SubSonic.StoredProcedure sp;
    //Saving Data//
    public int Save(UrgencyModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_SAVE_URGENCY_DATA");
            sp.Command.AddParameter("@U_CODE", model.Code, DbType.String);
            sp.Command.AddParameter("@U_NAME", model.Name, DbType.String);
            sp.Command.AddParameter("@U_ID", model.Status, DbType.String);
            sp.Command.AddParameter("@U_REMARKS", model.Remarks, DbType.String);
            sp.Command.AddParameter("@U_CRTBY", HttpContext.Current.Session["UID"], DbType.String);
        }
        catch {
            throw;
        }
        int flag = (int)sp.ExecuteScalar();
        return flag; 
    }
    //Updating Data//
    public Boolean Update(UrgencyModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_URGENCY_MASTER");
            sp.Command.AddParameter("@U_ID", model.Code, DbType.String);
            sp.Command.AddParameter("@U_NAME", model.Name, DbType.String);
            sp.Command.AddParameter("@U_STA_ID", model.Status, DbType.String);
            sp.Command.AddParameter("@U_REMARKS", model.Remarks, DbType.String);
            sp.Command.AddParameter("@U_UPDBY", HttpContext.Current.Session["UID"], DbType.String);
        }
        catch {
            throw;
        }
        sp.Execute();
        return true;
    }

    //Binding Data to Grid//
    public IEnumerable<UrgencyModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_BIND_GRIDDATA").GetReader())
        {
            try
            {
                List<UrgencyModel> Urgencylist = new List<UrgencyModel>();
                while (reader.Read())
                {
                    Urgencylist.Add(new UrgencyModel()
                    {
                        Code = reader.GetValue(0).ToString(),
                        Name = reader.GetValue(1).ToString(),
                        Status = reader.GetValue(2).ToString(),
                        Remarks = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return Urgencylist;
            }
            catch
            {
                throw;
            }
        }
        }
    }
            