﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class HDMcustomizedReportService
{
    SubSonic.StoredProcedure sp;
    List<HDMcustomizedData> Cust;
    HDMcustomizedData Custm;

    public object GetCustomizedObject(HDMcustomizedDetails Det)
    {
        try
        {
            //col names in ddl
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ESCALTION_ROLES");
            List<HDMcustomizedReportVM> CData = new List<HDMcustomizedReportVM>();
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    HDMcustomizedReportVM cols = new HDMcustomizedReportVM();
                    cols.COL = reader["ROL_DESCRIPTION"].ToString();
                    cols.value = reader["ROL_DESCRIPTION"].ToString();
                    cols.ticked = true;
                    CData.Add(cols);
                }
            }

            CData.Add(new HDMcustomizedReportVM { COL = "status", value = "status", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Main Category", value = "Main Category", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Sub Category", value = "Sub Category", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Child Category", value = "Child Category", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Requisition Id", value = "Requisition Id", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "RequestDescription", value = "Problem Description", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Location", value = "Location", ticked = true });
            //CData.Add(new HDMcustomizedReportVM { COL = "Comments", value = "Comments", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Closed Time", value = "Closed Time", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Call Log Date", value = "Call LogDate", ticked = true });
            CData.Add(new HDMcustomizedReportVM { COL = "Requested By", value = "REQUESTEDBY", ticked = true });


            var lst = GetCustomizedDetails(Det);
            if (lst != null)
            {                
                return new { data = new { coldata = CData, lst }, Message = MessagesVM.SER_OK };
            }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }

        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public object GetCustomizedDetails(HDMcustomizedDetails Details)
    {
        try
        {
            List<HDMcustomizedData> CData = new List<HDMcustomizedData>();

            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_CUSTOMIZED_RPT");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@FROMDATE", Details.FromDate.ToString(), DbType.String);
            sp.Command.AddParameter("@TODATE", Details.ToDate.ToString(), DbType.String);
            DataSet ds = sp.GetDataSet();

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcolscls.width = 100;
                gridcols.Add(gridcolscls);

                exportcls = new ExportClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };            
        }
        catch
        {
            throw;
        }
    }
}