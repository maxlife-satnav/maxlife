﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMconsolidatedReportService
/// </summary>
public class HDMReqStatusconsolidatedReportService
{
    SubSonic.StoredProcedure sp;
    public object BindGrid()
    {
        var hdmconslst = GetReportList();

        if (hdmconslst != null)
            return new { Message = MessagesVM.UM_OK, data = hdmconslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //public object GetReportList()
    public List<HDMReqStatusconsolidatedReportVM> GetReportList()
    {
        List<HDMReqStatusconsolidatedReportVM> hdmconslst = new List<HDMReqStatusconsolidatedReportVM>();
        HDMReqStatusconsolidatedReportVM hdmcons;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_CONSOLIDATED_RPT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                hdmcons = new HDMReqStatusconsolidatedReportVM();
                hdmcons.CNY_NAME = sdr["CNY_NAME"].ToString();
                hdmcons.CTY_NAME = sdr["CTY_NAME"].ToString();
                hdmcons.LCM_NAME = sdr["LCM_NAME"].ToString();
                hdmcons.Total_Requests = Convert.ToInt32(sdr["Total_Requests"]);
                hdmcons.Pending_Requests = Convert.ToInt32(sdr["Pending_Requests"]);
                hdmcons.In_Progress_Requests = Convert.ToInt32(sdr["In_Progress_Requests"]);
                hdmcons.Closed_Requests = Convert.ToInt32(sdr["Closed_Requests"]);
                hdmcons.Canceled_Requests = Convert.ToInt32(sdr["Canceled_Requests"]);
                hdmconslst.Add(hdmcons);
            }
            sdr.Close();
        }
        if (hdmconslst.Count != 0)
        {
            return hdmconslst;
        }
        else
            return null;
    }

    public object HDMconsolidatedChart()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_CONSOLIDATED_CHART_RPT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        DataSet ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

        if (arr.Length != 0)
            return new { Message = MessagesVM.UM_OK, data = arr };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}