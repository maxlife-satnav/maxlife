﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

public class SubCategoryService
{
    SubSonic.StoredProcedure sp;

    public int Save(HDMSubCategoryModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_SAVE_SUB_CATEGORY_DATA");
            sp.Command.AddParameter("@S_CODE", model.SUBC_CODE, DbType.String);
            sp.Command.AddParameter("@S_NAME", model.SUBC_NAME, DbType.String);
            sp.Command.AddParameter("@S_MAINCAT", model.MNC_CODE, DbType.String);
            sp.Command.AddParameter("@S_ID", model.SUBC_STA_ID, DbType.String);
            sp.Command.AddParameter("@S_REMARKS", model.SUBC_REM, DbType.String);
            sp.Command.AddParameter("@S_CRTBY", HttpContext.Current.Session["UID"], DbType.String);
        }
        catch
        {
            throw;
        }
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }
    public Boolean UpdateSubcatData(HDMSubCategoryModel updt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_SUB_CATEGORY_MASTER");
            sp.Command.AddParameter("@S_ID", updt.SUBC_CODE, DbType.String);
            sp.Command.AddParameter("@S_NAME", updt.SUBC_NAME, DbType.String);
            sp.Command.AddParameter("@S_MAINCAT", updt.MNC_CODE, DbType.String);
            sp.Command.AddParameter("@S_STA_ID", updt.SUBC_STA_ID, DbType.String);
            sp.Command.AddParameter("@S_REMARKS", updt.SUBC_REM, DbType.String);
            sp.Command.AddParameter("@S_UPDBY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Execute();
        }
        catch
        {
            throw;
        }
        return true;
    }

    public IEnumerable<MainCategoryModel> GetActiveMainCategories()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORIES").GetReader())
        {
            try
            {
                List<MainCategoryModel> maincategorylist = new List<MainCategoryModel>();
                while (reader.Read())
                {
                    maincategorylist.Add(new MainCategoryModel()
                    {
                        MNC_CODE = reader.GetValue(0).ToString(),
                        MNC_NAME = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
                return maincategorylist;
            }
            catch
            {
                throw;
            }
        }
    }
    public IEnumerable<HDMSubCategoryModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_BIND_SUB_CATEGORY_GRIDDATA").GetReader())
        {
            try
            {
                List<HDMSubCategoryModel> catlist = new List<HDMSubCategoryModel>();
                while (reader.Read())
                {
                    catlist.Add(new HDMSubCategoryModel()
                    {
                        SUBC_CODE = reader.GetValue(0).ToString(),
                        SUBC_NAME = reader.GetValue(1).ToString(),
                        MNC_CODE = reader.GetValue(2).ToString(),
                        MNC_NAME = reader.GetValue(3).ToString(),
                        SUBC_STA_ID = reader.GetValue(4).ToString(),
                        SUBC_REM = reader.GetValue(5).ToString()
                    });
                }
                reader.Close();
                return catlist;
            }
            catch
            {
                throw;
            }
        }
    }
}