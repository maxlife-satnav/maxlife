﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ServiceEscalationMappingService
/// </summary>
public class ServiceEscalationMappingService
{
    SubSonic.StoredProcedure sp;

    //country
    public IEnumerable<Country> GetActiveCountries()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GETCOUNTRY").GetReader())
        {
            List<Country> countrylist = new List<Country>();
            while (reader.Read())
            {
                countrylist.Add(new Country()
                {
                    CNY_CODE = reader["CNY_CODE"].ToString(),
                    CNY_NAME = reader["CNY_NAME"].ToString(),
                });
            }
            reader.Close();
            return countrylist;
        }
    }

    //get city by country
    public IEnumerable<City> GetActiveCityByCountry(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_City_By_Country");
        sp.Command.Parameters.Add("@CTY_CNY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<City> citylist = new List<City>();
            while (reader.Read())
            {
                citylist.Add(new City()
                {
                    CTY_CODE = reader["CTY_CODE"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString()
                });
            }
            reader.Close();
            return citylist;
        }
    }

    //GET location by city
    public IEnumerable<Location> GetActiveLocationByCity(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_location_By_City");
        sp.Command.Parameters.Add("@LCM_CTY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<Location> locationlist = new List<Location>();
            while (reader.Read())
            {
                locationlist.Add(new Location()
                {
                    LCM_CODE = reader["LCM_CODE"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                });
            }
            reader.Close();
            return locationlist;
        }
    }

    //GET tower by loc
    public IEnumerable<Tower> GetActiveTowerByLocation(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Tower_By_location");
        sp.Command.Parameters.Add("@TWR_LOC_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<Tower> locationlist = new List<Tower>();
            while (reader.Read())
            {
                locationlist.Add(new Tower()
                {
                    TWR_CODE = reader["TWR_CODE"].ToString(),
                    TWR_NAME = reader["TWR_NAME"].ToString(),
                });
            }
            reader.Close();
            return locationlist;
        }
    }


    //Main Category
    public IEnumerable<MainCategoryModel> GetActiveMainCategories()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORIES").GetReader())
        {
            List<MainCategoryModel> maincategorylist = new List<MainCategoryModel>();
            while (reader.Read())
            {
                maincategorylist.Add(new MainCategoryModel()
                {
                    MNC_CODE = reader["MNC_CODE"].ToString(),
                    MNC_NAME = reader["MNC_NAME"].ToString(),
                });
            }
            reader.Close();
            return maincategorylist;
        }
    }

    //sub category
    public IEnumerable<SubCategoryModel> GetActiveSubCategoriesByMain(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_SUBCAT_BY_MAINCAT");
        sp.Command.Parameters.Add("@SUBC_MNC_CODE", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<SubCategoryModel> subcategorylist = new List<SubCategoryModel>();
            while (reader.Read())
            {
                subcategorylist.Add(new SubCategoryModel()
                {
                    SUBC_CODE = reader["SUBC_CODE"].ToString(),
                    SUBC_NAME = reader["SUBC_NAME"].ToString(),
                });
            }
            reader.Close();
            return subcategorylist;
        }
    }
    //child category
    public IEnumerable<ChildCategoryModel> GetActiveChildCategoriesBySub(string id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_CHILDCAT_BY_SUBCAT");
        sp.Command.Parameters.Add("@CHC_TYPE_SUBC_CODE", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ChildCategoryModel> childcategorylist = new List<ChildCategoryModel>();
            while (reader.Read())
            {
                childcategorylist.Add(new ChildCategoryModel()
                {
                    CHC_TYPE_CODE = reader["CHC_TYPE_CODE"].ToString(),
                    CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                });
            }
            reader.Close();
            return childcategorylist;
        }
    }

    //Grid View
    public IEnumerable<ServiceEscalationMappingModel> GetSEMDetails()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_SERVICE_ESCALATION_MAPPING_DETAILS").GetReader())
        {
            List<ServiceEscalationMappingModel> Semlist = new List<ServiceEscalationMappingModel>();
            while (reader.Read())
            {
                Semlist.Add(new ServiceEscalationMappingModel()
                {

                    SEM_ID = (int)reader["SEM_ID"],
                    CNY_NAME = reader["CNY_NAME"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    TWR_NAME = reader["TWR_NAME"].ToString(),
                    MNC_NAME = reader["MNC_NAME"].ToString(),
                    SUBC_NAME = reader["SUBC_NAME"].ToString(),
                    CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                    SEM_CNY_CODE = reader["SEM_CNY_CODE"].ToString(),
                    SEM_CTY_CODE = reader["SEM_CTY_CODE"].ToString(),
                    SEM_LOC_CODE = reader["SEM_LOC_CODE"].ToString(),
                    SEM_TWR_CODE = reader["SEM_TWR_CODE"].ToString(),
                    SEM_MNC_CODE = reader["SEM_MNC_CODE"].ToString(),
                    SEM_SUBC_CODE = reader["SEM_SUBC_CODE"].ToString(),
                    SEM_CHC_CODE = reader["SEM_CHC_CODE"].ToString(),
                    SEM_MAP_TYPE = reader["SEM_MAP_TYPE"].ToString(),
                });
            }
            reader.Close();
            return Semlist;
        }
    }
    public IEnumerable<ServiceEscalationMappingDetails> GetRoleUserDetails()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Role_User_Details").GetReader())
        {
            List<ServiceEscalationMappingDetails> Semlist = new List<ServiceEscalationMappingDetails>();
            while (reader.Read())
            {
                Semlist.Add(new ServiceEscalationMappingDetails()
                {
                    ROL_ACRONYM = reader["ROL_ACRONYM"].ToString(),
                    AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                    ROL_ID = (int)reader["ROL_ID"],
                    AD_ID = reader["AUR_ID"].ToString(),
                    AUR_EMAIL = reader["AUR_EMAIL"].ToString(),
                    ROL_ORDER = (int)reader["ROL_ORDER"],

                });
            }
            reader.Close();
            return Semlist;
        }
    }

    // save
    public ServiceEscalationMappingModel Save(ServiceEscalationMappingVM model)
    {
        var SEM = new ServiceEscalationMappingModel();
        SEM.SEM_CNY_CODE = model.SEM.SEM_CNY_CODE;
        SEM.SEM_CTY_CODE = model.SEM.SEM_CTY_CODE;
        SEM.SEM_CHC_CODE = model.SEM.SEM_CHC_CODE;
        SEM.SEM_LOC_CODE = model.SEM.SEM_LOC_CODE;
        SEM.SEM_MNC_CODE = model.SEM.SEM_MNC_CODE;
        SEM.SEM_SUBC_CODE = model.SEM.SEM_SUBC_CODE;
        SEM.SEM_TWR_CODE = model.SEM.SEM_TWR_CODE;

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_SERVICE_ESCALATION_MAPPING");
        sp.Command.AddParameter("@SEM_CNY_CODE", model.SEM.SEM_CNY_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_CTY_CODE", model.SEM.SEM_CTY_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_LOC_CODE", model.SEM.SEM_LOC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_TWR_CODE", model.SEM.SEM_TWR_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_MNC_CODE", model.SEM.SEM_MNC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_SUBC_CODE", model.SEM.SEM_SUBC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_CHC_CODE", model.SEM.SEM_CHC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_USER_ID", HttpContext.Current.Session["UID"], DbType.String);

        int HDIcount = model.SEMD.Where(x => x.ROL_ID == 22).Count();
        //if(model.SEMD.Count==1)
        if (HDIcount == 1)
        {
            sp.Command.AddParameter("@SEM_MAP_TYPE", "SINGLE");
        }
        else
        {
            sp.Command.AddParameter("@SEM_MAP_TYPE", "MULTIPLE");
        }

        sp.Command.AddParameter("@SEM_ACTION_TYPE", "SCREEN");
        Object o = sp.ExecuteScalar();
        int sem_id = (int)o;
        if (sem_id == 0)
        {
            SEM.SEM_ID = sem_id;
        }
        else
        {
            List<ServiceEscalationMappingDetails> Semlist = model.SEMD.ToList();

            foreach (var lis in Semlist)
            {
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_SERVICE_ESCALATION_MAPPING_DETAILS");
                sp.Command.AddParameter("@SEMD_SEM_ID", sem_id, DbType.Int32);
                sp.Command.AddParameter("@SEMD_ROL_ID", lis.ROL_ID, DbType.String);
                sp.Command.AddParameter("@SEMD_EMP_ID", lis.AD_ID, DbType.String);
                sp.Command.AddParameter("@SEMD_EMAIL_ID", lis.AUR_EMAIL, DbType.String);
                sp.Command.AddParameter("@SEMD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SEMD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SEMD_ACTION_TYPE", "SCREEN");
                sp.Execute();
            }
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_SERVICE_ESCALATION_MAPPING_RECORD");
            sp.Command.Parameters.Add("@SEM_ID", sem_id);
            using (IDataReader reader = sp.GetReader())
            {

                while (reader.Read())
                {
                    SEM.CNY_NAME = reader["CNY_NAME"].ToString();
                    SEM.CTY_NAME = reader["CTY_NAME"].ToString();
                    SEM.LCM_NAME = reader["LCM_NAME"].ToString();
                    SEM.TWR_NAME = reader["TWR_NAME"].ToString();
                    SEM.MNC_NAME = reader["MNC_NAME"].ToString();
                    SEM.SUBC_NAME = reader["SUBC_NAME"].ToString();
                    SEM.CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString();
                    SEM.SEM_ID = sem_id;
                }
                reader.Close();
            }
        }
        //model.SEM = SEM;
        return SEM;
    }

    //edit
    public IEnumerable<ServiceEscalationMappingDetails> EditSEMDetails(int id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Edit_Service_Escalation_Mapping");
        sp.Command.Parameters.Add("@SEMD_SEM_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ServiceEscalationMappingDetails> SERVICElist = new List<ServiceEscalationMappingDetails>();
            while (reader.Read())
            {
                SERVICElist.Add(new ServiceEscalationMappingDetails()
                {
                    ROL_ID = (int)reader["SEMD_ROL_ID"],
                    AD_ID = reader["SEMD_EMP_ID"].ToString(),
                    AUR_EMAIL = reader["SEMD_EMAIL_ID"].ToString(),
                    ROL_ACRONYM = reader["ROL_ACRONYM"].ToString(),
                    AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString()

                });
            }
            reader.Close();
            return SERVICElist;
        }
    }

    public ServiceEscalationMappingModel UpdateSEMDetails(ServiceEscalationMappingVM model)
    {
        var SEM = new ServiceEscalationMappingModel();
        List<ServiceEscalationMappingVM> Semlist = new List<ServiceEscalationMappingVM>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Update_Service_Escalation");
        sp.Command.AddParameter("@SEM_ID", model.SEM.SEM_ID, DbType.String);
        sp.Command.AddParameter("@SEM_CNY_CODE", model.SEM.SEM_CNY_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_CTY_CODE", model.SEM.SEM_CTY_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_LOC_CODE", model.SEM.SEM_LOC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_TWR_CODE", model.SEM.SEM_TWR_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_MNC_CODE", model.SEM.SEM_MNC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_SUBC_CODE", model.SEM.SEM_SUBC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_CHC_CODE", model.SEM.SEM_CHC_CODE, DbType.String);
        sp.Command.AddParameter("@SEM_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);

        int HDIcount = model.SEMD.Where(x => x.ROL_ID == 22).Count();
        //if (model.SEMD.Count == 1)
        if (HDIcount == 1)
        {
            sp.Command.AddParameter("@SEM_MAP_TYPE", "SINGLE");
        }
        else
        {
            sp.Command.AddParameter("@SEM_MAP_TYPE", "MULTIPLE");
        }
        Object o = sp.ExecuteScalar();
        int sem_id = (int)o;
        if (sem_id == 0)
        {
            SEM.SEM_ID = sem_id;
        }
        else
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_DELETE_SERVICE_ESCALATION_MAPPING_DETAILS");
            sp.Command.AddParameter("@SEMD_SEM_ID", model.SEM.SEM_ID, DbType.String);
            sp.Execute();
            List<ServiceEscalationMappingDetails> Semlists = model.SEMD.ToList();

            foreach (var lis in Semlists)
            {
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_SERVICE_ESCALATION_MAPPING_DETAILS");
                sp.Command.AddParameter("@SEMD_SEM_ID", model.SEM.SEM_ID, DbType.Int32);
                sp.Command.AddParameter("@SEMD_ROL_ID", lis.ROL_ID, DbType.String);
                sp.Command.AddParameter("@SEMD_EMP_ID", lis.AD_ID, DbType.String);
                sp.Command.AddParameter("@SEMD_EMAIL_ID", lis.AUR_EMAIL, DbType.String);
                sp.Command.AddParameter("@SEMD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SEMD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SEMD_ACTION_TYPE", "SCREEN");
                sp.Execute();
            }
            SEM.SEM_ID = sem_id;
        }
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_SERVICE_ESCALATION_MAPPING_RECORD");
        sp.Command.Parameters.Add("@SEM_ID", sem_id);
        using (IDataReader reader = sp.GetReader())
        {

            while (reader.Read())
            {
                SEM.CNY_NAME = reader["CNY_NAME"].ToString();
                SEM.CTY_NAME = reader["CTY_NAME"].ToString();
                SEM.LCM_NAME = reader["LCM_NAME"].ToString();
                SEM.TWR_NAME = reader["TWR_NAME"].ToString();
                SEM.MNC_NAME = reader["MNC_NAME"].ToString();
                SEM.SUBC_NAME = reader["SUBC_NAME"].ToString();
                SEM.CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString();
                SEM.SEM_ID = sem_id;
            }
            reader.Close();
        }
        return SEM;
    }

}