﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for HDMConsolidatedReportService
/// </summary>
public class HDMConsolidatedReportService
{
    SubSonic.StoredProcedure sp;
    List<HDMConsolidatedReportVM> consVMList;// Cust;
    HDMConsolidatedReportVM consVM; //Custm;

    public object GetHDMConsolidatedData(HDMcustomizedDetails hdmUsr)
    {
        try
        {
            consVMList = GethdmConsReportData(hdmUsr);

            if (consVMList != null)
                return new { Message = MessagesVM.UM_OK, data = consVMList };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<HDMConsolidatedReportVM> GethdmConsReportData(HDMcustomizedDetails HDM)
    {

        try
        {

            List<HDMConsolidatedReportVM> VMlst = new List<HDMConsolidatedReportVM>();
            HDMConsolidatedReportVM cvm;
            TimeSpan ts = new TimeSpan(23, 59, 0);
            HDM.ToDate = HDM.ToDate + ts;


            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_CONSOLIDATED_REPORT");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@FromDate", HDM.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@ToDate", HDM.ToDate, DbType.DateTime);

            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    cvm = new HDMConsolidatedReportVM();
                    cvm.SER_REQ_ID = Convert.ToString(sdr["SER_REQ_ID"]);
                    cvm.MNC_NAME = Convert.ToString(sdr["MNC_NAME"]);
                    cvm.SUBC_NAME = Convert.ToString(sdr["SUBC_NAME"]);
                    cvm.CHC_TYPE_NAME = Convert.ToString(sdr["CHC_TYPE_NAME"]);
                    cvm.LCM_NAME = Convert.ToString(sdr["LCM_NAME"]);
                    cvm.REQUESTED_BY = Convert.ToString(sdr["REQUESTED_BY"]);
                    cvm.SER_CREATED_DT = Convert.ToString(sdr["SER_CREATED_DT"]);
                    cvm.SER_PROB_DESC = Convert.ToString(sdr["SER_PROB_DESC"]);
                    cvm.SER_UPDATED_DT =Convert.ToString(sdr["SER_UPDATED_DT"]);
                    cvm.ASSIGNED_DATE = Convert.ToString(sdr["ASSIGNED_DATE"]);
                    cvm.SERH_ASSIGN_TO = Convert.ToString(sdr["SERH_ASSIGN_TO"]);
                    cvm.CLOSED_TIME = Convert.ToString(sdr["CLOSED_TIME"]);
                    cvm.DEFINED_TAT = Convert.ToString(sdr["DEFINED_TAT"]);
                    cvm.Response_TAT = Convert.ToString(sdr["RESPONSE_TAT"]);
                    cvm.STA_TITLE = Convert.ToString(sdr["REQ_STATUS"]);
                    cvm.DELAYED_TAT = Convert.ToString(sdr["DELAYED_TAT"]);
                    cvm.LABOUR_COST = Convert.ToString(sdr["SER_LBR_COST"]);
                    cvm.SPARE_COST = Convert.ToString(sdr["SER_SPR_COST"]);
                    cvm.ADDITIONAL_COST = Convert.ToString(sdr["SER_ADDN_COST"]);
                    cvm.FEEBACK = Convert.ToString(sdr["FEEBACK"]);
                    VMlst.Add(cvm);
                }
                sdr.Close();
            }
            if (VMlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = VMlst };
                return VMlst;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }
}