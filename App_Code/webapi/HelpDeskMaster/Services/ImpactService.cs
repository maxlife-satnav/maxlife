﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
public class ImpactService
{
    SubSonic.StoredProcedure sp;
    //Saving Data//
    public int Save(ImpactModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_SAVE_IMPACT_DATA");
            sp.Command.AddParameter("@I_CODE", model.Code, DbType.String);
            sp.Command.AddParameter("@I_NAME", model.Name, DbType.String);
            sp.Command.AddParameter("@I_ID", model.Status, DbType.String);
            sp.Command.AddParameter("@I_REMARKS", model.Remarks, DbType.String);
            sp.Command.AddParameter("@I_CRTBY", HttpContext.Current.Session["UID"], DbType.String);
        }
        catch
        {
            throw;
        }
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }

    //Updating Data//
    public Boolean UpdateImpactData(ImpactModel updt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_IMPACT_MASTER");
            sp.Command.AddParameter("@I_ID", updt.Code, DbType.String);
            sp.Command.AddParameter("@I_NAME", updt.Name, DbType.String);
            sp.Command.AddParameter("@I_STA_ID", updt.Status, DbType.String);
            sp.Command.AddParameter("@I_REMARKS", updt.Remarks, DbType.String);
            sp.Command.AddParameter("@I_UPDBY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Execute();
        }
        catch
        {
            throw;
        }
        return true;
    }
    //Binding Data to Grid//
    public IEnumerable<ImpactModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_BIND_IMPACTGRIDDATA").GetReader())
        {
            try
            {
                List<ImpactModel> impactlist = new List<ImpactModel>();
                while (reader.Read())
                {
                    impactlist.Add(new ImpactModel()
                    {
                        Code = reader.GetValue(0).ToString(),
                        Name = reader.GetValue(1).ToString(),
                        Status = reader.GetValue(2).ToString(),
                        Remarks = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return impactlist;
            }
            catch {
                throw;
            }
        }

    }
}