﻿#region Name Spaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;

#endregion

public class TouchPointAdminPanelAPIController : ApiController
{
    #region Variables

    SubSonic.StoredProcedure sp;
    DataSet ds;

    #endregion

    #region Methods

    public Object GetTouchPoints([FromUri] SortingPagingInfo sortingpaginginfo)
    {
        sp = new SubSonic.StoredProcedure("GET_ALL_TOUCHPOINTS");
        sp.Command.Parameters.Add("@TENANT", HttpContext.Current.Session["TENANT"], DbType.String);
        ds = sp.GetDataSet();
        if (sortingpaginginfo.SortDirection==null )
        {
            sortingpaginginfo = new SortingPagingInfo();
            sortingpaginginfo.SortField = "TPA_MODULE";
            sortingpaginginfo.SortDirection = "asc";
            sortingpaginginfo.PageSize = 10;            
            sortingpaginginfo.CurrentPageIndex = 1;
            sortingpaginginfo.TotalCount = ds.Tables[0].Rows.Count;
        }        
        sortingpaginginfo.PageCount = Convert.ToInt32(Math.Ceiling(((double)ds.Tables[0].Rows.Count / sortingpaginginfo.PageSize)));
        var query = from r in ds.Tables[0].AsEnumerable() select new { TPA_ID = r.Field<Int32>("TPA_ID"), TPA_MODULE = r.Field<String>("TPA_MODULE"), TPA_NAME = r.Field<String>("TPA_NAME"), TPA_TYPE = r.Field<String>("TPA_TYPE"), TCA_STATUS = r.Field<String>("TCA_STATUS") }; //, TCA_EMAIL = r.Field<String>("TCA_EMAIL")
        int skip = (sortingpaginginfo.CurrentPageIndex - 1) * sortingpaginginfo.PageSize;
        var TableData=query.Skip(skip).Take(sortingpaginginfo.PageSize);
        return new { TableData = TableData, SortingPagingInfo = sortingpaginginfo };
    }

    [HttpPost]
    public string InsertTouchPoints([FromBody] TouchPointsVM Obj)
    {
        if (Obj != null)
        {
            string chkID = String.Join(",", Obj.Catarray);
            sp = new SubSonic.StoredProcedure("INSERT_TOUCHPOINTS_CUSTOMER_SELECTED");
            sp.Command.Parameters.Add("@TPA_ID", chkID, DbType.String);
            sp.Command.Parameters.Add("@TENANT", HttpContext.Current.Session["TENANT"], DbType.String);
            sp.ExecuteScalar();
        }
        return null;
    }

    #endregion
}
