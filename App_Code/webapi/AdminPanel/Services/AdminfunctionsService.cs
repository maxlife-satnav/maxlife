﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AdminfunctionsService
/// </summary>
public class AdminfunctionsService
{
    SubSonic.StoredProcedure sp;
    DataSet ds = new DataSet();
    public DataTable GetRoledata()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AMT_GetRoleDtls_SP");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }


    public string InsertUserRole()
    {
        return "Success";

    }
	
}