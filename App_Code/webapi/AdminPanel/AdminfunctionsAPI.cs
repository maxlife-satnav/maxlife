﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class AdminfunctionsAPIController : ApiController
{
    AdminfunctionsService afs = new AdminfunctionsService();
    public DataTable GetRoledata()
    {
        return afs.GetRoledata();
    }

    [HttpPost]
    public string InsertUserRole()
    {
        return afs.InsertUserRole();
    }
}
