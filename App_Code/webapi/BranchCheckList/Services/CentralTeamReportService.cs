﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for CentralTeamReportService
/// </summary>
public class CentralTeamReportService
{

    SubSonic.StoredProcedure sp;
    DataSet ds;
    
    public object CentralTeamReport(CentraTeamRequestModel SelectedDate )
    {
        try
        {
           DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[CENTRAL_TEAM_REPORT]");
            sp.Command.Parameters.Add("@CurrentDate", SelectedDate.FromDate, DbType.DateTime);
            sp.Command.Parameters.Add("@UserID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;
        }
        catch (Exception ex)
        {
            return new { Message = UtiltiyVM.MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

}