﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for CentralTeamService
/// </summary>
public class CentralTeamService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    //get ZFM details
    public object getCentralTeamdetails()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "DCL_CENTRAL_TEAM_GET_DETAILS_WEB");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data1 = ds.Tables[1], data2 = ds.Tables[2] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    



    #region InsertCentralList
    public object InsertCentralList(selectdata objd)
    {
        int sem_id = 0;
        int BCL_ID = 0;

        for (int i = 0; i < objd.Seldata.Count; i++)
        {
            if (objd.Seldata[i] != null)
            {
                if (i == 0)
                {
                    BCL_ID = objd.Seldata[i].BCL_ID;
                }

                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                param[0].Value = objd.Seldata[i].BCL_ID;
                param[1] = new SqlParameter("@CENTRAL_ACTION ", SqlDbType.VarChar);
                param[1].Value = objd.Seldata[i].CENTRAL_ACTION;
                param[2] = new SqlParameter("@CENTRAL_COMMENTS ", SqlDbType.VarChar);
                param[2].Value = objd.Seldata[i].CENTRAL_COMMENTS;
                param[3] = new SqlParameter("@CreatedBy", SqlDbType.VarChar);
                param[3].Value = HttpContext.Current.Session["UID"];
                param[4] = new SqlParameter("@ID", SqlDbType.Int);
                param[4].Value = objd.Seldata[i].ID;
                object a = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DCL_INSERT_CENTRAL_CHECK_DETAILS_WEB", param);
            }

        }

        SqlParameter[] param1 = new SqlParameter[5];
        param1[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param1[0].Value = BCL_ID;
        param1[1] = new SqlParameter("@ReviewingDate", SqlDbType.DateTime);
        param1[1].Value = objd.ReviewingDate;
        param1[2] = new SqlParameter("@CreatedBy", SqlDbType.VarChar);
        param1[2].Value = HttpContext.Current.Session["UID"];
        param1[3] = new SqlParameter("@SaveFlag", SqlDbType.VarChar);
        param1[3].Value = objd.SaveFlag;
        param1[4] = new SqlParameter("@FLAG", SqlDbType.Int);
        param1[4].Value = objd.Flag;
        object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DCL_INSERT_CENTRAL_DETAILS_WEB", param1);

        sem_id = (int)o;
        string msg;
        if (sem_id == 1)
        {
            msg = "Saved as Draft Submitted Succesfully";
        }

        else
            msg = "Submitted Succesfully";
        return new { data = msg, Message = msg };
    }

    #endregion 
}