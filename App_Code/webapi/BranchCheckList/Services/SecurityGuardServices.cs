﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SecurityGuardServices
/// </summary>
public class SecurityGuardServices
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    SecurityGuardModel SGM = new SecurityGuardModel();

    //Get Locartions
    public object getLocations()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "DCLC_GET_LOCATIONS_WEB");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //getAuthority
    public object getAuthority()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "DCLC_GET_AUTHORITY_WEB");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }





    //Get Inspectors
    public object getInspector()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCLC_GET_INSPECTORS_WEB");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    // Get Main Categories
    public object getMainCategories()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCL_MAIN_CATEGORIES_WEB");
        // sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data1 = ds.Tables[1] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };


    }

    //Get Sub Categories
    public object getSubCategoryData(string category)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCL_SUB_CATEGORIES_WEB");
        sp.Command.Parameters.Add("@Cat_code", category, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //Get Request ID
    public object getRequestID()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCL_REQUEST_ID_WEB");
        sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //Get CheckList Details
    public object getCheckListDetails(int BCL_ID)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCL_GET_CHECKLIST_DETAILS_WEB");
        sp.Command.Parameters.Add("@BCL_ID", BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data1 = ds.Tables[1] };
        //return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    #region getNewCheckListID

    public object getNewCheckListID()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "DCL_GET_NEW_CHECKLIST_ID_WEB");
        sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    #endregion 

    #region InsertDailyCheckList


    public object InsertDailyCheckList(selectdata objd)
    {
        int sem_id = 0;
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[0].Value = objd.BCL_ID;
        param[1] = new SqlParameter("@Location", SqlDbType.VarChar);
        param[1].Value = objd.Location;
        param[2] = new SqlParameter("@InspectdBy", SqlDbType.VarChar);
        param[2].Value = objd.InspectdBy;
        param[3] = new SqlParameter("@InspectdDate", SqlDbType.DateTime);
        param[3].Value = objd.InspectdDate;
        param[4] = new SqlParameter("@CreatedBy", SqlDbType.VarChar);
        param[4].Value = HttpContext.Current.Session["UID"];
        param[5] = new SqlParameter("@Company", SqlDbType.Int);
        param[5].Value = 1;
            //HttpContext.Current.Session["COMPANYID"];
        param[6] = new SqlParameter("@SaveFlag", SqlDbType.VarChar);
        param[6].Value = objd.SaveFlag;
        param[7] = new SqlParameter("@AuthorityBy", SqlDbType.VarChar);
        param[7].Value = objd.AuthorityBy;
        param[8] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[8].Value = objd.Flag;

        object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DCL_INSERT_CHECKLIST_WEB", param);
        sem_id = (int)o;
        if (sem_id > 0)
        {
            SqlParameter[] param2 = new SqlParameter[1];
            param2[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param2[0].Value = objd.BCL_ID;
            object ChkDelete = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DCL_CHECK_DETAILS_DELETE_WEB", param2);

            for (int i = 0; i < objd.Seldata.Count; i++)
            {
                if (objd.Seldata[i] != null)
                {
                    SqlParameter[] param1 = new SqlParameter[6];
                    param1[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                    param1[0].Value = objd.BCL_ID;
                    param1[1] = new SqlParameter("@CatCode", SqlDbType.VarChar);
                    param1[1].Value = objd.Seldata[i].CatCode;
                    param1[2] = new SqlParameter("@SubCatCode", SqlDbType.VarChar);
                    param1[2].Value = objd.Seldata[i].SubCatCode;
                    param1[3] = new SqlParameter("@WorkCondition", SqlDbType.VarChar);
                    param1[3].Value = objd.Seldata[i].workingCondition;
                    param1[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                    param1[4].Value = objd.Seldata[i].Comments;
                    param1[5] = new SqlParameter("@Files", SqlDbType.VarChar);
                    param1[5].Value = objd.Seldata[i].File.TrimStart(',');

                    object ChkDetails = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DCL_INSERT_CHECK_DETAILS_WEB", param1);
                }

            }
        }
        string msg;
        if (sem_id == 1)
        {
            msg = "Saved as Draft Submitted Succesfully";
        }

        else
            msg = "Submitted Succesfully";
        return new { data = msg, Message = msg };
    }

    #endregion


    public object UploadFiles(HttpRequest httpRequest)
    {
        try
        {
            var postedFile = httpRequest.Files[0];

            string MainfileName, fileExtension;
            string date = DateTime.Now.ToString("MMddyyyyhhssmm");
            MainfileName = postedFile.FileName;
            fileExtension = System.IO.Path.GetExtension(MainfileName);
            string FileName = Path.GetFileNameWithoutExtension(postedFile.FileName);
            FileName = FileName + date + fileExtension;

            var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(FileName));
            postedFile.SaveAs(filePath);

            if (FileName != "")
                return new { Message = MessagesVM.UM_OK, data = new { MainfileName = MainfileName, FileName = FileName, FilePath = filePath } };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }

    }

}
