﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

public class SecurityGuardController : ApiController
{

    SecurityGuardServices SGS = new SecurityGuardServices();
    //Get  Location
    [HttpGet]
    public Object getLocations()
    {
        return SGS.getLocations();
    }

    //getAuthority
    [HttpGet]
    public Object getAuthority()
    {
        return SGS.getAuthority();
    }



    //Get Inspected By
    [HttpGet]
    public Object getInspector()
    {
        return SGS.getInspector();
    }
    //Get Main Categories
    [HttpGet]
    public Object getMainCategories()
    {
        return SGS.getMainCategories();
    }

    //Get Sub Categories
    [HttpPost]
    public Object getSubCategoryData(string category)
    {
        return SGS.getSubCategoryData(category);
    }

    //Insert CheckList
    [HttpPost]
    public HttpResponseMessage InsertDailyCheckList(selectdata seledata)
    {
        var obj = SGS.InsertDailyCheckList(seledata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Get RequestID
    [HttpGet]
    public Object getRequestID()
    {
        return SGS.getRequestID();
    }

    //Get CheckList
    [HttpGet]
    public Object getCheckListDetails(int BCL_ID)
    {
        return SGS.getCheckListDetails(BCL_ID);
    }

    [HttpGet]
    #region getNewCheckListID

    public Object getNewCheckListID()
    {
        return SGS.getNewCheckListID();
    }
    #endregion


    [HttpPost]
    public HttpResponseMessage UploadFiles()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = SGS.UploadFiles(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}

