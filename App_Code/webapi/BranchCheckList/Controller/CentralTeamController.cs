﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class CentralTeamController : ApiController
{
    CentralTeamService CTS = new CentralTeamService();

    [HttpGet]
    public Object getCentralTeamdetails()
    {
        return CTS.getCentralTeamdetails();
    }

    //InsertCentralList
    [HttpPost]
    public HttpResponseMessage InsertCentralList(selectdata seledata)
    {
        var obj = CTS.InsertCentralList(seledata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
