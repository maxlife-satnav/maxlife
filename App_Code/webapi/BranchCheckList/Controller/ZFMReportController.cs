﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ZFMReportController : ApiController
{
    ZFMReportService service = new ZFMReportService();
    // GET api/<controller>
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }

    ////get grid data based on Dat
    [HttpPost]
    public HttpResponseMessage BindGrid([FromBody] ZfmRequestModel data)
    {
        var obj = service.ZfmReport(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    ////get grid data based on Date 

    //[HttpPost]
    //public HttpResponseMessage GetZFMReport([FromBody] DateTime data)
    //{
    //    var obj = service.ZfmReport(data);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    public HttpResponseMessage BindGrid1([FromBody] DailyRequestModel data)
    {
        var obj = service.DailyReport(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
