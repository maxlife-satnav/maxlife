﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ZonalFacilityManagerController : ApiController
{
    ZonalFacilityManagerServices ZFMS = new ZonalFacilityManagerServices();

    //getZFMdetails
    [HttpGet]
    public Object getZFMdetails()
    {
        return ZFMS.getZFMdetails();
    }

    //Insert CheckList
    [HttpPost]
    public HttpResponseMessage InsertZFMList(selectdata seledata)
    {
        var obj = ZFMS.InsertZFMList(seledata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
