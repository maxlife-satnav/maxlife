﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ZFMReportMdel
/// </summary>
public class ZFMReportMdel
{

    public string SecurityName { get; set; }
    public string LocationName { get; set; }
    public string Login_status { get; set; }
    public string SG_Status { get; set; }
    public string SubCategoery { get; set; }
    public string Comments { get; set; }
    public string CreatedDate { get; set; }
    public string ZFMAction { get; set; }
    public string ZfmComments { get; set; }

}

public class ZfmRequestModel
{
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string UserID { get; set; }
}
public class DailyRequestModel
{
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string UserID { get; set; }
}