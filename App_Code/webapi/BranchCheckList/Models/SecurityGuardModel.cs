﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SecurityGuardModel
/// </summary>
public class SecurityGuardModel
{

}
public class MainCategory
{
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public bool ticked { get; set; }
}
public class MCList
{
    public List<MainCategory> mclst { get; set; }
}

public class selectdata
{

    public int BCL_ID { get; set; }
    public string Location { get; set; }
    public string InspectdBy { get; set; }
    public DateTime InspectdDate { get; set; }
    public string CreatedBy { get; set; }
    public int Company { get; set; }
    public string SaveFlag { get; set; }
    public string AuthorityBy { get; set; }

    public DateTime ReviewingDate { get; set; }

    public int Flag { get; set; }
    public List<SelectedRadio> Seldata { get; set; }
    //public List<FileProperties> filesupload { get; set; }

}

public class SlecetedMainCategories
{
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
}
public class SelectedRadio
{
    public int ID { get; set; }
    public string CatCode { get; set; }
    public string CatName { get; set; }
    public string SubCatCode { get; set; }
    public string SubCatName { get; set; }
    public string workingCondition { get; set; }
    public string Comments { get; set; }
    public string File { get; set; }
    public string ZFM_ACTION { get; set; }
    public string ZFM_COMMENTS { get; set; }
    public string CENTRAL_ACTION { get; set; }
    public string CENTRAL_COMMENTS { get; set; }
    public int BCL_ID { get; set; }
}

public class GetCheckList
{
    public int BCL_ID { get; set; }
    // public string SubcatCode { get; set; }
}

public class SubCategory
{
    public string BCL_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public string BCL_SUB_MC_CODE { get; set; }

}


