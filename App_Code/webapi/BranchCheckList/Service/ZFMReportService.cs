﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for ZFMReportService
/// </summary>
public class ZFMReportService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object ZfmReport(ZfmRequestModel SelectedDate)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ZFMREPORT");
            sp.Command.Parameters.Add("@FromDate", SelectedDate.FromDate, DbType.DateTime);
            sp.Command.Parameters.Add("@UserID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@ToDate", SelectedDate.ToDate, DbType.DateTime);
            ds = sp.GetDataSet();
            return ds.Tables;
        }
        catch(Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public object DailyReport(DailyRequestModel SelectedDate)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DAILYREPORT_NEW");
            sp.Command.Parameters.Add("@FromDate", SelectedDate.FromDate, DbType.DateTime);
            sp.Command.Parameters.Add("@UserID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@ToDate", SelectedDate.ToDate, DbType.DateTime);
            ds = sp.GetDataSet();
            return ds.Tables;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

}