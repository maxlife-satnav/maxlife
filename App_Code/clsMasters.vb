Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Web.SessionState.HttpSessionState
Imports System.IO
Imports System.Security.Cryptography
Imports System.Web.HttpApplicationState
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Globalization
Imports System.Web
Imports System.Web.UI


Public Class clsMasters
    Private Shared dtResult As DataTable



    '<-----******** General ********************
    Private Sub Make_Active_Inactive(ByVal gv As GridView, ByVal strsql As String)
        Dim i As Integer
        gv.Columns(2).Visible = True
        gv.Columns(3).Visible = True
        BindGrid(strsql, gv)
        For i = 0 To gv.Rows.Count - 1
            Dim btn As LinkButton = CType(gv.Rows(i).Cells(1).Controls(0), LinkButton)
            If Trim(Val(gv.Rows(i).Cells(2).Text)) = 1 Then
                btn.Text = "Active"
            ElseIf Trim(Val(gv.Rows(i).Cells(2).Text)) = 2 Then
                btn.Text = "Inactive"
            End If
        Next
        gv.Columns(2).Visible = False
        gv.Columns(3).Visible = False
    End Sub
    Private Sub Make_Active_Inactive(ByVal gv As GridView, ByVal strsql As String, ByVal index As Integer)
        Dim i As Integer
        gv.Columns(index).Visible = True
        gv.Columns(index + 1).Visible = True
        BindGrid(strsql, gv)
        For i = 0 To gv.Rows.Count - 1
            Dim btn As LinkButton = CType(gv.Rows(i).Cells(index - 1).Controls(0), LinkButton)
            If Trim(Val(gv.Rows(i).Cells(index).Text)) = 1 Then
                btn.Text = "Active"
            ElseIf Trim(Val(gv.Rows(i).Cells(index).Text)) = 2 Then
                btn.Text = "Inactive"
            End If
        Next
        gv.Columns(index).Visible = False
        gv.Columns(index + 1).Visible = False
    End Sub
    Private Sub Make_Active_Inactive_location(ByVal gv As GridView, ByVal strsql As String)
        Dim i As Integer
        gv.Columns(8).Visible = True
        gv.Columns(9).Visible = True
        BindGrid(strsql, gv)
        For i = 0 To gv.Rows.Count - 1
            Dim btn As LinkButton = CType(gv.Rows(i).Cells(7).Controls(0), LinkButton)
            If Trim(Val(gv.Rows(i).Cells(8).Text)) = 1 Then
                btn.Text = "Active"
            ElseIf Trim(Val(gv.Rows(i).Cells(8).Text)) = 2 Then
                btn.Text = "Inactive"
            End If
        Next
        gv.Columns(8).Visible = False
        gv.Columns(9).Visible = False
    End Sub

    '******** Country Events *************---->

    Public Sub Country_LoadGrid(ByVal gv As GridView)
        'strSQL ="usp_getCountryDetails"
        strSQL = "usp_getCountryDetails"
        Make_Active_Inactive(gv, strSQL)
    End Sub

    Public Function Country_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer
        gv.Columns(2).Visible = True
        gv.Columns(3).Visible = True
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(2).Text.ToString()

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = selectedRow.Cells(3).Text
        Dim sp2 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp2.Value = btn
        Dim sp3 As New SqlParameter("@vc_Output", SqlDbType.Int, 50)
        sp3.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_ChangeCountryStatus", sp1, sp2, sp3)
        If sp3.Value = 0 Then
            Return 0
        Else
            Return 1
        End If
    End Function

    Public Sub BindCountry(ByVal ddl As DropDownList)
        strSQL = "usp_getActiveCountries"
        BindCombo(strSQL, ddl, "CNY_NAME", "CNY_CODE")
        ddl.SelectedIndex = 0
    End Sub
    Public Sub bindAllProjects(ByVal ddl As DropDownList)
        strSQL = "getAllactiveprojects"
        BindCombo(strSQL, ddl, "name", "code")
        ddl.SelectedIndex = 0
    End Sub
    Public Sub bindAllCostcenters(ByVal ddl As DropDownList)
        strSQL = "getAllActiveCostcenters"
        BindCombo(strSQL, ddl, "name", "code")
        ddl.SelectedIndex = 0
    End Sub

    Dim code, name, remarks, VRM, Vertical, bandcode As String
    Dim Capacity As Integer ' added by praveen
    Dim parent_entity, child_entity As String 'added by prasanna

    Property getname() As String
        Get
            name = name.Replace("'", "")
            Return name
        End Get
        Set(ByVal value As String)
            name = value
        End Set
    End Property

    Property getcode() As String
        Get
            code = code.Replace("'", "")
            Return code
        End Get

        Set(ByVal value As String)
            code = value
        End Set
    End Property
    Property getvertical() As String
        Get
            Vertical = Vertical.Replace("'", "")
            Return Vertical
        End Get

        Set(ByVal value As String)
            Vertical = value
        End Set
    End Property
    Property getvrm() As String
        Get
            VRM = VRM.Replace("'", "")
            Return VRM
        End Get

        Set(ByVal value As String)
            VRM = value
        End Set
    End Property
    Property getparententity() As String
        Get
            parent_entity = parent_entity.Replace("'", "")
            Return parent_entity
        End Get
        Set(ByVal value As String)
            parent_entity = value
        End Set
    End Property
    Property getchildentity() As String
        Get
            child_entity = child_entity.Replace("'", "")
            Return child_entity
        End Get
        Set(ByVal value As String)
            child_entity = value

        End Set
    End Property
    Property getRemarks() As String
        Get
            remarks = remarks.Replace("'", "'")
            Return remarks
        End Get
        Set(ByVal value As String)
            remarks = value
        End Set
    End Property
    Property getBand() As String
        Get
            bandcode = bandcode.Replace("'", "'")
            Return bandcode
        End Get
        Set(ByVal value As String)
            bandcode = value
        End Set
    End Property
    ' added by praveen for conference on15/02/2012
    Property getCapacity() As Integer
        Get
            Return Capacity
        End Get
        Set(value As Integer)
            Capacity = value
        End Set
    End Property

    Public Function InsertCountry(ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp4.Value = userid
        Dim sp5 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp5.Value = "1"
        Dim sp6 As New SqlParameter("@vc_Output", SqlDbType.Int, 50)
        sp6.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertCountry", sp1, sp2, sp3, sp4, sp5, sp6)
        If sp6.Value = 1 Then
            Return 1
            'ElseIf sp6.Value = 2 Then
            '    Return 2
        ElseIf sp6.Value = 0 Then
            Return 0
        End If
    End Function

    Public Function ModifyCountry(ByVal ddl As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        'strSQL = "update " & HttpContext.Current.HttpContext.Current.Session("TENANT") & "."  & "Country set CNY_NAME='" & name & "',CNY_REM='" & remarks.Replace("'", "''") & "',cny_upt_by='" & userid & "',cny_upt_dt='" & getoffsetdatetime(DateTime.Now) & "' where CNY_CODE='" & ddl.SelectedItem.Value & "'"
        'SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = ddl.SelectedValue
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp4.Value = userid
        Dim sp5 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp5.Value = "1"
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updateCountry", sp1, sp2, sp3, sp4, sp5)
        'PopUpMessage("Record has been modified", page)
        Return 1

    End Function

    Public Sub Country_SelectedIndex_Changed(ByVal ddl As DropDownList)
        Try
            'strSQL = "Select DISTINCT CNY_CODE,CNY_NAME,CNY_REM from " & HttpContext.Current.HttpContext.Current.Session("TENANT") & "."  & "Country where CNY_CODE='" & ddl.SelectedItem.Value & "' "
            'ObjDR = DataReader(strSQL)
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddl.SelectedValue
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getCnyDetailsforddl", sp1)
            While ObjDR.Read
                code = ObjDR("CNY_CODE")
                name = ObjDR("CNY_NAME")
                remarks = ObjDR("CNY_REM")
                ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(ObjDR("cny_name")))
            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub

    '******** City Events *************-->
    Public Sub BindCity(ByVal ddl As DropDownList)
        strSQL = "usp_getActiveCities"
        BindCombo(strSQL, ddl, "CTY_NAME", "CTY_CODE")
        ddl.SelectedIndex = 0
    End Sub

    Public Sub City_LoadGrid(ByVal gv As GridView)

        strSQL = "usp_getCities"
        Make_Active_Inactive(gv, strSQL, 5)
    End Sub

    Public Function City_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(5).Text.ToString()
        gv.Columns(5).Visible = True
        gv.Columns(6).Visible = True

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = selectedRow.Cells(6).Text
        Dim sp2 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp2.Value = btn
        Dim sp3 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp3.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_ChangeCityStatus", sp1, sp2, sp3)
        If sp3.Value = 0 Then
            Return 0
        Else
            Return 1
        End If
    End Function

    Public Function InsertCity(ByVal ddlcny As DropDownList, ByVal ddlzn As DropDownList, ByVal ddlste As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlcny.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp5.Value = userid
        Dim sp6 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp6.Value = "1"
        Dim sp7 As New SqlParameter("@VC_ZN_CODE", SqlDbType.NVarChar, 50)
        sp7.Value = ddlzn.SelectedItem.Value
        Dim sp8 As New SqlParameter("@VC_STE_CODE", SqlDbType.NVarChar, 50)
        sp8.Value = ddlste.SelectedItem.Value
        Dim sp9 As New SqlParameter("@vc_Output", SqlDbType.Int, 50)
        sp9.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertCity", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9)
        If sp9.Value = 1 Then
            Return 1
        ElseIf sp9.Value = 2 Then
            Return 2
        ElseIf sp9.Value = 0 Then
            Return 0
        End If
    End Function

    Public Sub City_SelectedIndex_Changed(ByVal ddl As DropDownList, ByVal ddl1 As DropDownList, ByVal ddlState As DropDownList, ByVal ddlZone As DropDownList)
        Try
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddl.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getCtyDetailsforddl", sp1)
            While ObjDR.Read
                code = ObjDR("CTY_CODE")
                name = ObjDR("CTY_NAME")
                ddl1.SelectedIndex = ddl1.Items.IndexOf(ddl1.Items.FindByValue(ObjDR("CTY_CNY_ID")))
                ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(ObjDR("CTY_STATE_ID")))
                ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(ObjDR("CTY_ZN_ID")))

                Try
                    remarks = ObjDR("CTY_REM")
                Catch ex As Exception
                End Try
            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub

    Public Function ModifyCity(ByVal ddlCountry As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlZone As DropDownList, ByVal ddlState As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = ddlCity.SelectedValue
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp4.Value = userid
        Dim sp5 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp5.Value = "1"
        Dim sp6 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp6.Value = ddlCountry.SelectedValue
        Dim sp7 As New SqlParameter("@VC_ZN_CODE", SqlDbType.NVarChar, 50)
        sp7.Value = ddlZone.SelectedValue
        Dim sp8 As New SqlParameter("@VC_STE_CODE", SqlDbType.NVarChar, 50)
        sp8.Value = ddlState.SelectedValue

        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_UpdateCity", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8)

        Return 1

    End Function
    '******** Location Events**********-->
    Public Sub Bindlocation(ByVal ddl As DropDownList)
        'strSQL = "Select distinct rtrim(ltrim(LCM_CODE))as LCM_CODE,LCM_NAME from " & HttpContext.Current.Session("TENANT") & "."  & "Location where LCM_STA_ID=1 and lcm_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city) and lcm_cny_id in (select cny_code from " & HttpContext.Current.Session("TENANT") & "."  & "country) order by LCM_NAME"
        strSQL = "GET_ALL_LOCATIONS"
        BindCombo(strSQL, ddl, "LCM_NAME", "LCM_CODE")
        ddl.SelectedIndex = 0
    End Sub
    Public Sub BindAllocVertical(ByVal ddl As DropDownList)

        strSQL = "usp_getVerticalAllocatedSeats"
        BindCombo(strSQL, ddl, "VER_NAME", "VER_CODE")
        ddl.SelectedIndex = 0
    End Sub

    Public Sub BindVerticalAllocationData(ByVal ddl As DropDownList)

        strSQL = "usp_getVerticalSeats"
        BindCombo(strSQL, ddl, "VER_NAME", "VER_CODE")
        ddl.SelectedIndex = 0
    End Sub
    '******** Location Events**********-->
    Public Sub BindVerticalCity(ByVal ddl As DropDownList)
        'strSQL = "Select distinct rtrim(ltrim(LCM_CODE))as LCM_CODE,LCM_NAME from " & HttpContext.Current.Session("TENANT") & "."  & "Location where LCM_STA_ID=1 and lcm_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city) and lcm_cny_id in (select cny_code from " & HttpContext.Current.Session("TENANT") & "."  & "country) order by LCM_NAME"
        strSQL = "usp_getcity"
        BindCombo(strSQL, ddl, "cty_NAME", "cty_code")
        ddl.SelectedIndex = 0
    End Sub
    Public Function insertlocation(ByVal ddl As DropDownList, ByVal ddl1 As DropDownList, ByVal txt4 As TextBox, ByVal ddlstatus As DropDownList, ByVal ddlState As DropDownList, ByVal ddlZone As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddl.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddl1.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp7.Value = "1"

        'Dim sp10 As New SqlParameter("@vc_fcNum", SqlDbType.Int)
        'sp10.Value = txt3.Text
        Dim spz As New SqlParameter("@vc_tendate", SqlDbType.DateTime)
        spz.Value = txt4.Text
        Dim sp8 As New SqlParameter("@vc_lcmStatus", SqlDbType.NVarChar, 50)
        sp8.Value = "1"
        Dim sp9 As New SqlParameter("@VC_STE_ID", SqlDbType.NVarChar, 50)
        sp9.Value = ddlState.SelectedItem.Value
        Dim sp10 As New SqlParameter("@VC_ZN_ID", SqlDbType.NVarChar, 50)
        sp10.Value = ddlZone.SelectedItem.Value
        Dim sp11 As New SqlParameter("@vc_Output", SqlDbType.Int, 50)
        sp11.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertLocation", sp1, sp2, sp3, sp4, sp5, sp6, sp7, spz, sp8, sp9, sp10, sp11)
        If sp11.Value = 1 Then
            Return 1
        ElseIf sp11.Value = 2 Then
            Return 2
        ElseIf sp11.Value = 0 Then
            Return 0
        End If

    End Function
    Public Function Modifyloc(ByVal ddlloc As DropDownList, ByVal ddlcity As DropDownList, ByVal ddlCty As DropDownList, ByVal txt4 As TextBox, ByVal ddlstatus As DropDownList, ByVal ddlstate As DropDownList, ByVal ddlzone As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = ddlloc.SelectedItem.Value
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlcity.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCty.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp7.Value = "1"
        Dim spz As New SqlParameter("@vc_tendate", SqlDbType.DateTime)
        spz.Value = txt4.Text
        Dim spSp As New SqlParameter("@VC_STE_CODE", SqlDbType.NVarChar, 50)
        spSp.Value = ddlstate.SelectedItem.Value
        Dim spZn As New SqlParameter("@VC_ZN_CODE", SqlDbType.NVarChar, 50)
        spZn.Value = ddlzone.SelectedItem.Value
        Dim sp8 As New SqlParameter("@vc_lcmStatus", SqlDbType.NVarChar, 50)
        sp8.Value = "1"
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updateLocation", sp1, sp2, sp3, sp4, sp5, sp6, sp7, spz, spSp, spZn, sp8)
        'PopUpMessage("Record has been modified", page)
        Return 1
    End Function
    Public Sub Loc_Selectedindex_changed(ByVal ddlLocation As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlCountry As DropDownList, ByVal txt4 As TextBox, ByVal ddlstatus As DropDownList, ByVal ddlstate As DropDownList, ByVal ddlZone As DropDownList)
        Try
            'strSQL = "select LCM_CODE,LCM_NAME,LCM_CTY_ID,LCM_CNY_ID,LCM_REM,LCM_WSTNUM,LCM_HCNUM,LCM_FCNUM,lcm_tendate,LCM_STATUS from " & HttpContext.Current.Session("TENANT") & "."  & "Location where LCM_CODE='" & ddlLocation.SelectedItem.Value & "'"
            'ObjDR = DataReader(strSQL)
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddlLocation.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getLocationDetailforddl", sp1)
            While ObjDR.Read
                code = ObjDR("LCM_CODE")
                name = ObjDR("LCM_NAME")
                remarks = ObjDR("LCM_REM")
                ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(ObjDR("LCM_CTY_ID")))
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(ObjDR("LCM_CNY_ID")))
                ddlstate.SelectedIndex = ddlstate.Items.IndexOf(ddlstate.Items.FindByValue(ObjDR("LCM_STATE_ID")))
                ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(ObjDR("LCM_ZN_ID")))
                txt4.Text = ObjDR("LCM_TENDATE")
                ddlstatus.SelectedIndex = ddlstatus.Items.IndexOf(ddlstatus.Items.FindByValue(ObjDR("LCM_STATUS")))
            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub

    Public Sub Location_LoadGrid(ByVal gv As GridView)

        strSQL = "usp_getLocationDetails"
        Make_Active_Inactive_location(gv, strSQL)
    End Sub
    Public Function Location_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(8).Text.ToString()
        gv.Columns(8).Visible = True
        gv.Columns(9).Visible = True

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = selectedRow.Cells(9).Text
        Dim sp2 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp2.Value = btn
        Dim sp3 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp3.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_ChangeLocationStatus", sp1, sp2, sp3)
        If sp3.Value = 0 Then
            Return 0
        Else
            Return 1
        End If

    End Function

    '*********************** TOWER EVENTS***********************************-->

    Public Sub BindTower(ByVal ddl As DropDownList)

        strSQL = "usp_getActiveTower"
        BindCombo(strSQL, ddl, "twr_name", "twr_code")
        ddl.SelectedIndex = 0
    End Sub
    Public Sub BindTowerLoc(ByVal ddl As DropDownList, ByVal strLoc As String)
        Dim sp1 As SqlParameter = New SqlParameter("@vc_LOC", SqlDbType.VarChar, 50)
        sp1.Value = strLoc
        strSQL = "usp_getActiveTower_LOC"
        BindCombo(strSQL, ddl, "twr_name", "twr_code", sp1)
        ' ddl.SelectedIndex = 0
    End Sub

    Public Sub BindCostVertical(ByVal ddl As DropDownList, ByVal strVert As String)
        Dim sp1 As SqlParameter = New SqlParameter("@VC_VERT", SqlDbType.VarChar, 50)
        sp1.Value = strVert
        strSQL = "USP_GETACTIVE_COSTCENTER_VERTICAL"
        BindCombo(strSQL, ddl, "COST_CENTER_NAME", "COST_CENTER_CODE", sp1)
        ddl.SelectedIndex = 0
    End Sub


    Public Sub BindProjectCost(ByVal ddl As DropDownList, ByVal strVert As String, ByVal strCost As String)
        strSQL = "USP_GETACTIVE_PROJECT_COSTCENTER '" & strVert & "','" & strCost & "'"
        BindCombo(strSQL, ddl, "PRJ_NAME", "PRJ_CODE")
        ddl.SelectedIndex = 0
    End Sub
    Public Sub BindAllocTower(ByVal ddl As DropDownList, ByVal strLoc As String, ByVal strVert As String)

        Dim sp1 As SqlParameter = New SqlParameter("@VC_VERT", SqlDbType.VarChar, 50)
        Dim sp2 As SqlParameter = New SqlParameter("@VC_COST", SqlDbType.VarChar, 50)
        sp1.Value = strLoc
        sp2.Value = strVert
        strSQL = "USP_GET_TOWER"
        BindCombo(strSQL, ddl, "twr_name", "twr_code", sp1, sp2)
        ddl.SelectedIndex = 0
    End Sub
    Public Sub BindAllocFloor(ByVal ddl As DropDownList, ByVal strTwr As String, ByVal strLoc As String, ByVal strVert As String)

        Dim sp1 As SqlParameter = New SqlParameter("@VC_Tower", SqlDbType.NVarChar, 50)
        Dim sp2 As SqlParameter = New SqlParameter("@VC_Loc", SqlDbType.NVarChar, 50)
        Dim sp3 As SqlParameter = New SqlParameter("@VC_Vert", SqlDbType.NVarChar, 50)
        sp1.Value = strTwr
        sp2.Value = strLoc
        sp3.Value = strVert
        strSQL = "USP_GET_AllocFloor"
        BindCombo(strSQL, ddl, "FLR_name", "FLR_code", sp1, sp2, sp3)
        ddl.SelectedIndex = 0
    End Sub

    Public Sub BindAllocWing(ByVal ddl As DropDownList, ByVal strTwr As String, ByVal strLoc As String, ByVal strVert As String, ByVal strFloor As String)

        Dim sp1 As SqlParameter = New SqlParameter("@VC_Tower", SqlDbType.NVarChar, 50)
        Dim sp2 As SqlParameter = New SqlParameter("@VC_Loc", SqlDbType.NVarChar, 50)
        Dim sp3 As SqlParameter = New SqlParameter("@VC_Vert", SqlDbType.NVarChar, 50)
        Dim sp4 As SqlParameter = New SqlParameter("@VC_floor", SqlDbType.NVarChar, 50)
        sp1.Value = strTwr
        sp2.Value = strLoc
        sp3.Value = strVert
        sp4.Value = strFloor
        strSQL = "USP_GET_AllocWing"
        BindCombo(strSQL, ddl, "WNG_NAME", "WNG_CODE", sp1, sp2, sp3, sp4)
        ddl.SelectedIndex = 0
    End Sub
    Public Function ModifyTower(ByVal ddl As DropDownList, ByVal ddlCny As DropDownList, ByVal ddlCty As DropDownList, ByVal ddlLoc As DropDownList, ByVal ddlState As DropDownList, ByVal ddlZn As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = ddl.SelectedItem.Value
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlCty.SelectedValue
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCny.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_LocCode", SqlDbType.NVarChar, 50)
        sp7.Value = ddlLoc.SelectedValue
        Dim sp8 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp8.Value = "1"
        Dim spS As New SqlParameter("@VC_STE_CODE", SqlDbType.NVarChar, 50)
        spS.Value = ddlState.SelectedValue
        Dim spz As New SqlParameter("@VC_ZN_CODE", SqlDbType.NVarChar, 50)
        spz.Value = ddlZn.SelectedValue
        Dim sp9 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp9.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updateTower", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, spS, spz, sp9)
        If sp9.Value = 1 Then
            Return 1
        ElseIf sp9.Value = 2 Then
            Return 2
        End If
    End Function
    Public Sub Tower_LoadGrid(ByVal gv As GridView)
        strSQL = "usp_getTowerDetails"
        Make_Active_Inactive(gv, strSQL, 7)
    End Sub
    Public Function Tower_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(7).Text.ToString()
        gv.Columns(7).Visible = True
        gv.Columns(8).Visible = True


        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = selectedRow.Cells(8).Text
        Dim sp2 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp2.Value = btn
        Dim sp3 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp3.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_ChangeTowerStatus", sp1, sp2, sp3)
        If sp3.Value = 0 Then
            Return 0
        Else
            Return 1
        End If


    End Function
    Public Function InsertTower(ByVal ddlCny As DropDownList, ByVal ddlCty As DropDownList, ByVal ddlLoc As DropDownList, ByVal ddlZn As DropDownList, ByVal ddlState As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlCty.SelectedValue
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCny.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_LocCode", SqlDbType.NVarChar, 50)
        sp7.Value = ddlLoc.SelectedValue
        Dim sp8 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp8.Value = "1"
        Dim spS As New SqlParameter("@VC_STE_CODE", SqlDbType.NVarChar, 50)
        spS.Value = ddlState.SelectedValue
        Dim spZ As New SqlParameter("@VC_ZN_CODE", SqlDbType.NVarChar, 50)
        spZ.Value = ddlZn.SelectedValue
        Dim sp9 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp9.Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertTower", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, spS, spZ, sp9)

        If sp9.Value = 1 Then
            Return 1
            'ElseIf sp9.Value = 2 Then
            '    Return 2
        ElseIf sp9.Value = 0 Then
            Return 0
        End If
        'PopUpMessage("Record has been modified", page)
    End Function

    Public Sub Tower_SelectedIndex_Changed(ByVal ddl As DropDownList, ByVal ddlCny As DropDownList, ByVal ddlCty As DropDownList, ByVal ddlLoc As DropDownList, ByVal ddlZn As DropDownList, ByVal ddlState As DropDownList)
        Try
            'strSQL = "Select TWR_CODE,TWR_NAME,TWR_CNY_ID,TWR_CTY_ID,TWR_LOC_ID,TWR_REM from " & HttpContext.Current.Session("TENANT") & "."  & "TOWER where TWR_CODE='" & ddl.SelectedItem.Value & "'"
            'ObjDR = DataReader(strSQL)
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddl.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getTowerDetailsforddl", sp1)
            While ObjDR.Read
                code = ObjDR("TWR_CODE")
                name = ObjDR("TWR_NAME")
                remarks = ObjDR("TWR_REM")
                ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(ObjDR("TWR_CNY_ID")))
                ddlCty.SelectedIndex = ddlCty.Items.IndexOf(ddlCty.Items.FindByValue(ObjDR("TWR_CTY_ID")))
                ddlLoc.SelectedIndex = ddlLoc.Items.IndexOf(ddlLoc.Items.FindByValue(ObjDR("TWR_LOC_ID")))
                ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(ObjDR("TWR_STE_ID")))
                ddlZn.SelectedIndex = ddlZn.Items.IndexOf(ddlZn.Items.FindByValue(ObjDR("TWR_ZN_ID")))
            End While

        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub



    '*********************** Floor EVENTS***********************************-->
    Public Sub BindFloor(ByVal ddl As DropDownList)
        'strSQL = "select distinct FLR_ID,flr_NAME + '/'+ (select twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code=flr_twr_id) as flr_name from " & HttpContext.Current.Session("TENANT") & "."  & "floor " & _
        '        "where flr_sta_id=1 and flr_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city where cty_sta_id=1)" & _
        '        "and flr_cny_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "."  & "country where cny_sta_id=1)" & _
        '        "and flr_loc_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "."  & "location where lcm_sta_id=1 )" & _
        '        "and flr_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_sta_id =1) order by flr_name"
        strSQL = "usp_getActiveFloors"
        BindCombo(strSQL, ddl, "FLR_NAME", "FLR_ID")
        ddl.SelectedIndex = 0
    End Sub


    Public Function insertFloor(ByVal ddltower As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlCny As DropDownList, ByVal ddlZN As DropDownList, ByVal ddlST As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlCity.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCny.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_LocCode", SqlDbType.NVarChar, 50)
        sp7.Value = ddlloc.SelectedValue
        Dim sp8 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp8.Value = "1"
        Dim sp9 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 50)
        sp9.Value = ddltower.SelectedItem.Value
        Dim spz As New SqlParameter("@VC_ZN_CODE", SqlDbType.NVarChar, 50)
        spz.Value = ddlZN.SelectedItem.Value
        Dim sps As New SqlParameter("@VC_STE_CODE", SqlDbType.NVarChar, 50)
        sps.Value = ddlST.SelectedItem.Value
        Dim sp10 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp10.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertFloor", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, spz, sps, sp10)
        If sp10.Value = 1 Then
            Return 1
        ElseIf sp10.Value = 2 Then
            Return 2
        ElseIf sp10.Value = 0 Then
            Return 0
        End If
    End Function
    Public Function ModifyFloor(ByVal ddlflr As DropDownList, ByVal ddltwr As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlCountry As DropDownList, ByVal ddlZn As DropDownList, ByVal ddlState As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = ddlflr.SelectedItem.Value
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlCity.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCountry.SelectedValue
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_LocCode", SqlDbType.NVarChar, 50)
        sp7.Value = ddlloc.SelectedValue
        Dim sp8 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 50)
        sp8.Value = ddltwr.SelectedItem.Value
        Dim sp9 As New SqlParameter("@VC_ZN_CODE", SqlDbType.NVarChar, 50)
        sp9.Value = ddlZn.SelectedItem.Value
        Dim sp10 As New SqlParameter("@VC_STE_CODE", SqlDbType.NVarChar, 50)
        sp10.Value = ddlState.SelectedItem.Value

        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updateFloor", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10)

        Return 1
    End Function

    Public Sub Floor_SelectedIndex_Changed(ByVal ddlflr As DropDownList, ByVal ddltwr As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlcty As DropDownList, ByVal ddlCny As DropDownList, ByVal ddlZne As DropDownList, ByVal ddlSte As DropDownList)
        Try
            'strSQL = "Select DISTINCT FLR_CODE,FLR_NAME,FLR_TWR_ID,FLR_LOC_ID,FLR_CTY_ID,FLR_CNY_ID,FLR_REM from " & HttpContext.Current.Session("TENANT") & "."  & "Floor where FLR_ID='" & ddlflr.SelectedItem.Value & "'"
            'ObjDR = DataReader(strSQL)
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddlflr.SelectedValue
            Dim sp2 As New SqlParameter("@vc_TwrId", SqlDbType.NVarChar, 50)
            sp2.Value = ddltwr.SelectedValue
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getFloorDetailsforddl", sp1, sp2)
            While ObjDR.Read
                code = ObjDR("FLR_CODE")
                name = ObjDR("FLR_NAME")
                remarks = ObjDR("FLR_REM")
                ddltwr.SelectedIndex = ddltwr.Items.IndexOf(ddltwr.Items.FindByValue(ObjDR("FLR_TWR_ID")))
                ddlloc.SelectedIndex = ddlloc.Items.IndexOf(ddlloc.Items.FindByValue(ObjDR("FLR_LOC_ID")))
                ddlcty.SelectedIndex = ddlcty.Items.IndexOf(ddlcty.Items.FindByValue(ObjDR("FLR_CTY_ID")))
                ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(ObjDR("FLR_CNY_ID")))
                ddlSte.SelectedIndex = ddlSte.Items.IndexOf(ddlSte.Items.FindByValue(ObjDR("FLR_STATE_ID")))
                ddlZne.SelectedIndex = ddlZne.Items.IndexOf(ddlZne.Items.FindByValue(ObjDR("FLR_ZN_ID")))
            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub
    Public Sub Floor_LoadGrid(ByVal gv As GridView)

        strSQL = "usp_getFloorDetails"
        Make_Active_Inactive(gv, strSQL, 5)
        gv.Columns(7).Visible = False
    End Sub
    Public Function Floor_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(5).Text.ToString()
        gv.Columns(5).Visible = True
        gv.Columns(6).Visible = True
        Dim lblTwrID As Label = CType(selectedRow.FindControl("lblTwrID"), Label)

        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = selectedRow.Cells(6).Text
        Dim sp2 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 50)
        sp2.Value = lblTwrID.Text
        Dim sp3 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 250)
        sp3.Value = btn
        Dim sp4 As New SqlParameter("@vc_Output", SqlDbType.NVarChar, 50)
        sp4.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_ChangeFloorStatus", sp1, sp2, sp3, sp4)
        If sp4.Value = 0 Then
            Return 0
        Else
            Return 1
        End If

    End Function
    '*********************** Wing EVENTS***********************************
    Public Sub BindFloor1(ByVal ddl As DropDownList)
        strSQL = "select FLR_ID as FLR_CODE,(flr_NAME+'/'+ (select twr_name from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_code=flr_twr_id)) as flr_NAME from " & HttpContext.Current.Session("TENANT") & "." & "floor " & _
                "where flr_sta_id=1 and flr_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "." & "city where cty_sta_id=1)" & _
                "and flr_cny_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "." & "country where cny_sta_id=1)" & _
                "and flr_loc_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1 )" & _
                "and flr_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_sta_id =1) order by flr_name"
        BindCombo(strSQL, ddl, "FLR_NAME", "FLR_CODE")
        ddl.SelectedIndex = 0
    End Sub

    Public Sub BindFloor2(ByVal ddl As DropDownList)
        'strSQL = "select FLR_CODE+'/'+FLR_TWR_ID AS FLR_CODE,(flr_NAME+'/'+ (select twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code=flr_twr_id)) as flr_NAME from " & HttpContext.Current.Session("TENANT") & "."  & "floor " & _
        '        "where flr_sta_id=1 and flr_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city where cty_sta_id=1)" & _
        '        "and flr_cny_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "."  & "country where cny_sta_id=1)" & _
        '        "and flr_loc_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "."  & "location where lcm_sta_id=1 )" & _
        '        "and flr_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_sta_id =1) order by flr_name"
        'FLR_CODE+'/'+FLR_TWR_ID AS
        strSQL = "select  FLR_CODE+'/'+FLR_TWR_ID AS FLR_CODE,(flr_NAME+'/'+ twr_name) as flr_NAME from " & HttpContext.Current.Session("TENANT") & "." & "floor ," & HttpContext.Current.Session("TENANT") & "." & "tower " & _
                "where flr_sta_id=1 and flr_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "." & "city where cty_sta_id=1) and twr_code=flr_twr_id " & _
                "and flr_cny_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "." & "country where cny_sta_id=1)" & _
                "and flr_loc_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1 )" & _
                "and flr_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_sta_id =1) order by flr_name"
        BindCombo(strSQL, ddl, "FLR_NAME", "FLR_CODE")
        ddl.SelectedIndex = 0
    End Sub


    Public Sub BindBlock(ByVal ddl As DropDownList)
        'strSQL = "Select distinct WNG_ID,wng_name + '/'+ isnull((select flr_name from " & HttpContext.Current.Session("TENANT") & "."  & "floor where flr_CODE=wng_flr_id AND FLR_TWR_ID=wng_twr_id" & _
        '        " and flr_twr_id= wng_twr_id),'NA') + '/'+ (select twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code=wng_twr_id) as wng_name from " & HttpContext.Current.Session("TENANT") & "."  & "wing " & _
        '        "where wng_sta_id = 1 and wng_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city)" & _
        '        " and wng_cry_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "."  & "country)" & _
        '        " and wng_lcn_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "."  & "location)" & _
        '        " and wng_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "."  & "tower) " & _
        '        " and wng_flr_id in (select flr_CODE from " & HttpContext.Current.Session("TENANT") & "."  & "floor)order by wng_name"
        strSQL = "usp_getActiveWings"
        BindCombo(strSQL, ddl, "WNG_NAME", "WNG_ID")
        ddl.SelectedIndex = 0
    End Sub
    Public Function insertBlock(ByVal ddlflr As DropDownList, ByVal ddltower As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlCty As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim iVar1 As Integer
        Dim str As String = ddlflr.SelectedValue.ToString().Trim()
        'Dim strFlrCode As String = ddlflr.SelectedValue.ToString().Trim()
        ' Dim strTwrCode As String = ddltower.SelectedValue.ToString().Trim()
        Dim strFlrCode As String = ddlflr.SelectedValue.ToString().Trim().Remove(ddlflr.SelectedValue.ToString().Trim().IndexOf("/"))
        Dim strTwrCode As String = str.Substring(str.IndexOf("/") + 1)


        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlCity.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCty.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_LocCode", SqlDbType.NVarChar, 50)
        sp7.Value = ddlloc.SelectedItem.Value
        Dim sp8 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp8.Value = "1"
        Dim sp9 As New SqlParameter("@vc_flrCode", SqlDbType.NVarChar, 50)
        sp9.Value = strFlrCode
        Dim sp10 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 50)
        sp10.Value = ddltower.SelectedItem.Value
        Dim sp11 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp11.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertWing", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11)
        If sp11.Value = 1 Then
            Return 1
        ElseIf sp11.Value = 2 Then
            Return 2
        ElseIf sp11.Value = 0 Then
            Return 0
        End If

    End Function

    Public Function ModifyBlock(ByVal ddlblk As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltwr As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlCountry As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim str As String = ddlflr.SelectedValue.ToString().Trim()
        Dim strFlrCode As String = ddlflr.SelectedValue.ToString().Trim().Remove(ddlflr.SelectedValue.ToString().Trim().IndexOf("/"))
        Dim strTwrCode As String = str.Substring(str.IndexOf("/") + 1)



        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = ddlblk.SelectedItem.Value
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = remarks.Replace("'", "").Trim()
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlCity.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCountry.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = userid
        Dim sp7 As New SqlParameter("@vc_LocCode", SqlDbType.NVarChar, 50)
        sp7.Value = ddlloc.SelectedItem.Value
        Dim sp9 As New SqlParameter("@vc_flrCode", SqlDbType.NVarChar, 50)
        sp9.Value = strFlrCode
        Dim sp8 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 50)
        sp8.Value = ddltwr.SelectedValue

        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updateWing", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9)

        Return 1
    End Function

    Public Sub Block_SelectedIndex_ChangedWing(ByVal ddlblk As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltwr As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlcty As DropDownList, ByVal ddlCny As DropDownList)
        Try
            'strSQL = "Select WNG_CODE,WNG_NAME,WNG_FLR_ID+'/'+WNG_TWR_ID as WNG_FLR_ID,WNG_TWR_ID,WNG_LCN_ID,WNG_CTY_ID,WNG_CRY_ID,WNG_REM from " & HttpContext.Current.Session("TENANT") & "."  & "Wing where WNG_ID='" & ddlblk.SelectedItem.Value & "'"
            'ObjDR = DataReader(strSQL)
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddlblk.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getWingDetailsforddl", sp1)
            While ObjDR.Read
                code = ObjDR("WNG_CODE")
                name = ObjDR("WNG_NAME")
                remarks = ObjDR("WNG_REM")
                ddlflr.SelectedIndex = ddlflr.Items.IndexOf(ddlflr.Items.FindByValue(ObjDR("WNG_FLR_ID").ToString().Trim()))
                'ddlflr.SelectedIndex = ddlflr.Items.IndexOf(ddlflr.Items.FindByValue(ObjDR("WNG_FLR_ID").Substring(0, ObjDR("WNG_FLR_ID").Length - 3)))
                ddltwr.SelectedIndex = ddltwr.Items.IndexOf(ddltwr.Items.FindByValue(ObjDR("WNG_TWR_ID").ToString().Trim()))
                ddlloc.SelectedIndex = ddlloc.Items.IndexOf(ddlloc.Items.FindByValue(ObjDR("WNG_LCN_ID").ToString().Trim()))
                ddlcty.SelectedIndex = ddlcty.Items.IndexOf(ddlcty.Items.FindByValue(ObjDR("WNG_CTY_ID").ToString().Trim()))
                ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(ObjDR("WNG_CRY_ID").ToString().Trim()))

            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub
    Public Sub Block_SelectedIndex_Changed(ByVal ddlblk As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltwr As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlcty As DropDownList, ByVal ddlCny As DropDownList)
        Try
            'strSQL = "Select WNG_CODE,WNG_NAME,WNG_FLR_ID+'/'+WNG_TWR_ID as WNG_FLR_ID,WNG_TWR_ID,WNG_LCN_ID,WNG_CTY_ID,WNG_CRY_ID,WNG_REM from " & HttpContext.Current.Session("TENANT") & "."  & "Wing where WNG_ID='" & ddlblk.SelectedItem.Value & "'"
            'ObjDR = DataReader(strSQL)
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddlblk.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getWingDetailsforddl", sp1)
            While ObjDR.Read
                code = ObjDR("WNG_CODE")
                name = ObjDR("WNG_NAME")
                remarks = ObjDR("WNG_REM")
                ddlflr.SelectedIndex = ddlflr.Items.IndexOf(ddlflr.Items.FindByValue(ObjDR("WNG_FLR_ID").ToString().Trim()))
                ddltwr.SelectedIndex = ddltwr.Items.IndexOf(ddltwr.Items.FindByValue(ObjDR("WNG_TWR_ID").ToString().Trim()))
                ddlloc.SelectedIndex = ddlloc.Items.IndexOf(ddlloc.Items.FindByValue(ObjDR("WNG_LCN_ID").ToString().Trim()))
                ddlcty.SelectedIndex = ddlcty.Items.IndexOf(ddlcty.Items.FindByValue(ObjDR("WNG_CTY_ID").ToString().Trim()))
                ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(ObjDR("WNG_CRY_ID").ToString().Trim()))

            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub

    Public Sub Block_LoadGrid(ByVal gv As GridView)

        'strSQL = "Select wng_code,wng_name,wng_sta_id,(select distinct flr_name from " & HttpContext.Current.Session("TENANT") & "."  & "floor where flr_CODE=wng_flr_id AND FLR_TWR_ID=wng_twr_id) as 'flr_name',(select twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code=wng_twr_id) as 'twr_name', wng_flr_id ,wng_twr_id from " & HttpContext.Current.Session("TENANT") & "."  & "wing " & _
        '        "where  wng_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city)" & _
        '        " and wng_cry_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "."  & "country)" & _
        '        " and wng_lcn_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "."  & "location)" & _
        '        " and wng_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "."  & "tower) " & _
        '        " and wng_flr_id in (select flr_CODE from " & HttpContext.Current.Session("TENANT") & "."  & "floor) order by wng_name"
        strSQL = "usp_getWingDetails"
        Make_Active_Inactive(gv, strSQL, 4)
        gv.Columns(6).Visible = False
        'gv.Columns(6).Visible = False
        'gv.Columns(7).Visible = False
    End Sub
    Public Function Block_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(4).Text.ToString()
        'gv.Columns(4).Visible = True
        'gv.Columns(5).Visible = True
        Dim lblFlrID As Label = CType(selectedRow.FindControl("lblFlrID"), Label)
        Dim lblTwrID As Label = CType(selectedRow.FindControl("lblTwrID"), Label)
        Dim btn2 As String = selectedRow.Cells(5).Text.ToString()


        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = selectedRow.Cells(5).Text
        Dim sp2 As New SqlParameter("@vc_flrCode", SqlDbType.NVarChar, 50)
        sp2.Value = lblFlrID.Text
        Dim sp3 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 250)
        sp3.Value = lblTwrID.Text
        Dim sp4 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp4.Value = btn
        Dim sp5 As New SqlParameter("@vc_Output", SqlDbType.NVarChar, 50)
        sp5.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_ChangeWingStatus", sp1, sp2, sp3, sp4, sp5)

        Return 1


    End Function


    '*******************  Conference Events   ******************************************** added by praveen15/02/2014
    Public Sub BindCFloor1(ByVal ddl As DropDownList)
        strSQL = "select FLR_ID as FLR_CODE,(flr_NAME+'/'+ (select twr_name from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_code=flr_twr_id)) as flr_NAME from " & HttpContext.Current.Session("TENANT") & "." & "floor " & _
                "where flr_sta_id=1 and flr_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "." & "city where cty_sta_id=1)" & _
                "and flr_cny_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "." & "country where cny_sta_id=1)" & _
                "and flr_loc_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1 )" & _
                "and flr_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_sta_id =1) order by flr_name"
        BindCombo(strSQL, ddl, "FLR_NAME", "FLR_CODE")
        ddl.SelectedIndex = 0
    End Sub

    Public Sub Get_Tower(ByVal value As String, ByVal ddl As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "EFM_SRQ_TOWER")
        sp.Command.AddParameter("@BDG_ADM_CODE", value, DbType.String)
        ddl.DataSource = sp.GetDataSet()
        ddl.DataTextField = "TWR_NAME"
        ddl.DataValueField = "TWR_CODE"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select--")
    End Sub
    Public Sub Get_Location(ByVal value As String, ByVal ddl As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "EFM_SRQ_LOCATION")
        sp.Command.AddParameter("@LCM_CITY_ID", value, DbType.String)
        ddl.DataSource = sp.GetDataSet()
        ddl.DataTextField = "BDG_NAME"
        ddl.DataValueField = "BDG_ADM_CODE"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select--")
    End Sub
    Public Sub Get_Floor(ByVal bdgtemp As String, ByVal twrtemp As String, ByVal ddlFLR As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "EFM_SRQ_FLOOR")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        ddlFLR.DataSource = sp.GetDataSet()
        ddlFLR.DataTextField = "FLR_NAME"
        ddlFLR.DataValueField = "FLR_CODE"
        ddlFLR.DataBind()
        ddlFLR.Items.Insert(0, "--Select--")
    End Sub
    Public Sub BindCFloor2(ByVal ddl As DropDownList)
        'strSQL = "select FLR_CODE+'/'+FLR_TWR_ID AS FLR_CODE,(flr_NAME+'/'+ (select twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code=flr_twr_id)) as flr_NAME from " & HttpContext.Current.Session("TENANT") & "."  & "floor " & _
        '        "where flr_sta_id=1 and flr_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city where cty_sta_id=1)" & _
        '        "and flr_cny_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "."  & "country where cny_sta_id=1)" & _
        '        "and flr_loc_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "."  & "location where lcm_sta_id=1 )" & _
        '        "and flr_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_sta_id =1) order by flr_name"
        'FLR_CODE+'/'+FLR_TWR_ID AS
        strSQL = "select  FLR_CODE+'/'+FLR_TWR_ID AS FLR_CODE,(flr_NAME+'/'+ twr_name) as flr_NAME from " & HttpContext.Current.Session("TENANT") & "." & "floor ," & HttpContext.Current.Session("TENANT") & "." & "tower " & _
                "where flr_sta_id=1 and flr_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "." & "city where cty_sta_id=1) and twr_code=flr_twr_id " & _
                "and flr_cny_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "." & "country where cny_sta_id=1)" & _
                "and flr_loc_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1 )" & _
                "and flr_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_sta_id =1) order by flr_name"
        BindCombo(strSQL, ddl, "FLR_NAME", "FLR_CODE")
        ddl.SelectedIndex = 0
    End Sub


    Public Sub BindConf(ByVal ddl As DropDownList)
        'strSQL = "Select distinct WNG_ID,wng_name + '/'+ isnull((select flr_name from " & HttpContext.Current.Session("TENANT") & "."  & "floor where flr_CODE=wng_flr_id AND FLR_TWR_ID=wng_twr_id" & _
        '        " and flr_twr_id= wng_twr_id),'NA') + '/'+ (select twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code=wng_twr_id) as wng_name from " & HttpContext.Current.Session("TENANT") & "."  & "wing " & _
        '        "where wng_sta_id = 1 and wng_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "."  & "city)" & _
        '        " and wng_cry_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "."  & "country)" & _
        '        " and wng_lcn_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "."  & "location)" & _
        '        " and wng_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "."  & "tower) " & _
        '        " and wng_flr_id in (select flr_CODE from " & HttpContext.Current.Session("TENANT") & "."  & "floor)order by wng_name"
        strSQL = "GET_CONFERENCE_DETAILS"
        BindCombo(strSQL, ddl, "CONFERENCE_ROOM_NAME", "CONFERENCE_ROOM_CODE")
        ddl.SelectedIndex = 0
    End Sub

    Public Sub BindConfType(ByVal ddl As DropDownList)
        strSQL = "GET_CONFERENCE_TYPE"
        BindCombo(strSQL, ddl, "SPC_CR_TYPE_NAME", "SPC_CR_TYPE_CODE")
        ddl.SelectedIndex = 0
    End Sub
    Public Function insertConf(ByVal ddlConfType As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltower As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal page As Page) As Integer
        Dim str As String = ddlflr.SelectedValue.ToString().Trim()
        'Dim strFlrCode As String = ddlflr.SelectedValue.ToString().Trim()
        ' Dim strTwrCode As String = ddltower.SelectedValue.ToString().Trim()
        Dim strFlrCode As String = ddlflr.SelectedValue.ToString()
        Dim strTwrCode As String = ddltower.SelectedValue.ToString()
        Dim strCType As String = ddlConfType.SelectedValue.ToString()

        Dim sp1 As New SqlParameter("@CONFERENCE_ROOM_CODE", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@CONFERENCE_ROOM_NAME", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@CAPACITY", SqlDbType.Int)
        sp3.Value = Capacity
        Dim sp4 As New SqlParameter("@TYPE", SqlDbType.NVarChar, 50)
        sp4.Value = strCType
        Dim sp5 As New SqlParameter("@CONF_LOC", SqlDbType.NVarChar, 50)
        sp5.Value = ddlloc.SelectedItem.Value
        Dim sp6 As New SqlParameter("@CONF_TOWER", SqlDbType.NVarChar, 50)
        sp6.Value = ddltower.SelectedItem.Value
        Dim sp7 As New SqlParameter("@CONF_FLOOR", SqlDbType.NVarChar, 50)
        sp7.Value = strFlrCode
        Dim sp8 As New SqlParameter("@CONF_CITY", SqlDbType.NVarChar, 50)
        sp8.Value = ddlCity.SelectedItem.Value
        Dim sp9 As New SqlParameter("@CONF_Output", SqlDbType.Int)
        sp9.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertConference", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9)
        If sp9.Value = 1 Then
            Return 1
        ElseIf sp9.Value = 2 Then
            Return 2
        ElseIf sp9.Value = 0 Then
            Return 0
        End If

    End Function

    Public Function ModifyConf(ByVal ddlConfType As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltower As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim str As String = ddlflr.SelectedValue.ToString().Trim()
        Dim strFlrCode As String = ddlflr.SelectedValue.ToString().Trim().Remove(ddlflr.SelectedValue.ToString().Trim().IndexOf("/"))
        Dim strTwrCode As String = str.Substring(str.IndexOf("/") + 1)
        Dim strCType As String = ddlConfType.SelectedValue.ToString()


        Dim sp1 As New SqlParameter("@CONFERENCE_ROOM_CODE", SqlDbType.NVarChar, 50)
        sp1.Value = code
        Dim sp2 As New SqlParameter("@CONFERENCE_ROOM_NAME", SqlDbType.NVarChar, 50)
        sp2.Value = name
        Dim sp3 As New SqlParameter("@CAPACITY", SqlDbType.Int)
        sp3.Value = Capacity
        Dim sp4 As New SqlParameter("@TYPE", SqlDbType.NVarChar, 50)
        sp4.Value = strCType
        Dim sp5 As New SqlParameter("@CONF_LOC", SqlDbType.NVarChar, 50)
        sp5.Value = ddlloc.SelectedItem.Value
        Dim sp6 As New SqlParameter("@CONF_TOWER", SqlDbType.NVarChar, 50)
        sp6.Value = ddltower.SelectedItem.Value
        Dim sp7 As New SqlParameter("@CONF_FLOOR", SqlDbType.NVarChar, 50)
        sp7.Value = strFlrCode
        Dim sp8 As New SqlParameter("@CONF_CITY", SqlDbType.NVarChar, 50)
        sp8.Value = ddlCity.SelectedItem.Value
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UPDATE_CONFERENCE_DETAILS", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8)

        Return 1
    End Function

    Public Sub Block_SelectedIndex_ChangedConf(ByVal ddlCName As DropDownList, ByVal ddlConftype As DropDownList, ByVal ddlFloor As DropDownList, ByVal ddlTower As DropDownList, ByVal ddlLocation As DropDownList, ByVal ddlCity As DropDownList)
        Try
            'strSQL = "Select WNG_CODE,WNG_NAME,WNG_FLR_ID+'/'+WNG_TWR_ID as WNG_FLR_ID,WNG_TWR_ID,WNG_LCN_ID,WNG_CTY_ID,WNG_CRY_ID,WNG_REM from " & HttpContext.Current.Session("TENANT") & "."  & "Wing where WNG_ID='" & ddlblk.SelectedItem.Value & "'"
            'ObjDR = DataReader(strSQL)
            Dim sp1 As New SqlParameter("@CONFERENCE_ROOM_CODE", SqlDbType.NVarChar, 50)
            sp1.Value = ddlCName.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CONFERENCE_DETAILS_forddl", sp1)
            While ObjDR.Read
                code = ObjDR("CONFERENCE_ROOM_CODE")
                name = ObjDR("CONFERENCE_ROOM_NAME")
                Capacity = ObjDR("CAPACITY")
                ddlConftype.SelectedIndex = ddlConftype.Items.IndexOf(ddlConftype.Items.FindByValue(ObjDR("TYPE").ToString().Trim()))
                'ddlflr.SelectedIndex = ddlflr.Items.IndexOf(ddlflr.Items.FindByValue(ObjDR("WNG_FLR_ID").Substring(0, ObjDR("WNG_FLR_ID").Length - 3)))
                ddlFloor.SelectedIndex = ddlFloor.Items.IndexOf(ddlFloor.Items.FindByValue(ObjDR("CONF_FLOOR").ToString().Trim()))
                ddlTower.SelectedIndex = ddlTower.Items.IndexOf(ddlTower.Items.FindByValue(ObjDR("CONF_TOWER").ToString().Trim()))
                ddlLocation.SelectedIndex = ddlLocation.Items.IndexOf(ddlLocation.Items.FindByValue(ObjDR("CONF_LOC").ToString().Trim()))
                ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(ObjDR("CONF_CITY").ToString().Trim()))

            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub

    Public Sub Block_LoadGridConf(ByVal gv As GridView)


        strSQL = "GET_CONFERENCE_DETAILS_GRID"

    End Sub
    Public Function Block_RowcommandConf(ByVal gv As GridView, ByVal index As Integer) As Integer
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(4).Text.ToString()
        'gv.Columns(4).Visible = True
        'gv.Columns(5).Visible = True
        Dim lblFlrID As Label = CType(selectedRow.FindControl("lblFlrID"), Label)
        Dim lblTwrID As Label = CType(selectedRow.FindControl("lblTwrID"), Label)
        Dim btn2 As String = selectedRow.Cells(5).Text.ToString()


        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = selectedRow.Cells(5).Text
        Dim sp2 As New SqlParameter("@vc_flrCode", SqlDbType.NVarChar, 50)
        sp2.Value = lblFlrID.Text
        Dim sp3 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 250)
        sp3.Value = lblTwrID.Text
        Dim sp4 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp4.Value = btn
        Dim sp5 As New SqlParameter("@vc_Output", SqlDbType.NVarChar, 50)
        sp5.Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_ChangeWingStatus", sp1, sp2, sp3, sp4, sp5)

        Return 1


    End Function




    '******************  Seat Master Events******************************************************
    Public Sub BindSeat(ByVal ddl As DropDownList)
        strSQL = "Select distinct SPC_code,SPC_name from " & HttpContext.Current.Session("TENANT") & "." & "SEATMASTER " & _
                "where SPC_STA_ID = 1 and SPC_CTY_ID in (select cty_code from " & HttpContext.Current.Session("TENANT") & "." & "city where cty_sta_id=1)" & _
                " and SPC_CNY_ID in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "." & "country where cny_sta_id=1)" & _
                " and SPC_LOC_ID in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1 )" & _
                " and SPC_TWR_ID in (select twr_code from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_sta_id =1) " & _
                " and SPC_FLR_ID in (select flr_code from " & HttpContext.Current.Session("TENANT") & "." & "floor where flr_sta_id=1)" & _
                " AND SPC_WNG_ID IN (SELECT WNG_CODE FROM " & HttpContext.Current.Session("TENANT") & "." & "WING WHERE WNG_STA_ID=1) order by spc_name"
        BindCombo(strSQL, ddl, "SPC_NAME", "SPC_CODE")
        ddl.SelectedIndex = 0
    End Sub
    Public Function insertSeat(ByVal ddlwng As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltower As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlCty As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Dim iVar1 As Integer
        strSQL = "Select count(*) from " & HttpContext.Current.Session("TENANT") & "." & "seatmaster where spc_CODE='" & code & "'"
        iVar1 = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)
        strSQL = "Select count(*) from " & HttpContext.Current.Session("TENANT") & "." & "seatmaster where spc_NAME='" & name & "'"
        Dim iVar2 As Integer = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)
        If iVar1 > 0 Then
            PopUpMessage("The seat Code Already Exists, Please Modify Code.", page)
            Return 0
            'ElseIf iVar2 > 0 Then
            '    PopUpMessage("The seat Name Already Exists, Please Modify Name.", page)
            '    Return 0
        Else
            strSQL = "insert into " & HttpContext.Current.Session("TENANT") & "." & "SEATMASTER(spc_CODE,SPC_NAME,SPC_FLR_ID,SPC_TWR_ID,SPC_LOC_ID,SPC_CTY_ID,SPC_CNY_ID,SPC_REMARKS,SPC_UPT_BY,SPC_UPT_DATE,SPC_STA_ID,SPC_WNG_ID) values" & _
                                                                    "('" & code & "','" & name & "','" & ddlflr.SelectedItem.Value & "','" & ddltower.SelectedItem.Value & "','" & ddlloc.SelectedItem.Value & "','" & ddlCity.SelectedItem.Value & "','" & ddlCty.SelectedItem.Value & "','" & remarks.Replace("'", "''") & "','" & userid & "','" & getoffsetdatetime(DateTime.Now) & "','1','" & ddlwng.SelectedItem.Value & "')"
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            PopUpMessage("Record has been inserted", page)
            Return 1
        End If
    End Function
    Public Function ModifySeat(ByVal ddlblk As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltwr As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlCity As DropDownList, ByVal ddlCountry As DropDownList, ByVal ddlseat As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "SEATMASTER set spc_NAME='" & name & "',spc_wng_id='" & ddlblk.SelectedItem.Value & "'spc_FLR_ID='" & ddlflr.SelectedItem.Value & "',spc_TWR_ID='" & ddltwr.SelectedItem.Value & "',spc_Loc_ID='" & ddlloc.SelectedItem.Value & "',spc_CTY_ID='" & ddlCity.SelectedItem.Value & "',spc_cny_ID='" & ddlCountry.SelectedItem.Value & "',spc_REMarks='" & remarks.Replace("'", "''") & "',spc_upt_by='" & userid & "',spc_upt_date='" & getoffsetdatetime(DateTime.Now) & "' where spc_CODE='" & ddlseat.SelectedItem.Value & "'"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        PopUpMessage("Record has been modified", page)
        Return 1
    End Function
    Public Sub Seat_SelectedIndex_Changed(ByVal ddlseat As DropDownList, ByVal ddlblk As DropDownList, ByVal ddlflr As DropDownList, ByVal ddltwr As DropDownList, ByVal ddlloc As DropDownList, ByVal ddlcty As DropDownList, ByVal ddlCny As DropDownList)
        Try
            strSQL = "Select spc_CODE,spc_NAME,spc_wng_id,spc_FLR_ID,spc_TWR_ID,spc_Loc_ID,spc_CTY_ID,spc_CnY_ID,spc_REMarks from " & HttpContext.Current.Session("TENANT") & "." & "SEATMASTER where spc_CODE='" & ddlseat.SelectedItem.Value & "'"
            ObjDR = DataReader(strSQL)
            While ObjDR.Read
                code = ObjDR("spc_CODE")
                name = ObjDR("spc_NAME")
                remarks = ObjDR("spc_REMarks")
                ddlflr.SelectedIndex = ddlflr.Items.IndexOf(ddlflr.Items.FindByValue(ObjDR("spc_FLR_ID")))
                ddltwr.SelectedIndex = ddltwr.Items.IndexOf(ddltwr.Items.FindByValue(ObjDR("spc_TWR_ID")))
                ddlloc.SelectedIndex = ddlloc.Items.IndexOf(ddlloc.Items.FindByValue(ObjDR("spc_Loc_ID")))
                ddlcty.SelectedIndex = ddlcty.Items.IndexOf(ddlcty.Items.FindByValue(ObjDR("spc_CTY_ID")))
                ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(ObjDR("spc_CnY_ID")))
                ddlblk.SelectedIndex = ddlblk.Items.IndexOf(ddlblk.Items.FindByValue(ObjDR("spc_wng_id")))
            End While
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try
    End Sub

    Public Sub Seat_LoadGrid(ByVal gv As GridView)

        strSQL = "Select distinct SPC_code,SPC_name,SPC_sta_id from " & HttpContext.Current.Session("TENANT") & "." & "SEATMASTER " & _
                "where  SPC_cty_id in (select cty_code from " & HttpContext.Current.Session("TENANT") & "." & "city where cty_sta_id=1)" & _
                " and SPC_cNy_id in (select cny_code from  " & HttpContext.Current.Session("TENANT") & "." & "country where cny_sta_id=1)" & _
                " and SPC_lOC_id in (select lcm_code from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1 )" & _
                " and SPC_twr_id in (select twr_code from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_sta_id =1) " & _
                " and SPC_flr_id in (select flr_code from " & HttpContext.Current.Session("TENANT") & "." & "floor where flr_sta_id=1)" & _
                " AND SPC_WNG_ID IN (SELECT WNG_CODE FROM " & HttpContext.Current.Session("TENANT") & "." & "WING WHERE WNG_STA_ID=1) order by SPC_name"
        Make_Active_Inactive(gv, strSQL)
    End Sub
    Public Sub Seat_Rowcommand(ByVal gv As GridView, ByVal index As Integer)
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(2).Text.ToString()
        gv.Columns(2).Visible = True
        gv.Columns(3).Visible = True
        Select Case Trim(btn)
            Case "1"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "SEATMASTER set SPC_STA_ID=2 where SPC_CODE='" & _
                         selectedRow.Cells(3).Text & "'"
            Case "2"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "SEATMASTER set SPC_STA_ID=1 where SPC_CODE='" & selectedRow.Cells(3).Text & "'"
            Case Else
        End Select
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
    End Sub

    '***************  To display the details in the respective combo Box *************
    Public Function seat_getwng_detls(ByVal wng_id As String) As SqlDataReader
        Dim sp1 As SqlParameter = New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = wng_id
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getWingDetailsforCombo", sp1)
        Return ds
    End Function
    Public Function block_getflr_detls(ByVal flr_id As String, ByVal strTwrID As String) As SqlDataReader
        'strSQL = "select flr_twr_id,flr_loc_id,flr_cty_id,flr_cny_id from " & HttpContext.Current.Session("TENANT") & "."  & "floor where flr_CODE='" & flr_id & "' and FLR_TWR_ID ='" & strTwrID & "'"
        Dim sp1 As SqlParameter = New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = flr_id
        Dim sp2 As SqlParameter = New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 50)
        sp2.Value = strTwrID
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getFloorDetailsforCombo", sp1, sp2)
        Return ds
    End Function
    Public Function block_getflr_detls_Wing(ByVal flr_id As String) As SqlDataReader
        'strSQL = "select flr_twr_id,flr_loc_id,flr_cty_id,flr_cny_id from " & HttpContext.Current.Session("TENANT") & "."  & "floor where flr_id='" & flr_id & "'"
        Dim sp1 As SqlParameter = New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = flr_id
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getFloorDetailsTowerWise", sp1)
        Return ds
    End Function
    Public Function floor_gettower_dtls(ByVal tow_id As String) As SqlDataReader
        'strSQL = "select twr_cty_id,twr_loc_id,twr_cny_id from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code='" & tow_id & "' "
        'Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Dim sp1 As SqlParameter = New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = tow_id
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getTowerDetailsforCombo", sp1)
        Return ds
    End Function
    Public Function tower_getloc_dtls(ByVal loc_id As String) As SqlDataReader
        'strSQL = "select lcm_cty_id,lcm_cny_id from " & HttpContext.Current.Session("TENANT") & "."  & "location where lcm_code='" & loc_id & "'"
        'Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Dim sp1 As SqlParameter = New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = loc_id
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getLocationDetailsforCombo", sp1)
        Return ds
    End Function
    Public Function location_getcity_dtls(ByVal cty_id As String) As SqlDataReader
        'strSQL = "select cty_cny_id from " & HttpContext.Current.Session("TENANT") & "."  & "city where cty_code='" & cty_id & "'"
        'Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Dim sp1 As SqlParameter = New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = cty_id
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getCountryDetailsforCombo", sp1)
        Return ds
    End Function


    '*****************  Department  Events ********************************
    Public Shared dtDept As DataTable
    Public Sub Department_LoadGrid(ByVal gv As GridView)
        'dtDept = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure,"USP_GET_DEPARTMENT")
        dtDept = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_DEPARTMENT")
        gv.DataSource = dtDept
        gv.DataBind()
    End Sub

    Public Sub Department_Rowcommand(ByVal intStatus As Integer, ByVal strDeptCode As String)
        Dim spStatus As New SqlParameter("@I_STATUSID", SqlDbType.SmallInt)
        Dim spVerticalCode As New SqlParameter("@VC_DEPTCODE", SqlDbType.NVarChar, 50)
        If intStatus = 1 Then
            intStatus = 2
        ElseIf intStatus = 2 Then
            intStatus = 1
        End If
        spStatus.Value = intStatus
        spVerticalCode.Value = strDeptCode
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_DEPARTMENT_UPDATE_STATUS", spStatus, spVerticalCode)



    End Sub

    Public Sub BindDepartment(ByVal ddl As DropDownList)
        BindCombo("USP_GET_DEPARTMENT", ddl, "DEP_NAME", "DEP_CODE")
        ddl.SelectedIndex = 0
    End Sub

    Public Function InsertDepartment(ByVal page As Page, ByVal intStatus As Integer) As Integer
        Try
            Dim userid As String = page.Session("uid")
            Dim spDep_CODE As New SqlParameter("@DEP_CODE", SqlDbType.NVarChar, 50)
            Dim spDep_NAME As New SqlParameter("@DEP_NAME", SqlDbType.NVarChar, 250)
            Dim spDep_REM As New SqlParameter("@DEP_REM", SqlDbType.NVarChar, 500)
            Dim spDep_UPT_BY As New SqlParameter("@DEP_UPT_BY", SqlDbType.NVarChar, 50)
            'Dim spDep_Vertical As New SqlParameter("@DEP_VERTICAL", SqlDbType.NVarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)

            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output

            spDep_CODE.Value = code
            spDep_NAME.Value = name
            spDep_REM.Value = remarks.Replace("'", "''")
            spDep_UPT_BY.Value = userid
            'spDep_Vertical.Value = ddl.SelectedValue.Trim()
            spi_Status.Value = intStatus
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_DEPARTMENT", spDep_CODE, spDep_NAME, spDep_REM, spDep_UPT_BY, spi_Status, spi_Op)
            Return spi_Op.Value

        Catch ex As Exception

        End Try


    End Function

    Public Function ModifyDepartment(ByVal ddl As DropDownList, ByVal DDL1 As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "DEPARTMENT set DEP_NAME='" & name & "',DEP_REM='" & remarks.Replace("'", "''") & "',DEP_VERTICAL ='" & DDL1.SelectedItem.Value & "',dep_upt_dt='" & getoffsetdatetime(DateTime.Now) & "',dep_upt_by='" & userid & "' where DEP_CODE='" & ddl.SelectedItem.Value & "'"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        PopUpMessage("Record has been modified", page)
        Return 1
    End Function



    Public Sub Department_SelectedIndex_Changed(ByVal ddl1 As DropDownList)
        Try
            Dim drDept() As DataRow = dtDept.Select("DEP_CODE='" & ddl1.SelectedItem.Value & "'")
            If drDept.Length > 0 Then
                code = drDept(0)("DEP_CODE")
                name = drDept(0)("DEP_NAME")
                remarks = drDept(0)("DEP_REM")
                'ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(drDept(0)("DEP_VERTICAL").ToString().Trim()))
            End If
        Catch ex As Exception
        Finally

        End Try


    End Sub
    '********************* VERTICAL EVENTS *****************************
    Public Shared dtVertical As DataTable
    Public Sub Vertical_LoadGrid(ByVal gv As GridView)


        dtVertical = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_VERTICAL")

        gv.DataSource = dtVertical
        gv.DataBind()
    End Sub
    Public Sub Vertical_Rowcommand(ByVal strVerticalCode As String, ByVal intStatus As Integer)
        Dim spStatus As New SqlParameter("@I_STATUSID", SqlDbType.SmallInt)
        Dim spVerticalCode As New SqlParameter("@VC_VERTICALCODE", SqlDbType.NVarChar, 50)
        If intStatus = 1 Then
            intStatus = 2
        ElseIf intStatus = 2 Then
            intStatus = 1
        End If
        spStatus.Value = intStatus
        spVerticalCode.Value = strVerticalCode
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_VERTICAL", spStatus, spVerticalCode)
    End Sub

    Public Sub BindVertical(ByVal ddl As DropDownList)
        BindCombo("USP_GET_VERTICAL", ddl, "VER_NAME", "VER_CODE")
        ddl.SelectedIndex = 0
    End Sub
    Public Sub BindAATID(ByVal ddl As DropDownList)
        BindCombo(HttpContext.Current.Session("TENANT") & "." & "AAT_GET_STATUS  1", ddl, "AAT_NAME", "AAT_CODE")
        ddl.SelectedIndex = 0
    End Sub


    Public Function InsertVertical(ByVal page As Page, ByVal status As String) As Integer
        Try
            Dim userid As String = page.Session("uid")

            Dim spVER_CODE As New SqlParameter("@VER_CODE", SqlDbType.NVarChar, 50)
            Dim spVER_NAME As New SqlParameter("@VER_NAME", SqlDbType.NVarChar, 250)
            Dim spVER_REM As New SqlParameter("@VER_REM", SqlDbType.NVarChar, 500)
            Dim spVER_UPT_BY As New SqlParameter("@VER_UPT_BY", SqlDbType.NVarChar, 50)
            Dim spVER_VRM As New SqlParameter("@VER_VRM", SqlDbType.VarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)
            Dim spVER_PE_ID As New SqlParameter("@VER_PE_CODE", SqlDbType.NVarChar, 150)
            Dim spVER_CH_ID As New SqlParameter("@VER_CH_CODE", SqlDbType.NVarChar, 150)

            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output

            spVER_CODE.Value = code
            spVER_NAME.Value = name
            spVER_REM.Value = remarks.Replace("'", "''")
            spVER_UPT_BY.Value = userid
            spVER_VRM.Value = VRM
            spVER_PE_ID.Value = parent_entity
            spVER_CH_ID.Value = child_entity

            If status = "MODIFY" Then
                spi_Status.Value = 2
            Else
                spi_Status.Value = 1
            End If

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_VERTICAL", spVER_CODE, spVER_NAME, spVER_REM, spVER_UPT_BY, spi_Status, spi_Op, spVER_VRM, spVER_PE_ID, spVER_CH_ID)
            Return spi_Op.Value

        Catch ex As Exception

        End Try



    End Function

    Public Function ModifyVertical(ByVal ddl As DropDownList, ByVal page As Page) As Integer
        Try
            Dim userid As String = page.Session("uid")

            Dim spVER_CODE As New SqlParameter("@VER_CODE", SqlDbType.NVarChar, 50)
            Dim spVER_NAME As New SqlParameter("@VER_NAME", SqlDbType.NVarChar, 250)
            Dim spVER_REM As New SqlParameter("@VER_REM", SqlDbType.NVarChar, 500)
            Dim spVER_UPT_BY As New SqlParameter("@VER_UPT_BY", SqlDbType.NVarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)
            Dim spVER_PE_ID As New SqlParameter("@VER_PE_CODE", SqlDbType.NVarChar, 150)
            Dim spVER_CH_ID As New SqlParameter("@VER_CH_CODE", SqlDbType.NVarChar, 150)


            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output

            spVER_CODE.Value = code
            spVER_NAME.Value = name
            spVER_REM.Value = remarks.Replace("'", "''")
            spVER_UPT_BY.Value = userid
            spi_Status.Value = 2
            spVER_PE_ID.Value = parent_entity
            spVER_CH_ID.Value = child_entity

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_VERTICAL", spVER_CODE, spVER_NAME, spVER_REM, spVER_UPT_BY, spi_Status, spi_Op, spVER_PE_ID, spVER_CH_ID)
            Return spi_Op.Value

        Catch ex As Exception

        End Try

    End Function

    Public Sub Vertical_SelectedIndex_Changed(ByVal ddl As DropDownList)


        Try
            Dim drVertical() As DataRow = dtVertical.Select("VER_CODE='" & ddl.SelectedItem.Value & "'")
            If drVertical.Length > 0 Then
                code = drVertical(0)("VER_CODE")
                name = drVertical(0)("VER_NAME")
                remarks = drVertical(0)("VER_REM")
                parent_entity = drVertical(0)("PE_NAME")
                child_entity = drVertical(0)("CHE_NAME")


                'ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(drVertical(0)("VER_CODE")))
            End If
        Catch ex As Exception
        Finally

        End Try


    End Sub


#Region " REQUEST ID GENARATION(RID)"
    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function
#Region " REQUEST ID GENARATION(Req)"
    Public Function REQGENARATION_REQ(ByVal Strfield As String, ByVal countfield As String, ByVal DataBase As String, ByVal Table As String, Optional ByVal Wherecondiction As String = "1=1", Optional ByVal field1 As String = "0", Optional ByVal field2 As String = "0") As String
        Dim TmpReqseqid As String
        Dim Reqseqid As Integer

        strSQL = "SELECT isnull(MAX(" & countfield & "),0)+1 FROM " & DataBase + Table & " where " & Wherecondiction
        Reqseqid = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)

        If Reqseqid < 10 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/000000" & Reqseqid
        ElseIf Reqseqid < 100 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/00000" & Reqseqid
        ElseIf Reqseqid < 1000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/0000" & Reqseqid
        ElseIf Reqseqid < 10000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/000" & Reqseqid
        ElseIf Reqseqid < 100000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/00" & Reqseqid
        ElseIf Reqseqid < 1000000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/0" & Reqseqid
        Else
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/" & Reqseqid
        End If
        Return TmpReqseqid
    End Function
#End Region
#End Region

    '************************  New Employee Events ********************************
    Public Sub binddept(ByVal ddl As DropDownList)
        strSQL = " select distinct dep_id,dep_name from " & HttpContext.Current.Session("TENANT") & "." & "Department order by dep_name"
        BindCombo(strSQL, ddl, "dep_name", "dep_id")
        ddl.SelectedIndex = 0
    End Sub
    Public Sub bindloc(ByVal ddl As DropDownList)
        strSQL = "select DISTINCT LCM_name,LCM_CODE from " & HttpContext.Current.Session("TENANT") & "." & "LOCATION ORDER BY LCM_NAME"
        BindCombo(strSQL, ddl, "LCM_name", "LCM_CODE")
    End Sub
    Public Sub binduser(ByVal ddl As DropDownList, ByVal user As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "USP_GETUSERDETAILSFORDDL")
        sp.Command.AddParameter("@AUR_ID", user, DbType.String)
        ddl.DataSource = sp.GetDataSet()
        ddl.DataTextField = "aur_known_as"
        ddl.DataValueField = "aur_id"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select--")
    End Sub
    Public Sub binddesig(ByVal ddl As DropDownList)
        strSQL = "select distinct DSN_CODE,DSN_AMT_TITLE from " & HttpContext.Current.Session("TENANT") & "." & "designation where DSN_STA_ID='1' order by DSN_AMT_TITLE"
        BindCombo(strSQL, ddl, "DSN_AMT_TITLE", "DSN_CODE")
    End Sub

    Public Sub insertuser(ByVal txtfname As TextBox, ByVal txtmname As TextBox, ByVal txtlname As TextBox, ByVal txtaurid As TextBox, ByVal ddlm1 As DropDownList, ByVal ddlm2 As DropDownList, ByVal ddld1 As DropDownList, ByVal ddld2 As DropDownList, ByVal ddly2 As DropDownList, ByVal ddly1 As DropDownList, ByVal ddltitle As DropDownList, ByVal txtmailid As TextBox, ByVal ddlrepman As DropDownList, ByVal ddldg As DropDownList, ByVal txtext As TextBox, ByVal txtphoff As TextBox, ByVal txtphres As TextBox, ByVal ddldesg As DropDownList, ByVal ddldept As DropDownList, ByVal txtcount As TextBox, ByVal txtaddress As TextBox, ByVal txtcity As TextBox, ByVal txtstate As TextBox, ByVal txtp As TextBox, ByVal txtGrade As TextBox, ByVal page As Page)
        Dim userid As String = page.Session("uid")
        Dim knownas As String
        knownas = txtfname.Text & " " & txtmname.Text & " " & txtlname.Text
        Dim valmax, aurno, dob, doj, gender, gender1 As String
        valmax = txtaurid.Text
        aurno = valmax
        dob = ddlm1.SelectedItem.Value & "/" & ddld1.SelectedItem.Text & "/" & ddly1.SelectedItem.Text
        doj = ddlm2.SelectedItem.Value & "/" & ddld2.SelectedItem.Text & "/" & ddly2.SelectedItem.Text
        If IsDate(dob) Then
        Else
            PopUpMessage("Select Valid Date", page)
            Exit Sub
        End If
        If IsDate(doj) Then
        Else
            PopUpMessage("Select Valid Date for Joining", page)
            Exit Sub
        End If
        If ddltitle.SelectedItem.Text = "Mr." Then
            gender = "Male"
            gender1 = "M"
        Else
            gender = "Female"
            gender1 = "F"
        End If
        str = "insert into " & HttpContext.Current.Session("TENANT") & "." & "AMANTRA_USER (AUR_ID,AUR_NO,AUR_TITLE,AUR_FIRST_NAME,AUR_MIDDLE_NAME, " & _
                "AUR_LAST_NAME,AUR_KNOWN_AS,aur_email,AUR_SSNO,AUR_INO,AUR_REPORTING_TO,AUR_BDG_ID,AUR_EXTENSION, " & _
                "AUR_DIRECT_LINE,AUR_RES_NUMBER,AUR_DESGN_ID,AUR_DEP_ID,AUR_INACTIVE,AUR_DOB,AUR_DOJ, " & _
                    "AUR_COUNTRY,AUR_GENDER,AUR_ADD1,AUR_CITY,AUR_STATE,AUR_STA_ID,AUR_GRADE)VALUES " & _
                    "('" & Replace(Trim(txtaurid.Text), "'", "''") & "','" & aurno & "','" & ddltitle.SelectedItem.Text & "','" & Replace(Trim(txtfname.Text), "'", "''") & "','" & Replace(Trim(txtmname.Text), "'", "''") & "', " & _
                     "'" & Replace(Trim(txtlname.Text), "'", "''") & "','" & knownas & "','" & Replace(Trim(txtmailid.Text), "'", "''") & "','NA','NA','" & ddlrepman.SelectedItem.Value & "'," & _
                     " '" & ddldg.SelectedItem.Value & "','" & Replace(Trim(txtext.Text), "'", "''") & "','" & Replace(Trim(txtphoff.Text), "'", "''") & "','" & Replace(Trim(txtphres.Text), "'", "''") & "','" & ddldesg.SelectedItem.Value & "'," & _
                        " '" & ddldept.SelectedItem.Value & "','N','" & dob & "','" & doj & "','" & Replace(Trim(txtcount.Text), "'", "''") & "'," & _
                        "'" & gender & "','" & Replace(Trim(txtaddress.Text), "'", "''") & "','" & Replace(Trim(txtcity.Text), "'", "''") & "','" & Replace(Trim(txtstate.Text), "'", "''") & "',1,'" & Replace(Trim(txtGrade.Text), "'", "''") & "') "

        SqlHelper.ExecuteNonQuery(CommandType.Text, str)

        str = "insert into " & HttpContext.Current.Session("TENANT") & "." & "[user](USR_ID,USR_LOGIN_PASSWORD,USR_NAME,USR_ACTIVE,USR_UPDATED_BY,USR_UPDATED_ON,USR_LOGGED) " & _
                "values('" & Replace(Trim(txtaurid.Text), "'", "''") & "','" & clsEDSecurity.Encrypt(Replace(Trim(txtp.Text), "'", "''")) & "','" & knownas & "','Y','" & userid & "','" & getoffsetdatetime(DateTime.Now) & "','N') "

        SqlHelper.ExecuteNonQuery(CommandType.Text, str)
        strSQL = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & "User_Role (Url_USR_Id,Url_Rol_Id,Url_ScopeMap_Id) VALUES('" & txtaurid.Text.Trim & "',1,21)"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)

    End Sub
    Public Sub insert_user(ByVal txtfname As String, ByVal txtmname As String, ByVal txtlname As String, ByVal txtaurid As String, ByVal txtpwd As String, ByVal ddltitle As String, ByVal txtmailid As String, ByVal ddlrepman As String, ByVal ddldg As String, ByVal txtaddress As String, ByVal ddlcity As String, ByVal txtp As String, ByVal txtGrade As String, ByVal page As Page, ByVal resno As String, ByVal desgnid As String, ByVal depid As String, ByVal country As String, ByVal ddltimezone As String, ByVal ddlcostcenter As String, ByVal ddlvertical As String)
        Try
            Dim userid As String = page.Session("uid")
            Dim knownas As String
            knownas = txtfname & " " & txtmname & " " & txtlname
            Dim valmax, aurno, gender As String
            valmax = txtaurid
            aurno = valmax

            If ddltitle = "Mr." Then
                gender = "Male"
                ' gender1 = "M"
            Else
                gender = "Female"
                ' gender1 = "F"
            End If

            Dim sp1 As New SqlParameter("@AUR_ID", SqlDbType.NVarChar)
            sp1.Value = txtaurid
            Dim sp2 As New SqlParameter("@AUR_NO", SqlDbType.NVarChar)
            sp2.Value = aurno
            Dim sp3 As New SqlParameter("@AUR_TITLE", SqlDbType.NVarChar)
            sp3.Value = ddltitle
            Dim sp4 As New SqlParameter("@AUR_FIRST_NAME", SqlDbType.NVarChar)
            sp4.Value = txtfname
            Dim sp5 As New SqlParameter("@AUR_MIDDLE_NAME", SqlDbType.NVarChar)
            sp5.Value = txtmname
            Dim sp6 As New SqlParameter("@AUR_LAST_NAME", SqlDbType.NVarChar)
            sp6.Value = txtlname
            Dim sp7 As New SqlParameter("@AUR_KNOWN_AS", SqlDbType.NVarChar)
            sp7.Value = knownas
            Dim sp8 As New SqlParameter("@AUR_EMAIL", SqlDbType.NVarChar)
            sp8.Value = txtmailid
            Dim sp9 As New SqlParameter("@AUR_REPORTING_TO", SqlDbType.NVarChar)
            If ddlrepman = "--Select--" Then
                sp9.Value = ""
            Else
                sp9.Value = ddlrepman
            End If

            Dim sp10 As New SqlParameter("@AUR_BDG_ID", SqlDbType.NVarChar)
            sp10.Value = ddldg
            Dim sp11 As New SqlParameter("@AUR_GENDER", SqlDbType.NVarChar)
            sp11.Value = gender
            Dim sp12 As New SqlParameter("@AUR_CITY", SqlDbType.NVarChar)
            sp12.Value = ddlcity
            Dim sp13 As New SqlParameter("@AUR_GRADE", SqlDbType.NVarChar)
            If txtGrade = "" Then
                sp13.Value = ""
            Else
                sp13.Value = txtGrade
            End If
            Dim sp14 As New SqlParameter("@USR_LOGIN_PASSWORD", SqlDbType.NVarChar)
            'sp14.Value = clsEDSecurity.Encrypt(Replace(Trim(txtp), "'", "''"))
            sp14.Value = txtpwd
            Dim sp15 As New SqlParameter("@USERID", SqlDbType.NVarChar)
            sp15.Value = userid

            Dim sp16 As New SqlParameter("@AUR_RES_NUMBER", SqlDbType.NVarChar)
            sp16.Value = resno
            Dim sp17 As New SqlParameter("@AUR_DESGN_ID", SqlDbType.NVarChar)
            If desgnid = "--Select--" Then
                sp17.Value = ""
            Else
                sp17.Value = desgnid
            End If

            Dim sp18 As New SqlParameter("@AUR_DEP_ID", SqlDbType.NVarChar)
            If depid = "--Select--" Then
                sp18.Value = ""
            Else
                sp18.Value = depid
            End If
            Dim sp19 As New SqlParameter("@AUR_COUNTRY", SqlDbType.NVarChar)
            sp19.Value = country
            'Dim sp20 As New SqlParameter("AUR_DOJ", SqlDbType.SmallDateTime)
            'sp20.Value = txtDOJ
            Dim sp20 As New SqlParameter("@AUR_TIME_ZONE", SqlDbType.NVarChar)
            sp20.Value = ddltimezone
            Dim sp21 As New SqlParameter("@AUR_VERT_CODE", SqlDbType.NVarChar)
            If ddlvertical = "--Select--" Then
                sp21.Value = ""
            Else
                sp21.Value = ddlvertical
            End If

            Dim sp22 As New SqlParameter("@AUR_PRJ_CODE", SqlDbType.NVarChar)
            If ddlcostcenter = "--Select--" Then
                sp22.Value = ""
            Else
                sp22.Value = ddlcostcenter
            End If
            Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12, sp13, sp14, sp15, sp16, sp17, sp18, sp19, sp20, sp21, sp22}
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_USER", parms)

        Catch ex As Exception

        End Try


    End Sub

    Public Function checkuser(ByVal txtaurid As String, ByVal page As Page) As Integer
        Dim sp As New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)
        sp.Value = txtaurid
        Return SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "USP_CHECK_USER", sp)
    End Function
    '**************************** EDIT USER EVERNTS *****************************************
    Public Sub bindemp(ByVal ddl As DropDownList, ByVal page As Page)
        Dim userid As String = page.Session("uid")
        strSQL = "SELECT USR_ID FROM " & HttpContext.Current.Session("TENANT") & "." & "[USER] where Usr_id = '" & userid & "' ORDER BY USR_ID ASC"
        BindCombo(strSQL, ddl, "usr_id", "usr_id")
    End Sub
    Public Sub bindemp(ByVal page As Integer, ByVal Uid As String)
        Dim userid As String = Uid
        strSQL = "SELECT USR_ID FROM " & HttpContext.Current.Session("TENANT") & "." & "[USER] where Usr_id = '" & userid & "' ORDER BY USR_ID ASC"
        'BindCombo(strSQL, ddl, "usr_id", "usr_id")
    End Sub
    Public Sub updateemp(ByVal cmbdg As DropDownList, ByVal txtEmpName As TextBox, ByVal cmbrepman As DropDownList, ByVal cmbAurId As DropDownList, ByVal page As Page, ByVal email As String)
        Dim valmax, aurno As String
        valmax = cmbAurId.SelectedItem.Value

        If Val(valmax) < 10 Then
            aurno = "000" & valmax
        ElseIf Val(valmax) < 100 And Val(valmax) >= 10 Then
            aurno = "00" & valmax
        ElseIf Val(valmax) < 1000 And Val(valmax) >= 100 Then
            aurno = "0" & valmax
        Else
            aurno = valmax
        End If

        Dim sp1 As New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)
        sp1.Value = cmbAurId.SelectedItem.Value
        Dim sp2 As New SqlParameter("@AUR_NO", SqlDbType.NVarChar, 50)
        sp2.Value = aurno
        Dim sp3 As New SqlParameter("@AUR_KNOWN_AS", SqlDbType.NVarChar, 50)
        sp3.Value = Replace(Trim(txtEmpName.Text), "'", "''")
        Dim sp4 As New SqlParameter("@AUR_EMAIL", SqlDbType.NVarChar, 50)
        sp4.Value = email
        Dim sp5 As New SqlParameter("@AUR_REPORTING_TO", SqlDbType.NVarChar, 50)
        sp5.Value = cmbrepman.SelectedItem.Value
        Dim sp6 As New SqlParameter("@AUR_BDG_ID", SqlDbType.NVarChar, 50)
        sp6.Value = cmbdg.SelectedItem.Value

        Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6}
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_UpdateUser", parms)
    End Sub
    Public Sub useridselectedindexchanged(ByVal mail As TextBox, ByVal txtEmpName As TextBox, ByVal cmbaurid As DropDownList, ByVal cmbrepman As DropDownList, ByVal txttoday As TextBox, ByVal cmbdg As DropDownList)
        Dim objData As SqlDataReader
        Dim str5 As String
        Dim str3 As String
        txttoday.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.ShortDate)

        Dim cnt As Integer
        cnt = 1
        'For year = 2001 To getoffsetdatetime(DateTime.Now).Year
        '    cmby2.Items.Insert(cnt, year)
        '    cnt = cnt + 1
        'Next
        'cnt = 1
        'For year = 1950 To getoffsetdatetime(DateTime.Now).Year - 15
        '    cmby1.Items.Insert(cnt, year)
        '    cnt = cnt + 1
        'Next

        strSQL = "select DISTINCT LCM_name,LCM_CODE from " & HttpContext.Current.Session("TENANT") & "." & "LOCATION ORDER BY LCM_NAME"
        BindCombo(strSQL, cmbdg, "LCM_name", "LCM_CODE")

        strSQL = " select distinct aur_id,AUR_KNOWN_AS from " & HttpContext.Current.Session("TENANT") & "." & "AMT_REPORTING_MNGERS_VW ORDER BY AUR_KNOWN_AS"
        BindCombo(strSQL, cmbrepman, "AUR_KNOWN_AS", "aur_id")


        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = cmbaurid.SelectedItem.Value
        objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getUserDetailsforEdit", sp1)
        While objData.Read

            'txtext.Text = objData("AUR_EXTENSION")
            ' txtphres.Text = objData("AUR_RES_NUMBER")
            txtEmpName.Text = objData("AUR_KNOWN_AS")
            mail.Text = objData("aur_email")
            cmbdg.SelectedIndex = cmbdg.Items.IndexOf(cmbdg.Items.FindByValue(objData("AUR_BDG_ID")))
            cmbrepman.SelectedIndex = cmbrepman.Items.IndexOf(cmbrepman.Items.FindByValue(objData("AUR_REPORTING_TO")))

            str3 = objData("AUR_TITLE")
            str5 = objData("AUR_DESGN_ID")
        End While

        objData.Close()

        strSQL = "select USR_CCC_CATEGORY from " & HttpContext.Current.Session("TENANT") & "." & "[User] where USR_ID='" & cmbaurid.SelectedItem.Value & "'"

        objData = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Do While objData.Read
        Loop
        objData.Close()
    End Sub


#Region "old grid row commands"


    Public Sub Country_Rowcommandold(ByVal gv As GridView, ByVal index As Integer)
        gv.Columns(2).Visible = True
        gv.Columns(3).Visible = True
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(2).Text.ToString()
        Select Case Trim(btn)
            Case "1"

                strSQL = "select count(*) from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_cny_id =''" & selectedRow.Cells(3).Text & "'' and lcm_sta_id='1'"

                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "Country set CNY_STA_ID=2 where CNY_CODE='" & selectedRow.Cells(3).Text & "'"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)

            Case "2"
                strSQL = "Update " & HttpContext.Current.Session("TENANT") & "." & "Country set CNY_STA_ID=1 where CNY_CODE='" & selectedRow.Cells(3).Text & "'"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            Case Else
        End Select
    End Sub
    Public Sub City_Rowcommandold(ByVal gv As GridView, ByVal index As Integer)
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(3).Text.ToString()
        gv.Columns(3).Visible = True
        gv.Columns(4).Visible = True
        Select Case Trim(btn)
            Case "1"
                strSQL = "select count(*) from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_cty_id='" & selectedRow.Cells(4).Text & "' and lcm_sta_id='1'"
                Dim iCount As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL))
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "City set CTY_STA_ID=2 where CTY_CODE='" & _
                         selectedRow.Cells(4).Text & "'"
            Case "2"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "City set CTY_STA_ID=1 where CTY_CODE='" & selectedRow.Cells(4).Text & "'"
            Case Else
        End Select
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
    End Sub

    Public Sub Location_Rowcommandold(ByVal gv As GridView, ByVal index As Integer)
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(9).Text.ToString()
        gv.Columns(9).Visible = True
        gv.Columns(10).Visible = True
        Select Case Trim(btn)
            Case "1"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "Location set LCM_STA_ID=2 where LCM_CODE='" & _
                         selectedRow.Cells(10).Text & "'"
            Case "2"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "Location set LCM_STA_ID=1 where LCM_CODE='" & selectedRow.Cells(10).Text & "'"
            Case Else
        End Select
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
    End Sub
    Public Sub Tower_Rowcommandold(ByVal gv As GridView, ByVal index As Integer)
        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(2).Text.ToString()
        gv.Columns(2).Visible = True
        gv.Columns(3).Visible = True
        Select Case Trim(btn)
            Case "1"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "TOWER set TWR_STA_ID=2 where TWR_CODE='" & _
                         selectedRow.Cells(3).Text & "'"
            Case "2"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "TOWER set TWR_STA_ID=1 where TWR_CODE='" & selectedRow.Cells(3).Text & "'"
            Case Else
        End Select
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
    End Sub
#End Region
    '**********************CostCenter Master Start************
    Dim dtCC As DataTable
    Public Sub costCenter_LoadGrid(ByVal gv As GridView)
        BindGrid("USP_GET_COSTCENTER", gv)
        'Make_Active_Inactive(gv, strSQL, 3)
    End Sub


    Public Sub BindCostCenter(ByVal ddl As DropDownList)
        'strSQL = "Select distinct PRJ_CODE,PRJ_NAME from " & HttpContext.Current.Session("TENANT") & "."  & "PROJECT where PRJ_STA_ID=1 and PRJ_VERTICAL in (select VER_code from " & HttpContext.Current.Session("TENANT") & "."  & "vertical) order by PRJ_NAME"
        BindCombo("USP_GET_COSTCENTER 2", ddl, "COST_CENTER_NAME", "COST_CENTER_CODE")
        'ddl.SelectedIndex = 0
    End Sub




    Public Sub CostCenter_SelectedIndex_Changed(ByVal ddlCC As String, ByVal ddlVertical As DropDownList, ByVal txtEmployee As TextBox, ByVal txtEmp As TextBox, ByVal txtDesc As TextBox, ByVal ddlBFMID As TextBox, ByVal ddlDH As TextBox)
        Try
            dtCC = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_COSTCENTER")
            Dim drProject() As DataRow = dtCC.Select("COST_CENTER_CODE='" & ddlCC & "'")
            If drProject.Length > 0 Then
                code = drProject(0)("COST_CENTER_CODE")
                name = drProject(0)("COST_CENTER_NAME")
                Vertical = drProject(0)("VER_NAME")

                txtDesc.Text = drProject(0)("VERTICAL_DESC").ToString().Trim()

                ddlBFMID.Text = drProject(0)("BFM_ID").ToString().Trim()
                ddlDH.Text = drProject(0)("DOMAIN_HEAD_ID").ToString().Trim()
                remarks = drProject(0)("REMARKS").ToString().Trim()
            Else
                code = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function Modifycostcenter(ByVal strCC_Code As String, ByVal strCC_Name As String, ByVal strprnt As String, ByVal strchld As String, ByVal strVCode As String, ByVal strBFM_ID As String, ByVal strDomainHeadID As String, ByVal strRemarks As String, ByVal I_Status As Int16, ByVal status As Integer, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        ' Try

        Dim spCOST_CENTER_CODE As New SqlParameter("@COST_CENTER_CODE", SqlDbType.NVarChar, 50)
        Dim spCOST_CENTER_NAME As New SqlParameter("@COST_CENTER_NAME", SqlDbType.NVarChar, 250)
        Dim spVERTICAL_CODE As New SqlParameter("@VERTICAL_CODE", SqlDbType.NVarChar, 50)
        Dim spBFM_ID As New SqlParameter("@BFM_ID", SqlDbType.NVarChar, 50)
        Dim spDOMAIN_HEAD_ID As New SqlParameter("@DOMAIN_HEAD_ID", SqlDbType.NVarChar, 50)
        Dim spCREATED_BY As New SqlParameter("@CREATED_BY", SqlDbType.NVarChar, 50)
        Dim spREMARKS As New SqlParameter("@REMARKS", SqlDbType.NVarChar, 500)
        Dim sp_Status As New SqlParameter("@STATUS", SqlDbType.Int)
        Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim SPCST_PE_CODE As New SqlParameter("@COST_PE_CODE", SqlDbType.NVarChar, 150)
        Dim SPCST_CH_CODE As New SqlParameter("@COST_CH_CODE", SqlDbType.NVarChar, 150)
        Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
        spi_Op.Direction = ParameterDirection.Output

        spCOST_CENTER_CODE.Value = strCC_Code
        spCOST_CENTER_NAME.Value = strCC_Name
        spVERTICAL_CODE.Value = strVCode
        spBFM_ID.Value = strBFM_ID
        spDOMAIN_HEAD_ID.Value = strDomainHeadID
        spCREATED_BY.Value = userid
        spREMARKS.Value = strRemarks
        sp_Status.Value = status
        spi_Status.Value = I_Status
        SPCST_PE_CODE.Value = strprnt
        SPCST_CH_CODE.Value = strchld

        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_MODIFY_COSTCENTER_TEST", spCOST_CENTER_CODE, spCOST_CENTER_NAME, spVERTICAL_CODE, spBFM_ID, spDOMAIN_HEAD_ID, spCREATED_BY, spREMARKS, sp_Status, spi_Status, SPCST_PE_CODE, SPCST_CH_CODE, spi_Op)

        'Catch ex As Exception
        'End Try
        Return spi_Op.Value
    End Function


    Public Function InsertModifyCostCenter(ByVal strCC_Code As String, ByVal strCC_Name As String, ByVal strprnt As String, ByVal strchld As String, ByVal strVCode As String, ByVal strBFM_ID As String, ByVal strDomainHeadID As String, ByVal strRemarks As String, ByVal I_Status As Int16, ByVal status As Integer, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        ' Try

        Dim spCOST_CENTER_CODE As New SqlParameter("@COST_CENTER_CODE", SqlDbType.NVarChar, 50)
        Dim spCOST_CENTER_NAME As New SqlParameter("@COST_CENTER_NAME", SqlDbType.NVarChar, 250)
        Dim spVERTICAL_CODE As New SqlParameter("@VERTICAL_CODE", SqlDbType.NVarChar, 50)
        Dim spBFM_ID As New SqlParameter("@BFM_ID", SqlDbType.NVarChar, 50)
        Dim spDOMAIN_HEAD_ID As New SqlParameter("@DOMAIN_HEAD_ID", SqlDbType.NVarChar, 50)
        Dim spCREATED_BY As New SqlParameter("@CREATED_BY", SqlDbType.NVarChar, 50)
        Dim spREMARKS As New SqlParameter("@REMARKS", SqlDbType.NVarChar, 500)
        Dim sp_Status As New SqlParameter("@STATUS", SqlDbType.Int)
        Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim SPCOST_PE_CODE As New SqlParameter("@COST_PE_CODE", SqlDbType.NVarChar, 150)
        Dim SPCOST_CH_CODE As New SqlParameter("@COST_CHE_CODE", SqlDbType.NVarChar, 150)
        Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
        spi_Op.Direction = ParameterDirection.Output

        spCOST_CENTER_CODE.Value = strCC_Code
        spCOST_CENTER_NAME.Value = strCC_Name
        spVERTICAL_CODE.Value = strVCode
        spBFM_ID.Value = strBFM_ID
        spDOMAIN_HEAD_ID.Value = strDomainHeadID
        spCREATED_BY.Value = userid
        spREMARKS.Value = strRemarks
        sp_Status.Value = status
        spi_Status.Value = I_Status
        SPCOST_PE_CODE.Value = strprnt
        SPCOST_CH_CODE.Value = strchld

        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_COSTCENTER_TEST", spCOST_CENTER_CODE, spCOST_CENTER_NAME, spVERTICAL_CODE, spBFM_ID, spDOMAIN_HEAD_ID, spCREATED_BY, spREMARKS, sp_Status, spi_Status, SPCOST_PE_CODE, SPCOST_CH_CODE, spi_Op)
        Return spi_Op.Value
        'Catch ex As Exception
        'End Try
        Return spi_Op.Value
    End Function
    '**********************CostCenter Master End************

    ' ****************************  Project Master Events *****************************************
    Public Shared dtProject As DataTable
    Public Sub BindProject(ByVal ddl As DropDownList)
        'strSQL = "Select distinct PRJ_CODE,PRJ_NAME from " & HttpContext.Current.Session("TENANT") & "."  & "PROJECT where PRJ_STA_ID=1 and PRJ_VERTICAL in (select VER_code from " & HttpContext.Current.Session("TENANT") & "."  & "vertical) order by PRJ_NAME"
        Dim sp1 As SqlParameter = New SqlParameter("@I_STATUS", SqlDbType.Int)
        sp1.Value = 2
        BindCombo("USP_GET_PROJECTS", ddl, "PRJ_NAME", "PRJ_CODE", sp1)
        'ddl.SelectedIndex = 0
    End Sub

    Public Sub project_LoadGrid(ByVal gv As GridView)

        BindGrid("USP_GET_PROJECTS", gv)
        'Make_Active_Inactive(gv, strSQL, 3)
    End Sub

    Public Function project_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer


        Dim selectedRow As GridViewRow = gv.Rows(index)
        Dim btn As String = selectedRow.Cells(3).Text.ToString()
        gv.Columns(3).Visible = True
        gv.Columns(4).Visible = True
        Select Case Trim(btn)
            Case "1"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "PROJECT set PRJ_STA_ID=2 where PRJ_CODE='" & _
                          selectedRow.Cells(4).Text & "'"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            Case "2"
                strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "PROJECT set PRJ_STA_ID=1 where PRJ_CODE='" & selectedRow.Cells(4).Text & "'"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
                Return 1
            Case Else
                Return 0
        End Select

    End Function

    Public Function InsertProject(ByVal ddl As DropDownList, ByVal strEmp As String, ByVal strCC As String, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")
        Try
            Dim spPRJ_CODE As New SqlParameter("@PRJ_CODE", SqlDbType.NVarChar, 50)
            Dim spPRJ_NAME As New SqlParameter("@PRJ_NAME", SqlDbType.NVarChar, 250)
            Dim spPRJ_VERTICAL As New SqlParameter("@PRJ_VERTICAL", SqlDbType.NVarChar, 500)
            Dim spPRJ_UPT_BY As New SqlParameter("@PRJ_UPT_BY", SqlDbType.NVarChar, 50)
            Dim spPRJ_REMARKS As New SqlParameter("@PRJ_REMARKS", SqlDbType.NVarChar, 500)
            Dim spCC As New SqlParameter("@VC_CC", SqlDbType.NVarChar, 50)

            Dim spPRJ_HEAD As New SqlParameter("@PRJ_HEAD", SqlDbType.NVarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)

            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output
            spPRJ_CODE.Value = code
            spPRJ_NAME.Value = name
            spPRJ_REMARKS.Value = remarks.Replace("'", "''")
            spCC.Value = strCC
            spPRJ_UPT_BY.Value = userid
            spi_Status.Value = 1
            spPRJ_VERTICAL.Value = ddl.SelectedValue
            spPRJ_HEAD.Value = strEmp
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_PROJECT", spPRJ_CODE, spPRJ_NAME, spPRJ_VERTICAL, spPRJ_UPT_BY, spPRJ_REMARKS, spPRJ_HEAD, spCC, spi_Status, spi_Op)
            Return spi_Op.Value

        Catch ex As Exception

        End Try


    End Function

    Public Sub project_SelectedIndex_Changed(ByVal ddl As DropDownList, ByVal ddl1 As DropDownList, ByVal txtEmp As TextBox, ByVal ddlCC As DropDownList)
        Try
            dtProject = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_PROJECTS")
            Dim drProject() As DataRow = dtProject.Select("prj_CODE='" & ddl.SelectedItem.Value & "'")
            If drProject.Length > 0 Then
                code = drProject(0)("PRJ_CODE")
                name = drProject(0)("PRJ_NAME")
                ddl1.SelectedIndex = ddl1.Items.IndexOf(ddl1.Items.FindByValue(drProject(0)("PRJ_VERTICAL").ToString().Trim()))
                Dim spVerticalCode As New SqlParameter("@VERTICAL_CODE", SqlDbType.NVarChar, 200)
                spVerticalCode.Value = ddl1.SelectedValue
                Dim dtCC As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_COSTCENTER_PROJECT", spVerticalCode)
                ddlCC.DataSource = dtCC
                ddlCC.DataTextField = "COST_CENTER_NAME"
                ddlCC.DataValueField = "COST_CENTER_CODE"
                ddlCC.DataBind()
                ddlCC.Items.Insert(0, "--Select--")
                ddlCC.SelectedIndex = ddlCC.Items.IndexOf(ddlCC.Items.FindByValue(drProject(0)("PRJ_COST_CENTER_CODE").ToString().Trim()))
                'ddl2.SelectedIndex = ddl2.Items.IndexOf(ddl2.Items.FindByValue(drProject(0)("PRJ_HEAD").ToString().Trim()))
                txtEmp.Text = drProject(0)("PRJ_HEAD").ToString().Trim()
                remarks = drProject(0)("PRJ_REMARKS").ToString().Trim()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub project_SelectedIndex_Changed(ByVal strProjCode As String, ByVal ddl1 As DropDownList, ByVal txtEmp As TextBox, ByVal ddlCC As DropDownList, ByVal ddl2 As TextBox)
        Try
            dtProject = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_PROJECTS")
            Dim drProject() As DataRow = dtProject.Select("prj_CODE='" & strProjCode & "'")
            If drProject.Length > 0 Then
                code = drProject(0)("PRJ_CODE")
                name = drProject(0)("PRJ_NAME")
                ddl1.SelectedIndex = ddl1.Items.IndexOf(ddl1.Items.FindByValue(drProject(0)("PRJ_VERTICAL").ToString().Trim()))
                Dim spVerticalCode As New SqlParameter("@VERTICAL_CODE", SqlDbType.NVarChar, 200)
                spVerticalCode.Value = ddl1.SelectedValue
                Dim dtCC As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_COSTCENTER_PROJECT", spVerticalCode)
                ddlCC.DataSource = dtCC
                ddlCC.DataTextField = "COST_CENTER_NAME"
                ddlCC.DataValueField = "COST_CENTER_CODE"
                ddlCC.DataBind()
                ddlCC.Items.Insert(0, "--Select--")
                ddlCC.SelectedIndex = ddlCC.Items.IndexOf(ddlCC.Items.FindByValue(drProject(0)("PRJ_COST_CENTER_CODE").ToString().Trim()))
                ' ddl2.SelectedIndex = ddl2.Items.IndexOf(ddl2.Items.FindByValue(drProject(0)("PRJ_HEAD").ToString().Trim()))
                ddl2.Text = drProject(0)("PRJ_HEAD").ToString().Trim()
                remarks = drProject(0)("PRJ_REMARKS").ToString().Trim()
            Else
                code = ""
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function ModifyProject(ByVal ddlVertical As DropDownList, ByVal strEmp As String, ByVal strCC As String, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")


        Try
            Dim spPRJ_CODE As New SqlParameter("@PRJ_CODE", SqlDbType.NVarChar, 50)
            Dim spPRJ_NAME As New SqlParameter("@PRJ_NAME", SqlDbType.NVarChar, 250)
            Dim spPRJ_VERTICAL As New SqlParameter("@PRJ_VERTICAL", SqlDbType.NVarChar, 500)
            Dim spPRJ_UPT_BY As New SqlParameter("@PRJ_UPT_BY", SqlDbType.NVarChar, 50)
            Dim spPRJ_REMARKS As New SqlParameter("@PRJ_REMARKS", SqlDbType.NVarChar, 500)
            Dim spPRJ_HEAD As New SqlParameter("@PRJ_HEAD", SqlDbType.NVarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)
            Dim spCC As New SqlParameter("@VC_CC", SqlDbType.NVarChar, 50)
            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output
            spPRJ_CODE.Value = code
            spPRJ_NAME.Value = name
            spPRJ_REMARKS.Value = remarks.Replace("'", "''")
            spPRJ_UPT_BY.Value = userid
            spi_Status.Value = 2
            spPRJ_VERTICAL.Value = ddlVertical.SelectedValue
            spPRJ_HEAD.Value = strEmp
            spCC.Value = strCC
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_PROJECT", spPRJ_CODE, spPRJ_NAME, spPRJ_VERTICAL, spPRJ_UPT_BY, spPRJ_REMARKS, spPRJ_HEAD, spCC, spi_Status, spi_Op)
            Return spi_Op.Value

        Catch ex As Exception

        End Try


    End Function


    '*********************************  Designition Master Events ************************
    Public Shared dtDesignation As DataTable

    Public Sub Desig_LoadGrid(ByVal gv As GridView)

        dtDesignation = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_DESIGNATION")
        gv.DataSource = dtDesignation
        gv.DataBind()
    End Sub

    Public Function Desig_Rowcommand(ByVal intStatus As Integer, ByVal strDesg As String) As Integer

        Try
            Dim spStatus As New SqlParameter("@STA_ID", SqlDbType.SmallInt)
            Dim spDesgCode As New SqlParameter("@DSN_CODE", SqlDbType.NVarChar, 50)
            If intStatus = 1 Then
                intStatus = 2
            ElseIf intStatus = 2 Then
                intStatus = 1
            End If
            spStatus.Value = intStatus
            spDesgCode.Value = strDesg
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_DESIGNATION_STATUS", spStatus, spDesgCode)

        Catch ex As Exception

        End Try


    End Function

    Public Sub BindDesig1(ByVal ddl As DropDownList)
        Dim sp As New SqlParameter("@I_STATUS", SqlDbType.SmallInt)
        sp.Value = 1
        dtDesignation = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_DESIGNATION", sp)
        sp.Value = 2
        BindCombo("USP_GET_DESIGNATION", ddl, "DSN_AMT_TITLE", "DSN_CODE", sp)
        ddl.SelectedIndex = 0
    End Sub
    Public Function InsertDesig(ByVal page As Page, ByVal intSta As Integer) As Integer
        Try
            Dim userid As String = page.Session("uid")
            '@DSN_CODE AS NVARCHAR(50),    
            '@DSN_AMT_TITLE AS NVARCHAR(250),    
            '@DSN_REM AS NVARCHAR(500), 
            '@DSN_UPT_BY AS NVARCHAR(50),    
            '@I_STATUS AS INT,    
            '@I_OP AS INT OUTPUT 
            Dim spDesg_code As New SqlParameter("@DSN_CODE", SqlDbType.NVarChar, 50)
            Dim spDesg_DESCRIPTION As New SqlParameter("@DSN_AMT_TITLE", SqlDbType.NVarChar, 250)
            Dim spDesg_band_code As New SqlParameter("@DSN_BAND_CODE", SqlDbType.NVarChar, 250)
            Dim spDesg_REMARKS As New SqlParameter("@DSN_REM", SqlDbType.NVarChar, 500)
            Dim spDesg_UPDATED_BY As New SqlParameter("@DSN_UPT_BY", SqlDbType.NVarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)

            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output

            spDesg_code.Value = code
            spDesg_DESCRIPTION.Value = name
            spDesg_band_code.Value = bandcode
            spDesg_REMARKS.Value = remarks.Replace("'", "''")
            spDesg_UPDATED_BY.Value = userid
            spi_Status.Value = intSta
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_DESIGNATION", spDesg_code, spDesg_DESCRIPTION, spDesg_band_code, spDesg_REMARKS, spDesg_UPDATED_BY, spi_Status, spi_Op)
            Return spi_Op.Value

        Catch ex As Exception

        End Try




    End Function

    Public Function ModifyDesig(ByVal ddl As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "designation set DSN_AMT_TITLE='" & name & "',DSN_REM='" & remarks.Replace("'", "''") & "',DSN_UPT_BY='" & userid & "',DSN_UPT_DT='" & getoffsetdatetime(DateTime.Now) & "' where DSN_CODE='" & ddl.SelectedItem.Value & "'"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        PopUpMessage("Record has been modified", page)
        Return 1

    End Function

    Public Sub Desig_SelectedIndex_Changed(ByVal ddl As DropDownList)
        Try
            Dim drDesg() As DataRow = dtDesignation.Select("DSN_CODE='" & ddl.SelectedItem.Value & "'")
            If drDesg.Length > 0 Then
                code = drDesg(0)("DSN_CODE").ToString().Trim()
                name = drDesg(0)("DSN_AMT_TITLE").ToString().Trim()
                remarks = drDesg(0)("DSN_REM").ToString().Trim()
                bandcode = drDesg(0)("DSN_BAND_CODE").ToString().Trim()
                'ddlBand.SelectedIndex = ddlBand.Items.IndexOf(ddlBand.Items.FindByValue(bandcode))
            End If
        Catch ex As Exception
        Finally
            'ObjDR.Close()
        End Try
    End Sub

    '**************************  Adding New Role Events **********************************
    Public Shared dvRole As DataView
    Public Shared dtRole As DataTable


    Public Sub Role_LoadGrid(ByVal gv As GridView)
        ' strSQL = "select DISTINCT ROL_ACRONYM,ROL_DESCRIPTION,ROL_STA_ID from " & HttpContext.Current.Session("TENANT") & "."  & "ROLE order by ROL_DESCRIPTION"
        'strSQL = "SELECT DISTINCT ROL_ID,convert(nvarchar,ROL_DESCRIPTION)ROL_DESCRIPTION,ROL_ACRONYM,ROL_STA_ID,(select isnull(count(url_rol_id),0) " & _
        '         "from " & HttpContext.Current.Session("TENANT") & "."  & "user_role where url_rol_id= ROL_ID )as Count FROM " & HttpContext.Current.Session("TENANT") & "."  & "ROLE WHERE ROL_ACTIVE = 'Y'"

        BindGrid("USP_GET_ROLES", gv)
    End Sub

    Public Sub BindRole(ByVal ddl As DropDownList)
        Dim sp As New SqlParameter("@i_Status", SqlDbType.SmallInt)
        sp.Value = 1
        dtRole = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_ROLES", sp)
        ddl.DataSource = dtRole
        ddl.DataTextField = "rol_description"
        ddl.DataValueField = "rol_acronym"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select--")
        ddl.SelectedIndex = 0
    End Sub

    Public Function Role_Rowcommand(ByVal gv As GridView, ByVal index As Integer) As Integer
        Try
            gv.Columns(2).Visible = True
            gv.Columns(3).Visible = True
            Dim selectedRow As GridViewRow = gv.Rows(index)
            Dim btn As String = selectedRow.Cells(2).Text.ToString()
            Dim spStatusID As New SqlParameter("@i_staID", SqlDbType.SmallInt)
            Dim spStatus As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 5)
            Dim spRole As New SqlParameter("@@vc_Role", SqlDbType.NVarChar, 50)
            spRole.Value = selectedRow.Cells(3).Text.Trim()

            Select Case Trim(btn)
                Case "1"
                    spStatusID.Value = 2
                    spStatus.Value = "N"
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ROLE_STATUS", spStatusID, spStatus, spRole)
                    Return 1
                Case "2"
                    spStatusID.Value = 1
                    spStatus.Value = "Y"
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ROLE_STATUS", spStatusID, spStatus, spRole)
                    Return 1
                Case Else
                    Return 0
            End Select
        Catch ex As Exception

        End Try


    End Function

    Public Sub role_SelectedIndex_Changed(ByVal ddl As DropDownList)
        Try
            Dim drRole() As DataRow = dtRole.Select("ROL_ACRONYM='" & ddl.SelectedItem.Value & "'")
            If drRole.Length > 0 Then
                code = drRole(0)("rol_acronym")
                name = drRole(0)("rol_description")
                remarks = drRole(0)("rol_remarks")
                ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(drRole(0)("rol_acronym")))
            End If
        Catch ex As Exception
        Finally
            'ObjDR.Close()
        End Try
    End Sub

    Public Function InsertRole(ByVal page As Page) As Integer
        Try
            Dim userid As String = page.Session("uid")
            '@ROL_ACRONYM as nvarchar(50),
            '@ROL_DESCRIPTION as nvarchar(250),
            '@ROL_REMARKS as nvarchar(500),
            '@ROL_UPDATED_BY as nvarchar(50),
            '@i_Status as int,
            '@i_Op as int out
            Dim spROL_ACRONYM As New SqlParameter("@ROL_ACRONYM", SqlDbType.NVarChar, 50)
            Dim spROL_DESCRIPTION As New SqlParameter("@ROL_DESCRIPTION", SqlDbType.NVarChar, 250)
            Dim spROL_REMARKS As New SqlParameter("@ROL_REMARKS", SqlDbType.NVarChar, 500)
            Dim spROL_UPDATED_BY As New SqlParameter("@ROL_UPDATED_BY", SqlDbType.NVarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)

            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output

            spROL_ACRONYM.Value = code
            spROL_DESCRIPTION.Value = name
            spROL_REMARKS.Value = remarks.Replace("'", "''")
            spROL_UPDATED_BY.Value = userid
            spi_Status.Value = 1
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_ROLE", spROL_ACRONYM, spROL_DESCRIPTION, spROL_REMARKS, spROL_UPDATED_BY, spi_Status, spi_Op)
            Return spi_Op.Value

        Catch ex As Exception

        End Try


    End Function

    Public Function ModifyRole(ByVal ddl As DropDownList, ByVal page As Page) As Integer
        Try
            Dim userid As String = page.Session("uid")
            '@ROL_ACRONYM as nvarchar(50),
            '@ROL_DESCRIPTION as nvarchar(250),
            '@ROL_REMARKS as nvarchar(500),
            '@ROL_UPDATED_BY as nvarchar(50),
            '@i_Status as int,
            '@i_Op as int out
            Dim spROL_ACRONYM As New SqlParameter("@ROL_ACRONYM", SqlDbType.NVarChar, 50)
            Dim spROL_DESCRIPTION As New SqlParameter("@ROL_DESCRIPTION", SqlDbType.NVarChar, 250)
            Dim spROL_REMARKS As New SqlParameter("@ROL_REMARKS", SqlDbType.NVarChar, 500)
            Dim spROL_UPDATED_BY As New SqlParameter("@ROL_UPDATED_BY", SqlDbType.NVarChar, 50)
            Dim spi_Status As New SqlParameter("@i_Status", SqlDbType.Int)
            Dim spi_Op As New SqlParameter("@i_Op", SqlDbType.Int)
            spi_Op.Direction = ParameterDirection.Output

            spROL_ACRONYM.Value = ddl.SelectedValue.ToString().Trim()
            spROL_DESCRIPTION.Value = name
            spROL_REMARKS.Value = remarks.Replace("'", "''")
            spROL_UPDATED_BY.Value = userid
            spi_Status.Value = 2
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_ROLE", spROL_ACRONYM, spROL_DESCRIPTION, spROL_REMARKS, spROL_UPDATED_BY, spi_Status, spi_Op)
            Return spi_Op.Value
        Catch ex As Exception
        End Try

        'Dim userid As String = page.Session("uid")
        'strSQL = "update " & HttpContext.Current.Session("TENANT") & "."  & "role set rol_description='" & name & "',rol_remarks='" & remarks.Replace("'", "''") & "',ROL_UPDATED_BY='" & userid & "',ROL_UPDATED_ON='" & getoffsetdatetime(DateTime.Now) & "' where rol_acronym='" & ddl.SelectedItem.Value & "'"
        'SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        'PopUpMessage("Record has been modified", page)
        'Return 1

    End Function
#Region " Status "
    Public Shared Function GetStatus(Optional ByVal strWhere As String = "") As DataTable
        Try
            strSQL = "SELECT STA_ID, STA_TITLE FROM STATUS "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere

            dtResult = SqlHelper.ExecuteDatatable(CommandType.Text, strSQL)
            Return dtResult

        Catch ex As Exception
            Dim dt As New DataTable
            Return dt
        End Try
    End Function
#End Region

    Sub Conftype_SelectedIndex_Changed(dropDownList As DropDownList)

    End Sub


    '****************************************  LOG EVENTS ****************************
    Public Sub Insert_Log(ByVal name As String, ByVal page As String, ByVal user As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOG_NAME", name)
        param(1) = New SqlParameter("@LOG_PAGE", page)
        param(2) = New SqlParameter("@LOG_USER", user)
        Dim LOG_NAME As New SqlParameter("@LOG_NAME", SqlDbType.NVarChar, 50)
        Dim LOG_PAGE As New SqlParameter("@LOG_PAGE", SqlDbType.NVarChar, 50)
        Dim LOG_USER As New SqlParameter("@LOG_USER", SqlDbType.NVarChar, 50)
        LOG_NAME.Value = name
        LOG_PAGE.Value = page
        LOG_USER.Value = user
        SqlHelper.ExecuteNonQuery("INSERT_LOG", LOG_NAME, LOG_PAGE, LOG_USER)
    End Sub
    Public Shared Function GetUserSession(ByVal UserName As String) As Integer
        Dim Status As Integer = 0
        Dim dr As SqlDataReader

        Try

            Dim param As SqlParameter() = New SqlParameter(2) {}
            param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
            param(0).Value = UserName
            param(1) = New SqlParameter("@TIMEOUT", SqlDbType.NVarChar)
            param(1).Value = AppSettings("TIMEOUT").ToString()
            param(2) = New SqlParameter("@LoginUniqueID", SqlDbType.NVarChar)
            param(2).Value = HttpContext.Current.Session("LoginUniqueID")
            dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_USER_SESSION", param)
            If (dr.Read()) Then
                Status = Convert.ToInt32(dr("FLAG"))
            Else
                Status = -2
            End If

            dr.Close()
            dr = Nothing

        Catch ex As Exception
        Finally

        End Try

        Return Status
    End Function
    Public Shared Function doLogout() As String
        Try

            HttpContext.Current.Session.Abandon()
            HttpContext.Current.Session.Clear()
            HttpContext.Current.Response.Redirect("~/login.aspx")

            Return "A"
        Catch ex As Exception

            Return "Error"
        End Try
    End Function
End Class
