Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System

Public Class clsExtenedRelease
    Dim strSQL As String = String.Empty
    Public Sub bindData(ByVal strTower As String, ByVal gv As GridView, ByVal intStatusID As Int16)
        strSQL = "usp_bindTowerDetailsforextension '" & strTower & "','" & intStatusID & "'"
        BindGrid(strSQL, gv)
    End Sub
    Public Sub bindData(ByVal strBuilding As String, ByVal gv As GridView, ByVal strTowerId As String, ByVal strFlrId As String, ByVal intStatusID As Int16)
        Dim sp1 As New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 50)
        sp1.Value = strBuilding
        Dim sp2 As New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50)
        sp2.Value = strTowerId
        Dim sp3 As New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50)
        sp3.Value = strFlrId
        Dim sp4 As New SqlParameter("@I_STATUS", SqlDbType.Int)
        sp4.Value = intStatusID
        Dim DS As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_bindTowerDetailsforPrjexten", sp1, sp2, sp3, sp4)

        gv.DataSource = DS
        gv.DataBind()

    End Sub
    Public Sub bindData(ByVal strBuilding As String, ByVal gv As GridView, ByVal strTowerId As String, ByVal strFlrId As String, ByVal strWngId As String, ByVal Vertical As String, ByVal intStatusID As Int16)
        Dim sp1 As New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 50)
        sp1.Value = strBuilding
        Dim sp2 As New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50)
        sp2.Value = strTowerId
        Dim sp3 As New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50)
        sp3.Value = strFlrId
        Dim sp4 As New SqlParameter("@WNG_ID", SqlDbType.NVarChar, 50)
        sp4.Value = strWngId
        Dim sp5 As New SqlParameter("@VERT", SqlDbType.NVarChar, 200)
        sp5.Value = Vertical

        Dim sp6 As New SqlParameter("@I_STATUS", SqlDbType.Int)
        sp6.Value = intStatusID
        Dim DS As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_BINDTOWERDETAILSFOREXTENSION", sp1, sp2, sp3, sp4, sp5, sp6)

        gv.DataSource = DS
        gv.DataBind()


    End Sub


    Public Sub bindDataForRelease(ByVal strTower As String, ByVal gv As GridView, ByVal intStatusID As String, ByVal strLocation As String)

        Dim spLocation As New SqlParameter("@VC_LOCATION", SqlDbType.NVarChar, 50)
        Dim spTower As New SqlParameter("@VC_TOWER", SqlDbType.NVarChar, 50)
        Dim spStatus As New SqlParameter("@VC_STATUS", SqlDbType.NVarChar, 50)

        spLocation.Value = strLocation
        spTower.Value = strTower
        spStatus.Value = intStatusID

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETSPACE_TO_RELEASE", spLocation, spTower, spStatus)
        gv.DataSource = dt
        gv.DataBind()
    End Sub
#Region "Update the Status of the Project wise Space "
    Public Sub updateSpace(ByVal strSpaceID As String, ByVal strRemarks As String, ByVal strReqID As String, ByVal dtExt As Date, ByVal AurID As String)

        Dim sp1 As New SqlParameter("@vc_spcId", SqlDbType.NVarChar, 50)
        sp1.Value = strSpaceID

        Dim sp2 As New SqlParameter("@vc_reqId", SqlDbType.NVarChar, 50)
        sp2.Value = strReqID
        Dim sp3 As New SqlParameter("@vc_dtExtened", SqlDbType.DateTime)
        sp3.Value = dtExt
        Dim sp4 As New SqlParameter("@vc_Remarks", SqlDbType.NVarChar, 500)
        sp4.Value = strRemarks
        Dim sp5 As New SqlParameter("@VC_AURID", SqlDbType.NVarChar, 50)
        sp5.Value = AurID
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updateExtensionDetails", sp1, sp2, sp3, sp4, sp5)
    End Sub
#End Region

#Region "Update the Status of the Project EXTENSION "
    Public Sub updateProject(ByVal strRemarks As String, ByVal strReqID As String, ByVal dtExt As Date, ByVal ddlloc As String, ByVal ddlfloor As String)
        Dim sp1 As New SqlParameter("@vc_reqId", SqlDbType.NVarChar, 50)
        sp1.Value = strReqID
        Dim sp2 As New SqlParameter("@vc_dtExtened", SqlDbType.DateTime)
        sp2.Value = dtExt
        Dim sp3 As New SqlParameter("@vc_Remarks", SqlDbType.NVarChar, 500)
        sp3.Value = strRemarks
        Dim sp4 As New SqlParameter("@vc_loc", SqlDbType.NVarChar, 500)
        sp4.Value = ddlloc
        Dim sp5 As New SqlParameter("@vc_floor", SqlDbType.NVarChar, 500)
        sp5.Value = ddlfloor
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updatePrjExtenDetails", sp1, sp2, sp3, sp4, sp5)
    End Sub

    Public Sub updateProjectExtensionApprove(ByVal strRemarks As String, ByVal dtExtendDate As Date, ByVal strReqID As String, ByVal intStatusID As Integer, ByVal ddlfloor As String)
        Dim sp1 As New SqlParameter("@vc_status", SqlDbType.NVarChar, 50)
        Dim sp2 As New SqlParameter("@vc_Verticalid", SqlDbType.NVarChar, 50)
        Dim sp3 As New SqlParameter("@vc_floor", SqlDbType.NVarChar, 50)
        sp1.Value = intStatusID
        sp2.Value = strReqID
        sp3.Value = ddlfloor
        strSQL = "usp_updateProjectAllocation"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL, sp1, sp2, sp3)
    End Sub

#End Region
    Public Sub getProjectidforExtension(ByVal gv As GridView, ByVal strTower As String, ByVal strfloor As String)
        Dim paramVC_TOWER As SqlParameter = New SqlParameter("@VC_TOWER", SqlDbType.NVarChar, 50)
        Dim paramvc_floor As SqlParameter = New SqlParameter("@vc_floor", SqlDbType.NVarChar, 50)
        paramVC_TOWER.Value = strTower
        paramvc_floor.Value = strfloor
        strSQL = "usp_getTowersforPRjExtenApporval"
        BindGrid(strSQL, gv, paramVC_TOWER, paramvc_floor)
    End Sub
    Public Sub getSpaceIDforExtension(ByVal gv As GridView, ByVal strTower As String)
        Dim paramVC_TOWER As SqlParameter = New SqlParameter("@VC_TOWER", SqlDbType.NVarChar, 50)
        paramVC_TOWER.Value = strTower
        strSQL = "usp_getTowersforExtensionApporval"
        BindGrid(strSQL, gv, paramVC_TOWER)
    End Sub
#Region "Update the Status of the Project wise Space "
    Public Sub updateSpaceExtensionApprove(ByVal strSpaceID As String, ByVal strRemarks As String, ByVal dtExtendDate As Date, ByVal strReqID As String, ByVal intStatusID As Integer)
        Dim paramvc_status As SqlParameter = New SqlParameter("@vc_status", SqlDbType.NVarChar, 50)
        Dim paramvc_SpaceID As SqlParameter = New SqlParameter("@vc_SpaceID", SqlDbType.NVarChar, 50)
        Dim paramREQ_ID As SqlParameter = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 50)
        paramvc_status.Value = intStatusID
        paramvc_SpaceID.Value = strSpaceID
        paramREQ_ID.Value = strReqID
        strSQL = "usp_updateSpaceAllocatioforPrj"
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, strSQL, paramvc_status, paramvc_SpaceID, paramREQ_ID)
    End Sub
#End Region
#Region " Bind the grid with details of Spaces which have to be released"
    Public Sub bindData1(ByVal strVert As String, ByVal strTower As String, ByVal strFloor As String, ByVal gv As GridView, ByVal intStatusID As Int16, ByVal strWing As String)
        Dim spVertId As New SqlParameter("@VC_VertID", SqlDbType.NVarChar, 50)
        Dim spTowerId As New SqlParameter("@VC_TOWERID", SqlDbType.NVarChar, 50)
        Dim spFloorId As New SqlParameter("@VC_FloorID", SqlDbType.NVarChar, 50)
        Dim spStatusID As New SqlParameter("@I_STATUSID", SqlDbType.Int)
        Dim spWngId As New SqlParameter("@VC_WingID", SqlDbType.NVarChar, 50)
        Dim strsql As String
        spVertId.Value = strVert
        spTowerId.Value = strTower
        spFloorId.Value = strFloor
        spStatusID.Value = intStatusID
        spWngId.Value = strWing
        strsql = "USP_GET_SPACE_TO_RELEASE_VERTICAL"
        BindGrid(strsql, gv, spVertId, spTowerId, spFloorId, spStatusID, spWngId)
    End Sub
#End Region
End Class
