Imports Microsoft.VisualBasic

Public Class GSCMaster

    Private mGSC_ID As Integer
    Private mGSC_CODE As String
    Private mGSC_NAME As String
    Private mGSC_REM As String
    Private mGSC_LCM_ID As String
    Private mGSC_STA_ID As String
    Private mGSC_UPT_DT As String
    Private mGSC_UPT_BY As String
    Private mGSC_Active_Inactive_link As String


    Public Property GSC_ID() As Integer
        Get
            Return mGSC_ID
        End Get
        Set(ByVal value As Integer)
            mGSC_ID = value
        End Set
    End Property

    Public Property GSC_CODE() As String
        Get
            Return mGSC_CODE
        End Get
        Set(ByVal value As String)
            mGSC_CODE = value
        End Set
    End Property

    Public Property GSC_NAME() As String
        Get
            Return mGSC_NAME
        End Get
        Set(ByVal value As String)
            mGSC_NAME = value
        End Set
    End Property


    Public Property GSC_REM() As String
        Get
            Return mGSC_REM
        End Get
        Set(ByVal value As String)
            mGSC_REM = value
        End Set
    End Property


    Public Property GSC_LCM_ID() As String
        Get
            Return mGSC_LCM_ID
        End Get
        Set(ByVal value As String)
            mGSC_LCM_ID = value
        End Set
    End Property

    Public Property GSC_STA_ID() As String
        Get
            Return mGSC_STA_ID
        End Get
        Set(ByVal value As String)
            mGSC_STA_ID = value
        End Set
    End Property

    Public Property GSC_UPT_DT() As String
        Get
            Return mGSC_UPT_DT
        End Get
        Set(ByVal value As String)
            mGSC_UPT_DT = value
        End Set
    End Property

    Public Property GSC_UPT_BY() As String
        Get
            Return mGSC_UPT_BY
        End Get
        Set(ByVal value As String)
            mGSC_UPT_BY = value
        End Set
    End Property

    Public Property GSC_Active_Inactive_link() As String
        Get
            Return mGSC_Active_Inactive_link
        End Get
        Set(ByVal value As String)
            mGSC_Active_Inactive_link = value
        End Set
    End Property
End Class
