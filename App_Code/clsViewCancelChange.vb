Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Web.UI.WebControls
Imports System

Public Class clsViewCancelChange
#Region "Bind the Requestids to release"
    Public Sub ViewChangeCancelRequests(ByVal ddl As DropDownList, ByVal sta As Integer)
        Dim parms As SqlParameter() = {New SqlParameter("@staid", SqlDbType.Int, 50)}
        parms(0).Value = sta
        Try
            Dim dr As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_release_req_sp", parms)
            ddl.DataSource = dr
            ddl.DataTextField = "ssa_srnreq_id"
            ddl.DataValueField = "ssa_srnreq_id"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select--")

        Catch ex As Exception

        End Try
    End Sub
#End Region
End Class
