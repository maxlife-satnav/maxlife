Imports Microsoft.VisualBasic
Imports System.Text
Imports System.Web
Imports System.Net
Imports System.IO
Imports System.Configuration
Imports System

Public Class httppostSMS
    Public Shared Function SendSMS_MLMSMS(ByVal Message As String, ByVal SendTo As String, ByVal SenderID As String) As String
        Try
            ' Send a message using the txtLocal transport
            Const TransportURL As String = "http://www.unicel.in/SendSMS/sendmsg.php"
            '"http://www.mlmsms.in/pushsms.php"
            Dim TransportUserName As String = ConfigurationManager.AppSettings("smsuid")
            Dim TransportPassword As String = ConfigurationManager.AppSettings("smspwd")

            Dim strPost As String
            ' Build POST String
            'uname=utpal&pass=utp12&send=SatGuide&dest=" & destnum & "&msg=" & txtmsg.Text.Replace("&", "-").Trim & "&concat=1
            If (Message.Length > 160) Then
                strPost = "uname=" + TransportUserName _
            + "&pass=" + TransportPassword _
            + "&msg=" + Message _
            + "&dest=91" + SendTo _
            + "&send=SatGuide" _
            + "&concat=1"
            Else
                strPost = "uname=" + TransportUserName _
            + "&pass=" + TransportPassword _
            + "&msg=" + Message _
            + "&dest=91" + SendTo _
            + "&send=SatGuide"
            End If
            ' Create POST
            Dim request As WebRequest = WebRequest.Create(TransportURL)
            request.Method = "POST"
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strPost)
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            ' Microsoft.VisualBasic.MsgBox("Messages Just Left US")
            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            ' Clean up the streams.
            reader.Close()
            dataStream.Close()
            response.Close()
            ' Return result to calling function
            If responseFromServer.Length > 0 Then
                Return responseFromServer
            Else
                Return CType(response, HttpWebResponse).StatusDescription
            End If

        Catch ex As Exception
            Return "Error : " + ex.Message
        End Try
    End Function


    Public Shared Function DeliveryReceipt_SentMessage(ByVal MessageID As String) As String
        Try

            Const TransportURL As String = "http://www.unicel.in/SendSMS/sendmsg.php"
            '"http://mlmsms.in/fetchdlr.php"
            Dim strPost As String
            ' Build POST String         
            strPost = "msgid=" & MessageID
            ' Create POST
            Dim request As WebRequest = WebRequest.Create(TransportURL)
            request.Method = "POST"
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strPost)
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            ' Microsoft.VisualBasic.MsgBox("Messages Just Left US")
            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            ' Clean up the streams.
            reader.Close()
            dataStream.Close()
            response.Close()
            ' Return result to calling function
            If responseFromServer.Length > 0 Then
                Return responseFromServer
            Else
                Return CType(response, HttpWebResponse).StatusDescription
            End If
        Catch ex As Exception
            Return "Error : " + ex.Message
        End Try
    End Function
End Class
