#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Data
Imports System.Data.Common
Imports System.Text
Imports System.Xml
Imports System.IO
Imports System.Configuration.Provider
Imports System.Web.Configuration
Imports Commerce.Common

Namespace Commerce.Providers
	Friend Class USPostalServiceShippingProvider
		Inherits FulfillmentProvider
		#Region "Variables"
		Private _connectionUrl As String = ""
		Private _uspsUserName As String = ""
		Private _uspsPassword As String = ""
		Private _uspsAdditionalHandlingCharge As String = ""
		Private _deliveryRestriction As DeliveryRestrictions = DeliveryRestrictions.None
		#End Region

		#Region "Provider specific behaviors"
		Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
			If config Is Nothing Then
				Throw New ArgumentNullException("config")
			End If

			MyBase.Initialize(name, config)

			_connectionUrl = config("connectionUrl").ToString()
			If String.IsNullOrEmpty(_connectionUrl) Then
				Throw New ProviderException("Empty or missing connectionUrl")
			End If
			config.Remove("connectionUrl")

			_uspsUserName = config("uspsUserName").ToString()
			If String.IsNullOrEmpty(_uspsUserName) Then
				Throw New ProviderException("Empty or missing uspsUserName")
			End If
			config.Remove("uspsUserName")

			_uspsPassword = config("uspsPassword").ToString()
			If String.IsNullOrEmpty(_uspsPassword) Then
			  Throw New ProviderException("Empty or missing uspsPassword")
			End If
			config.Remove("uspsPassword")

			_uspsAdditionalHandlingCharge = config("uspsAdditionalHandlingCharge").ToString()
			If String.IsNullOrEmpty(_uspsAdditionalHandlingCharge) Then
				Throw New ProviderException("Empty or missing uspsAdditionalHandlingCharge")
			End If
			config.Remove("uspsAdditionalHandlingCharge")

			'Throw an exception if unrecognized attributes remain
			If config.Count > 0 Then
				Dim attr As String = config.GetKey(0)
				If (Not String.IsNullOrEmpty(attr)) Then
					Throw New ProviderException ("Unrecognized attribute: " & attr)
				End If
			End If
		End Sub

		#End Region

		#Region "Methods"
		Public Overrides Function GetDeliveryOptions(ByVal package As PackageInfo) As DeliveryOptionCollection
			Dim http As HttpRequestHandler = New HttpRequestHandler(_connectionUrl)
			Dim rateXml As String = http.POST(rateRequest(package))
			Dim collection As DeliveryOptionCollection = UspsParseRates(rateXml)
			Return collection
		End Function

		Public Overrides Function GetDeliveryOptions(ByVal package As PackageInfo, ByVal restrictions As DeliveryRestrictions) As DeliveryOptionCollection
			'TODO: I need to put a little more thought into the restrictions
			If restrictions = DeliveryRestrictions.Download Then
				Throw New Exception("Shipping Error: This item is download only.")
			End If
			_deliveryRestriction = restrictions
			Dim http As HttpRequestHandler = New HttpRequestHandler(_connectionUrl)
			Dim rateXml As String = http.POST(rateRequest(package))
			Dim collection As DeliveryOptionCollection = UspsParseRates(rateXml)
			Return collection
		End Function


		Private Function IsPackageTooLarge(ByVal length As Integer, ByVal height As Integer, ByVal width As Integer) As Boolean
			Dim total As Integer = totalPackageSize(length, height, width)
			If total > 130 Then
				Return True
			Else
				Return False
			End If
		End Function

		Private Function totalPackageSize(ByVal length As Integer, ByVal height As Integer, ByVal width As Integer) As Integer
			Dim girth As Integer = height + height + width + width
			Dim total As Integer = girth + length
			Return total
		End Function

		Private Function IsPackageTooHeavy(ByVal weight As Integer) As Boolean
			If weight > 70 Then
				Return True
			Else
				Return False
			End If
		End Function

		Private Function GetPackageSize(ByVal length As Integer, ByVal height As Integer, ByVal width As Integer) As String
			Dim girth As Integer = height + height + width + width
			Dim total As Integer = girth + length
			If total <= 84 Then
				Return "REGULAR"
			End If
			If (total > 84) AndAlso (total <= 108) Then
				Return "LARGE"
			End If
			If (total > 108) AndAlso (total <= 130) Then
				Return "OVERSIZE"
			Else
				Throw New Exception("Shipping Error: Package too large.")
			End If
		End Function

		Private Function UspsParseRates(ByVal response As String) As DeliveryOptionCollection
			Dim optionCollection As DeliveryOptionCollection = New DeliveryOptionCollection()
			Dim sr As StringReader = New StringReader(response)
			Dim tr As XmlTextReader = New XmlTextReader(sr)
			Try
				Do While tr.Read()
					If (tr.Name = "Error") AndAlso (tr.NodeType = XmlNodeType.Element) Then
						Dim errorText As String = ""
						Do While tr.Read()
							If (tr.Name = "HelpContext") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								errorText &= "USPS Help Context: " & tr.ReadString() & ", "
							End If
							If (tr.Name = "Description") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								errorText &= "Error Desc: " & tr.ReadString()
							End If
						Loop
						Throw New ProviderException("USPS Error returned: " & errorText)
					End If
					If (tr.Name = "Postage") AndAlso (tr.NodeType = XmlNodeType.Element) Then
						Dim serviceCode As String = ""
						Dim postalRate As String = ""
						Do While tr.Read()
							If (tr.Name = "MailService") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								serviceCode = tr.ReadString()
								tr.ReadEndElement()
								If (tr.Name = "MailService") AndAlso (tr.NodeType = XmlNodeType.EndElement) Then
									Exit Do
								End If
							End If
							If ((tr.Name = "Postage") AndAlso (tr.NodeType = XmlNodeType.EndElement)) OrElse ((tr.Name = "Postage") AndAlso (tr.NodeType = XmlNodeType.Element)) Then
								Exit Do
							End If
							If (tr.Name = "Rate") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								postalRate = tr.ReadString()
								tr.ReadEndElement()
								If (tr.Name = "Rate") AndAlso (tr.NodeType = XmlNodeType.EndElement) Then
									Exit Do
								End If
							End If
						Loop
						Dim service As String = GetServiceName(serviceCode)
						Dim rate As Decimal = Convert.ToDecimal(postalRate)
						If (Not String.IsNullOrEmpty(_uspsAdditionalHandlingCharge)) Then
							Dim additionalHandling As Decimal = Convert.ToDecimal(_uspsAdditionalHandlingCharge)
							rate += additionalHandling
						End If
						'Weed out unwanted or unkown service rates
						If service.ToUpper() <> "UNKNOWN" Then
							Dim [option] As DeliveryOption = New DeliveryOption()
							[option].Rate = rate
							[option].Service = service
							optionCollection.Add([option])
						End If
					End If
				Loop
				sr.Dispose()
				Return optionCollection
			Catch
				Throw
			Finally
				sr.Close()
				tr.Close()
			End Try
		End Function

		Private Function GetServiceName(ByVal serviceCode As String) As String
			Select Case serviceCode
				Case "Parcel Post"
					Return "USPS Parcel Post"
				Case "Priority Mail"
					Return "USPS Priority Mail"
				Case "Express Mail PO to Addressee"
					Return "USPS Express Mail"
				Case Else
					Return "Unknown"
			End Select
		End Function

		'Create rating request XML string
		Private Function rateRequest(ByVal package As PackageInfo) As String
			'Changed weight logic per JeffreyABecker suggestions
			Dim tOz As Integer = CInt(Fix(Math.Ceiling(package.Weight * 16.0D)))
			Dim packageWeightPounds As Integer = tOz / 16
			Dim packageWeightOunces As Integer = tOz Mod 16
			Dim sb As StringBuilder = New StringBuilder()
			sb.AppendFormat("<RateV2Request USERID=""{0}"" PASSWORD=""{1}"">", _uspsUserName, _uspsPassword)
			Dim packageStr As String = BuildRatePackage(package, packageWeightPounds, packageWeightOunces, package.Length, package.Height, package.Width)
			sb.Append(packageStr)
			sb.Append("</RateV2Request>")
			Return "API=RateV2&XML=" & sb.ToString()

		End Function

		Private Function BuildRatePackage(ByVal package As PackageInfo, ByVal packageWeightPounds As Integer, ByVal packageWeightOunces As Integer, ByVal length As Integer, ByVal height As Integer, ByVal width As Integer) As String
			If ((Not IsPackageTooHeavy(packageWeightPounds))) AndAlso ((Not IsPackageTooLarge(length, height, width))) Then
				'Rate single package
				Dim packageSize As String = GetPackageSize(length, height, width)
				Dim sb As StringBuilder = New StringBuilder()
				sb.Append("<Package ID=""0"">")
				sb.Append("<Service>ALL</Service>")
				sb.AppendFormat("<ZipOrigination>{0}</ZipOrigination>", package.FromZip)
				sb.AppendFormat("<ZipDestination>{0}</ZipDestination>", package.ToZip)
				sb.AppendFormat("<Pounds>{0}</Pounds>", packageWeightPounds)
				sb.AppendFormat("<Ounces>{0}</Ounces>", packageWeightOunces)
				sb.AppendFormat("<Size>{0}</Size>", packageSize)
				sb.Append("<Machinable>FALSE</Machinable>") 'Packages are not machinable
				sb.Append("</Package>")
				Return sb.ToString()
			Else
				'Rate multiple packages
				Dim totalPackages As Integer = totalPackageSize(length, height, width) / 108
				Dim tempPackageWeightPounds As Integer = packageWeightPounds / totalPackages
				Dim temppackageWeightOunces As Integer = packageWeightOunces / totalPackages
				Dim tempPackageHeight As Integer = height / totalPackages
				Dim tempPackageWidth As Integer = width / totalPackages
				Dim packageSize As String = GetPackageSize(length, tempPackageHeight, tempPackageWidth)
				Dim sb As StringBuilder = New StringBuilder()
				Dim i As Integer = 0
				Do While i <= totalPackages
					sb.AppendFormat("<Package ID=""{0}"">", i.ToString())
					sb.Append("<Service>ALL</Service>")
					sb.AppendFormat("<ZipOrigination>{0}</ZipOrigination>", package.FromZip)
					sb.AppendFormat("<ZipDestination>{0}</ZipDestination>", package.ToZip)
					sb.AppendFormat("<Pounds>{0}</Pounds>", tempPackageWeightPounds)
					sb.AppendFormat("<Ounces>{0}</Ounces>", temppackageWeightOunces)
					sb.AppendFormat("<Size>{0}</Size>", packageSize)
					sb.Append("<Machinable>FALSE</Machinable>") 'Packages are not machinable
					sb.Append("</Package>")
					i += 1
				Loop
				Return sb.ToString()
			End If
		End Function
		#End Region
	End Class
End Namespace
