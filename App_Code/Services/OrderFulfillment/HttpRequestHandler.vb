#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Web
Imports System.Net
Imports System.IO

Namespace Commerce.Providers
	Public Class HttpRequestHandler
		#Region "Variables"
		Private _url As String
		#End Region

		#Region "Properties"
		Public Property Url() As String
			Get
				Return _url
			End Get
			Set
				_url = Value
			End Set
		End Property
		#End Region

		#Region "Constructors"

		Public Sub New(ByVal url As String)
			Me.Url = url
		End Sub

		#End Region

		#Region "Methods"

		Public Function POST(ByVal XmlPostData As String) As String
			Dim encodedData As ASCIIEncoding = New ASCIIEncoding()
			Dim byteArray As Byte()=encodedData.GetBytes(XmlPostData)
			Dim wr As HttpWebRequest = CType(WebRequest.Create(New Uri(Url)), HttpWebRequest)
			wr.Method = "POST"
			wr.KeepAlive = False
			wr.UserAgent = "dashCommerce"
			wr.ContentType = "application/x-www-form-urlencoded"
			wr.ContentLength = XmlPostData.Length
			Dim SendStream As Stream=wr.GetRequestStream()
			SendStream.Write(byteArray,0,byteArray.Length)
			SendStream.Close()
			Dim WebResp As HttpWebResponse = CType(wr.GetResponse(), HttpWebResponse)
			Dim res As String = ""
			Try
				Using sr As StreamReader = New StreamReader(WebResp.GetResponseStream())
					res = sr.ReadToEnd()
				End Using
				WebResp.Close()
				Return res
			Catch ex As Exception
				Throw New Exception("Http Request has failed. Exception: " & ex.ToString())
			End Try
		End Function
		#End Region
	End Class
End Namespace
