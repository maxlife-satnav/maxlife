#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Specialized
Imports System.Data
Imports System.Collections.Generic
Imports System.Text
Imports System.Xml
Imports System.IO
Imports System.Configuration.Provider
Imports System.Web.Configuration
Imports Commerce.Common

Namespace Commerce.Providers
	Friend Class UpsShippingProvider
		Inherits FulfillmentProvider
		Private _connectionUrl As String = ""
		Private _upsAccessKey As String = ""
		Private _upsUserName As String = ""
		Private _upsPassword As String = ""
		Private _upsPickupTypeCode As String = ""
		Private _upsCustomerClassification As String = ""
		Private _upsPackagingTypeCode As String = ""
		Private _upsAdditionalHandlingCharge As String = ""
		Private _deliveryRestriction As DeliveryRestrictions = DeliveryRestrictions.None

		#Region "Provider specific behaviors"
		Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
			If config Is Nothing Then
				Throw New ArgumentNullException("config")
			End If

			MyBase.Initialize(name, config)

			_connectionUrl = config("connectionUrl").ToString()
			If String.IsNullOrEmpty(_connectionUrl) Then
				Throw New ProviderException("Empty or missing connectionUrl")
			End If
			config.Remove("connectionUrl")

			_upsAccessKey = config("upsAccessKey").ToString()
			If String.IsNullOrEmpty(_upsAccessKey) Then
				Throw New ProviderException("Empty or missing upsAccessKey")
			End If
			config.Remove("upsAccessKey")

			_upsUserName = config("upsUserName").ToString()
			If String.IsNullOrEmpty(_upsUserName) Then
				Throw New ProviderException("Empty or missing upsUserName")
			End If
			config.Remove("upsUserName")

			_upsPassword = config("upsPassword").ToString()
			If String.IsNullOrEmpty(_upsPassword) Then
				Throw New ProviderException("Empty or missing upsPassword")
			End If
			config.Remove("upsPassword")

			_upsPickupTypeCode = config("upsPickupTypeCode").ToString()
			If String.IsNullOrEmpty(_upsPickupTypeCode) Then
				Throw New ProviderException("Empty or missing upsPickupTypeCode")
			End If
			config.Remove("upsPickupTypeCode")

			_upsCustomerClassification = config("upsCustomerClassification").ToString()
			If String.IsNullOrEmpty(_upsCustomerClassification) Then
				Throw New ProviderException("Empty or missing upsCustomerClassification")
			End If
			config.Remove("upsCustomerClassification")

			_upsPackagingTypeCode = config("upsPackagingTypeCode").ToString()
			If String.IsNullOrEmpty(_upsPackagingTypeCode) Then
				Throw New ProviderException("Empty or missing upsPackagingTypeCode")
			End If
			config.Remove("upsPackagingTypeCode")

			_upsAdditionalHandlingCharge = config("upsAdditionalHandlingCharge").ToString()
			If String.IsNullOrEmpty(_upsAdditionalHandlingCharge) Then
				Throw New ProviderException("Empty or missing upsAdditionalHandlingCharge")
			End If
			config.Remove("upsAdditionalHandlingCharge")

			'Throw an exception if unrecognized attributes remain
			If config.Count > 0 Then
				Dim attr As String = config.GetKey(0)
				If (Not String.IsNullOrEmpty(attr)) Then
					Throw New ProviderException ("Unrecognized attribute: " & attr)
				End If
			End If
		End Sub

		#End Region

		#Region "Constructors"

		Public Sub New()

		End Sub

		#End Region

		#Region "Methods"



		Public Overrides Function GetDeliveryOptions(ByVal package As PackageInfo) As DeliveryOptionCollection
			Dim http As HttpRequestHandler = New HttpRequestHandler(_connectionUrl)
			Dim rateXml As String = http.POST(rateRequest(package))
			Dim collection As DeliveryOptionCollection = UpsParseRates(rateXml)
			Return collection
		End Function

		Public Overrides Function GetDeliveryOptions(ByVal package As PackageInfo, ByVal restrictions As DeliveryRestrictions) As DeliveryOptionCollection
			'TODO: I need to put a little more thought into the restrictions
			If restrictions = DeliveryRestrictions.Download Then
				Throw New Exception("Shipping Error: This item is download only.")
			End If
			_deliveryRestriction = restrictions
			Dim http As HttpRequestHandler = New HttpRequestHandler(_connectionUrl)
			Dim rateXml As String = http.POST(rateRequest(package))
			Dim collection As DeliveryOptionCollection = UpsParseRates(rateXml)
			Return collection
		End Function
		Private Function UpsParseRates(ByVal response As String) As DeliveryOptionCollection
			Dim optionCollection As DeliveryOptionCollection = New DeliveryOptionCollection()
			Dim sr As StringReader = New StringReader(response)
			Dim tr As XmlTextReader = New XmlTextReader(sr)
			Try
				Do While tr.Read()
					If (tr.Name = "Error") AndAlso (tr.NodeType = XmlNodeType.Element) Then
						Dim errorText As String = ""
						Do While tr.Read()
							If (tr.Name = "ErrorCode") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								errorText &= "UPS Rating Error, Error Code: " & tr.ReadString() & ", "
							End If
							If (tr.Name = "ErrorDescription") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								errorText &= "Error Desc: " & tr.ReadString()
							End If
						Loop
						Throw New ProviderException("UPS Error returned: " & errorText)
					End If
					If (tr.Name = "RatedShipment") AndAlso (tr.NodeType = XmlNodeType.Element) Then
						Dim serviceCode As String = ""
						Dim monetaryValue As String = ""
						Do While tr.Read()
							If (tr.Name = "Service") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								Do While tr.Read()
									If (tr.Name = "Code") AndAlso (tr.NodeType = XmlNodeType.Element) Then
										serviceCode = tr.ReadString()
										tr.ReadEndElement()
									End If
									If (tr.Name = "Service") AndAlso (tr.NodeType = XmlNodeType.EndElement) Then
										Exit Do
									End If
								Loop
							End If
							If ((tr.Name = "RatedShipment") AndAlso (tr.NodeType = XmlNodeType.EndElement)) OrElse ((tr.Name = "RatedPackage") AndAlso (tr.NodeType = XmlNodeType.Element)) Then
								Exit Do
							End If
							If (tr.Name = "TotalCharges") AndAlso (tr.NodeType = XmlNodeType.Element) Then
								Do While tr.Read()
									If (tr.Name = "MonetaryValue") AndAlso (tr.NodeType = XmlNodeType.Element) Then
										monetaryValue = tr.ReadString()
										tr.ReadEndElement()
									End If
									If (tr.Name = "TotalCharges") AndAlso (tr.NodeType = XmlNodeType.EndElement) Then
										Exit Do
									End If
								Loop
							End If
						Loop
						Dim service As String = GetServiceName(serviceCode)
						Dim rate As Decimal = Convert.ToDecimal(monetaryValue)
						If (Not String.IsNullOrEmpty(_upsAdditionalHandlingCharge)) Then
							Dim additionalHandling As Decimal = Convert.ToDecimal(_upsAdditionalHandlingCharge)
							rate += additionalHandling
						End If
						'Weed out unwanted or unkown service rates
						If service.ToUpper() <> "UNKNOWN" Then
							Dim [option] As DeliveryOption = New DeliveryOption()
							[option].Rate = rate
							[option].Service = service
							optionCollection.Add([option])
						End If

					End If
				Loop
				sr.Dispose()
				Return optionCollection
			Catch
				Throw
			Finally
				sr.Close()
				tr.Close()
			End Try
		End Function
		' Changed Package Size and Weight restrictions based on:
		' http://www.ups.com/content/us/en/resources/prepare/weight_size.html?WT.svl=SubNav
		' old values were 130 for totalPackageSize and 70 for weight
		Private Function IsPackageTooLarge(ByVal length As Integer, ByVal height As Integer, ByVal width As Integer) As Boolean
			Dim total As Integer = totalPackageSize(length, height, width)
			If total > 165 Then
				Return True
			Else
				Return False
			End If
		End Function

		Private Function totalPackageSize(ByVal length As Integer, ByVal height As Integer, ByVal width As Integer) As Integer
			Dim girth As Integer = height + height + width + width
			Dim total As Integer = girth + length
			Return total
		End Function

		Private Function IsPackageTooHeavy(ByVal weight As Integer) As Boolean
			If weight > 150 Then
				Return True
			Else
				Return False
			End If
		End Function

		Private Function GetServiceName(ByVal serviceID As String) As String
			Select Case serviceID
				Case "01"
					Return "UPS NextDay Air"
				Case "02"
					Return "UPS 2nd Day Air"
				Case "03"
					Return "UPS Ground"
				Case "07"
					Return "UPS Worldwide Express"
				Case "08"
					Return "UPS Worldwide Expidited"
				Case "11"
					Return "UPS Standard"
				Case "12"
					Return "UPS 3 Day Select"
				Case "13"
					Return "UPS Next Day Air Saver"
				Case "14"
					Return "UPS Next Day Air Early A.M."
				Case "54"
					Return "UPS Worldwide Express Plus"
				Case "59"
					Return "UPS 2nd Day Air A.M."
				Case Else
					Return "Unknown"
			End Select
		End Function

		'Create rating request XML string
		Private Function rateRequest(ByVal package As PackageInfo) As String
			'Changed weight logic per JeffreyABecker suggestions
			Dim Weight As Integer = CInt(Fix(Math.Ceiling(package.Weight)))
			Dim sb As StringBuilder = New StringBuilder()
			'Build Access Request
			sb.Append("<?xml version='1.0'?>")
			sb.Append("<AccessRequest xml:lang='en-US'>")
			sb.AppendFormat("<AccessLicenseNumber>{0}</AccessLicenseNumber>", _upsAccessKey)
			sb.AppendFormat("<UserId>{0}</UserId>", _upsUserName)
			sb.AppendFormat("<Password>{0}</Password>", _upsPassword)
			sb.Append("</AccessRequest>")
			'Build Rate Request
			sb.Append("<?xml version='1.0'?>")
			sb.Append("<RatingServiceSelectionRequest xml:lang='en-US'>")
			sb.Append("<Request>")
			sb.Append("<TransactionReference>")
			sb.Append("<CustomerContext>Bare Bones Rate Request</CustomerContext>")
			sb.Append("<XpciVersion>1.0001</XpciVersion>")
			sb.Append("</TransactionReference>")
			sb.Append("<RequestAction>Rate</RequestAction>")
			sb.Append("<RequestOption>Shop</RequestOption>")
			sb.Append("</Request>")
			sb.Append("<PickupType>")
			sb.AppendFormat("<Code>{0}</Code>", _upsPickupTypeCode)
			sb.Append("</PickupType>")
			sb.Append("<CustomerClassification>")
			sb.AppendFormat("<Code>{0}</Code>", _upsCustomerClassification)
			sb.Append("</CustomerClassification>")
			sb.Append("<Shipment>")
			sb.Append("<Shipper>")
			sb.Append("<Address>")
			sb.AppendFormat("<PostalCode>{0}</PostalCode>", package.FromZip)
			sb.AppendFormat("<CountryCode>{0}</CountryCode>", package.FromCountryCode)
			sb.Append("</Address>")
			sb.Append("</Shipper>")
			sb.Append("<ShipTo>")
			sb.Append("<Address>")
			'Required to get accurate residential delivery rates
			sb.Append("<ResidentialAddressIndicator/>")
			sb.AppendFormat("<PostalCode>{0}</PostalCode>", package.ToZip)
			sb.AppendFormat("<CountryCode>{0}</CountryCode>", package.ToCountryCode)
			sb.Append("</Address>")
			sb.Append("</ShipTo>")
			sb.Append("<ShipFrom>")
			sb.Append("<Address>")
			sb.AppendFormat("<PostalCode>{0}</PostalCode>", package.FromZip)
			sb.AppendFormat("<CountryCode>{0}</CountryCode>", package.FromCountryCode)
			sb.Append("</Address>")
			sb.Append("</ShipFrom>")
			sb.Append("<Service>")
			sb.Append("<Code>03</Code>")
			sb.Append("</Service>")
			Dim packageStr As String = BuildRatePackage(_upsPackagingTypeCode, package.Length, package.Width, package.Height, Weight)
			sb.Append(packageStr)
			sb.Append("</Shipment>")
			sb.Append("</RatingServiceSelectionRequest>")
			Return sb.ToString()
		End Function

		Private Function BuildRatePackage(ByVal upsPackagingTypeCode As String, ByVal length As Integer, ByVal width As Integer, ByVal height As Integer, ByVal packageWeight As Integer) As String
			If ((Not IsPackageTooHeavy(packageWeight))) AndAlso ((Not IsPackageTooLarge(length, height, width))) Then
				'Rate single package
				Dim sb As StringBuilder = New StringBuilder()
				sb.Append("<Package>")
				sb.Append("<PackagingType>")
				sb.AppendFormat("<Code>{0}</Code>", upsPackagingTypeCode)
				sb.Append("</PackagingType>")
				sb.Append("<Dimensions>")
				sb.AppendFormat("<Length>{0}</Length>", length)
				sb.AppendFormat("<Width>{0}</Width>", width)
				sb.AppendFormat("<Height>{0}</Height>", height)
				sb.Append("</Dimensions>")
				sb.Append("<PackageWeight>")
				sb.AppendFormat("<Weight>{0}</Weight>", packageWeight)
				sb.Append("</PackageWeight>")
				sb.Append("</Package>")
				Return sb.ToString()
			Else
				'Rate multiple packages
				Dim totalPackages As Integer = 1
				Dim tempPackageWeight As Integer = 0
				Dim tempPackageHeight As Integer = 0
				Dim tempPackageWidth As Integer = 0
				If length <> 0 AndAlso height <> 0 AndAlso width <> 0 Then
					totalPackages = totalPackageSize(length, height, width) / 108
					tempPackageWeight = packageWeight / totalPackages
					tempPackageHeight = height / totalPackages
					tempPackageWidth = width / totalPackages
				End If
				Dim sb As StringBuilder = New StringBuilder()
				Dim i As Integer = 0
				Do While i <= totalPackages
					sb.Append("<Package>")
					sb.Append("<PackagingType>")
					sb.AppendFormat("<Code>{0}</Code>", upsPackagingTypeCode)
					sb.Append("</PackagingType>")
					sb.Append("<Dimensions>")
					sb.AppendFormat("<Length>{0}</Length>", length)
					sb.AppendFormat("<Width>{0}</Width>", tempPackageWidth)
					sb.AppendFormat("<Height>{0}</Height>", tempPackageHeight)
					sb.Append("</Dimensions>")
					sb.Append("<PackageWeight>")
					sb.AppendFormat("<Weight>{0}</Weight>", tempPackageWeight)
					sb.Append("</PackageWeight>")
					sb.Append("</Package>")
					i += 1
				Loop

				Return sb.ToString()
			End If
		End Function
		#End Region
	End Class
End Namespace