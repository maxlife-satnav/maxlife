#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Web.Configuration
Imports System.Collections.Specialized
Imports System.Configuration.Provider

Namespace Commerce.Providers
	Public Class SimpleShippingProvider
		Inherits FulfillmentProvider
		Private _connectionString As String = ""
		Private _connectionStringName As String = ""

		#Region "Provider overrides"
		Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
			' Verify that config isn't null
			If config Is Nothing Then
				Throw New ArgumentNullException("config")
			End If

			MyBase.Initialize(name, config)

			_connectionStringName = config("connectionStringName").ToString()

			If String.IsNullOrEmpty(_connectionStringName) Then
				Throw New ProviderException("Empty or missing connectionStringName")
			End If

			config.Remove("connectionStringName")

			If WebConfigurationManager.ConnectionStrings(_connectionStringName) Is Nothing Then
				Throw New ProviderException("Missing connection string")
			End If

			_connectionString = WebConfigurationManager.ConnectionStrings(_connectionStringName).ConnectionString

			If String.IsNullOrEmpty(_connectionString) Then
				Throw New ProviderException("Empty connection string")
			End If

		End Sub
		#End Region



		Public Overrides Function GetDeliveryOptions(ByVal package As PackageInfo) As DeliveryOptionCollection
			Dim db As Database = DatabaseFactory.CreateDatabase()

			Using cmd As DbCommand = db.GetStoredProcCommand("CSK_Shipping_GetRates")
				db.AddInParameter(cmd,"@weight",DbType.Decimal, package.Weight)
				Dim rdr As IDataReader = db.ExecuteReader(cmd)

				Dim coll As DeliveryOptionCollection = New DeliveryOptionCollection()
				coll.Load(rdr)
				rdr.Close()
				Return coll
			End Using
		End Function
		Public Overrides Function GetDeliveryOptions(ByVal package As PackageInfo, ByVal restrictions As DeliveryRestrictions) As DeliveryOptionCollection
			Dim db As Database = DatabaseFactory.CreateDatabase()
			Dim sp As String = "CSK_Shipping_GetRates"
			If restrictions = DeliveryRestrictions.Air Then
				sp="CSK_Shipping_GetRates_Air"
			ElseIf restrictions = DeliveryRestrictions.Freight OrElse restrictions = DeliveryRestrictions.Ground Then
				sp = "CSK_Shipping_GetRates_Ground"

			End If

			Using cmd As DbCommand = db.GetStoredProcCommand(sp)
				db.AddInParameter(cmd, "@weight", DbType.Decimal, package.Weight)
				Dim rdr As IDataReader = db.ExecuteReader(cmd)

				Dim coll As DeliveryOptionCollection = New DeliveryOptionCollection()
				coll.Load(rdr)
				rdr.Close()
				Return coll
			End Using
		End Function

	End Class
End Namespace
