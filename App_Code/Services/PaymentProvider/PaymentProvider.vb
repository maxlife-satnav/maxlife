#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Configuration.Provider
Imports System.Data

Namespace Commerce.Providers
	Public Class PaymentProviderCollection
		Inherits System.Configuration.Provider.ProviderCollection
		Public Shadows ReadOnly Default Property Item(ByVal name As String) As PaymentProvider
			Get
				Return CType(MyBase.Item(name), PaymentProvider)
			End Get
		End Property

		Public Overrides Sub Add(ByVal provider As ProviderBase)
			If provider Is Nothing Then
				Throw New ArgumentNullException("provider")
			End If

			If Not(TypeOf provider Is PaymentProvider) Then
				Throw New ArgumentException ("Invalid provider type", "provider")
			End If

			MyBase.Add(provider)
		End Sub
	End Class
	Public MustInherit Class PaymentProvider
		Inherits System.Configuration.Provider.ProviderBase



		#Region "Payment Methods"

		Public MustOverride Function Charge(ByVal order As Commerce.Common.Order) As Commerce.Common.Transaction
		Public MustOverride Function Refund(ByVal order As Commerce.Common.Order) As String

		#End Region

	End Class


End Namespace