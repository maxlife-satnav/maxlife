#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports SubSonic
Imports System.Collections.Generic
Imports System.Xml
Imports System.Xml.Serialization

Namespace Commerce.ContentManagement

    ''' <summary>
    ''' Strongly-typed collection for the TextEntry class.
    ''' </summary>
    Partial Public Class TextEntryCollection
        Inherits ActiveList(Of TextEntry)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As TextEntryCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As TextEntryCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As TextEntryCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As TextEntryCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As TextEntryCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As TextEntryCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As TextEntryCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As TextEntryCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Content_Text")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub


    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the TextEntry table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class TextEntry
        Inherits ActiveRecord(Of TextEntry)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Content_Text")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As TextEntry = New TextEntry()
            Return TextEntry.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Content_Text")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub
        Public Sub New(ByVal contentName As String)
            Dim rdr As IDataReader = TextEntry.FetchByParameter("contentName", contentName)
            If rdr.Read() Then
                Me.Load(rdr)
            End If
            rdr.Close()

        End Sub
#End Region

#Region "Public Properties"
        <XmlAttribute("ContentID")> _
        Public Property ContentID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ContentID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ContentID", Value)
            End Set
        End Property
        <XmlAttribute("ContentGUID")> _
        Public Property ContentGUID() As String
            Get
                Dim result As Object = Me.GetColumnValue("ContentGUID")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ContentGUID", Value)
            End Set
        End Property
        <XmlAttribute("Title")> _
        Public Property Title() As String
            Get
                Dim result As Object = Me.GetColumnValue("Title")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Title", Value)
            End Set
        End Property

        <XmlAttribute("PageDescription")> _
        Public Property PageDescription() As String
            Get
                Dim result As Object = Me.GetColumnValue("PageDescription")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("PageDescription", value)
            End Set
        End Property

        <XmlAttribute("PageKeyword")> _
        Public Property PageKeyword() As String
            Get
                Dim result As Object = Me.GetColumnValue("PageKeyword")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("PageKeyword", value)
            End Set
        End Property



        <XmlAttribute("ContentName")> _
        Public Property ContentName() As String
            Get
                Dim result As Object = Me.GetColumnValue("ContentName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ContentName", Value)
            End Set
        End Property
        <XmlAttribute("Content")> _
        Public Property Content() As String
            Get
                Dim result As Object = Me.GetColumnValue("Content")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Content", Value)
            End Set
        End Property
        <XmlAttribute("IconPath")> _
        Public Property IconPath() As String
            Get
                Dim result As Object = Me.GetColumnValue("IconPath")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("IconPath", Value)
            End Set
        End Property
        <XmlAttribute("DateExpires")> _
        Public Property DateExpires() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("DateExpires")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("DateExpires", Value)
            End Set
        End Property
        <XmlAttribute("ContentGroupID")> _
        Public Property ContentGroupID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ContentGroupID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ContentGroupID", Value)
            End Set
        End Property
        <XmlAttribute("LastEditedBy")> _
        Public Property LastEditedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("LastEditedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("LastEditedBy", Value)
            End Set
        End Property
        <XmlAttribute("ExternalLink")> _
        Public Property ExternalLink() As String
            Get
                Dim result As Object = Me.GetColumnValue("ExternalLink")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ExternalLink", Value)
            End Set
        End Property
        <XmlAttribute("Status")> _
        Public Property Status() As String
            Get
                Dim result As Object = Me.GetColumnValue("Status")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Status", Value)
            End Set
        End Property
        <XmlAttribute("ListOrder")> _
        Public Property ListOrder() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ListOrder")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ListOrder", Value)
            End Set
        End Property
        <XmlAttribute("CallOut")> _
        Public Property CallOut() As String
            Get
                Dim result As Object = Me.GetColumnValue("CallOut")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CallOut", Value)
            End Set
        End Property
        <XmlAttribute("CreatedOn")> _
        Public Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedOn", Value)
            End Set
        End Property
        <XmlAttribute("CreatedBy")> _
        Public Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedBy", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedOn", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedBy", Value)
            End Set
        End Property

#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal contentGUID As String, ByVal title As String, ByVal contentName As String, ByVal content As String, ByVal iconPath As String, ByVal dateExpires As DateTime, ByVal contentGroupID As Integer, ByVal lastEditedBy As String, ByVal externalLink As String, ByVal status As String, ByVal listOrder As Integer, ByVal callOut As String)
            Dim item As TextEntry = New TextEntry()
            item.ContentGUID = contentGUID
            item.Title = title
            item.ContentName = contentName
            item.Content = content
            item.IconPath = iconPath
            item.DateExpires = dateExpires
            item.ContentGroupID = contentGroupID
            item.LastEditedBy = lastEditedBy
            item.ExternalLink = externalLink
            item.Status = status
            item.ListOrder = listOrder
            item.CallOut = callOut

            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal contentID As Integer, ByVal contentGUID As String, ByVal title As String, ByVal contentName As String, ByVal content As String, ByVal iconPath As String, ByVal dateExpires As DateTime, ByVal contentGroupID As Integer, ByVal lastEditedBy As String, ByVal externalLink As String, ByVal status As String, ByVal listOrder As Integer, ByVal callOut As String)
            Dim item As TextEntry = New TextEntry()
            item.ContentID = contentID
            item.ContentGUID = contentGUID
            item.Title = title
            item.ContentName = contentName
            item.Content = content
            item.IconPath = iconPath
            item.DateExpires = dateExpires
            item.ContentGroupID = contentGroupID
            item.LastEditedBy = lastEditedBy
            item.ExternalLink = externalLink
            item.Status = status
            item.ListOrder = listOrder
            item.CallOut = callOut

            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared ContentID As String
            Public Shared ContentGUID As String
            Public Shared Title As String
            Public Shared ContentName As String
            Public Shared Content As String
            Public Shared IconPath As String
            Public Shared DateExpires As String
            Public Shared ContentGroupID As String
            Public Shared LastEditedBy As String
            Public Shared ExternalLink As String
            Public Shared Status As String
            Public Shared ListOrder As String
            Public Shared CallOut As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                ContentID = "contentID"
                ContentGUID = "contentGUID"
                Title = "title"
                ContentName = "contentName"
                Content = "content"
                IconPath = "iconPath"
                DateExpires = "dateExpires"
                ContentGroupID = "contentGroupID"
                LastEditedBy = "lastEditedBy"
                ExternalLink = "externalLink"
                Status = "status"
                ListOrder = "listOrder"
                CallOut = "callOut"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
            End Sub
        End Structure
#End Region

    End Class





End Namespace