Imports Microsoft.VisualBasic

Public Class clsAssetMovementReport
    Private mMMR_AST_CODE As String

    Public Property MMR_AST_CODE() As String
        Get
            Return mMMR_AST_CODE
        End Get
        Set(ByVal value As String)
            mMMR_AST_CODE = value
        End Set
    End Property
    Private mSTWR_NAME As String

    Public Property STWR_NAME() As String
        Get
            Return mSTWR_NAME
        End Get
        Set(ByVal value As String)
            mSTWR_NAME = value
        End Set
    End Property
    Private mSFLR_NAME As String

    Public Property SFLR_NAME() As String
        Get
            Return mSFLR_NAME
        End Get
        Set(ByVal value As String)
            mSFLR_NAME = value
        End Set
    End Property
    Private mMMR_FROMEMP_ID As String

    Public Property MMR_FROMEMP_ID() As String
        Get
            Return mMMR_FROMEMP_ID
        End Get
        Set(ByVal value As String)
            mMMR_FROMEMP_ID = value
        End Set
    End Property
    Private mMMR_TOEMP_ID As String

    Public Property MMR_TOEMP_ID() As String
        Get
            Return mMMR_TOEMP_ID
        End Get
        Set(ByVal value As String)
            mMMR_TOEMP_ID = value
        End Set
    End Property
    Private mMMR_MVMT_DATE As String

    Public Property MMR_MVMT_DATE() As String
        Get
            Return mMMR_MVMT_DATE
        End Get
        Set(ByVal value As String)
            mMMR_MVMT_DATE = value
        End Set
    End Property
    Private mMMR_REQ_ID As String

    Public Property MMR_REQ_ID() As String
        Get
            Return mMMR_REQ_ID
        End Get
        Set(ByVal value As String)
            mMMR_REQ_ID = value
        End Set
    End Property

    Private mDTWR_NAME As String

    Public Property DTWR_NAME() As String
        Get
            Return mDTWR_NAME
        End Get
        Set(ByVal value As String)
            mDTWR_NAME = value
        End Set
    End Property

    Private mDFLR_NAME As String

    Public Property DFLR_NAME() As String
        Get
            Return mDFLR_NAME
        End Get
        Set(ByVal value As String)
            mDFLR_NAME = value
        End Set
    End Property


End Class
