Imports Microsoft.VisualBasic

Public Class clsAssetLocationForInnerDetails
    Private mAssetCount As String

    Public Property AssetCount() As String
        Get
            Return mAssetCount
        End Get
        Set(ByVal value As String)
            mAssetCount = value
        End Set
    End Property
    Private mAssetName As String

    Public Property AssetName() As String
        Get
            Return mAssetName
        End Get
        Set(ByVal value As String)
            mAssetName = value
        End Set
    End Property
End Class
