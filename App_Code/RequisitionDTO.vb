Imports Microsoft.VisualBasic

Namespace Amantra.RequisitionDTON

    Public Class RequisitionDTO

#Region "'Private variable declaration'"

        Private m_srn_req_id As String = String.Empty
        Private m_cabins As Integer = 0
        Private m_cubicals As Integer = 0
        Private m_Work_Stations As Integer = 0
        Private m_srn_bdg_one As String = String.Empty
        Private m_srn_bdg_two As String = String.Empty
        Private m_srn_frm_dt As String = String.Empty
        Private m_srn_to_dt As String = String.Empty
        Private m_usr_name As String = String.Empty
        Private m_RptMgr_Name As String = String.Empty
        Private m_srn_sta_id As String = String.Empty
        Private m_srn_dept As String = String.Empty
        Private m_branch As String = String.Empty
        Private m_srn_rem As String = String.Empty
        Private m_srn_rm_remarks As String = String.Empty
        Private m_srn_bfm_remarks As String = String.Empty

#End Region
#Region "Constructors"

        Public Sub New()

        End Sub
        Public Sub New(ByVal p_srn_req_id As String)
            Me.m_srn_req_id = p_srn_req_id
        End Sub
        Public Sub New(ByVal p_srn_req_id As String, ByVal p_srn_sta_id As String)
            Me.m_srn_req_id = p_srn_req_id
            Me.m_srn_dept = p_srn_sta_id
        End Sub
        Public Sub New(ByVal p_srn_req_id As String, ByVal p_srn_sta_id As String, ByVal p_usr_name As String)
            Me.m_srn_req_id = p_srn_req_id
            Me.m_usr_name = p_usr_name
            Me.m_srn_sta_id = p_srn_sta_id
        End Sub
        Public Sub New(ByVal p_srn_req_id As String, ByVal p_cabins As Integer, ByVal p_cubicals As Integer, ByVal p_work_stations As Integer, ByVal p_srn_bdg_one As String, ByVal p_srn_bdg_two As String, ByVal p_srn_frm_dt As String, ByVal p_srn_to_dt As String, ByVal p_usr_name As String, ByVal p_RptMgr_Name As String, ByVal p_srn_sta_id As String, ByVal p_srn_dept As String, ByVal p_srn_rem As String)
            Me.m_srn_req_id = p_srn_req_id
            Me.m_cabins = p_cabins
            Me.m_cubicals = p_cubicals
            Me.m_Work_Stations = p_work_stations
            Me.m_srn_bdg_one = p_srn_bdg_one
            Me.m_srn_bdg_two = p_srn_bdg_two
            Me.m_srn_frm_dt = p_srn_frm_dt
            Me.m_srn_to_dt = p_srn_to_dt
            Me.m_usr_name = p_usr_name
            Me.m_RptMgr_Name = p_RptMgr_Name
            Me.m_srn_sta_id = p_srn_sta_id
            Me.m_srn_dept = p_srn_dept
            Me.m_srn_rem = p_srn_rem
        End Sub

        Public Sub New(ByVal p_srn_req_id As String, ByVal p_cabins As Integer, ByVal p_cubicals As Integer, ByVal p_work_stations As Integer, ByVal p_srn_bdg_one As String, ByVal p_srn_bdg_two As String, ByVal p_srn_frm_dt As String, ByVal p_srn_to_dt As String, ByVal p_usr_name As String, ByVal p_RptMgr_Name As String, ByVal p_srn_sta_id As String, ByVal p_srn_dept As String, ByVal p_srn_rem As String, ByVal p_rm_remarks As String)
            Me.m_srn_req_id = p_srn_req_id
            Me.m_cabins = p_cabins
            Me.m_cubicals = p_cubicals
            Me.m_Work_Stations = p_work_stations
            Me.m_srn_bdg_one = p_srn_bdg_one
            Me.m_srn_bdg_two = p_srn_bdg_two
            Me.m_srn_frm_dt = p_srn_frm_dt
            Me.m_srn_to_dt = p_srn_to_dt
            Me.m_usr_name = p_usr_name
            Me.m_RptMgr_Name = p_RptMgr_Name
            Me.m_srn_sta_id = p_srn_sta_id
            Me.m_srn_dept = p_srn_dept
            Me.m_srn_rem = p_srn_rem
            Me.m_srn_rm_remarks = p_rm_remarks
        End Sub

        Public Sub New(ByVal p_srn_req_id As String, ByVal p_cabins As Integer, ByVal p_cubicals As Integer, ByVal p_work_stations As Integer, ByVal p_srn_bdg_one As String, ByVal p_srn_bdg_two As String, ByVal p_srn_frm_dt As String, ByVal p_srn_to_dt As String, ByVal p_usr_name As String, ByVal p_RptMgr_Name As String, ByVal p_srn_sta_id As String, ByVal p_srn_dept As String, ByVal p_srn_rem As String, ByVal p_rm_remarks As String, ByVal p_branch_id As String)
            Me.m_srn_req_id = p_srn_req_id
            Me.m_cabins = p_cabins
            Me.m_cubicals = p_cubicals
            Me.m_Work_Stations = p_work_stations
            Me.m_srn_bdg_one = p_srn_bdg_one
            Me.m_srn_bdg_two = p_srn_bdg_two
            Me.m_srn_frm_dt = p_srn_frm_dt
            Me.m_srn_to_dt = p_srn_to_dt
            Me.m_usr_name = p_usr_name
            Me.m_RptMgr_Name = p_RptMgr_Name
            Me.m_srn_sta_id = p_srn_sta_id
            Me.m_srn_dept = p_srn_dept
            Me.m_srn_rem = p_srn_rem
            Me.m_srn_rm_remarks = p_rm_remarks
            Me.m_branch = p_branch_id
        End Sub

#End Region
#Region "Private variable declaration"

        Public Property RequestID() As String
            Get
                Return m_srn_req_id
            End Get
            Set(ByVal value As String)
                m_srn_req_id = value
            End Set
        End Property
        Public Property getCabins() As Integer
            Get
                Return m_cabins
            End Get
            Set(ByVal value As Integer)
                m_cabins = value
            End Set
        End Property
        Public Property getCubicals() As Integer
            Get
                Return m_cubicals
            End Get
            Set(ByVal value As Integer)
                m_cubicals = value
            End Set
        End Property
        Public Property getWrkStations() As Integer
            Get
                Return m_Work_Stations
            End Get
            Set(ByVal value As Integer)
                m_Work_Stations = value
            End Set
        End Property
        Public Property getBuildingOne() As String
            Get
                Return m_srn_bdg_one
            End Get
            Set(ByVal value As String)
                m_srn_bdg_one = value
            End Set
        End Property
        Public Property getBuildingTwo() As String
            Get
                Return m_srn_bdg_two
            End Get
            Set(ByVal value As String)
                m_srn_bdg_two = value
            End Set
        End Property
        Public Property getFromDate() As String
            Get
                Return m_srn_frm_dt
            End Get
            Set(ByVal value As String)
                m_srn_frm_dt = value
            End Set
        End Property
        Public Property getToDate() As String
            Get
                Return m_srn_to_dt
            End Get
            Set(ByVal value As String)
                m_srn_to_dt = value
            End Set
        End Property
        Public Property Branch() As String
            Get
                Return m_branch
            End Get
            Set(ByVal value As String)
                m_branch = value
            End Set
        End Property

        Public Property getUserName() As String
            Get
                Return m_usr_name
            End Get
            Set(ByVal value As String)
                m_usr_name = value
            End Set
        End Property
        Public Property getRptMgrName() As String
            Get
                Return m_RptMgr_Name
            End Get
            Set(ByVal value As String)
                m_RptMgr_Name = value
            End Set
        End Property
        Public Property getStatus() As Integer
            Get
                Return m_srn_sta_id
            End Get
            Set(ByVal value As Integer)
                m_srn_sta_id = value
            End Set
        End Property
        Public Property getDept() As String
            Get
                Return m_srn_dept
            End Get
            Set(ByVal value As String)
                m_srn_dept = value
            End Set
        End Property
        Public Property getRemarks() As String
            Get
                Return m_srn_rem
            End Get
            Set(ByVal value As String)
                m_srn_rem = value
            End Set
        End Property
        Public Property getRMRemarks() As String
            Get
                Return m_srn_rm_remarks
            End Get
            Set(ByVal value As String)
                m_srn_rm_remarks = value
            End Set
        End Property

        Public Property getBFMRemarks() As String
            Get
                Return m_srn_bfm_remarks
            End Get
            Set(ByVal value As String)
                m_srn_bfm_remarks = value
            End Set
        End Property
#End Region
    End Class

End Namespace
