#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic

Namespace Commerce.Common
    Partial Public Class Address
        Inherits ActiveRecord(Of Address)


#Region "Custom - not in db"

        Public ReadOnly Property FullAddress() As String
            Get
                Return Me.ToHtmlString()
            End Get
        End Property


        'Used for ExpressCheckouts
        Private _PayPalPayerID As String
        Private _PayPalToken As String

        Public Property PayPalToken() As String
            Get
                Return _PayPalToken
            End Get
            Set(ByVal value As String)
                _PayPalToken = Value
            End Set
        End Property

        Public Property PayPalPayerID() As String
            Get
                Return _PayPalPayerID
            End Get
            Set(ByVal value As String)
                _PayPalPayerID = Value
            End Set
        End Property
#End Region

#Region "ToString() Override"
        ''' <summary>
        ''' Override the base ToString() so that it will format nicely for a web-page
        ''' </summary>
        ''' <returns></returns>
        Public Overrides Function ToString() As String

            Dim sOut As String = FirstName & " " & LastName & Constants.vbCrLf & Address1 & Constants.vbCrLf
            If (Not String.IsNullOrEmpty(Address2)) Then
                sOut &= Address2 & Constants.vbCrLf
            End If
            sOut &= City & ", " & StateOrRegion & "  " & Zip & " " & Country
            Return sOut
        End Function
        Public Function ToHtmlString() As String
            Dim sOut As String = "<table>"
            sOut &= "<tr><td><b>" & FirstName & " " & LastName & "</b></td></tr>" & "<tr><td>" & Address1 & "</td></tr>"
            If (Not String.IsNullOrEmpty(Address2)) Then
                sOut &= "<tr><td>" & Address2 & "</td></tr>"
            End If
            sOut &= "<tr><td>" & City & ", " & StateOrRegion & "  " & Zip & " " & Country & "</td></tr>" & "<tr><td>" & Email & "</td></tr>" & "<tr><td>" & Phone & "</td></tr>"
            sOut &= "</table>"
            Return sOut
        End Function
#End Region

#Region "Comparisons"
        Public Overloads Function Equals(ByVal compareAddress As Address) As Boolean
            Dim bOut As Boolean = False
            'if the first, last, address1, city, state, etc are equal return true
            If compareAddress.FirstName.ToLower().Equals(Me.FirstName.ToLower()) AndAlso compareAddress.LastName.ToLower().Equals(Me.LastName.ToLower()) AndAlso compareAddress.Address1.ToLower().Equals(Me.Address1.ToLower()) AndAlso compareAddress.City.ToLower().Equals(Me.City.ToLower()) AndAlso compareAddress.StateOrRegion.ToLower().Equals(Me.StateOrRegion.ToLower()) Then
                bOut = True
            End If
            Return bOut
        End Function
#End Region


        Public Sub Remove()
            Dim q As Query = New Query(Address.GetTableSchema())
            q.AddWhere("AddressId", Me.AddressID)
            q.QueryType = QueryType.Delete
            q.Execute()
        End Sub
    End Class
End Namespace
