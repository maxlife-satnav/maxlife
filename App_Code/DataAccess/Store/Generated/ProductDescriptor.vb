Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the ProductDescriptor class.
    ''' </summary>

    <Serializable()> _
    Partial Public Class ProductDescriptorCollection
        Inherits ActiveList(Of ProductDescriptor)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As ProductDescriptorCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As ProductDescriptorCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As ProductDescriptorCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As ProductDescriptorCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As ProductDescriptorCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As ProductDescriptorCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As ProductDescriptorCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As ProductDescriptorCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Store_ProductDescriptor")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub

    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the CSK_Store_ProductDescriptor table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class ProductDescriptor
        Inherits ActiveRecord(Of ProductDescriptor)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Store_ProductDescriptor")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As ProductDescriptor = New ProductDescriptor()
            Return ProductDescriptor.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Store_ProductDescriptor")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

        Public Sub New(ByVal columnName As String, ByVal columnValue As Object)
            SetSQLProps()
            MyBase.LoadByParam(columnName, columnValue)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("DescriptorID")> _
        Public Property DescriptorID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("DescriptorID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("DescriptorID", Value)
            End Set
        End Property
        <XmlAttribute("ProductID")> _
        Public Property ProductID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ProductID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ProductID", Value)
            End Set
        End Property
        <XmlAttribute("Title")> _
        Public Property Title() As String
            Get
                Dim result As Object = Me.GetColumnValue("Title")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Title", Value)
            End Set
        End Property
        <XmlAttribute("Descriptor")> _
        Public Property Descriptor() As String
            Get
                Dim result As Object = Me.GetColumnValue("Descriptor")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Descriptor", Value)
            End Set
        End Property
        <XmlAttribute("IsBulletedList")> _
        Public Property IsBulletedList() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsBulletedList")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
            Set(ByVal value As Boolean)
                Me.MarkDirty()
                Me.SetColumnValue("IsBulletedList", Value)
            End Set
        End Property
        <XmlAttribute("ListOrder")> _
        Public Property ListOrder() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ListOrder")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ListOrder", Value)
            End Set
        End Property

#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal productID As Integer, ByVal title As String, ByVal descriptor As String, ByVal isBulletedList As Boolean, ByVal listOrder As Integer)
            Dim item As ProductDescriptor = New ProductDescriptor()
            item.ProductID = productID
            item.Title = title
            item.Descriptor = descriptor
            item.IsBulletedList = isBulletedList
            item.ListOrder = listOrder

            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal descriptorID As Integer, ByVal productID As Integer, ByVal title As String, ByVal descriptor As String, ByVal isBulletedList As Boolean, ByVal listOrder As Integer)
            Dim item As ProductDescriptor = New ProductDescriptor()
            item.DescriptorID = descriptorID
            item.ProductID = productID
            item.Title = title
            item.Descriptor = descriptor
            item.IsBulletedList = isBulletedList
            item.ListOrder = listOrder

            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared DescriptorID As String
            Public Shared ProductID As String
            Public Shared Title As String
            Public Shared Descriptor As String
            Public Shared IsBulletedList As String
            Public Shared ListOrder As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                DescriptorID = "descriptorID"
                ProductID = "productID"
                Title = "title"
                Descriptor = "descriptor"
                IsBulletedList = "isBulletedList"
                ListOrder = "listOrder"
            End Sub
        End Structure
#End Region

    End Class
End Namespace
