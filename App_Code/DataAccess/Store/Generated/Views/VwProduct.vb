Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the VwProduct class.
    ''' </summary>
    Public Class VwProductCollection
        Inherits ReadOnlyList(Of VwProduct)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As VwProductCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As VwProductCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As VwProductCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As VwProductCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As VwProductCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As VwProductCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As VwProductCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As VwProductCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("vwProduct")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub

    End Class

    ''' <summary>
    ''' This is an ReadOnly class which wraps the vwProduct table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class VwProduct
        Inherits ReadOnlyRecord(Of VwProduct)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("vwProduct")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As VwProduct = New VwProduct()
            Return VwProduct.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("vwProduct")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()

        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("ProductID")> _
        Public ReadOnly Property ProductID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ProductID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("Sku")> _
        Public ReadOnly Property Sku() As String
            Get
                Dim result As Object = Me.GetColumnValue("Sku")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ProductName")> _
        Public ReadOnly Property ProductName() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ShortDescription")> _
        Public ReadOnly Property ShortDescription() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShortDescription")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ManufacturerID")> _
        Public ReadOnly Property ManufacturerID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ManufacturerID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("StatusID")> _
        Public ReadOnly Property StatusID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("StatusID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("ProductTypeID")> _
        Public ReadOnly Property ProductTypeID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ProductTypeID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("ShippingTypeID")> _
        Public ReadOnly Property ShippingTypeID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ShippingTypeID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("ShipEstimateID")> _
        Public ReadOnly Property ShipEstimateID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ShipEstimateID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("TaxTypeID")> _
        Public ReadOnly Property TaxTypeID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("TaxTypeID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("StockLocation")> _
        Public ReadOnly Property StockLocation() As String
            Get
                Dim result As Object = Me.GetColumnValue("StockLocation")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("OurPrice")> _
        Public ReadOnly Property OurPrice() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("OurPrice")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("RetailPrice")> _
        Public ReadOnly Property RetailPrice() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("RetailPrice")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("Weight")> _
        Public ReadOnly Property Weight() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Weight")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("CurrencyCode")> _
        Public ReadOnly Property CurrencyCode() As String
            Get
                Dim result As Object = Me.GetColumnValue("CurrencyCode")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("UnitOfMeasure")> _
        Public ReadOnly Property UnitOfMeasure() As String
            Get
                Dim result As Object = Me.GetColumnValue("UnitOfMeasure")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("AdminComments")> _
        Public ReadOnly Property AdminComments() As String
            Get
                Dim result As Object = Me.GetColumnValue("AdminComments")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("Length")> _
        Public ReadOnly Property Length() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Length")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("Height")> _
        Public ReadOnly Property Height() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Height")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("Width")> _
        Public ReadOnly Property Width() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Width")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("DimensionUnit")> _
        Public ReadOnly Property DimensionUnit() As String
            Get
                Dim result As Object = Me.GetColumnValue("DimensionUnit")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("IsDeleted")> _
        Public ReadOnly Property IsDeleted() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsDeleted")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
        End Property
        <XmlAttribute("ListOrder")> _
        Public ReadOnly Property ListOrder() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ListOrder")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("RatingSum")> _
        Public ReadOnly Property RatingSum() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("RatingSum")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("TotalRatingVotes")> _
        Public ReadOnly Property TotalRatingVotes() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("TotalRatingVotes")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("CreatedOn")> _
        Public ReadOnly Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("CreatedBy")> _
        Public ReadOnly Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public ReadOnly Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public ReadOnly Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ShippingEstimate")> _
        Public ReadOnly Property ShippingEstimate() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShippingEstimate")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("LeadTimeDays")> _
        Public ReadOnly Property LeadTimeDays() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("LeadTimeDays")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("ImageFile")> _
        Public ReadOnly Property ImageFile() As String
            Get
                Dim result As Object = Me.GetColumnValue("ImageFile")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("CategoryID")> _
        Public ReadOnly Property CategoryID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("CategoryID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("Rating")> _
        Public ReadOnly Property Rating() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Rating")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
        End Property
        <XmlAttribute("Manufacturer")> _
        Public ReadOnly Property Manufacturer() As String
            Get
                Dim result As Object = Me.GetColumnValue("Manufacturer")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("Status")> _
        Public ReadOnly Property Status() As String
            Get
                Dim result As Object = Me.GetColumnValue("Status")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("TaxType")> _
        Public ReadOnly Property TaxType() As String
            Get
                Dim result As Object = Me.GetColumnValue("TaxType")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("TaxCode")> _
        Public ReadOnly Property TaxCode() As String
            Get
                Dim result As Object = Me.GetColumnValue("TaxCode")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ShippingType")> _
        Public ReadOnly Property ShippingType() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShippingType")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ShippingCode")> _
        Public ReadOnly Property ShippingCode() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShippingCode")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("DefaultImage")> _
        Public ReadOnly Property DefaultImage() As String
            Get
                Dim result As Object = Me.GetColumnValue("DefaultImage")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property
        <XmlAttribute("ProductGUID")> _
        Public ReadOnly Property ProductGUID() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductGUID")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
        End Property

#End Region


#Region "Columns Struct"
        Public Structure Columns
            Public Shared ProductID As String
            Public Shared Sku As String
            Public Shared ProductName As String
            Public Shared ShortDescription As String
            Public Shared ManufacturerID As String
            Public Shared StatusID As String
            Public Shared ProductTypeID As String
            Public Shared ShippingTypeID As String
            Public Shared ShipEstimateID As String
            Public Shared TaxTypeID As String
            Public Shared StockLocation As String
            Public Shared OurPrice As String
            Public Shared RetailPrice As String
            Public Shared Weight As String
            Public Shared CurrencyCode As String
            Public Shared UnitOfMeasure As String
            Public Shared AdminComments As String
            Public Shared Length As String
            Public Shared Height As String
            Public Shared Width As String
            Public Shared DimensionUnit As String
            Public Shared IsDeleted As String
            Public Shared ListOrder As String
            Public Shared RatingSum As String
            Public Shared TotalRatingVotes As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            Public Shared ShippingEstimate As String
            Public Shared LeadTimeDays As String
            Public Shared ImageFile As String
            Public Shared CategoryID As String
            Public Shared Rating As String
            Public Shared Manufacturer As String
            Public Shared Status As String
            Public Shared TaxType As String
            Public Shared TaxCode As String
            Public Shared ShippingType As String
            Public Shared ShippingCode As String
            Public Shared DefaultImage As String
            Public Shared ProductGUID As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                ProductID = "productID"
                Sku = "sku"
                ProductName = "productName"
                ShortDescription = "shortDescription"
                ManufacturerID = "manufacturerID"
                StatusID = "statusID"
                ProductTypeID = "productTypeID"
                ShippingTypeID = "shippingTypeID"
                ShipEstimateID = "shipEstimateID"
                TaxTypeID = "taxTypeID"
                StockLocation = "stockLocation"
                OurPrice = "ourPrice"
                RetailPrice = "retailPrice"
                Weight = "weight"
                CurrencyCode = "currencyCode"
                UnitOfMeasure = "unitOfMeasure"
                AdminComments = "adminComments"
                Length = "length"
                Height = "height"
                Width = "width"
                DimensionUnit = "dimensionUnit"
                IsDeleted = "isDeleted"
                ListOrder = "listOrder"
                RatingSum = "ratingSum"
                TotalRatingVotes = "totalRatingVotes"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
                ShippingEstimate = "shippingEstimate"
                LeadTimeDays = "leadTimeDays"
                ImageFile = "imageFile"
                CategoryID = "categoryID"
                Rating = "rating"
                Manufacturer = "manufacturer"
                Status = "status"
                TaxType = "taxType"
                TaxCode = "taxCode"
                ShippingType = "shippingType"
                ShippingCode = "shippingCode"
                DefaultImage = "defaultImage"
                ProductGUID = "productGUID"
            End Sub
        End Structure
#End Region

    End Class


End Namespace
