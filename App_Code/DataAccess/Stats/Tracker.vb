#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Commerce.Common
Imports SubSonic

Namespace Commerce.Stats

	''' <summary>
	''' A Persistable class that uses Generics to store it's state
	''' in the database. This class maps to the CSK_Stats_Tracker table.
	''' </summary>
	Public Class Tracker
		Inherits ActiveRecord(Of Tracker)

		#Region ".ctors"
		''' <summary>
		''' Sets the static Table property from our Base class. This property tells
		''' the base class how to create the CRUD queries, etc.
		''' </summary>
		Private Sub SetSQLProps()

			If Schema Is Nothing Then
				Schema = Query.BuildTableSchema("CSK_Stats_Tracker")

			End If
		End Sub

		Public Sub New()
			MyBase.New()
			SetSQLProps()
		End Sub
		Public Sub New(ByVal trackingID_Renamed As Integer)
			SetSQLProps()
			MyBase.LoadByKey(trackingID_Renamed)
		End Sub

		#End Region

		#Region "private vars"

		Private trackingID_Renamed As Integer = 0
		Private userName_Renamed As String = String.Empty
		Private adID_Renamed As Integer = 0
		Private promoID_Renamed As Integer = 0
		Private productSKU_Renamed As String = String.Empty
		Private categoryID_Renamed As Integer = 0
		Private pageURL_Renamed As String = String.Empty
		Private behaviorID As Integer = 0
		Private searchString_Renamed As String = String.Empty
		Private sessionID_Renamed As String = String.Empty

		#End Region

		#Region "Public Props"
		<XmlAttribute("TrackingID")> _
		Public Property TrackingID() As Integer
			Get
				Return trackingID_Renamed
			End Get
			Set
				trackingID_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("trackingID", Value)

			End Set
		End Property
		<XmlAttribute("UserName")> _
		Public Property UserName() As String
			Get
				Return userName_Renamed
			End Get
			Set
				userName_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("userName", Value)

			End Set
		End Property
		<XmlAttribute("AdID")> _
		Public Property AdID() As Integer
			Get
				Return adID_Renamed
			End Get
			Set
				adID_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("adID", Value)

			End Set
		End Property
		<XmlAttribute("PromoID")> _
		Public Property PromoID() As Integer
			Get
				Return promoID_Renamed
			End Get
			Set
				promoID_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("promoID", Value)

			End Set
		End Property
		<XmlAttribute("ProductSKU")> _
		Public Property ProductSKU() As String
			Get
				Return productSKU_Renamed
			End Get
			Set
				productSKU_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("productSKU", Value)

			End Set
		End Property
		<XmlAttribute("CategoryID")> _
		Public Property CategoryID() As Integer
			Get
				Return categoryID_Renamed
			End Get
			Set
				categoryID_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("categoryID", Value)

			End Set
		End Property
		<XmlAttribute("PageURL")> _
		Public Property PageURL() As String
			Get
				Return pageURL_Renamed
			End Get
			Set
				pageURL_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("pageURL", Value)

			End Set
		End Property
		<XmlAttribute("Behavior")> _
		Public Property Behavior() As BehaviorType
			Get
				Return CType(behaviorID, BehaviorType)
			End Get
			Set
				behaviorID = CInt(Fix(Value))
				Me.MarkDirty()
				Me.SetColumnValue("behaviorID", Value)

			End Set
		End Property
		<XmlAttribute("SearchString")> _
		Public Property SearchString() As String
			Get
				Return searchString_Renamed
			End Get
			Set
				searchString_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("searchString", Value)

			End Set
		End Property
		<XmlAttribute("SessionID")> _
		Public Property SessionID() As String
			Get
				Return sessionID_Renamed
			End Get
			Set
				sessionID_Renamed = Value
				Me.MarkDirty()
				Me.SetColumnValue("sessionID", Value)

			End Set
		End Property

		#End Region

		#Region "Extended Data Access - Stored Procedures"


		''' <summary>
		''' Executes "CSK_Stats_Tracker_GetByBehaviorID" and returns the results in an IDataReader 
		''' </summary>
		''' <returns>System.Data.IDataReader </returns>	
		Public Shared Function GetByBehaviorID(ByVal behaviorID As Integer) As IDataReader

			Dim cmd As QueryCommand = New QueryCommand("CSK_Stats_Tracker_GetByBehaviorID")
			cmd.CommandType = CommandType.StoredProcedure

			cmd.AddParameter("@behaviorID", behaviorID)

			Return DataService.GetReader(cmd)

		End Function

		''' <summary>
		''' Executes "CSK_Stats_Tracker_SynchTrackingCookie" and returns the results in an IDataReader 
		''' </summary>
		''' <returns>System.Data.IDataReader </returns>	
		Public Shared Function SynchTrackingCookie(ByVal userName_Renamed As String, ByVal trackerCookie As String) As IDataReader

			Dim cmd As QueryCommand = New QueryCommand("CSK_Stats_Tracker_SynchTrackingCookie")
			cmd.CommandType = CommandType.StoredProcedure
			cmd.AddParameter("@userName", userName_Renamed)
			cmd.AddParameter("@trackerCookie", trackerCookie)

			Return DataService.GetReader(cmd)

		End Function

		''' <summary>
		''' Executes "CSK_Stats_Tracker_GetRecentlyViewedProducts" and returns the results in an IDataReader 
		''' </summary>
		''' <returns>System.Data.IDataReader </returns>	
		Public Shared Function GetRecentlyViewedProducts(ByVal username As String) As IDataReader

			Dim cmd As QueryCommand = New QueryCommand("CSK_Stats_Tracker_GetRecentlyViewedProducts")
			cmd.CommandType = CommandType.StoredProcedure
			cmd.AddParameter("@userName", username)

			Return DataService.GetReader(cmd)

		End Function

		''' <summary>
		''' Executes "CSK_Stats_Tracker_GetByProductAndBehavior" and returns the results in an IDataReader 
		''' </summary>
		''' <returns>System.Data.IDataReader </returns>	
		Public Shared Function GetByProductAndBehavior(ByVal behaviorID As Integer, ByVal sku As String) As IDataReader


			Dim cmd As QueryCommand = New QueryCommand("CSK_Stats_Tracker_GetByProductAndBehavior")
			cmd.CommandType = CommandType.StoredProcedure
			cmd.AddParameter("@behaviorID", behaviorID)
			cmd.AddParameter("@sku", sku)

			Return DataService.GetReader(cmd)

		End Function
		#End Region

		#Region "Extended Add Routines"

		Public Shared Sub Add(ByVal behavior As BehaviorType, ByVal categoryID_Renamed As Integer, ByVal productSKU_Renamed As String, ByVal searchString_Renamed As String)
			Add(behavior, categoryID_Renamed, productSKU_Renamed, searchString_Renamed, 0, 0, "")
		End Sub
		Public Shared Sub Add(ByVal behavior As BehaviorType, ByVal productSKU_Renamed As String)
			Add(behavior, 0, productSKU_Renamed, "", 0, 0, "")
		End Sub
		Public Shared Sub Add(ByVal behavior As BehaviorType, ByVal categoryID_Renamed As Integer)
			Add(behavior, categoryID_Renamed, "", "", 0, 0, "")
		End Sub
		Public Shared Sub Add(ByVal behavior As BehaviorType)
			Add(behavior, 0, "", "", 0, 0, "")
		End Sub
		Public Shared Sub Add(ByVal track As Tracker)

			track.Save("System")

		End Sub
		Public Shared Sub Add(ByVal behavior As BehaviorType, ByVal categoryID_Renamed As Integer, ByVal productSKU_Renamed As String, ByVal searchString_Renamed As String, ByVal adID_Renamed As Integer, ByVal promoID_Renamed As Integer, ByVal pageUrl As String)

			'get the page from the server
			If pageUrl = String.Empty Then
				pageUrl = System.Web.HttpContext.Current.Request.Url.PathAndQuery
			End If

			Dim sessionID_Renamed As String = System.Web.HttpContext.Current.Session.SessionID

			Dim track As Tracker = New Tracker()
			track.Behavior = behavior
			track.CategoryID = categoryID_Renamed
			track.ProductSKU = productSKU_Renamed
			track.SearchString = searchString_Renamed
			track.AdID = adID_Renamed
			track.PromoID = promoID_Renamed
			track.PageURL = pageUrl
			track.SessionID = sessionID_Renamed
			track.UserName = Utility.GetUserName()

			Add(track)

		End Sub


		#End Region

	End Class
End Namespace
