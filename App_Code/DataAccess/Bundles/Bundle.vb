#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic
Imports Commerce.Common
Imports System.Web

Namespace Commerce.Promotions
    Public Class BundleCollection
        Inherits ActiveList(Of Bundle)

    End Class
    ''' <summary>
    ''' A Persistable class that uses Generics to store it's state
    ''' in the database. This class maps to the CSK_Promo_Bundle table.
    ''' </summary>
    Public Class Bundle
        Inherits ActiveRecord(Of Bundle)

#Region ".ctors"
        ''' <summary>
        ''' Sets the static Table property from our Base class. This property tells
        ''' the base class how to create the CRUD queries, etc.
        ''' </summary>
        Private Sub SetSQLProps()

            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema(HttpContext.Current.Session("TENANT").ToString() & "." & "CSK_Promo_Bundle")


            End If

            'set the default values
            Me.SetColumnValue("bundleID", 0)
            Me.SetColumnValue("bundleName", String.Empty)
            Me.SetColumnValue("discountPercent", 0)
            Me.SetColumnValue("description", String.Empty)

            'state properties - these are set automagically 
            'during save
            Me.SetColumnValue("createdOn", DateTime.UtcNow)
            Me.SetColumnValue("createdBy", String.Empty)
            Me.SetColumnValue("modifiedOn", DateTime.UtcNow)
            Me.SetColumnValue("modifiedBy", String.Empty)

        End Sub

        Public Shared Function GetTableSchema() As TableSchema.Table
            'instance an object to make sure
            'the table schema has been created
            Dim item As Bundle = New Bundle()
            Return Bundle.Schema
        End Function

        Public Sub New()
            SetSQLProps()
            Me.MarkNew()
        End Sub
        Public Sub New(ByVal bundleID As Integer)
            SetSQLProps()
            MyBase.LoadByKey(bundleID)

        End Sub

#End Region

#Region "Public Props"
        <XmlAttribute("BundleID")> _
        Public Property BundleID() As Integer
            Get
                Return Integer.Parse(Me.GetColumnValue("bundleID").ToString())
            End Get
            Set(value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("bundleID", value)

            End Set
        End Property
        <XmlAttribute("BundleName")> _
        Public Property BundleName() As String
            Get
                Return Me.GetColumnValue("bundleName").ToString()
            End Get
            Set(value As String)
                Me.MarkDirty()
                Me.SetColumnValue("bundleName", value)

            End Set
        End Property
        <XmlAttribute("DiscountPercent")> _
        Public Property DiscountPercent() As Integer
            Get
                Return Integer.Parse(Me.GetColumnValue("discountPercent").ToString())
            End Get
            Set(value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("discountPercent", value)

            End Set
        End Property
        <XmlAttribute("Description")> _
        Public Property Description() As String
            Get
                Return Me.GetColumnValue("description").ToString()
            End Get
            Set(value As String)
                Me.MarkDirty()
                Me.SetColumnValue("description", value)

            End Set
        End Property

#End Region

#Region "Extended Data Access - Stored Procedures"


        ''' <summary>
        ''' Executes "CSK_Promo_Bundle_GetAvailableProducts" and returns the results in an IDataReader 
        ''' </summary>
        ''' <returns>System.Data.IDataReader </returns>	
        Public Shared Function GetAvailableProducts(ByVal bundleID As Integer) As IDataReader
            Return SPs.PromoBundleGetAvailableProducts(bundleID).GetReader()

        End Function

        ''' <summary>
        ''' Executes "CSK_Promo_Bundle_GetSelectedProducts" and returns the results in an IDataReader 
        ''' </summary>
        ''' <returns>System.Data.IDataReader </returns>	
        Public Shared Function GetSelectedProducts(ByVal bundleID As Integer) As IDataReader


            Return SPs.PromoBundleGetSelectedProducts(bundleID).GetReader()


        End Function

        ''' <summary>
        ''' Executes "CSK_Promo_Bundle_AddProduct" and returns the results in an IDataReader 
        ''' </summary>
        ''' <returns>System.Data.IDataReader </returns>	
        Public Shared Function AddProduct(ByVal bundleID As Integer, ByVal productID As Integer) As IDataReader


            Return SPs.PromoBundleAddProduct(bundleID, productID).GetReader()

        End Function

        ''' <summary>
        ''' Executes "CSK_Promo_Bundle_RemoveProduct" and returns the results in an IDataReader 
        ''' </summary>
        ''' <returns> </returns>	
        Public Shared Sub RemoveProduct(ByVal bundleID As Integer, ByVal productID As Integer)

            Dim q As Query = New Query(HttpContext.Current.Session("TENANT").ToString() & "." & "CSK_Promo_Product_Bundle_Map")
            q.AddWhere("bundleID", bundleID)
            q.AddWhere("productID", productID)
            q.Execute()

        End Sub

        ''' <summary>
        ''' Executes "CSK_Promo_Bundle_GetByProductID" and returns the results in an IDataReader 
        ''' </summary>
        ''' <returns>System.Data.IDataReader </returns>	
        Public Shared Function GetByProductID(ByVal productID As Integer) As IDataReader

            Return SPs.PromoBundleGetByProductID(productID).GetReader()


        End Function
#End Region

    End Class
End Namespace
