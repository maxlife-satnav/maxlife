#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic
Imports Commerce.Common

Namespace Commerce.Promotions


	Public Class ProductDiscount

		Public Sub New()

		End Sub
		Public IsLoaded As Boolean = False
		#Region "private vars"

		Private _CampaignID As Integer
		Private _Campaign As String
		Private _PromoID As Integer
		Private _PromoCode As String
		Private _Title As String
		Private _Description As String
		Private _ProductID As Integer
		Private _Sku As String
		Private _ProductName As String
		Private _OurPrice As Decimal
		Private _RetailPrice As Decimal
		Private _DiscountAmount As Decimal
		Private _DiscountedPrice As Decimal
		Private _Discount As Decimal
		Private _QtyThreshold As Integer
		Private _IsActive As Boolean
		Private _DateEnd As DateTime
		#End Region

		#Region "Public Props"
		Public Property CampaignID() As Integer
			Get
				Return _CampaignID
			End Get
			Set
				_CampaignID = Value
			End Set
		End Property
		Public Property Campaign() As String
			Get
				Return _Campaign
			End Get
			Set
				_Campaign = Value
			End Set
		End Property
		Public Property PromoID() As Integer
			Get
				Return _PromoID
			End Get
			Set
				_PromoID = Value
			End Set
		End Property
		Public Property PromoCode() As String
			Get
				Return _PromoCode
			End Get
			Set
				_PromoCode = Value
			End Set
		End Property
		Public Property Title() As String
			Get
				Return _Title
			End Get
			Set
				_Title = Value
			End Set
		End Property
		Public Property Description() As String
			Get
				Return _Description
			End Get
			Set
				_Description = Value
			End Set
		End Property
		Public Property ProductID() As Integer
			Get
				Return _ProductID
			End Get
			Set
				_ProductID = Value
			End Set
		End Property
		Public Property Sku() As String
			Get
				Return _Sku
			End Get
			Set
				_Sku = Value
			End Set
		End Property
		Public Property ProductName() As String
			Get
				Return _ProductName
			End Get
			Set
				_ProductName = Value
			End Set
		End Property
		Public Property OurPrice() As Decimal
			Get
				Return _OurPrice
			End Get
			Set
				_OurPrice = Value
			End Set
		End Property
		Public Property RetailPrice() As Decimal
			Get
				Return _RetailPrice
			End Get
			Set
				_RetailPrice = Value
			End Set
		End Property
		Public Property DiscountAmount() As Decimal
			Get
				Return _DiscountAmount
			End Get
			Set
				_DiscountAmount = Value
			End Set
		End Property
		Public Property DiscountedPrice() As Decimal
			Get
				Return _DiscountedPrice
			End Get
			Set
				_DiscountedPrice = Value
			End Set
		End Property
		Public Property Discount() As Decimal
			Get
				Return _Discount
			End Get
			Set
				_Discount = Value
			End Set
		End Property
		Public Property QtyThreshold() As Integer
			Get
				Return _QtyThreshold
			End Get
			Set
				_QtyThreshold = Value
			End Set
		End Property
		Public Property IsActive() As Boolean
			Get
				Return _IsActive
			End Get
			Set
				_IsActive = Value
			End Set
		End Property
		Public Property DateEnd() As DateTime
			Get
				Return _DateEnd
			End Get
			Set
				_DateEnd = Value
			End Set
		End Property
		#End Region

		Public Sub Load(ByVal rdr As IDataReader)
				IsLoaded = True
            Try
                _CampaignID = CInt(Fix(rdr("CampaignID")))
            Catch
            End Try
            Try
                _Campaign = rdr("CampaignName").ToString()
            Catch
            End Try
            Try
                _PromoID = CInt(Fix(rdr("PromoID")))
            Catch
            End Try
            Try
                _PromoCode = rdr("PromoCode").ToString()
            Catch
            End Try
            Try
                _Title = rdr("Title").ToString()
            Catch
            End Try
            Try
                _Description = rdr("Description").ToString()
            Catch
            End Try
            Try
                _ProductID = CInt(Fix(rdr("ProductID")))
            Catch
            End Try
            Try
                _Sku = rdr("Sku").ToString()
            Catch
            End Try
            Try
                _ProductName = rdr("ProductName").ToString()
            Catch
            End Try
            Try
                _OurPrice = CDec(rdr("OurPrice"))
            Catch
            End Try
            Try
                _RetailPrice = CDec(rdr("RetailPrice"))
            Catch
            End Try
            Try
                _DiscountAmount = CDec(rdr("DiscountAmount"))
            Catch
            End Try
            Try
                _DiscountedPrice = CDec(rdr("DiscountedPrice"))
            Catch
            End Try
            Try
                _Discount = CDec(rdr("Discount"))
            Catch
            End Try
            Try
                _QtyThreshold = CInt(Fix(rdr("QtyThreshold")))
            Catch
            End Try
            Try
                _IsActive = CBool(rdr("IsActive"))
            Catch
            End Try
            Try
                _DateEnd = CDate(rdr("DateEnd"))
            Catch
            End Try
        End Sub

		#Region "Extended Data Access"
		Public Shared Function GetDiscounts() As List(Of ProductDiscount)

			Dim rdr As IDataReader = SPs.PromoProductMatrix().GetReader()
			Dim coll As List(Of ProductDiscount) = New List(Of ProductDiscount)()
			Dim disc As ProductDiscount
			Do While rdr.Read()
				disc = New ProductDiscount()
				disc.Load(rdr)
				coll.Add(disc)
			Loop
			rdr.Close()
			Return coll

		End Function
		#End Region


	End Class

End Namespace