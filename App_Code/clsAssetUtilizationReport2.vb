Imports Microsoft.VisualBasic

Public Class clsAssetUtilizationReport2
    Private mTOTAL As String

    Public Property TOTAL() As String
        Get
            Return mTOTAL
        End Get
        Set(ByVal value As String)
            mTOTAL = value
        End Set
    End Property
    Private mAVAILABLE As String

    Public Property AVAILABLE() As String
        Get
            Return mAVAILABLE
        End Get
        Set(ByVal value As String)
            mAVAILABLE = value
        End Set
    End Property
    Private mALLOCATED As String

    Public Property ALLOCATED() As String
        Get
            Return mALLOCATED
        End Get
        Set(ByVal value As String)
            mALLOCATED = value
        End Set
    End Property
End Class
