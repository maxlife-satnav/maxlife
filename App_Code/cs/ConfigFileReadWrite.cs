using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Configuration;
using System.Web.Configuration;

namespace SoftwareLocker
{
    class ConfigFileReadWrite
    {
        // Key for TripleDES encryption
        public static byte[] key = { 21, 10, 64, 10, 100, 40, 200, 4,
                    21, 54, 65, 246, 5, 62, 1, 54,
                    54, 6, 8, 9, 65, 4, 65, 9};

        private static byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0 };

        public static string ReadFromConfigfile()
        {
            Configuration configuration=WebConfigurationManager.OpenWebConfiguration("~");
            string str=configuration.AppSettings.Settings["SerialKey"].Value;
            if (string.IsNullOrEmpty(str))
               return string.Empty;

            return str;
        }

        public static void WriteIntoConfigfile(string Data)
        {
            Configuration configuration;
            configuration = WebConfigurationManager.OpenWebConfiguration("~");
            configuration.AppSettings.Settings["SerialKey"].Value = Data ;
            configuration.Save();
        }
     
    }
}
