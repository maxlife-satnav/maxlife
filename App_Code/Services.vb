Imports Microsoft.VisualBasic
Imports System
Imports System.Web
Imports System.Collections
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Commerce.Common
Imports System.Data
''' <summary>
''' Summary description for Services
''' </summary>
<WebService(Namespace := "http://tempuri.org/"), WebServiceBinding(ConformsTo := WsiProfiles.BasicProfile1_1)> _
Public Class Services
	Inherits System.Web.Services.WebService

	Public Sub New()

		'Uncomment the following line if using designed components 
		'InitializeComponent(); 
	End Sub

	<WebMethod> _
	Public Function GetProducts() As ProductCollection
		Return ProductController.GetAll()
	End Function

	<WebMethod> _
	Public Function GetProducts(ByVal categoryID As Integer) As ProductCollection
		Dim rdr As IDataReader=ProductController.GetByCategoryID(categoryID)
		Dim coll As ProductCollection = New ProductCollection()
		coll.Load(rdr)
		rdr.Close()
		Return coll
	End Function
	<WebMethod> _
	Public Function GetProduct(ByVal sku As String) As Product
		Return New Product(sku)
	End Function
	<WebMethod> _
	Public Function GetProduct(ByVal productID As Integer) As Product
		Return New Product(productID)
	End Function
	<WebMethod> _
	Public Function GetCategories() As CategoryCollection
		Return CategoryController.CategoryList
	End Function
	<WebMethod> _
	Public Function GetCategory(ByVal categoryGUID As String) As Category
		Return New Category("categoryGUID",categoryGUID)
	End Function
	<WebMethod> _
	Public Function TransactCreditCartOrder(ByVal order As Commerce.Common.Order) As Transaction
		Return OrderController.TransactOrder(order,TransactionType.CreditCardPayment)
	End Function
End Class

