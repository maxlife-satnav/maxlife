﻿
Imports System.IO
Imports System.IO.Compression
Imports System.Text
Imports System.Web
Imports System

Public Class ScriptCombiner
    Implements IHttpHandler
    Private Shared ReadOnly CACHE_DURATION As TimeSpan = TimeSpan.FromDays(30)
    Private context As HttpContext
    Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
        Me.context = context
        Dim request As HttpRequest = context.Request

        ' Read setName, version from query string
        Dim setName As String = If(request("s"), String.Empty)
        Dim version As String = If(request("v"), String.Empty)
        Dim type As String = If(request("type"), String.Empty)

        ' Decide if browser supports compressed response
        Dim isCompressed As Boolean = Me.CanGZip(context.Request)

        ' If the set has already been cached, write the response directly from
        ' cache. Otherwise generate the response and cache it
        If Not Me.WriteFromCache(setName, version, type, isCompressed) Then
            Using memoryStream As New MemoryStream(8092)
                ' Decide regular stream or gzip stream based on whether the response can be compressed or not
                'using (Stream writer = isCompressed ?  (Stream)(new GZipStream(memoryStream, CompressionMode.Compress)) : memoryStream)
                Using writer As Stream = If(isCompressed, DirectCast(New ICSharpCode.SharpZipLib.GZip.GZipOutputStream(memoryStream), Stream), memoryStream)
                    ' Read the files into one big string
                    Dim allScripts As New StringBuilder()
                    For Each fileName As String In GetScriptFileNames(setName)
                        allScripts.Append(File.ReadAllText(context.Server.MapPath(fileName)))
                    Next

                    ' Minify the combined script files and remove comments and white spaces
                    'Dim minifier = New JavaScriptMinifier()
                    'Dim minified As String = minifier.Minify(allScripts.ToString())

                    ' Send minfied string to output stream
                    Dim bts As Byte() = Encoding.UTF8.GetBytes(allScripts.ToString())
                    writer.Write(bts, 0, bts.Length)
                End Using

                ' Cache the combined response so that it can be directly written
                ' in subsequent calls 
                Dim responseBytes As Byte() = memoryStream.ToArray()
                context.Cache.Insert(GetCacheKey(setName, version, isCompressed), responseBytes, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, CACHE_DURATION)

                ' Generate the response
                Me.WriteBytes(responseBytes, type, isCompressed)
            End Using
        End If
    End Sub
    Private Function WriteFromCache(setName As String, version As String, type As String, isCompressed As Boolean) As Boolean
        Dim responseBytes As Byte() = TryCast(context.Cache(GetCacheKey(setName, version, isCompressed)), Byte())

        If responseBytes Is Nothing OrElse responseBytes.Length = 0 Then
            Return False
        End If

        Me.WriteBytes(responseBytes, type, isCompressed)
        Return True
    End Function

    Private Sub WriteBytes(bytes As Byte(), type As String, isCompressed As Boolean)
        Dim response As HttpResponse = context.Response

        response.AppendHeader("Content-Length", bytes.Length.ToString())
        response.ContentType = type
        If isCompressed Then
            response.AppendHeader("Content-Encoding", "gzip")
        Else
            response.AppendHeader("Content-Encoding", "utf-8")
        End If

        context.Response.Cache.SetCacheability(HttpCacheability.[Public])
        context.Response.Cache.SetExpires(DateTime.Now.Add(CACHE_DURATION))
        context.Response.Cache.SetMaxAge(CACHE_DURATION)

        response.ContentEncoding = Encoding.Unicode
        response.OutputStream.Write(bytes, 0, bytes.Length)
        response.Flush()
    End Sub

    Private Function CanGZip(request As HttpRequest) As Boolean
        Dim acceptEncoding As String = request.Headers("Accept-Encoding")
        If Not String.IsNullOrEmpty(acceptEncoding) AndAlso (acceptEncoding.Contains("gzip") OrElse acceptEncoding.Contains("deflate")) Then
            Return True
        End If
        Return False
    End Function

    Private Function GetCacheKey(setName As String, version As String, isCompressed As Boolean) As String
        Return (Convert.ToString((Convert.ToString("HttpCombiner.") & setName) + ".") & version) & "." & isCompressed
    End Function



    ' private helper method that return an array of file names inside the text file stored in App_Data folder
    Private Shared Function GetScriptFileNames(setName As String) As String()
        Dim scripts = New System.Collections.Generic.List(Of String)()
        Dim setPath As String = HttpContext.Current.Server.MapPath([String].Format("~/App_Data/{0}.txt", setName))
        Using setDefinition = File.OpenText(setPath)
            Dim fileName As String = Nothing
            While setDefinition.Peek() >= 0
                fileName = setDefinition.ReadLine()
                If Not [String].IsNullOrEmpty(fileName) Then
                    scripts.Add(fileName)
                End If
            End While
        End Using
        Return scripts.ToArray()

    End Function

    Public Shared Function GetScriptTags(setName As String, contentType As String, version As Integer) As String
        Dim result As String = Nothing
        '#If (DEBUG) Then
        'For Each fileName As String In GetScriptFileNames(setName)
        '    result += [String].Format(vbLf & "<script type=""text/javascript"" src=""{0}?v={1}""></script>", VirtualPathUtility.ToAbsolute(fileName), version)
        'Next
        '#Else

        If contentType.Equals("js") Then
            result += [String].Format("<script type=""text/javascript"" src=""ScriptCombiner.axd?s={0}&v={1}&type=text/javascript""></script>", setName, version)
        Else
            result += String.Format("<link href=""ScriptCombiner.axd?s={0}&v={1}&type=text/css"" rel=""stylesheet"" type=""text/css""/>", setName, version)
        End If

        '#End If
        Return result
    End Function

    Public ReadOnly Property IsReusable As Boolean Implements IHttpHandler.IsReusable
        Get
            Return True
        End Get
    End Property
End Class
