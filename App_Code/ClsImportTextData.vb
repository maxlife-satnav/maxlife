Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Public Class ClsImportTextData
    Public Sub New()
    End Sub
    ''' <summary>
    ''' Converts a given delimited file into a dataset. 
    ''' Assumes that the first line
    ''' of the text file contains the column names.
    ''' </summary>
    ''' <param name="File">Name of the file to read</param>
    ''' <param name="TableName">Name of the Table to be made in the dataset returned</param>
    ''' <param name="delimiter">delimiter character in the dataset</param>
    ''' <returns></returns>
    ''' <remarks>Pramod Kumar</remarks>

    Public Shared Function TextFilestoDatabase(ByVal File As String, ByVal TableName As String, ByVal delimiter As String) As Integer
        Dim da As SqlDataAdapter = New SqlDataAdapter("Select * from " + TableName, ConfigurationManager.AppSettings("AmantraAxisFramework"))
        Dim cmdBuilder As SqlCommandBuilder = New SqlCommandBuilder(da)
        Dim _dataset As New DataSet
        da.Fill(_dataset, TableName)
        'Open the file in a stream reader.
        Dim _streamReader As New StreamReader(File)
        'Split the first line into the columns       
        Dim _columns As String() = _streamReader.ReadLine().Split(delimiter.ToCharArray())
        'Add the new DataTable to the RecordSet
        '_dataset.Tables.Add(TableName)
        'Cycle the colums, adding those that don't exist yet 
        'and sequencing the one that do.
        'For Each _columnName As String In _columns
        '    Dim added As Boolean = False
        '    Dim [next] As String = ""
        '    Dim i As Integer = 0
        '    While Not added
        '        'Build the column name and remove any unwanted characters.
        '        Dim columnname As String = _columnName + [next]
        '        columnname = columnname.Replace("#", "")
        '        columnname = columnname.Replace("'", "")
        '        columnname = columnname.Replace("&", "")
        '        'See if the column already exists
        '        If Not _dataset.Tables(TableName).Columns.Contains(columnname) Then
        '            'if it doesn't then we add it here and mark it as added
        '            _dataset.Tables(TableName).Columns.Add(columnname)
        '            added = True
        '        Else
        '            'if it did exist then we increment the sequencer and try again.
        '            i += 1
        '            [next] = "_" & i.ToString()
        '        End If
        '    End While
        'Next
        'Read the rest of the data in the file.        
        Dim AllData As String = _streamReader.ReadToEnd()
        'Split off each row at the Carriage Return/Line Feed
        'Default line ending in most windows exports.  
        'You may have to edit this to match your particular file.
        'This will work for Excel, Access, etc. default exports.
        'AllData.Split(vbCr & vbLf.ToCharArray())
        Dim rows As String() = System.Text.RegularExpressions.Regex.Split(AllData, "\r\n")
        'Now add each row to the DataSet        
        For Each _row As String In rows
            Try
                'Split the row at the delimiter.
                Dim items As String() = _row.Split(delimiter.ToCharArray())
                'Add the item
                _dataset.Tables(TableName).Rows.Add(items)
            Catch ex As Exception
            End Try
        Next
        'Return the imported data.
        Return da.Update(_dataset, TableName)
        'Return _dataset
    End Function
End Class
