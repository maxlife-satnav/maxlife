#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common
Namespace Commerce.Web.UI.Controls
	''' <summary>
	''' Renders a transparent gif image with a particular size.
	''' Default size is 1x1.
	''' </summary>
	Public Class Spacer
		Inherits Control
		#Region "Members"

		Private htmlImage As String = "<div><img src=""{0}"" border=""0"" width=""{1}"" height=""{2}""></div>"

		#End Region

		Protected Overrides Sub Render(ByVal output As HtmlTextWriter)
			output.Write(String.Format(htmlImage, String.Format("{0}{1}", ImagePath, ImageName), Convert.ToInt32(Width.Value).ToString(), Convert.ToInt32(Height.Value).ToString()))
		End Sub

		#Region "Properties"

		''' <summary>
		''' Spacer image path.
		''' </summary>
		Private imagePath_Renamed As String = Nothing
		Public Property ImagePath() As String
			Get
				If Utility.IsNullOrEmpty(imagePath_Renamed) Then
					imagePath_Renamed = Utility.GetSiteRoot() & "/images/"
				End If

				If imagePath_Renamed.EndsWith("/") Then
					Return (imagePath_Renamed)
				Else
					Return (String.Format("{0}{1}", imagePath_Renamed, "/"))
				End If
			End Get
			Set
				imagePath_Renamed = Value
			End Set
		End Property

		''' <summary>
		''' Spacer image name. This usually is a 1x1 transparent gif.
		''' </summary>
		Private imageName_Renamed As String = Nothing
		Public Property ImageName() As String
			Get
				If Utility.IsNullOrEmpty(imageName_Renamed) Then
					imagePath_Renamed = "1pix.gif"
				End If

				Return imagePath_Renamed
			End Get
			Set
				imagePath_Renamed = Value
			End Set
		End Property

		''' <summary>
		''' Spacer width.
		''' </summary>
		Private width_Renamed As Unit = Unit.Pixel(1)
		Public Property Width() As Unit
			Get
				Return width_Renamed
			End Get
			Set
				width_Renamed = Value
			End Set
		End Property

		''' <summary>
		''' Spacer height.
		''' </summary>
		Private height_Renamed As Unit = Unit.Pixel(1)
		Public Property Height() As Unit
			Get
				Return height_Renamed
			End Get
			Set
				height_Renamed = Value
			End Set
		End Property

		#End Region
	End Class
End Namespace
