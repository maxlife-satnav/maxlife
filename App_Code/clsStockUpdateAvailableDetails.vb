Imports Microsoft.VisualBasic

Public Class clsStockUpdateAvailableDetails
    Private mAIM_CODE As String

    Public Property AIM_CODE() As String
        Get
            Return mAIM_CODE
        End Get
        Set(ByVal value As String)
            mAIM_CODE = value
        End Set
    End Property
    Private mAIM_NAME As String

    Public Property AIM_NAME() As String
        Get
            Return mAIM_NAME
        End Get
        Set(ByVal value As String)
            mAIM_NAME = value
        End Set
    End Property
    Private mAID_AVLBL_QTY As String

    Public Property AID_AVLBL_QTY() As String
        Get
            Return mAID_AVLBL_QTY
        End Get
        Set(ByVal value As String)
            mAID_AVLBL_QTY = value
        End Set
    End Property
    Private mAID_UPDATED_DATE As String

    Public Property AID_UPDATED_DATE() As String
        Get
            Return mAID_UPDATED_DATE
        End Get
        Set(ByVal value As String)
            mAID_UPDATED_DATE = value
        End Set
    End Property
    Private mAID_REM As String

    Public Property AID_REM() As String
        Get
            Return mAID_REM
        End Get
        Set(ByVal value As String)
            mAID_REM = value
        End Set
    End Property
End Class
