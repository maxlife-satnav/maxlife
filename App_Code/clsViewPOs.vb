Imports Microsoft.VisualBasic

Public Class clsViewPOs
    Private mAIP_PO_ID As String

    Public Property AIP_PO_ID() As String
        Get
            Return mAIP_PO_ID
        End Get
        Set(ByVal value As String)
            mAIP_PO_ID = value
        End Set
    End Property
    Private mAIP_PO_DATE As String

    Public Property AIP_PO_DATE() As String
        Get
            Return mAIP_PO_DATE
        End Get
        Set(ByVal value As String)
            mAIP_PO_DATE = value
        End Set
    End Property
    Private mAIP_AUR_ID As String

    Public Property AIP_AUR_ID() As String
        Get
            Return mAIP_AUR_ID
        End Get
        Set(ByVal value As String)
            mAIP_AUR_ID = value
        End Set
    End Property
    Private mAIP_DEPT As String

    Public Property AIP_DEPT() As String
        Get
            Return mAIP_DEPT
        End Get
        Set(ByVal value As String)
            mAIP_DEPT = value
        End Set
    End Property
    Private mAIPD_VENDORNAME As String

    Public Property AIPD_VENDORNAME() As String
        Get
            Return mAIPD_VENDORNAME
        End Get
        Set(ByVal value As String)
            mAIPD_VENDORNAME = value
        End Set
    End Property
    Private mAIPD_TOTALCOST As String

    Public Property AIPD_TOTALCOST() As String
        Get
            Return mAIPD_TOTALCOST
        End Get
        Set(ByVal value As String)
            mAIPD_TOTALCOST = value
        End Set
    End Property
    Private mAIPD_EXPECTED_DATEOF_DELIVERYFormatted As String

    Public Property AIPD_EXPECTED_DATEOF_DELIVERYFormatted() As String
        Get
            Return mAIPD_EXPECTED_DATEOF_DELIVERYFormatted
        End Get
        Set(ByVal value As String)
            mAIPD_EXPECTED_DATEOF_DELIVERYFormatted = value
        End Set
    End Property
End Class
