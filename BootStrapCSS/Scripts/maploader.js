﻿var fltered1 = '';
var box_bounds = null;

var map, layer, heatmap, markers;
var bounds = new OpenLayers.Bounds(-28166394, -2127107.75, 12066442, 14675112);
//var box_bounds = new OpenLayers.Bounds(@@box_bounds);

var fltered = 'flr_id='
//var fltered1= "'@@filter'"
var filterTID = ' and tenantid='
var filterTID1 = null; //"'T10'".toUpperCase()

var filter = null;

var AutoSizeFramedCloud, reflat_lon, ref_marker;

$(document).ready(function () {
    AutoSizeFramedCloud = OpenLayers.Class(OpenLayers.Popup.FramedCloud, {
        'autoSize': true,
        'minSize': new OpenLayers.Size(150, 75),
        'maxSize': new OpenLayers.Size(550, 400)
    });
    $.getJSON('../api/Space_mapAPI/Gettenantid', function (result) {
        filterTID1 = result.toUpperCase();
    });
    var bboxurl = null;
    var markerurl = null;
    var parameters = null;
    if (GetParameterValues('Item') == null) {
        bboxurl = '../api/Space_mapAPI/GetFloorIDBBox?lcm_code=' + GetParameterValues('lcm_code') + '&twr_code=' + GetParameterValues('twr_code') + '&flr_code=' + GetParameterValues('flr_code');
        markerurl = '../api/Space_mapAPI/GetMapdetails';
        parameters = { lcm_code: GetParameterValues('lcm_code'), flr_code: GetParameterValues('flr_code'), twr_code: GetParameterValues('twr_code') };
    }
    else {
        bboxurl = '../api/Space_mapAPI/GetFloorIDBBoxByID?lcm_code=' + GetParameterValues('lcm_code') + '&twr_code=' + GetParameterValues('twr_code') + '&flr_code=' + GetParameterValues('flr_code')
        markerurl = '../api/Space_mapAPI/GetMapdetailsbyFilters'
        parameters = { Item: GetParameterValues('Item'), subitem: GetParameterValues('subitem'), lcm_code: GetParameterValues('lcm_code'), flr_code: GetParameterValues('flr_code'), twr_code: GetParameterValues('twr_code') };
    }
    loadmap(bboxurl, markerurl, parameters);
});

function loadmap(bboxurl, markerurl, parameters) {

    $.post(bboxurl, function (result) {
        var bounds = result[0].BBOX.split(",");
        box_bounds = new OpenLayers.Bounds(bounds[0], bounds[1], bounds[2], bounds[3]);
        fltered1 = result[0].FLR_ID;
        filter = fltered + "'" + fltered1 + "'" + filterTID + "'" + filterTID1 + "'";
        init();
        loadmarkers(markerurl, parameters);
    });
}

function loadmarkers(markerurl, parameters) {
    //var parameters = { Item: GetParameterValues('Item'), subitem: GetParameterValues('subitem'), lcm_code: GetParameterValues('lcm_code'), flr_code: GetParameterValues('flr_code'), twr_code: GetParameterValues('twr_code') };\\
    while (map.popups.length) {
        map.removePopup(map.popups[0]);
    }
    $.post(markerurl, parameters, function (result) {
        data = result;
        markers.clearMarkers();
        if ($('#ddlsearchfor').val() == 3) {
            if (data.length == 0) {
                notifymsg('No AD Data Integrated to this location');
                return;
            }
            for (var i = 0; i < data.length; i++) {
                //alert("Fddl" + $('#ddlsearchfor').val()+ " Sddl " + $('#ddlsearchforsub').val()+ " DataLen"+ data.length);
                if (data[i].Item2 != null) {
                    var Login_date = data[i].Item2.LOG_IN_TIME.replace(/-/g, "/");
                    var Logout_date = data[i].Item2.LOG_OUT_TIME.replace(/-/g, "/");
                }
                else {
                    var Login_date = "NA";
                    var Logout_date = "NA";
                }

                if ($('#ddlsearchforsub').val() == 10)
                    addMarker(new OpenLayers.LonLat(data[i].Item1.LAT, data[i].Item1.LON), AutoSizeFramedCloud, data[i].Item1.spc_display + ": " + "Last Log in Time:" + Login_date, true, true, 'img', data[i].Item1.SSA_STA_ID, data[i].Item1.SPC_LAYER, data[i].Item1.SPC_ID);
                else if ($('#ddlsearchforsub').val() == 11)
                    addMarker(new OpenLayers.LonLat(data[i].Item1.LAT, data[i].Item1.LON), AutoSizeFramedCloud, data[i].Item1.spc_display + ": " + "Last Log Off Time:" + Logout_date, true, true, 'img', data[i].Item1.SSA_STA_ID, data[i].Item1.SPC_LAYER, data[i].Item1.SPC_ID);
                else {
                   
                    var str = data[i].Item1.spc_display + ": " + "Last Log in Time:" + Login_date + " , " + "Last Log Off Time:" + Logout_date

                    if (data[i].Item1.SSA_STA_ID == 1)
                        str = data[i].Item1.SPC_ID + " : Vacant";
                    addMarker(new OpenLayers.LonLat(data[i].Item1.LAT, data[i].Item1.LON), AutoSizeFramedCloud, str, true, true, 'img', data[i].Item1.SSA_STA_ID, data[i].Item1.SPC_LAYER, data[i].Item1.SPC_ID);
                }
            }
        }
        else {
            if (data.length == 0) {
                if ($('#ddlsearchfor').val() == 1) {
                    notifymsg('No seats available');
                    return;
                }
                if ($('#ddlsearchfor').val() == 2) {
                    notifymsg('No seats allocated for the selected vertical');
                    return;
                }
                if ($('#ddlsearchfor').val() == 4) {
                    notifymsg('No swiped data for the selected floor');
                    return;
                }
            }

            for (var i = 0; i < data.length; i++) {
                addMarker(new OpenLayers.LonLat(data[i].LAT, data[i].LON), AutoSizeFramedCloud, data[i].spc_display, true, true, data[i].IMGTYPE, data[i].SSA_STA_ID, data[i].SPC_LAYER, data[i].SPC_ID);
            }
        }
    });
}



function init() {

    var format = "image/jpeg";
    var options = {
        controls: [],
        maxExtent: box_bounds,
        maxResolution: 2542.0234375,
        minZoomLevel: 5,
        numZoomLevels: 20,
        projection: "EPSG:4326",
        units: 'degrees'
    };

    map = new OpenLayers.Map('heatmapArea', options);

	
    var flrmaps = new OpenLayers.Layer.WMS("Map Layout", "http://166.78.47.48:8080/geoserver/wms?",
                 {
                     layers: 'quickamantra:floormaps', cql_filter: filter, format_options: 'antialias:text',
                     isBaseLayer: true
                 });

    var flrmapseat = new OpenLayers.Layer.WMS("Space Type", "http://166.78.47.48:8080/geoserver/wms?",
                { layers: 'quickamantra:multi_space_type', transparent: true, isBaseLayer: true, cql_filter: filter, format_options: 'antialias:text' });

    var flrmapshared = new OpenLayers.Layer.WMS("Seat Type", "http://166.78.47.48:8080/geoserver/wms?",
                 { layers: 'quickamantra:multi_seat_type', transparent: true, isBaseLayer: true, cql_filter: filter, format_options: 'antialias:text' });


    map.addLayer(flrmaps);
    map.addLayer(flrmapseat);
    map.addLayer(flrmapshared);
    //map.addLayer(flrmaps);
    flrmapshared.setVisibility(true);
    flrmaps.setVisibility(true);
    flrmapseat.setVisibility(false);


    map.addControl(new OpenLayers.Control.LayerSwitcher());
    //map.addControl(new OpenLayers.Control.OverviewMap()); 
    // build up all controls            
    map.addControl(new OpenLayers.Control.PanZoomBar({
        position: new OpenLayers.Pixel(1, 15)
    }));
    map.addControl(new OpenLayers.Control.Navigation());

    if (!map.getCenter())
        map.zoomToMaxExtent();

    markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);
    map.zoomToExtent(box_bounds);
}

function addMarker(lon_lat, popupClass, popupContentHTML, closeBox, overflow, imgurl, SSA_STA_ID, SPACE_TYPE, space_id) {
    var feature = new OpenLayers.Feature(markers, lon_lat);
    feature.closeBox = closeBox;
    feature.popupClass = popupClass;
    //feature.data.popupContentHTML = "<a onclick='maptoggle(\"" + space_id + "\",true,this)'>" + popupContentHTML + "</a>";
    if ($('#ddlsearchfor').val() == 3 || $('#ddlsearchfor').val() == 4) {
        feature.data.popupContentHTML = "" + popupContentHTML + "";
    }
    else {
        feature.data.popupContentHTML = "<a onclick='maptoggle(\"" + space_id + "\",true,this)'>" + popupContentHTML + "</a>";
    }
    
    //switch (SPACE_TYPE) {
    //    case "PRN": icon.imageDiv.className += SPACE_TYPE;
    //        break;
    //    case "COM": icon.imageDiv.className += SPACE_TYPE;
    //        break;
    //    case "TEL": icon.imageDiv.className += SPACE_TYPE;
    //        break;
    //    default: icon.imageDiv.className += imgurl + SSA_STA_ID;
    //        break;
    //}
    var imageurl = "";
    switch (SSA_STA_ID) {
        case 1: imageurl = "../images/Chair_Green.gif";
            break;
        case 6: imageurl = "../images/Chair_Blue.gif";
            break;
        case 7: imageurl = "../images/Chair_Red.gif";
            break;
        case 8: imageurl = "../images/Chair_Black.gif";
            break;
        default: imageurl = "../images/Chair_Green.gif";
            break;
    }
    if (imgurl != "img")
        imageurl = "../images/chair_red_blink.gif";

    var size = new OpenLayers.Size(20, 20);
    var icon = new OpenLayers.Icon(imageurl, size);

    feature.data.icon = icon;
    feature.data.overflow = (overflow) ? "auto" : "hidden";

    var marker = feature.createMarker();
    var markerClick = function (evt) {

        if (this.popup == null) {
            this.popup = this.createPopup(this.closeBox);
            map.addPopup(this.popup);
            //  map.setCenter(lon_lat, 8);
            ref_marker = evt.object;
            this.popup.show();
        } else {
            this.popup.toggle();
        }
        currentPopup = this.popup;
        OpenLayers.Event.stop(evt);
    };
    marker.events.register("mousedown", feature, markerClick);

    var size = new OpenLayers.Size(20, 20);
    var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);

    markers.addMarker(marker);
    //var size = new OpenLayers.Size(21, 25);
    //var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
    //var icon = new OpenLayers.Icon('http://projects.a-mantra.com/spacedemo/images/marker_red.png', size, offset);
    //var size = new OpenLayers.Size(21, 25);
    return marker;
}

function setzoomlevel(lon_lat, zoomlevel) {
    resetzoom();
    map.setCenter(lon_lat, map.getZoom() + zoomlevel);
}

function resetzoom() {
    map.zoomToExtent(box_bounds);
}

var popupatag = null;
function maptoggle(spc_id, toggle, atag) {
    if (spc_id == 'undefined') {
        return false;
    }

    // Set the effect type
    var effect = 'slide';
    // Set the options for the effect type chosen

    // Set the duration (default: 400 milliseconds)
    var duration = 500;

    $('#maptoggle').toggle(effect, { direction: "left" }, duration);
    $('#alloctoggle').toggle(effect, { direction: "right" }, duration, function () {
        $("#searchemppnl").hide();
        $("#divgrid").hide();
        spcid = spc_id;
        if (toggle) {
            //Clear Prev Control Values                          
            clearvalues();

            //start/end time hrs 
            startendtimehrs();

            // Vertical/Cost center labels
            GetlblVertical();

            //ddlshift binding
            GetURL();

            //ddlvertical binding
            GetVerticals();

            //
            fnBindSA()

            //Auto complete empid
            SearchText();

            $("#btnSubmit").hide();

            //gvAssets  binding
            fnbindgvAssets();
            popupatag = atag;
        }
    });
}

function maptoggle_marker(toggle) {
    resetzoom();
    var effect = 'slide';
    var duration = 500;


    $('#maptoggle').toggle(effect, { direction: "left" }, duration);
    $('#alloctoggle').toggle(effect, { direction: "right" }, duration, function () {
        if (toggle) {
            AllocArray = Array();
            OccArray = Array();
            var table = $('#gvSharedEmployee');
            var tableAS = $('#gvAllocatedSeats');
            var status = null;
            var spc_id = null;
            var popuptext = "";
			 var imgtype = 'img';
            if ($('#gvSharedEmployee td').length <= 1 && $('#gvAllocatedSeats td').length > 1 && spacetyp != 2) {
                status = 6;
                tableAS.find('tr').each(function (i) {
                    if (i != 0) {
                        var $tds = $(this).find('td'),
                            ename = $tds.eq(1).text(),
                            shift = "(" + $tds.eq(5).text() + "-" + $tds.eq(6).text() + ")",
                                spcid = $tds.eq(2).text();
                    }
                    if (ename != null)
                        popuptext += "Allocated To " + ename //+ " (" + shift+")";
                    spc_id = spcid;
                });
            }
            if ($('#gvSharedEmployee td').length > 1 && $('#gvAllocatedSeats td').length > 1 && spacetyp != 2) {
                status = 7;
                var prvshift, shift;
                table.find('tr').each(function (i) {
                    if (i != 0) {
                       
                        var $tds = $(this).find('td'),
                            ename = $tds.eq(0).text() + " / " + $tds.eq(1).text(),
                            shift = "(" + $tds.eq(6).text() + "-" + $tds.eq(7).text() + ")",
                                spcid = $tds.eq(3).text();
                        shift = "(" + $tds.eq(6).text() + "-" + $tds.eq(7).text() + ")";
                         if (prvshift = shift)
                                imgtype = 'blink';
                        prvshift = "(" + $tds.eq(6).text() + "-" + $tds.eq(7).text() + ")";
                    }
                    if (ename != null)
                        popuptext += "Occupied By " + ename  //+" (" + shift+")";
                    spc_id = spcid;
                });
            }
            if ($('#gvSharedEmployee td').length <= 1 && $('#gvAllocatedSeats td').length <= 1) {
                status = 1;
                spc_id = spcid;
                popuptext = spcid;
            }
            else if (spacetyp == 2) {
                status = 6;
                var ename = "";
                var shift = "";

                $("#gvAllocatedSeats tr").each(function (i, v) {
                    AllocArray[i] = Array();
                    $(this).children('td').each(function (ii, vv) {
                        AllocArray[i][ii] = $(this).text();
                    });
                })

                $("#gvSharedEmployee tr").each(function (i, v) {
                    OccArray[i] = Array();
                    $(this).children('td').each(function (ii, vv) {
                        OccArray[i][ii] = $(this).text();
                    });
                });
                var ename = "";
                var shift = "";
                if (OccArray[1][1] == undefined) {
                    status = 6;
                    for (i = 0; i < AllocArray.length; i++) {
                        if (i != 0) {
                            ename = AllocArray[i][1];
                            shift = AllocArray[i][5] + '-' + AllocArray[i][6];
                            spcid = AllocArray[i][2];
                        }
                        if (ename != "") {
                            popuptext += "Allocated To " + ename + " (" + shift + ") ";
                            spc_id = spcid;
                        }
                    }
                }
                else {
                    console.log(AllocArray);
                    status = 7;
                    for (allc = 0; allc < AllocArray.length; allc++) {
                        if (allc != 0) {

                            spcid = AllocArray[allc][2];
                            var alFrmtime = AllocArray[allc][5];
                            var res = Checktime(alFrmtime);
                            if (res == null)
                                popuptext += "Allocated To " + AllocArray[allc][1] + " (" + AllocArray[allc][5] + " - " + AllocArray[allc][6] + ") ";
                            else {
                                status = 7;
                                popuptext += res;
                                spc_id = spcid;
                            }
                        }
                    }
                }
            }
            for (i = 0; i < map.popups.length; i++) {
                map.popups[i].hide();
            }
            if (popuptext == spc_id)
                popuptext = spc_id + " : Vacant";
            else
                popuptext = spc_id + " : " + popuptext;

            markers.removeMarker(ref_marker);
            var mrkr = addMarker(ref_marker.lonlat, AutoSizeFramedCloud, popuptext, true, true, imgtype, status, spacetyp, spc_id);
            point_to_marker(mrkr, popuptext, spc_id, status);

            var ddldata = { lcm_code: GetParameterValues('lcm_code'), flr_code: $('#ddlfloors').val(), twr_code: GetParameterValues('twr_code'), mode: 0 };
            bindspacetypetabl(ddldata);
            bindvaluesbycategory($('#ddlfloors').val(), 0)
        }
    });
}

var AllocArray = Array();
var OccArray = Array();

function Checktime(frmtime) {
    console.log(OccArray);
    for (occ = 0; occ < OccArray.length; occ++) {
        if (occ != 0) {
            var ocfrmtime = OccArray[occ][6];
            if (frmtime == ocfrmtime) {
                return "Occupied By " + OccArray[occ][0] + "/" + OccArray[occ][1] + " (" + OccArray[occ][6] + " - " + OccArray[occ][7] + ") ";
            }
        }
    }
    return null;
}

function point_to_marker(marker, popuptext, spc_id, status) {

    //$('.olFramedCloudPopupContent').css('width', '');
    $('.olFramedCloudPopupContent').css('height', '');   //Disables Scrolling on popup

    //popupatag.innerHTML = popuptext;
    //marker.icon.imageDiv.className = 'img' + status;
    marker.events.triggerEvent('mousedown', {});
    setzoomlevel(new OpenLayers.LonLat(marker.lonlat.lon, marker.lonlat.lat), 1);
    //map.popups[0].hide();
    marker = null;
}

