<%@ WebHandler Language="VB" Class="StockReport" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System

Public Class StockReport : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim s As String = request("sidx")
        Dim sortColumnName As String = s.Replace("StockReport_list_", "")
        Dim sortOrderBy As String = request("sord")
        Dim lt As String = request("lt")
        Dim tw As String = request("tw")
        Dim ft As String = request("ft")
        Dim sa As String = request("as")
      
        Dim totalRecords As Integer
        Dim clsStockReport As Collection(Of clsStockReport) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, lt, tw, ft, sa)
       
       
        Dim output As String = BuildJQGridResults(clsStockReport, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        'response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
        response.Write(output)
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal lt As String, ByVal tw As String, ByVal ft As String, ByVal sa As String) As Collection(Of clsStockReport)
        Dim users As New Collection(Of clsStockReport)()
        Dim ds As New DataSet
       
        Dim param(8) As SqlParameter
     
        param(0) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(0).Value = pageIndex
        param(1) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(1).Value = sortColumnName
        param(2) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(2).Value = sortOrderBy
        param(3) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(3).Value = numberOfRows
        param(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(4).Value = totalRecords
        param(5) = New SqlParameter("@LCM_CODE ", SqlDbType.NVarChar, 200)
        param(5).Value = lt
        param(6) = New SqlParameter("@TWR_CODE ", SqlDbType.NVarChar, 200)
        param(6).Value = tw
        param(7) = New SqlParameter("@FLR_CODE ", SqlDbType.NVarChar, 200)
        param(7).Value = ft
        param(8) = New SqlParameter("@AIM_CODE ", SqlDbType.NVarChar, 200)
        param(8).Value = ""
       
        
        ds = ObjSubsonic.GetSubSonicDataSet("GET_STOCKREPORT_V", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsStockReport
                user.LCM_NAME = ds.Tables(0).Rows(i).Item("LCM_NAME").ToString
                user.TWR_NAME = ds.Tables(0).Rows(i).Item("TWR_NAME").ToString
                user.FLR_NAME = ds.Tables(0).Rows(i).Item("FLR_NAME").ToString
                user.AIM_NAME = ds.Tables(0).Rows(i).Item("AIM_NAME").ToString
                user.AID_AVLBL_QTY = ds.Tables(0).Rows(i).Item("AID_AVLBL_QTY").ToString
                user.REQ_QTY = ds.Tables(0).Rows(i).Item("REQ_QTY").ToString
               
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsStockReport As Collection(Of clsStockReport), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsStockReport In clsStockReport
            Dim row As New JQGridRow()
            row.cell = New String(5) {}
            row.id = Summary.LCM_NAME.ToString()
            row.cell(0) = Summary.LCM_NAME.ToString()
            row.cell(1) = Summary.TWR_NAME.ToString()
            row.cell(2) = Summary.FLR_NAME.ToString()
            row.cell(3) = Summary.AIM_NAME.ToString()
            row.cell(4) = Summary.AID_AVLBL_QTY.ToString()
            row.cell(5) = Summary.REQ_QTY.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function

End Class