<%@ WebHandler Language="VB" Class="GetdataByRequestID" %>
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System


Public Class GetdataByRequestID : Implements IHttpHandler, IRequiresSessionState 
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim id As String = request("ct")
        Dim strcallback As String = request("callback")
      
        Dim totalRecords As Integer
        Dim clsGetdataByRequestID As Collection(Of clsGetdataByRequestID) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, id)
       
       
        Dim output As String = BuildJQGridResults(clsGetdataByRequestID, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        ' response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
  
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal id As String) As Collection(Of clsGetdataByRequestID)
        Dim users As New Collection(Of clsGetdataByRequestID)()
        Dim ds As New DataSet
       
        Dim param(0) As SqlParameter
     
        param(0) = New SqlParameter("@id", SqlDbType.NVarChar, 200)
        param(0).Value = id
      
        'param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        'param(1).Value = pageIndex
        'param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        'param(2).Value = sortColumnName
        'param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        'param(3).Value = sortOrderBy
        'param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        'param(4).Value = numberOfRows
        'param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        'param(5).Value = totalRecords
        ds = ObjSubsonic.GetSubSonicDataSet("AST_GET_VIEW_ITEMREQUISITION_GRID_REPORT", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsGetdataByRequestID
                user.AID_REQ_ID = ds.Tables(0).Rows(i).Item("AID_REQ_ID").ToString
                user.AID_AVLBL_QTY = ds.Tables(0).Rows(i).Item("AID_AVLBL_QTY").ToString
                user.AID_REQ_QTY = ds.Tables(0).Rows(i).Item("AID_REQ_QTY").ToString
                user.AID_REQ_DT = ds.Tables(0).Rows(i).Item("AID_REQ_DT").ToString
                user.CRNTQTY = ds.Tables(0).Rows(i).Item("CRNTQTY").ToString
             
                
                users.Add(user)
                
            Next
            ExporttoCSV(id & "_" & ".csv", ds)
            Return users
            
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
        
    End Function
    
    Private Sub ExporttoCSV(ByVal FileName As String, ByVal ds As DataSet)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Buffer = True
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & FileName)
        HttpContext.Current.Response.Charset = ""
        HttpContext.Current.Response.ContentType = "application/text"
        'GridView1.AllowPaging = False
        'GridView1.DataBind()
        Dim sb As New StringBuilder()
        For k As Integer = 0 To ds.Tables(0).Columns.Count - 1 'GridView1.Columns.Count - 1
            'add separator
            sb.Append(ds.Tables(0).Columns(k).ColumnName + ","c)
        Next
        'append new line
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            For k As Integer = 0 To ds.Tables(0).Columns.Count - 1
                'add separator
                sb.Append(ds.Tables(0).Rows(i).Item(k) & ","c)
            Next
            'append new line
            sb.Append(vbCr & vbLf)
        Next
        HttpContext.Current.Response.Output.Write(sb.ToString())
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()
    End Sub
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsGetdataByRequestID As Collection(Of clsGetdataByRequestID), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsGetdataByRequestID In clsGetdataByRequestID
            Dim row As New JQGridRow()
            row.cell = New String(4) {}
            row.id = Summary.AID_REQ_ID.ToString()
            row.cell(0) = Summary.AID_REQ_ID.ToString()
            row.cell(3) = Summary.AID_AVLBL_QTY.ToString()
            row.cell(2) = Summary.AID_REQ_QTY.ToString()
            row.cell(1) = Summary.AID_REQ_DT.ToString()
            row.cell(4) = Summary.CRNTQTY.ToString()
            
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function
    

   
End Class