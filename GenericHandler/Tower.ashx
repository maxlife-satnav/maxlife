<%@ WebHandler Language="VB" Class="Tower" %>
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System
Public Class Tower : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strloc As String = request("city")
        Dim strcallback As String = request("callback")
        
        Dim totalRecords As Integer
        Dim clsTower As Collection(Of clsTower) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, strloc)
       
       
        Dim output As String = BuildJQGridResults(clsTower, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
       
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal strloc As String) As Collection(Of clsTower)
        Dim users As New Collection(Of clsTower)()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 200)
        param(0).Value = strloc
        ds = ObjSubsonic.GetSubSonicDataSet("GET_LOCTWR", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsTower
                
                user.TWR_CODE = ds.Tables(0).Rows(i).Item("TWR_CODE").ToString
                user.TWR_NAME = ds.Tables(0).Rows(i).Item("TWR_NAME").ToString
                
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsTower As Collection(Of clsTower), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResultsDrop()
        Dim rows As New List(Of JQGridRowDrop)()
        For Each Summary As clsTower In clsTower
            Dim row As New JQGridRowDrop()
            row.Row = New String(0) {}
            row.row(0) = "branch" & """:""" & Summary.TWR_NAME.ToString() & """,""" & "branch_code" & """:""" & Summary.TWR_CODE.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.total_rows = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function


End Class