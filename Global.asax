<%@ Application Language="VB" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="System.Web.SessionState" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Security" %>
<%@ Import Namespace="System.Security.SecurityManager" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="System.Data.DataRowView" %>
<%@ Import Namespace="System.Web.Http" %>


<script RunAt="server">


    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request

        If (Request.Cookies("userculture") Is Nothing) Then
            Response.Cookies.Add(New HttpCookie("userculture", "en-IN"))
        Else
            Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo(Request.Cookies("userculture").Value)
            Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = System.Globalization.DateTimeFormatInfo.InvariantInfo
        End If

        Dim requestUrl As String = Request.ServerVariables("REQUEST_URI")
        Dim rewriteUrl As String = Request.ServerVariables("UNENCODED_URL")
        If rewriteUrl IsNot Nothing Then
            If rewriteUrl.Contains("//") AndAlso Not requestUrl.Contains("//") Then
                Response.RedirectPermanent(requestUrl)
            End If
        End If

        'Response.AppendHeader("Refresh", Convert.ToString((20 * 60) + 10) & "; URL=" & VirtualPathUtility.ToAbsolute("~/login.aspx"))
    End Sub

    Sub Application_PreSendRequestHeaders()
        Response.Headers.[Set]("Server", "FooServer")
        Response.Headers.Remove("X-AspNet-Version")
    End Sub

    Public Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)

        RouteTable.Routes.MapHttpRoute(name:="DefaultApi", routeTemplate:="api/{controller}/{action}/{id}", defaults:=New With { _
          Key .id = System.Web.Http.RouteParameter.[Optional] _
      }).RouteHandler = New SessionStateRouteHandler()

        ' Code that runs on application startup 
        Application("AppURL") = "http://AmantraAxis/"
        Application("server_ip") = "mail.satnavtech.com"

        Application("FMGLogout") = "~/login.aspx"
        Application("Logout") = "~/login.aspx"

        Application("oldAppConStr") = ConfigurationManager.AppSettings("AmantraAxisFramework")
        Application("oldframeworkConstr") = ConfigurationManager.AppSettings("AmantraAxisFramework")
    End Sub

    Public Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown 
        If Response.Cookies.Count > 0 Then
            For Each s As String In Response.Cookies.AllKeys
                If s = FormsAuthentication.FormsCookieName OrElse s.ToLower() = "asp.net_sessionid" Then
                    Response.Cookies(s).Secure = True
                End If
            Next
        End If

    End Sub

    Public Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim strErrorMsg As String = ""
        Dim ex As System.Exception = Context.[Error].GetBaseException()
        ' string strStack = Context.Error.StackTrace; 
        Dim strURL As String = HttpContext.Current.Request.RawUrl.ToString()
        If strURL.IndexOf("?") > 0 Then
            strURL = strURL.Substring(0, strURL.IndexOf("?"))
        Else
            strURL = strURL
        End If
        Dim Host As String = Environment.MachineName
        Dim UserID As Integer = 0
        Dim httpCtx As HttpContext = HttpContext.Current
        Dim objSession As System.Web.SessionState.HttpSessionState = httpCtx.Session
        Dim StatusCode As Integer = 0
        Dim ErrorText As String
        Dim httpException As New HttpException()
        If httpException IsNot Nothing Then
            StatusCode = 500
        End If
        ErrorText = ex.Message
        ' string stackMsg = ex.ToString(); 
        'strErrorMsg = (strErrorMsg & "<b>#Page Name: </b>") + strURL & "<BR>"
        strErrorMsg = ((strErrorMsg & "<b>#Error Message:</b> ") + ex.Message & "") + ex.StackTrace

        If objSession IsNot Nothing Then

            Dim sp As New SubSonic.StoredProcedure("INSERT_ERROR_LOG")
            sp.Command.AddParameter("@ERR_MSG", ex.Message, DbType.String)
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session("Uid"), DbType.String)
            sp.Command.AddParameter("@URL_NAME", strURL, DbType.String)
            sp.ExecuteScalar()

        End If

        'Context.Items.Add("ErrorSession", strErrorMsg)
        'Server.Transfer("~/ErrorPage.aspx")
    End Sub

    Public Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started 
        Session("serverip") = "mail.satnavtech.com"
        Session("temp") = ""
        Session("Key") = ""
        Session("PageURL") = ""
        Session("trace") = ""
        Session("MSG") = ""
        Session("Entered") = "No"
        Session("TENANT") = ""
        ' To check whether the user entered his login or not .... 
    End Sub

    Public Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'Dim spUserId As SqlParameter = New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 50)
            'spUserId.Value = Session("uid")
            'SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "USP_UPDATE_LOGIN_INFO", spUserId)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_UPDATE_LOGIN_INFO")
            sp.Command.AddParameter("@VC_USERID", Session("uid"), DbType.String)
            sp.ExecuteScalar()
            Session.RemoveAll()
            Response.Redirect("../Default.aspx")
        Catch ex As Exception
            'Throw New AmantraAxis.Exception.DataException("This error has been occured while Updating data", "TopFrame", "Load", ex)
        End Try

        Session.RemoveAll()
    End Sub

    Public Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        Dim str As String = ""

        '' Fires upon attempting to authenticate the user 
        'Dim Cookiename As String = FormsAuthentication.FormsCookieName
        '' To Create New Cookie 
        ''HttpCookie authCookie =new HttpCookie(Request.Cookies[Cookiename].Value) ; 
        ''HttpCookie authCookie = (HttpCookie)Request.Cookies[Cookiename].Value; 
        'Dim authCookie As New HttpCookie(Cookiename)
        'If authCookie Is Nothing Then
        '    Exit Sub
        'End If
        'Dim authTicket As FormsAuthenticationTicket = Nothing
        'Try
        '    authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        'Catch ex As Exception
        '    Exit Sub
        'End Try
        'If authTicket Is Nothing Then
        '    Exit Sub
        'End If
        'Dim roles As String() = authTicket.UserData.Split(New Char() {"|"c})
        'Dim id As New FormsIdentity(authTicket)
        'Dim principal As New GenericPrincipal(id, roles)
        'Context.User = principal
        ''Context.User = New GenericPrincipal(Context.User.Identity , roles) 
    End Sub
    Protected Sub Application_AcquireRequestState(sender As Object, e As EventArgs)
        Dim contextVal As HttpContext = HttpContext.Current
        If contextVal IsNot Nothing AndAlso contextVal.Session IsNot Nothing Then
            Context.Session("UID") = Session("UID")
            Context.Session("LoginUniqueID") = Session("LoginUniqueID")
            '  Context.Session("usr_" + Session("UID")) = HttpContext.Current.Session.SessionID
            If Session("UID") IsNot Nothing AndAlso Session("LoginUniqueID") IsNot Nothing Then
                Dim UsrSta As Integer
                UsrSta = Global.clsMasters.GetUserSession(Session("UID"))

                If UsrSta = 0 Or UsrSta = -1 Then ' IF USRSTA IS 0 THEN SESSION IS EXPIRED
                    Global.clsMasters.doLogout()

                End If
            Else

            End If
        End If
    End Sub

</script>

