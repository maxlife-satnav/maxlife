﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="page-wrapper" class="row">
    <div class="row form-wrapper">
        <form id="form1" class="form-horizontal" runat="server">
            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                <legend style="padding-top: 0px; width: 21%">Asset Management</legend>

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Asset Types - Count
                            </div>
                            <div class="panel-body">
                                <div id="AssetTypesCntContainer"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Mapped - Unmapped Capital Assets
                            </div>
                            <div class="panel-body">
                                <div id="MMAssets"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</html>

<script type="text/javascript">

    //Asset Types pie chart
    funfillAssetTypes();

    //Mapped Unmapped Assets
    funfillMapUnmapAssets();

    function funfillAssetTypes() {
        var chart = c3.generate({
            data: {
                columns: [],
                type: 'pie',
            },
            pie: {
                expand: false,
                label: {
                    format: function (value, ratio, id) {
                        return d3.format('')(value);
                    }
                }

            },
            axis: {
                x: {
                    position: 'outer-center',
                },
            }
        });

        $.ajax({
            url: '../api/AssetDBAPI/GetAssetTypes',
            type: 'GET',
            success: function (result) {
                chart.load({ columns: result });
            }
        });
        $("#AssetTypesCntContainer").append(chart.element);
    }
    
    function funfillMapUnmapAssets() {
        var chart = c3.generate({
            data: {
                columns: [],
                type: 'donut',
            },
            donut: {
                expand: false,
                label: {
                    format: function (value, ratio, id) {
                        return d3.format('')(value);
                    }
                }

            },
            axis: {
                x: {
                    position: 'outer-center',
                },
            }
        });

        $.ajax({
            url: '../api/AssetDBAPI/GetMapunmapassets',
            type: 'GET',
            success: function (result) {
                chart.load({ columns: result });
            }
        });
        $("#MMAssets").append(chart.element);
    }
</script>