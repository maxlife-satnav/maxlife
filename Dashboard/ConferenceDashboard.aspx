﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="page-wrapper" class="row">
    <div class="row form-wrapper">
        <form id="form1" class="form-horizontal" runat="server">
            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                <legend style="padding-top: 0px; width: 27%">Conference Management</legend>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart fa-fw"></i>Conferences Utilization for last 7 days
                            </div>
                            <div class="panel-body">
                                <div id="confUtlContainer"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                City-Wise Conferences Count
                            </div>
                            <div class="panel-body">
                                <div id="confCountContainer"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                City-Wise Conferences Utilization (Current Month) 
                            </div>
                            <div class="panel-body">
                                <div id="donutCointainer"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>
</html>
<script type="text/javascript">

    //City-Wise Conferences Count
    funcitywiseconfCount();

    //City-Wise Conferences Utilization (Current Month)
    funcitywiseconfUtil();

    //Conf utilization - last 7 days
    funConfUtilization();

    function funcitywiseconfCount() {
        var chart = c3.generate({
            data: {
                columns: [],
                type: 'pie',
            },
            pie: {
                expand: false,
                label: {
                    format: function (value, ratio, id) {
                        return d3.format('')(value);
                    }
                }

            },
            axis: {
                x: {
                    position: 'outer-center',
                },
            }
        });

        $.ajax({
            url: '../api/ConferenceDBAPI/GetcitywiseconfCount',
            type: 'GET',
            success: function (result) {
                chart.load({ columns: result });
            }
        });
        $("#confCountContainer").append(chart.element);
    }

    function funcitywiseconfUtil() {
        var chart = c3.generate({
            data: {
                columns: [],
                type: 'donut',
            },
            donut: {
                expand: false,
                label: {
                    format: function (value, ratio, id) {
                        return d3.format('')(value) + ' hr(s)';
                    }
                }

            },
            axis: {
                x: {
                    position: 'outer-center',
                },
            }
        });

        $.ajax({
            url: '../api/ConferenceDBAPI/GetcitywiseUtil',
            type: 'GET',
            success: function (result) {
                chart.load({ columns: result });
            }
        });
        $("#donutCointainer").append(chart.element);
    }

    function funConfUtilization() {
        $.ajax({
            url: '../api/ConferenceDBAPI/GetConfUtil',
            type: 'GET',
            success: function (result) {
                var chart = c3.generate({
                    data: {
                        x: 'x',
                        columns: result,
                        type: 'spline'
                    },
                    axis: {
                        x: {
                            type: 'timeseries',
                            tick: {
                                format: '%Y-%m-%d'
                            },
                            show: true,
                            label: {
                                text: 'Date',
                                position: 'outer-center'
                            }
                        },
                        y: {
                            show: true,
                            label: {
                                text: 'Hours Utilized',
                                position: 'outer-middle'
                            }
                        },
                    },
                });
                $("#confUtlContainer").append(chart.element);
            },
            failure: function (response) {

            }
        });
    }

</script>
