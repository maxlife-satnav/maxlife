﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO
Partial Class DashboardV2
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim mode As Integer
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        ' redirect it to your login page
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If

        If Not Page.IsPostBack Then
            Try
                GetTenantModules()
            Catch ex As Exception
            Finally
            End Try
        End If
    End Sub
    Private Sub GetTenantModules()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Tenant_Module")
        sp.Command.AddParameter("@TID", Session("TENANT"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        space.Visible = False
        [property].Visible = False
        asset.Visible = False
        conference.Visible = False
        maintenance.Visible = False
        helpdesk.Visible = False
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr1 As DataRow In ds.Tables(0).Rows
                Select Case dr1("T_MODULE")

                    Case "Space"
                        space.Visible = True

                    Case "Property"
                        [property].Visible = True

                    Case "Asset"
                        asset.Visible = True

                    Case "Conference"
                        conference.Visible = True

                    Case "Maintenance"
                        maintenance.Visible = True

                    Case "Helpdesk"
                        helpdesk.Visible = True
                    Case Else

                End Select
            Next
        End If

    End Sub
End Class
