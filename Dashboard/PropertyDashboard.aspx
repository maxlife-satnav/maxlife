﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style>
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        @-webkit-keyframes hvr-wobble-top {
            16.65% {
                -webkit-transform: skew(-12deg);
                transform: skew(-12deg);
            }

            33.3% {
                -webkit-transform: skew(10deg);
                transform: skew(10deg);
            }

            49.95% {
                -webkit-transform: skew(-6deg);
                transform: skew(-6deg);
            }

            66.6% {
                -webkit-transform: skew(4deg);
                transform: skew(4deg);
            }

            83.25% {
                -webkit-transform: skew(-2deg);
                transform: skew(-2deg);
            }

            100% {
                -webkit-transform: skew(0);
                transform: skew(0);
            }
        }

        @keyframes hvr-wobble-top {
            16.65% {
                -webkit-transform: skew(-12deg);
                transform: skew(-12deg);
            }

            33.3% {
                -webkit-transform: skew(10deg);
                transform: skew(10deg);
            }

            49.95% {
                -webkit-transform: skew(-6deg);
                transform: skew(-6deg);
            }

            66.6% {
                -webkit-transform: skew(4deg);
                transform: skew(4deg);
            }

            83.25% {
                -webkit-transform: skew(-2deg);
                transform: skew(-2deg);
            }

            100% {
                -webkit-transform: skew(0);
                transform: skew(0);
            }
        }

        .hvr-wobble-top {
            display: inline-block;
            vertical-align: middle;
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
            box-shadow: 0 0 1px rgba(0, 0, 0, 0);
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -moz-osx-font-smoothing: grayscale;
            -webkit-transform-origin: 0 100%;
            transform-origin: 0 100%;
        }

            .hvr-wobble-top:hover, .hvr-wobble-top:focus, .hvr-wobble-top:active {
                -webkit-animation-name: hvr-wobble-top;
                animation-name: hvr-wobble-top;
                -webkit-animation-duration: 1s;
                animation-duration: 1s;
                -webkit-animation-timing-function: ease-in-out;
                animation-timing-function: ease-in-out;
                -webkit-animation-iteration-count: 1;
                animation-iteration-count: 1;
            }

        @-webkit-keyframes hvr-pulse-grow {
            to {
                -webkit-transform: scale(1.1);
                transform: scale(1.1);
            }
        }

        @keyframes hvr-pulse-grow {
            to {
                -webkit-transform: scale(1.1);
                transform: scale(1.1);
            }
        }

        .hvr-pulse-grow {
            display: inline-block;
            vertical-align: middle;
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
            box-shadow: 0 0 1px rgba(0, 0, 0, 0);
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -moz-osx-font-smoothing: grayscale;
        }

            .hvr-pulse-grow:hover, .hvr-pulse-grow:focus, .hvr-pulse-grow:active {
                -webkit-animation-name: hvr-pulse-grow;
                animation-name: hvr-pulse-grow;
                -webkit-animation-duration: 0.3s;
                animation-duration: 0.3s;
                -webkit-animation-timing-function: linear;
                animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;
                animation-iteration-count: infinite;
                -webkit-animation-direction: alternate;
                animation-direction: alternate;
            }

        @-webkit-keyframes hvr-buzz-out {
            10% {
                -webkit-transform: translateX(3px) rotate(2deg);
                transform: translateX(3px) rotate(2deg);
            }

            20% {
                -webkit-transform: translateX(-3px) rotate(-2deg);
                transform: translateX(-3px) rotate(-2deg);
            }

            30% {
                -webkit-transform: translateX(3px) rotate(2deg);
                transform: translateX(3px) rotate(2deg);
            }

            40% {
                -webkit-transform: translateX(-3px) rotate(-2deg);
                transform: translateX(-3px) rotate(-2deg);
            }

            50% {
                -webkit-transform: translateX(2px) rotate(1deg);
                transform: translateX(2px) rotate(1deg);
            }

            60% {
                -webkit-transform: translateX(-2px) rotate(-1deg);
                transform: translateX(-2px) rotate(-1deg);
            }

            70% {
                -webkit-transform: translateX(2px) rotate(1deg);
                transform: translateX(2px) rotate(1deg);
            }

            80% {
                -webkit-transform: translateX(-2px) rotate(-1deg);
                transform: translateX(-2px) rotate(-1deg);
            }

            90% {
                -webkit-transform: translateX(1px) rotate(0);
                transform: translateX(1px) rotate(0);
            }

            100% {
                -webkit-transform: translateX(-1px) rotate(0);
                transform: translateX(-1px) rotate(0);
            }
        }

        @keyframes hvr-buzz-out {
            10% {
                -webkit-transform: translateX(3px) rotate(2deg);
                transform: translateX(3px) rotate(2deg);
            }

            20% {
                -webkit-transform: translateX(-3px) rotate(-2deg);
                transform: translateX(-3px) rotate(-2deg);
            }

            30% {
                -webkit-transform: translateX(3px) rotate(2deg);
                transform: translateX(3px) rotate(2deg);
            }

            40% {
                -webkit-transform: translateX(-3px) rotate(-2deg);
                transform: translateX(-3px) rotate(-2deg);
            }

            50% {
                -webkit-transform: translateX(2px) rotate(1deg);
                transform: translateX(2px) rotate(1deg);
            }

            60% {
                -webkit-transform: translateX(-2px) rotate(-1deg);
                transform: translateX(-2px) rotate(-1deg);
            }

            70% {
                -webkit-transform: translateX(2px) rotate(1deg);
                transform: translateX(2px) rotate(1deg);
            }

            80% {
                -webkit-transform: translateX(-2px) rotate(-1deg);
                transform: translateX(-2px) rotate(-1deg);
            }

            90% {
                -webkit-transform: translateX(1px) rotate(0);
                transform: translateX(1px) rotate(0);
            }

            100% {
                -webkit-transform: translateX(-1px) rotate(0);
                transform: translateX(-1px) rotate(0);
            }
        }

        .hvr-buzz-out {
            display: inline-block;
            vertical-align: middle;
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
            box-shadow: 0 0 1px rgba(0, 0, 0, 0);
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -moz-osx-font-smoothing: grayscale;
        }

            .hvr-buzz-out:hover, .hvr-buzz-out:focus, .hvr-buzz-out:active {
                -webkit-animation-name: hvr-buzz-out;
                animation-name: hvr-buzz-out;
                -webkit-animation-duration: 0.75s;
                animation-duration: 0.75s;
                -webkit-animation-timing-function: linear;
                animation-timing-function: linear;
                -webkit-animation-iteration-count: 1;
                animation-iteration-count: 1;
            }


        @-webkit-keyframes hvr-wobble-to-top-right {
            16.65% {
                -webkit-transform: translate(8px, -8px);
                transform: translate(8px, -8px);
            }

            33.3% {
                -webkit-transform: translate(-6px, 6px);
                transform: translate(-6px, 6px);
            }

            49.95% {
                -webkit-transform: translate(4px, -4px);
                transform: translate(4px, -4px);
            }

            66.6% {
                -webkit-transform: translate(-2px, 2px);
                transform: translate(-2px, 2px);
            }

            83.25% {
                -webkit-transform: translate(1px, -1px);
                transform: translate(1px, -1px);
            }

            100% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0);
            }
        }

        @keyframes hvr-wobble-to-top-right {
            16.65% {
                -webkit-transform: translate(8px, -8px);
                transform: translate(8px, -8px);
            }

            33.3% {
                -webkit-transform: translate(-6px, 6px);
                transform: translate(-6px, 6px);
            }

            49.95% {
                -webkit-transform: translate(4px, -4px);
                transform: translate(4px, -4px);
            }

            66.6% {
                -webkit-transform: translate(-2px, 2px);
                transform: translate(-2px, 2px);
            }

            83.25% {
                -webkit-transform: translate(1px, -1px);
                transform: translate(1px, -1px);
            }

            100% {
                -webkit-transform: translate(0, 0);
                transform: translate(0, 0);
            }
        }

        .hvr-wobble-to-top-right {
            display: inline-block;
            vertical-align: middle;
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
            box-shadow: 0 0 1px rgba(0, 0, 0, 0);
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -moz-osx-font-smoothing: grayscale;
        }

            .hvr-wobble-to-top-right:hover, .hvr-wobble-to-top-right:focus, .hvr-wobble-to-top-right:active {
                -webkit-animation-name: hvr-wobble-to-top-right;
                animation-name: hvr-wobble-to-top-right;
                -webkit-animation-duration: 1s;
                animation-duration: 1s;
                -webkit-animation-timing-function: ease-in-out;
                animation-timing-function: ease-in-out;
                -webkit-animation-iteration-count: 1;
                animation-iteration-count: 1;
            }
    </style>
</head>
<body>
    <div>
        <form id="form1" runat="server">

            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-inverse" id="btnLease">
                        <i class="fa fa-check" aria-hidden="true" id="selectedLease"></i>&nbsp Lease
                    </button>
                </div>

                <div class="col-md-3">
                    <button type="button" class="btn btn-inverse" id="btnTenant">
                        <i class="fa fa-check" aria-hidden="true" id="selectedTenant"></i>&nbsp Tenant
                    </button>
                </div>

            </div>

            <div class="clearfix">
                <h5 id="selectedDashboard" class="col-md-2"></h5>
            </div>
            <hr />
            <br />
            <div>
                <%--Lease Tag--%>
                <div id="divLease">
                    <%--Lease PrintProp Functionality--%>

                    <div id="Lease">
                        <div class="clearfix">
                            <div class="col-md-8">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                                        <i class="fa fa-bar-chart fa-fw"></i>Property Units by City
                            
                                    </div>
                                    <div align="right">
                                        <button type="button" class="btn btn-default btn-sm" onclick="Print('citywiseproperties')" style="color: rgba(58, 156, 193, 0.87)">
                                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                        </button>
                                    </div>
                                    <div class="panel-body">
                                        <div id="citywiseproperties"></div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />

                            <div class="col-md-3 col-md-6">

                                <div class="panel panel-danger">
                                    <div class="panel-heading" style="height: 88px">
                                        <div class="row">
                                            <div style="font-size: 12px" class="col-md-3">
                                                <i class="fa fa-warning fa-5x hvr-pulse-grow"></i>
                                            </div>
                                            <div class="col-md-9 text-center">
                                                <div>
                                                    <label class="huge" id="lblexpleasesCnt"></label>
                                                    <div></div>
                                                    <label style="font-size: 11px;" class="small">About to Expire Leases</label>
                                                    <label style="font-size: 11px" class="small">(Current Month)</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" id="AncExpLeases1">
                                        <div class="panel-footer" role="alert" data-toggle="modal" data-target="#ExpLeases1">
                                            <span class="hvr-icon-wobble-horizontal">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">

                                <div class="panel panel-danger">
                                    <div class="panel-heading" style="height: 88px">
                                        <div class="row">
                                            <div style="font-size: 12px" class="col-md-3">
                                                <i class="fa fa-shopping-cart fa-5x hvr-wobble-top"></i>
                                            </div>
                                            <div class="col-md-9 text-center">
                                                <div>
                                                    <label class="huge" id="lblleaseDues"></label>
                                                    <div></div>
                                                    <label style="font-size: 11px" class="small">Lease Dues</label>
                                                    <label style="font-size: 11px" class="small">(Current Year)</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" id="AncLeasesDues1">
                                        <div class="panel-footer" role="alert" data-toggle="modal" data-target="#divLeaseCurrentYear">
                                            <span class="hvr-icon-wobble-horizontal">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <%--Tenant Tag--%>
                <div id="divTenant">
                    <%--Tenant Print Functionality--%>

                    <div id="Tenant">
                        <div class="clearfix">
                            <div class="col-md-3">
                                <button type="button" id="btnVacantProperties" class="btn btn-info">
                                    Vacant Properties
                            <span class="badge" id="spnVacantProperties"></span>
                                </button>
                            </div>

                            <div class="col-md-3">
                                <button type="button" id="btnUpcomingVacancies" class="btn btn-danger">
                                    Upcoming Vacancies
                            <span class="badge" id="spnUpcomingVacancies"></span>
                                </button>
                            </div>

                        </div>
                        <br />
                        <div class="clearfix">
                            <div class="col-md-3 col-md-6">
                                <div class="panel panel-danger">
                                    <div class="panel-heading" style="height: 88px">
                                        <div class="row">
                                            <div style="font-size: 12px" class="col-md-3">
                                                <i class="fa fa-shopping-cart fa-5x hvr-wobble-top"></i>
                                            </div>
                                            <div class="col-md-9 text-center">
                                                <div>
                                                    <label class="huge" id="lblTenantDues"></label>
                                                    <div></div>
                                                    <label style="font-size: 11px" class="small">Tenant Dues(Current Month)</label>
                                                    <%--<label style="font-size: 11px" class="small"></label>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" id="tenantDues">
                                        <div class="panel-footer" style="height: 40px" role="alert" data-toggle="modal" data-target="#receivedAmtDetails">
                                            <span class="hvr-icon-wobble-horizontal">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-3 col-md-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading" style="height: 88px">
                                        <div class="row">
                                            <div style="font-size: 12px" class="col-md-3">
                                                <i class="fa fa-tasks fa-5x hvr-buzz-out"></i>
                                            </div>
                                            <div class="col-md-9 text-center">
                                                <label class="huge" id="lblwrtotcount"></label>
                                                <div></div>
                                                <label style="font-size: 11px" class="small">Total Work Request(Current Month)</label>
                                                <%--<label style="font-size: 11px" class="small"></label>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" id="ancWorkReqTotal">
                                        <div class="panel-footer" style="height: 40px" role="alert" data-toggle="modal" data-target="#TotalWRS">
                                            <span class="hvr-icon-wobble-horizontal">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading" style="height: 88px">
                                        <div class="row">
                                            <div style="font-size: 12px" class="col-md-3">
                                                <i class="fa fa-check-square-o fa-5x hvr-wobble-to-top-right"></i>
                                            </div>
                                            <div class="col-md-8 text-center">
                                                <label class="huge" id="lblWRclosed"></label>
                                              
                                                <label style="font-size: 11px" class="small">Completed Work Request(Current Month)</label>
                                                <%--<label style="font-size: 11px" class="small"></label>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" id="ancWorkReqCompleted">
                                        <div class="panel-footer" style="height: 40px" role="alert" data-toggle="modal" data-target="#CompletedWRS">
                                            <span class="hvr-icon-wobble-horizontal">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="panel panel-danger">
                                    <div class="panel-heading" style="height: 88px">
                                        <div class="row">
                                            <div style="font-size: 12px" class="col-md-3">
                                                <i class="fa fa-shopping-cart fa-5x  hvr-wobble-top"></i>
                                            </div>
                                            <div class="col-md-9 text-center">
                                                <div>
                                                    <label class="huge" id="lblpending"></label>
                                                    <div></div>
                                                    <label style="font-size: 11px" class="small">Pending Work Requests(Current Month)</label>
                                                    <%--<label style="font-size: 11px" class="small">(Current Month)</label>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" id="ancWorkReqPending">
                                        <div class="panel-footer" style="height: 40px" role="alert" data-toggle="modal" data-target="#PendingWRS">
                                            <span class="hvr-icon-wobble-horizontal">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix">

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                                <i class="fa fa-pie-chart fa-fw"></i>Properties Count
                            </div>
                            <div align="right">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('propertycontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                            <div class="panel-body">
                                <div id="propertycontainer"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" id="divTenantBarChart">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Received and Pending Amounts (Current Month)
                            </div>
                            <div align="right">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('rentscontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                            <div class="panel-body">
                                <div id="rentscontainer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="divLeaseBarChart">
                        <div class="panel panel-default">
                             <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Received and Pending Amounts (Current Year)
                            </div>
                            <div align="right">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('LeaseDuecontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                            <div class="panel-body">
                                <div id="LeaseDuecontainer">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <%--   <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-bar-chart fa-fw"></i>Expenses Utilized by Location                                 
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('expenseutilitycontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <div id="expenseutilitycontainer"></div>
                        </div>
                    </div>
                </div>
            </div>--%>

            <div class="modal fade" id="TotalWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H2">Work Requests Raised This Month</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tbltotalWRS')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tbltotalWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Requested Date</th>
                                    <th>Requested By</th>
                                    <th>Estimated Amount</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="CompletedWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H1">Completed Work Requests</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblCompletedWRS')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblCompletedWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Requested Date</th>
                                    <th>Requested By</th>
                                    <th>Last Updated</th>
                                    <th>Estimated Amount</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="PendingWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H3">Pending Work Requests</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblPendingWRS')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblPendingWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Requested Date</th>
                                    <th>Requested By</th>
                                    <th>Last Updated</th>
                                    <th>Estimated Amount</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ExpLeases1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H4">Leases Expiring This Month</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblExpLeasesThisMonth')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblExpLeasesThisMonth" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Lease Start Date</th>
                                    <th>Lease End Date</th>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Monthly Rent</th>
                                    <th>Security Deposit</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divLeaseCurrentYear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H10">Lease Dues This Year</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblLeaseDuesThisYr')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblLeaseDuesThisYr" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Lease </th>
                                    <th>Property</th>
                                    <th>Total Rent</th>
                                    <th>Paid Date</th>
                                    <th>Agreement Start Date</th>
                                    <th>Agreement End Date</th>

                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="PropDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H5">Property Details <span id="selectedTypeProp"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblPropDetails')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblPropDetails" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Property Name</th>
                                    <th>Location Name</th>
                                    <th>Last Updated</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="receivedAmtDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H6">Amount Details <span id="Span1"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('Table0')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="Table0" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Property Name</th>
                                    <th>Tenant Name</th>
                                    <th>Tenant Email</th>
                                    <th>Rent Amount</th>
                                </tr>
                            </table>

                            <table id="Table1" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Request ID</th>
                                    <th>Property Type</th>
                                    <th>Property Name</th>
                                    <th>Work Title</th>
                                    <th>Estimated Amount</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Phone</th>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="DivCityWiseProperties" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H7">Properties by City <span id="Span2"></span></h4>
                        </div>

                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblcityprops')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblcityprops" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>City Name</th>
                                    <th>Location Name</th>
                                    <th>Prop. Type</th>
                                    <th>Prop. Name</th>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="divVacantProperties" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H8">Property Details <span id="Span3"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblVacantProps')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblVacantProps" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Location Name</th>
                                    <th>Prop. Type</th>
                                    <th>Prop. Code</th>
                                    <th>Prop. Name</th>
                                    <th>Owner Name</th>
                                    <th>Ph. No.</th>
                                    <th>Last Updated Dt.</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divUpcomingVacancies" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H9">Tenant Details <span id="Span4"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblUpcomingVacancies')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblUpcomingVacancies" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Prop. Type</th>
                                    <th>Prop. Name</th>
                                    <th>Tenant Name</th>
                                    <th>Tenant Code</th>
                                    <th>Tenant Phone</th>
                                    <th>Tenant Email</th>
                                    <th>Rent Amount</th>
                                    <th>Security Deposit</th>
                                    <th>Joined Date</th>
                                    <th>End Date</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divBarGraph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="divBarGraphHeading"></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblBarGraph')">
                                    <span class="glyphicon glyphicon-print"></span>Print
                                </button>
                            </div>
                            <table id="tblBarGraph" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Location</th>
                                    <th>Expense Head</th>
                                    <th>Bill Date</th>
                                    <th>Bill No.</th>
                                    <th>Bill Invoice</th>
                                    <th>Amount</th>
                                    <th>Bill Generated By</th>
                                    <th>Remarks</th>


                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
<script type="text/javascript">
    var CompanySessionId = '<%= Session("COMPANYID")%>'
    var year
    $(document).ready(function () {
        year = (new Date().getFullYear()).toString();
    });
    $(document).ready(function () {
        $("#TenantCompany").change(function () {
            CompanySessionId = $(this).val();
            var CompanySessionId = CompanySessionId;
            funfillProperties(CompanySessionId);
            FunBindAmounts(CompanySessionId);
            funExpLeases(CompanySessionId);
            vacantProperties(CompanySessionId);
            tenanctVacancies(CompanySessionId);
            //ExpenseUtilityChart(CompanySessionId);
        });

        $("#LeaseCompany").change(function () {
            FuncBindLeaseDuesDet(CompanySessionId);
            funfillProperties(CompanySessionId);
            FunBindAmounts(CompanySessionId);
            funExpLeases(CompanySessionId);
            vacantProperties(CompanySessionId);
            tenanctVacancies(CompanySessionId);
            //ExpenseUtilityChart(CompanySessionId);
        });
        //ExpenseUtilityChart(CompanySessionId);
        //Properties Count
        funfillProperties(CompanySessionId);
        citywise_props();
        $('#divTenant').hide();
        $('#divTenantBarChart').hide();
        // $('#divLeaseBarChart').hide();

        $('#selectedTenant').hide();

        $('#selectedDashboard').text("Lease Dashboard");



        $('#btnTenant').click(function () {
            $('#selectedDashboard').text("Tenant Dashboard");
            $('#selectedTenant').show();
            $('#selectedLease').hide();
            $('#divLease').hide();
            $('#divTenant').show();
            $('#divLeaseBarChart').hide();
            $('#divTenantBarChart').show();

            tenanctVacancies(CompanySessionId);
            vacantProperties(CompanySessionId);


        });

        $('#btnLease').click(function () {
            $('#selectedDashboard').text("Lease Dashboard");
            $('#selectedTenant').hide();
            $('#selectedLease').show();
            $('#divTenant').hide();
            $('#divLease').show();
            $('#divTenantBarChart').hide();
            $('#divLeaseBarChart').show();
        });


        //Pending/Received Amounts

        FunBindAmounts(CompanySessionId);
        //LeaseDues
        FuncBindLeaseDuesDet(CompanySessionId);
        // properties tenant
        //funfillProperties_Tenant();

        //Work Request Count Binding
        GetWRCount("1");

        //Exp leases count/Details
        funExpLeases(CompanySessionId);

        

      
        var companyid = '<%= Session("COMPANYID")%>'
        //fillPropertyCompany(companyid);
        
        //Properties Count func
        function funfillProperties(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            var comp = param.companyId
            var properties = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" } },
                    // onclick: function (d, i) { console.log("onclick", d, i); },
                    //onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    //onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                    onclick: function (e) {
                        $("#PropDetails").modal("show");
                        GetPropertyDetailsByPropName(e.id);
                    }
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/PropertyDBAPI/BindProperties',
                data: param,
                type: 'POST',
                success: function (result) {
                    properties.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#propertycontainer").html("");
                $("#propertycontainer").append(properties.element);
                $("#propertycontainer").load();
            }, 1000);
        }

        //Pending/Received Amounts Bar chart  tenant    
        function FunBindAmounts(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/PropertyDBAPI/BindAmounts',
                type: 'POST',
                data: param,
                success: function (summarydata) {
                    $('#lblTenantDues').html(summarydata.ChartData[1].PendingReq);
                    var chart2 = c3.generate({
                        data: {
                            columns: [["Rents", summarydata.ChartData[0].TotalReq, summarydata.ChartData[1].PendingReq, summarydata.ChartData[2].WRExpenses, 0]],
                            type: 'bar',
                            onclick: function (e) {
                                $("#receivedAmtDetails").modal("show");
                                PendingAndReceivedAmounts(e.index);
                            },
                        },
                        bar: {
                            width: {
                                ratio: 0.5
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                categories: ["Total Rents Received", "Pending Rents", "Work Request Expenses", "Other Expenses"],
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Amount',
                                    position: 'outer-middle'
                                }
                            },

                        },
                        bindto: '#rentscontainer'
                    });
                }
            });
        }

        //Lease dues bar chart

        function FuncBindLeaseDuesDet(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/PropertyDBAPI/GetLeaseDueDetails',
                type: 'POST',
                data: param,
                success: function (summarydata) {
                    console.log(summarydata);
                    $('#lblleaseDues').html(summarydata.ChartData[1].PendingReq);
                    var chart2 = c3.generate({
                        data: {
                            columns: [["Rents", summarydata.ChartData[0].TotalReq, summarydata.ChartData[1].PendingReq, 0, 0]],
                            type: 'bar',
                            onclick: function (e) {
                                $("#divLeaseCurrentYear").modal("show");
                                funLeasesDues(e.index);
                            }

                        },
                        bar: {
                            width: {
                                ratio: 0.5
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                categories: ["Total Amount Received", " Lease Due", "Maintenance Expenses", "Other Expenses"],
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Amount',
                                    position: 'outer-middle'
                                }
                            },

                        },
                        bindto: '#LeaseDuecontainer'
                    });
                }
            });
        }


        function GetWRCount(type) {
            $.ajax({
                url: "../api/PropertyDBAPI/GetWorkRequests",
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                    if (type == "1") {
                        $("#lblwrtotcount").html(data.Table.length);
                        $("#lblWRclosed").html(data.Table1.length);
                        $("#lblpending").html(data.Table2.length);
                    }
                    //Total WR
                    if (type == "2") {
                        var table = $('#tbltotalWRS');
                        $('#tbltotalWRS td').remove();
                        for (var i = 0; i < data.Table.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data.Table[i].PN_NAME + "</td>" +
                                "<td>" + data.Table[i].BDG_ID + "</td>" +
                                "<td>" + data.Table[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table[i].AUR_FIRST_NAME + "</td>" +
                                "<td>" + data.Table[i].ESTIMATED_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.Table.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Work Requests Raised this Month</td>" + "</tr>");
                        }
                    }
                    //Completed
                    if (type == "3") {
                        var table = $('#tblCompletedWRS');
                        $('#tblCompletedWRS td').remove();
                        for (var i = 0; i < data.Table1.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data.Table1[i].PN_NAME + "</td>" +
                                "<td>" + data.Table1[i].BDG_ID + "</td>" +
                                "<td>" + data.Table1[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table1[i].AUR_FIRST_NAME + "</td>" +
                                "<td>" + data.Table1[i].LAST_UPDATED_DATE + "</td>" +
                                "<td>" + data.Table1[i].ESTIMATED_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.Table1.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Completed Requests Found this Month</td>" + "</tr>");
                        }
                    }

                    //Pending
                    if (type == "4") {
                        var table = $('#tblPendingWRS');
                        $('#tblPendingWRS td').remove();
                        for (var i = 0; i < data.Table2.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data.Table2[i].PN_NAME + "</td>" +
                                "<td>" + data.Table2[i].BDG_ID + "</td>" +
                                "<td>" + data.Table2[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table2[i].AUR_FIRST_NAME + "</td>" +
                                "<td>" + data.Table2[i].LAST_UPDATED_DATE + "</td>" +
                                "<td>" + data.Table2[i].ESTIMATED_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.Table2.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }


        function GetPropertyDetailsByPropName(type) {
            var param = { Type: type, companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetPropDetailsByPropName",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    var table = $('#tblPropDetails');
                    $('#tblPropDetails td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].PN_NAME + "</td>" +
                            "<td>" + data[i].LCM_NAME + "</td>" +
                            "<td>" + data[i].LAST_UPDATE_DATE + "</td>" +
                            //"<td>" + data.Table2[i].LAST_UPDATED_DATE + "</td>" +
                            //"<td>" + data.Table2[i].ESTIMATED_AMOUNT + "</td>" +
                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                    }

                },
                error: function (result) {
                }
            });
        }
        //Tenant View Details
        function PendingAndReceivedAmounts(type) {
            var param = { Type: type, companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetPendingAndReceivedAmounts",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#Table0').show();
                    $('#Table1').show();


                    if (type == 0 || type == 1) {
                        var table = $('#Table0');
                        $('#Table0 td').remove();
                        $('#Table1').hide();
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].PN_NAME + "</td>" +
                                "<td>" + data[i].TEN_NAME + "</td>" +
                                "<td>" + data[i].TEN_EMAIL + "</td>" +
                                "<td>" + data[i].TEN_OUTSTANDING_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                        }
                    }
                    else if (type == 2) {
                        $('#Table0').hide();
                        var table = $('#Table1');
                        $('#Table1 td').remove();
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].PN_WORKREQUEST_REQ + "</td>" +
                                "<td>" + data[i].PN_PROPERTYTYPE + "</td>" +
                                "<td>" + data[i].PN_NAME + "</td>" +
                                "<td>" + data[i].WORK_TITLE + "</td>" +
                                "<td>" + data[i].ESTIMATED_AMOUNT + "</td>" +
                                "<td>" + data[i].VENDOR_NAME + "</td>" +
                                "<td>" + data[i].VENDOR_PHONE + "</td>" +
                                "</tr>");
                        }
                        if (data.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                        }

                    }
                    else {
                    }

                },
                error: function (result) {
                }
            });
        }


        //View Leases and count
        function funExpLeases(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetExpLeasesDetails",
                data: param,
                //contentType: "application/json; charset=utf-8",
                empty: { label: { text: "No Data Available" } },
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $("#lblexpleasesCnt").html(data.length);
                    var table = $('#tblExpLeasesThisMonth');
                    $('#tblExpLeasesThisMonth td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].LEASE_START_DATE + "</td>" +
                            "<td>" + data[i].LEASE_END_DATE + "</td>" +
                            "<td>" + data[i].PN_NAME + "</td>" +
                            "<td>" + data[i].BDG_ID + "</td>" +
                            "<td>" + data[i].MONTHLY_RENT + "</td>" +
                            "<td>" + data[i].SECURITY_DEPOSIT + "</td>" +
                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='7' align='center'> No Leases to Expire This Month </td>" + "</tr>");
                    }
                }
            });
        }
        // Lease Dues details
        function funLeasesDues(type) {
            $.ajax({
                url: "../api/PropertyDBAPI/GetDueLeaseReceivedAmounts",
                data: { "category": type },
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    //$("#lblleaseDues").html(data.length);
                    var table = $('#tblLeaseDuesThisYr');
                    $('#tblLeaseDuesThisYr td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +

                            "<td>" + data[i].LEASE + "</td>" +
                             "<td>" + data[i].PROPERTY + "</td>" +
                            "<td>" + data[i].TOTAL_RENT + "</td>" +
                             "<td>" + data[i].PAID_DATE + "</td>" +
                            "<td>" + data[i].EFFECTIVE_AGREEMENT_DATE + "</td>" +
                            "<td>" + data[i].EXTESNION_TODATE + "</td>" +

                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='7' align='center'> No Lease Dues Found This Year </td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }


        function citywise_properties() {

            $.ajax({
                url: '../api/PropertyDBAPI/BindCityWise_Properties',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    var chart2 = c3.generate({

                        data: {
                            columns: data, //[['BANG', 30],['IND', 130]],
                            type: 'bar',
                        },
                        //bar: {
                        //    width: {
                        //        ratio: 0.5 // this makes bar width 50% of length between ticks
                        //    }
                        //},
                        axis: {
                            x: {
                                show: true,
                                label: {
                                    text: 'Cities',
                                }
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Count of Properties',
                                    position: 'outer-middle'
                                }
                            },
                        }

                    });
                    $("#citywiseproperties").append(chart2.element);

                },
                error: function (result) {

                }
            });
        }

        // city wise properties - pie
        function citywise_props() {
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" } },
                    onclick: function (e) {
                        $("#DivCityWiseProperties").modal("show");
                        // $('#selectedType').text(e.id);
                        GetPropertiesByCity(e.id);
                    }
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/PropertyDBAPI/BindCityWise_Properties',
                type: 'GET',
                success: function (result) {
                    chart.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#citywiseproperties").append(chart.element);
            }, 1000);
        }


        function GetPropertiesByCity(type) {

            $.ajax({
                url: "../api/PropertyDBAPI/GetProperties_City",
                data: { "cityName": type },
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    var table = $('#tblcityprops');
                    $('#tblcityprops td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                             "<td>" + data[i].CTY_NAME + "</td>" +
                            "<td>" + data[i].LCM_NAME + "</td>" +
                            "<td>" + data[i].PN_PROPERTYTYPE + "</td>" +
                            "<td>" + data[i].PN_NAME + "</td>" +
                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='4' align='center'> No Records Found</td>" + "</tr>");
                    }

                },
                error: function (result) {
                }
            });
        }


        //Total Work Requests count and View Details
        $("#ancWorkReqTotal").click(function (e) {
            e.preventDefault();
            GetWRCount("2");
        });

        //Total Work Requests count and View Details
        $("#ancWorkReqCompleted").click(function (e) {
            e.preventDefault();
            GetWRCount("3");
        });

        //Pending Work Requests count and View Details
        $("#ancWorkReqPending").click(function (e) {
            e.preventDefault();
            GetWRCount("4");
        });

        //AncExpLeases
        $("#AncExpLeases1").click(function (e) {
            e.preventDefault();
            funExpLeases(CompanySessionId);
        });
        //Lease Dues
        $("#AncLeasesDues1").click(function (e) {

            e.preventDefault();
            funLeasesDues(1);
        });

        //Tenant Dues
        $("#tenantDues").click(function (e) {
            e.preventDefault();
            PendingAndReceivedAmounts(1);
        });


        //vacant Properties
        $("#btnVacantProperties").click(function (e) {
            e.preventDefault();
            $("#divVacantProperties").modal("show");
            vacantProperties(CompanySessionId);

        });

        //upcoming vacancies
        $("#btnUpcomingVacancies").click(function (e) {
            e.preventDefault();
            $("#divUpcomingVacancies").modal("show");
            tenanctVacancies(CompanySessionId);
        });

        // tenant vacancies count and view details
        function tenanctVacancies(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetTenant_Vacancies",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $("#spnUpcomingVacancies").html(data.Table[0].NOOFTENANTS);
                    var table = $('#tblUpcomingVacancies');
                    $('#tblUpcomingVacancies td').remove();
                    for (var i = 0; i < data.Table1.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table1[i].PN_PROPERTYTYPE + "</td>" +
                            "<td>" + data.Table1[i].PN_NAME + "</td>" +
                            "<td>" + data.Table1[i].TEN_NAME + "</td>" +
                            "<td>" + data.Table1[i].TEN_CODE + "</td>" +
                            "<td>" + data.Table1[i].TEN_PHNO + "</td>" +
                            "<td>" + data.Table1[i].TEN_EMAIL + "</td>" +
                            "<td>" + data.Table1[i].RENT + "</td>" +
                            "<td>" + data.Table1[i].DEPOSIT + "</td>" +
                            "<td>" + data.Table1[i].TEN_JOINED_DATE + "</td>" +
                            "<td>" + data.Table1[i].TEN_END_DATE + "</td>" +
                            "</tr>");
                    }
                    if (data.Table1.length == 0) {
                        table.append("<tr>" + "<td colspan='10' align='center'> No Records Found </td>" + "</tr>");
                    }
                }
            });
        }

        //vacant properties count and details
        function vacantProperties(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/Get_Vacant_Properties",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $("#spnVacantProperties").html(data.Table[0].VACANT_PROPERTIES);
                    var table = $('#tblVacantProps');
                    $('#tblVacantProps td').remove();
                    for (var i = 0; i < data.Table1.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table1[i].LCM_NAME + "</td>" +
                            "<td>" + data.Table1[i].PN_PROPERTYTYPE + "</td>" +
                            "<td>" + data.Table1[i].PROP_CODE + "</td>" +
                            "<td>" + data.Table1[i].PN_NAME + "</td>" +
                            "<td>" + data.Table1[i].OWNERNAME + "</td>" +
                            "<td>" + data.Table1[i].PHNO + "</td>" +
                            "<td>" + data.Table1[i].LAST_UPDATED_DATE + "</td>" +
                            "</tr>");
                    }
                    if (data.Table1.length == 0) {
                        table.append("<tr>" + "<td colspan='7' align='center'> No Records Found </td>" + "</tr>");
                    }
                }
            });
        }

        return false;


        function ExpenseUtilityChart(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/PropertyDBAPI/ExpenseUtilityChartByLoc',
                data: param,
                //contentType: "application/json; charset=utf-8",
                empty: { label: { text: "No Data Available" } },
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    var chart2 = c3.generate({

                        data: {
                            json: JSON.parse(data.locVal),
                            keys: {
                                x: 'name',
                                value: JSON.parse(data.services),
                            },
                            type: 'bar',
                            onclick: function (e) {
                                $("#divBarGraph").modal("show");
                                var selectedService = this.categories()[e.index]
                                $('#divBarGraphHeading').html(e.id + "  - " + "(" + selectedService + ")");
                                GetExpenseUtilityDetailsByLoc(e.id, selectedService);
                            }
                        },
                        axis: {
                            x: {
                                type: 'category'
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Amount',
                                    position: 'outer-middle'
                                }
                            },
                        }
                    });
                    $("#expenseutilitycontainer").html("");
                    $("#expenseutilitycontainer").append(chart2.element);
                    $("#expenseutilitycontainer").load();
                },
                error: function (result) {
                }
            });
        }


        function GetExpenseUtilityDetailsByLoc(expense, location) {
            p = {
                location: location,
                expense: expense
            }
            $.ajax({
                url: "../api/PropertyDBAPI/GetExpenseUtilityDetailsByLoc",
                data: p,
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    var table = $('#tblBarGraph');
                    $('#tblBarGraph td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                        "<td>" + data[i].LOCATION + "</td>" +
                        "<td>" + data[i].EXPENSEHEAD + "</td>" +
                        "<td>" + data[i].BILLDATE + "</td>" +
                        "<td>" + data[i].BILL_NO + "</td>" +
                        "<td>" + data[i].BILL_INVOICE + "</td>" +
                        "<td>" + data[i].AMOUNT + "</td>" +
                        "<td>" + data[i].AURID + "</td>" +
                        "<td>" + data[i].REMARKS + "</td>" +

                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'>No Records Found.</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }
        function DDLDisable(compid) {
            if (compid == "1") {
                $("#TenantCompany").prop('disabled', false);
            }
            else {
                $("#TenantCompany").prop('disabled', true);
                $('#TenantCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }

        function LeaseCompanyDisable(compid) {
            if (compid == "1") {
                $("#LeaseCompany").prop('disabled', false);
            }
            else {
                $("#LeaseCompany").prop('disabled', true);
                $('#LeaseCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }
        setTimeout(function () { DDLDisable(companyid) }, 700);
        setTimeout(function () { LeaseCompanyDisable(companyid) }, 700);
    });
</script>
