﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="page-wrapper" class="row">
    <div class="row form-wrapper">
        <form id="form1" class="form-horizontal" runat="server">
            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                <legend style="padding-top: 0px; width: 27%">Guest House Booking</legend>
                <div id="ghdashboard">



                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('ghdashboard')">
                                <span class="glyphicon glyphicon-print hvr-icon-bounce" style="color: rgba(58, 156, 193, 0.87)"></span><span style="color: rgba(58, 156, 193, 0.87)">Print All</span>
                            </button>
                        </div>
                    </div>



                    <div class="row" id="facilityCount">
                        <div class="col-md-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Facilities Count by Location
                                   
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-sm" onclick="Print('facilityCount')">
                                        <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>
                                    </button>
                                </div>
                                <div class="panel-body">
                                    <div id="GHfacCountContainer"></div>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fa fa-tasks fa-5x"></i>
                                        </div>
                                        <div class="col-md-9 text-center">
                                            <div class="huge">
                                                <label id="lblwrtotcount"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            Total Facilities Booked
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            (Current Month)
                                        </div>
                                    </div>
                                </div>
                                <a href="#" id="ancWorkReqTotal">
                                    <div class="panel-footer" role="alert" data-toggle="modal" data-target="#TotalWRS">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>

                            </div>
                        </div>
                        <div class="col-md-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <i class="fa fa-check-square-o fa-5x"></i>
                                        </div>
                                        <div class="col-md-9 text-center">
                                            <div class="huge">
                                                <label id="lblWRclosed"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            Facilities Withhold
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            (Current Month)
                                        </div>
                                    </div>
                                </div>
                                <a href="#" id="ancWorkReqCompleted">
                                    <div class="panel-footer" role="alert" data-toggle="modal" data-target="#CompletedWRS">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>


                    </div>

                    <div class="modal fade" id="PropDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <%-- <h4 class="modal-title" id="H5">Facility Details <span id="selectedTypeProp"></span></h4>--%>
                                    <h4 class="modal-title" id="HeaderFacility"></h4>
                                </div>
                                <div class="modal-body">
                                    <div class=" col-md-3 pull-left">
                                        <button type="button" class="btn btn-default btn-sm" onclick="Print('tblPropDetails')">
                                            <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                        </button>
                                    </div>
                                    <table id="tblPropDetails" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>

                                            <th>Facility Type</th>
                                            <th>Facility Name</th>
                                            <%--<th>Location</th>--%>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="divBarGraph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <%--<h4 class="modal-title" id="H6">Guest House Booking details <span id="Span1"></span></h4>--%>
                                    <h4 class="modal-title" id="divBarGraphHeading"></h4>

                                </div>
                                <div class="modal-body">
                                    <div class=" col-md-3 pull-left">
                                        <button type="button" class="btn btn-default btn-sm" onclick="Print('tblBarGraph')">
                                            <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                        </button>
                                    </div>
                                    <table id="tblBarGraph" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>

                                            <th>Reference ID</th>
                                            <th>Facility Type</th>
                                            <th>Facility Name</th>
                                            <th>Room Name/Number</th>
                                            <th>Location</th>
                                            <th>Fromdate</th>
                                            <th>Todate</th>
                                            <th>FromTime</th>
                                            <th>ToTime</th>
                                            <th>Booked Date</th>
                                            <th>Booked By</th>
                                        </tr>
                                    </table>



                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="divbargraph">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart fa-fw"></i>Guset House Utilization for Last 30 Days
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default btn-sm" onclick="Print('divbargraph')">
                                        <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>
                                    </button>
                                </div>
                                <div class="panel-body">
                                    <div id="GHUtlContainer"></div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="modal fade" id="TotalWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="H2">Total Facilities Booked This Month</h4>
                                </div>
                                <div class="modal-body" style="padding: 16px">

                                    <div class=" col-md-3 pull-left">
                                        <button type="button" class="btn btn-default btn-sm" onclick="Print('tbltotalWRS')">
                                            <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                        </button>
                                    </div>

                                    <table id="tbltotalWRS" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>
                                            <th>Reference ID</th>
                                            <th>Facility Type</th>
                                            <th>Facility Name</th>
                                            <th>Room Name/Number</th>
                                            <th>Location</th>
                                            <th>Fromdate</th>
                                            <th>Todate</th>
                                            <th>FromTime</th>
                                            <th>ToTime</th>
                                            <th>Booked Date</th>
                                            <th>Booked By</th>

                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="CompletedWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="H1">Facilities Withhold This Month</h4>
                                </div>
                                <div class="modal-body" style="padding: 16px">
                                     <div class=" col-md-3 pull-left">
                                        <button type="button" class="btn btn-default btn-sm" onclick="Print('tblCompletedWRS')">
                                            <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                        </button>
                                    </div>
                                    <table id="tblCompletedWRS" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>
                                            <th>Reference ID</th>
                                            <th>Facility Type</th>
                                            <th>Facility Name</th>
                                            <th>Room Name/Number</th>
                                            <th>Location</th>
                                            <th>Fromdate</th>
                                            <th>Todate</th>
                                            <th>FromTime</th>
                                            <th>ToTime</th>
                                            <th>Booked Date</th>
                                            <th>Booked By</th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</html>
<script src="Scripts/jQuery.print.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        GetWRCount()
        GetWithHoldReqCount()
        //Work Request Count Binding
        //GetWRCount("1");
        //City-Wise Conferences Count
        // funLocationwiseFacilitiesCount();
        //  setTimeout(function () {


        funLocationwiseFacilitiesCount();
        // }, 2000);


        //City-Wise Conferences Utilization (Current Month)


        //Conf utilization - last 7 days
        funGuestHouseUtilization();


        function funLocationwiseFacilitiesCount() {
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" } },
                    onclick: function (e) {
                        $("#PropDetails").modal("show");

                        //var selectedService = this.categories()[e.index]
                        //$('#divBarGraphHeading').html("Guest House Request details -"+e.id + "  - " + "(" + selectedService + ")");
                        $('#HeaderFacility').html("Facilities Details -" + e.id);

                        GetPropertyDetailsByPropName(e.id);
                    }
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/GuestHouseDBAPI/GetFacilitiesCountbyLocation',
                type: 'GET',
                success: function (result) {
                    chart.load({ columns: result });
                }
            });
            $("#GHfacCountContainer").append(chart.element);
        }



        function funGuestHouseUtilization() {
            $.ajax({
                url: "../api/GuestHouseDBAPI/GHUtilization",
                type: 'GET',
                success: function (data) {

                    var chart2 = c3.generate({
                        data: {
                            json: JSON.parse(data.locVal),
                            keys: {
                                x: 'name',
                                value: JSON.parse(data.services),
                            },
                            type: 'bar',
                            onclick: function (e) {

                                $("#divBarGraph").modal("show");
                                //console.log(this.categories());
                                var selctedLocation = this.categories()[e.index]
                                console.log(selctedLocation);
                                //$('#divBarGraphHeading').html("Guest House Request details -"+e.id + "  - " + "(" + selectedService + ")");
                                $('#divBarGraphHeading').html("Guest House Request details -" + e.id);
                                //GetGHDetailsForBarChart(e.id, selectedService);
                                console.log(e.id);

                                GetGHDetailsForBarChart(e.id, selctedLocation);
                            }

                        },
                        axis: {
                            x: {
                                type: 'category'
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'No.of Requests',
                                    position: 'outer-middle'
                                }
                            },
                        }
                    });
                    $("#GHUtlContainer").append(chart2.element);
                },
                failure: function (response) {

                }
            });

        }


        function GetGHDetailsForBarChart(type, selectedLocation) {

            $.ajax({
                url: "../api/GuestHouseDBAPI/GetGHDetailsForBarChart",
                data: { "Category": type, "location": selectedLocation },
                //data: p,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    var table = $('#tblBarGraph');
                    $('#tblBarGraph td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].REFERENCE_ID + "</td>" +
                            "<td>" + data[i].RT_NAME + "</td>" +
                            "<td>" + data[i].RF_NAME + "</td>" +
                            "<td>" + data[i].RR_NAME + "</td>" +
                            "<td>" + data[i].LCM_NAME + "</td>" +
                            "<td>" + data[i].FROM_DATE + "</td>" +
                            "<td>" + data[i].TO_DATE + "</td>" +
                            "<td>" + data[i].FRM_TIME + "</td>" +
                            "<td>" + data[i].TO_TIME + "</td>" +
                            "<td>" + data[i].REQUESTED_DATE + "</td>" +
                            "<td>" + data[i].REQ_BY + "</td>" +

                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='11' align='center'>No Records Found.</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }

        function GetPropertyDetailsByPropName(type) {

            $.ajax({
                url: "../api/GuestHouseDBAPI/GetFacilitiesforPie",
                data: { "lcmName": type },
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    var table = $('#tblPropDetails');
                    $('#tblPropDetails td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].RT_NAME + "</td>" +
                            "<td>" + data[i].RF_NAME + "</td>" +
                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                    }

                },
                error: function (result) {
                }
            });
        }

        function GetWRCount() {
            $.ajax({
                url: "../api/GuestHouseDBAPI/GetTotalRequestsCount",
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                    $("#lblwrtotcount").html(data);

                },
                error: function (result) {
                }
            });
        }




        function GetRequestDetails() {
            $.ajax({
                url: "../api/GuestHouseDBAPI/GetTotalRequestsDetails",
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                    //$("#lblwrtotcount").html(data);
                    var table = $('#tbltotalWRS');
                    $('#tbltotalWRS td').remove();
                    for (var i = 0; i < data.Table.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table[i].BM_REFERENCE_ID + "</td>" +
                            "<td>" + data.Table[i].RT_NAME + "</td>" +
                            "<td>" + data.Table[i].RF_NAME + "</td>" +
                            "<td>" + data.Table[i].RR_NAME + "</td>" +
                               "<td>" + data.Table[i].LCM_NAME + "</td>" +
                            "<td>" + data.Table[i].BM_FROM_DATE + "</td>" +
                               "<td>" + data.Table[i].BM_TO_DATE + "</td>" +
                                "<td>" + data.Table[i].BM_FRM_TIME + "</td>" +
                               "<td>" + data.Table[i].BM_TO_TIME + "</td>" +

                                     "<td>" + data.Table[i].REQUESTED_DATE + "</td>" +
                                        "<td>" + data.Table[i].REQ_BY + "</td>" +

                            "</tr>");
                    }
                    if (data.Table.length == 0) {
                        table.append("<tr>" + "<td colspan='11' align='center'> No Requests Raised this Month</td>" + "</tr>");
                    }
                    //if (type == "1") {
                    //    $("#lblwrtotcount").html(data.Table.length);
                    //    //$("#lblWRclosed").html(data.Table1.length);
                    //    //$("#lblpending").html(data.Table2.length);
                    //}
                },
                error: function (result) {
                }
            });
        }
        //Total Work Requests count and View Details
        $("#ancWorkReqTotal").click(function (e) {
            e.preventDefault();
            GetRequestDetails();
        });

        //Total Work Requests count and View Details
        $("#ancWorkReqCompleted").click(function (e) {
            e.preventDefault();
            GetWithHoldReqDetails();
        });

        function GetWithHoldReqCount() {
            $.ajax({
                url: "../api/GuestHouseDBAPI/GetWithHoldRequestsCount",
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                    $("#lblWRclosed").html(data);
                    //if (type == "1") {
                    //    $("#lblWRclosed").html(data.Table.length);
                    //    //$("#lblWRclosed").html(data.Table1.length);
                    //    //$("#lblpending").html(data.Table2.length);
                    //}
                },
                error: function (result) {
                }
            });
        }
        function GetWithHoldReqDetails() {
            $.ajax({
                url: "../api/GuestHouseDBAPI/GetWithHoldRequestsDetails",
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                    //$("#lblwrtotcount").html(data);
                    var table = $('#tblCompletedWRS');
                    $('#tblCompletedWRS td').remove();
                    for (var i = 0; i < data.Table.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table[i].WM_REFERENCE_ID + "</td>" +
                            "<td>" + data.Table[i].RT_NAME + "</td>" +
                            "<td>" + data.Table[i].RF_NAME + "</td>" +
                            "<td>" + data.Table[i].RR_NAME + "</td>" +
                            //"<td>" + data.Table[i].CNY_NAME + "</td>" +
                            //   "<td>" + data.Table[i].CTY_NAME + "</td>" +
                                  "<td>" + data.Table[i].LCM_NAME + "</td>" +

                                   "<td>" + data.Table[i].WM_FROM_DATE + "</td>" +
                               "<td>" + data.Table[i].WM_TO_DATE + "</td>" +
                                "<td>" + data.Table[i].WM_FRM_TIME + "</td>" +
                               "<td>" + data.Table[i].WM_TO_TIME + "</td>" +

                                     "<td>" + data.Table[i].REQUESTED_DATE + "</td>" +
                                        "<td>" + data.Table[i].REQ_BY + "</td>" +

                            "</tr>");
                    }
                    if (data.Table.length == 0) {
                        table.append("<tr>" + "<td colspan='11' align='center'> No WithHold Requests Raised this Month</td>" + "</tr>");
                    }
                    //if (type == "1") {
                    //    $("#lblwrtotcount").html(data.Table.length);
                    //    //$("#lblWRclosed").html(data.Table1.length);
                    //    //$("#lblpending").html(data.Table2.length);
                    //}
                },
                error: function (result) {
                }
            });
        }



    });
</script>
<script type="text/javascript">
    function Print(divName) {
        var year
        $(document).ready(function () {
            year = (new Date().getFullYear()).toString();
        });
        $('#' + divName).print({
            globalStyles: true,
            mediaPrint: false,
            stylesheet: "http://fonts.googleapis.com/css?family=Inconsolata",
            iframe: true,
            prepend: "<h3>QuickFMS</h3> <hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-bottom: 0.5em;''margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'>" + "<br/>",
            append: "<hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'> <h6 align='center'>© QuickFMS " + year + " </h6>"
        });

    }
</script>
