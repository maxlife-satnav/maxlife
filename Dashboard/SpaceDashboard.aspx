﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SpaceDashboard.aspx.cs" Inherits="Dashboard_SpaceDashboard" %>

<style>
    #spce .c3 path, .c3 line {
        stroke: #F0AE7E !important;
    }
</style>
<div id="spce">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <form id="form1" class="form-horizontal" runat="server">
                <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                    <legend style="padding-top: 0px; width: 21%">Space Management</legend>
                    <div>
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <label id="ltlWorkstation"></label>
                            </div>
                          <%--  <div class="col-md-3 text-center">
                                <label id="ltlCabin"></label>
                            </div>--%>
                              <div class="col-md-3 text-center">
                                <label id="ltlShift"></label>
                            </div>
                            <div class="col-md-3 text-center">
                                <label id="ltlFTE"></label>
                            </div>
                          
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div id="chartws" style="width: 100%"></div>
                        </div>
                      <%--  <div class="col-md-3">
                            <div id="chartcb" style="width: 100%"></div>
                        </div>--%>   
                        <div class="col-md-3">
                            <div id="chartseatutil" style="width: 100%"></div>
                        </div>                   
                        <div class="col-md-3">
                            <div id="chartFTE" style="width: 100%"></div>
                        </div>
                          
                    </div>
                </fieldset>
               <%-- <div class="row">
                    <h3 style="font-family: 'Ebrima'; font-size: 20px; font-weight: bold; color: black;">Locations</h3>
                    <hr style="display: block; height: 1px; border: 0; border-top: 1px solid #000; margin: 2em 0; padding: 0;" />
                    <div>               
                        <asp:datalist id="DataList1" runat="server" repeatdirection="Vertical" repeatcolumns="3" width="100%">
                        <ItemTemplate>                           
                            <div class="form-group">
                                <div class="col-md-5">
                                    <asp:Label ID="Literal1" runat="server" Style="font-family: 'Ebrima'; font-size: 14px; font-weight: bold; color: black;" Text='<%# Eval("lcm_name")%>'></asp:Label>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label ID="ltlggn" runat="server" Text='<%# Eval("lcm_addr")%>' Style="font-family: Ebrima; font-size: 12px"></asp:Label>
                                </div>
                            </div>                        
                        </ItemTemplate>
                    </asp:datalist>
                    </div>
                </div>--%>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //Bind Workstation
        funBindWorkstation();

        //Bind cabins
        funbindCabin();

        //Bindoccupied by bu
        funbindShift();

        //Bind FTE
        funbindfunbindFTE();

        //total
        function funBindWorkstation() {
            var chart = c3.generate({
                setTimeout: 0,
                size: {
                    height: 480
                },
                data: {
                    x: 'x',
                    columns: [
                    ],
                    type: 'bar',
                    color: function (color, d) {
                        return '#FF9D3B';
                    }
                },
                axis: {
                    rotated: true,
                    x: {
                        type: 'category',
                        tick: {
                            rotate: 75,
                            multiline: false
                        },
                        height: 250
                    },
                    y: {
                        show: false,
                    },
                },
            });

            $.ajax({
                url: '../api/SpaceDBAPI/bindWorkstation',
                type: 'GET',
                success: function (result) {
                    chart.load(
                        {
                            columns: result.ws,
                            //[['x', 'IND', 'MADHAPUR  ', 'AMPT', 'KP', 'MP', 'PHIL', 'QQW', 'AAAA'],
                            //['Total Workstations', 1340, 120, 550, 450, 220, 525, 440, 85]]                            
                        });
                    $("#ltlWorkstation").html("<strong>Total Workstations</strong> <br/>" + result.ttlcount);
                }
            });
            $("#chartws").append(chart.element);
        }

        //occupied by BU
        function funbindShift() {
            var chartshift = c3.generate({
                setTimeout: 0,
                size: {
                    height: 480
                },
                data: {
                    x: 'x',
                    columns: [
                    ],
                    type: 'bar',
                    color: function (color, d) {
                        return '#FF9D3B';
                    }
                },
                axis: {
                    rotated: true,
                    x: {
                        type: 'category',
                        tick: {
                            rotate: 75,
                            multiline: false
                        },
                        height: 250
                    },
                    y: {
                        show: false,
                    },
                },
            });

            $.ajax({
                url: '../api/SpaceDBAPI/bindShift',
                type: 'GET',
                success: function (result) {
                    chartshift.load(
                        {
                            columns: result.shift,
                        });
                    $("#ltlShift").html("<strong>Occupied by BU</strong> <br/>" + result.ttlshift);
                }
            });
            $("#chartseatutil").append(chartshift.element);
        }

        //occupied by Fun
        function funbindfunbindFTE() {
            var chartFTE = c3.generate({
                setTimeout: 0,
                size: {
                    height: 480
                },
                data: {
                    x: 'x',
                    columns: [
                    ],
                    type: 'bar',
                    color: function (color, d) {
                        return '#FF9D3B';
                    }
                },
                axis: {
                    rotated: true,
                    x: {
                        type: 'category',
                        tick: {
                            rotate: 75,
                            multiline: false
                        },
                        //show: false,
                        height: 250
                    },
                    y: {
                        show: false,
                    },
                },
            });

            $.ajax({
                url: '../api/SpaceDBAPI/bindFTE',
                type: 'GET',
                success: function (result) {
                    chartFTE.load(
                        {
                            columns: result.FTE,
                        });
                    $("#ltlFTE").html("<strong>Occupied by Function/Employee(s)</strong> <br/>" + result.ttlFTE);
                }
            });
            $("#chartFTE").append(chartFTE.element);
        }
       

        function funbindCabin() {
            var chart2 = c3.generate({
                setTimeout: 0,
                data: {
                    x: 'x',
                    columns: [
                    ],
                    type: 'bar',
                    color: function (color, d) {
                        return '#FF9D3B';
                    }
                },
                axis: {
                    rotated: true,
                    x: {
                        type: 'category',
                        tick: {
                            rotate: 75,
                            multiline: false
                        },
                        //show: false,
                        height: 250
                    },
                    y: {
                        show: false,
                    },
                },
            });

            $.ajax({
                url: '../api/SpaceDBAPI/bindCabin',
                type: 'GET',
                success: function (result) {
                    chart2.load(
                        {
                            columns: result.cabins,
                        });
                    $("#ltlCabin").html("<strong>Total Cabins(FC + HC)</strong> <br/>" + result.ttlcabinscount);
                }
            });
            $("#chartcb").append(chart2.element);
        }

    });

</script>
