﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DashboardV2.aspx.vb" Inherits="DashboardV2" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="CSS/dashboardstyle.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div>
        <div class="board">
            <div class="board-inner">
                <ul class="nav nav-tabs">
                    <div class="liner"></div>
                    <li class="active">
                        <a href="#profile" data-toggle="tab" title="" data-original-title="Profile" data-placement="bottom">
                            <span class="round-tabs two">
                                <i class="fa fa-user"></i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="#property" data-toggle="tab" title="" data-original-title="Properties" data-placement="bottom">
                            <span class="round-tabs three">
                                <%--<i class="fa fa-building-o"></i>--%>
                                <i>
                                    <img src="Images/property_icon.png" style="margin-top: -5px;" />
                                </i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="#space" data-toggle="tab" title="" data-original-title="Space" data-placement="bottom">
                            <span class="round-tabs one">
                                <%--<i class="fa fa-pie-chart"></i>--%>
                                <i>
                                    <img src="Images/sapce_icon.png" style="margin-top: -10px;" />
                                </i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="#asset" data-toggle="tab" title="" data-original-title="Assets" data-placement="bottom">
                            <span class="round-tabs four">
                                <%--<i class="fa fa-briefcase"></i>--%>
                                <i>
                                    <img src="Images/asset_icon.png" />
                                </i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="#maintenance" data-toggle="tab" title="" data-original-title="Maintenance" data-placement="bottom">
                            <span class="round-tabs six">
                                <i>
                                    <img src="Images/maintenance_icon.png" />
                                </i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="#helpdesk" data-toggle="tab" title="" data-original-title="Help Desk" data-placement="bottom">
                            <span class="round-tabs seven">
                                <i class="fa fa-support"></i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="#conference" data-toggle="tab" title="" data-original-title="Conference" data-placement="bottom">
                            <span class="round-tabs five">
                                <i>
                                    <img src="Images/conference_icon.png" />
                                </i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="#Branch" data-toggle="tab" title="" data-original-title="Branch Checklist" data-placement="bottom" >
                        <span class="round-tabs five">
                            <i>
                                <img src="Images/gh.png" style="padding-bottom: 15px;" />
                            </i>
                        </span>
                       </a>
                    </li>

                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="profile"></div>
                <div class="tab-pane fade" id="property" runat="server"></div>
                <div class="tab-pane fade" id="space" runat="server"></div>
                <div class="tab-pane fade" id="asset" runat="server"></div>
                <div class="tab-pane fade" id="maintenance" runat="server"></div>
                <div class="tab-pane fade" id="helpdesk" runat="server"></div>
                <div class="tab-pane fade" id="conference" runat="server"></div>
                <div class="tab-pane fade" id="guesthouse" runat="server"></div>
                 <div class="tab-pane fade" id="Branch" runat="server"></div>
            </div>
        </div>
    </div>
    <%--C3 charts start here--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="C3/d3.v3.min.js"></script>
    <script src="C3/c3.min.js"></script>
    <link href="C3/c3.css" rel="stylesheet" />
    <script src="C3/jshint.js"></script>
    <script src="Scripts/jQuery.print.js"></script>
    <script type="text/javascript">
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };
        $(function () {
            $('a[title]').tooltip();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href")
                if (target != "#profile")
                    $(target).load(target.substring(1) + "Dashboard.aspx");
                else
                    $("#profile").load("../WebFiles/frmMyProfile.aspx");
            });
            if (getParameterByName("back") != "") {
                //activaTab("maintenance");
                var dbname = getParameterByName("back");
                activaTab(dbname);
            }
            else
                $("#profile").load("../WebFiles/frmMyProfile.aspx");
        });
    </script>
    <%--C3 charts end here--%>

    <script src="Scripts/jQuery.print.js"></script>
    <script src="Scripts/DashboardPrint.js"></script>


</body>
</html>
