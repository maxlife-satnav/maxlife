﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<div id="page-wrapper" class="row">
    <div class="row form-wrapper">
        <form id="form1" class="form-horizontal" runat="server">
            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                <legend style="padding-top: 0px; width: 27%">Help Desk Management</legend>
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart fa-fw"></i>Requests Raised for last 7 days
                            </div>
                            <div class="panel-body">
                                <div id="helpdeskcontainer"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-heading">
                            </div>
                            <div class="panel-heading">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-10">
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-shopping-cart fa-5x"></i>
                                            </div>
                                            <div class="col-md-10 text-right">
                                                <div class="huge">
                                                    <label id="lblslacount"></label>
                                                </div>
                                                <div>SLA (Service Level Agreement) Exceeded Request Count</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="panel panel-red">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <i class="fa fa-warning fa-5x"></i>
                                            </div>
                                            <div class="col-md-9 text-right">
                                                <div class="huge">
                                                    <label id="lblreqserper"></label>
                                                </div>
                                                <div>% of Requests Served to Request Raised</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart fa-fw"></i>Location-Wise Pending Services 
                            </div>
                            <div class="panel-body">
                                <div id="helpdeskdonutCointainer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        //Requests raised in last 7 days
        funGetHDreqWeekly();

        //SLA's label
        fnfillSLA();

        //Location-Wise Pending Services
        funfillCategories();

        function fnfillSLA() {
            $.ajax({
                url: '../api/HelpDeskDBAPI/GetSLACount',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    $("#lblslacount").html(data.SLACount);
                    $("#lblreqserper").html(data.SLAratio);
                },
                error: function (result) {
                }
            });
        }

        function funGetHDreqWeekly() {
            //[['x','2015-04-01','2015-04-10','2015-04-23'],['Date',3,1,2]]
            var chart = c3.generate({
                setTimeout: 0,
                data: {
                    x: 'x',
                    columns: [
                    ],
                },

                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {
                            format: '%Y-%m-%d',
                            rotate: 90,
                            multiline: false
                        },
                    },
                    y: {
                        show: true,
                        label: {
                            text: 'No of Requests',
                            position: 'outer-middle'
                        }
                    },
                }
            });

            $.ajax({
                url: '../api/HelpDeskDBAPI/GetHDreqWeekly',
                type: 'GET',
                success: function (result) {
                    chart.load(
                        {
                            columns: result //[['x', '2013-01-01', '2013-01-02', '2013-01-03', '2013-01-04', '2013-01-05', '2013-01-06'], ['data1', 30, 200, 100, 400, 150, 250]]
                        });
                }
            });
            $("#helpdeskcontainer").append(chart.element);
        }

        function funfillCategories() {

            $.ajax({
                url: '../api/HelpDeskDBAPI/BindHDCategories',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                        var chart2 = c3.generate({
                            data: {
                                json: JSON.parse(data.locVal),
                                keys: {
                                    x:'name',    
                                    value: JSON.parse(data.services),
                                },
                                type: 'bar',
                            },
                            axis: {
                                x: {                                    
                                    type: 'category'
                                },
                                y: {
                                    show: true,
                                    label: {
                                        text: 'Count',
                                        position: 'outer-middle'
                                    }
                                },
                            }
                        });
                        $("#helpdeskdonutCointainer").append(chart2.element);
                    
                },
                error: function (result) {
                }
            });
        }
    });
</script>
</html>
