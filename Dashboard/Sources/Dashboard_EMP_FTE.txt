<script type="text/javascript">
        $(function () {
            var chart, merge = Highcharts.merge;
            //<![CDATA[ 
            $(document).ready(function () {
                var perShapeGradient = {
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                };
                var colors = Highcharts.getOptions().colors;
                colors = [{
                    linearGradient: perShapeGradient,
                    stops: [
                        [0, '#F2A32D'],
                        [1, '#E2D15B']
                    ]
                }, {
                    linearGradient: merge(perShapeGradient),
                    stops: [
                        [0, '#1384CA'],
                        [1, '#54AA87']
                    ]
                }, {
                    linearGradient: merge(perShapeGradient),
                    stops: [
                        [0, '#3C3E3B'],
                        [1, '#8E8D93']
                    ]
                }]
                chart = new Highcharts.Chart({
                    chart: {

                        renderTo: 'Div2',
                        defaultSeriesType: 'bar',
						 marginTop: 100
                    },
                    title: {
                        text: '@@TOTFTE',
						style: {
                                fontSize: '25px',
                                fontFamily: 'Malgun Gothic, sans-serif',
								fontWeight: 'bold'
                            }
                    },
                    subtitle: {
                        text: 'FTEs',
						style: {
                                fontSize: '16px',
                                fontFamily: 'Malgun Gothic, sans-serif',
								color:'#000000'
                            }
                    },


                    xAxis: {
                        type: 'category',
                        //labels: {
                        //    rotation: 0,
                        //    style: {
                        //        fontSize: '13px',
                        //        fontFamily: 'Malgun Gothic, sans-serif'
                        //    }
                        //}
                        labels: {
                            enabled: false
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            enabled: false
                        },
                        lineWidth: 0,
                        minorGridLineWidth: 0,
                        lineColor: 'transparent',
                        labels: {
                            enabled: false
                        },
                        minorTickLength: 0,
                        gridLineColor: 'transparent',
                        tickLength: 0,
						gridLineWidth:0
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                         pointFormat:  ": {point.y:,.0f}"
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.1,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'China',
                        data: @@dataFTE,
                        color: 'white',


                        dataLabels: {
                            enabled: true,
                            rotation: 0,
                            color: colors[0],
                            align: 'right',
                            x: 0,
                            y: 0,
                            style: {
                                fontSize: '9px',
                                fontFamily: 'Malgun Gothic, sans-serif',
                                textShadow: '0 0 0px'
                            }
                        }
                    }]

                }, function (chart) {

                    var bar = chart.series[0].data[0].graphic
				bar.attr('width',parseInt(bar.attr('width'))+10);
				bar.attr('x',parseInt(bar.attr('x'))-5); // keep it centered

                    var bar = chart.series[0].data[1].graphic
				bar.attr('width',parseInt(bar.attr('width'))+10);
				bar.attr('x',parseInt(bar.attr('x'))-5); // keep it centered

			//chart.renderer.text('<span> @@CNT</span>', chart.plotSizeY*0.30 , 85)
           // .attr({
					
        
           // })
           // .css({
           //     color: '#777',
			//	fontFamily: 'Malgun Gothic, sans-serif',
           //     fontSize: '13px'
          //  })
          //  .add();

			//chart.renderer.image('@@IMG', chart.plotSizeY*0.32, 73 , 15, 15)
            //.add();

                });


            });
            //]]> 
        });
    </script>