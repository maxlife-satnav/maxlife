﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Dashboard_C3_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../CSS/dashboardstyle.css" rel="stylesheet" />
      <link href="c3.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
   
</head>
<body>
<div id="page-wrapper" class="row">
    <div class="row form-wrapper">
        <form id="form1" class="form-horizontal" runat="server">
            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                <legend style="padding-top: 0px; width: 24%">Property Management</legend>

                <div class="row">
                    <div class="col-md-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-md-9 text-right">
                                        <div class="huge">
                                            <asp:label id="lblwrtotcount" runat="server"></asp:label>
                                        </div>
                                        <div>Total Work Requests!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="../WorkSpace/SMS_Webfiles/frmWorkRequestReport.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-3">
                                        <i class="fa fa-check-square-o fa-5x"></i>
                                    </div>
                                    <div class="col-md-9 text-right">
                                        <div class="huge">
                                            <asp:label id="lblclosed" runat="server"></asp:label>
                                        </div>
                                        <div>Closed Work Requests!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="../WorkSpace/SMS_Webfiles/frmWorkRequestReport.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-md-10 text-right">
                                        <div class="huge">
                                            <asp:label id="lblpending" runat="server"></asp:label>
                                        </div>
                                        <div>Pending Work Requests!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="../WorkSpace/SMS_Webfiles/frmWorkRequestReport.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-3">
                                        <i class="fa fa-warning fa-5x"></i>
                                    </div>
                                    <div class="col-md-9 text-right">
                                        <div class="huge">
                                            <asp:label id="lblexpleases" runat="server"></asp:label>
                                        </div>
                                        <div>About to Expire Leases!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="../WorkSpace/SMS_Webfiles/frmViewLeases.aspx">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-pie-chart fa-fw"></i>Properties Count
                            </div>
                            <div class="panel-body">
                                <div id="propertycontainer"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Received and Pending amounts
                            </div>
                            <div class="panel-body">
                                <div id="rentscontainer" style="width: 35%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</body>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
   
    <script src="d3.v3.min.js"></script>     
     <script src="c3.min.js"></script> 

   <script type="text/javascript">

       var chart = c3.generate({
           bindto: '#propertycontainer',
           data: {

               columns: [
               <%= chartData%>
            ],
            type: 'pie',
            onclick: function (d, i) { console.log("onclick", d, i); },
            onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            onmouseout: function (d, i) { console.log("onmouseout", d, i); }
        }

    });
</script>

</html>

