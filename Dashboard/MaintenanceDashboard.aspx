﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<div id="page-wrapper" class="row">
    <div class="row form-wrapper">
        <form id="form1" class="form-horizontal" runat="server">
            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                <legend style="padding-top: 0px; width: 30%">Maintenance Management</legend>
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-pie-chart fa-fw"></i>Category-Wise Assets Count
                            </div>
                            <div class="panel-body">
                                <div id="maintenancecontainer"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">

                        <div class="row">
                            <div class="col-md-10">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-shopping-cart fa-5x"></i>
                                            </div>
                                            <div class="col-md-10 text-right">
                                                <div class="huge">
                                                    <label id="lblamcamount"></label>
                                                </div>
                                                <div>AMC Amount received this Month </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- <a href="../MaintenanceManagement/AMC/Reports/AMC_PaymentAdvice.aspx?rurl=/Dashboard/DashboardV2.aspx">--%>
                                    <a href="#" id="ancamcvd">
                                        <div class="panel-footer" data-toggle="modal" data-target="#totalamcs">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="panel panel-red">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <i class="fa fa-warning fa-5x"></i>
                                            </div>
                                            <div class="col-md-9 text-right">
                                                <div class="huge">
                                                    <label id="lblexpleases"></label>
                                                </div>
                                                <div>AMC Assets Expiring this month!</div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<a href="../MaintenanceManagement/AMC/Reports/ExpiredAMC.aspx?rurl=/Dashboard/DashboardV2.aspx">--%>
                                    <a href="#" id="ancAssetsexp">
                                        <div class="panel-footer" data-toggle="modal" data-target="#divamcexp">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart fa-fw"></i>Location-wise Asset Categories
                            </div>
                            <div class="panel-body">
                                <div id="amcnonamccontainer"></div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="totalamcs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="H2">AMC Amount Received this Month</h4>
                            </div>
                            <div class="modal-body">
                                <table id="tblamcamounts" style="margin-left: -34px;" class="table table-condensed table-bordered table-hover table-striped">
                                    <tr>
                                        <th>Advice No</th>
                                        <th>Work Order No</th>
                                        <th>Vendor</th>
                                        <th>Mode Of Payment</th>
                                        <th>Amount</th>
                                        <th>Updated Date</th>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="divamcexp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content" style="width: 760px;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="H1">AMC Assets Expiring this Month</h4>
                            </div>
                            <div class="modal-body">
                                <table id="tblamcexp" class="table table-condensed table-bordered table-hover table-striped">
                                    <tr>
                                        <th>Plan ID</th>
                                        <th>Asset</th>
                                        <th>Vendor</th>
                                        <th>Location</th>
                                        <th>From Date</th>
                                        <th>To Date</th>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

</html>

<script type="text/javascript">
    $(document).ready(function () {
        //Pie
        funfillAMCTypes();

        //bar chart
        funfillAMCNonAMCAssets();


        //Assets Exp this month
        funAMCAssets();

        //AMC Amounts received
        funAMCamountsrec();

        function funfillAMCTypes() {
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/MaintenanceDBAPI/BindAMCTypes',
                type: 'GET',
                success: function (result) {                    
                    chart.load({ columns: result });                    
                }
            });
            $("#maintenancecontainer").append(chart.element);
        }

        function funfillAMCNonAMCAssets() {
            var chart = c3.generate({
                data: {
                    x: 'x',
                    columns: [
                    //['x', 'www.site1.com', 'www.site2.com', 'www.site3.com'],
                    //['AMC', 30, 200, 100],
                    //['NONAMC', 90, 100, 140],
                    ],

                    groups: [['AMC', 'Non-AMC']],
                    type: 'bar',
                },
                axis: {
                    x: {
                        type: 'category',
                        position: 'outer-center',
                    },
                    y: {
                        show: true,
                        label: {
                            text: 'Count',
                            position: 'outer-middle'
                        }
                    },
                }
            });

            $.ajax({
                url: '../api/MaintenanceDBAPI/BindAMCNonAMCBarCh',
                type: 'GET',
                success: function (result) {                                      
                    chart.load({
                        columns: result
                    });
                }
            });
            $("#amcnonamccontainer").append(chart.element);
        }
        

        function funAMCamountsrec() {
            $.ajax({
                url: '../api/MaintenanceDBAPI/GetAMCAmounts',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {                    
                    var table = $('#tblamcamounts');
                    $('#tblamcamounts td').remove();
                    if(data.length>0){
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].APA_PAYADVICE_NO + "</td>" +
                                "<td>" + data[i].APA_MPWWRKORD_ID + "</td>" +
                                "<td>" + data[i].CONTRACTOR + "</td>" +
                                "<td>" + data[i].APA_PAYMENT_MODE + "</td>" +
                                "<td>" + data[i].APA_PAYMENT_AMOUNT + "</td>" +
                                "<td>" + data[i].ACD_UPDATED_DT + "</td>" +
                                "</tr>");
                        }
                        var total = 0;                        
                        $('#tblamcamounts tr').each(function () {                            
                            var value = parseInt($('td', this).eq(4).text());
                            if (!isNaN(value)) {
                                total+= value;
                            }
                            $("#lblamcamount").html(total);
                        });
                    }
                    else {
                        $("#lblamcamount").html("0");
                        table.append("<tr>" + "<td colspan='6' align='center'> No AMC Amounts Received Current Month</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }

        function funAMCAssets() {
            $.ajax({
                url: '../api/MaintenanceDBAPI/GetAMCAssetsExp',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    var table = $('#tblamcexp');
                    $('#tblamcexp td').remove();
                    $("#lblexpleases").html(data.length);
                    if (data.length > 0) {                        
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].AMN_PLAN_ID + "</td>" +
                                "<td>" + data[i].ASSETNAME + "</td>" +
                                "<td>" + data[i].AVR_NAME + "</td>" +
                                "<td>" + data[i].BDG_NAME + "</td>" +
                                "<td>" + data[i].FROMDATE + "</td>" +
                                "<td>" + data[i].TODATE + "</td>" +
                                "</tr>");
                        }
                    }
                    else {                        
                        table.append("<tr>" + "<td colspan='6' align='center'> No AMC Assets Expiring Current Month</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }


        //amc amounts rec this month      
        $("#ancamcvd").click(function (e) {
            e.preventDefault();
            // funAMCamountsrec();
        });

        //AMC Assets Exp Current Month        
        $("#ancAssetsexp").click(function (e) {
            e.preventDefault();
            funAMCAssets();
        });

    });
</script>
