Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.Web.Security
Imports System.Configuration.ConfigurationManager
Imports System.Exception
Imports SqlHelper

Partial Class frmAMTDefault
    Inherits System.Web.UI.Page

#Region "Variables"

    Dim objdata As SqlDataReader
    Dim strTpwd, uid, pwd As String
    Dim strSql As String = String.Empty
    Dim strADID As String = String.Empty
    Dim strUsername As String = String.Empty
    Dim strclass As clsQueries
    Dim idrole, roles As String
    Dim strlogged As String = String.Empty
    Dim strQuery As String
    Dim iQuery As Integer
    Dim iQuery1 As Integer
    Dim COUNT As Integer

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                Dim UID As String = Session("uid")
            End If


            ' Session("uname") = "ManNay"
            If Session("uname") <> String.Empty Then
                Label1.Text = Session("uname").ToString()
                strQuery = "select count(aur_id) from " & Session("TENANT") & "." & "amantra_user where AUR_ID =  '" & Label1.Text.Trim() & "'"
                iQuery1 = SqlHelper.ExecuteScalar(CommandType.Text, strQuery)
                If iQuery1 > 0 Then
                    strQuery = "select aur_id from " & Session("TENANT") & "." & "amantra_user where AUR_ID =  '" & Label1.Text.Trim & "'"
                    objdata = SqlHelper.ExecuteReader(CommandType.Text, strQuery)
                    Do While objdata.Read
                        strADID = objdata("aur_id").ToString()
                        Session("uid") = strADID
                    Loop
                    objdata.Close()
                    strQuery = "Select USR_ACTIVE from " & Session("TENANT") & "." & "[USER]" & " WHERE USR_ID='" & strADID & "'"
                    objdata = SqlHelper.ExecuteReader(CommandType.Text, strQuery)
                    Dim strActive As String = ""
                    While (objdata.Read())
                        strActive = objdata("USR_ACTIVE").ToString()
                    End While
                    objdata.Close()
                    If (strActive = "N" Or strActive = "n") Then
                        Label1.Text = "De-Activated User ID!"
                        Exit Sub
                    End If
                    If Session("uid") <> String.Empty Then
                        If validateUser(Session("uid").ToString()) Then

                            strSql = ("WebFiles/frmDetails.aspx")
                        Else
                            strSql = ("login.aspx")
                        End If
                    End If
                Else
                    ' strSql = ("login.aspx")
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Default", "Load", exp)
        End Try
        If strSql <> String.Empty Then
            Response.Redirect(strSql)
        End If
    End Sub

    Public Function validateUser(ByVal strUser As String) As Boolean
        Dim strlogged As String = ""
        If strUser <> String.Empty Then
            strQuery = "select count(aur_id) from " & Session("TENANT") & "." & "amantra_user where aur_id =  '" & strUser & "'"
            iQuery = SqlHelper.ExecuteScalar(CommandType.Text, strQuery)
            Dim sp As New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 50)
            sp.Value = Session("uid")
            objdata = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "USER_SP_LOGIN_CHECKLOGIN", sp)
            Do While objdata.Read
                strlogged = objdata("Usr_logged").ToString
                Session("uemail") = objdata("AUR_EMAIL").ToString
                ' Session("uname") = objdata("AUR_first_name").ToString & " " & objdata("AUR_middle_name").ToString & " " & objdata("AUR_last_name").ToString
                Session("bdg") = objdata("aur_bdg_id").ToString
                Dim uTime As Integer
                uTime = (DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds
                Session("LoginTime") = uTime
            Loop
            objdata.Close()
            If strlogged = "Y" Then
                Label1.Text = "User Already Logged In."
                Exit Function
            End If
            If iQuery = 0 Then
                Label1.Text = "Invalid Login ID.Please Login and try once again"
            Else

                validateUser = True
            End If
        Else
            Label1.Text = "Invalid Login ID.Please Login and try once again"
        End If
    End Function
End Class
