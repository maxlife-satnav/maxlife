﻿Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports MaintenanceRpt
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class EFM_EFM_Webfiles_ConsolidatedReport
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindGrid()
        End If
    End Sub

    Public Sub bindGrid()
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("HDM_CONSOLIDATED_REPORT")
        'GridView1.DataSource = ds.Tables(0)
        'GridView1.DataBind()
        Dim rds As New ReportDataSource()
        rds.Name = "ConsolidatedDS"
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/ConsolidatedReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub
End Class
