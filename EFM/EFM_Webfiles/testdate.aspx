﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="testdate.aspx.vb" Inherits="EFM_EFM_Webfiles_testdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

        <script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Visitor Log
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="85%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
          <tr>
                <td>
                    <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Visitor Log</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table cellpadding="1" cellspacing="0" width="100%">
                       <tr>
                        <td align="left" style="height: 26px" width="25%">
                             From Date
                        </td>
                        <td align="left" style="height: 26px" width="25%">
                            <asp:TextBox ID="txtfromdate" runat="server" CssClass="clsTextField"></asp:TextBox>
                           
                        </td>
                        <td align="left" style="height: 26px" width="25%">
                             To Date
                        </td>
                        <td align="left" style="height: 26px" width="25%"">
                            <asp:TextBox ID="txttoDate" runat="server" CssClass="clsTextField"></asp:TextBox>
                           
                        </td>
                       </tr>
                       <tr>
                        <td align="Center"  colspan="4">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="GO"  />
                        </td>
                     
                       
                    </tr>
                      <tr>
                            <td style="height: 20px" colspan="4">
                                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    Width="100%" PageSize="10" EmptyDataText="Sorry No Requests Raised">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_REQID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Visitor Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblname" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email">
                                            <ItemTemplate>
                                                <asp:Label ID="lblemail" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_EMAIL") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmobile" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MOBILE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Whom to Meet">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmeet" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MEET") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="InTime">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIntime" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_INTIME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OutTime">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOuttime" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_OUTTIME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
        </td>
          <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
            &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px; width: 524px;" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
       
        </table>
    </div>
</asp:Content>

