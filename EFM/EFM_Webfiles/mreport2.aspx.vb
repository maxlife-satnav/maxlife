Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class EFM_EFM_Webfiles_mreport2
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("MaintenanceReport2.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        BindGridView()
    End Sub

    Public Sub BindGridView()
        Dim param(12) As SqlParameter
        param(0) = New SqlParameter("@FDATE", SqlDbType.DateTime)
        param(0).Value = Request.QueryString("FDate")
        param(1) = New SqlParameter("@TDATE", SqlDbType.DateTime)
        param(1).Value = Request.QueryString("TDATE")
        param(2) = New SqlParameter("@REQ_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = Request.QueryString("RequestType")
        param(3) = New SqlParameter("@REQ", SqlDbType.NVarChar, 200)
        param(3).Value = Request.QueryString("Request")
        param(4) = New SqlParameter("@FTIME", SqlDbType.NVarChar, 200)
        param(4).Value = Request.QueryString("FTime")
        param(5) = New SqlParameter("@TTIME", SqlDbType.NVarChar, 200)
        param(5).Value = Request.QueryString("TTime")
        param(6) = New SqlParameter("@SER_STATUS", SqlDbType.NVarChar, 200)
        param(6).Value = Request.QueryString("stat")
        param(7) = New SqlParameter("@type", SqlDbType.NVarChar, 200)
        param(7).Value = Request.QueryString("ddltype")
        param(8) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(8).Value = 1
        param(9) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(9).Value = "REQUESTED_DATE"
        param(10) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(10).Value = "ASC"
        param(11) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(11).Value = 10

        param(12) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(12).Value = 0
        param(12).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("HDM_GET_MAINTENANCE_REPORT2", param)
        GridView1.DataSource = ds
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGridView()
    End Sub
End Class
