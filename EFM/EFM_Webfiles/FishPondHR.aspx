<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FishPondHR.aspx.vb" Inherits="ESP_ESP_Webfiles_FishPondHR" Title="Fish Pond HR Approval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Fish Pond HR Approval
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-md-12">
                                    <div class="row">
                                        <asp:GridView ID="gvitems" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="true" AllowSorting="true"
                                             AutoGenerateColumns="false"  EmptyDataText="No Suggestion Type Found.">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Suggestion Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsuggestiontype" runat="Server" Text='<%#Eval("SUGGESTION_TYPE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblremarks" runat="Server" Text='<%#Eval("REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:ButtonField Text="Approve" CommandName="Approve" />
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblstaid" runat="Server" Text='<%#Eval("STA_ID") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:ButtonField Text="Delete" CommandName="Delete" />--%>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>


                            <div id="Panel1" runat="server">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Suggestion Type<span style="color: red;"></span></label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtSuggestionType" runat="server" CssClass="form-control" MaxLength="250">                                               
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Remarks<span style="color: red;"></span></label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtFishPondMsg" runat="server" CssClass="form-control"
                                                        MaxLength="4000" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">HR Comments<span style="color: red;"></span></label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txthrfishpondmsg" runat="server" CssClass="form-control"
                                                        MaxLength="4000" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="row">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"
                                                CausesValidation="True" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div id="pnlApproved" runat="Server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row text-center">
                                                <strong>VIEW APPROVED MESSAGES</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                        <div class="row">
                                            <asp:GridView ID="gvapproved" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="true" AllowSorting="true"
                                                AutoGenerateColumns="false"  EmptyDataText="No Suggestion Type Found.">
                                                <Columns>
                                                    <asp:TemplateField Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsno1" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Suggestion Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsuggestiontype1" runat="Server" Text='<%#Eval("SUGGESTION_TYPE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblremarks1" runat="Server" Text='<%#Eval("REMARKS") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks Posted On">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblrmposted1" runat="Server" Text='<%#Eval("POSTED_ON") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reply Comments">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblhrremarks1" runat="Server" Text='<%#Eval("HR_COMMENTS") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Comments Posted On">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblhrpostedon1" runat="Server" Text='<%#Eval("HR_POSTED_ON") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:ButtonField Text="Delete" CommandName="Delete" />
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


