Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Partial Class EFM_EFM_Webfiles_MaintenanceReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack() Then
            temp.Visible = False
            BindDepartment()
            GetRequestCount()
            txtfromdate.Attributes.Add("onClick", "displayDatePicker('" + txtfromdate.ClientID + "')")
            txtfromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txttodate.Attributes.Add("onClick", "displayDatePicker('" + txttodate.ClientID + "')")
            txttodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
    End Sub
    Private Sub GetRequestCount()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUESTS_COUNT")
        Dim dummy As String = sp.Command.CommandSql
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        lbtnopen.Text = " Pending Requests   " & ds.Tables(0).Rows(0).Item("PENDING")
        lbtnprogress.Text = " Progress Requests   " & ds.Tables(1).Rows(0).Item("INPROGRESS")
        lbtnclosed.Text = " Closed Requests   " & ds.Tables(2).Rows(0).Item("CLOSED")
    End Sub
    Private Sub BindDepartment()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_DEPARTMENTS")
        Dim dummy As String = sp.Command.CommandSql
        ddldep.DataSource = sp.GetDataSet()
        ddldep.DataTextField = "DEP_NAME"
        ddldep.DataValueField = "DEP_CODE"
        ddldep.DataBind()
        ddldep.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        ddldep.Items.Insert(1, New ListItem("All", " "))
    End Sub
    
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub ddlassigned_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlassigned.SelectedIndexChanged
        If ddlassigned.SelectedValue = "Employee" Then
            temp.Visible = True
        Else
            temp.Visible = False
        End If
        txtempid.Text = ""
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Response.Redirect("mreport.aspx?Dep=Nothing&User=" + txtempid.Text + "&FromDate=" + txtfromdate.Text + "&toDate=" + txttodate.Text + "&stat=Nothing&Usertype=" + ddlassigned.SelectedItem.Value + " ")
    End Sub

    Protected Sub lbtnopen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnopen.Click
        Response.Redirect("mreport.aspx?Dep=Nothing&User=Nothing&FromDate=Nothing&toDate=Nothing&stat=1&Usertype=Nothing ")
    End Sub

    Protected Sub lbtnprogress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnprogress.Click
        Response.Redirect("mreport.aspx?Dep=Nothing&User=Nothing&FromDate=Nothing&toDate=Nothing&stat=2&Usertype=Nothing ")
    End Sub

    Protected Sub lbtnclosed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnclosed.Click
        Response.Redirect("mreport.aspx?Dep=Nothing&User=Nothing&FromDate=Nothing&toDate=Nothing&stat=3&Usertype=Nothing ")
    End Sub
End Class
