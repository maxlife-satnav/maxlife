<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mreport1.aspx.vb" Inherits="EFM_EFM_Webfiles_mreport1" Title="Customized Report" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../BootStrapCSS/datepicker.css">
    <link href="../../BootStrapCSS/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/amantra.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/form-style.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <%--    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>--%>

    <script language="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Customized Report
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="col-md-12 control-label"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="Server" CssClass="clsMessage"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <div class="row">

                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
                                        <asp:Button ID="btnexport" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export to Excel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvitems" runat="server" AllowPaging="True" AllowSorting="FALSE" PageSize="15"
                                    AutoGenerateColumns="true" EmptyDataText="No Records Found" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
