<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VehicleEntry.aspx.vb" Inherits="ESP_ESP_Webfiles_VehicleEntry" Title="Untitled Page" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Vehicle Entry
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        
                                            <asp:HyperLink ID="hypAddVehicle" runat="server" Text="Add Vehicle" NavigateUrl="AddVehicle.aspx"></asp:HyperLink>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                              <div class="col-md-12">
                                    <div class="row">
                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Vehicle Entry Found.">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_REQID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vehicle Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemail" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_EMAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Vehicle Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmobile" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MOBILE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmeet" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MEET") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="In-Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIntime" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_INTIME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField Text="Stop" CommandName="PunchOut" />
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

