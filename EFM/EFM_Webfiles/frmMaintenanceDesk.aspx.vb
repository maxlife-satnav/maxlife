Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports InfronicsPostTrackSMS
Imports System.Configuration
Imports SENDSMS
Partial Class EFM_EFM_Webfiles_frmMaintenanceDesk
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim MaintenanceReq As String
    Dim lbl As Integer = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then  
            temp.Visible = False
            tdetails.Visible = False
            tothers.Visible = False
            tabcomm.Visible = False
            personal.Visible = False
            btn.Visible = False
            search.Visible = False
            Mobile.Visible = False
            Email.Visible = False
            loc.Visible = False
            Twr.Visible = False
            flr.Visible = False
            wng.Visible = False
            BindCity()
            BindService_request_category(ddlType, 0)
            txtconvdate.Text = getoffsetdatetime(DateTime.Now).ToString("dd-MMM-yyyy")
        End If
    End Sub
    Private Sub BindService_request_category(ByRef ddl As DropDownList, ByVal idparent As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddl, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        If ddlCity.Items.Count = 2 Then
            ddlCity.SelectedIndex = 1
        End If
    End Sub
    Private Sub BindLocationL()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION1")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        ddlloc.DataSource = sp.GetDataSet()
        ddlloc.DataTextField = "LCM_NAME"
        ddlloc.DataValueField = "LCM_CODE"
        ddlloc.DataBind()
        ddlloc.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub Cleardetails()
        ddlCity.Items.Clear()
        ddlloc.Items.Clear()
        ddltwr.Items.Clear()
        ddlfloor.Items.Clear()
        ddlwing.Items.Clear()
        ddlType.SelectedIndex = -1
        ddlSpace.Items.Clear()
        txtesname.Text = ""
        txtdept.Text = ""
        txtdesig.Text = ""
        txtescontact.Text = ""
        txtSMSMobile.Text = ""
        txtSMSEmail.Text = ""
        txtname.Text = ""
        txtemail.Text = ""
        txtcontact.Text = ""
        ddlreqtype.Items.Clear()
        txtProbDesc.Text = ""
        cboHr.Items.Clear()
    End Sub
    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Mobile.Visible = False
        Email.Visible = False
        If rbActions.SelectedItem.Value = "Employee" Then
            ValidateUser()
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MOBILE_DETAILS")
        If rbActions.SelectedItem.Value = "Employee" Then
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
        End If
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtSMSMobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        End If
        If rbActions.SelectedItem.Value = "Others" Then
            txtSMSEmail.Text = txtemail.Text
        End If

    End Sub
    Private Sub ValidateUser()
        Dim ValidUser As Integer
        ValidUser = ValidateEmp()
        If ValidUser = 0 Then
            Cleardetails()
            BindCity()
            BindDetails()
            lblMsg.Text = ""
            temp.Visible = True
            tdetails.Visible = True
            personal.Visible = True
            tabcomm.Visible = True
            btn.Visible = True
        Else
            temp.Visible = True
            tdetails.Visible = False
            tabcomm.Visible = False
            personal.Visible = False
            btn.Visible = False
            lblMsg.Text = "No Records Found"
        End If
        tothers.Visible = False
        loc.Visible = False
        Twr.Visible = False
        flr.Visible = False
        wng.Visible = False
    End Sub
    Private Sub BindDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_USER_DETAILS")
        If rbActions.SelectedItem.Value = "Employee" Then
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
        End If
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            ' ddlCity.ClearSelection()
            ' ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("aur_city")).Selected = True

            'ddlloc.Items.Clear()
            'BindLocation()
            'ddlloc.ClearSelection()
            'ddlloc.Items.FindByValue(ds.Tables(0).Rows(0).Item("aur_location")).Selected = True

            'ddltow.Items.Clear()
            'BindTower(ddlloc.SelectedValue)
            'ddltow.ClearSelection()
            'ddltow.Items.FindByValue(ds.Tables(0).Rows(0).Item("Aur_Tower")).Selected = True

            'ddlfloor.Items.Clear()
            'BindFloor(ddlloc.SelectedValue, ddltow.SelectedValue)
            'ddlfloor.ClearSelection()
            'ddlfloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("Aur_Floor")).Selected = True

            'ddlwing.Items.Clear()
            'BindWing(ddlloc.SelectedValue, ddltow.SelectedValue, ddlfloor.SelectedValue)
            'ddlwing.ClearSelection()
            'ddlwing.Items.FindByValue(ds.Tables(0).Rows(0).Item("Aur_Wing")).Selected = True
            txtesname.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
            txtdept.Text = ds.Tables(0).Rows(0).Item("DEP")
            txtdesig.Text = ds.Tables(0).Rows(0).Item("DESIGNATION")
            txtescontact.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
        End If
    End Sub
    Private Function ValidateEmp()
        Dim validemp As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_DESK_EMPLOYEE")
        sp.Command.AddParameter("@aur_id", txtempid.Text, DbType.String)
        validemp = sp.ExecuteScalar()
        Return validemp
    End Function
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If rbActions.SelectedItem.Value = "Others" Then
            ddlSpace.Items.Clear()
            BindLocation()
            If ddlloc.Items.Count = 2 Then
                ddlloc.Items(1).Selected = True
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddlloc.SelectedValue, ddltwr.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()


            End If
        Else
            If ddlCity.SelectedIndex = 0 Then
                ddlSpace.Items.Clear()
            Else
                BindSpace(ddlCity.SelectedItem.Value)
            End If
        End If
    End Sub
    Private Sub BindSpace(ByVal SPC_CTY_ID As String)


        Dim procname As String = ""
        If rbActions.SelectedItem.Value = "Employee" Then

            procname = "GET_SPACE_CITY"

        Else
            procname = "GET_SPACE_CITY2"

        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &procname)

        sp.Command.AddParameter("@SPC_CTY_ID", ddlCity.SelectedItem.Value, DbType.String)
        If rbActions.SelectedItem.Value = "Employee" Then
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
        Else
            sp.Command.AddParameter("@AUR_ID", "", DbType.String)
        End If
        sp.Command.AddParameter("@SPACE_TYPE", ddlType.SelectedItem.Text, DbType.String)
        ddlSpace.DataSource = sp.GetDataSet()
        ddlSpace.DataTextField = "SPC_VIEW_NAME"
        ddlSpace.DataValueField = "SPC_ID"
        ddlSpace.DataBind()
        ddlSpace.Items.Insert(0, New ListItem("--Select--", "0"))
        If ddlSpace.Items.Count = 2 Then
            ddlSpace.SelectedIndex = 1
        End If
    End Sub
    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        'If rbActions.SelectedItem.Value = "Others" Then
        If ddlType.SelectedItem.Text = "Others" Then
            loc.Visible = True
            Twr.Visible = True
            flr.Visible = True
            wng.Visible = True
            ddlSpace.Items.Clear()
            BindService_request_category(ddlReq, ddlType.SelectedItem.Value)
            Getdetails()
        Else
            loc.Visible = False
            Twr.Visible = False
            flr.Visible = False
            wng.Visible = False
            'If ddlType.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            '    BindSpace(ddlType.SelectedItem.Text)
            '    BindService_request_category(ddlReq, ddlType.SelectedItem.Value)
            'End If
            If ddlType.SelectedIndex > 0 Then
                BindSpace(ddlType.SelectedItem.Text)
                BindService_request_category(ddlReq, ddlType.SelectedItem.Value)
            End If
        End If
    End Sub
    Protected Sub ddlloc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlloc.SelectedIndexChanged
        If ddlType.SelectedItem.Text = "Others" Then
            If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 Then
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddlloc.SelectedValue, ddltwr.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        Else
            ddlSpace.Items.Clear()
            If ddlloc.SelectedIndex > 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_LOCATION")
                sp.Command.AddParameter("@LOCATION", ddlloc.SelectedItem.Value, DbType.String)
                ddlSpace.DataSource = sp.GetDataSet()
                ddlSpace.DataTextField = "SPC_VIEW_NAME"
                ddlSpace.DataValueField = "SPC_ID"
                ddlSpace.DataBind()
                ddlSpace.Items.Insert(0, New ListItem("--Select--", "--Select--"))
            End If
        End If

    End Sub
    Protected Sub ddltwr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltwr.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 Then
            BindFloor(ddlloc.SelectedValue, ddltwr.SelectedValue)
            If ddlfloor.Items.Count = 2 Then
                ddlfloor.SelectedIndex = 1
                BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
                If ddlwing.Items.Count = 2 Then
                    ddlwing.SelectedIndex = 1
                    BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                    If ddlSpace.Items.Count = 2 Then
                        ddlSpace.SelectedIndex = 1
                    Else
                        ddlSpace.SelectedIndex = 0
                    End If
                Else
                    ddlwing.SelectedIndex = 0
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlfloor.SelectedIndex = 0
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        End If
    End Sub
    Protected Sub ddlfloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlfloor.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 And ddlfloor.SelectedIndex > 0 Then
            BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
            If ddlwing.Items.Count = 2 Then
                ddlwing.SelectedIndex = 1
                BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                If ddlSpace.Items.Count = 2 Then
                    ddlSpace.SelectedIndex = 1
                Else
                    ddlSpace.SelectedIndex = 0
                End If
            Else
                ddlwing.SelectedIndex = 0
                ddlSpace.Items.Clear()
            End If
        End If
    End Sub
    Private Sub BindRoom(ByVal loc As String, ByVal tow As String, ByVal flr As String, ByVal wing As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_OTHERS")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", tow, DbType.String)
        sp.Command.AddParameter("@FLOOR", flr, DbType.String)
        sp.Command.AddParameter("@WING", wing, DbType.String)
        sp.Command.AddParameter("@SPC_TYPE", ddlType.SelectedItem.Text, DbType.String)
        ddlSpace.DataSource = sp.GetDataSet()
        ddlSpace.DataTextField = "SPC_VIEW_NAME"
        ddlSpace.DataValueField = "SPC_ID"
        ddlSpace.DataBind()
        ddlSpace.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If Page.IsValid = True Then
                If txtconvdate.Text <> "" Then
                    If CDate(txtconvdate.Text) < getoffsetdate(Date.Today) Then
                        lblMsg.Text = "Convenient Date Should be greater than or equal to  Todays Date"
                        Exit Sub
                    End If
                End If
                lblMsg.Text = ""
                Dim Main As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
                MaintenanceReq = Main + "/" + GetTodaysReqCount().ToString()
                RaiseRequest(MaintenanceReq)
                ClearSMSdetails()
                Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks1.aspx?id=13&Reqid=" & MaintenanceReq)
            End If
        Catch ex As Exception
            ''Response.Write(ex.Message)
        End Try
    End Sub
    Function GetTodaysReqCount() As String
        Dim cnt As String
        cnt = 0
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TODAYS_REQUEST_ID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ds = sp.GetDataSet()
        cnt = CInt(ds.Tables(0).Rows(0).Item("cnt")) + 1
        Return cnt
    End Function
    Private Sub RaiseRequest(ByVal MaintenanceReq As String)
        Dim count As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"RASIE_HELP_DESK_REQUEST")
        sp.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
        sp.Command.AddParameter("@AUR_TYPE", rbActions.SelectedItem.Value, DbType.String)
        If rbActions.SelectedItem.Value = "Employee" Then
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
        Else
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        End If
        sp.Command.AddParameter("@Name", txtname.Text, DbType.String)
        sp.Command.AddParameter("@Email", txtemail.Text, DbType.String)
        sp.Command.AddParameter("@Contact", txtSMSMobile.Text, DbType.String)
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_TYPE", ddlType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_RAISED_FOR", ddlReq.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_TYPE", ddlreqtype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_SPC_ID", ddlSpace.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@DESCRIPTION", txtProbDesc.Text, DbType.String)
        sp.Command.AddParameter("@MOBILE", "", DbType.String)
        sp.Command.AddParameter("@RESPONSE_BY", radlstactions.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ", ddlType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SMS", txtSMSMobile.Text, DbType.String)
        sp.Command.AddParameter("@AUR_EMAIL", txtSMSEmail.Text, DbType.String)
        If txtconvdate.Text = "" Then
            sp.Command.AddParameter("@CONV_DATE", DBNull.Value, DbType.DateTime)
        Else
            sp.Command.AddParameter("@CONV_DATE", txtconvdate.Text, DbType.DateTime)
        End If
        sp.Command.AddParameter("@CONV_TIME", cboHr.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CONV_TIME1", cboHr.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@CONREMARKS", "NA", DbType.String)
        sp.ExecuteScalar()
        Clearall()
        lblMsg.Text = "Request Raised Succesfully"
    End Sub
    Private Sub Clearall()
        temp.Visible = False
        tdetails.Visible = False
        tothers.Visible = False
        tabcomm.Visible = False
        btn.Visible = False
        search.Visible = False
        personal.Visible = False
    End Sub
    Private Sub ClearSMSdetails()
        txtSMSMobile.Text = ""
    End Sub
    Protected Sub radlstactions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radlstactions.SelectedIndexChanged
        If rbActions.SelectedItem.Value = "Employee" Then
            ClearSMSdetails()
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MOBILE_DETAILS")
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtSMSMobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
                txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            End If
        Else
            ClearSMSdetails()
            txtSMSEmail.Text = txtemail.Text

        End If
        If radlstactions.Items("0").Selected = True Then
            Mobile.Visible = True
        ElseIf radlstactions.Items("0").Selected = False Then
            Mobile.Visible = False
        End If

    End Sub
    Private Sub BindLocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        ddlloc.DataSource = sp.GetDataSet()
        ddlloc.DataTextField = "LCM_NAME"
        ddlloc.DataValueField = "LCM_CODE"
        ddlloc.DataBind()
        ddlloc.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindTower(ByVal loc As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TOWER_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        ddltwr.DataSource = sp.GetDataSet()
        ddltwr.DataTextField = "TWR_NAME"
        ddltwr.DataValueField = "TWR_CODE"
        ddltwr.DataBind()
        ddltwr.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindFloor(ByVal loc As String, ByVal tow As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_FLOOR_TOW_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", tow, DbType.String)
        ddlfloor.DataSource = sp.GetDataSet()
        ddlfloor.DataTextField = "FLR_NAME"
        ddlfloor.DataValueField = "FLR_CODE"
        ddlfloor.DataBind()
        ddlfloor.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindWing(ByVal loc As String, ByVal tow As String, ByVal floor As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_WING_FLR_TOW_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", tow, DbType.String)
        sp.Command.AddParameter("@FLOOR", floor, DbType.String)
        ddlwing.DataSource = sp.GetDataSet()
        ddlwing.DataTextField = "WNG_NAME"
        ddlwing.DataValueField = "WNG_CODE"
        ddlwing.DataBind()
        ddlwing.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        lblMsg.Text = ""
        If rbActions.SelectedItem.Value = "Employee" Then
            lblroom.Text = "SubLocation"
            temp.Visible = True
            tdetails.Visible = False
            tothers.Visible = False
            tabcomm.Visible = False
            btn.Visible = False
            search.Visible = True
            personal.Visible = False
            Cleardetails()
            Mobile.Visible = False
            Email.Visible = False
            loc.Visible = False
            Twr.Visible = False
            flr.Visible = False
            wng.Visible = False
            ddlloc.Items.Clear()
            ddltwr.Items.Clear()
            ddlfloor.Items.Clear()
            ddlwing.Items.Clear()
            ddlSpace.Items.Clear()
            radlstactions.Items(1).Selected = True
            ddlType.SelectedIndex = 0
        ElseIf rbActions.SelectedItem.Value = "Others" Then
            Cleardetails()
            ddlType.SelectedIndex = 4
            lblroom.Text = "Room"
            txtempid.Text = ""
            BindService_request_category(ddlReq, ddlType.SelectedItem.Value)
            Getdetails()
            loc.Visible = True
            Twr.Visible = True
            flr.Visible = True
            wng.Visible = True
            radlstactions.Items(1).Selected = True
            personal.Visible = False
            temp.Visible = False
            tdetails.Visible = True
            search.Visible = False
            tothers.Visible = True
            tabcomm.Visible = True
            Mobile.Visible = False
            Email.Visible = False
            btn.Visible = True
        End If
    End Sub
    Private Sub Getdetails()
        ddlloc.Items.Clear()
        ddltwr.Items.Clear()
        ddlfloor.Items.Clear()
        ddlwing.Items.Clear()
        ddlSpace.Items.Clear()
        BindCity()
        If ddlCity.Items.Count = 2 Then
            BindLocation()
            If ddlloc.Items.Count = 2 Then
                ddlloc.Items(1).Selected = True
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddlloc.SelectedValue, ddltwr.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        Else
            ddlloc.Items.Clear()
            ddltwr.Items.Clear()
            ddlfloor.Items.Clear()
            ddlwing.Items.Clear()
            ddlSpace.Items.Clear()
        End If
    End Sub
    Protected Sub ddlReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReq.SelectedIndexChanged
        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQ_TYPE_MAINTENANCE")
        'sp1.Command.AddParameter("@req_id", ddlReq.SelectedItem.Value, DbType.String)
        'Dim ds1 As New DataSet()
        'ds1 = sp1.GetDataSet()
        'If ds1.Tables(0).Rows.Count > 0 Then
        '    lbl = ds1.Tables(0).Rows(0).Item("TYPE")
        'End If
        'If rbActions.SelectedItem.Value = "Others" Then
        '    txtSMSEmail.Text = txtemail.Text
        'End If
        'If ddlReq.SelectedIndex > 0 Then
        '    If txtconvdate.Text <> "" Then
        '        If CDate(txtconvdate.Text) = getoffsetdate(Date.Today) Then
        '            BindTimings(lbl)
        '        Else
        '            BindTimings1(lbl)
        '        End If
        '    Else
        '        BindTimings1(lbl)
        '    End If
        'End If
        BindRequest()
        If ddlreqtype.Items.Count = 2 Then
            ddlreqtype.SelectedIndex = 1
            BindTimings()
        Else
            ddlreqtype.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindRequest()
        Dim count As Integer = 0
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_REQUEST_TYPE_COUNT")
        sp1.Command.AddParameter("@REQ_TYPE", ddlReq.SelectedItem.Value, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        If ds1.Tables(0).Rows.Count > 0 Then
            count = ds1.Tables(0).Rows(0).Item("COUNT")
        End If
        count = count + 1
        BindService_request_category(ddlreqtype, ddlReq.SelectedItem.Value)
    End Sub
   
    Private Sub BindTimings(ByVal lbl As Integer)
        cboHr.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TIMINGS_2")
        sp.Command.AddParameter("@TIMING", lbl, DbType.Int32)
        cboHr.DataSource = sp.GetDataSet()
        cboHr.DataTextField = "TIMINGS"
        cboHr.DataValueField = "VALUE"
        cboHr.DataBind()
        cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))
    End Sub
    Private Sub BindTimings1(ByVal lbl As Integer)
        cboHr.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TIMINGS_1")
        sp.Command.AddParameter("@TIMING", lbl, DbType.Int32)
        cboHr.DataSource = sp.GetDataSet()
        cboHr.DataTextField = "TIMINGS"
        cboHr.DataValueField = "VALUE"
        cboHr.DataBind()
        cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))

    End Sub
    Protected Sub ddlwing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlwing.SelectedIndexChanged

        If ddlReq.SelectedIndex > 0 And ddlwing.SelectedIndex > 0 Then
            BindRequest()
        Else
            ddlreqtype.Items.Clear()
        End If
        If ddlwing.SelectedIndex > 0 And ddlfloor.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
            If ddlSpace.Items.Count = 2 Then
                ddlSpace.SelectedIndex = 1
            Else
                ddlSpace.SelectedIndex = 0
            End If
        End If


    End Sub

    

   
    Protected Sub ddlreqtype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlreqtype.SelectedIndexChanged
        BindTimings()
    End Sub

   
    Protected Sub txtconvdate_TextChanged(sender As Object, e As EventArgs) Handles txtconvdate.TextChanged
        BindTimings()
    End Sub
    Private Sub BindTimings()
        If ddlreqtype.SelectedIndex > 0 And txtconvdate.Text <> "" Then
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CMS_TIMINGS")
            sp1.Command.AddParameter("@req_id", ddlreqtype.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@date", txtconvdate.Text, DbType.Date)
            cboHr.DataSource = sp1.GetDataSet()
            cboHr.DataTextField = "TIMINGS"
            cboHr.DataValueField = "VALUE"
            cboHr.DataBind()
            cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))

        End If
    End Sub
End Class
