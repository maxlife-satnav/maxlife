Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_frmLveRMApprovalCompOff
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim REQID As String = Request.QueryString("ReqID")
            APPROVE_LEAVE_COMP(REQID)
            Mail_RMApproved(REQID)
            Response.Redirect("status.aspx?StaId=2&ReqId=" + REQID)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub APPROVE_LEAVE_COMP(ByVal REQID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_COMP_REQ_APPROVED")
            sp.Command.AddParameter("@LMS_COMP_REQ", REQID, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDep()
            Dept()
            BindRM()
            BindDetails(Request.QueryString("ReqID"))
            txtAssociateID.ReadOnly = True
            txtAssociateName.ReadOnly = True
            ddlRM.Enabled = False
            ddlDep.Enabled = False
            txtDesig.ReadOnly = True
            txtWorking.ReadOnly = True
            txtComp.ReadOnly = True
            txtFromDate.ReadOnly = True
            txtToDate.ReadOnly = True
            txtContactNo.ReadOnly = True
        End If
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails(ByVal ReqID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_COMPOFF_GETBYREQID")
            sp.Command.AddParameter("@LMS_COM_REQID", ReqID, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("LMS_COM_AUR_ID")
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True
                txtContactNo.Text = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
                txtFromDate.Text = ds.Tables(0).Rows(0).Item("LMS_FDATE")
                txtToDate.Text = ds.Tables(0).Rows(0).Item("LMS_TDATE")
                txtWorking.Text = ds.Tables(0).Rows(0).Item("LMS_WORKING")
                txtComp.Text = ds.Tables(0).Rows(0).Item("LMS_COMP")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim REQID As String = Request.QueryString("ReqID")
            REJECT_LEAVE_ENCASH(REQID)
            Mail_RMRejected(REQID)
            Response.Redirect("status.aspx?StaId=2&ReqId=" + REQID)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        
    End Sub
    Private Sub REJECT_LEAVE_ENCASH(ByVal REQID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_COMP_REJECT")
            sp.Command.AddParameter("@LMS_COMP_REQ", REQID, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("frmLveViewRequisition.aspx")
    End Sub
    Private Sub Mail_RMApproved(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays, fromdt, todt, NoofHours, reason As String
        strSubj = " Compensatory Leave Approval."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_COMP_MAIL_REQUEST1")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LMS_COMREQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LMS_COMREQ_DATE")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LMS_COM_DAYS")
        fromdt = ds.Tables(0).Rows(0).Item("LMS_FDATE")
        todt = ds.Tables(0).Rows(0).Item("LMS_TDATE")
        NoofHours = ds.Tables(0).Rows(0).Item("LMS_COM_HRS")

        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "amantraadmin"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "amantraadmin"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Approved Compensatory Leave Request." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & NoofHours & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_COMP1")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Compensatory Leave has been Approved." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>" & NoofHours & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        strSubj = " Compensatory Leave Request Approved - by " & strAppEmpName & " from " & fromdt & " - " & todt & " for " & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Sub Mail_RMRejected(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays, fromdt, todt, NoofHours As String
        strSubj = " Compensatory Leave Rejected."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_COMP_MAIL_REQUEST2")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LMS_COMREQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LMS_COMREQ_DATE")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LMS_COM_DAYS")
        fromdt = ds.Tables(0).Rows(0).Item("LMS_FDATE")
        todt = ds.Tables(0).Rows(0).Item("LMS_TDATE")
        NoofHours = ds.Tables(0).Rows(0).Item("LMS_COM_HRS")

        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "amantraadmin"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "amantraadmin"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Reject of Compensatory Leave Request." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & NoofHours & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_COMP1")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Compensatory Leave has been Approved." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>" & NoofHours & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        strSubj = " Compensatory Leave Request Rejected - by " & strAppEmpName & " from " & fromdt & " - " & todt & " for " & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
End Class
