﻿
function dg_view_maintenance(User, req) {
    $("#UsersGrid").jqGrid("GridUnload")
    $("#UsersGrid").jqGrid(
    {
        url: '../../Generics_Handler/viewApprovedMaintenance.ashx?Mid=' + User + '&Mreq=' + req + '',
        datatype: 'json',
        height: 250,

        colNames: ['Requisition ID', 'Space ID', 'Request Description', 'Status', 'Alert'],
        colModel: [
                           { name: 'REQUESITION_ID', width: 170, sortable: true },
                           { name: 'SPACE_ID', width: 250, sortable: true },
                           { name: 'REQUEST_DESCRIPTION', width: 350, sortable: true },
                          { name: 'SER_STATUS', width: 150, sortable: true },
                          { name: 'ALERT', width: 40, sortable: true }
        ],
        rowNum: 10,
        rowList: [10, 50, 100, 150, 200, 250, 300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'REQUESITION_ID',
        viewrecords: true,
        sortorder: 'DESC'
        //caption: 'Report'
    }).navGrid('#UsersGridPager', { view: false, edit: false, del: false, search: false, add: false, refresh: true }
                     );

    // add custom button to export the data to excel
    jQuery("#UsersGrid").jqGrid('navButtonAdd', '#UsersGridPager',
    {
        caption: 'Export to Excel', buttonicon: "ui-icon-calculator", onClickButton: function () {
            var csv_url = 'Exports/viewApprovedMaintenance.aspx?Mid=' + User + '&Mreq=' + req + ''; jQuery("#UsersGrid").jqGrid('excelExport', { url: csv_url });
        }

    });
};
