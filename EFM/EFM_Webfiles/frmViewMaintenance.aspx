<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmViewMaintenance.aspx.vb" Inherits="EFM_EFM_Webfiles_frmViewMaintenance"
    Title="View Maintenance Requests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%-- <script language="JavaScript" type="text/javascript">

function confirmDelete()
 {

if (confirm("Are You Sure you want to Close the Request?")) 
{
       
  return true; 

}
 else
  {  
       
  return false; 

}
}
</script>--%>

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/flick/jquery-ui.css"
        type="text/css" media="all" />
    <link href="../../Source/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>


    <script src="../../Source/js/i18n/grid.locale-en.js" type="text/javascript"></script>

    <script src="../../Source/js/jquery.jqGrid.min.js" type="text/javascript"></script>

    <script src="../../EFM/EFM_Webfiles/ReportsJS/ViewMaintenance.js" type="text/javascript"></script>

    <script type="text/javascript">
        function dg_reportc() {
            var User = '<%=session("UID")%>';
    var req = $('#<%= txtreqid.ClientID %>').val();


    $("#UsersGrid").setGridParam({ url: '../../Generics_Handler/viewMaintenance.ashx?Mid=' + User + '&Mreq=' + req });
    dg_view_maintenance(User, req);
    $("#UsersGrid").trigger("reloadGrid");

}

window.onload = dg_reportc;

    </script>
    <div style="min-height: 500px">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">View Maintenance Requests
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="85%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>View Maintenance Requests</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                <td align="Center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />


                    <table id="t" runat="server" cellpadding="0" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="width:20%">
                                <div>
                                    Search by Requisition ID<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfv1" runat="server" Display="None" ErrorMessage="Please enter Requisition ID"
                                    ControlToValidate="txtreqid" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                </div>                                
                            </td>
                            <td>
                                <asp:TextBox ID="txtreqid" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            
                            <td>
                               <input type="button" value="Go" onclick="dg_reportc();" causesvalidation="TRUE" validationgroup="Val2"/>
                            </td>
                        </tr>
                    </table>

                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px; height: 100%;">&nbsp;</td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">&nbsp;</td>
                <td>&nbsp;
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px; height: 100%;">&nbsp;</td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">&nbsp;</td>
                <td align="CENTER" width="100%">

                    <table id="Table1" runat="server" cellpadding="0" width="100%" cellspacing="0">
                    </table>
                    <table id="UsersGrid" cellpadding="0" cellspacing="0" width="100%">
                    </table>
                    <div id="UsersGridPager" style="height:100%">
                    </div>

                </td>
                <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
