<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmViewUserMaintenance.aspx.vb" Inherits="EFM_EFM_Webfiles_frmViewUserMaintenance"
    Title="View Requests" %>

<%@ Register Src="InProgressRequser.ascx" TagName="InProgressRequser" TagPrefix="uc1" %>
<%@ Register Src="ClosedRequests.ascx" TagName="ClosedRequests" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/flick/jquery-ui.css"
        type="text/css" media="all" />
    <link href="../../Source/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>

    <script src="../../Source/js/i18n/grid.locale-en.js" type="text/javascript"></script>

    <script src="../../Source/js/jquery.jqGrid.min.js" type="text/javascript"></script>

    <script src="../../EFM/EFM_Webfiles/ReportsJS/ViewUserMaintenance.js" type="text/javascript"></script>

    <script type="text/javascript">
function dg_reportc()
{
var User =  '<%=session("UID")%>';
var req=" ";
             $("#UsersGrid").setGridParam({url:'../../Generics_Handler/ViewUserMaintenance.ashx?Mid='+User+'&Mreq='+req});
		        dg_view_maintenance(User,req);
		        $("#UsersGrid").trigger("reloadGrid");
	      }
		      window.onload = dg_reportc;
    </script>

    <div style="min-height: 500px">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">View Requests Status
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="85%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>View Requests Status</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table id="Table1" runat="server" cellpadding="0" width="100%" cellspacing="0">
                            </table>
                            <table id="UsersGrid" cellpadding="0" cellspacing="0" width="100%">
                            </table>
                            <div id="UsersGridPager" style="height:100%">
                            </div>
                </td>
                <td background="../../Images/table_right_mid_bg.gif">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
