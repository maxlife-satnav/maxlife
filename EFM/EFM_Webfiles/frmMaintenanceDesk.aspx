<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMaintenanceDesk.aspx.vb" Inherits="EFM_EFM_Webfiles_frmMaintenanceDesk"
    Title="Maintenance Desk" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            var toDate = new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if (sender._selectedDate < toDate) {
                document.getElementById("<%= txtconvdate.ClientID %>").value = "";
               alert("You can't select day earlier than today!");


               sender._selectedDate = toDate;
               //set the date back to the current date
               sender._textbox.set_Value(sender._selectedDate.format(sender._format))
           }
       }
    </script>

    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <div style="min-height: 425px;">
                <table cellspacing="0" cellpadding="0" width="100%">
                  
                        <tr>
                            <td align="center" width="100%">
                                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False" ForeColor="Black">Raise Request
             <hr align="center" width="60%" /></asp:Label>
                                &nbsp;
                                <br />
                            </td>
                        </tr>
           
                </table>
                <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                
                        <tr>
                            <td>
                                <img height="27" alt="" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                            <td class="tableHEADER" align="left" width="100%">&nbsp;<strong>Raise Request</strong></td>
                            <td style="WIDTH: 17px">
                                <img height="27" alt="" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                        </tr>

                        <tr>
                            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>

                            <td align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage" ForeColor="" ValidationGroup="Val1"></asp:ValidationSummary>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                                &nbsp; &nbsp; &nbsp; 
             <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
       

                     <tr>
                         <td align="center" colspan="2">
                             <asp:RadioButtonList ID="rbActions" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal" AutoPostBack="True">
                                 <asp:ListItem Value="Employee">Staff/Student</asp:ListItem>
                                 <asp:ListItem Value="Others">Others</asp:ListItem>
                             </asp:RadioButtonList>
                         </td>
                     </tr>


                     <tr id="temp" runat="server">
                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Enter Staff/Student ID<font class="clsNote">*</font>
                             <asp:RequiredFieldValidator ID="rfvempid" runat="server" ValidationGroup="Val2" ControlToValidate="txtempid" Display="NONE" ErrorMessage="Please Enter Staff/Student ID" Enabled="true"></asp:RequiredFieldValidator>
                         </td>
                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                             <asp:TextBox ID="txtempid" runat="server" CssClass="clsTextField" Width="97%" AutoComplete="off" AutoCompleteType="Disabled"></asp:TextBox>
                             <cc1:AutoCompleteExtender ID="autoCompleteEx1" runat="server" BehaviorID="AutoCompleteEx1" CompletionSetCount="5" MinimumPrefixLength="1" TargetControlID="txtempid" ServicePath="~/SearchUsers1.asmx" ServiceMethod="SearchUser1" ContextKey="0" CompletionInterval="1000" DelimiterCharacters=";,">
                             </cc1:AutoCompleteExtender>
                         </td>
                     </tr>



                     <%--<TR id="tstud" runat="server"><TD style="WIDTH: 50%; HEIGHT: 26px" align=left>Enter StudentId\Email<FONT class="clsNote">*</FONT> <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ValidationGroup="Val2" ControlToValidate="txtstuid" Display="NONE" ErrorMessage="Please Enter Employee ID or Email" Enabled="true"></asp:RequiredFieldValidator> </TD><TD style="WIDTH: 50%; HEIGHT: 26px" align=left><asp:TextBox id="txtstuid" tabIndex=2 runat="server" CssClass="clsTextField" Width="97%" AutoCompleteType="Disabled" MaxLength="50"></asp:TextBox> <cc1:autocompleteextender id="AutoCompleteExtender1" runat="server" behaviorid="AutoCompleteEx2" completionsetcount="5" minimumprefixlength="1" targetcontrolid="txtstuid" servicepath="~/SearchUsers2.asmx" servicemethod="SearchUser2" contextkey="0" completioninterval="1000" delimitercharacters=";,">
                    </cc1:autocompleteextender> </TD></TR>--%>


                     <tr id="search" runat="server">
                         <td align="center" colspan="6">
                             <asp:Button ID="btnsearch" TabIndex="3" runat="server" CssClass="button" Text="Search" CausesValidation="true" ValidationGroup="Val2"></asp:Button>&nbsp; </td>
                     </tr>



                     <tr id="tothers" runat="server">
                         <td colspan="4">
                             <table id="tothrs" runat="server" cellspacing="1" cellpadding="0" width="100%" border="1">
                             
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Name <font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvname" runat="server" ValidationGroup="Val1" ControlToValidate="txtname" Display="NONE" ErrorMessage="Please Enter Name" Enabled="true"></asp:RequiredFieldValidator>
                                         </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtname" TabIndex="3" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                         </td>
                                     </tr>

                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Email<%-- <FONT class="clsNote">*</FONT> <asp:RequiredFieldValidator id="rfvemail" runat="server" ValidationGroup="Val1" ControlToValidate="txtemail" Display="NONE" ErrorMessage="Please Enter Email" Enabled="true"></asp:RequiredFieldValidator> --%><asp:RegularExpressionValidator ID="revemail" runat="server" ValidationGroup="Val1" ControlToValidate="txtemail" Display="None" ErrorMessage="Please Enter Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtemail" TabIndex="4" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                         </td>
                                     </tr>

                                     <tr runat="server" visible="false">
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Contact Details<font class="clsNote">*</font> <%--<asp:RequiredFieldValidator id="rfvcontact" runat="server" ValidationGroup="Val1" ControlToValidate="txtcontact" Display="NONE" ErrorMessage="Please Enter Contact Details" Enabled="true"></asp:RequiredFieldValidator> <asp:RegularExpressionValidator id="revcontc" runat="server" ValidationGroup="Val1" ControlToValidate="txtcontact" Display="None" ErrorMessage="Please Enter Valid Contact Details" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator> --%></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtcontact" TabIndex="5" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                         </td>
                                     </tr>

                             
                             </table>
                         </td>
                     </tr>

                     <tr id="personal" runat="server">
                         <td colspan="4">

                             <table id="tpersonal" cellspacing="0" cellpadding="0" width="100%" border="1" runat="server">

                               
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Name </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtesname" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="true"></asp:TextBox>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Designation </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtdesig" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="true"></asp:TextBox>
                                         </td>
                                     </tr>

                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Department </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtdept" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="true"></asp:TextBox>
                                         </td>
                                     </tr>

                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Extension Number<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Val1" ControlToValidate="txtescontact" Display="NONE" ErrorMessage="Enter Extension Number"></asp:RequiredFieldValidator>
                                             <%--<asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" ValidationGroup="Val1" ControlToValidate="txtescontact" Display="None" ErrorMessage="Please Enter Numerics Only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>--%> </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtescontact" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                         </td>
                                     </tr>

                               
                             </table>
                         </td>
                     </tr>

                     <tr id="tdetails" runat="server">
                         <td align="center" colspan="2">

                             <table id="tdtls" cellspacing="0" cellpadding="0" width="100%" border="1" runat="server">
                              
                                     <tr>
                                         <td style="HEIGHT: 26px" align="left" width="50%">
                                             <asp:Label ID="lblSatus" runat="server" CssClass="bodytext" Text="Location"></asp:Label>
                                             <font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ValidationGroup="Val1" ControlToValidate="ddlType" Display="None" ErrorMessage="Select Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                         </td>
                                         <td align="left" width="50%">
                                             <asp:DropDownList ID="ddlType" runat="server" CssClass="clsComboBox" Width="99%" AutoPostBack="True">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">City<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvCity" runat="server" ValidationGroup="Val1" ControlToValidate="ddlCity" Display="NONE" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                         </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:DropDownList ID="ddlCity" TabIndex="6" runat="server" CssClass="clsComboBox" Width="99%" AutoPostBack="True">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>

                                     <tr id="loc" runat="server">
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Select Building<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvloc" runat="server" ValidationGroup="Val1" ControlToValidate="ddlloc" Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:DropDownList ID="ddlloc" TabIndex="14" runat="server" CssClass="clsComboBox" Width="99%" AutoPostBack="True"></asp:DropDownList>
                                         </td>
                                     </tr>

                                     <tr id="Twr" runat="server">
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Select Tower<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ValidationGroup="Val1" ControlToValidate="ddltwr" Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:DropDownList ID="ddltwr" TabIndex="14" runat="server" CssClass="clsComboBox" Width="99%" AutoPostBack="True"></asp:DropDownList>
                                         </td>
                                     </tr>


                                     <tr id="flr" runat="Server">
                                         <td align="left" style="height: 26px; width: 50%">Floor<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlfloor"
                                                 Display="NONE" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--"
                                                 Enabled="true"></asp:RequiredFieldValidator>
                                         </td>
                                         <td align="left" style="height: 26px; width: 50%">
                                             <asp:DropDownList ID="ddlfloor" runat="server" TabIndex="9" CssClass="clsComboBox"
                                                 AutoPostBack="true" Width="99%">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>
                                     <tr id="wng" runat="server">
                                         <td align="left" style="height: 26px; width: 50%">Wing<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvwing" runat="server" ControlToValidate="ddlwing"
                                                 Display="NONE" ErrorMessage="Please Select Wing" ValidationGroup="Val1" InitialValue="--Select--"
                                                 Enabled="true"></asp:RequiredFieldValidator>
                                         </td>
                                         <td align="left" style="height: 26px; width: 50%">
                                             <asp:DropDownList ID="ddlwing" runat="server" TabIndex="10" CssClass="clsComboBox"
                                                 AutoPostBack="true" Width="99%">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>




                                     <tr runat="Server" id="room">
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:Label ID="lblroom" runat="server" CssClass="bodytext" Text="SubLocation"></asp:Label>
                                             <font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvroom" runat="server" ValidationGroup="Val1" ControlToValidate="ddlSpace" Display="NONE" ErrorMessage="Please Select SubLocation" Enabled="true" InitialValue="0"></asp:RequiredFieldValidator></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:DropDownList ID="ddlSpace" TabIndex="14" runat="server" CssClass="clsComboBox" Width="99%">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>

                              
                             </table>
                         </td>
                     </tr>

                     <tr id="tabcomm" runat="server">
                         <td colspan="4">
                             <table id="common" cellspacing="0" cellpadding="1" width="100%" border="1" runat="server">
                             
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Service Request Category<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvreq" runat="server" ValidationGroup="Val1" ControlToValidate="ddlReq" Display="NONE" ErrorMessage="Please Select Service Request Category" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:DropDownList ID="ddlReq" TabIndex="11" runat="server" CssClass="clsComboBox" Width="99%" AutoPostBack="True">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Service Request<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvreqtype" runat="server" ValidationGroup="Val1" ControlToValidate="ddlreqtype" Display="NONE" ErrorMessage="Please Select Service Request" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:DropDownList ID="ddlreqtype" TabIndex="12" runat="server" CssClass="clsComboBox" Width="99%" AutoPostBack="True">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Problem Description<font class="clsNote">*</font>
                                             <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ValidationGroup="Val1" ControlToValidate="txtProbDesc" Display="NONE" ErrorMessage="Problem Description Required"></asp:RequiredFieldValidator>
                                         </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtProbDesc" TabIndex="15" runat="server" Width="97%" MaxLength="10000" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Convenient date when the personnel can visit to carry out the work <%--<FONT class="clsNote">*</FONT> <asp:RequiredFieldValidator id="rfvconvdate" runat="server" ValidationGroup="Val1" ControlToValidate="txtconvdate" Display="NONE" ErrorMessage="Please Select Convenient date" Enabled="true"></asp:RequiredFieldValidator>--%></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:TextBox ID="txtconvdate" runat="server" CssClass="clsTextField" Width="60%" AutoPostBack="True"></asp:TextBox>
                                             <%--  <asp:Image ID="Image1" runat="server" ImageUrl="~/images/calen1.gif.PNG" Height="21px"></asp:Image>--%>
                                             <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="txtconvdate" OnClientDateSelectionChanged="checkDate" TargetControlID="txtconvdate" Format="dd-MMM-yyyy">
                                             </cc1:CalendarExtender>
                                             <%-- <asp:LinkButton ID="LinkButton2" runat="server">Refresh convinient time</asp:LinkButton>--%>
                                         </td>
                                     </tr>
                                     <tr id="tr1" runat="server">
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Convenient time when the personnel can visit to carry out the work <%--<FONT class="clsNote">*</FONT> <asp:RequiredFieldValidator id="cfvHr" runat="server" ValidationGroup="Val1" ControlToValidate="cboHr" Display="None" ErrorMessage="Please Select Convenient time!" InitialValue="-HH--"></asp:RequiredFieldValidator>--%></td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:DropDownList ID="cboHr" runat="server" CssClass="clsComboBox" Width="48%" AutoPostBack="True">
                                             </asp:DropDownList>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">An email will be sent to ISB EmailID.If you wish to receive SMS as well,Please Click Yes<font class="clsNote">*</font> </td>
                                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                             <asp:RadioButtonList ID="radlstactions" runat="server" CssClass="clsRadioButton" RepeatDirection="horizontal" AutoPostBack="True">
                                                 <asp:ListItem Value="0">Yes</asp:ListItem>
                                                 <asp:ListItem Value="1" Selected="true">No</asp:ListItem>
                                             </asp:RadioButtonList>
                                         </td>
                                     </tr>
                            
                             </table>
                         </td>
                     </tr>

                     <tr id="Mobile" runat="server">
                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Enter Mobile Number<font class="clsNote">*</font>
                             <asp:RequiredFieldValidator ID="rfvSMSMobile" runat="server" ValidationGroup="Val1" ControlToValidate="txtSMSMobile" Display="NONE" ErrorMessage="Enter Mobile Number to send SMS"></asp:RequiredFieldValidator>
                             <asp:RegularExpressionValidator ID="rgvmobile" runat="server" ValidationGroup="Val1" ControlToValidate="txtSMSMobile" Display="None" ErrorMessage="Please Enter Numerics Only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                         </td>
                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                             <asp:TextBox ID="txtSMSMobile" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                         </td>
                     </tr>
                     <tr id="Email" runat="server">
                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Enter Email ID<font class="clsNote">*</font> <%--<asp:RequiredFieldValidator ID="rfvSMSEmail" runat="server" ValidationGroup="Val1"
                                            ErrorMessage="Enter Email id " Display="NONE" ControlToValidate="txtSMSEmail"></asp:RequiredFieldValidator>--%><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="Val1" ControlToValidate="txtSMSEmail" Display="None" ErrorMessage="Please Enter Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                         <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                             <asp:TextBox ID="txtSMSEmail" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                         </td>
                     </tr>
                     <tr id="btn" runat="server">
                         <td align="center" colspan="6">
                             <asp:Button ID="btnSubmit" TabIndex="17" runat="server" CssClass="button" Text="Submit" CausesValidation="true" ValidationGroup="Val1"></asp:Button>&nbsp; </td>
                     </tr>
           
             </table>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                            <td background="../../images/table_bot_mid_bg.gif">
                                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                            <td>
                                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                        </tr>
             
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
