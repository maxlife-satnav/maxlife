<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="ViewUserDetails.aspx.vb" Inherits="EFM_EFM_Webfiles_ViewUserDetails"
    Title="View Requests" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Solution.ascx" TagName="Solution" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function checkDate(sender, args) {
            var toDate= new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if ( sender._selectedDate < toDate) {
              document.getElementById("<%= txtconvdate.ClientID %>").value = "";
                alert("You can't select day earlier than today!");
                 
               
                sender._selectedDate = toDate;
                //set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>

    <%-- <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/flick/jquery-ui.css"
        type="text/css" media="all" />
    <link href="../../Source/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>

    <script src="../../Source/js/i18n/grid.locale-en.js" type="text/javascript"></script>

    <script src="../../Source/js/jquery.jqGrid.min.js" type="text/javascript"></script>

    <script src="../../EFM/EFM_Webfiles/ReportsJS/Activities.js" type="text/javascript"></script>

    <script type="text/javascript">

function dg_reportc()
    {
        hu = window.location.search.substring(1);
        gy = hu.split("&");
       for (i=0;i<gy.length;i++) 
         {
            ft = gy[i].split("=");
            if (ft[0] == "id") 
               {
                $("#UsersGrid").setGridParam({url:'../../Generics_Handler/Activities.ashx?MUser='+ft[1]});
		        dg_maintenance_Activities(ft[1]);
		        $("#UsersGrid").trigger("reloadGrid");
	           }
          } 
    }

		     window.onload = dg_reportc;
		       
    </script>--%>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">View  Requests Status Details
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>View Requests Status Details</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <br />
                    <table align="left" id="t1" runat="server" cellpadding="1" cellspacing="0" border="0"
                        width="100%">
                        <tr>
                            <td>
                                <table id="t6" align="left" runat="server" cellpadding="0" cellspacing="0" width="50%">
                                    <tr>
                                        <td align="left" style="height: 26px; width: 25%">
                                            Requisition ID : <strong>
                                                <asp:Label ID="lblreqid" runat="server"></asp:Label></strong>
                                        </td>
                                    </tr>
                                </table>
                                <table id="t7" align="right" runat="server" cellpadding="0" cellspacing="0" width="50%">
                                    <tr>
                                        <td align="right" style="height: 26px; width: 25%">
                                            Requested by:<strong>
                                                <asp:Label ID="lblreqby" runat="server"></asp:Label></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="height: 26px; width: 25%">
                                            Requested Date: <strong>
                                                <asp:Label ID="lblreqdate" runat="server"></asp:Label></strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                        <tr runat="server" visible="false">
                            <td align="center" colspan="2">
                                <asp:RadioButtonList ID="rbActions" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                    AutoPostBack="True" Enabled="False">
                                    <asp:ListItem Value="Employee">Staff/Student</asp:ListItem>
                                    <asp:ListItem Value="Others">Others</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr id="temp" runat="server" visible="false">
                            <td id="Td2" align="left" style="height: 26px; width: 50%" runat="server">
                                Staff/Student ID
                            </td>
                            <td id="Td3" align="left" style="height: 26px; width: 50%" runat="server">
                                <asp:TextBox ID="txtempid" runat="server" TabIndex="1" CssClass="clsTextField" Width="97%"
                                    MaxLength="50" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="tothers" runat="server">
                            <td id="Td6" colspan="4" runat="server">
                                <table id="tothrs" runat="server" cellpadding="0" cellspacing="1" width="100%" border="1">
                                    <tr id="Tr1" runat="server">
                                        <td id="Td7" align="left" style="height: 26px; width: 50%" runat="server">
                                            Name
                                        </td>
                                        <td id="Td8" align="left" style="height: 26px; width: 50%" runat="server">
                                            <asp:TextBox ID="txtname" runat="server" TabIndex="3" CssClass="clsTextField" Width="97%"
                                                MaxLength="50" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr2" runat="server">
                                        <td id="Td9" align="left" style="height: 26px; width: 50%" runat="server">
                                            Email
                                        </td>
                                        <td id="Td10" align="left" style="height: 26px; width: 50%" runat="server">
                                            <asp:TextBox ID="txtemail" runat="server" TabIndex="4" CssClass="clsTextField" Width="97%"
                                                MaxLength="50" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr3" runat="server" visible="FALSE">
                                        <td id="Td11" align="left" style="height: 26px; width: 50%" runat="server">
                                            Contact Details
                                        </td>
                                        <td id="Td12" align="left" style="height: 26px; width: 50%" runat="server">
                                            <asp:TextBox ID="txtcontact" runat="server" TabIndex="5" CssClass="clsTextField"
                                                Width="97%" MaxLength="10" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="personal" runat="server">
                            <td id="Td13" colspan="4" runat="server">
                                <table id="tpersonal" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                    border="1">
                                    <tr id="Tr4" runat="server">
                                        <td id="Td14" align="left" style="height: 26px; width: 50%" runat="server">
                                            Name
                                        </td>
                                        <td id="Td15" align="left" style="height: 26px; width: 50%" runat="server">
                                            <asp:TextBox ID="txtesname" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr5" runat="server">
                                        <td id="Td16" align="left" style="height: 26px; width: 50%" runat="server">
                                            Designation
                                        </td>
                                        <td id="Td17" align="left" style="height: 26px; width: 50%" runat="server">
                                            <asp:TextBox ID="txtdesig" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr6" runat="server">
                                        <td id="Td18" align="left" style="height: 26px; width: 50%" runat="server">
                                            Department
                                        </td>
                                        <td id="Td19" align="left" style="height: 26px; width: 50%" runat="server">
                                            <asp:TextBox ID="txtdept" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr7" runat="server">
                                        <td id="Td20" align="left" style="height: 26px; width: 50%" runat="server">
                                            Extension Number
                                        </td>
                                        <td id="Td21" align="left" style="height: 26px; width: 50%" runat="server">
                                            <asp:TextBox ID="txtescontact" runat="server" CssClass="clsTextField" Width="97%"
                                                ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%" style="height: 26px">
                                <asp:Label ID="lblSatus" runat="server" CssClass="bodytext" Text="Location"></asp:Label>
                            </td>
                            <td align="left" width="50%">
                                <asp:DropDownList ID="ddlType" runat="server" Width="99%" CssClass="clsComboBox"
                                    Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <table id="tdtls" runat="server" cellspacing="0" cellpadding="0" width="100%" border="1">
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            City
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlCity" runat="server" TabIndex="6" CssClass="clsComboBox"
                                                Width="99%" AutoPostBack="True" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="loc" runat="server">
                                        <td style="width: 50%; height: 26px" align="left">
                                            Building
                                            </td>
                                        <td style="width: 50%; height: 26px" align="left">
                                            <asp:DropDownList ID="ddlloc" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                Width="99%" enabled="false" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="Twr" runat="server">
                                        <td style="width: 50%; height: 26px" align="left">
                                             Tower
                                            </td>
                                        <td style="width: 50%; height: 26px" align="left">
                                            <asp:DropDownList ID="ddltwr" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                Width="99%" enabled="false" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="flr" runat="Server">
                                        <td align="left" style="height: 26px; width: 50%">
                                            Floor
                                            
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlfloor" runat="server" TabIndex="9" CssClass="clsComboBox"
                                                 Width="99%" enabled="false" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="wng" runat="server">
                                        <td align="left" style="height: 26px; width: 50%">
                                            Wing
                                            
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlwing" runat="server" TabIndex="10" CssClass="clsComboBox"
                                                 Width="99%" enabled="false" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                           <asp:Label id="lblroom" runat="server" CssClass="bodytext" Text="SubLocation"></asp:Label> </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlSpace" runat="server" TabIndex="14" CssClass="clsComboBox"
                                                Width="99%" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="tabcomm" runat="server">
                                        <td id="Td33" colspan="4" runat="server">
                                            <table id="common" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                                <tr id="Tr13" runat="server">
                                                    <td id="Td34" align="left" style="height: 26px; width: 50%" runat="server">
                                                        Service Request Category</td>
                                                    <td id="Td35" align="left" style="height: 26px; width: 50%" runat="server">
                                                        <asp:DropDownList ID="ddlReq" runat="server" TabIndex="11" CssClass="clsComboBox"
                                                            Width="99%" AutoPostBack="true" Enabled="false" >
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="Tr14" runat="server">
                                                    <td id="Td36" align="left" style="height: 26px; width: 50%" runat="server">
                                                        Service Category</td>
                                                    <td id="Td37" align="left" style="height: 26px; width: 50%" runat="server">
                                                        <asp:DropDownList ID="ddlreqtype" runat="server" TabIndex="12" CssClass="clsComboBox"
                                                            Width="99%" Enabled="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="Tr17" runat="server">
                                                    <td id="Td41" align="left" style="height: 26px; width: 50%" runat="server">
                                                        Problem Description
                                                    </td>
                                                    <td id="Td42" align="left" style="height: 26px; width: 50%" runat="server">
                                                        <asp:TextBox ID="txtProbDesc" runat="server" Width="97%" Rows="3" TabIndex="15" TextMode="MultiLine"
                                                            MaxLength="10000" ReadOnly="true" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="height: 26px; width: 50%">
                                                        Convenient date when the personnel can visit to carry out the work
                                                    </td>
                                                    <td align="left" style="height: 26px; width: 50%">
                                                        <asp:TextBox ID="txtconvdate" runat="server" CssClass="clsTextField" Width="40%"
                                                            AutoPostBack="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="tr8" runat="server">
                                                    <td align="left" style="height: 26px; width: 50%">
                                                        Convenient time when the personnel can visit to carry out the work
                                                        <asp:RequiredFieldValidator ID="cfvHr" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select Convenient time!"
                                                            Display="None" ControlToValidate="cboHr" InitialValue="-HH--"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td align="left" style="height: 26px; width: 50%">
                                                        <asp:DropDownList ID="cboHr" runat="server" Width="48%" CssClass="clsComboBox" Enabled="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="Tr21" runat="server">
                                                    <td align="left" style="height: 26px; width: 50%">
                                                        An email will be sent to EmailID.If you wish to receive SMS as well,Please Click
                                                        Yes
                                                    </td>
                                                    <td align="left" style="height: 26px; width: 50%">
                                                        <asp:RadioButtonList AutoPostBack="True" ID="radlstactions" runat="server" CssClass="clsRadioButton"
                                                            RepeatDirection="horizontal" Enabled="false">
                                                            <asp:ListItem Value="0">Yes</asp:ListItem>
                                                            <asp:ListItem Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="Mobile">
                                                    <td id="Td51" align="left" style="height: 26px; width: 50%" runat="server">
                                                        Mobile Number<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvSMSMobile" runat="server" ValidationGroup="Val1"
                                                            ControlToValidate="txtSMSMobile" Display="NONE" ErrorMessage="Enter Mobile Number to send SMS"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rgvmobile" runat="server" ValidationGroup="Val1"
                                                            ControlToValidate="txtSMSMobile" Display="None" ErrorMessage="Please Enter Mobile Number in Numerics Only"
                                                            ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                    </td>
                                                    <td id="Td52" align="left" style="height: 26px; width: 50%" runat="server">
                                                        <asp:TextBox ID="txtSMSMobile" runat="server" CssClass="clsTextField" MaxLength="10"
                                                            Width="97%" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="Email" visible="false">
                                                    <td id="Td53" align="left" style="height: 26px; width: 50%" runat="server">
                                                        Email ID</td>
                                                    <td id="Td54" align="left" style="height: 26px; width: 50%" runat="server">
                                                        <asp:TextBox ID="txtSMSEmail" runat="server" CssClass="clsTextField" Width="97%"
                                                            ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif">
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="width: 17px">
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
