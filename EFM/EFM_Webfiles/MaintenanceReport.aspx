<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="MaintenanceReport.aspx.vb" Inherits="EFM_EFM_Webfiles_MaintenanceReport"
    Title="Maintenance Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr>
                        <td align="center" width="100%">
                            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" ForeColor="Black" Font-Underline="False" Width="86%" __designer:wfdid="w77">Maintenance Report
             <hr align="center" width="60%" /></asp:Label>
                            &nbsp;
                            <br />
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="VERTICAL-ALIGN: top" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                <tbody>
                    <tr>
                        <td align="left" colspan="3">
                            <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. " __designer:wfdid="w78">(*) Mandatory Fields. </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img height="27" alt="" src="../../images/table_left_top_corner.gif" width="9" /></td>
                        <td class="tableHEADER" align="left" width="100%">&nbsp;<strong>Maintenance Report</strong></td>
                        <td>
                            <img height="27" alt="" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage" ForeColor="" ValidationGroup="Val1" __designer:wfdid="w79"></asp:ValidationSummary>
                            <br />
                            <br />
                            <asp:Label ID="lblMsg" runat="Server" CssClass="clsMessage" __designer:wfdid="w80"></asp:Label>
                            <br />
                            <table id="t1" cellspacing="0" cellpadding="2" width="100%" runat="Server">
                                
                                    <tr>
                                        <td style="WIDTH: 33%; HEIGHT: 26px" align="center">
                                            <asp:LinkButton ID="lbtnopen" runat="server" __designer:wfdid="w81"></asp:LinkButton>
                                            <%--   <a href='mreport.aspx?Dep=" "&User=" "&FromDate=" "&toDate=" "&stat=1&Usertype=" " '>
                      
                                <asp:Label ID="lbtnopen" runat="server"></asp:Label></a>--%></td>
                                        <td style="WIDTH: 33%; HEIGHT: 26px" align="center">
                                            <asp:LinkButton ID="lbtnprogress" runat="server" __designer:wfdid="w82"></asp:LinkButton>
                                            <%-- <a href='mreport.aspx?Dep=" "&User=" "&FromDate=" "&toDate=" "&stat=2&Usertype=" " '>
                                <asp:Label ID="lbtnprogress" runat="server"></asp:Label></a>--%></td>
                                        <td style="WIDTH: 33%; HEIGHT: 26px" align="center">
                                            <asp:LinkButton ID="lbtnclosed" runat="server" __designer:wfdid="w83"></asp:LinkButton>
                                            <%-- <a href='mreport.aspx?Dep=" "&User=" "&FromDate=" "&toDate=" "&stat=3&Usertype=" " '>
                                <asp:Label ID="lbtnclosed" runat="server"></asp:Label></a>--%></td>
                                    </tr>
                                
                            </table>
                            <table id="tab" cellspacing="0" cellpadding="1" width="100%" border="1" runat="server">
                              
                                    <tr runat="server" visible="False">
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Select Department<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvdep" runat="server" ValidationGroup="Val1" InitialValue="--Select--" ErrorMessage="Please Select Department" Display="None" ControlToValidate="ddldep" __designer:wfdid="w84"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                            <asp:DropDownList ID="ddldep" runat="server" CssClass="clsComboBox" Width="97%" AutoPostBack="True" __designer:wfdid="w85">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Select User<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvassigned" runat="server" ValidationGroup="Val1" InitialValue="--Select--" ErrorMessage="Please Select Assigned User" Display="None" ControlToValidate="ddlassigned" __designer:wfdid="w86"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                            <asp:DropDownList ID="ddlassigned" runat="server" CssClass="clsComboBox" Width="97%" AutoPostBack="true" __designer:wfdid="w87">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Employee">Staff/Student</asp:ListItem>
                                                <asp:ListItem Value="Others">Others</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="temp" runat="server">
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Enter Staff/Student ID<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvempid" runat="server" ValidationGroup="Val2" ErrorMessage="Please Enter Staff/Student ID" Display="NONE" ControlToValidate="txtempid" Enabled="true" __designer:wfdid="w88"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                            <asp:TextBox ID="txtempid" runat="server" CssClass="clsTextField" Width="97%" AutoCompleteType="Disabled" AutoComplete="off" __designer:wfdid="w89"></asp:TextBox>
                                            <cc1:AutoCompleteExtender ID="autoCompleteEx1" runat="server" DelimiterCharacters=";," CompletionInterval="1000" ContextKey="0" ServiceMethod="SearchUser1" ServicePath="~/SearchUsers1.asmx" TargetControlID="txtempid" MinimumPrefixLength="1" CompletionSetCount="5" BehaviorID="AutoCompleteEx1" __designer:wfdid="w90">
                                            </cc1:AutoCompleteExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Select From Date<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ValidationGroup="Val1" ErrorMessage="Please Enter From Date" Display="None" ControlToValidate="txtfromdate" __designer:wfdid="w91"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                            <asp:TextBox ID="txtfromdate" runat="server" CssClass="clsTextField" Width="97%" __designer:wfdid="w92"></asp:TextBox>
                                            <%--<cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                TargetControlID="txtfromdate">
                            </cc1:CalendarExtender>--%></td>
                                    </tr>
                                    <tr>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">Select To Date<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Val1" ErrorMessage="Please Enter To Date" Display="None" ControlToValidate="txttodate" __designer:wfdid="w93"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="WIDTH: 50%; HEIGHT: 26px" align="left">
                                            <asp:TextBox ID="txttodate" runat="server" CssClass="clsTextField" Width="97%" __designer:wfdid="w94"></asp:TextBox>
                                            <%--<cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                TargetControlID="txttodate">
                            </cc1:CalendarExtender>--%></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" CausesValidation="true" CssClass="button" ValidationGroup="Val1" __designer:wfdid="w95"></asp:Button>
                                        </td>
                                    </tr>
                                
                            </table>
                        </td>
                        <td style="WIDTH: 10px; HEIGHT: 100%" background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="WIDTH: 10px; HEIGHT: 17px">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="HEIGHT: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="WIDTH: 17px; HEIGHT: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnsubmit" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <%-- </div>--%>
</asp:Content>
