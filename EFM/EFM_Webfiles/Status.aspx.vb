Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_Status
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim StaID As Integer = GetStatusId()
            Dim ReqId As String = GetRequestId()
            Dim Message As String = ""

            Dim StatusId As Integer = GetRequestStatus(ReqId, StaID)
            Select Case StatusId
                Case 1047
                    Message = "Requisition (" + ReqId + ") raised successfully."
                Case 1048
                    Message = "Requisition (" + ReqId + ") updated successfully."
                Case 1050
                    Message = "Requisition (" + ReqId + ") canceled successfully."
                Case 1051
                    Message = "Requisition (" + ReqId + ") approved by RM."
                Case 1052
                    Message = "Requisition (" + ReqId + ") rejected by RM."
            End Select

            lblMsg.Text = Message
            lblMsg.Visible = True
        End If
    End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("ReqID")
        Return ReqId
    End Function
    Private Function GetStatusId() As String
        Dim StaID As String = Request("StaId")
        Return StaID
    End Function
    Private Function GetRequestStatus(ByVal ReqId As String, ByVal Sta As Integer) As String
        Dim StatusId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_REQUISITION_GetByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, Data.DbType.String)
        sp.Command.AddParameter("@StaId", Sta, DbType.Int32)
        Dim dr As SqlDataReader = sp.GetReader
        If dr.Read() Then
            Integer.TryParse(dr("LMS_STATUS_ID"), StatusId)
        End If
        Return StatusId
    End Function
End Class
