<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HRViewDetails.aspx.vb" Inherits="ESP_ESP_Webfiles_HRViewDetails" Title="View Raised Requests" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>HR Approval for Raise Request
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">

                            <div class="row">
                              <div class="col-md-12">
                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Approval Request Found.">
                                        <Columns>
                                            <asp:TemplateField Visible="false">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("REQ_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:HyperLinkField DataTextField="ASSOCIATENAME" DataNavigateUrlFields="REQ_ID" DataNavigateUrlFormatString="~/EFM/EFM_Webfiles/HRApprovalRaiseRequest.aspx?id={0}" HeaderText="RequestedBy" />


                                            <asp:TemplateField HeaderText="Requested On">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblToDate" runat="server" CssClass="bodyText" Text='<%#Eval("CREATED_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Card Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltype" runat="Server" CssClass="bodyText" Text='<%#Eval("CARD_TYPE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


