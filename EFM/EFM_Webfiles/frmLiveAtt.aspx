<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmLiveAtt.aspx.vb" Inherits="ESP_ESP_Webfiles_frmLiveAtt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../BootStrapCSS/datepicker.css">
    <link href="../../BootStrapCSS/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/amantra.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/form-style.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <script language="javascript" type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Attendance
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">


                            <div class="row">
                               <div class="col-md-12">
                                    <asp:GridView ID="gvIn" runat="server"  CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                                        EmptyDataText="Sorry! No Records Found" BorderStyle="None">
                                        <Columns>
                                            <asp:BoundField DataField="INTIME" HeaderText="In-Time" />
                                            <%--<asp:TemplateField HeaderText="In-Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInTime1" runat="server" Text='<%#Eval("INTIME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                        </Columns>
                                          <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-md-12">
                                    <asp:GridView ID="gvOut" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                                        BorderStyle="None" SelectedRowStyle-VerticalAlign="Top">
                                        <Columns>
                                            <asp:BoundField DataField="OUTTIME" HeaderText="Out-Time" />
                                            <%-- <asp:TemplateField HeaderText="Out-Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOutTime1" runat="server" Text='<%#Eval("OUTTIME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                        </Columns>
                                          <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/bootstrap.min.js"></script>
    <script src='../../BootStrapCSS/Scripts/bootstrap-datepicker.js'></script>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

</body>
</html>


