<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmLveRMApprovalCompOff.aspx.vb" Inherits="ESP_ESP_Webfiles_frmLveRMApprovalCompOff" Title="Approval For CompOff Leaves" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>RM Comp. Off Approval
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlDep" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAssociateID" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlRM" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Contact Number<span style="color: red;">*</span></label>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvstartdate" runat="server" ControlToValidate="txtFromDate"
                                            Display="None" ErrorMessage="Please pick FromDate " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Click On the TextBox of FromDate to Pick the date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtToDate"
                                            Display="None" ErrorMessage="Please pick ToDate " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Click On the TextBox of ToDate to Pick the date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Working Hours<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtWorking" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Compensatory Days<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtComp" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="row">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" CausesValidation="true" />
                                    <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

