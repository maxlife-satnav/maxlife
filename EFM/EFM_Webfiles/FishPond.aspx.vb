Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_FishPond
    Inherits System.Web.UI.Page

    'Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
    '    gvitems.PageIndex = e.NewPageIndex()
    '    BindGrid()
    'End Sub

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    If Not IsPostBack Then
    '        Dim Validate_User As Integer
    '        Validate_User = Validate_SuperUser()
    '        If Validate_User = 0 Then
    '            'Employee
    '            BindGrid()
    '            pnlFishpond.Visible = True
    '            pnlfishponssuperuser.Visible = False
    '        Else
    '            'Super User
    '            BindGridSuperUser()
    '            pnlFishpond.Visible = False
    '            pnlfishponssuperuser.Visible = True

    '        End If
    '    End If
    'End Sub
    'Private Sub BindGridSuperUser()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_SP_GETFISHPOND")
    '        gvitemssuperuser.DataSource = sp.GetDataSet()
    '        gvitemssuperuser.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Private Function Validate_SuperUser()
    '    Dim Validate_User As Integer
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_Validate_SUPERUSER")
    '    sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
    '    Validate_User = sp.ExecuteScalar()
    '    Return Validate_User
    'End Function
    'Private Sub BindGrid()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_SP_GETFISHPOND")
    '        gvitems.DataSource = sp.GetDataSet()
    '        gvitems.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
    '    If txtFishPondMsg.Text <> "" Then
    '        AddFishPond()
    '        BindGrid()
    '        BindGridSuperUser()
    '        txtFishPondMsg.Text = ""
    '    End If
    'End Sub
    'Private Sub AddFishPond()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_ADD_FISHPOND")
    '        sp.Command.AddParameter("@MESSAGE", txtFishPondMsg.Text, DbType.String)
    '        sp.ExecuteScalar()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Protected Sub gvitemssuperuser_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitemssuperuser.PageIndexChanging
    '    gvitemssuperuser.PageIndex = e.NewPageIndex()
    '    BindGridSuperUser()
    'End Sub

    'Protected Sub gvitemssuperuser_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitemssuperuser.RowCommand
    '    If e.CommandName = "DELETE" Then
    '        Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
    '        Dim lblsupersno As Label = DirectCast(gvitemssuperuser.Rows(rowIndex).FindControl("lblsupersno"), Label)
    '        Dim sno1 As Integer = lblsupersno.Text
    '        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_FISHPOND_MESSAGE_DELETE")
    '        SP.Command.AddParameter("@SNO", sno1, DbType.Int32)
    '        SP.ExecuteScalar()
    '        BindGridSuperUser()
    '    End If
    'End Sub

    'Protected Sub gvitemssuperuser_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvitemssuperuser.RowDeleting

    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SP_ADD_EMPLOYEE_FISH_POND")
            sp.Command.AddParameter("@SUGGESTION_TYPE", txtSuggestionType.Text, DbType.String)
            sp.Command.AddParameter("@REMARKS", txtFishPondMsg.Text, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Text = "Your message has been sent to HR for approval"
            ClearDetails()
        Catch ex As Exception

            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ClearDetails()
        txtSuggestionType.Text = ""
        txtFishPondMsg.Text = ""
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_FISHPOND_GET_MESSAGES")
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
End Class
