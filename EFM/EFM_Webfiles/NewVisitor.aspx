<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NewVisitor.aspx.vb" Inherits="ESP_ESP_Webfiles_NewVisitor" Title="Add Visitor" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Add Visitor
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Visitor Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvvisitorname" runat="server" Display="none" ErrorMessage="Please Enter Visitor Name"
                                                ControlToValidate="txtvisitorName" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtvisitorName" runat="server" CssClass="form-control"
                                                    MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Email<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvemail" runat="server" Display="none" ErrorMessage="Please Enter Email"
                                                ControlToValidate="txtemail" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revld2email" runat="server" ControlToValidate="txtemail"
                                                ErrorMessage="Please Enter valid  Email " Display="None" ValidationGroup="Val1" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Mobile<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RFVMOBILE" runat="server" Display="none"
                                                ErrorMessage="Please Enter Mobile Number" ControlToValidate="txtvisitorName" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revld2mob" runat="server" ControlToValidate="txtmobile"
                                                ErrorMessage="Please Enter valid Mobile Number " ValidationGroup="Val1" Display="None" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtmobile" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Address<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RFVADDRESS" runat="server" Display="none"
                                                ErrorMessage="Please Enter Address" ControlToValidate="txtvisitorName" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtaddress" runat="Server" CssClass="form-control" TextMode="MultiLine"
                                                    Rows="3" MaxLength="1000"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Whom to Meet<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvmeet" runat="server" Display="none" ErrorMessage="Please Enter Person Name Whom You are Going to Meet"
                                                ControlToValidate="txtMeet" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtMeet" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="row">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
