<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmEditProfile.aspx.vb" Inherits="ESP_ESP_Webfiles_frmEditProfile" Title="Edit Profile" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap-select.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/amantra.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/font-awesome/css/font-awesome.min.css")%>' rel="stylesheet" />
    <link rel="stylesheet" href='<%= Page.ResolveUrl("~/BootStrapCSS/datepicker.css")%>' />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <script language="javascript" type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Employee Details
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">EmployeeID</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtEmpID" runat="server" CssClass="form-control" MaxLength="50" TabIndex="1"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">IT ID</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtITID" runat="server" CssClass="form-control" MaxLength="50" TabIndex="2"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">First Name <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvFname" runat="server" ControlToValidate="txtFrstName"
                                                Display="None" ErrorMessage="Please Enter First Name!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtFrstName" runat="server" CssClass="form-control"
                                                    MaxLength="50" TabIndex="3"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Middle Name</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtMdlName" runat="server" CssClass="form-control" MaxLength="50" TabIndex="4"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">
                                                Last Name</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtLstName" runat="server" CssClass="form-control" MaxLength="50" TabIndex="5"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Email<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                Display="None" ErrorMessage="Please Enter Email!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revEmail3" runat="server" ControlToValidate="txtEmail"
                                                Display="None" ErrorMessage="Please enter valid Email" ValidationGroup="Val1"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="50" TabIndex="6"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Mobile Number<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvPhno" runat="server" ControlToValidate="txtRes"
                                                Display="None" ErrorMessage="Please Enter Residence Phno!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revphno" runat="server" ControlToValidate="txtRes"
                                                Display="None" ErrorMessage="Please Enter Valid Residence Phno." ValidationExpression="^[0-9]*$"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRes" runat="server" CssClass="form-control" MaxLength="50" TabIndex="7"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Date of Birth <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDOB"
                                                ErrorMessage="Please Select Date of Birth" ValidationGroup="Val1" SetFocusOnError="True"
                                                Display="None"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" TabIndex="8"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Date of Joining</label>
                                            <%-- <span style="color: red;">*</span>--%>
                                            <%-- <asp:RequiredFieldValidator ID="rfvDOJ" runat="server" ControlToValidate="txtDOJ"
                                        ErrorMessage="Please Select Date of Joining" ValidationGroup="Val1" SetFocusOnError="True"
                                        Display="None"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtDOJ" runat="server" CssClass="form-control" ReadOnly="true" TabIndex="9"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Reporting Manager<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvmgr" runat="server" ControlToValidate="ddlRept"
                                                Display="None" ErrorMessage="Please Select Reporting Manager" ValidationGroup="Val1"
                                                InitialValue="0">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlRept" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="10">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Extension No. <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvExt" runat="server" ControlToValidate="txtExtn"
                                                Display="None" ErrorMessage="Please Enter Extension No.!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revExt" runat="server" ControlToValidate="txtExtn"
                                                Display="None" ErrorMessage="Invalid Extension No." ValidationExpression="^[0-9]*$"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtExtn" runat="server" CssClass="form-control" MaxLength="50" TabIndex="11"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Select Department <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvdept" runat="server" ControlToValidate="ddlDept"
                                                Display="None" ErrorMessage="Please Select Department" ValidationGroup="Val1"
                                                InitialValue="0">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlDept" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" TabIndex="12">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Employee Type<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvEType" runat="server" ControlToValidate="ddlEmpType"
                                                Display="None" ErrorMessage="Please Select EmployeeType" ValidationGroup="Val1"
                                                InitialValue="0">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlEmpType" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="13">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="Employee">Employee</asp:ListItem>
                                                    <asp:ListItem Value="Contract">Contract</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Grade<span style="color: red;">*</span></label>

                                            <asp:RequiredFieldValidator ID="rfvGreade" runat="server" Width="48px" ControlToValidate="ddlGrade"
                                                Display="None" ErrorMessage="Please Select Grade!!" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlGrade" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="14">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                                Display="None" ErrorMessage="Select Status" ValidationGroup="Val1" InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="15">
                                                    <asp:ListItem>--Select Status--</asp:ListItem>
                                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                                    <asp:ListItem Value="0">InActive</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Change Password(Maximum 8 characters)</label>
                                            <asp:CompareValidator ID="CmpValpwd" runat="server" ControlToValidate="txtpwd" Display="None"
                                                ErrorMessage="Passwords do not match" ControlToCompare="txtcpwd" ValidationGroup="Val1"></asp:CompareValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter password upto 8 characters')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtpwd" runat="server" CssClass="form-control" MaxLength="15"
                                                        TextMode="Password" TabIndex="16"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Confirm Password(Maximum 8 characters)</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtcpwd" runat="server" CssClass="form-control" MaxLength="15"
                                                    TextMode="Password" TabIndex="17"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <h4>
                                                <label class="col-md-12 control-label" style="text-align: left;">Personal Details </label>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Father Name <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvFathName" runat="server" ControlToValidate="txtFthrName"
                                                Display="None" ErrorMessage="Please Enter Father Name!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtFthrName" runat="server" CssClass="form-control"
                                                    MaxLength="50" TabIndex="18"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Blood Group <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvBG" runat="server" ControlToValidate="txtBldGrp"
                                                Display="None" ErrorMessage="Please Enter Blood Group!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtBldGrp" runat="server" CssClass="form-control" MaxLength="50" TabIndex="19"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Date of Anniversary</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtDOA" runat="server" CssClass="form-control" TabIndex="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Address<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtAddr"
                                                Display="None" ErrorMessage="Please Enter Address!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtAddr" runat="server" CssClass="form-control" MaxLength="50"
                                                    Rows="3" TextMode="MultiLine" TabIndex="21"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Select Shift <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvshift" runat="server" ControlToValidate="ddlShift"
                                                Display="None" ErrorMessage="Select Shift" ValidationGroup="Val1" InitialValue="--Select Shift--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlShift" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="22">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Experience<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvExp" runat="server" ControlToValidate="txtExp"
                                                Display="None" ErrorMessage="Please Enter Experience!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtExp" runat="server" CssClass="form-control" MaxLength="50" TabIndex="23"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Skills<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvSkills" runat="server" ControlToValidate="txtSkills"
                                                Display="None" ErrorMessage="Please Enter Skills!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtSkills" runat="server" CssClass="form-control" MaxLength="200"
                                                    Rows="3" TextMode="MultiLine" TabIndex="24"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Qualification<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvQual" runat="server" ControlToValidate="txtQlfctn"
                                                Display="None" ErrorMessage="Please Enter Qualification!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtQlfctn" runat="server" CssClass="form-control" MaxLength="200"
                                                    Rows="3" TextMode="MultiLine" TabIndex="25"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Passport No <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvpasport" runat="server" ControlToValidate="txtPsptNo"
                                                Display="None" ErrorMessage="Please Enter Passport No.!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtPsptNo" runat="server" CssClass="form-control" MaxLength="50" TabIndex="26"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Pancard No<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvpan" runat="server" ControlToValidate="txtPanNo"
                                                Display="None" ErrorMessage="Please Enter Pancard No.!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtPanNo" runat="server" CssClass="form-control" MaxLength="50" TabIndex="27"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Insurance No<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvInc" runat="server" ControlToValidate="txtInsNo"
                                                Display="None" ErrorMessage="Please Enter Insurance No.!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtInsNo" runat="server" CssClass="form-control" MaxLength="50" TabIndex="28"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Emergency No<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvemer" runat="server" ControlToValidate="txtEmerNo"
                                                Display="None" ErrorMessage="Please Enter Emergency No.!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtEmerNo" runat="server" CssClass="form-control" MaxLength="50" TabIndex="29"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--<asp:Panel ID="pnlChk" runat="server" CssClass="tbl" Width="100%">
                            <table cellspacing="0" cellpadding="0" width="100%" border="1">
                                <tr>
                                    <td align="Center" width="100%">
                                        <strong>Roles</strong>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:CheckBoxList ID="chkList" runat="server" Width="100%" RepeatColumns="3">
                            </asp:CheckBoxList>
                        </asp:Panel>--%>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="row">
                                        <asp:Button ID="btnNext" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"
                                            CausesValidation="true" TabIndex="30"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-datepicker.js") %>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/ObjectKeys.js")%>'></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/wz_tooltip.js")%>' type="text/javascript"></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" temp_src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" temp_src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" temp_src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>
</html>



