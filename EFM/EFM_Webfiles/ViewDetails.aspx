<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="ViewDetails.aspx.vb" Inherits="EFM_EFM_Webfiles_ViewDetails" Title="View Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Solution.ascx" TagName="Solution" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/flick/jquery-ui.css"
        type="text/css" media="all" />
    <link href="../../Source/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>

    <script src="../../Source/js/i18n/grid.locale-en.js" type="text/javascript"></script>

    <script src="../../Source/js/jquery.jqGrid.min.js" type="text/javascript"></script>

    <script src="../../EFM/EFM_Webfiles/ReportsJS/Activities.js" type="text/javascript"></script>
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>
    <script type="text/javascript">


        function checkDate(sender, args) {
            var toDate = new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if (sender._selectedDate < toDate) {
                document.getElementById("<%= txtconvdate.ClientID %>").value = "";
                alert("You can't select day earlier than today!");


                sender._selectedDate = toDate;
                //set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }


        function dg_reportc() {
            hu = window.location.search.substring(1);
            gy = hu.split("&");
            for (i = 0; i < gy.length; i++) {
                ft = gy[i].split("=");
                if (ft[0] == "id") {
                    $("#UsersGrid").setGridParam({ url: '../../Generics_Handler/Activities.ashx?MUser=' + ft[1] });
                    dg_maintenance_Activities(ft[1]);
                    $("#UsersGrid").trigger("reloadGrid");
                }
            }
        }

        window.onload = dg_reportc;

    </script>

    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">View Maintenance Requests
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="85%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>View Maintenance Requests</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <br />
                    <table align="left" id="t1" runat="server" cellpadding="1" cellspacing="0" border="0"
                        width="100%">
                        <tr>
                            <td>
                                <table id="t6" align="left" runat="server" cellpadding="0" cellspacing="0" width="50%">
                                    <tr>
                                        <td align="left" style="height: 26px; width: 25%">Requisition ID : <strong>
                                            <asp:Label ID="lblreqid" runat="server"></asp:Label></strong>
                                        </td>
                                    </tr>
                                </table>
                                <table id="t7" align="right" runat="server" cellpadding="0" cellspacing="0" width="50%">
                                    <tr>
                                        <td align="right" style="height: 26px; width: 25%">Requested by:<strong>
                                            <asp:Label ID="lblreqby" runat="server"></asp:Label></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="height: 26px; width: 25%">Requested Date: <strong>
                                            <asp:Label ID="lblreqdate" runat="server"></asp:Label></strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <asp:Menu ID="mnuTabs" runat="server" Orientation="Horizontal" BackColor="#F7F6F3"
                        DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#004040"
                        StaticSubMenuIndent="10px" Width="55%" Height="30px" BorderColor="#404040">
                        <Items>
                            <asp:MenuItem Selected="True" Text="General" Value="0"></asp:MenuItem>
                            <asp:MenuItem Text="Solution" Value="1"></asp:MenuItem>
                            <asp:MenuItem Text="Activities" Value="2"></asp:MenuItem>
                        </Items>
                        <StaticSelectedStyle BackColor="#5D7B9D" />
                        <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <DynamicHoverStyle BackColor="#7C6F57" ForeColor="White" />
                        <DynamicMenuStyle BackColor="#F7F6F3" />
                        <DynamicSelectedStyle BackColor="#5D7B9D" />
                        <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                        <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
                    </asp:Menu>
                    <asp:MultiView ID="multiTabs" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                                <tr runat="server" visible="false">
                                    <td align="center" colspan="2">
                                        <asp:RadioButtonList ID="rbActions" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                            AutoPostBack="True" Enabled="False">
                                            <asp:ListItem Value="Employee">Staff/Student</asp:ListItem>
                                            <asp:ListItem Value="Others">Others</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr id="temp" runat="server">
                                    <td id="Td2" align="left" style="height: 26px; width: 50%" runat="server">Staff/Student ID
                                    </td>
                                    <td id="Td3" align="left" style="height: 26px; width: 50%" runat="server">
                                        <asp:TextBox ID="txtempid" runat="server" TabIndex="1" CssClass="clsTextField" Width="97%"
                                            MaxLength="50" ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="tothers" runat="server">
                                    <td id="Td6" colspan="4" runat="server">
                                        <table id="tothrs" runat="server" cellpadding="0" cellspacing="1" width="100%" border="1">
                                            <tr id="Tr1" runat="server">
                                                <td id="Td7" align="left" style="height: 26px; width: 50%" runat="server">Name
                                                </td>
                                                <td id="Td8" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtname" runat="server" TabIndex="3" CssClass="clsTextField" Width="97%"
                                                        MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="Tr2" runat="server">
                                                <td id="Td9" align="left" style="height: 26px; width: 50%" runat="server">Email
                                                </td>
                                                <td id="Td10" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtemail" runat="server" TabIndex="4" CssClass="clsTextField" Width="97%"
                                                        MaxLength="50" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="Tr3" runat="server" visible="false">
                                                <td id="Td11" align="left" style="height: 26px; width: 50%" runat="server">Contact Details
                                                </td>
                                                <td id="Td12" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtcontact" runat="server" TabIndex="5" CssClass="clsTextField"
                                                        Width="97%" MaxLength="10" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="personal" runat="server">
                                    <td id="Td13" colspan="4" runat="server">
                                        <table id="tpersonal" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                            border="1">
                                            <tr id="Tr4" runat="server">
                                                <td id="Td14" align="left" style="height: 26px; width: 50%" runat="server">Name
                                                </td>
                                                <td id="Td15" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtesname" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="Tr5" runat="server">
                                                <td id="Td16" align="left" style="height: 26px; width: 50%" runat="server">Designation
                                                </td>
                                                <td id="Td17" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtdesig" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="Tr6" runat="server">
                                                <td id="Td18" align="left" style="height: 26px; width: 50%" runat="server">Department
                                                </td>
                                                <td id="Td19" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtdept" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="Tr7" runat="server">
                                                <td id="Td20" align="left" style="height: 26px; width: 50%" runat="server">Extension Number
                                                </td>
                                                <td id="Td21" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtescontact" runat="server" CssClass="clsTextField" Width="97%"
                                                        ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%" style="height: 26px">
                                        <asp:Label ID="lblSatus" runat="server" CssClass="bodytext" Text="Select Location"></asp:Label>
                                        <%--<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlType"
                                    Display="None" ErrorMessage="Select Request Type" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:DropDownList ID="ddlType" runat="server" Width="97%" CssClass="clsComboBox"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <table id="tdtls" runat="server" cellspacing="0" cellpadding="0" width="100%" border="1">
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">City<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity"
                                                        Display="NONE" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:DropDownList ID="ddlCity" runat="server" TabIndex="6" CssClass="clsComboBox"
                                                        Width="99%" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="loc" runat="server">
                                                <td style="width: 50%; height: 26px" align="left">Select Building<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvloc" runat="server" ValidationGroup="Val1" ControlToValidate="ddlloc"
                                                        Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                                <td style="width: 50%; height: 26px" align="left">
                                                    <asp:DropDownList ID="ddlloc" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                        Width="99%" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="Twr" runat="server">
                                                <td style="width: 50%; height: 26px" align="left">Select Tower<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ValidationGroup="Val1" ControlToValidate="ddltwr"
                                                        Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                                <td style="width: 50%; height: 26px" align="left">
                                                    <asp:DropDownList ID="ddltwr" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                        Width="99%" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="flr" runat="Server">
                                                <td align="left" style="height: 26px; width: 50%">Floor<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlfloor"
                                                        Display="NONE" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--"
                                                        Enabled="true"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:DropDownList ID="ddlfloor" runat="server" TabIndex="9" CssClass="clsComboBox"
                                                        AutoPostBack="true" Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="wng" runat="server">
                                                <td align="left" style="height: 26px; width: 50%">Wing<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvwing" runat="server" ControlToValidate="ddlwing"
                                                        Display="NONE" ErrorMessage="Please Select Wing" ValidationGroup="Val1" InitialValue="--Select--"
                                                        Enabled="true"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:DropDownList ID="ddlwing" runat="server" TabIndex="10" CssClass="clsComboBox"
                                                        AutoPostBack="true" Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">SubLocation<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvroom" runat="server" ControlToValidate="ddlSpace"
                                                        Display="NONE" ErrorMessage="Please Select Space" ValidationGroup="Val1" InitialValue="--Select--"
                                                        Enabled="true"></asp:RequiredFieldValidator></td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:DropDownList ID="ddlSpace" runat="server" TabIndex="14" CssClass="clsComboBox"
                                                        Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="tabcomm" runat="server">
                                    <td id="Td33" colspan="4" runat="server">
                                        <table id="common" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                            <tr id="Tr13" runat="server">
                                                <td id="Td34" align="left" style="height: 26px; width: 50%" runat="server">Service Request Category<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvreq" runat="server" ControlToValidate="ddlReq"
                                                        Display="NONE" ErrorMessage="Please Select Request Raised for" ValidationGroup="Val1"
                                                        InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                                <td id="Td35" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:DropDownList ID="ddlReq" runat="server" TabIndex="11" CssClass="clsComboBox"
                                                        Width="99%" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="Tr14" runat="server">
                                                <td id="Td36" align="left" style="height: 26px; width: 50%" runat="server">Service Request<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvreqtype" runat="server" ControlToValidate="ddlreqtype"
                                                        Display="NONE" ErrorMessage="Please Select Request Type" ValidationGroup="Val1"
                                                        InitialValue="--Select--" Enabled="true"></asp:RequiredFieldValidator></td>
                                                <td id="Td37" align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:DropDownList ID="ddlreqtype" AutoPostBack="true" runat="server" TabIndex="12"
                                                        CssClass="clsComboBox" Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td align="left" style="height: 26px; width: 50%" runat="server">Problem Description<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtProbDesc"
                                                        ValidationGroup="Val1" Display="NONE" ErrorMessage="Problem Description Required"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtProbDesc" runat="server" Width="97%" Rows="3" TabIndex="15" TextMode="MultiLine"
                                                        MaxLength="10000"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td align="left" style="height: 26px; width: 50%" runat="server">Assigned to
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:DropDownList ID="ddluser" runat="server" CssClass="clsComboBox" TabIndex="17"
                                                        Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td align="left" style="height: 26px; width: 50%" runat="server">Recent Comments
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtcomments" runat="server" Width="97%" Rows="3" TabIndex="18" TextMode="MultiLine"
                                                        MaxLength="10000"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">Convenient date when the personnel can visit to carry out the work
                                                    
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%; z-index: 900; position: inherit;">
                                                    <asp:TextBox ID="txtconvdate" runat="server" CssClass="clsTextField" Width="60%"
                                                        AutoPostBack="true"></asp:TextBox>

                                                  

                                                </td>
                                                </tr>
                                            <tr id="tr8" runat="server">
                                                <td align="left" style="height: 26px; width: 50%">Convenient time when the personnel can visit to carry out the work<%--<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="cfvHr" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select No of Hours!"
                                                        Display="None" ControlToValidate="cboHr" InitialValue="-HH--"></asp:RequiredFieldValidator>--%></td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:DropDownList ID="cboHr" runat="server" Width="48%" CssClass="clsComboBox">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td align="left" style="height: 26px; width: 50%">An email will be sent to EmailID.If you wish to receive SMS as well,Please Click
                                                    Yes<font class="clsNote">*</font>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:RadioButtonList AutoPostBack="True" ID="radlstactions" runat="server" CssClass="clsRadioButton"
                                                        RepeatDirection="horizontal">
                                                        <asp:ListItem Value="0">Yes</asp:ListItem>
                                                        <asp:ListItem Value="1">No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="Mobile">
                                                <td align="left" style="height: 26px; width: 50%" runat="server">Enter Mobile Number<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvSMSMobile" runat="server" ValidationGroup="Val1"
                                                        ControlToValidate="txtSMSMobile" Display="NONE" ErrorMessage="Enter Mobile Number to send SMS"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rgvmobile" runat="server" ValidationGroup="Val1"
                                                        ControlToValidate="txtSMSMobile" Display="None" ErrorMessage="Please Enter Mobile Number in Numerics Only"
                                                        ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtSMSMobile" runat="server" CssClass="clsTextField" MaxLength="10"
                                                        Width="97%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="Email" visible="false">
                                                <td align="left" style="height: 26px; width: 50%" runat="server">Enter Email ID</td>
                                                <td align="left" style="height: 26px; width: 50%" runat="server">
                                                    <asp:TextBox ID="txtSMSEmail" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnModify" runat="server" CssClass="button" Text="Modify" ValidationGroup="Val1" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <table id="t" runat="server" cellpadding="1" cellspacing="0">
                                <tr id="Tr16" runat="server">
                                    <td id="Td1" align="left" style="height: 26px; width: 50%" runat="server">Enter Comments <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvcomments1" runat="server" Display="None" ErrorMessage="Please Enter Comments"
                                            ControlToValidate="txtcomments1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td38" align="left" style="height: 26px; width: 50%" runat="server">
                                        <asp:TextBox ID="txtcomments1" runat="server" TextMode="MultiLine" Rows="3" MaxLength="750"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="Td39" align="left" style="height: 26px; width: 50%" runat="server">Select Request Status
                                    </td>
                                    <td id="Td40" align="left" style="height: 26px; width: 50%" runat="server">
                                        <asp:DropDownList ID="ddlstatus1" runat="server" CssClass="clsComboBox" TabIndex="20"
                                            Width="99%">
                                            <asp:ListItem Value="1">Pending</asp:ListItem>
                                            <asp:ListItem Value="2">In-Progress</asp:ListItem>
                                            <asp:ListItem Value="3">Closed</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="Tr17" runat="server">
                                    <td id="Td41" align="center" colspan="2" runat="server">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" />
                                    </td>
                                </tr>
                            </table>
                            <uc1:Solution ID="Solution1" runat="server" />
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                            <table id="Table1" runat="server" cellpadding="0" width="100%" cellspacing="0">
                            </table>
                            <table id="UsersGrid" cellpadding="0" cellspacing="0" width="100%">
                            </table>
                            <div id="UsersGridPager">
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </td>
                <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
