Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Imports SENDSMS
Partial Class EFM_EFM_Webfiles_RaiseRequest
    Inherits System.Web.UI.Page
    Dim lbl As Integer = 1
    Dim MaintenanceReq As String = ""
    'Dim objWebService As New localhost.AutoComplete
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack() Then
            Mobile.Visible = False
            Email.Visible = False
            personal.Visible = False
            BindType()
            BindCity()
            BindDetails()
            loc.Visible = False
            Twr.Visible = False
            flr.Visible = False
            wng.Visible = False
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MOBILE_DETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtSMSMobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
                txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            End If
            txtconvdate.Text = getoffsetdatetime(DateTime.Now).ToString("dd-MMM-yyyy")
        End If
        txtconvdate.Attributes.Add("onClick", "displayDatePicker('" + txtconvdate.ClientID + "')")
        txtconvdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    End Sub
    Private Sub BindType()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = 0
        ObjSubSonic.Binddropdown(ddlType, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
    End Sub
    Private Sub BindService_request_category(ByVal idparent As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddlReq, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
    End Sub
    Private Sub BindSpace(ByVal SPC_CTY_ID As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_CITY")
        sp.Command.AddParameter("@SPC_CTY_ID", SPC_CTY_ID, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@SPACE_TYPE", ddlType.SelectedItem.Text, DbType.String)
        ddlSpace.DataSource = sp.GetDataSet()
        ddlSpace.DataTextField = "SPC_VIEW_NAME"
        ddlSpace.DataValueField = "SPC_ID"
        ddlSpace.DataBind()
        ddlSpace.Items.Insert(0, New ListItem("--Select--", "0"))
        If ddlSpace.Items.Count = 2 Then
            ddlSpace.SelectedIndex = 1
        End If
    End Sub
    Function GetTodaysReqCount() As String
        Dim cnt As String
        cnt = 0
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TODAYS_REQUEST_ID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ds = sp.GetDataSet()
        cnt = CInt(ds.Tables(0).Rows(0).Item("cnt")) + 1
        Return cnt
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If txtconvdate.Text <> "" Then
                If CDate(txtconvdate.Text) < getoffsetdate(Date.Today) Then
                    lblMsg.Text = "Convenient Date Should be greater than or equal to  Todays Date"
                    Exit Sub
                End If
            End If
            lblMsg.Text = ""
            Dim Main As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
            MaintenanceReq = Main + "/" + GetTodaysReqCount().ToString()
            Session("reqid") = MaintenanceReq
            If radlstactions.Items("0").Selected = False Then
                RaiseRequest("", MaintenanceReq)

            Else
                RaiseRequest(txtSMSMobile.Text, MaintenanceReq)
            End If
            
            ClearAll()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks1.aspx?id=10&Reqid=" & MaintenanceReq)
    End Sub
  
    
    Private Sub RaiseRequest(ByVal mobile As String, ByVal MaintenanceReq As String)
        Dim count As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"RASIE_HELP_DESK_REQUEST1")
        sp.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
        sp.Command.AddParameter("@AUR_TYPE", "", DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@SER_TYPE", ddlType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@Name", "", DbType.String)
        sp.Command.AddParameter("@Email", "", DbType.String)
        sp.Command.AddParameter("@Contact", "", DbType.String)
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_RAISED_FOR", ddlReq.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_TYPE", ddlreqtype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_SPC_ID", ddlSpace.SelectedItem.Value, DbType.String)
         sp.Command.AddParameter("@DESCRIPTION", txtProbDesc.Text, DbType.String)
        sp.Command.AddParameter("@MOBILE", "", DbType.String)
        sp.Command.AddParameter("@RESPONSE_BY", radlstactions.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ", ddlType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SMS", txtSMSMobile.Text, DbType.String)
        sp.Command.AddParameter("@AUR_EMAIL", txtSMSEmail.Text, DbType.String)
        If txtconvdate.Text = "" Then
            sp.Command.AddParameter("@CONV_DATE", DBNull.Value, DbType.DateTime)
        Else
            sp.Command.AddParameter("@CONV_DATE", txtconvdate.Text, DbType.DateTime)
        End If
        sp.Command.AddParameter("@CONV_TIME", cboHr.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CONV_TIME1", cboHr.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@CONREMARKS", "NA", DbType.String)
        sp.ExecuteScalar()
        'Clearall()
        lblMsg.Text = "Request Raised Succesfully"
    End Sub
    Private Sub ClearAll()
        tabcomm.Visible = False
        tdtls.Visible = False
        Mobile.Visible = False
        Email.Visible = False
        btn.Visible = False
    End Sub   
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        If ddlCity.Items.Count = 2 Then
            ddlCity.SelectedIndex = 1
        End If
    End Sub
    Private Sub ClearSMSdetails()
        txtSMSMobile.Text = ""
        txtSMSEmail.Text = ""
    End Sub
    Private Sub BindDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_USER_DETAILS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtesname.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
            txtdept.Text = ds.Tables(0).Rows(0).Item("DEP")
            txtdesig.Text = ds.Tables(0).Rows(0).Item("DESIGNATION")
            txtescontact.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
        End If
    End Sub   
    Private Sub BindRequest()
        Dim count As Integer = 0
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_REQUEST_TYPE_COUNT")
        sp1.Command.AddParameter("@REQ_TYPE", ddlReq.SelectedItem.Value, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        If ds1.Tables(0).Rows.Count > 0 Then
            count = ds1.Tables(0).Rows(0).Item("COUNT")
        End If
        count = count + 1
        BindService_request_category(ddlreqtype, ddlReq.SelectedItem.Value)
       
    End Sub
    Private Sub BindService_request_category(ByRef ddl As DropDownList, ByVal idparent As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddl, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
    End Sub

   
    Protected Sub ddlReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReq.SelectedIndexChanged
        If ddlReq.SelectedIndex > 0 Then
            'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQ_TYPE_MAINTENANCE")
            'sp1.Command.AddParameter("@req_id", ddlReq.SelectedItem.Value, DbType.String)
            'Dim ds1 As New DataSet()
            'ds1 = sp1.GetDataSet()
            'If ds1.Tables(0).Rows.Count > 0 Then
            '    lbl = ds1.Tables(0).Rows(0).Item("TYPE")
            'End If
            'If ddlReq.SelectedIndex > 0 Then
            '    If txtconvdate.Text <> "" Then
            '        If CDate(txtconvdate.Text) = getoffsetdate(Date.Today) Then
            '            BindTimings(lbl)
            '        Else
            '            BindTimings1(lbl)
            '        End If
            '    Else
            '        BindTimings1(lbl)
            '    End If
            'End If
            BindRequest()
            If ddlreqtype.Items.Count = 2 Then
                ddlreqtype.SelectedIndex = 1
                BindTimings()
            Else
                ddlreqtype.SelectedIndex = 0
            End If

        End If
    End Sub
    Protected Sub radlstactions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radlstactions.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MOBILE_DETAILS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtSMSMobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        End If
        If radlstactions.Items("0").Selected = True Then
            ClearSMSdetails()
            txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            txtSMSMobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            Mobile.Visible = True
        ElseIf radlstactions.Items("0").Selected = False Then
            ClearSMSdetails()
            Mobile.Visible = False
            txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")

        End If
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex = 0 Then
            ddlSpace.Items.Clear()
        Else
            BindSpace(ddlCity.SelectedItem.Value)
        End If
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged

        If ddlType.SelectedItem.Text = "Others" Then
            loc.Visible = True
            Twr.Visible = True
            flr.Visible = True
            wng.Visible = True
            ddlSpace.Items.Clear()
            BindService_request_category(ddlReq, ddlType.SelectedItem.Value)
            Getdetails()
        Else
            loc.Visible = False
            Twr.Visible = False
            flr.Visible = False
            wng.Visible = False
           ' If ddlType.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
                BindSpace(ddlType.SelectedItem.Value)
                BindService_request_category(ddlReq, ddlType.SelectedItem.Value)
           ' End If
        End If
       
    End Sub
    Private Sub Getdetails()
        ddlloc.Items.Clear()
        ddltwr.Items.Clear()
        ddlfloor.Items.Clear()
        ddlwing.Items.Clear()
        ddlSpace.Items.Clear()
        BindCity()
        If ddlCity.Items.Count = 2 Then
            BindLocation()
            If ddlloc.Items.Count = 2 Then
                ddlloc.Items(1).Selected = True
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddlloc.SelectedValue, ddltwr.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        Else
            ddlloc.Items.Clear()
            ddltwr.Items.Clear()
            ddlfloor.Items.Clear()
            ddlwing.Items.Clear()
            ddlSpace.Items.Clear()
        End If
    End Sub
   
    Private Sub BindTimings(ByVal lbl As Integer)
        cboHr.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TIMINGS_2")
        sp.Command.AddParameter("@TIMING", lbl, DbType.Int32)
        cboHr.DataSource = sp.GetDataSet()
        cboHr.DataTextField = "TIMINGS"
        cboHr.DataValueField = "VALUE"
        cboHr.DataBind()
        cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))  
    End Sub
    Private Sub BindTimings1(ByVal lbl As Integer)
        cboHr.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TIMINGS_1")
        sp.Command.AddParameter("@TIMING", lbl, DbType.Int32)
        cboHr.DataSource = sp.GetDataSet()
        cboHr.DataTextField = "TIMINGS"
        cboHr.DataValueField = "VALUE"
        cboHr.DataBind()
        cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))
    End Sub

    Protected Sub ddlloc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlloc.SelectedIndexChanged
        If ddlType.SelectedItem.Text = "Others" Then
            If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 Then
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddlloc.SelectedValue, ddltwr.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        Else
            ddlSpace.Items.Clear()
            If ddlloc.SelectedIndex > 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_LOCATION")
                sp.Command.AddParameter("@LOCATION", ddlloc.SelectedItem.Text, DbType.String)
                ddlSpace.DataSource = sp.GetDataSet()
                ddlSpace.DataTextField = "SPC_VIEW_NAME"
                ddlSpace.DataValueField = "SPC_ID"
                ddlSpace.DataBind()
                ddlSpace.Items.Insert(0, New ListItem("--Select--", "--Select--"))
            End If
        End If
    End Sub

    Protected Sub ddltwr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltwr.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 Then
            BindFloor(ddlloc.SelectedValue, ddltwr.SelectedValue)
            If ddlfloor.Items.Count = 2 Then
                ddlfloor.SelectedIndex = 1
                BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
                If ddlwing.Items.Count = 2 Then
                    ddlwing.SelectedIndex = 1
                    BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                    If ddlSpace.Items.Count = 2 Then
                        ddlSpace.SelectedIndex = 1
                    Else
                        ddlSpace.SelectedIndex = 0
                    End If
                Else
                    ddlwing.SelectedIndex = 0
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlfloor.SelectedIndex = 0
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        End If
    End Sub

    Protected Sub ddlfloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlfloor.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 And ddlfloor.SelectedIndex > 0 Then
            BindWing(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue)
            If ddlwing.Items.Count = 2 Then
                ddlwing.SelectedIndex = 1
                BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
                If ddlSpace.Items.Count = 2 Then
                    ddlSpace.SelectedIndex = 1
                Else
                    ddlSpace.SelectedIndex = 0
                End If
            Else
                ddlwing.SelectedIndex = 0
                ddlSpace.Items.Clear()
            End If
        End If
    End Sub
    Private Sub BindRoom(ByVal loc As String, ByVal tow As String, ByVal flr As String, ByVal wing As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", tow, DbType.String)
        sp.Command.AddParameter("@FLOOR", flr, DbType.String)
        sp.Command.AddParameter("@WING", wing, DbType.String)
        sp.Command.AddParameter("@SPC_TYPE", "", DbType.String)
        ddlSpace.DataSource = sp.GetDataSet()
        ddlSpace.DataTextField = "SPC_VIEW_NAME"
        ddlSpace.DataValueField = "SPC_ID"
        ddlSpace.DataBind()
        ddlSpace.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub BindLocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        ddlloc.DataSource = sp.GetDataSet()
        ddlloc.DataTextField = "LCM_NAME"
        ddlloc.DataValueField = "LCM_CODE"
        ddlloc.DataBind()
        ddlloc.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindTower(ByVal loc As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TOWER_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        ddltwr.DataSource = sp.GetDataSet()
        ddltwr.DataTextField = "TWR_NAME"
        ddltwr.DataValueField = "TWR_CODE"
        ddltwr.DataBind()
        ddltwr.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindFloor(ByVal loc As String, ByVal tow As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_FLOOR_TOW_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", tow, DbType.String)
        ddlfloor.DataSource = sp.GetDataSet()
        ddlfloor.DataTextField = "FLR_NAME"
        ddlfloor.DataValueField = "FLR_CODE"
        ddlfloor.DataBind()
        ddlfloor.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindWing(ByVal loc As String, ByVal tow As String, ByVal floor As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_WING_FLR_TOW_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", tow, DbType.String)
        sp.Command.AddParameter("@FLOOR", floor, DbType.String)
        ddlwing.DataSource = sp.GetDataSet()
        ddlwing.DataTextField = "WNG_NAME"
        ddlwing.DataValueField = "WNG_CODE"
        ddlwing.DataBind()
        ddlwing.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub ddlwing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlwing.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 And ddlfloor.SelectedIndex > 0 And ddlwing.SelectedIndex > 0 Then
            BindRoom(ddlloc.SelectedValue, ddltwr.SelectedValue, ddlfloor.SelectedValue, ddlwing.SelectedValue)
            If ddlSpace.Items.Count = 2 Then
                ddlSpace.SelectedIndex = 1
            Else
                ddlSpace.SelectedIndex = 0
            End If
        End If
        If ddlReq.SelectedIndex > 0 And ddlwing.SelectedIndex > 0 Then
            BindRequest()
        Else
            ddlreqtype.Items.Clear()
        End If

    End Sub

    Protected Sub ddlreqtype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlreqtype.SelectedIndexChanged
         BindTimings()
    End Sub

  




    Private Sub BindTimings()
        If ddlreqtype.SelectedIndex > 0 And txtconvdate.Text <> "" Then
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CMS_TIMINGS")
            sp1.Command.AddParameter("@req_id", ddlreqtype.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@date", txtconvdate.Text, DbType.Date)
            cboHr.DataSource = sp1.GetDataSet()
            cboHr.DataTextField = "TIMINGS"
            cboHr.DataValueField = "VALUE"
            cboHr.DataBind()
            cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))

        End If
    End Sub

    Protected Sub btnreload_Click(sender As Object, e As EventArgs) Handles btnreload.Click
        BindTimings()
    End Sub

   
End Class
