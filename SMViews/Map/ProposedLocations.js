﻿app.service('ProposedLocationsService', function ($http, $q, UtilityService) {

    this.GetStatewiseLoc = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ProposedLocations/GetStatewiseLoc')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.GetMarkers = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ProposedLocations/GetMarkers')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});

app.controller('ProposedLocationsController', function ($scope, $q, ProposedLocationsService, UtilityService, $filter) {

    var map;
    var markers = [];
    $scope.Markers = [];
    $scope.Location = [];
    $scope.SearchSpace = {};
    $scope.SearchSpace.State = [];
    $scope.SearchSpace.City = [];
    $scope.SearchSpace.Location = [];
    $scope.SearchSpace.ToLocation = [];
    $scope.State = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.ToLocation = [];
    $scope.GetToLocation = [];
    $scope.OfficeType = [];
    $scope.OfficeType2 = [];
    $scope.GetLatLong = [];
    $scope.From_Latlong = [];
    $scope.To_Latlong = [];

    var URLparams = { URL: location.pathname }

    UtilityService.ValidatePagePath(URLparams).then(function (data) {
        if (data.data == 1) //0
        {
            window.location = "/maxlifeuat/login.aspx";
        }
    });

    ProposedLocationsService.GetStatewiseLoc().then(function (response) {
        if (response != undefined && response != null) {
            $scope.Citylst = response;
            $scope.totalsum = _.sumBy($scope.Citylst, 'LCMCNT');
            $scope.totalsum1 = _.sumBy($scope.Citylst, 'LCMCNT1');
        }

    });
    ProposedLocationsService.GetMarkers().then(function (response) {
        if (response != undefined && response != null) {
            angular.forEach(response, function (value, key) {
                addMarker(value);
            });
            $scope.GetLatLong.push(response);
        }
    });
    $scope.initMap = function () {
        var haightAshbury = { lat: 23.0885976, lng: 86.1615858 };
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: haightAshbury,
            mapTypeId: 'terrain'
        });
    }

    $scope.initMap();
    var infowindow = new google.maps.InfoWindow();

    // Adds a marker to the map and push to the array.
    function addMarker(data) {
        var marker = new google.maps.Marker({
            position: { lat: data.LAT, lng: data.LONG },
            map: map,
            title: data.LCM_NAME,
            icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'

        });
        if (data.LOCTYPE == "1")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');


        marker.ZN_CODE = data.ZN_CODE;
        marker.STE_CODE = data.STE_CODE;
        marker.LCM_CODE = data.LCM_CODE;
        marker.CTY_NAME = data.CTY_NAME;
        marker.LCM_NAME = data.LCM_NAME;
        marker.LOCTYPE = data.LOCTYPE;
        marker.TWR_CODE = data.TWR_CODE;
        marker.TWR_NAME = data.TWR_NAME;
        marker.CTY_CODE = data.CTY_CODE;
        var content = '<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1 > ' + data.LCM_NAME + ' (' + data.LCM_CODE + ') <BR/> <b>Address:</b>  ' + data.LCM_ADDR + '   </font>'

        marker.addListener('click', function () {
            if (infowindow)
                infowindow.close();
            infowindow.setContent(content);
            infowindow.open(map, marker);
        });
        $scope.Markers.push(marker);
    }
    var tempMarker;
    $scope.CityClick = function (state) {
        clearMarkers();
        var markers = $filter('filter')($scope.Markers, function (x)
        {
            
            if (x.CTY_CODE === state.CTY_CODE)
                return x;
        });
        tempMarker = markers;      
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    $scope.brnchClick = function (pcncode) {
        clearMarkers();
        var markers = [];
        if (tempMarker != null) {
            console.log(tempMarker);
            markers = $filter('filter')(tempMarker, function (x)
            {
                console.log(x);
                if (x.LOCTYPE == pcncode)
                    return x;
            });
        }
        else {
            console.log($scope.Markers);
            markers = $filter('filter')($scope.Markers, function (x) { if (x.LOCTYPE == pcncode) return x; });
        }
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    $scope.DistanceMarkers = function (marker) {
        marker[0].setMap(map);
    }
    function setMapOnAll(map) {

        for (var i = 0; i < $scope.Markers.length; i++) {
            $scope.Markers[i].setMap(map);
        }
    }
    function clearMarkers() {
        setMapOnAll(null);
    }

    $scope.showMarkers = function () {
        tempMarker = $scope.Markers;
        setMapOnAll(map);
    }

    function deleteMarkers() {
        clearMarkers();
        $scope.Markers = [];
    }

});
