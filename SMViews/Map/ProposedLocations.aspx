﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" data-ng-app="QuickFMS">
<head runat="server">
    <title></title>
    <style type="text/css">
        #map {
            height: 400px;
        }

        #floating-panel {
            position: absolute;
            bottom: 10px;
            left: 5%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        .list-group-item {
            height: 50px;
        }
    </style>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4fwDEUvCfBUYVNlVeoWqTjI5jxEwYo-Q"></script>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body data-ng-controller="ProposedLocationsController">
    <%-- <script type='text/javascript'>
        var map = new L.Map('Div1', { center: new L.LatLng(51.51, -0.11), zoom: 9 });
        var googleLayer = new L.Google('ROADMAP');
        map.addLayer(googleLayer);
    </script>--%>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Proposed Locations</legend>
                    </fieldset>
                    <div class="row">
                        <div class="col-md-12">                            
                            <div class="col-md-8">
                                <div id="map" style="height: 500px"></div>
                                <div id="results"></div>
                                <div class="pull-right">
                                    <strong>*Note</strong>: Click on the marker display for relevant details
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-6" ng-click="brnchClick('0')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/red-dot.png" />
                                    Maxlife
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-6" ng-click="brnchClick('1')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/green-dot.png" />
                                    Proposed Location
                                </a>
                            </div>
                            <div class="col-md-4">
                                <div class="list-group">
                                    <a href="" class="list-group-item list-group-item-primary">
                                        <div class="clearfix">
                                            <div class="pull-left"><strong>States</strong></div>
                                            <div class="pull-right" ng-click="showMarkers()"><span class="fa fa-refresh"></span></div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-2">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/red-dot.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-2">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/green-dot.png" height="16px" width="16px" />
                                            </div>
                                        </div>
                                    </a>
                                    <div style='overflow-y: auto; height: 380px;'>
                                        <a href="" class="list-group-item" ng-class="{'list-group-item-success' : $index%2==0, 'list-group-item-warning' : $index % 2!=0}"
                                            ng-click="CityClick(City)" name="Citylst" ng-repeat="City in Citylst">
                                            <div class="col-md-6">{{City.CTY_NAME}}</div>
                                            <div class="col-md-2"><span class="badge" ng-bind="City.LCMCNT" ng-init="0"></span></div>
                                            <div class="col-md-2"><span class="badge" ng-bind="City.LCMCNT1" ng-init="0"></span></div>
                                        </a>
                                    </div>
                                    <a href="" class="list-group-item list-group-item-danger">
                                        <div class="clearfix">
                                            <div class="pull-left" name="stelsttotal"><strong>Total </strong></div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-2"><span class="badge" ng-bind="totalsum" ng-init="0"></span></div>
                                            <div class="col-md-2"><span class="badge" ng-bind="totalsum1" ng-init="0"></span></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script src="../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
    </script>
    <script src="../Utility.js"></script>
    <script src="ProposedLocations.js"></script>
  
</body>
</html>
