﻿//agGrid.initialiseAgGridWithAngular1(angular);

//var app = angular.module('QuickFMS', ["agGrid"]);


app.service('MaploaderService', function ($http, $q, UtilityService) {

    this.bindMap = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.bindMarkers = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetMarkers', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.GetFlrIdbyLoc = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Space_mapAPI/GetFloorIDBBox?lcm_code=' + GetParameterValues('lcm_code') + '&twr_code=' + GetParameterValues('twr_code') + '&flr_code=' + GetParameterValues('flr_code'))
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetSpaceDetailsBySPCID = function (spcid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: $('#ddlfloors').val(), CatValue: spcid };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySPCID', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetSpaceDetailsByREQID = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsByREQID', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetSpaceDetailsBySUBITEM = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySUBITEM', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetSpaceDetailsBySHIFT = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySHIFT', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.release = function (reqdet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/ReleaseSelectedseat', reqdet)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    }

    this.getEmpDetails = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetEmpDetails', selCostCenters)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getAllocEmpDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetAllocEmpDetails', selDet)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };


    this.allocateSeats = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/AllocateSeats', selSpaces)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.spcAvailabilityByShift = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/SpcAvailabilityByShift', selSpaces)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getLegendsSummary = function (flrid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: flrid };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsSummary', dataobj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getAllocDetails = function (flrid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: flrid };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetAllocDetails', dataobj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getallFilterbyItem = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaploaderAPI/GetallFilterbyItem')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getallFilterbySubItem = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetallFilterbySubItem', dataobj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.GetTotalAreaDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetTotalAreaDetails', selDet)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.GetSeatingCapacity = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSeatingCapacity', selDet)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.Validate = function (ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/Validate?id=' + ID + ' ')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});

app.controller('MaploaderController', function ($scope, $q, MaploaderService, UtilityService, SpaceRequisitionService, $filter, blockUI) {
    $scope.SelLayers = [];
    $scope.Markers = [];
    $scope.marker = {};
    $scope.pageSize = '1000';
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.SpaceAlloc = {};
    $scope.SpaceAlloc.Verticals = [];
    $scope.Verticals = [];
    $scope.CostCenters = [];
    $scope.Shifts = [];
    $scope.EmpDetails = [];
    $scope.SpaceAlloc.CostCenters = [];
    $scope.selectedSpaces = [];
    $scope.Markerdata = [];
    $scope.Items = [];
    $scope.SubItems = [];
    $scope.seatingCapacity = [];
    //$scope.FilterVar = "EmpSearch";
    $scope.ddldisplay = false;
    $scope.MarkerLblLayer = [];
    $scope.MarkerMeetingLblLayer = [];
    $scope.SpcCountArea = [];
    $scope.FLRDET = {};
    $scope.Role = {}

    blockUI.start();
    $scope.EmpSearchItems = [{ CODE: 'AUR_ID', NAME: 'User ID' },
    { CODE: 'AUR_KNOWN_AS', NAME: 'Name' },
    { CODE: 'AUR_EMAIL', NAME: 'Email ID' }];

    var map = L.map('leafletMap');
    $scope.Markerdata = new L.FeatureGroup();
    $scope.MarkerLblLayer = new L.FeatureGroup();

    $scope.MarkerMeetingLblLayer = new L.FeatureGroup();
    map.addLayer($scope.Markerdata);
    var overlayMaps = {
        "Markers": $scope.Markerdata,
        "Labels": $scope.MarkerLblLayer
    };

    L.control.layers({}, overlayMaps).addTo(map);

    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var AllocateStyle = { fillColor: '#3498DB', opacity: 0.8, fillOpacity: 0.8 };
    var ReservedStyle = { fillColor: '#17202A', opacity: 0.8, fillOpacity: 0.8 };
    var OccupiedStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };


    //Initialize map

    //This is currently the only way to get access to the drawnItems feature group which
    //is required to implement the edit functionality

    /// Refresh - resets the markers button
    L.easyButton('fa fa-refresh', function () {
        progress(0, 'Loading...', true);
        $scope.selectedSpaces = [];
        $scope.SelectedType = "";
        $scope.SelectedID = "";
        $scope.LoadDet = [];
        $scope.ddldisplay = false;
        $scope.FilterVar = "";

        $scope.Markerdata.clearLayers();
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle);
                    break;
                case 1002: marker.setStyle(AllocateStyle);
                    break;
                case 1004: marker.setStyle(OccupiedStyle);
                    break;
                case 1003: marker.setStyle(OccupiedStyle);
                    break;
                default: marker.setStyle(VacantStyle);
                    break;
            }
            marker.bindLabel(marker.SPC_DESC, {
                noHide: false,
                direction: 'auto',
                zIndexOffset: 2000
            });//.setOpacity(1);
            $scope.Markerdata.addLayer(marker);
        });
        map.fitBounds($scope.bounds);
        $scope.$apply(function () {
            $scope.ddlItem = "costcenteralloc";
        });
        $scope.loadThemes();

    }, "Refresh").addTo(map);

    //// Filter button
    L.easyButton('fa fa-filter', function () {
        $scope.$apply(function () {
            $scope.FilterVar = "flrfilter";
            $('.slide_content').slideToggle();
        });
    }, "Select Map").addTo(map);

    ///// View Grid button
    L.easyButton('fa fa-list-ul', function () {
        $scope.$apply(function () {
            $scope.FilterVar = "spcList";
            $('.slide_content').slideToggle();
        });
    }, "Search Filter").addTo(map);

    /// Allocate button
    L.easyButton('fa fa-user', function () {
        $scope.validate = false;
        progress(0, 'Loading...', true);
        if ($scope.selectedSpaces.length != 0) {
            progress(0, '', false);
            $('#SeatAllocation').modal('show');
        }
        else {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select atleast one seat to allocate / release");
        }
    }, "Allocate / Release").addTo(map);

    L.easyButton('fa fa-search', function () {
        $scope.$apply(function () {
            $scope.FilterVar = "EmpSearch";
            $('.slide_content').hide();
        });
    }, "Search Employee").addTo(map);
    L.easyButton('fa fa-check-square-o', function () {        
       
        MaploaderService.Validate($scope.SearchSpace.Floor[0].FLR_CODE).then(function (response) {
            if (response.data=1) {
                showNotification('success', 8, 'bottom-right', 'Floor Layout Validated Successfully');
            }
        });

    }, "Validate").addTo(map);

    L.easyPrint({
        title: 'Print Map',
        elementsToHide: '.gitButton, .easy-button-button, .leaflet-control-zoom, .leaflet-control-layers-overlays, #accordion, #divsidebar1, #divsidebar2, #divsidebar3, #divsidebar4'
    }).addTo(map);


    $scope.columDefsAlloc = [
       { headerName: "Select All", field: "ticked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>", headerCellRenderer: headerCellRendererFunc, pinned: 'left' },
       { headerName: "Space ID", field: "SPC_NAME", width: 200, cellClass: "grid-align", pinned: 'left' },
       { headerName: "Space Type", field: "SPC_TYPE_NAME", width: 110, cellClass: "grid-align" },
       { headerName: "Space Sub Type", field: "SST_NAME", width: 110, cellClass: "grid-align" },
       { headerName: "Ver", width: 110, cellClass: "grid-align", field: "VER_NAME", cellRenderer: customEditor },
       { headerName: "Cost", width: 110, cellClass: "grid-align", field: "Cost_Center_Name", cellRenderer: customEditor },
       { headerName: "From Date", field: "FROM_DATE", width: 120, cellClass: 'grid-align', cellRenderer: createFromDatePicker },
       { headerName: "To Date", field: "TO_DATE", width: 120, cellClass: 'grid-align', cellRenderer: createToDatePicker },
       { headerName: "Shift Type", field: "SH_CODE", width: 110, cellClass: "grid-align", filter: 'set', template: "<select  class='form-control' data-ng-change='shiftChange(data)' ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in Shifts' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>" },
       { headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME", cellRenderer: customEditor },
      //{ headerName: "Designation", field: "AUR_DES_NAME", width: 110, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_DES_NAME}}</label>" },
       //{ headerName: "Release", width: 80, cellClass: "grid-align", filter: 'set', template: "<a class='btn btn-primary btn-xs custom-button-color' ng-show='data.STATUS != 1 && data.SSA_SRNREQ_ID != \"\" ' data-ng-click ='Release(data)'>Release</a>" },
    ];
    blockUI.stop();
    function customEditor(params) {
        var editing = false;

        var eCell = document.createElement('span');

        var eLabel = document.createTextNode('Please Select');
        if (params.value)
            eLabel.nodeValue = params.value;
        eCell.appendChild(eLabel);

        var eSelect = document.createElement("select");
        eSelect.setAttribute("width", 50);
        eCell.addEventListener('click', function () {
            if (!editing) {
                progress(0, 'Loading...', true);
                if (params.column.colId === "VER_NAME") {
                    angular.forEach($scope.Verticals, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.VER_CODE);
                        eOption.setAttribute("text", item.VER_NAME);
                        eOption.innerHTML = item.VER_NAME;
                        eSelect.appendChild(eOption);
                    });
                    progress(0, '', false);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    $scope.getCostcenterByVertical(params.data, eSelect);
                    console.log(eSelect);
                }
                else {
                    if (params.data.Cost_Center_Code != "")
                        $scope.cstChange(params.data, eSelect);
                }
                setTimeout(function () {
                    eSelect.value = params.value;
                    eCell.removeChild(eLabel);
                    eCell.appendChild(eSelect);
                    eSelect.focus();
                    editing = true;
                }, 400);
            }
        });

        eSelect.addEventListener('blur', function () {
            if (editing) {
                editing = false;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
            }
        });
        eSelect.addEventListener('change', function () {
            if (editing) {
                editing = false;

                var newValue = eSelect.options[eSelect.selectedIndex].text;
                eLabel.nodeValue = newValue;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
                if (params.column.colId === "VER_NAME") {
                    params.data.VERTICAL = eSelect.value;
                    params.data.VER_NAME = newValue;
                    $scope.getCostcenterByVertical(params.data, eSelect);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    params.data.Cost_Center_Code = eSelect.value;
                    params.data.Cost_Center_Name = newValue;
                    $scope.cstChange(params.data, eSelect);
                }
                else {
                    params.data.AUR_ID = eSelect.value;
                    params.data.AUR_NAME = newValue;
                    $scope.EmpChange(params.data);
                }
            }
        });
        return eCell;
    }

    UtilityService.getVerticals(3).then(function (response) {
        if (response.data != null) {
            $scope.Verticals = response.data;
        }
    });

    MaploaderService.getallFilterbyItem().then(function (response) {

        if (response != null) {
            $scope.Items = response;
        }
    });

    $scope.getCostcenterByVertical = function (data, eSelect) {
        $scope.CostCenters = [];
        data.STATUS = 1002;
        data.STACHECK = UtilityService.Modified;
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eSelect.appendChild(eOption);
        UtilityService.getCostcenterByVerticalcode({ VER_CODE: data.VERTICAL }, 2).then(function (response) {
            $scope.CostCenters = response.data;
            angular.forEach(response.data, function (item, key) {
                var eOption = document.createElement("option");
                eOption.setAttribute("value", item.Cost_Center_Code);
                eOption.setAttribute("text", item.Cost_Center_Name);
                eOption.innerHTML = item.Cost_Center_Name;
                console.log(eOption);
                eSelect.appendChild(eOption);
            });
            progress(0, '', false);

        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.EmpChange = function (data) {
        data.STATUS = 1004;
        data.STACHECK = UtilityService.Modified;

    }

    function removeOptions(selectbox) {
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    $scope.cstChange = function (data, eSelect) {
        data.STATUS = 1003;
        data.STACHECK = UtilityService.Modified;
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eOption.setAttribute("text", "Please select");
        eSelect.appendChild(eOption);

        var dataobj = {
            lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE,
            Item: data.VERTICAL, subitem: data.Cost_Center_Code
        };
        MaploaderService.getEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {
                if (response.data.length != 0) {
                    $scope.EmpDetails = response.data;
                    angular.forEach(response.data, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);
                        if (data.AUR_ID === item.AUR_ID)
                            eSelect.value = data.AUR_ID;
                        progress(0, '', false);
                    });
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Employee Found');
                }
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message + $scope.BsmDet.Child);
            }

        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.shiftChange = function (data) {
        if (data.STATUS != 1 && (data.SSA_SRNREQ_ID != "" || data.FROM_DATE != "")) {
            MaploaderService.spcAvailabilityByShift(data).then(function (response) {
                if (response.data == null) {
                    showNotification('error', 8, 'bottom-right', response.Message);
                }

            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.FilterSpaces = function () {
        progress(0, 'Loading...', true);
        $scope.loadThemes();
        updateSummaryCount();
        $scope.GetSeatingCapacity();
        $scope.GetSeatingArea();
        $scope.GetTotalAreaDetails();
        $scope.LoadMap($scope.SearchSpace.Floor[0].FLR_CODE);
        $('.slide_content').slideToggle();

    }

    function createFromDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.FROM_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        })
        return newDate;
    }

    function createToDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.TO_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        })
        return newDate;
    }

    $scope.gridSpaceAllocOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
    };

    $('#SeatAllocation').on('shown.bs.modal', function () {
        $scope.relfrom = {};
        $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
        $scope.gridSpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
        $scope.gridSpaceAllocOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
        //$scope.gridSpaceAllocOptions.api.refreshView();
        $scope.gridSpaceAllocOptions.api.refreshHeader();
    });

    $scope.Release = function (reltypecode, reltypename) {
        //progress(0, 'Loading...', true);
        $scope.relfrom = {};
        $scope.relfrom.Name = reltypename;
        $scope.relfrom.Value = reltypecode;
        var dataobj = { sad: $scope.selectedSpaces, reltype: reltypecode };
        MaploaderService.release(dataobj).then(function (response) {
            if (response.data != null) {
                angular.forEach($scope.selectedSpaces, function (value, key) {
                    if (value.ticked = true) {
                        var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                        switch ($scope.relfrom.Value) {
                            case 1004:
                                value.AUR_ID = "";
                                value.STACHECK = UtilityService.Deleted;
                                spcid.STATUS = 1003;
                                spcid.setStyle(OccupiedStyle);
                                spcid.bindLabel(spcid.SPC_NAME + " : Allocated to " + value.Cost_Center_Name, {
                                    noHide: false,
                                    direction: 'auto',
                                    zIndexOffset: 2000
                                });//.setOpacity(1);
                                spcid.SPC_DESC = spcid.SPC_NAME + " : Allocated to " + value.Cost_Center_Name;
                                spcid.STACHECK = UtilityService.Deleted;
                                showNotification('success', 8, 'bottom-right', response.Message);
                                break;
                            case 1003:
                                value.Cost_Center_Code = "";
                                value.SH_CODE = "";
                                value.AUR_ID = "";
                                value.STACHECK = UtilityService.Deleted;
                                spcid.STATUS = 1002;
                                spcid.setStyle(AllocateStyle);
                                spcid.bindLabel(spcid.SPC_NAME + " : Allocated to " + value.VER_NAME, {
                                    noHide: false,
                                    direction: 'auto',
                                    zIndexOffset: 2000
                                });//.setOpacity(1);
                                spcid.SPC_DESC = spcid.SPC_NAME + " : Allocated to " + value.VER_NAME;
                                spcid.STACHECK = UtilityService.Deleted;
                                spcid.STATUS = 1002;
                                break;
                            case 1002:
                                value.VERTICAL = "";
                                value.Cost_Center_Code = "";
                                value.SH_CODE = "";
                                value.AUR_ID = "";
                                value.FROM_DATE = "";
                                value.TO_DATE = "";
                                value.STATUS = 1;
                                value.SSA_SRNREQ_ID = "";
                                spcid.setStyle(VacantStyle);
                                spcid.STATUS = 1;
                                spcid.bindLabel(value.SPC_NAME + " : Vacant", {
                                    noHide: false,
                                    direction: 'auto',
                                    zIndexOffset: 2000
                                });//.setOpacity(1);
                                spcid.SSA_SRNREQ_ID = "";
                                spcid.VERTICAL = "";
                                spcid.Cost_Center_Code = "";
                                spcid.SH_CODE = "";
                                spcid.AUR_ID = "";
                                spcid.FROM_DATE = "";
                                spcid.TO_DATE = "";
                                spcid.STATUS = 1;
                                spcid.SSA_SRNREQ_ID = "";
                                spcid.SPC_DESC = value.SPC_NAME + " : Vacant";
                                spcid.STACHECK = UtilityService.Deleted;
                                break;

                        }
                    }
                });
                setTimeout(function () {
                    progress(0, '', false);
                    $('#SeatAllocation').modal('hide');
                    $scope.selectedSpaces = [];
                    updateSummaryCount();
                    $scope.gridSpaceAllocOptions.api.refreshView();
                    showNotification('success', 8, 'bottom-right', "Selected Spaces Released From " + $scope.relfrom.Name);
                }, 300);
            }
            else {
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (error) {
        });
    }

    $scope.AllocateSeats = function () {
        progress(0, 'Loading...', true);
        var count = 0;

        $scope.FinalSelectedSpaces = [];
        angular.forEach($scope.selectedSpaces, function (value, key) {
            if (value.ticked == true) {
                if (value.VERTICAL == undefined || value.VERTICAL == "") {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                    return;
                }
                else if (value.FROM_DATE == undefined) {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Please Select From Date');
                    return;
                }
                else if (value.TO_DATE == undefined) {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Please Select To Date');
                    return;
                }
                else if (value.SH_CODE == undefined || value.SH_CODE == '' || value.SH_NAME == '') {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Please Select Shift');
                    return;
                }
                else {
                    $scope.FinalSelectedSpaces.push(value);
                    count = count + 1;
                }
            }
        });
        if (count == 0 && flag) {
            return;
        }

        MaploaderService.allocateSeats($scope.FinalSelectedSpaces).then(function (response) {
            if (response.data != null) {
                angular.forEach(response.data, function (value, key) {
                    var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                    spcid.STATUS = value.STATUS;
                    spcid.VERTICAL = value.VERTICAL;
                    spcid.Cost_Center_Name = value.Cost_Center_Name;
                    spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                    spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                    var labelval = "";
                    switch (value.STATUS) {
                        case 1: spcid.setStyle(VacantStyle);
                            labelval = value.AUR_ID;
                            break;
                        case 1002: spcid.setStyle(AllocateStyle);
                            labelval = value.AUR_ID;
                            break;
                        case 1004: spcid.setStyle(OccupiedStyle);
                            labelval = value.AUR_ID;
                            break;
                        case 1003: spcid.setStyle(OccupiedStyle);
                            labelval = value.AUR_ID;
                            break;
                        default: spcid.setStyle(VacantStyle);
                            break;
                    }
                    spcid.bindLabel(labelval, {
                        noHide: false,
                        direction: 'auto',
                        zIndexOffset: 2000
                    });//.setOpacity(1);
                    spcid.SPC_DESC = labelval;

                });
                $('#SeatAllocation').modal('hide');
                $scope.selectedSpaces = [];
                updateSummaryCount();
                $scope.gridSpaceAllocOptions.api.refreshView();
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', 'Seleceted Spaces Allocated Successfully');
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        });
    }

    ///   Load location data
    UtilityService.getCountires(2).then(function (Countries) {
        progress(0, 'Loading...', true);
        if (Countries.data != null) {
            $scope.Country = Countries.data;
        }
        UtilityService.getZone(2).then(function (Znresponse) {
            if (Znresponse.data != null) {
                $scope.Zone = Znresponse.data;
            }

            UtilityService.getState(2).then(function (Stresponse) {
                if (Stresponse.data != null) {
                    $scope.State = Stresponse.data;
                }

                UtilityService.getCities(2).then(function (Ctresponse) {
                    if (Ctresponse.data != null) {
                        $scope.City = Ctresponse.data;
                    }
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;
                        }
                        UtilityService.getTowers(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Tower = response.data;
                            }
                            UtilityService.getFloors(2).then(function (response) {
                                debugger;
                                if (response.data != null) {
                                    $scope.Floor = response.data;
                                    angular.forEach($scope.Floor, function (Value, index) {

                                        if (Value.FLR_CODE == GetParameterValues('flr_code')) {
                                            Value.ticked = true;
                                            $scope.SearchSpace.Floor[0] = Value;

                                            var cny = _.find($scope.Country, { CNY_CODE: Value.CNY_CODE });
                                            cny.ticked = true;
                                            $scope.SearchSpace.Country[0] = cny;

                                            var zn = _.find($scope.Zone, { ZN_CODE: Value.ZN_CODE });
                                            zn.ticked = true;
                                            $scope.SearchSpace.Zone[0] = zn;

                                            var ste = _.find($scope.State, { STE_CODE: Value.STE_CODE });
                                            ste.ticked = true;
                                            $scope.SearchSpace.State[0] = ste;

                                            var cty = _.find($scope.City, { CTY_CODE: Value.CTY_CODE });
                                            cty.ticked = true;
                                            $scope.SearchSpace.City[0] = cty;

                                            var twr = _.find($scope.Tower, { TWR_CODE: Value.TWR_CODE });
                                            twr.ticked = true;
                                            $scope.SearchSpace.Tower[0] = twr;

                                            var lcm = _.find($scope.Location, { LCM_CODE: Value.LCM_CODE });
                                            lcm.ticked = true;
                                            $scope.SearchSpace.Location[0] = lcm;

                                        }
                                    });

                                    SpaceRequisitionService.getShifts($scope.SearchSpace.Location).then(function (response) {
                                        $scope.Shifts = response.data;
                                    }, function (error) {
                                        console.log(error);
                                    });

                                    MaploaderService.GetFlrIdbyLoc().then(function (response) {
                                        $scope.LoadMap(response[0].FLR_ID);
                                    }, function (error) {
                                    });
                                }
                            });

                        });

                    });
                });
            });
        });
    });

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });



    $scope.LoadMap = function (flrid) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });

        $scope.Markerdata.clearLayers();
        $scope.MarkerLblLayer.clearLayers();
        $scope.MarkerMeetingLblLayer.clearLayers();
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        map.addLayer($scope.Markerdata);

        MaploaderService.bindMap(flrid).then(function (response) {
            if (response.mapDetails != null) {

                angular.forEach(response.mapDetails, function (value, index) {
                    // do something

                    var wkt = new Wkt.Wkt();
                    wkt.read(value.Wkt);

                    var theLayer = wkt.toObject();
                    theLayer.dbId = value.ID;
                    theLayer.options.color = '#000000';
                    theLayer.options.weight = 1;
                    theLayer.options.seattype = value.SEATTYPE;
                    theLayer.options.spacetype = value.layer;
                    theLayer.options.seatstatus = value.STAID;
                    theLayer.options.spacesubtype = value.SPC_SUB_TYPE;
                    theLayer.options.spaceid = value.SPACE_ID;
                    theLayer.options.checked = false;
                    theLayer.setStyle({ fillColor: '#E8E8E8', opacity: 0.5, fillOpacity: 0.5 });
                    $scope.drawnItems.addLayer(theLayer);

                });
                console.log(response);
                $scope.bounds = [[response.BBOX[0].MinY, response.BBOX[0].MinX], [response.BBOX[0].MaxY, response.BBOX[0].MaxX]];
                $scope.FLRDET = response.BBOX[0];
                map.fitBounds($scope.bounds);
                $scope.LoadMarkers(response.FloorDetails);
                $scope.loadThemes();
                updateSummaryCount();
                $scope.GetTotalAreaDetails();






            }
        });
    }

    function updateSummaryCount() {
        MaploaderService.getLegendsSummary($scope.SearchSpace.Floor[0].FLR_CODE).then(function (response) {
            if (response != null) {
                $scope.SeatSummary = response;
            }
        }, function (error) {
        });

        MaploaderService.getAllocDetails($scope.SearchSpace.Floor[0].FLR_CODE).then(function (response) {
            if (response != null) {
                $scope.SeatDet = response.Table;
                console.log($scope.SeatDet);
            }
        }, function (error) {
        });
    }

    $scope.LoadMarkers = function (flrdata) {
        MaploaderService.bindMarkers(flrdata).then(function (response) {
            if (response != null) {
                angular.forEach(response, function (value, index) {
                    var style = {};
                    if (value.SPC_TYPE_CODE == 'WS') {

                        switch (value.STATUS) {
                            case 1: chairicon = VacantStyle;
                                break;
                            case 1001: chairicon = AllocateStyle;
                                break;
                            case 1002: chairicon = AllocateStyle;
                                break;
                            case 1004: chairicon = OccupiedStyle;
                                break;
                            case 1003: chairicon = OccupiedStyle;
                                break;
                            case 1006: chairicon = ReservedStyle;
                                break;
                            default: chairicon = VacantStyle;
                                break;
                        }

                        $scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID, spacetype: 'CHA' } });
                        $scope.marker.setStyle(chairicon);
                        //var marker = L.marker([value.x, value.y], { icon: chairicon, layer: value.LAYER, spcid: value.SPCID, checked: false, status: value.STATUS });
                        //$scope.marker = L.marker([value.x, value.y], { icon: chairicon });
                        $scope.marker.lat = value.x;
                        $scope.marker.lon = value.y;
                        $scope.marker.SPC_ID = value.SPC_ID;
                        $scope.marker.SPC_NAME = value.SPC_NAME;
                        $scope.marker.SPC_DESC = value.SPC_DESC;
                        $scope.marker.layer = value.SPC_TYPE_CODE;
                        $scope.marker.VERTICAL = value.VERTICAL;
                        $scope.marker.VER_NAME = value.VER_NAME;
                        $scope.marker.COSTCENTER = value.COSTCENTER;
                        $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                        $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                        $scope.marker.SST_CODE = value.SST_CODE;
                        $scope.marker.SST_NAME = value.SST_NAME;
                        $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                        $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                        $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                        $scope.marker.STACHECK = value.STACHECK;
                        $scope.marker.STATUS = value.STATUS;
                        $scope.marker.STA_DESC = value.STA_DESC;
                        $scope.marker.ticked = false;

                        $scope.marker.bindLabel(value.SPC_DESC, {
                            noHide: false,
                            direction: 'auto',
                            zIndexOffset: 2000
                        });//.setOpacity(1);

                        if (value.STATUS != 1006)
                            $scope.marker.on('click', markerclicked);

                        //var SpaceIdbyName = value.SPC_NAME.split('-');
                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 20]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });


                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        $scope.Markers.push($scope.marker);
                        $scope.Markerdata.addLayer($scope.marker);


                    }
                    else {

                        var SpaceIdbyName = value.SPC_NAME.split('-');
                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_NAME,
                                iconAnchor: [16, 16]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel.bindLabel(value.SPC_DESC, {
                            noHide: false,
                            direction: 'auto',
                            zIndexOffset: 2000
                        });//.setOpacity(1);
                        $scope.MarkerMeetingLblLayer.addLayer($scope.MarkerLabel);

                    }

                });
                //angular.forEach($scope.Markers, function (marker, key) {
                //    $scope.Markerdata.addLayer(marker);
                //    //marker.addTo(map);
                //});
                $scope.gridOptions.api.setRowData($scope.Markers);
                progress(0, '', false);
                //$scope.gridOptions.api.sizeColumnsToFit();
            }
            else {
                showNotification('error', 8, 'bottom-right', 'No records Found');
            }
        }, function (error) {
        });
    }

    UtilityService.GetRoleByUserId().then(function (response) {
        if (response.data != null) {
            $scope.Role = response.data;
        }
    });
    function markerclicked(e) {
        if (!this.ticked) {
            if ($scope.Role.Rol_id == 34) {
                this.ticked = false;
            }
            else {
                this.setStyle(selctdChrStyle)
                this.ticked = true;
                var dataobj = { flr_code: GetParameterValues('flr_code'), category: this.SPC_ID, subitem: this.SSA_SRNREQ_ID ? this.SSA_SRNREQ_ID : "" };
                MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {
                    if (response != null) {
                        angular.forEach(response, function (value, index) {
                            $scope.selectedSpaces.push(value);
                        })
                    }
                }, function (error) {
                });
            }
        }
        else {

            switch (this.STATUS) {
                case 1: chairicon = VacantStyle;
                    break;
                case 1002: chairicon = AllocateStyle;
                    break;
                case 1004: chairicon = OccupiedStyle;
                    break;
                case 1003: chairicon = OccupiedStyle;
                    break;
                default: chairicon = VacantStyle;
                    break;
            }
            _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SPC_ID: this.SPC_ID }));

            this.setStyle(chairicon)
            this.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

    /****************  Grid Display ***************/

    var columnDefs = [
       { headerName: "Select", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkedChanged(data)' />", cellClass: 'grid-align' },
       //{ headerName: "Space ID", field: "SPC_ID", cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" },
       { headerName: "Space ID", field: "SPC_NAME", cellClass: 'grid-align', enableSorting: true },
       { headerName: "Business Unit", field: "VER_NAME", enableSorting: true },
       { headerName: "Function", field: "COSTCENTER" },
       { headerName: "Space Type", field: "SPC_TYPE_NAME", enableSorting: true },
       { headerName: "Space Sub Type", field: "SST_NAME", enableSorting: true },
       { headerName: "Status", field: "STA_DESC", enableSorting: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        showToolPanel: true,
        rowAggregatePanelShow: 'none',
        enableColResize: true,
        enableCellSelection: false,
        suppressRowClickSelection: true,
        onready: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode('Select All');
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = true;
                        value.setStyle(selctdChrStyle)
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = false;
                        value.setStyle(VacantStyle)
                        switch (value.STATUS) {
                            case 1: value.setStyle(VacantStyle);
                                break;
                            case 1002: value.setStyle(AllocateStyle);
                                break;
                            case 1004: value.setStyle(OccupiedStyle);
                                break;
                            case 1003: value.setStyle(OccupiedStyle);
                                break;
                            default: value.setStyle(VacantStyle);
                                break;
                        }

                    });
                });
            }
        });
        return eHeader;
    }

    /****************  Filter ***************/

    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    $("#txtCountFilter").change(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keydown(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keyup(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).bind('paste', function () {
        onReq_SelSpacesFilterChanged($(this).val());
    });

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    function onReq_SelSpacesFilterChanged(value) {
        $scope.gridSpaceAllocOptions.api.setQuickFilter(value);
    }


    /****************  Check change event ***************/
    $scope.chkedChanged = function (data) {
        if (data.ticked) {
            if ($scope.Role.Rol_id == 34) {
                data.ticked = false;
            }
            else {
                data.setStyle(selctdChrStyle)
                data.ticked = true;
                var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, category: data.SPC_ID, subitem: data.SSA_SRNREQ_ID ? data.SSA_SRNREQ_ID : "" };
                MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {
                    if (response != null) {
                        angular.forEach(response, function (value, index) {
                            $scope.selectedSpaces.push(value);
                        })
                    }
                }, function (error) {
                });
               
            }
        }
        else {
            switch (data.STATUS) {
                case 1: chairicon = VacantStyle;
                    break;
                case 1002: chairicon = AllocateStyle;
                    break;
                case 1004: chairicon = OccupiedStyle;
                    break;
                case 1003: chairicon = OccupiedStyle;
                    break;
                default: chairicon = VacantStyle;
                    break;
            }
            _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SPC_ID: data.SPC_ID }));

            data.setStyle(chairicon)
            data.ticked = false;
        }
    }

    /****************  Filters ***************/

    $scope.CnyChangeAll = function () {
        $scope.SearchSpace.Country = $scope.Country;
        $scope.CnyChanged();
    }
    $scope.CnySelectNone = function () {
        $scope.SearchSpace.Country = [];
        $scope.CnyChanged();
    }
    $scope.CnyChanged = function () {
        if ($scope.SearchSpace.Country.length != 0) {
            UtilityService.getZoneByCny($scope.SearchSpace.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.Zone = response.data;
                else
                    $scope.Zone = [];
            });
        }
        else
            $scope.City = [];
    }

    //zone
    $scope.ZoneChanged = function () {
        UtilityService.getStateByZone($scope.SearchSpace.Zone, 1).then(function (response) {
            if (response.data != null)
                $scope.State = response.data;
            else
                $scope.State = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;

                $scope.SearchSpace.Country.push(cny);
            }
        });
    }
    $scope.ZoneChangeAll = function () {
        $scope.SearchSpace.Zone = $scope.Zone;
        $scope.ZoneChanged();
    }
    $scope.ZoneSelectNone = function () {
        $scope.SearchSpace.Zone = [];
        $scope.ZoneChanged();
    }

    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.SearchSpace.State, 1).then(function (response) {
            if (response.data != null)
                $scope.City = response.data;
            else
                $scope.City = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.State, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.State, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.SearchSpace.State = $scope.State;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.SearchSpace.State = [];
        $scope.SteChanged();
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SearchSpace.City = $scope.City;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SearchSpace.City = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SearchSpace.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.SearchSpace.State.push(ste);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SearchSpace.Location = $scope.Location;
        $scope.LcmChanged();
    }
    $scope.LcmSelectNone = function () {
        $scope.SearchSpace.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SearchSpace.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.SearchSpace.State.push(ste);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SearchSpace.Tower = $scope.Tower;
        $scope.TwrChanged();
    }
    $scope.TwrSelectNone = function () {
        $scope.SearchSpace.Tower = [];
        $scope.TwrChanged();
    }
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SearchSpace.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.SearchSpace.State.push(ste);
            }
        });


    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SearchSpace.Floor = $scope.Floor;
        $scope.FlrChanged();
    }
    $scope.FlrSelectNone = function () {
        $scope.SearchSpace.Floor = [];
        $scope.FlrChanged();
    }
    $scope.FlrChanged = function () {
        progress(0, 'Loading...', true);
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SearchSpace.Tower.push(twr);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.SearchSpace.State.push(ste);
            }
        });
        progress(0, 'Loading...', false);
    }


    $scope.loadThemes = function () {

        $scope.SubItems = [];
        $scope.ddlSubItem = "";
        var item = {};
        $scope.ddlselval = _.find($scope.Items, { CODE: $scope.ddlItem });

        if ($scope.ddlItem) {
            if ($scope.ddlselval.CHKDDL == 1) {

                var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: $scope.ddlItem, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
                MaploaderService.getallFilterbySubItem(dataobj).then(function (response) {
                    $scope.ddldisplay = true;
                    $scope.SubItems = response;
                    progress(0, '', false);
                }, function (error) {
                    console.log(error);
                });
                progress(0, '', false);
            }
            else {

                $scope.ddldisplay = false;
                setTimeout(function () { $scope.GetLegendCount() }, 200);
            }
        }
    }

    $scope.loadSubDetails = function () {
        progress(0, 'Loading...', true);
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: $scope.ddlItem, subitem: $scope.ddlSubItem, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetSpaceDetailsBySUBITEM(dataobj).then(function (response) {
            if (response != null) {

                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find(response.Table, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                    else {
                        if (layer.options.spacetype == 'CHA') {
                            switch (spcid.STATUS) {
                                case 1: chairicon = VacantStyle;
                                    break;
                                case 1002: chairicon = AllocateStyle;
                                    break;
                                case 1004: chairicon = OccupiedStyle;
                                    break;
                                case 1003: chairicon = OccupiedStyle;
                                    break;

                                default: chairicon = VacantStyle;
                                    break;
                            }
                            layer.setStyle(chairicon);
                        }
                        else {

                            var spcclrobj = _.find($scope.SubItemColordet, { SPC_ID: spcid.SPC_ID });
                            if (spcclrobj != undefined)
                                layer.setStyle({ fillColor: spcclrobj.COLOR, opacity: 0.65 });
                        }

                    }
                });
            }
            progress(0, '', false);
        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }
    //work area details
    $scope.GetLegendCount = function () {
        $scope.SpcCount = [];
        var themeobj = { Item: $scope.ddlItem, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE };
        $.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsCount', themeobj, function (result) {
            if ($scope.ddlItem=='Vacant')
            {
                var vacspaces = [];
                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    if (marker.STATUS == 1)
                    {
                        marker.setStyle(VacantStyle);
                        vacspaces.push(marker);
                    }            
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find(vacspaces, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                    else {
                        layer.setStyle({ fillColor: "#228B22", opacity: 0.65 });
                    }
                });

            }
            else if ($scope.ddlItem != "verticalalloc" && $scope.ddlItem != "costcenteralloc") {
                if (result.Table) {
                    $scope.$apply(function () {
                        $scope.SpcCount = result.Table;
                        if ($scope.SpcCountArea.length == 0)
                            $scope.SpcCountArea = result.Table;
                        console.log($scope.SpcCount);
                    });

                    for (var i = 0; i < result.Table.length; i++) {
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options[$scope.ddlItem] == result.Table[i].CODE && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: result.Table[i].COLOR, opacity: 0.65 });
                            }
                        });
                    }

                    progress(0, '', false);
                }
            }
            else {
                $scope.SubItemColordet = result.Table;
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.SubItemColordet, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;

                    switch (marker.STATUS) {
                        case 1: marker.setStyle(VacantStyle);
                            break;
                        case 1002: marker.setStyle(AllocateStyle);
                            break;
                        case 1004: marker.setStyle(OccupiedStyle);
                            break;
                        case 1003: marker.setStyle(OccupiedStyle);
                            break;
                        default: marker.setStyle(VacantStyle);
                            break;
                    }
                });

                angular.forEach($scope.SubItemColordet, function (Value, key) {
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        if (layer.options.spaceid == Value.SPC_ID && layer.options.spacetype == Value.SPC_LAYER && layer.options.spacetype != "CHA") {
                            layer.setStyle({ fillColor: Value.COLOR, opacity: 0.65 });
                            return;
                        }
                    });
                });
                $scope.$apply(function () {
                    $scope.SpcCount = result.Table1;
                    $scope.ddldisplay = true;

                    if ($scope.SpcCount.length == 0) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Spaces are not allocated to any BU/Function');
                    }
                    $scope.SubItems = $scope.SpcCount;
                });
                //progress(0, '', false);
            }

        });
    }

    $('#divSeatStatus').on('shown.bs.collapse', function () {
        if ($scope.SeatSummary == undefined)
            updateSummaryCount();
    });

    $('#divSeatcap').on('shown.bs.collapse', function () {
        if ($scope.seatingCapacity.length == 0)
            $scope.GetSeatingCapacity();
    });

    $('#divWorkarea').on('shown.bs.collapse', function () {
        if ($scope.SpcCountArea.length == 0)
            $scope.GetSeatingArea();

    });

    $('#divotherarea').on('shown.bs.collapse', function () {
        if ($scope.Totalareadet == undefined)
            $scope.GetTotalAreaDetails()
    });

    $scope.SelectAllocEmp = function (SelectedEmp) {
        $scope.LoadDet = [];
        $scope.SelectedID = "";
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: SelectedEmp, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.getAllocEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {

                $scope.LoadDet = response.data;
            }
        });
    }
    //Total and other area details
    $scope.GetTotalAreaDetails = function () {
        $scope.Totalareadet = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetTotalAreaDetails(dataobj).then(function (result) {

            if (result.data) {
                $scope.Totalareadet = result.data;
                progress(0, '', false);
            }
        });
    }

    $scope.GetSeatingCapacity = function () {
        progress(0, 'Loading...', false);
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetSeatingCapacity(dataobj).then(function (result) {
            if (result.data) {
                $scope.seatingCapacity = result.data;
                progress(0, '', false);
            }
        });
    }
    //work area details
    $scope.GetSeatingArea = function () {
        progress(0, 'Loading...', false);
        $scope.seatingCapacity = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        var themeobj = { Item: 'spacetype', flr_code: $scope.SearchSpace.Floor[0].FLR_CODE };
        $.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsCount', themeobj, function (result) {
            $scope.$apply(function () {
                if ($scope.SpcCountArea.length == 0)
                    $scope.SpcCountArea = result.Table;
            });
        });
    }

    $scope.ShowEmponMap = function (SelectedID) {
        var marker = _.find($scope.Markers, { SPC_ID: SelectedID });
        setTimeout(function () { marker.setStyle(VacantStyle) }, 500);
        setTimeout(function () { marker.setStyle(AllocateStyle) }, 1000);
        setTimeout(function () { marker.setStyle(OccupiedStyle) }, 1500);
        setTimeout(function () { marker.setStyle(VacantStyle) }, 2000);
        if (map.getZoom() + 2 <= $scope.ZoomLvl)
            map.setView(L.latLng(marker.lat, marker.lon), map.getZoom() + 2);
        else
            map.panTo(L.latLng(marker.lat, marker.lon));

        setTimeout(function () {
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle)
                    break;
                case 1002: marker.setStyle(AllocateStyle)
                    break;
                case 1004: marker.setStyle(OccupiedStyle)
                    break;
                case 1003: marker.setStyle(OccupiedStyle)
                    break;
                default: marker.setStyle(VacantStyle)
                    break;
            }
        }, 3000)



    }

    var zoomtreshold = 0;
    map.once('moveend zoomend', function () {
        $scope.ZoomLvl = map.getZoom();
    }).on('zoomend', function () {
        zoomtreshold = 0;
        map.addLayer($scope.MarkerMeetingLblLayer);

        if ($scope.ZoomLvl >= 14)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else if ($scope.ZoomLvl >= 12)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 3;
        else if ($scope.ZoomLvl >= 9)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        setTimeout(function () {
            if (map.getZoom() < zoomtreshold) {
                if (map.hasLayer($scope.MarkerLblLayer)) {
                    map.removeLayer($scope.MarkerLblLayer);
                } else {
                    //console.log("no point layer active");
                }
            }

            if (map.getZoom() >= zoomtreshold) {
                if (map.hasLayer($scope.MarkerLblLayer)) {
                    //console.log("layer already added");
                } else {
                    map.addLayer($scope.MarkerLblLayer);
                }
            }
        }, 200);

    });
});


function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}



////// Layer Context Menu
//var contextMenuOptions = {
//    contextmenu: true,
//    contextmenuWidth: 140,
//    contextmenuItems: [{
//        text: 'Edit layer',
//        icon: '../../BootStrapCSS/leaflet/icons/Edit2.ico',
//        callback: editFeature
//    }, {
//        text: 'Delete Layer',
//        icon: '../../BootStrapCSS/leaflet/icons/Delete2.ico',
//        callback: deleteFeature
//    }, '-', {
//        text: 'Add Details',
//        icon: '../../BootStrapCSS/leaflet/icons/Edit1.ico'
//    },
//    {
//        text: 'Cancel Editing',
//        icon: '../../BootStrapCSS/leaflet/icons/close.ico',
//        callback: cancelFeature
//    }]
//};

//function editFeature(e) {
//    editableLayer.editing.enable();
//};
//function deleteFeature(e) {
//    leafletData.getMap('leafletMap').then(function (map) {
//        data = {
//            "Id": editableLayer.dbId
//        }
//        doSilentlyInBackground('DELETE', qfms.Maploader, data, '30', function (response) {
//            map.removeLayer(editableLayer);
//        });

//    });
//};
//function cancelFeature(e) {
//    editableLayer.editing.disable();
//};

//Function to retrieve a raw reference to the leaflet map.
//leafletData.getMap('leafletMap').then(function (map) {
//    map.addLayer(drawnItems);

//    map.on('draw:created', function (e) {
//        var layer = e.layer;
//        //Add shape to ModalMap

//        drawnItems.addLayer(layer);
//        layer.bindContextMenu(contextMenuOptions);
//        layer.on('contextmenu', function (e, position, latlon, data) {
//            editableLayer = e.target;
//        });
//        //Save WKT to the dictionary
//        var wkt = new Wkt.Wkt();
//        wkt.fromJson(layer.toGeoJSON());

//        data = {
//            "Wkt": wkt.write(),
//            "flr_id": $scope.flr_id
//        }

//        doSilentlyInBackground('POST', qfms.Maploader, data, '30', function (response) {
//            $scope.$apply(function () {
//                e.layer.dbId = response.ID;
//                showNotification('error', 8, 'bottom-right', "Please add Marker to the current created Layer");
//            });
//        });

//        //$http.post(url, data).then(function (result) {
//        //    e.layer.dbId = result.data.Id;
//        //});
//    });

//    map.on('draw:edited', function (e) {
//        var layers = e.layers;
//        layers.eachLayer(function (layer) {
//            var idToEdit = layer.dbId;

//            //Get WKT
//            var wkt = new Wkt.Wkt();
//            wkt.fromJson(layer.toGeoJSON());

//            data = {
//                "Id": idToEdit,
//                "Wkt": wkt.write()
//            }

//            doSilentlyInBackground('PUT', qfms.Maploader, data, '30', function (response) {
//            });
//            //$http.put(url + "/" + idToEdit, data);
//        });
//    });

//    map.on('draw:deleted', function (e) {
//        var layers = e.layers;
//        layers.eachLayer(function (layer) {
//            var idToEdit = layer.dbId;
//            var idToDelete = layer.dbId;
//            //$http.delete(url + "/" + idToDelete);
//            data = {
//                "Id": idToDelete
//            }
//            doSilentlyInBackground('DELETE', qfms.Maploader, data, '30', function (response) {
//            });
//        });
//    });

//    map.on('mousemove', function (e) {
//        $scope.mouseUpEvnt = e;
//    });
//});


//////  Add Marker by drag and drop
//$(".drag").draggable({
//    helper: 'clone',
//    containment: 'map',
//    start: function (evt, ui) {
//        $('#box').fadeTo('fast', 0.6, function () { });
//    },
//    stop: function (evt, ui) {
//        $('#box').fadeTo('fast', 1.0, function () { });
//    }
//});

//$("#leafletMap").droppable({
//    drop: function (event, ui) {
//        setTimeout(function () {
//            leafletData.getMap('leafletMap').then(function (map) {
//                var marker = L.marker($scope.mouseUpEvnt.latlng, {
//                    icon: chairicon,
//                    draggable: true
//                });
//                marker.bindContextMenu(contextMenuMarkerOptions);
//                marker.on('contextmenu', function (e, position, latlon, data) {
//                    editMarker = e.target;
//                });
//                marker.addTo(map);
//            });
//        }, 100);

//    }
//});


/////// data load functions
//function GetLayerDet() {
//    if ($scope.SpaceLayers == undefined) {
//        doSilentlyInBackground('GET', qfms.Maploader + "GetLayerDet", null, '30', function (response) {
//            $scope.SpaceLayers = response;
//        });
//    }
//    if ($scope.EditFlrDetails != null) {
//        $scope.EditFlrDetails.LAYER = editMarker.options.layer;
//        $scope.EditFlrDetails.SPACE_ID = editMarker.options.spcid;
//    }
//}

//function searchClear() {
//    $scope.EditFlrDetails = {};
//    $("#Markermodal").modal('hide');
//}

//function AddMarkerDet() {
//    var dataobj = { SPACE_ID: $scope.EditFlrDetails.SPACE_ID, layer: $scope.EditFlrDetails.LAYER, x: editMarker.latlng.lat, y: editMarker.latlng.lng };
//    doSilentlyInBackground('GET', qfms.Maploader + "InsertMarkerDet", dataobj, '30', function (response) {
//        $scope.SpaceLayers = response;
//    });
//}