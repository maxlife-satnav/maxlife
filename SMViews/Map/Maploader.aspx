﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/main.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/leaflet/Print/easyPrint.css" rel="stylesheet" />
    <link href="../../Scripts/angular-block-ui.min.css" rel="stylesheet" />

    <style>
        .leaflet-container {
            background-color: white;
        }

        #main {
            width: 100%;
            height: 420px;
            position: relative;
            border: 1px solid black;
        }

        #leafletMap {
            width: 100%;
            height: 100%;
        }

        #button-wrapper {
            position: absolute;
            bottom: 10px;
            width: 100%;
        }
        
        .text-labels {
            font-size: 10px;
            font-weight: 100;
            padding-top: 14px;
            width: 20px !important;
            /* Use color, background, set margins for offset, etc */
        }
    </style>
</head>
<body class="amantra">
    <div class="content" data-ng-controller="MaploaderController">
        <div id="maptoggle">
            <fieldset>
                <table width="100%">
                    <tr>
                        <td width="35%">
                            <h5>
                                <label ng-bind="FLRDET.LAYOUTNAME"></label>
                            </h5>
                        </td>
                        <td width="18%">
                            <strong>Occupancy Update date</strong>
                            <label ng-bind="FLRDET.SPC_UPT_DT"></label>
                            <br />
                            <strong></strong>
                            <label></label>
                        </td>
                        <td width="20%">

                            <strong>Version control no : </strong>
                            <label ng-bind="FLRDET.FLR_VERSION"></label>
                            <br />
                            <strong>Version control modify date : </strong>
                            <label ng-bind="FLRDET.FLR_UPL_DT"></label>

                        </td>
                        <td style="width: 40%;">
                            <strong><span ng-bind="FLRDET.LCM_CODE"></span>: </strong><span ng-bind="FLRDET.LCM_ADDR"></span>
                        </td>
                    </tr>
                </table>

            </fieldset>
            <div class="col-md-9">
                <div id="main">
                    <div id="leafletMap"></div>
                    <!--<leaflet defaults="leafletDefaults" center="leafletCenter" bounds="leafletBounds" markers="leafletMarkers" controls="leafletDraw" id="leafletMap"></leaflet>-->
                </div>
            </div>

            <%--<div class="btn-group dropup">
                <button type="button" class="btn btn-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            </div>--%>
            <div class="col-md-3">
                <div class="panel panel-primary" id="divsidebar1">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Themes</a>

                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">

                        <div id="ThemeAccordian" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="clearfix">
                                    <label class="control-label row col-md-5">Items: </label>
                                    <select class="selectpicker-xs col-md-7 pull-right" name="ddlItem" ng-init="ddlItem = 'costcenteralloc'" ng-model="ddlItem" ng-change="loadThemes()">
                                        <option value="" ng-selected="">--Select--</option>
                                        <option ng-repeat="item in Items" value="{{item.CODE}}">{{item.NAME}}</option>
                                    </select>
                                </div>
                                <div data-ng-show="ddldisplay">
                                    <label class="control-label row col-md-5">Sub Items: </label>
                                    <select class="selectpicker-xs col-md-7 pull-right" name="ddlSubItem" ng-model="ddlSubItem" ng-change="loadSubDetails()">
                                        <option value="" ng-selected="">--Select--</option>
                                        <option ng-repeat="item in SubItems" value="{{item.CODE}}">{{item.NAME}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="box-body">
                                <ul class="list-group" id="ulThemes">

                                    <li class="list-group-item" ng-repeat="item in SpcCount">{{item.NAME}}
                                        <span class="label pull-right " style='background-color: {{item.COLOR}}' ng-bind="item.CNT" ng-init="0"></span>
                                    </li>

                                    <%--  <li class="list-group-item">
                                        <i class="fa fa-square green"></i>Dedicated
                                          <span class="label label-warning pull-right">0</span>
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fa fa-square yellow"></i>Shared
                                      <span class="label label-danger pull-right" for="lblshared">0</span>
                                    </li>--%>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary" id="divsidebar2">

                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatStatus">Seat Status</a>
                        </h4>
                    </div>
                    <div id="divSeatStatus" class="panel-collapse collapse ">
                        <ul class="list-group" id="ulSeatStatus">
                            <li class="list-group-item">Total Seats<span class="label label-primary pull-right" ng-bind="SeatSummary.TOTAL" ng-init="0"></span>
                            </li>
                            <li class="list-group-item">Vacant<span class="label label-success pull-right" ng-bind="SeatSummary.VACANT" ng-init="0"></span>
                            </li>
                            <%-- <li class="list-group-item">Occupied by <span for="parent" ng-bind="BsmDet.Parent"></span>
                                <span class="label label-info pull-right" ng-bind="SeatSummary.ALLOCATEDVER" ng-init="0"></span>
                            </li>--%>
                            <li class="list-group-item">Occupied by <span for="parent" ng-bind="BsmDet.Child"></span>/ Employee
                                                                    <span class="label label-danger pull-right" ng-bind="SeatSummary.OCCUPIED + SeatSummary.ALLOCATEDCST" ng-init="0"></span>
                            </li>
                            <li class="list-group-item">Overloaded Seats
                                                                    <span class="label label-default pull-right" ng-bind="SeatSummary.OVERLOAD" ng-init="0"></span>
                            </li>
                            <li class="list-group-item">Selected Seats
                                                                    <span class="label selectedseat pull-right">{{selectedSpaces.length}}</span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-primary" id="divsidebar3">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatcap">Seating Capacity Details</a>

                        </h4>
                    </div>

                    <div id="divSeatcap" class="panel-collapse collapse">
                        <ul class="list-group" id="ulCapacity">
                            <li class="list-group-item" ng-repeat="item in seatingCapacity">{{item.SPC_NAME}}
                                      <span class="label pull-right" style='background-color: {{item.COLOR}}' ng-bind="item.CAPACITY" ng-init="0"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            <%--</div>
              <div >
                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
            </div>
            <div class="col-md-3">--%>

                <div class="panel panel-primary" id="divsidebar4">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#divWorkarea">Built Up Area details (Sq.ft)</a>

                        </h4>
                    </div>

                    <div id="divWorkarea" class="panel-collapse collapse">
                        <ul class="list-group" id="ulArea">
                            <li class="list-group-item" ng-repeat="item in SpcCountArea">{{item.NAME}}
                                <span class="label pull-right" style='background-color: {{item.COLOR}}' ng-bind="item.SUM" ng-init="0"></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#divotherarea">Office Area details (Sq.ft)</a>
                        </h4>
                    </div>
                    <div id="divotherarea" class="panel-collapse collapse in">
                        <ul class="list-group" id="ulC">
                            <li class="list-group-item" ng-repeat="item in Totalareadet">{{item.AREA}}
                                <span class="label pull-right label-primary" ng-bind="item.SFT" ng-init="0"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="slide_block">
                <div class=" slide_head clearfix">
                    <div class="pull-left Icons_right" ng-show="FilterVar == 'spcList'">
                        <input id="filtertxt" placeholder="Filter..." type="text" class="form-control input-xs" />
                    </div>
                    <div class="pull-left" ng-show="FilterVar == 'EmpSearch'">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Search By<span style="color: red;">*</span></label>
                                <select name="SelectedType" data-ng-model="SelectedType" class="form-control" id="ddlvert" required="" data-ng-change="SelectAllocEmp(SelectedType)">
                                    <option value="" selected>--Select--</option>
                                    <option ng-repeat="emp in EmpSearchItems" value="{{emp.CODE}}">{{emp.NAME}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label class="control-label">Value<span style="color: red;">*</span></label>
                            <select name="SelectedID" data-ng-model="SelectedID" class="form-control" id="Select1" required="" ng-change="ShowEmponMap(SelectedID)">
                                <option value="" selected>--Select--</option>
                                <option ng-repeat="det in LoadDet" value="{{det.CODE}}">{{det.NAME}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="pull-right Icons_right">
                        <%--<div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" />
                            <span class="input-group-addon">
                                <span class="fa fa-search" onclick="setup('fromdate')"></span>
                            </span>
                        </div>--%>
                    </div>
                </div>
                <div class="slide_content">
                    <div class="col-md-12" ng-show="FilterVar == 'spcList'">
                        <div class="form-group">
                            <div style="height: 230px;">
                                <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" ng-show="FilterVar == 'flrfilter'">
                        <form id="Form1" name="frmSearchspc" data-valid-submit="FilterSpaces()" novalidate>
                            <div class="col-md-12">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SearchSpace.Country[0].CNY_NAME" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid">Please select country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.ZN_NAME.$invalid}">
                                            <label class="control-label">Zone <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Zone" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Zone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME"
                                                data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SearchSpace.Zone" name="ZN_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.ZN_NAME.$invalid">Please select Zone </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.STE_NAME.$invalid}">
                                            <label class="control-label">State <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="State" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.State" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME"
                                                data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SearchSpace.State" name="STE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.STE_NAME.$invalid">Please select state </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid}">
                                            <label class="control-label">City <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="City" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SearchSpace.City[0].CTY_NAME" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid">Please select city </span>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Location" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SearchSpace.Location[0].LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid">Please select location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Tower" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SearchSpace.Tower[0].TWR_NAME" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select tower </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floor" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SearchSpace.Floor[0].FLR_NAME" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid">Please select floor </span>
                                        </div>
                                    </div>
                                    <div class="">
                                        <input type="submit" class="btn btn-primary" value="Apply Filter" />
                                        <button type="button" class="btn btn-default" onclick="$('.slide_content').slideToggle();">Close</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12"></div>
                </div>
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="SeatAllocation" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="H1">Seat Allocation</h4>
                            </div>
                            <form id="Form2" name="frmSpaceAlloc">
                                <div class="modal-body">
                                    <div class="row">
                                        <div style="height: 280px" ng-show="selectedSpaces.length != 0">
                                            <input id="txtCountFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                            <div data-ag-grid="gridSpaceAllocOptions" style="height: 100%;" class="ag-blue"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="button" ng-click="AllocateSeats()" class="btn btn-primary" value="Allocate Seats" />
                                    <div class="btn-group dropup">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Release From {{relfrom.Name}}  <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a ng-click="Release(1004,'Employee')">Employee</a></li>
                                            <li><a ng-click="Release(1003,BsmDet.Child)">{{BsmDet.Child}}</a></li>
                                            <li><a ng-click="Release(1002,BsmDet.Parent)">{{BsmDet.Parent}}</a></li>
                                            <%--<li role="separator" class="divider"></li>
                                            <li><a href="#" ng-click="relfrom = 'Complete Release'; relfrom = 1001">Complete Release</a></li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <table class="table table-condensed table-bordered table-hover table-striped table-condensed">
                    <tr style="background-color: #366599; color: #E2E2E2">
                        <th>Business Unit</th>
                        <th>Function</th>
                        <th># of Total Seats</th>
                        <th># of Occupied by Employee</th>
                        <th># of Vacant</th>
                    </tr>
                    <tr data-ng-repeat="row in SeatDet">
                        <td>{{row.BU}}</td>
                        <td>{{row.FUNCTION}}</td>
                        <td>{{row.ALLOCATED + row.OCCUPIED}}</td>
                        <td>{{row.OCCUPIED}}</td>
                        <td>{{row.ALLOCATED}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script src="../../BootStrapCSS/leaflet/addons/easy-button.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/Print/leaflet.easyPrint.js"></script>
    <script src="../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
    </script>
    <script src="../Utility.js"></script>

    <%--<script src="../../BootStrapCSS/Scripts/AllocateSeatToEmployeeVertical.js"></script>--%>
    <script src="maploader.js"></script>
    <script src="../DeptAlloc/Js/SpaceRequisition.js"></script>
    <script>
        $("img.userIcon").load(function () {
            if ($(this).height() > 100) {
                $(this).addClass("bigImg");
            }
        });

        function slideTogglefn() {
            $('#slide_trigger').click(function () {
                $(this).toggleClass("glyphicon-chevron-down");
                $('.slide_content').slideToggle();
            });
        }
        $(document).ready(function () {
            $('#toggleSlide').click(function () {
                $('#slide_trigger').click();
            });
            slideTogglefn();
        });
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
</body>
</html>

