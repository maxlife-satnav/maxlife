﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" data-ng-app="QuickFMS">
<head runat="server">
    <title></title>
    <style type="text/css">
        #map {
            height: 400px;
        }

        #floating-panel {
            position: absolute;
            bottom: 10px;
            left: 5%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        .list-group-item {
            height: 50px;
        }
    </style>
    <%--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4fwDEUvCfBUYVNlVeoWqTjI5jxEwYo-Q"></script>--%>
    <%--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2VFxFciUKK8WFncWXE5ofNElExRdSepg"></script>--%>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0v33sip72ssNJVf5LsHxr37LDCcPQxmI"></script>
    
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body data-ng-controller="MapMarkerController">
    <%-- <script type='text/javascript'>
        var map = new L.Map('Div1', { center: new L.LatLng(51.51, -0.11), zoom: 9 });
        var googleLayer = new L.Google('ROADMAP');
        map.addLayer(googleLayer);
    </script>--%>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View Locations</legend>
                    </fieldset>
                    <div class="row">
                        <div class="col-md-12">
                            <%-- <div id="floating-panel">
                                <div class="form-group">
                                    <label>Filter By Location</label>
                                    
                                    <input autocomplete ng-model="selected" />
                                </div>
                            </div>--%>
                            <div class="col-md-12">
                                <div id="map" style="height: 500px"></div>
                                <div id="results"></div>
                                <div class="pull-right">
                                    <strong>*Note</strong>: Click on the marker display for relevant details
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-4" ng-click="brnchClick('0')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/red-dot.png" />
                                    Maxlife
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('3')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/yellow-dot.png" />
                                    ICICI
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('4')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/ltblue-dot.png" />
                                    SBI
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('1')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/green-dot.png" />
                                    HDFC
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('2')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/blue-dot.png" />
                                    IDBI
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-4" ng-click="brnchClick('9')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/yellow.png" />
                                    Tata AIA
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('8')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/blue.png" />
                                    LIC
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('6')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/orange.png" />
                                    Birla
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('7')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/purple.png" />
                                    Kotak
                                </a>
                                <a href="" class="list-group-item list-group-item-danger pull-left col-md-2" ng-click="brnchClick('5')">
                                    <img src="http://maps.google.com/mapfiles/ms/icons/pink.png" />
                                    Bajaj
                                </a>
                            </div>
                            <div class="col-md-12">
                                <div class="list-group">
                                    <a href="" class="list-group-item list-group-item-primary">
                                        <div class="clearfix">
                                            <div class="pull-left"><strong>States</strong></div>
                                            <div class="pull-right" ng-click="showMarkers()"><span class="fa fa-refresh"></span></div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/red-dot.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/yellow-dot.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/ltblue-dot.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/green-dot.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/blue-dot.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/yellow.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/blue.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/orange.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/purple.png" height="16px" width="16px" />
                                            </div>
                                            <div class="col-md-1">
                                                <img src="http://maps.google.com/mapfiles/ms/icons/pink.png" height="16px" width="16px" />
                                            </div>
                                        </div>
                                    </a>
                                    <div style='overflow-y: auto; height: 380px;'>
                                        <a href="" class="list-group-item" ng-class="{'list-group-item-success' : $index%2==0, 'list-group-item-warning' : $index % 2!=0}"
                                            ng-click="SteClick(state)" name="Statelst" ng-repeat="state in Statelst">
                                            <div class="col-md-2">{{state.STE_NAME}}</div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT3" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT4" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT1" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT2" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT9" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT8" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT6" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT7" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="state.LCMCNT5" ng-init="0"></span></div>
                                        </a>
                                    </div>
                                    <a href="" class="list-group-item list-group-item-danger">
                                        <div class="clearfix">
                                            <div class="pull-left" name="stelsttotal"><strong>Total </strong></div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum3" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum4" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum1" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum2" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum9" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum8" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum6" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum7" ng-init="0"></span></div>
                                            <div class="col-md-1"><span class="badge" ng-bind="totalsum5" ng-init="0"></span></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <form id="frmSearchspc" name="frmSearchspc" novalidate>
                            <div class="clearfix">
                                <div class="col-md-12">
                                    <div class="panel panel-primary" id="divsidebar1">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Distance Calculation</a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div id="ThemeAccordian" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="clearfix">
                                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.STE_NAME.$invalid}">
                                                                <label class="control-label">State <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="State" data-output-model="SearchSpace.State" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME"
                                                                    data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.State" name="STE_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.STE_NAME.$invalid">Please select state </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid}">
                                                                <label class="control-label">City <span style="color: red;">**</span></label>
                                                                <div isteven-multi-select data-input-model="City" data-output-model="SearchSpace.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.City" name="CTY_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid">Please select city </span>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.OT1N.$invalid}">
                                                                <label class="control-label">Office Type (From) <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="OfficeType" data-output-model="SearchSpace.OfficeType" data-button-label="icon OT1N" data-item-label="icon OT1N"
                                                                    data-on-item-click="OT1Changed()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.OfficeType" name="OT1N" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.OT1N.$invalid">Please select Office Type(From) </span>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                                                <label class="control-label">From Location <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="Location" data-output-model="SearchSpace.Location" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                                    data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Location" name="TWR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select from location </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.OT2N.$invalid}">
                                                                <label class="control-label">Office Type (To) <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="OfficeType2" data-output-model="SearchSpace.OfficeType2" data-button-label="icon OT2N" data-item-label="icon OT2N"
                                                                    data-on-item-click="OT2Changed()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.OfficeType2" name="OT2N" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.OT2N.$invalid">Please select Office Type (To) </span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix">

                                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                                                <label class="control-label">To Location <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="ToLocation" data-output-model="SearchSpace.ToLocation" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                                    data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.ToLocation" name="TWR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select to location </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-sm-6 col-xs-12" style="padding-top: 18px">
                                                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color" ng-click="FindDistance()">Find Distance</button>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12" style="padding-top: 22px">
                                                        <table id="DistanceCalculation" class="table table-condensed table-bordered table-hover table-striped" border="1">
                                                            <tr style="color: #ffffff; background: #337ab7;">
                                                                <th>Source Lat-Longs</th>
                                                                <th>From Location</th>
                                                                <th>Destination Lat-Longs</th>
                                                                <th>To Location</th>
                                                                <th>Kilometers</th>
                                                                <th>Time Elapsed</th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="dvDistance"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script src="../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
    </script>
    <script src="../Utility.js"></script>
    <script src="MapMarker.js"></script>
</body>
</html>
