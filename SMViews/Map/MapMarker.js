﻿app.service('MapMarkerService', function ($http, $q, UtilityService) {
    this.GetZonewiseLoc = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MapMarker/GetZonewiseLoc')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.GetStatewiseLoc = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MapMarker/GetStatewiseLoc')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.GetMarkers = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MapMarker/GetMarkers')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});

app.controller('MapMarkerController', function ($scope, $q, MapMarkerService, UtilityService, $filter) {

    var map;
    var markers = [];
    $scope.Markers = [];
    $scope.Location = [];
    $scope.SearchSpace = {};
    $scope.SearchSpace.State = [];
    $scope.SearchSpace.City = [];
    $scope.SearchSpace.Location = [];
    $scope.SearchSpace.ToLocation = [];
    $scope.State = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.ToLocation = [];
    $scope.GetToLocation = [];
    $scope.OfficeType = [];
    $scope.OfficeType2 = [];
    $scope.OfficeType = [{ OT1C: 0, OT1N: 'Max Life' }, { OT1C: 1, OT1N: 'HDFC' }, { OT1C: 2, OT1N: 'IDBI' }, { OT1C: 3, OT1N: 'ICICI' }, { OT1C: 4, OT1N: 'SBI' },
                         { OT1C: 5, OT1N: 'Tata AIA' }, { OT1C: 6, OT1N: 'LIC' }, { OT1C: 7, OT1N: 'Birla' }, { OT1C: 8, OT1N: 'Kotak' }, { OT1C: 9, OT1N: 'Bajaj' }]

    $scope.OfficeType2 = [{ OT2C: 0, OT2N: 'Max Life' }, { OT2C: 1, OT2N: 'HDFC' }, { OT2C: 2, OT2N: 'IDBI' }, { OT2C: 3, OT2N: 'ICICI' }, { OT2C: 4, OT2N: 'SBI' },
                          { OT2C: 5, OT2N: 'Tata AIA' }, { OT2C: 6, OT2N: 'LIC' }, { OT2C: 7, OT2N: 'Birla' }, { OT2C: 8, OT2N: 'Kotak' }, { OT2C: 9, OT2N: 'Bajaj' }]
    $scope.GetLatLong = [];
    $scope.From_Latlong = [];
    $scope.To_Latlong = [];

    var URLparams = { URL: location.pathname }

    UtilityService.ValidatePagePath(URLparams).then(function (data) {
        if (data.data == 1) //0
        {
            window.location = "/maxlifeuat/login.aspx";
        }
    });

    MapMarkerService.GetStatewiseLoc().then(function (response) {
        if (response != undefined && response != null) {
            $scope.Statelst = response;
            $scope.totalsum = _.sumBy($scope.Statelst, 'LCMCNT');
            $scope.totalsum1 = _.sumBy($scope.Statelst, 'LCMCNT1');
            $scope.totalsum2 = _.sumBy($scope.Statelst, 'LCMCNT2');
            $scope.totalsum3 = _.sumBy($scope.Statelst, 'LCMCNT3');
            $scope.totalsum4 = _.sumBy($scope.Statelst, 'LCMCNT4');
            $scope.totalsum5 = _.sumBy($scope.Statelst, 'LCMCNT5');
            $scope.totalsum6 = _.sumBy($scope.Statelst, 'LCMCNT6');
            $scope.totalsum7 = _.sumBy($scope.Statelst, 'LCMCNT7');
            $scope.totalsum8 = _.sumBy($scope.Statelst, 'LCMCNT8');
            $scope.totalsum9 = _.sumBy($scope.Statelst, 'LCMCNT9');
        }

    });
    MapMarkerService.GetMarkers().then(function (response) {
        if (response != undefined && response != null) {
            angular.forEach(response, function (value, key) {
                addMarker(value);
            });
            $scope.GetLatLong.push(response);
        }
    });
    $scope.initMap = function () {
        var haightAshbury = { lat: 23.0885976, lng: 86.1615858 };

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: haightAshbury,
            mapTypeId: 'terrain'
        });
    }

    $scope.initMap();
    var infowindow = new google.maps.InfoWindow();

    // Adds a marker to the map and push to the array.
    function addMarker(data) {
        var marker = new google.maps.Marker({
            position: { lat: data.LAT, lng: data.LONG },
            map: map,
            title: data.LCM_NAME,
            icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'

        });
        if (data.LOCTYPE == "1")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

        if (data.LOCTYPE == "2")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
        if (data.LOCTYPE == "3")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
        if (data.LOCTYPE == "4")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/ltblue-dot.png');
        if (data.LOCTYPE == "5")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/pink.png');
        if (data.LOCTYPE == "6")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/orange.png');
        if (data.LOCTYPE == "7")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/purple.png');
        if (data.LOCTYPE == "8")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue.png');
        if (data.LOCTYPE == "9")
            marker.setIcon('http://maps.google.com/mapfiles/ms/icons/yellow.png');
       

        marker.ZN_CODE = data.ZN_CODE;
        marker.STE_CODE = data.STE_CODE;
        marker.LCM_CODE = data.LCM_CODE;
        marker.CTY_NAME = data.CTY_NAME;
        marker.LCM_NAME = data.LCM_NAME;
        marker.LOCTYPE = data.LOCTYPE;
        marker.TWR_CODE = data.TWR_CODE;
        marker.TWR_NAME = data.TWR_NAME;

        var content = '<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1 > ' + data.LCM_NAME + ' (' + data.LCM_CODE + ')  <br> <b>Total Seats:-</b> : ' + data.TOTAL_SEATS + ' <br/><b>Available:-</b> : ' + (parseInt(data.TOTAL_SEATS) - (parseInt(data.ALLOCATED_SEATS) + parseInt(data.OCCUPIED_SEATS))) + ' <br/><b>Occupied by Business Unit:-</b> : ' + data.ALLOCATED_SEATS + '  <br/><b>Occupied by Function/ Employee:-</b> : ' + data.OCCUPIED_SEATS + '   <BR/> <b>Address:</b>  ' + data.LCM_ADDR + '   </font>'
        marker.addListener('click', function () {
            if (infowindow)
                infowindow.close();
            infowindow.setContent(content);
            infowindow.open(map, marker);
        });

        $scope.Markers.push(marker);

    }
    var tempMarker;
    $scope.SteClick = function (state) {
        console.log(state);
        clearMarkers();
        var markers = $filter('filter')($scope.Markers, function (x) { if (x.STE_CODE === state.STE_CODE) return x; });
        //var markers = $filter('filter')($scope.Markers, { STE_CODE: state.STE_CODE });
        console.log(markers);
        tempMarker = markers;
        console.log(markers);
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    $scope.brnchClick = function (pcncode) {
        clearMarkers();
        var markers = [];
        if (tempMarker != null) {
            markers = $filter('filter')(tempMarker, function (x) { if (x.LOCTYPE === pcncode) return x; });
            //var markers = $filter('filter')(tempMarker, { LOCTYPE: state });
        }
        else {
            markers = $filter('filter')($scope.Markers, function (x) { if (x.LOCTYPE === pcncode) return x; });
            //var markers = $filter('filter')($scope.Markers, { LOCTYPE: state });

        }
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    $scope.DistanceMarkers = function (marker) {
        marker[0].setMap(map);
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {

        for (var i = 0; i < $scope.Markers.length; i++) {
            $scope.Markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    $scope.showMarkers = function () {
        tempMarker = $scope.Markers;
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        $scope.Markers = [];
    }

    UtilityService.getState(1).then(function (Stresponse) {
        if (Stresponse.data != null) {
            $scope.State = Stresponse.data;
        }

        UtilityService.getCities(1).then(function (Ctresponse) {
            if (Ctresponse.data != null) {
                $scope.City = Ctresponse.data;
            }
            UtilityService.getTowers(1).then(function (response) {
                if (response.data != null) {
                    $scope.Location = response.data;
                    angular.copy(response.data, $scope.ToLocation);
                    //$scope.ToLocation = response.data;
                }

            });
        });
    });

    //$scope.GetRoute = function () {
    //    var directionsDisplay;
    //    var directionsService = new google.maps.DirectionsService();

    //    //*********DIRECTIONS AND ROUTE**********************//
    //    source = "28.628119,77.221595";
    //    destination = "28.641218,77.337437";

    //    var request = {
    //        origin: source,
    //        destination: destination,
    //        travelMode: google.maps.TravelMode.DRIVING
    //    };
    //    //directionsService.route(request, function (response, status) {
    //    //    if (status == google.maps.DirectionsStatus.OK) {
    //    //        directionsDisplay.setDirections(response);
    //    //    }
    //    //});

    //    //*********DISTANCE AND DURATION**********************//
    //    var service = new google.maps.DistanceMatrixService();
    //    service.getDistanceMatrix({
    //        origins: [source],
    //        destinations: [destination],
    //        travelMode: google.maps.TravelMode.DRIVING,
    //        unitSystem: google.maps.UnitSystem.METRIC,
    //        avoidHighways: false,
    //        avoidTolls: false
    //    }, function (response, status) {
    //        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
    //            var distance = response.rows[0].elements[0].distance.text;
    //            var duration = response.rows[0].elements[0].duration.text;
    //            var dvDistance = document.getElementById("dvDistance");
    //            dvDistance.innerHTML = "";
    //            dvDistance.innerHTML += "Distance: " + distance + "<br />";
    //            dvDistance.innerHTML += "Duration:" + duration;

    //        } else {
    //            alert("Unable to find the distance via road.");
    //        }
    //    });
    //}

    $scope.FindDistance = function () {
        clearMarkers();
        $('#DistanceCalculation td').remove();
        var origin1 = _.filter($scope.Markers, { TWR_CODE: $scope.SearchSpace.Location[0].TWR_CODE });
        var table = $('#DistanceCalculation');
        $('#DistanceCalculation tr td').remove();
        var geocoder = new google.maps.Geocoder;
        directionsService = new google.maps.DirectionsService,
        directionsDisplay = new google.maps.DirectionsRenderer

        var bounds = new google.maps.LatLngBounds();

        //bounds.extend(origin1[0].getPosition());
        var service = new google.maps.DistanceMatrixService;
        $scope.DistanceMarkers(origin1);
        console.log($scope.SearchSpace.ToLocation);
        var minlat = 0.0, maxlat = 0.0, minlon = 0.0, maxlon = 0.0;
        var cnt = 1;
        angular.forEach($scope.SearchSpace.ToLocation, function (value, key) {
            var destinationA = _.filter($scope.Markers, { TWR_CODE: value.TWR_CODE });
            $scope.DistanceMarkers(destinationA);
            if (destinationA[0].getPosition() != undefined) {
                if (minlat != 0 && minlat > destinationA[0].getPosition().lat()) {
                    minlat = destinationA[0].getPosition().lat()
                    minlon = destinationA[0].getPosition().lng()
                }
                else if (minlat == 0) {
                    minlat = destinationA[0].getPosition().lat()
                    minlon = destinationA[0].getPosition().lng()
                }

                if (maxlat != 0 && maxlat < destinationA[0].getPosition().lat()) {
                    maxlat = destinationA[0].getPosition().lat()
                    maxlon = destinationA[0].getPosition().lng()
                }
                else if (maxlat == 0) {
                    maxlat = destinationA[0].getPosition().lat()
                    maxlon = destinationA[0].getPosition().lng()
                }
                service.getDistanceMatrix({
                    origins: [origin1[0].getPosition()],
                    destinations: [destinationA[0].getPosition()],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                },
                function (response, status) {

                    if (status !== 'OK') {
                        console.log(response);
                        cnt++;
                        alert('Error was: ' + status);
                    } else {
                        console.log(cnt);
                        cnt++;
                        table.append("<tr >" +
                            "<td>" + origin1[0].getPosition().lat().toFixed(5) + ',' + origin1[0].getPosition().lng().toFixed(5) + "</td>" +
                            "<td>" + origin1[0].TWR_NAME + '/' + origin1[0].LCM_NAME + "</td>" +
                            "<td>" + destinationA[0].getPosition().lat().toFixed(5) + ',' + destinationA[0].getPosition().lng().toFixed(5) + "</td>" +
                            "<td>" + destinationA[0].TWR_NAME + '/' + destinationA[0].LCM_NAME + "</td>" +
                            "<td>" + response.rows[0].elements[0].distance.text + "</td>" +
                            "<td>" + response.rows[0].elements[0].duration.text + "</td>" +
                            "</tr>");
                    }
                });
            }
            else {
                //console.log('position null ' + value.TWR_NAME);
            }

        });
        bounds.extend(new google.maps.LatLng(minlat, minlon));
        bounds.extend(new google.maps.LatLng(maxlat, maxlon));
        map.fitBounds(bounds);
    }
    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.SearchSpace.State, 1).then(function (response) {
            if (response.data != null)
                $scope.City = response.data;
            else
                $scope.City = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.State, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.State, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.SearchSpace.State = $scope.State;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.SearchSpace.State = [];
        $scope.SteChanged();
    }
    //// City Events

    $scope.CtyChangeAll = function () {
        $scope.SearchSpace.City = $scope.City;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SearchSpace.City = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.GetTowersByCity($scope.SearchSpace.City).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.SearchSpace.State.push(ste);
            }
        });
    }


    ///Office Type (From Change)
    $scope.OT1Changed = function () {
        UtilityService.GetTowersByCity($scope.SearchSpace.City).then(function (response) {
            var OfficeTypeLocations = $filter('filter')(response.data, { TWR_PCN_CODE: $scope.SearchSpace.OfficeType[0].OT1C })
            if (response.data != null) {
                $scope.Location = [];
                $scope.Location = OfficeTypeLocations;
            }
            else {
                $scope.Location = [];
            }
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.SearchSpace.Zone.push(zn);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.SearchSpace.State.push(ste);
            }
        });
    }

    ///Office Type (To Change)
    $scope.OT2Changed = function () {

        UtilityService.GetToLocations($scope.SearchSpace.OfficeType2[0].OT2C).then(function (response) {
            var OfficeTypeLocations2 = $filter('filter')(response.data, { TWR_PCN_CODE: $scope.SearchSpace.OfficeType2[0].OT2C })
            if (OfficeTypeLocations2 != null) {
                $scope.ToLocation = [];
                $scope.ToLocation = OfficeTypeLocations2;
            }
            else { $scope.ToLocation = []; }
        });
    }
    ///// Location Events

    $scope.LcmSelectNone = function () {
        $scope.SearchSpace.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SearchSpace.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });


        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.SearchSpace.State.push(ste);
            }
        });


    }

});
