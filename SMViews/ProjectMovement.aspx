﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Project Movement Requisition</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Source</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select City </label>
                                                <div class="col-md-6">
                                                    <select id="ddlcity" runat="server" data-live-search="true" class="selectpicker">
                                                        <option value="1">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select Location</label>
                                                <div class="col-md-6">
                                                    <select id="ddlsrcloc" runat="server" class="selectpicker" data-live-search="true">
                                                        <option value="1">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select Vertical</label>
                                                <div class="col-md-6">
                                                    <select id="ddlVertical" runat="server" class="selectpicker" data-live-search="true">
                                                        <option value="1">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select Cost Center</label>
                                                <div class="col-md-6">
                                                    <select id="ddlcostcenter" runat="server" class="selectpicker" data-live-search="true">
                                                        <option value="1">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-2 btn btn-default" id="verticalradio">
                                                    <input type="radio" name="group" value="Vertical" />
                                                    Vertical
                                                </label>
                                                <div class="col-md-6">
                                                    <label class="col-md-7 btn btn-default" id="verticalemployee">
                                                        <input type="radio" name="group" value="Employee" />
                                                        Vertical + Employee
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <table border="1" class="table table-bordered" style="display:none"id="verttable">
                                                        <tr>
                                                            <th colspan="1">
                                                                <h4></h4>
                                                            </th>
                                                            <th colspan="3">
                                                                <h4>Dedicated</h4>
                                                            </th>
                                                            <th colspan="3">
                                                                <h4>Shared</h4>
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th>Floor</th>
                                                            <th>Full Cabin</th>
                                                            <th>Half Cabin</th>
                                                            <th>Workstation</th>
                                                            <th>Full Cabin</th>
                                                            <th>Half Cabin</th>
                                                            <th>Workstation</th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Destination</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select City </label>
                                                <div class="col-md-6">
                                                    <select id="ddldescity" runat="server" data-live-search="true" class="selectpicker">
                                                        <option value="1">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select Location</label>
                                                <div class="col-md-6">
                                                    <select id="ddldesloc" runat="server" class="selectpicker" data-live-search="true">
                                                        <option value="1">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                <%--  <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select Floor</label>
                                                <div class="col-md-6">
                                                    <select id="ddldesflr" runat="server" class="selectpicker" data-live-search="true">
                                                        <option value="1">---Select---</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>     
                           <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <table border="1" class="table table-bordered" style="display:none" id="DestTable">
                                                        <tr>
                                                            <th colspan="1">
                                                                <h4></h4>
                                                            </th>
                                                            <th colspan="3">
                                                                <h4>Dedicated</h4>
                                                            </th>
                                                            <th colspan="3">
                                                                <h4>Shared</h4>
                                                            </th>
                                                        </tr>
                                                        <tr>      
                                                            <th>Floor</th>                                                      
                                                            <th>Full Cabin</th>
                                                            <th>Half Cabin</th>
                                                            <th>Workstation</th>
                                                            <th>Full Cabin</th>
                                                            <th>Half Cabin</th>
                                                            <th>Workstation</th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="button" value="move" class="btn btn-primary custom-button-color">Integrate</button>
                            </div>
                        </div>
                            </div>
                        </div>
<%--                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <table border="1" class="table table-condensed table-bordered table-hover table-striped" id="desgrid" style="display: none">
                                        <tr>
                                            <th>Floor</th>
                                            <th>Space ID</th>
                                            <th>Space Type</th>
                                            <th>Seat Type</th>
                                            <th>
                                                <input type='checkbox' name="check" id='dftchkAll' class="chkAll" />
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>--%>

                      <%--  <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="button" value="move" class="btn btn-primary custom-button-color">Move</button>
                            </div>
                        </div>--%>

                    </form>
                </div>

        <%-- <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select To Project</label>
                                        <div class="col-md-7">
                                            <select id="ddltoprj" runat="server" class="selectpicker" data-live-search="true">
                                                <option value="1">---Select---</option> 
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>  --%>
        <%--<div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input id="btnavail" type="button" value="Check Availability" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div> 
                        <div class="row" style="margin-top: 10px" id="trgrid" runat="server">
                            <div class="col-md-12">
                                <table border="1" class="table table-condensed table-bordered table-hover table-striped">
                                    <tr>
                                        <th>Select</th>
                                        <th>Seats</th>
                                        <th>Location</th>
                                        <th>Tower</th>
                                        <th>Floor</th>
                                        <th>Space_type</th>
                                        <th>Project</th>
                                        <th>Status</th>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="row" id="trbutton" runat="server" >
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input id="btnSubmit" type="button" runat="server" class="btn btn-primary custom-button-color" value="Submit" />                                   
                                </div>
                            </div>
                       </div> --%>
    </div>
    </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script type="text/javascript">  
    $(document).ready(function () {        
        getcity();
        getdescity();
        getvertical();
        getradio();
        //checkall();        
        $("#ddlcity").change(function () {
            getlocation();
            $("#ddlsrcloc option").remove();
        });

        $("#ddlVertical").change(function () {
            getcostcenter();
            $("#ddlcostcenter option").remove();
        });

        $("#ddldescity").change(function () {
            getdeslocation();
            $("#ddldesloc option").remove();
        });
        
        $("#ddldesloc").change(function () {
            $("#DestTable").show();            
            getdesseatdetails();
            $("#DestTable td").remove();
          //  getdesdetails();            
           // $("#desgrid td").remove();
        });


        //$("#ddldesloc").change(function () {
        //    getdesfloor();
        //    $("#ddldesflr option").remove();
        //});

        //$("#ddlcostcenter").change(function () {
        //    $("#table").show();
        //    getsrcdetails();
        //    $("#table td").remove();
        //});

    });

    function getcity() {
        $.getJSON("../api/ProjectMovementAPI/GetCity", function (result) {
            $.each(result, function (key, value) {
                $("#ddlcity").append($("<option></option>").val(value.CTY_CODE).html(value.CTY_NAME));
            });
            $("#ddlcity").selectpicker("refresh");
        });
    }

    function getlocation() {
        var dataObject = { CityId: $("#ddlcity").val() };
        $.post("../api/ProjectMovementAPI/GetLocation", dataObject, function (result) {
            $("#ddlsrcloc").append($("<option></option>").val("---Select---").html("---Select---"));
            $.each(result, function (key, value) {
                $("#ddlsrcloc").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
            $("#ddlsrcloc").selectpicker("refresh");
        });
    }

    function getvertical() {
        $.getJSON("../api/ProjectMovementAPI/GetVertical", function (result) {
            $.each(result, function (key, value) {
                $("#ddlVertical").append($("<option></option>").val(value.VER_CODE).html(value.VER_NAME));
            });
            $("#ddlVertical").selectpicker("refresh");
        });
    }

    function getcostcenter() {
        var dataobject = { getver: $("#ddlVertical").val() };
        $.post("../api/ProjectMovementAPI/GetCostCenter", dataobject, function (result) {
            $("#ddlcostcenter").append($("<option></option>").val("---Select---").html("---Select---"));
            $.each(result, function (key, value) {
                $("#ddlcostcenter").append($("<option></option>").val(value.cost_center_code).html(value.cost_center_name));
            })
            $("#ddlcostcenter").selectpicker("refresh");
        });
    }

    function getdescity() {
        $.getJSON("../api/ProjectMovementAPI/GetdesCity", function (result) {
            $.each(result, function (key, value) {
                $("#ddldescity").append($("<option></option>").val(value.CTY_CODE).html(value.CTY_NAME));
            });
            $("#ddldescity").selectpicker("refresh");
        });
    }

    function getdeslocation() {
        var dataObject = { CitydesId: $("#ddldescity").val() };
        $.post("../api/ProjectMovementAPI/GetdesLocation", dataObject, function (result) {
            $("#ddldesloc").append($("<option></option>").val("---Select---").html("---Select---"));
            $.each(result, function (key, value) {
                $("#ddldesloc").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
            $("#ddldesloc").selectpicker("refresh");
        });
    }

    function getdesfloor() {
        var dataObject = { descity: $("#ddldescity").val(), deslocation: $("#ddldesloc").val() };
        $.post("../api/ProjectMovementAPI/GetdesFloor", dataObject, function (result) {
            $("#ddldesflr").append($("<option></option>").val("---Select---").html("---Select---"));
            $.each(result, function (key, value) {
                $("#ddldesflr").append($("<option></option>").val(value.FLR_CODE).html(value.FLR_NAME));
            });
            $("#ddldesflr").selectpicker("refresh");
        });
    }


    function getverticaldetails() {
        var dataObject = {
            getempcity: $("#ddlcity").val(), getemploc: $("#ddlsrcloc").val(), getempver: $("#ddlVertical").val(), getempcost: $("#ddlcostcenter").val(),
            gettype: $("input[name=group]:checked").val()
        };
        $.post("../api/ProjectMovementAPI/Getsourceverticaldetails", dataObject, function (resultdata) {
            $.each(resultdata, function (key, value) {
                $("#verttable").append("<tr>" +                                    
                                    "<td>" + value.FLOOR + "</td>" +
                                    "<td>" + value.DEDICATEDFC + "</td>" +
                                    "<td>" + value.DEDICATEDHC + "</td>" +
                                    "<td>" + value.DEDICATEDWS + "</td>" +
                                    "<td>" + value.SHARABLEFC + "</td>" +
                                    "<td>" + value.SHARABLEHC + "</td>" +
                                    "<td>" + value.SHARABLEWS + "</td>" +
                                    "</tr>");                
            });               
            if (resultdata.length == 0) {
                $("#verttable").append("<tr>" + "<td colspan='7' align='center'> No Records Found.</td>" + "</tr>");                
            }           
        });       
    }

    function getdesseatdetails() {        
        var dataObject = {
            getdestcity: $("#ddldescity").val(), getdestloc: $("#ddldesloc").val()
        };
        $.post("../api/ProjectMovementAPI/Getdestseatdetails", dataObject, function (resultdata) {            
            $.each(resultdata, function (key, value) {
                $("#DestTable").append("<tr>" +                                    
                                    "<td>" + value.Floor + "</td>" +
                                    "<td>" + value.DedicatedFC + "</td>" +
                                    "<td>" + value.DedicatedHC + "</td>" +
                                    "<td>" + value.DedicatedWS + "</td>" +
                                    "<td>" + value.SharableFC + "</td>" +
                                    "<td>" + value.SharableHC + "</td>" +
                                    "<td>" + value.SharableWS + "</td>" +
                                    "</tr>");                
            });            
            if (resultdata.length == 0) {
                $("#DestTable").append("<tr>" + "<td colspan='7' align='center'> No Records Found.</td>" + "</tr>");
            }
        });
    }

    //function getdesdetails() {        
    //    var dataObject = { getdescity: $("#ddldescity").val(), getdesloc: $("#ddldesloc").val() };
    //    $.post("../api/ProjectMovementAPI/Getdesseatdetails", dataObject, function (result) {
    //        $.each(result, function (key, value) {
    //            $("#desgrid").append("<tr>" +
    //                                "<td>" + value.FLOOR + "</td>" +
    //                                "<td>" + value.SPACE_ID + "</td>" +
    //                                "<td>" + value.SPACE_TYPE + "</td>" +
    //                                "<td>" + value.SEAT_TYPE + "</td>" +
    //                                "<td>" + "<input type='checkbox' name='check' class='check'/>" + "</td>" +
    //                                "</tr>");
    //        });
            
    //    });
    //}

    //function checkall() {
    //    $("#dftchkAll").on('click', function () {
    //        $(".check").prop('checked', $(this).prop('checked'));
    //    });
    //}

    function getradio() {
        $('input[name="group"]').on('click', function () {            
            $('#verttable').show();
            getverticaldetails();
            $("#verttable td").remove();
        });
    }
</script>
