﻿
app.directive('validSubmit', ['$parse', function ($parse) {
    return {
        require: 'form',
        link: function (scope, element, iAttrs, form) {
            form.$submitted = false;
            var fn = $parse(iAttrs.validSubmit);
            element.on('submit', function (event) {
                scope.$apply(function () {
                    form.$submitted = true;
                    if (form.$valid) {
                        fn(scope, { $event: event });
                        form.$submitted = false;
                    }
                });
            });
        }
    };
}
]);

app.service("UtilityService", function ($http, $q) {
   this.path = window.location.origin;

   //this.path = window.location.origin + "/maxlifeuat";
    var deferred = $q.defer();

    this.ValidatePagePath = function (pagepath) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/ValidatePagePath', pagepath)
          .then(function (response) {
              deferred.resolve(response.data);

          }, function (response) {
              deferred.reject(response);

          });
        return deferred.promise;
    };
    // get employees names for auto complete

    this.GetEmployeeNameAndID = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetEmployeeNameAndID')
          .then(function (response) {
              deferred.resolve(response.data);

          }, function (response) {
              deferred.reject(response);

          });
        return deferred.promise;
    };

    this.getSysPreferences = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetSysPreferences')
          .then(function (response) {
              deferred.resolve(response.data);

          }, function (response) {
              deferred.reject(response);

          });
        return deferred.promise;
    };

    //get all users
    this.getallUsers = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetAllUsers')
          .then(function (response) {
              deferred.resolve(response.data);

          }, function (response) {
              deferred.reject(response);

          });
        return deferred.promise;
    };


    this.getCountires = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCountries?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getZone = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetZone?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getState = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetState?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getCities = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCities?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getCitiesByCountry = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/getCitiesByCountry?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getCitiesByLocations = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/getCitiesByLocations?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getLocations = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetLocations?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.GetToLocations = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetToLocations?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.getTowers = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetTowers?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;

    };

    this.getFloors = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetFloors?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getVerticals = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetVerticals?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getVerticalsByFlrid = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetVerticalsByFlrid?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getDesignation = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetDesignation')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getCostCenters = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCostCenters?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getCostCentersByFlrid = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCostCentersByFlrid?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getCostcenterByVertical = function (selectedVerticals, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCostcenterByVertical?id=' + ID + ' ', selectedVerticals)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getCostcenterByVerticalcode = function (selectedVerticals, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCostcenterByVerticalcode?id=' + ID + ' ', selectedVerticals)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.GetCityByStateCode = function (selectedState, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCityByStateCode?id=' + ID + ' ', selectedState)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getCitiesbyCny = function (cnylst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCitiesBycountry?id=' + ID + ' ', cnylst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getLocationsByCity = function (ctylst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetLocationsByCity?id=' + ID + ' ', ctylst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.GetLocationsByCostcenters = function (costlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetLocationsByCostcenters?id=' + ID + ' ', costlst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.GetTowersByCity = function (ctylst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetTowersByCity', ctylst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.getZoneByCny = function (cnylst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetZoneByCny?id=' + ID + ' ', cnylst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.getStateByZone = function (Znlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetStateByZone?id=' + ID + ' ', Znlst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.getCityByState = function (Znlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCityByState?id=' + ID + ' ', Znlst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getTowerByLocation = function (lcmlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetTowerByLocation?id=' + ID + ' ', lcmlst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getFloorByTower = function (twrlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetFloorByTower?id=' + ID + ' ', twrlst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;

    };
    this.getParentEntity = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetParentEntity?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;

    };

    this.getChildEntity = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetChildEntity?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;

    };

    this.getchildbyparent = function (pelst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetChildEntityByParent?id=' + ID + ' ', pelst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.getverticalbychild = function (chelst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetVerticalByChildEntity?id=' + ID + ' ', chelst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.getBussHeirarchy = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetBussHeirarchy')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.GetRoleByUserId = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetRoleByUserId')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
    this.GetRoles = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetRoles?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Canceled = 1;
    this.Unchanged = 2;
    this.Added = 4;
    this.Deleted = 8;
    this.Modified = 16;
    this.Approved = 32;
    this.Rejected = 64;

    this.ReqDate = 'MMM dd, yyyy';

    this.DateValidationOnSubmit = 'To Date Should be greater than From Date';

    // guest house booking

    this.getReservationTypes = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetReservationTypes?id=' + ID + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    this.GetFacilityNamesbyType = function (ID, rtlst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetFacilityNamesbyType?id=' + ID + ' ', rtlst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    // property 

    this.getPropertyTypes = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetPropTypes')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };


    this.getPropertyNames = function (lcmlst, ptlst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetProperties', lcmlst, ptlst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

});

/** Messenger Call SetUp **/
Messenger.options = {
    'parentLocations': ['body'],
    extraClasses: 'messenger-fixed messenger-on-top messenger-on-right',
    theme: 'future'
};


var progressIndicator = function (trgt, size) {
    var cl = new CanvasLoader(trgt);
    cl.setColor('#05569A');
    cl.setShape('spiral');
    cl.setDiameter(size);
    cl.setDensity(100);
    cl.setRange(1);
    cl.setSpeed(8);
    cl.setFPS(50);
    cl.show();
};

var progress = function (timeout, message, show) {
    Messenger().hideAll();
    if (!show) {
        $('.prg').remove();
        return true;
    }
    //
    var prgid = Math.ceil(Math.random() * 456);
    //
    $('.prg').remove();
    //
    var prg = '<ul id="prg_' + prgid + '" class="prg messenger messenger-fixed messenger-on-right messenger-on-bottom messenger-theme-future">';
    prg += '<li class="Overlay">';
    prg += '<li class="messenger-message-slot">';
    prg += '<div class="messenger-message message default alert-default messenger-will-hide-after">';
    prg += '<div class="messenger-message-inner">' + message + '</div>';
    prg += '<div class="messenger-spinner" id="prg_sp_' + prgid + '">';
    prg += '<span class="messenger-spinner-side messenger-spinner-side-left">';
    prg += '<span class="messenger-spinner-fill"></span>';
    prg += '</span>';
    prg += '<span class="messenger-spinner-side messenger-spinner-side-right">';
    prg += '<span class="messenger-spinner-fill"></span>';
    prg += '</span>';
    prg += '</div>';
    prg += '</div>';
    prg += '</li>';
    prg += '</li>';
    prg += '</ul>';
    //
    $('.amantra').append(prg);
    //
    progressIndicator("prg_sp_" + prgid, 26);
    //
    $('.prg').fadeIn();
    //
    if (timeout != 0)
        setTimeout(function () {
            $('.prg').remove();
        }, timeout * 1000);
    return true;
};



var showNotification = function (_type, _timeout, _template, _message) {
    switch (_template) {
        case "top":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-top',
                    'theme': 'block'
                };
                break;
            }
        case "bottom":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-bottom',
                    'theme': 'block'
                };
                break;
            }
        case "right":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-top messenger-on-right',
                    'theme': 'air'
                };
                break;
            }
        case "bottom-left":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-bottom messenger-on-left',
                    'theme': 'air'
                };
                break;
            }
        case "bottom-right":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger messenger-fixed messenger-on-bottom messenger-on-right messenger-theme-future',
                    'theme': 'Future'
                };
                break;
            }
    }
    Messenger().hideAll();
    switch (_type) {
        case 'success':
            Messenger().post({
                id: 'amantra',
                message: _message,
                type: _type,
                hideAfter: 60, //_timeout
                hideOnNavigate: true,
                showCloseButton: true
            });
            break;
        case 'error':
            Messenger().post({
                id: 'amantra',
                message: _message,
                type: _type,
                hideAfter: 60, //_timeout
                hideOnNavigate: true,
                showCloseButton: true
            });
            break;
        case 'Logout':
            Messenger().post({
                id: 'amantra',
                message: _message,
                type: _type,
                hideAfter: 10, //_timeout
                hideOnNavigate: false,
                showCloseButton: true
            });
            break;
    }

};

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return decodeURIComponent(urlparam[1]);
        }
    }
}
