﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%--    <link href="../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />



    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            width: 400px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>


</head>
<body data-ng-controller="ViewVerticalRequisitionController" class="amantra" onload="setDateVals()">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View and Approve {{BsmDet.Parent}} Requisitions</legend>
                        <div class="clearfix" data-ng-show="Viewstatus==1">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field &nbsp &nbsp    <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                    </fieldset>
                    <div class="well">
                        <div data-ng-show="Viewstatus==0">
                            <form role="form" id="ViewVerticalReq" name="frmViewVerReq" data-valid-submit="UpdateAllRequests()" novalidate>


                                <h4>My Requisitions</h4>
                                <div style="height: 300px">
                                    <input id="ReqFilter" placeholder="Filter by any..." type="text" style="width: 25%" />
                                    <div data-ag-grid="MyrequisitonsOptions" style="height: 104%; width: 100%" class="ag-blue"></div>
                                </div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <h4>Pending Requisitions / Waiting For Approval</h4>
                                <div style="height: 300px">
                                    <input id="ApprvlFilter" placeholder="Filter by any..." type="text" style="width: 25%" />
                                    <div data-ag-grid="approvalOptions" style="height: 104%;" class="ag-blue"></div>
                                </div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <div class="col-md-13 text-right">
                                    <div class="col-md-10" style="padding-left: 500px">
                                        <textarea rows="2" data-ng-model="SVR_APPR_REM" class="form-control" placeholder="Approval Remarks"></textarea>
                                    </div>
                                    <input type="submit" value="Approve" class="btn btn-primary custom-button-color" data-ng-click="setStatus('ApproveAll')" />
                                    <input type="submit" value="Reject" class="btn btn-primary custom-button-color" data-ng-click="setStatus('RejectAll')" />
                                </div>
                            </form>
                        </div>
                        <div data-ng-show="Viewstatus==1">
                            <form role="form" id="SearchSpaces" name="frmblksearch" data-valid-submit="SearchSpaces()" novalidate>


                                <div class="clearfix">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Request ID :</b> </label>
                                            {{currentblkReq.SVR_REQ_ID}}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Requested By :</b> </label>
                                            {{currentblkReq.AUR_KNOWN_AS}}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Requested Date :</b> </label>
                                            {{currentblkReq.SVR_REQ_DATE| date: dateformat}}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12 ">
                                        <div class="form-group">
                                            <label><b>Status :</b> </label>
                                            {{currentblkReq.STA_DESC}}
                                        </div>
                                    </div>

                                </div>


                                <div class="clearfix">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.CNY_NAME.$invalid}">
                                            <label for="txtcode">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Getcountry" data-output-model="VerReq.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                                data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==2||EnableStatus==1">
                                            </div>
                                            <input type="text" data-ng-model="VerReq.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                            <span id="message" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.ZN_NAME.$invalid}">
                                            <label class="control-label">Zone <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Zonelst" data-output-model="VerReq.SelectedZone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME"
                                                data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VerReq.SelectedZone" name="ZN_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.ZN_NAME.$invalid">Please Select Zone </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.STE_NAME.$invalid}">
                                            <label class="control-label">State <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Statelst" data-output-model="VerReq.SelectedState" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME"
                                                data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VerReq.SelectedState" name="STE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.STE_NAME.$invalid">Please Select State </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.CTY_NAME.$invalid}">
                                            <label for="txtcode">City <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Citylst" data-output-model="VerReq.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                                data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==2||EnableStatus==1">
                                            </div>
                                            <input type="text" data-ng-model="VerReq.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                            <span id="Span1" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.CTY_NAME.$invalid " style="color: red">Please Select City </span>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.LCM_NAME.$invalid}">
                                            <label for="txtcode">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locationlst" data-output-model="VerReq.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus != 0">
                                            </div>
                                            <input type="text" data-ng-model="VerReq.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                            <span id="Span2" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.TWR_NAME.$invalid}">
                                            <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Towerlist" data-output-model="VerReq.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                                data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus != 0">
                                            </div>
                                            <input type="text" data-ng-model="VerReq.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                            <span id="Span3" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.TWR_NAME.$invalid" style="color: red">Please Select Tower </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.FLR_NAME.$invalid}">
                                            <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floorlist" data-output-model="VerReq.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                                data-on-item-click="FlrChanged()" data-on-select-all="FlrChanged()" data-on-select-none="FlrSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus != 0">
                                            </div>
                                            <input type="text" data-ng-model="VerReq.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                            <span id="Span4" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.FLR_NAME.$invalid" style="color: red">Please Select Floor </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.SVR_FROM_DATE.$invalid}">
                                            <label for="fromdate">From Date <span style="color: red;">*</span></label>
                                            <div class='input-group date' id='fromdate'>
                                                <input type="text" id="txtFromDate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SVR_FROM_DATE" data-ng-model="currentblkReq.SVR_FROM_DATE" data-ng-disabled="EnableStatus != 0" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.SVR_TO_DATE.$invalid}">
                                            <label for="todate">To Date <span style="color: red;">*</span></label>
                                            <div class='input-group date' id='todate' onclick="setup('todate')">
                                                <input type="text" id="txtTodate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SVR_TO_DATE" data-ng-model="currentblkReq.SVR_TO_DATE" data-ng-disabled="EnableStatus != 0" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                                        <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus != 0">Search Spaces</button>
                                        <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                                    </div>
                                </div>

                            </form>

                            <form role="form" id="ApproveSelected" name="frmApproveSelected" data-valid-submit="UpdateRequest()" novalidate>
                                <div class="clearfix">

                                    <div class="col-md-8">


                                        <div style="height: 290px">
                                            <input id="UpdteFilter" placeholder="Filter by any..." type="text" style="width: 25%" />
                                            <div data-ag-grid="UpdateapprOptions" style="height: 275px; width: 100%" class="ag-blue"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <div>
                                            <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.SVR_VER_CODE.$invalid}">
                                                <label for="txtcode">{{BsmDet.Parent}}<span style="color: red;">*</span></label>
                                                <select data-ng-model="currentblkReq.SVR_VER_CODE" id="ddlvertical" required="" data-ng-disabled="EnableStatus !=0 " class="form-control" name="SVR_VER_CODE" data-live-search="true">
                                                    <option data-ng-repeat="obj in Verticallist" value="{{obj.VER_CODE}}">{{obj.VER_NAME}}</option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="txtcode">Requestor Remarks</label>
                                            <textarea class="form-control input-sm" rows="3" data-ng-model="currentblkReq.SVR_REQ_REM" name="SVR_REQ_REM" data-ng-disabled="EnableStatus != 0"></textarea>
                                        </div>
                                        <div class="form-group" data-ng-hide="EnableStatus!=1">
                                            <label for="txtcode">Approver Remarks</label>
                                            <textarea class="form-control input-sm" rows="3" data-ng-model="currentblkReq.SVR_APPR_REM" name="SVR_APPR_REM"></textarea>
                                        </div>




                                        <div class="col-md-12 text-right">
                                            <input type="button" id="btnviewinmap" data-ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                            <input type="submit" value="Update" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus != 0" data-ng-hide="EnableStatus ==1" data-ng-click="setStatus('Update')" />
                                            <input type="submit" value="Cancel" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus != 0" data-ng-hide="EnableStatus ==1" data-ng-click="setStatus('Cancel')" />
                                            <input type="submit" value="Approve" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus ==2" data-ng-hide="EnableStatus !=1" data-ng-click="setStatus('Approve')" />
                                            <input type="submit" value="Reject" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus ==2 " data-ng-hide="EnableStatus !=1" data-ng-click="setStatus('Reject')" />

                                        </div>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                    <div class="modal fade bs-example-modal-lg" id="historymodal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="panel-group box box-primary" id="Div2">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <div class="panel-heading">
                                            <h4 class="panel-title"
                                                data-target="#collapseTwo">Plot On the Map
                                            </h4>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <div isteven-multi-select data-input-model="MapFloors" data-selection-mode="single" data-helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                            data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color text-right" value="Proceed" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        Legends:
                                                    </div>
                                                    <div class="col-md-4">
                                                        <span class="label label-success pull-right">Available </span>
                                                        <span class="label selectedseat pull-right">Selected Seats </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form role="form" name="form2" id="form3">
                                            <div class="clearfix">
                                                <div class="col-md-12">
                                                    <div id="main">
                                                        <div id="leafletMap"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>

    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
        var UID = '<%= Session["UID"] %>';
        function setDateVals() {
            $('#txtFromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#txtToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }

    </script>
    <script src="../../Utility.js"></script>
    <script src="../Js/SearchSpaces.js"></script>
    <script src="../Js/ViewVerticalRequisitionSM.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
</body>

</html>

