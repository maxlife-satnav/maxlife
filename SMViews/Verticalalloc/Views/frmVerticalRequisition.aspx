﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="VerticalRequisitionController" class="amantra" onload="setDateVals()">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>{{BsmDet.Parent}} Requisition</legend>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field  &nbsp; &nbsp;    <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                    </fieldset>
                    <div class="well">
                        <form id="Form1" name="frmVertReq" data-valid-submit="SearchSpaces()" novalidate>

                            <%--<label class="control-label"><span style="color: red;">&nbsp&nbsp**</span>&nbsp&nbsp{{Autoselect}}</label>--%>
                            <%--<div class="panel-body">--%>
                            <div class="row">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="VertRequest.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.CNY_NAME.$invalid">Please Select Country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.ZN_NAME.$invalid}">
                                            <label class="control-label">Zone <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Zone" data-output-model="VertRequest.Zone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME"
                                                data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Zone" name="ZN_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.ZN_NAME.$invalid">Please Select Zone </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.STE_NAME.$invalid}">
                                            <label class="control-label">State <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="State" data-output-model="VertRequest.State" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME"
                                                data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.State" name="STE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.STE_NAME.$invalid">Please Select State </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.CTY_NAME.$invalid}">
                                            <label class="control-label">City <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="VertRequest.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.CTY_NAME.$invalid">Please Select City </span>

                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Location" data-output-model="VertRequest.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Location[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.LCM_NAME.$invalid">Please Select Location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Tower" data-output-model="VertRequest.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Tower[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.TWR_NAME.$invalid">Please Select Tower </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floor" data-output-model="VertRequest.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()"
                                                data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Floor[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.FLR_NAME.$invalid">Please Select Floor </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.SVR_FROM_DATE.$invalid}">
                                            <label class="control-label">From Date <span style="color: red;">*</span></label>
                                            <div class='input-group date' id='fromdate'>
                                                <input type="text" id="txtFrmDate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SVR_FROM_DATE" ng-model="currentblkReq.SVR_FROM_DATE" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.SVR_FROM_DATE.$invalid" style="color: red">Please Select From Date </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.SVR_TO_DATE.$invalid}">
                                            <label class="control-label">To Date <span style="color: red;">*</span></label>
                                            <div class='input-group date' id='todate'>
                                                <input type="text" id="txtTodate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SVR_TO_DATE" ng-model="currentblkReq.SVR_TO_DATE" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.SVR_TO_DATE.$invalid" style="color: red">Please Select To date </span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                                    <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </form>

                        <form role="form" id="Form2" name="frmblkreq" data-valid-submit="RaiseRequest()" novalidate ng-show="Markers.length !=0">
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div style="height: 325px;">
                                            <input id="filtertxt" placeholder="Filter..." type="text" style="width: 25%" />
                                            <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid}">
                                                <label class="control-label">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                                <select name="SVR_VER_CODE" data-ng-model="currentblkReq.SVR_VER_CODE" class="form-control" id="ddlvert" required="">
                                                    <option value="" selected>--Select--</option>
                                                    <option ng-repeat="Vert in Verticals" value="{{Vert.VER_CODE}}">{{Vert.VER_NAME}}

                                                    </option>

                                                </select>
                                                <span class="error" data-ng-show="frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid">Please select {{BsmDet.Parent}} </span>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.TWR_NAME.$invalid}">
                                                <label class="control-label">Requestor Remarks <span style="color: red;"></span></label>
                                                <textarea rows="4" cols="50" name="SVR_REQ_REM" data-ng-model="currentblkReq.SVR_REQ_REM" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="box-footer text-right">
                                            <button type="submit" id="btnRequest" class="btn btn-primary custom-button-color">Raise {{BsmDet.Parent}} Request</button>
                                            <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal fade bs-example-modal-lg" id="historymodal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="panel-group box box-primary" id="Div2">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <div class="panel-heading">
                                            <h4 class="panel-title"
                                                data-target="#collapseTwo">Plot On the Map
                                            </h4>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                            data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color text-right" value="Proceed" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        Legends:
                                                    </div>
                                                    <div class="col-md-4">
                                                        <span class="label label-success pull-right">Available </span>
                                                        <span class="label selectedseat pull-right">Selected Seats </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form role="form" name="form2" id="form3">
                                            <div class="clearfix">
                                                <div class="col-md-12">
                                                    <div id="main">
                                                        <div id="leafletMap"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--</form>-->
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
        function setDateVals() {
            $('#txtFrmDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#txtTodate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>
    <script src="../../Utility.js"></script>
    <script src="../Js/VerticalRequisition.js"></script>
</body>
</html>
