﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />

    <%-- <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
    <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>--%>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>


</head>
<body data-ng-controller="VerticalReleaseContoller" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>{{BsmDet.Parent}} Release</legend>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field     &nbsp; &nbsp;  <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                    </fieldset>
                    <div class="well">
                        <form role="form" id="verticalRelease" name="frmVerticalRelease" data-valid-submit="GetSpacesToRelease()" novalidate>

                            <div class="clearfix">

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.CNY_NAME.$invalid}">
                                        <label for="txtcode">Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Countrylst" data-output-model="VR.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                            data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.ZN_NAME.$invalid}">
                                        <label class="control-label">Zone <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Zonelst" data-output-model="VR.SelectedZone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME"
                                            data-on-item-click="ZonChanged()" data-on-select-all="ZonChangeAll()" data-on-select-none="ZonSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.SelectedZone" name="ZN_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.ZN_NAME.$invalid">Please select Zone </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.STE_NAME.$invalid}">
                                        <label class="control-label">State <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Statelst" data-output-model="VR.SelectedState" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME"
                                            data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.SelectedState" name="STE_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.STE_NAME.$invalid">Please select state </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.CTY_NAME.$invalid}">
                                        <label for="txtcode">City <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Citylst" data-output-model="VR.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                            data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.LCM_NAME.$invalid}">
                                        <label for="txtcode">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locationlst" data-output-model="VR.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.TWR_NAME.$invalid}">
                                        <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Towerlist" data-output-model="VR.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                            data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.FLR_NAME.$invalid}">
                                        <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Floorlist" data-output-model="VR.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                            data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                    </div>
                                </div>


                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmVerticalRelease.$submitted && frmVerticalRelease.VER_NAME.$invalid}">
                                        <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Verticallist" data-output-model="VR.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                            data-on-select-none="verticalSelectNone()" data-tick-property="ticked" max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="VR.selectedVerticals" name="VER_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmVerticalRelease.$submitted && frmVerticalRelease.VER_NAME.$invalid" style="color: red">Please select {{BsmDet.Parent |lowercase}} </span>
                                    </div>
                                </div>


                            </div>
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <button type="submit" value="Search" class="btn btn-primary custom-button-color">Search</button>
                                    <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>



                        </form>

                        <form role="form" id="verticalReleaseDetails" name="frmVerticalReleaseDetails">
                            <div data-ng-show="VRShowGrid">
                                <div class="row" style="padding-left: 16px;">
                                    <input id="filtertxt" placeholder="Filter..." type="text" style="width: 25%" />
                                </div>
                                <div class="row">
                                    <%--   <div style="height: 25px; padding-left: 36px;">
                                    <input type='checkbox' data-ng-model="SelectAll" data-ng-click='selectAllRows()'>
                                </div>--%>
                                    <div class="col-md-12" style="height: 320px">

                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-4">
                                    </div>

                                    <div class="col-md-4">
                                        <textarea rows="2" data-ng-model="SVRL_REL_REM" class="form-control" placeholder="Remarks"></textarea>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                        <input type="submit" data-ng-click="SaveDetails()" value="Release Selected Seats" class="btn btn-primary custom-button-color" />
                                    </div>

                                </div>
                                <div class="row">
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="modal fade bs-example-modal-lg" id="historymodal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="panel-group box box-primary" id="Div2">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <div class="panel-heading">
                                            <h4 class="panel-title"
                                                data-target="#collapseTwo">Plot On the Map
                                            </h4>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                            data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color text-right" value="Proceed" />
                                                    </div>
                                                    <div class="col-md-4">
                                                        Legends:
                                                    </div>
                                                    <div class="col-md-4">
                                                        <span class="label label-success pull-right">Available </span>
                                                        <span class="label selectedseat pull-right">Selected Seats </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form role="form" name="form2" id="form3">
                                            <div class="clearfix">
                                                <div class="col-md-12">
                                                    <div id="main">
                                                        <div id="leafletMap"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script type="text/javascript">
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
    </script>

    <script src="../../Utility.js"></script>

    <script src="../Js/VerticalRelease.js"></script>
</body>
</html>


