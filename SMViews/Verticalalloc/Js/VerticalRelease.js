﻿app.service("VerticalReleaseService", function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    this.GetSpaceAllocationsForVerticalRelease = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/VerticalRelease/GetSpaceAllocationsForVerticalRelease', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    this.Release_Allocated_Seats = function (VRObj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/VerticalRelease/UpdateSSAAndInsertVerticalRelease', VRObj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('VerticalReleaseContoller', function ($scope, $q, VerticalReleaseService, UtilityService, $filter, blockUI) {
    $scope.Viewstatus = 0;
    $scope.VR = {};
    $scope.Countrylst = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Zonelst = [];
    $scope.Statelst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.VRShowGrid = false;
    $scope.Map = {};
    $scope.Map.Floor = [];
    $scope.Markers = [];
    sendCheckedValsObj = [];
    $scope.SelRowData = [];
    $scope.VerticalReleaseDDLObj = {};
    blockUI.start();

   
    var map = L.map('leafletMap');//.setView([17.561298804683357, 79.6684030798511], 11);

    var URLparams = { URL: location.pathname }

    UtilityService.ValidatePagePath(URLparams).then(function (data) {
        if (data.data == 1) //0
        {
            window.location = "/maxlifeuat/login.aspx";
        }
    });


    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });
    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Countrylst = response.data;
        }
        UtilityService.getZone(2).then(function (response) {
            if (response.data != null) {
                $scope.Zonelst = response.data;
            }
            UtilityService.getState(2).then(function (response) {
                if (response.data != null) {
                    $scope.Statelst = response.data;
                }
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Citylst = response.data;
                    }
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locationlst = response.data;
                        }
                        UtilityService.getTowers(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Towerlist = response.data;
                            }

                            UtilityService.getFloors(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Floorlist = response.data;
                                }
                            });

                        });
                    });
                });
            });
        });
    });

    //country
    $scope.CountryChanged = function () {
        //UtilityService.getZoneByCny($scope.VR.selectedCountries, 2).then(function (response) {
        //    $scope.Citylst = response.data
        //}, function (error) {
        //    console.log(error);
        //});

        if ($scope.VR.selectedCountries.length != 0) {
            UtilityService.getZoneByCny($scope.VR.selectedCountries, 2).then(function (response) {
                if (response.data != null)
                    $scope.Zonelst = response.data;
                else
                    $scope.Zonelst = [];
            });
        }
        else
            $scope.Zonelst = [];
    }
    $scope.CnyChangeAll = function () {
        //UtilityService.getCitiesbyCny($scope.Countrylst, 2).then(function (response) {
        //    $scope.Citylst = response.data;
        //}, function (error) {
        //    console.log(error);
        //});
        $scope.VR.selectedCountries = $scope.Countrylst;
        $scope.CountryChanged();
    }
    $scope.cnySelectNone = function () {
        $scope.VR.selectedCountries = [];
        $scope.CountryChanged();

    }
    //zone
    $scope.ZonChanged = function () {
        UtilityService.getStateByZone($scope.VR.SelectedZone, 1).then(function (data) {
            if (data.data == null) { $scope.Statelst = [] } else {
                $scope.Statelst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.VR.selectedCountries.push(cny);
            }
        });
    }
    $scope.ZonChangeAll = function () {
        $scope.VR.SelectedZone = $scope.Zonelst;
        $scope.ZonChanged();
    }
    $scope.ZonSelectNone = function () {
        $scope.VR.SelectedZone =[];
        $scope.ZonChanged();
    }

    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.VR.SelectedState, 1).then(function (data) {
            if (data.data == null) { $scope.Citylst = [] } else {
                $scope.Citylst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.VR.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var zne = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zne != undefined && value.ticked == true) {
                zne.ticked = true;
                $scope.VR.SelectedZone.push(zne);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.VR.SelectedState = $scope.Statelst;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.VR.SelectedState = [];
        $scope.SteChanged();
    }
    //city
    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.VR.selectedCities, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.VR.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var zne = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zne != undefined && value.ticked == true) {
                zne.ticked = true;
                $scope.VR.SelectedZone.push(zne);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.VR.SelectedState.push(ste);
            }
        });

    }
    $scope.CtyChangeAll = function () {
        //UtilityService.getLocationsByCity($scope.Citylst, 2).then(function (response) {
        //    $scope.Locationlst = response.data;
        //}, function (error) {
        //    console.log(error);
        //});
        //$scope.CityChanged();
        $scope.VR.selectedCities = $scope.Citylst;
        $scope.CityChanged();
    }
    $scope.ctySelectNone = function () {
        $scope.VR.selectedCities = $scope.Citylst;
        $scope.CityChanged();
    }


    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.VR.selectedLocations, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });



        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        $scope.VR.selectedCountries = [];
        $scope.VR.selectedCities = [];

        angular.forEach($scope.Locationlst, function (value, key) {

            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.VR.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.VR.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var zne = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zne != undefined && value.ticked == true) {
                zne.ticked = true;
                $scope.VR.SelectedZone.push(zne);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.VR.SelectedState.push(ste);
            }
        });
    }
    $scope.LCMChangeAll = function () {
        //UtilityService.getTowerByLocation($scope.Locationlst, 2).then(function (response) {
        //    $scope.Towerlist = response.data;
        //}, function (error) {
        //    console.log(error);
        //});

        //$scope.LocChange();
        $scope.VR.selectedLocations = $scope.Locationlst;
        $scope.LocChange();
    }
    $scope.lcmSelectNone = function () {
        $scope.VR.selectedLocations = [];
        $scope.LocChange();
    }

    $scope.TwrChange = function () {
        UtilityService.getFloorByTower($scope.VR.selectedTowers, 2).then(function (response) {
            $scope.Floorlist = response.data;
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.VR.selectedCountries = [];
        $scope.VR.selectedCities = [];
        $scope.VR.selectedLocations = [];

        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.VR.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.VR.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.VR.selectedLocations.push(lcm);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var zne = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zne != undefined && value.ticked == true) {
                zne.ticked = true;
                $scope.VR.SelectedZone.push(zne);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.VR.SelectedState.push(ste);
            }
        });


    }
    $scope.TwrChangeAll = function () {
        //UtilityService.getFloorByTower($scope.Towerlist, 2).then(function (response) {
        //    $scope.Floorlist = response.data;
        //}, function (error) {
        //    console.log(error);
        //});
        //$scope.TwrChange();
        $scope.VR.selectedTowers = $scope.Towerlist;
        $scope.TwrChange();
    }
    $scope.twrSelectNone = function () {
        $scope.VR.selectedTowers = [];
        $scope.TwrChange();
    }

    $scope.FloorChangeAll = function () {
        $scope.VR.selectedFloors = $scope.Floorlist;
        $scope.FloorChange();
    }
    $scope.FloorSelectNone = function () {
        $scope.VR.selectedFloors = [];
        $scope.FloorChange();
    }
    $scope.FloorChange = function () {
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.VR.selectedCountries = [];
        $scope.VR.selectedCities = [];
        $scope.VR.selectedLocations = [];
        $scope.VR.selectedTowers = [];

        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.VR.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.VR.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.VR.selectedLocations.push(lcm);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.VR.selectedTowers.push(twr);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var zne = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zne != undefined && value.ticked == true) {
                zne.ticked = true;
                $scope.VR.SelectedZone.push(zne);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.VR.SelectedState.push(ste);
            }
        })
    }

    UtilityService.getVerticals(2).then(function (response) {
        $scope.Verticallist = response.data;
    }, function (error) {
        console.log(error);
    });

    $scope.GetSpacesToRelease = function () {
        progress(0, 'Loading...', true);
        $scope.Markers = [];
        $scope.SelRowData = [];
        $scope.SVRL_REL_REM = "";
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.vrvm = {
            selectedFloors: $scope.VR.selectedFloors,
            selectedVerticals: $scope.VR.selectedVerticals,
        };
        VerticalReleaseService.GetSpaceAllocationsForVerticalRelease($scope.vrvm).then(function (response) {
            if (response.data != null) {
                $scope.VRShowGrid = true;
                $scope.gridata = response.data;
                $scope.gridOptions.api.setRowData([]);
                GetMarkers(response.data);
                $scope.gridOptions.api.setRowData($scope.Markers);
                console.log($scope.Markers);
                $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                $scope.gridOptions.api.refreshHeader();
                progress(0, 'Loading...', false);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                $scope.VRShowGrid = false;
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', response.Message);



            }
        }, function (error) {
            console.log(error);
            progress(0, 'Loading...', false);
        });
    }

    var columnDefs = [
        { headerName: "Select All", field: "ticked", width: 200, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)' />", cellClass: 'grid-align', headerCellRenderer: headerCellRendererFunc, pinned: 'left', suppressMenu: true, },
         { headerName: "Space", field: "SPC_NAME", width: 700, cellClass: 'grid-align', pinned: 'left' },
         { headerName: "Tower", field: "TWR_NAME", width: 500, cellClass: 'grid-align' },
         { headerName: "Floor", field: "FLR_NAME", width: 500, cellClass: 'grid-align' },
         { headerName: "", field: "VER_NAME", width: 400, cellClass: 'grid-align' },
         { headerName: "Space Type", field: "SSA_SPC_TYPE", width: 400, cellClass: 'grid-align' },
         { headerName: "Space Sub Type", field: "SSA_SPC_SUB_TYPE", width: 500, cellClass: 'grid-align' },
         { headerName: "From Date", template: '{{data.SSA_FROM_DATE | date: "dd MMM, yyyy"}}', field: "SSA_FROM_DATE", width: 350, cellClass: 'grid-align', suppressMenu: true, },
         { headerName: "To Date", template: '{{data.SSA_TO_DATE | date: "dd MMM, yyyy"}}', field: "SSA_TO_DATE", width: 350, cellClass: 'grid-align', suppressMenu: true, },
    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $scope.chkChanged = function (data) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: data.SSA_SPC_ID, spacetype: 'CHA' } });
            if (data.ticked) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
                //data.setIcon(selctdChricon)
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
                //data.setIcon(Vacanticon)
            }
        }
    }

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = true;
                    });

                    if ($scope.drawnItems) {

                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SSA_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = true;
                                value.setStyle(selctdChrStyle);
                            }
                        });
                    }

                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = false;
                    });
                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SSA_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = false;
                                value.setStyle(VacantStyle);
                            }
                        });
                    }
                });
            }
        });
        return eHeader;
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        rowData: null,
        enableSorting: true,
        rowSelection: 'multiple',
        angularCompileRows: true,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };


    $scope.SaveDetails = function () {
        var sendCheckedValsObj = [];
        angular.forEach($scope.Markers, function (Value, Key) {
            $scope.selspcObj = {};
            if (Value.ticked) {
                $scope.selspcObj.SSA_SRNREQ_ID = Value.SSA_SRNREQ_ID;
                $scope.selspcObj.SSA_SRNCC_ID = Value.SSA_SRNCC_ID;
                $scope.selspcObj.SSED_VER_NAME = Value.SSED_VER_NAME;
                $scope.selspcObj.SSA_VER_CODE = Value.SSA_VER_CODE;
                $scope.selspcObj.TWR_NAME = Value.TWR_NAME;
                $scope.selspcObj.FLR_NAME = Value.FLR_NAME;
                $scope.selspcObj.VER_NAME = Value.VER_NAME;
                $scope.selspcObj.SSA_SPC_ID = Value.SSA_SPC_ID;
                $scope.selspcObj.SSA_FROM_DATE = Value.SSA_FROM_DATE;
                $scope.selspcObj.SSA_TO_DATE = Value.SSA_TO_DATE;
                $scope.selspcObj.SSA_STA_ID = Value.SSA_STA_ID;
                $scope.selspcObj.ticked = Value.ticked;
                $scope.selspcObj.SSA_SPC_TYPE = Value.SSA_SPC_TYPE;
                $scope.selspcObj.SSA_SPC_SUB_TYPE = Value.SSA_SPC_SUB_TYPE;
                $scope.selspcObj.SPC_NAME = Value.SPC_NAME;
                $scope.selspcObj.SPC_FLR_ID = Value.SPC_FLR_ID;
                $scope.selspcObj.lat = Value.lat;
                $scope.selspcObj.lon = Value.lon;
                sendCheckedValsObj.push($scope.selspcObj);
            }
        });
        if (sendCheckedValsObj.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Check atleast One Space Id To Release.');
        }
        else {
            $scope.dataobject = {
                VR: sendCheckedValsObj,
                SVRL_REL_REM: $scope.SVRL_REL_REM
            };
            VerticalReleaseService.Release_Allocated_Seats($scope.dataobject).then(function (response) {
                var spaceids = "";
                angular.forEach(response.data, function (data) {
                    spaceids = spaceids + data.SSA_SPC_ID + ", ";
                });
                spaceids = spaceids.substring(0, spaceids.length - 1);
                console.log(response.STATUS);
                if (response.STATUS == "FAIL")
                    showNotification('success', 8, 'bottom-right', response.Message + spaceids);
                else {
                    showNotification('success', 8, 'bottom-right', response.Message);
                    $scope.Clear();
                }

            }, function (error) {
                console.log(error);
            })
        }

    }
    //For Filter
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    blockUI.stop();
    $scope.SelectAll = false;

    $scope.selectAllRows = function selectAll() {
        angular.forEach($scope.gridata, function (data) {
            data.ticked = $scope.SelectAll;
        });
    };

    $scope.Clear = function () {
        $scope.gridata = [];
        $scope.Markers = [];
        $scope.VR = {};

        angular.forEach($scope.Countrylst, function (country) {
            country.ticked = false;
        });
        angular.forEach($scope.Citylst, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floorlist, function (floor) {
            floor.ticked = false;
        });

        angular.forEach($scope.Verticallist, function (ver) {
            ver.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (floor) {
            floor.ticked = false;
        });

        angular.forEach($scope.Statelst, function (ver) {
            ver.ticked = false;
        });

        $scope.VR.selectedCountries = [];
        $scope.VR.selectedCities = [];
        $scope.VR.selectedLocations = [];
        $scope.VR.selectedTowers = [];
        $scope.VR.selectedFloors = [];
        $scope.VR.selectedVerticals = [];
        $scope.VR.SelectedState = [];
        $scope.VR.SelectedZone = [];

        $scope.VRShowGrid = false;
    }


    ///// For map layout
    $scope.ViewinMap = function () {
        //$scope.MapFloors = [];
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.VR.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }

        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        if ($scope.SelRowData.length == 0)
            $scope.loadmap();
    });

    $scope.FlrSectMap = function (data) {
        $scope.SelectedFlr = data;
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        //$('#leafletMap').empty();

        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });
    }


    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
           // success
           // results: an array of data objects from each deferred.resolve(data) call
           function (results) {
               var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
               map.fitBounds(bounds);
               $scope.SelRowData = $filter('filter')($scope.Markers, { SPC_FLR_ID: $scope.Map.Floor[0].FLR_CODE });
               console.log($scope.SelRowData);
               angular.forEach($scope.SelRowData, function (value, key) {
                   $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SSA_SPC_ID, spacetype: 'CHA' } });
                   $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                   $scope.marker.SSA_SRNCC_ID = value.SSA_SRNCC_ID;
                   $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
                   $scope.marker.SSA_VER_CODE = value.SSA_VER_CODE;
                   $scope.marker.TWR_NAME = value.TWR_NAME;
                   $scope.marker.FLR_NAME = value.FLR_NAME;
                   $scope.marker.VER_NAME = value.VER_NAME;
                   $scope.marker.SSA_SPC_ID = value.SSA_SPC_ID;
                   $scope.marker.SSA_FROM_DATE = value.SSA_FROM_DATE;
                   $scope.marker.SSA_TO_DATE = value.SSA_TO_DATE;
                   $scope.marker.SSA_STA_ID = value.SSA_STA_ID;;
                   $scope.marker.ticked = value.ticked;
                   if (value.ticked)
                       $scope.marker.setStyle(selctdChrStyle);
                   else
                       $scope.marker.setStyle(VacantStyle)
                   $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
                   $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
                   $scope.marker.SPC_NAME = value.SPC_NAME;
                   $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                   $scope.marker.lat = value.lat;
                   $scope.marker.lon = value.lon;
                   $scope.marker.layer = value.SSA_SPC_TYPE;
                   $scope.marker.bindLabel(value.SPC_NAME);
                   $scope.marker.on('click', markerclicked);
                   $scope.marker.addTo(map);
               });
           },
           // error
           function (response) {
           }
        );
    };

        //var Vacanticon = L.icon({
        //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
        //    iconSize: [16, 16], // size of the icon
        //});
        //var selctdChricon = L.icon({
        //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
        //    iconSize: [16, 16], // size of the icon
        //});


        var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
        var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};
            $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
            $scope.marker.SSA_SRNCC_ID = value.SSA_SRNCC_ID;
            $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
            $scope.marker.SSA_VER_CODE = value.SSA_VER_CODE;
            $scope.marker.TWR_NAME = value.TWR_NAME;
            $scope.marker.FLR_NAME = value.FLR_NAME;
            $scope.marker.VER_NAME = value.VER_NAME;
            $scope.marker.SSA_SPC_ID = value.SSA_SPC_ID;
            $scope.marker.SSA_FROM_DATE = value.SSA_FROM_DATE;
            $scope.marker.SSA_TO_DATE = value.SSA_TO_DATE;
            $scope.marker.SSA_STA_ID = value.SSA_STA_ID;
            $scope.marker.ticked = value.ticked;
            $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
            $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
            $scope.marker.SPC_NAME = value.SPC_NAME;
            $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
            $scope.marker.lat = value.lat;
            $scope.marker.lon = value.lon;
            $scope.marker.layer = value.SSA_SPC_TYPE;
            $scope.Markers.push($scope.marker);
        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SSA_SPC_ID: this.SSA_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            //this.setIcon(selctdChricon)
            this.ticked = true;
            marker.ticked = true;
        }
        else {

            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

});