﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />

</head>
<body data-ng-controller="UploadSpacesController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Upload Space Allocation</legend>
                    </fieldset>
                    <div class="well">
                        <div id="divDwnTemplt" class="animate-show" ng-show="ToggleDiv">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>
                            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                                <legend style="padding-top: 0px; padding-left: 5px; width: 21%; font-size: 14px; font-weight: 500">Download data template</legend>

                                <form id="Form1" name="frmUploadspc" data-valid-submit="DownloadExcel()" novalidate>

                                    <%--<label class="control-label"><span style="color: red;">&nbsp&nbsp**</span>&nbsp&nbsp{{Autoselect}}</label>--%>
                                    <%--<div class="panel-body">--%>
                                    <div class="clearfix">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="UploadSpaces.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UploadSpaces.Country" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.CNY_NAME.$invalid">Please select country </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.ZN_NAME.$invalid}">
                                            <label class="control-label">Zone <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Zone" data-output-model="UploadSpaces.Zone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME"
                                                data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UploadSpaces.Zone" name="ZN_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.ZN_NAME.$invalid">Please select Zone </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.STE_NAME.$invalid}">
                                            <label class="control-label">State <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="State" data-output-model="UploadSpaces.State" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME"
                                                data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UploadSpaces.State" name="STE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.STE_NAME.$invalid">Please select state </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.CTY_NAME.$invalid}">
                                            <label class="control-label">City <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="UploadSpaces.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UploadSpaces.City" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.CTY_NAME.$invalid">Please select city </span>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Location" data-output-model="UploadSpaces.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UploadSpaces.Location" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.LCM_NAME.$invalid">Please select location </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Tower" data-output-model="UploadSpaces.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UploadSpaces.Tower" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.TWR_NAME.$invalid">Please select tower </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floor" data-output-model="UploadSpaces.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UploadSpaces.Floor" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.FLR_NAME.$invalid">Please select floor </span>
                                        </div>
                                    </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.AllocType.$invalid}">
                                                <label class="control-label">Allocation Type<span style="color: red;">*</span></label>
                                                <select name="AllocType" data-ng-model="UploadSpaces.AllocType" class="form-control" id="ddlvert" ng-change="AllocTypeChange()" required="">
                                                    <option value="" selected>None Selected</option>
                                                    <option ng-repeat="Alloc in AllocType" value="{{Alloc.CODE}}">{{Alloc.NAME}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.AllocType.$invalid">Please select Allocation Type</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label></label>
                                            <div class="box-footer text-right">
                                                <input type="submit" id="btnsubmit" value="Download Template" class="btn btn-primary custom-button-color" />
                                                <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </fieldset>
                            <br />
                        </div>
                        <div id="divUploadTemplt">
                            <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                                <legend style="padding-top: 0px; padding-left: 5px; width: 21%; font-size: 14px; font-weight: 500">Space Allocation data upload</legend>
                                <form role="form" id="FileUsrUpl" name="FileUsrUpl" data-valid-submit="UploadFile()" novalidate>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <a href="" id="lnkdwn" ng-click="ToggleDiv = !ToggleDiv">Click here to download the template </a>
                                    </div>

                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': FileUsrUpl.$submitted && FileUsrUpl.AllocType.$invalid}">
                                            <label>Upload Type : </label>
                                            <div class="form-group">
                                                <label ng-repeat="Alloc in UplAllocType" class="col-md-6">
                                                    <input type="radio" name="UplAllocType" ng-model="UploadSpaces.UplAllocType" ng-value="Alloc.CODE" required="" />
                                                    {{Alloc.NAME}}
                                                </label>
                                            </div>
                                            <span class="error" data-ng-show="FileUsrUpl.$submitted && FileUsrUpl.UplAllocType.$invalid">Please select Upload Type</span>
                                        </div>

                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label class="custom-file">
                                            <input type="file" name="UPLFILE" id="FileUpl" required="" class="custom-file-input">
                                            <span class="custom-file-control"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">More Options 
                                        </a>
                                        <div class="collapse" id="collapseExample">
                                            <div class="card card-block">
                                                <label ng-repeat="Alloc in UplOptions" class="col-md-12">
                                                    <input type="checkbox" ng-model="Alloc.isChecked" />
                                                    {{Alloc.NAME}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-footer text-right">
                                        <input type="submit" id="btnUpload" class="btn btn-primary custom-button-color" value="Upload Excel" />
                                          <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                    </div>
                                    <div ng-show="DispDiv">
                                        <div class="box-footer text-right" id="table2">
                                            <br />
                                            <a data-ng-click="GenReport('doc')"><i id="word" data-ng-show="DocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                            <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                            <a data-ng-click="GenReport('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                        </div>
                                        <div>
                                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                        </div>
                                    </div>

                                </form>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Utility.js"></script>
    <script src="UploadSpaceAllocation.js"></script>
</body>

</html>
