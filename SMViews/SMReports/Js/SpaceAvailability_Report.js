﻿app.service("SpaceAvailabilityReportService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (SpaceAvailability) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAvailabilityReport/GetAvlGrid', SpaceAvailability)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAvailabilityReport/GetDetailsOnSelection/', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


});

app.controller('SpaceAvailabilityReportController', function ($scope, $q, $http, SpaceAvailabilityReportService, UtilityService, $timeout, blockUI) {
    $scope.SpaceAvailability = {};
    $scope.Avldata = {};
    $scope.Type = [];
    $scope.SelValue = [];
    $scope.ActionStatus = 0;
    $scope.DocTypeVisible = 0;
    $scope.PopDocTypeVisible = 0;
    blockUI.start();
    $scope.columnDefs = [
              { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align', width: 110, suppressMenu: true },
              { headerName: "Zone", field: "ZN_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "State", field: "STE_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 160 },
              { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true },
              { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "Space Type", field: "SPC_TYPE", cellClass: 'grid-align', width: 110 },
              { headerName: "Space Sub Type", field: "SPACE_SUB_TYPE", cellClass: 'grid-align', width: 150 },
              { headerName: "Total Seats", field: "TOTAL_SEATS_COUNT", cellClass: 'grid-align', width: 80, suppressMenu: true, pinned: 'right' },
              { headerName: "Vacant Seats", field: "VACANT_SEATS", cellClass: 'grid-align', width: 100, filter: 'set', template: '<a ng-click="ShowPopup(data)">{{data.VACANT_SEATS}}</a>', suppressMenu: true, pinned: 'right' }];

    var rowData = [];
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {
            FromDate: $scope.SpaceAvailability.FromDate,
            ToDate: $scope.SpaceAvailability.ToDate
         
        };
        SpaceAvailabilityReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                    progress(0, 'Loading...', false);
            }
            else {
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 600);

            }
            $scope.SpaceAvalChart();
            //chart.unload();
            //chart.load({ columns: data });
            //setTimeout(function () {
            //    $("#AvalGraph").append(chart.element);
            //}, 700);
        }, function (error) {
            console.log(error);
        });
    }

 

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }

    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: rowData,
        enableColResize: true,        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },        showToolPanel: true

    };
    var PopDefs = [
          { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align', width: 110 },
              { headerName: "Zone", field: "ZN_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "State", field: "STE_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
              { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 150 },
              { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110 },
              { headerName: "Space Id", field: "SPC_ID", width: 190, cellClass: 'grid-align' },
       
    ];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        onReady: function () {
            $scope.PopOptions.api.sizeColumnsToFit()
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.PopOptions.api.getFilterModel()))
                $scope.PopDocTypeVisible = 0;
            else
                $scope.PopDocTypeVisible = 1;
        }
    }
    $scope.ShowPopup = function (data) {
        console.log(data);
        $scope.SelValue = data;
        $scope.CurrentProfile = [];
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        SpaceAvailabilityReportService.GetDetailsOnSelection($scope.SelValue).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.popdata = response.data;

            $scope.PopOptions.api.setRowData($scope.popdata);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 500);

        });
    });

    $("#Tabular").fadeIn();
   
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();
    var chart;
    chart = c3.generate({
        data: {
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Zone'],
                height: 130,
                show: true,
            },
            y: {
                show: true,
                label: {
                    text: 'Vacant Seats Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });

    $scope.SpaceAvalChart = function (AvlData) {
        $http({
            url: UtilityService.path + '/api/SpaceAvailabilityReport/GetSpaceAvailabilityChartData',
            method: 'POST',
            data: AvlData
        }).success(function (result) {
            // chart.unload();
            chart.unload({
                ids: ['AvalGraph']
            });
            chart.load({ columns: result });
            setTimeout(function () {
                $("#AvalGraph").append(chart.element);
            }, 700);
        });
    }
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    blockUI.stop();
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" },{ title: "Zone", key: "ZN_NAME" },{ title: "State", key: "STE_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Space Type", key: "SPC_TYPE" }, { title: "Space Sub Type", key: "SPACE_SUB_TYPE" }, { title: "Total Seats", key: "TOTAL_SEATS_COUNT" }, { title: "Vacant Seats", key: "VACANT_SEATS" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("SpaceAvailabilityReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "SpaceAvailabilityReport.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    document.addEventListener("DOMContentLoaded", function () {
        var eGridDiv = document.getElementById('myGrid');
        new agGrid.Grid(eGridDiv, gridOptions);
    });




    $scope.GenReport = function (Avldata, Type) {
        progress(0, 'Loading...', true);
        Avldata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {

          
            if (Avldata.Type == "pdf") {
                $scope.ActionStatus = 1;
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $scope.ActionStatus = 0;
            $http({
                url: UtilityService.path + '/api/SpaceAvailabilityReport/GetAvailReport',
                method: 'POST',
                data: Avldata,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceAvailabilityReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {
            });
        };
    }


    $scope.GenReportVacant = function (Avldata, Type) {
        progress(0, 'Loading...', true);
        $scope.SelValue.Type = Type;
        $http({
            url: UtilityService.path + '/api/SpaceAvailabilityReport/GetVacantSeatReport',
            method: 'POST',
            data: $scope.SelValue,
            responseType: 'arraybuffer'

        }).success(function (data, status, headers, config) {
            var file = new Blob([data], {
                type: 'application/' + Type
            });
            var fileURL = URL.createObjectURL(file);
            $("#reportcontainer").attr("src", fileURL);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = 'SpaceAvailabilityReport.' + Type;
            document.body.appendChild(a);
            a.click();
            progress(0, '', false);
        }).error(function (data, status, headers, config) {
        });
    };

    //$scope.selVal = "LASTMONTH";
    //$scope.rptDateRanges = function () {
    //    switch ($scope.selVal) {
    //        case 'TODAY':
    //            $scope.SpaceAvailability.FromDate = moment().format('DD-MMM-YYYY');
    //            $scope.SpaceAvailability.ToDate = moment().format('DD-MMM-YYYY');
    //            break;
    //        case 'YESTERDAY':
    //            $scope.SpaceAvailability.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
    //            $scope.SpaceAvailability.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
    //            break;
    //        case '7':
    //            $scope.SpaceAvailability.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
    //            $scope.SpaceAvailability.ToDate = moment().format('DD-MMM-YYYY');
    //            break;
    //        case '30':
    //            $scope.SpaceAvailability.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
    //            $scope.SpaceAvailability.ToDate = moment().format('DD-MMM-YYYY');
    //            break;
    //        case 'THISMONTH':
    //            $scope.SpaceAvailability.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
    //            $scope.SpaceAvailability.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
    //            break;
    //        case 'LASTMONTH':
    //            $scope.SpaceAvailability.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
    //            $scope.SpaceAvailability.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
    //            break;

    //    }
    //}
    $timeout($scope.LoadData, 500);

    
});