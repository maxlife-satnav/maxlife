﻿app.service("CustomizedReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CustomizedReport/GetCustomizedDetails', Customized)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('CustomizedReportController', function ($scope, $q, $http, CustomizedReportService, UtilityService, $timeout, $filter, blockUI) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.statuslst = [];
    blockUI.start();
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.Cols[9].COL = $scope.BsmDet.Parent;
            $scope.Cols[10].COL = $scope.BsmDet.Child;
            $scope.statuslst = [
                { Value: "All", Name: "All" },
                { Value: "1", Name: "Vacant" },
                { Value: "1002", Name: "Occupied To " + $scope.BsmDet.Parent },
                { Value: "1003", Name: "Occupied By " + $scope.BsmDet.Child },
                { Value: "1004", Name: "Occupied By Employee" }
            ];

        }
    });

    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            progress(0, 'Loading...', true);
            if (response.data != null) {
                $scope.Country = response.data;
            }
            UtilityService.getZone(2).then(function (Znresponse) {
                if (Znresponse.data != null) {

                    $scope.Zone = Znresponse.data;
                }
                UtilityService.getState(2).then(function (Stresponse) {
                    if (Stresponse.data != null) {

                        $scope.State = Stresponse.data;
                    }
                    UtilityService.getCities(2).then(function (response) {
                        if (response.data != null) {
                            $scope.City = response.data;
                        }
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                            }
                            UtilityService.getTowers(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Towers = response.data;
                                }

                                UtilityService.getFloors(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Floors = response.data;
                                    }
                                });
                                progress(0, 'Loading...', false);
                            });
                        });

                    });
                });
            });
           
        });
    }
    //country
    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.cnySelectNone = function () {
        $scope.Customized.Country = [];
        $scope.getCitiesbyCny();
    }

    //zone
    $scope.ZoneChanged = function () {
        UtilityService.getStateByZone($scope.Customized.Zone, 1).then(function (response) {
            if (response.data != null)
                $scope.State = response.data;
            else
                $scope.State = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;

                $scope.Customized.Country.push(cny);
            }
        });
    }
    $scope.ZoneChangeAll = function () {
        $scope.Customized.Zone = $scope.Zone;
        $scope.ZoneChanged();
    }
    $scope.ZoneSelectNone = function () {
        $scope.Customized.Zone = [];
        $scope.ZoneChanged();
    }

    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.Customized.State, 1).then(function (response) {
            if (response.data != null)
                $scope.City = response.data;
            else
                $scope.City = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.State, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country.push(cny);
            }
        });
        angular.forEach($scope.State, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.Customized.Zone.push(zn);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.Customized.State = $scope.State;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.Customized.State = [];
        $scope.SteChanged();
    }

    //city
    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.Customized.Zone[0] = zn;
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.Customized.State[0] = ste;
            }
        });
    }
    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.ctySelectNone = function () {
        $scope.Customized.City = [];
        $scope.getLocationsByCity();
    }

    //location
    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.Customized.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.Customized.Zone[0] = zn;
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.Customized.State[0] = ste;
            }
        });
    }
    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }
    $scope.lcmSelectNone = function () {
        $scope.Customized.Locations = [];
        $scope.getTowerByLocation();
    }

    //tower
    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.Customized.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            Console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });
        angular.forEach($scope.Towers, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.Customized.Zone[0] = zn;
            }
        });
        angular.forEach($scope.Towers, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.Customized.State[0] = ste;
            }
        });
    }
    $scope.twrSelectAll = function () {
        $scope.Customized.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }
    $scope.twrSelectNone = function () {
        $scope.Customized.Towers = [];
        $scope.getFloorByTower();
    }

    //floor
    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.Floors, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.Customized.Zone[0] = zn;
            }
        });
        angular.forEach($scope.Floors, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.Customized.State[0] = ste;
            }
        });
        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
         
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.Customized.Towers[0] = twr;
            }
        });
      
        

    }
    $scope.floorChangeAll = function () {
        $scope.Customized.Floors = $scope.Floors;
        $scope.FloorChange();
    }
    $scope.FloorSelectNone = function () {
        $scope.Customized.Floors = [];
        $scope.FloorChange();
    }

    $scope.Cols = [
        { COL: "Country", value: "COUNTRY", ticked: false },
        { COL: "Zone", value: "ZONE", ticked: false },
        { COL: "State", value: "STATE", ticked: false },
        { COL: "City", value: "CITY", ticked: false },
        { COL: "Location", value: "LOCATION", ticked: false },
        { COL: "TOWER", value: "TOWER", ticked: false },
        { COL: "FLOOR", value: "FLOOR", ticked: false },
        { COL: "Space", value: "SPACE", ticked: false },
        { COL: "Area", value: "AREA", ticked: false },
        //{ COL: "Parent Entity", value: "PE_CODE", ticked: false },
        //{ COL: "Child Entity", value: "CHE_CODE", ticked: false },
        { COL: $scope.BsmDet.Parent, value: "VERTICAL", ticked: false },
        { COL: $scope.BsmDet.Child, value: "COSTCENTER", ticked: false },
        { COL: "Employee Id", value: "EMP_ID", ticked: false },
        { COL: "Employee Name", value: "EMP_NAME", ticked: false },
        { COL: "Employee Mail", value: "MAIL", ticked: false },
        { COL: "Reporting To Id", value: "REPORTING", ticked: false },
        { COL: "Reporting To Name", value: "REPORTING_NAME", ticked: false },       
        { COL: "Employee Type", value: "AUR_TYPE", ticked: false },
        { COL: "Grade", value: "AUR_GRADE", ticked: false },
        { COL: "Status", value: "STATUS", ticked: false },
        { COL: "Seating Capacity", value: "STATUS", ticked: false },
    ];

    $scope.columnDefs = [
             { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', width: 80, suppressMenu: true },
             { headerName: "Zone", field: "ZONE", cellClass: 'grid-align', width: 100 },
             { headerName: "State", field: "STATE", cellClass: 'grid-align', width: 150 },
             { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100 },
             { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 150 },
             { headerName: "Tower", field: "TOWER", cellClass: 'grid-align', width: 100 },
             { headerName: "Floor", field: "FLOOR", cellClass: 'grid-align', width: 100 },
             { headerName: "Space", field: "SPACE", cellClass: 'grid-align', width: 200, suppressMenu: true, },
             { headerName: "Area", field: "AREA", cellClass: 'grid-align', width: 60, suppressMenu: true, },
             //{ headerName: "Parent Entity", field: "PE_CODE", cellClass: 'grid-align', width: 60, suppressMenu: true, },
             //{ headerName: "Child Entity", field: "CHE_CODE", cellClass: 'grid-align', width: 60, suppressMenu: true, },
             { headerName: "", field: "VERTICAL", cellClass: 'grid-align', width: 150 },
             { headerName: "", field: "COSTCENTER", cellClass: 'grid-align', width: 200 },
             { headerName: "Employee Id", field: "EMP_ID", cellClass: 'grid-align', width: 100, suppressMenu: true, },
             { headerName: "Employee Name", field: "EMP_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
             { headerName: "Employee Mail", field: "MAIL", cellClass: 'grid-align', width: 150, suppressMenu: true, },
             { headerName: "Reporting To Id", field: "REPORTING", cellClass: 'grid-align', width: 100 },
             { headerName: "Reporting To Name", field: "REPORTING_NAME", cellClass: 'grid-align', width: 150 },            
             { headerName: "Employee Type", field: "AUR_TYPE", cellClass: 'grid-align', width: 150, suppressMenu: true, },
             { headerName: "Grade", field: "AUR_GRADE", cellClass: 'grid-align', width: 150, suppressMenu: true, },
             { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 150, suppressMenu: true, },
             { headerName: "Seating Capacity", field: "SPC_SEATING_CAPACITY", cellClass: 'grid-align', width: 150, suppressMenu: true, },
    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.AREA += parseFloat((data.AREA).toFixed(2));
        });
        return sums;
    }
    $scope.LoadData = function () {
        var params = {
            flrlst: $scope.Customized.Floors,
            Request_Type: $scope.Customized.Request_Type
        };
        CustomizedReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
         
            if ($scope.gridata == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.columnApi.getColumn("VERTICAL").colDef.headerName = $scope.BsmDet.Parent;
                $scope.gridOptions.columnApi.getColumn("COSTCENTER").colDef.headerName = $scope.BsmDet.Child;
                $scope.gridOptions.api.refreshHeader();
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });

    }, function (error) {
        console.log(error);
    }
    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    blockUI.stop();
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "COUNTRY" },
            { title: "Zone", key: "ZONE" }, { title: "State", key: "STATE" }, { title: "City", key: "CITY" },
            { title: "Location", key: "LOCATION" }, { title: "Tower", key: "TOWER" }, { title: "Floor", key: "FLOOR" }, { title: "Space", key: "SPACE" }, { title: "Area", key: "AREA" },
            { title: "BU", key: "VERTICAL" }, { title: "Function", key: "COSTCENTER" }, { title: "Employee ID", key: "EMP_ID" }, { title: "Employee Name", key: "EMP_NAME" },
            { title: "Employee Email", key: "MAIL" }, { title: "Reporting To", key: "REPORTING" }, { title: "Reporting To Name", key: "REPORTING_NAME" }, { title: "Employee Type", key: "AUR_TYPE" }
        , { title: "Grade", key: "AUR_GRADE" }, { title: "Status", key: "STATUS" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("CustomizationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "CustomizationReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Customized.Type = Type;
        Customized.flrlst = Customized.Floors;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/CustomizedReport/GetCustomizedData',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'CustomizedReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    //$timeout($scope.LoadData, 500);
    $scope.Pageload();
});
