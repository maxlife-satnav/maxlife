﻿app.service("SpaceOccupancyReportService", function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    this.GetGriddata = function (SpaceOccupancy) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceOccupancyReport/GetOccupGrid', SpaceOccupancy)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('SpaceOccupancyReportController', function ($scope, $q, $http, SpaceOccupancyReportService, UtilityService, $timeout, blockUI) {
    $scope.SpaceOccupancy = {};
    $scope.Type = [];
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;
    blockUI.start();
    $scope.columnDefs = [

               { headerName: "Space Id", field: "SPC_ID", cellClass: 'grid-align', width: 200, suppressMenu: true },
               { headerName: "Space Type", field: "SPACE_TYPE", cellClass: 'grid-align', width: 150 },
               { headerName: "Space Sub Type", field: "SPACE_SUB_TYPE", cellClass: 'grid-align', width: 150 },
               //{ headerName: "Entity", field: "ENTITY", cellClass: 'grid-align', width: 110, suppressMenu: true },
               //{ headerName: "Child Entity", field: "CHILD_ENTITY", cellClass: 'grid-align', width: 110, suppressMenu: true },
               { headerName: "Business Unit", field: "VER_NAME", cellClass: 'grid-align', width: 150 },
               { headerName: "Function", field: "DEP_NAME", cellClass: 'grid-align', width: 150 },
               { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align', width: 110, suppressMenu: true },
               { headerName: "Zone", field: "ZN_NAME", cellClass: 'grid-align', width: 110, },
               { headerName: "State", field: "STE_NAME", cellClass: 'grid-align', width: 110, },
               { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110, },
               { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 160 },
               { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 110, },
               { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110 },
               { headerName: "Shift", field: "SHIFT", cellClass: 'grid-align', width: 200, suppressMenu: true },
               { headerName: "Employee Id", field: "AUR_ID", cellClass: 'grid-align', width: 200 },
               { headerName: "Employee Name", field: "EMP_ID", cellClass: 'grid-align', width: 200 },
               { headerName: "Employee Type", field: "AUR_TYPE", cellClass: 'grid-align', width: 200 },

                { headerName: "Gender", field: "AUR_GENDER", cellClass: 'grid-align', width: 200 },
                { headerName: "Employee Band", field: "AUR_GRADE", cellClass: 'grid-align', width: 200 },
                { headerName: "Designation", field: "DSN_AMT_TITLE", cellClass: 'grid-align', width: 200 },
                { headerName: "Manager Name/ID", field: "AUR_REPORTING_TO", cellClass: 'grid-align', width: 200 },
                //{ headerName: "Request Type", field: "REQUEST_TYPE", cellClass: 'grid-align', width: 150, suppressMenu: true }
    ];

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.gridOptions.api.refreshHeader();
        }
    });

    $scope.LoadData = function () {
        var params = {
            FromDate: $scope.SpaceOccupancy.FromDate,
            ToDate: $scope.SpaceOccupancy.ToDate,
        };
        var fromdate = moment($scope.SpaceOccupancy.FromDate);
        var todate = moment($scope.SpaceOccupancy.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {

            $scope.GridVisiblity = true;
            SpaceOccupancyReportService.GetGriddata(params).then(function (data) {
                progress(0, 'Loading...', true);
                $scope.gridata = data;
                if ($scope.gridata == null) {
                    $scope.gridOptions.api.setRowData([]);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 600);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                    $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
                    $scope.gridOptions.api.refreshHeader();
                    console.log($scope.gridOptions.columnApi.getColumn("VER_NAME"));

                    $scope.gridOptions.showToolPanel = true;
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 600);
                }
                $scope.OccupChartDetails(params);

            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.ColumnNames = [];    $scope.OccupDataLoad = function () {
        progress(0, 'Loading...', true);
        $scope.Pageload = {
            FromDate: $scope.SpaceOccupancy.FromDate,
            ToDate: $scope.SpaceOccupancy.ToDate,
        };
        SpaceOccupancyReportService.GetGriddata($scope.Pageload).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
                $scope.gridOptions.api.showToolPanel(true);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.gridOptions.api.refreshHeader();
                progress(0, '', false);
            }
            $scope.OccupChartDetails($scope.Pageload);
        }, function (error) {
            console.log(error);
        });
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    chart = c3.generate({
        data: {
            columns: [],
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Zone'],
                height: 130,
                show: true,
            },
            y: {
                show: true,
                label: {
                    text: 'Occupancy Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    $scope.OccupChartDetails = function (spcData) {
        $http({
            url: UtilityService.path + '/api/SpaceOccupancyReport/GetDetailsCount',
            method: 'POST',
            data: spcData
        }).success(function (response) {
            //chart.unload();            
            chart.unload({
                ids: ['OccupGraph']
            });
            chart.load({ columns: response });
            setTimeout(function () {
                $("#OccupGraph").append(chart.element);
            }, 700);
        });
    }
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });
    blockUI.stop();
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Space Id", key: "SPC_ID" }, { title: "Space Type", key: "SPACE_TYPE" }, { title: "Space sub Type", key: "SPACE_SUB_TYPE" }, { title: "BU", key: "VER_NAME" }, { title: "Function", key: "DEP_NAME" }, { title: "Country", key: "CNY_NAME" }, { title: "Zone", key: "ZN_NAME" }, { title: "State", key: "STE_NAME" },
            { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" },
             { title: "Employee", key: "EMP_ID" }, { title: "Employee Type", key: "AUR_ID" },
            { title: "Manager Name", key: "AUR_REPORTING_TO" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("SpaceOccupancyReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);

    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SpaceOccupancyReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Occupdata, Type) {
        progress(0, 'Loading...', true);
        Occupdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Occupdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpaceOccupancyReport/GetSpaceOccupancyReportdata',
                method: 'POST',
                data: Occupdata,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceOccupancyReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {
            });
        };
    }
    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.SpaceOccupancy.FromDate = moment().format('MM/DD/YYYY');
                $scope.SpaceOccupancy.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.SpaceOccupancy.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.SpaceOccupancy.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.SpaceOccupancy.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.SpaceOccupancy.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.SpaceOccupancy.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.SpaceOccupancy.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.SpaceOccupancy.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.SpaceOccupancy.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.SpaceOccupancy.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.SpaceOccupancy.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
    $timeout($scope.OccupDataLoad, 500);
});