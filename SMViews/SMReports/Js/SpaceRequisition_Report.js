﻿app.service("SpaceRequisitionReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (SpaceRequisition) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisitionReport/GetGrid', SpaceRequisition)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisitionReport/GetDetailsOnSelection/', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('SpaceRequisitionReportController', function ($scope, $q, $http, SpaceRequisitionReportService, UtilityService, $timeout,blockUI) {
    $scope.SpaceRequisition = {};
    $scope.GridVisiblity = true;
    $scope.Spcdata = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.REQUEST_TYPE_For_PopUP = "";
    blockUI.start();
    $scope.columnDefs = [
              {
              headerName: "Requisition Id", field: "REQ_ID", width: 165, cellClass: 'grid-align', template: '<a ng-click="ShowPopup(data)">{{data.REQ_ID}}</a>',  filter: 'text',pinned:'left', suppressMenu: true},
              { headerName: "Requisition Date", field: "REQ_DATE", template: '<span>{{data.REQ_DATE | date:"dd MMM, yyyy"}}</span>', width: 110, cellClass: 'grid-align', suppressMenu: true, },
              { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Zone", field: "ZN_NAME", cellClass: 'grid-align', width: 100,  },
              { headerName: "State", field: "STE_NAME", cellClass: 'grid-align', width: 100, },
              { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110,  },
              { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 150 },
              { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 100,  },
              { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
              { headerName: "Business Unit", field: "VER_NAME", cellClass: 'grid-align', width: 150 },
              { headerName: "Total Count", field: "REQ_COUNT", cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 200 },
              { headerName: "Request Type", field: "REQUEST_TYPE", cellClass: 'grid-align', width: 150, suppressMenu: true, }, ];

    $scope.LoadData = function () {
        var params = {
            FromDate: $scope.SpaceRequisition.FromDate,
            ToDate: $scope.SpaceRequisition.ToDate,
            Request_Type: $scope.SpaceRequisition.Request_Type
        };
        var fromdate = moment($scope.SpaceRequisition.FromDate);
        var todate = moment($scope.SpaceRequisition.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            SpaceRequisitionReportService.GetGriddata(params).then(function (data) {
                $scope.gridata = data;
                if ($scope.gridata == null) {

                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                $scope.SpaceChartDetails(params);
                chart.unload();
                chart.load({ columns: data });
                setTimeout(function () {
                    $("#SpcGraph").append(chart.element);
                }, 700);
                //setTimeout(function () {
                //    progress(0, 'Loading...', false);
                //}, 600);
                progress(0, 'Loading...', false);
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.SpcdataLoad = function () {
        progress(0, 'Loading...', true);
        $scope.Pageload = {
            FromDate: $scope.SpaceRequisition.FromDate,
            ToDate: $scope.SpaceRequisition.ToDate,
            Request_Type: $scope.SpaceRequisition.Request_Type
        };
        SpaceRequisitionReportService.GetGriddata($scope.Pageload).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                progress(0, 'Loading...', true);             
                $scope.gridOptions.api.setRowData($scope.gridata);
                var params = {
                    FromDate: $scope.SpaceRequisition.FromDate,
                    ToDate: $scope.SpaceRequisition.ToDate,
                    Request_Type: $scope.SpaceRequisition.Request_Type
                };
                if ($scope.SpaceRequisition.Request_Type == "ALL") {
                    //if ($scope.gridata.REQUEST_TYPE == "Vertical Requisition") {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
                    $scope.SpaceChartDetails(params);
                }
                    //}
                else if ($scope.gridata.REQUEST_TYPE == "Space Extension") {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
                    $scope.SpaceChartDetails(params);
                }
                else if ($scope.gridata.REQUEST_TYPE == "Space Requisition") {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
                    $scope.SpaceChartDetails(params);
                }
                }
                progress(0, '', false);
            
        }, function (error) {
            console.log(error);
        });
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    var PopDefs = [
          { headerName: "Space Id", field: "SPACE_ID", width: 190, cellClass: 'grid-align', suppressMenu: true, },
          { headerName: "Employee Name", field: "REQ_BY", width: 190, cellClass: 'grid-align' },
          { headerName: "Space Type", field: "SPACE_TYPE", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },
          { headerName: "Space Sub Type", field: "SPACE_SUB_TYPE", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },
          { headerName: "Shift Type", field: "SHIFT", width: 190, cellClass: 'grid-align', hide: false },
          { headerName: "Seat Extension Date", field: "EXT_DT", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },
          { headerName: "Designation", field: "DESIGNATION", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, }];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        onReady: function () {
            $scope.PopOptions.api.sizeColumnsToFit()
        }
    }
    $scope.ShowPopup = function (data) {
        $scope.SelValue = data;
        $scope.REQUEST_TYPE_For_PopUP = data.REQUEST_TYPE;
        $scope.CurrentProfile = [];
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        SpaceRequisitionReportService.GetDetailsOnSelection($scope.SelValue).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.popdata = response.data;
            $scope.PopOptions.api.setRowData($scope.popdata);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 500);

            if ($scope.REQUEST_TYPE_For_PopUP == "Vertical Requisition") {
                $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
            }
            else if ($scope.REQUEST_TYPE_For_PopUP == "Space Extension") {

                $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', true);
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
            }
            else if ($scope.REQUEST_TYPE_For_PopUP == "Space Requisition") {
                if ($scope.SelValue.PREF_CODE == 1040) {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', true);
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', true);
                }
                else if ($scope.SelValue.PREF_CODE == 1039) {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', true);
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
                }
                else if ($scope.SelValue.PREF_CODE == 1041) {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);

                }
            }
        });
    });

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();
    var chart;
    chart = c3.generate({
        data: {
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
            },
            y: {
                show: true,
                label: {
                    text: 'Requisition Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });
    $scope.SpaceChartDetails = function (spcData) {
        
        $http({
            url: UtilityService.path + '/api/SpaceRequisitionReport/GetSpaceChartData',
            method: 'POST',
            data: spcData
        }).success(function (result) {
            console.log(result);
            //chart.unload();
            chart.unload({
                ids: ['SpcGraph']
            });
            chart.load({ columns: result });
        });
        setTimeout(function () {
            $("#SpcGraph").append(chart.element);
        }, 700);
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });
    blockUI.stop();
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "Zone", key: "ZN_NAME" },
            { title: "State", key: "STE_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" },
            { title: "Floor", key: "FLR_NAME" }, { title: "BU", key: "VER_NAME" }, { title: "Total Count", key: "REQ_COUNT" }, { title: "Status", key: "STATUS" },
            { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log($scope.gridOptions.api.isAnyFilterPresent($scope.gridOptions.api.getModel()));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("SpaceRequisitionReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SpaceRequisitionReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (spcdata, Type) {
        progress(0, 'Loading...', true);
        spcdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (spcdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpaceRequisitionReport/GetSpaceRequisitionReportdata',
                method: 'POST',
                data: spcdata,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                //var popupwin = window.open(fileURL, "SpaceRequisitionReport", "toolbar=no,width=500,height=500");
                //setTimeout(function () {
                //    popupwin.focus();
                //}, 500);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceRequisitionReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.SpaceRequisition.FromDate = moment().format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.SpaceRequisition.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.SpaceRequisition.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.SpaceRequisition.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.SpaceRequisition.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.SpaceRequisition.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
    $timeout($scope.SpcdataLoad, 500);
    $scope.SpaceRequisition.Request_Type = "ALL";
});