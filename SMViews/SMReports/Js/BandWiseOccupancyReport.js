﻿
app.service("BandOccupancyService", function ($http, $q, UtilityService) {
    //For Grid
    this.getRptGrid = function (BandData) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BandWiseOccupancyReport/GetRptGrid', BandData)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetGriddata = function (BandData) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BandWiseOccupancyReport/GetGriddata', BandData)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


});

app.filter('total', function () {
    return function (input, property) {
        var i = input instanceof Array ? input.length : 0;
        if (typeof property === 'undefined' || i === 0) {
            return i;
        } else if (isNaN(input[0][property].CNT)) {
            throw 'filter total can count only numeric values';
        } else {
            var total = 0;
            while (i--) {
                total += input[i][property].CNT;
            }
            return total;
        }
    };
}).filter('grandtotal', function () {
    return function (input, property) {
        var i = input instanceof Array ? input.length : 0;
        var grandtotal = 0;
        if (i != 0) {
            console.log(input);
            for (var y = i - 1; y >= 0; y--) {
                for (var j = i - 1; j >= 0; j--) {
                    grandtotal += input[y].Value[j].Value.CNT;
                }
            }
            return grandtotal;
        }
        else return grandtotal;
    };

}).filter('GreenPer', function () {
    return function (input, property) {
        var i = input instanceof Array ? input.length : 0;
        var y = i;
        if (typeof property === 'undefined' || i === 0) {
            return i;
        } else if (isNaN(input[0][property].CNT)) {
            throw 'filter total can count only numeric values';
        } else {
            var total = 0;
            while (i--) {
                total += input[i][property].CNT;
            }
            var green = 0.0;
            while (y--) {
                if (input[y][property].EMP_BAND == input[y][property].SPC_BAND && total > 0) {
                    if (input[y][property].CNT == total)
                        return "100";

                    green = (input[y][property].CNT / total * 100).toPrecision(2);
                    return green;
                }
            }
            return green;
        }
    };
}).filter('RedPer', function () {
    return function (input, property) {
        var i = input instanceof Array ? input.length : 0;
        var y = i;
        if (typeof property === 'undefined' || i === 0) {
            return i;
        } else if (isNaN(input[0][property].CNT)) {
            throw 'filter total can count only numeric values';
        } else {
            var total = 0;
            while (i--) {
                total += input[i][property].CNT;
            }
            var red = 0.0;
            var redtot = 0;
            while (y--) {
                if (input[y][property].EMP_BAND > input[y][property].SPC_BAND && total > 0) {
                    if (input[y][property].CNT == total)
                        return "100";
                    redtot += input[y][property].CNT;
                    red = (redtot / total * 100).toPrecision(2)
                    return red;
                }
            }
            return red;
        }
    };
}).filter('BluePer', function () {
    return function (input, property) {
        var i = input instanceof Array ? input.length : 0;
        var y = i;
        if (typeof property === 'undefined' || i === 0) {
            return i;
        } else if (isNaN(input[0][property].CNT)) {
            throw 'filter total can count only numeric values';
        } else {
            var total = 0;
            while (i--) {
                total += input[i][property].CNT;
            }
            var blue = 0.0;
            var bluetot = 0;
            while (y--) {
                if (input[y][property].EMP_BAND < input[y][property].SPC_BAND && total > 0) {
                    if (input[y][property].CNT == total)
                        return "100";
                    bluetot += input[y][property].CNT;
                    blue = (bluetot / total * 100).toPrecision(2)
                    return blue;
                }
            }
            return blue;
        }
    };

}).filter('totalCol', function () {
    return function (input, index) {
        var tot = 0;
        var i = input instanceof Array ? input.length : 0;

        for (var j = 0; j < i; j++) {

            tot += input[j].Value[index].Value.CNT;
        }
        //grandtotal = grandtotal + tot;
        return tot;
    }
});

app.controller('BandOccupancycontroller', function ($scope, $q, $http, $timeout, BandOccupancyService, UtilityService, $filter, blockUI) {
    $scope.SearchSpace = {};

    $scope.SearchSpace.Tower = [];
    $scope.SearchSpace.Floor = [];

    //For Timer view
    $("#Graphicaldiv").fadeOut();
    $("#btnexcel").fadeOut();

    UtilityService.getTowers(3).then(function (response) {
        if (response.data != null) {
            $scope.Tower = response.data;

        }
        UtilityService.getFloors(3).then(function (response) {
            if (response.data != null) {
                $scope.Floor = response.data;
            }
        });

    });


    $scope.getClass = function (strValue) {
        if (strValue.EMP_BAND > strValue.SPC_BAND && strValue.CNT != 0)
            return "bgRed";
        else if (strValue.EMP_BAND < strValue.SPC_BAND && strValue.CNT != 0)
            return "bgBlue";
        else if (strValue.EMP_BAND == strValue.SPC_BAND && strValue.CNT != 0)
            return "bgGreen";
    }

    $scope.GetBandOccupancy = function (BandData) {
        BandOccupancyService.getRptGrid(BandData).then(function (response) {
            $scope.Bands = response.BANDS;
            $scope.BandOccupancy = response.BANDDET;
            $scope.Subtypes = response.SUBTYPEBAND;
        }, function (error) {
        });
    }


    $scope.LoadData = function (status) {

        var BandData = {
            flrlst: $scope.SearchSpace.Floor,
            mode: status
        };
        // progress(0, 'Loading...', true);
        BandOccupancyService.GetGriddata(BandData).then(function (data) {
            console.log(data);
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            $scope.GetBandOccupancy(BandData);
            //setTimeout(function () {
            //    progress(0, 'Loading...', false);
            //}, 500);
        }, function (error) {
            console.log(error);
        });

    }

    $scope.columnDefs = [
            { headerName: "Space Id", field: "SPC_ID", width: 190, cellClass: 'grid-align', width: 110, suppressMenu: true, },
            { headerName: "Space Type", field: "SUB_TYPE", cellClass: 'grid-align' },
            { headerName: "Space Band", field: "SPC_BAND", cellClass: 'grid-align' },
            { headerName: "Employee Id", field: "EMP_ID", cellClass: 'grid-align' },
            { headerName: "Employee Name", field: "AUR_KNOWN_AS", width: 190, cellClass: 'grid-align' },
            { headerName: "Employee Band", field: "EMP_BAND", cellClass: 'grid-align' },
            { headerName: "Employee Type", field: "AUR_TYPE", cellClass: 'grid-align' },
            { headerName: "Business Unit", field: "VER_NAME", cellClass: 'grid-align' },
            { headerName: "Function", field: "Cost_Center_Name", cellClass: 'grid-align' },
            { headerName: "Staus", field: "STATUS", cellClass: 'grid-align' },
    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    //// Tower Events
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SearchSpace.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

    }
    $scope.TwrSelectNone = function () {
        $scope.SearchSpace.Tower = [];
        $scope.TwrChanged();
    }
    $scope.TwrChangeAll = function () {
        $scope.SearchSpace.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    //// floor events   
    $scope.FlrChanged = function () {


        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SearchSpace.Tower.push(twr);
            }
        });
    }
    $scope.FlrSelectNone = function () {
        $scope.SearchSpace.Floor = [];
        $scope.FlrChanged();
    }
    $scope.FlrChangeAll = function () {
        $scope.SearchSpace.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "BandWiseOccupancyReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (spcdata, Type) {
        progress(0, 'Loading...', true);
        spcdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (spcdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/BandWiseOccupancyReport/GetSpaceAllocationReport',
                method: 'POST',
                data: spcdata,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'BandWiseOccupancyReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }


    $timeout($scope.LoadData(1), 1500);
   
});

$('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
        $("#Graphicaldiv").fadeOut(function () {
            $("#divband").fadeIn();
            $("#btnprint").fadeIn();
            $("#btnexcel").fadeOut();
            $("#Graphicaldiv").fadeOut();
        });
    }
    else {
        $("#divband").fadeOut(function () {
            $("#Graphicaldiv").fadeIn();
            $("#btnprint").fadeOut();
            $("#btnexcel").fadeIn();
        });
    }
});


