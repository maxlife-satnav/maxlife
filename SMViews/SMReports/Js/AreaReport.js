﻿app.service("SpaceAreaReport", function ($http, $q, UtilityService) {
    this.GetGridData = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAreaReport/GetAreaReportBindGrid')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

});

app.controller('SpaceAreaReportController', function ($scope, $q, SpaceAreaReport, UtilityService, $timeout, $http, blockUI) {
    $scope.Viewstatus = 0;
    $scope.rptArea = {};
    //$scope.AreaReport = {};
    $scope.GridVisiblity = true;
    //$scope.Spcdata = {};
    //$scope.CurrentProfile = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    blockUI.start();
    $scope.getAreaReportBindGrid = function () {
        progress(0, 'Loading...', true);
        SpaceAreaReport.GetGridData().then(function (response) {
            $scope.RptByUsrGrid = true;
            $scope.gridata = response.data;

            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);

                progress(0, 'Loading...', false);

                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData($scope.gridata);

                progress(0, 'Loading...', false);
            }
            $scope.AreaChartDetais();
        }, function (error) {
            console.log(error);
        });
    }

    var columnDefs = [
            { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', filter: 'set', suppressMenu: true },
            { headerName: "Zone", field: "ZN_NAME", width: 100, cellClass: 'grid-align' },
            { headerName: "State", field: "STE_NAME", width: 100, cellClass: 'grid-align' },
            { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align' },
            { headerName: "Location", field: "LCM_NAME", width: 100, cellClass: 'grid-align', },
            { headerName: "Tower", field: "TWR_NAME", width: 150, cellClass: 'grid-align', },
            { headerName: "Floor", field: "FLR_NAME", width: 150, cellClass: 'grid-align', suppressMenu: true },
       
            { headerName: "Workstation Area", field: "WORK_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Circulation Area", field: "FLR_CIRCULATION_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Support Area", field: "SUPPORT_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Utility Area", field: "UTILITY_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Wall+Pillar+Shaft+Stairs", field: "FLR_COMMON_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },

            { headerName: "Leasable Area", field: "FLR_LEASABLE_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Built Up Area", field: "FLR_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Carpet Area", field: "CARPET_AREA", width: 100, cellClass: 'grid-align', suppressMenu: true },

            { headerName: "Spaces", field: "TOTAL_SPACES", width: 100, cellClass: 'grid-align', suppressMenu: true }

    ];

    $scope.pageSize = '10';    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


    //$scope.getAreaReportBindGrid();
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })




    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Area in Sq.Ft',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });


    $scope.AreaChartDetais = function () {
        $http({
            url: UtilityService.path + '/api/SpaceAreaReport/GetAreaChartData',
            method: 'POST',
            data: ''
        }).
            success(function (result) {
                //chart.unload();
                chart.unload({
                    ids: ['SpcGraph']
                });
                chart.load({ columns: result });
                setTimeout(function () {
                    $("#SpcGraph").append(chart.element);
                }, 700);
            });


    }
    //$scope.TotalSpaceDetails = function (area) {
    //    $.ajax({
    //        url: UtilityService.path + '/api/SpaceAreaReport/GetAreaChartData',
    //        type: 'POST',
    //        data: area,
    //        success: function (result) {
    //            Linechart.unload();
    //            Linechart.load({ columns: result });
    //        }
    //    });
    //    setTimeout(function () {
    //        $("#TotalGraph").append(Linechart.element);
    //    }, 700);

    //}
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });
    blockUI.stop();
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "Zone", key: "ZN_NAME" }, { title: "State", key: "STE_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" },
            { title: "Workstation Area", key: "WORK_AREA" }, { title: "Circular Area", key: "FLR_CIRCULATION_AREA" }, { title: "Support Area", key: "SUPPORT_AREA" }, { title: "Utility Area", key: "UTILITY_AREA" }, { title: "Wall+Pillar+Shaft+Stairs", key: "FLR_COMMON_AREA" }, { title: "Leasable Area", key: "FLR_LEASABLE_AREA" },
            { title: "Built Up Area", key: "FLR_AREA" }, { title: "Carpet Area", key: "CARPET_AREA" }, , { title: "Spaces", key: "TOTAL_SPACES" }
        ];
        progress(0, 'Loading...', true);
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("AreaReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "AreaReport.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.rptArea.DocType = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpaceAreaReport/GetAreaReportdata',
                method: 'POST',
                data: $scope.rptArea,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'AreaReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $timeout($scope.getAreaReportBindGrid, 1000);

});

