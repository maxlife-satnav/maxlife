﻿app.service("SpaceAllocationReportService", function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    this.GetGriddata = function (SpaceAllocation) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocationReport/GetAllocGrid', SpaceAllocation)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('SpaceAllocationReportController', function ($scope, $q, $http, SpaceAllocationReportService, UtilityService, $timeout, $filter, blockUI) {
    $scope.SpaceAllocation = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.GridVisiblity = true;
    blockUI.start();
    $scope.columnDefs = [
            { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align', width: 110, suppressMenu: true, },
            { headerName: "Zone", field: "ZN_NAME", cellClass: 'grid-align', width: 110, },
            { headerName: "State", field: "STE_NAME", cellClass: 'grid-align', width: 110,  },
            { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110,  },
            { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 160 },
            { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 110, },
            { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true },
            //{ headerName: "Entity", field: "ENTITY", cellClass: 'grid-align', width: 110, suppressMenu: true },
            //{ headerName: "Child Entity", field: "CHILD_ENTITY", cellClass: 'grid-align', width: 110, suppressMenu: true },
            { headerName: "Business Unit", field: "VER_NAME", cellClass: 'grid-align', width: 110 },
            { headerName: "Function", field: "DEP_NAME", cellClass: 'grid-align', width: 110 },
            { headerName: "Shift", field: "SHIFT", cellClass: 'grid-align', width: 200, suppressMenu: true },
            { headerName: "Occupied By BU", field: "ALLOC_COUNT", cellClass: 'grid-align', width: 110, suppressMenu: true },
            { headerName: "Occupied By Employee/Function", field: "OCCUP_COUNT", cellClass: 'grid-align', width: 110, suppressMenu: true },
            { headerName: "Vacant Count", field: "VAC_COUNT", cellClass: 'grid-align', width: 110, suppressMenu: true },
            { headerName: "Request Type", field: "REQUEST_TYPE", cellClass: 'grid-align', width: 110, }];

  

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.gridOptions.api.refreshHeader();
        }
    });


    $scope.LoadData = function () {
        var params = {
            FromDate: $scope.SpaceAllocation.FromDate,
            ToDate: $scope.SpaceAllocation.ToDate,
        };
        var fromdate = moment($scope.SpaceAllocation.FromDate);
        var todate = moment($scope.SpaceAllocation.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            SpaceAllocationReportService.GetGriddata(params).then(function (data) {
                $scope.gridata = data;
                if ($scope.gridata == null) {
                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                $scope.AllocChartDetails(params);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 500);
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.ColumnNames = [];
    $scope.AllocDataLoad = function () {
       
        $scope.Pageload = {
            FromDate: $scope.SpaceAllocation.FromDate,
            ToDate: $scope.SpaceAllocation.ToDate,
        };
        SpaceAllocationReportService.GetGriddata($scope.Pageload).then(function (data) {
            progress(0, 'Loading...', true);
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 600);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 600);
            }
            $scope.AllocChartDetails($scope.Pageload);
        }, function (error) {
            console.log(error);
        });
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value)
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        groupHeaders: true,

        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        showToolPanel: true
    };
    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $scope.chart;
    $scope.chart = c3.generate({
        data: {
            x: 'LCM_NAME',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: 75,
                    multiline: false
                },
                height: 130,
                show: true,
            },
            y: {
                show: true,
                label: {
                    text: 'Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    $scope.AllocChartDetails = function (spcData) {
        $http({
            url: UtilityService.path + '/api/SpaceAllocationReport/GetDetailsCount',
            method: 'POST',
            data: spcData
        }).success(function (response) {
            console.log(response.data.Columnnames);
            var arr = [];
            arr.push(response.data.Columnnames);
            angular.forEach(response.data.Details, function (value, key) {
                arr.push(value);
            });

            $scope.chart.unload({
                ids: ['AllocGraph']
            });
            $scope.chart.load({ columns: arr });
           
            setTimeout(function () {
                $("#AllocGraph").append($scope.chart.element);
            }, 700);
        });
    }
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });
    blockUI.stop();
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "Zone", key: "ZN_NAME" }, { title: "State", key: "STE_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" },
            { title: "BU", key: "VER_NAME" }, { title: "Function", key: "DEP_NAME" }, { title: "Shift", key: "SHIFT" }, { title: "Occupied by BU", key: "ALLOC_COUNT" }, { title: "Occ by Emp/Fnc", key: "OCCUP_COUNT" },
            { title: "Vacant Count", key: "VAC_COUNT" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("SpaceAllocationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);

    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SpaceAllocationReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenReport = function (Allocdata, Type) {
        progress(0, 'Loading...', true);
        Allocdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Allocdata.Type == "pdf") {
                progress(0, 'Loading...', true);
                $scope.GenerateFilterPdf();
                progress(0, '', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GenerateFilterExcel();
                progress(0, '', false);
            }

        }
        else {
            $scope.DocTypeVisible = 0
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/SpaceAllocationReport/GetSpaceAllocationReport',
                method: 'POST',
                data: Allocdata,
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceAllocationReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {
            });
        }
    };
    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.SpaceAllocation.FromDate = moment().format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.SpaceAllocation.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.SpaceAllocation.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.SpaceAllocation.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.SpaceAllocation.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.SpaceAllocation.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
    $timeout($scope.AllocDataLoad, 500);
});