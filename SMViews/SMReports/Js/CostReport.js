﻿app.service("CostReportService", function ($http, $q, UtilityService) {
    this.GetGridData = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CostReport/GetCostReportBindGrid')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

});

app.controller('CostReportController', function ($scope, $q, CostReportService, UtilityService, $timeout, $http, $timeout, $filter, blockUI) {
    $scope.Viewstatus = 0;
    $scope.rptArea = {};
    $scope.GridVisiblity = true;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    blockUI.start();
    var columnDefs = [
         { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', filter: 'set', suppressMenu: true },
         { headerName: "Zone", field: "ZN_NAME", width: 100, cellClass: 'grid-align' },
         { headerName: "State", field: "STE_NAME", width: 100, cellClass: 'grid-align' },
         { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align' },
         { headerName: "Location", field: "LCM_NAME", width: 200, cellClass: 'grid-align' },
         { headerName: "Tower", field: "TWR_NAME", width: 100, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Floor", field: "FLR_NAME", width: 100, cellClass: 'grid-align' },
         //{ headerName: "Parent Entity", field: "PE_NAME", width: 100, cellClass: 'grid-align'},
         //{ headerName: "Child Entity", field: "CHE_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0 },
         { headerName: "Business Unit", field: "VER_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0 },
         { headerName: "Function", field: "DEP_NAME", width: 150, cellClass: 'grid-align', },
         { headerName: "Shift", field: "SHIFT", width: 150, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Space Type", field: "SPC_TYPE", width: 150, cellClass: 'grid-align' },
         { headerName: "Total No.Of Occupied Seats", field: "ALLOC_SEATS_COUNT", width: 200, cellClass: 'grid-align', suppressMenu: true, aggFunc: 'sum' },
         { headerName: "Unit Cost Rs", field: "UNITCOST", width: 130, cellClass: 'grid-align', suppressMenu: true},
         {
             headerName: "Total Business Unit WS Cost", field: "TOTAL_BUSS_UNITCOST", width: 200, cellClass: 'grid-align', pinned: 'right', suppressMenu: true, aggFunc: 'sum' //valueGetter: BusinessCost,
           //floatingCellRenderer: function (params) {
           //    return '<b>' + 'Grand Total:' + '&nbsp&nbsp  ' + '<i class="fa fa-inr "></i>' + '&nbsp' + GrandTotal(params) + '</b>'
           //}
         }
    ];

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.gridOptions.api.refreshHeader();
        }
    });
    //'<i class="fa fa-inr"></i>'    '&#2352' +  'background-color': 'white' 
    var sum = 0;
    var res = 0;

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        //getRowStyle: function (params) {
        //    if (params.node.floating) {
        //        return { 'font-weight': 'bold', color: '#366599', 'font-size': 24, 'text-align': 'left'  }
        //    }
        //},
        showToolPanel: true,
        //floatingBottomRowData: [],
        //onBeforeFilterChanged: function () {
        //    sum = 0;
        //    res = 0;
        //    if (angular.equals({}, $scope.gridOptions.api.getFilterModel())) {
        //        $scope.DocTypeVisible = 0;
               
        //    }

        //    else {
        //        $scope.DocTypeVisible = 1;
      

        //    }
        //}

    };

    function BusinessCost(params) {
        var a = parseInt(params.data.ALLOC_SEATS_COUNT);
        var b = parseFloat(params.data.UNITCOST);
        GrandTotal(params);
        return a * b;
    }

    function GrandTotal(params) {
        if (angular.equals({}, $scope.gridOptions.api.getFilterModel())) {
            sum = sum + parseFloat(params.data.TOTAL_BUSS_UNITCOST);
        }
        else {
            sum = sum + parseFloat(params.data.TOTAL_BUSS_UNITCOST);            
        }
       return sum;
    }

    function createData() {
        var result = [];
        //for (var i = 0; i < count; i++) {
        result.push({
            //CNY_NAME: prefix + ' Country ' + i,
            //CTY_NAME: prefix + ' City ' + i,
            //LCM_NAME: prefix + ' Location ' + i,
            //TWR_NAME: prefix + ' Tower ' + i,
            //FLR_NAME: prefix + ' Floor ' + i,
            //VER_NAME: prefix + ' Vertical ' + i,
            //DEP_NAME: prefix + ' Department ' + i,
            //SHIFT: prefix + ' Shift ' + i,
            //SPC_TYPE: prefix + ' Space Type ' + i,
            //ALLOC_SEATS_COUNT: prefix + ' Total No.Of Allocated Seeats ' + i,
            //UNITCOST: prefix + ' Unit Cost Rs ' + i,
            //UNITCOST:0,
            TOTAL_BUSS_UNITCOST: 0,

        });
        //}

        return result;
        console.log(result);
    }

    //$scope.pageSize = '10';


    function onFloatingBottomCount(footerRowsToFloat) {
        var count = Number(footerRowsToFloat);
        //var rows = createData(count, 'Bottom');
        var rows = createData();
        $scope.gridOptions.api.setFloatingBottomRowData(rows);
    }



    $scope.GetCostBindGrid = function () {
        // For creating the row column in the bottom line
        //onFloatingBottomCount(1);

        progress(0, 'Loading...', true);
        CostReportService.GetGridData().then(function (response) {

            $scope.RptByUsrGrid = true;

            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);

       
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData($scope.gridata);
          
                progress(0, 'Loading...', false);

            }

            //function bindLocChart() {
            //    $scope.LocationWiseCostDetails();
            //}
            //function bindSubCatGraph() {
            //    $scope.CostVerticalChartData();
            //}
            //$timeout(bindLocChart, 500);
            //$timeout(bindSubCatGraph, 1000);

            //$scope.GetFilterData();
            $scope.LocationWiseCostDetails();
            //$scope.CostVerticalChartData();
        }, function (error) {
            console.log(error);
        });
    }

    //$scope.GetCostBindGrid();

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
            sum = 0;
            res = 0;
        } else {
            $scope.DocTypeVisible = 0;
        }

    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })



    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();


    var chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Zone'],
                height: 130,
                show: true,
                label: {
                    text: '',
                    position: 'outer-center'
                }
            },
            y: {
                show: true,
                label: {
                    text: 'Total Cost',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });
    $scope.LocationWiseCostDetails = function () {
        $http({
            url: UtilityService.path + '/api/CostReport/GetCostChartData',
            method: 'POST',
            data: ''
        }).
            success(function (result) {
                //chart.unload();
                chart.unload({
                    ids: ['SpcGraph']
                });
                console.log(result);
                chart.load({ columns: result });
                setTimeout(function () {
                    $("#SpcGraph").append(chart.element);
                }, 700);
            });
    }

  
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });


    blockUI.stop();

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "Zone", key: "ZN_NAME" }, { title: "State", key: "STE_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" },
            { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "BU", key: "VER_NAME" }, { title: "Function", key: "DEP_NAME" }, { title: "Shift", key: "SHIFT" }, { title: "Space Type", key: "SPC_TYPE" },
        { title: "Total No.Of Occupied Seeats", key: "ALLOC_SEATS_COUNT" }, { title: "Unit Cost Rs", key: "UNITCOST" }, { title: "Total Business Unit WS Cost", key: "TOTAL_BUSS_UNITCOST" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("CostReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "CostReport.csv"
        };

        $scope.gridOptions.api.exportDataAsCsv(Filterparams);

        
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.rptArea.DocType = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: '../../../api/CostReport/GetCostReportdata',
                method: 'POST',
                data: $scope.rptArea,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'CostReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, 'Loading...', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $timeout($scope.GetCostBindGrid, 500);

});

