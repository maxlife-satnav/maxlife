﻿app.service("FinanceReportService", function ($http, $q, UtilityService) {

    this.GetOccupancy = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/FinanceReport/GetOccupancy')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    
});



app.controller('FinanceReportController', function ($scope, $q, $http, FinanceReportService, UtilityService, $timeout, $filter) {

    $scope.FinanceReport = {};

    function onFilterChanged(value) {
        $scope.Options.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible1 = 1
        }
        else { $scope.DocTypeVisible1 = 0 }
    }    $("#filtertxt1").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


    $scope.Options = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,        
        groupHideGroupColumns: true,
        angularCompileRows: true,
        
    };


    $scope.LoadData = function () {
    FinanceReportService.GetOccupancy().then(function (response) {

        ExportColumns = response.exportCols;
        $scope.Options.api.setColumnDefs(response.Coldef);
        $scope.gridata = response.griddata;

        if ($scope.gridata == null) {
            $scope.Visiblity = false;
            $scope.Visiblity1 = false;
            $scope.Options.api.setRowData([]);
        }
        else {
            $scope.Visiblity = true;
            $scope.Visiblity1 = true;
            $scope.Options.api.setRowData($scope.gridata);

        }

    });
    }, function (error) {
        console.log(error);
    }

    $timeout($scope.LoadData, 100);

    $scope.GenerateReport = function (FinanceReport, Type) {
        progress(0, 'Loading...', true);
        $scope.FinanceReport.Type = Type;
        if ($scope.Options.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf1();
            }
            else {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterExcel1();
                }
            }
        }
        else {
            if (Type == 'xls') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    JSONToCSVConvertor($scope.gridata, "Finance Report", true, "FinanceReport");
                }
                else {
                    JSONToCSVConvertor($scope.gridata, "Finance Report", true, "FinanceReport");
                }
            }
            else if (Type == 'pdf') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterPdf1();
                }
                else {
                    $scope.GenerateFilterPdf1();
                }
            }
        };
        progress(0, '', false);
    }


    $scope.GenerateFilterPdf1 = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns;
        var model = $scope.Options.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("FinanceReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "FinanceReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


});
