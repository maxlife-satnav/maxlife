﻿app.service("OverloadReportService", function ($http, $q, UtilityService) {
    this.GetGridData = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/OverloadReport/GetOverloadReportBindGrid')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

});

app.controller('OverloadReportController', function ($scope, $q, OverloadReportService, UtilityService, $timeout, $http, blockUI) {
    $scope.Viewstatus = 0;
    $scope.rptArea = {};
    //$scope.AreaReport = {};
    $scope.GridVisiblity = true;
    //$scope.Spcdata = {};
    //$scope.CurrentProfile = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    blockUI.start();
    $scope.GetOverLoadGrid = function () {
        progress(0, 'Loading...', true);
        OverloadReportService.GetGridData().then(function (response) {
            $scope.RptByUsrGrid = true;
            $scope.gridata = response.data;        
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
             
                $("#table2").hide();
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, 'Loading...', true);
                $("#table2").show();
                $scope.gridOptions.api.setRowData($scope.gridata);

                progress(0, 'Loading...', false);
            }
            $scope.OverloadChartDetails();
            //$scope.CostVerticalChartData();
        }, function (error) {
            console.log(error);
        });
    }

    var columnDefs = [
         { headerName: "Country", field: "CNY_NAME", width: 50, cellClass: 'grid-align', filter: 'set',suppressMenu:true },
         { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align'},
         { headerName: "Location", field: "LCM_NAME", width: 100, cellClass: 'grid-align' },
         { headerName: "Tower", field: "TWR_NAME", width: 50, cellClass: 'grid-align',  },
         { headerName: "Floor", field: "FLR_NAME", width: 50, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Space Id", field: "SPACE_ID", width: 120, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Overload Count", field: "OVERLOAD_COUNT", width: 80, cellClass: 'grid-align', suppressMenu: true }
    ];

    $scope.pageSize = '10';    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


   // $scope.GetOverLoadGrid();
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })




    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();
    var chart;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Overload Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });

    //var Linechart;
    //Linechart = c3.generate({
    //    data: {
    //        //x: 'x',
    //        columns: [],
    //        cache: false,
    //        type: 'bar',
    //        empty: { label: { text: "Sorry, No Data Found" } },
    //    },
    //    legend: {
    //        position: 'top'
    //    },
    //    axis: {
    //        x: {
    //            type: 'category',
    //            categories: ['Vertical'],
    //            height: 130,
    //            show: true,
    //            label: {
    //                text: '',
    //                position: 'outer-center'
    //            }
    //        },
    //        y: {
    //            show: true,
    //            label: {
    //                text: 'Total Cost',
    //                position: 'outer-middle'
    //            }
    //        }
    //    },

    //    width:
    //    {
    //        ratio: 0.5
    //    }
    //});
    $scope.OverloadChartDetails = function () {
        $http({
            url: UtilityService.path + '/api/OverloadReport/GetOverloadChartData',
            method: 'POST',
            data: ''
        }).
            success(function (result) {
                chart.unload();
                chart.unload({
                    ids: ['SpcGraph']
                });                
                chart.load({ columns: result });
                setTimeout(function () {
                    $("#SpcGraph").append(chart.element);
                }, 700);
            });
    }
    //$scope.CostVerticalChartData = function () {
    //    $.ajax({
    //        url: '../../../api/CostReport/GetCostVerticalChartData',
    //        type: 'POST',
    //        data: '',
    //        success: function (result) {
    //            Linechart.unload();
    //            Linechart.load({ columns: result });
    //        }
    //    });
    //    setTimeout(function () {
    //        $("#TotalGraph").append(Linechart.element);
    //    }, 700);

    //}
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });
    blockUI.stop();
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Space Id", key: "SPACE_ID" }, { title: "Overload Count", key: "OVERLOAD_COUNT" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("OverloadReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "OverloadReport.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.rptArea.DocType = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/OverloadReport/OverloadReportData',
                method: 'POST',
                data: $scope.rptArea,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'OverloadReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    $timeout($scope.GetOverLoadGrid, 500);
});

