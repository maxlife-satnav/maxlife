﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BandWiseOccupancyReport.aspx.cs" Inherits="SMViews_SMReports_Views_BandWiseOccupancyReport" %>--%>

<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
            /*border-bottom: 1px solid #eee;*/
            /*background-color: #428bca;*/
            /*-webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .bgRed {
            background-color: #E74C3C;
        }

        .bgGreen {
            background-color: #78AB46;
        }

        .bgBlue {
            background-color: #3498DB;
        }
    </style>
</head>
<body class="amantra" data-ng-controller="BandOccupancycontroller">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Band Wise Occupancy Report</legend>
                    </fieldset>
                    <div class="well">
                        <form id="form1" name="frmSearchspc" data-valid-submit="LoadData(2)" novalidate>
                              <div class="col-md-2 col-sm-2 col-xs-8">
                                <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                    <label class="control-label">Tower <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Tower" data-output-model="SearchSpace.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                        data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SearchSpace.Tower" name="TWR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please Select Tower </span>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-8">
                                <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid}">
                                    <label class="control-label">Floor <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Floor" data-output-model="SearchSpace.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                        data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SearchSpace.Floor" name="FLR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid">Please Select Floor </span>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-8">
                                <br />
                                <div class="clearfix">
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div align="right">
                                    <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />
                                </div>
                                <div class="col-md-12" align="right">
                                    <button type="button" class="btn btn-default btn-sm" onclick="Print('divband')" style="color: rgba(58, 156, 193, 0.87)" id="btnprint">
                                        <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                    </button>
                                    <a data-ng-click="GenReport(SearchSpace,'xls')" id="btnexcel"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                </div>


                                <div id="divband">
                                    <table data-ng-table="tableParams" class="table table-condensed table-bordered table-hover table-striped" id="mytable">
                                        <tr style="color: #ffffff; background: #337ab7;">
                                            <th>Type Of WS </th>
                                            <th data-ng-repeat="subtype in Subtypes">{{subtype.SST_NAME}}</th>
                                        </tr>
                                        <tr style="color: #ffffff; background: #337ab7;">
                                            <th>Band </th>
                                            <th data-ng-repeat="band in Bands">{{band.BAND_NAME}}</th>
                                            <th>Grand Total</th>
                                            <th style="color: #ffffff; background: #78AB46;">As Per Band</th>
                                            <th style="color: #ffffff; background: #3498DB;">Better WS</th>
                                            <th style="color: #ffffff; background: #E74C3C;">Worse</th>
                                        </tr>
                                        <tr data-ng-repeat="bnddet in BandOccupancy"  style="text-align:center">
                                            <td style="vertical-align: central !important; text-align:left; color: #ffffff; background: #337ab7;">
                                                <label>{{bnddet.Key.BAND_NAME}}</label>
                                            </td>
                                            <td data-ng-repeat="innerdet in bnddet.Value" id="lblCol{{$index}}" data-ng-form="innerForm" data-ng-class="getClass(innerdet.Value)" >
                                                <div data-ng-class="form-group">
                                                    <label class="control-label">{{innerdet.Value.CNT}}</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div data-ng-class="form-group">
                                                    <label class="control-label">{{ bnddet.Value | total:'Value'}}</label>
                                                </div>
                                            </td>
                                            <td style="background: #78AB46">
                                                <div data-ng-class="form-group">
                                                    <label class="control-label">{{ bnddet.Value | GreenPer:'Value'}}%</label>
                                                </div>
                                            </td>
                                            <td style="background: #3498DB;">
                                                <div data-ng-class="form-group">
                                                    <label class="control-label">{{ bnddet.Value | BluePer:'Value'}}%</label>
                                                </div>
                                            </td>
                                            <td style="background: #E74C3C;">
                                                <div data-ng-class="form-group">
                                                    <label class="control-label">{{ bnddet.Value | RedPer:'Value'}}%</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr  style="text-align:center">
                                            <td style="color: #ffffff; background: #337ab7;">Total</td>
                                            <td data-ng-repeat="innerdet in BandOccupancy[0].Value">
                                                <div data-ng-class="form-group">
                                                    <label class="control-label">{{ BandOccupancy |totalCol: $index}}</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div data-ng-class="form-group">
                                                    <label class="control-label">{{ BandOccupancy |grandtotal}}</label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 30px; padding-bottom: 30px" id="Graphicaldiv">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script type="text/javascript">
    var app = angular.module('QuickFMS', ["isteven-multi-select", "blockUI", "agGrid"]);
    function Print(divName) {
        var year;
        year = (new Date().getFullYear()).toString();
        $('#' + divName).print({
            globalStyles: true,
            mediaPrint: false,
            stylesheet: "http://fonts.googleapis.com/css?family=Inconsolata",
            iframe: true,
            prepend: "<h3>Max Life Occupancy HO Dashboard</h3> <hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-bottom: 0.5em;''margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'>" + "<br/>",
            append: "<hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'> <h6 align='center'>© QuickFMS " + year + " </h6>"
        });

    }
</script>
<script src="../../Utility.js"></script>
<script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
<script src="../../../Scripts/angular-block-ui.min.js"></script>
<script src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
<script src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
<script src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
<script src="../../../Scripts/Lodash/lodash.min.js"></script>
<script src="../Js/BandWiseOccupancyReport.js"></script>
<script src="../../../Dashboard/Scripts/jQuery.print.js"></script>

<script src="../../Utility.js"></script>

</html>
