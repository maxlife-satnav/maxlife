﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FinanceReport.aspx.cs" Inherits="SMViews_SMReports_Views_FinanceReport" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>

<body data-ng-controller="FinanceReportController" class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Finance Report</legend>
                    </fieldset>
                    <div class="well">

                        <form id="form2" name="FinanceReport">
                            <div id="export2">
                                <div class="box-footer text-right" data-ng-show="Visiblity1" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    <a data-ng-click="GenerateReport(FinanceReport,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenerateReport(FinanceReport,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                            <div data-ng-show="Visiblity" id="Grid2">
                                <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt1" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="Options" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                        </form>







                    </div>
                </div>
            </div>
        </div>
    </div>



    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js"></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script src="../../../Scripts/JSONToCSVConvertor.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
    </script>
    <script src="../../Utility.js"></script>
    <script src="../Js/FinanceReport.js"></script>
</body>
</html>
