﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            /*padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }
        #wrd {
             color: #4813CA;
        }

        #exl {
           color: #2AE214;
        }

        #pf {
            color: #FF0023;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="SpaceAvailabilityReportController" class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Availability Report</legend>
                    </fieldset>
                    <div class="well">
                       <%-- <form id="form1" name="frmSpaceAvailability" data-valid-submit="LoadData()">--%>
                            <%--   <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Select Range<span style="color: red;">*</span></label>
                                        <br />
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <option value="">Select Range</option>
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="SpaceAvailability.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSpaceAvailability.$submitted && frmSpaceAvailability.FromDate.$invalid" style="color: red;">Please From Date</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="SpaceAvailability.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSpaceAvailability.$submitted && frmSpaceAvailability.ToDate.$invalid" style="color: red;">Please To Date</span>
                                    </div>
                                </div>

                            </div>--%>
                            <%--  <div class="clearfix">
                                <div class="box-footer text-right">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    <input type="button" value="Back" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>--%>
                            <div class="row" style="padding-left:18px">
                        <div class="col-md-6">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small"
                                    data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />
                                <%--<div class="col-md-12">
                                    &nbsp;
                                </div>--%>
                            </div>
                       <div class="col-md-6" id="table2" ><br />

                                    <a data-ng-click="GenReport(SpaceAvailability,'doc')"><i id="word" data-ng-show="DocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>

                                    <a data-ng-click="GenReport(SpaceAvailability,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(SpaceAvailability,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div></div>
                      <%--  </form>--%>
                        <br />
                        <form id="form2">
                            <div id="Tabular">
                               <%-- <div class="row">

                                    <a data-ng-click="GenReport(SpaceAvailability,'doc')"><i id="word" data-ng-show="DocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>

                                    <a data-ng-click="GenReport(SpaceAvailability,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(SpaceAvailability,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>--%>

                                <div class="row" style="padding-left:30px">
                                   <%-- <div style="height: 565px">
                                        <div>--%>
                                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 315px; width: auto"></div>
                                     <%--   </div>
                                    </div>--%>
                                </div>
                            </div>
                            <div id="Graphicaldiv">
                                <div id="AvalGraph">&nbsp</div>
                            </div>
                        </form></div>

                        <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="panel-group box box-primary" id="Div2">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div class="panel-heading ">
                                                <h4 class="panel-title modal-header-primary" data-target="#collapseTwo">Vacant Seat Details</h4>
                                                <form role="form" name="form3" id="form3">
                                                    <div class="clearfix">
                                                         <div class="row">
                                                                <a data-ng-click="GenReportVacant(SpaceAvailability,'doc')"><i id="wrd" data-ng-show="PopDocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                                                <a data-ng-click="GenReportVacant(SpaceAvailability,'xls')"><i id="exl" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                                                <a data-ng-click="GenReportVacant(SpaceAvailability,'pdf')"><i id="pf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                                            </div><br />
                                                        <div class="row">
                                                          <%--  <div class="col-md-12">--%>
                                                                <div class="box">
                                                                    <div class="box-danger ">
                                                                        <div data-ag-grid="PopOptions" class="ag-blue" style="height: 300px; width:auto"></div>
                                                                    </div>
                                                                </div>
                                                           <%-- </div>--%>
                                                           
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--    <script src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../../Dashboard/C3/c3.min.js"></script>--%>

    <script src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../../Scripts/jspdf.min.js"></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "blockUI"]);
    </script>
    <script src="../Js/SpaceAvailability_Report.js"></script>
    <script src="../../Utility.js"></script>


    <%--    <script src="../../../Scripts/moment.min.js"></script>--%>
    <%--    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY')));
        }
    </script>--%>
</body>
</html>
