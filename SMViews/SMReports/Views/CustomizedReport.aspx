﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="CustomizedReportController" class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Customizable Report</legend>
                    </fieldset>
                    <div class="well">
                        <form id="form1" name="CustomizedReport" data-valid-submit="LoadData()" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid}">
                                        <label class="control-label">Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.ZN_NAME.$invalid}">
                                        <label class="control-label">Zone <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Zone" data-output-model="Customized.Zone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME"
                                            data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Zone[0]" name="ZN_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ZN_NAME.$invalid">Please Select Zone </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.STE_NAME.$invalid}">
                                        <label class="control-label">State <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="State" data-output-model="Customized.State" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME"
                                            data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.State[0]" name="STE_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.STE_NAME.$invalid">Please Select State </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid}">
                                        <label class="control-label">City<span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid" style="color: red">Please Select City </span>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid}">
                                        <label class="control-label">Tower <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Towers" data-output-model="Customized.Towers" data-button-label="icon TWR_NAME "
                                            data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid" style="color: red">Please Select Tower </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.FLR_NAME.$invalid}">
                                        <label class="control-label">Floor <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Floors" data-output-model="Customized.Floors" data-button-label="icon FLR_NAME"
                                            data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FLR_NAME.$invalid" style="color: red">Please Select Floor </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.Request_Type.$invalid}">
                                        <label class="control-label">Status Type<span style="color: red;">*</span></label>

                                        <select id="Request_Type" name="Request_Type" required="" data-ng-model="Customized.Request_Type" class="form-control">
                                            <option value="">--Select--</option>
                                            <option ng-repeat="sta in statuslst" ng-value="{{sta.Value}}">{{sta.Name}}</option>
                                            <%--<option value="All">All</option>
                                            <option value="1">Vacant</option>
                                            <option value="1002">Allocated To {{$scope.BsmDet.Parent}}</option>
                                            <option value="1003">Allocated To {{$scope.BsmDet.Child}}</option>
                                            <option value="Allocated Occupied">Occupied By Employee</option>--%>
                                        </select>
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Request_Type.$invalid" style="color: red">Please Select Status</span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.Columns.$invalid}">
                                        <label class="control-label">Select Columns<span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Cols" data-output-model="COL" button-label="icon COL" item-label="icon COL" tick-property="ticked" data-on-select-all="" data-on-select-none="">
                                        </div>
                                        <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Columns.$invalid" style="color: red">Please Select Columns </span>
                                    </div>
                                </div>

                                <div class="col-md-1 col-sm-6 col-xs-12">
                                    <br />
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form id="form2">
                            <div data-ng-show="GridVisiblity">
                                <div class="row">
                                    <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                                <div class="row" style="padding-left: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js"></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
    </script>
    <script src="../../Utility.js"></script>
    <script src="../Js/CustomizedReport.js"></script>
</body>
</html>
