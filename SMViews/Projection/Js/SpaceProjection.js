﻿app.service("SpaceProjectionService", function ($http, $q, UtilityService) {
    var deferred = $q.defer();
    this.GetYears = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceProjection/GetYears')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //to save
    this.SaveSPDetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceProjection/SaveDetails', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    this.GetSPRJ_Details = function (ssp) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceProjection/GetSPRJ_Details', ssp)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('SpaceProjectioncontroller', function ($scope, $q, SpaceProjectionService, UtilityService) {
    $scope.SpaceProjection_Details = false;
    $scope.SP_Details_Table = false;
    $scope.SP_Master = {};
    $scope.SP_MasterVals = {};
    $scope.countrylist = [];
    $scope.Citylist = [];
    $scope.yearlist = [];
    $scope.locationlist = [];
    $scope.VerticalList = [];
    $scope.ShowMessage = false;
    $scope.Projection_Type = [{ value: "Quarterly", Name: "Quarterly" }, { value: "Halfyearly", Name: "Halfyearly" }, { value: "Yearly", Name: "Yearly" }];

    UtilityService.getCountires(2).then(function (data) {
        $scope.countrylist = data.data;
    }, function (error) {
        console.log(error);
    });

    UtilityService.getVerticals(2).then(function (data) {
        $scope.VerticalList = data.data;
    }, function (error) {
        console.log(error);
    });
    SpaceProjectionService.GetYears().then(function (data) {
        $scope.yearlist = data;
    }, function (error) {
        console.log(error);
    });

    $scope.GetSPRJ_Details = function () {
        progress(0, 'Loading...', true);
        $scope.getspdtls = {
            SSP_VER_CODE: $scope.SP_MasterVals.VER_NAME,
            SSP_CNY_CODE: $scope.SP_MasterVals.SSP_CNY_CODE,
            SSP_PROJECTION_TYPE: $scope.SP_MasterVals.SSPD_PROJECTION_TYPE,
            SSP_YEAR: $scope.SP_MasterVals.SSP_YEAR,

        };
        SpaceProjectionService.GetSPRJ_Details($scope.getspdtls).then(function (data) {
            if (data.SP_Master != null) {
                progress(0, '', false);
                $scope.SP_Master = data.SP_Master;
                $scope.MonthListdata = data.MonthList;
                $scope.totals = data.totals;
                $scope.SpaceProjection_Details = true;

                console.log(data.SP_Master[0].Value);

            }
            else
            {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', data.Message);
                $scope.SpaceProjection_Details = false;
            }
           
        }, function (error) {
            console.log(error);
        });
        return false;
    }

    $scope.SaveSP_Details_As_Draft = function (statusId) {
        $scope.dataobject = {
            SP_Main: $scope.SP_Master,
            ssp: {
                SSP_VER_CODE: $scope.SP_MasterVals.VER_NAME,
                SSP_CNY_CODE: $scope.SP_MasterVals.SSP_CNY_CODE,
                SSP_CTY_CODE: $scope.SP_MasterVals.SSP_CTY_CODE,
                SSP_PROJECTION_TYPE: $scope.SP_MasterVals.SSPD_PROJECTION_TYPE,
                SSP_YEAR: $scope.SP_MasterVals.SSP_YEAR,
                SSP_REQ_REM: $scope.SP_MasterVals.SSP_REQ_REM,
                SSP_STA_ID: statusId
            }
        };
        SpaceProjectionService.SaveSPDetails($scope.dataobject).then(function (dataobj) {
            $scope.ShowMessage = true;
                showNotification('success', 8, 'bottom-right', dataobj.Message);
        }, function (error) {
            $scope.ShowMessage = true;
            $scope.Success = error.data;
            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.ShowMessage = false;
                });
            }, 500);
            console.log(error);
        });
    }
    $scope.doSum = function (monthNumber) {
        $scope.totals[monthNumber].Value = 0;
        angular.forEach($scope.SP_Master, function (obj) {
            $scope.totals[monthNumber].Value += parseFloat(isNaN(obj.Value[monthNumber].Value.SSPD_PRJ_VALUE) ? 0 : obj.Value[monthNumber].Value.SSPD_PRJ_VALUE);
        });
    };
});