﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
</head>

<body data-ng-controller="SpaceRequisitionController" class="amantra" onload="setDateVals()">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Requisition</legend>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field       <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                    </fieldset>
                    <div class="well">
                        <form id="Form1" name="frmSpaceRequisition" data-valid-submit="getSpaces()" novalidate>
                            <div class="row">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="SpaceRequisition.Country" data-button-label="icon CNY_NAME"
                                                data-item-label="icon CNY_NAME maker" data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.CNY_NAME.$invalid">Please Select Country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.ZN_NAME.$invalid}">
                                            <label class="control-label">Zone<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Zone" data-output-model="SpaceRequisition.Zone" data-button-label="icon ZN_NAME"
                                                data-item-label="icon ZN_NAME maker" data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.Zone[0]" name="ZN_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.ZN_NAME.$invalid">Please Select Zone </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.STE_NAME.$invalid}">
                                            <label class="control-label">State<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="State" data-output-model="SpaceRequisition.State" data-button-label="icon STE_NAME"
                                                data-item-label="icon STE_NAME maker" data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.State[0]" name="STE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.STE_NAME.$invalid">Please Select State </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="SpaceRequisition.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.CTY_NAME.$invalid">Please Select city </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Location" data-output-model="SpaceRequisition.Location" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.Location[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.LCM_NAME.$invalid">Please Select Location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Tower" data-output-model="SpaceRequisition.Tower" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.Tower[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.TWR_NAME.$invalid">Please Select Tower </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floor" data-output-model="SpaceRequisition.Floor" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="FlrChanged()" data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.Floor[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.FLR_NAME.$invalid">Please Select Floor </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.VER_NAME.$invalid}">
                                            <label class="control-label">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                            <div isteven-multi-select selection-mode="single" data-input-model="Verticals" data-output-model="SpaceRequisition.Verticals" data-button-label="icon VER_NAME"
                                                data-item-label=" icon VER_NAME maker" data-on-item-click="getCostcenterByVertical()" data-on-select-all="verticalChangeAll()" data-on-select-none="verticalSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.Verticals[0]" name="VER_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.VER_NAME.$invalid">Please Select {{BsmDet.Parent |lowercase}} </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.Cost_Center_Name.$invalid}">
                                            <label class="control-label">{{BsmDet.Child}}<span style="color: red;">**</span></label>
                                            <div isteven-multi-select selection-mode="single" data-input-model="CostCenters" data-output-model="SpaceRequisition.CostCenters" data-button-label="icon Cost_Center_Name"
                                                data-on-item-click="CostCenterChagned()" data-on-select-all="costCenterChangeAll()" data-on-select-none="costCenterSelectNone()"
                                                data-item-label=" icon Cost_Center_Name maker" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpaceRequisition.CostCenters[0]" name="Cost_Center_Name" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.Cost_Center_Name.$invalid">Please Select {{BsmDet.Child|lowercase}}  </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_FROM_DATE.$invalid}">
                                            <label class="control-label">From Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='fromdate'>
                                                <input type="text" class="form-control" data-ng-model="SpcReqSearch.SRN_FROM_DATE" id="SRN_FROM_DATE" name="SRN_FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_FROM_DATE.$invalid">Please Select From Date </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_TO_DATE.$invalid}">
                                            <label class="control-label">To Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='todate'>
                                                <input type="text" class="form-control" data-ng-model="SpcReqSearch.SRN_TO_DATE" id="SRN_TO_DATE" name="SRN_TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_TO_DATE.$invalid">Please Select To Date </span>
                                        </div>
                                    </div>
                                    <div class="box-footer text-right">
                                        <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                                        <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form role="form" id="Form2" name="frmSpcReq" data-valid-submit="RaiseRequest()" novalidate data-ng-show="Markers.length !=0">

                            <div style="height: 275px">
                                <input id="filtertxt" placeholder="Filter..." type="text" style="width: 25%" />
                                <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                            </div>
                            <br />
                            <br />
                            <div class="clearfix">
                                <div class="pull-left">
                                    <div class="form-group">
                                        <label class="control-label">Requestor Remarks </label>
                                        <textarea rows="3" cols="50" name="SRN_REQ_REM" data-ng-model="SpcReqSearch.SRN_REQ_REM" class="form-control" placeholder="Requestor remarks"></textarea>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="row box-footer text-right">
                                    <input type="button" id="btnviewinmap" data-ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                    <button type="submit" id="btnRequest" class="btn btn-primary custom-button-color">Raise Request</button>
                                </div>
                            </div>
                        </form>


                        <form role="form" id="frmCountReq" name="frmCountReq" data-valid-submit="RaiseCountRequest()" novalidate data-ng-show="gridCountData.length !=0">
                            <div style="height: 275px">
                                <input id="txtCountFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                <div data-ag-grid="gridCountOptions" style="height: 100%;" class="ag-blue"></div>
                            </div>
                            <br />
                            <br />
                            <div class="clearfix">
                                <div class="pull-left">
                                    <div class="form-group" data-ng-class="{'has-error': frmCountReq.$submitted && frmCountReq.SRN_REQ_REM.$invalid}">
                                        <label class="control-label">Requestor Remarks (Please enter EmpId/Name/CustomerName)<span style="color: red;">*</span></label>
                                        <textarea rows="3" cols="50" name="SRN_REQ_REM" data-ng-model="SpcReqSearch.SRN_REQ_REM" required class="form-control" placeholder="Please enter EmpId/Name/CustomerName"></textarea>
                                        <span class="error" data-ng-show="frmCountReq.$submitted && frmCountReq.SRN_REQ_REM.$invalid">Please enter requestor remarks </span>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="row box-footer text-right">
                                    <button type="submit" id="btnCountRequest" class="btn btn-primary custom-button-color">Raise Request</button>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="modal fade bs-example-modal-lg" id="historymodal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="panel-group box box-primary" id="Div2">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <div class="panel-heading">
                                            <h4 class="panel-title"
                                                data-target="#collapseTwo">Plot On the Map
                                            </h4>
                                            <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                data-on-item-click="FlrSectMap()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color text-right" value="Proceed" />
                                        </div>

                                        <form role="form" name="form2" id="form3">
                                            <div class="clearfix">
                                                <div class="col-md-12">
                                                    <div id="main">
                                                        <div id="leafletMap"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="box-header with-border text-center">
                                                            <h3 class="box-title">Status</h3>
                                                        </div>
                                                        <div class="box-body">
                                                            <ul class="products-list product-list-in-box mapeditor-legend">

                                                                <li class="item">
                                                                    <div class="product-info">
                                                                        <a class="product-title">
                                                                            <img class="direct-chat-img" src="../../../images/chair_yellow.gif" alt="message user image">
                                                                            Selected Seats
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--<script src="../../../Scripts/aggrid/ag-grid.min.js"></script>--%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
        function setDateVals() {
            $('#SRN_FROM_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#SRN_TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            // $('#SRN_FROM_DATE').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY')));
            //$('#SRN_TO_DATE').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY')));
        }
    </script>
    <script src="../../Utility.js"></script>
    <script src="../Js/SpaceRequisition.js"></script>

</body>
</html>
