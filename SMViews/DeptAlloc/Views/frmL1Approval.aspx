﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            width: 380px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>


</head>
<body data-ng-controller="L1ApprovalController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>L1 Approval</legend>
                    </fieldset>
                    <div class="well">
                        <div data-ng-show="Viewstatus==0">
                            <form role="form" id="Form1" name="frmApproveAll" data-valid-submit="ApproveAllReq()" novalidate>
                                <h4>Pending Requisitions / Waiting For Approval</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="height: 25px;">
                                            <input id="ApprvlFilter" placeholder="Filter by any..." type="text" style="width: 25%;" />
                                        </div>
                                        <div style="height: 300px">
                                            <div data-ag-grid="L1approvalOptions" style="height: 100%;" class="ag-blue"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="row">
                                        <div class="pull-left" style="padding-left: 25px">
                                            <div class="form-group">
                                                <label class="control-label">Approver Remarks </label>
                                                <textarea rows="3" cols="50" name="SRN_L1_REM" data-ng-model="currentblkReq.SRN_L1_REM" class="form-control" placeholder="Approver Remarks"></textarea>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="row box-footer text-right" style="padding-right: 35px">
                                            <input type="submit" value="Approve" class="btn btn-primary custom-button-color" data-ng-click="setStatus('ApproveAll')" />
                                            <input type="submit" value="Reject" class="btn btn-primary custom-button-color" data-ng-click="setStatus('RejectAll')" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div data-ng-show="Viewstatus==1">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Request ID :</b> </label>
                                        {{currentblkReq.SRN_REQ_ID}}
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Requested By :</b> </label>
                                        {{currentblkReq.AUR_KNOWN_AS}}
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Requested Date :</b> </label>
                                        {{currentblkReq.SRN_REQ_DATE| date: dateformat}}
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 ">
                                    <div class="form-group">
                                        <label><b>Status :</b> </label>
                                        {{currentblkReq.STA_DESC}}
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.CNY_NAME.$invalid}">
                                        <label for="txtcode">Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="countrylist" data-output-model="L1Approve.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                            data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==0||EnableStatus == 1">
                                        </div>

                                        <input type="text" data-ng-model="L1Approve.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                        <span id="message" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.ZN_NAME.$invalid}">

                                        <label for="txtcode">Zone <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Zonelist" data-output-model="L1Approve.selectedZone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME maker"
                                            data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="ButtonStatus == 2">
                                        </div>
                                        <input type="text" data-ng-model="L1Approve.selectedZone" name="ZN_NAME" style="display: none" required="" />
                                        <span id="Span7" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.ZN_NAME.$invalid" style="color: red">Please Select Zone </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.STE_NAME.$invalid}">

                                        <label for="txtcode">State <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Statelist" data-output-model="L1Approve.selectedState" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME maker"
                                            data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="ButtonStatus == 2">
                                        </div>
                                        <input type="text" data-ng-model="L1Approve.selectedState" name="STE_NAME" style="display: none" required="" />
                                        <span id="Span8" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.STE_NAME.$invalid" style="color: red">Please Select State </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.CTY_NAME.$invalid}">
                                        <label for="txtcode">City <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Citylst" data-output-model="L1Approve.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                            data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==0||EnableStatus == 1">
                                        </div>

                                        <input type="text" data-ng-model="L1Approve.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                        <span id="Span1" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.CTY_NAME.$invalid " style="color: red">Please Select City </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.LCM_NAME.$invalid}">
                                        <label for="txtcode">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locationlst" data-output-model="L1Approve.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                        </div>

                                        <input type="text" data-ng-model="L1Approve.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                        <span id="Span2" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.TWR_NAME.$invalid}">
                                        <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Towerlist" data-output-model="L1Approve.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                            data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                        </div>

                                        <input type="text" data-ng-model="L1Approve.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                        <span id="Span3" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.TWR_NAME.$invalid" style="color: red">Please Select Tower </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.FLR_NAME.$invalid}">
                                        <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Floorlist" data-output-model="L1Approve.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                            data-on-item-click="FloorChange()" data-on-select-all="FloorChange()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                        </div>

                                        <input type="text" data-ng-model="L1Approve.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                        <span id="Span4" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.FLR_NAME.$invalid" style="color: red">Please Select Floor </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.VER_NAME.$invalid}">
                                        <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Verticallist" data-output-model="L1Approve.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                            data-on-item-click="VerticalChange()" data-on-select-all="VerticalChange()" data-on-select-none="VerticalSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                        </div>

                                        <input type="text" data-ng-model="L1Approve.selectedVerticals" name="VER_NAME" style="display: none" required="" />
                                        <span id="Span5" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.VER_NAME.$invalid" style="color: red">Please Select {{BsmDet.Parent |lowercase}} </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.Cost_Center_Name.$invalid}">
                                        <label for="txtcode">{{BsmDet.Child}}  <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Costcenterlist" data-output-model="L1Approve.selectedCostcenters" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                            data-on-item-click="CostcenterChange()" data-on-select-all="CostcenterChange()" data-on-select-none="CostcenterSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                        </div>

                                        <input type="text" data-ng-model="L1Approve.selectedVerticals" name="Cost_Center_Name" style="display: none" required="" />
                                        <span id="Span6" class="error" data-ng-show="frmblksearch.$submitted && frmblksearch.Cost_Center_Name.$invalid" style="color: red">Please Select {{BsmDet.Child |lowercase}} </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.SRN_FROM_DATE.$invalid}">
                                        <label class="control-label">From Date <span style="color: red;">*</span></label>
                                        <div class='input-group date' id='fromdate'>
                                            <input type="text" id="txtFrmDate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SRN_FROM_DATE" data-ng-model="currentblkReq.SRN_FROM_DATE" data-ng-disabled="EnableStatus == 0||EnableStatus == 1" />

                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmblksearch.$submitted && frmblksearch.SRN_TO_DATE.$invalid}">
                                        <label class="control-label">To Date <span style="color: red;">*</span></label>
                                        <div class='input-group date' id='todate' onclick="setup('todate')">
                                            <input type="text" id="txtTodate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SRN_TO_DATE" data-ng-model="currentblkReq.SRN_TO_DATE" data-ng-disabled="EnableStatus == 0 ||EnableStatus == 1" />

                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer text-right" data-ng-show="Viewstatus==1">

                                    <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                                </div>
                            </div>

                            <form role="form" id="ApproveSelected" name="frmApproveSelected" data-valid-submit="ApproveRequisitions()" data-ng-show="EnableStatus == 0" novalidate>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="height: 25px;">
                                            <input id="UpdteFilter" placeholder="Filter by any..." type="text" style="width: 25%;" />
                                        </div>
                                        <div style="height: 250px">
                                            <div data-ag-grid="UpdateapprOptions" style="height: 100%; width: 100%" class="ag-blue"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-12">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Requestor Remarks </label>
                                                <textarea rows="3" cols="35" name="SRN_REQ_REM" data-ng-model="currentblkReq.SRN_REQ_REM" class="form-control" data-ng-disabled="EnableStatus == 0 "></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Approver Remarks </label>
                                                <textarea rows="3" cols="50" name="SRN_L1_REM" data-ng-model="currentblkReq.SRN_L1_REM" class="form-control" placeholder="Approver Remarks"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12 box-footer text-right">
                                        <input type="button" id="Button2" data-ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                        <input type="submit" value="Approve" class="btn btn-primary custom-button-color" data-ng-click="setStatus('Approve')" />
                                        <input type="submit" value="Reject" class="btn btn-primary custom-button-color" data-ng-click="setStatus('Reject')" />
                                    </div>
                                </div>
                            </form>

                            <form role="form" id="frmCountReq" name="frmCountReq" data-valid-submit="ApproveRequisitions()" data-ng-show="EnableStatus == 1" novalidate>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="height: 25px;">

                                            <input id="txtCountFilter" placeholder="Filter by any..." type="text" style="width: 25%;" />
                                        </div>

                                        <div style="height: 320px">
                                            <div data-ag-grid="gridCountOptions" style="height: 100%; width: 100%" class="ag-blue"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-12">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Requestor Remarks (Please enter EmpId/ Name /CustomerName) </label>
                                                <textarea rows="3" cols="35" name="SRN_REQ_REM" data-ng-model="currentblkReq.SRN_REQ_REM" class="form-control" data-ng-disabled="EnableStatus == 1"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Approver Remarks </label>
                                                <textarea rows="3" cols="35" name="SRN_L1_REM" data-ng-model="currentblkReq.SRN_L1_REM" class="form-control" placeholder="Approver Remarks"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 box-footer text-right" style="padding-right: 35px">
                                        <input type="submit" value="Approve" class="btn btn-primary custom-button-color" data-ng-click="setStatus('Approve')" />
                                        <input type="submit" value="Reject" class="btn btn-primary custom-button-color" data-ng-click="setStatus('Reject')" />
                                    </div>
                                </div>


                            </form>

                            <div class="modal fade bs-example-modal-lg" id="historymodal">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="panel-group box box-primary" id="Div2">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title"
                                                        data-target="#collapseTwo">Plot on the Map
                                                    </h4>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="col-md-4">
                                                                <div isteven-multi-select data-input-model="MapFloors" data-selection-mode="single" data-helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                                    data-on-item-click="FlrSectMap()" data-on-select-none="FlrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color text-right" value="Proceed" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                Legends:
                                                            </div>
                                                            <div class="col-md-4">
                                                                <span class="label label-success pull-right">Available </span>
                                                                <span class="label selectedseat pull-right">Selected Seats </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <form role="form" name="form2" id="form3">
                                                    <div class="clearfix">
                                                        <div class="col-md-12">
                                                            <div id="main">
                                                                <div id="leafletMap"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../Js/SpaceRequisition.js"></script>
    <script src="../../Utility.js"></script>
    <script src="../Js/L1Approval.js"></script>


</body>

</html>

