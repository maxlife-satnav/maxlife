﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
    <link href="../../../Scripts/angular-block-ui.min.css" rel="stylesheet" />

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <%-- <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
    <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>--%>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

        function SelectAllExtnDates(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }


    </script>

</head>
<body data-ng-controller="SpaceExtensionController" class="amantra" onload="setDateVals()">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">

                    <fieldset>
                        <legend>Space Extension</legend>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                    </fieldset>


                    <div class="well">
                        <form id="spaceExtension" name="frmSpaceExtension" data-valid-submit="GetSpaceExtensionDetails()" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.CNY_NAME.$invalid}">--%>
                                    <label for="txtcode">Country <span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="Countrylst" data-output-model="SEM.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                        data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedCountries" name="CNY_NAME" style="display: none" />
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                    </div>--%>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.ZN_NAME.$invalid}">--%>
                                    <label class="control-label">Zone<span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Zonelst" data-output-model="SEM.selectedZones" data-button-label="icon ZN_NAME"
                                        data-item-label="icon ZN_NAME maker" data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedZones" name="ZN_NAME" style="display: none" required="" />
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.ZN_NAME.$invalid">Please select Zone </span>
                                        </div>--%>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.STE_NAME.$invalid}">--%>
                                    <label class="control-label">State<span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Statelst" data-output-model="SEM.selectedStates" data-button-label="icon STE_NAME"
                                        data-item-label="icon STE_NAME maker" data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedStates" name="STE_NAME" style="display: none" required="" />
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.STE_NAME.$invalid">Please select State </span>
                                        </div>--%>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.CTY_NAME.$invalid}">--%>
                                    <label for="txtcode">City <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Citylst" data-output-model="SEM.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                        data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedCities" name="CTY_NAME" style="display: none" />
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                    </div>--%>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.LCM_NAME.$invalid}">--%>
                                    <label for="txtcode">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Locationlst" data-output-model="SEM.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                        data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedLocations" name="LCM_NAME" style="display: none" />
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                    </div>--%>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.TWR_NAME.$invalid}">--%>
                                    <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Towerlist" data-output-model="SEM.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                        data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedTowers" name="TWR_NAME" style="display: none" />
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                    </div>--%>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.FLR_NAME.$invalid}">
                                    <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Floorlist" data-output-model="SEM.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                        data-item-label="icon FLR_NAME maker" data-on-item-click="FlrChanged()" data-on-select-all="FlrChangeAll()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedFloors" name="FLR_NAME" style="display: none" required="required"/>
                                     <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.VER_NAME.$invalid}">--%>
                                    <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="Verticallist" data-output-model="SEM.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                        data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedVerticals" name="VER_NAME" style="display: none" />
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.VER_NAME.$invalid" style="color: red">Please select {{BsmDet.Parent |lowercase}} </span>
                                    </div>--%>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.Cost_Center_Name.$invalid}">
                                        <label for="txtcode">{{BsmDet.Child}} <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="SEM.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                            data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SEM.selectedCostcenter" name="Cost_Center_Name" style="display: none" required="required"/>
                                        <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.Cost_Center_Name.$invalid" style="color: red">Please select {{BsmDet.Child |lowercase}} </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <%--<div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.FROM_DATE.$invalid}">--%>
                                    <label for="txtcode">From Date <span style="color: red;"></span></label>
                                    <div class="input-group date" id='fromdate'>
                                        <input type="text" class="form-control" data-ng-model="SEM.FROM_DATE" id="FROM_DATE" name="FROM_DATE" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                        </span>
                                    </div>
                                    <%--<span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.FROM_DATE.$invalid" style="color: red">Please select from date </span>
                                    </div>--%>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <%-- <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.TO_DATE.$invalid}">--%>
                                    <label for="txtcode">To Date <span style="color: red;"></span></label>
                                    <div class="input-group date" id='todate'>
                                        <input type="text" class="form-control" data-ng-model="SEM.TO_DATE" id="TO_DATE" name="TO_DATE" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                        </span>
                                    </div>
                                    <%-- <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.TO_DATE.$invalid" style="color: red">Please select to date </span>
                                        </div>--%>
                                </div>

                            </div>
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <button type="submit" value="Search" class="btn btn-primary custom-button-color">Search</button>
                                    <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </form>
                        <br />

                        <form id="spaceExtensionDetails" name="frmSpaceExtensionDetails">
                            <div data-ng-show="SEShowGrid">
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <input id="SpcExtnFilter" placeholder="Filter..." type="text" style="width: 50%;" />
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <%--<label for="txtcode">Extension Date <span style="color: red;">*</span></label>--%>
                                        <%--style="padding-left: 710px;"--%>
                                                                Extension Date
                                    <div class="input-group date " id='EXTDATE'>

                                        <input type="text" class="form-control" data-ng-model="SEM.SSED_EXTN_DT" data-ng-change="setAllDates()" id="EXTN_DATE" name="EXTN_DATE" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('EXTDATE')"></i>
                                        </span>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="height: 320px">
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <textarea rows="2" data-ng-model="SSE_REQ_REM" class="form-control" placeholder="Remarks"></textarea>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                        <input type="submit" data-ng-click="RaiseRequest()" value="Raise Request" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div class="modal fade bs-example-modal-lg" id="historymodal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="panel-group box box-primary" id="Div2">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <div class="panel-heading">
                                            <h4 class="panel-title"
                                                data-target="#collapseTwo">Plot on the Map
                                            </h4>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                            data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color text-right" value="Proceed" />

                                                    </div>
                                                    <div class="col-md-4">
                                                        Legends:
                                                    </div>
                                                    <div class="col-md-4">
                                                        <span class="label label-success pull-right">Available </span>
                                                        <span class="label selectedseat pull-right">Selected Seats </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form name="form2" id="form3">
                                            <div class="clearfix">
                                                <div class="col-md-12">
                                                    <div id="main">
                                                        <div id="leafletMap"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script src="../../../Scripts/angular-block-ui.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI"]);
        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#EXTN_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>

    <script src="../../Utility.js"></script>
    <script src="../Js/SpaceExtension.js"></script>
</body>
</html>
