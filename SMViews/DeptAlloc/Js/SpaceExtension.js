﻿app.service("SpaceExtensionService", function ($http, $q, UtilityService) {
    this.GetSpaceExtensionDetails = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceExtension/GetSpaceExtensionDetails', searchObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.RaiseRequest = function (saveObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceExtension/RaiseRequest', saveObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.checkIfSpaceIdIsAllocatedOrNotBeforeRequest = function (checkObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceExtension/CheckIfSpaceisAllocatedOrNot', checkObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.ApproveAndRejectRequests = function (sendCheckedValsObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceExtension/ApproveAndRejectRequests', sendCheckedValsObj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('SpaceExtensionController', function ($scope, $q, SpaceExtensionService, UtilityService, $filter, blockUI) {
    $scope.SpaceExtension = {};
    $scope.Getcountry = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.CostCenterlist = [];
    $scope.SEShowGrid = false;
    sendCheckedValsObj = [];
    $scope.tempspace = {};
    $scope.Map = {};
    $scope.Map.Floor = [];
    $scope.Markers = [];
    $scope.SelRowData = [];
    var map = L.map('leafletMap');//.setView([17.561298804683357, 79.6684030798511], 11);
    blockUI.start();
    var URLparams = { URL: location.pathname }

    UtilityService.ValidatePagePath(URLparams).then(function (data) {
        if (data.data == 1) //0
        {
            window.location = "/maxlifeuat/login.aspx";
        }
    });

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });


    UtilityService.getCountires(2).then(function (Countries) {
        progress(0, 'Loading...', true);
        if (Countries.data != null) {
            $scope.Countrylst = Countries.data;
        }
        UtilityService.getZone(2).then(function (Znresponse) {
            if (Znresponse.data != null) {
                $scope.Zonelst = Znresponse.data;
            }
            UtilityService.getState(2).then(function (Stresponse) {
                if (Stresponse.data != null) {
                    $scope.Statelst = Stresponse.data;
                }

                UtilityService.getCities(2).then(function (Ctresponse) {
                    if (Ctresponse.data != null) {
                        $scope.Citylst = Ctresponse.data;
                    }
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locationlst = response.data;
                        }
                        UtilityService.getTowers(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Towerlist = response.data;
                            }
                            UtilityService.getFloors(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Floorlist = response.data;
                                }
                            });

                        });

                    });
                });
            });
        });
        progress(0, 'Loading...', false);
    });

    // Country
    $scope.CnyChangeAll = function () {
        $scope.SEM.selectedCountries = $scope.Countrylst;
        $scope.CnyChanged();
    }
    $scope.CnySelectNone = function () {
        $scope.SEM.selectedCountries = [];
        $scope.CnyChanged();
    }
    $scope.CnyChanged = function () {
        if ($scope.SEM.selectedCountries.length != 0) {
            UtilityService.getZoneByCny($scope.SEM.selectedCountries, 2).then(function (response) {
                if (response.data != null)
                    $scope.Zonelst = response.data;
                else
                    $scope.Zonelst = [];
            });
        }
        else
            $scope.Citylst = [];
    }

    //zone
    $scope.ZoneChanged = function () {
        UtilityService.getStateByZone($scope.SEM.selectedZones, 1).then(function (response) {
            if (response.data != null)
                $scope.Statelst = response.data;
            else
                $scope.Statelst = [];
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        $scope.SEM.selectedCountries = [];
        angular.forEach($scope.Zonelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
    }
    $scope.ZoneChangeAll = function () {
        $scope.SEM.selectedZones = $scope.Zonelst;
        $scope.ZoneChanged();
    }
    $scope.ZoneSelectNone = function () {
        $scope.SEM.selectedZones = [];
        $scope.ZoneChanged();
    }

    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.SEM.selectedStates, 1).then(function (response) {
            if (response.data != null)
                $scope.Citylst = response.data;
            else
                $scope.Citylst = [];
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });


        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        angular.forEach($scope.Statelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.SEM.selectedStates = $scope.Statelst;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.SEM.selectedStates = [];
        $scope.SteChanged();
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SEM.selectedCities = $scope.Citylst;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SEM.selectedCities = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SEM.selectedCities, 2).then(function (response) {
            if (response.data != null)
                $scope.Locationlst = response.data;
            else
                $scope.Locationlst = [];
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];

        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SEM.selectedLocations = $scope.Locationlst;
        $scope.LcmChanged();
    }
    $scope.LcmSelectNone = function () {
        $scope.SEM.selectedLocations = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SEM.selectedLocations, 2).then(function (response) {
            if (response.data != null)
                $scope.Towerlist = response.data;
            else
                $scope.Towerlist = [];
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];
        $scope.SEM.selectedCities = [];

        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SEM.selectedCities.indexOf(cty) === -1)
                    $scope.SEM.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SEM.selectedTowers = $scope.Towerlist;
        $scope.TwrChanged();
    }
    $scope.TwrSelectNone = function () {
        $scope.SEM.selectedTowers = [];
        $scope.TwrChanged();
    }
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SEM.selectedTowers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floorlist = response.data;
            else
                $scope.Floorlist = [];
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];
        $scope.SEM.selectedCities = [];
        $scope.SEM.selectedLocations = [];

        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SEM.selectedCities.indexOf(cty) === -1)
                    $scope.SEM.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                if ($scope.SEM.selectedLocations.indexOf(lcm) === -1)
                    $scope.SEM.selectedLocations.push(lcm);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });


    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SEM.selectedFloors = $scope.Floorlist;
        $scope.FlrChanged();
    }
    $scope.FlrSelectNone = function () {
        $scope.SEM.selectedFloors = [];
        $scope.FlrChanged();
    }
    $scope.FlrChanged = function () {

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];
        $scope.SEM.selectedCities = [];
        $scope.SEM.selectedLocations = [];
        $scope.SEM.selectedTowers = [];

        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SEM.selectedCities.indexOf(cty) === -1)
                    $scope.SEM.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                if ($scope.SEM.selectedLocations.indexOf(lcm) === -1)
                    $scope.SEM.selectedLocations.push(lcm);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SEM.selectedTowers.push(twr);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });

    }


    UtilityService.getVerticals(2).then(function (response) {
        $scope.Verticallist = response.data;
    }, function (error) {
        console.log(error);
    });

    UtilityService.getCostCenters(2).then(function (data) {
        $scope.CostCenterlist = data.data;
    }, function (error) {
        console.log(error);
    });

    $scope.VerticalChange = function () {
        UtilityService.getCostcenterByVertical($scope.SEM.selectedVerticals, 2).then(function (data) {
            $scope.CostCenterlist = data.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.VerticalChangeAll = function () {
        UtilityService.getCostcenterByVertical($scope.Verticallist, 2).then(function (data) {
            $scope.CostCenterlist = data.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.verticalSelectNone = function () {
        $scope.CostCenterlist = [];
    }


    $scope.CostCenterChange = function () {
        angular.forEach($scope.Verticallist, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.CostCenterlist, function (value, key) {
            var cc = _.find($scope.Verticallist, { VER_CODE: value.Vertical_Code });
            if (cc != undefined && value.ticked == true) {
                cc.ticked = true;
                $scope.SEM.selectedVerticals = cc;
            }
        });
    }
    $scope.CostCenterChangeAll = function () {
        $scope.CostCenterChange();
    }
    $scope.CostCenterSelectNone = function () {
        $scope.CostCenterChange();
    }
 

    var columnDefs = [
         { headerName: "Space", field: "SPC_NAME", width: 180, cellClass: 'grid-align', cellStyle: changeRowColor, pinned: 'left' },
         { headerName: "Space Type", field: "SSA_SPC_TYPE", width: 150, cellClass: 'grid-align', cellStyle: changeRowColor },
         { headerName: "Space Sub Type", field: "SSA_SPC_SUB_TYPE", width: 150, cellClass: 'grid-align', cellStyle: changeRowColor },
         { headerName: "Shift Type", field: "SH_NAME", width: 250, cellClass: 'grid-align', cellStyle: changeRowColor },
         { headerName: "", field: "SSED_VER_NAME", width: 150, cellClass: 'grid-align', cellStyle: changeRowColor },
         { headerName: "", field: "Cost_Center_Name", width: 150, cellClass: 'grid-align', cellStyle: changeRowColor },
         { headerName: "Employee", field: "AUR_KNOWN_AS", width: 250, cellClass: 'grid-align', cellStyle: changeRowColor },
         { headerName: "Designation", field: "EMP_DESIGNATION", width: 250, cellClass: 'grid-align', cellStyle: changeRowColor },
         {
             headerName: "From Date", template: '<span>{{data.SSAD_FROM_DATE | date: "dd MMM, yyyy"}}</span>', field: "SSAD_FROM_DATE",
             width: 150, cellClass: 'grid-align', cellStyle: changeRowColor, pinned: 'right', suppressMenu: true,
         },
         {
             headerName: "To Date", template: '<span>{{data.SSAD_TO_DATE | date: "dd MMM, yyyy"}}</span>', width: 150, field: "SSAD_TO_DATE",
             cellClass: 'grid-align', cellStyle: changeRowColor, pinned: 'right', suppressMenu: true,
         },
         {
             headerName: "Extension Date", field: "SSED_EXTN_DT", width: 120, cellClass: 'grid-align', cellRenderer: createDatePicker, cellStyle: changeRowColor,
             pinned: 'right', suppressMenu: true,
         },
         {
             headerName: "Select All", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>",
             cellClass: 'grid-align', headerCellRenderer: headerCellRendererFunc, cellStyle: changeRowColor, pinned: 'right', suppressMenu: true,
         },
    ];


    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        rowData: null,
        enableSorting: true,
        enableColResize: true,
        angularCompileRows: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
    };
    blockUI.stop();

    $scope.GetSpaceExtensionDetails = function () {
        var fromdate = moment($scope.SEM.FROM_DATE);
        var todate = moment($scope.SEM.TO_DATE);
        if (fromdate > todate) {
            $scope.SRShowGrid = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.Markers = [];
            $scope.SelRowData = [];
            map.eachLayer(function (layer) {
                map.removeLayer(layer);
            });
            $scope.SEVM = {
                spceextn_flr_ver: { selectedFloors: $scope.SEM.selectedFloors, selectedCostcenters: $scope.SEM.selectedCostcenter },
                spcextn: { SSE_FROM_DATE: $scope.SEM.FROM_DATE, SSE_TO_DATE: $scope.SEM.TO_DATE }
            };
            SpaceExtensionService.GetSpaceExtensionDetails($scope.SEVM).then(function (response) {
                if (response.data == null) {
                    $scope.SEShowGrid = false;
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                   
                }
                else {
                    $scope.gridOptions.api.setRowData([]);
                    $scope.SEShowGrid = true;
                    GetMarkers(response.data);
                    $scope.gridOptions.api.setRowData($scope.Markers);
                    $scope.gridOptions.columnApi.getColumn("SSED_VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                    $scope.gridOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
                    $scope.gridOptions.api.refreshHeader();
                    progress(0, '', false);
                }
            }, function (error) {
                console.log(error);
                progress(0, '', false);
            });
        }
    }
    $scope.chkChanged = function (selctedRow) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: data.SSED_SPC_ID, spacetype: 'CHA' } });
            if (data.ticked) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
                //data.setIcon(selctdChricon)
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
                //data.setIcon(Vacanticon)
            }
        }
    }

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = true;
                    });
                    if ($scope.drawnItems) {

                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = true;
                                value.setStyle(selctdChrStyle);
                            }
                        });
                    }


                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = false;
                    });
                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = false;
                                value.setStyle(VacantStyle);
                            }
                        });
                    }
                });
            }
        });
        return eHeader;
    }

    function createDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.SSED_EXTN_DT');
        newDate.setAttribute('ng-change', 'checkIfSpaceIdIsAllocatedOrNotBeforeRequest(data)');
        newDate.type = "input";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        })
        return newDate;
    }

    function changeRowColor(params) {
        if (params.data.STATUS == "FAIL") {
            return { 'background-color': 'yellow' };
        }
    }

    $scope.checkIfSpaceIdIsAllocatedOrNotBeforeRequest = function (data) {
        $scope.selspcObj = {};
        $scope.selspcObj.SSAD_SRN_REQ_ID = data.SSAD_SRN_REQ_ID;
        $scope.selspcObj.SSED_SPC_ID = data.SSED_SPC_ID;
        $scope.selspcObj.SSAD_FROM_DATE = data.SSAD_FROM_DATE;
        $scope.selspcObj.SSAD_TO_DATE = data.SSAD_TO_DATE;
        $scope.selspcObj.SSAD_FROM_TIME = data.SSAD_FROM_TIME;
        $scope.selspcObj.SSAD_TO_TIME = data.SSAD_TO_TIME;
        var todate = moment(data.SSAD_TO_DATE);
        var extndate = moment(data.SSED_EXTN_DT);
        if (todate > extndate) {
            data.ticked = false;
            showNotification('error', 8, 'bottom-right', 'Space Extension Date should be greater than ToDate.');
        }
        else {
            SpaceExtensionService.checkIfSpaceIdIsAllocatedOrNotBeforeRequest($scope.selspcObj).then(function (response) {
                if (response.length != 0) {
                    data.ticked = false;
                    $scope.chkChanged(data);
                    var status = response[0].STATUS;
                    if (status == "FAIL") {
                        var foundobj = _.find($scope.Markers, { SSAD_SRN_REQ_ID: response[0].SSAD_SRN_REQ_ID });
                        foundobj.STATUS = "FAIL";
                        $scope.gridOptions.api.refreshView();
                        var msg = "Space is already allocated to other person between your selected to date extension date.";
                        showNotification('error', 8, 'bottom-right', response[0].REMARKS);
                    }
                    setTimeout(function () {
                        $scope.$apply(function () {
                            angular.forEach($scope.Markers, function (data) { data.STATUS = "Default" });
                            $scope.gridOptions.api.refreshView();
                        });
                    }, 3000);
                }
                else {
                    data.ticked = true;
                    $scope.chkChanged(data);
                }

            }, function (error) {
                console.log(error);
            })
        }
    }

    $scope.RaiseRequest = function () {
        var sendCheckedValsObj = [];
        $scope.selectedSpaces = [];
        angular.forEach($scope.Markers, function (Value, Key) {
            $scope.selspcObj = {};
            if (Value.ticked) {
                $scope.selspcObj.SSAD_SRN_REQ_ID = Value.SSAD_SRN_REQ_ID;
                $scope.selspcObj.SSED_SPC_ID = Value.SSED_SPC_ID;
                $scope.selspcObj.SPC_NAME = Value.SPC_NAME;
                $scope.selspcObj.SPC_FLR_ID = Value.SPC_FLR_ID;
                $scope.selspcObj.lat = Value.lat;
                $scope.selspcObj.lon = Value.lon;
                $scope.selspcObj.SSAD_FROM_DATE = Value.SSAD_FROM_DATE;
                $scope.selspcObj.SSAD_TO_DATE = Value.SSAD_TO_DATE;
                $scope.selspcObj.SSAD_FROM_TIME = Value.SSAD_FROM_TIME;
                $scope.selspcObj.SSAD_TO_TIME = Value.SSAD_TO_TIME;
                $scope.selspcObj.SSA_SPC_TYPE = Value.SSA_SPC_TYPE;
                $scope.selspcObj.SSA_SPC_SUB_TYPE = Value.SSA_SPC_SUB_TYPE;
                $scope.selspcObj.SH_NAME = Value.SH_NAME;
                $scope.selspcObj.Cost_Center_Name = Value.Cost_Center_Name;
                $scope.selspcObj.AUR_KNOWN_AS = Value.AUR_KNOWN_AS;
                $scope.selspcObj.SSED_AUR_ID = Value.SSED_AUR_ID;
                $scope.selspcObj.SSED_VER_NAME = Value.SSED_VER_NAME;
                $scope.selspcObj.EMP_DESIGNATION = Value.EMP_DESIGNATION;
                $scope.selspcObj.STATUS = Value.STATUS;
                $scope.selspcObj.SSED_EXTN_DT = Value.SSED_EXTN_DT;
                $scope.selspcObj.ticked = Value.ticked;
                $scope.selspcObj.SSAD_ID = Value.SSAD_ID;
                sendCheckedValsObj.push($scope.selspcObj);
            }
        });
        //angular.forEach($scope.Markers, function (data) {
        //     if (data.ticked == true) {
        //         sendCheckedValsObj.push(data);
        //     }
        // });
        var saveObjDet = [];
        angular.forEach(sendCheckedValsObj, function (saveObj) {
            var todate = moment(saveObj.SSAD_TO_DATE);
            var extndate = moment(saveObj.SSED_EXTN_DT);
            if (todate < extndate) {
                saveObjDet.push(saveObj);
            }
        })

        if (saveObjDet.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Space Extension Date should be greater than To Date.');
        }

        if (sendCheckedValsObj.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Check atleast One Space Id To Extend.');
        }
        else if (sendCheckedValsObj.length == saveObjDet.length) {
            progress(0, 'Loading...', true);
            $scope.dataobject = {
                spcextn: { SSE_REQ_REM: $scope.SSE_REQ_REM, SSE_FROM_DATE: $scope.SEM.FROM_DATE, SSE_TO_DATE: $scope.SEM.TO_DATE },
                spceextn_flr_ver: { selectedFloors: $scope.SEM.selectedFloors, selectedCostcenters: $scope.SEM.selectedCostcenter, selectedSpaces: saveObjDet },
                FLAG: 4
            };
            SpaceExtensionService.RaiseRequest($scope.dataobject).then(function (response) {
                if (response.data != null) {
                    if (response.STATUS == "FAIL") {
                        angular.forEach(response.data, function (data) {
                            var foundobj = _.find($scope.Markers, { SSAD_SRN_REQ_ID: data.SSAD_SRN_REQ_ID });
                            foundobj.STATUS = "FAIL";
                        });
                        $scope.gridOptions.api.refreshView();
                        setTimeout(function () {
                            $scope.$apply(function () {
                                angular.forEach($scope.Markers, function (data) { data.STATUS = "Default" });
                                $scope.gridOptions.api.refreshView();
                            });
                        }, 3000);
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                    else {
                        $scope.SEShowGrid = false;
                        $scope.Clear();
                        progress(0, '', false);

                        showNotification('success', 8, 'bottom-right', response.Message);
                    }
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }, function (response) {
                progress(0, '', false);
            });
        }
        else {
            showNotification('error', 8, 'bottom-right', 'Space Extn Date should be greater than ToDate.');
        }

    }

    $scope.SelectAll = false;

    $scope.selectAllRows = function selectAll() {
        angular.forEach($scope.Markers, function (data) {
            data.ticked = $scope.SelectAll;
        });
    };

    $scope.setAllDates = function setAllExtnDates() {
        angular.forEach($scope.Markers, function (data) {
            data.SSED_EXTN_DT = $('#EXTN_DATE').val();
        });
    }

    $scope.Clear = function () {
       
        $scope.SEM = {};
        $scope.gridata = [];
        $scope.selectedSpaces = [];
        $scope.Markers = [];
        $scope.gridOptions.rowData = [];

        angular.forEach($scope.Countrylst, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.Citylst, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floorlist, function (floor) {
            floor.ticked = false;
        });

        angular.forEach($scope.Verticallist, function (ver) {
            ver.ticked = false;
        });

        angular.forEach($scope.CostCenterlist, function (cc) {
            cc.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (zn) {
            zn.ticked = false;
        });

        angular.forEach($scope.Statelst, function (ste) {
            ste.ticked = false;
        });

       
        $scope.SEM.Getcountry = [];
        $scope.SEM.Citylst = [];
        $scope.SEM.Locationlst = [];
        $scope.SEM.Towerlist = [];
        $scope.SEM.Floorlist = [];
        $scope.SEM.Verticallist = [];
        $scope.SEM.CostCenterlist = [];
        $scope.SEM.Zonelst = [];
        $scope.SEM.Statelst = [];

        $scope.SEShowGrid = false;
        $scope.SEM.FROM_DATE = "";
        $scope.SEM.TO_DATE = "";

    }

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#SpcExtnFilter").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    })

    ///// For map layout
   
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            angular.forEach($scope.SEM.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }

        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        if ($scope.SelRowData.length == 0)
            $scope.loadmap();
    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });
    }


    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
           // success
           // results: an array of data objects from each deferred.resolve(data) call
           function (results) {
               var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
               map.fitBounds(bounds);
               $scope.SelRowData = $filter('filter')($scope.Markers, { SPC_FLR_ID: $scope.Map.Floor[0].FLR_CODE });
               angular.forEach($scope.SelRowData, function (value, key) {

                   $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SSED_SPC_ID, spacetype: 'CHA' } });
                   $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                   $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
                   $scope.marker.SPC_NAME = value.SPC_NAME;
                   $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                   $scope.marker.lat = value.lat;
                   $scope.marker.lon = value.lon;
                   $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
                   $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
                   $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
                   $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
                   $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
                   $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
                   $scope.marker.SH_NAME = value.SH_NAME;
                   $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
                   $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
                   $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
                   $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
                   $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
                   $scope.marker.STATUS = value.STATUS;
                   $scope.marker.SSED_EXTN_DT = value.SSED_EXTN_DT;
                   $scope.marker.ticked = value.ticked;
                   $scope.marker.SSAD_ID = value.SSAD_ID;
                   if (value.ticked)
                       $scope.marker.setStyle(selctdChrStyle);
                   else
                       $scope.marker.setStyle(VacantStyle);
                   $scope.marker.layer = value.SSA_SPC_TYPE;
                   $scope.marker.bindLabel(value.SPC_NAME);
                   $scope.marker.on('click', markerclicked);
                   $scope.marker.addTo(map);
               });
           },
           // error
           function (response) {
           }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});


    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};
            $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
            $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
            $scope.marker.SPC_NAME = value.SPC_NAME;
            $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
            $scope.marker.lat = value.lat;
            $scope.marker.lon = value.lon;
            $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
            $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
            $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
            $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
            $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
            $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
            $scope.marker.SH_NAME = value.SH_NAME;
            $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
            $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
            $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
            $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
            $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
            $scope.marker.STATUS = value.STATUS;
            $scope.marker.SSED_EXTN_DT = value.SSED_EXTN_DT;
            $scope.marker.ticked = value.ticked;
            $scope.marker.SSAD_ID = value.SSAD_ID;
            $scope.marker.layer = value.SSA_SPC_TYPE;
            $scope.Markers.push($scope.marker);
        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SSED_SPC_ID: this.SSED_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

});

