﻿app.service("SpaceReleaseService", function ($http, $q, UtilityService) {
    this.GetSpacesToRelease = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRelease/GetSpacesToRelease', searchObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.ReleaseSelectedSpaces = function (saveObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRelease/ReleaseSelectedSpaces', saveObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});

app.controller('SpaceReleaseController', function ($scope, $q, SpaceReleaseService, UtilityService, $filter, blockUI) {
    $scope.SpaceRelease = {};
    $scope.SEM = {};
    $scope.Countrylst = [];
    $scope.Zonelst = [];
    $scope.Statelst = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.CostCenterlist = [];
    $scope.SRShowGrid = false;
    sendCheckedValsObj = [];
    $scope.tempspace = {};

    $scope.Map = {};
    $scope.Map.Floor = [];
    $scope.Markers = [];
    sendCheckedValsObj = [];
    $scope.SelRowData = [];
    var map = L.map('leafletMap');//.setView([17.561298804683357, 79.6684030798511], 11);

    blockUI.start();

    var URLparams = { URL: location.pathname }

    UtilityService.ValidatePagePath(URLparams).then(function (data) {
        if (data.data == 1) //0
        {
            window.location = "/maxlifeuat/login.aspx";
        }
    });


    $scope.SPACE_RELEASE = [
 	    { release: "Employee", RELEASE_STA_ID: "1049", ticked: false }, //////     From Employee to department 
        { release: "Function", RELEASE_STA_ID: "1050", ticked: false }, /////////  From Dept to Vertical
        { release: "Employee & Function", RELEASE_STA_ID: "1051", ticked: false } //////////  From Employee To vertical 
    ];


    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });


    UtilityService.getCountires(2).then(function (Countries) {
        if (Countries.data != null) {
            $scope.Countrylst = Countries.data;
        }
        UtilityService.getZone(2).then(function (Znresponse) {
            if (Znresponse.data != null) {
                $scope.Zonelst = Znresponse.data;
            }


            UtilityService.getState(2).then(function (Stresponse) {
                if (Stresponse.data != null) {
                    $scope.Statelst = Stresponse.data;
                }

                UtilityService.getCities(2).then(function (Ctresponse) {
                    if (Ctresponse.data != null) {
                        $scope.Citylst = Ctresponse.data;
                    }
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locationlst = response.data;
                        }
                        UtilityService.getTowers(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Towerlist = response.data;
                            }
                            UtilityService.getFloors(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Floorlist = response.data;
                                }
                            });

                        });

                    });
                });
            });
        });
    });

   

    UtilityService.getVerticals(2).then(function (response) {
        $scope.Verticallist = response.data;
    }, function (error) {
        console.log(error);
    });

    UtilityService.getCostCenters(2).then(function (data) {
        $scope.CostCenterlist = data.data;
    }, function (error) {
        console.log(error);
    });

    $scope.VerticalChange = function () {        
        UtilityService.getCostcenterByVertical($scope.SEM.selectedVerticals, 2).then(function (data) {            
            $scope.CostCenterlist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.VerticalChangeAll = function () {
        $scope.SEM.selectedVerticals = $scope.Verticallist;
        UtilityService.getCostcenterByVertical($scope.Verticallist, 2).then(function (data) {
            $scope.CostCenterlist = data.data;
        }, function (error) {
            console.log(error);
        });
        //$scope.CostCenterChange();
    }

    $scope.verticalSelectNone = function () {
        $scope.CostCenterlist = [];
        $scope.CostCenterChange();
    }

    $scope.CostCenterChange = function () {
      //  if ($scope.SEM.selectedVerticals.length == 0) {
            angular.forEach($scope.Verticallist, function (value, key) {
                value.ticked = false;
            });

            angular.forEach($scope.CostCenterlist, function (value, key) {
                var cc = _.find($scope.Verticallist, { VER_CODE: value.Vertical_Code });
                if (cc != undefined && value.ticked == true) {
                    cc.ticked = true;
                    $scope.SEM.selectedVerticals = cc;
                }
            });
        //}
    }

    $scope.CostCenterChangeAll = function () {
        $scope.CostCenterChange();
    }

    $scope.CostCenterSelectNone = function () {
        $scope.CostCenterChange();
    }

    var columnDefs = [
        {
            headerName: "Select All", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>",
            cellClass: 'grid-align', headerCellRenderer: headerCellRendererFunc, pinned: 'left', suppressMenu: true,
        },
         { headerName: "Space", field: "SPC_NAME", width: 250, cellClass: 'grid-align', pinned: 'left' },
         { headerName: "Space Type", field: "SSA_SPC_TYPE", width: 120, cellClass: 'grid-align', },
         { headerName: "Space Sub Type", field: "SSA_SPC_SUB_TYPE", width: 120, cellClass: 'grid-align', },
         { headerName: "Shift Type", field: "SH_NAME", width: 250, cellClass: 'grid-align', },
         { headerName: "", field: "SSED_VER_NAME", width: 200, cellClass: 'grid-align', },
         { headerName: "", field: "Cost_Center_Name", width: 200, cellClass: 'grid-align', },
         { headerName: "Employee", hide: true, field: "AUR_KNOWN_AS", width: 250, pinned: 'left', cellClass: 'grid-align' },
         { headerName: "Designation", hide: true, field: "EMP_DESIGNATION", width: 250, cellClass: 'grid-align' },
         { headerName: "From Date", template: '{{data.SSAD_FROM_DATE | date: "dd MMM, yyyy"}}', field: "SSAD_FROM_DATE", width: 130, cellClass: 'grid-align', suppressMenu: true, },
         { headerName: "To Date", template: '{{data.SSAD_TO_DATE | date: "dd MMM, yyyy"}}', field: "SSAD_TO_DATE", width: 130, cellClass: 'grid-align', suppressMenu: true, },

    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        //rowData: null,
        enableSorting: true,
        //rowSelection: 'multiple',
        enableColResize: true,
        angularCompileRows: true,
    };
    blockUI.stop();
    // Country
    $scope.CnyChangeAll = function () {
        $scope.SEM.selectedCountries = $scope.Countrylst;
        $scope.CnyChanged();
    }
    $scope.CnySelectNone = function () {
        $scope.SEM.selectedCountries = [];
        $scope.CnyChanged();
    }
    $scope.CnyChanged = function () {
        if ($scope.SEM.selectedCountries.length != 0) {
            UtilityService.getZoneByCny($scope.SEM.selectedCountries, 2).then(function (response) {
                if (response.data != null)
                    $scope.Zonelst = response.data;
                else
                    $scope.Zonelst = [];
            });
        }
        else
            $scope.Citylst = [];
    }

    //zone
    $scope.ZoneChanged = function () {
        UtilityService.getStateByZone($scope.SEM.selectedZones, 1).then(function (response) {
            if (response.data != null)
                $scope.Statelst = response.data;
            else
                $scope.Statelst = [];
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        $scope.SEM.selectedCountries = [];
        angular.forEach($scope.Zonelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
    }
    $scope.ZoneChangeAll = function () {
        $scope.SEM.selectedZones = $scope.Zonelst;
        $scope.ZoneChanged();
    }
    $scope.ZoneSelectNone = function () {
        $scope.SEM.selectedZones = [];
        $scope.ZoneChanged();
    }

    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.SEM.selectedStates, 1).then(function (response) {
            if (response.data != null)
                $scope.Citylst = response.data;
            else
                $scope.Citylst = [];
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });


        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        angular.forEach($scope.Statelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.SEM.selectedStates = $scope.Statelst;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.SEM.selectedStates = [];
        $scope.SteChanged();
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SEM.selectedCities = $scope.Citylst;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SEM.selectedCities = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SEM.selectedCities, 2).then(function (response) {
            if (response.data != null)
                $scope.Locationlst = response.data;
            else
                $scope.Locationlst = [];
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];

        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SEM.selectedLocations = $scope.Locationlst;
        $scope.LcmChanged();
    }
    $scope.LcmSelectNone = function () {
        $scope.SEM.selectedLocations = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SEM.selectedLocations, 2).then(function (response) {
            if (response.data != null)
                $scope.Towerlist = response.data;
            else
                $scope.Towerlist = [];
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];
        $scope.SEM.selectedCities = [];

        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SEM.selectedCities.indexOf(cty) === -1)
                    $scope.SEM.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SEM.selectedTowers = $scope.Towerlist;
        $scope.TwrChanged();
    }
    $scope.TwrSelectNone = function () {
        $scope.SEM.selectedTowers = [];
        $scope.TwrChanged();
    }
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SEM.selectedTowers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floorlist = response.data;
            else
                $scope.Floorlist = [];
        });

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];
        $scope.SEM.selectedCities = [];
        $scope.SEM.selectedLocations = [];

        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SEM.selectedCities.indexOf(cty) === -1)
                    $scope.SEM.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                if ($scope.SEM.selectedLocations.indexOf(lcm) === -1)
                    $scope.SEM.selectedLocations.push(lcm);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });


    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SEM.selectedFloors = $scope.Floorlist;
        $scope.FlrChanged();
    }
    $scope.FlrSelectNone = function () {
        $scope.SEM.selectedFloors = [];
        $scope.FlrChanged();
    }
    $scope.FlrChanged = function () {

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });

        $scope.SEM.selectedCountries = [];
        $scope.SEM.selectedZones = [];
        $scope.SEM.selectedStates = [];
        $scope.SEM.selectedCities = [];
        $scope.SEM.selectedLocations = [];
        $scope.SEM.selectedTowers = [];

        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SEM.selectedCountries.indexOf(cny) === -1)
                    $scope.SEM.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SEM.selectedCities.indexOf(cty) === -1)
                    $scope.SEM.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                if ($scope.SEM.selectedLocations.indexOf(lcm) === -1)
                    $scope.SEM.selectedLocations.push(lcm);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SEM.selectedTowers.push(twr);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SEM.selectedZones.indexOf(zn) === -1)
                    $scope.SEM.selectedZones.push(zn);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SEM.selectedStates.indexOf(ste) === -1)
                    $scope.SEM.selectedStates.push(ste);
            }
        });

    }

    $scope.GetSpacesToRelease = function () {
        $scope.SSR_REQ_REM = "";

        var fromdate = moment($scope.SEM.FROM_DATE);
        var todate = moment($scope.SEM.TO_DATE);
        if (fromdate > todate) {
            $scope.SRShowGrid = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.Markers = [];
            $scope.SelRowData = [];
            map.eachLayer(function (layer) {
                map.removeLayer(layer);
            });
            var selctdVal = _.find($scope.SPACE_RELEASE, { ticked: true });
            $scope.ReleaseFrom = selctdVal.RELEASE_STA_ID;
            $scope.SRVM = {
                EMP_ID: $scope.SEM.EMP_ID,
                FromDate: $scope.SEM.FROM_DATE,
              
                //RELEASE_STA_ID: $scope.ReleaseFrom,
                RELEASE_STA_ID: parseInt(selctdVal.RELEASE_STA_ID),
                ToDate: $scope.SEM.TO_DATE,
                selectedCities: $scope.SEM.selectedCities,
                selectedCostcenters: $scope.SEM.selectedCostcenter,
                selectedCountries: $scope.SEM.selectedCountries,
                selectedFloors: $scope.SEM.selectedFloors,
                selectedLocations: $scope.SEM.selectedLocations,
                selectedTowers: $scope.SEM.selectedTowers,
                selectedVerticals: $scope.SEM.selectedVerticals,
                selectedZones: $scope.SEM.selectedZones,
                selectedStates: $scope.SEM.selectedStates
            };
            SpaceReleaseService.GetSpacesToRelease($scope.SRVM).then(function (response) {
                if (response.data == null) {
                    $scope.SRShowGrid = false;
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', response.Message);

                }
                else {
                    $scope.SRShowGrid = true;
                    $scope.gridOptions.api.setRowData([]);

                    $scope.gridOptions.columnApi.getColumn("SSED_VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                    $scope.gridOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
                    $scope.gridOptions.api.refreshHeader();

                    $scope.gridata = response.data;
                    if ($scope.ReleaseFrom == "1049" || $scope.ReleaseFrom == "1051") {
                        $scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', true);
                        $scope.gridOptions.columnApi.setColumnVisible('EMP_DESIGNATION', true);
                    }
                    else {
                        $scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', false);
                        $scope.gridOptions.columnApi.setColumnVisible('EMP_DESIGNATION', false);
                    }
                    GetMarkers(response.data);
                    $scope.gridOptions.api.setRowData($scope.Markers);

                    progress(0, 'Loading...', false);
                }
            }, function (error) {
                console.log(error);
                progress(0, 'Loading...', false);
            });
        }
    }

    $scope.chkChanged = function (data) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: data.SSED_SPC_ID, spacetype: 'CHA' } });
            if (data.ticked) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
                //data.setIcon(selctdChricon)
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
                //data.setIcon(Vacanticon)
            }
        }
    }

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = true;
                    });
                    if ($scope.drawnItems) {

                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = true;
                                value.setStyle(selctdChrStyle);
                            }
                        });
                    }


                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = false;
                    });
                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = false;
                                value.setStyle(VacantStyle);
                            }
                        });
                    }
                });
            }
        });
        return eHeader;
    }

    $scope.ReleaseSelectedSpaces = function () {
        var selctdVal = _.find($scope.SPACE_RELEASE, { ticked: true });
        selctdVal = selctdVal.RELEASE_STA_ID;
        var sendCheckedValsObj = [];
        angular.forEach($scope.Markers, function (value, Key) {
            $scope.selspcObj = {};
            if (value.ticked) {
                $scope.selspcObj.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                $scope.selspcObj.SSAD_AUR_ID = value.SSAD_AUR_ID;
                $scope.selspcObj.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                $scope.selspcObj.SSAD_SRN_CC_ID = value.SSAD_SRN_CC_ID;
                $scope.selspcObj.SSED_SPC_ID = value.SSED_SPC_ID;
                $scope.selspcObj.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
                $scope.selspcObj.SSAD_TO_DATE = value.SSAD_TO_DATE;
                $scope.selspcObj.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
                $scope.selspcObj.SSAD_TO_TIME = value.SSAD_TO_TIME;
                $scope.selspcObj.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
                $scope.selspcObj.SSED_AUR_ID = value.SSED_AUR_ID;
                $scope.selspcObj.STATUS = value.STATUS;
                $scope.selspcObj.SSED_EXTN_DT = value.SSED_EXTN_DT;
                $scope.selspcObj.ticked = value.ticked;
                $scope.selspcObj.SSAD_ID = value.SSAD_ID;

                sendCheckedValsObj.push($scope.selspcObj);
            }
        });
        if (sendCheckedValsObj.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Check atleast One Space Id To Release.');
        }
        else {
            $scope.dataobject = {
                SSR: sendCheckedValsObj,
                Remarks: $scope.SSR_REQ_REM,
                RELEASE_STA_ID: selctdVal,
            };
            SpaceReleaseService.ReleaseSelectedSpaces($scope.dataobject).then(function (response) {
                if (response.data != null) {
                    $scope.Clear();
                    showNotification('success', 8, 'bottom-right', response.Message);
                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }, function (response) {
                progress(0, '', false);
            });
        }

    }

    $scope.Clear = function () {
        //$scope.SEM = {};
        $scope.gridata = [];
        $scope.selectedSpaces = [];
        $scope.Markers = [];
        $scope.gridOptions.rowData = [];
        angular.forEach($scope.Countrylst, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.Citylst, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (zone) {
            zone.ticked = false;
        });
        angular.forEach($scope.Statelst, function (state) {
            state.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floorlist, function (floor) {
            floor.ticked = false;
        });

        angular.forEach($scope.Verticallist, function (ver) {
            ver.ticked = false;
        });

        angular.forEach($scope.CostCenterlist, function (cc) {
            cc.ticked = false;
        });

        angular.forEach($scope.SPACE_RELEASE, function (SR) {
            SR.ticked = false;
        });

        $scope.SEM.Countrylst = [];
        $scope.SEM.Zonelst = [];
        $scope.SEM.Statelst = [];
        $scope.SEM.Citylst = [];
        $scope.SEM.Locationlst = [];
        $scope.SEM.Towerlist = [];
        $scope.SEM.Floorlist = [];
        $scope.SEM.Verticallist = [];
        $scope.SEM.CostCenterlist = [];

        $scope.release = [];

        $scope.SEM.FROM_DATE = "";
        $scope.SEM.TO_DATE = "";
        $scope.SSR_REQ_REM = "";
        $scope.SRShowGrid = false;
    }

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#SpaceReleaseReqFilter").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    })

    ///// For map layout
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            angular.forEach($scope.SEM.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }

        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        if ($scope.SelRowData.length == 0)
            $scope.loadmap();
    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        //$('#leafletMap').empty();

        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE };

        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });
    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;

            
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
           // success
           // results: an array of data objects from each deferred.resolve(data) call
           function (results) {
               var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
               map.fitBounds(bounds);
               $scope.SelRowData = $filter('filter')($scope.Markers, { SPC_FLR_ID: $scope.Map.Floor[0].FLR_CODE });
               angular.forEach($scope.SelRowData, function (value, key) {

                   $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SSED_SPC_ID, spacetype: 'CHA' } });
                   $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                   $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                   $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                   $scope.marker.SSAD_SRN_CC_ID = value.SSAD_SRN_CC_ID;
                   $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
                   $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
                   $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
                   $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
                   $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
                   $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
                   $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
                   $scope.marker.SH_NAME = value.SH_NAME;
                   $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
                   $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
                   $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
                   $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
                   $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
                   $scope.marker.STATUS = value.STATUS;
                   $scope.marker.SSED_EXTN_DT = value.SSED_EXTN_DT;
                   $scope.marker.ticked = value.ticked;
                   if (value.ticked)
                       $scope.marker.setStyle(selctdChrStyle);
                   else
                       $scope.marker.setStyle(VacantStyle);
                   $scope.marker.SSAD_ID = value.SSAD_ID;
                   $scope.marker.SPC_NAME = value.SPC_NAME;
                   $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                   $scope.marker.lat = value.lat;
                   $scope.marker.lon = value.lon;
                   $scope.marker.layer = value.SSA_SPC_TYPE;
                   $scope.marker.bindLabel(value.SPC_NAME);
                   $scope.marker.on('click', markerclicked);
                   $scope.marker.addTo(map);
               });
           },
           // error
           function (response) {
           }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});


    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };
    
    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};

            $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
            $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
            $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
            $scope.marker.SSAD_SRN_CC_ID = value.SSAD_SRN_CC_ID;
            $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
            $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
            $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
            $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
            $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
            $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
            $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
            $scope.marker.SH_NAME = value.SH_NAME;
            $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
            $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
            $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
            $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
            $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
            $scope.marker.STATUS = value.STATUS;
            $scope.marker.SSED_EXTN_DT = value.SSED_EXTN_DT;
            $scope.marker.ticked = value.ticked;
            $scope.marker.SSAD_ID = value.SSAD_ID;
            $scope.marker.SPC_NAME = value.SPC_NAME;
            $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
            $scope.marker.lat = value.lat;
            $scope.marker.lon = value.lon;
            $scope.marker.layer = value.SSA_SPC_TYPE;
            $scope.Markers.push($scope.marker);
        });
    };

    function markerclicked(e) {

        var marker = _.find($scope.Markers, { SSED_SPC_ID: this.SSED_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

});

