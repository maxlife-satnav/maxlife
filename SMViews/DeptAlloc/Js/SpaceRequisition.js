﻿app.service("SpaceRequisitionService", function ($http, $q, UtilityService) {

    this.getSpaces = function (searchObj) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetSpaces', searchObj)
         .then(function (response) {
             //console.log(response.data);
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getReqCountDetails = function (FlrObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetReqCountDetails', FlrObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getShifts = function (selLocs) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetShifts', selLocs)
         .then(function (response) {
             //console.log(response.data);
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getEmpDetails = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetEmpDetails', selCostCenters)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSubTypes = function (SpaceType) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetSubTypes?SpaceType=' + SpaceType + '')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.checkExists = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/CheckExists', data)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.CheckSpace = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/CheckSpace', data)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.raiseRequest = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/RaiseRequest', ReqObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.RaiseCountRequest = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/RaiseCountRequest', ReqObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getEmpTypes = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetEmpTypes', selCostCenters)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getEmpSubTypes = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceRequisition/getEmpSubTypes')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSpaceTypes = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceRequisition/GetSpaceTypes')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSpaceSubTypes = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceRequisition/GetSpaceSubTypes')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

});

app.controller('SpaceRequisitionController', function ($scope, $q, SpaceRequisitionService, UtilityService, $filter, blockUI) {

    $scope.SpaceRequisition = {};
    $scope.SpcReqSearch = {};

    $scope.Country = [];
    $scope.Zone = [];
    $scope.State = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.Verticals = [];
    $scope.CostCenters = [];
    $scope.Shifts = [];
    $scope.EmpDetails = [];
    $scope.tempspace = {};
    $scope.selectedSpaces = [];
    $scope.selectedSpacesCount = [];
    $scope.gridata = [];
    $scope.gridCountData = [];

    $scope.SysPreference = [];
    $scope.AllocationType = {};
    $scope.ReqCountType = {};
    $scope.Type = [];
    $scope.SubType = [];

    $scope.SelRowData = [];
    $scope.MapFloors = [];
    $scope.Markers = [];
    $scope.SelLayers = [];
    var map = L.map('leafletMap');
    blockUI.start();
    var URLparams = { URL: location.pathname }

    UtilityService.ValidatePagePath(URLparams).then(function (data) {
        if (data.data == 1) //0
        {
            window.location = "/maxlifeuat/login.aspx";
        }
    });

    $scope.Clear = function () {

        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });       
        angular.forEach($scope.State, function (state) {
            state.ticked = false;
        });
        angular.forEach($scope.Zone, function (zone) {
            zone.ticked = false;
        });
        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Location, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floor, function (floor) {
            floor.ticked = false;
        });
        angular.forEach($scope.Verticals, function (vertical) {
            vertical.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (costCenter) {
            costCenter.ticked = false;
        });
        $scope.SpcReqSearch = {};
        $scope.selectedSpaces = [];
        $scope.Markers = [];
        $scope.gridOptions.rowData = [];
        $scope.gridCountOptions.rowData = [];
        $scope.gridata = [];
        $scope.gridCountData = [];
    }

    UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPreference = response.data;
            $scope.AllocationType = _.find($scope.SysPreference, { SYSP_CODE: "Allocation Type" })
            $scope.ReqCountType = _.find($scope.SysPreference, { SYSP_CODE: "Requisition Count Type" })
        }
    });

   
    UtilityService.getCountires(2).then(function (Countries) {
        if (Countries.data != null) {
            console.log(Countries);
            $scope.Country = Countries.data;
        }
        UtilityService.getZone(2).then(function (Znresponse) {
            if (Znresponse.data != null) {
                console.log(Znresponse);
                $scope.Zone = Znresponse.data;
            }


            UtilityService.getState(2).then(function (Stresponse) {
                if (Stresponse.data != null) {
                    console.log(Stresponse);
                    $scope.State = Stresponse.data;
                }

                UtilityService.getCities(2).then(function (Ctresponse) {
                    if (Ctresponse.data != null) {
                        $scope.City = Ctresponse.data;
                    }
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;
                        }
                        UtilityService.getTowers(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Tower = response.data;
                            }
                            UtilityService.getFloors(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Floor = response.data;
                                }
                            });

                        });

                    });
                });
            });
        });
    });

    // Country
    $scope.CnyChangeAll = function () {
        $scope.SpaceRequisition.Country = $scope.Country;
        $scope.CnyChanged();
    }
    $scope.CnySelectNone = function () {
        $scope.SpaceRequisition.Country = [];
        $scope.CnyChanged();
    }
    $scope.CnyChanged = function () {
        if ($scope.SpaceRequisition.Country.length != 0) {
            UtilityService.getZoneByCny($scope.SpaceRequisition.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.Zone = response.data;
                else
                    $scope.Zone = [];
            });
        }
        else
            $scope.City = [];
    }

    //zone
    $scope.ZoneChanged = function () {
        UtilityService.getStateByZone($scope.SpaceRequisition.Zone, 1).then(function (response) {
            if (response.data != null)
                $scope.State = response.data;
            else
                $scope.State = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        $scope.SpaceRequisition.Country = [];
        angular.forEach($scope.Zone, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SpaceRequisition.Country.indexOf(cny) === -1)
                $scope.SpaceRequisition.Country.push(cny);
            }
        });
    }
    $scope.ZoneChangeAll = function () {
        $scope.SpaceRequisition.Zone = $scope.Zone;
        $scope.ZoneChanged();
    }
    $scope.ZoneSelectNone = function () {
        $scope.SpaceRequisition.Zone = [];
        $scope.ZoneChanged();
    }

    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.SpaceRequisition.State, 1).then(function (response) {
            if (response.data != null)
                $scope.City = response.data;
            else
                $scope.City = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });


        $scope.SpaceRequisition.Country = [];
        $scope.SpaceRequisition.Zone = [];
        angular.forEach($scope.State, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SpaceRequisition.Country.indexOf(cny) === -1)
                $scope.SpaceRequisition.Country.push(cny);
            }
        });
        angular.forEach($scope.State, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SpaceRequisition.Zone.indexOf(zn) === -1)
                $scope.SpaceRequisition.Zone.push(zn);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.SpaceRequisition.State = $scope.State;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.SpaceRequisition.State = [];
        $scope.SteChanged();
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SpaceRequisition.City = $scope.City;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SpaceRequisition.City = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SpaceRequisition.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        $scope.SpaceRequisition.Country = [];
        $scope.SpaceRequisition.Zone = [];
        $scope.SpaceRequisition.State = [];

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SpaceRequisition.Country.indexOf(cny) === -1)
                $scope.SpaceRequisition.Country.push(cny);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SpaceRequisition.Zone.indexOf(zn) === -1)
                $scope.SpaceRequisition.Zone.push(zn);
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SpaceRequisition.State.indexOf(ste) === -1)
                $scope.SpaceRequisition.State.push(ste);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SpaceRequisition.Location = $scope.Location;
        $scope.LcmChanged();
    }
    $scope.LcmSelectNone = function () {
        $scope.SpaceRequisition.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SpaceRequisition.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        $scope.SpaceRequisition.Country = [];
        $scope.SpaceRequisition.Zone = [];
        $scope.SpaceRequisition.State = [];
        $scope.SpaceRequisition.City = [];

        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SpaceRequisition.Country.indexOf(cny) === -1)
                $scope.SpaceRequisition.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SpaceRequisition.City.indexOf(cty) === -1)
                $scope.SpaceRequisition.City.push(cty);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SpaceRequisition.Zone.indexOf(zn) === -1)
                $scope.SpaceRequisition.Zone.push(zn);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SpaceRequisition.State.indexOf(ste) === -1)
                $scope.SpaceRequisition.State.push(ste);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SpaceRequisition.Tower = $scope.Tower;
        $scope.TwrChanged();
    }
    $scope.TwrSelectNone = function () {
        $scope.SpaceRequisition.Tower = [];
        $scope.TwrChanged();
    }
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SpaceRequisition.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        $scope.SpaceRequisition.Country = [];
        $scope.SpaceRequisition.Zone = [];
        $scope.SpaceRequisition.State = [];
        $scope.SpaceRequisition.City = [];
        $scope.SpaceRequisition.Location = [];

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SpaceRequisition.Country.indexOf(cny) === -1)
                $scope.SpaceRequisition.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SpaceRequisition.City.indexOf(cty) === -1)
                $scope.SpaceRequisition.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                if ($scope.SpaceRequisition.Location.indexOf(lcm) === -1)
                $scope.SpaceRequisition.Location.push(lcm);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SpaceRequisition.Zone.indexOf(zn) === -1)
                $scope.SpaceRequisition.Zone.push(zn);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SpaceRequisition.State.indexOf(ste) === -1)
                $scope.SpaceRequisition.State.push(ste);
            }
        });


    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SpaceRequisition.Floor = $scope.Floor;
        $scope.FlrChanged();
    }
    $scope.FlrSelectNone = function () {
        $scope.SpaceRequisition.Floor = [];
        $scope.FlrChanged();
    }
    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });

        $scope.SpaceRequisition.Country = [];
        $scope.SpaceRequisition.Zone = [];
        $scope.SpaceRequisition.State = [];
        $scope.SpaceRequisition.City = [];
        $scope.SpaceRequisition.Location = [];
        $scope.SpaceRequisition.Tower = [];

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                if ($scope.SpaceRequisition.Country.indexOf(cny) === -1) 
                    $scope.SpaceRequisition.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                if ($scope.SpaceRequisition.City.indexOf(cty) === -1)
                $scope.SpaceRequisition.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                if ($scope.SpaceRequisition.Location.indexOf(lcm) === -1)
                $scope.SpaceRequisition.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SpaceRequisition.Tower.push(twr);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                if ($scope.SpaceRequisition.Zone.indexOf(zn) === -1)
                $scope.SpaceRequisition.Zone.push(zn);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var ste = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                if ($scope.SpaceRequisition.State.indexOf(ste) === -1)
                $scope.SpaceRequisition.State.push(ste);
            }
        });

    }

    UtilityService.getVerticals(2).then(function (response) {
        if (response.data != null) {
            $scope.Verticals = response.data;
        }
    });

    UtilityService.getCostCenters(2).then(function (response) {
        if (response.data != null) {
            $scope.CostCenters = response.data;
        }
    });

  
    $scope.getCostcenterByVertical = function () {
        UtilityService.getCostcenterByVertical($scope.SpaceRequisition.Verticals, 2).then(function (response) {
            if (response.data != null) {
                $scope.CostCenters = response.data;
            }
            else { $scope.CostCenters = [] }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.verticalChangeAll = function () {
        $scope.SpaceRequisition.Verticals = $scope.Verticals;
        $scope.getCostcenterByVertical();
    }

     //$scope.getEmpDetails = function () {      
    //    $scope.SpcReqSearch.SRN_FROM_DATE = $scope.SpcReqSearch.SRN_FROM_DATE;
    //    var SpaceData = { cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch }; 
    //    SpaceRequisitionService.getEmpDetails(SpaceData).then(function (response) {
    //        $scope.EmpDetails = response.data;           
    //    }, function (error) {
    //        console.log(error);
    //    }); 
    //}
    $scope.CostCenterChagned = function () {
        angular.forEach($scope.Verticals, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (value, key) {
            var ver = _.find($scope.Verticals, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.SpaceRequisition.Verticals[0] = ver;
            }
        });
    }
    $scope.getSpaces = function () {
        progress(0, 'Loading...', true);
        SpaceRequisitionService.getShifts($scope.SpaceRequisition.Location).then(function (response) {
            $scope.Shifts = [];
            $scope.Shifts = response.data;
        }, function (error) {
            console.log(error);
        });
        $scope.SpcReqSearch.SRN_FROM_DATE = $scope.SpcReqSearch.SRN_FROM_DATE;
        var SpaceData = { cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch };

        SpaceRequisitionService.getEmpDetails(SpaceData).then(function (response) {
            $scope.EmpDetails = response.data;
        }, function (error) {
            console.log(error);
        });
        var searchObj = { flrlst: $scope.SpaceRequisition.Floor, verlst: $scope.SpaceRequisition.Verticals, cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch };
        $scope.Markers = [];
        if ($scope.AllocationType.SYSP_VAL1 == "1039") {
            if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                SpaceRequisitionService.getSpaces(searchObj).then(function (response) {
                    if (response.data != null) {
                        $scope.Markers = [];
                        GetMarkers(response.data);
                        $scope.gridOptions.api.setRowData($scope.Markers);
                        progress(0, '', false);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected vertical');

                    }
                });
            }
        }
            // 1040=Request Raised for Employee
        else if ($scope.AllocationType.SYSP_VAL1 == "1040") {
            if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                SpaceRequisitionService.getSpaces(searchObj).then(function (response) {
                    if (response.data != null) {
                        $scope.Markers = [];
                        GetMarkers(response.data);
                        $scope.gridOptions.api.setRowData($scope.Markers);
                        $scope.gridOptions.columnApi.setColumnVisible('SRD_AUR_ID', true);
                        $scope.gridOptions.columnApi.setColumnVisible('AUR_DES_NAME', true);
                        progress(0, '', false);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected vertical');
                    }
                });
            }
        }
            // 1041=Request Raised for Future
        else if ($scope.AllocationType.SYSP_VAL1 == "1041") {
            if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                $scope.gridCountOptions.rowData = [];
                var flrObj = { flrlst: $scope.SpaceRequisition.Floor, REQ_CNT_STA_FUT: $scope.ReqCountType.SYSP_VAL1 };
                SpaceRequisitionService.getReqCountDetails(flrObj).then(function (response) {
                    if (response.data != null) {
                        $scope.gridCountData = response.data;
                        $scope.gridCountOptions.api.setRowData($scope.gridCountData);
                        if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Employee Count";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Grade Type";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Request Type";
                            $scope.gridCountOptions.api.refreshHeader();
                            $scope.Type = [];
                            SpaceRequisitionService.getEmpTypes($scope.SpaceRequisition.CostCenters).then(function (response) {
                                if (response.data != null) {
                                    $scope.Type = response.data;
                                }
                            });
                            SpaceRequisitionService.getEmpSubTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.SubType = response.data;
                                }
                            });
                            progress(0, '', false);
                        }
                        else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Space Count";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Space Type";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Space Sub Type";
                            $scope.gridCountOptions.api.refreshHeader();
                            $scope.Type = [];
                            $scope.SubType = [];
                            SpaceRequisitionService.getSpaceTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.Type = response.data;
                                }
                            });
                            SpaceRequisitionService.getSpaceSubTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.SubType = response.data;
                                }
                            });
                            progress(0, '', false);
                        }
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected vertical');
                    }
                });
            }
        }
    }

    $scope.RaiseRequest = function () {
        $scope.selectedSpaces = [];
        var flag = false;
        for (var i = 0; $scope.Markers != null && i < $scope.Markers.length; i += 1) {
            $scope.selspcObj = {};

            if ($scope.Markers[i].ticked && ($scope.Markers[i].SRD_SH_CODE == undefined || $scope.Markers[i].SRD_SH_CODE == '')) {
                $scope.selectedSpaces = [];
                showNotification('error', 8, 'bottom-right', 'Please Select Shift');
                flag = true;
                break;
            }
            else if ($scope.Markers[i].ticked && $scope.Markers[i].SRD_AUR_ID == undefined && $scope.AllocationType.SYSP_VAL1 == "1040") {
                $scope.selectedSpaces = [];
                showNotification('error', 8, 'bottom-right', 'Please Select Employee');
                flag = true;
                break;
            }
            else if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                flag = true;
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            }
            else if ($scope.Markers[i].ticked) {
                $scope.selspcObj.SRD_SSA_SRNREQ_ID = $scope.Markers[i].SRD_SSA_SRNREQ_ID;
                $scope.selspcObj.SRD_LCM_CODE = $scope.Markers[i].SRD_LCM_CODE;
                $scope.selspcObj.SRD_SPC_ID = $scope.Markers[i].SRD_SPC_ID;
                $scope.selspcObj.SRD_SPC_TYPE = $scope.Markers[i].layer;
                $scope.selspcObj.SRD_SPC_SUB_TYPE = $scope.Markers[i].SRD_SPC_SUB_TYPE;
                $scope.selspcObj.SRD_SH_CODE = $scope.Markers[i].SRD_SH_CODE;
                $scope.selspcObj.SRD_AUR_ID = $scope.Markers[i].SRD_AUR_ID;
                $scope.selspcObj.SSA_FLR_CODE = $scope.Markers[i].SSA_FLR_CODE;
                $scope.selspcObj.SRN_STA_ID = $scope.Markers[i].SRN_STA_ID;
                $scope.selspcObj.STACHECK = $scope.Markers[i].STACHECK;
                $scope.selspcObj.ticked = $scope.Markers[i].ticked;
                $scope.selectedSpaces.push($scope.selspcObj);
            }
        };
        $scope.SpcReqSearch.STACHECK = 4;
        //1039 = Request Raised  for Department
        if ($scope.AllocationType.SYSP_VAL1 == "1039") {
            $scope.SpcReqSearch.SRN_STA_ID = 1010;
            $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1039";
        }
            //1040 = Request Raised for Employee
        else if ($scope.AllocationType.SYSP_VAL1 == "1040") {
            $scope.SpcReqSearch.SRN_STA_ID = 1017;
            $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1040";
        }
        if (flag == false) {
            setTimeout(function () {
                if ($scope.selectedSpaces.length != 0) {

                    progress(0, 'Loading...', true);
                    var ReqObj = { flrlst: $scope.SpaceRequisition.Floor, cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch, spcreqdet: $scope.selectedSpaces, ALLOCSTA: 4 };
                    console.log(ReqObj);
                    SpaceRequisitionService.raiseRequest(ReqObj).then(function (response) {
                        if (response.data != null) {
                            $scope.Clear();
                            progress(0, '', false);
                            showNotification('success', 8, 'bottom-right', response.Message);
                        }
                        else {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', response.Message);
                        }
                    }, function (response) {
                        progress(0, '', false);
                    });
                }
                else
                    showNotification('error', 8, 'bottom-right', 'Please select atleast one space ID to allocate');
            }, 500);
        }

    }

    $scope.RaiseCountRequest = function () {
        if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        } else {
            $scope.SpcReqSearch.STACHECK = 4;
            $scope.selectedSpaces = [];
            angular.forEach($scope.gridCountData, function (o) {
                if (o.SRC_REQ_SEL_TYPE != null && o.SRC_REQ_SEL_TYPE != "" && o.SRC_REQ_TYPE != null && o.SRC_REQ_TYPE != "")
                    $scope.selectedSpaces.push(o);
            });
            //1038 = Employee Count
            if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                $scope.SpcReqSearch.SRN_STA_ID = 1042;
                $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1041";
            }
                //1037 = Space Count
            else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                $scope.SpcReqSearch.SRN_STA_ID = 1042;
                $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1041";
            }
            setTimeout(function () {
                if ($scope.selectedSpaces.length != 0) {
                    progress(0, 'Loading...', true);
                    var ReqObj = { flrlst: $scope.SpaceRequisition.Floor, cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch, spcreqcount: $scope.selectedSpaces, ALLOCSTA: 4, };
                    SpaceRequisitionService.RaiseCountRequest(ReqObj).then(function (response) {
                        if (response.data != null) {
                            $scope.Clear();
                            progress(0, '', false);
                            showNotification('success', 8, 'bottom-right', response.Message);
                        }
                        else {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', response.Message);
                        }
                    }, function (response) {
                        progress(0, '', false);
                    });
                }
                else if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                    showNotification('error', 8, 'bottom-right', 'Please Select Grade and RequestType');
                }
                else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                    showNotification('error', 8, 'bottom-right', 'Please Select Space and Space Subtype');
                }
            }, 500);
        }
    }

    //For Grid column Headers
    var columnDefs = [
        { headerName: "Select All", field: "ticked", width: 90, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>", headerCellRenderer: headerCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Space ID", field: "SRD_SPC_NAME", width: 300, cellClass: "grid-align", pinned: 'left', suppressMenu: true, },
        { headerName: "Space Type", field: "SRD_SPC_TYPE_NAME", width: 180, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SRD_SPC_SUB_TYPE_NAME", width: 180, cellClass: "grid-align" },
        { headerName: "Floor", field: "SSA_FLR_CODE", width: 180, cellClass: "grid-align" },
        { headerName: "Shift Type", field: "SRD_SH_CODE", suppressMenu: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<select  class='form-control' data-ng-change='CheckSpace(data)' ng-model='data.SRD_SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in Shifts | filter:data.SRD_LCM_CODE' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>" },
        { headerName: "Employee", field: "SRD_AUR_ID", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<select  class='form-control'  ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data,\"{{data.SRD_AUR_ID}}\")'><option value=''>--Select--</option><option data-ng-disabled='Emp.ticked' ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_KNOWN_AS}}</option></select>" },
        { headerName: "Designation", field: "AUR_DES_NAME", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_DES_NAME}}</label>" },
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };




    $scope.pageSize = '10';

    // For Grid column Headers
    var columnDefsCount = [
        { headerName: "Country", field: "SRC_CNY_NAME", cellClass: "grid-align" },
        { headerName: "Zone", field: "SRC_ZN_NAME", cellClass: "grid-align" },
        { headerName: "State", field: "SRC_STE_NAME", cellClass: "grid-align" },
        { headerName: "City", field: "SRC_CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "SRC_LCM_NAME", cellClass: "grid-align" },
        { headerName: "Tower", field: "SRC_TWR_NAME", cellClass: "grid-align" },
        { headerName: "Floor", field: "SRC_FLR_NAME", cellClass: "grid-align" },
        { headerName: "", field: "SRC_REQ_CNT", cellClass: "grid-align", filter: 'set', template: "<input type='textbox' ng-model='data.SRC_REQ_CNT'>", suppressMenu: true, },
        {
            headerName: "", field: "SRC_REQ_SEL_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select  class='form-control'  ng-model='data.SRC_REQ_SEL_TYPE' data-ng-change='getSubTypes(data)'><option value=''>--Select--</option><option ng-repeat='Ty in Type' value='{{Ty.CODE}}'>{{Ty.NAME}}</option></select>"
        },
        {
            headerName: "", field: "SRC_REQ_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select  class='form-control'  ng-init='' ng-model='data.SRC_REQ_TYPE'><option value=''>--Select--</option><option ng-repeat='SubTy in SubType' value='{{SubTy.CODE}}'>{{SubTy.NAME}}</option></select>"
        },
    ];

    $scope.gridCountOptions = {
        columnDefs: columnDefsCount,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridCountOptions.api.sizeColumnsToFit()
        }
    };

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        //var cb1 = document.createElement('input');
        //var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        //cb1.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        //var eHeader = document.createElement('label');
        //var eTitle = document.createTextNode('This page');
        //var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        //eHeader.appendChild(br);
        //eHeader.appendChild(cb1);
        //eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = true;
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = false;
                    });
                });
            }
        });
        //cb1.addEventListener('change', function (e) {
        //    if ($(this)[0].checked) {
        //        $scope.$apply(function () {
        //            angular.forEach($scope.Markers, function (value, key) {
        //                value.ticked = true;
        //            });
        //        });
        //    } else {
        //        $scope.$apply(function () {
        //            angular.forEach($scope.Markers, function (value, key) {
        //                value.ticked = false;
        //            });
        //        });
        //    }
        //});

        return eHeader;
    }

    $scope.chkChanged = function (selctedRow) {
        if (selctedRow.ticked == false) {
            selctedRow.setIcon(Vacanticon)
            //_.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SRD_SPC_ID: selctedRow.SRD_SPC_ID }));
            //selctedRow.STACHECK = UtilityService.Deleted;
            //$scope.selectedSpaces.push(selctedRow);
        }
        else {
            selctedRow.setIcon(selctdChricon)
            //angular.copy(selctedRow, $scope.tempspace);
            //$scope.tempspace.STACHECK = UtilityService.Added;
            //$scope.selectedSpaces.push($scope.tempspace);
            //$scope.tempspace = {};
        }
    }

    $scope.CheckSpace = function (Data) {
        $scope.selspcObj = {};
        $scope.spcreqdet = [];

        $scope.selspcObj.SRD_SH_CODE = Data.SRD_SH_CODE;
        $scope.selspcObj.SRD_SPC_ID = Data.SRD_SPC_ID;

        $scope.spcreqdet.push($scope.selspcObj);
        var SpaceData = { spcreq: $scope.SpcReqSearch, spcreqdet: $scope.spcreqdet };

        SpaceRequisitionService.CheckSpace(SpaceData).then(function (response) {
            if (response.data == 2) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else if (response.data == 0) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                $scope.chkChanged(Data);
                Data.ticked = true;
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.GetDesignation = function (Aur, old) {
        //console.log(old);
        Aur.AUR_DES_NAME = "";
        for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
            if ($scope.EmpDetails[i].AUR_ID === Aur.SRD_AUR_ID) {
                //SpaceRequisitionService.checkExists($scope.EmpDetails[i]).then(function (response) {
                //    $scope.SubType = response.data;
                //    //data.SRC_REQ_TYPE=
                //}, function (error) {
                //    console.log(error);
                //});
                Aur.AUR_DES_NAME = $scope.EmpDetails[i].AUR_DES_NAME;
                $scope.EmpDetails[i].ticked = true;
                //console.log($scope.EmpDetails);
            }
            if ($scope.EmpDetails[i].AUR_ID === old) {
                $scope.EmpDetails[i].ticked = false;
            }
        }
    }

    $scope.getSubTypes = function (SpaceType) {
        if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
            $scope.SubType = [];
            SpaceRequisitionService.getSubTypes(SpaceType.SRC_REQ_SEL_TYPE).then(function (response) {
                $scope.SubType = response.data;
                //data.SRC_REQ_TYPE=
            }, function (error) {
                console.log(error);
            });
        }
    }

    ///// For map layout
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.SpaceRequisition.Floor, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }
        $("#historymodal").modal('show');
    }

    $('#historymodal').on('shown.bs.modal', function () {
        progress(0, 'Loading...', true);

        if ($scope.SelRowData.length == 0) {
            $scope.loadmap();
        }
        else {
            progress(0, 'Loading...', false);
        }

    });
    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
           function (results) {
               var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
               map.fitBounds(bounds);
               $scope.SelRowData = $filter('filter')($scope.Markers, { SSA_FLR_CODE: $scope.Map.Floor[0].FLR_CODE });
               angular.forEach($scope.SelRowData, function (marker, key) {
                   marker.addTo(map);
               });
           },
           function (response) {
           }
        );
    };
    var Vacanticon = L.icon({
        iconUrl: UtilityService.path + '/images/chair_Green.gif',
        iconSize: [16, 16], // size of the icon
    });
    var selctdChricon = L.icon({
        iconUrl: UtilityService.path + '/images/chair_yellow.gif',
        iconSize: [16, 16], // size of the icon
    });

    function GetMarkers(data) {
        jQuery.each(data, function (index, value) {
            $scope.marker = L.marker([value.lat, value.lon], { icon: Vacanticon });
            $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
            $scope.marker.SRD_LCM_CODE = value.SRD_LCM_CODE;
            $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
            $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
            $scope.marker.layer = value.SRD_SPC_TYPE;
            $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
            $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
            $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
            $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
            $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
            $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ticked = value.ticked;
            $scope.marker.bindLabel(value.SRD_SPC_ID);
            $scope.marker.on('click', markerclicked);
            $scope.Markers.push($scope.marker);
        });

    };
    blockUI.stop();
    function markerclicked(e) {
        if (!this.ticked) {
            this.setIcon(selctdChricon)
            this.ticked = true;
        }
        else {
            this.setIcon(Vacanticon)
            this.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterCountChanged(value) {
        $scope.gridCountOptions.api.setQuickFilter(value);
    }

    $("#txtCountFilter").change(function () {
        onFilterCountChanged($(this).val());
    }).keydown(function () {
        onFilterCountChanged($(this).val());
    }).keyup(function () {
        onFilterCountChanged($(this).val());
    }).bind('paste', function () {
        onFilterCountChanged($(this).val());
    })

});