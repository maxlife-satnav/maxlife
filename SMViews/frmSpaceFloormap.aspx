﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <link href="../BootStrapCSS/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/bootstrap-select.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/amantra.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/datepicker.min.css" rel="stylesheet" />
    <link href="../BootStrapCSS/jquery-ui.min.css" rel="stylesheet" />
    <link href="../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .glyphiconcol {
            color: #5bc0de;
        }
    </style>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <form id="form1" runat="server">
                        <div class="clearfix"></div>
                        <br />
                        <div id="maptoggle">
                            <div class="form-group">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-info" id="divspace">
                                        <div class="panel-heading">
                                            <div>
                                                <label id="lblHead"></label>
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="float: right"><span class='glyphicon glyphicon-search'></span>&nbsp;Advanced Search</a>
                                                <a id="btn1" style="float: right; padding-right: 30px; cursor: pointer;"><span class='glyphicon glyphicon-refresh'></span>&nbsp;Refresh </a>
                                            </div>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse out">
                                            <div class="panel-body">
                                                <label class="col-md-1 control-label">Floor:</label>
                                                <div class="col-md-4">
                                                    <select id="ddlfloors" class="selectpicker" data-live-search="true"></select>
                                                </div>
                                                <label class="col-md-1 control-label">Search By:</label>
                                                <div class="col-md-2">
                                                    <select id="ddlsearchfor" class="selectpicker" data-live-search="true">
                                                    </select>
                                                </div>
                                                <label class="col-md-1 control-label">Sub-Item:</label>
                                                <div class="col-md-2">
                                                    <select id="ddlsearchforsub" class="selectpicker" data-live-search="true">
                                                    </select>
                                                </div>
                                                <div>
                                                    <button id="btnSearch" value="Filter" style="margin-left: -5px;" class="btn btn-primary custom-button-color">Submit </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divmapload" style="display:none;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <img id="img1" src="../BootStrapCSS/images/loading_1.gif" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <!-- <iframe id="midFrame" height="400px" width="100%" style='margin: 0; margin-left: 0px'
                                    frameborder="0" scrolling="no"></iframe>-->
                                    <div id="main">
                                        <div id="heatmapArea"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row panel-primary">
                                <div role="tabpanel">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist" style="background-color: #F2DEDE;">
                                        <li role="presentation" class="active"><a href="#Legend" aria-controls="Legend" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;Legends</a></li>
                                        <li role="presentation"><a href="#SeatType" aria-controls="SeatType" role="tab" data-toggle="tab"><i class="fa fa-wheelchair"></i>&nbsp;Seat Type Information</a></li>
                                        <li role="presentation"><a href="#Vertical" aria-controls="Vertical" role="tab" data-toggle="tab"><i class="fa fa-building-o"></i>&nbsp;<label for="lblSelVertical1"></label>&nbsp;Details</a></li>
                                        <li role="presentation"><a href="#Searchemp" aria-controls="Searchemp" role="tab" data-toggle="tab"><i class="fa fa-users"></i>&nbsp;Search Employee</a></li>
                                        <li role="presentation"><a href="#Consolidated" aria-controls="Consolidated" role="tab" data-toggle="tab"><i class="fa fa-bar-chart"></i>&nbsp;Consolidated Report</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="Legend">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <h4>
                                                                    <label class="col-md-12 control-label">Status</label></h4>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image8" src="../images/chair_green.gif" />
                                                                        Available                                                         
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image11" src="../images/Chair_Black.gif" />
                                                                        Reserved
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image12" src="../images/Chair_Blue.gif" />
                                                                        Allocated but Vacant
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image13" src="../images/chair_red.gif" />
                                                                        Allocated and Occupied
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image14" src="../images/chair_blink.gif" />
                                                                        Duplicate Values
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 text-left">
                                                                <h4>
                                                                    <label>Seat Type</label></h4>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image9" src="http://166.78.47.48:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=multi_seat_type" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 text-left">
                                                                <h4>
                                                                    <label>Space Type</label></h4>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image10" src="http://166.78.47.48:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=multi_space_type" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 text-left">
                                                                <h4>
                                                                    <label class="col-md-12 control-label">Assets</label></h4>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image1" src="http://devv4.a-mantra.com/images/p1.png" />
                                                                        Printer                                                         
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image2" src="http://devv4.a-mantra.com/images/com.png" />
                                                                        Computer
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 control-label">
                                                                        <img id="Image3" src="http://devv4.a-mantra.com/images/tel.png" />
                                                                        Telephone
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="SeatType">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <table id="tblseattype" class="table table-condensed table-bordered table-hover table-striped">
                                                            <tr>
                                                                <th>Seat Type</th>
                                                                <th>Count</th>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Vertical">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <table id="tblVertical" class="table table-condensed table-bordered table-hover table-striped">
                                                            <tr>
                                                                <th>
                                                                    <label for="lblSelVertical1" style="font-weight: bold;"></label>
                                                                </th>
                                                                <th>Occupied Count</th>
                                                                <th>Allocated Count</th>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Searchemp">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <strong>
                                                                <label class="col-md-5">Field</label></strong>
                                                            <div class="col-md-7">
                                                                <select id="drpdwnCategory" class="selectpicker" data-live-search="true">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <strong>
                                                                <label class="col-md-5">Value</label></strong>
                                                            <div class="col-md-7">
                                                                <select id="drpdwnValue" class="selectpicker" data-live-search="true">
                                                                </select>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <a id="ImgFetch" class="btn btn-primary custom-button-color">
                                                        <span class='glyphicon glyphicon-search'></span>&nbsp;Search User
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="Consolidated">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tblconsolidate">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="alloctoggle" style="display: none;">

                             <div class="row" id="divloading">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <img id="imgloading" src="../BootStrapCSS/images/loading_1.gif" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                            <div id="empdetails" class="form-group">
                                <div class="panel-group" id="searchemppnl">
                                    <label for="lblMsg" class="control-label" style="color: red;"></label>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Allocate to Employee / Vertical
                                                <a id="A1" style="float: right; padding-right: 30px; cursor: pointer;" onclick="maptoggle_marker(true)"><span class='glyphicon glyphicon-backward'></span>&nbsp;Back </a>

                                            </h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                                            <div class="col-md-7">
                                                                <div class='input-group date datepicker' id='extdate'>
                                                                    <input name='txtFrmDate' type='text' id='txtFrmDate' class='form-control' required="required" />
                                                                    <span class='input-group-addon'><span class='fa fa-calendar' onclick="setup('extdate')"></span></span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>

                                                            <div class="col-md-7">
                                                                <div class='input-group date datepicker' id='indate'>
                                                                    <input name='txtToDate' type='text' id='txtToDate' class='form-control' required="required" />
                                                                    <span class='input-group-addon'><span class='fa fa-calendar' onclick="setup('indate')"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="trshift" class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">Shift<span style="color: red;">*</span></label>

                                                            <div class="col-md-7">

                                                                <select id="ddlshift" class="selectpicker" data-live-search="true">
                                                                    <option value='--Select--'>--Select--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="trTimeSlot" class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">From:<span style="color: red;">*</span></label>
                                                            <div class="col-md-7">
                                                                <select id="starttimehr" class="selectpicker" data-live-search="true" required="required">
                                                                    <option value="Hr" selected>Hr</option>
                                                                </select>
                                                                <select id="starttimemin" class="selectpicker" data-live-search="true" required="required">
                                                                    <option value="Min">Min</option>
                                                                    <option value="00">00</option>
                                                                    <option value="15">15</option>
                                                                    <option value="30">30</option>
                                                                    <option value="45">45</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">To:<span style="color: red;">*</span></label>
                                                            <div class="col-md-7">
                                                                <select id="endtimehr" class="selectpicker" data-live-search="true" required="required">
                                                                    <option value="Hr" selected>Hr</option>
                                                                </select>

                                                                <select id="endtimemin" class="selectpicker" data-live-search="true" required="required">
                                                                    <option value="Min">Min</option>
                                                                    <option value="00">00</option>
                                                                    <option value="15">15</option>
                                                                    <option value="30">30</option>
                                                                    <option value="45">45</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label for="lblSelVertical" class="col-md-5 control-label"><span style="color: red;">*</span></label>
                                                            <div class="col-md-7">
                                                                <select id="ddlVertical" class="selectpicker" data-live-search="true" required="required">
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">Employee </label>
                                                            <div class="col-md-7">
                                                                <input type='text' id='txtEmpName' class='form-control' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12 text-right">
                                                                <button type='button' id="Button1" class="btn btn-primary custom-button-color">Allocate to Vertical</button>
                                                                <button type='button' id="btnallocate" class="btn btn-primary custom-button-color">Check Employee Details</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                           

                                            <div id="divemployeedetails" class="panel panel-info">
                                                <div class="panel-heading">Employee Details</div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h5><span class="glyphicon glyphicon-user glyphiconcol"></span>
                                                                &nbsp;&nbsp;Employee ID :&nbsp;<label for="lblEmpSearchId"></label>
                                                                /
                                                                    <label for="lblEmpName"></label>
                                                            </h5>
                                                            <h5><span class="glyphicon glyphicon-certificate glyphiconcol"></span>
                                                                &nbsp;&nbsp;Designation :&nbsp;<label for="lblDesignation"></label></h5>
                                                            <h5><span class="glyphicon glyphicon-bookmark glyphiconcol"></span>
                                                                &nbsp;&nbsp;<label for="lblSelVertical1"></label>
                                                                :&nbsp;<label for="lblAUR_VERT_CODE"></label></h5>
                                                            <h5><span class="glyphicon glyphicon-tag glyphiconcol"></span>
                                                                &nbsp;&nbsp;Existing Space ID :&nbsp;
                                                                <label for="hypSpaceId" style="font: bold; color: red"></label>
                                                            </h5>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h5><span class="glyphicon glyphicon-envelope glyphiconcol"></span>
                                                                &nbsp;&nbsp;Email ID :&nbsp;<label for="lblEmpEmailId"></label></h5>
                                                            <h5><span class="glyphicon glyphicon-bookmark glyphiconcol"></span>
                                                                &nbsp;&nbsp;Department :&nbsp;<label for="lblDepartment"></label></h5>
                                                            <h5><span class="glyphicon glyphicon-tag glyphiconcol"></span>
                                                                &nbsp;&nbsp;<label for="lblSelProject"></label>
                                                                :&nbsp;<label for="lblPrjCode"></label></h5>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 text-right">
                                                            <button type='button' id="btnSubmit" name="btnSubmit" class="btn btn-primary custom-button-color">Allocate to Employee</button>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-danger" id="divgrid">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Allocated / Occupancy Details
                                            <a id="A2" style="float: right; padding-right: 30px; cursor: pointer;" onclick="maptoggle_marker(true)"><span class='glyphicon glyphicon-backward'></span>&nbsp;Back </a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div style="display: none;">
                                            <table id="gdavail" class="table table-condensed table-bordered table-hover table-striped">
                                                <tr>
                                                    <th>Spaces Available</th>
                                                    <th>Seat Type</th>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="col-md-12">
                                            <table id="gvSharedEmployee" class="table table-condensed table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Employee Id</th>
                                                    <th>Employee Name</th>
                                                    <th>Email ID</th>
                                                    <th>Space ID</th>
                                                    <th>From Date</th>
                                                    <th>To Date</th>
                                                    <th>From Time</th>
                                                    <th>To Time</th>
                                                    <th>Employee Release</th>
                                                </tr>
                                                    </thead>
                                            </table>
                                        </div>

                                        <div class="col-md-12">
                                            <table id="gvAllocatedSeats" class="table table-condensed table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th style="display: none;">Reqid</th>
                                                    <th>Vertical</th>
                                                    <th>Space ID</th>
                                                    <th>From Date</th>
                                                    <th>To Date</th>
                                                    <th>From Time</th>
                                                    <th>To Time</th>
                                                    <th>
                                                        <label for="lblSelVertical1" style="font-weight: bold;"></label>
                                                        Release</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>

                                        <div class="col-md-12">
                                            <h4><strong>Tagged Asset Details</strong></h4>
                                        </div>
                                        <div class="col-md-12">
                                            <table id="gvAssets" class="table table-condensed table-bordered table-hover table-striped">
                                                <tr>
                                                    <th>Asset Code</th>
                                                    <th>Asset Name</th>
                                                    <th>Space Asset Id</th>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<%--<script src="//code.jquery.com/jquery-1.10.1.min.js"></script>--%>
<script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
<script src="../BootStrapCSS/Scripts/jquery-ui.min.js"></script>
<script src="../BootStrapCSS/Scripts/bootstrap.min.js"></script>
<script src="../BootStrapCSS/Scripts/bootstrap-datepicker.min.js"></script>
<script src="../BootStrapCSS/Scripts/wz_tooltip.min.js"></script>
<script src="../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>
<script src="../BootStrapCSS/Scripts/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="../WorkSpace/GIS/MAP_GIS/ol/OpenLayers.js"></script>
<script src="../BootStrapCSS/Scripts/maploader.js"></script>
<script src="../BootStrapCSS/Scripts/AllocateSeatToEmployeeVertical.js"></script>

<script>
    function GetParameterValues(param) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    }

    $(document).ready(function () {
        var myApp;
        var ddldata;
        myApp = myApp || (function () {
            var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
            return {
                showPleaseWait: function () {
                    pleaseWaitDiv.modal();
                },
                hidePleaseWait: function () {
                    pleaseWaitDiv.modal('hide');
                },

            };
        })();

        GetlblVertical();
        //var url = "HtmlPage.aspx?lcm_code=" + GetParameterValues('lcm_code') + "&twr_code=" + GetParameterValues('twr_code') + "&flr_code=" + GetParameterValues('flr_code');
        //$('#midFrame').attr("src", url);
        var floorflag = false;
        ddldata = { lcm_code: GetParameterValues('lcm_code'), flr_code: GetParameterValues('flr_code'), twr_code: GetParameterValues('twr_code'), mode: 1 };
        bindspacetypetabl(ddldata);

        $.getJSON('../api/Space_mapAPI/GetallFloorsbyuser', function (result) {
            $('#ddlfloors option').remove();
            $('#ddlfloors').append($("<option></option>").val("--Select--").html("--Select--"));
            $.each(result, function (key, value) {
                $('#ddlfloors').append($("<option></option>").val(value.FLR_ID).html(value.FLR_NAME));
            });
            $('#ddlfloors').selectpicker('refresh');
            loadfloordetails(GetParameterValues('flr_code'), 1);
        });

        $('#ddlfloors').on('change', function (e) {
            $('#ddlsearchforsub option').remove()
            $('#ddlsearchforsub').selectpicker('refresh');
            $('#ddlsearchfor').val('');
            $('#ddlsearchfor').selectpicker('refresh');
            floorflag = true;
        });


        $.getJSON('../api/Space_mapAPI/GetallFilterbyItem', function (result) {
            $('#ddlsearchfor option').remove()
            $('#ddlsearchfor').append($("<option></option>").val("0").html("--Select--"));
            $.each(result, function (key, value) {
                $('#ddlsearchfor').append($("<option></option>").val(value.SSQ_ID).html(value.SSQ_NAME));
            });
            $('#ddlsearchfor').selectpicker('refresh');
        });

        $('#ddlsearchfor').on('change', function (e) {
            var dataObject = { Item: $('#ddlsearchfor').val(), lcm_code: GetParameterValues('lcm_code'), flr_code: $('#ddlfloors').val() };

            $.post('../api/Space_mapAPI/GetallFilterbySubItem', dataObject, function (result) {
                $('#ddlsearchforsub option').remove()
                //$('#ddlsearchforsub').append($("<option></option>").val("0").html("--Select--"));
                if ($('#ddlsearchfor').val() ==1)
                    $('#ddlsearchforsub').append($("<option></option>").val("--All--").html("--All--"));
                $.each(result, function (key, value) {
                    $('#ddlsearchforsub').append($("<option></option>").val(value.SUBITEM_ID).html(value.SUBITEM_NAME));
                });
                $('#ddlsearchforsub').selectpicker('refresh');
            });
        });

        $('#btn1').on('click', function (e) {
            //var url = "HtmlPage.aspx?lcm_code=" + GetParameterValues('lcm_code') + "&twr_code=" + GetParameterValues('twr_code') + "&flr_code=" + GetParameterValues('flr_code');
            //$('#midFrame').attr("src", url);              
            //ddlsearchforsub clear
            $('#ddlsearchforsub option').remove();
            $('#ddlsearchforsub').append($("<option></option>").val("0").html("--Select--"));
            var markerurl = '../api/Space_mapAPI/GetMapdetailsbyFilters';
            var parameters = { lcm_code: GetParameterValues('lcm_code'), flr_code: $('#ddlfloors').val(), twr_code: GetParameterValues('twr_code') };
            $('#ddlsearchforsub').val('');
            $('#ddlsearchfor').val('');
            $('#ddlsearchforsub').selectpicker('refresh');
            $('#ddlsearchfor').selectpicker('refresh');
            loadmarkers(markerurl, parameters);
            ddldata = { lcm_code: GetParameterValues('lcm_code'), flr_code: $('#ddlfloors').val(), twr_code: GetParameterValues('twr_code'), mode: 0 };
            bindspacetypetabl(ddldata);
            bindvaluesbycategory($('#ddlfloors').val(), 0)
            resetzoom(3);
            return false;
        });

        $('#btnSearch').on('click', function (e) {
            //var url = "HtmlPage.aspx?lcm_code=" + GetParameterValues('lcm_code') + "&twr_code=" + GetParameterValues('twr_code') + "&flr_code=" + $('#ddlfloors').val() + "&Item=" + $('#ddlsearchfor').val() + "&subitem=" + $('#ddlsearchforsub').val();
            //$('#midFrame').attr("src", url);
            $("#divmapload").show();
            if (navigator.userAgent.search("Firefox") >= 0) {
                $("#divmapload").show();
            }
            else if (navigator.userAgent.search("Chrome") > 0) {
                setTimeout(function () { $("#divmapload").show(); }, 200);
            }
            var bboxurl, markerurl, parameters;
            if (floorflag) {
                bboxurl = '../api/Space_mapAPI/GetFloorIDBBoxByID?lcm_code=' + GetParameterValues('lcm_code') + '&twr_code=' + GetParameterValues('twr_code') + '&flr_code=' + $('#ddlfloors').val()
                markerurl = '../api/Space_mapAPI/GetMapdetailsbyFilters'
                parameters = { Item: $('#ddlsearchfor').val(), subitem: $('#ddlsearchforsub').val(), lcm_code: GetParameterValues('lcm_code'), flr_code: $('#ddlfloors').val(), twr_code: GetParameterValues('twr_code') };
                $('#heatmapArea').empty();               
                loadmap(bboxurl, markerurl, parameters);               
            }
            else {              
                markerurl = '../api/Space_mapAPI/GetMapdetailsbyFilters'
                //alert("L" + markerurl.length);
                parameters = { Item: $('#ddlsearchfor').val(), subitem: $('#ddlsearchforsub').val(), lcm_code: GetParameterValues('lcm_code'), flr_code: $('#ddlfloors').val(), twr_code: GetParameterValues('twr_code') };
                loadmarkers(markerurl, parameters);
            }
            

            ddldata = { lcm_code: GetParameterValues('lcm_code'), flr_code: $('#ddlfloors').val(), twr_code: GetParameterValues('twr_code'), mode: 0 };
            bindspacetypetabl(ddldata);
            bindvaluesbycategory($('#ddlfloors').val(), 0)
            loadfloordetails($('#ddlfloors').val(), 0)
            floorflag = false;
            $("#divmapload").hide();
            if (navigator.userAgent.search("Firefox") >= 0) {
                $("#divmapload").hide();
            }
            else if (navigator.userAgent.search("Chrome") > 0) {
                setTimeout(function () { $("#divmapload").hide(); }, 300);
            }
            return false;
        });

        $.getJSON('../api/Space_mapAPI/GetQueryAnalycat', function (result) {
            $('#drpdwnCategory option').remove()
            //$('#drpdwnCategory').append($("<option></option>").val("--Select--").html("--Select--"));
            $.each(result, function (key, value) {
                $('#drpdwnCategory').append($("<option></option>").val(value.smq_Id).html(value.smq_field));
            });
            $('#drpdwnCategory').selectpicker('refresh');
            bindvaluesbycategory(GetParameterValues('flr_code'), 1)
        });


        $('#drpdwnCategory').on('change', function (e) {
            bindvaluesbycategory(GetParameterValues('flr_code'), 1);
        });

        //Search User Click->  Image click
        $('#ImgFetch').on('click', function (e) {
            $.post('../api/Space_mapAPI/GetMapbyQueryAnalyser', { CatValue: $('#drpdwnValue').val() }, function (result) {

                var img;
                if (result[0].aur_imagepath == null)
                    img = "../userprofiles/default-user-icon-profile.png";
                else img = "../userprofiles/" + result[0].aur_imagepath;
                var spaceinfo =
    "<div><table class='seatdetails' ><tbody>" +
                    "<tr><td colspan='7' align='center'><h4><i class='fa fa-wheelchair'></i> Seat Information</h4></td></tr>" +
                    "<tr><td ><b>Space Id</b></td><td colspan='5'> :" + result[0].spc_id + "</td>" +
                "<td rowspan='7'><img src=" + img + " Height='140px' Width='120px'></td></tr>" +
		"<tr><td><b>Location</b></td><td> : " + result[0].lcm_name + "</td></tr>" +
        "<tr><td><b>Tower</b></td><td> : " + result[0].TWR_NAME + "</td></tr>" +
		"<tr><td><b>Floor</b></td><td> : " + result[0].flr_name + "</td></tr> " +
		"<tr><td><b>Emp Name/Id</b></td><td> : " + result[0].aur_known_as + " / " + result[0].AUR_ID + "</td></tr>" +
		"<tr><td><b>Email</b></td><td > : " + result[0].AUR_EMAIL + "</td></tr>" +
		"<tr><td><b>Vertical</b></td><td> : " + result[0].VER_NAME + "</td></tr></tbody>" +
        "</table></div>"

                addMarker(new OpenLayers.LonLat(result[0].lat, result[0].lon), AutoSizeFramedCloud, spaceinfo, true, true, 'blink', 7, result[0].spc_id);
                setzoomlevel(new OpenLayers.LonLat(result[0].lat, result[0].lon), 1);
            });
        });

        // binding table data in bottom panels
    });

    function bindvaluesbycategory(FLR_CODE, MODE) {
        var ddlcategory = { category: $('#drpdwnCategory').val(), lcm_code: GetParameterValues('lcm_code'), flr_code: FLR_CODE, twr_code: GetParameterValues('twr_code'), mode: MODE };

        $.post('../api/Space_mapAPI/GetQueryAnalyValuebyCat', ddlcategory, function (result) {
            $('#drpdwnValue option').remove()
            //$('#drpdwnCategory').append($("<option></option>").val("--Select--").html("--Select--"));
            $.each(result, function (key, value) {
                $('#drpdwnValue').append($("<option></option>").val(value.Value).html(value.DisValue));
            });
            $('#drpdwnValue').selectpicker('refresh');
        });
    }


    function bindspacetypetabl(ddldata) {
        var modeldata = ddldata;
        $.post('../api/Space_mapAPI/GetSpaceTypedetails', modeldata, function (result) {
            $('#tblseattype td').remove();
            var table = $('#tblseattype');
            for (var i = 0; i < result.length; i++) {
                table.append("<tr>" +
                    "<td>" + result[i].SPC_TYPE + "</td>" +
                    "<td>" + result[i].CNT + "</td>" +
                    "</tr>");
            }
        });

        $.post('../api/Space_mapAPI/GetVerticalAllocation', modeldata, function (result) {
            $('#tblVertical td').remove();
            var table = $('#tblVertical');
            for (var i = 0; i < result.length; i++) {
                table.append("<tr>" +
                    "<td>" + result[i].VER_NAME + "</td>" +
                    "<td>" + result[i].Allocated + "</td>" +
                    "<td>" + result[i].Occupied + "</td>" +
                    "</tr>");
            }
        });

        $.post('../api/Space_mapAPI/GetConsolidatedreport', modeldata, function (result) {
            $('#tblconsolidate').empty();
            var table = $('#tblconsolidate');
            for (var i = 0; i < result.length; i++) {
                table.append(result[i].SUMMARY);
            }
        });

    }

    function loadfloordetails(flr_code, MODE) {
        $.post('../api/Space_mapAPI/Getfloordetails', { lcm_code: GetParameterValues('lcm_code'), flr_code: flr_code, mode: MODE }, function (result) {
            $('#lblHead').text(result[0].HEADING);
            $('#ddlfloors').val(result[0].FLR_ID);
            $('#ddlfloors').selectpicker('refresh');
        });
    }

</script>


