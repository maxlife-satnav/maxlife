<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmForgetPwd.aspx.vb" Inherits="frmForgetPwd" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>    
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="login-div">
                        <form id="form1" runat="server">
                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <asp:TextBox ID="tenantID" runat="server" type="text" class="form-control" placeholder="Company Id"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtnt" runat="server" ControlToValidate="tenantID"
                                    Display="None" ErrorMessage="Please Enter Company Id" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                            </div>

                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <asp:TextBox ID="txtUsrId" runat="server" type="text" class="form-control" placeholder="User Id"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvusr" runat="server" ControlToValidate="tenantID"
                                    Display="None" ErrorMessage="Please Enter User Id" ValidationGroup="Val1"> </asp:RequiredFieldValidator>

                            </div>
                            <div>
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color pull-right" Text="Submit" style="margin-bottom: 10px" ValidationGroup="Val1" />
                            </div>
                            <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger pull-left"
                                ForeColor="Red" ValidationGroup="Val1" />                           
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
