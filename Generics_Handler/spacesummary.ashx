﻿<%@ WebHandler Language="VB" Class="spacesummary" %>
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports clsJson
Imports clsSubSonicCommonFunctions

Public Class spacesummary : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response

        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strCity As String = request("Mcity")
        Dim strlocation As String = request("Mlocation")
        Dim strfromdate As String = request("Mfromdate")
        Dim strtodate As String = request("Mtodate")
        Dim strfromtimehh As String = request("Mfromtimehh")
        Dim strfromtimemm As String = request("Mfromtimemm")
        Dim strtotimehh As String = request("Mtotimehh")
        Dim strtotimemm As String = request("Mtotimemm")
        Dim strbcp As Int32 = request("Mbcp")
        Dim strshared As String = request("Mshared")
        Dim strvertical As String = request("Mvertical")
        Dim strnoofselected As Int32 = request("Mnoofselected")
        
        Dim totalRecords As Integer
        
        
        Dim ftime As String = ""
        Dim ttime As String = ""

        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"


        If strfromtimehh <> "Hr" Then
            StartHr = strfromtimehh
        End If
        If strfromtimemm = "Min" Then
            StartMM = strfromtimemm
        End If
        If strtotimehh <> "Hr" Then
            EndHr = strtotimehh
        End If
        If strtotimemm = "Min" Then
            EndMM = strtotimemm
        End If




        If strshared = "1" Then
            ftime = "00:00"
            ttime = "23:30"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If


        
        Dim clsJson As Collection(Of clsJson) = GetspaceSummary(strCity, strlocation, strfromdate, strtodate, ftime, ttime, strbcp, strshared, strvertical, strnoofselected, numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
        Dim output As String = BuildJQGridResults(clsJson, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsJson As Collection(Of clsJson), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String

        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Space1 As clsJson In clsJson
            Dim row As New JQGridRow()

            row.cell = New String(7) {}
            row.cell(0) = Space1.strlcm_name.ToString()
            row.cell(1) = Space1.strSpaceID.ToString()
            row.cell(2) = Space1.strspclayer.ToString()
            row.cell(3) = Space1.strspctype.ToString()
            row.cell(3) = Space1.strspccarved.ToString()
            row.cell(4) = Space1.strspcportstatus.ToString()
            row.cell(5) = Space1.strspcporttype.ToString()
            row.cell(6) = Space1.strbcpseattype.ToString()
            row.cell(7) = Space1.strmcpseat.ToString()
            row.cell(8) = Space1.strportnumber.ToString()
            row.cell(9) = Space1.strAreatype.ToString()
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords / numberOfRows
        result.records = totalRecords
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function
    
    
    Private Function GetspaceSummary(ByVal strCity As String, ByVal strlocation As String, ByVal strfromdate As String, ByVal strtodate As String, ByVal ftime As String, ByVal ttime As String, ByVal strbcp As Integer, ByVal strshared As String, ByVal strvertical As String, ByVal strnoofselected As Integer, ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of clsJson)
        Dim users As New Collection(Of clsJson)()
        
        
     
        
        
        
        Dim param(14) As SqlParameter
        param(0) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = strCity
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = strlocation
        param(2) = New SqlParameter("@frmdate", SqlDbType.DateTime)
        param(2).Value = strfromdate
        param(3) = New SqlParameter("@toDate", SqlDbType.DateTime)
        param(3).Value = strtodate
        param(4) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        param(4).Value = ftime
        param(5) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        param(5).Value = ttime
        param(6) = New SqlParameter("@bcp", SqlDbType.Int)
        param(6).Value = strbcp
        param(7) = New SqlParameter("@shared", SqlDbType.NVarChar, 50)
        param(7).Value = strshared
        param(8) = New SqlParameter("@vertical", SqlDbType.NVarChar, 50)
        param(8).Value = strvertical
        param(9) = New SqlParameter("@numberofseatsreq", SqlDbType.Int)
        param(9).Value = strnoofselected
        param(10) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(10).Value = pageIndex
        param(11) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(11).Value = sortColumnName
        param(12) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(12).Value = sortOrderBy
        param(13) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(13).Value = Convert.ToInt32(numberOfRows)
        param(14) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(14).Value = 20
        param(14).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("USP_GET_LOCATION_SEATS_PART1_TEST", param)
        
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim user As clsJson
            user = New clsJson()
            user.strlcm_name = Convert.ToString(ds.Tables(0).Rows(i).Item("SPC_BDG_ID"))
            user.strSpaceID = Convert.ToString(ds.Tables(0).Rows(i).Item("spc_id"))
            user.strspclayer = Convert.ToString(ds.Tables(0).Rows(i).Item("spc_layer"))
            user.strspctype = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_TYPE"))
            user.strspccarved = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_CARVED"))
            user.strspcportstatus = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_PORT_STATUS"))
            user.strspcporttype = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_PORT_TYPE"))
            user.strbcpseattype = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_BCP_SEAT_TYPE"))
            user.strmcpseat = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_MCP_SEAT"))
            user.strportnumber = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_PORT_NUMBER"))
            user.strAreatype = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_AREA_TYPE"))
          
            users.Add(user)
            ' totalRecords = CInt(ds.Tables(1).Rows(0).Item("TotalRecords"))
        Next
         
        Return users
    End Function

End Class