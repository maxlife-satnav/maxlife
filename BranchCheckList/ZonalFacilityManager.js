﻿app.service("ZonalFacilityManagerServices", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.getZFMdetails = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ZonalFacilityManager/getZFMdetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.InsertZFMList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ZonalFacilityManager/InsertZFMList', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };



}]);

app.controller('ZonalFacilityManagerController', function ($scope, $q, $location, ZonalFacilityManagerServices, UtilityService, $filter) {


    //getZFMdetails
    $scope.CheckList = {};
    $scope.DailyTList = {};
    $scope.CheckList.ReviewingDate = [];
    $scope.ReviewingDate = [];
    $scope.CheckList.ReviewingDate = moment(new Date()).format('DD/MM/YYYY');

    ZonalFacilityManagerServices.getZFMdetails().then(function (response) {
        var div = document.getElementById("btndev");
        div.style.display = "none";

        if (response.data != null) {
            $scope.LoadTableData(response);
        }
        else {
            showNotification('error', 8, 'bottom-right', response.Message);
        }
    }, function (response) {
        progress(0, '', false);


    });
    debugger;

    //load table data
    $scope.LoadTableData = function (response) {

        if (response.data[0] != null) {
            var div = document.getElementById("btndev");
            div.style.display = "block";

            document.getElementById("AuthorityName").value = response.data[0].ZFM_USER;
            document.getElementById("LocationsCount").value = response.data[0].LocationCount;
            document.getElementById("InspectionsCount").value = response.data[0].IntialCount;

            var table_body = '<div class="container"><div class="table-responsive">';

            for (i = 0; i < response.data.length; i++) {
                var n = i + 1;
                var color;

                lcolor = "background-color:cadetblue;";
                color = "background-color:darkslategray;",
                    tdcolor = "background-color:dimgray;";

                var BCL_ID = response.data[i]["BCL_ID"];

                table_body += '<table class="table table-bordered" id="example"><head><tr style=' + lcolor + '  href="#demo' + n + '"  data-toggle="collapse" ><th style=' + lcolor + ' class="col-md-3 col-sm-6 col-xs-12"><center><b>';
                table_body += response.data[i]["BCL_LOC_NAME"];
                table_body += '</b></center></th></tr></head>';
                table_body += '<tbody><tr id="demo' + n + '" class="collapse">';
                table_body += '<td>';

                if (response.data1 != null) {
                    for (k = 0; k < response.data1.length; k++) {
                        if (BCL_ID == response.data1[k]["BCL_ID"]) {
                            var category = response.data1[k]["BCLD_CAT_CODE"];

                            table_body += '<table class="table table-bordered"><head><tr style=' + color + '  href="#Cat' + k + n + '"  data-toggle="collapse" ><th style=' + color + ' class="col-md-12 col-sm-6 col-xs-12"><center><b>';
                            table_body += response.data1[k]["BCLD_CAT_NAME"];
                            table_body += '</b></center></th></tr></head>';
                            table_body += '<tbody><tr id="Cat' + k + n + '" class="collapse">';
                            table_body += '<td>';

                            if (response.data2 != null) {
                                table_body += '<table class="table table-bordered"><tbody> <tr><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Sub Category</b></th><th  style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Inspection Comments</b></th><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Zonal FM Actions</b></th><th style= ' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Upload Proofs</b></th></tr>';

                                for (j = 0; j < response.data2.length; j++) {
                                    if (category == response.data2[j]["BCLD_CAT_CODE"]) {

                                        var SubCatName = response.data2[j]["BCLD_SUBCAT_NAME"];
                                        var Comments = response.data2[j]["BCLD_INSP_COMENTS"];
                                        var ID = response.data2[j]["ID"];
                                        var File = response.data2[j]["File"];

                                        var Files = "";
                                        if (File != "") {
                                            var a = File.split(',');
                                            for (l = 0; l < a.length; l++) {
                                                var b = a[l].split('<br>');
                                                var DownLoadFileName = b[0];
                                                var MainfileName = b[1];
                                                Files = Files + '<a id=' + DownLoadFileName + ' onclick="FileDownload(this.id)" >' + MainfileName + '</a><br/>';
                                            }
                                        }

                                        if (j == 0) {
                                            $scope.DailyTList = [{ BCL_ID: BCL_ID, Location: response.data[i]["BCL_LOC_NAME"], CatName: response.data1[k]["BCLD_CAT_NAME"], SubCatName: SubCatName, ID: ID, ZFM_Action: 1, ZFM_COMMENTS: "" }];

                                        }
                                        else {
                                            $scope.DailyTList.push({ BCL_ID: BCL_ID, Location: response.data[i]["BCL_LOC_NAME"], CatName: response.data1[k]["BCLD_CAT_NAME"], SubCatName: SubCatName, ID: ID, ZFM_Action: 1, ZFM_COMMENTS: "" });

                                        }

                                        table_body += '<tr>';
                                        table_body += '<td class="col-xs-3">';
                                        table_body += SubCatName;
                                        table_body += '</td>';

                                        table_body += '<td class="col-xs-3">';
                                        table_body += Comments;
                                        table_body += '</td>';

                                        table_body += '<td class="col-xs-3">';
                                        table_body += '<div><select class="Textbox"  name="Select' + ID + '" id="Select' + ID + '" onchange="selectfn(this.value)"><option value="1,' + ID + '">Validated</option> <option value="2,' + ID + '">Closed</option> <option value="3,' + ID + '">Repeated</option> <option value="4,' + ID + '">Locally Resolved</option></select><div>';
                                        table_body += '<div>&nbsp;</div><div><textarea class="Textbox" id="textarea_' + ID + '"  name="textarea_' + ID + '"  rows="1" cols="20"></textarea></div>';
                                        table_body += '</td>';
                                        table_body += '<td class="col-xs-3">';
                                        table_body += Files;
                                        table_body += '</td>';

                                        table_body += '</tr>';
                                    }

                                }


                                table_body += '</tbody ></table >';
                            }

                            table_body += '</td>';
                            table_body += '</tr>';
                            table_body += '</tbody ></table >';

                        }
                    }
                }

                table_body += '</td>';
                table_body += '</tr>';
                table_body += '</tbody ></table >';

            }
            table_body += '</div></div>';
            $('#tableDiv').html(table_body);
        }

    };

    //checked working condition   
    $scope.Selectfn = function (res) {
        debugger;

        if ($scope.DailyTList != null) {
            for (j = 0; j < $scope.DailyTList.length; j++) {
                if (res[1] == $scope.DailyTList[j].ID) {
                    $scope.DailyTList.push($scope.DailyTList[j].ZFM_Action = res[0]);

                    if (res[0] != 1) {
                        var ra = "textarea_" + $scope.DailyTList[j].ID;
                        document.getElementById(ra).value = "";
                    }
                }
            }
        }
    };

    $(function () {
        $("#Save").click(function () {
            var result = confirm("Areyou sure wants to continue ?");

            if (result == true) {
                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].ZFM_Action == "1") {
                            var ra = "textarea_" + $scope.DailyTList[j].ID;
                            var x = document.getElementById(ra).value;
                            $scope.DailyTList.push($scope.DailyTList[j].ZFM_COMMENTS = x);
                        }
                    }
                }

                $scope.data =
                {
                    ReviewingDate: $scope.CheckList.ReviewingDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                    SaveFlag: '2',
                    Seldata: $scope.DailyTList
                };

                ZonalFacilityManagerServices.InsertZFMList($scope.data).then(function (response) {
                    if (response != null) {
                        //alert("Save Draft Successfully");
                    }
                }, function (error) {
                    console.log(error);
                });

                showNotification('Save Draft', 8, 'bottom-right', "Save Draft");
                var div = document.getElementById("btndev");
                div.style.display = "block";
            }
        });

        $("#Submit").click(function () {
            var result = confirm("Areyou sure wants to continue ?");

            if (result == true) {
                debugger;

                var flag = 0;
                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].ZFM_Action == "1") {
                            var ra = "textarea_" + $scope.DailyTList[j].ID;
                            var x = document.getElementById(ra).value;
                            $scope.DailyTList.push($scope.DailyTList[j].ZFM_COMMENTS = x);
                        }
                    }
                }

                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].ZFM_Action == "1") {
                            if ($scope.DailyTList[j].ZFM_COMMENTS == "") {
                                alert("Location :" + $scope.DailyTList[j].Location + "<br/>" + "Cat Name :" + $scope.DailyTList[j].CatName + "<br/>" + "Sub Cat Name :" + $scope.DailyTList[j].SubCatName + "<br/>" + "Please enter Comments");
                                flag = 1;
                                break;
                            }
                        }
                    }

                    if (flag == 0) {

                        $scope.data =
                        {
                            ReviewingDate: $scope.CheckList.ReviewingDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                            SaveFlag: '3',
                            Seldata: $scope.DailyTList
                        };

                        ZonalFacilityManagerServices.InsertZFMList($scope.data).then(function (response) {
                            console.log(response);
                            if (response != null) {
                            }
                        }, function (error) {
                            console.log(error);
                        });

                        var div = document.getElementById("btndev");
                        div.style.display = "none";

                        showNotification('success', 8, 'bottom-right', "Success");
                    }
                }
            }
        });
    });

});