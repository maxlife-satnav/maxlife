﻿app.service("CentralTeamReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CentralTeamReport/BindGrid', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});

app.controller('CentalTeamReportController', function ($scope, $q, $http, CentralTeamReportService, $timeout) {

    $scope.CentralTeamReport = {};
    $scope.CentralTeamReport.FromDate = moment().format('MM/DD/YYYY');


    $scope.columnDefs = [
        { headerName: "Security Name", field: "SecurityName", width: 190, cellClass: 'grid-align', width: 110 },
        { headerName: "Location Name", field: "LocationName", cellClass: 'grid-align', width: 110 },
        { headerName: "Zone", field: "Zone", cellClass: 'grid-align', width: 110 },
        { headerName: "SG Status", field: "SG_Status", cellClass: 'grid-align', width: 110 },
        { headerName: "Sub Categoery", field: "SubCategoery", cellClass: 'grid-align', width: 110 },
        { headerName: "Comments", field: "Comments", cellClass: 'grid-align', width: 200 },
        { headerName: "Created Date", field: "CreatedDate", cellClass: 'grid-align', width: 150 },
        { headerName: "ZFM Action", field: "ZFMAction", cellClass: 'grid-align', width: 110 },
        { headerName: "ZfM Comments", field: "ZfmComments", width: 190, cellClass: 'grid-align' },
        { headerName: "Central Team Actions", field: "CentralTeamActions", width: 190, cellClass: 'grid-align' },
        { headerName: "Central Team Comments", field: "CentralTeamComments", width: 190, cellClass: 'grid-align' },
        { headerName: "ZFM Updated Date", field: "ZFMUpdatedDate", width: 190, cellClass: 'grid-align' },
        { headerName: "Central TeamUpdated Date", field: "CentralTeamUpdatedDate", width: 190, cellClass: 'grid-align' },
    ];
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,

        showToolPanel: true

    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value)
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


    $scope.LoadData = function () {
     

            var params = {
                FromDate: $scope.CentralTeamReport.FromDate,
            };
            CentralTeamReportService.GetGriddata(params).then(function (data) {
                $scope.gridata = data;
                if ($scope.gridata == null) {
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, 'Loading...', false);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata[0]);
                    progress(0, 'Loading...', false);
                }
            });

    }


    $scope.selVal = "TODAY";
    $scope.rptDateRanges = function () {
        debugger;
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.CentralTeamReport.FromDate = moment().format('MM/DD/YYYY');
                $scope.CentralTeamReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.CentralTeamReport.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.CentralTeamReport.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.CentralTeamReport.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.CentralTeamReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.CentralTeamReport.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.CentralTeamReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.CentralTeamReport.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.CentralTeamReport.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.CentralTeamReport.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.CentralTeamReport.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
      
            
    $scope.GenerateFilterExcel = function () {
                progress(0, 'Loading...', true);

                var Filterparams = {

                    columnGroups: true,
                    allColumns: true,
                    onlySelected: false,
                    columnSeparator: ',',
                    fileName: "CentralTeamReport.csv"
                };
                $scope.gridOptions.api.exportDataAsCsv(Filterparams);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }

    $scope.GenReport = function () {
                progress(0, 'Loading...', true);
                $scope.GenerateFilterExcel();
            }


    $timeout(function () { $scope.LoadData() }, 500);

    })