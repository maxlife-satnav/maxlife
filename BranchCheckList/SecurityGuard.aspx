﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SecurityGuard.aspx.cs" Inherits="BranchCheckList_DesginAndJs_SecurityGuard" %>

<!DOCTYPE html>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script type="text/javascript" defer>     

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                maxDate: 'today'
            });
        };

        function OnlyNumeric(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


        function checkfn(data) {
            debugger;
                var scope = angular.element(document.getElementById('idForJS')).scope();
                var res = data.split(",");

                if (res[2] == 1) {
                    $("#textarea_" + res[0] + "").hide();
                    $("#files_" + res[0] + "").hide();
                    $("#Submit_" + res[0] + "").hide();
                }
                else {
                    $("#textarea_" + res[0] + "").show();
                    $("#files_" + res[0] + "").show();
                    $("#Submit_" + res[0] + "").show();
                }
                scope.checkfn(res);
           
        }
       
        function FilesSubmit(res) {
            var scope = angular.element(document.getElementById('idForJS')).scope();
            scope.Upload(res);
        }

        function FileDownload(FileName) {              
            window.location.href = "./Download.aspx?FileName=" + FileName;
        }
        

    </script>

    <style>
        .col-xs-3 selected {
            border-color: blue;
        }

        .has-error2 {
            border-style: solid;
            border-color: #ff0000;
        }

        .has-error3 {
        }

        .mystyle {
            border-color: red !important;
            border-width: 2px !important;
        }

        .thead {
            border-style: solid;
            border-color: #ff0000;
        }

        .highlight {
            background-color:red;
        }

        /*input[type='radio'], label {
            margin: 10px;
        }*/

        .clearBoth {
            clear: both;
        }

        .Fonts {
            font-size: 14px;
        }

        /*input {
            height: 35px;
            font-size: 15px;
        }*/
        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .list-inline {
            display: block;
        }

            .list-inline li {
                display: inline-block;
            }

                .list-inline li:after {
                    content: '|';
                    margin: 0 10px;
                }



        .radio1 :after {
            width: 15px;
            height: 15px;
            border-radius: 15px;
            top: -2px;
            left: -1px;
            position: relative;
            background-color: #d1d3d1;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid white;
        }

        .radio1:checked:after {
            width: 15px;
            height: 15px;
            border-radius: 15px;
            top: -2px;
            left: -1px;
            position: relative;
            background-color: lightseagreen;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid white;
        }

        .radio2:after {
            width: 15px;
            height: 15px;
            border-radius: 15px;
            top: -2px;
            left: -1px;
            position: relative;
            background-color: #d1d3d1;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid white;
        }

        .radio2:checked:after {
            width: 15px;
            height: 15px;
            border-radius: 15px;
            top: -2px;
            left: -1px;
            position: relative;
            background-color: red;
            content: '';
            display: inline-block;
            visibility: visible;
            border: 2px solid white;
        }
    </style>

    <%--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <%-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>--%>
    <%--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--%>
</head>
<body id="idForJS" data-ng-controller="SecurityGuardController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 42px;">
                            <h6 class="panel-title"><b>Security Guard Daily Checklist</b></h6>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">
                                <form id="Form1" name="frmDailyCheckList" <%--data-valid-submit="Creation()"--%> novalidate>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.LCM_NAME.$invalid}">
                                                <label class="control-label">Location Name <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Location" data-output-model="CheckList.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                                <input type="text" data-ng-model="CheckList.Location" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.URL_USR_ID.$invalid}">
                                                <label class="control-label">Reviewing Authority  <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Authority" data-output-model="CheckList.Authority" data-button-label="icon URL_USR_ID" data-item-label="icon URL_USR_ID"
                                                    data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                                <input   type="text" data-ng-model="CheckList.Authority" name="URL_USR_ID" style="display: none; width:100px;" required="" />
                                                <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.URL_USR_ID.$invalid" style="color: red">Please Select Reviewing Authority</span>
                                            </div>
                                        </div>
                                       
                                       <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.INSPECTOR.$invalid}">
                                                <label class="control-label">Inspected By <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Inspection" data-output-model="CheckList.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                                    data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                                <input type="text" data-ng-model="CheckList.Inspection" name="INSPECTOR" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.INSPECTOR.$invalid" style="color: red">Please Select Inspection By  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.SVR_FROM_DATE.$invalid}">
                                                    <label class="control-label">Inspection Date <span style="color: red;">*</span></label>
                                                    <div  class="input-group date" style="width: 150px" id='todate'>
                                                        <input type="text"  class="form-control" placeholder="mm/dd/yyyy" id="InspectionDate" name="InspectionDate" data-ng-model="CheckList.InspectionDate" required />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                        </span>
                                                    </div>
                                                    <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please Select Date</span>
                                                </div>
                                            </div>
                                         </div>
                                    <div class="row">
                                        
                                        
                                        <div class="col-md-3 col-sm-6 col-xs-12" >
                                            <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.BCL_ID.$invalid}">
                                                <label class="control-label">Inspection Id<span style="color: red;">*</span> </label>
                                                <div  isteven-multi-select data-input-model="RequestID" data-output-model="CheckList.RequestID" data-button-label="icon BCL_ID" data-item-label="icon BCL_ID"
                                                    data-on-item-click="getCheckListDetails()" data-on-select-all="getCheckListDetails()" data-on-select-none="getCheckListDetails()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                    
                                                </div>
                                                <input type="text" data-ng-model="CheckList.RequestID" name="BCL_ID" style="display: none" required="" /><%--<a href="CheckList.RequestID"></a>--%>
                                                <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.BCL_ID.$invalid" style="color: red">Please select Request ID </span>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                      <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Status </label><br />
                                            <input id="status" runat="server" Class="Textbox" readonly="readonly" />
                                            </div>  
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12" align="right">
                                            <div class="box-footer text-middle" align="right" id="btndev" ><br />
                                                <%-- <button type="submit" id="btnSub" class="btn btn-primary custom-button-color">Search</button>--%>
                                                <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-secondory custom-button-color" />
                                                <input type="submit" id="Save" value="Save as draft" class="btn btn-primary custom-button-color" data-ng-click="Save()" <%--data-ng-hide="true"--%> />
                                                <input type="button" id="Submit" value="Submit" class="btn btn-primary custom-button-color" style="background-color :blue;" data-ng-click="Submit()" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                     <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.CheckedValue.$invalid" style="color: red">Please Select All Fields</span>

                                    </div>

                                    

                                    <div id="tableDiv" class="ag-blue" style="margin-top: 1px;">
                                        Table will generate here.
                                    </div>

                                </form>                                 
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="SecurityGuard.js"></script>
</body>
</html>