﻿app.service("ZFMReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        console.log(data);
        return $http.post(UtilityService.path + '/api/ZFMReport/BindGrid1', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});

app.controller('ZFMReportController', function ($scope, ZFMReportService, $timeout) {

    $scope.ZfmReport = {};
    $scope.ZfmReport.FromDate = moment().format('MM/DD/YYYY');
    //$scope.ZfmReport.UserID = UserID;

    //$scope.ZfmReport.ToDate = moment().format('MM/DD/YYYY');

    $scope.columnDefs = [
        { headerName: "Security Name", field: "SG_NAME", width: 190, cellClass: 'grid-align', width: 110 },
        { headerName: "Location Code", field: "LOCATION_CODE", cellClass: 'grid-align', width: 90 },
        { headerName: "Location Name", field: "LOCATION_NAME", cellClass: 'grid-align', width: 90 },
        { headerName: "Zone", field: "ZONE", cellClass: 'grid-align', width: 70 },
        { headerName: "Checklist Type", field: "CHECKLIST_TYPE", cellClass: 'grid-align', width: 110 },
        { headerName: "Main Category", field: "MAIN_CATEGORY", cellClass: 'grid-align', width: 110 },
        { headerName: "Sub Categoery", field: "SUB_CATEGORY", cellClass: 'grid-align', width: 110 },
        { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 70 },
        { headerName: "Comments", field: "COMMENTS", cellClass: 'grid-align', width: 110 },
        { headerName: "Created Date", field: "CREATED_DT", cellClass: 'grid-align', width: 150 },


    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,

        showToolPanel: true

    };
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value)
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.LoadData = function () {

        var params = {
            FromDate: $scope.ZfmReport.FromDate,
            ToDate: $scope.ZfmReport.ToDate,
        };
        ZFMReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                $scope.gridOptions.api.setRowData($scope.gridata[0]);
                progress(0, 'Loading...', false);
            }
        });
    }


    $scope.GenerateFilterPdf = function () {
        debugger   
        progress(0, 'Loading...', true);
        var columns = [{ title: "Property Name", key: "MAIN_CATEGORY" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("DailyReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ZFMReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function () {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }

    $scope.selVal = "TODAY";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.ZfmReport.FromDate = moment().format('MM/DD/YYYY');
                $scope.ZfmReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.ZfmReport.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.ZfmReport.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.ZfmReport.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.ZfmReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.ZfmReport.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.ZfmReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.ZfmReport.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.ZfmReport.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.ZfmReport.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.ZfmReport.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }


    $timeout(function () { $scope.LoadData() }, 500);

    $scope.rptDateRanges();

})