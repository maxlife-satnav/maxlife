﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<script runat="server">


</script>


<html lang="en" data-ng-app="QuickFMS">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::amantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
      <style>
          
     .grid-align {
            text-align: center;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            width: 355px;

        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
     <style>
       
        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }
        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }
      </style>
    </head>

<body data-ng-controller="ZFMReportController" class="amantra">
   <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                            <legend>Daily Report
                            </legend>
                        </fieldset>
<%--                    <div class="well">--%>
                        <form id="form1" data-valid-submit="LoadData()">
                            <div class="clearfix"> 
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Select Range</label>
                                        <br />
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>


                                 <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">From Date</label>
                                                <div class="input-group date" id='fromdate'>
                                                    <input type="text" class="form-control" data-ng-model="ZFMReport.FromDate" id="FromDate" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                    </span>
                                                </div>
                                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>--%>
                                            </div>

                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">To Date</label>
                                                <div class="input-group date" id='todate'>
                                                    <input type="text" class="form-control" data-ng-model="ZFMReport.ToDate" id="ToDate" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                    </span>
                                                </div>
                                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>--%>
                                            </div>
                                        </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 17px;">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                            <br />
                            <div class="row" style="padding-left: 18px">
                                <div class="box-footer text-right" id="table2" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    <br />
                                    <%--<a data-ng-click="GenReport(LeaseRep,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>--%>
                                    <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(ZFMReport,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    <%--<a data-ng-click="GenReport(LeaseRep,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 20px;">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto">
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../Scripts/jspdf.min.js"></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>

    <script src="../../Scripts/moment.min.js"></script>
    <%--<script src="ZfmReport.js"></script>--%>
    <script src="DailyReport.js"></script>
    <script src="../../SMViews/Utility.js"></script>

</body>
</html>
