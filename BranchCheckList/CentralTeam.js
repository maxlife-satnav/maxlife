﻿
app.service("CentralTeamCheckListServices", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.getCentralTeamdetails = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CentralTeamCheckList/getCentralTeamdetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.InsertCentralList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CentralTeamCheckList/InsertCentralList', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


}]);

app.controller('CentralTeamController', function ($scope, $q, $location, CentralTeamCheckListServices, UtilityService, $filter) {

    $scope.CheckList = {};
    $scope.DailyTList = {};
    $scope.CheckList.ReviewingDate = [];
    $scope.ReviewingDate = [];
    $scope.CheckList.ReviewingDate = moment(new Date()).format('DD/MM/YYYY');

    debugger;
    CentralTeamCheckListServices.getCentralTeamdetails().then(function (response) {
        var div = document.getElementById("btndev");
        div.style.display = "none";

        if (response.data != null) {
            $scope.LoadTableData(response);
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);

    }, function (response) {
        progress(0, '', false);
    });


    //load table data
    $scope.LoadTableData = function (response) {
        if (response.data[0] != null) {

            var div = document.getElementById("btndev");
            div.style.display = "block";

            document.getElementById("AuthorityName").value = response.data[0].CENTRAL_USER;
            document.getElementById("LocationsCount").value = response.data[0].LocationCount;
            document.getElementById("ValidationCount").value = response.data[0].ValidationCount;

            var table_body = '<div class="container"><div class="table-responsive">';

            for (i = 0; i < response.data.length; i++) {
                var n = i + 1;
                var color;

                lcolor = "background-color:cadetblue;";
                color = "background-color:darkslategray;",
                    tdcolor = "background-color:dimgray;";

                var BCL_ID = response.data[i]["BCL_ID"];

                table_body += '<table class="table table-bordered" id="example"><head><tr style=' + lcolor + '  href="#demo' + n + '"  data-toggle="collapse" ><th style=' + lcolor + ' class="col-md-3 col-sm-6 col-xs-12"><center><b>';
                table_body += response.data[i]["BCL_LOC_NAME"];
                table_body += '</b></center></th></tr></head>';
                table_body += '<tbody><tr id="demo' + n + '" class="collapse">';
                table_body += '<td>';

                if (response.data1 != null) {
                    for (k = 0; k < response.data1.length; k++) {
                        if (BCL_ID == response.data1[k]["BCL_ID"]) {
                            var category = response.data1[k]["BCLD_CAT_CODE"];

                            table_body += '<table class="table table-bordered"><head><tr style=' + color + '  href="#Cat' + k + n + '"  data-toggle="collapse" ><th style=' + color + ' class="col-md-12 col-sm-6 col-xs-12"><center><b>';
                            table_body += response.data1[k]["BCLD_CAT_NAME"];
                            table_body += '</b></center></th></tr></head>';
                            table_body += '<tbody><tr id="Cat' + k + n + '" class="collapse">';
                            table_body += '<td>';

                            if (response.data2 != null) {
                                table_body += '<table class="table table-bordered"><tbody> <tr><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Sub Category</b></th><th  style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Inspection Comments</b></th><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>ZFM Comments</b></th><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Central Team Action</b></th><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Central Team Comments</b></th><th class="col-md-3 col-sm-6 col-xs-12" style=' + tdcolor + ' ><b>Upload Proofs</b></th></tr>';

                                for (j = 0; j < response.data2.length; j++) {
                                    if (category == response.data2[j]["BCLD_CAT_CODE"]) {

                                        var SubCatName = response.data2[j]["BCLD_SUBCAT_NAME"];
                                        var InspComments = response.data2[j]["BCLD_INSP_COMENTS"];
                                        var ZFMComments = response.data2[j]["BCLD_ZF_COMENTS"];
                                        var ID = response.data2[j]["ID"];
                                        var File = response.data2[j]["File"];

                                        var Files = "";
                                        if (File != "") {
                                            var a = File.split(',');
                                            for (l = 0; l < a.length; l++) {
                                                var b = a[l].split('<br>');
                                                var DownLoadFileName = b[0];
                                                var MainfileName = b[1];
                                                Files = Files + '<a id=' + DownLoadFileName + ' onclick="FileDownload(this.id)" >' + MainfileName + '</a><br/>';
                                            }
                                        }

                                        if (j == 0) {
                                            $scope.DailyTList = [{ BCL_ID: BCL_ID, Location: response.data[i]["BCL_LOC_NAME"], CatName: response.data1[k]["BCLD_CAT_NAME"], SubCatName: SubCatName, ID: ID, CENTRAL_ACTION: 1, CENTRAL_COMMENTS: "" }];

                                        }
                                        else {
                                            $scope.DailyTList.push({ BCL_ID: BCL_ID, Location: response.data[i]["BCL_LOC_NAME"], CatName: response.data1[k]["BCLD_CAT_NAME"], SubCatName: SubCatName, ID: ID, CENTRAL_ACTION: 1, CENTRAL_COMMENTS: "" });

                                        }

                                        table_body += '<tr>';
                                        table_body += '<td class="col-xs-3">';
                                        table_body += SubCatName;
                                        table_body += '</td>';

                                        table_body += '<td class="col-xs-3">';
                                        table_body += InspComments;
                                        table_body += '</td>';

                                        table_body += '<td class="col-xs-3">';
                                        table_body += ZFMComments;
                                        table_body += '</td>';

                                        table_body += '<td class="col-xs-3">';
                                        table_body += '<select Class="Textbox1" name="Select' + ID + '" id="Select' + ID + '" onchange="selectfn(this.value)"><option value="1,' + ID + '">No Action</option> <option value="2,' + ID + '">Raise Incident</option> </select>';
                                        table_body += '</td>';

                                        table_body += '<td class="col-xs-3">';
                                        table_body += '<textarea Class="Textbox1"  id="textarea_' + ID + '"  name="textarea_' + ID + '"  rows="1" cols="20"></textarea>';

                                        table_body += '</td>';
                                        table_body += '<td Class="Textbox1">';
                                        table_body += Files;
                                        table_body += '</td>';

                                        table_body += '</tr>';
                                    }
                                }


                                table_body += '</tbody ></table >';
                            }

                            table_body += '</td>';
                            table_body += '</tr>';
                            table_body += '</tbody ></table >';

                        }
                    }
                }

                table_body += '</td>';
                table_body += '</tr>';
                table_body += '</tbody ></table >';

            }
            table_body += '</div></div>';
            $('#tableDiv').html(table_body);
        }

    };

    $(function () {
        $("#Save").click(function () {
            var result = confirm("Are you sure wants to continue ?");

            if (result == true) {
                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].CENTRAL_ACTION == "1") {
                            var ra = "textarea_" + $scope.DailyTList[j].ID;
                            var x = document.getElementById(ra).value;
                            $scope.DailyTList.push($scope.DailyTList[j].CENTRAL_COMMENTS = x);
                        }
                    }
                }

                $scope.data =
                {
                    ReviewingDate: $scope.CheckList.ReviewingDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                    SaveFlag: '3',
                    Seldata: $scope.DailyTList
                };

                CentralTeamCheckListServices.InsertCentralList($scope.data).then(function (response) {
                    if (response != null) {
                        //alert("Save Draft Successfully");
                    }
                }, function (error) {
                    console.log(error);
                });

                showNotification('Save Draft', 8, 'bottom-right', "Save Draft");
                var div = document.getElementById("btndev");
                div.style.display = "block";
            }
        });

        $("#Submit").click(function () {
            var result = confirm("Are you sure wants to continue ?");

            if (result == true) {
                debugger;

                var flag1 = 0;
                var flag2 = 0;

                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].CENTRAL_ACTION != null) {
                            if ($scope.DailyTList[j].CENTRAL_ACTION == "1") {
                                flag1 = 1;
                                var ra = "textarea_" + $scope.DailyTList[j].ID;
                                var x = document.getElementById(ra).value;
                                $scope.DailyTList.push($scope.DailyTList[j].CENTRAL_COMMENTS = x);
                            }
                            else {
                                flag2 = 2;
                            }
                        }
                    }
                }


                if (flag1 == 1 && flag2 == 2) {

                    $scope.data =
                    {
                        ReviewingDate: $scope.CheckList.ReviewingDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                        SaveFlag: '4',
                        Seldata: $scope.DailyTList
                    };

                    CentralTeamCheckListServices.InsertCentralList($scope.data).then(function (response) {
                        console.log(response);
                        if (response != null) {
                        }
                    }, function (error) {
                        console.log(error);
                    });

                    var div = document.getElementById("btndev");
                    div.style.display = "none";

                    showNotification('success', 8, 'bottom-right', "Success");

                }
                else if (flag1 == 1) {
                    var result1 = confirm("No action select, Are you sure wants to continue ?");

                    if (result1 == true) {

                        $scope.data =
                        {
                            ReviewingDate: $scope.CheckList.ReviewingDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                            SaveFlag: '4',
                            Seldata: $scope.DailyTList
                        };

                        CentralTeamCheckListServices.InsertCentralList($scope.data).then(function (response) {
                            console.log(response);
                            if (response != null) {
                            }
                        }, function (error) {
                            console.log(error);
                        });

                        var div = document.getElementById("btndev");
                        div.style.display = "none";

                        showNotification('success', 8, 'bottom-right', "Success");
                    }

                }
                else {
                    $scope.data =
                    {
                        ReviewingDate: $scope.CheckList.ReviewingDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                        SaveFlag: '4',
                        Seldata: $scope.DailyTList
                    };

                    CentralTeamCheckListServices.InsertCentralList($scope.data).then(function (response) {
                        console.log(response);
                        if (response != null) {
                        }
                    }, function (error) {
                        console.log(error);
                    });

                    var div = document.getElementById("btndev");
                    div.style.display = "none";

                    showNotification('success', 8, 'bottom-right', "Success");

                }

            }
        });
    });

    //checked working condition   
    $scope.Selectfn = function (res) {
        debugger;

        if ($scope.DailyTList != null) {
            for (j = 0; j < $scope.DailyTList.length; j++) {
                if (res[1] == $scope.DailyTList[j].ID) {
                    $scope.DailyTList.push($scope.DailyTList[j].CENTRAL_ACTION = res[0]);

                    if (res[0] != 1) {
                        var ra = "textarea_" + $scope.DailyTList[j].ID;
                        document.getElementById(ra).value = "";
                    }
                }
            }
        }
    };

});