﻿
app.service("SecurityGuardServices", function ($http, $q, UtilityService) {

    this.getLocations = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SecurityGuard/getLocations')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getAuthority = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SecurityGuard/getAuthority')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getInspector = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SecurityGuard/getInspector')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getMainCategories = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SecurityGuard/getMainCategories')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getSubCategoryData = function (category) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SecurityGuard/getSubCategoryData?category=' + category + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.InsertDailyCheckList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SecurityGuard/InsertDailyCheckList', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getRequestID = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SecurityGuard/getRequestID')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getNewCheckListID = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SecurityGuard/getNewCheckListID')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getCheckListDetails = function (BCL_ID) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SecurityGuard/getCheckListDetails?BCL_ID=' + BCL_ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});
app.controller('SecurityGuardController', function ($scope, $q, $location, SecurityGuardServices, UtilityService, $filter) {


    $scope.CheckList = {};
    $scope.DailyTList = {};
    $scope.DailyCountList = {};
    $scope.RequestID = [];
    $scope.Location = [];
    $scope.Authority = [];
    $scope.Inspection = [];
    $scope.CheckList.Authority = [];
    $scope.CheckList.Location = [];
    $scope.CheckList.Inspection = [];
    $scope.CheckList.InspectionDate = [];
    $scope.CheckList.Category = [];
    $scope.CheckList.RequestID = [];
    $scope.CheckList.tabledata = [];
    $scope.InspectionDate = [];
    $scope.DraftData = [];
    $scope.TotalData = [];
    rowData = [];
    MultiUploadData = [];
    $scope.CheckList.InspectionDate = moment(new Date()).format('DD/MM/YYYY');
    //getLocations
    SecurityGuardServices.getLocations().then(function (response) {
        debugger;
        if (response.data != null) {
            $scope.Location = response.data;
            angular.forEach($scope.Location, function (value, key) {
                var a = _.find($scope.Location);
                a.ticked = true;
            });
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);


    }, function (response) {
        progress(0, '', false);
    });

    //getAuthority
    SecurityGuardServices.getAuthority().then(function (response) {
        if (response.data != null) {
            $scope.Authority = response.data;
            angular.forEach($scope.Authority, function (value, key) {
                var a = _.find($scope.Authority);
                a.ticked = true;
            });
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });


    //getInspector
    SecurityGuardServices.getInspector().then(function (response) {
        if (response.data != null) {
            $scope.Inspection = response.data;
            angular.forEach($scope.Inspection, function (value, key) {
                var a = _.find($scope.Inspection);
                a.ticked = true;
            });
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    //get Categories data
    SecurityGuardServices.getMainCategories().then(function (response) {
        debugger;
        $scope.TotalData = response;
        $scope.LoadTableData(response);
    }, function (response) {
        progress(0, '', false);
    });

    // new id 
    SecurityGuardServices.getRequestID().then(function (response) {
        if (response.data != null) {
            $scope.RequestID = response.data;

            SecurityGuardServices.getNewCheckListID().then(function (responseNewID) {
                if (responseNewID.data != null) {
                    var NewID = responseNewID.data[0].BCL_ID;
                    var BCL_SUBMIT = responseNewID.data[0].BCL_SUBMIT;

                    angular.forEach($scope.RequestID, function (getRequestID) {
                        if (getRequestID.BCL_ID == NewID) {
                            getRequestID.ticked = true;

                            // $scope.LoadTableData($scope.tabledata);

                            SecurityGuardServices.getCheckListDetails(NewID).then(function (response) {
                                if (response != null) {
                                    $scope.SubCategories = response.data;
                                }
                                else
                                    showNotification('error', 8, 'bottom-right', response.Message);
                                progress(0, '', false);
                            }, function (error) {
                                console.log(error);
                            });
                        }
                    });

                    if (BCL_SUBMIT == 2) {
                        document.getElementById('Save').style.visibility = 'hidden';
                        document.getElementById('Submit').style.visibility = 'hidden';
                    }
                    else if (BCL_SUBMIT == 1) {
                        document.getElementById('Save').style.visibility = 'visible';
                        document.getElementById('Submit').style.visibility = 'visible';
                    }
                }
            });
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });



    //getCheckListDetails
    $scope.getCheckListDetails = function () {
        debugger;
        var ID = $scope.CheckList.RequestID[0].BCL_ID;

        $scope.LoadTableData($scope.TotalData);

        SecurityGuardServices.getCheckListDetails(ID).then(function (response) {
            if (response != null) {
                $scope.BindSelectedData(response);
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });

    };

    $(function () {
        debugger;
        $("#Save").click(function () {
            var result = confirm("Are you sure wants to continue ?");

            if (result == true) {
                var flag = 0;
                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].WorkingCondition == "2") {
                            var ra = "textarea_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                            var x = document.getElementById(ra).value;
                            $scope.DailyTList.push($scope.DailyTList[j].Comments = x);
                        }
                    }
                }

                $scope.data =
                {
                    BCL_ID: $scope.CheckList.RequestID[0].BCL_ID,
                    Location: $scope.CheckList.Location[0].LCM_CODE,
                    AuthorityBy: $scope.CheckList.Authority[0].URL_ID,
                    InspectdBy: $scope.CheckList.Inspection[0].INSPECTOR,
                    InspectdDate: $scope.CheckList.InspectionDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                    SaveFlag: '1',
                    Seldata: $scope.DailyTList
                };

                SecurityGuardServices.InsertDailyCheckList($scope.data).then(function (response) {
                    if (response != null) {
                        alert("Save Draft Successfully");
                    }
                }, function (error) {
                    console.log(error);
                });
                showNotification('Save Draft', 8, 'bottom-right', "Save Draft");
            }

        });

        $("#Submit").click(function () {
            var result = confirm("Are you sure wants to continue ?");

            if (result == true) {
                debugger;

                var flag = 0;
                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].WorkingCondition == "2") {
                            var ra = "textarea_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                            var x = document.getElementById(ra).value;
                            $scope.DailyTList.push($scope.DailyTList[j].Comments = x);
                        }
                    }
                }

                if ($scope.DailyTList != null) {
                    for (j = 0; j < $scope.DailyTList.length; j++) {
                        if ($scope.DailyTList[j].WorkingCondition == "") {
                            // showNotification('success', 8, 'bottom-right', "Success");
                            alert("Cat Name :" + $scope.DailyTList[j].CatName + "<br/>" + "Sub Cat Name :" + $scope.DailyTList[j].SubCatName + "<br/>" + "Please select Working condition");
                            flag = 1;
                            break;
                        }
                        if ($scope.DailyTList[j].WorkingCondition == "2") {
                            if ($scope.DailyTList[j].Comments == "") {
                                alert("Cat Name :" + $scope.DailyTList[j].CatName + "<br/>" + "Sub Cat Name :" + $scope.DailyTList[j].SubCatName + "<br/>" + "Please enter Comments");
                                flag = 1;
                                break;
                            }
                            else if ($scope.DailyTList[j].dFile == "") {
                                alert("Cat Name :" + $scope.DailyTList[j].CatName + "<br/>" + "Sub Cat Name :" + $scope.DailyTList[j].SubCatName + "<br/>" + "Please upload any Proof");
                                flag = 1;
                                break;
                            }
                        }
                    }

                    if (flag == 0) {

                        $scope.data =
                        {
                            BCL_ID: $scope.CheckList.RequestID[0].BCL_ID,
                            Location: $scope.CheckList.Location[0].LCM_CODE,
                            InspectdBy: $scope.CheckList.Inspection[0].INSPECTOR,
                            AuthorityBy: $scope.CheckList.Authority[0].URL_ID,
                            InspectdDate: $scope.CheckList.InspectionDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'),
                            SaveFlag: '2',
                            Seldata: $scope.DailyTList
                        };

                        SecurityGuardServices.InsertDailyCheckList($scope.data).then(function (response) {
                            console.log(response);
                            if (response != null) {
                                //$.ajax({
                                //    type: "POST",
                                //    url: 'https://live.quickfms.com/api/SecurityGuard/UploadFiles',    // CALL WEB API TO SAVE THE FILES.                        
                                //    contentType: false,
                                //    processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                                //    cache: false,
                                //    data: '', 		        // DATA OR FILES IN THIS CONTEXT.
                                //    success: function (data, textStatus, xhr) {
                                //        showNotification('success', 8, 'bottom-right', response.Message);
                                //    },
                                //    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                //        alert(textStatus + ': ' + errorThrown);
                                //    }
                                //});
                            }
                        }, function (error) {
                            console.log(error);
                        });
                        showNotification('success', 8, 'bottom-right', "Success");

                        document.getElementById("status").value = "Submit";
                        var div = document.getElementById("btndev");
                        div.style.display = "none";

                        // $scope.Clear();
                        //$scope.getCheckListDetails();
                    }
                }
            }

        });
    });

    //checked working condition   
    $scope.checkfn = function (res) {
        debugger;
        if ($scope.DailyTList != null) {
            for (j = 0; j < $scope.DailyTList.length; j++) {
                if (res[1] == $scope.DailyTList[j].SubCatCode)
                    $scope.DailyTList.push($scope.DailyTList[j].WorkingCondition = res[2]);
                if (res[2] == 1) {
                    var textData = "textarea_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                    var divFiles = "DivFiles_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                    // document.getElementById(textData).value = "";
                    //document.getElementById(divFiles).innerHTML = "";
                    // $scope.DailyTList.push($scope.DailyTList[j].File = "");                   
                }
            }
        }
        $scope.RowCount();

    };


    $scope.RowCount = function () {
        debugger;
        if ($scope.DailyCountList != null) {
            for (k = 0; k < $scope.DailyCountList.length; k++) {
                var Okcount = 0;
                var NotOkcount = 0;
                var TotalCount = 0;

                for (p = 0; p < $scope.DailyTList.length; p++) {
                    if ($scope.DailyTList[p] != null) {
                        if ($scope.DailyCountList[k].CatCode == $scope.DailyTList[p].CatCode) {
                            if ($scope.DailyTList[p].WorkingCondition == 1) {
                                Okcount = Okcount + 1;
                            }
                            else if ($scope.DailyTList[p].WorkingCondition == 2) {
                                NotOkcount = NotOkcount + 1;
                            }
                            TotalCount = TotalCount + 1;
                        }
                    }
                }

                var NotOk = "NotOkCount_" + $scope.DailyCountList[k].CatCode;
                var Ok = "OkCount_" + $scope.DailyCountList[k].CatCode;
                var Total = "TotalCount_" + $scope.DailyCountList[k].CatCode;
                document.getElementById(NotOk).value = NotOkcount;
                document.getElementById(Ok).value = Okcount;
                document.getElementById(Total).value = TotalCount;
            }
        }
    }


    // load table data
    $scope.LoadTableData = function (response) {
        if (response.data != null) {
            document.getElementById("status").value = "Save as draft";
            var div = document.getElementById("btndev");
            div.style.display = "block";

            var table_body = '<div class="container"><div class="table-responsive">';
            for (i = 0; i < response.data.length; i++) {
                if (response.data[i] != null) {
                    var n = i + 1;

                    var color;
                    if (i == i) {
                        color = "background-color:darkgrey;",
                            tdcolor = "background-color:gainsboro;"
                    };
                    if (i == 0) {
                        color = "background-color:darkgrey;",
                            tdcolor = "background-color:dimgray;"
                    }
                    if (i == 1) {
                        color = "background-color:cornflowerblue;",
                            tdcolor = "background-color:darkseagreen;"
                    };
                    if (i == 2) {
                        color = "background-color:darkgoldenrod;",
                            tdcolor = "background-color:darkolivegreen;"
                    };
                    if (i == 3) {
                        color = "background-color:burlywood;",
                            tdcolor = "background-color:cadetblue;"
                    };
                    if (i == 4) {
                        color = "background-color:darkgrey;",
                            tdcolor = "background-color:gainsboro;"

                    }
                    if (i == 5) {
                        color = "background-color:cornflowerblue;",
                            tdcolor = "background-color:aliceblue;"
                    };
                    if (i == 6) {
                        color = "background-color:darkgoldenrod;",
                            tdcolor = "background-color:beige;"
                    };
                    if (i == 7) {
                        color = "background-color:burlywood;",
                            tdcolor = "background-color:antiquewhite;"
                    };
                    if (i == 8) {
                        color = "background-color:darkgrey;",
                            tdcolor = "background-color:gainsboro;"

                    }
                    if (i == 9) {
                        color = "background-color:cornflowerblue;",
                            tdcolor = "background-color:aliceblue;"
                    };
                    if (i == 10) {
                        color = "background-color:darkgoldenrod;",
                            tdcolor = "background-color:beige;"
                    };
                    if (i == 11) {
                        color = "background-color:burlywood;",
                            tdcolor = "background-color:antiquewhite;"
                    };
                    if (i == 12) {
                        color = "background-color:darkgrey;",
                            tdcolor = "background-color:gainsboro;"

                    }
                    if (i == 13) {
                        color = "background-color:cornflowerblue;",
                            tdcolor = "background-color:aliceblue;"
                    };
                    if (i == 14) {
                        color = "background-color:darkgoldenrod;",
                            tdcolor = "background-color:beige;"
                    };
                    if (i == 15) {
                        color = "background-color:burlywood;",
                            tdcolor = "background-color:antiquewhite;"
                    };

                    var category = response.data[i]["BCL_MC_CODE"];

                    table_body += '<table class="table table-bordered" id="example"><head><tr style=' + color + '  href="#demo' + n + '"  data-toggle="collapse" ><th style=' + color + ' class="col-md-3 col-sm-6 col-xs-12"><div><div class="col-md-3 col-sm-6 col-xs-12">&nbsp; </div><div class="col-md-6 col-sm-6 col-xs-12"><center><b>';
                    table_body += response.data[i]["BCL_MC_NAME"];
                    table_body += '</b> &nbsp;&nbsp;<input class="btn btn-primary custom-button-color" type="button" id="TotalCount_' + category + '" name = "TotalCount_' + category + '" /></center></div><div class="col-md-3 col-sm-6 col-xs-12">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Not Ok &nbsp;<input style="background-color :red;"  class="btn btn-primary custom-button-color"  type="button" id="NotOkCount_' + category + '" name = "NotOkCount_' + category + '" />';
                    table_body += '&nbsp;&nbsp; Ok &nbsp;<input type="button" style="background-color :green;"  class="btn btn-primary custom-button-color" id="OkCount_' + category + '" name = "OkCount_' + category + '" />'
                    table_body += '<div>'
                    table_body += '</div></th></tr></head>';

                    //table_body += '&nbsp;&nbsp;<input class="btn btn-primary custom-button-color" type="button" id="TotalCount_' + category + '" name = "TotalCount_' + category + '" /></b>';
                    //table_body += '</center><div style="width:50px;">&nbsp;</div>';
                    //table_body += '&nbsp;&nbsp; Not Ok &nbsp; <input style="background-color :red;" class="btn btn-primary custom-button-color" type="button" id="NotOkCount_' + category + '" name="NotOkCount_' + category + '" />';
                    //table_body += '&nbsp;&nbsp; Ok &nbsp;<input type="button" style="background-color :green;"  class="btn btn-primary custom-button-color" id="OkCount_' + category + '" name = "OkCount_' + category + '" />';
                    //table_body += '</th ></tr ></head > ';
                    table_body += '<tbody><tr id="demo' + n + '" class="collapse">';
                    table_body += '<td>';
                    table_body += '<table class="table table-bordered"><tbody id="inner"> <tr><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Sub Category</b></th><th  style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Working Condition</b></th><th  style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Inspection Comments</b></th><th style=' + tdcolor + ' class="col-md-3 col-sm-6 col-xs-12"><b>Upload Proofs </b></th></tr>';

                    if (i == 0) {
                        $scope.DailyCountList = [{ CatCode: response.data[i]["BCL_MC_CODE"] }];

                    }
                    else {
                        $scope.DailyCountList.push({ CatCode: response.data[i]["BCL_MC_CODE"] });

                    }

                    if (response.data1 != null) {
                        for (j = 0; j < response.data1.length; j++) {
                            if (response.data1[j] != null) {
                                if (category == response.data1[j]["BCL_SUB_MC_CODE"]) {
                                    var categoryName = response.data[i]["BCL_MC_NAME"];
                                    var SubCatCode = response.data1[j]["BCL_SUB_CODE"];
                                    var SubCatName = response.data1[j]["BCL_SUB_NAME"];

                                    if (j == 0) {
                                        $scope.DailyTList = [{ CatCode: response.data[i]["BCL_MC_CODE"], CatName: response.data[i]["BCL_MC_NAME"], SubCatCode: response.data1[j]["BCL_SUB_CODE"], SubCatName: response.data1[j]["BCL_SUB_NAME"], WorkingCondition: "", Comments: "", row: j, File: "" }];

                                    }
                                    else {
                                        $scope.DailyTList.push({ CatCode: response.data[i]["BCL_MC_CODE"], CatName: response.data[i]["BCL_MC_NAME"], SubCatCode: response.data1[j]["BCL_SUB_CODE"], SubCatName: response.data1[j]["BCL_SUB_NAME"], WorkingCondition: "", Comments: "", row: j, File: "" });

                                    }

                                    table_body += '<tr>';
                                    table_body += '<td class="col-xs-3">';
                                    table_body += response.data1[j]["BCL_SUB_NAME"];
                                    table_body += '</td>';
                                    var value = response.data1[j]["BCL_SUB_MC_CODE"] + j + "," + SubCatCode + ",";
                                    table_body += '<td class="col-xs-3" id="txtRadio">';
                                    table_body += '<input type="radio" class="radio1"  name="radio_' + SubCatCode + '"  onclick="checkfn(this.value)" value="' + value + '1" id="radio_1' + SubCatCode + '" /> &nbsp; Ok &nbsp;&nbsp;&nbsp;';
                                    table_body += '<input type="radio" class="radio2" name="radio_' + SubCatCode + '"  onclick="checkfn(this.value)" value="' + value + '2" id="radio_2' + SubCatCode + '" /> &nbsp; Not Ok &nbsp;&nbsp;&nbsp; ';
                                    table_body += '</td>';
                                    table_body += '<td class="col-xs-3">';
                                    table_body += '<textarea class="Textbox1" style="display:none;" id="textarea_' + response.data1[j]["BCL_SUB_MC_CODE"] + j + '"  name="textarea_' + response.data1[j]["BCL_SUB_MC_CODE"] + j + '"  rows="4" cols="40"></textarea>';
                                    table_body += '</td>';
                                    table_body += '<td class="col-xs-3">';
                                    table_body += '<div id="DivFiles_' + response.data1[j]["BCL_SUB_MC_CODE"] + j + '"></div> <br/><br/>';
                                    table_body += '<input style="display:none;" type="file" id="files_' + response.data1[j]["BCL_SUB_MC_CODE"] + j + '" name="files_' + response.data1[j]["BCL_SUB_MC_CODE"] + j + '"  accept=".png,.jpg,.xlsx,.pdf,.docx"  > <br/>';
                                    table_body += '<input style="display:none;" class="btn btn-primary custom-button-color"  type="button" id="Submit_' + response.data1[j]["BCL_SUB_MC_CODE"] + j + '"  value="Upload"  onclick="FilesSubmit(files_' + response.data1[j]["BCL_SUB_MC_CODE"] + j + ')" />';
                                    table_body += '</td>';
                                    table_body += '</tr>';

                                }
                            }


                        }
                    }
                    table_body += '</tbody ></table >';
                    table_body += '</td>';
                    table_body += '</tr>';
                }

            }
            table_body += '</tbody></table></div>';
            $('#tableDiv').html(table_body);
        }
        else {
            showNotification('error', 8, 'bottom-right', response.Message);
        }

        $scope.RowCount();

    };


    $scope.Upload = function (res) {
        debugger;
        var rowName = res.id.split("_");
        var formData = new FormData();
        var UplFile = $(res)[0];
        formData.append("UplFile", UplFile.files[0]);
        $.ajax({
            url: UtilityService.path + "/api/SecurityGuard/UploadFiles",
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.data != null) {
                    var DownLoadFileName = response.data.FileName;
                    var MainfileName = response.data.MainfileName;

                    for (j = 0; j < $scope.DailyTList.length; j++) {

                        if (rowName[1] == $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row) {
                            $scope.DailyTList.push($scope.DailyTList[j].File = $scope.DailyTList[j].File + "," + DownLoadFileName + '<br>' + MainfileName);
                            var FileRow = "DivFiles_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                            document.getElementById(FileRow).innerHTML = document.getElementById(FileRow).innerHTML + '<a id=' + DownLoadFileName + ' onclick="FileDownload(this.id)" >' + MainfileName + '</a><br/>';
                            document.getElementById(res.id).value = "";
                            break;
                        }
                    }
                    showNotification('success', 8, 'bottom-right', response.Message);

                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }
        });

    }


    // selected data bind
    $scope.BindSelectedData = function (res) {
        debugger;
        var flag = 0;
        var div = document.getElementById("btndev");
        if (res.data != null) {
            if (res.data[0].BCL_SUBMIT == 0 || res.data[0].BCL_SUBMIT == 1) {
                document.getElementById("status").value = "Save as draft";
                div.style.display = "block";
            }
            else {
                document.getElementById("status").value = "Submit";
                div.style.display = "none";
                flag = 1;
            }
        }

        if (res.data1 != null) {

            if ($scope.DailyTList != null) {
                for (i = 0; i < res.data1.length; i++) {

                    for (j = 0; j < $scope.DailyTList.length; j++) {

                        if (res.data1[i].SubCatCode == $scope.DailyTList[j].SubCatCode) {
                            var radio;
                            if (res.data1[i].WorkingCondition == "1") {
                                var value = $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row + "," + $scope.DailyTList[j].SubCatCode + "," + 1;
                                document.getElementById("radio_1" + $scope.DailyTList[j].SubCatCode).click(value);
                            }
                            else if (res.data1[i].WorkingCondition == "2") {
                                var value = $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row + "," + $scope.DailyTList[j].SubCatCode + "," + 2;
                                document.getElementById("radio_2" + $scope.DailyTList[j].SubCatCode).click(value);
                            }

                        }
                    }
                }

                for (i = 0; i < res.data1.length; i++) {

                    for (j = 0; j < $scope.DailyTList.length; j++) {

                        if (res.data1[i].SubCatCode == $scope.DailyTList[j].SubCatCode) {

                            var comments = "textarea_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                            var files = "files_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                            var Submit = "Submit_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;
                            var radio1 = "radio_1" + $scope.DailyTList[j].SubCatCode;
                            var radio2 = "radio_2" + $scope.DailyTList[j].SubCatCode;


                            document.getElementById(comments).value = res.data1[i].Comments;
                            if (flag == 1) {
                                document.getElementById(comments).disabled = true;
                                document.getElementById(radio1).disabled = true;
                                document.getElementById(radio2).disabled = true;
                                document.getElementById(files).style.display = "none";
                                document.getElementById(Submit).style.display = "none";
                            }

                            var FileRow = "DivFiles_" + $scope.DailyTList[j].CatCode + $scope.DailyTList[j].row;

                            var file = res.data1[i].File;

                            if (res.data1[i].File != "") {
                                var a = res.data1[i].File.split(',');
                                for (l = 0; l < a.length; l++) {
                                    var b = a[l].split('<br>');
                                    var DownLoadFileName = b[0];
                                    var MainfileName = b[1];
                                    document.getElementById(FileRow).innerHTML = document.getElementById(FileRow).innerHTML + '<a id=' + DownLoadFileName + ' onclick="FileDownload(this.id)" >' + MainfileName + '</a><br/>';
                                    $scope.DailyTList.push($scope.DailyTList[j].File = $scope.DailyTList[j].File + "," + DownLoadFileName + '<br>' + MainfileName);
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.RowCount();

    };


    //Clear  the selected Values
    $scope.Clear = function () {
        var flag = 0;
        angular.forEach($scope.RequestID, function (getRequestID) {
            if (flag == 0) {
                getRequestID.ticked = true;
            }
            else {
                getRequestID.ticked = false;
            }
            flag = 1;
        });

        angular.forEach($scope.Location, function (Location) {
            // Location.ticked = false;
            var a = _.find($scope.Location);
            a.ticked = true;
        });
        angular.forEach($scope.Inspection, function (Inspection) {
            // Inspection.ticked = false;
            var a = _.find($scope.Inspection);
            a.ticked = true;
        });

        $scope.CheckList.InspectionDate = moment(new Date()).format('DD/MM/YYYY');
        $scope.getCheckListDetails();
    };

});

