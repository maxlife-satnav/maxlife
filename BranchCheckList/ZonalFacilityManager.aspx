﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZonalFacilityManager.aspx.cs" Inherits="BranchCheckList_ZonalFacilityManager" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script type="text/javascript" defer>

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                maxDate: 'today'
            });
        };      

        function selectfn(data) {
            debugger;
            var scope = angular.element(document.getElementById('idForJS')).scope();
            var res = data.split(",");
            
            if (res[0] == 1) {
                $("#textarea_" + res[1] + "").show();               
            }
            else {
                $("#textarea_" + res[1] + "").hide();              
            }
            scope.Selectfn(res);
        }

        function FileDownload(FileName) {
            window.location.href = "./Download.aspx?FileName=" + FileName;
        }
        
    </script>   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
</head>
<body id="idForJS" data-ng-controller="ZonalFacilityManagerController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 42px;">
                            <h6 class="panel-title"><b>Zonal Maintenance</b></h6>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">
                                <form id="Form1" name="frmZonalFMCheckList" <%--data-valid-submit="Creation()"--%> novalidate>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" >
                                                <label class="control-label">Reviewing Authority </label><br />
                                                <input  id="AuthorityName" runat="server" Class="Textbox" readonly="readonly" />
                                            </div>
                                        </div>    
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" >
                                                <label class="control-label">Pending Locations </label><br />
                                                <input id="LocationsCount" runat="server" Class="Textbox" readonly="readonly" />
                                            </div>
                                        </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" >
                                                <label class="control-label">Pending Inspections </label><br />
                                                <input id="InspectionsCount" runat="server" Class="Textbox" readonly="readonly" />
                                            </div>
                                        </div>
                                   <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmZonalFMCheckList.$submitted && frmZonalFMCheckList.SVR_FROM_DATE.$invalid}">
                                                    <label class="control-label">Reviewing Date <span style="color: red;">*</span></label>
                                                    <div  class="input-group date" style="width: 150px" id='todate'>
                                                        <input type="text"  class="form-control" placeholder="mm/dd/yyyy" id="ReviewingDate" name="ReviewingDate" data-ng-model="CheckList.ReviewingDate" required />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                        </span>
                                                    </div>
                                                    <span class="error" data-ng-show="frmZonalFMCheckList.$submitted && frmZonalFMCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please Select Date</span>
                                                </div>
                                            </div>
                                         </div>
                                </div>                               
                                <div class="row" align="right">
                                    <div class="box-footer text-right" id="btndev" style="padding-left: 20px; padding-right: 100px; padding-top: 26px">
                                                <input type="submit" id="Save" value="Save as draft" class="btn btn-primary custom-button-color" data-ng-click="Save()" <%--data-ng-hide="true"--%> />
                                                <input type="button" id="Submit" value="Submit" class="btn btn-primary custom-button-color" style="background-color :blue;" data-ng-click="Submit()" />
                                    </div>
                                </div>

                                <div id="tableDiv" class="ag-blue" style="margin-top: 1px;">
                                    Table will generate here.
                                </div>
                            </form>                             
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="ZonalFacilityManager.js"></script>
    <%--<script src="../../BranchCheckListManagement/JS/CheckListCreation.js"></script>--%>
</body>
</html>

