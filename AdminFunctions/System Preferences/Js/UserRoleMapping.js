﻿app.service("UserRoleMappingService", function ($http, $q, UtilityService) {
    //For Grid
    this.GetUserRoleList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetUserRoleDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetUserOnSelection = function (Id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetUserOnSelection/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    //for Module
    this.Getmodules = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/Getmodules')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //accessBy Module
    this.accessbymodule = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/GetAccessByModule', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/SaveDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    //save emp details step1
    this.SaveEmployeeDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/SaveEmpDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    //save Role/Module details step2
    this.SaveRMDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/SaveRoleModDetls', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //save Mapping details step3
    this.SaveMappingDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/SaveMappingDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //Get Role-module details step2
    this.GetRMDetails_Step2 = function (Id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetRMDetails_Step2/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //Get Mapping details step3
    this.LoadMappingDtls_Step3 = function (Id) {
        var deferred = $q.defer();

        ShowProgress();
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetMappingDetails_Step3/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('UserRoleMappingController', function ($scope, $q, UserRoleMappingService, UtilityService, $timeout, $filter, $http, $ngConfirm, $log) {
    $scope.sta_id = [{ value: "1", Name: "Active" }, { value: "2", Name: "In-Active" }]
    $scope.Viewstatus = 0;
    $scope.UserRole = {};
    $scope.CurrentProfile = {};
    $scope.Currentmodule = {};
    $scope.modules = {};
    $scope.acceslevel = {};
    $scope.Countrylst = [];
    $scope.Zonelst = [];
    $scope.Statelst = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.CostCenterlist = [];
    $scope.Parentlist = [];
    $scope.Childlist = [];
    $scope.Currentmodule.MapAccess = "Y";

    var EmployeeId = '';
    $scope.ViewUploption = true;
    $scope.UserStatus = true;
    $scope.Currentmodule.GH_Visibility = true;
    $scope.Currentmodule.VERT_COST = true;      //FOR ALL MODULE, SHOW VERT/CC
    $scope.RolValidation = false;
    $scope.SpaceVisible = true;
    $scope.reqdate = "MM/dd/yyyy"; //mm / dd / yyyy
    //For Grid column Headers
    var columnDefs = [
        { headerName: "Employee ID", field: "AUR_ID", cellClass: "grid-align" },
        { headerName: "Employee Name", field: "AUR_KNOWN_AS", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data)">{{data.AUR_KNOWN_AS}}</a>' },
        { headerName: "Role Name", field: "ROL_DESCRIPTION", cellClass: "grid-align" },
        { headerName: "Email ID", field: "AUR_EMAIL", cellClass: "grid-align" },
        { headerName: "Designation", field: "AUR_DESGN_ID", cellClass: "grid-align" }
    ];

    var columnDefs1 = [
        { headerName: "Country ", field: "HST_CNY_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Zone", field: "ZN_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "State", field: "STE_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "City", field: "HST_CTY_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Location", field: "HST_LCM_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Tower", field: "HST_TWR_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Floor", field: "HST_FLR_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Vertical Name", field: "HST_VERT_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Cost Center Name", field: "HST_CST_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "User Role", field: "HST_USR_ROLE_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "User Id", field: "HST_USR_ID", cellClass: "grid-align", width: 100 },
        { headerName: "Remarks", field: "HST_REM", cellClass: "grid-align", width: 450 }
    ];
    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        UserRoleMappingService.GetUserRoleList().then(function (data) {
            if (data != null) {
                $scope.gridata = data.data;
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, data.Message);
            }
        }, function (error) {
            progress(0, '', false);
        });
        $("#UserId").attr("readonly", true); $("#Username").attr("readonly", true); $("#timezone").attr("readonly", true);
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        rowSelection: 'single',
        //  onRowSelected: onRowSelectedFunc,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $scope.pageSize = '20';

    setTimeout(function () {
        $scope.LoadData();
    }, 700);

    $scope.gridOptions1 = {
        columnDefs: columnDefs1,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        rowSelection: 'single',
        onReady: function () {
            $scope.gridOptions1.api.sizeColumnsToFit()
        }
    };
    $scope.onRowSelectedFunc = function (data) {
        progress(0, 'Loading...', true);

        $('div.setup-panel div a').removeClass('btn-primary').addClass('btn-default');
        $('.setup-content').hide();
        $target = $($('#stp1').attr('href'));
        $('#stp1').addClass('btn-primary');
        $target.show();
        $target.find('input:eq(0)').focus();
        $scope.SelectdEmp = data;
        $scope.CurrentProfile = {};
        $scope.CurrentProfile = {};
        //var Id = data.AUR_ID;
        $scope.ViewUploption = false;
        EmployeeId = data.AUR_ID;
        $scope.Currentmodule.MapAccess = "Y";

        //var Id = data.AUR_ID.replace("/", "__");
        //var Id = data.AUR_ID.split("/").join("__");

        $scope.CurrentProfile = {};
        var Id = data.AUR_ID;
        $scope.ViewUploption = false;
        UserRoleMappingService.GetUserOnSelection(Id).then(function (response) {
            progress(0, 'Loading...', true);
            console.log(response.UserRoleMapping);
            $scope.CurrentProfile = response.UserRoleMapping;
            $scope.usrStatus = [{ value: 0, Name: "In-Active" }, { value: 1, Name: "Active" }];
            setTimeout(function () { $('#ddlusrStatus').selectpicker('refresh'); }, 200);
            UtilityService.getVerticals(1).then(function (usrver) {
                if (usrver.data != null) {
                    $scope.Business = usrver.data;
                    var BU = $filter("filter")($scope.Business, { VER_CODE: $scope.CurrentProfile.AUR_DEP_ID });
                    if (BU != undefined) {
                        BU[0].ticked = true;
                    }
                    $scope.CurrentProfile.VER_CODE = response.UserRoleMapping.AUR_PRJ_CODE;
                }
            });
            UtilityService.getCostcenterByVerticalcode({ VER_CODE: response.UserRoleMapping.AUR_DEP_ID }, 1).then(function (Costdata) {
                if (Costdata != null) {
                    $scope.Functions = Costdata.data;
                    var CC = $filter("filter")($scope.Functions, { Cost_Center_Code: $scope.CurrentProfile.AUR_PRJ_CODE });
                    if (CC != undefined) {
                        CC[0].ticked = true;
                    }
                    $scope.CurrentProfile.Cost_Center_Code = response.UserRoleMapping.AUR_PRJ_CODE;
                }
            });
            UtilityService.getDesignation().then(function (response) {
                if (response.data != null) {
                    $scope.designationddl = response.data;
                    var ddl = $filter("filter")($scope.designationddl, { DSG_CODE: $scope.CurrentProfile.AUR_DESGN_ID });
                    if (ddl != undefined) {
                        ddl[0].ticked = true;
                        $scope.CurrentProfile.DSG_CODE = $scope.CurrentProfile.AUR_DESGN_ID;
                    }
                }

            });
            UtilityService.getLocations(1).then(function (data) {
                if (data.data != null) {
                    $scope.Location = data.data;
                    var LOC = $filter("filter")($scope.Location, { LCM_CODE: $scope.CurrentProfile.AUR_LOCATION });
                    if (LOC != undefined) {
                        LOC[0].ticked = true;
                    }
                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                }
            });

            $scope.UserRole = response.USERMAPPINGDETAILS;
            $scope.UserRole.selectedParentEntity = response.USERMAPPINGDETAILS.selectedParentEntity;
            $scope.Viewstatus = 1;
            $scope.CurrentProfile.AUR_PASSWORD = response.UserRoleMapping.AUR_PASSWORD;
            $scope.CurrentProfile.AUR_DOJ = $filter('date')($scope.CurrentProfile.AUR_DOJ, $scope.reqdate);
            $scope.CurrentProfile.AUR_DOB = $filter('date')($scope.CurrentProfile.AUR_DOB, $scope.reqdate);
            $scope.CurrentProfile.AUR_STA_ID = $scope.CurrentProfile.AUR_STA_ID.toString();
            $scope.reselectedEmp($scope.CurrentProfile.AUR_REPORTING_TO);




            //fill reporting mang ddl            

            //countries
            //UtilityService.getCountires(1).then(function (Condata) {
            //    $scope.Countrylst = Condata.data;
            //    if (response.USERMAPPINGDETAILS.selectedCountries != null) {
            //        for (i = 0; i < response.USERMAPPINGDETAILS.selectedCountries.length; i++) {
            //            var a = _.find($scope.Countrylst, { CNY_CODE: response.USERMAPPINGDETAILS.selectedCountries[i].CNY_CODE });
            //            a.ticked = true;
            //        }
            //    }
            //    //Zone
            //    UtilityService.getZone(1).then(function (zndata) {
            //        $scope.Zonelst = zndata.data;
            //        if (response.USERMAPPINGDETAILS.selectedZone != null) {
            //            for (i = 0; i < response.USERMAPPINGDETAILS.selectedZone.length; i++) {
            //                var a = _.find($scope.Zonelst, { ZN_CODE: response.USERMAPPINGDETAILS.selectedZone[i].ZN_CODE });
            //                a.ticked = true;
            //            }
            //        }
            //        //State
            //        UtilityService.getState(1).then(function (stedata) {
            //            $scope.Statelst = stedata.data;
            //            if (response.USERMAPPINGDETAILS.selectedState != null) {
            //                for (i = 0; i < response.USERMAPPINGDETAILS.selectedState.length; i++) {
            //                    var a = _.find($scope.Statelst, { STE_CODE: response.USERMAPPINGDETAILS.selectedState[i].STE_CODE });
            //                    a.ticked = true;
            //                }
            //            }
            //            //cities
            //            UtilityService.getCities(1).then(function (Cnydata) {
            //                $scope.Citylst = Cnydata.data;
            //                if (response.USERMAPPINGDETAILS.selectedCities != null) {
            //                    for (i = 0; i < response.USERMAPPINGDETAILS.selectedCities.length; i++) {
            //                        var a = _.find($scope.Citylst, { CTY_CODE: response.USERMAPPINGDETAILS.selectedCities[i].CTY_CODE });
            //                        a.ticked = true;
            //                    }
            //                }
            //                else {
            //                    $scope.Citylst = Cnydata.data;
            //                }
            //                //locs
            //                UtilityService.getLocations(1).then(function (Ctydata) {
            //                    $scope.Locationlst = Ctydata.data;
            //                    if (response.USERMAPPINGDETAILS.selectedLocations != null) {
            //                        for (i = 0; i < response.USERMAPPINGDETAILS.selectedLocations.length; i++) {
            //                            var a = _.find($scope.Locationlst, { LCM_CODE: response.USERMAPPINGDETAILS.selectedLocations[i].LCM_CODE });
            //                            a.ticked = true;
            //                        }
            //                    }
            //                    else {
            //                        $scope.Locationlst = Ctydata.data;
            //                    }
            //                    //towers

            //                    UtilityService.getTowers(1).then(function (Locdata) {
            //                        $scope.Towerlist = Locdata.data;
            //                        if (response.USERMAPPINGDETAILS.selectedTowers != null) {
            //                            for (i = 0; i < response.USERMAPPINGDETAILS.selectedTowers.length; i++) {
            //                                var a = _.find($scope.Towerlist, { TWR_CODE: response.USERMAPPINGDETAILS.selectedTowers[i].TWR_CODE });
            //                                a.ticked = true;
            //                            }
            //                        }
            //                        else {
            //                            $scope.Towerlist = Locdata.data;
            //                        }
            //                        //floors
            //                        UtilityService.getFloors(1).then(function (Flrdata) {
            //                            $scope.Floorlist = Flrdata.data;
            //                            if (response.USERMAPPINGDETAILS.selectedFloors != null) {
            //                                for (i = 0; i < response.USERMAPPINGDETAILS.selectedFloors.length; i++) {
            //                                    var a = _.find($scope.Floorlist, { FLR_CODE: response.USERMAPPINGDETAILS.selectedFloors[i].FLR_CODE });
            //                                    if (a)
            //                                        a.ticked = true;
            //                                }
            //                            }
            //                            else {
            //                                $scope.Floorlist = Flrdata.data;
            //                                //$scope.CostCenterlist = [];
            //                            }
            //                            //parent entity
            //                            UtilityService.getParentEntity(1).then(function (parent) {
            //                                $scope.Parentlist = parent.data;
            //                                if (response.USERMAPPINGDETAILS.selectedParentEntity != null) {
            //                                    for (i = 0; i < response.USERMAPPINGDETAILS.selectedParentEntity.length; i++) {
            //                                        var a = _.find($scope.Parentlist, { PE_CODE: response.USERMAPPINGDETAILS.selectedParentEntity[i].PE_CODE });
            //                                        a.ticked = true;
            //                                    }
            //                                }
            //                                else {
            //                                    //$scope.Parentlist = parent.data;
            //                                    //$scope.Childlist = [];
            //                                    //$scope.CostCenterlist = [];
            //                                    //$scope.Verticallist = [];
            //                                }
            //                                //child
            //                                UtilityService.getChildEntity(1).then(function (chddata) {
            //                                    $scope.Childlist = chddata.data;
            //                                    if (response.USERMAPPINGDETAILS.selectedChildEntity != null) {
            //                                        for (i = 0; i < response.USERMAPPINGDETAILS.selectedChildEntity.length; i++) {
            //                                            var a = _.find($scope.Childlist, { CHE_CODE: response.USERMAPPINGDETAILS.selectedChildEntity[i].CHE_CODE });
            //                                            a.ticked = true;
            //                                        }
            //                                    }
            //                                    else {
            //                                        //$scope.Childlist = chddata.data;
            //                                        //$scope.CostCenterlist = [];
            //                                        //$scope.Verticallist = [];
            //                                    }
            //                                    //vert
            //                                    UtilityService.getVerticals(1).then(function (Vedata) {
            //                                        $scope.Verticallist = Vedata.data;
            //                                        if (response.USERMAPPINGDETAILS.selectedVerticals != null) {
            //                                            for (i = 0; i < response.USERMAPPINGDETAILS.selectedVerticals.length; i++) {
            //                                                var a = _.find($scope.Verticallist, { VER_CODE: response.USERMAPPINGDETAILS.selectedVerticals[i].VER_CODE });
            //                                                a.ticked = true;
            //                                            }
            //                                        }
            //                                        else {
            //                                            $scope.Verticallist = Vedata.data;
            //                                            //$scope.CostCenterlist = [];
            //                                        }
            //                                        //cost cent
            //                                        UtilityService.getCostCenters(1).then(function (Csdata) {
            //                                            $scope.CostCenterlist = Csdata.data;
            //                                            if (response.USERMAPPINGDETAILS.selectedCostcenter != null) {
            //                                                for (i = 0; i < response.USERMAPPINGDETAILS.selectedCostcenter.length; i++) {
            //                                                    var a = _.find($scope.CostCenterlist, { Cost_Center_Code: response.USERMAPPINGDETAILS.selectedCostcenter[i].Cost_Center_Code });
            //                                                    a.ticked = true;
            //                                                }
            //                                            } else { $scope.CostCenterlist = Csdata.data; }
            //                                            //
            //                                            if (response.UserRoleVM.length > 0) {
            //                                                var id = $scope.Currentmodule.MOD_NAME;
            //                                                UserRoleMappingService.accessbymodule(id).then(function (rol) {
            //                                                    $scope.acceslevel = rol;
            //                                                    for (i = 0; i < response.UserRoleVM.length; i++) {
            //                                                        var a = _.find($scope.acceslevel, { ROL_ID: response.UserRoleVM[i].ROL_ID });
            //                                                        a.isChecked = true;
            //                                                    }
            //                                                });
            //                                            }
            //                                            else {
            //                                                UtilityService.GetRoles(1).then(function (rol) {
            //                                                    $scope.acceslevel = rol.data;
            //                                                });
            //                                            }
            //                                            $scope.Viewstatus = 1;
            //                                            // setTimeout(function () {
            //                                            $scope.CurrentProfile.AUR_PASSWORD = response.UserRoleMapping.AUR_PASSWORD;
            //                                            $scope.CurrentProfile.AUR_DOJ = $filter('date')($scope.CurrentProfile.AUR_DOJ, $scope.reqdate);
            //                                            $scope.CurrentProfile.AUR_DOB = $filter('date')($scope.CurrentProfile.AUR_DOB, $scope.reqdate);
            //                                            $scope.CurrentProfile.AUR_STA_ID = response.UserRoleMapping.AUR_STA_ID.toString();
            //                                            progress(0, '', false);
            //                                            // }, 2000)
            //                                            UserRoleMappingService.GetUserRoleList().then(function (usrResponse) {
            //                                                $scope.allUsers = usrResponse.data;
            //                                                $scope.CurrentProfile.AUR_REPORTING_TO = response.UserRoleMapping.AUR_REPORTING_TO;
            //                                            });
            //                                        });
            //                                    });
            //                                });
            //                            });
            //                        });
            //                    });
            //                });
            //            });
            //        });
            //    });
            //});



        }, function (error) {
            progress(0, '', false);
        });
    };
    $scope.selectedEmp = function (selected) {
        if (selected) {
            $scope.selectedEmployee = selected.originalObject;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.CurrentProfile.AUR_REPORTING_TO = $scope.selectedEmployee.AUR_ID;
            //setTimeout(function () { $('.selectpicker').selectpicker('refresh'); }, 400);
        }
    };

    $scope.remoteUrlRequestFn = function (str) {
        return { q: str };
    };




    $scope.reselectedEmp = function (obj) {
        if (obj) {
            //$scope.selectedEmployee = obj.selected.data;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', obj);
            $scope.CurrentProfile.AUR_REPORTING_TO = obj;
        }

    };
    //Save Employee Details
    $scope.SaveEmpDetails = function () {
        var fromdate = moment($scope.CurrentProfile.AUR_DOB);
        var todate = moment($scope.CurrentProfile.AUR_DOJ);
        if (fromdate > todate) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            if ($scope.CurrentProfile.AUR_PASSWORD != "" && $scope.CurrentProfile.LCM_CODE != "") {
                progress(0, 'Loading...', true);
                var encoding = encodeURIComponent(EmployeeId);
                //var Id = encoding.replace(/%2F/i, "_");
                var Id = encoding.split(/%2F/i).join("_");
                $scope.CurrentProfile.AUR_ID = Id;
                if ($scope.CurrentProfile.Cost_Center_Code.length > 0) {
                    $scope.CurrentProfile.AUR_PRJ_CODE == $scope.CurrentProfile.Cost_Center_Code[0].Cost_Center_Code;
                    $scope.CurrentProfile.AUR_DEP_ID == $scope.CurrentProfile.Cost_Center_Code[0].Vertical_Code;
                }
                var params = {
                    UserRoleMapping: $scope.CurrentProfile,
                    USERMAPPINGDETAILS: {
                        selectedLocations: $scope.CurrentProfile.LCM_CODE,
                    }
                }
                UserRoleMappingService.SaveEmployeeDetails(params).then(function (data) {
                    progress(0, 'Loading...', false);
                    nxtbtnclick();
                    if (data.data == undefined) {
                        showNotification('error', 8, 'bottom-right', data.Message);
                    }
                    else {


                        if (data.data.AUR_PASSWORD != null) {
                            $scope.CurrentProfile.AUR_OLD_PASSWORD = data.data.AUR_PASSWORD;
                        }
                        $scope.modules = data.data.MRD.modulelist;
                        //$scope.UserRole = response.USERMAPPINGDETAILS;

                        if (data.data.MRD.UserRoleVM.length > 0) {
                            $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                            UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                                $scope.acceslevel = rol;
                                for (i = 0; i < data.data.MRD.UserRoleVM.length; i++) {
                                    var a = _.find($scope.acceslevel, { ROL_ID: data.data.MRD.UserRoleVM[i].ROL_ID });
                                    a.isChecked = true;
                                }
                            });
                        }
                        else {
                            // UtilityService.GetRoles(1).then(function (rol) {
                            $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                            UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                                $scope.acceslevel = rol;
                                for (i = 0; i < $scope.acceslevel.length; i++) {
                                    // var a = _.find($scope.acceslevel, { ROL_ID: 6 });
                                    if ($scope.acceslevel[i].ROL_ID == "6") {
                                        $scope.acceslevel[i].isChecked = true;
                                    }
                                }
                            });
                        }
                        $scope.SelectdEmp.AUR_EMAIL = $scope.CurrentProfile.AUR_EMAIL;
                        $scope.gridOptions.api.refreshView();
                        showNotification('success', 8, 'bottom-right', 'Employee Details Saved Successfully');
                    }
                });
            }
            else {
                showNotification('Error', 8, 'bottom-right', 'Please enter password.');
                //    alert("Please select LOB/Costcenter.");
            }
        }
    };

    function nxtbtnclick() {

        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    }
    /////////////////////////////////////////////////////////////////////////////////////


    $scope.BuChange = function () {
        UtilityService.getCostcenterByVerticalcode({ VER_CODE: $scope.CurrentProfile.VER_CODE[0].VER_CODE }, 1).then(function (Costdata) {
            if (Costdata != null) {
                $scope.Functions = Costdata.data;
            }
        });
    }
    //for Module DropDown
    UserRoleMappingService.Getmodules().then(function (data) {
        $scope.modules = data;
        UtilityService.GetRoles(1).then(function (rol) {
            $scope.acceslevel = rol.data;
        });
    }, function (error) {
        console.log(error);
    });

    //For Access  by Module change
    $scope.accessbymodule = function () {
        var id = $scope.Currentmodule.MOD_NAME;
        UserRoleMappingService.accessbymodule(id).then(function (data) {
            $scope.acceslevel = data;
        }, function (error) {
            console.log(error);
        });
        //other than All, Space hide vert, costcenter
        if (id == "ALL" || id == "Space") {
            $scope.Currentmodule.GH_Visibility = true;
            $scope.Currentmodule.VERT_COST = true;
        }
        else if (id == "GuestHouse") {
            $scope.Currentmodule.GH_Visibility = false;
            $scope.Currentmodule.VERT_COST = true;
        }
        else {
            $scope.Currentmodule.GH_Visibility = true;
            $scope.Currentmodule.VERT_COST = false;
        }
        // $scope.IsVisible = $scope.IsVisible ? false : true;
    }

    //For Status Name
    $scope.ShowstatusName = function (sta) {
        for (var i = 0; i < $scope.sta_id.length; i += 1) {
            var result = $scope.sta_id[i];
            if (result.value == sta) {
                return result.Name;
            }
        }
    }
    //country
    $scope.CountryChanged = function () {
        UtilityService.getZoneByCny($scope.UserRole.selectedCountries, 1).then(function (response) {
            if (response.data != null) {
                $scope.Zonelst = response.data
            } else { $scope.Zonelst = [] }
        }, function (error) {
            console.log(error);
        });
    }
    $scope.CnyChangeAll = function () {
        $scope.UserRole.selectedCountries = $scope.Countrylst;
        $scope.CountryChanged();
    }
    $scope.cnySelectNone = function () {
        $scope.Zonelst = [];
        $scope.Statelst = [];
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    //zone
    $scope.ZonChanged = function () {
        UtilityService.getStateByZone($scope.UserRole.selectedZone, 1).then(function (data) {
            if (data.data == null) { $scope.Statelst = [] } else {
                $scope.Statelst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });
    }
    $scope.ZonChangeAll = function () {
        $scope.UserRole.selectedZone = $scope.Zonelst;
        $scope.ZonChanged();
    }
    $scope.ZonSelectNone = function () {
        $scope.Statelst = [];
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    //state
    $scope.SteChanged = function () {

        UtilityService.getCityByState($scope.UserRole.selectedState, 1).then(function (data) {
            if (data.data == null) { $scope.Citylst = [] } else {
                $scope.Citylst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var zne = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zne != undefined && value.ticked == true) {
                zne.ticked = true;
                $scope.UserRole.selectedZone.push(zne);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.UserRole.selectedState = $scope.Statelst;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.city = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    //city
    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.UserRole.selectedCities, 1).then(function (data) {
            if (data.data == null) { $scope.Locationlst = [] } else {
                $scope.Locationlst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.UserRole.selectedZone.push(zn);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.UserRole.selectedState.push(ste);
            }
        });
    }
    $scope.CtyChangeAll = function () {
        $scope.UserRole.selectedCities = $scope.Citylst;
        $scope.CityChanged();
    }
    $scope.ctySelectNone = function () {
        $scope.UserRole.selectedCities = [];
        $scope.CityChanged();
    }
    //location
    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.UserRole.selectedLocations, 1).then(function (data) {
            if (data.data != null) { $scope.Towerlist = data.data } else { $scope.Towerlist = [] }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UserRole.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.UserRole.selectedZone.push(zn);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.UserRole.selectedState.push(ste);
            }
        });
    }
    $scope.LCMChangeAll = function () {
        $scope.UserRole.selectedLocations = $scope.Locationlst;
        $scope.LocChange();
    }
    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    //tower
    $scope.TwrChange = function () {
        UtilityService.getFloorByTower($scope.UserRole.selectedTowers, 1).then(function (data) {
            if (data.data != null) { $scope.Floorlist = data.data } else { $scope.Floorlist = [] }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var twr = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.UserRole.selectedZone.push(twr);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.UserRole.selectedState.push(ste);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UserRole.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.UserRole.selectedLocations.push(lcm);
            }
        });
    }
    $scope.TwrChangeAll = function () {
        $scope.UserRole.selectedTowers = $scope.Towerlist;
        $scope.TwrChange();
    }
    $scope.twrSelectNone = function () {
        $scope.UserRole.selectedTowers = [];
        $scope.TwrChange();
    }

    //floor
    $scope.FloorChange = function () {
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.UserRole.selectedZone.push(twr);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.UserRole.selectedState.push(ste);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UserRole.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.UserRole.selectedLocations.push(lcm);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.UserRole.selectedTowers.push(twr);
            }
        });
    }
    $scope.FloorChangeAll = function () {
        $scope.UserRole.selectedFloors = $scope.Floorlist;
        $scope.FloorChange();
    }
    $scope.ParentEntityChange = function () {
        UtilityService.getchildbyparent($scope.UserRole.selectedParentEntity, 1).then(function (response) {
            console.log(response.data);
            $scope.Childlist = [];
            if (response.data != null) { $scope.Childlist = response.data } else { $scope.Childlist = [] }
        }, function (error) {
            console.log(error);
        });
    }

    //parent Entity
    $scope.ParentEntityChangeAll = function () {
        $scope.UserRole.selectedParentEntity = $scope.Parentlist;
        $scope.ParentEntityChange();
    }
    $scope.ParentEntitySelectNone = function () {
        $scope.Childlist = [];
        $scope.Verticallist = [];
        $scope.CostCenterlist = [];
    }

    //child entity
    $scope.ChildEntityChange = function () {
        UtilityService.getverticalbychild($scope.UserRole.selectedChildEntity, 1).then(function (response) {
            if (response.data != null) {
                $scope.Verticallist = response.data
                UtilityService.getCostcenterByVertical($scope.Verticallist, 1).then(function (response) {
                    if (response.data != null) { $scope.CostCenterlist = response.data } else { $scope.CostCenterlist = [] }
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                $scope.Verticallist = []
            }
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Parentlist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Childlist, function (value, key) {
            $scope.UserRole.selectedParentEntity = [];
            var par = _.find($scope.Parentlist, { PE_CODE: value.PE_CODE });
            if (par != undefined && value.ticked == true) {
                par.ticked = true;
                $scope.UserRole.selectedParentEntity.push(par);
            }
        });
    }
    $scope.ChildEntityChangeAll = function () {
        $scope.UserRole.selectedChildEntity = $scope.Childlist;
        $scope.ChildEntityChange();
    }
    $scope.ChildEntitySelectNone = function () {
        $scope.Verticallist = [];
        $scope.CostCenterlist = [];
    }

    //vertical
    $scope.VerticalChange = function () {
        UtilityService.getCostcenterByVertical($scope.UserRole.selectedVerticals, 1).then(function (data) {
            if (data.data != null) { $scope.CostCenterlist = data.data } else { $scope.CostCenterlist = [] }
        }, function (error) {
            console.log(error);
        });
    }
    $scope.VerticalChangeAll = function () {
        $scope.UserRole.selectedVerticals = $scope.Verticallist;
        $scope.VerticalChange();
    }
    $scope.verticalSelectNone = function () {
        $scope.CostCenterlist = [];
    }

    //costcenter
    $scope.CostCenterChange = function () {
        angular.forEach($scope.Verticallist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.CostCenterlist, function (value, key) {
            var ver = _.find($scope.Verticallist, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.UserRole.selectedVerticals.push(ver);
            }
        });
    }
    $scope.CostCenterChangeAll = function () {
        $scope.UserRole.selectedCostcenter = $scope.CostCenterlist;
        $scope.CostCenterChange();
    }
    //For Back Button
    $scope.back = function () {
        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.Countrylst = [];
        $scope.acceslevel = [];
        $scope.Verticallist = [];
        $scope.Parentlist = [];
        $scope.Childlist = [];
        $scope.CostCenterlist = [];
        $scope.Currentmodule.MOD_NAME = "ALL";
        $scope.ViewUploption = true;
    }
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    //For Filter
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.Currentmodule.MOD_NAME = "ALL";


    //For Save
    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);
        var selectedRoles = [];
        //console.log($scope.CurrentProfile);
        for (i = 0; i < $scope.acceslevel.length; i++) {
            var role = $scope.acceslevel[i];
            if (role.isChecked) {
                selectedRoles.push(role);
            }
        };
        if (selectedRoles.length > 0) {
            $scope.dataobject = { UserRoleMapping: $scope.CurrentProfile, UserRoleVM: selectedRoles, USERMAPPINGDETAILS: $scope.UserRole };

            UserRoleMappingService.SaveDetails($scope.dataobject).then(function (data) {
                progress(0, 'Loading...', false);
                UserRoleMappingService.GetUserRoleList().then(function (data) {
                    if (data != null) {
                        $scope.gridata = data.data;
                        $scope.gridOptions.api.setRowData($scope.gridata);
                    }
                    else {
                        $scope.gridOptions.api.setRowData([]);
                        showNotification('error', 8, data.Message);
                    }
                }, function (error) {
                });
                $scope.back();
                showNotification('success', 8, 'bottom-right', 'Role Assigned Successfully');
            }, function (error) {
                progress(0, '', false);
            });
        } else {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Please Select Atleast One Role to Assign');
        }
    }

    //save role module details / step2
    $scope.SaveRMDetls = function () {
        //EmployeeId        
        progress(0, 'Loading...', true);
        $scope.selectedRoles = [];
        $scope.selectedRoles = $filter('filter')($scope.acceslevel, { isChecked: true });
        if ($scope.selectedRoles.length > 0) { //selectedRoles        
            $scope.RolValidation = true;
            $scope.dataobject = { UserRoleMapping: { AUR_ID: EmployeeId }, UserRoleVM: $scope.selectedRoles, MapLevelAccess: $scope.Currentmodule.MapAccess };
            UserRoleMappingService.SaveRMDetails($scope.dataobject).then(function (response) {
                $scope.UsrMappingDtls(response);

                progress(0, 'Loading...', false);
                nxtbtnclick();
                showNotification('success', 8, 'bottom-right', 'Role Assigned Successfully');
            }, function (error) {
                progress(0, '', false);
            });

            var RoleLst = '';

            for (i = 0; i < $scope.selectedRoles.length; i++) {
                if ($scope.selectedRoles[i].isChecked == true) {
                    RoleLst = RoleLst + ',' + $scope.selectedRoles[i].ROL_DESCRIPTION;
                }
            }
            RoleLst = RoleLst.slice(1);

            $scope.SelectdEmp.ROL_DESCRIPTION = RoleLst;
            $scope.gridOptions.api.refreshView();

        } else {
            progress(0, 'Loading...', false);
            $scope.RolValidation = false;
            showNotification('error', 8, 'bottom-right', 'Please Select Atleast One Role to Assign');
        }
    };


    //excel upload
    $scope.UploadFile = function () {
        if ($('#FileUpl', $('#FileUsrUpl')).val()) {
            var filetype = $('#FileUpl', $('#FileUsrUpl')).val().substring($('#FileUpl', $('#FileUsrUpl')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                var formData = new FormData();
                // $scope.currentSLA.SLA_CREATED_BY = login;
                var UplFile = $('#FileUpl')[0];

                formData.append("UplFile", UplFile.files[0]);
                formData.append("CurrObj", "");
                $.ajax({
                    url: UtilityService.path + '/api/UserRoleMapping/UploadTemplate',
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        var respdata = JSON.parse(data);
                        if (respdata.data != null)
                            $scope.LoadUploadedData(respdata.data);
                        else
                            showNotification('error', 8, 'bottom-right', respdata.Message);
                    }
                });
            }
            else
                showNotification('error', 8, 'bottom-right', 'Please Upload only Excel (.xlsx) File(s)');
        }
        else
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');
    }
    $scope.Alert = function () {
        if ($('#ddlusrStatus').val() == "0") {
            alert('When You Inactive Current Employee Then It Will Be Released From The Allocated Space');
        }
    }

    $scope.LoadUploadedData = function (data) {
        $scope.$apply(function () {
            $scope.Viewstatus = 2;
            $scope.gridata1 = data.Table;
            $scope.gridOptions1.api.setRowData($scope.gridata1);
        });
    }
    $scope.GenReport = function (Allocdata, Type) {
        progress(0, 'Loading...', true);
        Allocdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                progress(0, 'Loading...', true);
                $scope.GenerateFilterPdf();
                progress(0, '', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GenerateFilterExcel();
                progress(0, '', false);
            }

        }
        else {
            $scope.DocTypeVisible = 0
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/UserRoleMapping/GetEmployeeData',
                method: 'POST',
                data: Allocdata,
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'EmployeeData.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {
            });
        }
    };


    //step2 click -> Getting Role/Module details
    $scope.LoadRMdetails_Step2 = function () {
        progress(0, 'Loading...', true);
        var encoding = encodeURIComponent(EmployeeId);
        //var Id = encoding.replace(/%2F/i, "_");
        var Id = encoding.split(/%2F/i).join("_");

        UserRoleMappingService.GetRMDetails_Step2(Id).then(function (response) {
            $scope.RolValidation = true;
            $scope.modules = response.data.modulelist;

            if (response.data.UserRoleVM.length > 0) {
                var id = $scope.Currentmodule.MOD_NAME;
                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                    $scope.acceslevel = rol;
                    $scope.Currentmodule.MapAccess = response.data.UserRoleVM[0].ROL_ACRONYM;
                    for (i = 0; i < response.data.UserRoleVM.length; i++) {
                        var a = _.find($scope.acceslevel, { ROL_ID: response.data.UserRoleVM[i].ROL_ID });
                        if (a)
                            a.isChecked = true;
                    }
                    // ChkLength                     
                    var ChkLength = $filter('filter')($scope.acceslevel, { isChecked: true }).length;
                    if (ChkLength == 0) {
                        var a = _.find($scope.acceslevel, { ROL_ID: 6 });
                        a.isChecked = true;
                        //for (i = 0; i < $scope.acceslevel.length ; i++) {                      
                        //    if ($scope.acceslevel[i].ROL_ID == "6") {
                        //        $scope.acceslevel[i].isChecked = true;
                        //    }
                        //}
                    }
                });
            }
            else {
                //UtilityService.GetRoles(1).then(function (rol) {
                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                    $scope.acceslevel = rol;
                    for (i = 0; i < $scope.acceslevel.length; i++) {
                        // var a = _.find($scope.acceslevel, { ROL_ID: 6 });                        
                        if ($scope.acceslevel[i].ROL_ID == "6") {
                            $scope.acceslevel[i].isChecked = true;
                        }
                    }
                });
            }

            progress(0, 'Loading...', false);
        }, function (error) {
            progress(0, '', false);
        });
    };

    $scope.LoadMappingDtls_Step3 = function () {
        progress(0, 'Loading...', true);
        var encoding = encodeURIComponent(EmployeeId);
        //var Id = encoding.replace(/%2F/i, "_");
        var Id = encoding.split(/%2F/i).join("__");

        UserRoleMappingService.LoadMappingDtls_Step3(Id).then(function (response) {
            //response
            $scope.UsrMappingDtls(response);
            progress(0, 'Loading...', false);
        }, function (error) {
            progress(0, '', false);
        });
    };

    $scope.UsrMappingDtls = function (response) {
        console.log(response);
        progress(0, 'Loading...', true);
        UtilityService.getCountires(1).then(function (Condata) {
            $scope.Countrylst = Condata.data;
            if (response.data.selectedCountries != null) {
                for (i = 0; i < response.data.selectedCountries.length; i++) {
                    var a = _.find($scope.Countrylst, { CNY_CODE: response.data.selectedCountries[i].CNY_CODE });
                    a.ticked = true;
                }
            }
            //Zone
            UtilityService.getZone(1).then(function (zndata) {
                $scope.Zonelst = zndata.data;
                if (response.data.selectedZone != null) {
                    for (i = 0; i < response.data.selectedZone.length; i++) {
                        var a = _.find($scope.Zonelst, { ZN_CODE: response.data.selectedZone[i].ZN_CODE });
                        a.ticked = true;
                    }
                }
                //State
                UtilityService.getState(1).then(function (stedata) {
                    $scope.Statelst = stedata.data;
                    if (response.data.selectedState != null) {
                        for (i = 0; i < response.data.selectedState.length; i++) {
                            var a = _.find($scope.Statelst, { STE_CODE: response.data.selectedState[i].STE_CODE });
                            a.ticked = true;
                        }
                    }
                    //cities
                    UtilityService.getCities(1).then(function (Cnydata) {
                        $scope.Citylst = Cnydata.data;
                        if (response.data.selectedCities != null) {
                            for (i = 0; i < response.data.selectedCities.length; i++) {
                                var a = _.find($scope.Citylst, { CTY_CODE: response.data.selectedCities[i].CTY_CODE });
                                a.ticked = true;
                            }
                        }
                        //locs
                        UtilityService.getLocations(1).then(function (Ctydata) {
                            $scope.Locationlst = Ctydata.data;
                            if (response.data.selectedLocations != null) {
                                for (i = 0; i < response.data.selectedLocations.length; i++) {
                                    var a = _.find($scope.Locationlst, { LCM_CODE: response.data.selectedLocations[i].LCM_CODE });
                                    a.ticked = true;
                                }
                            }
                            //towers

                            UtilityService.getTowers(1).then(function (Locdata) {
                                $scope.Towerlist = Locdata.data;
                                if (response.data.selectedTowers != null) {
                                    for (i = 0; i < response.data.selectedTowers.length; i++) {
                                        var a = _.find($scope.Towerlist, { TWR_CODE: response.data.selectedTowers[i].TWR_CODE });
                                        a.ticked = true;
                                    }
                                }
                                //floors
                                UtilityService.getFloors(1).then(function (Flrdata) {
                                    $scope.Floorlist = Flrdata.data;
                                    if (response.data.selectedFloors != null) {
                                        for (i = 0; i < response.data.selectedFloors.length; i++) {
                                            var a = _.find($scope.Floorlist, { FLR_CODE: response.data.selectedFloors[i].FLR_CODE });
                                            if (a)
                                                a.ticked = true;
                                        }
                                    }
                                    //parent entity
                                    UtilityService.getParentEntity(1).then(function (parent) {
                                        $scope.Parentlist = parent.data;
                                        if (response.data.selectedParentEntity != null) {
                                            for (i = 0; i < response.data.selectedParentEntity.length; i++) {
                                                var a = _.find($scope.Parentlist, { PE_CODE: response.data.selectedParentEntity[i].PE_CODE });
                                                a.ticked = true;
                                            }
                                        }
                                        //child
                                        UtilityService.getChildEntity(1).then(function (chddata) {
                                            $scope.Childlist = chddata.data;
                                            if (response.data.selectedChildEntity != null) {
                                                for (i = 0; i < response.data.selectedChildEntity.length; i++) {
                                                    var a = _.find($scope.Childlist, { CHE_CODE: response.data.selectedChildEntity[i].CHE_CODE });
                                                    a.ticked = true;
                                                }
                                            }
                                            //vert
                                            UtilityService.getVerticals(1).then(function (Vedata) {
                                                $scope.Verticallist = Vedata.data;
                                                if (response.data.selectedVerticals != null) {
                                                    for (i = 0; i < response.data.selectedVerticals.length; i++) {
                                                        var a = _.find($scope.Verticallist, { VER_CODE: response.data.selectedVerticals[i].VER_CODE });
                                                        a.ticked = true;
                                                    }
                                                }
                                                else {
                                                    $scope.Verticallist = Vedata.data;
                                                    //$scope.CostCenterlist = [];
                                                }
                                                //cost cent
                                                UtilityService.getCostCenters(1).then(function (Csdata) {
                                                    $scope.CostCenterlist = Csdata.data;
                                                    if (response.data.selectedCostcenter != null) {
                                                        for (i = 0; i < response.data.selectedCostcenter.length; i++) {
                                                            var a = _.find($scope.CostCenterlist, { Cost_Center_Code: response.data.selectedCostcenter[i].Cost_Center_Code });
                                                            a.ticked = true;
                                                        }
                                                        progress(0, 'Loading...', false);
                                                    } else {
                                                    $scope.CostCenterlist = Csdata.data;
                                                        progress(0, 'Loading...', false);}
                                                    
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    };

});



