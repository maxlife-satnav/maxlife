﻿var app = angular.module('QuickFMS', []);
app.service("SystemPreferencesService", function ($http, $q)
{
    
    this.GetController = function () {
        var deferred = $q.defer();
        return $http.get('../../../api/SystemPreferences')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetModules = function () {
        var deferred = $q.defer();
        return $http.get('../../../api/SystemPreferences/AllModules')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

$(document).on('click', '.panel-heading span.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    }
})



app.controller('SystemPreferencesController', function ($scope, $q, SystemPreferencesService) {
    $scope.Currentmodule = {};
    $scope.modules = {};

    SystemPreferencesService.GetModules().then(function (data) {
        $scope.modules = data;
        console.log(data);
    }, function (error) {
        console.log(error);
    });
    $scope.Currentmodule.MOD_NAME = "ALL";
});
