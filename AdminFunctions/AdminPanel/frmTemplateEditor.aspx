﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmTemplateEditor.aspx.vb" Inherits="AdminFunctions_AdminPanel_Template_Editors" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
   <%-- <link href="../../BootStrapCSS/RichText%20Editor/css/bootstrap-wysihtml5.css" rel="stylesheet" />--%>
    <script src="../../BootStrapCSS/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" language="javascript">
        tinyMCE.init({
            // General options
            mode: "textareas",
            theme: "advanced",
            plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups",

        });
    </script>
    
</head>
<body>
    <form method="POST" id="testformid">
    </form>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Template Editing
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Mail Description <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlMailDescription" Display="None"
                                            ErrorMessage="Please Select Mail Subject" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlMailDescription" runat="server" CssClass="selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlMailDescription_SelectedIndexChanged " AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <h6>
                                            <label class="col-md-12 control-label" style="text-align: left;">*Modify your Template Below</label></h6>
                                    </div>
                                </div>
                                <div class="hero-unit" style="margin-top: 40px">
                                    <%--<textarea form="testformid" name="taname" class="textarea form-control" style="width: 810px; height: 200px" id="txtAreaContent" wrap="soft" runat="server" contenteditable></textarea>--%>
                                    <asp:TextBox ID="txtAreaContent" runat="server" TextMode="MultiLine" Height="600px" Width="800px"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit"
                                        CssClass="btn btn-primary custom-button-color" />
                                    <span id="spantext" runat="server"></span>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/RichText%20Editor/js/wysihtml5-0.3.0.js"></script>
    <script src="../../BootStrapCSS/RichText%20Editor/js/bootstrap3-wysihtml5.js"></script>

    <script>
        $('.textarea').wysihtml5();

    </script>

    <script type="text/javascript" charset="utf-8">
        $(prettyPrint);
    </script>
</body>
</html>
