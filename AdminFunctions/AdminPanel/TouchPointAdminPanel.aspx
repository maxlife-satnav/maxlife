﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Admin Panel</title>
    <link href="../../BootStrapCSS/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/bootstrap-select.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/amantra.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <input type="hidden" id="SortField" />
    <input type="hidden" id="SortDirection" />
    <input type="hidden" id="PageCount" />
    <input type="hidden" id="PageSize" />
    <input type="hidden" id="CurrentPageIndex" />
    <input type="hidden" id="TotalCount" />

    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Auto Emails Admin Panel 
                            </legend>
                        </fieldset>
                        <form id="form1" runat="server" class="form-horizontal">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <b>Select Page Size:</b>
                                            <select id="ddlPageSize" class="btn dropdown-toggle selectpicker btn-default" data-live-search="true">
                                                <option value="All">All</option>
                                                <option value="10" selected="selected">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>
                                        </div>
                                        <div class="panel-body">
                                            <table id="tbltpuchpoints" class="table table-condensed table-bordered table-hover table-striped">
                                                <tr>
                                                    <th style='width: 80px;'>Module</th>
                                                    <th style='width: 350px;'>Name</th>
                                                    <th style='width: 60px;'>Type</th>
                                                    <%--<th style='width: 150px;'>Map</th>--%>
                                                    <th style='width: 80px;'>
                                                        <input type='checkbox' disabled="disabled" name="chkAll" id='chkAll' class="chkAll" />Select All</th>
                                                </tr>
                                            </table>
                                            <div id="paginationdiv" style="margin: -39px; flex-align: end; padding-left: 420px">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button id="btnSubmit" style="display: none" class='btn btn-primary'><span class='glyphicon glyphicon-refresh'></span>Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
<script src="../../BootStrapCSS/Scripts/bootstrap.min.js"></script>
<script src="../../BootStrapCSS/Scripts/modernizr.min.js"></script>
<script src="../../BootStrapCSS/Scripts/bootstrap-paginator.js"></script>
<script>
    $(document).ready(function () {
        FunBindGrid();
    });

    $("#ddlPageSize").change(function () {

        if ($("#ddlPageSize").val() == 'All') {
            $("#PageSize").val($("#TotalCount").val());
        }
        else {
            $("#PageSize").val($("#ddlPageSize").val());
            $("#btnSubmit").hide()
            $("input.chkAll").attr("disabled", true);
        }
        FunBindGrid();
    })

    $('.chkAll').click(function () {
        $('.chk1').prop('checked', $(this).is(':checked'));
    });

    $("#btnSubmit").click(function () {
        if ($('#tbltpuchpoints').find('input[type=checkbox]:checked').length == 0) {
            alert('Please select atleast one checkbox');
            return false;
        }
        var values = new Array();
        $.each($("input[name='chk1']:checked"),
               function () {
                   values.push($(this).attr('id'));
                   //emailids.push($("#txtEmail" + $(this).attr("id")).val());                   
               });
        
        var dataObject = { Catarray: values };
        console.log(dataObject);
        $.post("../../api/TouchPointAdminPanelAPI/InsertTouchPoints", dataObject, function (result) {
            alert('Auto Emails Updated Successfully');
        });
        
        return false;
    });
    
    function chkAllRemove() {
        $("#chkAll").prop('checked', $("input:checkbox[name=chk1]:checked").length == $("input:checkbox[name=chk1]").length);
    }

    function Enable() {
        $("#btnSubmit").show()
        $("input.chkAll").attr("disabled", false);
        $("input:checkbox[name=chk1]").prop("disabled", false);
    }
    function FunBindGrid(e, originalEvent, type, page) {

        var pageindex = page;
        if (pageindex == undefined)
            pageindex = 1;

        $("#CurrentPageIndex").val(pageindex);
        var varsortingpaginginfo = { SortField: $("#SortField").val(), SortDirection: $("#SortDirection").val(), CurrentPageIndex: $("#CurrentPageIndex").val(), PageSize: $("#PageSize").val(), PageCount: $("#PageCount").val(), TotalCount: $("#TotalCount").val() };
        totalPages: $("#PageCount").val();
        $.ajax({
            url: '../../api/TouchPointAdminPanelAPI/GetTouchPoints',
            data: varsortingpaginginfo,
            method: "GET",
            contentType: "application/json",
            success: function (data) {
                var table = $('#tbltpuchpoints');
                $("#tbltpuchpoints").find('tr').slice(1).remove();
                $("#SortField").val(data.SortingPagingInfo.SortField);
                $("#SortDirection").val(data.SortingPagingInfo.SortDirection);
                $("#CurrentPageIndex").val(data.SortingPagingInfo.CurrentPageIndex);
                $("#PageSize").val(data.SortingPagingInfo.PageSize);
                $("#PageCount").val(data.SortingPagingInfo.PageCount);
                $("#TotalCount").val(data.SortingPagingInfo.TotalCount);
                $.each(data.TableData, function (key, value) {
                    table.append("<tr>" +
                        "<td>" + value.TPA_MODULE + "</td>" +
                        "<td>" + value.TPA_NAME + "</td>" +
                        "<td>" + value.TPA_TYPE + "</td>" +
                        //"<td>" + "<textarea type='text' id='txtEmail" + value.TPA_ID + "' name='txtEmail[]' cols='40' rows='3'> " + value.TCA_EMAIL + " </textarea> " + "</td>" +
                        //"<td>" + "<textarea name='txtEMail' id='txtEmail' data-TPA_ID=" + value.TPA_ID + " cols='40' rows='3'>" + value.TCA_EMAIL + "</textarea> " + "</td>" +
                        "<td>" + "<input type='checkbox' disabled='disabled' name ='chk1' id=" + value.TPA_ID + " " + value.TCA_STATUS + " class='chk1' onclick='chkAllRemove()' /> " + "</td>" +
                        "</tr>");
                });
                Pagination();
                if ($("#ddlPageSize").val() == 'All')
                    Enable();
                chkAllRemove();
            }
        });
    }

    function Pagination() {
        $("#paginationdiv").bootstrapPaginator({
            currentPage: $("#CurrentPageIndex").val(),
            totalPages: $("#PageCount").val(),
            onPageClicked: FunBindGrid
        });
    }
</script>
