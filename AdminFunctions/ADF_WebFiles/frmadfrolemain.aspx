<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfrolemain.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfrolemain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

			<blockquote dir="ltr" style="MARGIN-RIGHT: 0px"> <blockquote dir="ltr" style="MARGIN-RIGHT: 0px">
					<blockquote dir="ltr" style="MARGIN-RIGHT: 0px"> <blockquote dir="ltr" style="MARGIN-RIGHT: 0px">
							<blockquote dir="ltr" style="MARGIN-RIGHT: 0px"> <blockquote dir="ltr" style="MARGIN-RIGHT: 0px">
									
										<asp:label id="lblHead" runat="server" Width="95%" cssclass="clsHead">Admin Functions<hr width="70%" align="center" /></asp:label><asp:panel id="pnlRole2" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 48px" runat="server"
											Width="95%" HorizontalAlign="Right">
											<p class="clsHypLink"><a style="WIDTH: 200px" href="frmADFaddrole.aspx?src=NEW">Add New 
													Role</a></p> 
										</asp:panel><asp:panel id="pnlRole" runat="server" Width="95%" HorizontalAlign="Center" Height="161px">
											<table width="100%">
												<tr>
													<td class="clstblnew" width="50%" colspan="2">Roles</td>
												</tr>
											</table>
											<div align="center">
												<asp:gridview id="grdProcess" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="true">
													<Columns>
														<asp:BoundField Visible="False" DataField="rol_id"></asp:BoundField>
														<asp:HyperLinkField DataNavigateUrlFields="rol_id" DataNavigateUrlformatString="frmADFaddrole.aspx?rol_id={0}"
															DataTextField="ROL_ACRONYM" HeaderText="Role Acronym" DataTextformatString="{0:c}">
															<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
														</asp:HyperLinkField>
														<asp:BoundField DataField="ROL_DESCRIPTION" HeaderText="Role Description">
															<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
														</asp:BoundField>
														<asp:BoundField DataField="SCP_DESCRIPTION" HeaderText="Role Scope">
															<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
														</asp:BoundField>
														<asp:ButtonField DataTextField="ROL_ACTIVE" HeaderText="Active">
															<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonField>
													</Columns>
												</asp:gridview></div>
											<div align="center"><br />
												<asp:Label id="lblmsg" cssclass="clsMessage" Runat="server"></asp:Label></div>
											
										</asp:panel>
									
								</blockquote></blockquote></blockquote></blockquote></blockquote>
			</blockquote>
		</asp:Content>