Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfaddrole
    Inherits System.Web.UI.Page
#Region "General Variables"
    Dim drAddRole As SqlDataReader
    Dim strRoleAcr, strRoledesc, strSql As String
    Dim iRoleScopeId As Integer
    Dim iRole As Integer
    Dim dsAddRole As New DataSet()
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Put user code to initialize the page here

            If Not IsPostBack Then
                strSql = "SELECT SCP_ID,SCP_DESCRIPTION " & _
                         " FROM  " & Session("TENANT") & "."  & "SCOPE WHERE SCP_ACTIVE = 1 ORDER BY SCP_DESCRIPTION"

                drAddRole = SqlHelper.ExecuteReader(CommandType.Text, strSql)
                cboScope.DataSource = drAddRole
                cboScope.DataTextField = "SCP_DESCRIPTION"
                cboScope.DataValueField = "SCP_ID"
                cboScope.DataBind()
                cboScope.Items.Insert(0, "--Select--")
                drAddRole.Close()

                If Request.QueryString("src") = "NEW" Then
                    changescr("New", 0)
                Else
                    changescr("Amd", Request.QueryString("rol_id"))
                End If
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub

    Public Sub changescr(ByVal str As String, ByVal sid As Integer)
        Try
            Select Case str
                Case "Amd"
                    'lblAddNew.Text = "Edit Role"
                    str = "SELECT * FROM " & _
                          "  " & Session("TENANT") & "."  & "ROLE WHERE ROL_ID=" & sid


                    dsAddRole = SqlHelper.ExecuteDataset(CommandType.Text, str)

                    txtRoleAcro.Text = dsAddRole.tables(0).Rows(0).Item("ROL_ACRONYM")
                    txtRoleDesc.Text = dsAddRole.tables(0).Rows(0).Item("ROL_DESCRIPTION")
                    txtHact.Value = dsAddRole.tables(0).Rows(0).Item("ROL_ACTIVE")
                    For iRole = 0 To cboScope.Items.Count - 1
                        If cboScope.Items(iRole).Value = dsAddRole.tables(0).Rows(0).Item("ROL_SCP_ID").ToString Then
                            cboScope.SelectedIndex = iRole
                        End If
                    Next
                    btnSubmit.Visible = False
                    btnUpdt.Visible = True
                Case "New"
                    ' lblAddNew.Text = "Add Role"
                    txtRoleAcro.Text = ""
                    txtRoleDesc.Text = ""
                    cboScope.SelectedIndex = 0
                    btnSubmit.Visible = True
                    btnUpdt.Visible = False
            End Select
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        'Getting all details into variables
        strRoleAcr = Replace(Trim(txtRoleAcro.Text), "'", "''")
        strRoledesc = Replace(Trim(txtRoleDesc.Text), "'", "''")
        iRoleScopeId = cboScope.SelectedItem.Value
        strSql = "SELECT COUNT(*) " & _
                 " FROM  " & Session("TENANT") & "."  & "ROLE WHERE ROL_ACRONYM='" & strRoleAcr & "'"

        If CType(SqlHelper.ExecuteScalar(CommandType.Text, strSql), Integer) > 0 Then
            lblErr.Text = ""
            lblErr.Text = "This Role Id already Exists!Please Enter Another!"
            lblErr.Visible = True
        Else
            'updating the database
            lblErr.Text = ""
            strSql = "Insert INTO  " & Session("TENANT") & "."  & "ROLE (ROL_ACRONYM,ROL_DESCRIPTION,ROL_SCP_ID,ROL_ACTIVE," _
                     & "ROL_UPDATED_BY) VALUES ('" & strRoleAcr & " ','" & strRoledesc & "'," & iRoleScopeId _
                     & "," & "'Y'" & ",'" & Session("UID") & "')"

            SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
            'Redirects to starting page after updating the status table
            Response.Redirect("frmADFrolemain.aspx?strMsg=Role+is+Added")
        End If
        'Catch ex As Exception
        '    'Dim pageURL, trace, MSG As String
        '    Session("pageURL") = ex.GetBaseException.ToString
        '    Session("trace") = (ex.StackTrace.ToString)
        '    Session("MSG") = Session("pageURL") + Session("Trace")
        '    Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        'End Try

    End Sub

    Private Sub btnUpdt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        'Getting all details into variables
        strRoleAcr = Replace(Trim(txtRoleAcro.Text), "'", "''")
        strRoledesc = Replace(Trim(txtRoleDesc.Text), "'", "''")
        iRoleScopeId = cboScope.SelectedItem.Value
        'updating the database
        lblErr.Text = ""
        strSql = "UPDATE  " & Session("TENANT") & "."  & "ROLE SET ROL_ACRONYM='" & strRoleAcr & "'," _
                 & "ROL_DESCRIPTION='" & strRoledesc & "',ROL_SCP_ID=" & iRoleScopeId _
                 & ",rol_ACTIVE='" & txtHact.Value & "',rol_UPDATED_BY='" & Session("UID") & "' WHERE ROL_ID=" _
                 & Request.QueryString("ROL_ID")


        SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
        Response.Redirect("frmADFrolemain.aspx?strSql=Role+is+Updated")
        '   Redirects to starting page after updating the status table
        'Catch ex As Exception
        '    'Dim pageURL, trace, MSG As String
        '    Session("pageURL") = ex.GetBaseException.ToString
        '    Session("trace") = (ex.StackTrace.ToString)
        '    Session("MSG") = Session("pageURL") + Session("Trace")
        '    Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        'End Try

    End Sub


    Private Sub btnReset_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtRoleAcro.Text = ""
        txtRoleDesc.Text = ""
    End Sub


    Protected Sub btnUpdt_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdt.Click

    End Sub

    Protected Sub btnSubmit_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

    End Sub
End Class


