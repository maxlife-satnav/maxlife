<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfaddpsy.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfaddpsy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<asp:panel id="Panel1" runat="server" HorizontalAlign="Center" Width="77.02%">
<asp:label id="lblPProcess" runat="server" cssclass="clsHead">Admin Functions<hr width="70%" align="center" /></asp:label>
			</asp:panel><br />
			&nbsp;
<asp:Panel id="Panel2" runat="server" Width="100%" Height="96px">
<table id="table2" cellspacing="0" cellpadding="0" width="100%" border="0">
				</table>
<table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
					<tr>
						<td><img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
						<td width="100%" bgColor="#f5821f"><strong>&nbsp;&nbsp;Add New Participating Process</strong></td>
						<td><img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16"/></td>
					</tr>
					<tr>
						<td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
						<td align="center">
							<asp:panel id="pnlPProcess" runat="server" Width="95%" HorizontalAlign="Center">
								<div align="left">
									<div align="left">
										<asp:Label id="lblMnd" runat="server" cssclass="clsNote">(*) Mandatory Fields.</asp:Label></div>
								</div>
								<table id="tblPProcess" width="100%" align="center">
									<tr>
										<td class="clslabel" width="30%">Participating Process<font class="clsNote">* </font>
										</td>
										<td width="70%">
											<asp:textbox id="txtPsyTitle" runat="server" Width="100%" cssclass="clsTextField"></asp:textbox>
											<asp:requiredfieldvalidator id="rfvPsyStem" runat="server" ErrorMessage="Please Enter the Participating Process!"
												ControlToValidate="txtPsytitle" Display="None"></asp:requiredfieldvalidator></td>
									</tr>
									<tr>
										<td class="clslabel" width="30%">URL<font class="clsNote">*
												<asp:requiredfieldvalidator id="rfvpurl" runat="server" ErrorMessage="Please Enter The Participating Process URL !"
													ControlToValidate="txtPsyurl" Display="None"></asp:requiredfieldvalidator></font></td>
										<td width="70%">
											<asp:textbox id="txtPsyUrl" runat="server" Width="100%" cssclass="clsTextField"></asp:textbox></td>
									</tr>
									<tr>
										<td class="clslabel" width="30%">Service Page<font class="clsNote">* </font>
										</td>
										<td width="70%">
											<asp:textbox id="txtService" runat="server" Width="100%" cssclass="clsTextField" ReadOnly="true"></asp:textbox>
											<asp:requiredfieldvalidator id="rfvser" runat="server" ErrorMessage="Please Enter the Service Page!" ControlToValidate="txtService"
												Display="None"></asp:requiredfieldvalidator></td>
									</tr>
									<tr>
										<td class="clslabel" width="30%">Description
										</td>
										<td width="70%">
											<asp:textbox id="txtPsyDesc" runat="server" Width="100%" cssclass="clsTextField" Height="50px"
												TextMode="MultiLine"></asp:textbox></td>
									</tr>
								</table>
								<br />
								<table id="table1" cellspacing="1" cellpadding="1" width="100%" border="1">
									<tr>
										<td style="WIDTH: 311px" align="center">
											<asp:button id="btnSubmit" runat="server" cssclass="clsButton" Text="Submit"></asp:button>
											<asp:button id="btnUpdt" runat="server" cssclass="clsButton" Text="Update" Visible="False"></asp:button></td>
										<td align="center"><input class="clsButton" id="btnReset" type="reset" value="Reset" runat="server" DESIGNTIMEDRAGDROP="52"/></td>
										<td align="center"><input class="clsButton" id="btnBack" onclick="butBack_OnClick()" type="button" value="Back"
												runat="server" /></td>
									</tr>
								</table>
								<input id="txtHact" type="hidden" size="1" name="hact" runat="server"/>
								<asp:ValidationSummary id="vsPProcess" runat="server"  CssClass="clsMessage" ShowMessageBox="true" ShowSummary="False"></asp:ValidationSummary>
								
									<asp:Label id="lblErr" runat="server" cssclass="clsWarn" Visible="False"></asp:Label>
							</asp:panel></td>
						<td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
					</tr>
					<tr>
						<td><img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9"></td>
						<td background="../../Images/table_bot_mid_bg.gif"><img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25"></td>
						<td><img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16"></td>
					</tr>
				</table></asp:Panel>
			<div align="left">&nbsp;</div>
	
</asp:Content>