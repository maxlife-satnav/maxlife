Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfaddpsy
    Inherits System.Web.UI.Page
#Region "General Variables"
    Dim drAddPsy As SqlDataReader
    Dim strSql As String
    Dim strSql2
    Dim dsAddPsy As New DataSet
    Dim strTitle, strURL, strSerPage, strDescript As String
#End Region

    Public Sub changescr(ByVal strSql As String, ByVal sid As Integer)
        Try
            Select Case strSql
                Case "Amd"
                    lblPProcess.Text = "Edit Participating System"

                    strSql = "SELECT * FROM  " & Session("TENANT") & "."  & "PARTICIPATING_SYSTEM" & _
                             " WHERE PSY_ID=" & sid
                    dsAddPsy = SqlHelper.ExecuteDataset(CommandType.Text, strSql)
                    txtPsyDesc.Text = dsAddPsy.tables(0).Rows(0).Item("PSY_DESCRIPTION")
                    txtPsyTitle.Text = dsAddPsy.tables(0).Rows(0).Item("PSY_TITLE")
                    txtPsyUrl.Text = dsAddPsy.tables(0).Rows(0).Item("PSY_URL")
                    txtService.Text = dsAddPsy.tables(0).Rows(0).Item("PSY_SERVICE_PAGE")
                    txtHact.Value = dsAddPsy.tables(0).Rows(0).Item("PSY_ACTIVE")
                    btnSubmit.Visible = False
                    btnUpdt.Visible = True
                Case "New"
                    lblPProcess.Text = "Add Participating Process"
                    txtPsyDesc.Text = ""
                    txtPsyTitle.Text = ""
                    txtPsyUrl.Text = Application("FMGURL")
                    txtService.Text = "frmAMTchk.aspx"
                    btnSubmit.Visible = True
                    btnUpdt.Visible = False
            End Select
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try

            If Not IsPostBack Then
                If Request.QueryString("src") = "Add" Then
                    changescr("New", 0)
                Else
                    changescr("Amd", Request.QueryString("SID"))
                End If
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Try
        'Getting all details into variables
        strTitle = Replace(Trim(txtPsyTitle.Text), "'", "''")
        strURL = Replace(Trim(txtPsyUrl.Text), "'", "''")
        strSerPage = Replace(Trim(txtService.Text), "'", "''")
        strDescript = Replace(Trim(txtPsyDesc.Text), "'", "''")

        strSql2 = "SELECT COUNT(*) FROM  " & Session("TENANT") & "."  & "PARTICIPATING_SYSTEM WHERE PSY_TITLE='" & strTitle & "'"


        If CType(SqlHelper.ExecuteScalar(CommandType.Text, strSql2), Integer) > 0 Then
            lblErr.Text = "This Participating System already Exists! Please Enter Another!"
            lblErr.Visible = True
        Else
            'updating the database
            lblErr.Text = ""
            strSql = "INSERT INTO  " & Session("TENANT") & "." & "PARTICIPATING_SYSTEM (PSY_TITLE,PSY_URL,PSY_DESCRIPTION,PSY_SERVICE_PAGE," _
                     & "PSY_ACTIVE,PSY_UPDATED_BY,PSY_UPDATED_ON) Values (" & "'" & strTitle & "'," & _
                     "'" & strURL & "'," & _
                     "'" & strDescript & "'," & _
                     "'" & strSerPage & "'," & _
                     "  'Y' " & ",'" & Session("UID") & "','" & getoffsetdatetime(DateTime.Now) & "')"

            SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)

            '   Redirects to starting page after updating the status table
            Response.Redirect("frmADFpsydb.aspx?strMsg=New+Participating+Process+is+Added")
        End If
        'Catch ex As Exception
        '    'Dim pageURL, trace, MSG As String
        '    Session("pageURL") = ex.GetBaseException.ToString
        '    Session("trace") = (ex.StackTrace.ToString)
        '    Session("MSG") = Session("pageURL") + Session("Trace")
        '    Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        'End Try
    End Sub
    Private Sub btnUpdt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdt.Click
        'Try
        'Getting all details into variables
        strTitle = Replace(Trim(txtPsyTitle.Text), "'", "''")
        strURL = Replace(Trim(txtPsyUrl.Text), "'", "''")
        strSerPage = Replace(Trim(txtService.Text), "'", "''")
        strDescript = Replace(Trim(txtPsyDesc.Text), "'", "''")
        'updating the database
        lblErr.Text = ""
        strSql = "UPDATE  " & Session("TENANT") & "." & "PARTICIPATING_SYSTEM  SET PSY_TITLE='" & strTitle & "'," _
                 & "PSY_URL='" & strURL & "',PSY_DESCRIPTION='" & strDescript _
                 & "',PSY_SERVICE_PAGE='" & strSerPage & "',PSY_ACTIVE='" & txtHact.Value _
                 & "',PSY_UPDATED_BY='" & Session("UID") & "',PSY_UPDATED_ON='" & getoffsetdatetime(DateTime.Now) _
                 & "' WHERE PSY_ID=" & Request.QueryString("SID")

        SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)


        Response.Redirect("frmADFpsydb.aspx?strMsg=Process+is+Updated")
     
    End Sub
   
    Private Sub btnReset_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.ServerClick
        Try
            txtPsyTitle.Text = ""
            txtPsyUrl.Text = ""
            txtService.Text = ""
            txtPsyDesc.Text = ""
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
End Class


