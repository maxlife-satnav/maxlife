<%@ Page Language="vb" AutoEventWireup="false" Inherits="frmADFempdtls" CodeFile="frmADFempdtls.aspx.vb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body >
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Employee Details</legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Name</label>
                                            <div class="col-md-8">
                                                <asp:TextBox ID="txtEname" TabIndex="6" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Email</label>
                                            <div class="col-md-8">
                                                <asp:TextBox ID="txtEmail" TabIndex="6" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <%--    <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">City<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Location</label>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="ddlSelectLocation" runat="server" CssClass="selectpicker" data-live-search="true" multiple="multiple">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            <div id="pnlChk" runat="server" cssclass="row">
                                <fieldset>
                                    <legend>Roles</legend>
                                </fieldset>
                                <div class="row">&nbsp</div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <asp:CheckBoxList ID="chkList" runat="server" TabIndex="6" RepeatColumns="3" CssClass="col-md-12 control-label">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <asp:Button ID="btnNext" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"></asp:Button>
                                    <asp:Button ID="btnback2" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/AdminFunctions/ADF_WebFiles/frmADFsearch.aspx" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblmessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="False">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
   <%-- <script type="text/javascript">
        function foo() {
            HideProductGroupCheckBox("chkList", "0,6,8,12,15,17,20", "control-label text-left");
        }

        function HideProductGroupCheckBox(checkBoxGroupId, checkBoxIdArray, cssClassForLabel) {
            if (checkBoxIdArray != '') {
                //Splits out the ids
                var checkBoxGroupIdArray = checkBoxIdArray.split(',');
                alert(checkBoxGroupIdArray);
                for (var i = 0; i < checkBoxGroupIdArray.length; i++) {
                    // Trim the excess whitespace.
                    //checkBoxGroupIdArray[i] = checkBoxGroupIdArray[i].replace(/^\s*/, "").replace(/\s*$/, "");
                    // Hide the checkbox
                    $("input[id='" + checkBoxGroupId + "_" + checkBoxGroupIdArray[i] + "']").css('display', 'none');
                    // Add class to checkbox label
                    $("label[for='" + checkBoxGroupId + "_" + checkBoxGroupIdArray[i] + "']").addClass(cssClassForLabel);
                }
            }
        }

    </script>--%>
    <script type="text/javascript">
        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        $(document).ready(function () {
            $('#btnNext').on('click', function () {
                var selecteditems = $('#ddlSelectLocation').val();
                //alert(GetParameterValues('Eid'));

            });
        });
        </script>

</body>
</html>
