Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsSubSonicCommonFunctions

Partial Class AdminFunctions_ADF_WebFiles_frmadfaddprocessowner
    Inherits System.Web.UI.Page
#Region "General Variables"
    Dim strSql As String
    Dim iPsyID, iUsrID As Integer
    Dim strMsg, strTfld, strSqlVfld As String

    Dim dsPOwner As New DataSet()

#End Region
    Public Sub loadvalues(ByVal strSql As String, ByVal CMBSRC As UI.WebControls.DropDownList)
        Try
            Select Case strSql
                Case "Process"
                    strSql = "SELECT PSY_ID,PSY_TITLE " & _
                              " FROM  " & Session("TENANT") & "." & "PARTICIPATING_SYSTEM ORDER BY PSY_TITLE"
                    'strSql = "NP_INLINE_PROCESS_SYSTEM"
                    strMsg = "No Processes Avilable!"
                    strTfld = "PSY_TITLE"
                    strSqlVfld = "PSY_ID"
                Case "Owner"
                    'strSql = "NP_INLINE_OWNER_SYSTEM"
                    strSql = "SELECT DISTINCT URL_USR_ID " & _
                            " FROM  " & Session("TENANT") & "." & "USER_ROLE ORDER BY URL_USR_ID"
                    strMsg = "No Users Available!"
                    strTfld = "URL_USR_ID"
                    strSqlVfld = "URL_USR_ID"
            End Select
            dsPOwner = SqlHelper.ExecuteDataset(CommandType.Text, strSql)
            CMBSRC.DataSource = dsPOwner
            CMBSRC.DataTextField = strTfld
            CMBSRC.DataValueField = strSqlVfld
            CMBSRC.DataBind()
            dsPOwner.Clear()
            CMBSRC.Items.Insert(0, "--Select--")
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If Not IsPostBack Then
                loadvalues("Process", cboProcess)
                loadvalues("Owner", cboOwner)
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Try
        iPsyID = Trim(cboProcess.SelectedItem.Value)
        iUsrID = Trim(cboOwner.SelectedItem.Value)

        strSql = "SELECT COUNT(*) " & _
                 " FROM " & Session("TENANT") & "." & "PROCESS_OWNER " & _
                 " WHERE POW_PSY_ID =" & iPsyID & " AND POW_USR_ID = '" & iUsrID & "'"

        'strSql = "NP_INLINE_PROCESS_OWNER_COUNT"
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "NP_INLINE_PROCESS_OWNER_COUNT")
        'sp.Command.AddParameter("@PSYID", iPsyID, DbType.String)
        'sp.Command.AddParameter("@USRID", iUsrID, DbType.String)
        'If CType(sp.ExecuteScalar(), Integer) > 0 Then
        If CType(SqlHelper.ExecuteScalar(CommandType.Text, strSql), Integer) > 0 Then

            lblerr.Text = "This Process Owner Already Exists!"
            lblerr.Visible = True
        Else
            strSql = "INSERT INTO  " & Session("TENANT") & "." & "PROCESS_OWNER (POW_PSY_ID, POW_USR_ID, POW_ACTIVE," _
                     & "POW_UPDATED_BY, POW_UPDATED_ON)VALUES(" & iPsyID & "," & iUsrID & ",'Y'," _
                     & "'" & Session("UID") & "','" & getoffsetdatetime(DateTime.Now) & "')"
            'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "NP_INLINE_INSERT_PROCESS_OWNER")
            'sp1.Command.AddParameter("@PSYID", iPsyID, DbType.String)
            'sp1.Command.AddParameter("@USRID", iUsrID, DbType.String)
            'sp1.Command.AddParameter("@UPDATEDBY", Session("Uid"), DbType.String)
            'sp1.ExecuteScalar()
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
            Response.Redirect("frmADFprocessownermain.aspx?strMsg=Process+Owner+is+Updated")

        End If
        'Catch exp As Exception
        '    Response.Write(exp.GetBaseException)
        'End Try
        '  Catch ex As Exception
        '    'Dim pageURL, trace, MSG As String
        '    Session("pageURL") = ex.GetBaseException.ToString
        '    Session("trace") = (ex.StackTrace.ToString)
        '    Session("MSG") = Session("pageURL") + Session("Trace")
        '    Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        'End Try
    End Sub

    Private Sub btnReset_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboProcess.SelectedItem.Value = ""
            cboOwner.SelectedItem.Value = ""
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
End Class
