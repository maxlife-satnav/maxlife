Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager


Partial Class frmADFemployeesearch
    Inherits System.Web.UI.Page

#Region "Variables"

    Dim temployees As Array
    Dim strEmpid As String
    Dim iEmp, iFlg As Integer
    Dim ObjCon As New SqlConnection()
#End Region

    Private Sub page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim ObjData As SqlDataReader
        If Not IsPostBack Then
            'strSQL = "select DISTINCT DEp_ID,DEp_NAME" & _
            '         " FROM  " & Session("TENANT") & "."  & "Department where DEp_STA_ID=1"

            strSQL = "AMT_DeptName_SP"
            ObjData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, strSQL)
            cboDept.DataSource = ObjData
            cboDept.DataValueField = "DEp_ID"
            cboDept.DataTextField = "DEp_NAME"
            cboDept.DataBind()
            ObjData.Close()
            cboDept.Items.Insert(0, "--Select--")
            cboDept.Items.Insert(1, "--All--")

        End If


    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        If Page.IsValid = True Then
            iFlg = 0
            If Trim(TxtEid.Text) <> "" Then
                temployees = Split(TxtEid.Text, ",")
                For iEmp = LBound(temployees) To UBound(temployees)
                    strEmpid = temployees(iEmp)
                    If IsNumeric(strEmpid) = False Then
                        iFlg = 1
                    End If
                Next
            End If
            If iFlg = 0 Then
                lblchkemp.Visible = False
                Dim strUrl As String = "frmADFempsearchresults.aspx?eid=" & TxtEid.Text & "&Fname=" & txtFname.Text & "&Mname=" & txtMname.Text & "&LName=" & TxtLname.Text & "&Ext=" & Txtext.Text & "&email=" & txtEmail.Text & "&ccname=" & cboDept.SelectedIndex & ""
                Response.Redirect(strUrl, False)
            Else
                lblchkemp.Visible = True
                lblchkemp.Text = "please enter a numeric value for the Employee Id"
            End If
        End If
    End Sub

End Class

