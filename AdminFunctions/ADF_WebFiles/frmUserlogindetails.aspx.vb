﻿
Imports System
Imports System.Data
Imports Microsoft.Reporting.WebForms

Partial Class AdminFunctions_ADF_WebFiles_frmUserlogindetails
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure("dbo." & "GET_LOGIN_DETAILS_SCHEMA_WISE")
        sp.Command.AddParameter("@SCHEMA", ddlclient.SelectedValue, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        'gdavail.DataSource = Session("dataset")
        'gdavail.DataBind()

        Dim rds As New ReportDataSource()
        rds.Name = "GET_LOGIN_DETAILS_SCHEMA_WISE"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/AdminFunctions/Userlogindetails.rdlc")
        'ReportViewer1.LocalReport.EnableHyperlinks = True

    End Sub

    'Public Sub bindgrid()
    '    Dim sp As New SubSonic.StoredProcedure("dbo." & "GET_LOGIN_DETAILS_SCHEMA_WISE")
    '    sp.Command.AddParameter("@SCHEMA", ddlclient.SelectedValue, DbType.String)
    '    Session("dataset") = sp.GetDataSet()
    '    gdavail.DataSource = Session("dataset")
    '    gdavail.DataBind()
    'End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure("dbo." & "GET_SCHEMA_DETAILS")
            ddlclient.DataSource = sp.GetDataSet()
            ddlclient.DataValueField = "code"
            ddlclient.DataTextField = "name"
            ddlclient.DataBind()
            ddlclient.Items.Insert(0, "--All--")
        End If
    End Sub

    'Protected Sub gdavail_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gdavail.PageIndexChanging
    '    gdavail.PageIndex = e.NewPageIndex
    '    bindgrid()
    'End Sub

    'Protected Sub gdavail_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gdavail.Sorting
    '    Dim ds As DataSet = TryCast(Session("dataset"), DataSet)
    '    Dim dt = ds.Tables(0)

    '    If dt IsNot Nothing Then
    '        'Sort the data.
    '        dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
    '        gdavail.DataSource = dt
    '        gdavail.DataBind()
    '    End If
    'End Sub

    'Private Function GetSortDirection(ByVal column As String) As String

    '    ' By default, set the sort direction to ascending.
    '    Dim sortDirection = "ASC"

    '    ' Retrieve the last column that was sorted.
    '    Dim sortExpression = TryCast(ViewState("SortExpression"), String)

    '    If sortExpression IsNot Nothing Then
    '        ' Check if the same column is being sorted.
    '        ' Otherwise, the default value can be returned.
    '        If sortExpression = column Then
    '            Dim lastDirection = TryCast(ViewState("SortDirection"), String)
    '            If lastDirection IsNot Nothing _
    '              AndAlso lastDirection = "ASC" Then

    '                sortDirection = "DESC"

    '            End If
    '        End If
    '    End If

    '    ' Save new values in ViewState.
    '    ViewState("SortDirection") = sortDirection
    '    ViewState("SortExpression") = column

    '    Return sortDirection

    'End Function

End Class
