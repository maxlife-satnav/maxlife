<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfaddprocessowner.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfaddprocessowner" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script language="JavaScript" type="text/javascript">
			function butBack_OnClick()
					{
						window.history.back(-1);
					}
		</script>
		<script language="javascript" type="text/javascript"> 
    function noway(go)
    {
        if(document.all)
        {
            if (event.button == 2)
            {
                alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                return false;
            }
        }
        if (document.layers) 
        {
            if (go.which == 3)
            {
                alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                return false;
            }
        }
    } 
     if (document.layers) 
     {
        document.captureEvents(Event.MOUSEDOWN); 
     }
     document.onmousedown=noway;
    </script>
                            <table class="clstable" id="tbl" width="100%" align="center">
                            <tr>
                            <td width="100%" align="left"> <asp:label id="lblHead"  runat="server"	Width="100%" cssclass="clshead">Admin Functions
   <hr width="70%" align="center"/></asp:label></td>
                            </tr>
                            <tr>
                            <td width="100%" align="left"><asp:Label id="lblMnd" runat="server" cssclass="clsNote">(*) Mandatory Fields.</asp:Label>
			</td>
                            </tr>
                            <tr>
                            <td width="100%" align="left"><asp:ValidationSummary  CssClass="clsMessage" id="vsOwner" runat="server"
				ShowMessageBox="true" ShowSummary="False"></asp:ValidationSummary></td>
                            </tr>
                            </table>
  
   
			
			
   <asp:panel id="pnlService" 	runat="server" Width="95%" HorizontalAlign="Center">
				<table class="clstable" id="tblOwner" width="100%" align="center">
					<tr>
						<td class="clstblnew" width="50%" colspan="3">Add Process Owner</td>
					</tr>
					<tr>
						<td class="clsLabel" width="30%">Participating Process<font class="clsNote">*
								<asp:CompareValidator id="cfvPS" runat="server" Operator="NotEqual" ValueToCompare="--Select--" Display="None"
									ControlToValidate="cboProcess" ErrorMessage="Please Select Participating Process !"></asp:CompareValidator></font></td>
						<td width="70%" colspan="2">
							<asp:DropDownList id="cboProcess" runat="server" Width="100%" cssclass="clsComboBox"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="clsLabel" width="30%">User ID<font class="clsNote">*
								<asp:CompareValidator id="cfvOwn" runat="server" Operator="NotEqual" ValueToCompare="--Select--" Display="None"
									ControlToValidate="cboOwner" ErrorMessage="Please Select The Process Owner !"></asp:CompareValidator></font></td>
						<td width="70%" colspan="2">
							<asp:DropDownList id="cboOwner" runat="server" Width="100%" cssclass="clsComboBox"></asp:DropDownList></td>
					</tr>
				</table>
				<br />
				<table id="table1" cellspacing="1" cellpadding="1" width="100%" border="1">
					<tr>
						<td align="center">
							<asp:button id="btnSubmit" runat="server" cssclass="clsButton" Text="Submit"></asp:button></td>
						<td align="center"><asp:button runat="server" class="clsButton" id="btnReset" type="reset" value="Reset" name="btnReset" /></td>
						<td align="center"><asp:button class="clsButton" id="btnBack" Visible="false" type="button" value="Back"
								name="btnBack" runat="server" /></td>
					</tr>
				</table>
				<br />
				<br />
				<asp:Label id="lblerr" runat="server" cssclass="clsMessage" Visible="False"></asp:Label>
				
			</asp:panel>
			
</asp:Content>

