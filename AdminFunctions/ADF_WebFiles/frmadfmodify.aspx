<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfmodify.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfmodify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:label id="Label1"  runat="server" Width="169px" cssclass="label" font-Size="Small">Employee Details</asp:label>
			<table id="table4"  cellspacing="1" cellpadding="1" width="300" border="1">
				<tr>
					<td class="label">&nbsp;Location</td>
					<td class="label">&nbsp;Building</td>
					<td class="label">Department</td>
				</tr>
				<tr>
					<td><asp:dropdownlist id="cmbLoc" runat="server" Width="120px" AutoPostBack="true"></asp:dropdownlist></td>
					<td><asp:dropdownlist id="cmbPremise" runat="server" Width="120px" AutoPostBack="true"></asp:dropdownlist></td>
					<td><asp:dropdownlist id="cmbCenter" runat="server" Width="120px" AutoPostBack="true"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td align="center" colspan="3">
						<asp:button id="btnView" runat="server" cssclass="button" Text="View" CausesValidation="False"></asp:button></td>
				</tr>
			</table>
			<asp:panel id="pnlEmpDtls" runat="server" Width="649px" Visible="False">
				<table id="table3" style="WIDTH: 656px; HEIGHT: 176px" bordercolor="maroon" cellspacing="1" cellpadding="1" width="656" border="1">
					<tr>
						<td class="LABEL" style="WIDTH: 150px">Employee ID</td>
						<td class="LABEL" style="WIDTH: 147px">
							<asp:TextBox id="txtid" runat="server" Width="168px" ReadOnly="true"></asp:TextBox></td>
						<td class="LABEL" style="WIDTH: 109px">E-mail ID</td>
						<td class="LABEL">
							<asp:TextBox id="txtemail" runat="server" Width="216px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="LABEL" style="WIDTH: 150px">First Name</td>
						<td class="LABEL" style="WIDTH: 147px">
							<asp:TextBox id="txtfname" runat="server" Width="168px"></asp:TextBox></td>
						<td class="LABEL" style="WIDTH: 109px">Extn No.</td>
						<td class="LABEL">
							<asp:TextBox id="txtextNo" runat="server" Width="216px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="LABEL" style="WIDTH: 150px">Middle Name</td>
						<td class="LABEL" style="WIDTH: 147px">
							<asp:TextBox id="txtmname" runat="server" Width="168px"></asp:TextBox></td>
						<td class="LABEL" style="WIDTH: 109px">Contact No.</td>
						<td class="LABEL">
							<asp:TextBox id="txtcontact" runat="server" Width="216px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="LABEL" style="WIDTH: 150px">Last Name</td>
						<td class="LABEL" style="WIDTH: 147px">
							<asp:TextBox id="txtlname" runat="server" Width="168px"></asp:TextBox></td>
						<td class="LABEL" style="WIDTH: 109px">Mobile No.</td>
						<td class="LABEL">
							<asp:TextBox id="txtMobile" runat="server" Width="216px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="LABEL" style="WIDTH: 150px">Known As</td>
						<td class="LABEL" style="WIDTH: 147px">
							<asp:TextBox id="txtknown" runat="server" Width="168px"></asp:TextBox></td>
						<td class="LABEL" style="WIDTH: 109px">Reporting To</td>
						<td class="LABEL">
							<asp:TextBox id="txtreport" runat="server" Width="216px"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="LABEL" style="WIDTH: 150px">Basic Salary</td>
						<td class="LABEL" style="WIDTH: 147px">
							<asp:TextBox id="txtbsal" runat="server" Width="168px"></asp:TextBox></td>
						<td class="LABEL" style="WIDTH: 109px">Employee Resigned</td>
						<td class="LABEL">
							<asp:DropDownList id="cmbstatus" runat="server" Width="140px">
								<asp:ListItem Value="Y">Yes</asp:ListItem>
								<asp:ListItem Value="N">No</asp:ListItem>
							</asp:DropDownList></td>
					</tr>
					<tr>
						<td class="LABEL" style="WIDTH: 150px">Building</td>
						<td class="LABEL" style="WIDTH: 147px">
							<asp:DropDownList id="cmbPrm" runat="server" Width="140px"></asp:DropDownList></td>
						<td class="LABEL" style="WIDTH: 109px">Department</td>
						<td class="LABEL">
							<asp:DropDownList id="cmbCCenter" runat="server" Width="140px"></asp:DropDownList></td>
					</tr>
					<tr>
					<td colspan="4" align="center">
					<asp:Button id="btnUpdate" runat="server" cssclass="button" CausesValidation="False" Text="Update"></asp:Button>
					</td>
					</tr>
				</table>
				
					
			</asp:panel><asp:panel id="pnllist" runat="server" Width="480px" Visible="False">
				<table id="table2" style="WIDTH: 488px; HEIGHT: 102px" borderColor="maroon" cellspacing="1" cellpadding="1" width="488" border="1">
					<tr>
						<td class="LABEL" style="WIDTH: 98px">Search by ID</td>
						<td class="LABEL">
							<asp:TextBox id="txtSearchId" runat="server"></asp:TextBox>
							<asp:Button id="btnSearch" runat="server" Width="25px" Text="Go"></asp:Button>
							<asp:RequiredFieldValidator id="rvsearchid" runat="server" ControlToValidate="txtSearchId" Display="None" ErrorMessage="plz Enter Numberic Value!">rfv</asp:RequiredFieldValidator>
							<asp:CompareValidator id="cvBookVal" runat="server" ControlToValidate="txtSearchId" Display="None" ErrorMessage="plz Enter Numberic Value!" Type="Double" Operator="DataTypeCheck">cv</asp:CompareValidator></td>
					</tr>
					<tr>
						<td class="LABEL" style="WIDTH: 98px">Select Employee</td>
						<td class="LABEL">
							<asp:listbox id="lstEMP" runat="server" Width="377px" AutoPostBack="true" Height="104px"></asp:listbox></td>
					</tr>
				</table>
			</asp:panel>
			<asp:DropDownList id="cboLocation"  runat="server" Width="140px" AutoPostBack="true" Visible="False"></asp:DropDownList>
			<asp:ValidationSummary id="ValidationSummary1"  runat="server" Width="227px" ShowMessageBox="true" ShowSummary="False"></asp:ValidationSummary>
	
</asp:Content>
