<%@ Page Language="VB" AutoEventWireup="true" CodeFile="frmADFSearch.aspx.vb" Inherits="AdminFunctions_frmADFSearch1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Users 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12 control-label" style="text-align: left;">
                                            <span style="color: red;">Note: </span>To search for users, enter the employee id.</label>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmNewUser.aspx">Add New User</asp:HyperLink>
                                <asp:LinkButton ID="lnkbtnsearch" runat="server" Visible="False" BorderStyle="None">Advanced Search</asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6 control-label">User ID</label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtEmployee" runat="server" CssClass="form-control" MaxLength="8" TabIndex="1"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row col-md-12">
                                    <asp:Button ID="btnSearch" AccessKey="s" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search"
                                        CommandName="entry" TabIndex="2"></asp:Button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblEmp" runat="server" Visible="False" class="col-md-12 control-label" Style="text-align: center;">
                                            No employee found. </asp:Label>
                                        <asp:Label ID="lblMsg" runat="server" Visible="False" class="col-md-12 control-label" Style="text-align: left;">
                                           <span style="color: red;">Note:</span> Click on Employee ID to proceed further</asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvEmp" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped"
                                    DataKeyNames="AUR_ID" AutoGenerateColumns="False" CellPadding="3"
                                    AllowSorting="True" AllowPaging="True">
                                    <Columns>
                                        <asp:HyperLinkField DataNavigateUrlFields="AUR_ID" DataNavigateUrlFormatString="frmADFempdtls.aspx?Eid={0}"
                                            DataTextField="Name" HeaderText="Employee Name" DataTextFormatString="{0:c}">
                                            <HeaderStyle Width="20%" CssClass="clstblHead" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:HyperLinkField>
                                        <asp:BoundField Visible="False" DataField="Name" HeaderText="Name">
                                            <HeaderStyle Width="20%" CssClass="clstblHead" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Roles" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:GridView ID="gvRoles" runat="server" GridLines="None" Height="100%" ShowHeader="False"
                                                    Width="100%">
                                                    <RowStyle HorizontalAlign="Left" />
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="AUR_EMAIL" HeaderText="Email">
                                            <HeaderStyle Width="20%" CssClass="clstblHead" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AUR_id" HeaderText="aurId">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:HyperLinkField DataNavigateUrlFields="AUR_ID" DataNavigateUrlFormatString="frmADFEmpedit.aspx?Uid={0}"
                                            Text="Edit" HeaderText="Edit Details" DataTextFormatString="{0:c}">
                                            <HeaderStyle Width="20%" CssClass="clstblHead" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:HyperLinkField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
