Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class frmADFprivilegessubmit
    Inherits System.Web.UI.Page

    Private Sub page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'strSQL = "delete FROM  " & Session("TENANT") & "."  & "pRIVILEGES WHERE pRV_ROL_ID=" & Request.Form("rid")
        Dim depid As SqlParameter = New SqlParameter("@depid", SqlDbType.Int)
        strSQL = "AMT_DelPrivileges_Sp"
        depid.Value = Request.Form("rid")
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, strSQL, depid)
        Dim arrprv, intI
        arrprv = Split(Request.Form("chkprv"), "_")
        For intI = LBound(arrprv) To UBound(arrprv)
            If (arrprv(intI) <> "") Then
                If arrprv(intI).ToString().Contains(",") Then
                    arrprv(intI) = arrprv(intI).ToString().Replace(",", "")
                End If
                Dim sp1 As New SqlParameter("@rol_id", SqlDbType.NVarChar, 50)
                sp1.Value = Request.Form("rid")
                Dim sp2 As New SqlParameter("@cls_id", SqlDbType.NVarChar, 50)
                sp2.Value = arrprv(intI)
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertRoles", sp1, sp2)
            End If
        Next
        Response.Redirect("frmADFprivileges.aspx")
    End Sub
End Class
