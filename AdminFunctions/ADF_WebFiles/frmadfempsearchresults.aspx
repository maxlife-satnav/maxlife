<%@ Page Language="VB"  MasterPageFile="~/MasterPage.master"  AutoEventWireup="false" CodeFile="frmadfempsearchresults.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfempsearchresults" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:label cssclass="clsHead" id="lblEmp" runat="server" Width="309px">Employee Search Results</asp:label>
				<br />
				<br />
				
				
				<br />
				<br />
				<table width="95%" style="BORDER-COLLAPSE: collapse" border="0" id="tblSearch1">
					<tr>
						<td class="label" colspan="4">
							<asp:label ID="lblStatus"  cssclass="clsLabel1" runat="server"  />
						</td>
					</tr>
				</table>
				<a name="this"></a>
				<asp:DataList ID="dlstSearch" Runat="server" Width="95%" EnableViewState="False">
					<HeaderTemplate>
						<table width="100%" style="BORDER-COLLAPSE: collapse;" cellpadding="0" cellspacing="0" border="1">
							<tr>
								<td class="clsTblHead">
								Emp Id</td>
								<td class="clsTblHead">
								Name</font></td>
								<td class="clsTblHead">
								Email</font></td>
								<td class="label">
								Department</td>
							</tr>
					</HeaderTemplate>
					<ItemTemplate>
					<tr>
						<td><a href="frmADFempdtls.aspx?Eid='<%#DataBinder.Eval(Container.DataItem, "AUR_ID") %>'">'<%#DataBinder.Eval(Container.DataItem, "AUR_ID")%>'</a>  </td>
						<td>'<%# DataBinder.Eval(Container.DataItem, "AUR_name") %>'</td>
						<td>'<%# DataBinder.Eval(Container.DataItem, "AUR_Email") %>'</td>
						<td>'<%# DataBinder.Eval(Container.DataItem, "DEP_NAME") %>'</td>
					</tr>					
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:DataList>
				<table width="95%" class="label" style="BORDER-COLLAPSE: collapse" border="0" >
					<tr>
						<td class="label">
							<a href="frmEmpSearchResults.aspx" id="lbtnFirst" onserverclick="ShowFirst" runat="server">
								First</a>
						</td>
						<td class="label">
							<a href="frmEmpSearchResults.aspx" id="lbtnPrevious" onserverclick="ShowPrevious" runat="server">
								Previous</a>
						</td>
						<td class="label">
							<a href="frmEmpSearchResults.aspx" id="lbtnNext" onserverclick="ShowNext" runat="server">
								Next</a>
						</td>
						<td class="label" align="right">
							<a href="frmEmpSearchResults.aspx" id="lbtnLast" onserverclick="ShowLast" runat="server">
								Last</a>
						</td>
					</tr>
				</table>
				<br />
				<table class="label">
					<tr>
						<td class="label" ><asp:label ID="lblHead" Visible="False" Runat="server" />
						</td>
					</tr>
					<tr>
						<td>
							<asp:gridview id="dgSearch" style="Z-INDEX: 101; LEFT: 87px" cellpadding="3" BackColor="#DEBA84" BorderWidth="1px" cellspacing="2" BorderStyle="None" BorderColor="#DEBA84" Width="399px" Height="108px" AutoGenerateColumns="False" runat="server">
								<SelectedRowStyle font-Bold="True" ForeColor="White" BackColor="#738A9C"></SelectedRowStyle>
								<RowStyle ForeColor="#8C4510" BackColor="#FFF7E7"></RowStyle>
								<HeaderStyle font-Bold="True" ForeColor="White" BackColor="BlanchedAlmond"></HeaderStyle>
								<FooterStyle ForeColor="#8C4510" BackColor="#F7DFB5"></FooterStyle>
								<Columns>
									<asp:BoundField DataField="AUR_ID" HeaderText="Emp ID"></asp:BoundField>
									<asp:BoundField DataField="AUR_NAME" HeaderText="Name"></asp:BoundField>
									<asp:BoundField DataField="AUR_EMAIL" HeaderText="E-Mail"></asp:BoundField>
									<asp:BoundField DataField="Department_Name" HeaderText="Department"></asp:BoundField>
									<asp:ButtonField Text="Delete"  CommandName="Select" />
								</Columns>
							</asp:gridview>
						</td>
					</tr>
				</table>
				<br />
				
					<asp:button id="btnSub" tabIndex="3" runat="server" Height="26px" Width="180px" Visible="false" Text="View User Role Scope Mappings"></asp:button>
				<!---<center><input type=Button name=btnsub onclick="submitme()" value="Submit" ></center>--->
				<table class="label">
					<tr>
						<td class="label" ><asp:label ID="lblNodata" Visible="False" Runat="server" />
						</td>
					</tr>
				</table>
				<asp:TextBox id="txtempid" visible="True" runat="server" Height="0px" AutoPostBack="True" Width="0px"></asp:TextBox>
				<asp:TextBox id="txtHidPartEmail" visible="True" runat="server" Height="0px" AutoPostBack="True" Width="0px"></asp:TextBox>
				<asp:TextBox id="txtHidPartId" visible="True" runat="server" Height="0px" AutoPostBack="True" Width="0px"></asp:TextBox>
				<asp:label ID="lblCurrIndex" Visible="False" Runat="server" />
				<asp:label ID="lblPageSize" Visible="False" Runat="server" />
				<asp:label ID="lblRecordCount" Visible="False" Runat="server" />
				<asp:label ID="lbldisp" Visible="False" Runat="server" />
	</asp:Content>