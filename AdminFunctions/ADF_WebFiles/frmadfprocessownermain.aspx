<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfprocessownermain.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfprocessownermain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	
								
<asp:Panel id="Panel1" runat="server" Width="95%" HorizontalAlign="Center" >
					<asp:label id="Label1" runat="server" cssclass="clshead">Admin Functions
					<hr width="70%" align="center" /></asp:label>
				</asp:Panel>
				<asp:Panel id="pnlOwnerMain" runat="server" Height="14px" Width="95%" HorizontalAlign="Right"
				cssclass="clsLink">	<a href="frmADFaddprocessowner.aspx">Add Process Owner</a>
				</asp:Panel>
				<asp:panel id="pnlOwner" runat="server" HorizontalAlign="Center" Width="95%" Height="249px">
					
					<div align="left">
						<asp:Label id="lblMnd" runat="server" cssclass="clsNote">(*) Mandatory Fields.</asp:Label></div>
					<table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1">
						<tr>
							<td class="clstblnew" width="50%" colspan="2">Process Owner</td>
						</tr>
						<tr>
							<td class="clslabel" width="50%">
								<asp:Label id="lblmsg" cssclass="clsLabel" Runat="server">Select a Participating Process</asp:Label><font class="clsNote">*</font></td>
							<td width="50%">
								<asp:DropDownList id="cboOwner" Width="100%" cssclass="clsComboBox" Runat="server" AutoPostBack="true"></asp:DropDownList></td>
						</tr>
					</table>
					<p>
						<asp:gridview id="grdProcess" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="true">
							<Columns>
								<asp:BoundField Visible="False" DataField="POW_ID"></asp:BoundField>
								<asp:BoundField DataField="PSY_TITLE" HeaderText="Participating Process">
									<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
								</asp:BoundField>
								<asp:BoundField DataField="POW_USR_ID" HeaderText="User ID">
									<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
								</asp:BoundField>
								<asp:ButtonField DataTextField="POW_ACTIVE" HeaderText="Active">
									<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonField>
							</Columns>
						</asp:gridview></p>
					
						<asp:Label id="lblMessage" runat="server" cssclass="clsMessage"></asp:Label><br />
					
				</asp:panel>
			
	</asp:Content>