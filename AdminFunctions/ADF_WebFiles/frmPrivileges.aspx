﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmPrivileges.aspx.cs" Inherits="AdminFunctions_ADF_WebFiles_frmPrivileges" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/menu/css/metismenu.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/menu/css/menu.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
   
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }


        .list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
            z-index: 2;
            color: #fff;
            background-color: white;
            border-color: #337ab7;
        }



    </style>
</head>
<body>
    <div id="page-wrapper" class="wrapper amantra" data-ng-controller="PrivilegesController">



        <div class="row form-wrapper">
            <div class="row">


                <fieldset>
                    <legend>Add Privileges </legend>
                </fieldset>

             <div class="form-horizontal well">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <h4>
                                        <label class="col-md-12 control-label" style="text-align: center;">
                                        Roles Selection For 
                                                    <label id="lblRoleName"></label>
                                        </label>
                                    </h4>
                                </div>
                              </div>
                        </div>
                    </div>

                    <div class="col-md-12">

                        <!-- sidebar: style can be found in sidebar.less -->
                        <ul id="privilegemenu" style="list-style: none;" class="list-group">
                            <li class="list-group-item" style="list-style: none;" data-ng-repeat="nodes in tree" data-ng-include="'node_template.html'"></li>
                        </ul>
                        <!-- /.sidebar -->

                    </div>


                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" id="btnSubmit" data-ng-click="SaveDetails()" class="btn btn-primary custom-button-color" value="Submit" />
                                <input type="submit" id="btnBack" class="btn btn-primary custom-button-color" value="Back" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>




        </div>


    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    
    <script src="../../BootStrapCSS/menu/scripts/metismenu.min.js"></script>

    <script type="text/javascript">
        $(function () {
            var name = GetParameterValues('Value');
            $('#lblRoleName').html(name);
        });
        $('#btnBack').click(function () {
            window.location.href = "../../AdminFunctions/Privileges.aspx";
        });
    </script>
      
    <script>

        var app = angular.module('QuickFMS', ["agGrid"]);
        app.service("PrivilegesService", function ($http, $q, UtilityService) {
            var roleid;
            var deferred = $q.defer();
            this.GetGriddata = function (roleid) {
                deferred = $q.defer();
                return $http.get(UtilityService.path + '/api/Privileges/GetRoledataObj/' + roleid)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            //to save
            this.SavePrivilegeDetails = function (dataobject) {
                deferred = $q.defer();
                return $http.post(UtilityService.path + '/api/Privileges/SavePrivileges', dataobject)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;

                  });
            };

        });

        app.controller('PrivilegesController', function ($scope, $q, PrivilegesService) {
            progress(0, 'Loading...', true);
            var roleid = GetParameterValues('_id');
            $scope.tree = [];
            $scope.LoadData = function () {
                PrivilegesService.GetGriddata(roleid).then(function (data) {
                    progress(0, 'Loading...', false);
                    if (data.data == null) {
                        showNotification('error', 8, 'bottom-right', data.Message);
                    }
                    else {
                        $scope.tree = data.data;
                        setTimeout(function () {
                            $('#privilegemenu').metisMenu();
                        }, 200);
                    }
                }, function (error) {
                    console.log(error);
                });
            }
            $scope.LoadData();
            $scope.SaveDetails = function () {
                $scope.dataobject = { Main: $scope.tree, ROL_ID: roleid };
                PrivilegesService.SavePrivilegeDetails($scope.dataobject).then(function (dataobj) {
                    if (dataobj.data != null) {
                        showNotification('success', 8, 'bottom-right', dataobj.Message);
                    }
                    else {
                        showNotification('error', 8, 'bottom-right', dataobj.Message);
                    }
                }, function (error) {
                });
            }

        });

    </script>
   <script src="../../SMViews/Utility.js"></script>
    <script type="text/ng-template" id="node_template.html">
        <input id="{{nodes.Value.CLS_ID}}" type="checkbox"  ng-model="nodes.Value.selected" /> 
        <a href="" title="">
         <span class="li-longText">{{nodes.Key}}</span>.
        </a>
        <ul class="list-group" ng-if="nodes.Value.data" style="list-style: none; ">
             <li class="list-group-item" data-ng-repeat="nodes in nodes.Value.data" data-ng-include="'node_template.html'"></li>
        </ul>
    </script>
</body>
</html>

