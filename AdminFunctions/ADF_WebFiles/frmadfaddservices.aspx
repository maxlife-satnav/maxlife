<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmadfaddservices.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfaddservices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="lblSer" runat="server" Width="95%" CssClass="clsHead">Admin Functions
<hr width="70%" align="center"/></asp:Label>
    <asp:Panel ID="Panel2" runat="server" Width="100%" Height="96px">
        <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="0">
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" bgcolor="#f5821f" align="left">
                    <strong>&nbsp;&nbsp;Add Services</strong></td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="center">
                    <asp:Panel ID="pnlService" runat="server" Width="95%" Height="297px" HorizontalAlign="Center">
                        <div align="left">
                            <asp:Label ID="lblMnd" runat="server" CssClass="clsNote">(*) Mandatory Fields.</asp:Label></div>
                        <table id="clsSer" width="100%" align="center">
                            <tr>
                                <td class="clstblnew" width="50%" colspan="2">
                                    Add Services</td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px; height: 21px" width="385">
                                    Participating&nbsp;Process<font class=""><font class="clsNote">*</font>
                                        <asp:CompareValidator ID="cfvAlpo" runat="server" Operator="NotEqual" ValueToCompare="--Select--"
                                            Display="None" ControlToValidate="cboProcess" ErrorMessage="Please Select The Participating Process !"></asp:CompareValidator></font></td>
                                <td style="height: 21px" width="70%">
                                    <asp:DropDownList ID="cboProcess" runat="server" CssClass="clsComboBox" Width="100%">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px" width="385">
                                    Parent Service<font class="clsNote">*</font>
                                    <asp:CompareValidator ID="cfvAlpser" runat="server" Operator="NotEqual" ValueToCompare="--Select--"
                                        Display="None" ControlToValidate="cboParent" ErrorMessage="Please Select The Parent Service !"></asp:CompareValidator></td>
                                <td width="50%">
                                    <asp:DropDownList ID="cboParent" runat="server" CssClass="clsComboBox" Width="100%">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px" width="385">
                                    Services Title<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvStitle" runat="server" Display="None" ControlToValidate="txtsertitle"
                                        ErrorMessage="Please Enter The Service Title !"></asp:RequiredFieldValidator></td>
                                <td width="50%">
                                    <asp:TextBox ID="txtSerTitle" runat="server" CssClass="clsTextField" Width="100%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px" width="385">
                                    Is Parent Service<font class="clsNote">*</font></td>
                                <td width="50%">
                                    <asp:CheckBox ID="chkParent" runat="server" CssClass="clsCheckBox" Width="146px"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px" width="385">
                                    tracking Page</td>
                                <td width="50%">
                                    <asp:TextBox ID="txtSerPage" runat="server" CssClass="clsTextField" Width="100%"
                                        ReadOnly="true">frmAMTchk.aspx</asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px" width="385">
                                    Service Page<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvTpage" runat="server" Display="None" ControlToValidate="txttrackpage"
                                        ErrorMessage="Please Enter The  Service Page !"></asp:RequiredFieldValidator></td>
                                <td width="50%">
                                    <asp:TextBox ID="txttrackPage" runat="server" CssClass="clsTextField" Width="100%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px" width="385">
                                </td>
                                <td width="50%">
                                </td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px; height: 22px" width="385">
                                    Description
                                </td>
                                <td style="height: 22px" width="50%">
                                    <asp:TextBox ID="txtdesc" runat="server" CssClass="clsTextField" Width="100%" Height="43px"
                                        TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="Label" style="width: 385px; height: 25px" width="385">
                                    Auto Generation of Request ID</td>
                                <td style="height: 25px" width="50%">
                                    <asp:CheckBox ID="chkReqId" runat="server" CssClass="clsCheckBox"></asp:CheckBox></td>
                            </tr>
                            <tr>
                                <td style="height: 24px" width="50%" colspan="2">
                                    &nbsp;</td>
                            </tr>
                            <tr bgcolor="#324324">
                                <td style="width: 385px" align="center" width="385">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="clsButton" Text="Submit"></asp:Button></td>
                                <td style="height: 25px" align="center" width="50%">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input class="clsButton" id="btnReset" type="reset" value="Reset" name="hreset" runat="server" /></td>
                            </tr>
                        </table>
                        <asp:Label ID="lblErr" runat="server"></asp:Label>
                        <asp:ValidationSummary ID="vsAddServices" CssClass="clsMessage" runat="server" ShowSummary="False"
                            ShowMessageBox="true"></asp:ValidationSummary>
                        <asp:Label ID="lblMessage" runat="server" CssClass="clsMessage" Visible="False">Successfully added</asp:Label>
                    </asp:Panel>
                </td>
                <td background="../../Images/table_right_mid_bg.gif">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <br />
  <%-- <asp:panel id="Panel1" 	runat="server" Width="95%" Height="297px" HorizontalAlign="Center" >
				<div align="left">
					<asp:Label id="Label1" runat="server" cssclass="clsNote">(*) Mandatory Fields.</asp:Label></div>
				<table id="table1" width="100%" align="center">
					<tr>
						<td class="clstblnew" width="50%" colspan="2" align="left">Add Services</td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px; HEIGHT: 21px" width="385">Participating&nbsp;Process<font class=""><font class="clsNote">*</font>
								<asp:CompareValidator id="CompareValidator1" runat="server" Operator="NotEqual" ValueToCompare="--Select--" Display="None"
									ControlToValidate="cboProcess" ErrorMessage="Please Select The Participating Process !"></asp:CompareValidator></font></td>
						<td style="HEIGHT: 21px" width="70%">
							<asp:DropDownList id="DropDownList1" runat="server" cssclass="clsComboBox" Width="100%"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px" width="385">Parent Service<font class="clsNote">*</font>
							<asp:CompareValidator id="CompareValidator2" runat="server" Operator="NotEqual" ValueToCompare="--Select--" Display="None"
								ControlToValidate="cboParent" ErrorMessage="Please Select The Parent Service !"></asp:CompareValidator></td>
						<td width="50%">
							<asp:DropDownList id="DropDownList2" runat="server" cssclass="clsComboBox" Width="100%"></asp:DropDownList></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px" width="385">Services Title<font class="clsNote">*</font>
							<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Display="None" ControlToValidate="txtsertitle" ErrorMessage="Please Enter The Service Title !"></asp:RequiredFieldValidator></td>
						<td width="50%">
							<asp:TextBox id="TextBox1" runat="server" cssclass="clsTextField" Width="100%"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px" width="385">Is Parent Service<font class="clsNote">*</font></td>
						<td width="50%">
							<asp:CheckBox id="CheckBox1" runat="server" cssclass="clsCheckBox" Width="146px"></asp:CheckBox></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px" width="385">tracking Page</td>
						<td width="50%">
							<asp:TextBox id="TextBox2" runat="server" cssclass="clsTextField" Width="100%" ReadOnly="true">frmAMTchk.aspx</asp:TextBox></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px" width="385">Service Page<font class="clsNote">*</font>
							<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" Display="None" ControlToValidate="txttrackpage" ErrorMessage="Please Enter The  Service Page !"></asp:RequiredFieldValidator></td>
						<td width="50%">
							<asp:TextBox id="TextBox3" runat="server" cssclass="clsTextField" Width="100%"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px" width="385"></td>
						<td width="50%"></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px; HEIGHT: 22px" width="385">Description
						</td>
						<td style="HEIGHT: 22px" width="50%">
							<asp:TextBox id="TextBox4" runat="server" cssclass="clsTextField" Width="100%" Height="43px" TextMode="MultiLine"></asp:TextBox></td>
					</tr>
					<tr>
						<td class="Label" style="WIDTH: 385px; HEIGHT: 25px" width="385">Auto Generation of 
							Request ID</td>
						<td style="HEIGHT: 25px" width="50%">
							<asp:CheckBox id="CheckBox2" runat="server" cssclass="clsCheckBox"></asp:CheckBox></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" width="50%" colSpan="2">&nbsp;</td>
					</tr>
					<tr bgColor="#324324">
						<td style="WIDTH: 385px" align="center" width="385">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="Button1" runat="server" cssclass="clsButton" Text="Submit"></asp:button></td>
						<td style="HEIGHT: 25px" align="center" width="50%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input class="clsButton" id="Reset1" type="reset" value="Reset" name="hreset" runat="server" /></td>
					</tr>
				</table>
				<asp:Label id="Label2" runat="server"></asp:Label>
				
					<asp:ValidationSummary id="ValidationSummary1" runat="server" ShowSummary="False" CssClass="clsMessage" ShowMessageBox="true"></asp:ValidationSummary>
					<asp:Label id="Label3" runat="server" cssclass="clsMessage" Visible="False">Successfully added</asp:Label>
			</asp:panel>--%>
</asp:Content>
