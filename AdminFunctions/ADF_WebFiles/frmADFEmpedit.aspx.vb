﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Collections.Generic
Imports System.Web.Services
Imports cls_OLEDB_postgresSQL

Partial Class AdminFunctions_ADF_WebFiles_frmADFEmpedit
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters
    Dim Objsubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            lblmsg.Text = String.Empty
            If Request.QueryString("UID") IsNot Nothing Then
                BindEmployeeAllocationdata()
                obj.BindDepartment(ddldept)
                obj.binddesig(ddldesn)
                obj.binduser(ddlrepmgr, Session("uid"))
                obj.BindCountry(ddlcountry)
                'obj.BindCostCenter(ddlcostcenter)
                obj.BindVertical(ddlVertical)
                obj.BindCity(ddlcity)
                obj.bindloc(ddlLocation)
                BindTimeZones()
                lblProcess.Text = "Select " + Session("Child")
                lblVertical.Text = "Select " + Session("Parent")
                'CompareValidator6.ErrorMessage = "Please select " + Session("Parent")
                'CompareValidator5.ErrorMessage = "Please select " + Session("Child")

                'Dim param(0) As SqlParameter
                'param(0) = New SqlParameter("@aur_id", SqlDbType.VarChar, 50)
                'param(0).Value = Session("UID")
                Objsubsonic.Binddropdown(ddlCostcenter, "GET_COSTCENTER", "Cost_Center_Name", "Cost_Center_Code")
                BindEmployeeData()
            End If
        End If
    End Sub
    Private Sub BindTimeZones()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TIME_ZONES_BYCTYID")
        sp.Command.AddParameter("@CNY_CODE", ddlcountry.SelectedItem.Value, DbType.String)
        ddlTimeZone.DataSource = sp.GetDataSet()
        ddlTimeZone.DataTextField = "TIME_ZONE_NAME"
        ddlTimeZone.DataValueField = "TIME_ZONE"
        ddlTimeZone.DataBind()
        ddlTimeZone.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Public Sub BindEmployeeAllocationdata()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.VarChar, 50)
        param(0).Value = LTrim(RTrim(Request.QueryString("UID")))

        Dim ds As New DataSet
        ds = Objsubsonic.GetSubSonicDataSet("GETEMPALLOCATIONDETAILS", param)
        gvempdetials.DataSource = ds
        gvempdetials.DataBind()

    End Sub
    Public Sub BindEmployeeData()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.VarChar, 50)
        param(0).Value = LTrim(RTrim(Request.QueryString("UID")))

        Dim dr As SqlDataReader
        dr = Objsubsonic.GetSubSonicDataReader("GETEMPDETAILS", param)
        If dr.Read() Then


            txtfname.Text = dr.Item("AUR_FIRST_NAME").ToString()
            txtmname.Text = dr.Item("AUR_MIDDLE_NAME").ToString()
            txtlname.Text = dr.Item("AUR_LAST_NAME").ToString()
            txtmailid.Text = dr.Item("AUR_EMAIL").ToString()
            txtmob.Text = dr.Item("AUR_RES_NUMBER").ToString()
            If rdbGender.Items.FindByValue(dr.Item("AUR_GENDER").ToString()) IsNot Nothing Then
                rdbGender.Items.FindByValue(dr.Item("AUR_GENDER").ToString()).Selected = True
            Else
            End If
            txtGrade.Text = dr.Item("AUR_GRADE").ToString()
            ddltitle.SelectedValue = PreventUnlistedValueError(ddltitle, dr.Item("AUR_TITLE").ToString())
            txtaurid.Text = dr.Item("AUR_ID").ToString()
            ddlVertical.SelectedValue = PreventUnlistedValueError(ddlVertical, dr.Item("AUR_VERT_CODE").ToString())
            ddlTimeZone.SelectedValue = PreventUnlistedValueError(ddlTimeZone, dr.Item("AUR_TIME_ZONE").ToString())
            ddlcountry.SelectedValue = PreventUnlistedValueError(ddlcountry, dr.Item("AUR_COUNTRY").ToString())
            ddlcity.SelectedValue = PreventUnlistedValueError(ddlcity, dr.Item("AUR_CITY").ToString())
            ddlLocation.SelectedValue = PreventUnlistedValueError(ddlLocation, dr.Item("AUR_LOCATION").ToString())
            ddldept.SelectedValue = PreventUnlistedValueError(ddldept, dr.Item("AUR_DEP_ID").ToString())
            ddldesn.SelectedValue = PreventUnlistedValueError(ddldesn, dr.Item("AUR_DESGN_ID").ToString())
            ddlrepmgr.SelectedValue = PreventUnlistedValueError(ddlrepmgr, dr.Item("AUR_REPORTING_TO").ToString())
            ddlStatus.SelectedValue = PreventUnlistedValueError(ddlStatus, dr.Item("AUR_INACTIVE").ToString())
            ddlCostcenter.SelectedValue = PreventUnlistedValueError(ddlCostcenter, dr.Item("AUR_PRJ_CODE").ToString())
            'txtDOJ.Text = dr.Item("AUR_DOJ").ToString()

        End If
    End Sub

    Public Sub BindCostcenters()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@BE_ID", SqlDbType.VarChar, 50)
        param(0).Value = ddlVertical.SelectedValue
        param(1) = New SqlParameter("@aur_id", SqlDbType.VarChar, 50)
        param(1).Value = Session("UID")
        Objsubsonic.Binddropdown(ddlCostcenter, "GET_COSTCENTER_BYBE", "Cost_Center_Name", "Cost_Center_Code", param)

    End Sub
    Protected Function PreventUnlistedValueError(li As DropDownList, val As String) As String
        If li.Items.FindByValue(val) Is Nothing Then
            'option 1: add the value to the list and display
            Dim arr(3) As String

            arr = val.Split("/")
            If arr.Length > 0 Then

                Dim lit As New ListItem()
                lit.Text = arr(0)
                lit.Value = val
                'option 2: set a default e.g.
                'val="";
                'val = "--Select--"
                li.Items.Insert(li.Items.Count, lit)
            End If

        End If
        Return val
    End Function

    Protected Sub gvempdetials_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvempdetials.RowCommand
        If e.CommandName = "SpaceRelease" Then
            Try

                Dim lblspc_ID As String = (e.CommandArgument.ToString())
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter("@VC_SPACEID", SqlDbType.NVarChar, 200)
                param(0).Value = lblspc_ID
                param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
                param(1).Value = LTrim(RTrim(Request.QueryString("UID")))
                Objsubsonic.GetSubSonicExecute("VACANTSEAT_BYEMPID", param)

                UpdateRecord(lblspc_ID, 1, lblspc_ID)
                BindEmployeeAllocationdata()
                'Response.Redirect("AllocateSeatToEmployeeVertical.aspx?id=" & Request.QueryString("id"))

                lblmsg.Text = "Successfully Released !"
                lblmsg.Visible = True
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ddlStatus.SelectedValue = "N" Then
            updatedetails()
        ElseIf ddlStatus.SelectedValue = "Y" Then
            updatedetails()
            Dim lblspc_ID As String = String.Empty
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
            param(0).Value = LTrim(RTrim(Request.QueryString("UID")))
            lblspc_ID = Objsubsonic.GetSubSonicExecuteScalar("VACANTSEAT_BYEMPIDExisting", param)
            UpdateRecord(lblspc_ID, 1, lblspc_ID)
            BindEmployeeAllocationdata()
        End If
    End Sub
    Public Sub updatedetails()
        Dim ddltitle As String = String.Empty
        If rdbGender.SelectedValue = "M" Then
            ddltitle = "Mr."
        Else
            ddltitle = "Ms."
        End If

        Dim knownas As String
        knownas = txtfname.Text & " " & txtlname.Text
        Dim param(19) As SqlParameter

        param(0) = New SqlParameter("@AUR_GENDER", SqlDbType.NVarChar, 50)
        param(0).Value = rdbGender.SelectedValue
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)
        param(1).Value = txtaurid.Text
        param(2) = New SqlParameter("@AUR_TITLE", SqlDbType.NVarChar, 50)
        param(2).Value = ddltitle
        param(3) = New SqlParameter("@AUR_FIRST_NAME", SqlDbType.NVarChar, 50)
        param(3).Value = txtfname.Text
        param(4) = New SqlParameter("@AUR_MIDDLE_NAME", SqlDbType.NVarChar, 50)
        param(4).Value = txtlname.Text
        param(5) = New SqlParameter("@AUR_KNOWN_AS", SqlDbType.NVarChar, 150)
        param(5).Value = knownas
        param(6) = New SqlParameter("@AUR_EMAIL", SqlDbType.NVarChar, 250)
        param(6).Value = txtmailid.Text
        param(7) = New SqlParameter("@AUR_COUNTRY", SqlDbType.NVarChar, 50)
        param(7).Value = ddlcountry.SelectedValue
        param(8) = New SqlParameter("@AUR_CITY", SqlDbType.NVarChar, 50)
        param(8).Value = ddlcity.SelectedValue
        param(9) = New SqlParameter("@AUR_BDG_ID", SqlDbType.NVarChar, 50)
        param(9).Value = ddlLocation.SelectedValue
        param(10) = New SqlParameter("@AUR_DEP_ID", SqlDbType.NVarChar, 50)
        param(10).Value = ddldept.SelectedValue
        param(11) = New SqlParameter("@AUR_DESGN_ID", SqlDbType.NVarChar, 50)
        param(11).Value = ddldesn.SelectedValue
        param(12) = New SqlParameter("@AUR_REPORTING_TO", SqlDbType.NVarChar, 50)
        param(12).Value = ddlrepmgr.SelectedValue
        param(13) = New SqlParameter("@AUR_RES_NUMBER", SqlDbType.NVarChar, 50)
        param(13).Value = txtmob.Text
        param(14) = New SqlParameter("@USERID", SqlDbType.NVarChar, 50)
        param(14).Value = Session("UID")
        param(15) = New SqlParameter("@AUR_INACTIVE", SqlDbType.NVarChar, 50)
        param(15).Value = ddlStatus.SelectedValue
        param(16) = New SqlParameter("@AUR_VERT_CODE", SqlDbType.NVarChar, 50)
        param(16).Value = ddlVertical.SelectedValue
        param(17) = New SqlParameter("@AUR_PROJ_CODE", SqlDbType.NVarChar, 50)
        param(17).Value = ddlCostcenter.SelectedValue
        param(18) = New SqlParameter("@AUR_TIME_ZONE", SqlDbType.NVarChar, 50)
        param(18).Value = ddlTimeZone.SelectedValue
        'param(19) = New SqlParameter("@AUR_DOJ", SqlDbType.DateTime)
        'param(19).Value = CDate(txtDOJ.Text)
        param(19) = New SqlParameter("@AUR_GRADE", SqlDbType.DateTime)
        If txtGrade.Text = "" Then
            param(19).Value = ""
        Else
            param(19).Value = txtGrade.Text
        End If

        Dim flag As Integer = Objsubsonic.GetSubSonicExecuteScalar("USP_UPDATE_USER_DETAILS", param)
        If flag = 1 Then
            lblmsg.Text = "Successfully Updated !"
            cleardata()
        Else
            lblmsg.Text = "Error occured ! Please try after some time."
        End If
    End Sub
    Public Sub cleardata()
        txtfname.Text = String.Empty
        txtlname.Text = String.Empty
        txtmailid.Text = String.Empty
        txtmob.Text = String.Empty
        txttoday.Text = String.Empty
        ddlVertical.ClearSelection()
        ddlcity.ClearSelection()
        ddlCostcenter.ClearSelection()
        ddlcountry.ClearSelection()
        ddldept.ClearSelection()
        ddldesn.ClearSelection()
        ddlLocation.ClearSelection()
        ddlrepmgr.ClearSelection()
        ddlStatus.ClearSelection()
        ddlTimeZone.ClearSelection()
        txtaurid.Text = String.Empty
        rdbGender.ClearSelection()
        'txtDOJ.Text = String.Empty
        ddltitle.ClearSelection()
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVertical.SelectedIndexChanged
        If ddlVertical.SelectedIndex <> 0 Then
            BindCostcenters()
        End If
    End Sub

End Class

