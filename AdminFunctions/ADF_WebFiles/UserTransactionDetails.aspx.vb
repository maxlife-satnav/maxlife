﻿
Imports System
Imports System.Data
Imports Microsoft.Reporting.WebForms

Partial Class AdminFunctions_ADF_WebFiles_UserTransactionDetails
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure("dbo." & "GET_SCHEMA_DETAILS")
            ddlclient.DataSource = sp.GetDataSet()
            ddlclient.DataValueField = "code"
            ddlclient.DataTextField = "name"
            ddlclient.DataBind()
            ddlclient.Items.Insert(0, "--Select--")
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure("dbo." & "GET_USER_TRANSACTION_DETAILS_SCHEMA_WISE")
        sp.Command.AddParameter("@SCHEMA", ddlclient.SelectedValue, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
    
        Dim rds As New ReportDataSource()
        rds.Name = "GET_USER_TRANSACTION_DETAILS_SCHEMA_WISE_DS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/AdminFunctions/UserTransactionDetails.rdlc")
        'ReportViewer1.LocalReport.EnableHyperlinks = True

    End Sub
End Class
