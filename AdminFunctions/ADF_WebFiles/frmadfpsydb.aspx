<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfpsydb.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfpsydb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

			<asp:panel id="Panel1" runat="server"	HorizontalAlign="Center" Width="95%">
<asp:label id="lblHead" runat="server" cssclass="clsHead">Admin Functions
<hr width="70%" align="center"/></asp:label>
			</asp:panel><asp:panel id="pnlProcess2"	runat="server" HorizontalAlign="Right" Width="95%" Height="14px"><a class="clsHypLink" href="frmADFaddpsy.aspx?src=Add">Add 
					New Participating Process</a>
			</asp:panel><asp:panel id="pnlProcess1" runat="server" HorizontalAlign="Center" Width="95%" Height="150px">
				<table width="100%">
					<tr>
						<td class="clstblnew" width="50%" colspan="2">Process</td>
					</tr>
				</table>
				<asp:gridview id="grdProcess" runat="server" Width="100%" AllowPaging="true" AutoGenerateColumns="False">
					<Columns>
						<asp:BoundField Visible="False" DataField="PSY_ID"></asp:BoundField>
						<asp:HyperLinkField DataNavigateUrlFields="PSY_ID" DataNavigateUrlformatString="frmADFaddpsy.aspx?sid={0}"
							DataTextField="PSY_TITLE" HeaderText="Participating Process" DataTextformatString="{0:c}">
							<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
						</asp:HyperLinkField>
						<asp:BoundField DataField="PSY_DESCRIPTION" HeaderText="Description">
							<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
						</asp:BoundField>
						<asp:ButtonField DataTextField="PSY_ACTIVE" HeaderText="Active">
							<HeaderStyle cssclass="clsTblHead"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:ButtonField>
					</Columns>
				</asp:gridview>
				<br />
				<div><br />
					<asp:Label id="lblProcess" cssclass="clsMessage" Runat="server" Visible="False"></asp:Label></div>
				<br />
			</asp:panel>
			<div>&nbsp;</div>
	</asp:Content>	