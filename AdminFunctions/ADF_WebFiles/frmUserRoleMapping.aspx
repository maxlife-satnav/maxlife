﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Messenger/messenger.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/ag-grid.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />


    <%--    <link rel="stylesheet"
        href="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>--%>

    <style>
        .grid-align {
            /*text-align: center;*/
        }

        a:hover {
            cursor: pointer;
        }

        #user {
            color: #FFFFFF;
        }

        #excel {
            color: #2AE214;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .grid-align {
            /*text-align: center;*/
        }

        a:hover {
            cursor: pointer;
        }

        #user {
            color: #FFFFFF;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }


        /*form wizard start*/
        body {
            margin-top: 02px;
        }

        .stepwizard-step p {
            margin-top: 10px;
        }

        .stepwizard-row {
            display: table-row;
        }

        .stepwizard {
            display: table;
            width: 100%;
            position: relative;
        }

        .stepwizard-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;
        }

        .stepwizard-step {
            display: table-cell;
            /*text-align: center;*/
            position: relative;
        }

        .btn-circle {
            width: 120px;
            height: 30px;
            text-align: center;
            /*padding: 6px 0;*/
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
        /*form wizard end*/
    </style>
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body data-ng-controller="UserRoleMappingController as ctrl" class="amantra" ng-cloak>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>User Role Mapping</legend>
                        <div class="clearfix" data-ng-show="Viewstatus==1">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field &nbsp &nbsp    <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                    </fieldset>

                    <form role="form" id="FileUsrUpl" data-ng-show="ViewUploption" name="FileUsrUpl" class="form-horizontal well" data-valid-submit="UploadFile()" novalidate>
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <a href="../../Masters/Mas_Webfiles/User_Role_Mapping_Template.xlsx">Click here to download template</a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Upload File </label>
                                        <input type="file" name="UPLFILE" id="FileUpl" required="" />
                                    </div>
                                </div>
                                <div class="box-footer text-right">
                                    <input type="submit" id="btnUpload" class="btn btn-primary custom-button-color" value="Upload Excel" />
                                </div>
                            </div>
                        </div>
                    </form>

                    <form role="form" id="frmUserRole" name="frmUserRole" class="form-horizontal well" data-valid-submit="SaveDetails()" novalidate>
                        <div class="row">
                            <div class="col-md-12 text-right" data-ng-show="Viewstatus==0">
                                <a data-ng-click="GenReport(CurrentProfile,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmNewUser.aspx" onmouseover="Tip('Add New User')" onmouseout="UnTip()"> <img id="user" src="../../images/glyphicons-7-user-add.png" /></asp:HyperLink>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group"></div>
                            </div>
                        </div>

                        <div style="height: 530px" data-ng-show="Viewstatus==0">
                            <input id="filtertxt" placeholder="Filter by any..." type="text" style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                        </div>

                        <div style="height: 530px" data-ng-show="Viewstatus==2">
                            <input id="Text1" placeholder="Filter by any..." type="text" style="width: 25%" />
                            <div data-ag-grid="gridOptions1" style="height: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                    <div data-ng-show="Viewstatus==1" class="container-fluid">
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">&nbsp; &nbsp;  <span style="color: red;">**</span> Select to auto fill the data
                                </div>
                            </div>
                            <div class="container" style="padding-top: 10px;">
                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step">
                                            <a href="#step-1" id="stp1" type="button" class="btn btn-primary btn-circle">Employee Details</a>
                                        </div>
                                        <div class="stepwizard-step">
                                            <a href="#step-2" id="stp2" type="button" ng-click='LoadRMdetails_Step2()' class="btn btn-default btn-circle">Role Assignment</a>
                                        </div>
                                        <div class="stepwizard-step">
                                            <a href="#step-3" id="stp3" type="button" ng-click="LoadMappingDtls_Step3()" class="btn btn-default btn-circle">Mapping</a>
                                        </div>
                                    </div>
                                </div>
                                <form role="form" id="EmpDetails" name="frmUserRole" novalidate>
                                    <div class="row setup-content" id="step-1">
                                        <br />
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>User ID</b><span style="color: red;">*</span></label>
                                                    <input id="UserId" type="text" class="form-control" data-ng-model="CurrentProfile.AUR_ID" name="AUR_ID" required />
                                                    <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.AUR_ID.$invalid" style="color: red">Please enter Employee ID</span>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Name</b> </label>
                                                    <br />
                                                    <input type="text" id="Username" class="form-control" data-ng-model="CurrentProfile.AUR_KNOWN_AS" />
                                                </div>
                                            </div>


                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Email</b></label>
                                                    <div data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.AUR_EMAIL.$invalid}">
                                                        <input type="text" class="form-control" data-ng-model="CurrentProfile.AUR_EMAIL" data-output-model="CurrentProfile.AUR_EMAIL" name="AUR_EMAIL" ng-pattern="/^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z-.]+\.[a-zA-Z.]{2,5}$/" />
                                                        <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.AUR_EMAIL.$invalid" style="color: red">Please enter valid Email Id</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Phone No</b> </label>
                                                    <div data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.AUR_RES_NUMBER.$invalid}">
                                                        <input type="text" class="form-control" data-ng-model="CurrentProfile.AUR_RES_NUMBER" data-output-model="CurrentProfile.AUR_RES_NUMBER" maxlength="11" minlength="10" name="AUR_RES_NUMBER" ng-pattern="/^[0-9]+$/" />
                                                        <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.AUR_RES_NUMBER.$invalid" style="color: red">Please Enter 10 digit Mobile Number</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Date Of Birth</b></label>
                                                    <div class="input-group date" id='frmdate'>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('frmdate')"></span>
                                                        </span>
                                                        <%-- <div>
                                                                <input type="text" class="form-control" id="startDate" ng-model="frmdate" />
                                                                   </div>--%>
                                                        <input class="form-control" id="txtreqfrmdate" data-ng-model="CurrentProfile.AUR_DOB" data-output-model="CurrentProfile.AUR_DOB" name="AUR_DOB" type="text" />
                                                        <%--  <asp:CompareValidator ID="cmpcalender" runat="server" Type="Date" ControlToCompare="txtdoj" ControlToValidate="txtreqfrmdate" Operator="GreaterThan" ErrorMessage="Date Of Birth must be greater than Date of Joining"></asp:CompareValidator>--%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Date Of Joining</b></label>
                                                    <div class="input-group date" id='doj'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('doj')"></i>
                                                        </div>
                                                        <input class="form-control" id="txtdoj" data-ng-model="CurrentProfile.AUR_DOJ" data-output-model="CurrentProfile.AUR_DOJ" name="AUR_DOJ" type="text" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.AUR_PASSWORD.$invalid}">
                                                    <label><b>Password</b></label>
                                                    <input type="text" class="form-control" data-ng-model="CurrentProfile.AUR_PASSWORD" data-output-model="CurrentProfile.AUR_PASSWORD" name="AUR_PASSWORD" id="AUR_PASSWORD" />
                                                    <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.AUR_PASSWORD.$invalid" style="color: red">Please enter Password</span>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <label>Reporting To</label>
                                                </div>
                                                <div angucomplete-alt
                                                    id="ex7"
                                                    placeholder="Search Employee"
                                                    pause="500"
                                                    selected-object="selectedEmp"
                                                    remote-url="../../../api/Utility/GetEmployeeNameAndID"
                                                    remote-url-request-formatter="remoteUrlRequestFn"
                                                    remote-url-data-field="items"
                                                    title-field="NAME"
                                                    minlength="2"
                                                    input-class="form-control"
                                                    match-class="highlight"
                                                    required>
                                                </div>
                                                <input type="text" id="txtrptTo" data-ng-model="CurrentProfile.AUR_REPORTING_TO" data-output-model="CurrentProfile.AUR_REPORTING_TO" style="display: none" name="AUR_REPORTING_TO" />
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Extension No</b></label>
                                                    <input type="text" class="form-control" data-ng-model="CurrentProfile.AUR_EXTENSION" data-output-model="CurrentProfile.AUR_EXTENSION" />
                                                    <%--<asp:RequiredFieldValidator ID="rfvext" runat="server" ControlToValidate="text" ErrorMessage="Please Enter Extension No" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txtDesg"><b>Designation</b> </label>
                                                    <div isteven-multi-select data-input-model="designationddl" data-output-model="CurrentProfile.DSG_CODE" data-button-label="icon DSG_NAME" data-item-label="icon DSG_NAME maker"
                                                        data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                    </div>
                                                    <input type="text" data-ng-model="CurrentProfile.DSG_CODE" name="DSG_NAME" style="display: none" />
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txtBu"><b>Business Unit</b></label>
                                                    <div isteven-multi-select data-input-model="Business" data-output-model="CurrentProfile.VER_CODE" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                                        data-on-item-click="BuChange()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                    </div>
                                                    <input type="text" data-ng-model="CurrentProfile.VER_CODE" name="VER_NAME" style="display: none" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txtFN"><b>Function</b></label>
                                                    <div isteven-multi-select data-input-model="Functions" data-output-model="CurrentProfile.Cost_Center_Code" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                                        data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                    </div>
                                                    <input type="text" data-ng-model="CurrentProfile.Cost_Center_Code" name="Cost_Center_Name" style="display: none" />
                                                </div>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Status</b></label><br />
                                                    <select data-ng-model="CurrentProfile.AUR_STA_ID" data-output-model="CurrentProfile.AUR_STA_ID" class="selectpicker" id="ddlusrStatus" data-ng-change="Alert()"
                                                        name="AUR_STA_ID" cssclass="form-control selectpicker" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="sts in usrStatus" value="{{sts.value}}">{{sts.Name}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Remarks</b> </label>
                                                    <br />
                                                    <textarea rows="1" cols="10" id="UserRemarks" class="form-control" data-ng-model="CurrentProfile.AUR_EMP_REMARKS" data-output-model="CurrentProfile.AUR_EMP_REMARKS"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label><b>Address</b> </label>
                                                    <br />
                                                    <textarea rows="1" cols="10" id="UserAddress" class="form-control" data-ng-model="CurrentProfile.AUR_ADDRESS" data-output-model="CurrentProfile.AUR_ADDRESS"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.LCM_NAME.$invalid}">
                                                    <label for="txtCty"><b>Location</b> <span style="color: red;">**</span></label>
                                                    <div isteven-multi-select data-input-model="Location" data-output-model="CurrentProfile.LCM_CODE" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                        data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                    </div>
                                                    <input type="text" data-ng-model="CurrentProfile.LCM_CODE" name="LCM_NAME" style="display: none" />
                                                    <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                        <input type="submit" class="btn btn-primary nextBtn pull-right" value="Save & Continue" data-ng-click="SaveEmpDetails()" />
                                    </div>
                                </form>
                            </div>

                            <form role="form" id="frmMRdetails" name="frmMREmp" data-valid-submit="SaveRMDetls()" novalidate>
                                <div class="row setup-content" id="step-2">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label><b>Module</b> <span style="color: red;">*</span></label>
                                            <select id="module" data-ng-model="Currentmodule.MOD_NAME" data-ng-change="accessbymodule(Currentmodule.MOD_NAME)"
                                                class="form-control" name="MOD_NAME" data-live-search="true">
                                                <option value="ALL" selected>ALL</option>
                                                <option data-ng-repeat="mod in modules" value="{{mod.MOD_NAME}}">{{mod.MOD_NAME}}</option>
                                            </select>
                                        </div>
                                        <br />
                                        <br />
                                    </div>
                                    <br />
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12" data-ng-disabled="!RolStatus">
                                            <div class="form-group">
                                                <label><b>Access Level</b> <span style="color: red;">*</span></label>
                                                <div data-ng-class="{'has-error': frmMREmp.$submitted && frmMREmp.AUR_RES_NUMBER.$invalid}">
                                                    <div data-ng-repeat="obj in acceslevel">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <label>
                                                                <input type="checkbox" data-ng-model="obj.isChecked" ng-value="{{obj.isChecked}}" ng-change="Rolechk()" />
                                                                {{obj.ROL_DESCRIPTION}}                                                                                 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--{{(acceslevel | filter: {isChecked :  true}).length}}--%>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="row" data-ng-show="SpaceVisible == true">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Map Level Access</b></label>
                                                <br />
                                                <input type="radio" name="userdetails" value="Y" ng-model="Currentmodule.MapAccess" />&nbsp View & Edit &nbsp&nbsp&nbsp&nbsp
                                                        <input type="radio" name="userdetails" value="N" ng-model="Currentmodule.MapAccess" />&nbsp Only View
                                                        <br />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <span class="error" data-ng-show="RolValidation == false" style="color: red">Please Select Atleast One Role to Assign</span>
                                    <%--<input type="submit" class="btn btn-primary nextBtn btn-lg pull-right" data-ng-disabled="RolValidation == 'true'"  value="Save & Continue" />--%>
                                    <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                    <button type="submit" id="btnsave" class="btn btn-primary nextBtn pull-right" data-ng-disabled="RolValidation == false" value="Save & Continue">Save & Continue</button>
                                </div>
                            </form>
                            <form role="form" id="frmMappingDetails" name="frmUserRole" data-valid-submit="SaveMappingDetls()" novalidate>
                                <div class="row setup-content" id="step-3">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.CNY_NAME.$invalid}">
                                                <label for="txtcode">Country <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Countrylst" data-output-model="UserRole.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                                    data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.ZN_NAME.$invalid}">
                                                <label for="txtcode">Zone <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Zonelst" data-output-model="UserRole.selectedZone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME maker"
                                                    data-on-item-click="ZonChanged()" data-on-select-all="ZonChangeAll()" data-on-select-none="ZonSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedZone" name="ZN_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.ZN_NAME.$invalid" style="color: red">Please select Zone </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.STE_NAME.$invalid}">
                                                <label for="txtcode">State <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Statelst" data-output-model="UserRole.selectedState" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME maker"
                                                    data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedState" name="STE_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.STE_NAME.$invalid" style="color: red">Please select State </span>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.CTY_NAME.$invalid}">
                                                <label for="txtcode">City <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Citylst" data-output-model="UserRole.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                                    data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.CTY_NAME.$invalid " style="color: red">Please select city </span>
                                            </div>
                                        </div>
                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.LCM_NAME.$invalid}">
                                                <label for="txtcode">Location <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Locationlst" data-output-model="UserRole.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                    data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.TWR_NAME.$invalid}">
                                                <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Towerlist" data-output-model="UserRole.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                                    data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.FLR_NAME.$invalid}">
                                                <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Floorlist" data-output-model="UserRole.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                                    data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.PE_NAME.$invalid}">
                                                <label for="txtcode">Parent Entity  <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Parentlist" data-output-model="UserRole.selectedParentEntity" data-button-label="icon PE_NAME" data-item-label="icon PE_NAME maker"
                                                    data-on-item-click="ParentEntityChange()" data-on-select-all="ParentEntityChangeAll()" data-on-select-none="ParentEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedParentEntity" name="PE_NAME" style="display: none" ng-required="Currentmodule.PCENTITY" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.PE_NAME.$invalid" style="color: red">Please select parent entity </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.CHE_NAME.$invalid}">
                                                <label for="txtcode">Child Entity  <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Childlist" data-output-model="UserRole.selectedChildEntity" data-button-label="icon CHE_NAME" data-item-label="icon CHE_NAME maker"
                                                    data-on-item-click="ChildEntityChange()" data-on-select-all="ChildEntityChangeAll()" data-on-select-none="ChildEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedChildEntity" name="CHE_NAME" style="display: none" ng-required="Currentmodule.PCENTITY" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.CHE_NAME.$invalid" style="color: red">Please select child entity </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                          <div id="showhide" data-ng-show="Currentmodule.VERT_COST">
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.VER_NAME.$invalid}">
                                                    <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                                    <div isteven-multi-select data-input-model="Verticallist" data-output-model="UserRole.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                                        data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UserRole.selectedVerticals" name="VER_NAME" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                    <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.VER_NAME.$invalid" style="color: red">Please select  {{BsmDet.Parent |lowercase}}  </span>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.Cost_Center_Name.$invalid}">
                                                    <label for="txtcode">{{BsmDet.Child}}  <span style="color: red;">**</span></label>
                                                    <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="UserRole.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                                        data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UserRole.selectedCostcenter" name="Cost_Center_Name" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                    <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.Cost_Center_Name.$invalid" style="color: red">Please select {{BsmDet.Child |lowercase}} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                    <input type="submit" class="btn btn-primary pull-right" value="Finish" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <%--  --%>
                <%--  --%>
                <%--  --%>
                <%--<div data-ng-show="Viewstatus==1">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <div class="clearfix">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-6"><b>Employee ID</b></label>
                                            <input id="UserId" class="col-md-5" type="text" data-ng-model="CurrentProfile.AUR_ID" />
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Name</b> </label>
                                            <div>
                                                <div class="form-group">
                                                    <input id="Username" class="col-md-6" type="text" data-ng-model="CurrentProfile.AUR_KNOWN_AS" name="AUR_KNOWN_AS" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Email</b> </label>
                                            <div data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.AUR_RES_NUMBER.$invalid}">
                                                <div class="form-group">
                                                    <input class="col-md-6" type="text" required data-ng-model="CurrentProfile.AUR_EMAIL" name="AUR_EMAIL" ng-pattern="/^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z-.]+\.[a-zA-Z.]{2,5}$/" />
                                                    <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.AUR_EMAIL.$invalid" style="color: red">Please enter valid Email Id</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.AUR_RES_NUMBER.$invalid}">
                                                <label class="col-md-6"><b>Phone No</b> </label>
                                                <input class="col-md-5" type="text" data-ng-model="CurrentProfile.AUR_RES_NUMBER" name="AUR_RES_NUMBER" ng-pattern="/^[0-9]+$/" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.AUR_RES_NUMBER.$invalid" style="color: red">Please enter numerics of 10 digits</span>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Date Of Birth</b></label>
                                            <div class="form-group">
                                                <div class="input-group date" id='frmdate'>
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('frmdate')"></i>
                                                    </div>
                                                    <input class="form-control input-sm col-md-5" id="txtreqfrmdate" data-ng-model="CurrentProfile.AUR_DOB" name="AUR_DOB" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Date Of Join</b></label>
                                            <div class="input-group date col-md-7" id='doj'>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('doj')"></i>
                                                </div>
                                                <input class="form-control input-sm" id="txtdoj" data-ng-model="CurrentProfile.AUR_DOJ" name="AUR_DOJ" type="text" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-6"><b>Password</b></label>                                           
                                             <div data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.AUR_PASSWORD.$invalid}">
                                                <div class="form-group">
                                                    <input  id="password"  class="col-md-5" type="text" required data-ng-model="CurrentProfile.AUR_PASSWORD" name="AUR_PASSWORD"  />
                                                    <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.AUR_PASSWORD.$invalid" style="color: red">Please enter password</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Reporting To</b></label>
                                            <div class="form-group">
                                                <select class="col-md-6" data-ng-model="CurrentProfile.AUR_REPORTING_TO" id="ddlrepTo"
                                                    name="AUR_REPORTING_TO" data-live-search="true">
                                                    <option value="">--Select--</option>
                                                    <option data-ng-repeat="usr in allUsers" value="{{usr.AUR_KNOWN_AS}}">{{usr.AUR_KNOWN_AS}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Extension No</b></label>
                                            <input class="col-md-6" type="text" data-ng-model="CurrentProfile.AUR_EXTENSION" />
                                        </div>
                                    </div>

                                    <div class="clearfix">

                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-6"><b>Status</b></label>
                                            <select class="col-md-5" data-ng-model="CurrentProfile.AUR_STA_ID" id="ddlusrStatus" data-ng-change="Alert()"
                                                name="AUR_STA_ID" data-live-search="true">
                                                <option value="">--Select--</option>
                                                <option data-ng-repeat="sts in usrStatus" value="{{sts.value}}">{{sts.Name}}</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Business Unit</b></label>
                                            <div class="form-group">
                                                <select class="col-md-6" data-ng-model="CurrentProfile.VER_CODE" id="empver"
                                                    name="VER_NAME" data-live-search="true" data-ng-change="BuChange()">
                                                    <option value="">--Select--</option>
                                                    <option data-ng-repeat="bu in Business" value="{{bu.VER_CODE}}">{{bu.VER_NAME}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="col-md-5"><b>Function</b></label>
                                            <div class="form-group">
                                                <select class="col-md-6" data-ng-model="CurrentProfile.Cost_Center_Code" id="empcostcenter"
                                                    name="Cost_Center_Name" data-live-search="true">
                                                    <option value="">--Select--</option>
                                                    <option data-ng-repeat="Fnc in Functions" value="{{Fnc.Cost_Center_Code}}">{{Fnc.Cost_Center_Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="box">
                                <div class="clearfix">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <label><b>Module</b> <span style="color: red;">*</span></label>
                                        <select id="module" data-ng-model="Currentmodule.MOD_NAME" data-ng-change="accessbymodule(Currentmodule.MOD_NAME)"
                                            class="form-control" name="MOD_NAME" data-live-search="true">
                                            <option value="ALL" selected>ALL</option>
                                            <option data-ng-repeat="mod in modules" value="{{mod.MOD_NAME}}">{{mod.MOD_NAME}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12" data-ng-disabled="!RolStatus">
                                        <label><b>Access Level</b> <span style="color: red;">*</span></label>
                                        <div class="list-group checked-list-box" style="max-height: 130px; overflow: auto;">
                                            <div class="panel panel-default">
                                                <div data-ng-repeat="obj in acceslevel">
                                                    <label class="list-group-item">
                                                        <input type="checkbox" data-ng-model="obj.isChecked" value="{{obj.isChecked}}">{{obj.ROL_DESCRIPTION}}                                                    
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="box">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.CNY_NAME.$invalid}">
                                            <label for="txtcode">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Countrylst" data-output-model="UserRole.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                                data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UserRole.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.ZN_NAME.$invalid}">
                                            <label for="txtcode">Zone <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Zonelst" data-output-model="UserRole.selectedZone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME maker"
                                                data-on-item-click="ZonChanged()" data-on-select-all="ZonChangeAll()" data-on-select-none="ZonSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UserRole.selectedZone" name="ZN_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.ZN_NAME.$invalid" style="color: red">Please select Zone </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.STE_NAME.$invalid}">
                                            <label for="txtcode">State <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Statelst" data-output-model="UserRole.selectedState" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME maker"
                                                data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UserRole.selectedState" name="STE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.STE_NAME.$invalid" style="color: red">Please select State </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.CTY_NAME.$invalid}">
                                            <label for="txtcode">City <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Citylst" data-output-model="UserRole.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                                data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UserRole.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.CTY_NAME.$invalid " style="color: red">Please select city </span>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.LCM_NAME.$invalid}">
                                            <label for="txtcode">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locationlst" data-output-model="UserRole.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UserRole.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                        </div>
                                    </div>
                                    <div id="GHvisDiv" data-ng-show="Currentmodule.GH_Visibility">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.TWR_NAME.$invalid}">
                                                <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Towerlist" data-output-model="UserRole.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                                    data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.FLR_NAME.$invalid}">
                                                <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Floorlist" data-output-model="UserRole.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                                    data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.PE_NAME.$invalid}">
                                            <label for="txtcode">Parent Entity  <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Parentlist" data-output-model="UserRole.selectedParentEntity" data-button-label="icon PE_NAME" data-item-label="icon PE_NAME maker"
                                                data-on-item-click="ParentEntityChange()" data-on-select-all="ParentEntityChangeAll()" data-on-select-none="ParentEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UserRole.selectedParentEntity" name="PE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.PE_NAME.$invalid" style="color: red">Please select parent entity </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.CHE_NAME.$invalid}">
                                            <label for="txtcode">Child Entity  <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Childlist" data-output-model="UserRole.selectedChildEntity" data-button-label="icon CHE_NAME" data-item-label="icon CHE_NAME maker"
                                                data-on-item-click="ChildEntityChange()" data-on-select-all="ChildEntityChangeAll()" data-on-select-none="ChildEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="UserRole.selectedChildEntity" name="CHE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.CHE_NAME.$invalid" style="color: red">Please select child entity </span>
                                        </div>
                                    </div>
                                    <div id="showhide" data-ng-show="Currentmodule.VERT_COST">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.VER_NAME.$invalid}">
                                                <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Verticallist" data-output-model="UserRole.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                                    data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedVerticals" name="VER_NAME" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.VER_NAME.$invalid" style="color: red">Please select  {{BsmDet.Parent |lowercase}}  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmUserRole.$submitted && frmUserRole.Cost_Center_Name.$invalid}">
                                                <label for="txtcode">{{BsmDet.Child}}  <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="UserRole.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                                    data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCostcenter" name="Cost_Center_Name" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                <span class="error" data-ng-show="frmUserRole.$submitted && frmUserRole.Cost_Center_Name.$invalid" style="color: red">Please select {{BsmDet.Child |lowercase}} </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>--%>






                <%-- <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color">
                    <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                </div>--%>
                <%--</form>--%>
            </div>
        </div>
    </div>
    <%--</div>--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--    <script src="../../BootStrapCSS/Messenger/heartcode-canvasloader.js"></script>
    <script src="../../BootStrapCSS/Messenger/messenger.js"></script>
    <script src="../../Scripts/angular.js"></script>
    <script src="../../Scripts/angular-resource.min.js"></script>
    <script src="../../Scripts/aggrid/ag-grid.min.js"></script>--%>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js" defer></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script src="../../BootStrapCSS/qtip/qtip.js" defer></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt", "cp.ngConfirm"]);
        FormWizard();


        //FORM WIZARD END JS

        function FormWizard() {
            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function () {

                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        };
    </script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../System%20Preferences/Js/UserRoleMapping.js"></script>
    <script src="../../BootStrapCSS/FMS_UI/js/moment.js"></script>
</body>
</html>


