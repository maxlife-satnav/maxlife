<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmADFAddNewRole.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmADFAddNewRole" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function CheckDate() {
            var dtFrom = document.getElementById("txtFromdate").Value;
            var dtTo = document.getElementById("txtTodate").Value;
            if (dtFrom < dtTo) {
                alert("Invalid Dates");
            }
        }
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>

    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Add Role 
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblError" runat="server" Text="lblError" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-md-6">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Role and Select Modify to modify the existing Role" />
                                        Add
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Role and Select Modify to modify the existing Role" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                            
                            <div id="trCName" runat="server" class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblDesignition" runat="server" class="col-md-5 control-label">Select Role<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="ddlRole" Display="None" ErrorMessage="Please Select Role" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                    ToolTip="Select Country Name">
                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Role Acronym<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvAC" runat="server"
                                                ControlToValidate="txtRoleAcronym" Display="None" ErrorMessage="Please Enter Role Acronym"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtRoleAcronym" runat="server" CssClass="form-control"
                                                        MaxLength="15"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Role Description<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvRoleDescription" ControlToValidate="txtDescription" runat="server" Display="None"
                                                ErrorMessage="Please Enter Role Description"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter Name in alphabets,numbers and (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"
                                                        MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                            <asp:CustomValidator ID="cvRemarks" runat="server" ErrorMessage="Remarks Shoud be less than 500 characters !"
                                                ClientValidationFunction="maxLength" ControlToValidate="txtRemarks" Display="None"></asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                Display="None" ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="500"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">

                                    <div class="row">
                                        <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="btn btn-primary custom-button-color" Text="Submit"></asp:Button>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10">
                                        <Columns>
                                            <asp:BoundField DataField="ROL_DESCRIPTION" HeaderText="Role">
                                                <ControlStyle Width="30%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="No of Users Associated To This Role">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCount" runat="server" CssClass="bodyText" Visible="false" Text='<%#Eval("Count")%>'></asp:Label>
                                                    <%--<a href="#" onclick="showPopWin('frmaddroleviewusers.aspx?ROL_ID=<%#Eval("ROL_ID")%>',300,300,null)">
                                                            <%#Eval("Count")%></a>--%>
                                                    <a href="#" onclick="showPopWin('<%#Eval("ROL_ID")%>')"><%#Eval("Count")%></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ROL_ACRONYM" HeaderText="Role Acronym" Visible="False">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Employee Details</h4>
                </div>
                <div id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            //$("#modelcontainer").load("frmaddroleviewusers.aspx?ROL_ID=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal().fadeIn();
            //});
            $("#modalcontentframe").attr("src", "frmaddroleviewusers.aspx?ROL_ID=" + id);
            $("#myModal").modal().fadeIn();

            return false;
        }
    </script>
</body>
</html>


