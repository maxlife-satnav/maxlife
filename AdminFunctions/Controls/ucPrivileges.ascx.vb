Imports SubSonic
Imports System.Data
Imports System.Data.SqlClient

Partial Class Modules_ucPrivileges
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
         
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetRoleDtls_SP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvPrivileges.DataSource = ds
            gvPrivileges.DataBind()
        End If
    End Sub
End Class
