<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="Privileges.aspx.vb" Inherits="Privileges" Title="Privileges" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <%--    <script type="text/javascript">

        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>--%>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Privileges 
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div id="privilegesList">
                                <div class="row">
                                    <div class="col-sm-6" id="General_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">General Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="General" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="Property_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Property Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="Property" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-6" id="Space_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Space Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="Space" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="Conference_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Conference Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="Conference" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-6" id="Asset_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Asset Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="Asset" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="Maintenance_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Maintenance Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="Maintenance" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-6" id="Helpdesk_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Help desk Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="Helpdesk" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-6" id="GuestHouse_Mod" style="display: none">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Guest House Roles</h3>
                                            </div>
                                            <div class="panel-body">
                                                <table id="GuestHouse" class="table table-condensed table-bordered table-hover table-striped">
                                                    <tr style="background-color: #366599; color: #E2E2E2">
                                                        <th>Role Acronym</th>
                                                        <th>Role Name</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="privilegesadd">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


<%--<script type="text/javascript">
    $(document).ready(function () {
        $.getJSON("../api/AdminfunctionsAPI/GetRoledata", function (resultdata) {
            $.each(resultdata, function (key, value) {
                var table = $("#" + value.ROL_MODULE);
                table.append("<tr>" +
                                    "<td><a href='ADF_WebFiles/frmPrivileges.aspx?_id=" + value.ROL_ID + "&Value=" + value.ROL_ACRONYM + "'>" + value.ROL_ACRONYM + "</a></td>" +
                                    "<td>" + value.ROL_DESCRIPTION + "</td>" +
                                    "</tr>");

                $("#" + value.ROL_MODULE + "_Mod").removeAttr("style");
            });
        });
    });
</script>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $.getJSON("../api/AdminfunctionsAPI/GetRoledata", function (resultdata) {
            $.each(resultdata, function (key, value) {
                var table = $("#" + value.ROL_MODULE);
                table.append("<tr>" +
                                    "<td><a href='AddPrivileges.aspx?_id=" + value.ROL_ID + "'>" + value.ROL_ACRONYM + "</a></td>" +
                                    "<td>" + value.ROL_DESCRIPTION + "</td>" +
                                    "</tr>");
                $("#" + value.ROL_MODULE + "_Mod").removeAttr("style");
            });
        });
    });
</script>
