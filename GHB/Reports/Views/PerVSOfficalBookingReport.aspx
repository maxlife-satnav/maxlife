﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
     <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="PerVSOfficalBookingReportController" onload="setDateVals()" class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Personal VS Official Booking Report</legend>
                    </fieldset>
                    <div class="well">
                        <form id="form1" name="Pervsoffical" data-valid-submit="LoadData()" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.CNY_NAME.$invalid}">
                                        <label class="control-label">Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Country" data-output-model="Pervsofficalbook.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Pervsofficalbook.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.CTY_NAME.$invalid}">
                                        <label class="control-label">City<span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="City" data-output-model="Pervsofficalbook.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Pervsofficalbook.City[0]" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="Pervsofficalbook.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Pervsofficalbook.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                    </div>
                                </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.Request_Type.$invalid}">
                                        <label class="control-label">Status Type</label>
                                        <select id="Request_Type" name="Request_Type" required="" data-ng-model="Pervsofficalbook.Request_Type" class="selectpicker">
                                            <option value="ALL" selected>--ALL--</option>
                                            <option value="2">Booked</option>
                                            <option value="5">Vacant</option>
                                            <option value="6">Withhold</option>
                                            <option data-ng-repeat="sta in Status" value="{{sta.STA_ID}}">{{sta.STA_TITLE}}</option>
                                        </select>
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.Request_Type.$invalid" style="color: red">Please Select Status</span>
                                    </div>
                                </div>

                 

                            </div>
                            <div class="clearfix">
                                               <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Quick Select <span style="color: red;">**</span></label>
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date</label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="Pervsofficalbook.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.FromDate.$invalid" style="color: red;">Please From Date</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date</label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="Pervsofficalbook.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.ToDate.$invalid" style="color: red;">Please To Date</span>
                                    </div>
                                </div>

                    
                                       <div class="col-md-3 col-sm-6 col-xs-12" id="divyear" data-ng-show="EnableYearStatus">

                                    <div class="form-group">
                                        <label for="txtcode">Select Year</label>
                                        <br />
                                        <select id="year" class="selectpicker" data-ng-model="Customized.DateParam">
                                            <option value="">Select</option>
                                        </select>
                                    </div>

                                    <%--<div data-ng-show="EnableHalfyearStatus">
  Half Yearly:<br>
  <select id="halfyear"></select>
</div>
<div id="quarter"  data-ng-show="EnableQuarterStatus">
  Quarterly:<br>
  <select id="quarteryear"></select>
</div>--%>
                                </div>
                                <div class="col-md-1 col-sm-6 col-xs-12">
                                    <br />
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>


                            <div class="row" style="padding-left: 18px">
                                <div class="col-md-6">
                                    <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />
                                    <%-- <div class="col-md-12">
                                    &nbsp;
                                </div>--%>
                                </div>
                                <div class="col-md-6" id="table2">
                                    <br />
                                    <a data-ng-click="GenReport(Pervsofficalbook,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Pervsofficalbook,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Pervsofficalbook,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>

                        </form>
                        <form id="form2">
                            <div id="Tabular" data-ng-show="GridVisiblity">
                                <div class="row" style="padding-left: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div id="Grid" data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                            <div id="Graphicaldiv">
                                <div id="PerOffGraph">&nbsp</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
       <script src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
      <script src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../../Scripts/jspdf.min.js"></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
  
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));
        }

    </script>

    <script src="../../../SMViews/Utility.js"></script>
    <script src="../Js/PerVSOfficalBookingReport.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
</body>
</html>
