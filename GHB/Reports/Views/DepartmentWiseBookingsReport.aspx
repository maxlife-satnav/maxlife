﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
     <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="DepartmentWiseBookingsReportController"  class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Department Wise Bookings Report</legend>
                    </fieldset>
                    <div class="well">
                        <form id="form1" name="Pervsoffical" data-valid-submit="LoadData()" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.CNY_NAME.$invalid}">
                                        <label class="control-label">Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Country" data-output-model="Pervsofficalbook.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Pervsofficalbook.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.CTY_NAME.$invalid}">
                                        <label class="control-label">City<span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="City" data-output-model="Pervsofficalbook.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Pervsofficalbook.City[0]" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="Pervsofficalbook.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Pervsofficalbook.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                    </div>
                                </div>
                                         <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.Request_Type.$invalid}">
                                        <label class="control-label">Status Type</label>
                                        <select id="Request_Type" name="Request_Type" required="" data-ng-model="Pervsofficalbook.Request_Type" class="selectpicker">
                                            <option value="ALL" selected>--ALL--</option>
                                            <option value="2">Booked</option>
                                            <option value="5">Vacant</option>
                                            <option value="6">Withhold</option>
                                            <option data-ng-repeat="sta in Status" value="{{sta.STA_ID}}">{{sta.STA_TITLE}}</option>
                                        </select>
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.Request_Type.$invalid" style="color: red">Please Select Status</span>
                                    </div>
                                </div>
                        
                 

                            </div>
                            <div class="clearfix">
                                                         <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.Booking_Type.$invalid}">
                                        <label class="control-label">Booking Type</label>
                                        <select id="Booking_Type" name="Booking_Type" required="" data-ng-model="Pervsofficalbook.Booking_Type" class="selectpicker">
                                            <option value="ALL" >--ALL--</option>
                                            <option value="1">Personal</option>
                                            <option value="2">Official</option>                         
                                         
                                        </select>
                                        <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.Booking_Type.$invalid" style="color: red">Please Select Booking Type</span>
                                    </div>
                                </div>
                                   <div class="col-md-3 col-sm-6 col-xs-12" id="divMonth" >
                                      <div class="form-group">
                                        <label for="txtcode">Select Month</label>
                                      

                                        <br />
                                        <select id="month" class="selectpicker" data-ng-model="Pervsofficalbook.Month">
                                           
                                                <option value="1">January</option>
                                          <option value="2">February</option>
                                                <option value="3">March</option>
                                          <option value="4">April</option>
                                                <option value="5">May</option>
                                                <option value="6">June</option>
                                                <option value="7">July</option>
                                          <option value="8">August</option>
                                                <option value="9">September</option>
                                          <option value="10" selected>October</option>
                                                <option value="11">November</option>
                                          <option value="12">December</option>
                                    
                                           
                                        </select>
                                    </div></div>
                       
                                       <div class="col-md-3 col-sm-6 col-xs-12" id="divyear" >
                                    <div class="form-group">
                                        <label for="txtcode">Select Year</label>
                                        <br />
                                        <select id="year" class="selectpicker" data-ng-model="Pervsofficalbook.Year">
                                           <option value="2005">2005</option>
                                          <option value="2006">2006</option>
                                                <option value="2007">2007</option>
                                          <option value="2008">2008</option>
                                                <option value="2009">2009</option>
                                                <option value="2010">2010</option>
                                                <option value="2011">2011</option>
                                          <option value="2012">2012</option>
                                                <option value="2013">2013</option>
                                          <option value="2014" >2014</option>
                                                <option value="2015">2015</option>
                                          <option value="2016" selected>2016</option>
                                            <option value="2017">2017</option>
                                        </select>
                                    </div>
                                           
                              
                                </div>
                                  <div class="col-md-1 col-sm-6 col-xs-12">
                                    <br />
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                                </div>
                               


                            <div class="row" style="padding-left: 18px">
                                <div class="col-md-6">
                                  <%--  <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />--%>
                                       </div>
                                    <%-- <div class="col-md-12">
                                    &nbsp;
                                </div>--%>
                             
                                <div class="col-md-6" id="table2">
                                    <br />
                                    <a data-ng-click="GenReport(Pervsofficalbook,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Pervsofficalbook,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Pervsofficalbook,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>

                        </form>
                        <form id="form2">
                            <div id="Tabular" data-ng-show="GridVisiblity">
                                <div class="row" style="padding-left: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div id="Grid" data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                            <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                                <div id="PerOffGraph">&nbsp</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
       <script src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
      <script src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../../Scripts/jspdf.min.js"></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
  
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY')));
        }

    </script>
       <script type="text/javascript">
           //inline javascript
           $(function () {
               //$('#Yearmonth').yearpicker();
               //$('#month').monthpicker();
               //$('#year').yearpicker();
               $('#halfyear').halfyearpicker();
               $('#quarteryear').quarteryearpicker();
           });

           0

           //picker.js  
           $.fn.extend(
           {
               //yearpicker: function () {
               //    var select = $(this);

               //    var year = new Date().getFullYear();
               //    for (var i = -10; i < 1; i++) {
               //        var option = $('<option/>');
               //        var year_to_add = year + i;

               //        option.val(year_to_add).text(year_to_add);

               //        if (year == year_to_add) {
               //            option
               //              .css('font-weight', 'bold')
               //              .attr('selected', 'selected');
               //        }

               //        select.append(option);
               //    }
               //},
               halfyearpicker: function () {
                   var select = $(this);

                   var date = new Date();
                   var year = date.getFullYear();
                   var half = Math.floor(date.getMonth() / 6);

                   for (var i = -10; i < 11; i++) {
                       var year_to_add = year + i;

                       for (var j = 0; j < 2; j++) {
                           var option = $('<option/>');
                           var half_text = j == 0 ? 'Jan-Jun' : 'Jul-Dec';
                           var value = year_to_add + '-' + (j + 1);
                           var text = year_to_add + ' ' + half_text;

                           option.val(value).text(text);

                           if (year_to_add == year && half == j) {
                               option
                                 .css('font-weight', 'bold')
                                 .attr('selected', 'selected');
                           }

                           select.append(option);
                       }
                   }
               },
               quarteryearpicker: function () {
                   var select = $(this);

                   var date = new Date();
                   var year = date.getFullYear();
                   var quarter = Math.floor(date.getMonth() / 3);

                   for (var i = -10; i < 11; i++) {
                       var year_to_add = year + i;

                       for (var j = 0; j < 4; j++) {
                           var option = $('<option/>');
                           var quarter_text = get_quarter_text(j);

                           var value = year_to_add + '-' + (j + 1);
                           var text = year_to_add + ' ' + quarter_text;

                           option.val(value).text(text);

                           if (year_to_add == year && quarter == j) {
                               option
                                 .css('font-weight', 'bold')
                                 .attr('selected', 'selected');
                           }

                           select.append(option);
                       }
                   }

                   function get_quarter_text(num) {
                       switch (num) {
                           case 0:
                               return 'Jan-Mar';
                           case 1:
                               return 'Apr-Jun';
                           case 2:
                               return 'Jul-Sep';
                           case 3:
                               return 'Oct-Dec';
                       }
                   }

               }
               //,
                   //monthpicker: function () {
               //    var select = $(this);
               //    var d = new Date();
               //    var monthArray = new Array();
               //    monthArray[0] = "January";
               //    monthArray[1] = "February";
               //    monthArray[2] = "March";
               //    monthArray[3] = "April";
               //    monthArray[4] = "May";
               //    monthArray[5] = "June";
               //    monthArray[6] = "July";
               //    monthArray[7] = "August";
               //    monthArray[8] = "September";
               //    monthArray[9] = "October";
               //    monthArray[10] = "November";
               //    monthArray[11] = "December";
               //    for (m = 0; m <= 11; m++) {
               //        var optn = document.createElement("OPTION");
               //        optn.text = monthArray[m];
               //        // server side month start from one
               //        optn.value = (m + 1);

               //        // if june selected
               //        if (m == 9) {
               //            optn.selected = true;
               //        }

               //        document.getElementById('month').options.add(optn);
               //    }
               //}
           });



    </script>

    <script src="../../../SMViews/Utility.js"></script>
    <script src="../Js/DepartmentWiseBookingsReport.js"></script>
   <%-- <script src="../Js/PerVSOfficalBookingReport.js"></script>--%>
    <script src="../../../Scripts/moment.min.js"></script>
</body>
</html>
