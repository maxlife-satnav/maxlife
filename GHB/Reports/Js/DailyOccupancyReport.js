﻿app.service("DailyOccupancyReportService", function ($http, $q, UtilityService) {
    this.path = window.location.origin;
    this.GetDailyOccupancyDetails = function (Dataobj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DailyOccupancyReport/GetDailyOccupancyDetails', Dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };



});

app.controller('DailyOccupancyReportController', function ($scope, $q, $http, DailyOccupancyReportService, UtilityService, $timeout, $filter) {
    $scope.DailyOccupancy = {};
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;

    $scope.DailyOccupancy.Country = [];
    $scope.DailyOccupancy.Locations = [];


    $scope.DailyOccupancy.FromDate = moment(new Date()).format('MM/DD/YYYY');


    $scope.columnDefs = [
         { headerName: "Reference Id", field: "RB_REFERENCE_ID", cellClass: 'grid-align', width: 150 },
         { headerName: "Reserved By", field: "RB_CREATEDBY", cellClass: 'grid-align', width: 200, },
         { headerName: "Reserved For", field: "RB_RESERVED_FOR", cellClass: 'grid-align', width: 200 },
         { headerName: "From Date", field: "RB_FROM_DATE", template: '<span>{{data.RB_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
         { headerName: "To Date", field: "RB_TO_DATE", template: '<span>{{data.RB_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
         { headerName: "Room Number/Name", field: "RR_NAME", cellClass: 'grid-align', width: 150 },
         { headerName: "Facility Type", field: "RT_NAME", width: 150, cellClass: 'grid-align' },
         { headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align', width: 150 },
         { headerName: "City", field: "RBS_CTY_CODE", cellClass: 'grid-align', width: 100 },
         { headerName: "Location", field: "RBS_LOC_CODE", cellClass: 'grid-align', width: 150 },
         { headerName: "Cost Center", field: "Cost_Center_Name", cellClass: 'grid-align', width: 150 },
         { headerName: "Department", field: "DEP_NAME", cellClass: 'grid-align', width: 150 },
         { headerName: "Reserved On", field: "RB_CREATEDON", template: '<span>{{data.RB_CREATEDON | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, },
         { headerName: "Reserved For Email", field: "RESVERED_FOR_EMAIL", cellClass: 'grid-align', width: 200 },
         { headerName: "Reserved By Email", field: "RESVERED_BY_EMAIL", cellClass: 'grid-align', width: 200 },
         { headerName: "Check In Date", field: "RB_CHK_IN_DATE", template: '<span>{{data.RB_CHK_IN_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
         { headerName: "Check In Time", field: "RB_CHK_IN_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
         { headerName: "Check Out Date", field: "RB_CHK_OUT_DATE", template: '<span>{{data.RB_CHK_OUT_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
         { headerName: "Check Out Time", field: "RB_CHK_OUT_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },


        //  { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 100 },
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.Pageload = function () {
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                    $scope.DailyOccupancy.Country.push(value);
                });
            }
        });

        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                });
            }
        });

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                    $scope.DailyOccupancy.Locations.push(value);
                });
            }
        });


    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.DailyOccupancy.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.DailyOccupancy.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DailyOccupancy.Country[0] = cny;
            }
        });
    }
    $scope.cnySelectAll = function () {
        $scope.DailyOccupancy.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.ctySelectAll = function () {
        $scope.DailyOccupancy.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.getTowerByLocation = function () {
        $scope.DailyOccupancy.Locations = $scope.Locations;

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DailyOccupancy.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DailyOccupancy.City[0] = cty;
            }
        });
    }
    $scope.locSelectAll = function () {
        $scope.DailyOccupancy.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }
    $scope.lcmSelectNone = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
    }
    $scope.Pageload();

  
    $scope.LoadData = function () {

        var Dataobj = {
            Date: $scope.DailyOccupancy.FromDate,
            lcmlst: $scope.DailyOccupancy.Locations,
        };

            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            DailyOccupancyReportService.GetDailyOccupancyDetails(Dataobj).then(function (data) {
                $scope.gridata = data.data;
                console.log(data);
                if ($scope.gridata == null) {

                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                progress(0, 'Loading...', false);
            }, function (error) {
                console.log(error);
            });
        
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Requisition Id", key: "REQ_ID" }, { title: "Reservation Type", key: "RT_NAME" }, { title: "Reservation Code", key: "RR_CODE" },
            { title: "Reservation Name", key: "RR_NAME" }, { title: "City", key: "RBS_CTY_CODE" }, { title: "Location", key: "RBS_LOC_CODE" }, { title: "Location", key: "LCM_NAME" },
            { title: "From Date", key: "RB_FROM_DATE" }, { title: "To Date", key: "RB_TO_DATE" }, { title: "Department", key: "DEP_NAME" }, { title: "Reserved By", key: "RB_CREATEDBY" },
            { title: "Reserved On", key: "RB_CREATEDON" }, { title: "Reserved For", key: "RB_RESERVED_FOR" }, { title: "Reserved For Email", key: "RESVERED_FOR_EMAIL" }, { title: "Reserved By Email", key: "RESVERED_BY_EMAIL" }, { title: "Check In", key: "RBC_CHK_IN_TIME" }, { title: "Check Out", key: "RBC_CHK_OUT_TIME" },
            { title: "Status", key: "RB_TITLE" }];

        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log($scope.gridOptions.api.isAnyFilterPresent($scope.gridOptions.api.getModel()));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("DailyOccupancyReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "DailyOccupancyReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenReport = function (dailyusage, Type) {
        progress(0, 'Loading...', true);
        cpydailyusage = {};
        angular.copy(dailyusage, cpydailyusage);
        cpydailyusage.Type = Type;

        var Dataobj = {
            Date: cpydailyusage.FromDate,
            lcmlst: cpydailyusage.Locations,
            Type: cpydailyusage.Type
        };

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (dailyusage.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/DailyOccupancyReport/GetDailyOccupancyDetailsReport',
                method: 'POST',
                data: Dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'DailyOccupancyReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });
});