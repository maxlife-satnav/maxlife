﻿app.service("PerVSOfficalBookingReportService", function ($http, $q, UtilityService) {

    this.path = window.location.origin;
    this.GetGriddata = function (Dataobj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/PerVSOfficalBookingReport/GetGuesthousedetails', Dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('PerVSOfficalBookingReportController', function ($scope, $q, $http, PerVSOfficalBookingReportService, UtilityService, $timeout, $filter) {

    $scope.Pervsofficalbook = {};
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;

    $scope.Pervsofficalbook.Country = [];
    $scope.Pervsofficalbook.Locations = [];


    $scope.columnDefs = [
       { headerName: "Facility Type", field: "RT_NAME",  cellClass: 'grid-align', suppressMenu: true, },
       { headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align' },
       { headerName: "Room Number/Name", field: "RR_NAME", cellClass: 'grid-align',  suppressMenu: true, },
       { headerName: "Capcity", field: "RR_CAPCITY", cellClass: 'grid-align'},
       { headerName: "City", field: "RF_CTY_CODE", cellClass: 'grid-align' },
       { headerName: "Location", field: "RF_LOC_CODE", cellClass: 'grid-align',  suppressMenu: true, },
       { headerName: "Personal", field: "PERSONAL", cellClass: 'grid-align',},
       { headerName: "Official", field: "OFFICIAL", cellClass: 'grid-align',  suppressMenu: true, },
       
    ];


    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onReady: function sizeToFit() {
        
            $scope.gridOptions.api.sizeColumnsToFit();
        },

        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.Pageload = function () {
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                    $scope.Pervsofficalbook.Country.push(value);
                });
            }
        });

        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                });
            }
        });

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                    //$scope.Pervsofficalbook.Locations.push(value);
                });
            }
        });
    }


    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Pervsofficalbook.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Pervsofficalbook.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Pervsofficalbook.Country[0] = cny;
            }
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Pervsofficalbook.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.ctySelectAll = function () {
        $scope.Pervsofficalbook.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.getTowerByLocation = function () {
        $scope.Pervsofficalbook.Locations = $scope.Locations;

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Pervsofficalbook.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Pervsofficalbook.City[0] = cty;
            }
        });
    }
    $scope.locSelectAll = function () {
        $scope.Pervsofficalbook.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }
    $scope.lcmSelectNone = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
    }

    $scope.Pageload();




    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Pervsofficalbook.FromDate = moment().format('MM/DD/YYYY');
                $scope.Pervsofficalbook.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Pervsofficalbook.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Pervsofficalbook.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Pervsofficalbook.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Pervsofficalbook.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Pervsofficalbook.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Pervsofficalbook.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Pervsofficalbook.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Pervsofficalbook.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Pervsofficalbook.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Pervsofficalbook.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }

    $scope.Pervsofficalbook.Request_Type = "ALL";

    $scope.Pervsofficalbook.Locations = $scope.Locations;

    $scope.LoadData = function () {

        var Dataobj = {
            FromDate: $scope.Pervsofficalbook.FromDate,
            ToDate: $scope.Pervsofficalbook.ToDate,
            lcmlst: $scope.Pervsofficalbook.Locations,
            Status: $scope.Pervsofficalbook.Request_Type
        };
 
        var fromdate = moment($scope.Pervsofficalbook.FromDate);
        var todate = moment($scope.Pervsofficalbook.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            PerVSOfficalBookingReportService.GetGriddata(Dataobj).then(function (data) {
                $scope.gridata = data;
                if ($scope.gridata == null) {

                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                progress(0, 'Loading...', false);
               
            }, function (error) {
                console.log(error);
            });
        }
    }

    setTimeout(function () {
        $scope.LoadData();
        
    }, 1000);


    setTimeout(function () {
        var Dataobj2 = {
            FromDate: $scope.Pervsofficalbook.FromDate,
            ToDate: $scope.Pervsofficalbook.ToDate,
            lcmlst: $scope.Pervsofficalbook.Locations,
            Status: $scope.Pervsofficalbook.Request_Type
        };

        $scope.AllocChartDetails(Dataobj2)
    }, 2000);

    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();

    var chart;
    chart = c3.generate({
        data: {
            columns: [],
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Official','Personal'],
                height: 130,
                show: true,
            },
            y: {
                show: true,
                label: {
                    text: 'Occupancy Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    

    $scope.AllocChartDetails = function (spcData) {
        console.log(spcData);
        $http({
            url: UtilityService.path + '/api/PerVSOfficalBookingReport/GetPersonalVSOfficialChart',
            method: 'POST',
            data: spcData
        }).success(function (response) {
           chart.unload();
            //chart.load({ columns: response.data.Details });
           chart.load({ columns: response});
            //response.data.Columnnames.splice(0, 1);
            //$scope.ColumnNames = response.data.Columnnames;
        });
        setTimeout(function () {
            $("#PerOffGraph").append(chart.element);
        }, 700);

    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });


    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Facility Type", key: "RT_NAME" }, { title: "Facility Code", key: "RF_NAME" },
            { title: "Room Number/Name", key: "RR_NAME" }, { title: "City", key: "RF_CTY_CODE" }, { title: "Location", key: "RF_LOC_CODE" }, { title: "Capcity", key: "RR_CAPCITY" },
             { title: "Personal", key: "PERSONAL" } ,{ title: "Official", key: "OFFICIAL" }];

        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("PervsofficalReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "PervsofficalReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (ghudata, Type) {
        progress(0, 'Loading...', true);
        ghudata.Type = Type;

        var Dataobj = {
            FromDate: ghudata.FromDate,
            ToDate: ghudata.ToDate,
            lcmlst: ghudata.Locations,
            Status: ghudata.Request_Type,
            Type: ghudata.Type
        };

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (ghudata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/PerVSOfficalBookingReport/GetPerVsofficalReportdata',
                method: 'POST',
                data: Dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'PervsofficalReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

});