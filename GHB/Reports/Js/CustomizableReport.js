﻿app.service("CustomizableReportService", function ($http, $q, UtilityService) {
    this.path = window.location.origin;
    this.GetGriddata = function (Dataobj) {        
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CustomizableReport/GetCustomizabledetails', Dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('CustomizableReportController', function ($scope, $q, $http, CustomizableReportService, UtilityService, $timeout, $filter) {
    $scope.Customizable = {};
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;

    $scope.Customizable.Country = [];
    $scope.Customizable.Locations = [];


    $scope.Cols = [
     { COL: "Room Name", value: "RT_NAME", ticked: false },
     { COL: "Country", value: "RBS_CNY_CODE", ticked: false },
     { COL: "City", value: "RBS_CTY_CODE", ticked: false },
     { COL: "Location", value: "RBS_LOC_CODE", ticked: false },
     { COL: "Reserved By", value: "RB_CREATEDBY", ticked: false },
     { COL: "Reserved On", value: "RB_CREATEDON", ticked: false },
     { COL: "Reserved For", value: "RB_RESERVED_FOR", ticked: false },
     { COL: "From Date", value: "RB_FROM_DATE", ticked: false },
     { COL: "To Date", value: "RB_TO_DATE", ticked: false },
     { COL: "Check In", value: "RB_CHK_IN_TIME", ticked: false },
     { COL: "Check Out", value: "RB_CHK_OUT_TIME", ticked: false },
     { COL: "Status", value: "STA_TITLE", ticked: false },
    ];

    $scope.columnDefs = [
          { headerName: "Reference Id", field: "RB_REFERENCE_ID", width: 165, cellClass: 'grid-align',   pinned: 'left'},
          { headerName: "Facility Type", field: "RT_NAME", width: 150, cellClass: 'grid-align', suppressMenu: true, },
          { headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align', width: 150 },
          { headerName: "Room Number/Name", field: "RR_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
          { headerName: "Capcity", field: "RR_CAPCITY", cellClass: 'grid-align', width: 150 },
          { headerName: "Country", field: "RBS_CNY_CODE", cellClass: 'grid-align', width: 100 },
          { headerName: "City", field: "RBS_CTY_CODE", cellClass: 'grid-align', width: 100 },
          { headerName: "Location", field: "RBS_LOC_CODE", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "From Date", field: "RB_FROM_DATE", template: '<span>{{data.RB_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "To Date", field: "RB_TO_DATE", template: '<span>{{data.RB_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },

           { headerName: "From Time", field: "RB_FRM_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "To Time", field: "RB_TO_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },

          { headerName: "Department", field: "DEP_NAME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "Reserved By", field: "RB_CREATEDBY", cellClass: 'grid-align', width: 150, suppressMenu: true, },
          { headerName: "Reserved On", field: "RB_CREATEDON", template: '<span>{{data.RB_CREATEDON | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, },
          { headerName: "Reserved For", field: "RB_RESERVED_FOR", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "Reserved For Email", field: "RESVERED_FOR_EMAIL", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "Reserved By Email", field: "RESVERED_BY_EMAIL", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "Check In", field: "RB_CHK_IN_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "Check Out", field: "RB_CHK_OUT_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 100 },
    ];


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        showToolPanel: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };



    $scope.Pageload = function () {
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                    $scope.Customizable.Country.push(value);
                });
            }
        });

        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                });
            }
        });

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                    $scope.Customizable.Locations.push(value);
                });
            }
        });

        angular.forEach($scope.Cols, function (value, key) {
            value.ticked = true;
        });

    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customizable.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customizable.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customizable.Country[0] = cny;
            }
        });
    }
    $scope.cnySelectAll = function () {
        $scope.Customizable.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.ctySelectAll = function () {
        $scope.Customizable.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.getTowerByLocation = function () {
        $scope.Customizable.Locations = $scope.Locations;

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customizable.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customizable.City[0] = cty;
            }
        });
    }
    $scope.locSelectAll = function () {
        $scope.Customizable.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }
    $scope.lcmSelectNone = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
    }
    $scope.Pageload();

    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {        
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customizable.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customizable.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customizable.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customizable.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customizable.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customizable.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customizable.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customizable.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customizable.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customizable.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customizable.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customizable.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }

    $scope.Customizable.Request_Type = "ALL";

    $scope.LoadData = function () {

        var Dataobj = {
            FromDate: $scope.Customizable.FromDate,
            ToDate: $scope.Customizable.ToDate,
            Lcmlst: $scope.Customizable.Locations,
            Status: $scope.Customizable.Request_Type
        };

        var fromdate = moment($scope.Customizable.FromDate);
        var todate = moment($scope.Customizable.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            CustomizableReportService.GetGriddata(Dataobj).then(function (data) {
                $scope.gridata = data;
                if ($scope.gridata == null) {

                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    var cols = [];
                    var unticked = _.filter($scope.Cols, function (item) {
                        return item.ticked == false;
                    });
                    var ticked = _.filter($scope.Cols, function (item) {
                        return item.ticked == true;
                    });
                    for (i = 0; i < unticked.length; i++) {
                        cols[i] = unticked[i].value;
                    }
                    $scope.gridOptions.columnApi.setColumnsVisible(cols, false);                    cols = [];                    for (i = 0; i < ticked.length; i++) {
                        cols[i] = ticked[i].value;
                    }                                        $scope.gridOptions.columnApi.setColumnsVisible(cols, true);

                }
                progress(0, 'Loading...', false);
            }, function (error) {
                console.log(error);
            });
        }
    }


    setTimeout(function () {
        $scope.LoadData();
    }, 1000);

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Requisition Id", key: "REQ_ID" }, { title: "Reservation Type", key: "RT_NAME" }, { title: "Reservation Code", key: "RR_CODE" },
            { title: "Reservation Name", key: "RR_NAME" }, { title: "Country", key: "RBS_CNY_CODE" }, { title: "City", key: "RBS_CTY_CODE" }, { title: "Location", key: "RBS_LOC_CODE" }, { title: "Location", key: "LCM_NAME" },
            { title: "From Date", key: "RB_FROM_DATE" }, { title: "To Date", key: "RB_TO_DATE" }, { title: "Department", key: "DEP_NAME" }, { title: "Reserved By", key: "RB_CREATEDBY" },
            { title: "Reserved On", key: "RB_CREATEDON" }, { title: "Reserved For", key: "RB_RESERVED_FOR" }, { title: "Check In", key: "RBC_CHK_IN_TIME" }, { title: "Check Out", key: "RBC_CHK_OUT_TIME" },
            { title: "Status", key: "RB_TITLE" }];        

        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));        
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("CustomizableReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "CustomizableReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenReport = function (Custdata, Type) {
        progress(0, 'Loading...', true);
        Custdata.Type = Type;

        var Dataobj = {
            FromDate: Custdata.FromDate,
            ToDate: Custdata.ToDate,
            lcmlst: Custdata.Locations,
            Status: Custdata.Request_Type,
            Type: Custdata.Type
        };

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Custdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/CustomizableReport/GetCustomizableReportdata',
                method: 'POST',
                data: Dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'CustomizableReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }


});