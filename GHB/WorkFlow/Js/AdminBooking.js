﻿app.service("AdminBookingService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (vabData) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AdminBooking/GetViewAvailabilityDetails', vabData)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.SaveBookingRequest = function (crt) {
        deferred = $q.defer();
        return $http.post('../../../api/AdminBooking/SaveBookingRequest', crt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };




});
app.controller('AdminBookingController', function ($scope, $q, $http, AdminBookingService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.rtlist = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.SaveBooking = {};
    $scope.Employee = [];
    $scope.RFlst = [];

    $scope.disableSavebutton = false;
    var facilityName = "";
    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                //angular.forEach($scope.Country, function (value, key) {
                //    value.ticked = true;
                //})



                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        //angular.forEach($scope.City, function (value, key) {
                        //    value.ticked = true;
                        //})

                        //     $scope.City[0].ticked = true;



                    }

                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locations = response.data;
                            //angular.forEach($scope.Locations, function (value, key) {
                            //    value.ticked = true;
                            //})
                            //$scope.CityList = response.data
                            $scope.Locations[0].ticked = true;

                            angular.forEach($scope.Locations, function (value, key) {
                                var loc = _.find($scope.City, { CTY_CODE: value.CTY_CODE });

                                if (loc != undefined && value.ticked == true) {
                                    loc.ticked = true;

                                }
                            });

                            angular.forEach($scope.City, function (value, key) {
                                var cty = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });

                                if (cty != undefined && value.ticked == true) {
                                    cty.ticked = true;

                                }
                            });

                        }
                        UtilityService.getReservationTypes(2).then(function (response) {
                            $scope.RTlst = response.data;
                            //angular.forEach($scope.RTlst, function (value, key) {
                            //    value.ticked = true;
                            //})


                            UtilityService.GetFacilityNamesbyType(1, $scope.RTlst).then(function (response) {
                                $scope.RFlst = response.data;
                           
                                //angular.forEach($scope.RFlst, function (value, key) {
                                //    value.ticked = true;
                                //})
                                $scope.RFlst[0].ticked = true;

                                angular.forEach($scope.RFlst, function (value, key) {
                                    var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });

                                    if (cny != undefined && value.ticked == true) {
                                        cny.ticked = true;

                                    }
                                });

                            })

                        }, function (error) {
                            console.log(error);
                        });
                    });
                });
            }
        });




    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }



    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.LocationChange();
    }

    $scope.LocationChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });
    }

    $scope.LocationSelectNone = function () {
        $scope.Locationlst = [];
        $scope.LocationChange();
    }



    $scope.FTSelectAll = function () {
        //$scope.Customized.RTlst = $scope.RFlst;
        //angular.forEach($scope.RFlst, function (value, key) {
        //    value.ticked = false;
        //});
        $scope.FTChange();
    }

    $scope.FTChange = function () {

        angular.forEach($scope.RFlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.RTlst, function (value, key) {
            var cny = _.find($scope.RFlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO[0] = cny;
            }
        });

        //UtilityService.GetFacilityNamesbyType(1, $scope.RTlst).then(function (response) {
        //    $scope.RFlst = response.data;
        //    angular.forEach($scope.RFlst, function (value, key) {
        //        value.ticked = true;
        //    })

        //}, function (error) {
        //    console.log(error);
        //});


    }
    $scope.getFacilityNamesbyType = function () {

        UtilityService.GetFacilityNamesbyType(0, $scope.RTlst).then(function (response) {

            $scope.RFlst = response.data;

        }, function (error) {
            console.log(error);
        });

    }

    $scope.FTSelectNone = function () {
        $scope.RFlst = [];
        //  $scope.FTChange();
    }


    // fecility name change multiselect

    $scope.FNSelectAll = function () {
        $scope.Customized.RTlst = $scope.RFlst;


        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO[0] = cny;
            }
        });

        //angular.forEach($scope.RTlst, function (value, key) {
        //    value.ticked = true;
        //});
    }
    $scope.FNChange = function () {
        angular.forEach($scope.RTlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO = cny;
            }
        });
    }
    $scope.FNSelectNone = function () {
        //$scope.RTlst = [];
        $scope.FNChange();
    }




    $scope.Customized.selVal = "30";

    $scope.rptDateRanges = function () {
        switch ($scope.Customized.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTYEAR':
                $scope.Customized.FromDate = moment().subtract(1, 'year').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;


        }
    }

    $scope.columnDefs = [
              {
                  headerName: "Select All", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)' />", cellClass: 'grid-align',
                  headerCellRenderer: headerCellRendererFunc
              },
             { headerName: "Facility Type", field: "RT_NAME", cellClass: 'grid-align', width: 150 },
             { headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align', width: 200 },
             { headerName: "Room Name/Number", field: "RR_NAME", cellClass: 'grid-align', width: 150 },
              { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', width: 80 },
              { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100 },
             { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 180 },

        //     { headerName: "Choose", field: "", width: 55, cellClass: 'grid-align', template: '<a ng-click="viewCalendar(data)" ><span class="glyphicon glyphicon-calendar"></span></a>', filter: 'text', pinned: 'right', suppressMenu: true },


    ];

 

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        // showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },

        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


    $scope.initCal = function () {
        var loccode = _.find($scope.Locations, { ticked: true });
        facilityName = _.find($scope.RFlst, { ticked: true });
        $('#bookingdetails').hide();
        events = new Array();
        $('#calendar').fullCalendar({
            //titleFormat: {
            //    month: 'MMMM yyyy',
            //    week: "MMM d[ yyyy]{ '-'[ MMM] d yyyy}",
            //    day: 'MM/dd'
            //},
            header: {
                left: 'prev,next today',
                center: 'title',
                //right: 'month,agendaWeek,agendaDay'
                right: 'month,agendaWeek,listDay,listWeek'

            },

            views: {
                listDay: { buttonText: 'list day' },
                listWeek: { buttonText: 'list week' },
                //month: { // name of view
                //    titleFormat: 'YYYY, MM, DD'
                //    // other view-specific options here
                //}
            },

            eventLimit: true, // for all non-agenda views
            timeFormat: 'H:mm',
            // defaultView: 'month',
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                ShowEventPopup(start, end);
                clearNUpdateVals();
            },
            editable: true,
            allDaySlot: false,
            selectable: true,
            events: function (start, end, timezone, callback) {

                $.ajax({
                    url: '../../../api/AdminBooking/GetBookingDetails',
                    type: 'post',
                    data: {
                        obj: $scope.SaveBooking,
                        objList: $scope.ABDetailslst,
                        ScreenType: "AdminBooking",
                        LocationCode: loccode.LCM_CODE,
                        start: start.unix(),
                        end: end.unix()
                    },
                    success: function (data) {
                        var events = [];
                        angular.forEach(data, function (value, key) {
                            events.push({
                                title: value.title,
                                start: value.start,
                                end: value.end,
                                color: value.color,
                                referenceID: value.ReferenceID,
                                reservedFor: value.ReservedFor,
                                remarks: value.Remarks,
                                AUR_NAME: value.AUR_NAME,
                                RB_TYPE: value.RB_TYPE,
                                RoomNames: value.RoomNames,
                                LCM_NAME: value.LCM_NAME,
                                RF_NAME:value.RF_NAME
                            });
                        });
                        callback(events);
                    }

                });
            },
            eventClick: function (calEvent, jsEvent, view) {
                $('#bookingdetails').show();
                $scope.Employee = [];
                $scope.$apply(function () {
                    $scope.$broadcast('angucomplete-alt:clearInput');
                    $scope.SaveBooking.selectedEmp = {
                        selected: {
                            data: { AUR_ID: calEvent.ReservedFor, NAME: calEvent.AUR_NAME, ticked: false },
                            title: ""
                        }
                    }

                    $scope.reselectedEmp($scope.SaveBooking.selectedEmp);
                    var aurid = calEvent.ReservedFor;
                    $scope.SaveBookingObj.bookinghead = "Booking Details:";
                    $scope.SaveBooking.BM_TITLE = calEvent.title;
                    $scope.SaveBooking.BM_FROM_DATE = moment(calEvent.start).format('MM/DD/YYYY');
                    $scope.SaveBooking.BM_TO_DATE = moment(calEvent.end).format('MM/DD/YYYY');
                    $scope.SaveBooking.BM_FROM_TIME = moment(calEvent.start).format("HH:mm");
                    $scope.SaveBooking.BM_TO_TIME = moment(calEvent.end).subtract(0, 'minutes').format("HH:mm");
                    $scope.SaveBooking.BM_REFRERENCE_ID = calEvent.ReferenceID;
              
                    $scope.SaveBooking.BM_TYPE = calEvent.RB_TYPE;
                    $scope.SaveBooking.BM_STA_ID = "2";
                    $scope.SaveBooking.BM_REMARKS = calEvent.Remarks;
                    $scope.disableSavebutton = true;
                    $scope.emp = { NAME: calEvent.AUR_NAME, AUR_ID: calEvent.ReservedFor };
                    $scope.Employee.push($scope.emp);
                    $scope.SaveBooking.BM_REMARKS = calEvent.Remarks;

                    $scope.SaveBooking.RoomName = calEvent.RoomNames;
                    $scope.SaveBooking.LocationName = calEvent.LCM_NAME;
                    $scope.SaveBooking.FacilityName = calEvent.RF_NAME;

                    //$scope.changeInput(calEvent.ReservedFor, calEvent.AUR_NAME);
                });
            },
         

            eventAfterRender: function (event, $el, view) {
                event.editable = false;
                
                var fromTime = $.fullCalendar.moment(event.start).format("HH:mm");
                var toTime = $.fullCalendar.moment(event.end).format("HH:mm");
                var formattedTime = fromTime + " - " + toTime;
                //  var formattedTime = $.fullCalendar.formatDates(event.start, event.end, "HH:mm { - HH:mm}");
                // If FullCalendar has removed the title div, then add the title to the time div like FullCalendar would do
                if ($el.find(".fc-event-title").length === 0) {

                    $el.find(".fc-time").text(formattedTime);
                }
                else {
                    $el.find(".fc-time").text(formattedTime);
                }

            },

            eventRender: function (event, element) {
                var tooltip = '<div class="tooltipevent" style="width:250px;height:150px">'
                    + '<b>Location Name:</b> ' + event.LCM_NAME + "<br/>"
                    + '<b>Title:</b> ' + event.title + "<br/>"
                    + '<b>Facility Name:</b> ' + event.RF_NAME + "<br/>"
                    + '<b>Room Name:</b> ' + event.RoomNames + "<br/>"
                    + '<b>Booked To:</b> ' + event.AUR_NAME + "<br/>"
                 '</div>';
                var $tootlip = $(tooltip).appendTo('body');
                element.qtip({
                    style: {
                        classes: 'qtip-bootstrap',
                       //qtip-shadow{ } /* Adds a shadows to your tooltips */
                        //qtip-rounded{ } /* Adds a rounded corner to your tooltips */
                        //qtip-bootstrap{ } /* Bootstrap style */
                        //qtip-tipsy{ } /* Tipsy style */
                        //qtip-youtube{ } /* Youtube style */
                        //qtip-jtools{ } /* jTools tooltip style */
                        //qtip-cluetip{ } /* ClueTip style */
                        //qtip-tipped{ } /* Tipped style */
                    },
                    content: $tootlip,
                    position: {
                        my: 'top left',  // Position my top left...
                        at: 'bottom right', // at the bottom right of...
                    }
                    
                });
                $('.tooltipevent').hide();
            },
            dayClick: function (date, allDay, jsEvent, view) {
                clearNUpdateVals();
            },
        });

    }

    function clearNUpdateVals() {
        $scope.disableSavebutton = false;
        $scope.$apply(function () {
            $scope.SaveBookingObj.bookinghead = "Current Booking:";
            $scope.SaveBooking.BM_TITLE = "";
            $scope.SaveBooking.BM_REFRERENCE_ID = "";
            $scope.SaveBooking.BM_REMARKS = "";
            $scope.SaveBooking.BM_RESERVED_FOR = "";
            $scope.$broadcast('angucomplete-alt:clearInput', 'ex7');
            $scope.frmSubmitBooking.$setPristine();
            $scope.frmSubmitBooking.$setUntouched();
            var rrnames = "";
            for (i = 0; i < $scope.selectedRows.length; i++) {
                rrnames = rrnames + "," + $scope.selectedRows[i].RR_NAME;
            }
            if (rrnames.charAt(0) === ',')
                rrnames = rrnames.slice(1);
            var loccode = _.find($scope.Locations, { ticked: true });
            $scope.SaveBooking.RoomName = rrnames;
            $scope.SaveBooking.LocationName = loccode.LCM_NAME;
            $scope.SaveBooking.FacilityName = facilityName.RF_NAME;

        });
    }





    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.LoadData = function (stat, ReqType) {

        var params = {
            loclst: $scope.Customized.Locations,
            STAT: stat,
            rflist: $scope.Customized.RFlst
        };

        AdminBookingService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            $scope.GridVisiblity = true;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                $scope.selectedRows = [];
                showNotification('', 8, 'bottom-right', '');
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
            var cols = [];
            var unticked = _.filter($scope.Cols, function (item) {
                return item.ticked == false;
            });
            var ticked = _.filter($scope.Cols, function (item) {
                return item.ticked == true;
            });
            for (i = 0; i < unticked.length; i++) {
                cols[i] = unticked[i].value;
            }

            $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
            cols = [];
            for (i = 0; i < ticked.length; i++) {
                cols[i] = ticked[i].value;
            }
            $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            //progress(0, '', false);
        });

    }, function (error) {
        console.log(error);
    }


    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.Pageload();
    $scope.Customized.Request_Type = "All";
    setTimeout(function () {
        $scope.LoadData(0, 'a');
    }, 2000);

    setTimeout(function () { progress(0, 'Loading...', true); }, 200);
    //$scope.gridOptions.rowHeight = 70;

    /// booking submission

    $scope.SaveBooking = {};
    $scope.SaveBookingObj = {};
    var title = "";

    function ShowEventPopup(from, to) {
        $('#bookingdetails').show();
        $scope.$apply(function () {
            $scope.SaveBooking.BM_FROM_DATE = $.fullCalendar.moment(from).format('MM/DD/YYYY');
            $scope.SaveBooking.BM_TO_DATE = $.fullCalendar.moment(to).subtract(1, 'days').format('MM/DD/YYYY');
            //   $scope.SaveBooking.BM_TO_DATE = $.fullCalendar.moment(from).format('MM/DD/YYYY');
            $scope.SaveBooking.BM_FROM_TIME = $.fullCalendar.moment(from).format("HH:mm");
            $scope.SaveBooking.BM_TO_TIME = $.fullCalendar.moment(to).subtract(1, 'minutes').format("HH:mm");
        });

    }


    $scope.viewCalendar = function (data) {
        if (data.length != 0) {
        
            $scope.SaveBookingObj = data;
            $('#calendar').fullCalendar('destroy');
            $scope.ABDetailslst = [];

            angular.forEach($scope.selectedRows, function (value, key) {
                var wdobj = {};
                wdobj.BD_RT_SNO = value.RR_RT_SNO;
                wdobj.BD_RF_SNO = value.RR_RF_SNO;
                wdobj.BD_RR_SNO = value.RR_SNO;
                $scope.ABDetailslst.push(wdobj);

            });
            setTimeout(function () {
                $scope.initCal();
            }, 500);
            $("#viewCalendar").modal('show');
        }
        else {
            showNotification('error', 8, 'bottom-right', "Please Select atleast one Room to Proceed.");
        }
    }

    $('#viewCalendar').on('shown.bs.modal', function () {
        $("#calendar").fullCalendar('render');
        $('#qtip-fullcalendar').hide();
        $('.tooltipevent').hide();
    });

    $scope.SaveBookingRequest = function () {
        $scope.SaveBooking.BM_STA_ID = 2;

        $scope.ABDetailslst = [];

        angular.forEach($scope.selectedRows, function (value, key) {
            var wdobj = {};
            wdobj.BD_RT_SNO = value.RR_RT_SNO;
            wdobj.BD_RF_SNO = value.RR_RF_SNO;
            wdobj.BD_RR_SNO = value.RR_SNO;
            $scope.ABDetailslst.push(wdobj);

        });

        var loccode = _.find($scope.Locations, { ticked: true });
        var ctycode = _.find($scope.City, { ticked: true });
        var cnycode = _.find($scope.Country, { ticked: true });
        $scope.SaveBooking.BM_LCM_CODE = loccode.LCM_CODE;
        $scope.SaveBooking.BM_CTY_CODE = ctycode.CTY_CODE;
        $scope.SaveBooking.BM_CNY_CODE = cnycode.CNY_CODE;

        var ReqObj = { obj: $scope.SaveBooking, objList: $scope.ABDetailslst };
    
        AdminBookingService.SaveBookingRequest(ReqObj).then(function (response) {
            $scope.ShowMessage = true;
            $scope.Success = response.Message;
            showNotification('success', 8, 'bottom-right', $scope.Success);
            $scope.SaveBooking = {};
            $("#viewCalendar").modal('hide');
        }, function (error) {
            $scope.ShowMessage = true;
            $scope.Success = error.data;
            setTimeout(function () {
                $scope.$apply(function () {
                    showNotification('error', 8, 'bottom-right', $scope.Success);
                    $scope.ShowMessage = false;
                });
            }, 1000);
            console.log(error);
        });



    }

    $scope.clearData = function (form_) {
        //  $scope.SaveBooking = {};
        $('#bookingdetails').hide();
        $scope.SaveBookingObj.bookinghead = "";
    }




    $scope.selectedEmp = function (selected) {
        if (selected) {
            $scope.selectedEmployee = selected.originalObject;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.SaveBooking.BM_RESERVED_FOR = $scope.selectedEmployee.AUR_ID;
  
        } else {

        }
    };

    $scope.reselectedEmp = function (obj) {
        if (obj) {
            $scope.selectedEmployee = obj.selected.data;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.SaveBooking.BM_RESERVED_FOR = $scope.selectedEmployee.AUR_ID;
    
        } else {

       
        }
    };



    $scope.selectedRows = [];

    $scope.chkChanged = function (data) {
        if (data.ticked) {
            $scope.selectedRows.push(data);
        }
        else {
            $scope.selectedRows = _.reject($scope.selectedRows, function (d) {
                return d.RR_SNO == data.RR_SNO;
            });
        }
    }
    $scope.remoteUrlRequestFn = function (str) {
        return { q: str };
    };
    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    //angular.forEach($scope.gridOptions.rowData, function (value, key) {
                    //    value.ticked = true;
                    //    $scope.selectedRows.push(value);
                    //});
                    if ($scope.gridOptions.api.isAnyFilterPresent()) {
                        $scope.gridOptions.api.forEachNodeAfterFilter(function (node, index) {
                            node.data.ticked = true;
                            $scope.selectedRows.push(node.data);
                        });
                    }
                    else {
                        angular.forEach($scope.gridOptions.rowData, function (value, key) {
                            value.ticked = true;
                            $scope.selectedRows.push(value);

                        });
                    }
                });
            } else {
                $scope.$apply(function () {
                angular.forEach($scope.gridOptions.rowData, function (value, key) {
                   
                        $scope.selectedRows = _.reject($scope.selectedRows, function (d) {
                            return d.RR_SNO == value.RR_SNO;
                        });
                        value.ticked = false;
                    });
                  
                });
            }
        });
        return eHeader;
    }

    $scope.SaveBooking.BM_TYPE = "2";


});
