﻿
app.service("ViewAndModifyService", function ($http, $q, UtilityService) {

    this.path = window.location.origin;
    this.GetBookedRequests = function (type) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewAndModify/GetBookedRequests?ScreenType=' + type + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.UpdateBookedRequest = function (obj) {
        deferred = $q.defer();
        return $http.post('../../../api/ViewAndModify/UpdateBookedRequest', obj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetRequestDetails = function (reqid) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAndModifyWithhold/GetRequestDetails?ReqID=' + reqid + '')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('ViewAndModifyController', function ($scope, $q, $http, ViewAndModifyService, UtilityService, $timeout, $filter, AdminBookingService) {
    $scope.ViewAndModify = {};

    $scope.ViewAndModify.OPCountry = [];
    $scope.ViewAndModify.OPCity = [];
    $scope.ViewAndModify.OPLocation = [];

    $scope.ipCountry = [];
    $scope.ipCity = [];
    $scope.ipLocation = [];

    $scope.Viewstatus = 0;
    $scope.GridVisiblity = true;
    var facilityName = "";
    var loccode;
    $scope.selectedRows = [];

    $scope.columnDefs = [
    { headerName: "Requisition Id", field: "BM_REQ_ID", width: 165, cellClass: 'grid-align', filter: 'text', template: '<a ng-click="onRowSelectedFunc(data)">{{data.BM_REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
     { headerName: "Reference Id", field: "BM_REFERENCE_ID", cellClass: 'grid-align', width: 180 },
     { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 180 },
     { headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align', width: 180 },
      { headerName: "Room Name/Number", field: "RR_NAME", cellClass: 'grid-align', width: 150 },
      { headerName: "From Date", field: "BM_FROM_DATE", template: '<span>{{data.BM_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "To Date", field: "BM_TO_DATE", template: '<span>{{data.BM_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "From Time", field: "BM_FRM_TIME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "To Time", field: "BM_TO_TIME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "Reserved By", field: "RESERVED_BY", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved By Email", field: "RESERVED_BY_EMAIL", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved For", field: "RESERVED_FOR", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved For Email", field: "RESERVED_FOR_EMAIL", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved Date", field: "RESERVED_DT", template: '<span>{{data.RESERVED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, },
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.setRowData()
        },
    };

    $scope.back = function () {
        $scope.Viewstatus = 0;
    }
    $scope.LoadData = function () {
        setTimeout(function () { progress(0, 'Loading...', true); }, 200);
        ViewAndModifyService.GetBookedRequests('Adminbooking').then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 200);
            }
            else {
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 200);
            }
        });
    }
  
  //  setTimeout(function () {
        $scope.LoadData();
  //  }, 500);


    

    $scope.RequestDetailsGrid = [];

 
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.rtlist = [];
    $scope.GridVisiblityV = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];


    $scope.RFlst = [];


    $scope.Pageload = function (data) {
        var rowdata = data;
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.ipCountry = response.data;
                angular.forEach(rowdata, function (value, key) {
                    var cny = _.find($scope.ipCountry, { CNY_CODE: value.RF_CNY_CODE });
                    if (cny != undefined) {
                        cny.ticked = true;
                    }
                });
            }

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.ipCity = response.data;
                    angular.forEach(rowdata, function (value, key) {
                        var cty = _.find($scope.ipCity, { CTY_CODE: value.RF_CTY_CODE });
                        if (cty != undefined) {
                            cty.ticked = true;
                        }
                    });
                }

                UtilityService.getLocations(2).then(function (response) {
                    if (response.data != null) {
                        $scope.ipLocation = response.data;
                        angular.forEach(rowdata, function (value, key) {
                            var loc = _.find($scope.ipLocation, { LCM_CODE: value.RF_LOC_CODE });
                            if (loc != undefined) {
                                loc.ticked = true;
                            }
                        });
                    }

                    UtilityService.getReservationTypes(2).then(function (response) {
                        $scope.RTlst = response.data;
                        angular.forEach(rowdata, function (value, key) {
                            var rt = _.find($scope.RTlst, { RT_SNO: value.RR_RT_SNO });
                            if (rt != undefined) {
                                rt.ticked = true;
                            }
                        });
                        UtilityService.GetFacilityNamesbyType(1, $scope.RTlst).then(function (response) {
                            $scope.RFlst = response.data;
                            angular.forEach(rowdata, function (value, key) {
                                var rf = _.find($scope.RFlst, { RF_SNO: value.RR_RF_SNO });
                                if (rf != undefined) {
                                    rf.ticked = true;
                                }
                            });

                        });
                    });
                });
            });
        }, function (error) {
            console.log(error);
        });

    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }



    $scope.locSelectAll = function () {
        $scope.LocationChange();
    }

    $scope.LocationChange = function () {

        angular.forEach($scope.ipCountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.ipCity, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.ipLocation, function (value, key) {
            var cny = _.find($scope.ipCountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ViewAndModify.OPCountry = cny;
            }
        });

        angular.forEach($scope.ipLocation, function (value, key) {
            var cty = _.find($scope.ipCity, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ViewAndModify.OPCity = cty;
            }
        });


    }

    $scope.LocationSelectNone = function () {
        $scope.LocationChange();
    }



    $scope.FTSelectAll = function () {
        //$scope.Customized.RTlst = $scope.RFlst;
        //angular.forEach($scope.RFlst, function (value, key) {
        //    value.ticked = false;
        //});
        $scope.FTChange();
    }

    $scope.FTChange = function () {

        angular.forEach($scope.RFlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.RTlst, function (value, key) {
            var cny = _.find($scope.RFlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO[0] = cny;
            }
        });

    }

    $scope.FTSelectNone = function () {
        $scope.RFlst = [];
        //  $scope.FTChange();
    }


    // fecility name change multiselect

    $scope.FNSelectAll = function () {
        $scope.Customized.RTlst = $scope.RFlst;


        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                //  $scope.Customized.RT_SNO[0] = cny;
            }
        });

        //angular.forEach($scope.RTlst, function (value, key) {
        //    value.ticked = true;
        //});
    }
    $scope.FNChange = function () {
        angular.forEach($scope.RTlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO = cny;
            }
        });
    }
    $scope.FNSelectNone = function () {
        //$scope.RTlst = [];
        $scope.FNChange();
    }

    $scope.getFacilityNamesbyType = function () {
        
        UtilityService.GetFacilityNamesbyType(0, $scope.RTlst).then(function (response) {
            
            $scope.RFlst = response.data;

        }, function (error) {
            console.log(error);
        });

    }
    $scope.columnDefsV = [
     { headerName: "Select All", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)' />", cellClass: 'grid-align', headerCellRenderer: headerCellRendererFunc },
     { headerName: "Facility Type", field: "RT_NAME", cellClass: 'grid-align', width: 150 },
     { headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align', width: 200 },
     { headerName: "Room Name/Number", field: "RR_NAME", cellClass: 'grid-align', width: 150 },
     { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', width: 80 },
     { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100 },
     { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 180 },

    ];

    $scope.gridOptionsV = {
        columnDefs: $scope.columnDefsV,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        // showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },

        onReady: function () {
            $scope.gridOptionsV.api.sizeColumnsToFit()
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptionsV.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

   
    $scope.LoadDataV = function (stat, ReqType) {


     
        
        var params = {
            loclst: $scope.ViewAndModify.OPLocation,
            STAT: stat,
            rflist: $scope.Customized.RFlst
        };
        // console.log(params);
        AdminBookingService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            progress(0, 'Loading...', true);

            if ($scope.gridata == null) {
                $scope.GridVisiblityV = true;
                $scope.gridOptionsV.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.GridVisiblityV = true;
                $scope.selectedRows = [];
                $scope.gridOptionsV.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }

        });

    }, function (error) {
        console.log(error);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
  

    $scope.CalClick = function (calEvent) {
        $scope.$broadcast('angucomplete-alt:clearInput');
        $scope.SaveBookingObj.bookinghead = "Booking Details:";
        $scope.SaveBooking.BM_TITLE = calEvent.title;
        $scope.SaveBooking.BM_FROM_DATE = moment(calEvent.start).format('MM/DD/YYYY');
        $scope.SaveBooking.BM_TO_DATE = moment(calEvent.end).format('MM/DD/YYYY');
        $scope.SaveBooking.BM_FROM_TIME = moment(calEvent.start).format("HH:mm");
        $scope.SaveBooking.BM_TO_TIME = moment(calEvent.end).subtract(0, 'minutes').format("HH:mm");
        $scope.SaveBooking.BM_REFRERENCE_ID = calEvent.ReferenceID;
        $scope.SaveBooking.BM_RESERVED_FOR = calEvent.ReservedFor;
        $scope.SaveBooking.BM_REMARKS = calEvent.Remarks;
        $scope.SaveBooking.BM_TYPE = calEvent.RB_TYPE;
        $scope.SaveBooking.BM_STA_ID = "4";
      
        $scope.SaveBooking.selectedEmp = {
            selected: {
                data: { AUR_ID: calEvent.ReservedFor, NAME: calEvent.AUR_NAME, ticked: false },
                title: ""
            }
        }
        $scope.reselectedEmp($scope.SaveBooking.selectedEmp);
        $scope.SaveBooking.RoomName = calEvent.RoomNames;
        $scope.SaveBooking.LocationName = calEvent.LCM_NAME;
        $scope.SaveBooking.FacilityName = calEvent.RF_NAME;
    }

    $scope.remoteUrlRequestFn = function (str) {
        return { q: str };
    };


    $scope.selectedEmp = function (selected) {
        if (selected) {
            $scope.selectedEmployee = selected.originalObject;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.SaveBooking.BM_RESERVED_FOR = $scope.selectedEmployee.AUR_ID;
        } else {
            
        }
    };
    $scope.reselectedEmp = function (obj) {
        if (obj) {
            $scope.selectedEmployee = obj.selected.data;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.SaveBooking.BM_RESERVED_FOR = $scope.selectedEmployee.AUR_ID;
            
        } else {
               
        }
    };
   
    $scope.viewCalendar = function (data) {
        if (data.length != 0) {
            $scope.SaveBookingObj = data;
            $('#calendar').fullCalendar('destroy');
            $scope.initCal();
            $("#viewCalendar").modal('show');
            $scope.SaveBookingObj.bookinghead = "";
        }
        else {
            showNotification('error', 8, 'bottom-right', "Please Select atleast one Room to Proceed.");
        }
  
    }

    $scope.initCal = function () {
        facilityName = _.find($scope.RFlst, { ticked: true });
        $('#bookingdetails').hide();
        events = new Array();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                //right: 'month,agendaWeek,agendaDay'
                right: 'month,agendaWeek,listDay,listWeek'

            },
            views: {
                listDay: { buttonText: 'list day' },
                listWeek: { buttonText: 'list week' },
                //month: { // name of view
                //    titleFormat: 'YYYY, MM, DD'
                //    // other view-specific options here
                //}
            },

            eventLimit: true, // for all non-agenda views
            timeFormat: 'H:mm',
            // defaultView: 'month',
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                //$('#bookingdetails').show();
                ShowEventPopup(start, end);
                clearNUpdateVals();
            },
            editable: true,
            allDaySlot: false,
            selectable: true,
            //slotMinutes: 15,
            events: {
                url: '../../../api/ViewAndModify/GetBookedEvent',
                type: 'GET',
                data: {
                    RB_REQ_ID: $scope.ViewAndModify.BM_REQ_ID,
                    screenType: "Adminbooking",
                    LocationCode: loccode
                }

            },
            //eventColor: '#FF0000',
            eventClick: function (calEvent, jsEvent, view) {
                // console.log(calEvent);
                //alert('You clicked on event id: ' + calEvent.id
                //    + "\nSpecial ID: " + calEvent.someKey
                //    + "\nAnd the title is: " + calEvent.title);
                $('#bookingdetails').show();
                
                $scope.$apply(function () {
                    $scope.CalClick(calEvent);
                });

            },

            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
                if (confirm("Confirm Update?")) {
                    $scope.SaveBooking.BM_TYPE = event.RB_TYPE;
                    $scope.SaveBooking.BM_TITLE = event.title;
                    $scope.SaveBooking.BM_FROM_DATE = $.fullCalendar.moment(event._start._d).format('MM/DD/YYYY');
                    //$scope.SaveBooking.BM_TO_DATE = $.fullCalendar.moment(event._end._d).format('MM/DD/YYYY');
                    $scope.SaveBooking.BM_TO_DATE = $.fullCalendar.moment(event._start._d).format('MM/DD/YYYY');
                    $scope.SaveBooking.BM_FROM_TIME = moment(event.start._i).format("HH:mm");
                    $scope.SaveBooking.BM_TO_TIME = moment(event.end._i).format("HH:mm");
                    $scope.SaveBooking.BM_REFRERENCE_ID = event.ReferenceID;
                    $scope.SaveBooking.BM_RESERVED_FOR = event.ReservedFor;
                    $scope.SaveBooking.BM_REMARKS = event.Remarks;
                    $scope.UpdateBookedRequest(4);
                }
                else {
                    revertFunc();
                }
            },

        

            eventAfterRender: function (event, $el, view) {
                var fromTime = $.fullCalendar.moment(event.start).format("HH:mm");
                var toTime = $.fullCalendar.moment(event.end).format("HH:mm");
                var formattedTime = fromTime + " - " + toTime;
                if ($el.find(".fc-event-title").length === 0) {
                    $el.find(".fc-time").text(formattedTime);
                }
                else {
                    $el.find(".fc-time").text(formattedTime);
                }
                $($el).css("background-color", "#008000");
            },
            eventRender: function (event, element) {
                var tooltip = '<div class="tooltipevent" style="width:250px;height:150px">'
                    + '<b>Location Name:</b> ' + event.LCM_NAME + "<br/>"
                    + '<b>Title:</b> ' + event.title + "<br/>"
                    + '<b>Facility Name:</b> ' + event.RF_NAME + "<br/>"
                    + '<b>Room Name:</b> ' + event.RoomNames + "<br/>"
                    + '<b>Booked To:</b> ' + event.AUR_NAME + "<br/>"
                '</div>';
                var $tootlip = $(tooltip).appendTo('body');
                element.qtip({
                    style: {
                        classes: 'qtip-bootstrap',
                    },
                    content: $tootlip,
                    position: {
                        my: 'top left',  // Position my top left...
                        at: 'bottom right', // at the bottom right of...
                    }

                });
                $('.tooltipevent').hide();
            },
            dayClick: function (date, allDay, jsEvent, view) {
                clearNUpdateVals();
            },
        });

        CalLoading = false;

    }

    $('#viewCalendar').on('shown.bs.modal', function () {
        $("#calendar").fullCalendar('render');
    });

    function clearNUpdateVals() {
        $("#txttowhom").hide();
        $("#ex7").show();
        $scope.$apply(function () {
            $scope.SaveBookingObj.bookinghead = "Current Booking:";
            $scope.SaveBooking.BookedCountry = $scope.SaveBooking.COUNTRY;
            $scope.SaveBooking.BookedCity = $scope.SaveBooking.CITY;
            $scope.SaveBooking.BookedLocation = $scope.SaveBooking.LOCATION;
            $scope.SaveBooking.BookedReservationName = $scope.SaveBooking.RR_NAME;

            $scope.SaveBooking.BM_TYPE = "1";
            $scope.SaveBooking.BM_TITLE = "";
            $scope.SaveBooking.BM_REFRERENCE_ID = "";
            $scope.SaveBooking.BM_RESERVED_FOR = "";
            $scope.SaveBooking.BM_REMARKS = "";
            $scope.$broadcast('angucomplete-alt:clearInput', 'ex7');
            $scope.countrySelected('');
            $scope.frmSubmitBooking.$setPristine();
            $scope.frmSubmitBooking.$setUntouched();

            var rrnames = "";
            for (i = 0; i < $scope.selectedRows.length; i++) {
                rrnames = rrnames + "," + $scope.selectedRows[i].RR_NAME;
            }
            if (rrnames.charAt(0) === ',')
                rrnames = rrnames.slice(1);
            var loccode = _.find($scope.ipLocation, { ticked: true });
            $scope.SaveBooking.RoomName = rrnames;
            $scope.SaveBooking.LocationName = loccode.LCM_NAME;
            $scope.SaveBooking.FacilityName = facilityName.RF_NAME;

        });
    }

    $scope.SaveBooking = {};
    $scope.SaveBookingObj = {};
    var title = "";
    function ShowEventPopup(from, to) {
        $('#bookingdetails').show();
        $scope.$apply(function () {
            $scope.SaveBooking.BM_FROM_DATE = $.fullCalendar.moment(from).format('MM/DD/YYYY');
            // $scope.SaveBooking.BM_TO_DATE = $.fullCalendar.moment(to).subtract(1, 'days').format('MM/DD/YYYY');
            $scope.SaveBooking.BM_TO_DATE = $.fullCalendar.moment(from).format('MM/DD/YYYY');
            $scope.SaveBooking.BM_FROM_TIME = $.fullCalendar.moment(from).format("HH:mm");
            $scope.SaveBooking.BM_TO_TIME = $.fullCalendar.moment(to).subtract(1, 'minutes').format("HH:mm");
        });

    }

    $scope.countrySelected = function (selected) {
        if (selected) {
            $scope.selectedEmployee = selected.originalObject;
            $scope.SaveBooking.BM_RESERVED_FOR = $scope.selectedEmployee.AUR_ID;
        } else {
            
        }
    };
    $scope.clearData = function (form_) {
        $('#bookingdetails').hide();
        $scope.SaveBookingObj.bookinghead = "";
    }

  

    $scope.UpdateBookedRequest = function (RB_STA_ID) {
        $scope.SaveBooking.BM_STA_ID = RB_STA_ID;
        $scope.SaveBooking.BM_REQ_ID = $scope.ViewAndModify.BM_REQ_ID;
        //$scope.SaveBooking.BM_STA_ID = 6;

        $scope.VMDetailslst = [];

        angular.forEach($scope.selectedRows, function (value, key) {
            var wdobj = {};
            wdobj.BD_RT_SNO = value.RR_RT_SNO;
            wdobj.BD_RF_SNO = value.RR_RF_SNO;
            wdobj.BD_RR_SNO = value.RR_SNO;
            $scope.VMDetailslst.push(wdobj);
        });

        var loccode = _.find($scope.ipLocation, { ticked: true });
        var ctycode = _.find($scope.ipCity, { ticked: true });
        var cnycode = _.find($scope.ipCountry, { ticked: true });
        $scope.SaveBooking.BM_LCM_CODE = loccode.LCM_CODE;
        $scope.SaveBooking.BM_CTY_CODE = ctycode.CTY_CODE;
        $scope.SaveBooking.BM_CNY_CODE = cnycode.CNY_CODE;

        var ReqObj = { obj: $scope.SaveBooking, objList: $scope.VMDetailslst };
        //  var ReqObj = { loclst: $scope.ViewAndModify.OPLocation, rtlist: $scope.Customized.RTlst, abObj: $scope.SaveBooking, ScreenType: 'Adminbooking' };
        
        ViewAndModifyService.UpdateBookedRequest(ReqObj).then(function (response) {
            $scope.ShowMessage = true;
            $scope.Viewstatus = 0;
            $scope.LoadData();
            setTimeout(function () {
                $scope.Success = response.Message;
                showNotification('success', 8, 'bottom-right', $scope.Success);
            }, 1000);
            $scope.SaveBooking = {};
            $("#viewCalendar").modal('hide');
            $scope.selectedRows = [];
        }, function (error) {
            $scope.ShowMessage = true;
            $scope.Success = error.data;
            setTimeout(function () {
                $scope.$apply(function () {
                    showNotification('error', 8, 'bottom-right', $scope.Success);
                    $scope.ShowMessage = false;
                });
            }, 1000);
            console.log(error);
        });
    }


    $scope.chkChanged = function (data) {
        if (data.ticked) {
            $scope.selectedRows.push(data);
        }
        else {
            $scope.selectedRows = _.reject($scope.selectedRows, function (d) {
                return d.RR_SNO == data.RR_SNO;
            });
            //  console.log($scope.selectedRows);
        }
    }
    $scope.onRowSelectedFunc = function (data) {
        
        progress(0, 'Loading...', true);
        $scope.Viewstatus = 1;
        $scope.ViewAndModify.BM_REQ_ID = data.BM_REQ_ID;
        $scope.SaveBooking.LocationName = data.LCM_NAME;
        $scope.SaveBooking.RoomName = data.RR_NAME;
        $scope.SaveBooking.FacilityName = data.RF_NAME;
        ViewAndModifyService.GetRequestDetails(data.BM_REQ_ID).then(function (response) {
            if (response != null) {
                
                $scope.RequestDetailsGrid = response.data;
                if ($scope.RequestDetailsGrid == null) {
                    $scope.GridVisiblityV = true;
                    $scope.gridOptionsV.api.setRowData([]);
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Records Found');
                }
                else {
                    showNotification('', 8, 'bottom-right', '');
                    $scope.GridVisiblityV = true;
                    $scope.gridOptionsV.api.setRowData($scope.RequestDetailsGrid);
                    $scope.Pageload($scope.RequestDetailsGrid);
                    $scope.selectedRows = [];
                    angular.forEach($scope.RequestDetailsGrid, function (key, value) {
                        $scope.selectedRows.push(key);
                    });
                    
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000);

                                        
                    loccode = data.BM_LCM_CODE;
                    $('#calendar').fullCalendar('destroy');
                    $scope.initCal();
                    $("#viewCalendar").modal('show');
                    $('#viewCalendar').on('shown.bs.modal', function () {
                        $("#calendar").fullCalendar('render');
                    });
                    setTimeout(function () {
                        var eventlst = $('#calendar').fullCalendar('clientEvents');
                        $('#bookingdetails').show();
                        $scope.$apply(function () {
                            $scope.CalClick(eventlst[0]);
                        });
                    }, 2000);
                }
            }
        });
    };

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptionsV.rowData, function (value, key) {
                        value.ticked = true;
                        $scope.selectedRows.push(value);
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptionsV.rowData, function (value, key) {

                        $scope.selectedRows = _.reject($scope.selectedRows, function (d) {
                            return d.RR_SNO == value.RR_SNO;
                        });
                        value.ticked = false;
                    });

                });
            }
        });
        return eHeader;
    }

});