﻿
app.service("CheckInAndoutService", function ($http, $q, UtilityService) {

    this.path = window.location.origin;
    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckInAndOut/GetChkInOutDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.GetDetails = function (reqid) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckInAndOut/GetDetailsByReqId?reqId=' + reqid + '')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.SubmitCheckinoutData = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckInAndOut/SubmitCheckinoutData', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('CheckInAndoutController', function ($scope, $q, $http, CheckInAndoutService, UtilityService, $timeout, $filter) {
    $scope.ChkInOut = {};
    $scope.Viewstatus = 0;
    $scope.GridVisiblity = true;
    
    $scope.columnDefs = [
      { headerName: "Requisition Id", field: "RB_REQ_ID", width: 165, cellClass: 'grid-align', filter: 'text', template: '<a ng-click="onRowSelectedFunc(data)">{{data.RB_REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
      { headerName: "Reference Id", field: "RB_REFERENCE_ID", cellClass: 'grid-align', width: 180 },
      { headerName: "Facility Type", field: "RT_NAME", width: 150, cellClass: 'grid-align',  },
      { headerName: "Facility Name", field: "RF_NAME", width: 150, cellClass: 'grid-align',  },
      { headerName: "Room Number/Name", field: "RR_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 100 },
      { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "From Date", field: "RB_FROM_DATE", template: '<span>{{data.RB_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "To Date", field: "RB_TO_DATE", template: '<span>{{data.RB_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "From Time", field: "RB_FRM_TIME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "To Time", field: "RB_TO_TIME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "Reserved By", field: "RESERVED_BY", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved By Email", field: "RESERVED_BY_EMAIL", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved For", field: "RESERVED_FOR", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved For Email", field: "RESERVED_FOR_EMAIL", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved Date", field: "RESERVED_DT", template: '<span>{{data.RESERVED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, },
       { headerName: "Action", field: "CHKSTATUS", width: 165, cellClass: 'grid-align', filter: 'text', template: '<a ng-click="onRowSelUpdateStatus(data)">{{data.CHKSTATUS}}</a>', pinned: 'right', suppressMenu: true },
    ];
    
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.setRowData()
        },
    };

    $scope.back = function () {
        $scope.Viewstatus = 0;
    }
    $scope.LoadData = function () {
        CheckInAndoutService.GetGriddata().then(function (data) {
            
            $scope.gridata = data.data;           
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 200);
            }
            else {
                $scope.gridOptions.api.setRowData($scope.gridata);


                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
        });
    }

   // setTimeout(function () {
        $scope.LoadData();
  //  }, 1000);

    setTimeout(function () { progress(0, 'Loading...', true); }, 200);

    $scope.onRowSelUpdateStatus = function (data) {
        console.log(data);
        $scope.SaveCheckInOut = {};
        $scope.ViewReqData = {};
        $scope.ViewReqData = data;
        $scope.SaveCheckInOut.RB_REQ_ID = data.RB_REQ_ID;

     


        if (data.CHKSTATUS == "CHECK OUT") {
            $scope.SaveCheckInOut.checkinout = data.RB_TO_DATE;
            $scope.SaveCheckInOut.Time = data.RB_TO_TIME;
            $scope.SubmitCheckinoutData('CHECKOUT');
            setTimeout(function () {
                $scope.LoadData();
            }, 1000);
        }
        else if (data.CHKSTATUS == "CHECK IN") {
            $scope.SaveCheckInOut.checkinout = data.RB_FROM_DATE;
            $scope.SaveCheckInOut.Time = data.RB_FRM_TIME;
            $scope.SubmitCheckinoutData('CHECKIN');
            setTimeout(function () {
                $scope.LoadData();
            }, 1000);
        }
        else {
            console.log("Completed");
        }
    };

    $scope.onRowSelectedFunc = function (data) {
        $scope.enableInButton = 1;
        $scope.enableOutButton = 1;

        CheckInAndoutService.GetDetails(data.RB_REQ_ID).then(function (data) {
            console.log(data.Table);
            $scope.ChkIn_Details = data.Table[0];
            $scope.ChkOut_Details = data.Table[1];

        });

        console.log(data);
        if (data.CHKSTATUS == "Completed") {
            $scope.enableInButton = 0;
            $scope.enableOutButton = 0;
        }
        else if (data.CHKSTATUS == "CHECK IN") {
            $scope.enableOutButton = 0;
        
        }
        else {
            $scope.enableInButton = 0;
        }
        $scope.SaveCheckInOut = {};
        $scope.Viewstatus = 1;
        
        $scope.ViewReqData = {};
        $scope.ViewReqData = data;
        $scope.SaveCheckInOut.RB_REQ_ID = data.RB_REQ_ID;
        //var d = new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY'));
        //console.log(d);
        //$scope.SaveCheckInOut.checkinout = d;
        
    };

    $scope.SubmitCheckinoutData = function (type) {
        $scope.SaveCheckInOut.Type = type;
        var params = { reqlcm: $scope.SaveCheckInOut }
        console.log(params);
        CheckInAndoutService.SubmitCheckinoutData($scope.SaveCheckInOut).then(function (data) {
            console.log(data);
         
            $scope.Viewstatus = 0;

            setTimeout(function () {
                $scope.LoadData();
            }, 1000);

            setTimeout(function () { showNotification('success', 8, 'bottom-right', data.Message); }, 1000);

        });
    };
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

});