﻿<%@ Page Language="C#"  %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .timepicker {z-index: 1151 !important;}

        .grid-align {
            text-align: center;
        }

            .calmodaldiv {
        width: 800px;
        height: 550px;
        overflow: auto;
    }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="ViewAndModifyWithholdController" onload="setDateVals()" class="amantra">
    <%--<input type="hidden" name="RB_REQ_ID" ng-model="ViewAndModify.RB_REQ_ID">--%>
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View & Modify Withhold Booking</legend>
                    </fieldset>
                    <div class="well">
                        <div data-ng-show="Viewstatus==0">
                            <form role="form" id="CheckInOut" name="CheckInOut" novalidate>
                                <div class="row" style="padding-left: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </form>
                        </div>
                        <div data-ng-show="Viewstatus==1">
                            <form id="frmAdminBooking" name="frmAdminBooking" data-valid-submit="LoadDataV(2,'a')" novalidate>

                                  <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Requisition ID :</b> </label>
                                            {{ViewAndModifyWithhold.WM_REQ_ID}}
                                        </div>
                                    </div>
                                      </div>

                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.CNY_NAME.$invalid}">
                                        <label>Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="2" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.CTY_NAME.$invalid}">

                                        <label>City<span style="color: red;">**</span></label>

                                        <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.LCM_NAME.$invalid}">

                                        <label>Location <span style="color: red;">**</span></label>

                                        <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.RT_NAME.$invalid}">
                                        <div class="row">
                                            <label>Facility Type<span style="color: red;">*</span></label>

                                            <div isteven-multi-select data-input-model="RTlst" data-output-model="Customized.RTlst" button-label="icon RT_NAME" item-label="RT_NAME maker" tick-property="ticked" 
                                                  data-on-item-click="getFacilityNamesbyType()" data-on-select-all="FTSelectAll()" data-on-select-none="FTSelectNone()" 
                                                data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="Customized.RTlst[0]" name="RT_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.RT_NAME.$invalid" style="color: red">Please Select Facility Type </span>

                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.RF_NAME.$invalid}">
                                        
                                            <label>Facility Name<span style="color: red;">**</span></label>

                                            <div isteven-multi-select data-input-model="RFlst" data-output-model="Customized.RFlst" button-label="icon RF_NAME" item-label="RF_NAME maker" tick-property="ticked" 
                                               data-on-item-click="FNChange()" data-on-select-all="FNSelectAll()" data-on-select-none="FNSelectNone()"  
                                                data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="Customized.RFlst[0]" name="RF_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.RF_NAME.$invalid" style="color: red">Please Select Facility Name </span>

                                        
                                    </div>
                                </div>

                               
                                
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    </div>

                            </div>

                             <div class="clearfix">
                                    <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                                        <button type="submit" id="btnchkin" class="btn btn-primary custom-button-color">Search</button>
                                        <%--<button type="submit" id="btnchkout" class="btn btn-primary custom-button-color">Check Out</button>--%>
                                        <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                                    </div>
                              </div>
                          
                        </form>
                        <br />
                        
                            <div data-ng-show="GridVisiblityV">
                               <div class="row" style="padding-left: 30px">
                               <%--     <input type="text" class="selectpicker" id="Text1" placeholder="Filter by any..." style="width: 25%" />--%>
                                    <div data-ag-grid="gridOptionsV" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-12 text-right">
                                        <input type="button" value="View Calendar" ng-click="viewCalendar(selectedRows)" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>

                        
                        </div>

                       <div class="modal fade bs-example-modal-lg col-md-12 " id="viewCalendar">
                            <div class="modal-dialog modal-lg" style="width: 850px; height: auto">
                                <div class="modal-content calmodaldiv">
                                    <div class="modal-header">
                                        <div class="panel-group box box-primary" id="Div2">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div class="panel-heading ">
                                                <h4 class="panel-title modal-header-primary" style="padding-left: 17px" data-target="#collapseTwo"></h4>

                                                <label style="color: blue">{{SaveBookingObj.bookinghead}}</label>

                                              

                                                <br />
                                                <div class="row">
                                                    <div class="clearfix">
                                                        <div id="bookingdetails">
                                                            <form id="frmSubmitBooking" name="frmSubmitBooking"   novalidate>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">

                                                                        <div class="radio">
                                                                            <label>
                                                                                <input ng-model="SaveBooking.WM_TYPE" type="radio" name="rdoType" ng-value="1">
                                                                                Personal
                                                                            </label>
                                                                            <label>
                                                                                <input ng-model="SaveBooking.WM_TYPE" type="radio" name="rdoType" ng-value="2">
                                                                                Official
                                                                            </label>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                             <%--   <div class="clearfix">

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <label>Country</label>
                                                                        <input type="text" ng-model="SaveBooking.BookedCountry" id="Text7" readonly><br />
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <label>City &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;</label>
                                                                        <input type="text" ng-model="SaveBooking.BookedCity" id="Text8" readonly><br />
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <label>Location</label>
                                                                        <input type="text" ng-model="SaveBooking.BookedLocation" id="Text9" readonly><br />
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <label>Room Number/Name</label>
                                                                        <input type="text" ng-model="SaveBooking.BookedReservationName" id="Text10" readonly><br />
                                                                    </div>

                                                                </div>--%>

                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                    </div>
                                                                    <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.type.$invalid}">
                                                                            <label>
                                                                                <input type="radio" ng-model="bookingType.Type" name="type" value="1">
                                                                                Personal
                                                                            </label>

                                                                            <label>
                                                                                <input type="radio" ng-model="bookingType" name="type" value="2">
                                                                                Official
                                                                            </label>
                                                                        </div>
                                                                    </div>--%>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                    </div>
                                                                </div>
                                                                   <div class="clearfix">
                                                                     <div class="col-md-4 col-sm-6 col-xs-12">
                                                                        <label> <b>Location</b></label> <br />
                                                                        {{SaveBooking.LocationName}}
                                                                    </div>
                                                                      <div class="col-md-4 col-sm-4 col-xs-12">
                                                                        <label> <b>Facility Name</b></label> <br />
                                                                        {{SaveBooking.FacilityName}}
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                                                        <label><b>Room Names</b></label> <br />
                                                                        {{SaveBooking.RoomName}}
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.TITLE.$invalid}">
                                                                            <label>Booking Title <span style="color: red;">*</span></label>

                                                                            <input type="text" class="form-control form-control-small" data-ng-model="SaveBooking.WM_TITLE" name="TITLE" required="" />
                                                                            <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.TITLE.$invalid" style="color: red">Please enter Booking Title</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>From Date</label>
                                                                            <div class="input-group date" id='fromdatebooking'>
                                                                                <input type="text" class="form-control" data-ng-model="SaveBooking.WM_FROM_DATE" id="Text3" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-calendar" onclick="setup('fromdatebooking')"></i>
                                                                                </span>
                                                                            </div>
                                                                            <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                                                        </div>
                                                                    </div>



                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>To Date</label>
                                                                            <div class="input-group date" id='todatebooking'>
                                                                                <input type="text" class="form-control" data-ng-model="SaveBooking.WM_TO_DATE" id="Text4" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                                                <span class="input-group-addon">
                                                                                    <i class="fa fa-calendar" onclick="setup('todatebooking')"></i>
                                                                                </span>
                                                                            </div>
                                                                            <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.FromTime.$invalid}">
                                                                            <div class="bootstrap-timepicker">
                                                                                <div class="form-group">
                                                                                    <label>From Time</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" id="Text5" name="FromTime" class="form-control timepicker" data-ng-model="SaveBooking.WM_FROM_TIME"
                                                                                            required="" />
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-clock-o"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="clearfix">

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.ToTime.$invalid}">
                                                                            <div class="bootstrap-timepicker">
                                                                                <div class="form-group">
                                                                                    <label>To Time</label>
                                                                                    <div class="input-group">
                                                                                        <input type="text" id="Text6" name="ToTime" class="form-control timepicker" data-ng-model="SaveBooking.WM_TO_TIME"
                                                                                            required="" />
                                                                                        <div class="input-group-addon">
                                                                                            <i class="fa fa-clock-o"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <label>Reference ID</label>
                                                                        <input type="text" ng-model="SaveBooking.WM_REFRERENCE_ID" id="refID"><br />
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.ToWhom.$invalid}">
                                                                            <label>To Whom</label>
                                                                             <div  angucomplete-alt 
                                                                        id="ex7"
                                                                        placeholder="Search Employee"
                                                                        pause="500"
                                                                        selected-object="selectedEmp"
                                                                        remote-url="../../../api/Utility/GetEmployeeNameAndID"
                                                                        remote-url-request-formatter="remoteUrlRequestFn"
                                                                        remote-url-data-field="items"
                                                                        title-field="NAME"
                                                                        minlength="2"
                                                                        input-class="form-control form-control-small"
                                                                        match-class="highlight">
                                                                    </div>

                                                                            <input type="text" id="txttowhom" data-ng-model="SaveBooking.WM_RESERVED_FOR" name="ToWhom" style="display: none" />
                                                                            <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.ToWhom.$invalid" style="color: red">Please enter To Whom</span>
                                                                        </div>
                                                                    </div>



                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <label>Remarks</label>
                                                                        <textarea type="text" data-ng-model="SaveBooking.WM_REMARKS" id="eventRemarks"></textarea><br />
                                                                    </div>
                                                                </div>


                                                                <br />
                                                                <div class="clearfix">
                                                                    <div class="box-footer text-right">
                                                                        <input type="submit" value="Modify Withhold" ng-click="frmSubmitBooking.$valid && UpdateBookedRequest(6)" class="btn btn-primary custom-button-color" />
                                                                        <input type="submit" value="Cancel Withhold" ng-click="frmSubmitBooking.$valid && UpdateBookedRequest(1)" class="btn btn-primary custom-button-color" />
                                                                        <input type="button" ng-click="clearData('frmSubmitBooking')" value="Close" class="btn btn-primary custom-button-color" />
                                                                    </div>
                                                                </div>

                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>

                                                <br />
                                                <br />

                                                <div class="row">
                                                    <div class="clearfix">
                                                            <div id='calendar' style="width: 85%"></div>
                                                      </div>
                                                </div>




                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                       <div id="popupEventForm" class="modal fade bs-example-modal-lg col-md-12 ">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="H3">Details</h4>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js"></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script src="../../../Scripts/bootstrap-timepicker.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt"]);

        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY')));
            $(".timepicker").timepicker({
                showInputs: false,
                showMeridian: false,
                // template: 'modal'
            });
        }

    </script>

    <script src="../../../SMViews/Utility.js"></script>
    <script src="../Js/AdminBooking.js"></script>
    <script src="../Js/ViewAndModify_Withhold.js"></script>
     <%--<script src="../../../BootStrapCSS/Scripts/jquery-ui.min.js"></script>--%>
    <script src="../../../BootStrapCSS/qtip/qtip.js"></script>
</body>
</html>

