﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="CheckInAndoutController" onload="setDateVals()" class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Check In/Out</legend>
                    </fieldset>
                    <div class="well">
                        <div data-ng-show="Viewstatus==0">
                            <form role="form" id="CheckInOut" name="CheckInOut" novalidate>
                                <div class="row" style="padding-left: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </form>
                        </div>
                        <div data-ng-show="Viewstatus==1">
                            <form role="form" id="ChkInOut" name="ChkInOut" novalidate>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Requisition ID</b></label>
                                            <div>{{ViewReqData.RB_REQ_ID}}</div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Reserved By </b> </label>
                                          <div>  {{ViewReqData.RESERVED_BY}}</div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Reserved by Email </b> </label>
                                           <div> {{ViewReqData.RESERVED_BY_EMAIL}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Reserved Date </b> </label>
                                           <div> {{ViewReqData.RESERVED_DT| date: dateformat}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Facility Type </b> </label>
                                            <div>{{ViewReqData.RT_NAME}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Facility Name </b> </label>
                                          <div>  {{ViewReqData.RF_NAME}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Room Number/Name </b> </label>
                                          <div>  {{ViewReqData.RR_NAME}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>City </b> </label>
                                         <div>   {{ViewReqData.CTY_NAME}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Location </b> </label>
                                          <div>  {{ViewReqData.LCM_NAME}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>From Date </b> </label>
                                          <div>  {{ViewReqData.RB_FROM_DATE | date: dateformat}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>To Date </b> </label>
                                          <div>  {{ViewReqData.RB_TO_DATE | date: dateformat}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>From Time </b> </label>
                                            <div>{{ViewReqData.RB_FRM_TIME}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>To Time </b> </label>
                                            <div>{{ViewReqData.RB_TO_TIME}}</div>
                                        </div>
                                    </div>

                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Check In Date</b> </label>
                                            <div>{{ChkIn_Details.RBC_CHK_IN_DATE}}</div>
                                        </div>
                                    </div>

                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Check In Time </b> </label>
                                            <div>{{ChkIn_Details.RBC_CHK_IN_TIME}}</div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Check Out Date</b> </label>
                                            <div>{{ChkOut_Details.RBC_CHK_OUT_DATE}}</div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix">
                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Check Out Time </b> </label>
                                            <div>{{ChkOut_Details.RBC_CHK_OUT_TIME}}</div>
                                        </div>
                                    </div>

                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Check In Remarks </b> </label>
                                            <div>{{ChkIn_Details.RBC_CHKIN_REMARKS}}</div>
                                        </div>
                                    </div>

                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Check Out Remarks </b> </label>
                                            <div>{{ChkOut_Details.RBC_CHKOUT_REMARKS}}</div>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix">


                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': ChkInOut.$submitted && ChkInOut.checkinout.$invalid}">
                                            <div class="form-group">

                                                <label>Check In/Out Date :</label>

                                                <div class="input-group date" id='fromdate'>
                                                    <input type="text" class="form-control" data-ng-model="SaveCheckInOut.checkinout" required id="Text1" name="checkinout" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': ChkInOut.$submitted && ChkInOut.ToTime.$invalid}">
                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label>Check In/Out Time</label>
                                                    <div class="input-group">
                                                        <input type="text" id="Text6" name="ToTime" class="form-control timepicker" data-ng-model="SaveCheckInOut.Time"
                                                            required="" />
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">

                                            <label for="txtcode"><b>Remarks </b></label>
                                            <textarea class="form-control input-sm" rows="3" data-ng-model="SaveCheckInOut.REMARKS" name="REMARKS"></textarea>

                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                                        <button type="submit" id="btnchkin" class="btn btn-primary custom-button-color" ng-click="ChkInOut.$valid && SubmitCheckinoutData('CHECKIN')" ng-disabled="enableInButton==0">Check In</button>
                                        <button type="submit" id="btnchkout" class="btn btn-primary custom-button-color" ng-click="ChkInOut.$valid && SubmitCheckinoutData('CHECKOUT')" ng-disabled="enableOutButton==0">Check Out</button>
                                        <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js"></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../Scripts/bootstrap-timepicker.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        function setDateVals() {
            $('#Text1').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#Text1').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY')));

            $(".timepicker").timepicker({
                showInputs: false,
                showMeridian: false
            });
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });
    </script>
    <script src="../../../SMViews/Utility.js"></script>
    <script src="../Js/CheckInAndOut.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>

</body>
</html>
