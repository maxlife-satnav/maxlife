<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="SetTenantReminders.aspx.vb" Inherits="Masters_Configuration_SetTenantReminders"
    Title="Set Tenant Reminders" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=tenantReminderChkbox.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            var count = 0;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    count++;
                    if (count > 3) {
                        count = 0;
                        isValid = false;
                        break;
                    }
                }
            }
            args.IsValid = isValid;
        }
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Set Tenant Reminders 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ShowSummary="true" ForeColor="Red" ValidationGroup="Val1" />

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please Select Location"
                                            ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Location" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Property Type<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please Select Property Type"
                                            ControlToValidate="ddlproptype" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Property Type" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Property Name<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Please Select Property Name"
                                            ControlToValidate="ddlpropname" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlpropname" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Proprty Name" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div runat="server" id="tendetails" visible="false">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Tenant Name</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="lbltenantname" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">
                                                Tenant Code</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="lbltenantcode" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="tenrentdetails" visible="false">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Payment Term</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="lblpaymenttype" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Rent Amount</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="lblrent" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div runat="server" id="chkboxRow" visible="false">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Set Tenant Reminders for<span style="color: red;">*</span> </label>
                                            <asp:CustomValidator ID="CustomValidator2" ErrorMessage="Please select at least 1 Reminders & atmost 3 Reminders."
                                                ClientValidationFunction="ValidateCheckBoxList" runat="server" Display="None" ValidationGroup="Val1" />
                                            <div class="col-md-7">
                                                <asp:CheckBoxList runat="server" ID="tenantReminderChkbox"
                                                    RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" CssClass="col-md-8 control-label">
                                                    <asp:ListItem Text="10 Days" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="7 Days" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="3 Days" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="1 Day" Value="1"></asp:ListItem>
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Status</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status">
                                                <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" CausesValidation="false"></asp:Button>
                                    <asp:Button ID="btnback" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvitems" TabIndex="9" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="True" EmptyDataText="No Tenant Reminders Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tenant Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltencode" runat="server" Text='<%#Eval("TEN_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpropname" runat="server" Text='<%#Eval("PROP_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("PROP_LOC")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSpaceRelease" CausesValidation="false" CommandArgument='<%#Eval("TEN_CODE")%>'
                                                    CommandName="Edit" runat="server">Edit</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
