Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Masters_Configuration_SetTenantReminders
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocations()
            BindGrid()
        End If
    End Sub

    Protected Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATIONS_BY_PROPERTY_ADMIN")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ALL_TENANT_REMINDERS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"))
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        lblMsg.Text = String.Empty
        BindPropertyTypes(ddlLocation.SelectedItem.Value.ToString())
        chkboxRow.Visible = False
        tendetails.Visible = False
        tenrentdetails.Visible = False
        ddlpropname.Items.Clear()
    End Sub

    Protected Sub ddlproptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproptype.SelectedIndexChanged
        lblMsg.Text = String.Empty
        chkboxRow.Visible = False
        tendetails.Visible = False
        tenrentdetails.Visible = False
        If ddlLocation.SelectedIndex > 0 And ddlproptype.SelectedIndex > 0 Then
            BindPropNames()
        End If
    End Sub

    Protected Sub ddlpropname_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlpropname.SelectedIndexChanged
        lblMsg.Text = String.Empty
        GetTenantDetails()
    End Sub

    Private Sub GetTenantDetails()
        If (ddlLocation.SelectedIndex > 0 And ddlproptype.SelectedIndex > 0 And ddlpropname.SelectedIndex > 0) Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_DETAILS_BY_LOC_PROPERTYDETAILS")
            sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@PROPERTY_TYPE", ddlproptype.SelectedValue.ToString(), DbType.String)
            sp.Command.AddParameter("@PN_NAME", ddlpropname.SelectedValue.ToString(), DbType.String)
            sp.Command.AddParameter("@TEN_CODE", String.Empty, DbType.String)
            sp.Command.AddParameter("@USER", String.Empty, DbType.String)
            sp.Command.AddParameter("@GET_ALL_REC", String.Empty, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count <> 0 Then
                lbltenantname.Text = ds.Tables(0).Rows(0)("TEN_NAME").ToString()
                lbltenantcode.Text = ds.Tables(0).Rows(0)("TEN_CODE").ToString()
                lblpaymenttype.Text = ds.Tables(0).Rows(0)("TEN_PAYMENTTERMS").ToString()
                lblrent.Text = ds.Tables(0).Rows(0)("RENT").ToString()
            End If
            chkboxRow.Visible = True
            tendetails.Visible = True
            tenrentdetails.Visible = True
        Else
            chkboxRow.Visible = False
            tendetails.Visible = False
            tenrentdetails.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Gets property name by location and property type
    ''' </summary>  
    Private Sub BindPropNames()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTYNAMES_BY_LOCID_PROPERTYTYPE")
        sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
        sp.Command.AddParameter("@PROP_TYPE", ddlproptype.SelectedValue.ToString(), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count <> 0 Then
            ddlpropname.DataSource = ds
            ddlpropname.DataTextField = "PN_NAME"
            ddlpropname.DataValueField = "PN_NAME"
            ddlpropname.DataBind()
            ddlpropname.Items.Insert(0, New ListItem("--Select--", "0"))
        Else
            ddlpropname.DataSource = ""
            ddlpropname.DataBind()
            tendetails.Visible = False
            chkboxRow.Visible = False
            tenrentdetails.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' GETS PROPERTY TYPES BASED ON LOCATIONS SET FOR PA.
    ''' </summary>
    ''' <param name="loc_code"></param>
    ''' <remarks></remarks>
    Private Sub BindPropertyTypes(ByVal loc_code As String)
        chkboxRow.Visible = False
        tendetails.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTYTYPES_BY_lOCID")
        sp.Command.AddParameter("@LOC_ID", loc_code, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count <> 0 Then
            ddlproptype.DataSource = ds
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PROPERTY_TYPE"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
            chkboxRow.Visible = True
            tendetails.Visible = True
            tenrentdetails.Visible = True
        Else
            ddlproptype.DataSource = ""
            ddlproptype.DataBind()
        End If
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Cleardata()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        lblMsg.Text = String.Empty
        If Not IsValid Then
            Exit Sub
        End If
        If btnSubmit.Text = "Submit" Then
            lblMsg.Text = ""
            AddReminder()
            BindGrid()
        ElseIf btnSubmit.Text = "Modify" Then
            lblMsg.Text = ""
            ModifyReminder()
            BindGrid()
        End If
        'Cleardata()
    End Sub

    Private Sub AddReminder()
        Try
            Dim days As String = String.Empty
            For Each item As ListItem In Me.tenantReminderChkbox.Items
                If item.Selected Then
                    days += item.Value.ToString() + ","
                End If
            Next
            If ddlLocation.SelectedIndex > 0 Then
                If ddlproptype.SelectedIndex > 0 Then
                    If ddlpropname.SelectedIndex > 0 Then
                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_TENANT_REMINDER")
                        sp.Command.AddParameter("@PROP_TYPE", ddlproptype.SelectedItem.Value.ToString(), DbType.String)
                        sp.Command.AddParameter("@PROP_NAME", ddlpropname.SelectedItem.Value.ToString(), DbType.String)
                        sp.Command.AddParameter("@TEN_CODE", lbltenantcode.Text, DbType.String)
                        sp.Command.AddParameter("@PROP_LOC", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
                        sp.Command.AddParameter("@DAYS", days, DbType.String)
                        sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
                        sp.Command.AddParameter("@STATUS_VAL", ddlStatus.SelectedItem.Value, DbType.String)
                        Dim status As Integer
                        status = sp.ExecuteScalar()
                        If status = 0 Then
                            lblMsg.Text = "Cannot Add, Reminder already exists for the selected Tenant. Please use Edit to modify."
                        Else
                            lblMsg.Text = "Reminder Added Successfully"
                            BindGrid()
                            Cleardata()
                        End If
                    Else
                        lblMsg.Text = "Please Select Property Name"
                    End If

                Else
                    lblMsg.Text = "Please Select Property Type"
                End If

                Else
                    lblMsg.Text = "Please Select Location"
                End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Private Sub ModifyReminder()
        Try
            Dim days As String = String.Empty
            For Each item As ListItem In Me.tenantReminderChkbox.Items
                If item.Selected Then
                    days += item.Value.ToString() + ","
                End If
            Next

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MODIFY_TENANT_REMINDER")
            sp.Command.AddParameter("@PROP_TYPE", ddlproptype.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@PROP_NAME", ddlpropname.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@TEN_CODE", lbltenantcode.Text, DbType.String)
            sp.Command.AddParameter("@PROP_LOC", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@DAYS", days, DbType.String)
            sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@STATUS_VAL", ddlStatus.SelectedItem.Value, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Text = "Reminder Modified Successfully"
            BindGrid()
        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Private Sub Cleardata()
        ddlLocation.SelectedIndex = 0
        ddlproptype.Items.Clear()
        ddlpropname.Items.Clear()
        tenantReminderChkbox.ClearSelection()
        chkboxRow.Visible = False
        tenrentdetails.Visible = False
        tendetails.Visible = False
        btnSubmit.Text = "Submit"
        ddlLocation.Enabled = True
        ddlpropname.Enabled = True
        ddlproptype.Enabled = True
    End Sub


    Protected Sub gvitems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems.RowCommand
        Try
            lblMsg.Text = ""
            tenantReminderChkbox.ClearSelection()
            If e.CommandName = "Edit" Then
                ViewState("TEN_CODE") = e.CommandArgument.ToString()
                btnSubmit.Text = "Modify"

                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_REMINDER_INFO")
                sp.Command.AddParameter("@TEN_CODE", ViewState("TEN_CODE").ToString(), DbType.String)
                Dim ds As DataSet
                ds = sp.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlLocation.SelectedValue = ds.Tables(0).Rows(0)("PROP_LOC").ToString()
                    BindPropertyTypes(ddlLocation.SelectedItem.Value.ToString())
                    ddlproptype.SelectedValue = ds.Tables(0).Rows(0)("PROP_TYPE").ToString()
                    BindPropNames()
                    ddlpropname.SelectedValue = ds.Tables(0).Rows(0)("PROP_NAME").ToString()
                    ddlStatus.SelectedValue = ds.Tables(0).Rows(0)("STATUS_VAL").ToString()
                    GetTenantDetails()
                    ddlLocation.Enabled = False
                    ddlpropname.Enabled = False
                    ddlproptype.Enabled = False

                    For Each item As ListItem In Me.tenantReminderChkbox.Items
                        For Each row As DataRow In ds.Tables(1).Rows
                            If item.Value.ToString() = row.Item(0).ToString() Then
                                item.Selected = True
                            End If
                        Next
                    Next

                End If
            End If
            BindGrid()
        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub gvitems_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/MaintenanceManagement/Masters/Mas_Webfiles/frmPropMasters.aspx")
    End Sub
End Class