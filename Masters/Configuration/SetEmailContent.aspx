<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="SetEmailContent.aspx.vb" Inherits="Masters_Configuration_SetEmailContent"
    Title="Set Email Content" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Set Email Content
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td>
                <img height="27" alt="" src='<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>'
                    width="9" /></td>
            <td class="tableHEADER" align="left" width="100%">
                &nbsp;<strong>Set Email Content</strong>
            </td>
            <td>
                <img height="27" alt="" src='<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>'
                    width="16" /></td>
        </tr>
        <tr>
            <td background='<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>'>
                &nbsp;</td>
            <td align="left">
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td>
                            Mail Subject:</td>
                        <td>
                            <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mail Description:</td>
                        <td>
                            <asp:TextBox ID="txtMailDescription" TextMode="MultiLine" Height="50" Width="250"
                                runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            Mail Body:
                        </td>
                        <td valign="top" align="left">
                            <FTB:FreeTextBox ID="FreeTextBox1" runat="server">
                            </FTB:FreeTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Save" CausesValidation="true">
                            </asp:Button></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
                                AllowPaging="True" Width="100%" PageSize="5" EmptyDataText="Sorry ! No Records" OnRowCommand="gvItems_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Mail Subject">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltenantcode" runat="server" Text='<%#Eval("MAIL_SUBJECT") %>'></asp:Label>
                                            <asp:Label ID="lblMAIL_ID" Visible="false" runat="server" Text='<%#Eval("MAIL_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 10px; height: 100%" background='<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>'>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px">
                <img height="17" alt="" src='<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>'
                    width="9" /></td>
            <td style="height: 17px" background='<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>'>
                <img height="17" alt="" src='<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>'
                    width="25" /></td>
            <td style="width: 17px; height: 17px">
                <img height="17" alt="" src='<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>'
                    width="16" /></td>
        </tr>
    </table>
</asp:Content>
