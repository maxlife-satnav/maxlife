Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Web.Services
Imports System.Collections.Generic
Partial Class Masters_Mas_Webfiles_frmPrjHead
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            GetPrjHeadData()
        End If

    End Sub
    Private Sub GetPrjHeadData()
        Dim dtCC As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[USP_GETPROJHEAD_NAME]")
        gvItem.DataSource = dtCC
        gvItem.DataBind()

    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        If DropDownList1.SelectedItem.Text = "10" Then
            gvItem.PageSize = Convert.ToInt32(DropDownList1.SelectedValue)
        ElseIf DropDownList1.SelectedItem.Text = "20" Then
            gvItem.PageSize = Convert.ToInt32(DropDownList1.SelectedValue)
        End If
        GetPrjHeadData()
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        GetPrjHeadData()
    End Sub
End Class
