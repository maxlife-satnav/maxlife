<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASFloor.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASFloor" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Floor Master</legend>
                    </fieldset>
                    <form class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-md-6">
                                <label class="col-md-2 btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true" />
                                    Add
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true" />
                                    Modify
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trLName" runat="server">
                                        <label class="col-md-4 control-label">Select Floor<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFloor"
                                            Display="None" ErrorMessage="Please Select Floor " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlFloor" runat="server" AutoPostBack="True" ToolTip="Select the Floor" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Floor Code<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFloorcode"
                                            Display="None" ErrorMessage="Please Enter Floor Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtFloorcode" MaxLength="15" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Floor Name<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="None"
                                            ErrorMessage="Please Enter Floor Name in alphanumerics and (space,-,_ ,(,),, allowed)"
                                            ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ControlToValidate="txtFloorName" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtFloorName"
                                            Display="None" ErrorMessage="Please Enter Floor Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtFloorName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Tower Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTower" runat="server" ControlToValidate="ddlTower"
                                            Display="None" ErrorMessage="Please Select Tower Name " InitialValue="--Select--" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlTower" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Location Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location Name "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">City Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server"
                                            ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City Name "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">State Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlState"
                                            Display="None" ErrorMessage="Please Select State Name " InitialValue="--Select--" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Zone Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlZone"
                                            Display="None" ErrorMessage="Please Select Zone Name " InitialValue="--Select--" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlZone" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Country Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                            Display="None" ErrorMessage="Please Select Country Name " InitialValue="--Select--" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" Width="76px" Text="Submit" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Floor Found."
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:BoundField DataField="FLR_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Floor Name">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="twr_name" ItemStyle-HorizontalAlign="Left" HeaderText="Tower Name">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                         <asp:BoundField DataField="LCM_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Location Name">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CTY_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="City Name">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:ButtonField HeaderText="Status" ItemStyle-HorizontalAlign="Left" CommandName="Status" Text="Button">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="FLR_STA_ID" ItemStyle-HorizontalAlign="Left" HeaderText="STID">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FLR_CODE" ItemStyle-HorizontalAlign="Left" HeaderText="Floor Code">
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblTwrID" Text='<% #Bind("flr_twr_id")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
