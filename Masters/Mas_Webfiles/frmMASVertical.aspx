<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASVertical.aspx.vb" Inherits="Masters_Mas_Webfiles_frm" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>
                                <asp:Label ID="lblHeader" runat="server" />
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Upload Document  (Only Excel) <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                                                ControlToValidate="fpBrowseDoc" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                                                ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                                                ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                                            </asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <div class="btn btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5  control-label">
                                                <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="upload" CausesValidation="true" ValidationGroup="Val2" />
                                            </div>
                                            <div class="col-md-7">
                                                <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/BusinessUnitMaster.xls"></asp:HyperLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-md-6">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Vertical and Select Modify to modify the existing Vertical" />
                                        Add
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Vertical and Select Modify to modify the existing Vertical" />
                                        Modify
                                    </label>
                                </div>
                            </div>

                            <div class="row" id="trVerName" runat="server">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblLocationname" class="col-md-5 control-label" runat="server"><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvvertical1" runat="server" ControlToValidate="ddlVertical"
                                                Display="None" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlVertical" runat="server" AutoPostBack="True"
                                                    CssClass="selectpicker" data-live-search="true" TabIndex="1">
                                                    <asp:ListItem Value="1">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblverticalCode" class="col-md-5 control-label" runat="server"><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvVerCode" runat="server" ControlToValidate="txtVerticalCode"
                                                Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtVerticalCode" runat="server" CssClass="form-control" TabIndex="2" MaxLength="15"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblverticalName" class="col-md-5 control-label" runat="server"><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvVeName" runat="server" ControlToValidate="txtVerticalName"
                                                Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtVerticalName" runat="server" CssClass="form-control"
                                                        TabIndex="3" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Parent Entity<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvparent" runat="server" ControlToValidate="ddlParent"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Parent Entity"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlParent" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"
                                                    TabIndex="3">
                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Child Entity<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvchild" runat="server" ControlToValidate="ddlChild"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Child Entity"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlChild" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="3">
                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="maxLength"
                                                    ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters " ValidationGroup="Val1"></asp:CustomValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"
                                                        TextMode="MultiLine" MaxLength="200" TabIndex="6"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" TabIndex="7"></asp:Button>
                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" TabIndex="8"></asp:Button>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Vertical Found."
                                        CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="20">
                                        <Columns>
                                           <asp:BoundField DataField="VER_CODE" HeaderText="Vertica" ItemStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="VER_NAME" HeaderText="Vertical Name" ItemStyle-HorizontalAlign="Left" />
                                             
                                            <asp:BoundField DataField="PE_NAME" HeaderText="Parent Entity Name" ItemStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="CHE_NAME" HeaderText="Child Entity Name" ItemStyle-HorizontalAlign="Left" />
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkStatus" CausesValidation="false" runat="Server" Text='<% # Bind("Status")%>'
                                                        OnClick="lnkStatus_Click" CommandArgument='<% #Bind("VER_CODE")%>' AccessKey='<% #Bind("VER_STA_ID")%>'></asp:LinkButton><asp:Label Visible="false" runat="server" ID="lblVerticalID" Text='<% # Bind("VER_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        function ShowHelpWindow() {
            window.open('frmPrjHead.aspx', 'Window', 'toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450');
            return false;
        }
    </script>
</body>
</html>
