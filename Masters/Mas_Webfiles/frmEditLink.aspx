<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmEditLink.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmLinkMaster" title="Edit Request" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
<ContentTemplate>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Edit Request 
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Edit Request </strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="height: 158px">
                        &nbsp;</td>
                    <td align="left" style="height: 158px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                            <tr id="trLname" runat="server">
                                <td align="left"  style="height: 11px; width: 50%;" colspan="2">
                                   &nbsp;Select Request
                                    <span style="font-size: 8pt; color: #ff0000">*</span>
                                    <asp:RequiredFieldValidator ID="rfvlink" runat="server" InitialValue="--Select--" Display="none"
                                        ControlToValidate="ddlLinkName" ErrorMessage="Please select Request" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 43%; height: 11px" colspan="2">
                                    <asp:DropDownList ID="ddlLinkName" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True"
                                      >
                                    </asp:DropDownList></td>
                            </tr>
                            
                            
                             <tr >
                                <td align="left"  style="height: 11px; width: 50%;" colspan="2">
                                 &nbsp;Parent ID
                                    <span style="font-size: 8pt; color: #ff0000">*</span>
                                    <asp:RequiredFieldValidator ID="rfvParentLink" runat="server"  Display="none"
                                        ControlToValidate="ddlparentlink" ErrorMessage="Please select Link" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 43%; height: 11px" colspan="2">
                                    <asp:DropDownList ID="ddlparentlink" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True"
                                      >
                                    </asp:DropDownList></td>
                            </tr>

                             <tr>
                                <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                    &nbsp;Request Name  <font class="clsNote">*<asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Request Name " ControlToValidate="txtreqname"
                                        Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                       <%-- <asp:RegularExpressionValidator ID="revLinkName" runat="server" ControlToValidate="txtLinkName"
                                            Display="None" ErrorMessage="Please Enter Valid Request Name" ValidationExpression="^[0-9a-zA-Z ]+"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>--%>

                                                        </font></td>
                                <td align="left" style="width: 43%; height: 26px;" colspan="2">
                                   
                                        <asp:TextBox ID="txtreqname" MaxLength="50" runat="server" Width="97%" CssClass="clsTextField"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                    &nbsp;Request Description  <font class="clsNote">*<asp:RequiredFieldValidator
                                        ID="rfvCode" runat="server" ErrorMessage="Please Enter Request Description " ControlToValidate="txtLinkName"
                                        Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                       <%-- <asp:RegularExpressionValidator ID="revLinkName" runat="server" ControlToValidate="txtLinkName"
                                            Display="None" ErrorMessage="Please Enter Valid Request Name" ValidationExpression="^[0-9a-zA-Z ]+"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>--%>

                                                        </font></td>
                                <td align="left" style="width: 43%; height: 26px;" colspan="2">
                                  
                                        <asp:TextBox ID="txtLinkName" MaxLength="50" runat="server" Width="97%" CssClass="clsTextField"></asp:TextBox>
                                </td>
                            </tr>
                            
                           
                           
                            <tr >
                                <td align="left"  style="height: 11px; width: 50%;" colspan="2">
                                &nbsp;Select Status<font class="clsNote">*</font> 
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="--Select--" Display="none"
                                        ControlToValidate="ddlLinkName" ErrorMessage="Please select Status" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 43%; height: 11px" colspan="2">
                                    <asp:DropDownList ID="ddlstatus" runat="server" Width="97%" CssClass="clsComboBox"
                                        ToolTip="Select Status ">
                                        <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="In-Active"></asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                              <tr id="trTimeSlot" runat="server" >
                                <td align="left"  style="height: 11px; width: 25%;">From:<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr"
                                        Display="None" ErrorMessage="Please Enter From Time " InitialValue="Hr" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>

                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimemin"
                                        Display="None" ErrorMessage="Please Enter From Minutes" InitialValue="Min" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>

                                </td>
                                <td align="left"  style="height: 11px; width: 25%;">
                                    <asp:DropDownList ID="starttimehr" runat="server" Width="60">
                                        <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="starttimemin" runat="server" Width="60">
                                        <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                        
                                    </asp:DropDownList>
                                    
                                </td>

                                <td align="left"  style="height: 11px; width: 25%;">To:<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr"
                                        Display="None" ErrorMessage="Please Enter To Time " InitialValue="Hr" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>

                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimemin"
                                        Display="None" ErrorMessage="Please Enter To Minutes" InitialValue="Min" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator></td>
                                <td align="left"  style="height: 11px; width: 25%;">
                                    <asp:DropDownList ID="endtimehr" runat="server" Width="60">
                                        <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="endtimemin" runat="server" Width="60">
                                        <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                      
                                    </asp:DropDownList>
                                    
                                </td>
                            </tr>
                        </table>
                        <table class="table" style="height: 22px" width="100%" border="1">
                            <tr>
                                <td align="center" style="height: 20px">
                                    &nbsp;
                                    <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit"
                                        ValidationGroup="Val1"></asp:Button>&nbsp;
                               </td>
                            </tr>
                        </table>
                            <asp:Panel ID="panelgrid" runat="server" Width="100%">
<asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    Width="100%" PageSize="10" EmptyDataText=" No Records Found">
                                    <Columns>
                                        <asp:TemplateField Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsno" runat="server" CssClass="lblsno" Text='<%#Eval("idcategory")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqType" runat="server" CssClass="lblReqType" Text='<%#Eval("categoryname")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Parent Category">
                                            <ItemTemplate>
                                                <asp:Label ID="lblparent" runat="server" CssClass="lblparent" Text='<%#Eval("idparentcategory")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="FromTime">
                                            <ItemTemplate>
                                                <asp:Label ID="lblfromtime" runat="server" CssClass="lblReqType" Text='<%#Eval("FROM_TIME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="ToTime">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltotime" runat="server" CssClass="lblparent" Text='<%#Eval("TO_TIME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                        </asp:Panel>


                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 158px;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </ContentTemplate>

    

</asp:UpdatePanel>
</asp:Content>

