<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmMASMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASMasters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
    <style>
        .btn {
            border-radius: 4px;
            background-color : #3A618F;
        }
    </style>
</head>
<body>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <div id="page-wrapper" class="row">
        <div class="row ng-scope">
            <div class="box box-primary">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Primary Masters</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="CountryHLdiv" runat="server">
                                    <asp:HyperLink ID="CountryHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCountry.aspx">Country Master</asp:HyperLink>
                                </div>
                                  <div class="col-md-4 col-sm-12 col-xs-12" id="ZoneHLdiv" runat="server">
                                    <asp:HyperLink ID="ZoneHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/Zone.aspx">Zone Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="StateHLdiv" runat="server">
                                    <asp:HyperLink ID="StateHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/StateMaster.aspx"> State Master</asp:HyperLink>
                                </div>
                               
                            </div>
                            <br />
                            <div class="clearfix">
                                 <div class="col-md-4 col-sm-12 col-xs-12" id="CityHLdiv" runat="server">
                                    <asp:HyperLink ID="CityHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCity.aspx">City Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="LocationHLdiv" runat="server">
                                    <asp:HyperLink ID="LocationHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASLocation.aspx"> Location Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="TowerHLdiv" runat="server">
                                    <asp:HyperLink ID="TowerHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASTower.aspx">Tower Master</asp:HyperLink>
                                </div>
                               
                            </div>
                            <br />
                            <div class="clearfix">
                                 <div class="col-md-4 col-sm-12 col-xs-12" id="FloorHLdiv" runat="server">
                                    <asp:HyperLink ID="FloorHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFloor.aspx">Floor Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="DesignatioHLdiv" runat="server">
                                    <asp:HyperLink ID="DesignatioHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASDesignition.aspx">Designation Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="DepartmentHLdiv" runat="server">
                                    <asp:HyperLink ID="DepartmentHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASDepartment.aspx">Department Master</asp:HyperLink>
                                </div>
                               
                            </div>
                            <br />
                              <div class="clearfix">
                                   <div class="col-md-4 col-sm-12 col-xs-12" id="UplaodMasterDataHLdiv" runat="server">
                                    <asp:HyperLink ID="UplaodMasterDataHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/uploadmasterdata.aspx">Upload Master Data</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="UploadHRMSDataHLdiv" runat="server">
                                    <asp:HyperLink ID="UploadHRMSDataHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMasUploadHRMSData.aspx">Upload HRMS Data</asp:HyperLink>
                                </div>
                                  <div class="col-md-4 col-sm-12 col-xs-12" id="Div1" runat="server">
                                    <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAMGVendorNewRecord1.aspx">Add Vendor</asp:HyperLink>
                                </div>
                                  </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/bootstrap.min.js"></script>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
</body>
</html>
