Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_MAS_WebFiles_frmMASCity
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Private Sub Cleardata()
        txtCitycode.Text = String.Empty
        txtCityName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlCity.SelectedIndex = 0
        ddlCountry.SelectedIndex = 0
        ddlState.SelectedIndex = 0
        ddlZone.SelectedIndex = 0
    End Sub

    Private Sub Modifydata()
        obj.getname = txtCityName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyCity(ddlCountry, ddlCity, ddlZone, ddlState, Me) > 0) Then
            Cleardata()
            lblMsg.Text = "City Updated Successfully "
        End If
        obj.City_LoadGrid(gvItem)
        obj.BindCity(ddlCity)
    End Sub

    Private Sub GetZone()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ZONE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlZone.DataSource = sp.GetDataSet()
            ddlZone.DataTextField = "ZN_NAME"
            ddlZone.DataValueField = "ZN_CODE"
            ddlZone.DataBind()
            ddlZone.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub GetState()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_STATE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlState.DataSource = sp.GetDataSet()
            ddlState.DataTextField = "STE_NAME"
            ddlState.DataValueField = "STE_CODE"
            ddlState.DataBind()
            ddlState.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtCitycode.Text
        obj.getname = txtCityName.Text
        obj.getRemarks = txtRemarks.Text
        Dim iStatus As Integer = obj.InsertCity(ddlCountry, ddlZone, ddlState, Me)
        If iStatus = 1 Then
            lblMsg.Text = "City Code Already Exists "
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "City Inserted Successfully "
            lblMsg.Visible = True
            Cleardata()
        ElseIf iStatus = 0 Then
            lblMsg.Text = "City Inserted Successfully "
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.City_LoadGrid(gvItem)
        obj.BindCity(ddlCity)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If Not Page.IsPostBack Then
                obj.City_LoadGrid(gvItem)
                trCName.Visible = False
                rbActions.Checked = True
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                GetState()
                GetZone()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "Page_Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trCName.Visible = False
                txtCitycode.ReadOnly = False
                ddlState.Enabled = True
                ddlZone.Enabled = True
                ddlCountry.Enabled = True
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                trCName.Visible = True
                txtCitycode.ReadOnly = True
                btnSubmit.Text = "Modify"
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                GetState()
                GetZone()
                Cleardata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmCountry", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            If ddlCity.SelectedItem.Value <> "--Select--" Then
                obj.BindCountry(ddlCountry)
                obj.City_SelectedIndex_Changed(ddlCity, ddlCountry, ddlState, ddlZone)
                txtCitycode.Text = obj.getcode
                txtCityName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
                ddlCountry.Enabled = False
                ddlState.Enabled = False
                ddlZone.Enabled = False

            Else
                Cleardata()
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "ddlCity_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.City_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.City_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(5).Visible = False
                    gvItem.HeaderRow.Cells(6).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(5).Visible = False
                        gvItem.Rows(i).Cells(6).Visible = False
                    Next
                    lblMsg.Text = "First Inactivate all the Location(s) under this city "
                    Exit Sub
                End If
                obj.City_LoadGrid(gvItem)
                Cleardata()
            End If
            'obj.BindCity(ddlCity)
            'obj.BindCountry(ddlCountry)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmCountry", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/Mas_Webfiles/frmMASMasters.aspx")
    End Sub

    Protected Sub ddlState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlState.SelectedIndexChanged
        Try
            If (ddlState.SelectedValue <> "--Select--") Then


                Dim sp1 As SqlParameter = New SqlParameter("@STE_CODE", SqlDbType.NVarChar, 50)
                sp1.Value = ddlState.SelectedItem.Value
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_STATE_BY_ID", sp1)

                If (dr.Read) Then

                    ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(dr("STE_CNY_ID")))
                    ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(dr("STE_ZN_ID")))
                    ddlCountry.Enabled = False
                    ddlZone.Enabled = False

                End If
            Else
                ddlCountry.SelectedValue = "--Select--"
                ddlZone.SelectedValue = "--Select--"
            End If

        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "ddlState_SelectedIndexChanged", exp)
        End Try
    End Sub
End Class