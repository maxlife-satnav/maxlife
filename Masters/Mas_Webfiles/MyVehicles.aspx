<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="MyVehicles.aspx.vb" Inherits="Masters_Mas_Webfiles_MyVehicles" Title="My Vechicles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black"> My Vehicles 
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong> My Vehicles </strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table cellspacing="2" cellpadding="2" width="100%" border="0">
                    <tr>
                        <td valign="top" align="left">
                            <fieldset>
                                <legend>Claimed Vehicles </legend>
                                <asp:GridView ID="gvClaimedVehicles" TabIndex="9" runat="server" Width="100%" AutoGenerateColumns="False"
                                    AllowPaging="True" EmptyDataText="No Records Found">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reg. No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegNo" runat="Server" Text='<%#Eval("VHL_REG_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Eng. No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEngNo" runat="Server" Text='<%#Eval("VHL_ENG_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CC ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCC" runat="Server" Text='<%#Eval("VHL_CC") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <a href='ModifyVehicle.aspx?SNO=<%#Eval("SNO")%>'>View Details</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <fieldset>
                                <legend>Unclaimed Vehicles </legend>
                                <asp:GridView ID="gvUnClaimedVehicleDetails" TabIndex="9" runat="server" Width="100%"
                                    AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Records Found">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reg. No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegNo" runat="Server" Text='<%#Eval("VHL_REG_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Eng. No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEngNo" runat="Server" Text='<%#Eval("VHL_ENG_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CC ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCC" runat="Server" Text='<%#Eval("VHL_CC") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <a href='ModifyVehicle.aspx?SNO=<%#Eval("SNO")%>&rtnurl=MyVehicles.aspx'>View Details</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnClaimNow" CommandName="Claim" CommandArgument='<%#Eval("SNO")%>'
                                                    runat="server">Claim Now</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</asp:Content>
