Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_Mas_Webfiles_frmMasRequest
    Inherits System.Web.UI.Page
    Dim lcmtemp As String
    Dim bdgtemp As String
    Dim twrtemp As String
    Dim spctemp As String
    Dim flrtemp As String
    Dim sertemp As String
    Dim sertemp1 As String
    Dim ctycode As String
    Dim sms1_count As Integer = 0
    Dim sms2_count As Integer = 0
    Dim sms3_count As Integer = 0
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim SQLSTRING As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindService_request_category(ddlType, 0)
            BindCity()
            BindGrid()
            btnsubmit.Visible = True
            btnModify.Visible = False
        End If

    End Sub


    Private Sub BindService_request_category(ByRef ddl As DropDownList, ByVal idparent As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddl, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
    End Sub


    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlcity.DataSource = sp.GetDataSet()
        ddlcity.DataTextField = "CTY_NAME"
        ddlcity.DataValueField = "CTY_CODE"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub


    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_MATRIX_GRID")
        Dim dummy As String = sp.Command.CommandSql
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If Page.IsValid = True Then
            For Each li As ListItem In liitems.Items
                If li.Selected = True Then
                    txtstore.Text = txtstore.Text + li.Value.ToString() + ","
                End If
            Next
            txtstore.Text = txtstore.Text.Remove(txtstore.Text.Length - 1)
            SQLSTRING = txtstore.Text

            AddRequest(SQLSTRING)
            Cleardata()
            Clearusers()

            BindGrid()
        End If
    End Sub

    Public Sub BindEscalation()
        Dim dr As SqlDataReader
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SRQUSP_ESCALATIONS")
        sp.Command.AddParameter("@SRQ_MAP_CITY", ddlcity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SRQ_MAP_REQID", ddlreqtype.SelectedValue, DbType.String)
        dr = sp.GetReader()
        If dr.Read() Then
            txtUser1Contact.Text = dr("LOCINC1")
            txtmob1.Text = dr("INCNUM1")
            txtuser2.Text = dr("LOCINC2")
            txtuser3.Text = dr("LOCINC3")
            txtmob2.Text = dr("INCNUM2")
            txtmob3.Text = dr("INCNUM3")
        End If
        txtUser1Contact.Text = dr("LOCINC1")
        txtmob1.Text = dr("INCNUM1")
        txtuser2.Text = dr("LOCINC2")
        txtuser3.Text = dr("LOCINC3")
        txtmob2.Text = dr("INCNUM2")
        txtmob3.Text = dr("INCNUM3")
    End Sub

    Private Sub Clearusers()
        txtUser1Contact.ReadOnly = False
        txtuser2.ReadOnly = False
        txtuser3.ReadOnly = False
    End Sub

    Private Sub AddRequest(ByVal loc As String)
        Try
            Dim req As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("uid")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_REQUEST_MATRIX")
            sp.Command.AddParameter("@REQ_ID", req, DbType.String)
            sp.Command.AddParameter("@TYPE", ddlType.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REQ_TYPE", ddlreqtype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REQ_NAME", ddlRequests.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LEVEL1_USER", txtUser1Contact.Text, DbType.String)
            sp.Command.AddParameter("@LEVEL2_USER", txtuser2.Text, DbType.String)
            sp.Command.AddParameter("@LEVEL3_USER", txtuser3.Text, DbType.String)
            sp.Command.AddParameter("@LEVEL1_HRS", txtL1.Text, DbType.String)
            sp.Command.AddParameter("@LEVEL2_HRS", txtL2.Text, DbType.String)
            sp.Command.AddParameter("@LEVEL3_HRS", txtL3.Text, DbType.String)
            sp.Command.AddParameter("@REMARKS", txtRem.Text, DbType.String)
            sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)

            sp.Command.AddParameter("@LOCATION", loc, DbType.String)
            sp.Command.AddParameter("@STAT", ddlStatus.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REQ_CITY", ddlcity.SelectedItem.Value, DbType.String)
            sp.ExecuteScalar()

            Dim strEmpId As String = ""
            Dim strEmpId2 As String = ""
            Dim strEmpId3 As String = ""
            If Trim(txtUser1Contact.Text) <> "" Then
                Dim arrTEmployees As Array = Split(txtUser1Contact.Text, ",")
                strEmpId = strEmpId & arrTEmployees(0)
            End If
            If Trim(txtuser2.Text) <> "" Then
                Dim arrTEmployees2 As Array = Split(txtuser2.Text, ",")
                strEmpId2 = strEmpId2 & arrTEmployees2(0)
            End If
            If Trim(txtuser3.Text) <> "" Then
                Dim arrTEmployees3 As Array = Split(txtuser3.Text, ",")
                strEmpId3 = strEmpId3 & arrTEmployees3(0)
            End If


            '----------------- USER ID AND EMAILS -----------------------------------------
            If Trim(txtUser1Contact.Text) <> "" Then
                Dim strArray As String() = Trim(txtUser1Contact.Text).Split(",")
                Dim i As Integer
                For i = 0 To strArray.Length - 1
                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"INSERT_EMAIL_REQUEST")
                    sp2.Command.AddParameter("@REQ_TYPE", req, DbType.String)
                    sp2.Command.AddParameter("@AUR_ID", strArray(i), DbType.String)
                    sp2.Command.AddParameter("@ESC_LEVEL", 0, DbType.Int32)
                    sp2.ExecuteScalar()
                Next i
            End If

            If Trim(txtuser2.Text) <> "" Then
                Dim strArray As String() = Trim(txtuser2.Text).Split(",")
                Dim i As Integer
                For i = 0 To strArray.Length - 1
                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"INSERT_EMAIL_REQUEST")
                    sp2.Command.AddParameter("@REQ_TYPE", req, DbType.String)
                    sp2.Command.AddParameter("@AUR_ID", strArray(i), DbType.String)
                    sp2.Command.AddParameter("@ESC_LEVEL", 1, DbType.Int32)
                    sp2.ExecuteScalar()
                Next i
            End If

            If Trim(txtuser3.Text) <> "" Then
                Dim strArray As String() = Trim(txtuser3.Text).Split(",")
                Dim i As Integer
                For i = 0 To strArray.Length - 1
                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"INSERT_EMAIL_REQUEST")
                    sp2.Command.AddParameter("@REQ_TYPE", req, DbType.String)
                    sp2.Command.AddParameter("@AUR_ID", strArray(i), DbType.String)
                    sp2.Command.AddParameter("@ESC_LEVEL", 2, DbType.Int32)
                    sp2.ExecuteScalar()
                Next i
            End If

            '--------------------------------------------------------------



            If txtmob1.Text <> "" Then
                Dim strArray As String() = Trim(txtmob1.Text).Split(",")
                Dim i As Integer
                For i = 0 To strArray.Length - 1
                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SMS_REQ_0")
                    sp2.Command.AddParameter("@SMS_REQ_TYPE", req, DbType.String)
                    sp2.Command.AddParameter("@SMS_PHONE_NUMBER", strArray(i), DbType.String)
                    sp2.ExecuteScalar()
                Next i
            End If

            If txtmob2.Text <> "" Then
                Dim strArray1 As String() = Trim(txtmob2.Text).Split(",")
                Dim J As Integer
                For J = 0 To strArray1.Length - 1
                    Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SMS_REQ_1")
                    sp3.Command.AddParameter("@SMS_REQ_TYPE", req, DbType.String)
                    sp3.Command.AddParameter("@SMS_PHONE_NUMBER", strArray1(J), DbType.String)
                    sp3.ExecuteScalar()
                Next J
            End If

            If txtmob3.Text <> "" Then
                Dim strArray2 As String() = Trim(txtmob3.Text).Split(",")
                Dim K As Integer
                For K = 0 To strArray2.Length - 1
                    Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SMS_REQ_2")
                    sp4.Command.AddParameter("@SMS_REQ_TYPE", req, DbType.String)
                    sp4.Command.AddParameter("@SMS_PHONE_NUMBER", strArray2(K), DbType.String)
                    sp4.ExecuteScalar()
                Next K
            End If

            Dim validassigned As Integer
            validassigned = ValidateAssigned(strEmpId)
            If validassigned = 1 Then
                UpdateAssignedRole(strEmpId)

            End If
            Dim validassigned1 As Integer
            validassigned1 = ValidateAssigned1(strEmpId2)
            If validassigned = 1 Then
                UpdateAssignedRole1(strEmpId2)

            End If
            Dim validassigned2 As Integer
            validassigned2 = ValidateAssigned2(strEmpId3)
            If validassigned = 1 Then
                UpdateAssignedRole2(strEmpId3)

            End If
            lblMsg.Text = "Request added Succesfully"
        Catch ex As Exception

        End Try
    End Sub
    Private Function ValidateAssigned(ByVal strEmpId As String)
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_ROLE")
        sp.Command.AddParameter("@AUR_ID", strempid, DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function

    Private Sub UpdateAssignedRole(ByVal strEmpId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_ASSIGNED_ROLE")
        sp.Command.AddParameter("@AUR_ID", strEmpId, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Function ValidateAssigned1(ByVal strEmpId2 As String)
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_ROLE")
        sp.Command.AddParameter("@AUR_ID", strEmpId2, DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function

    Private Sub UpdateAssignedRole1(ByVal strEmpId2 As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_ASSIGNED_ROLE")
        sp.Command.AddParameter("@AUR_ID", strEmpId2, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Function ValidateAssigned2(ByVal strEmpId3 As String)
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_ROLE")
        sp.Command.AddParameter("@AUR_ID", strEmpId3, DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function

    Private Sub UpdateAssignedRole2(ByVal strEmpId3 As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_ASSIGNED_ROLE")
        sp.Command.AddParameter("@AUR_ID", strEmpId3, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub Cleardata()
       ' ddlreqtype.SelectedIndex = 0
        ddlType.SelectedIndex = -1
        txtuser2.Text = ""
        txtuser3.Text = ""
        txtL1.Text = ""
        txtL2.Text = ""
        txtL3.Text = ""
        txtRem.Text = ""
        ddlStatus.SelectedIndex = 0
        txtmob1.Text = ""
        txtmob2.Text = ""
        txtmob3.Text = ""
        liitems.Items.Clear()
        ddlRequests.Items.Clear()
        txtUser1Contact.Text = ""
        txtstore.Text = ""
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "EDIT" Then
            lblMsg.Text = ""
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblreq As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblreq"), Label)
            txtcode.Text = lblreq.Text
            GetRequestDetails()
            btnsubmit.Visible = False
            btnModify.Visible = True
        ElseIf e.CommandName = "DELETE" Then
            btnsubmit.Visible = True
            btnModify.Visible = False
            lblMsg.Text = ""
            Cleardata()
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblreq As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblreq"), Label)
            Dim valid As Integer = 0
            valid = Validate_Request(lblreq.Text)
            If valid = 1 Then
                lblMsg.Text = "You cannot delete this request "
            Else
                Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DEL_RECORD")
                SP.Command.AddParameter("@sno", lblreq.Text, DbType.String)
                SP.ExecuteScalar()
            End If

        End If
        BindGrid()
    End Sub
    Private Function Validate_Request(ByVal req As String)
        Dim req1 As Integer = 0
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_SRQ_EMP_REQ")
        sp1.Command.AddParameter("@req", req, DbType.String)
        req1 = sp1.ExecuteScalar()
        Return req1
    End Function

    Private Sub GetRequestDetails()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_MATRIX_DETAILS")
        sp.Command.AddParameter("@SNO", txtcode.Text, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            
            ddlreqtype.ClearSelection()
            Dim li As ListItem
            li = ddlreqtype.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_TYPE"))
            If li IsNot Nothing Then
                li.Selected = True
            End If


            'txtDesc.Text = ds.Tables(0).Rows(0).Item("REQ_NAME")
            txtL1.Text = ds.Tables(0).Rows(0).Item("L1_HR")
            txtL2.Text = ds.Tables(0).Rows(0).Item("L2_HR")
            txtL3.Text = ds.Tables(0).Rows(0).Item("L3_HR")
           
            ddlType.ClearSelection()
            li = ddlType.Items.FindByValue(ds.Tables(0).Rows(0).Item("COMP_TYPE"))
            If li IsNot Nothing Then
                li.Selected = True
            End If

            BindService_request_category(ddlreqtype, ddlType.SelectedItem.Value)

            ddlreqtype.ClearSelection()
            Try
                li = ddlreqtype.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_TYPE"))
                If li IsNot Nothing Then
                    li.Selected = True
                End If
            Catch ex As Exception

            End Try


            Try
                BindService_request_category(ddlRequests, ddlreqtype.SelectedItem.Value)
            Catch ex As Exception

            End Try


            ddlRequests.ClearSelection()
            li = ddlRequests.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_NAME"))
            If li IsNot Nothing Then
                li.Selected = True
            End If


             ddlcity.ClearSelection()
            li = ddlcity.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_CITY"))
            If li IsNot Nothing Then
                li.Selected = True
            End If

            liitems.Items.Clear()
            BindLocation()
            liitems.ClearSelection()


            Dim mystrings As String()
            Dim text As String = ds.Tables(0).Rows(0).Item("REQ_LOC")

            mystrings = text.Split(",")
            Try
                For Each str As String In mystrings
                    liitems.Items.FindByValue(str).Selected = True
                Next
            Catch ex As Exception

            End Try

            


            ddlStatus.ClearSelection()
            li = ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("STA_ID"))
            If li IsNot Nothing Then
                li.Selected = True
            End If
            txtRem.Text = ds.Tables(0).Rows(0).Item("REMARKS")
        End If
       
        txtUser1Contact.Text = ""
        txtmob1.Text = ""
        txtmob2.Text = ""
        txtuser2.Text = ""
        txtmob3.Text = ""
        txtuser3.Text = ""

        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_SMS1")
        sp3.Command.AddParameter("@REQ", txtcode.Text, DbType.String)
        Dim dr3 As SqlDataReader = sp3.GetReader()
        While (dr3.Read())
            txtmob1.Text = txtmob1.Text & CStr(dr3("SMS_PHONE_NUMBER")) & ","
         End While

        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_SMS2")
        sp4.Command.AddParameter("@REQ", txtcode.Text, DbType.String)
        Dim dr4 As SqlDataReader = sp4.GetReader()
        While (dr4.Read())
            txtmob2.Text = txtmob2.Text & CStr(dr4("SMS_PHONE_NUMBER")) & ","
         End While
 
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_SMS3")
        sp5.Command.AddParameter("@REQ", txtcode.Text, DbType.String)
        Dim dr5 As SqlDataReader = sp5.GetReader()
        While (dr5.Read())
            txtmob3.Text = txtmob3.Text & CStr(dr5("SMS_PHONE_NUMBER")) & ","
         End While



        Try
            txtmob1.Text = txtmob1.Text.Remove(txtmob1.Text.Length - 1)
            txtmob2.Text = txtmob2.Text.Remove(txtmob2.Text.Length - 1)
            txtmob3.Text = txtmob3.Text.Remove(txtmob3.Text.Length - 1)

        Catch ex As Exception

        End Try



        Dim spparam As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_EMAILUSERS")
        spparam.Command.AddParameter("@REQ", txtcode.Text, DbType.String)
        spparam.Command.AddParameter("@ESC_LEVEL", 0, DbType.Int32)
        Dim drparam As SqlDataReader = spparam.GetReader()
        While (drparam.Read())
            txtUser1Contact.Text = txtUser1Contact.Text & CStr(drparam("EMAIL_AUR_ID")) & ","
        End While

        Dim spparam1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_EMAILUSERS")
        spparam1.Command.AddParameter("@REQ", txtcode.Text, DbType.String)
        spparam1.Command.AddParameter("@ESC_LEVEL", 1, DbType.Int32)
        Dim drparam1 As SqlDataReader = spparam1.GetReader()
        While (drparam1.Read())
            txtuser2.Text = txtuser2.Text & CStr(drparam1("EMAIL_AUR_ID")) & ","
        End While


        Dim spparam2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_EMAILUSERS")
        spparam2.Command.AddParameter("@REQ", txtcode.Text, DbType.String)
        spparam2.Command.AddParameter("@ESC_LEVEL", 2, DbType.Int32)
        Dim drparam2 As SqlDataReader = spparam2.GetReader()
        While (drparam2.Read())
            txtuser3.Text = txtuser3.Text & CStr(drparam2("EMAIL_AUR_ID")) & ","
        End While
        Try
            txtUser1Contact.Text = txtUser1Contact.Text.Remove(txtUser1Contact.Text.Length - 1)
            txtuser2.Text = txtuser2.Text.Remove(txtuser2.Text.Length - 1)
            txtuser3.Text = txtuser3.Text.Remove(txtuser3.Text.Length - 1)

        Catch ex As Exception

        End Try



    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        Try
            If Page.IsValid = True Then
                ModifyRequest()
                Cleardata()
                Clearusers()
                lblMsg.Text = "Request Modified Succesfully"
                BindGrid()
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub ModifyRequest()
        Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DEL_RECORD")
        SP1.Command.AddParameter("@sno", txtcode.Text, DbType.String)
        SP1.ExecuteScalar()
        For Each li As ListItem In liitems.Items
            If li.Selected = True Then
                txtstore.Text = txtstore.Text + li.Value.ToString() + ","
            End If
        Next
        txtstore.Text = txtstore.Text.Remove(txtstore.Text.Length - 1)
        SQLSTRING = txtstore.Text

        AddRequest(SQLSTRING)
    End Sub

    Protected Sub gvitems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvitems.RowDeleting

    End Sub

    Protected Sub gvitems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub


    Protected Sub txtUser1Contact_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUser1Contact.TextChanged
        txtUser1Contact.ReadOnly = True
    End Sub

    Protected Sub txtuser2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtuser2.TextChanged
        txtuser2.ReadOnly = True
    End Sub

    Protected Sub txtuser3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtuser3.TextChanged
        txtuser3.ReadOnly = True
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedIndex > 0 Then
            BindService_request_category(ddlreqtype, ddlType.SelectedItem.Value)
        Else
            '  liitems.Items.Clear()
        End If
    End Sub


    Protected Sub ddlreqtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlreqtype.SelectedIndexChanged
        If ddlreqtype.SelectedIndex > 0 Then
            BindService_request_category(ddlRequests, ddlreqtype.SelectedItem.Value)
        End If
    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcity.SelectedIndexChanged
        If ddlcity.SelectedIndex > 0 Then
            BindLocation()
        Else
            liitems.Items.Clear()
        End If
    End Sub
    Private Sub BindLocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_CITY")
        sp.Command.AddParameter("@CITY", ddlcity.SelectedItem.Value, DbType.String)
        liitems.DataSource = sp.GetDataSet()
        liitems.DataTextField = "LCM_NAME"
        liitems.DataValueField = "LCM_CODE"
        liitems.DataBind()

    End Sub

    Protected Sub ddlRequests_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRequests.SelectedIndexChanged
        If ddlRequests.SelectedIndex <> 0 Then
            BindEscalation()
        End If
    End Sub
End Class
