﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager

Partial Class Masters_Mas_Webfiles_frmMASspacetypeGradeMapping
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Or Session("Parent") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = String.Empty
        If Not Page.IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            bindSpacetype()
            'bindGrades()
            BindBand()
            BindGrid()
        End If
    End Sub

    Public Sub bindSpacetype()
        ddlSpaceType.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSpaceType, "GET_SPACE_SUB_LAYERS", "SST_NAME", "SST_CODE")
    End Sub

    'Public Sub bindGrades()
    '    ddlfromGrade.Items.Clear()
    '    ObjSubsonic.BindListBox(ddlfromGrade, "AMANTRA_EMPLOYEE_GRADE_GETDETAILS", "grade_name", "grade_Code")
    '    'ObjSubsonic.Binddropdown(ddlToGrade, "AMANTRA_EMPLOYEE_GRADE_GETDETAILS", "grade_name", "grade_Code")
    '    'For i As Integer = 1 To 350
    '    '    ddlfromGrade.Items.Insert((i - 1).ToString(), i.ToString())
    '    '    ddlToGrade.Items.Insert((i - 1).ToString(), i.ToString())
    '    'Next

    'End Sub
    Private Sub BindBand()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SMS_BIND_BAND_GRIDDATA")
        ddlBand.DataSource = sp.GetDataSet()
        ddlBand.DataTextField = "BAND_NAME"
        ddlBand.DataValueField = "BAND_CODE"
        ddlBand.DataBind()
        ' ddlBand.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Public Sub BindGrid()
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
        param(0).Value = ""
        param(1) = New SqlParameter("@ID", SqlDbType.Int)
        param(1).Value = 0
        param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
        param(2).Value = "Get"
        param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
        param(3).Value = ""
        param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
        param(4).Value = ""
        param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
        param(5).Value = ""

        ObjSubsonic.BindGridView(gvItem, "INSERT_SPACE_TYPE_GRADE_MAPPING", param)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strEroorMsg As String = String.Empty
        Dim selectedIndex As String = String.Empty

        For Each item As ListItem In ddlBand.Items
            If item.Selected Then
                If selectedIndex = "" Then
                    selectedIndex = item.Value
                Else
                    selectedIndex = selectedIndex & ", " & item.Value
                End If
            End If
        Next

        If Not selectedIndex = "" Then
            If btnSubmit.Text = "Submit" Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If ddlSpaceType.SelectedItem.Value = "--Select--" Or ddlBand.SelectedItem.Value = "--Select--" Then
                    ' PopUpMessage("Please Enter Mandatory fields", Me)
                    lblMsg.Text = "Please Enter Mandatory fields"
                    lblMsg.Visible = True
                Else
                    insertdata(selectedIndex)
                    'obj.Bindlocation(ddlselectLoc)
                End If
            ElseIf btnSubmit.Text = "Modify" Then
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If ddlSpaceType.SelectedItem.Value = "--Select--" Or ddlBand.SelectedItem.Value = "--Select--" Then
                    'PopUpMessage("Please Enter Mandatory fields", Me)
                    lblMsg.Text = "Please Enter Mandatory fields"
                    lblMsg.Visible = True
                Else
                    Modifydata(selectedIndex)
                    'obj.Bindlocation(ddlselectLoc)
                    If lblMsg.Text = "" Then
                        btnSubmit.Text = "Submit"
                    End If
                End If
            End If
            BindGrid()
        Else
            lblMsg.Text = "Please select atleast one grade"
        End If


    End Sub

    Private Sub insertdata(selectedIndex As String)

        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
        param(0).Value = ddlSpaceType.SelectedValue
        param(1) = New SqlParameter("@ID", SqlDbType.Int)
        param(1).Value = 0
        param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
        param(2).Value = "INSERT"
        param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
        param(3).Value = selectedIndex
        param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
        param(4).Value = ddlstatus.SelectedValue
        param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
        param(5).Value = Session("UID")

        Dim iStatus As Integer = ObjSubsonic.GetSubSonicExecute("INSERT_SPACE_TYPE_GRADE_MAPPING", param)
        If iStatus = 0 Then
            lblMsg.Text = "Space Type inserted Successfully "
            lblMsg.Visible = True
            Cleardata()
        ElseIf iStatus = 1 Then
            lblMsg.Text = "Space Type is already Mapped"
            lblMsg.Visible = True
            Cleardata()
        End If
        BindGrid()
    End Sub
    Private Sub Modifydata(selectedIndex As String)
        Dim param1(0) As SqlParameter
        param1(0) = New SqlParameter("@SPC_TYPE", SqlDbType.NVarChar, 50)
        param1(0).Value = ddlSpaceType.SelectedValue
        Dim stat As Integer = ObjSubsonic.GetSubSonicExecuteScalar("SMS_CHECK_SPCREQ_GRADECHANGE", param1)
        If stat = 0 Then

            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
            param(0).Value = ddlSpaceType.SelectedValue
            param(1) = New SqlParameter("@ID", SqlDbType.Int)
            param(1).Value = ViewState("id")
            param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
            param(2).Value = "UPDATE"
            param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
            param(3).Value = selectedIndex
            param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
            param(4).Value = ddlstatus.SelectedValue
            param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
            param(5).Value = Session("UID")

            Dim iStatus As Integer = ObjSubsonic.GetSubSonicExecute("INSERT_SPACE_TYPE_GRADE_MAPPING", param)

            If iStatus = 0 Then
                lblMsg.Text = "Space Type Modified Successfully "
                lblMsg.Visible = True
                Cleardata()
            ElseIf iStatus = 1 Then
                lblMsg.Text = "Space Type is already Mapped to the Grade"
                lblMsg.Visible = True
                Cleardata()
            End If

            BindGrid()
        Else
            lblMsg.Text = "Please approve the pending space requests."
        End If
    End Sub

    Public Sub Cleardata()
        ddlSpaceType.ClearSelection()
        ddlBand.ClearSelection()
        ddlstatus.ClearSelection()
        btnSubmit.Text = "Submit"
    End Sub


    Protected Sub gvItem_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try

            If e.CommandName = "Edit" Then
                btnSubmit.Text = "Modify"
                ViewState("id") = e.CommandArgument.ToString()
                Dim dr As SqlDataReader
                bindSpacetype()
                ' bindGrades()
                BindBand()
                Dim param(5) As SqlParameter
                param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
                param(0).Value = ""
                param(1) = New SqlParameter("@ID", SqlDbType.Int)
                param(1).Value = ViewState("id")
                param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
                param(2).Value = "TYPE"
                param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
                param(3).Value = ""
                param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
                param(4).Value = ""
                param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
                param(5).Value = ""
                dr = ObjSubsonic.GetSubSonicDataReader("INSERT_SPACE_TYPE_GRADE_MAPPING", param)
                If (dr.HasRows) Then
                    If dr.Read() Then
                        ddlSpaceType.SelectedValue = dr("SGM_LAYER").ToString()
                        ddlstatus.SelectedValue = dr("SGM_STA_ID").ToString()
                    End If
                    dr.NextResult()
                    While (dr.Read())
                        For Each i As ListItem In ddlBand.Items
                            If i.Value = dr("SGM_GRADE").ToString() Then
                                i.Selected = True
                            End If
                        Next
                    End While
                Else
                    lblMsg.Text = "No Space type is Mapped"
                    lblMsg.Visible = True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvItem_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvItem.RowEditing

    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Cleardata()
    End Sub
End Class
