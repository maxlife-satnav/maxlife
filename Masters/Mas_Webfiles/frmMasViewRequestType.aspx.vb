Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Masters_Mas_Webfiles_frmMasViewRequestType
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim All As String = "All"
            BindGrid(All)
            lbtn1.Visible = False
        End If
    End Sub
    Private Sub BindGrid(ByVal value As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PN_MAINTENANCE_REQUEST_TYPE")
            sp.Command.AddParameter("@REQTYPE", value, DbType.String)
            gvItem.DataSource = sp.GetDataSet()
            gvItem.DataBind()
            For i As Integer = 0 To gvItem.Rows.Count - 1
                Dim lblStatus As Label = CType(gvItem.Rows(i).FindControl("lblStatus"), Label)
                If lblStatus.Text = 0 Then
                    lblStatus.Text = "InActive"
                Else
                    lblStatus.Text = "Active"
                End If
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
        Dim All As String = "All"
        BindGrid(All)
        lbtn1.Visible = False
        txtfindcode.Text = ""
    End Sub

    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
        Dim req As String = txtfindcode.Text
        BindGrid(req)
        lbtn1.Visible = True
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex()
        Dim All As String = "All"
        BindGrid(All)
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblsno As Label = DirectCast(gvItem.Rows(rowIndex).FindControl("lblsno"), Label)
            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DEL_PN_MAINTENANCE_REQUEST_TYPE")
            SP.Command.AddParameter("@SNO", lblsno.Text, DbType.Int32)
            SP.ExecuteScalar()
        End If
        Dim All As String = "All"
        BindGrid(All)
    End Sub

    Protected Sub gvItem_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItem.RowDeleting

    End Sub
End Class
