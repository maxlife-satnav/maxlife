﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Masters_Mas_Webfiles_AddVehicle
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            lblMsg.Visible = False
            BindRCBook()
            BindTAXINVOICE()
            BindPolicy()
            BindStatus()
            clearall()
        End If
    End Sub

    Private Sub BindStatus()
        ObjSubSonic.Binddropdown(ddlVehStatus, "GETVEHICLESTATUS", "VHL_STATUS_TEXT", "VHL_STATUS_CODE")
    End Sub
    Private Sub BindPolicy()
        ObjSubSonic.Binddropdown(ddlInsPolicy, "GETVEHICLEPOLICYSTATUS", "VHL_INSPOLICY_STATUS_TEXT", "VHL_INSPOLICY_STATUS_CODE")
    End Sub

    Private Sub BindTAXINVOICE()
        ObjSubSonic.Binddropdown(ddlTaxInvoice, "GETVEHICLETAXINVOICESTATUS", "TAX_INVOICE_TEXT", "TAX_INVOICE_CODE")
    End Sub

    Private Sub BindRCBook()
        ObjSubSonic.Binddropdown(ddlRCBookStatus, "GETVEHICLERCBOOKSTATUS", "RC_BOOK_TEXT", "RC_BOOK_CODE")
    End Sub


    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            lblMsg.Text = ""

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETEMPLOYEEDETAILS")
            sp.Command.AddParameter("@AUR_ID", txtEmpId.Text)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                pnlEmpName.Visible = True
                pnlInfo.Visible = True
                txtEmpName.Text = ds.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
                txtCostCenter.Text = ds.Tables(0).Rows(0).Item("AUR_VERT_CODE")
                txtNewGrade.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                lblMsg.Visible = False
                lblMsg.Text = ""
            Else
                lblMsg.Text = "Employee ID Not found."
                lblMsg.Visible = True
                pnlEmpName.Visible = False
                pnlInfo.Visible = False
            End If

        Catch ex As Exception

        End Try


        'Try
        '    lblMsg.Visible = False
        '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_VEHICLE_DETAILS")
        '    sp.Command.AddParameter("@EMP_ID", txtEmpId.Text)
        '    Dim ds As New DataSet
        '    ds = sp.GetDataSet()
        '    If ds.Tables(0).Rows.Count > 0 Then
        '        pnlEmpName.Visible = True
        '        pnlInfo.Visible = True
        '        txtEmpName.Text = ds.Tables(0).Rows(0).Item("EMP_FIRST_NAME")
        '        txtNewGrade.Text = ds.Tables(0).Rows(0).Item("EMP_NEW_GRADE")
        '        txtCostCenter.Text = ds.Tables(0).Rows(0).Item("EMP_COST_CENTER")
        '    Else
        '        lblMsg.Visible = True
        '        lblMsg.Text = "Employee Id Not Existed"
        '        pnlEmpName.Visible = False
        '        pnlInfo.Visible = False
        '    End If
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        insetdata()
    End Sub
    Private Sub insetdata()
        Try

            If ddlVehStatus.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Please select Status."
                lblMsg.Visible = True
                Exit Sub
            End If
            If ddlInsPolicy.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Please select Status of Insurance Policy."
                lblMsg.Visible = True
                Exit Sub
            End If
            If ddlTaxInvoice.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Please select Tax Invoice Status."
                lblMsg.Visible = True
                Exit Sub
            End If
            If ddlRCBookStatus.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Please select RC Book Status."
                lblMsg.Visible = True
                Exit Sub
            End If

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"INSERT_VEHICLE_DATA")
            sp.Command.AddParameter("@EMP_ID", txtEmpId.Text, DbType.String)
            sp.Command.AddParameter("@VHL_POCAP_DT", txtPODate.Text, DbType.Date)
            sp.Command.AddParameter("@VHL_INV_DT", txtInvDate.Text, DbType.Date)
            sp.Command.AddParameter("@VHL_INV_NO", txtInvNo.Text, DbType.String)
            sp.Command.AddParameter("@VHL_ASSET_MODEL", txtAssetModel.Text, DbType.String)
            sp.Command.AddParameter("@VHL_REG_NO", txtRegNo.Text, DbType.String)
            sp.Command.AddParameter("@VHL_ENG_NO", txtEngnNo.Text, DbType.String)
            sp.Command.AddParameter("@VHL_CHS_NO", txtChasisNo.Text, DbType.String)
            sp.Command.AddParameter("@VHL_CC", txtCC.Text, DbType.String)
            sp.Command.AddParameter("@VHL_NEW_LIMIT", txtNewLimit.Text, DbType.String)
            sp.Command.AddParameter("@VHL_TOT_VCOST", txtTotVehicleCost.Text, DbType.String)
            sp.Command.AddParameter("@VHL_REG_DT", txtRegDate.Text, DbType.Date)
            sp.Command.AddParameter("@VHL_REPLCMNT_DT", txtDOFReplacement.Text, DbType.Date)
            sp.Command.AddParameter("@VHL_FUEL_TYPE", txtFuelType.Text, DbType.String)
            sp.Command.AddParameter("@VHL_ORG_BUYER", txtOrgBuyer.Text, DbType.String)
            sp.Command.AddParameter("@VHL_YOF_PURCHASE", txtYOP.Text, DbType.String)
            sp.Command.AddParameter("@VHL_DISPOSAL_DT", txtDOP.Text, DbType.Date)
            sp.Command.AddParameter("@VHL_MNTH_RETIREMENT", txtMOR.Text, DbType.String)
            sp.Command.AddParameter("@VHL_INS_NO", txtInsPlcyNo.Text, DbType.String)
            sp.Command.AddParameter("@VHL_INSFRM_DT", txtInsFrmDate.Text, DbType.Date)
            sp.Command.AddParameter("@VHL_INSTO_DT", txtInsToDate.Text, DbType.Date)
            sp.Command.AddParameter("@VHL_YR_EXP", txtYOE.Text, DbType.String)
            sp.Command.AddParameter("@VHL_RC_BOOK", ddlRCBookStatus.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@VHL_TAX_INVOICE", ddlTaxInvoice.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@VHL_FILE_NO", txtFileNo.Text, DbType.String)
            sp.Command.AddParameter("@VHL_REM", txtRemarks.Text, DbType.String)
            sp.Command.AddParameter("@VHL_STATUS", ddlVehStatus.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@VHL_INSPOLICY_STATUS", ddlInsPolicy.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@VHL_FILLING_STATUS", txtFillingStatus.Text, DbType.String)

            sp.ExecuteScalar()
            lblMsg.Visible = True
            lblMsg.Text = "Vehicle details added successfully..."
            clearall()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub clearall()
        For Each control As Control In Me.Controls
            If TypeOf control Is TextBox Then
                Dim txt As TextBox = DirectCast(control, TextBox)
                txt.Text = ""
            End If
        Next
    End Sub
 
End Class
