Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Partial Class Masters_Mas_Webfiles_frmLocationAvailableEdit
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_LOCATIONS_AVAILABLE_OCCUPIED_BIND")
            sp.Command.AddParameter("@LOCATION", txtloc.Text, DbType.String)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblloc As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblloc"), Label)
            Dim lblawt As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblawt"), Label)
            Dim lblowt As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblowt"), Label)
            Dim lblawi As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblawi"), Label)
            Dim lblowi As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblowi"), Label)
            Dim lblawbpo As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblawbpo"), Label)
            Dim lblowbpo As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblowbpo"), Label)
            Dim lblawstl As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblawstl"), Label)
            Dim lblowstl As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblowstl"), Label)
            Dim param(8) As SqlParameter
            param(0) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
            param(0).Value = lblloc.Text
            param(1) = New SqlParameter("@available_wt", SqlDbType.Int)
            param(1).Value = lblawt.Text
            param(2) = New SqlParameter("@Occupied_wt", SqlDbType.Int)
            param(2).Value = lblowt.Text
            param(3) = New SqlParameter("@available_wi", SqlDbType.Int)
            param(3).Value = lblawi.Text
            param(4) = New SqlParameter("@Occupied_wi", SqlDbType.Int)
            param(4).Value = lblowi.Text
            param(5) = New SqlParameter("@available_wb", SqlDbType.Int)
            param(5).Value = lblawbpo.Text
            param(6) = New SqlParameter("@Occupied_wb", SqlDbType.Int)
            param(6).Value = lblowbpo.Text
            param(7) = New SqlParameter("@available_wstl", SqlDbType.Int)
            param(7).Value = lblawstl.Text
            param(8) = New SqlParameter("@Occupied_wstl", SqlDbType.Int)
            param(8).Value = lblowstl.Text
            ObjSubSonic.GetSubSonicExecute("DEL_LOCATION_AVAILABLE_OCCUPIED", param)
        End If
        BindGrid()
    End Sub

    Protected Sub gvitems_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles gvitems.RowDeleted

    End Sub

    Protected Sub gvitems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvitems.RowDeleting

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtloc.Text <> "" Then
            BindGrid()
        End If
    End Sub
End Class
