﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;

public partial class Masters_Mas_Webfiles_Zone : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uid"].ToString() == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        if (!IsPostBack)
        {
           FillGridData();
            trSName.Visible = false;
            rbActions.Checked = true;
            GetCountry();
        }
    }
    private void FillGridData()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_ZONE_DATA");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);

        gvItem.DataSource = sp.GetDataSet();
        gvItem.DataBind();
        ddlZone.DataSource = sp.GetDataSet();
        ddlZone.DataTextField = "ZN_NAME";
        ddlZone.DataValueField = "ZN_CODE";
        ddlZone.DataBind();
        ddlZone.Items.Insert(0, "--Select--");

    }
    protected void GetCountry()
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GETCOUNTRYZONE");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlCny.DataSource = sp.GetDataSet();
        ddlCny.DataTextField = "CNY_NAME";
        ddlCny.DataValueField = "CNY_CODE";
        ddlCny.DataBind();
        ddlCny.Items.Insert(0, "--Select--");
    }
    protected void gvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        FillGridData();
        gvItem.PageIndex = e.NewPageIndex;
        gvItem.DataBind();
        lblMsg.Text = "";
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (rbActionsModify.Checked == true)
            {
                ddlZone.SelectedIndex = 0;
                trSName.Visible = true;
                btnSubmit.Text = "Modify";
                InsertRecord(2);
            }
            else
            {
                trSName.Visible = false;
                ddlZone.SelectedIndex = 0;
                btnSubmit.Text = "Submit";
                InsertRecord(1);
                ddlCny.Enabled = true;
                txtZCode.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }
    public void InsertRecord(int STA)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "INSERT_ZONE_DETAILS");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@ZN_CODE", txtZCode.Text, DbType.String);
            sp.Command.AddParameter("@ZN_NAME", txtZoneName.Text, DbType.String);
            sp.Command.AddParameter("@ZN_CNY_ID", ddlCny.SelectedItem.Value, DbType.String);
            sp.Command.AddParameter("@ZN_REM", txtRemarks.Text, DbType.String);
            sp.Command.AddParameter("@STATUS", STA, DbType.String);
            object a = sp.ExecuteScalar();
            lblMsg.Visible = true;
            if ((int)a > 0)
            {

                if (STA == 1)
                {

                    lblMsg.Text = "Zone Created Successfully";

                }
                else
                {

                    lblMsg.Text = "Zone Updated Successfully";
                }


                txtZCode.Text = String.Empty;
                FillGridData();
                cleardata();
            }
            else
            {

                lblMsg.Text = "Zone Exists With Same Code";
                txtZCode.Text = String.Empty;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void cleardata()
    {

        txtZCode.Text = String.Empty;
        txtZoneName.Text = String.Empty;
        txtRemarks.Text = String.Empty;
        ddlCny.SelectedIndex = 0;

    }
    protected void rbActionsModify_CheckedChanged(object sender, EventArgs e)
    {
        lblMsg.Visible = false;
        if (rbActionsModify.Checked == true)
        {
            ddlZone.SelectedIndex = 0;
            trSName.Visible = true;
            btnSubmit.Text = "Modify";
        }
        else
        {
            trSName.Visible = false;
            btnSubmit.Text = "Submit";
            ddlCny.Enabled = true;
            ddlZone.SelectedIndex = 0;
            txtZCode.Enabled = true;
        }
        txtZCode.Text = String.Empty;

        cleardata();
    }
    protected void ddlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMsg.Visible = false;
        try
        {

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ZONE_BY_ID");
            sp.Command.AddParameter("@ZN_CODE", ddlZone.SelectedItem.Value, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    txtRemarks.Text = reader["ZN_REM"].ToString();
                    txtZoneName.Text = reader["ZN_NAME"].ToString();
                    txtZCode.Text = reader["ZN_CODE"].ToString();
                    ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(reader["ZN_CNY_ID"].ToString()));
                    txtZCode.Enabled = false;
                    ddlCny.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void gvItem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Status")
        {
            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblZN_Code = (Label)gvRow.FindControl("lblZN_Code");
            Label lblStatus_Code = (Label)gvRow.FindControl("lblStatus_Code");

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_CHANGE_ZONE_STATUS");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@ZN_CODE", lblZN_Code.Text, DbType.String);
            sp.Command.AddParameter("@STATUS", lblStatus_Code.Text, DbType.String);
            object a = sp.ExecuteScalar();

            if ((int)a == 0)
            {
                lblMsg.Text = "First inactivate all the states under this Zone ";
                lblMsg.Visible = true;
            }
            else
            {
                lblMsg.Visible = false;
                FillGridData();
                GetCountry();
                cleardata();
            }
        }
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMASMasters.aspx");
    }
}