﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.Configuration.ConfigurationManager
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.IO
Partial Class Masters_Mas_Webfiles_frmMasBusinessSpecificMaster
    Inherits System.Web.UI.Page

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            clear()
            btnSubmit.Text = "Add"
            lblMsg.Visible = False
        Else
            clear()
            displaydata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Page.IsValid Then
            If btnSubmit.Text = "Add" Then
                insertdata(txtParent.Text, txtChild.Text)
            Else
                modifydata()
            End If
        End If
    End Sub

    Private Sub clear()
        txtChild.Text = ""
        txtParent.Text = ""
    End Sub

    Private Sub displaydata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_BSM_GETALL")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            txtParent.Text = ds.Tables(0).Rows(0).Item("AMT_BSM_PARENT")
            txtChild.Text = ds.Tables(0).Rows(0).Item("AMT_BSM_CHILD")
            lblID.Text = ds.Tables(0).Rows(0).Item("AMT_BSM_ID")
        End If
    End Sub

    Private Sub modifydata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_BSM_MODIFY")
        sp.Command.AddParameter("@AMT_BSM_ID", CInt(lblID.Text))
        sp.Command.AddParameter("@AMT_BSM_PARENT", txtParent.Text)
        sp.Command.AddParameter("@AMT_BSM_CHILD", txtChild.Text)
        sp.Command.AddParameter("@AMT_BSM_UPTBY", Session("Uid"))
        sp.ExecuteScalar()
        Session("Parent") = txtParent.Text
        Session("Child") = txtChild.Text
        clear()
        lblMsg.Visible = True
        lblMsg.Text = "Record Modified successfully..."
    End Sub

    Private Sub insertdata(getparent As String, getchild As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_BSM_INSERT")
        sp.Command.AddParameter("@AMT_BSM_PARENT", getparent)
        sp.Command.AddParameter("@AMT_BSM_CHILD", getchild)
        sp.Command.AddParameter("@AMT_BSM_UPTBY", Session("Uid"))
        sp.ExecuteScalar()
        clear()
        lblMsg.Visible = True
        lblMsg.Text = "Record added successfully..."
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~\Masters/MAS_Webfiles/frmMasSpaceMasters.aspx")
    End Sub
End Class
