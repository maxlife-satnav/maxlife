
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Collections.Generic
Imports System.Web.Services

Partial Class Masters_Mas_Webfiles_frmNewUser
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lbl1.Visible = True
            lbl1.Text = String.Empty
            Dim s As String = Session("uid")
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
                'txtDOJ.Attributes.Add("readonly", "readonly")
            End If
            EmpCount()
            If Not Page.IsPostBack Then
                lblHead.Visible = True
                lblHead.Text = "Add New User"
                lblProcess.Text = "Select " + Session("Child")
                lblVertical.Text = "Select " + Session("Parent")
                'CompareValidator6.ErrorMessage = "Please select " + Session("Parent")
                'CompareValidator5.ErrorMessage = "Please select " + Session("Child")

                'Dim year As Integer, cnt As Integer
                'cnt = 1
                'For year = 2001 To getoffsetdatetime(DateTime.Now).Year
                '    ddly2.Items.Insert(cnt, year)
                '    cnt = cnt + 1
                'Next
                'cnt = 1
                'For year = 1950 To getoffsetdatetime(DateTime.Now).Year - 15
                '    ddly1.Items.Insert(cnt, year)
                '    cnt = cnt + 1
                'Next

                txttoday.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.ShortDate)
                'Panel2.Visible = False
                'obj.BindDepartment(ddldept)
                'obj.Bindlocation(ddldg)
                'bindCity()
                obj.BindDepartment(ddldept)
                obj.binddesig(ddldesn)
                obj.binduser(ddlrepmgr, Session("uid"))
                obj.BindCountry(ddlcountry)
                ObjSubsonic.Binddropdown(ddlGrade, "GET_EMP_GRADE", "GRADE_NAME", "GRADE_CODE")
                'ddlcity.SelectedIndex = 0

                'BindCity()
                'BindTimeZones()
                'ddldg.Items.Insert(0, New ListItem("--Select--", "0"))



                'txtDOJ.Text = getoffsetdate(Date.Today)

                'txtDOJ.Attributes.Add("onClick", "displayDatePicker('" + txtDOJ.ClientID + "')")
                'txtDOJ.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                obj.BindVertical(ddlVertical)
                'obj.BindDesig1(ddldesg)


            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmNewUser", "Page_Load", exp)
        End Try
    End Sub

    Private Sub EmpCount()

        Dim sp As New SubSonic.StoredProcedure("CHECK_TOTAL_EMP_COUNT")
        sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
        Dim EmpCount As Integer
        EmpCount = sp.ExecuteScalar()
        'EmpCount = 1
        If EmpCount = 1 Then
            lbl1.Text = "You Have Exceeded the Maximum Number of Employees."
            Button1.Enabled = False
            Exit Sub
        End If

    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GTE_CITYLOC")
        sp.Command.AddParameter("@coun", ddlcountry.SelectedItem.Value, DbType.String)
        ddlcity.DataSource = sp.GetDataSet()
        ddlcity.DataTextField = "cty_name"
        ddlcity.DataValueField = "cty_code"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindTimeZones()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TIME_ZONES_BYCTYID")
        sp.Command.AddParameter("@CNY_CODE", ddlcountry.SelectedItem.Value, DbType.String)
        ddlTimeZone.DataSource = sp.GetDataSet()
        ddlTimeZone.DataTextField = "TIME_ZONE_NAME"
        ddlTimeZone.DataValueField = "TIME_ZONE"
        ddlTimeZone.DataBind()
        ddlTimeZone.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    <WebMethod()> _
       <System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GetEmployeeList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 12
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            'strSQL = "select AUR_ID from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID like '" & prefixText & "%' order by AUR_ID"
            'dtEle = SqlHelper.ExecuteDatatable(CommandType.Text, strSQL)
            Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETUSERS_NAME", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            'count = dtcount
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmNewUser", "GetEmployeeList", exp)
        End Try
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            If txtpwd.Text <> txtcpwd.Text Then
                lbl1.Text = " Please enter correct password "
                txtpwd.Text = String.Empty
                txtcpwd.Text = String.Empty
                Exit Sub
            Else
                txtp.Text = txtpwd.Text.Trim
            End If
            'If txtpwd.Text.Trim().Length < 8 Then

            '    lbl1.Text = "User ID should be minimum of 8 Characters"
            '    Exit Sub
            'End If
            'Dim bol As Boolean = clsSecurity.checkPassword(txtpwd.Text.Trim())
            'If bol = False Then
            '    lbl1.Text = "Password should be minimum of 8 Characters, 2 numerics and 1 Specail Character."
            '    Exit Sub
            'End If

            'If txtaurid.Text.Trim().Length < 8 Then
            '    lbl1.Text = "User ID should be minimum of 8 Characters"
            '    Exit Sub
            'End If

            'If Not txtEmployee.Text.Contains("/") Then
            '    lbl1.Text = "Please enter valid Employee ID"
            '    Exit Sub
            'End If
            Dim iVal As Integer = obj.checkuser(txtaurid.Text.Trim(), Page)
            Try
                'Dim doj As Date = ddlm2.SelectedValue & "/" & ddld2.SelectedValue & "/" & ddly2.SelectedValue
                'If doj > getoffsetdatetime(DateTime.Now) Then
                '    lbl1.Text = "Date of joining should be greater than the todays date "
                '    Exit Sub
                'End If
            Catch ex As Exception
                lbl1.Text = "Please check Date Of Birth"
                Exit Sub
            End Try
            'Try
            '    Dim doB As Date = ddlm1.SelectedValue & "/" & ddld1.SelectedValue & "/" & ddly1.SelectedValue
            'Catch ex As Exception
            '    lbl1.Text = "Please check Date Of Joining"
            '    Exit Sub
            'End Try

            If iVal < 1 Then
                'Dim dob, doj As String

                'dob = ddlm1.SelectedValue.Trim() & "/" & ddld1.SelectedValue.Trim() & "/" & ddly1.SelectedValue.Trim()
                'doj = ddlm2.SelectedValue.Trim() & "/" & ddld2.SelectedValue.Trim() & "/" & ddly2.SelectedValue.Trim()
                'If IsDate(dob) Then
                'Else
                '    lbl1.Text = "Select Valid Date of Birth"
                '    Exit Sub
                'End If
                'If IsDate(doj) Then
                'Else
                '    lbl1.Text = " Select Valid Date for Joining"
                '    Exit Sub
                'End If

                insertdata()
                lbl1.Text = "User Added Successfully"
                Cleardata()
                'PNLCONTAINER.Visible = False
                'lbl1.Text = ""
                'lblHead.Visible = False
                ''face=Zurich BT
                'Label1.Visible = True
                'Label1.Text = "<b><center><br><br><br><br><br><font  size=High color=black>"
                'Label1.Text = Label1.Text & "<font size=High >" & txtfname.Text & " " & txtmname.Text & " " & txtlname.Text & "</font> "
                'Label1.Text = Label1.Text & "<br><br> Account Has Been Created Successfully. </font>"
                ''Response.Write("<br><font face=Zurich BT size=medium >" & txtpwd.Text & "</font> ")
                'Label1.Text = Label1.Text & "<br><br><font face=Zurich BT size=High >Thank you for using the system.</font></center>"
            Else
                PopUpMessageNormal("USER ID :" & txtaurid.Text & " Already Exists. Please choose another ID !", Me.Page)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while inserting data", "frmNewUser", "Button1_Click", exp)
        End Try
    End Sub

    Public Sub insertdata()
        Dim strEmp As String = String.Empty
        strEmp = ddlrepmgr.SelectedValue
        'obj.insert_user(txtfname.Text.Trim.Replace("'", "''"), txtmname.Text.Trim.Replace("'", "''"), txtlname.Text.Replace("'", "''"), txtaurid.Text.Trim("'", "''"), txtpwd.Text, ddltitle.SelectedItem.Text.Trim(), txtmailid.Text.Trim().Replace("'", "''"), strEmp, ddldg.SelectedItem.Value, txtAddress.Text.Trim().Replace("'", "''"), ddlcity.SelectedItem.Value, txtp.Text.Trim().Replace("'", "''"), txtGrade.Text.Trim().Replace("'", "''"), Me, txtmob.Text.Trim().Replace("'", "''"), ddldesn.SelectedItem.Text, ddldept.SelectedValue, ddlcountry.SelectedValue, txtDOJ.Text, ddlTimeZone.SelectedValue, ddlCostcenter.SelectedValue, ddlVertical.SelectedValue)
        obj.insert_user(txtfname.Text.Trim.Replace("'", "''"), txtmname.Text.Trim.Replace("'", "''"), txtlname.Text.Replace("'", "''"), txtaurid.Text.Trim("'", "''"), txtpwd.Text, ddltitle.SelectedItem.Text.Trim(), txtmailid.Text.Trim().Replace("'", "''"), strEmp, ddldg.SelectedItem.Value, txtAddress.Text.Trim().Replace("'", "''"), ddlcity.SelectedItem.Value, txtp.Text.Trim().Replace("'", "''"), ddlGrade.SelectedValue.Trim().Replace("'", "''"), Me, txtmob.Text.Trim().Replace("'", "''"), ddldesn.SelectedItem.Text, ddldept.SelectedValue, ddlcountry.SelectedValue, ddlTimeZone.SelectedValue, ddlCostcenter.SelectedValue, ddlVertical.SelectedValue)
        'obj.BindDepartment(ddldept)
        obj.Bindlocation(ddldg)
        'obj.BindDesig1(ddldesg)


    End Sub

    Private Sub Cleardata()
        ddltitle.SelectedIndex = 0
        txtfname.Text = ""
        txtmname.Text = ""
        txtlname.Text = ""
        txtmailid.Text = ""
        txtaurid.Text = ""

        ddlcountry.SelectedIndex = 0
        ddlTimeZone.SelectedIndex = 0
        ddlcity.SelectedIndex = 0
        ddlGrade.ClearSelection()
        ddldg.SelectedIndex = 0
        ddldept.SelectedIndex = 0
        ddldesn.SelectedIndex = 0
        ddlrepmgr.SelectedIndex = 0
        ddlCostcenter.SelectedIndex = 0
        txtAddress.Text = ""
        txtmob.Text = ""
        txtpwd.Text = ""
        txtcpwd.Text = ""
        'txtDOJ.Text = ""
        'lblMsg.Text = ""
    End Sub


    Protected Sub txtpwd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtpwd.TextChanged

    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        'Response.Redirect("~/AdminFunctions/Adf_WEBFILES/frmADFsearch.aspx")
        Response.Redirect("~/AdminFunctions/Adf_WEBFILES/frmUserRoleMapping.aspx")
    End Sub

    Protected Sub ddlcountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcountry.SelectedIndexChanged
        If ddlcountry.SelectedIndex > 0 Then
            BindCity()
            BindTimeZones()
        Else
            ddlcity.Items.Clear()
            ' ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlcity.SelectedIndex = 0
        End If

    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlcountry.SelectedIndex > 0 And ddlcity.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETLOC")
            sp.Command.AddParameter("@city", ddlcity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@country", ddlcountry.SelectedItem.Value, DbType.String)
            ddldg.DataSource = sp.GetDataSet()
            ddldg.DataTextField = "LCM_NAME"
            ddldg.DataValueField = "LCM_CODE"
            ddldg.DataBind()
            ddldg.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        If (ddlVertical.SelectedIndex > 0) Then
            Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
            sp1.Value = ddlVertical.SelectedValue
            ddlCostcenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvscostcenter", sp1)
            ddlCostcenter.DataTextField = "cost_center_name"
            ddlCostcenter.DataValueField = "cost_center_code"
            ddlCostcenter.DataBind()
            ddlCostcenter.Items.Insert(0, "--Select--")
        Else
            ddlCostcenter.SelectedIndex = 0
            ddlCostcenter.Items.Clear()
        End If
    End Sub

    Protected Sub ddltitle_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltitle.SelectedIndexChanged
        lbl1.Text = ""
    End Sub
End Class
