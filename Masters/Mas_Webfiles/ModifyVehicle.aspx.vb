﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Masters_Mas_Webfiles_ModifyVehicle
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim id As String = Request.QueryString("SNO")
            lblMsg.Visible = False
            lblTemp.Text = id
            BindRCBook()
            BindTAXINVOICE()
            BindPolicy()
            BindStatus()
            dispdata()
            pnlInfo.Visible = True
        End If
    End Sub


    Private Sub BindStatus()
        ObjSubSonic.Binddropdown(ddlVehStatus, "GETVEHICLESTATUS", "VHL_STATUS_TEXT", "VHL_STATUS_CODE")
    End Sub
    Private Sub BindPolicy()
        ObjSubSonic.Binddropdown(ddlInsPolicy, "GETVEHICLEPOLICYSTATUS", "VHL_INSPOLICY_STATUS_TEXT", "VHL_INSPOLICY_STATUS_CODE")
    End Sub

    Private Sub BindTAXINVOICE()
        ObjSubSonic.Binddropdown(ddlTaxInvoice, "GETVEHICLETAXINVOICESTATUS", "TAX_INVOICE_TEXT", "TAX_INVOICE_CODE")
    End Sub

    Private Sub BindRCBook()
        ObjSubSonic.Binddropdown(ddlRCBookStatus, "GETVEHICLERCBOOKSTATUS", "RC_BOOK_TEXT", "RC_BOOK_CODE")
    End Sub

    Private Sub dispdata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DISPLAY_VEHICLE_DETAILS")
        sp.Command.AddParameter("@SNO", Request.QueryString("SNO"), DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtPODate.Text = ds.Tables(0).Rows(0).Item("VHL_POCAP_DT")
            txtInvDate.Text = ds.Tables(0).Rows(0).Item("VHL_INV_DT")
            txtInvNo.Text = ds.Tables(0).Rows(0).Item("VHL_INV_NO")
            txtAssetModel.Text = ds.Tables(0).Rows(0).Item("VHL_ASSET_MODEL")
            txtRegNo.Text = ds.Tables(0).Rows(0).Item("VHL_REG_NO")
            txtEngnNo.Text = ds.Tables(0).Rows(0).Item("VHL_ENG_NO")
            txtChasisNo.Text = ds.Tables(0).Rows(0).Item("VHL_CHS_NO")
            txtCC.Text = ds.Tables(0).Rows(0).Item("VHL_CC")
            txtNewLimit.Text = ds.Tables(0).Rows(0).Item("VHL_NEW_LIMIT")
            txtTotVehicleCost.Text = ds.Tables(0).Rows(0).Item("VHL_TOT_VCOST")
            txtRegDate.Text = ds.Tables(0).Rows(0).Item("VHL_REG_DT")
            txtDOFReplacement.Text = ds.Tables(0).Rows(0).Item("VHL_REPLCMNT_DT")
            txtFuelType.Text = ds.Tables(0).Rows(0).Item("VHL_FUEL_TYPE")
            txtOrgBuyer.Text = ds.Tables(0).Rows(0).Item("VHL_ORG_BUYER")
            txtYOP.Text = ds.Tables(0).Rows(0).Item("VHL_YOF_PURCHASE")
            txtDOP.Text = ds.Tables(0).Rows(0).Item("VHL_DISPOSAL_DT")
            txtMOR.Text = ds.Tables(0).Rows(0).Item("VHL_MNTH_RETIREMENT")
            txtInsPlcyNo.Text = ds.Tables(0).Rows(0).Item("VHL_INS_NO")
            txtInsFrmDate.Text = ds.Tables(0).Rows(0).Item("VHL_INSFRM_DT")
            txtInsToDate.Text = ds.Tables(0).Rows(0).Item("VHL_INSTO_DT")
            txtYOE.Text = ds.Tables(0).Rows(0).Item("VHL_YR_EXP")
            ddlVehStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("VHL_STATUS")).Selected = True
            ddlTaxInvoice.Items.FindByValue(ds.Tables(0).Rows(0).Item("VHL_TAX_INVOICE")).Selected = True
            ddlInsPolicy.Items.FindByValue(ds.Tables(0).Rows(0).Item("VHL_INSPOLICY_STATUS")).Selected = True
            ddlRCBookStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("VHL_RC_BOOK")).Selected = True
            txtFileNo.Text = ds.Tables(0).Rows(0).Item("VHL_FILE_NO")
            txtRemarks.Text = ds.Tables(0).Rows(0).Item("VHL_REM")
            'txtStatus.Text = ds.Tables(0).Rows(0).Item("VHL_STATUS")
            'txtStatusIns.Text = ds.Tables(0).Rows(0).Item("VHL_INSPOLICY_STATUS")
            txtFillingStatus.Text = ds.Tables(0).Rows(0).Item("VHL_FILLING_STATUS")

        End If
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim rtrnurl As String = ""
        rtrnurl = Request.QueryString("rtnurl")
        If rtrnurl = "" Then
            Response.Redirect("frmEditVehicle.aspx")
        Else
            Response.Redirect(rtrnurl)
        End If

    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        modifydata()
    End Sub
    Private Sub modifydata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_VEHICLE_DATA")
        sp.Command.AddParameter("@SNO", Request.QueryString("SNO"), DbType.Int32)
        sp.Command.AddParameter("@VHL_POCAP_DT", txtPODate.Text, DbType.Date)
        sp.Command.AddParameter("@VHL_INV_DT", txtInvDate.Text, DbType.Date)
        sp.Command.AddParameter("@VHL_INV_NO", txtInvNo.Text, DbType.String)
        sp.Command.AddParameter("@VHL_ASSET_MODEL", txtAssetModel.Text, DbType.String)
        sp.Command.AddParameter("@VHL_REG_NO", txtRegNo.Text, DbType.String)
        sp.Command.AddParameter("@VHL_ENG_NO", txtEngnNo.Text, DbType.String)
        sp.Command.AddParameter("@VHL_CHS_NO", txtChasisNo.Text, DbType.String)
        sp.Command.AddParameter("@VHL_CC", txtCC.Text, DbType.String)
        sp.Command.AddParameter("@VHL_NEW_LIMIT", txtNewLimit.Text, DbType.String)
        sp.Command.AddParameter("@VHL_TOT_VCOST", txtTotVehicleCost.Text, DbType.String)
        sp.Command.AddParameter("@VHL_REG_DT", txtRegDate.Text, DbType.Date)
        sp.Command.AddParameter("@VHL_REPLCMNT_DT", txtDOFReplacement.Text, DbType.Date)
        sp.Command.AddParameter("@VHL_FUEL_TYPE", txtFuelType.Text, DbType.String)
        sp.Command.AddParameter("@VHL_ORG_BUYER", txtOrgBuyer.Text, DbType.String)
        sp.Command.AddParameter("@VHL_YOF_PURCHASE", txtYOP.Text, DbType.String)
        sp.Command.AddParameter("@VHL_DISPOSAL_DT", txtDOP.Text, DbType.Date)
        sp.Command.AddParameter("@VHL_MNTH_RETIREMENT", txtMOR.Text, DbType.String)
        sp.Command.AddParameter("@VHL_INS_NO", txtInvNo.Text, DbType.String)
        sp.Command.AddParameter("@VHL_INSFRM_DT", txtInsFrmDate.Text, DbType.Date)
        sp.Command.AddParameter("@VHL_INSTO_DT", txtInsToDate.Text, DbType.Date)
        sp.Command.AddParameter("@VHL_YR_EXP", txtYOE.Text, DbType.String)
        sp.Command.AddParameter("@VHL_RC_BOOK", ddlRCBookStatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VHL_TAX_INVOICE", ddlTaxInvoice.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VHL_FILE_NO", txtFileNo.Text, DbType.String)
        sp.Command.AddParameter("@VHL_REM", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@VHL_STATUS", ddlVehStatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VHL_INSPOLICY_STATUS", ddlInsPolicy.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@VHL_FILLING_STATUS", txtFillingStatus.Text, DbType.String)
        sp.ExecuteScalar()
        lblMsg.Visible = True
        lblMsg.Text = "Vehilce details modified successfully..."

    End Sub
 
End Class
