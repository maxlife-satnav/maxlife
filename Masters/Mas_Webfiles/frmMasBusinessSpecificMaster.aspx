﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmMasBusinessSpecificMaster.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasBusinessSpecificMaster" %>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>    
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Business Specific Master
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-md-6">
                                <label class="col-md-2 btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                        ToolTip="Please Select Add to add new Business Specific Master and Select Modify to modify the existing Business Specific Master" />
                                    Add
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                        ToolTip="Please Select Add to add new Business Specific Master and Select Modify to modify the existing Business Specific Master" />
                                    Modify
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Business Specific Parent<span style="color: red;">*</span></label>
                                        <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtParent"
                                            Display="none" ErrorMessage="Please Enter Business Specific Parent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtParent" runat="server" CssClass="form-control" Width="99%"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Business Specific Child<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtChild"
                                            Display="none" ErrorMessage="Please Enter Business Specific Child" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtChild" runat="server" CssClass="form-control" Width="99%"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/Masters/Mas_Webfiles/frmMasBusinessSpecificMaster.aspx" CausesValidation="False" />                               
                                </div>
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                                <asp:GridView ID="gvLocation" runat="server" AllowPaging="True" AllowSorting="False" EmptyDataText="No Business Specific Found."
                                    RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" Width="100%"
                                    PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("AMT_BSM_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Specific Parent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("AMT_BSM_PARENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Specific Child">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AMT_BSM_CHILD")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
