﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Zone.aspx.cs" Inherits="Masters_Mas_Webfiles_Zone" %>

<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
      <script type="text/javascript">
          function maxLength(s, args) {
              if (args.Value.length >= 500)
                  args.IsValid = false;
          }
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Zone Master</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-md-6">
                                <label class="col-md-2 btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                        ToolTip="Please Select Add to add new Zone and Select Modify to modify the existing Zone" OnCheckedChanged="rbActionsModify_CheckedChanged"/>
                                    Add
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                        ToolTip="Please Select Add to add new Zone and Select Modify to modify the existing Zone" OnCheckedChanged="rbActionsModify_CheckedChanged" />
                                    Modify
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trSName" runat="server">
                                        <label class="col-md-4 control-label">Zone <span style="color: red;">*</span></label>

                                        <asp:RequiredFieldValidator ID="rfvctry" runat="server" ErrorMessage="Please Select Zone "
                                            InitialValue="--Select--" ControlToValidate="ddlZone" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlZone" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Zone Name" OnSelectedIndexChanged="ddlZone_SelectedIndexChanged">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Zone Code<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="revCode" runat="server" ControlToValidate="txtZCode"
                                            Display="None" ErrorMessage="Please enter Zone code in alphabets and numbers, upto 15 characters allowed"
                                            ValidationExpression="^[A-Za-z0-9]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvcode" runat="server" ControlToValidate="txtZCode"
                                            Display="None" ErrorMessage="Please Enter Zone Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtZCode" runat="server" MaxLength="15" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Zone Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtZoneName"
                                            Display="None" ErrorMessage="Please Enter Zone Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="txtZoneName"
                                            Display="None" ErrorMessage="Please Enter Zone Name in alphabets and numbers and (space,-,_ ,(,) allowed), upto 50 characters allowed"
                                            ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Zone Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtZoneName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">                            
                             <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="Div2" runat="server">
                                        <label class="col-md-4 control-label"> Country <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Country "
                                            InitialValue="--Select--" ControlToValidate="ddlCny" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCny" runat="server" AutoPostBack="false" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Country Name">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="500"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true" ValidationGroup="Val1" OnClick="btnSubmit_Click" ></asp:Button>
                                    <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" OnClick="btnback_Click"></asp:Button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Zone Found."
                                    CssClass="table table-condensed table-bordered table-hover table-striped"  OnRowCommand="gvItem_RowCommand" OnPageIndexChanging="gvItem_PageIndexChanging">
                                    <Columns>
                                       
                                         <asp:BoundField DataField="ZN_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Zone">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                         <asp:BoundField DataField="CNY_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Country">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                          <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkStatus" runat="server" Text='<%#Eval("Status")%>' CommandName="Status"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Zone Code" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblZN_Code" Text='<%#Eval("ZN_CODE")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status Code" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus_Code" Text='<%#Eval("ZN_STA_ID")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>

</html>
