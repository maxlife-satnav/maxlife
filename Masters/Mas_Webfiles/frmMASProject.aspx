<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMASProject.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel id="updatepanle1" runat="server">
        <contenttemplate>
<SCRIPT language="javascript" type="text/javascript"> 
     function maxLength(s,args)
     { 
     if(args.Value.length >= 500)
     args.IsValid=false;
     }

    </SCRIPT>

<SCRIPT language="javascript" src="../../Scripts/wz_tooltip.js" type="text/javascript"></SCRIPT>
<DIV><TABLE id="table1" cellSpacing=0 cellPadding=0 width="100%" align=center border=0><TBODY><TR><TD align=center width="100%"><asp:Label id="lblHead" runat="server" __designer:wfdid="w117" CssClass="clsHead" Width="86%" Font-Underline="False" ForeColor="Black">Project
             <hr align="center" width="60%" /></asp:Label></TD></TR></TBODY></TABLE><asp:Panel id="PNLCONTAINER" runat="server" __designer:wfdid="w118" Width="85%" Height="100%"><TABLE style="VERTICAL-ALIGN: top" id="table3" cellSpacing=0 cellPadding=0 width="95%" align=center border=0><TBODY><TR><TD align=left colSpan=3><asp:Label id="LBLNOTE" runat="server" __designer:wfdid="w119" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label> </TD></TR><TR><TD><IMG height=27 src="../../Images/table_left_top_corner.gif" width=9 /></TD><TD class="tableHEADER" align=left width="100%">&nbsp;<STRONG>Project Details</STRONG></TD><TD><IMG height=27 src="../../Images/table_right_top_corner.gif" width=16 /></TD></TR><TR><TD background="../../Images/table_left_mid_bg.gif">&nbsp;</TD><TD align=center><asp:ValidationSummary id="ValidationSummary1" runat="server" __designer:wfdid="w120" CssClass="clsMessage" ForeColor="" ValidationGroup="Val1"></asp:ValidationSummary><asp:ValidationSummary id="ValidationSummary2" runat="server" __designer:wfdid="w121" CssClass="divmessagebackground" ForeColor="" ValidationGroup="get"></asp:ValidationSummary> &nbsp;<BR />&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; <asp:Label id="lblError" runat="server" __designer:wfdid="w122" CssClass="clsMessage" Visible="False"></asp:Label><BR /><TABLE id="table2" cellSpacing=1 cellPadding=1 width="100%" border=1><TBODY><TR><TD style="HEIGHT: 43px" align=center colSpan=2><asp:RadioButtonList id="rbActions" runat="server" __designer:wfdid="w123" CssClass="clsRadioButton" RepeatDirection="Horizontal" AutoPostBack="True">
                                            <asp:ListItem Value="0" Selected="True">Add</asp:ListItem>
                                            <asp:ListItem Value="1">Modify</asp:ListItem>
                                        </asp:RadioButtonList><asp:TextBox id="txtProjC" tabIndex=3 runat="server" __designer:wfdid="w124" CssClass="clsTextField" Width="5%" ValidationGroup="get" Visible="False" MaxLength="50"></asp:TextBox><asp:Button id="btnGet" runat="server" Text="Get" __designer:wfdid="w125" CssClass="button" ValidationGroup="get" Visible="False"></asp:Button> </TD></TR><TR id="trProjCode" runat="server"><TD style="WIDTH: 50%" class="clslabel" align=left>&nbsp;Project <SPAN style="COLOR: red">*<asp:RequiredFieldValidator id="rfvPrjCode" runat="server" __designer:wfdid="w126" ValidationGroup="get" ControlToValidate="txtProjC" Display="None" ErrorMessage="Please enter Project Code/Name"></asp:RequiredFieldValidator></SPAN></TD><TD style="WIDTH: 43%" align=left><asp:DropDownList id="ddlProjectName" tabIndex=2 runat="server" __designer:wfdid="w127" CssClass="clsComboBox" Width="97%" AutoPostBack="True">
                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                    </asp:DropDownList> <%-- <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtProjC"
                                            MinimumPrefixLength="1" ServiceMethod="GetProjects" CompletionSetCount="0"
                                            UseContextKey="true" ContextKey="0" FirstRowSelected="true">
                                        </ajaxToolkit:AutoCompleteExtender>--%></TD></TR><TR><TD style="WIDTH: 50%" class="clslabel" align=left>&nbsp;Project Code&nbsp;<FONT class="clsNote">*</FONT> <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtProjectCode"
                                            Display="None" ErrorMessage="Please enter Project code in alphabets and numbers, upto 15 characters allowed"
                                            ValidationExpression="^[A-Za-z0-9-_]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>--%><asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" __designer:wfdid="w128" ValidationGroup="Val1" ControlToValidate="txtProjectCode" Display="None" ErrorMessage="Please enter Project Code"></asp:RequiredFieldValidator> &nbsp;</TD><TD style="WIDTH: 43%" align=left><DIV onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()"><asp:TextBox id="txtProjectCode" runat="server" __designer:wfdid="w129" CssClass="clsTextField" Width="97%" MaxLength="15"></asp:TextBox> </DIV></TD></TR><TR><TD style="WIDTH: 50%" class="clslabel" align=left>&nbsp;Project Name&nbsp;<FONT class="clsNote">*</FONT> <asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" __designer:wfdid="w130" ValidationGroup="Val1" ControlToValidate="txtProjectName" Display="None" ErrorMessage="Please Enter Project Name in alphabets and numbers and (space,-,_allowed)" ValidationExpression="^[A-Za-z0-9-_ ]+"></asp:RegularExpressionValidator> <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" __designer:wfdid="w131" ValidationGroup="Val1" ControlToValidate="txtProjectName" Display="None" ErrorMessage="Please enter Project Name"></asp:RequiredFieldValidator> &nbsp; &nbsp;</TD><TD style="WIDTH: 43%" align=left><DIV onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ allowed) and upto 50 characters allowed')" onmouseout="UnTip()"><asp:TextBox id="txtProjectName" tabIndex=1 runat="server" __designer:wfdid="w132" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox> </DIV></TD></TR><TR><TD style="WIDTH: 50%" class="clslabel" align=left>&nbsp; vertical&nbsp;<FONT class="clsNote">*</FONT> <asp:RequiredFieldValidator id="rfvVertical" runat="server" __designer:wfdid="w133" ValidationGroup="Val1" ControlToValidate="ddlVertical" Display="None" ErrorMessage="Please select  vertical" InitialValue="--Select--"></asp:RequiredFieldValidator></TD><TD style="WIDTH: 43%" align=left><asp:DropDownList id="ddlVertical" tabIndex=2 runat="server" __designer:wfdid="w134" CssClass="clsComboBox" Width="97%" AutoPostBack="True">
                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 50%" class="clslabel" align=left>&nbsp;Cost Center&nbsp;<FONT class="clsNote">*</FONT> <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" __designer:wfdid="w135" ValidationGroup="Val1" ControlToValidate="ddlCostCenter" Display="None" ErrorMessage="Please select Cost Center" InitialValue="--Select--"></asp:RequiredFieldValidator></TD><TD style="WIDTH: 43%" align=left><asp:DropDownList id="ddlCostCenter" tabIndex=2 runat="server" __designer:wfdid="w136" CssClass="clsComboBox" Width="97%">
                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                        </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 50%" class="clslabel" align=left>&nbsp;Project Head <FONT class="clsNote">*</FONT> <asp:RequiredFieldValidator id="rfvHead" runat="server" __designer:wfdid="w137" ValidationGroup="Val1" ControlToValidate="txtEmployee" Display="None" ErrorMessage="Please enter Project Head"></asp:RequiredFieldValidator></TD><TD style="WIDTH: 43%" align=left><%--<asp:DropDownList ID="ddlProjHead" runat="server" Width="97%"
                                            CssClass="clsComboBox" TabIndex="2" AutoPostBack="True">
                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                    </asp:DropDownList>&nbsp;--%><asp:TextBox id="txtEmployee" tabIndex=3 runat="server" __designer:wfdid="w138" CssClass="clsTextField" Width="80%" MaxLength="50"></asp:TextBox> &nbsp;&nbsp;<asp:LinkButton id="lnkHelp" runat="server" Text="Help" CausesValidation="False" __designer:wfdid="w139"></asp:LinkButton> <%--   <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtEmployee"
                                            MinimumPrefixLength="1" ServiceMethod="GetEmployeeList" CompletionSetCount="0"
                                            UseContextKey="true" ContextKey="0" FirstRowSelected="true">
                                        </ajaxToolkit:AutoCompleteExtender>--%></TD></TR><TR><TD style="WIDTH: 50%; HEIGHT: 16px" class="clslabel" align=left>&nbsp;Remarks<FONT class="clsNote">*</FONT> <asp:CustomValidator id="CustomValidator1" runat="server" __designer:wfdid="w140" ValidationGroup="Val1" ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters !" ClientValidationFunction="maxLength"></asp:CustomValidator> <asp:RequiredFieldValidator id="rfvRemarks" runat="server" __designer:wfdid="w141" ValidationGroup="Val1" ControlToValidate="txtRemarks" Display="None" ErrorMessage="Please enter Remarks"></asp:RequiredFieldValidator> </TD><TD style="WIDTH: 43%" align=left><DIV onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()"><asp:TextBox id="txtRemarks" tabIndex=4 runat="server" __designer:wfdid="w142" CssClass="clsTextField" Width="97%" Height="35px" MaxLength="500" TextMode="MultiLine"></asp:TextBox> </DIV></TD></TR></TBODY></TABLE><TABLE style="HEIGHT: 22px" class="table" width="100%" border=1><TBODY><TR><TD style="HEIGHT: 20px" align=center>&nbsp;  <asp:Button id="btnback" tabIndex=5 runat="server" Text="Back" __designer:wfdid="w144" CssClass="button" Width="76px">
                                        </asp:Button><asp:Button id="btnSubmit" tabIndex=5 runat="server" Text="Submit" __designer:wfdid="w143" CssClass="button" Width="76px" ValidationGroup="Val1">
                                        </asp:Button>&nbsp;</TD></TR><TR><TD style="HEIGHT: 20px" align=center>&nbsp; &nbsp; </TD></TR><TR><TD style="HEIGHT: 20px" align=center><asp:GridView id="gvItem" runat="server" __designer:wfdid="w145" Width="100%" AutoGenerateColumns="False" PageSize="20" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Project Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblProjName" Text='<% #bind("PRJ_NAME") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblProjCode" Text='<% #bind("prj_code") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText=" vertical Name">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblVerName" Text='<% #bind("ver_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkStatus" Text='<% #bind("status") %>' CommandArgument='<%#BIND("prj_sta_id") %>'
                                                            OnClick="lnkStatus_Click" CausesValidation="False"></asp:LinkButton>
                                                        <asp:Label runat="server" ID="Label2" Text='<% #bind("prj_sta_id") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView> </TD></TR></TBODY></TABLE><BR /></TD><TD style="WIDTH: 10px; HEIGHT: 100%" background="../../Images/table_right_mid_bg.gif">&nbsp;</TD></TR><TR><TD style="WIDTH: 10px; HEIGHT: 17px"><IMG height=17 src="../../Images/table_left_bot_corner.gif" width=9 /></TD><TD style="HEIGHT: 17px" background="../../Images/table_bot_mid_bg.gif"><IMG height=17 src="../../Images/table_bot_mid_bg.gif" width=25 /></TD><TD style="HEIGHT: 17px"><IMG height=17 src="../../Images/table_right_bot_corner.gif" width=16 /></TD></TR></TBODY></TABLE></asp:Panel> </DIV>
</contenttemplate>
        <triggers>
<asp:PostBackTrigger ControlID="btnSubmit"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="lnkHelp"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="gvItem"></asp:PostBackTrigger>
</triggers>
    </asp:UpdatePanel>
</asp:Content>
