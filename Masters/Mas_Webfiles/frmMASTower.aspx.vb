Imports System.Data
Imports System.Data.SqlClient

Partial Class Masters_Mas_Webfiles_frmMASTower
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters

    Public Sub Cleardata()
        txtTowercode.Text = String.Empty
        txtTowerName.Text = String.Empty
        ddlCityname.SelectedIndex = 0
        ddlCountryName.SelectedIndex = 0
        ddlState.SelectedIndex = 0
        ddlZone.SelectedIndex = 0
        ddlLName.SelectedIndex = -1
        ddlTName.SelectedIndex = -1
        txtRemarks.Text = String.Empty
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                'Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
                'Dim host As String = HttpContext.Current.Request.Url.Host
                'Dim param(1) As SqlParameter
                'param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
                'param(0).Value = Session("UID")
                'param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
                'param(1).Value = path
                'Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                '    If sdr.HasRows Then
                '    Else
                '        Response.Redirect(Application("FMGLogout"))
                '    End If
                'End Using
                obj.Tower_LoadGrid(gvItem)
                trLName.Visible = False
                rbActions.Checked = True
                obj.BindCity(ddlCityname)
                obj.BindCountry(ddlCountryName)
                obj.Bindlocation(ddlLName)
                GetZone()
                GetState()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "Page_Load", exp)
        End Try
    End Sub

    Private Sub Modifydata()
        obj.getname = txtTowerName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyTower(ddlTName, ddlCountryName, ddlCityname, ddlLName, ddlState, ddlZone, Me) = 1) Then
            Cleardata()
            lblMsg.Text = "Tower Updated Successfully"
            lblMsg.Visible = True
        End If
        obj.BindCity(ddlCityname)
        obj.BindCountry(ddlCountryName)
        obj.Bindlocation(ddlLName)
        obj.BindTower(ddlTName)
        obj.Tower_LoadGrid(gvItem)
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtTowercode.Text
        obj.getname = txtTowerName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.InsertTower(ddlCountryName, ddlCityname, ddlLName, ddlZone, ddlState, Me) = 0) Then
            Cleardata()
            lblMsg.Text = "Tower Inserted Successfully"
            lblMsg.Visible = True
        ElseIf (obj.InsertTower(ddlCountryName, ddlCityname, ddlLName, ddlZone, ddlState, Me) = 2) Then
            Cleardata()
            lblMsg.Text = "Tower Inserted Successfully"
            lblMsg.Visible = True
        ElseIf (obj.InsertTower(ddlCountryName, ddlCityname, ddlLName, ddlZone, ddlState, Me) = 1) Then
            lblMsg.Text = "Tower Code already exists"
            lblMsg.Visible = True
        End If
        obj.BindCity(ddlCityname)
        obj.BindCountry(ddlCountryName)
        obj.Bindlocation(ddlLName)
        obj.Tower_LoadGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        ddlCityname.Enabled = True
        ddlCountryName.Enabled = True
        ddlLName.Enabled = True
        Try
            If rbActions.Checked = True Then
                trLName.Visible = False
                txtTowercode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                Cleardata()
                trLName.Visible = True
                txtTowercode.ReadOnly = True
                ddlCityname.Enabled = False
                ddlCountryName.Enabled = False
                ddlLName.Enabled = False
                btnSubmit.Text = "Modify"
                obj.BindTower(ddlTName)
                obj.BindCity(ddlCityname)
                obj.BindCountry(ddlCountryName)
                obj.Bindlocation(ddlLName)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASTower", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlTName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTName.SelectedIndexChanged
        Try
            If ddlTName.SelectedItem.Value <> "--Select--" Then
                obj.Tower_SelectedIndex_Changed(ddlTName, ddlCountryName, ddlCityname, ddlLName, ddlZone, ddlState)
                txtTowercode.Text = obj.getcode
                txtTowerName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
                ddlState.Enabled = False
                ddlZone.Enabled = False
            Else
                Cleardata()
                obj.BindTower(ddlTName)
                obj.BindCountry(ddlCountryName)
                obj.BindCity(ddlCityname)
                obj.Bindlocation(ddlLName)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "ddlTName_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Tower_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Tower_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(7).Visible = False
                    gvItem.HeaderRow.Cells(8).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(7).Visible = False
                        gvItem.Rows(i).Cells(8).Visible = False
                    Next
                    lblMsg.Text = "First inactivate all the floors under this Tower !"
                    lblMsg.Visible = True
                    Exit Sub
                End If
                obj.Tower_LoadGrid(gvItem)
            End If
            obj.BindTower(ddlTName)
            obj.Bindlocation(ddlLName)
            obj.BindCity(ddlCityname)
            obj.BindCountry(ddlCountryName)
            Cleardata()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASTower", "gvItem_RowCommand", exp)
        End Try

    End Sub

    Protected Sub ddlLName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLName.SelectedIndexChanged
        Try
            If (ddlLName.SelectedIndex <> 0) Then
                Dim dr As SqlDataReader = obj.tower_getloc_dtls(ddlLName.SelectedValue)
                If (dr.Read) Then
                    ddlCityname.SelectedValue = dr("lcm_cty_id")
                    ddlCountryName.SelectedValue = dr("lcm_cny_id")
                    ddlState.SelectedValue = dr("LCM_STATE_ID")
                    ddlZone.SelectedValue = dr("LCM_ZN_ID")
                    ddlCityname.Enabled = False
                    ddlCountryName.Enabled = False
                    ddlState.Enabled = False
                    ddlZone.Enabled = False
                End If
            Else
                ddlCityname.SelectedIndex = 0
                ddlCountryName.SelectedIndex = 0
                ddlState.SelectedIndex = 0
                ddlZone.SelectedIndex = 0
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "ddlLName_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub

    Private Sub GetZone()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ZONE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlZone.DataSource = sp.GetDataSet()
            ddlZone.DataTextField = "ZN_NAME"
            ddlZone.DataValueField = "ZN_CODE"
            ddlZone.DataBind()
            ddlZone.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetState()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_STATE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlState.DataSource = sp.GetDataSet()
            ddlState.DataTextField = "STE_NAME"
            ddlState.DataValueField = "STE_CODE"
            ddlState.DataBind()
            ddlState.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
