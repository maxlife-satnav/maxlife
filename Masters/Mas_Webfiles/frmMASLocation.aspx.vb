Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_MAS_WebFiles_frmLocation
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Private Sub Cleardata()
        txtLocationcode.Text = String.Empty
        txtLocationName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlCity.SelectedIndex = 0
        ddlState.SelectedIndex = 0
        ddlZone.SelectedIndex = 0
        ddlCountry.SelectedIndex = 0
        ddlLocation.SelectedIndex = -1
        cmbstatus.SelectedIndex = 1
        txtDate.Text = String.Empty
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtLocationcode.Text
        obj.getname = txtLocationName.Text
        obj.getRemarks = txtRemarks.Text
        Dim iStatus As Integer = obj.insertlocation(ddlCity, ddlCountry, txtDate, cmbstatus, ddlState, ddlZone, Me)
        If iStatus = 1 Then
            lblMsg.Text = "Location Code Already Exists "
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "Location inserted Successfully "
            lblMsg.Visible = True
            Cleardata()
        ElseIf iStatus = 0 Then
            lblMsg.Text = "Location inserted Successfully "
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.Location_LoadGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getname = txtLocationName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.Modifyloc(ddlLocation, ddlCity, ddlCountry, txtDate, cmbstatus, ddlState, ddlZone, Me) > 0) Then
            lblMsg.Text = "Location Updated Successfully "
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.Location_LoadGrid(gvItem)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtDate.Attributes.Add("readonly", "readonly")
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                obj.Location_LoadGrid(gvItem)
                trLName.Visible = False
                rbActions.Checked = True
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                GetZone()
                GetState()
                txtDate.Attributes.Add("onClick", "displayDatePicker('" + txtDate.ClientID + "')")
                txtDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        ddlCity.Enabled = True
        ddlCountry.Enabled = True
        Try
            If rbActions.Checked = True Then
                trLName.Visible = False
                txtLocationcode.ReadOnly = False
                btnSubmit.Text = "Submit"
                trLName.Visible = False
                Cleardata()
            Else
                Cleardata()
                trLName.Visible = True
                txtLocationcode.ReadOnly = True

                ddlCity.Enabled = False
                ddlCountry.Enabled = False
                btnSubmit.Text = "Modify"
                Cleardata()
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata()
                obj.Bindlocation(ddlLocation)
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
                obj.Bindlocation(ddlLocation)
            End If
            obj.Location_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmLocation", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedItem.Value <> "--Select--" Then
               obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                obj.Loc_Selectedindex_changed(ddlLocation, ddlCity, ddlCountry, txtDate, cmbstatus, ddlState, ddlZone)
                txtLocationcode.Text = obj.getcode
                txtLocationName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
                ddlState.Enabled = False
            Else
                Cleardata()
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "ddlLocation_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Location_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Location_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(8).Visible = False
                    gvItem.HeaderRow.Cells(9).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(8).Visible = False
                        gvItem.Rows(i).Cells(9).Visible = False
                    Next
                    lblMsg.Text = "First inactivate all towers under this location "
                    lblMsg.Visible = True
                    Exit Sub
                End If
                obj.Location_LoadGrid(gvItem)
                Cleardata()
            End If
            obj.BindCity(ddlCity)
            obj.BindCountry(ddlCountry)
            obj.Bindlocation(ddlLocation)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmLocation", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            If (ddlCity.SelectedValue <> "--Select--") Then
                Dim dr As SqlDataReader = obj.location_getcity_dtls(ddlCity.SelectedValue)
                If (dr.Read) Then
                    ddlCountry.SelectedValue = dr("CTY_CNY_ID")
                    ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(dr("CTY_STATE_ID")))
                    ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(dr("CTY_ZN_ID")))
                    ddlZone.Enabled = False
                    ddlState.Enabled = False
                    ddlCountry.Enabled = False
                End If
            Else
                ddlCountry.SelectedValue = "--Select--"
                ddlState.SelectedValue = "--Select--"
                ddlZone.SelectedValue = "--Select--"

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "ddlCity_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
    Private Sub GetZone()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ZONE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlZone.DataSource = sp.GetDataSet()
            ddlZone.DataTextField = "ZN_NAME"
            ddlZone.DataValueField = "ZN_CODE"
            ddlZone.DataBind()
            ddlZone.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub GetState()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_STATE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlState.DataSource = sp.GetDataSet()
            ddlState.DataTextField = "STE_NAME"
            ddlState.DataValueField = "STE_CODE"
            ddlState.DataBind()
            ddlState.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
