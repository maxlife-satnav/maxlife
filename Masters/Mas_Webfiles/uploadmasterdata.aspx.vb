﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Data.OleDb
Imports System.Collections.Generic
Partial Class Masters_Mas_Webfiles_uploadmasterdata
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack Then
            lblMsg.Text = ""
        End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1
              
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "country code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be COUNTRY CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "country name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be COUNTRY NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "zone code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be ZONE CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "zone name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be ZONE NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "state code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be STATE CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "state name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be STATE NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(7).ToString) <> "city code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CITY CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(8).ToString) <> "city name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CITY NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(9).ToString) <> "location code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be LOCATION CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(10).ToString) <> "location name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be LOCATION NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(11).ToString) <> "tower code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be TOWER CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(12).ToString) <> "tower name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be TOWER NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(13).ToString) <> "floor code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be FLOOR CODE	"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(14).ToString) <> "floor name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be FLOOR NAME	"
                            Exit Sub
                       
                        
                        ElseIf LCase(ds.Tables(0).Columns(15).ToString) <> "parent entity code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be PARENT ENTITY CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(16).ToString) <> "parent entity name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be PARENT ENTITY NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(17).ToString) <> "child entity code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CHILD ENTITY CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(18).ToString) <> "child entity name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CHILD ENTITY NAME"
                            Exit Sub
                        End If

                    Next
                Next
                Dim loc_codecnt As Integer = 1
                Dim loc_namecnt As Integer = 1
                Dim twr_codecnt As Integer = 1
                Dim twr_namecnt As Integer = 1
                Dim flr_codecnt As Integer = 1
                Dim flr_namecnt As Integer = 1
                Dim zn_codecnt As Integer = 1
                Dim zn_namecnt As Integer = 1
                Dim ste_codecnt As Integer = 1
                Dim ste_namecnt As Integer = 1
                Dim wng_codecnt As Integer = 1
                Dim wng_namecnt As Integer = 1
                Dim cty_codecnt As Integer = 1
                Dim cty_namecnt As Integer = 1
                Dim cny_codecnt As Integer = 1
                Dim cny_namecnt As Integer = 1
                Dim pe_codecnt As Integer = 1
                Dim pe_namecnt As Integer = 1
                Dim che_codecnt As Integer = 1
                Dim che_namecnt As Integer = 1
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'Start inseting excel data
                    Dim str1 As String = ds.Tables(0).Rows(i).Item("LOCATION NAME").ToString
                    If String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("LOCATION CODE").ToString(), " ", "")) Then
                        loc_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("LOCATION NAME").ToString(), " ", "")) Then
                        loc_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("TOWER CODE").ToString(), " ", "")) Then
                        twr_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("TOWER NAME").ToString(), " ", "")) Then
                        twr_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("FLOOR CODE").ToString(), " ", "")) Then
                        flr_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("FLOOR NAME").ToString(), " ", "")) Then
                        flr_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("CITY CODE").ToString(), " ", "")) Then
                        cty_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("CITY NAME").ToString(), " ", "")) Then
                        cty_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("COUNTRY CODE").ToString(), " ", "")) Then
                        cny_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("COUNTRY NAME").ToString(), " ", "")) Then
                        cny_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("ZONE CODE").ToString(), " ", "")) Then
                        zn_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("ZONE NAME").ToString(), " ", "")) Then
                        zn_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("STATE CODE").ToString(), " ", "")) Then
                        ste_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("STATE NAME").ToString(), " ", "")) Then
                        ste_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("PARENT ENTITY CODE").ToString(), " ", "")) Then
                        pe_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("PARENT ENTITY NAME").ToString(), " ", "")) Then
                        pe_namecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("CHILD ENTITY CODE").ToString(), " ", "")) Then
                        che_codecnt = 0
                    ElseIf String.IsNullOrEmpty(Replace(ds.Tables(0).Rows(i).Item("CHILD ENTITY NAME").ToString(), " ", "")) Then
                        che_namecnt = 0
                   
                    End If
                    If loc_codecnt <> 0 And loc_namecnt <> 0 And twr_namecnt <> 0 And twr_codecnt <> 0 And flr_codecnt <> 0 And flr_namecnt <> 0 And wng_codecnt <> 0 And wng_namecnt <> 0 And cty_codecnt <> 0 And cty_namecnt <> 0 And cny_codecnt <> 0 And cny_namecnt <> 0 And zn_codecnt <> 0 And zn_namecnt <> 0 And ste_codecnt <> 0 And ste_namecnt <> 0 Then
                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SPC_INSERT_ALL")
                        sp.Command.AddParameter("@LOCATION_CODE", ds.Tables(0).Rows(i).Item("LOCATION CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@LOCATION_NAME", ds.Tables(0).Rows(i).Item("LOCATION NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@TOWER_NAME", ds.Tables(0).Rows(i).Item("TOWER NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@TOWER_CODE", ds.Tables(0).Rows(i).Item("TOWER CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@FLOOR_NAME", ds.Tables(0).Rows(i).Item("FLOOR NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@FLOOR_CODE", ds.Tables(0).Rows(i).Item("FLOOR CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@CITY_CODE", ds.Tables(0).Rows(i).Item("CITY CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@CITY_NAME", ds.Tables(0).Rows(i).Item("CITY NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@COUNTRY_CODE", ds.Tables(0).Rows(i).Item("COUNTRY CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@COUNTRY_NAME", ds.Tables(0).Rows(i).Item("COUNTRY NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@PE_CODE", ds.Tables(0).Rows(i).Item("PARENT ENTITY CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@PE_NAME", ds.Tables(0).Rows(i).Item("PARENT ENTITY NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@CHE_CODE", ds.Tables(0).Rows(i).Item("CHILD ENTITY CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@CHE_NAME", ds.Tables(0).Rows(i).Item("CHILD ENTITY NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@ZN_NAME", ds.Tables(0).Rows(i).Item("ZONE NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@ZN_CODE", ds.Tables(0).Rows(i).Item("ZONE CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@STE_NAME", ds.Tables(0).Rows(i).Item("STATE NAME").ToString, DbType.String)
                        sp.Command.AddParameter("@STE_CODE", ds.Tables(0).Rows(i).Item("STATE CODE").ToString, DbType.String)
                        sp.Command.AddParameter("@USRID", Session("Uid"), DbType.String)
                        sp.ExecuteScalar()
                    End If
                Next
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."

            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please select file..."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~\Masters\MAS_Webfiles\frmMASMasters.aspx")
    End Sub
End Class
