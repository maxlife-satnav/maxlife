Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.IO
Imports System.Collections.Generic

Partial Class Masters_Mas_Webfiles_frm
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim obj As New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Text = ""
            lblLocationname.Text = "Select " + Session("Parent")
            lblHeader.Text = Session("Parent") + " Master"
            lblverticalCode.Text = Session("Parent") + " Code"
            rfvVerCode.ErrorMessage = "Please Enter " + lblverticalCode.Text
            rfvvertical1.ErrorMessage = "Please Select " + Session("Parent")
            lblverticalName.Text = Session("parent") + " Name"
            rfvVeName.ErrorMessage = "Please Enter " + lblverticalName.Text


            If Not Page.IsPostBack Then
                Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
                Dim host As String = HttpContext.Current.Request.Url.Host
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
                param(0).Value = Session("UID")
                param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
                param(1).Value = path
                Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End Using
                BindParentEntity()
                LoadVerticalGrid(gvItem)
                trVerName.Visible = False
                rbActions.Checked = True
                'obj.BindVertical(ddlVertical)
                BindVerticals()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "Page_Load", exp)
        End Try
    End Sub

    Private Sub Cleardata()
        txtVerticalCode.Text = String.Empty
        txtVerticalName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlVertical.SelectedIndex = 0
        ddlParent.SelectedIndex = 0
        ddlChild.SelectedIndex = 0
    End Sub
    Private Sub BindVerticals()
        BindCombo("GET_VERTICAL_DATA", ddlVertical, "VER_NAME", "VER_CODE")
        ddlVertical.SelectedIndex = 0
    End Sub
    Private Sub BindParentEntity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_ACTIVE_PARENT_ENTITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlParent.DataSource = sp.GetDataSet()
        ddlParent.DataTextField = "PE_NAME"
        ddlParent.DataValueField = "PE_CODE"
        ddlParent.DataBind()
        ddlParent.Items.Insert(0, "--Select--")
    End Sub
    Private Sub BindChildEntityByParent()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CHILD_ENTITY_BYPARENT")
        sp.Command.AddParameter("@PRNT_CODE", ddlParent.SelectedValue, DbType.String)
        ddlChild.DataSource = sp.GetDataSet()
        ddlChild.DataTextField = "CHE_NAME"
        ddlChild.DataValueField = "CHE_CODE"
        ddlChild.DataBind()
        ddlChild.Items.Insert(0, "--Select--")

    End Sub
    Private Sub insertdata()
        obj.getcode = txtVerticalCode.Text
        obj.getname = txtVerticalName.Text
        obj.getRemarks = txtRemarks.Text.Replace("'", "''")
        obj.getparententity = ddlParent.SelectedItem.Value
        obj.getchildentity = ddlChild.SelectedItem.Value

        Dim intStatus As Integer = obj.InsertVertical(Me, "INSERT")
        If intStatus = 1 Then
            lblMsg.Text = "The Vertical Code Already Exists, Please Modify Code."
        ElseIf intStatus = 2 Then
            lblMsg.Text = "The Vertical Name Already Exists, Please Modify Name."
        ElseIf intStatus = 3 Then
            lblMsg.Text = "Record has been inserted."
            Cleardata()
        End If
        LoadVerticalGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getcode = txtVerticalCode.Text.Trim()
        obj.getname = txtVerticalName.Text
        obj.getRemarks = txtRemarks.Text.Replace("'", "''")
        obj.getparententity = ddlParent.SelectedValue
        obj.getchildentity = ddlChild.SelectedValue

        Dim intStatus As Integer = obj.InsertVertical(Me, "MODIFY")
        If intStatus = 4 Then
            lblMsg.Text = "Record has been modified."
            Cleardata()
        Else
            lblMsg.Text = "Failed to modify."
        End If
        LoadVerticalGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trVerName.Visible = False
                txtVerticalCode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                Cleardata()
                trVerName.Visible = True
                txtVerticalCode.ReadOnly = True
                btnSubmit.Text = "Modify"
                Cleardata()
                BindVerticals()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If ddlVertical.SelectedItem.Text = "--Select--" Or txtVerticalCode.Text = String.Empty Or txtVerticalName.Text = String.Empty Then
                    lblMsg.Text = "Enter mandatory fields"
                ElseIf txtRemarks.Text.Length > 500 Then
                    lblMsg.Text = "Enter Remarks in less than or equal to 500 characters"
                Else
                    Modifydata()
                    BindVerticals()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASVertical", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        Try
            Dim strsql As String = String.Empty
            'Dim dr As SqlDataReader
            Dim iQry As Integer = 0
            If ddlVertical.SelectedValue <> "--Select--" Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_DROPDOWN_BY_VERTICAL")
                sp.Command.AddParameter("@VER_CODE", ddlVertical.SelectedItem.Value, DbType.String)
                Dim Ds As New DataSet
                Ds = sp.GetDataSet

                txtVerticalCode.Text = Ds.Tables(0).Rows(0)("VER_CODE")
                txtVerticalName.Text = Ds.Tables(0).Rows(0)("VER_NAME")
                BindParentEntity()
                ddlParent.SelectedValue = Ds.Tables(0).Rows(0)("VER_PE_CODE")
                BindChildEntityByParent()
                ddlChild.SelectedValue = Ds.Tables(0).Rows(0)("VER_CHE_CODE")
                txtRemarks.Text = Ds.Tables(0).Rows(0)("VER_REM")


                'obj.Vertical_SelectedIndex_Changed(ddlVertical)
                'txtVerticalCode.Text = obj.getcode
                'txtVerticalName.Text = obj.getname
                'txtRemarks.Text = obj.getRemarks



                'strsql = "Select VER_PE_CODE, VER_CHE_CODE from " & Session("TENANT") & ".VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
                'dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
                'If (dr.Read()) Then
                '    BindParentEntity()
                '    ddlParent.SelectedValue = dr("VER_PE_CODE").ToString()
                '    BindChildEntityByParent()
                '    ddlChild.SelectedValue = dr("VER_CHE_CODE").ToString()
                'End If
            Else
                Cleardata()
                BindVerticals()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "ddlVertical_SelectedIndexChanged", exp)
        End Try
    End Sub
    Public Shared dt As DataTable
    Public Sub LoadVerticalGrid(ByVal gv As GridView)

        dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_VERTICAL_DATA")
        gv.DataSource = dt
        gv.DataBind()

    End Sub
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            LoadVerticalGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub lnkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton = CType(sender, LinkButton)
        Dim Status As String = lnk.AccessKey
        Dim verticalCode As String = lnk.CommandArgument
        Try
            obj.Vertical_Rowcommand(verticalCode, Status)
            BindVerticals()
            LoadVerticalGrid(gvItem)
            Cleardata()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASVertical", "lnkStatus_Click", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMasSpaceMasters.aspx")
    End Sub

    Protected Sub btnbrowse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename
                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                msheet = listSheet(0).ToString()
                mfilename = msheet
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    sheetname = mfilename
                    str = "Select * from [" & sheetname & "]"
                End If
                Dim snocnt As Integer = 1
                Dim ver_codecnt As Integer = 1
                Dim ver_namecnt As Integer = 1
                Dim pe_codecnt As Integer = 1
                Dim che_codecnt As Integer = 1
                Dim rm_idcnt As Integer = 1
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "bu_code" Then
                            ver_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be BU_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "bu_name" Then
                            ver_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be BU_NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "pe_code" Then
                            pe_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be PE_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "che_code" Then
                            che_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CHE_CODE"
                            Exit Sub
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "ver_code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Business Unit Code is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "ver_name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Business Unit Name is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                 
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_INSERT_MODIFY_VERTICAL")
                    sp.Command.AddParameter("@VER_CODE", ds.Tables(0).Rows(i).Item("BU_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_NAME", ds.Tables(0).Rows(i).Item("BU_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_PE_CODE", ds.Tables(0).Rows(i).Item("PE_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_CH_CODE", ds.Tables(0).Rows(i).Item("CHE_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_REM", ds.Tables(0).Rows(i).Item("BU_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_UPT_BY", "", DbType.String)
                    sp.Command.AddParameter("@i_Status", 1, DbType.Int32)
                    sp.Command.AddParameter("@i_Op", 1, DbType.Int32)
                    sp.ExecuteScalar()
                Next
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                LoadVerticalGrid(gvItem)
            Else
                lblMsg.Text = ""
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItem_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItem.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then

            e.Row.Cells(0).Text = Session("Parent") + " Code"
            e.Row.Cells(1).Text = Session("Parent") + " Name"

        End If
    End Sub

    Protected Sub ddlParent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlParent.SelectedIndexChanged
        BindChildEntityByParent()
    End Sub
End Class
