Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports System.Configuration

Partial Class Masters_Mas_Webfiles_frmMASMasters
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
    End Sub

    'Public Sub TenantModule()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Tenant_Module")
    '    sp.Command.AddParameter("@TID", Session("TENANT"), DbType.String)
    '    Dim ds As New DataSet
    '    ds = sp.GetDataSet
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each row As DataRow In ds.Tables(0).Rows
    '            If row.Item("T_MODULE") = "Space" Then
    '                Dim section As New NameValueCollection
    '                section = CType(ConfigurationManager.GetSection(row.Item("T_MODULE")), NameValueCollection)
    '                For Each key In section.Keys
    '                    Dim ctrl As Control
    '                    ctrl = Me.FindControl(key)
    '                    ctrl.Visible = Boolean.Parse(section(key))
    '                Next
    '                Exit Sub
    '            Else
    '                Dim section As New NameValueCollection
    '                section = CType(ConfigurationManager.GetSection("Space"), NameValueCollection)
    '                For Each key In section.Keys
    '                    Dim ctrl As Control
    '                    ctrl = Me.FindControl(key)
    '                    ctrl.Visible = Boolean.Parse(section(key))
    '                Next
    '                Exit Sub
    '            End If
    '        Next
    '    End If
    'End Sub
End Class
