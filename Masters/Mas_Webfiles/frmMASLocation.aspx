<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASLocation.aspx.vb" Inherits="Masters_MAS_WebFiles_frmLocation" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Location Master</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-md-6">
                                <label class="col-md-2 btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                        ToolTip="Please Select Add to add new Location and Select Modify to modify the existing Location" />
                                    Add
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                        ToolTip="Please Select Add to add new Location and Select Modify to modify the existing Location" />
                                    Modify
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trLName" runat="server">
                                        <asp:Label ID="lblLocationname" runat="server" class="col-md-4 control-label">Select Location <span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Please Select Location " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Location">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Location Code <span style="color: red;">*</span></label>
                                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtLocationcode"
                                            Display="None" ErrorMessage="Enter Location code in alphabets and numbers, upto 15 characters allowed"
                                            ValidationExpression="^[0-9a-zA-Z]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>--%>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtLocationcode"
                                            Display="None" ErrorMessage="Please Enter Location Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtLocationcode" MaxLength="15" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Location Name<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtLocationName"
                                            Display="None" ErrorMessage="Enter Location Name in alphabets and numbers and (space,-,_,(,),\,, allowed) and upto 50 characters allowed"
                                            ValidationExpression="^[0-9a-zA-Z-_\(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtLocationName"
                                            Display="None" ErrorMessage="Please Enter Location Name  " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Country Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtLocationName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">City Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvcty" runat="server"
                                            ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City Name"
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCity" runat="server" Width="97%" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select City Name">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">State Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                            ControlToValidate="ddlState" Display="None" ErrorMessage="Please Select State Name"
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlState" runat="server" Width="97%" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select State Name">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Zone Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlZone"
                                            Display="None" ErrorMessage="Please Select Zone Name" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlZone" runat="server" Enabled="false" Width="97%" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Zone Name">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Country Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvcnt" runat="server" ControlToValidate="ddlCountry"
                                            Display="None" ErrorMessage="Please Select Country Name" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCountry" runat="server" Enabled="false" Width="97%" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Country Name">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Completion Date(Tentative)<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDate" Display="None"
                                            ErrorMessage="Please Select Completion Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtDate"
                                            ValidationExpression="^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)&#13;&#10;  &#13;&#10;   ([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)&#13;&#10;   &#13;&#10;     ([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$"
                                            ErrorMessage="Select Completion Date From Calendar" Display="None" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-8">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" visible="false" runat="server">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvStatus" runat="server" ControlToValidate="cmbstatus" Display="None" ErrorMessage="Please Select Status  " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="cmbstatus" runat="server" Visible="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="2">In-Active</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseout="UnTip()" onmouseover="Tip('Enter Remarks upto 500 characters')">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Location Found."
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:BoundField DataField="LCM_NAME" HeaderText="Location Name">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="cty_name" HeaderText="City Name">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                         <asp:BoundField DataField="STE_NAME" HeaderText="State Name">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                         <asp:BoundField DataField="ZN_NAME" HeaderText="Zone Name">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="cny_name" HeaderText="Country Name">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LCM_STATUS" HeaderText="Location Status" Visible="false">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LCM_TENDATE" HeaderText="Date">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:ButtonField HeaderText="Status" CommandName="Status" Text="Button">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="LCM_STATUS" HeaderText="STID">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LCM_CODE" HeaderText="Location Code">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
