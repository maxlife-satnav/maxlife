﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;

public partial class Masters_Mas_Webfiles_StateMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uid"].ToString() == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        if (!IsPostBack)
        {
            FillGridData();
            trSName.Visible = false;
            rbActions.Checked = true;
            GetZone();
            GetCountry();
        }
    }

    private void FillGridData()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_STATE_DATA");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        gvItem.DataSource = sp.GetDataSet();
        gvItem.DataBind();
        ddlState.DataSource = sp.GetDataSet();
        ddlState.DataTextField = "STE_NAME";
        ddlState.DataValueField = "STE_CODE";
        ddlState.DataBind();
        ddlState.Items.Insert(0, "--Select--");
    }

    protected void GetZone()
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_ZONE");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@MODE", 1, DbType.Int32);
        ddlZone.DataSource = sp.GetDataSet();
        ddlZone.DataTextField = "ZN_NAME";
        ddlZone.DataValueField = "ZN_CODE";
        ddlZone.DataBind();
        ddlZone.Items.Insert(0, "--Select--");
    }

    protected void GetCountry()
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GETCOUNTRYZONE");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlCny.DataSource = sp.GetDataSet();
        ddlCny.DataTextField = "CNY_NAME";
        ddlCny.DataValueField = "CNY_CODE";
        ddlCny.DataBind();
        ddlCny.Items.Insert(0, "--Select--");
    }

    protected void gvItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        FillGridData();
        gvItem.PageIndex = e.NewPageIndex;
        gvItem.DataBind();
        lblMsg.Text = "";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (rbActionsModify.Checked == true)
            {
                ddlState.SelectedIndex = 0;
                trSName.Visible = true;
                btnSubmit.Text = "Modify";
                InsertRecord(2);
            }
            else
            {
                trSName.Visible = false;
                btnSubmit.Text = "Submit";
                InsertRecord(1);
                txtCCode.Enabled = true;
                ddlState.SelectedIndex = 0;
                ddlZone.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }

    public void InsertRecord(int STA)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "INSERT_STATE_DETAILS");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@STE_CODE", txtCCode.Text, DbType.String);
            sp.Command.AddParameter("@STE_NAME", txtStateName.Text, DbType.String);
            sp.Command.AddParameter("@STE_ZN_ID", ddlZone.SelectedItem.Value, DbType.String);
            sp.Command.AddParameter("@STE_CNY_ID", ddlCny.SelectedItem.Value, DbType.String);
            sp.Command.AddParameter("@STE_REM", txtRemarks.Text, DbType.String);
            sp.Command.AddParameter("@STATUS", STA, DbType.String);
            object a = sp.ExecuteScalar();
            lblMsg.Visible = true;
            if ((int)a > 0)
            {

                if (STA == 1)
                {

                    lblMsg.Text = "State Created Successfully";
                }
                else
                {

                    lblMsg.Text = "State Updated Successfully";
                }
                txtCCode.Text = String.Empty;
                FillGridData();
                cleardata();
            }
            else
            {

                lblMsg.Text = "State Exists With Same Code";
                txtCCode.Text = String.Empty;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void cleardata()
    {

        txtCCode.Text = String.Empty;
        txtStateName.Text = String.Empty;
        txtRemarks.Text = String.Empty;
        ddlCny.SelectedIndex = 0;
        ddlZone.SelectedIndex = 0;
    }

    protected void rbActionsModify_CheckedChanged(object sender, EventArgs e)
    {
        lblMsg.Visible = false;
        if (rbActionsModify.Checked == true)
        {
            ddlState.SelectedIndex = 0;
            trSName.Visible = true;
            btnSubmit.Text = "Modify";
        }
        else
        {
            ddlState.SelectedIndex = 0;
            trSName.Visible = false;
            btnSubmit.Text = "Submit";
            txtCCode.Enabled = true;
            ddlZone.Enabled = true;
        }
        txtCCode.Text = String.Empty;
        cleardata();
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMsg.Visible = false;
        try
        {

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_STATE_BY_ID");
            sp.Command.AddParameter("@STE_CODE", ddlState.SelectedItem.Value, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    txtRemarks.Text = reader["STE_REM"].ToString();
                    txtStateName.Text = reader["STE_NAME"].ToString();
                    txtCCode.Text = reader["STE_CODE"].ToString();
                    ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(reader["STE_ZN_ID"].ToString()));
                    ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(reader["STE_CNY_ID"].ToString()));
                    txtCCode.Enabled = false;
                    ddlZone.Enabled = false;
                    ddlCny.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void gvItem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Status")
        {
            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblSte_Code = (Label)gvRow.FindControl("lblSte_Code");
            Label lblStatus_Code = (Label)gvRow.FindControl("lblStatus_Code");

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_CHANGESTATESTATUS");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@STE_CODE", lblSte_Code.Text, DbType.String);
            sp.Command.AddParameter("@STATUS", lblStatus_Code.Text, DbType.String);
            object a = sp.ExecuteScalar();

            if ((int)a == 0)
            {
                lblMsg.Text = "First inactivate all the cities under this State ";
                lblMsg.Visible = true;
            }
            else
            {
                lblMsg.Visible = false;
                FillGridData();
                GetCountry();
                cleardata();
            }
        }
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmMASMasters.aspx");
    }

    protected void ddlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if ((ddlZone.SelectedIndex != 0))
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_ZONEDETAILS_FORCOMBO");
                sp.Command.AddParameter("@ZN_CODE", ddlZone.SelectedItem.Value, DbType.String);

                using (IDataReader reader = sp.GetReader())
                {
                    while (reader.Read())
                    {

                        ddlCny.SelectedIndex = ddlCny.Items.IndexOf(ddlCny.Items.FindByValue(reader["ZN_CNY_ID"].ToString()));
                        ddlCny.Enabled = false;
                    }
                }
            }
            else
            {
                ddlCny.SelectedIndex = 0;

            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }
}