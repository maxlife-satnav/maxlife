﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SLATrackerReport.aspx.vb" Inherits="Tracker_Reports_SLATrackerReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .trrr:hover
        {
            background-color: orange;
            cursor: pointer;
        }
    </style>

    <script>
        function ColorRow() {
            $('table:contains("L1"):last tr').addClass('trrr');
        }
    </script>
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/mm/yyyy',
                //todayHighlight: true,
                autoclose: true
            });
        };
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>SLA Breached Requests
                        </legend>
                    </fieldset>

                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="Server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server"  CssClass="alert alert-danger" ForeColor="Red"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Tenant<span style="color: red;">*</span></label>                                        
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddltenant"
                                            Display="None" ErrorMessage="Please Select Tenant"  ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>                                        
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddltenant" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date</label>
                                        <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ControlToValidate="txtfromdate"
                                            Display="None" ErrorMessage="Please Select From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttodate"
                                            Display="None" ErrorMessage="Please Select To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 text-right">
                                <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />

                                <asp:Button ID="btnback" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />
                            </div>
                        </div>
                        <br />
                        <div class="row table table table-condensed">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
