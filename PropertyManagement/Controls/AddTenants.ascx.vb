Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AddTenants
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Public Sub AddTenant()

        Dim param1(22) As SqlParameter
        param1(0) = New SqlParameter("@BDG_PROP_TYPE", SqlDbType.NVarChar, 200)
        param1(0).Value = ddlproptype.SelectedItem.Value
        param1(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param1(1).Value = ddlLocation.SelectedItem.Value
        param1(2) = New SqlParameter("@TEN_CODE", SqlDbType.NVarChar, 200)
        param1(2).Value = txttcode.Text
        param1(3) = New SqlParameter("@PN_NAME", SqlDbType.NVarChar, 200)
        param1(3).Value = ddlBuilding.SelectedItem.Value
        param1(4) = New SqlParameter("@BDG_ADM_CODE", SqlDbType.NVarChar, 200)
        param1(4).Value = ddlCity.SelectedItem.Value
        param1(5) = New SqlParameter("@TEN_OCCUPIEDAREA", SqlDbType.VarChar, 100)
        param1(5).Value = txtTenantOccupiedArea.Text
        param1(6) = New SqlParameter("@TEN_RENT_PER_SFT", SqlDbType.Money, 100)
        param1(6).Value = txtRent.Text
        param1(7) = New SqlParameter("@TEN_COMMENCE_DATE", SqlDbType.DateTime, 100)
        param1(7).Value = txtDate.Text
        param1(8) = New SqlParameter("@TEN_SECURITY_DEPOSIT", SqlDbType.Money, 100)
        param1(8).Value = txtSecurityDeposit.Text
        param1(9) = New SqlParameter("@TEN_PAYMENTTERMS", SqlDbType.NVarChar, 100)
        param1(9).Value = ddlPaymentTerms.SelectedItem.Value
        param1(10) = New SqlParameter("@TEN_NEXTPAYABLEDATE", SqlDbType.DateTime, 100)
        param1(10).Value = txtPayableDate.Text
        param1(11) = New SqlParameter("@TEN_NO_PARKING", SqlDbType.VarChar, 100)
        param1(11).Value = txtNoofparking.Text
        param1(12) = New SqlParameter("@TEN_MAINT_FEE", SqlDbType.Money, 100)
        param1(12).Value = txtfees.Text
        param1(13) = New SqlParameter("@TEN_OUTSTANDING_AMOUNT", SqlDbType.Money, 100)
        param1(13).Value = txtamount.Text
        param1(14) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param1(14).Value = ddluser.SelectedItem.Value
        param1(15) = New SqlParameter("@TEN_REMARKS", SqlDbType.NVarChar, 100)
        param1(15).Value = txtRemarks.Text
        Dim selectedItems As String = [String].Join(",", TntRem.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
        param1(16) = New SqlParameter("@TEN_REM", SqlDbType.NVarChar, 100)
        param1(16).Value = selectedItems
        param1(17) = New SqlParameter("@CreatedBy", SqlDbType.NVarChar, 100)
        param1(17).Value = HttpContext.Current.Session("uid")
       
        param1(18) = New SqlParameter("@SUBAGREE", SqlDbType.NVarChar, 300)
        param1(18).Value = txtSubject.Text
        param1(19) = New SqlParameter("@PURAGREE", SqlDbType.NVarChar, 300)
        param1(19).Value = txtPurpose.Text
        param1(20) = New SqlParameter("@FRM_DT", SqlDbType.DateTime, 100)
        param1(20).Value = TntFrm.Text
        param1(21) = New SqlParameter("@TO_DT", SqlDbType.DateTime, 100)
        param1(21).Value = txtPayableDate.Text
        param1(22) = New SqlParameter("@ADD_PARKING", SqlDbType.Money, 100)
        param1(22).Value = txtcar.Text
        ObjSubSonic.GetSubSonicExecute("PM_ADD_TENANTS", param1)
        'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=7")
        lblMsg.Text = "New Tenant Added Successfully"
        Cleardata()
    End Sub
    Private Sub BindUser()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENUSER")
            sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindCity()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddlCity.DataSource = sp.GetDataSet()
            ddlCity.DataTextField = "CTY_NAME"
            ddlCity.DataValueField = "CTY_CODE"
            ddlCity.DataBind()
            ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindPropertyType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPaymentTerms()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PAYMENT_TERMS")
            ddlPaymentTerms.DataSource = sp.GetDataSet()
            ddlPaymentTerms.DataTextField = "PM_PT_NAME"
            ddlPaymentTerms.DataValueField = "PM_PT_SNO"
            ddlPaymentTerms.DataBind()
            ddlPaymentTerms.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROP_tenant")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()

            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub Cleardata()
        ddlproptype.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        TntFrm.Text = ""
        ddlBuilding.Items.Clear()
        ddlBuilding.Items.Insert(0, New ListItem("--Select --", "0"))
        ddlBuilding.SelectedIndex = 0

        txtTenantOccupiedArea.Text = ""
        txtRent.Text = ""
        txtDate.Text = getoffsetdate(Date.Today)
        txtSecurityDeposit.Text = ""
        txttcode.Text = ""
        txtRemarks.Text = ""
        TntRem.ClearSelection()
        ddlPaymentTerms.SelectedIndex = -1
        txtPayableDate.Text = ""
        'ddlstatus.SelectedIndex = -1

        txtNoofparking.Text = ""
        txtfees.Text = ""
        txtamount.Text = ""

        ddluser.SelectedValue = 0

    End Sub
    Private Function ValidateTenantCode()
        Dim ValidateCode As Integer
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VALIDATE_TENANT_CODE")
        sp3.Command.AddParameter("@TEN_CODE", txttcode.Text, DbType.String)
        ValidateCode = sp3.ExecuteScalar()
        Return ValidateCode
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            BindPropertyType()
            BindCity()
            BindUser()
            BindPaymentTerms()
            TntFrm.Text = getoffsetdate(Date.Today)
        End If
        txtamount.Attributes.Add("readonly", "readonly")
        TntFrm.Attributes.Add("readonly", "readonly")
        txtPayableDate.Attributes.Add("readonly", "readonly")
        txtDate.Attributes.Add("readonly", "readonly")
    End Sub
    Protected Sub ddlproptype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlproptype.SelectedIndexChanged
        lblMsg.Text = ""
        ddlCity.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        BindCityLoc()
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            BindProp()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub
    Protected Sub txtfees_TextChanged(sender As Object, e As EventArgs) Handles txtfees.TextChanged
        'If txtRent.Text <> Nothing And txtfees.Text <> Nothing And txtcar.Text <> Nothing Then
        '    txtamount.ReadOnly = False
        '    txtamount.Text = Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtcar.Text)
        '    txtamount.ReadOnly = True
        'Else
        '    txtamount.Text = Nothing
        'End If

        If txtRent.Text <> Nothing Then
            'And txtRent.Text <> Nothing And txtcar.Text <> Nothing Then
            txtamount.ReadOnly = False
            If txtfees.Text = "" Then
                txtfees.Text = 0
            End If
            If txtcar.Text = "" Then
                txtcar.Text = 0
            End If
            txtamount.Text = Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtcar.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If

    End Sub
    Protected Sub txtRent_TextChanged(sender As Object, e As EventArgs) Handles txtRent.TextChanged
        If txtRent.Text <> Nothing Then
            'And txtRent.Text <> Nothing And txtcar.Text <> Nothing Then
            txtamount.ReadOnly = False
            If txtfees.Text = "" Then
                txtfees.Text = 0
            End If
            If txtcar.Text = "" Then
                txtcar.Text = 0
            End If
            txtamount.Text = Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtcar.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If CDate(txtPayableDate.Text) < CDate(txtDate.Text) Then
                lblMsg.Text = "The payable date should not be less than the joining date"
                lblMsg.Visible = True
            Else

                Dim Validatecode As Integer
                Validatecode = ValidateTenantCode()
                If Validatecode = 0 Then
                    lblMsg.Text = "Tenant Code already exist please enter another code"
                    lblMsg.Visible = True
                Else
                    AddTenant()
                End If


            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlBuilding_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBuilding.SelectedIndexChanged

    End Sub

    Protected Sub txtcar_TextChanged(sender As Object, e As EventArgs) Handles txtcar.TextChanged
        'If txtfees.Text <> Nothing And txtRent.Text <> Nothing And txtcar.Text <> Nothing Then
        '    txtamount.ReadOnly = False
        '    txtamount.Text = Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtcar.Text)
        '    txtamount.ReadOnly = True
        'Else
        '    txtamount.Text = Nothing
        'End If
        If txtcar.Text <> Nothing Then
            'And txtRent.Text <> Nothing And txtcar.Text <> Nothing Then
            txtamount.ReadOnly = False
            If txtfees.Text = "" Then
                txtfees.Text = 0
            End If
            If txtcar.Text = "" Then
                txtcar.Text = 0
            End If
            txtamount.Text = Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtcar.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub
End Class
