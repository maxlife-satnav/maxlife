<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddTenants.ascx.vb" Inherits="Controls_AddTenants" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
            </asp:Label>
        </div>
    </div>
</div>
<br />
<div class="panel panel-default " role="tab" runat="server" id="div0">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-multiselectable="true">Property Details</a>
        </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="clearfix">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Property Type <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                            Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                        
                            <asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >City <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <div  >
                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Location <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <div  >
                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                <asp:ListItem Value="">--Select--</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Property <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlBuilding"
                            Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <div  >
                            <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="">--Select--</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Tenant Occupied Area (Sqft)<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                            Display="None" ErrorMessage="Please Enter Tenant Occupied Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                            ErrorMessage="Please Enter Valid Tenant Occupied Area in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                            Display="None" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                        <div  >
                            <div onmouseover="Tip('Enter Tenant Occupied Area in numbers upto 2 decimal places.')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control"
                                    MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Subject of Agreement</label>
                        <div  >
                            <asp:TextBox ID="txtSubject" Rows="3" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Purpose of Agreement</label>
                        <div  >
                            <asp:TextBox ID="txtPurpose" runat="server" Rows="3" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Tenant From Date <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TntFrm"
                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Tenant From Date"></asp:RequiredFieldValidator>
                        <div  >
                            <div class='input-group date' id='TFrm'>
                                <asp:TextBox ID="TntFrm" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('TFrm')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Tenant End Date <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvPayDate" runat="server" ControlToValidate="txtPayableDate"
                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Tenant End Date"></asp:RequiredFieldValidator>
                        <div  >
                            <div class='input-group date' id='Payabledate'>
                                <asp:TextBox ID="txtPayableDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('Payabledate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default " role="tab" runat="server" id="div1">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Tenant Details</a>
        </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in">
        <div class="panel-body color">
            <div class="clearfix">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Tenant <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="cvuser" runat="server" ControlToValidate="ddluser"
                            Display="None" ErrorMessage="Please Select User" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <div  >
                            <asp:DropDownList ID="ddluser" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Tenant Code <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvtcode" runat="server" ControlToValidate="txttcode"
                            Display="None" ErrorMessage="Please Enter Tenant Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div  >
                            <asp:TextBox ID="txttcode" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Parking Spaces</label>
                        <asp:RegularExpressionValidator ID="revNoofParking" ValidationGroup="Val1" runat="server"
                            Display="none" ControlToValidate="txtNoofparking" ErrorMessage="Please Enter Valid Number of Parking"
                            ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                        <div  >
                            <div onmouseover="Tip('Enter numbers only with maximum length 20')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control"
                                    MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default " role="tab" runat="server" id="div2">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Payment Details</a>
        </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse in">
        <div class="panel-body color">
            <div class="clearfix">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Tenant Rent <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfRent" runat="server" ControlToValidate="txtRent"
                            Display="None" ErrorMessage="Please Enter Tenant Rent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRent"
                            Display="None" ErrorMessage="Please Enter Valid Tenant Rent in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                            ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                        <div  >
                            <div onmouseover="Tip('Enter tenant rent in numbers upto 2 decimal places.')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtRent" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Security Deposit <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                            Display="None" ErrorMessage="Please Enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                            Display="None" ErrorMessage="Please Enter Valid Security Deposit in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                            ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                        <div  >
                            <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control"
                                    MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Joining Date <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Joining Date"></asp:RequiredFieldValidator>
                        <div  >
                            <div class='input-group date' id='fromdate'>
                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Payment Terms <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvpayment" runat="server" ControlToValidate="ddlPaymentTerms"
                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Payment Terms"
                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <div  >
                            <asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem>--Select--</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Maintenance Fees</label>
                        <asp:RegularExpressionValidator ID="revfees" runat="server" ControlToValidate="txtfees"
                            Display="none" ErrorMessage="Please Enter Valid Maintenance fees in Numbers or Decimal Number with 2 Decimal Places." ValidationExpression="((\d+)((\.\d{1,2})?))$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                        <div  >
                            <div onmouseover="Tip('Enter Maintenance Fees in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtfees" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Additional Car Parking Fees</label>
                        <div  >
                            <asp:TextBox ID="txtcar" runat="server" CssClass="form-control" AutoPostBack="true" Rows="3" MaxLength="500"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Total Rent Amount <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvamt" runat="server" ControlToValidate="txtamount"
                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount."></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexpamount" runat="server" ControlToValidate="txtamount" Display="None"
                            ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount in Numbers or Decimal Number with 2 Decimal Places" ValidationExpression="((\d+)((\.\d{1,2})?))$">

                        </asp:RegularExpressionValidator>
                        <div  >
                            <div onmouseover="Tip('Enter Total Rent Amount in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label  >Remarks <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div  >
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                Rows="3" MaxLength="500"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default " role="tab" runat="server" id="div3">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Tenant Reminder</a>
        </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse in">
        <div class="panel-body color">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label  >Tenant Reminders For</label>
                    <div   style="width: 500px;">
                        <div class="bootstrap-tagsinput">
                            <asp:CheckBoxList ID="TntRem" runat="server" CellPadding="25" CellSpacing="100" RepeatColumns="6" RepeatLayout="Table" RepeatDirection="Vertical" CausesValidation="True">
                                <asp:ListItem Value="10">10 Days</asp:ListItem>
                                <asp:ListItem Value="3">3 Days</asp:ListItem>
                                <asp:ListItem Value="7">7 Days</asp:ListItem>
                                <asp:ListItem Value="2">2 Days</asp:ListItem>
                                <asp:ListItem Value="5">5 Days</asp:ListItem>
                                <asp:ListItem Value="1">1 Day</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="clearfix">
<div class="col-md-12 text-right" style="padding-top: 5px">
    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />
</div>

</div>


