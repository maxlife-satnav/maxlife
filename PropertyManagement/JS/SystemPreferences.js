﻿app.service("SystemPreferences", function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    //Bind Months
    this.BindMonths = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SystemPreferences/BindMonths')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    //update months
    this.UpdateDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SystemPreferences/UpdateDetails', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //Bind Grid
    this.BindGrid = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SystemPreferences/BindGrid')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('SystemPreferencesController', function ($scope, $q, SystemPreferences, UtilityService, $filter, $timeout) {

    $scope.System = {};
    $scope.ActionStatus = 0;
    $scope.Monthlist = [];
    $scope.SystemPref = [];


    SystemPreferences.BindMonths().then(function (response) {
        $scope.Monthlist = response.data;
    }, function (response) {
        progress(0, '', false);
    });



    $scope.MonthSelectAll = function () {
        $scope.System.Monthlist = $scope.Monthlist;

    }
    $scope.MonthSelectNone = function () {
        $scope.System.Monthlist = [];

    }


    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);
        $scope.dataobject = { MonLst: $scope.System.Monthlist, SYSP_FROM_DATE: $scope.SystemPref.SYSP_FROM_DATE, SYSP_TO_DATE: $scope.SystemPref.SYSP_TO_DATE };
        console.log($scope.dataobject);
        SystemPreferences.UpdateDetails($scope.dataobject).then(function (data) {
            progress(0, 'Loading...', false);
            var updatedobj = {};
            angular.copy($scope.SystemPref, updatedobj)
            $scope.gridata.unshift(updatedobj);
            $scope.ShowMessage = true;
            $scope.Success = "Data Updated Successfully";
            $scope.System.Monthlist = [];
            $scope.SystemPref = [];
            SystemPreferences.BindGrid().then(function (data) {
                $scope.gridata = data;
                $scope.gridOptions.api.setRowData(data);
            }, function (error) {
                console.log(error);
            });
            showNotification('success', 8, 'bottom-right', $scope.Success);
        }, function (error) {
        })
    }

    //for GridView
    var columnDefs = [
       { headerName: "Month", field: "AST_SYSP_CODE", width: 25, cellClass: 'grid-align', suppressMenu: true },
       { headerName: "From Date", field: "SYSP_FROM_DATE", template: '<span>{{data.SYSP_FROM_DATE | date:"dd MMM, yyyy"}}</span>', width: 25, cellClass: 'grid-align', suppressMenu: true },
       { headerName: "To Date", field: "SYSP_TO_DATE", template: '<span>{{data.SYSP_TO_DATE | date:"dd MMM, yyyy"}}</span>', width: 25, cellClass: 'grid-align', suppressMenu: true },       
       { headerName: "Edit", width: 5, suppressMenu: true, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        SystemPreferences.BindMonths().then(function (response) {
            $scope.Monthlist = response.data;
            SystemPreferences.BindGrid().then(function (data) {
                $scope.gridata = data;
                console.log($scope.gridata);
                $scope.gridOptions.api.setRowData(data);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 700);
            }, function (error) {
                console.log(error);
            });
        }, function (error) {
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        enableCellSelection: false,
        suppressHorizontalScroll: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    $scope.EditFunction = function (data) {
        $scope.SystemPref = {};        
       
        angular.copy(data, $scope.SystemPref);
        angular.forEach(data, function (value, key) {
            var Mn = _.find($scope.Monthlist, { AST_SYSP_CODE: value });
            if (Mn != undefined && Mn.ticked == false) {
                Mn.ticked = true;
            }
        });
    }

    $timeout($scope.LoadData, 1000);
});