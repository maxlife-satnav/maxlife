﻿app.service("TenantReportService", function ($http, $q, UtilityService) {
    this.GetTenantDetails = function (TenantRep) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/TenantReport/GetTenantDetails', TenantRep)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('TenantReportController', function ($scope, $q, $http, UtilityService, TenantReportService, $timeout) {    
    $scope.TenantRep = {};
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Loclist = [];
    $scope.PrpTypelist = [];
    $scope.PrpNamelist = [];    
   
    $scope.Zonelst = [];
    $scope.Statelst = [];
    $scope.Citylst = [];

    $scope.columnDefs = [
           { headerName: "Tenant-Property Type", field: "TEN_PRPTYPE", width: 350, cellClass: 'grid-align', },
           { headerName: "Tenant Code", field: "TEN_CODE", cellClass: 'grid-align', width: 350, },
           { headerName: "Tenant Name", field: "TEN_NAME", cellClass: 'grid-align', width: 350 },
           { headerName: "Tenant Email", field: "TEN_EMAIL", cellClass: 'grid-align', width: 350 },
           { headerName: "Occupied Area(Sqft.)", field: "OCC_AREA", cellClass: 'grid-align', width: 350 },
           { headerName: "Tenant Rent", field: "TEN_RENT", cellClass: 'grid-align', width: 350 },
           { headerName: "Maintenance Fee", field: "MAINT_FEE", cellClass: 'grid-align', width: 350 },
           { headerName: "Total Rent Amount", field: "TOT_RENT_AMT", cellClass: 'grid-align', width: 350 }
    ];



    //Zone
    UtilityService.getZone(1).then(function (zndata) {
        $scope.Zonelst = zndata.data;
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = true;
        });
        //State
        UtilityService.getState(1).then(function (stedata) {
            $scope.Statelst = stedata.data;
            angular.forEach($scope.Statelst, function (value, key) {
                value.ticked = true;
            });
            //cities
            UtilityService.getCities(1).then(function (Cnydata) {
                $scope.Citylst = Cnydata.data;
                angular.forEach($scope.Citylst, function (value, key) {
                    value.ticked = true;
                });

                UtilityService.getLocations(1).then(function (response) {
                    if (response.data != null) {
                        $scope.Loclist = response.data;
                        angular.forEach($scope.Loclist, function (value, key) {
                            value.ticked = true;
                        });
                    }
                });

            });
        });
    });

  
   


    UtilityService.getPropertyTypes().then(function (response) {
        if (response.data != null) {
            $scope.PrpTypelist = response.data;
            $scope.PrpTypelist.ticked = true;
            angular.forEach($scope.PrpTypelist, function (value, key) {
                value.ticked = true;
            });
        }
    });

    $scope.LcmChanged = function () {      
        var params =
   {
       selectedLoc: $scope.TenantRep.selectedLoc,
       selectedPrpType: $scope.TenantRep.selectedPrpType,
       selectedPrpName: $scope.TenantRep.selectedPrpName,
       CNP_NAME: $scope.TenantRep.CNP_NAME[0].CNP_ID
   }
        UtilityService.getPropertyNames(params).then(function (response) {
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                $scope.PrpNamelist.ticked = true;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.PrpNamelist = [];
        });

    }
   

    $scope.PrpChanged = function () {
        var params =
   {
       selectedLoc: $scope.TenantRep.selectedLoc,
       selectedPrpType: $scope.TenantRep.selectedPrpType    
   }
        UtilityService.getPropertyNames(params).then(function (response) {
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.PrpNamelist = [];
        });
    }

    $scope.PrpChangeAll = function () {
        var params =
{
    selectedLoc: $scope.Loclist,
    selectedPrpType: $scope.PrpTypelist
    //selectedPrpName: $scope.TenantRep.selectedPrpName,
    //CNP_NAME: $scope.TenantRep.CNP_NAME[0].CNP_ID
}
        UtilityService.getPropertyNames(params).then(function (response) {
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                $scope.PrpNamelist.ticked = true;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.PrpNamelist = [];
        });
    }
   

 

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params =
        {
            selectedLoc: $scope.TenantRep.selectedLoc,
            selectedPrpType: $scope.TenantRep.selectedPrpType,
            selectedPrpName: $scope.TenantRep.selectedPrpName,
         }
        progress(0, 'Loading...', true);
        $scope.GridVisiblity = true;
        TenantReportService.GetTenantDetails(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        }, function (error) {
            console.log(error);
        });
    }


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };

    $scope.GenReport = function (TenantRep, Type) {
        progress(0, 'Loading...', true);
        woobj = {};
        angular.copy(TenantRep, woobj);
        woobj.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/TenantReport/GetGrid',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'TenantReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

        var columns = [{ title: "Tenant-Property Type", key: "TEN_PRPTYPE" }, { title: "Tenant Code ", key: "TEN_CODE" }, { title: "Tenant Name", key: "TEN_NAME" },
                        { title: "Tenant Email", key: "TEN_EMAIL" }, { title: "Occupied Area(Sqft.)", key: "OCC_AREA" }, { title: "Tenant Rent", key: "TEN_RENT" },
                        { title: "Maintenance Fee", key: "MAINT_FEE" }, { title: "Total Rent Amount", key: "TOT_RENT_AMT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("TenantReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "TenantReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }



    setTimeout(function () {
        var params =
     {
         selectedLoc: $scope.TenantRep.selectedLoc,
         selectedPrpType: $scope.TenantRep.selectedPrpType,
         selectedPrpName: $scope.TenantRep.selectedPrpName,
      
     }
        UtilityService.getPropertyNames(params).then(function (response) {
            console.log(response);
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                console.log($scope.PrpNamelist);
                $scope.PrpNamelist.ticked = true;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
        });
        setTimeout(function () {
            $scope.LoadData();
        }, 500);
    }, 1000);



    //zone
    $scope.ZonChanged = function () {
        UtilityService.getStateByZone($scope.TenantRep.selectedZone, 1).then(function (data) {
            if (data.data == null) { $scope.Statelst = [] } else {
                $scope.Statelst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.TenantRep.selectedCountries.push(cny);
            }
        });
    }
    $scope.ZonChangeAll = function () {
        $scope.TenantRep.selectedZone = $scope.Zonelst;
        $scope.ZonChanged();
    }
    $scope.ZonSelectNone = function () {
        $scope.Statelst = [];
        $scope.Citylst = [];
        $scope.Loclist = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    //state
    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.TenantRep.selectedState, 1).then(function (data) {
            if (data.data == null) { $scope.Citylst = [] } else {
                $scope.Citylst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.TenantRep.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Statelst, function (value, key) {
            var zne = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zne != undefined && value.ticked == true) {
                zne.ticked = true;
                $scope.TenantRep.selectedZone.push(zne);
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.TenantRep.selectedState = $scope.statelst;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.city = [];
        $scope.Loclist = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    //city
    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.TenantRep.selectedCities, 1).then(function (data) {
            if (data.data == null) { $scope.Loclist = [] } else {
                $scope.Loclist = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.TenantRep.selectedCountries.push(cny);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.TenantRep.selectedZone.push(zn);
            }
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.TenantRep.selectedState.push(ste);
            }
        });
    }
    $scope.CtyChangeAll = function () {
        $scope.TenantRep.selectedCities = $scope.Citylst;
        $scope.CityChanged();
    }
    $scope.ctySelectNone = function () {
        $scope.TenantRep.selectedCities = [];
        $scope.CityChanged();
    }


    //location
    $scope.LocChange = function () {
        //UtilityService.getTowerByLocation($scope.TenantRep.selectedLocations, 1).then(function (data) {
        //    if (data.data != null) { $scope.Towerlist = data.data } else { $scope.Towerlist = [] }
        //}, function (error) {
        //    console.log(error);
        //});
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zonelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Statelst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Loclist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.TenantRep.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Loclist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.TenantRep.selectedCities.push(cty);
            }
        });
        angular.forEach($scope.Loclist, function (value, key) {
            var zn = _.find($scope.Zonelst, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.TenantRep.selectedZone.push(zn);
            }
        });
        angular.forEach($scope.Loclist, function (value, key) {
            var ste = _.find($scope.Statelst, { STE_CODE: value.STE_CODE });
            if (ste != undefined && value.ticked == true) {
                ste.ticked = true;
                $scope.TenantRep.selectedState.push(ste);
            }
        });
    }
    $scope.LCMChangeAll = function () {
        $scope.TenantRep.selectedLocations = $scope.Loclist;
        $scope.LocChange();
    }
    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }


});