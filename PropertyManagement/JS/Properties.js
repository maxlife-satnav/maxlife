﻿app.service("PropertiesService", function ($http, $q, UtilityService) {

    this.GetGriddata = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Properties/GetProperties')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetMarkers = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Properties/GetMarkers')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});
app.controller('PropertiesController', function ($scope, $q, $http, PropertiesService, UtilityService, $filter) {
    $scope.Property = {};
    $scope.GridVisiblity = true;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Markers = [];
    $scope.columnDefs = [
          { headerName: "Property Type", field: "PN_PROPERTYTYPE", width: 110, cellClass: 'grid-align', width: 190, },
          { headerName: "Property Name", field: "PM_PPT_NAME", width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
          { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 150, },
          { headerName: "Carpet Area(Sqft)", field: "PM_AR_CARPET_AREA", cellClass: 'grid-align', width: 150 },
          { headerName: "Build-up Area (Sqft)", field: "PM_AR_BUA_AREA", cellClass: 'grid-align', width: 150 },
          { headerName: "Owner Name", field: "PM_OWN_NAME", cellClass: 'grid-align', width: 150, },
          { headerName: "Tenant Name", field: "AUR_KNOWN_AS", cellClass: 'grid-align', width: 200 },
          { headerName: "Phone Number", field: "AUR_RES_NUMBER", cellClass: 'grid-align', width: 150, },
          { headerName: "Email Id", field: "AUR_EMAIL", cellClass: 'grid-align', width: 150, },
          { headerName: "Insurance Type", field: "PM_IT_NAME", cellClass: 'grid-align', width: 150, },
          { headerName: "Insurance Vendor", field: "PM_INS_VENDOR", cellClass: 'grid-align', width: 150, },
          { headerName: "Insurance Amount", field: "PM_INS_AMOUNT", cellClass: 'grid-align', width: 150, },
          { headerName: "Insurance Policy Number", field: "PM_INS_PNO", cellClass: 'grid-align', width: 180, },
          { headerName: "Insurance Start Date", field: "PM_INS_START_DT", template: '<span>{{data.PM_INS_START_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
          { headerName: "Insurance End Date", field: "PM_INS_END_DT", template: '<span>{{data.PM_INS_END_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true }];

    $scope.LoadData = function () {
       
        $scope.GridVisiblity = true;
        PropertiesService.GetGriddata().then(function (response) {
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        });
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,

        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    PropertiesService.GetMarkers().then(function (response) {
        if (response != undefined && response != null) {
            console.log(response.data);
            $scope.citylst = response.data.Table1;
            $scope.totalsum = _.sumBy($scope.citylst, 'CNT');
            angular.forEach(response.data.Table, function (value, key) {
                addMarker(value);
            });
        }

    });

    $scope.GenReport = function (Property, Type) {
        Property.Type = Type;
        //var flrObj = { flrlst: response.data.SELSPACES.flrlst };
        progress(0, 'Loading...', true);
        woobj = {};
        woobj.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Property.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/Properties/GetAllProperties',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Properties.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    setTimeout(function () {
        progress(0, 'Loading...', true);
        $scope.LoadData();
    }, 700);


    $scope.initMap = function () {
        var haightAshbury = { lat: 23.0885976, lng: 86.1615858 };

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: haightAshbury,
            mapTypeId: 'terrain'
        });
        // This event listener will call addMarker() when the map is clicked.
        //map.addListener('click', function (event) {
        //    addMarker(event.latLng);
        //});

        // Adds a marker at the center of the map.
        //addMarker(haightAshbury);
    }

    $scope.initMap();
    var infowindow = new google.maps.InfoWindow();

    // Adds a marker to the map and push to the array.
    function addMarker(data) {
        console.log(data);
        var marker = new google.maps.Marker({
            position: { lat: parseFloat(data.LATITUDE), lng: parseFloat(data.LONGITUDE) },
            map: map,
            title: data.PN_NAME
        });
        marker.CTY_CODE = data.CTY_CODE;
        marker.LCM_CODE = data.LCM_CODE;
        marker.CTY_NAME = data.CTY_NAME;
        marker.LCM_NAME = data.LCM_NAME;

        var content = '<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1 > ' + data.PN_NAME + ' (' + data.PN_CODE + ') <br/><b>Property Address:-</b> : ' + data.PM_PPT_ADDRESS + '  </font>'
        marker.addListener('click', function () {
            if (infowindow)
                infowindow.close();
            infowindow.setContent(content);
            infowindow.open(map, marker);
        });

        $scope.Markers.push(marker);
    }

    $scope.CTYClick = function (city) {
        clearMarkers();
        var markers = $filter('filter')($scope.Markers, { CTY_CODE: city.CTY_CODE });
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < $scope.Markers.length; i++) {
            $scope.Markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    $scope.showMarkers = function () {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        $scope.Markers = [];
    }


});