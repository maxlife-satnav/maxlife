﻿app.service('PropInsValidityService', function ($http, $q, UtilityService) {
    
    this.GetGriddata = function (Ddata) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Prop_Ins_Validity/GetGriddata', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('PropInsValidityController', function ($scope, $q, PropInsValidityService, UtilityService, $http, $timeout) {
    $scope.ActionStatus = 0;
    $scope.GridVisible = 0;
    $scope.Company = [];
    $scope.CompanyVisible = 0;
    $scope.Ddata = {};
    $scope.InsValidity = {};
    //for GridView
    var columnDefs = [
       { headerName: "Property Type", field: "PPT_TYPE", width: 130, cellClass: 'grid-align' },
       { headerName: "Property Name", field: "PPT_NAME", width: 130, cellClass: 'grid-align' },
       { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align' },
       { headerName: "Address", field: "PPT_ADDR", width: 180, cellClass: 'grid-align' },
       { headerName: "Owner", field: "OWNER", width: 100, cellClass: 'grid-align' },
       { headerName: "Phone Number", field: "PHNO", width: 100, cellClass: 'grid-align' },
       { headerName: "Insurance Type", field: "INS_TYPE", width: 120, cellClass: 'grid-align' },
       { headerName: "Insurance Vendor", field: "INS_VENDOR", width: 130, cellClass: 'grid-align' },
       { headerName: "Insurance Amount", field: "INS_AMOUNT", width: 120, cellClass: 'grid-align' },
       { headerName: "Insurance Policy Number", field: "INS_PNO", width: 150, cellClass: 'grid-align' },
       { headerName: "Insurance Start Date", template: '<span>{{data.INS_START_DT | date: "dd MMM, yyyy"}}</span>', field: "INS_START_DT", width: 120, cellClass: 'grid-align' },
       { headerName: "Insurance End Date", template: '<span>{{data.INS_END_DT | date: "dd MMM, yyyy"}}</span>', field: "INS_END_DT", width: 120, cellClass: 'grid-align' },
    ];
    
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        //suppressHorizontalScroll: true,
    };

    $scope.LoadGrid = function () {       
       
        var obj = {
            FROM_DATE: $scope.InsValidity.FromDate,
            TO_DATE: $scope.InsValidity.ToDate,
        }
      
        PropInsValidityService.GetGriddata(obj).then(function (response) {
            $scope.gridata = response.data;
            //$scope.createNewDatasource();
            $scope.gridOptions.api.setRowData([]);
            $scope.gridOptions.api.setRowData(response.data);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 700);
            if(response.data == null)
                $scope.gridOptions.api.setRowData([]);
        }, function (error) {
            console.log(error);
        });
    }
  

    $scope.GenReport = function (ReqDet, Type) {
        progress(0, 'Loading...', true);
        console.log(ReqDet);
      
        //woobj.Type = Type;
        $scope.InsValidity.Type = Type;
        $scope.InsValidity.FROM_DATE = ReqDet.FromDate
        $scope.InsValidity.TO_DATE = ReqDet.ToDate
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/Prop_Ins_Validity/GetGrid',
                method: 'POST',
                data: $scope.InsValidity,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'InsuranceValidity.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

                var columns = [{ title: "Property Type", key: "PPT_TYPE" }, { title: "Property Name", key: "PPT_NAME" }, { title: "Location", key: "LCM_NAME" },
                        { title: "Address", key: "PPT_ADDR" }, { title: "Owner)", key: "OWNER" }, { title: "Phone Number", key: "PHNO" },
                        { title: "Insurance Type", key: "INS_TYPE" }, { title: "Insurance Vendor", key: "INS_VENDOR" },
                        { title: "Insurance Amount", key: "INS_AMOUNT" }, { title: "Insurance Policy Number", key: "INS_PNO" },
                        { title: "Insurance Start Date", key: "INS_START_DT" }, { title: "Insurance End Date", key: "INS_END_DT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("PropInsuValidityReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "PropInsuValidityReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.InsValidity.FromDate = moment().format('MM/DD/YYYY');
                $scope.InsValidity.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.InsValidity.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.InsValidity.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.InsValidity.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.InsValidity.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.InsValidity.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.InsValidity.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.InsValidity.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.InsValidity.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.InsValidity.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.InsValidity.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.InsValidity.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.InsValidity.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;

        }
    }

    $timeout(function () { $scope.LoadGrid() }, 500);


});