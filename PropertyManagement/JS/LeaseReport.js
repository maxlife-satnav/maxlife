﻿app.service("LeaseReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseReport/GetGriddata', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetGrid = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseReport/GetGrid', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseReport/GetDetailsOnSelection/', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('LeaseReportController', function ($scope, $q, $http, LeaseReportService, UtilityService, $timeout) {
  
    $scope.GridVisiblity = true;
  
    $scope.Type = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.DocTypeVisible = 0;


    $scope.columnDefs = [
              { headerName: "Lease-Property Type", field: "PROP_TYPE", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Property Name", field: "PRP_NAME", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Lease Start Date", field: "LEASE_SDATE", cellClass: 'grid-align', width: 150, suppressMenu: true, },
              { headerName: "Lease End Date", field: "LEASE_EDATE", cellClass: 'grid-align', width: 200 },
              { headerName: "Monthly Rent", field: "MON_RENT", cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Security Deposit", field: "SEC_DEPOSIT", cellClass: 'grid-align', width: 300 },
              { headerName: "Broker Name", field: "BRO_NAME", cellClass: 'grid-align', width: 150 },
              { headerName: "Broker Fee", field: "BRO_FEE", cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Landlord Name", field: "LAND_NAME", cellClass: 'grid-align', width: 200 },
              { headerName: "Landlord Address", field: "LAND_ADDR", cellClass: 'grid-align', width: 150, suppressMenu: true, },
              { headerName: "Landlord Rent", field: "LAND_RENT", cellClass: 'grid-align', width: 200 },
              { headerName: "Landlord Security Deposit", field: "LAND_SEC_DEP", cellClass: 'grid-align', width: 200 }, ];
            
   
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {
   
            FromDate: $scope.LeaseRep.FromDate,
            ToDate: $scope.LeaseRep.ToDate
       
        };      
             
        $scope.GridVisiblity = true;
        console.log(params);
            LeaseReportService.GetGriddata(params).then(function (data) {               
                $scope.gridata = data.data;
                if ($scope.gridata == null) {

                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata);
                } 
                progress(0, 'Loading...', false);
            }, function (error) {
                console.log(error);
            });
       
    }    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };  
 

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Lease-Property Type", key: "PROP_TYPE" }, { title: "Property Name", key: "PRP_NAME" }, { title: "Lease Start Date", key: "LEASE_SDATE" },
            { title: "Lease End Date", key: "LEASE_EDATE" }, { title: "Monthly Rent", key: "MONTHLY_RENT" }, { title: "Security Deposit", key: "SEC_DEPOSIT" }, { title: "Broker Name", key: "BRO_NAME" },
            { title: "Broker Fee", key: "BRO_FEE" }, { title: "Landlord Name", key: "LAND_NAME" }, { title: "Landlord Address", key: "LAND_ADDR" }, { title: "Landlord Rent", key: "LAND_RENT" }, { title: "Landlord Security Deposit", key: "LAND_SEC_DEP" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("LeaseReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "LeaseReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (LeaseRep, Type) {
        progress(0, 'Loading...', true);
        LeaseRep.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (LeaseRep.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/LeaseReport/GetGrid',
                method: 'POST',
                data: LeaseRep,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
              
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'LeaseReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.LeaseRep.FromDate = moment().format('MM/DD/YYYY');
                $scope.LeaseRep.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.LeaseRep.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.LeaseRep.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.LeaseRep.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.LeaseRep.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.LeaseRep.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.LeaseRep.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.LeaseRep.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.LeaseRep.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.LeaseRep.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.LeaseRep.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.LeaseRep.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.LeaseRep.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;

        }
    }

    $timeout(function () { $scope.LoadData() }, 500);
});