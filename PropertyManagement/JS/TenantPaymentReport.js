﻿app.service("TenantPaymentReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/TenantPaymentReport/GetGriddata', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetGrid = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/TenantPaymentReport/GetGrid', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/TenantPaymentReport/GetDetailsOnSelection/', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('TenantPaymentReportController', function ($scope, $q, $http, TenantPaymentReportService, UtilityService, $timeout) {
  
    $scope.GridVisiblity = true;
  
    $scope.Type = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.DocTypeVisible = 0;


    $scope.columnDefs = [
              { headerName: "Tenant", field: "TENANT", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Property Name", field: "PRP_NAME", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Rent", field: "RENT", cellClass: 'grid-align', width: 150, suppressMenu: true, },
              { headerName: "Maintenance Fee", field: "MAIN_FEE", cellClass: 'grid-align', width: 200 },
              { headerName: "Total Rent Amount", field: "TOT_RENT", cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "Paid Date", field: "PAIDDATE", cellClass: 'grid-align', width: 300 },
              { headerName: "From Date", field: "FROMDATE", cellClass: 'grid-align', width: 150 },
              { headerName: "To Date", field: "TODATE", cellClass: 'grid-align', width: 100, suppressMenu: true, },
              { headerName: "TDS", field: "TDS", cellClass: 'grid-align', width: 200 },
              { headerName: "Payment Mode", field: "PAY_MODE", cellClass: 'grid-align', width: 150, suppressMenu: true, },
              { headerName: "Cheque No", field: "CHEQUENO", cellClass: 'grid-align', width: 200 },
              { headerName: "Cheque Date", field: "CHEQUEDATE", cellClass: 'grid-align', width: 200 },
              { headerName: "Issuing Bank", field: "ISS_BANK", cellClass: 'grid-align', width: 150, suppressMenu: true, },
              { headerName: "IFSC", field: "IFSC", cellClass: 'grid-align', width: 200 },
              { headerName: "Account Number", field: "ACCNUM", cellClass: 'grid-align', width: 200 }, ];
            
  
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {
            FromDate: $scope.TenantRep.FromDate,
            ToDate: $scope.TenantRep.ToDate
       
        };      
             
        $scope.GridVisiblity = true;
        console.log(params);
            TenantPaymentReportService.GetGriddata(params).then(function (data) {               
                $scope.gridata = data.data;
                console.log(data);
                var rent = 0;
                var Totrent = 0;
                $scope.TenantRep.Rent = 0;
                $scope.TenantRep.TotRent = 0;
                if (data.data == null) {
                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    for (i = 0; i < data.data.length; i++) {
                        rent = rent + data.data[i].RENT;
                        Totrent = Totrent + data.data[i].TOT_RENT;
                    }
                    $scope.TenantRep.Rent = rent;
                    $scope.TenantRep.TotRent = Totrent;
                    $scope.gridOptions.api.setRowData($scope.gridata);
                } 
                progress(0, 'Loading...', false);
            }, function (error) {
                console.log(error);
            });       
    }    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };  
 

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Lease-Property Type", key: "PROP_TYPE" }, { title: "Property Name", key: "PRP_NAME" }, { title: "Lease Start Date", key: "LEASE_SDATE" }, { title: "Lease End Date", key: "LEASE_EDATE" }, { title: "Monthly Rent", key: "MONTHLY_RENT" }, { title: "Security Deposit", key: "SEC_DEPOSIT" }, { title: "Broker Name", key: "BRO_NAME" }, { title: "Broker Fee", key: "BRO_FEE" }, { title: "Landlord Name", key: "LAND_NAME" }, { title: "Landlord Address", key: "LAND_ADDR" }, { title: "Landlord Rent", key: "LAND_RENT" }, { title: "Landlord Security Deposit", key: "LAND_SEC_DEP" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("TenantReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "TenantReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (TenantRep, Type) {
        progress(0, 'Loading...', true);
        TenantRep.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (TenantRep.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/TenantPaymentReport/GetGrid',
                method: 'POST',
                data: TenantRep,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
              
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'TenantReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }



    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.TenantRep.FromDate = moment().format('MM/DD/YYYY');
                $scope.TenantRep.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.TenantRep.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.TenantRep.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.TenantRep.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.TenantRep.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.TenantRep.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.TenantRep.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.TenantRep.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.TenantRep.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.TenantRep.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.TenantRep.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.TenantRep.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.TenantRep.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;

        }
    }

    $timeout(function () { $scope.LoadData() }, 500);

});