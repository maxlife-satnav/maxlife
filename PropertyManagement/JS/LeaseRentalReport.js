﻿app.service("LeaseRentalReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Rental) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseRentalReport/GetRentalDetails', Rental)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('LeaseRentalReportController', function ($scope, $q, $http, LeaseRentalReportService, UtilityService, $timeout, $filter) {
    $scope.Rental = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.GridVisiblity2 = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Country = [];
    $scope.Zone = [];
    $scope.zonlst = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.CNP_NAME = [];


    $scope.Pageload = function () {

        UtilityService.getZone(2).then(function (Znresponse) {
            if (Znresponse.data != null) {

                $scope.zonlst = Znresponse.data;

                angular.forEach($scope.zonlst, function (value, key) {
                    value.ticked = true;
                });
            }
        });
        
    }

    $scope.Cols = [

                        { COL: "Lease Name", value: "LEASE_NAME", ticked: false },
                        { COL: "Zone Cod", value: "ZN_CODE", ticked: false },
                        { COL: "Zone Name", value: "ZN_NAME", ticked: false },
                        { COL: "Location Code", value: "LOCATION_CODE", ticked: false },
                        { COL: "Location Name", value: "LOCATION_NAME", ticked: false },

                        { COL: "Landlord Code", value: "LANDLORD_CODE", ticked: false },
                        { COL: "Landlord Name", value: "LANDLORD_NAME", ticked: false },

                        { COL: "Lease Start Date", value: "LEASE_START_DATE", ticked: false },
                        { COL: "Lease Expiry Date", value: "LEASE_END_DATE", ticked: false },
                        //{ COL: "Type of Agreement", value: "TYPE_OF_AGREEMENT", ticked: false },
                          { COL: "Current Monthly Rental (A)", value: "RENT", ticked: false },
                            { COL: "Current Maintenace Cost (B)", value: "MAINTENANCE", ticked: false },
                              { COL: "Other Contractual Payouts (C)", value: "OTHER", ticked: false },
                        { COL: "Gross Monthly Rent", value: "GROSS_MONTHLY_RENT", ticked: false },
                        { COL: "Security Deposit", value: "SECURITY_DEPOSIT", ticked: false },

                        { COL: "Remarks", value: "REMARKS", ticked: false },
                        { COL: "Rental Payment Validation", value: "RENTAL_PAYMENT_VALIDATION", ticked: false },
                        { COL: "Carry Forward Rent", value: "CARRY_FORWARD_RENT", ticked: false },
                        { COL: "Payment Date", value: "PAYMENT_DATE", ticked: false },
                        { COL: "Validation Date", value: "VALIDATION_DATE", ticked: false },
                        { COL: "Rental Validator User", value: "RENTAL_VALIDATOR", ticked: false },
    ];

    $scope.columnDefs = [


                        { headerName: "Lease name", field: "LEASE_NAME", cellClass: 'grid-align', width: 130, },
                        { headerName: "Zone Code", field: "ZN_CODE", cellClass: 'grid-align', width: 100, },
                        { headerName: "Zone Name", field: "ZN_NAME", cellClass: 'grid-align', width: 100, },
                        { headerName: "Location Code", field: "LOCATION_CODE", cellClass: 'grid-align', width: 100, },
                        { headerName: "Location Name", field: "LOCATION_NAME", cellClass: 'grid-align', width: 100, },
                        { headerName: "Landlord Code", field: "LANDLORD_CODE", cellClass: 'grid-align', width: 130, },
                        { headerName: "Landlord Name", field: "LANDLORD_NAME", cellClass: 'grid-align', width: 130, },
                        { headerName: "Lease Start Date", field: "LEASE_START_DATE", template: '<span>{{data.LEASE_START_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, },
                        { headerName: "Lease Expiry Date", field: "LEASE_END_DATE", template: '<span>{{data.LEASE_END_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, },
                        //{ headerName: "Type of Agreement", field: "TYPE_OF_AGREEMENT", cellClass: 'grid-align', width: 100, },
                        { headerName: "Current Monthly Rental (A)", field: "RENT", cellClass: 'grid-align', width: 100, },
                        { headerName: "Current Maintenace Cost (B)", field: "MAINTENANCE", cellClass: 'grid-align', width: 100, },
                        { headerName: "Other Contractual Payouts (C)", field: "OTHER", cellClass: 'grid-align', width: 100, },
                        { headerName: "Gross Monthly Rent (A+B+C)", field: "GROSS_MONTHLY_RENT", cellClass: 'grid-align', width: 130, suppressMenu: true, },
                        { headerName: "Security Deposit", field: "SECURITY_DEPOSIT", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
                        { headerName: "Remarks", field: "REMARKS", cellClass: 'grid-align', width: 130,},
                        { headerName: "Rental Payment Validation", field: "RENTAL_PAYMENT_VALIDATION", cellClass: 'grid-align', width: 130 },
                        { headerName: "Carry Forward Rent", field: "CARRY_FORWARD_RENT", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
                        { headerName: "Payment Date", field: "PAYMENT_DATE",  cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
                        { headerName: "Validation Date", field: "VALIDATION_DATE",  cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
                        { headerName: "Rental Validator User", field: "RENTAL_VALIDATOR", cellClass: 'grid-align', width: 130, hide: true },
    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            BUILTUP_AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.BUILTUP_AREA += parseFloat((data.BUILTUP_AREA).toFixed(2));
        });
        return sums;
    }

    //setTimeout(function () {
    //    progress(0, 'Loading...', true);


    // }, 500);

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var tickedZones = [];
        tickedZones = $filter('filter')($scope.zonlst, { ticked: true });
        var params = {
            FromDate: $scope.Rental.FromDate,
            ToDate: $scope.Rental.ToDate,
            zonlst: tickedZones
        };
        
        LeaseRentalReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.GridVisiblity2 = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', data.Message);
            }
            else {

                $scope.GridVisiblity = true;
                $scope.GridVisiblity2 = true;

                $scope.gridOptions.api.refreshHeader();
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
                progress(0, '', false);
            }

        });

    }, function (error) {
        console.log(error);
    }

    $timeout($scope.LoadData, 1000);    
    
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Lease name", key: "LEASE_NAME" }, { title: "Zone Code", key: "ZN_CODE" },
             { title: "Zone Name", key: "ZN_NAME" },
            { title: "Location Code", key: "Location_Code" }, { title: "Location Name", key: "Location_Name" }, { title: "Landlord Code", key: "LANDLORD_CODE" },
            { title: "Landlord Name", key: "LANDLORD_NAME" }, { title: "Lease Start Date", key: "LEASE_START_DATE" }, { title: "Lease End Date", key: "LEASE_END_DATE" },
            { title: "Type of Agreement", key: "Type_of_Agreement" },
            { title: "Gross Monthly Rent", key: "Gross_Monthly_Rent" }, { title: "Security Deposit", key: "SECURITY_DEPOSIT" },
            { title: "Remarks", key: "Remarks" }, { title: "Rental Payment Validation", key: "RENTAL_PAYMENT_VALIDATION" },
            { title: "Validation Date", key: "VALIDATION_DATE" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("LeaseRentalReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "LeaseRentalReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Rental, Type) {
        progress(0, 'Loading...', true);
        Rental.Type = Type;
        Rental.Request_Type = 2;

        Rental.zonlst = $scope.zonlst;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/LeaseRentalReport/GetRentalData',
                method: 'POST',
                data: Rental,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'LeaseRentalReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.selVal = "LASTMONTH";
    $scope.rptDateRanges = function () {        
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Rental.FromDate = moment().format('MM/DD/YYYY');
                $scope.Rental.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Rental.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Rental.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Rental.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Rental.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Rental.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Rental.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Rental.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                console.log($scope.Rental.FromDate);
                $scope.Rental.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Rental.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Rental.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.Rental.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.Rental.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.Pageload();

    $scope.rptDateRanges();

});
