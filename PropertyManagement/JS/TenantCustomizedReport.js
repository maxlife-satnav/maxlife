﻿app.service("TenantCustomizedReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/TenantCustomizedReport/GetCustomizedDetails', Customized)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('TenantCustomizedReportController', function ($scope, $q, $http, TenantCustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.GridVisiblity2 = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Country = [];
    $scope.Zone = [];
    $scope.State = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.Company = [];



    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                //angular.forEach($scope.Country, function (value, key) {
                //    value.ticked = true;
                //});
                UtilityService.getZone(2).then(function (Znresponse) {
                    if (Znresponse.data != null) {

                        $scope.Zone = Znresponse.data;
                    }
                    UtilityService.getState(2).then(function (Stresponse) {
                        if (Stresponse.data != null) {

                            $scope.State = Stresponse.data;
                        }
                        UtilityService.getCities(2).then(function (response) {
                            if (response.data != null) {
                                $scope.City = response.data;
                                //angular.forEach($scope.City, function (value, key) {
                                //    value.ticked = true;
                                //});
                                UtilityService.getLocations(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Locations = response.data;
                                        //angular.forEach($scope.Locations, function (value, key) {
                                        //    value.ticked = true;
                                        //    $scope.Customized.Locations.push(value);
                                        //});

                                        //setTimeout(function () {
                                        //    $scope.LoadData();
                                        //}, 1000);
                                    }
                                });

                            }
                        });
                    });
                });
            }
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getZonebyCny($scope.Customized.Country, 1).then(function (response) {
            $scope.Zone = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getZonebyCny();
    }
    $scope.cnySelectNone = function () {
        $scope.Customized.Country = [];
        $scope.getZonebyCny();
    }

    $scope.ZoneChanged = function () {
        UtilityService.getStateByZone($scope.Customized.Zone, 1).then(function (response) {
            if (response.data != null)
                $scope.State = response.data;
            else
                $scope.State = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;

                $scope.Customized.Country[0] = cny;
            }
        });
    }
    $scope.ZoneChangeAll = function () {
        $scope.Customized.Zone = $scope.Zone;
        $scope.ZoneChanged();
    }
    $scope.ZoneSelectNone = function () {
        $scope.Customized.Zone = [];
        $scope.ZoneChanged();
    }

    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.Customized.State, 1).then(function (response) {
            if (response.data != null)
                $scope.City = response.data;
            else
                $scope.City = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.State, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.State, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.Customized.Zone[0] = zn;
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.Customized.State = $scope.State;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.Customized.State = [];
        $scope.SteChanged();
    }


    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 1).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Zone[0] = cny;
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.State[0] = cny;
            }
        });
    }
    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.ctySelectNone = function () {
        $scope.Customized.City = [];
        $scope.getLocationsByCity();
    }

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.LocationChange();
    }
    $scope.LocationChange = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Zone[0] = cny;
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.State[0] = cny;
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
    }
    $scope.lcmSelectNone = function () {
        $scope.Customized.Locations = [];
        $scope.LocationChange();
    }


    $scope.Cols = [

                { COL: "Property Code", value: "PM_PPT_CODE", ticked: false },
                { COL: "Property Name", value: "PM_PPT_NAME", ticked: false },
                { COL: "Property Type", value: "PN_PROPERTYTYPE", ticked: false },

                { COL: "Property Address", value: "PM_PPT_ADDRESS", ticked: false },
                { COL: "Location", value: "LCM_NAME", ticked: false },
                { COL: "City", value: "CTY_NAME", ticked: false },
                //{ COL: "Country", value: "CNY_NAME", ticked: false },

                { COL: "Owner Name", value: "PM_OWN_NAME", ticked: false },
                { COL: "Tenant Code", value: "PM_TEN_CODE", ticked: false },
                { COL: "Tenant Name", value: "TEN_NAME", ticked: false },
                { COL: "Tenant Start Date", value: "PM_TEN_FRM_DT", ticked: false },
                { COL: "Tenant End Date", value: "PM_TEN_TO_DT", ticked: false },
                { COL: "Tenant Status", value: "TEN_STATUS", ticked: false },

                { COL: "Total Rent Paid", value: "PM_TD_RENT", ticked: false },
                { COL: "Tenant Occupied Area", value: "PM_AR_CARPET_AREA", ticked: false },
                { COL: "Maintenance Fee", value: "PM_TD_MAINT_FEES", ticked: false },
                { COL: "Joining Date", value: "PM_TD_JOIN_DT", ticked: false },

                { COL: "Security Deposit", value: "PM_TD_SECURITY_DEPOSIT", ticked: false },
                { COL: "Tenant Email", value: "TEN_EMAIL", ticked: false },
                { COL: "Payment Terms", value: "PM_PT_NAME", ticked: false },



    ];

    $scope.columnDefs = [


                { headerName: "Property Code", field: "PM_PPT_CODE", cellClass: 'grid-align', width: 130, hide: true },
                { headerName: "Property Name", field: "PM_PPT_NAME", cellClass: 'grid-align', width: 130 },
                { headerName: "Property Type", field: "PN_PROPERTYTYPE", cellClass: 'grid-align', width: 130, },

                { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 130, suppressMenu: true, },
                { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 150, },
                { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 150, },
                //{ headerName: "Country", field: "CNY_NAME", cellClass: 'grid-align', width: 150, },

                { headerName: "Owner Name", field: "PM_OWN_NAME", cellClass: 'grid-align', width: 130, },

                { headerName: "Tenant Code", field: "PM_TEN_CODE", width: 100, cellClass: 'grid-align', width: 130, hide: true },
                { headerName: "Tenant Name", field: "TEN_NAME", cellClass: 'grid-align', width: 130, },
                { headerName: "Tenant Start Date", field: "PM_TEN_FRM_DT", cellClass: 'grid-align', width: 130, template: '<span>{{data.PM_TEN_FRM_DT | date:"dd MMM, yyyy"}}</span>', suppressMenu: true, },
                { headerName: "Tenant End Date", field: "PM_TEN_TO_DT", cellClass: 'grid-align', width: 130, template: '<span>{{data.PM_TEN_TO_DT | date:"dd MMM, yyyy"}}</span>', suppressMenu: true, },

                { headerName: "Total Rent Paid", field: "PM_TD_RENT", cellClass: 'grid-align', width: 100, suppressMenu: true },
                { headerName: "Tenant Occupied Area", field: "PM_AR_CARPET_AREA", cellClass: 'grid-align', width: 150, suppressMenu: true, },
                { headerName: "Maintenance Fee", field: "PM_TD_MAINT_FEES", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
                { headerName: "Next Payable Date", field: "PM_TD_JOIN_DT", cellClass: 'grid-align', width: 150, template: '<span>{{data.PM_TD_JOIN_DT | date:"dd MMM, yyyy"}}</span>', hide: true, suppressMenu: true, },
                { headerName: "Security Deposit", field: "TEN_SECURITY_DEPOSIT", cellClass: 'grid-align', width: 130, hide: true, suppressMenu: true, },

                { headerName: "Tenant Email", field: "TEN_EMAIL", cellClass: 'grid-align', width: 180, suppressMenu: true, },
                { headerName: "Payment Terms", field: "PM_PT_NAME", cellClass: 'grid-align', width: 150, },
                 { headerName: "Tenant Status", field: "TEN_STATUS", cellClass: 'grid-align', width: 130, pinned: 'right' },


    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            PM_AR_CARPET_AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.PM_AR_CARPET_AREA += parseFloat((data.PM_AR_CARPET_AREA).toFixed(2));
        });
        return sums;
    }
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            loclst: $scope.Customized.Locations

        };
       
        TenantCustomizedReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.GridVisiblity2 = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', data.Message);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.GridVisiblity = true;
                $scope.GridVisiblity2 = true;
                progress(0, 'Loading...', false);
            }

           
        });

    }, function (error) {
        console.log(error);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Property Name", key: "PROPERTY_NAME" }, { title: "Property Type", key: "PN_PROPERTYTYPE" }, { title: "Location", key: "LCM_NAME" },
            { title: "Property Address", key: "PROPERTY_DESCRIPTION" }, { title: "Owner Name", key: "OWNERNAME" }, { title: "Builtup Area", key: "BUILTUP_AREA" }, { title: "Tenant Name", key: "TEN_NAME" },
            { title: "Tenant Start Date", key: "TEN_COMMENCE_DATE" }, { title: "Next Payable Date", key: "TEN_NEXTPAYABLEDATE" }, { title: "Amount Paid", key: "RENTPAID" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("TenantCustomizableReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "TenantCustomizableReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Customized.Type = Type;
        Customized.Request_Type = 2;

        Customized.loclst = Customized.Locations;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/TenantCustomizedReport/GetCustomizedData',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'TenantCustomizableReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.selVal = "THISYEAR";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.Customized.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;

        }
    }
    $scope.Pageload();
    //$timeout($scope.LoadData(1), 1000);



});