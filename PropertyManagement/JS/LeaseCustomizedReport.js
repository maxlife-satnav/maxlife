﻿app.service("LeaseCustomizedReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseCustomizedReport/GetCustomizedDetails', Customized)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('LeaseCustomizedReportController', function ($scope, $q, $http, LeaseCustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.GridVisiblity2 = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Country = [];
    $scope.Zone = [];
    $scope.State = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.CNP_NAME = [];


    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                //angular.forEach($scope.Country, function (value, key) {
                //    value.ticked = true;
                //});
                UtilityService.getZone(2).then(function (Znresponse) {
                    if (Znresponse.data != null) {

                        $scope.Zone = Znresponse.data;
                    }
                    UtilityService.getState(2).then(function (Stresponse) {
                        if (Stresponse.data != null) {

                            $scope.State = Stresponse.data;
                        }
                        UtilityService.getCities(2).then(function (response) {
                            if (response.data != null) {
                                $scope.City = response.data;
                                //angular.forEach($scope.City, function (value, key) {
                                //    value.ticked = true;
                                //});
                                UtilityService.getLocations(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Locations = response.data;
                                        //angular.forEach($scope.Locations, function (value, key) {
                                        //    value.ticked = true;
                                        //})
                                    }

                                });
                            }
                        });
                    });
                });

            }
        });



    }

    $scope.getZonebyCny = function () {
        UtilityService.getZoneByCny($scope.Customized.Country, 2).then(function (response) {
            $scope.Zone = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getZonebyCny();
    }
    $scope.cnySelectNone = function () {
        $scope.Customized.Country = [];
        $scope.getZonebyCny();
    }

    $scope.ZoneChanged = function () {
        UtilityService.getStateByZone($scope.Customized.Zone, 1).then(function (response) {
            if (response.data != null)
                $scope.State = response.data;
            else
                $scope.State = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;

                $scope.Customized.Country[0] = cny;
            }
        });
    }
    $scope.ZoneChangeAll = function () {
        $scope.Customized.Zone = $scope.Zone;
        $scope.ZoneChanged();
    }
    $scope.ZoneSelectNone = function () {
        $scope.Customized.Zone = [];
        $scope.ZoneChanged();
    }

    $scope.SteChanged = function () {
        UtilityService.getCityByState($scope.Customized.State, 1).then(function (response) {
            if (response.data != null)
                $scope.City = response.data;
            else
                $scope.City = [];
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.State, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.State, function (value, key) {
            var zn = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.Customized.Zone[0] = zn;
            }
        });
    }
    $scope.SteChangeAll = function () {
        $scope.Customized.State = $scope.State;
        $scope.SteChanged();
    }
    $scope.SteSelectNone = function () {
        $scope.Customized.State = [];
        $scope.SteChanged();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Zone[0] = cny;
            }
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.State[0] = cny;
            }
        });
    }
    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.ctySelectNone = function () {
        $scope.Customized.City = [];
        $scope.getLocationsByCity();
    }

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.LocationChange();
    }
    $scope.LocationChange = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Zone, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Zone, { ZN_CODE: value.ZN_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Zone[0] = cny;
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.State, { STE_CODE: value.STE_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.State[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
    }
    $scope.lcmSelectNone = function () {
        $scope.Customized.Locations = [];
        $scope.LocationChange();
    }

    $scope.Cols = [

                { COL: "Lease Name", value: "LEASE_NAME", ticked: false },
                //{ COL: "Lease Type", value: "PN_LEASE_TYPE", ticked: false },
                { COL: "Lease Start Date", value: "LEASE_START_DATE", ticked: false },
                { COL: "Lease End Date", value: "LEASE_END_DATE", ticked: false },

                //{ COL: "Country", value: "COUNTRY", ticked: false },
                { COL: "City", value: "CITY", ticked: false },
                { COL: "Location", value: "LOCATION", ticked: false },


                { COL: "Landlord Name", value: "LANDLORD_NAME", ticked: false },
                { COL: "Landlord Address", value: "LANDLORD_ADDRESS", ticked: false },
                { COL: "Lease Monthly Rent", value: "MONTHLY_RENT", ticked: false },
                //{ COL: "Lesse Name", value: "EMPLOYEE_NAME", ticked: false },
                //{ COL: "Amount Paid", value: "PAID_AMOUNT", ticked: false },


                { COL: "Property Type", value: "PN_PROPERTYTYPE", ticked: false },
                //{ COL: "Builtup area", value: "BUILTUP_AREA", ticked: false },
                //{ COL: "Property Address", value: "COMPLETE_ADDRESS", ticked: false },

                { COL: "Security Deposit", value: "SECURITY_DEPOSIT", ticked: false },
                { COL: "Maintenance Charges", value: "MAINTENANCE_CHARGES", ticked: false },
                { COL: "Service Tax", value: "SERVICE_TAX", ticked: false },
                 { COL: "Lock In Period(Months)", value: "PM_LAD_LOCK_INPERIOD", ticked: false },
                //{ COL: "Rent Revision", value: "RENT_REVISION", ticked: false },
                { COL: "Lease Escalation", value: "LEASE_ESCALATION", ticked: false },

    ];

    $scope.columnDefs = [


                { headerName: "Lease name", field: "LEASE_NAME", cellClass: 'grid-align', width: 130, },
                { headerName: "Lease Type", field: "PN_LEASE_TYPE", cellClass: 'grid-align', width: 130 },
                { headerName: "Lease Start Date", field: "LEASE_START_DATE", template: '<span>{{data.LEASE_START_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: true, },
                { headerName: "Lease End Date", field: "LEASE_END_DATE", template: '<span>{{data.LEASE_END_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: true, },
                { headerName: "Lease Extension Date", field: "EXTESNION_TODATE", template: '<span>{{data.EXTESNION_TODATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: true, },

                //{ headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true },
                 { headerName: "Zone", field: "ZONE", cellClass: 'grid-align', width: 100, },
                { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100, },
                { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 100, },

                { headerName: "Landlord name", field: "LANDLORD_NAME", cellClass: 'grid-align', width: 130, },
                { headerName: "Landlord Address", field: "LANDLORD_ADDRESS", cellClass: 'grid-align', width: 130, suppressMenu: true },
                { headerName: "Lease Monthly Rent", field: "MONTHLY_RENT", cellClass: 'grid-align', width: 130, suppressMenu: true, },
                //{ headerName: "Lesse Name", field: "EMPLOYEE_NAME", cellClass: 'grid-align', width: 130 },
                //{ headerName: "Amount Paid", field: "PAID_AMOUNT", cellClass: 'grid-align', width: 130 },


                { headerName: "Property Type", field: "PN_PROPERTYTYPE", cellClass: 'grid-align', width: 130 },
                //{ headerName: "Builtup area", field: "BUILTUP_AREA", cellClass: 'grid-align', width: 130, aggFunc: 'sum' },
                //{ headerName: "Property Address", field: "COMPLETE_ADDRESS", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },

                { headerName: "Security Deposit", field: "SECURITY_DEPOSIT", cellClass: 'grid-align', width: 130, suppressMenu: true },
                { headerName: "Maintenance charges", field: "MAINTENANCE_CHARGES", cellClass: 'grid-align', width: 130, suppressMenu: true },
                { headerName: "Service Tax", field: "SERVICE_TAX", cellClass: 'grid-align', width: 130, suppressMenu: true},
                { headerName: "Lock In Period(Months)", field: "PM_LAD_LOCK_INPERIOD", cellClass: 'grid-align', width: 130, suppressMenu: true },
                //{ headerName: "Rent Revision", field: "RENT_REVISION", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
                { headerName: "Lease Escalation", field: "LEASE_ESCALATION", cellClass: 'grid-align', width: 130, hide: true },


             // { headerName: "Lease Renewal Staus", field: "RENEWAL_STATUS", cellClass: 'grid-align', width: 100, suppressMenu: true, }

    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            BUILTUP_AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.BUILTUP_AREA += parseFloat((data.BUILTUP_AREA).toFixed(2));
        });
        return sums;
    }

    //setTimeout(function () {
    //    progress(0, 'Loading...', true);


    // }, 500);

    $scope.LoadData = function () {
        var params = {
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            loclst: $scope.Customized.Locations
        };
        LeaseCustomizedReportService.GetGriddata(params).then(function (data) {
            console.log(data);
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.GridVisiblity2 = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', data.Message);
            }
            else {
               
                $scope.GridVisiblity = true;
                $scope.GridVisiblity2 = true;

                $scope.gridOptions.api.refreshHeader();
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
                progress(0, '', false);
            }
           
        });

    }, function (error) {
        console.log(error);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Lease name", key: "LEASE_NAME" }, { title: "Lease Type", key: "PN_LEASE_TYPE" }, { title: "Lease Start Date", key: "LEASE_START_DATE" }, { title: "Lease End Date", key: "LEASE_END_DATE" }, { title: "Lease Extension Date", key: "EXTESNION_TODATE" }, { title: "Zone", key: "ZONE" }, { title: "City", key: "CITY" },
            { title: "Location", key: "LOCATION" }, { title: "Landlord Name", key: "LANDLORD_NAME" }, { title: "Landlord Address", key: "LANDLORD_ADDRESS" },
             { title: "Property Type", key: "PN_PROPERTYTYPE" }, { title: "Security Deposit", key: "SECURITY_DEPOSIT" }, { title: "Maintenance Charges", key: "MAINTENANCE_CHARGES" }, { title: "Service Tax", key: "SERVICE_TAX" }, { title: "Lease Escalation", key: "LEASE_ESCALATION" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("LeaseCustomizableReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "LeaseCustomizableReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Customized.Type = Type;
        Customized.Request_Type = 2;

        Customized.loclst = Customized.Locations;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/LeaseCustomizedReport/GetCustomizedData',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'LeaseCustomizableReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.selVal = "THISYEAR";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.Customized.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.Pageload();

});
