﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper
Imports System.IO
Imports System.Data.OleDb
Partial Class PropertyManagement_Views_PropertiesForLease
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            fillReqs()
            If Request.QueryString("back") <> Nothing Then
                panel1.Visible = True
                GetProperties(Request.QueryString("back"))
            Else
                panel1.Visible = False
            End If
        End If
    End Sub

    Private Sub fillReqs()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_APPROVED_PROPERTIES_FOR_LEASE")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)

        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvReqs.DataSource = ds
        gvReqs.DataBind()
        Session("Reqs") = ds
    End Sub

    Protected Sub gvReqs_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvReqs.PageIndexChanging
        gvReqs.PageIndex = e.NewPageIndex()
        gvReqs.DataSource = Session("Reqs")
        gvReqs.DataBind()
    End Sub

    Protected Sub gvrReqProperties_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvrReqProperties.PageIndexChanging
        gvrReqProperties.PageIndex = e.NewPageIndex()
        gvrReqProperties.DataSource = Session("reqDetails")
        gvrReqProperties.DataBind()
    End Sub

    Private Sub GetProperties(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES_BY_REQID")
        sp.Command.AddParameter("@REQ_ID", Reqid, DbType.String)

        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub

    Protected Sub gvReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqs.RowCommand
        Try
            If e.CommandName = "GetProperties" Then
                panel1.Visible = True
                hdnReqid.Value = e.CommandArgument

                GetProperties(hdnReqid.Value)
                Response.Redirect("~/PropertyManagement/Views/frmHRAddLease.aspx?Rid=" + hdnReqid.Value)
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvrReqProperties_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvrReqProperties.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbl As Label = e.Row.FindControl("lblStsValue")
            Dim sts As String = DirectCast(e.Row.DataItem, System.Data.DataRowView).Row.ItemArray(13)
            'e.Row.Cells(9).Enabled = IIf(sts <> "1", True, False)

            If sts = "4001" Then

                ' e.Row.Cells(11).Text = "Edit"
                Dim txt = e.Row.Cells(11).Text
            Else
                Dim txt = e.Row.Cells(11).Text

                ' e.Row.Cells(11).Text = "View"
            End If
        End If
    End Sub

    Protected Sub gvrReqProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvrReqProperties.RowCommand
        Try
            If e.CommandName = "VIEW" Then
                Dim lnkView As LinkButton = DirectCast(e.CommandSource, LinkButton)
                Dim dealId As String = lnkView.CommandArgument
                Dim sts = gvrReqProperties.Rows(0).Cells(10).Text
            Else

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
    '    Response.Redirect("~/PropertyManagement/Views/frmAddPropertyDetails.aspx?Rid=" + hdnReqid.Value)
    'End Sub
 


    Protected Sub btnSearchRequest_Click(sender As Object, e As EventArgs) Handles btnSearchRequest.Click
        If txtFilterText.Text = "" Then
            fillReqs()
        Else
            lblmsg.Visible = False
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES_BY_FILTER")
            sp.Command.AddParameter("@PROP_NAME", txtFilterText.Text, DbType.String)

            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvReqs.DataSource = ds
            gvReqs.DataBind()

        End If
    End Sub
    Protected Sub Btnupload_Click(sender As Object, e As EventArgs) Handles Btnupload.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                'Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filename As String = Path.GetFileNameWithoutExtension(fpBrowseDoc.FileName) & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                'Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename
                Dim filepath As String = Replace(Server.MapPath("~\UploadFiles\"), "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                'fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "~\UploadFiles\" + filename)
                fpBrowseDoc.SaveAs(filepath)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim str1 As String = ""
                Dim sheetname As String = ""
                Dim sheetname1 As String = ""
                Dim msheet As String = ""
                Dim msheet1 As String = ""
                Dim mfilename As String = ""
                Dim mfilename1 As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                msheet1 = listSheet(1).ToString()
                mfilename1 = msheet1
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    sheetname1 = mfilename1
                    str1 = "Select * from [" & sheetname1 & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim cmd1 As New OleDbCommand(str1, con)
                Dim ds As New DataSet
                Dim ds1 As New DataSet
                Dim da As New OleDbDataAdapter
                Dim da1 As New OleDbDataAdapter
                da.SelectCommand = cmd
                da1.SelectCommand = cmd1
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                da1.Fill(ds1, sheetname1.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If

                If Trim(LCase(ds.Tables(0).Columns(0).ToString)) <> "location" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 1: Column name should be Location"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(1).ToString)) <> "location code" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 2: Column name should be Location Code"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(2).ToString)) <> "zone" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 3: Column name should be Zone"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(3).ToString)) <> "property code" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 4: Column name should be Property Code"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(4).ToString)) <> "property address" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 5: Column name should be Property Address"
                    Exit Sub

                ElseIf Trim(LCase(ds.Tables(0).Columns(5).ToString)) <> "carpet area" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 6: Column name should be Carpet Area"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(6).ToString)) <> "super built up area" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 7: Column name should be Super Built Up Area"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(7).ToString)) <> "lease tenure" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 8: Column name should be Lease Tenure"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(8).ToString)) <> "lease start date" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 9: Column name should be Lease Start Date"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(9).ToString)) <> "lease end date" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 10: Column name should be Lease End Date "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(10).ToString)) <> "lock in period" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 11: Column name should be Lock In Period"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(11).ToString)) <> "rent free period" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 12: Column name should be Rent Free Period"
                    Exit Sub
                
                ElseIf Trim(LCase(ds.Tables(0).Columns(12).ToString)) <> "initial monthly rental" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 13: Column name should be Initial Monthly Rental "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(13).ToString)) <> "current monthly rental" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 14: Column name should be Current Monthly Rental "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(14).ToString)) <> "maintenace cost" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 15: Column name should be Maintenace Cost "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(15).ToString)) <> "contractual payout" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 16: Column name should be Contractual Payout "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(16).ToString)) <> "total payout" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 17: Column name should be Total Payout "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(17).ToString)) <> "rental escalation percentage" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 18: Column name should be Rental Escalation Percentage"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(18).ToString)) <> "rental escalation interval" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 19: Column name should be Rental escalation interval"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(19).ToString)) <> "security deposit" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 20: Column name should be Security Deposit "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(20).ToString)) <> "any other deposit" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 21: Column name should be Any other deposit "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(21).ToString)) <> "security deposit adjustment right" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 22: Column name should be Security Deposit adjustment right"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(22).ToString)) <> "tax liability" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 23: Column name should be Tax Liability"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(23).ToString)) <> "electricity connection" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 24: Column name should be Electricity connection "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(24).ToString)) <> "electricity load" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 25: Column name should be Electricity Load "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(25).ToString)) <> "signage space right" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 26: Column name should be Signage space right"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(26).ToString)) <> "rights to put antenna" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 27: Column name should be Rights to put antenna "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(27).ToString)) <> "generator/back up" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 28: Column name should be Generator/Back up"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(28).ToString)) <> "parking" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 29: Column name should be Parking "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(29).ToString)) <> "property attorned" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 30: Column name should be Property Attorned "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(30).ToString)) <> "termination notice period" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 31: Column name should be Termination notice period "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(31).ToString)) <> "stamp duty/ registration charges" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 32: Column name should be Stamp Duty/ Registration Charges  "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(32).ToString)) <> "lease registration status" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 33: Column name should be Lease Registration status "
                    Exit Sub
             
                End If

                If Trim(LCase(ds1.Tables(0).Columns(0).ToString)) <> "property code" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "At Column 1: Column name should be Property Code"
                    Exit Sub
                ElseIf Trim(LCase(ds1.Tables(0).Columns(1).ToString)) <> "lessor code" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 2: Column name should be Lessor Code"
                    Exit Sub
                ElseIf Trim(LCase(ds1.Tables(0).Columns(2).ToString)) <> "number of lessors" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 3: Column name should be Number of Lessors"
                    Exit Sub
                ElseIf Trim(LCase(ds1.Tables(0).Columns(3).ToString)) <> "land lord name" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 4: Column name should be Land Lord Name "
                    Exit Sub
                ElseIf Trim(LCase(ds1.Tables(0).Columns(4).ToString)) <> "land lord address" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 5: Column name should be Land Lord Address"
                    Exit Sub
                ElseIf Trim(LCase(ds1.Tables(0).Columns(5).ToString)) <> "land lord state" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 6: Column name should be Land Lord State"
                    Exit Sub

                ElseIf Trim(LCase(ds1.Tables(0).Columns(6).ToString)) <> "land lord city" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 7: Column name should be Land Lord City"
                    Exit Sub
                ElseIf Trim(LCase(ds1.Tables(0).Columns(7).ToString)) <> "mode of payment of rent" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 8: Column name should be Mode of payment of rent"
                    Exit Sub
                ElseIf Trim(LCase(ds1.Tables(0).Columns(8).ToString)) <> "landlord bank details" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 9: Column name should be Landlord Bank Details"
                    Exit Sub

                ElseIf Trim(LCase(ds1.Tables(0).Columns(9).ToString)) <> "landlord bank ifsc code" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 10: Column name should be Landlord Bank IFSC Code"
                    Exit Sub

                ElseIf Trim(LCase(ds1.Tables(0).Columns(10).ToString)) <> "land lord security deposite" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 11: Column name should be Land Lord Security Deposite"
                    Exit Sub

               
                End If

                Dim remarks As String = ""



                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "location" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Location is null or empty, "
                            End If
                        End If


                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "location code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Location Code Code is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "zone" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Zone is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "property code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Property Code is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "property address" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Property Address is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "property address" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Property Address is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "carpet area" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Carpet Area is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "super built up area" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Super Built Up Area is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "lease tenure" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Lease Tenure is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "lease start date" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Lease Start Date is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "lease End Date" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Lease End Date is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "lock in period" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Lock In Period is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "rent free period" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Rent Free Period is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "initial monthly rental" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Initial Monthly Rental Date is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "rent free period" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Rent Free Period is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "initial monthly rental" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Initial Monthly Rental is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "current monthly rental" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Current Monthly Rental is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "maintenace cost" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Maintenace Cost is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "contractual payout" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Contractual Payout is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Total Payout" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Total Payout  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "rental escalation percentage" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Rental Escalation Percentage  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "rental escalation interval" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Rental escalation interval is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "security deposit" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Security Deposit  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "any other deposit" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Any other deposit   is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "security deposit adjustment right" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Security Deposit adjustment right is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "tax liability" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Tax Liability  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "electricity connection" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Electricity connection is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "electricity load" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Electricity Load  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "signage space right Then" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Signage space right is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "rights to put antenna" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Rights to put antenna is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "generator/back up" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "generator/back up is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "parking" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Parking is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "property attorned" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Property Attorned  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "termination notice period" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Termination notice period  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "stamp duty/ registration charges " Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Stamp Duty/ Registration Charges  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Stamp Duty/ Registration Charges" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Stamp Duty/ Registration Charges  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Lease Registration status" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + "Lease Registration status  is null or empty, "
                            End If
                        End If
                        
                    Next




                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPLOAD_PROPERTY_LEASE_DATA")
                    sp.Command.AddParameter("@LOC", ds.Tables(0).Rows(i).Item(0).ToString, DbType.String)
                    sp.Command.AddParameter("@LOC_CODE", ds.Tables(0).Rows(i).Item(1).ToString, DbType.String)
                    sp.Command.AddParameter("@ZONE", ds.Tables(0).Rows(i).Item(2).ToString, DbType.String)
                    sp.Command.AddParameter("@PROP_CODE", ds.Tables(0).Rows(i).Item(3).ToString, DbType.String)
                    sp.Command.AddParameter("@PROP_ADDR", ds.Tables(0).Rows(i).Item(4).ToString, DbType.String)
                    sp.Command.AddParameter("@CARPET", ds.Tables(0).Rows(i).Item(5).ToString, DbType.String)
                    sp.Command.AddParameter("@SPR_BUILTUP_AREA", ds.Tables(0).Rows(i).Item(6).ToString, DbType.String)
                    sp.Command.AddParameter("@LEASE_TENURE", ds.Tables(0).Rows(i).Item(7).ToString, DbType.String)
                    sp.Command.AddParameter("@LS_ST_DT", ds.Tables(0).Rows(i).Item(8).ToString, DbType.String)
                    sp.Command.AddParameter("@LS_ED_DT", ds.Tables(0).Rows(i).Item(9).ToString, DbType.String)
                    sp.Command.AddParameter("@LOCKIN_PERIOD", ds.Tables(0).Rows(i).Item(10).ToString, DbType.String)
                    sp.Command.AddParameter("@RENT_FREE", ds.Tables(0).Rows(i).Item(11).ToString, DbType.String)
                    sp.Command.AddParameter("@INITIAL_MNT_RENT", ds.Tables(0).Rows(i).Item(12).ToString, DbType.String)
                    sp.Command.AddParameter("@CURRENT_MNT_RENT", ds.Tables(0).Rows(i).Item(13).ToString, DbType.String)
                    sp.Command.AddParameter("@MAINT_COST", ds.Tables(0).Rows(i).Item(14).ToString, DbType.String)
                    sp.Command.AddParameter("@CONTRACTUAL_PAYOUT", ds.Tables(0).Rows(i).Item(15).ToString, DbType.String)
                    sp.Command.AddParameter("@TOTAL_PAYOUT", ds.Tables(0).Rows(i).Item(16).ToString, DbType.String)
                    sp.Command.AddParameter("@RENT_ESC_PER", ds.Tables(0).Rows(i).Item(17).ToString, DbType.String)
                    sp.Command.AddParameter("@RENT_ESC_INTERVAL", ds.Tables(0).Rows(i).Item(18).ToString, DbType.String)
                    sp.Command.AddParameter("@SEC_DEP", ds.Tables(0).Rows(i).Item(19).ToString, DbType.String)
                    sp.Command.AddParameter("@OTHER_DEP", ds.Tables(0).Rows(i).Item(20).ToString, DbType.String)
                    sp.Command.AddParameter("@SEC_DEP_AJT", ds.Tables(0).Rows(i).Item(21).ToString, DbType.String)
                    sp.Command.AddParameter("@TAX_LIABILITY", ds.Tables(0).Rows(i).Item(22).ToString, DbType.String)
                    sp.Command.AddParameter("@ELEC_CONN", ds.Tables(0).Rows(i).Item(23).ToString, DbType.String)
                    sp.Command.AddParameter("@ELLEC_LOAD", ds.Tables(0).Rows(i).Item(24).ToString, DbType.String)
                    sp.Command.AddParameter("@SIGNAGE_SPACE", ds.Tables(0).Rows(i).Item(25).ToString, DbType.String)
                    sp.Command.AddParameter("@RIGHTS_TO_ANTENNA", ds.Tables(0).Rows(i).Item(26).ToString, DbType.String)
                    sp.Command.AddParameter("@GEN_BKP", ds.Tables(0).Rows(i).Item(27).ToString, DbType.String)
                    sp.Command.AddParameter("@PARKING", ds.Tables(0).Rows(i).Item(28).ToString, DbType.String)
                    sp.Command.AddParameter("@POA", ds.Tables(0).Rows(i).Item(29).ToString, DbType.String)
                    sp.Command.AddParameter("@NOTICE_PERIOD", ds.Tables(0).Rows(i).Item(30).ToString, DbType.String)
                    sp.Command.AddParameter("@STAMP_REG", ds.Tables(0).Rows(i).Item(31).ToString, DbType.String)
                    sp.Command.AddParameter("@LS_REG_STATUS", ds.Tables(0).Rows(i).Item(32).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_PROP_CODE", "")
                    sp.Command.AddParameter("@LL_CODE", "")
                    sp.Command.AddParameter("@LL_NO", "")
                    sp.Command.AddParameter("@LL_NAME", "")
                    sp.Command.AddParameter("@LL_ADDR", "")
                    sp.Command.AddParameter("@LL_STATE", "")
                    sp.Command.AddParameter("@LL_CITY", "")
                    sp.Command.AddParameter("@LL_MODE_OF_PAY", "")
                    sp.Command.AddParameter("@LL_ACC", "")
                    sp.Command.AddParameter("@LL_IFSC", "")
                    sp.Command.AddParameter("@LL_SEC_DEP", "")
                    sp.Command.AddParameter("@FLAG", 1)
                    sp.ExecuteScalar()

                    remarks = ""
                Next

                For x As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                    For y As Integer = 0 To ds1.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "property code" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Property Code  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "lessor code" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Lessor Code  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "number of lessors" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Number of Lessors  is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "land lord name" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Land Lord Name is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "land lord address" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Land Lord Address is null or empty, "
                            End If
                        End If


                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "land lord state" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Land Lord State is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "land lord city" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Land Lord City is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "Mode of payment of rent" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Mode of payment of rent is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "Landlord Bank Details" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Landlord Bank Detailsis null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "Landlord Bank IFSC Code" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Landlord Bank IFSC Code is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds1.Tables(0).Columns(y).ToString)) = "land lord security deposite" Then
                            If IsDBNull(ds1.Tables(0).Rows(x).Item(y)) = True Then

                                remarks = remarks + "Land Lord Security Deposite is null or empty, "
                            End If
                        End If
                      

                    Next

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPLOAD_PROPERTY_LEASE_DATA")
                    sp.Command.AddParameter("@LOC", "")
                    sp.Command.AddParameter("@LOC_CODE", "")
                    sp.Command.AddParameter("@ZONE", "")
                    sp.Command.AddParameter("@PROP_CODE", "")
                    sp.Command.AddParameter("@PROP_ADDR", "")
                    sp.Command.AddParameter("@CARPET", "")
                    sp.Command.AddParameter("@SPR_BUILTUP_AREA", "")
                    sp.Command.AddParameter("@LEASE_TENURE", "")
                    sp.Command.AddParameter("@LS_ST_DT", "")
                    sp.Command.AddParameter("@LS_ED_DT", "")
                    sp.Command.AddParameter("@LOCKIN_PERIOD", "")
                    sp.Command.AddParameter("@RENT_FREE", "")
                    sp.Command.AddParameter("@INITIAL_MNT_RENT", "")
                    sp.Command.AddParameter("@CURRENT_MNT_RENT", "")
                    sp.Command.AddParameter("@MAINT_COST", "")
                    sp.Command.AddParameter("@CONTRACTUAL_PAYOUT", "")
                    sp.Command.AddParameter("@TOTAL_PAYOUT", "")
                    sp.Command.AddParameter("@RENT_ESC_PER", "")
                    sp.Command.AddParameter("@RENT_ESC_INTERVAL", "")
                    sp.Command.AddParameter("@SEC_DEP", "")
                    sp.Command.AddParameter("@OTHER_DEP", "")
                    sp.Command.AddParameter("@SEC_DEP_AJT", "")
                    sp.Command.AddParameter("@TAX_LIABILITY", "")
                    sp.Command.AddParameter("@ELEC_CONN", "")
                    sp.Command.AddParameter("@ELLEC_LOAD", "")
                    sp.Command.AddParameter("@SIGNAGE_SPACE", "")
                    sp.Command.AddParameter("@RIGHTS_TO_ANTENNA", "")
                    sp.Command.AddParameter("@GEN_BKP", "")
                    sp.Command.AddParameter("@PARKING", "")
                    sp.Command.AddParameter("@POA", "")
                    sp.Command.AddParameter("@NOTICE_PERIOD", "")
                    sp.Command.AddParameter("@STAMP_REG", "")
                    sp.Command.AddParameter("@LS_REG_STATUS", "")
                    sp.Command.AddParameter("@LL_PROP_CODE", ds1.Tables(0).Rows(x).Item(0).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_CODE", ds1.Tables(0).Rows(x).Item(1).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_NO", ds1.Tables(0).Rows(x).Item(2).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_NAME", ds1.Tables(0).Rows(x).Item(3).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_ADDR", ds1.Tables(0).Rows(x).Item(4).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_STATE", ds1.Tables(0).Rows(x).Item(5).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_CITY", ds1.Tables(0).Rows(x).Item(6).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_MODE_OF_PAY", ds1.Tables(0).Rows(x).Item(7).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_ACC", ds1.Tables(0).Rows(x).Item(8).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_IFSC", ds1.Tables(0).Rows(x).Item(9).ToString, DbType.String)
                    sp.Command.AddParameter("@LL_SEC_DEP", ds1.Tables(0).Rows(x).Item(10).ToString, DbType.String)
                    sp.Command.AddParameter("@FLAG", 2)
                    sp.ExecuteScalar()

                    remarks = ""


                Next



                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_INSERT_PROPERTY_LEASE_DATA")
                sp1.Command.AddParameter("@AUR_ID", HttpContext.Current.Session("UID"), DbType.String)
                sp1.ExecuteScalar()
                Dim dss As New DataSet
                dss = sp1.GetDataSet()
                If dss.Tables(0).Rows.Count > 0 Then
                    GridView1.DataSource = dss.Tables(0)
                    GridView1.DataBind()
                End If
                If dss.Tables(1).Rows.Count > 0 Then
                    GridView2.DataSource = dss.Tables(1)
                    GridView2.DataBind()
                End If

                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."

            Else
                lblMsg.Text = ""

            End If
        Catch ex As Exception
            CommonModules.PopUpMessage(ex.Message, Page)
        End Try



    End Sub


End Class
