<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSendRentRemainder.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSendRentRemainder" Title="Send Rent Remainder" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Sent Rent Reminder
                            </legend>
                        </fieldset>
                        
                            <form id="form1" class="well" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />
                                <div class="clearfix">
                                    
                                        <asp:GridView ID="gvTenantRent" runat="server" EmptyDataText="No Rent Reminder Found."
                                            AutoGenerateColumns="false" AllowPaging="true"
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Tenant Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcode" runat="server" Text='<%# Eval("TenantCode")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("TEN_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rent Payable Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblnpdate" runat="server" Text='<%# Eval("PM_RP_TEN_PAYABLE_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment-Terms">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpterm" runat="server" Text='<%# Eval("PM_PT_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLoc" runat="server" Text='<%# Eval("LCM_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reminder Sent Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRSdate" runat="server" Text='<%# Eval("SentDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" HorizontalAlign="Center" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    
                                </div>

                            </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
