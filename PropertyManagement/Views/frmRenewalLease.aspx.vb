Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmRenewalLease
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            BindGrid()
            updatepanel.Visible = False

            'BindProperty()
            BindState()
            BindPayMode()
            'BindLeaseExpences()
            BindTenure()
            panel1.Visible = False
            divCTS.Visible = False
            divEnt.Visible = False
            panel2.Visible = False
            divrentfree.Visible = False
            txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")
        End If
    End Sub

    Private Sub BindGrid()
        Try
            'lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASES_FOR_RENEWAL")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            'Dim ds As DataSet
            'ds = sp.GetDataSet()
            'Session("PPTY") = ds.Tables(0).Rows(0).Item("PM_PPT_SNO").ToString()
            gvLDetails_Lease.DataSource = sp.GetDataSet()
            gvLDetails_Lease.DataBind()

        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub

    'Public Sub BindPropertyImages()
    '    Dim dtDocs As New DataTable("Documents")
    '    param = New SqlParameter(0) {}
    '    param(0) = New SqlParameter("@PM_LES_SNO", SqlDbType.NVarChar, 50)
    '    param(0).Value = hdnLSNO.Value
    '    Dim ds As New DataSet
    '    ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_PROEPRTY_IMAGES", param)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        gvboard.DataSource = ds
    '        gvboard.DataBind()
    '        tblGridDocs.Visible = True
    '        lblDocsMsg.Text = ""
    '    Else
    '        tblGridDocs.Visible = False
    '        lblMsg.Visible = True
    '        lblDocsMsg.Text = "No Property Images Available"
    '    End If
    '    dtDocs = Nothing

    'End Sub

    'Private Sub BindLegalDocuments(ByVal PM_LES_SNO As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEGAL_DOCS_OF_LEASE_BY_SNO")
    '    sp.Command.AddParameter("@PM_LES_SNO", PM_LES_SNO, DbType.Int32)
    '    gvLegalDocs.DataSource = sp.GetDataSet()
    '    gvLegalDocs.DataBind()

    'End Sub

    Private Sub BindOfficeType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_OFFICE_TYPE")
        ddlOffice.DataSource = sp3.GetDataSet()
        ddlOffice.DataTextField = "PM_TOO"
        ddlOffice.DataValueField = "PM_OID"
        ddlOffice.DataBind()
        ddlOffice.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Protected Sub gvLDetails_Lease_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLDetails_Lease.RowCommand
        Try
            lblMsg.Text = String.Empty
            If e.CommandName = "GetLeases" Then
                panel1.Visible = True

                hdnLSNO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim ReqID As String = DirectCast(row.FindControl("lnkReqId"), LinkButton).Text.ToString()
                Session("REQ_ID") = ReqID
                lblLeaseReqId.Text = ReqID
                GetSelectedLeaseDetails(hdnLSNO.Value)
                ' BindPropertyImages()
                ' BindLegalDocuments(hdnLSNO.Value)
                BindRentRevision()
            Else
                panel1.Visible = False
            End If

        Catch ex As Exception
            Response.Write(ex)
        End Try
        txtTermofLease.Focus()
    End Sub

    Private Sub GetSelectedLeaseDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GRID_VIEW_MODIFY")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Session("Property") = ds.Tables(0).Rows(0).Item("PM_PPT_SNO").ToString()
        SetLeaseValuesToUpdate(ds)
        updatepanel.Visible = True
    End Sub


    Private Sub SetLeaseValuesToUpdate(ByVal leaseAllSections As DataSet)
        Dim LeaseNAreaNDetails As DataTable
        Dim charges As DataTable
        Dim agreementDetails As DataTable
        Dim brokerageDt As DataTable
        Dim UtilityDt As DataTable
        Dim OtherServicesDt As DataTable
        Dim OtherDetailsDt As DataTable
        Dim LeaseDetailsDt As DataTable
        Dim LeaseExpDt As DataTable
        Dim POA As DataTable
        Dim PropertyDetails As DataTable
        Dim OfficeConnectivity As DataTable
        Dim Civil As DataTable
        Dim Damages As DataTable


        LeaseNAreaNDetails = leaseAllSections.Tables(0)
        charges = leaseAllSections.Tables(2)
        agreementDetails = leaseAllSections.Tables(3)
        brokerageDt = leaseAllSections.Tables(4)
        UtilityDt = leaseAllSections.Tables(5)
        OtherServicesDt = leaseAllSections.Tables(6)
        OtherDetailsDt = leaseAllSections.Tables(7)
        LeaseDetailsDt = leaseAllSections.Tables(8)
        LeaseExpDt = leaseAllSections.Tables(9)
        POA = leaseAllSections.Tables(10)
        RentRevision = leaseAllSections.Tables(11)
        PropertyDetails = leaseAllSections.Tables(12)
        OfficeConnectivity = leaseAllSections.Tables(13)
        Civil = leaseAllSections.Tables(14)
        Damages = leaseAllSections.Tables(15)
        BindLeaseExpences()

        'Property Details section start
        'txtAge.Text = PropertyDetails.Rows(0).Item("PM_PPT_AGE")
        txtPropIDName.Text = PropertyDetails.Rows(0).Item("PM_PPT_NAME")
        txtCarpetArea.Text = PropertyDetails.Rows(0).Item("PM_AR_CARPET_AREA")
        txtBuiltupArea.Text = PropertyDetails.Rows(0).Item("PM_AR_BUA_AREA")
        ' txtEfficiency.Text = PropertyDetails.Rows(0).Item("PM_AR_PREF_EFF")
        txtNoofFloors.Text = PropertyDetails.Rows(0).Item("PM_PPT_TOT_FLRS")
        'txtCeilingHight.Text = PropertyDetails.Rows(0).Item("PM_AR_FTC_HIGHT")
        ' txtBeamBottomHight.Text = PropertyDetails.Rows(0).Item("PM_AR_FTBB_HIGHT")
        'TxtFlooringType.Text = PropertyDetails.Rows(0).Item("PM_FT_TYPE")
        txtSignageLocation.Text = PropertyDetails.Rows(0).Item("PM_PPT_SIGN_LOC")
        'txtToilet.Text = PropertyDetails.Rows(0).Item("PM_PPT_TOT_TOI")

        txtInvestedArea.Text = PropertyDetails.Rows(0).Item("PM_BASIC_RENT")
        txtmain1.Text = PropertyDetails.Rows(0).Item("PM_MAINT_CHRG")

        txttotalrent.Text = PropertyDetails.Rows(0).Item("TOTALRENT")

        Dim costtype As String
        costtype = PropertyDetails.Rows(0).Item("OD_COST_TYPE")
        rblCostType.SelectedValue = costtype
        If costtype = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtRentPerSqftCarpet.Text = PropertyDetails.Rows(0).Item("OD_RENT_PER_SQFT_BUA")
            txtRentPerSqftBUA.Text = PropertyDetails.Rows(0).Item("OD_RENT_PER_SQFT_CARPET")

        ElseIf costtype = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtSeatCost.Text = PropertyDetails.Rows(0).Item("OD_SEAT_COST")

        End If
        'txtOfficeType.Text = PropertyDetails.Rows(0).Item("PM_OFFICE_TYPE")
        BindOfficeType()
        If PropertyDetails.Rows(0).Item("OT") = "" Then
            ddlOffice.Items.FindByValue("").Selected = True
        Else
            ddlOffice.Items.FindByValue(PropertyDetails.Rows(0).Item("OT")).Selected = True
        End If

        txtshopnumberoccupy.Text = PropertyDetails.Rows(0).Item("PM_SHOP_NUMBER_TO_OCCUPY")
        txtTermofLease.Text = PropertyDetails.Rows(0).Item("PM_TERMS_OF_LEASE")

        'ddlRollingShutter.ClearSelection()
        ' Dim rolShutter As String = PropertyDetails.Rows(0)("PM_ROLLING_SHUTTER")
        'ddlRollingShutter.Items.FindByValue(rolShutter).Selected = True
        'proerty details section end


        'Lease Details Section Start
        If LeaseNAreaNDetails.Rows(0)("PM_LES_STA_ID") = 4024 Then
            lblMsg.Text = "Plese add at least one landlord to add lease successfully"
        Else
            lblMsg.Text = ""
        End If
        txtNoLanlords.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_NO_OF_LL")
        'Lease Details Section End

        'Area & Cost Details Section start
        ddlSecurityDepMonths.ClearSelection()
        Dim secrtyDepositMonths As String = LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEP_MONTHS")
        ddlSecurityDepMonths.Items.FindByValue(secrtyDepositMonths).Selected = True
        ddlTenure.ClearSelection()
        Dim tenure As String = LeaseNAreaNDetails.Rows(0)("PM_LES_TENURE")
        ddlTenure.Items.FindByValue(tenure).Selected = True
        'Dim costType As String = LeaseNAreaNDetails.Rows(0)("PM_LES_COST_TYPE")
        rblCostType.Items.FindByValue(costtype).Selected = True
        If String.Equals(costtype, "Seat") Then
            Costype1.Visible = False
            Costype2.Visible = True

        Else
            Costype1.Visible = True
            Costype2.Visible = False

        End If

        'txtSeatCost.Text = IIf(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST") = 0, 0, Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST")))
        'txtRentPerSqftCarpet.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_CARPET"))
        'txtRentPerSqftBUA.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_BUA"))

        txtLnumber.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_CTS_NO")
        txtentitle.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_ENTITLED_AMT"))
        txtInvestedArea.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_BASIC_RENT"))
        txtpay.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEPOSIT"))
        txtRentFreePeriod.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_FREE_PERIOD")
        ' txtInteriorCost.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_INTERIOR_COST"))
        'Area & Cost Details Section end

        'Charges Section start
        txtregcharges.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_REGS_CHARGES"))
        txtsduty.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_STMP_DUTY_CHARGES"))
        txtfurniture.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_FF_CHARGES"))
        'txtbrokerage.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_CONSLBROKER_CHARGES"))
        'txtpfees.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROF_CHARGES"))
        'txtmain1.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_MAINT_CHARGES"))
        'txtservicetax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_SERVICE_TAX"))
        txtproptax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROPERTY_TAX"))

        txtOtherTax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_OTHER_TAX"))

        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
        'Charges Section end

        'Agreement Details Section start
        'txtsdate.Text = agreementDetails.Rows(0)("PM_LAD_EFFE_DT_AGREEMENT")
        'txtedate.Text = agreementDetails.Rows(0)("PM_LAD_EXP_DT_AGREEMENT")
        txtsdate.Enabled = False
        txtsdate.Text = agreementDetails.Rows(0)("PM_LAD_EXP_DT_AGREEMENT")
        txtedate.Text = Convert.ToDateTime(agreementDetails.Rows(0)("PM_LAD_EXP_DT_AGREEMENT")).AddYears(1).ToString("MM/dd/yyyy")

        txtlock.Text = agreementDetails.Rows(0)("PM_LAD_LOCK_INPERIOD")
        txtLeasePeiodinYears.Text = agreementDetails.Rows(0)("PM_LAD_LEASE_PERIOD")
        txtNotiePeriod.Text = agreementDetails.Rows(0)("PM_LAD_NOTICE_PERIOD")
        ddlAgreementbyPOA.ClearSelection()
        Dim AgreementbyPOA As String = agreementDetails.Rows(0)("PM_LES_SIGNED_POA")
        ddlAgreementbyPOA.Items.FindByValue(AgreementbyPOA).Selected = True
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
            txtPOAName.Text = POA.Rows(0)("PM_POA_NAME")
            txtPOAAddress.Text = POA.Rows(0)("PM_POA_ADDRESS")
            txtPOAMobile.Text = POA.Rows(0)("PM_POA_PHONE_NO")
            txtPOAEmail.Text = POA.Rows(0)("PM_POA_EMAIL")
        Else
            panPOA.Visible = False
        End If
        txtReminderEmail.Text = agreementDetails.Rows(0)("EMAIL_ADDRESS")
        'Agreement Details Section end
        BindRentRevision()

        'Brokerage Section start
        'txtbrkamount.Text = Convert.ToDouble(brokerageDt.Rows(0)("PM_LBD_PAID_AMOUNT"))
        'txtbrkname.Text = brokerageDt.Rows(0)("PM_LBD_NAME")
        'txtbrkpan.Text = brokerageDt.Rows(0)("PM_LBD_PAN_NO")
        'txtbrkmob.Text = brokerageDt.Rows(0)("PM_LBD_PHONE_NO")
        'txtbrkremail.Text = brokerageDt.Rows(0)("PM_LBD_EMAIL")
        'txtbrkaddr.Text = brokerageDt.Rows(0)("PM_LBD_ADDRESS")
        'Brokerage Section end

        'Utility/Power back up Details Section start
        ddlDgSet.ClearSelection()
        Dim dgSetVal As String = UtilityDt.Rows(0)("PM_LUP_DGSET")
        ddlDgSet.Items.FindByValue(dgSetVal).Selected = True

        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If

        txtDgSetPerUnit.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_COMMER_PER_UNIT")
        txtDgSetLocation.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_LOCATION")
        txtSpaceServoStab.Text = UtilityDt.Rows(0)("PM_LUP_SPACE_SERVO_STABILIZER")

        ddlElectricalMeter.ClearSelection()
        Dim eleMeter As String = UtilityDt.Rows(0)("PM_LUP_ELE_METER")
        ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        'ele meter yes start

        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If

        txtMeterLocation.Text = UtilityDt.Rows(0)("PM_LUP_MET_LOCATION")
        txtEarthingPit.Text = UtilityDt.Rows(0)("PM_LUP_EARTHING_PIT")
        txtAvailablePower.Text = UtilityDt.Rows(0)("PM_LUP_AVAIL_POWER")
        txtAdditionalPowerKWA.Text = UtilityDt.Rows(0)("PM_LUP_ADDITIONAL_POWER")
        txtPowerSpecification.Text = UtilityDt.Rows(0)("PM_LUP_POWER_SPECIFICATION")


        'txtpathElecCable.Text = UtilityDt.Rows(0)("PM_LUP_PATH_ELEC_CABLE")
        'txtgroundEarthling.Text = UtilityDt.Rows(0)("PM_LUP_GROUND_EARTH")
        'txtearthType.Text = UtilityDt.Rows(0)("PM_LUP_EARTH_TYPE")
        txtLiftPowerCut.Text = UtilityDt.Rows(0)("PM_LUP_LIFT_POWERCUT")
        ' txtTransLocation.Text = UtilityDt.Rows(0)("PM_LUP_TRANSFORMER_LOCATION")
        txtWaterConnection.Text = UtilityDt.Rows(0)("PM_LUP_WATER_CONNECTION")



        ' ele meter yes end

        'Utility/Power back up Details Section end

        'Other Services Section start
        txtNoOfTwoWheelerParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_TWO_PARK")
        txtNoOfCarsParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_CAR_PARK")
        'txtDistanceFromAirPort.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_AIRPT")
        'txtDistanceFromRailwayStation.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_RAIL")
        'txtDistanceFromBustop.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_BUS")


        ddlLift.SelectedValue = OtherServicesDt.Rows(0)("PM_LO_LIFT_YES_NO")

        If ddlLift.SelectedValue = "Yes" Then
            divLift.Visible = True

            txtMake.Text = OtherServicesDt.Rows(0)("PM_LO_MAKE")
            txtCapacity.Text = OtherServicesDt.Rows(0)("PM_LO_CAPACITY")
            txtStairCaseWidth.Text = OtherServicesDt.Rows(0)("PM_LO_MAIN_STAIRCASE_WIDTH")
            txtFireStaircase.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_STAIRCASE")


        Else
            divLift.Visible = False

            txtACOutdoor.Text = OtherServicesDt.Rows(0)("PM_LO_AC_OUTDOOR_SPACE")
            'anyACSatloc.Text = OtherServicesDt.Rows(0)("PM_LO_ACS_AT_LOCATION")
            txtsignOrbrand.Text = OtherServicesDt.Rows(0)("PM_LO_SIGNAGE_BRANDING")
            'txtFireExitstairs.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_EXIT_STAIRS")
            'txtFireFightingSystem.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_FIGHTING_SYSTEM")
            txtLiftNLicense.Text = OtherServicesDt.Rows(0)("PM_LO_LIFTS_AVAILABLE_LICENSE")
            txtMaintAgencyLift.Text = OtherServicesDt.Rows(0)("PM_LO_MAINT_AGENCY_LIFT")

        End If


        'Other Services Section end

        'Other Details Section start
        'txtCompetitorsVicinity.Text = OtherDetailsDt.Rows(0)("PM_LO_COMP_VICINITY")

        'txtOfficeEquipments.Text = OtherDetailsDt.Rows(0)("PM_LO_OFF_EQUIPMENTS")
        'Other Details Section end

        'Lease Escalation Details Section start
        ddlesc.ClearSelection()
        Dim leaseEsc As String = LeaseDetailsDt.Rows(0)("PM_LD_LES_ESCALATON")
        ddlesc.Items.FindByValue(leaseEsc).Selected = True
        ddlLeaseEscType.ClearSelection()
        Dim leaseEscType As String = LeaseDetailsDt.Rows(0)("PM_LD_ESCALTION_TYPE")
        ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
        'txtLeaseHoldImprovements.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_IMPROV")
        txtComments.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_COMMENTS")
        'ddlDueDilegence.ClearSelection()
        Dim dueDilegence As String = LeaseDetailsDt.Rows(0)("PM_LD_DUE_DILIGENCE_CERT")
        'ddlDueDilegence.Items.FindByValue(dueDilegence).Selected = True

        Dim reminderBefore As String
        reminderBefore = LeaseDetailsDt.Rows(0).Item("PM_LRE_REMINDER_BEFORE")
        Dim parts As String() = reminderBefore.Split(New Char() {","c})
        For i As Integer = 0 To parts.Length - 1
            For j As Integer = 0 To ReminderCheckList.Items.Count - 1
                If ReminderCheckList.Items(j).Value = parts(i) Then
                    ReminderCheckList.Items(j).Selected = True
                End If
            Next
        Next
        'Lease Escalation Details Section end


        'office connectivity start
        'txtTeleSPB.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_SP_BUILDING")
        'txtTeleSPFO.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_SP_FIRST_OCCUPANT")
        'txtLeaseSPB.Text = OfficeConnectivity.Rows(0)("PM_OC_LEASE_LINE_SP_BUILD")
        'txtLeaseSPFO.Text = OfficeConnectivity.Rows(0)("PM_OC_LEASE_LINE_SP_FIRST_OCCPNT")
        'txtTeleVenFR.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_VEN_FEASIBILITY")
        'txtITVenFR.Text = OfficeConnectivity.Rows(0)("PM_OC_IT_VEN_FEASIBILITY")
        'office connectivity end

        'CIVIL SECTION START
        'txtPunnWall.Text = Civil.Rows(0)("PM_C_PUNNG_WALLS")
        'txtCracks.Text = Civil.Rows(0)("PM_C_CRACKS")
        'txtWashToilet.Text = Civil.Rows(0)("PM_C_TOILET")
        'txtPantry.Text = Civil.Rows(0)("PM_C_PANTRY")
        'txtDrainagelinedrawings.Text = Civil.Rows(0)("PM_C_DRAINAGE_DRAWNGS")
        'txtWindowHeight.Text = Civil.Rows(0)("PM_C_WINDOW_HEIGHT")

        'txtWaterLeak.Text = Civil.Rows(0)("PM_C_WATER_LEAKAGE")
        'txtStaircase.Text = Civil.Rows(0)("PM_C_STAIRCASE")
        'txtEmergExit.Text = Civil.Rows(0)("PM_C_EMRG_EXIT")
        'txtHeightAvailability.Text = Civil.Rows(0)("PM_C_HEIGH_AVAILABILITY")
        'txtAddtnlCivilWork.Text = Civil.Rows(0)("PM_C_ADD_CIVIL_WORK")
        'CIVIL SECTION END

        'out standing amounts/damages start
        ddlOutOrDamagesAmount.SelectedValue = Damages.Rows(0)("PM_D_ANY_DAMAGES")
        ddlDamangesExtStay.SelectedValue = Damages.Rows(0)("PM_D_EXTND_STAY")
        ddlPossessionHandover.SelectedValue = Damages.Rows(0)("PM_D_HAND_OVER")
        'txtOfficeEquipments.Text = Damages.Rows(0)("PM_D_OFFC_EQUIPMNTS")
        'txtWorkHrs.Text = Damages.Rows(0)("PM_D_WRKNG_HRS_CONSTRUCTION")
        'txtCommonToilet.Text = Damages.Rows(0)("PM_D_USAGE_TOILET")
        'txtServiceLift.Text = Damages.Rows(0)("PM_D_USAGE_LIFT")
        'txtSecSystem.Text = Damages.Rows(0)("PM_D_SECURITY_SYSTEM")
        'out standing amounts/damages end

        ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
        ViewState("LES_TOT_RENT") = Convert.ToDecimal(txttotalrent.Text)
        ViewState("LES_SEC_DEPOSIT") = Convert.ToDecimal(txtpay.Text)

        gvlandlordItems.DataSource = leaseAllSections.Tables(1)
        gvlandlordItems.DataBind()
        If leaseAllSections.Tables(1).Rows.Count >= ViewState("NO_OF_LL") Then
            btnAddNewLandlord.Enabled = False
        Else
            btnAddNewLandlord.Enabled = True
            txtName.Focus()
        End If

    End Sub

    Protected Sub gvlldata_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvlandlordItems.RowCommand
        Try
            If e.CommandName = "GetLandlordDetails" Then
                Landlord.Visible = True
                hdnLandlordSNO.Value = e.CommandArgument
                GetSelectedLandlordDetails(hdnLandlordSNO.Value)
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetSelectedLandlordDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORD_DETAILS_BY_ID")

        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_LL_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        updatepanel.Visible = True
        Landlord.Visible = True
        SetLandlordValsToUpdate(ds.Tables(0))
    End Sub

    Private Sub BindPayMode()
        ObjSubSonic.Binddropdown(ddlpaymentmode, "PM_GET_PAYMENT_MODE", "NAME", "CODE")
    End Sub
    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            Else
                panel1.Visible = False
                panel2.Visible = False
            End If
        End If
    End Sub
    Private Sub BindState()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_STATE")
        sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
        sp.Command.AddParameter("@MODE", 1, DbType.Int32)
        ddlState.DataSource = sp.GetDataSet()
        ddlState.DataTextField = "STE_NAME"
        ddlState.DataValueField = "STE_CODE"
        ddlState.DataBind()
        ddlState.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY_BY_STATE_CODE")
        sp.Command.AddParameter("@STELST", ddlState.SelectedValue, DbType.String)
        sp.Command.AddParameter("@MODE", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    'Private Sub BindProperty()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_BIND_PROPERTIES_FOR_LEASE")
    '    '  sp.Command.AddParameter("@dummy", 0, DbType.String)
    '    sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)

    '    sp.Command.AddParameter("@FLAG", 1, DbType.String)
    '    ddlproperty.DataSource = sp.GetDataSet()
    '    ddlproperty.DataTextField = "PN_NAME"
    '    ddlproperty.DataValueField = "PM_PPT_SNO"
    '    ddlproperty.DataBind()
    '    ddlproperty.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    'End Sub
    'Protected Sub ddlproperty_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproperty.SelectedIndexChanged
    '    If ddlproperty.SelectedIndex > 0 Then
    '        param = New SqlParameter(0) {}
    '        param(0) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar, 200)
    '        param(0).Value = ddlproperty.SelectedValue
    '        ds = New DataSet
    '        ds = ObjSubSonic.GetSubSonicDataSet("BIND_DETAILS_BY_PROP", param)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            txtPropAddedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
    '            txtApprovedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
    '            'txtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_PPT_CODE").ToString()
    '        End If
    '    Else
    '        txtPropAddedBy.Text = ""
    '        txtApprovedBy.Text = ""
    '    End If
    'End Sub


    Private Sub SetLandlordValsToUpdate(ByVal landlorddt As DataTable)
        'Dim selectedProp As String = landlorddt.Rows(0)("AIR_ITEM_SUBCAT")
        'ddlproperty.Items.FindByValue(selectedProp).Selected = True
        btnLandlord.Text = "Update"
        btnBack.Text = "Cancel"
        btnDelLandlord.Visible = True
        txtName.Text = landlorddt.Rows(0)("PM_LL_NAME")

        txtAddress.Text = landlorddt.Rows(0)("PM_LL_ADDRESS1")
        txtAddress2.Text = landlorddt.Rows(0)("PM_LL_ADDRESS2")
        txtAddress3.Text = landlorddt.Rows(0)("PM_LL_ADDRESS3")
        'ddlState.Text = landlorddt.Rows(0)("PM_LL_STATE")

        ddlState.ClearSelection()
        Dim selectedState As String = landlorddt.Rows(0)("PM_LL_STATE")
        ddlState.Items.FindByValue(selectedState).Selected = True

        ddlCity.ClearSelection()
        Dim selectedCity As String = landlorddt.Rows(0)("PM_LL_CITY_CODE")
        ddlCity.Items.FindByValue(selectedCity).Selected = True

        txtld1Pin.Text = landlorddt.Rows(0)("PM_LL_PINCODE")
        txtPAN.Text = landlorddt.Rows(0)("PM_LL_PAN")


        txtldemail.Text = landlorddt.Rows(0)("PM_LL_EMAIL")


        'ddlServiceTaxApplicable.ClearSelection()
        'Dim serviceTax As String = landlorddt.Rows(0)("PM_LL_SERVTAX_APPLICABLE")
        'ddlServiceTaxApplicable.Items.FindByValue(serviceTax).Selected = True

        'txtServiceTaxlnd.Text = landlorddt.Rows(0)("PM_LL_SERVICE_TAX")


        'ddlPropertyTaxApplicable.ClearSelection()
        'Dim propertyTax As String = landlorddt.Rows(0)("PM_LL_PPTAX_APPLICABLE")
        'ddlPropertyTaxApplicable.Items.FindByValue(propertyTax).Selected = True


        'txtPropertyTax.Text = landlorddt.Rows(0)("PM_LL_PROPERTY_TAX")

        txtContactDetails.Text = landlorddt.Rows(0)("PM_LL_PHONE_NO")


        ddlAmountIn.ClearSelection()
        Dim amountIn As String = landlorddt.Rows(0)("PM_LL_AMOUNT_IN")
        ddlAmountIn.Items.FindByValue(amountIn).Selected = True



        txtpmonthrent.Text = landlorddt.Rows(0)("PM_LL_MON_RENT_PAYABLE")
        txtpsecdep.Text = landlorddt.Rows(0)("PM_LL_SECURITY_DEPOSIT")


        ddlpaymentmode.ClearSelection()
        Dim paymentMode As String = landlorddt.Rows(0)("PM_LL_PAYMENT_MODE")
        ddlpaymentmode.Items.FindByValue(paymentMode).Selected = True


        txtBankName.Text = landlorddt.Rows(0)("PM_LL_BANK")
        txtAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")

        'NEFT Transfer
        txtNeftBank.Text = landlorddt.Rows(0)("PM_LL_BANK")
        txtNeftAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")
        txtNeftBrnch.Text = landlorddt.Rows(0)("PM_LL_BRANCH")
        txtNeftIFSC.Text = landlorddt.Rows(0)("PM_LL_IFSC")
        txtNeftAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")

        If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
            panel1.Visible = True
            panel2.Visible = False
        ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
            panel1.Visible = False
            panel2.Visible = True
        Else
            panel1.Visible = False
            panel2.Visible = False
        End If

        'txtName.Text = landlorddt.Rows(0)("PM_LL_FROM_DATE")
        'txtName.Text = landlorddt.Rows(0)("PM_LL_TO_DATE")

        'txtName.Text = landlorddt.Rows(0)("PM_LL_TDS")
        'txtName.Text = landlorddt.Rows(0)("PM_LL_TDS_REMARKS")

    End Sub

    Protected Sub btnLandlord_Click(sender As Object, e As EventArgs) Handles btnLandlord.Click
        lblMsg.Text = ""
        lblMsgLL.Text = ""
        Try
            Dim spToCall As String
            Dim id As Integer
            Dim successMsg As String

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
            sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp1.Command.AddParameter("@ID", hdnLSNO.Value, DbType.Int32)
            Dim ds As New DataSet
            ds = sp1.GetDataSet
            If ViewState("NO_OF_LL") = ds.Tables(0).Rows(0).Item("LL_COUNT") + 1 Then
                btnAddNewLandlord.Enabled = False
            End If

            If String.Equals(btnLandlord.Text, "Submit") Then
                spToCall = "PM_ADD_LANDLORD_DETAILS"
                id = Convert.ToInt32(hdnLSNO.Value)
                successMsg = "Landlord Details Added Successfully."
            Else
                spToCall = "PM_UPDATE_LANDLORD_DETAILS"
                id = Convert.ToInt32(hdnLandlordSNO.Value)
                successMsg = "Landlord Details Updated Successfully."
            End If

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & spToCall)
            sp.Command.AddParameter("@ID", id, DbType.Int32)
            sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp.Command.AddParameter("@LAN_NAME", txtName.Text, DbType.String)
            sp.Command.AddParameter("@LAN_ADDRESS1", txtAddress.Text, DbType.String)
            sp.Command.AddParameter("@LAN_ADDRESS2", txtAddress2.Text, DbType.String)
            sp.Command.AddParameter("@LAN_ADDRESS3", txtAddress3.Text, DbType.String)
            sp.Command.AddParameter("@LAN_STATE", ddlState.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LAN_CITY", ddlCity.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LAN_PINCODE", txtld1Pin.Text, DbType.String)
            sp.Command.AddParameter("@LAN_PAN", txtPAN.Text, DbType.String)
            sp.Command.AddParameter("@TOA", ddlOffice.SelectedValue, DbType.String)
            'sp.Command.AddParameter("@LAN_SERV_TAX", IIf(txtServiceTaxlnd.Text = "", 0, txtServiceTaxlnd.Text), DbType.Decimal)
            'sp.Command.AddParameter("@LAN_PROP_TAX_APP", ddlPropertyTaxApplicable.SelectedValue, DbType.String)
            'sp.Command.AddParameter("@LAN_PROP_TAX", IIf(txtPropertyTax.Text = "", 0, txtPropertyTax.Text), DbType.Decimal)
            sp.Command.AddParameter("@LAN_CONTACT_DET", txtContactDetails.Text, DbType.String)
            sp.Command.AddParameter("@LAN_EMAIL", txtldemail.Text, DbType.String)
            sp.Command.AddParameter("@LAN_AMOUNT_IN", ddlAmountIn.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LAN_RENT", IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text), DbType.Decimal)
            sp.Command.AddParameter("@LAN_SECURITY", IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text), DbType.Decimal)
            sp.Command.AddParameter("@LAN_PAY_MODE", ddlpaymentmode.SelectedValue, DbType.Int32)
            If ddlpaymentmode.SelectedIndex > 0 Then
                If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                    sp.Command.AddParameter("@LAN_BANK", txtBankName.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_ACCNO", txtAccNo.Text, DbType.String)
                ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                    sp.Command.AddParameter("@LAN_BANK", txtNeftBank.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_ACCNO", txtNeftAccNo.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_NEFT_BRANCH", txtNeftBrnch.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_NEFT_IFSC", txtNeftIFSC.Text, DbType.String)
                End If
            End If
            sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
            Dim res As String = sp.ExecuteScalar()
            If res = "SUCCESS" Then
                ddlAmountIn.Enabled = False
                '  btnFinalize.Enabled = True
                btnBack.Enabled = True
                lblMsg.Text = successMsg
                ClearLandlord()
                txtName.Focus()
                'GetSelectedLeaseDetails(hdnLSNO.Value)
                GetLandlords()
                Landlord.Visible = False
            Else
                lblMsg.Text = "Something went wrong. Please try again later."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub



    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Landlord.Visible = False
    End Sub
    Public Sub ClearLandlord()
        txtName.Text = ""
        txtAddress.Text = ""
        txtAddress2.Text = ""
        txtAddress3.Text = ""
        ddlState.ClearSelection()
        ddlCity.ClearSelection()
        txtld1Pin.Text = ""
        txtPAN.Text = ""
        'ddlServiceTaxApplicable.ClearSelection()
        'txtServiceTaxlnd.Text = "0"
        'ddlPropertyTaxApplicable.ClearSelection()
        'txtPropertyTax.Text = "0"
        txtContactDetails.Text = ""
        txtldemail.Text = ""
        'ddlAmountIn.ClearSelection()
        txtpmonthrent.Text = "0"
        txtpsecdep.Text = "0"
        ddlpaymentmode.ClearSelection()
        txtBankName.Text = ""
        txtAccNo.Text = ""
        txtNeftBank.Text = ""
        txtNeftAccNo.Text = ""
        txtNeftBrnch.Text = ""
        txtNeftIFSC.Text = ""

    End Sub


    Protected Sub gvLDetails_Lease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetails_Lease.PageIndexChanging
        gvLDetails_Lease.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Private Sub BindLeaseExpences()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXPENSES_OF_LEASE")
        sp.Command.AddParameter("@PM_LES_SNO", hdnLSNO.Value, DbType.String)
        gvLeaseExpences.DataSource = sp.GetDataSet()
        gvLeaseExpences.DataBind()
    End Sub

    Private Sub BindTenure()
        ObjSubSonic.Binddropdown(ddlTenure, "PM_GET_PAYMENT_TERMS", "PM_PT_NAME", "PM_PT_SNO")
        ddlTenure.Items.RemoveAt(0)
    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub
    Private Sub BindRentRevision()
        Dim rowCount As Integer = 0
        rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
        If rowCount > 1 Then
            RentRevisionPanel.Visible = True
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            For i As Integer = 1 To rowCount - 1
                rr_obj = New RentRevision()
                rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
                rr_obj.RR_Percentage = "0"
                RR_lst.Add(rr_obj)
            Next
            rpRevision.DataSource = RR_lst
            rpRevision.DataBind()
        Else
            RentRevisionPanel.Visible = False
        End If
    End Sub

    Protected Sub txtedate_TextChanged(sender As Object, e As EventArgs) Handles txtedate.TextChanged
        BindRentRevision()
    End Sub

    Protected Sub txtsdate_TextChanged(sender As Object, e As EventArgs) Handles txtsdate.TextChanged
        BindRentRevision()
    End Sub


    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub
    Protected Sub btnDelLandlord_Click(sender As Object, e As EventArgs) Handles btnDelLandlord.Click
        'ClearLandlord()
        DeleteLandlord()
        Landlord.Visible = False
        'btnLandlord.Text = "Submit"
        'btnBack.Text = "Cancel"
    End Sub

    Protected Sub DeleteLandlord()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_DELETE_LANDLORD_DETAILS")
            sp.Command.AddParameter("@FLAG", 2, DbType.Int32) ' UPDATE FLAG = 1, DELETE FLAG = 2
            sp.Command.AddParameter("@ID", Convert.ToInt32(hdnLandlordSNO.Value), DbType.Int32)
            Dim res As String = sp.ExecuteScalar()
            If res = "SUCCESS" Then
                ddlAmountIn.Enabled = False
                btnBack.Enabled = True
                lblMsg.Text = "Landlord Deleted Successfully."
                ClearLandlord()
                txtName.Focus()
                GetSelectedLeaseDetails(hdnLSNO.Value)
                Landlord.Visible = False
            Else
                lblMsg.Text = "Something went wrong. Please try again later."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Function checkPptyReqisition() As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_CHECK_PPTY_RENE_REQ")
        sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        Return sp.ExecuteScalar
    End Function

    Private Sub BindReqId()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GENERATE_REQUESTID_ADD_LEASE")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDLEASE", DbType.String)
        Dim dt = DateTime.Now.ToString("MMddyyyyHHmm")
        lblLeaseReqId.Text = dt + "/LESREQ/" + Convert.ToString(sp3.ExecuteScalar())
    End Sub

    Dim FileUploadName As String
    Dim legalDocs As List(Of DocClas) = New List(Of DocClas)
    Dim IC As DocClas
    'Private Sub SaveDocs()
    '    For Each row As GridViewRow In gvLegalDocs.Rows
    '        'if using TemplateField columns then you may need to use FindControl method

    '        Dim fu As FileUpload = DirectCast(row.FindControl("fuLegalDoc"), FileUpload)

    '        Dim lblDocSNO As Label = DirectCast(row.FindControl("lblDocSNO"), Label)


    '        If fu.PostedFiles IsNot Nothing Then
    '            For Each File In fu.PostedFiles
    '                If File.ContentLength > 0 Then
    '                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
    '                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
    '                    File.SaveAs(filePath)
    '                    FileUploadName = Upload_Time & "_" & File.FileName
    '                    IC = New DocClas()
    '                    IC.Filename = File.FileName
    '                    IC.FilePath = Upload_Time & "_" & File.FileName
    '                    IC.Doc_SNO = lblDocSNO.Text
    '                    legalDocs.Add(IC)
    '                End If
    '            Next
    '        End If
    '    Next
    'End Sub
    Protected Sub btnAddNewLandlord_Click(sender As Object, e As EventArgs) Handles btnAddNewLandlord.Click
        ClearLandlord()
        Landlord.Visible = True
        btnDelLandlord.Visible = False
        btnLandlord.Text = "Submit"
        btnBack.Text = "Cancel"
    End Sub
    Protected Sub txtNoLanlords_TextChanged(sender As Object, e As EventArgs) Handles txtNoLanlords.TextChanged
        If txtNoLanlords.Text = "" Then
            txtNoLanlords.Text = "0"
        End If
        ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
        If (gvlandlordItems.Rows.Count < ViewState("NO_OF_LL")) Then
            btnAddNewLandlord.Enabled = True
        Else
            btnAddNewLandlord.Enabled = False
        End If
        gvlandlordItems.Focus()
    End Sub
    'Record Renewal On Suibmit  Click

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            'checking whether this ppty already has renewal requisitions or not
            Dim res As Integer = checkPptyReqisition()
            If res > 0 Then
                lblMsg.Text = Convert.ToString(Session("REQ_ID")) & " is Already Under Renewal Requisition or L1 Renewal Requisition Approval Process"
                Exit Sub
            End If

           
            Dim param(119) As SqlParameter
            'Area &Cost
            param(0) = New SqlParameter("@CTS_NUMBER", SqlDbType.VarChar)
            param(0).Value = txtLnumber.Text
            param(1) = New SqlParameter("@ENTITLE_LEASE_AMOUNT", SqlDbType.Decimal)
            param(1).Value = IIf(txtentitle.Text = "", 0, Convert.ToDecimal(txtentitle.Text))
            param(2) = New SqlParameter("@LEASE_RENT", SqlDbType.Decimal)
            param(2).Value = IIf(txtInvestedArea.Text = "", 0, Convert.ToDecimal(txtInvestedArea.Text))
            param(3) = New SqlParameter("@SECURITY_DEPOSIT", SqlDbType.Decimal)
            param(3).Value = IIf(txtpay.Text = "", 0, Convert.ToDecimal(txtpay.Text))
            param(4) = New SqlParameter("@SECURITY_DEP_MONTHS", SqlDbType.Int)
            param(4).Value = Convert.ToInt32(ddlSecurityDepMonths.SelectedValue)
            param(5) = New SqlParameter("@RENT_FREE_PRD", SqlDbType.Int)
            param(5).Value = Convert.ToInt32(txtRentFreePeriod.Text)
            param(6) = New SqlParameter("@RENT_SFT_CARPET", SqlDbType.Decimal)
            param(6).Value = IIf(txtRentPerSqftCarpet.Text = "0", 0, Convert.ToDecimal(txtRentPerSqftCarpet.Text))
            param(7) = New SqlParameter("@RENT_SFT_BUA", SqlDbType.Decimal)
            param(7).Value = IIf(txtRentPerSqftBUA.Text = "0", 0, Convert.ToDecimal(txtRentPerSqftBUA.Text))
            param(8) = New SqlParameter("@INTERIORCOST", SqlDbType.Decimal)
            param(8).Value = 0

            'Charges
            param(9) = New SqlParameter("@REGISTRATION_CHARGES", SqlDbType.Decimal)
            param(9).Value = IIf(txtregcharges.Text = "0", 0, Convert.ToDecimal(txtregcharges.Text))
            param(10) = New SqlParameter("@STAMP_DUTY", SqlDbType.Decimal)
            param(10).Value = IIf(txtsduty.Text = "0", 0, Convert.ToDecimal(txtsduty.Text))
            param(11) = New SqlParameter("@FURNITURE", SqlDbType.Decimal)
            param(11).Value = IIf(txtfurniture.Text = "0", 0, Convert.ToDecimal(txtfurniture.Text))
            param(12) = New SqlParameter("@PROFESSIONAL_FEES", SqlDbType.Decimal)
            param(12).Value = 0
            param(13) = New SqlParameter("@MAINTENANCE_CHARGES", SqlDbType.Decimal)
            param(13).Value = IIf(txtmain1.Text = "0", 0, Convert.ToDecimal(txtmain1.Text))
            param(14) = New SqlParameter("@SERVICE_TAX", SqlDbType.Decimal)
            param(14).Value = 0
            param(15) = New SqlParameter("@PROPERTY_TAX", SqlDbType.Decimal)
            param(15).Value = IIf(txtproptax.Text = "0", 0, Convert.ToDecimal(txtproptax.Text))
            param(16) = New SqlParameter("@BROKERAGE_AMOUNT_CHARGE", SqlDbType.Decimal)
            param(16).Value = 0

            'AGREEMENT DETAILS 
            param(17) = New SqlParameter("@EFF_DOA", SqlDbType.DateTime)
            param(17).Value = Convert.ToDateTime(txtsdate.Text)
            param(18) = New SqlParameter("@EXP_DOA", SqlDbType.DateTime)
            param(18).Value = Convert.ToDateTime(txtedate.Text)
            param(19) = New SqlParameter("@PPT_SNO", SqlDbType.Int)
            'param(19).Value = ddlproperty.SelectedValue
            param(19).Value = Session("Property")
            param(20) = New SqlParameter("@LOCKINPERIOD", SqlDbType.Int)
            param(20).Value = Convert.ToInt32(txtlock.Text)
            param(21) = New SqlParameter("@LEASEPERIOD", SqlDbType.Int)
            param(21).Value = Convert.ToInt32(txtLeasePeiodinYears.Text)
            param(22) = New SqlParameter("@NOTICEPERIOD", SqlDbType.Int)
            param(22).Value = Convert.ToInt32(txtNotiePeriod.Text)

            'BROKAGE DETAILS
            param(23) = New SqlParameter("@BROKERAGE_AMOUNT", SqlDbType.Decimal)
            param(23).Value = 0
            param(24) = New SqlParameter("@BROKER_NAME", SqlDbType.VarChar)
            param(24).Value = ""
            param(25) = New SqlParameter("@BROKER_ADDR", SqlDbType.VarChar)
            param(25).Value = ""
            param(26) = New SqlParameter("@BROKER_PAN", SqlDbType.VarChar)
            param(26).Value = ""
            param(27) = New SqlParameter("@BROKER_EMAIL", SqlDbType.VarChar)
            param(27).Value = ""
            param(28) = New SqlParameter("@BROKER_CONTACT", SqlDbType.VarChar)
            param(28).Value = ""

            'UTILITY/POWER BACKUP DETAILS
            param(29) = New SqlParameter("@DGSET", SqlDbType.VarChar)
            param(29).Value = ddlDgSet.SelectedValue
            param(30) = New SqlParameter("@DGSET_COM_UNIT", SqlDbType.VarChar)
            param(30).Value = txtDgSetPerUnit.Text
            param(31) = New SqlParameter("@DGSET_LOCATION", SqlDbType.VarChar)
            param(31).Value = txtDgSetLocation.Text
            param(32) = New SqlParameter("@SPACE_SERVO_STAB", SqlDbType.VarChar)
            param(32).Value = txtSpaceServoStab.Text
            param(33) = New SqlParameter("@ELEC_METER", SqlDbType.VarChar)
            param(33).Value = ddlElectricalMeter.SelectedValue
            param(34) = New SqlParameter("@METER_LOC", SqlDbType.VarChar)
            param(34).Value = txtMeterLocation.Text
            param(35) = New SqlParameter("@EARTHING_PIT", SqlDbType.VarChar)
            param(35).Value = txtEarthingPit.Text
            param(36) = New SqlParameter("@AVAIL_POWER", SqlDbType.Float)
            param(36).Value = IIf(txtAvailablePower.Text = "", 0, txtAvailablePower.Text)
            param(37) = New SqlParameter("@ADDITIONAL_POWER", SqlDbType.Float)
            param(37).Value = IIf(txtAdditionalPowerKWA.Text = "", 0, txtAdditionalPowerKWA.Text)
            param(38) = New SqlParameter("@POWER_SPEC", SqlDbType.VarChar)
            param(38).Value = txtPowerSpecification.Text

            'OTHER SERVICES
            param(39) = New SqlParameter("@TWO_WHL_PARK", SqlDbType.Int)
            param(39).Value = Convert.ToInt32(txtNoOfTwoWheelerParking.Text)
            param(40) = New SqlParameter("@CAR_PARK", SqlDbType.Int)
            param(40).Value = Convert.ToInt32(txtNoOfCarsParking.Text)
            param(41) = New SqlParameter("@AIRPRT_DIST", SqlDbType.Float)
            param(41).Value = 0
            param(42) = New SqlParameter("@RAIL_DIST", SqlDbType.Float)
            param(42).Value = 0
            param(43) = New SqlParameter("@RAIL_BUSTOP", SqlDbType.Float)
            param(43).Value = 0

            'LEASE ESCALATION DETAILS   
            param(44) = New SqlParameter("@OPTION_LEASE_ESC", SqlDbType.VarChar)
            param(44).Value = ddlesc.SelectedValue
            param(45) = New SqlParameter("@ESC_TYPE", SqlDbType.VarChar)
            param(45).Value = ddlLeaseEscType.SelectedValue
            param(46) = New SqlParameter("@LEASEHOLD_IMPROVEMENTS", SqlDbType.VarChar)
            param(46).Value = "Na"
            param(47) = New SqlParameter("@LEASE_COMMENTS", SqlDbType.VarChar)
            param(47).Value = txtComments.Text
            param(48) = New SqlParameter("@DUE_DILIGENCE", SqlDbType.VarChar)
            param(48).Value = "No"
            Dim selectedItems As String = [String].Join(",", ReminderCheckList.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
            param(49) = New SqlParameter("@REMINDER_BEFORE", SqlDbType.VarChar)
            param(49).Value = selectedItems

            'Rent Revision
            param(50) = New SqlParameter("@TOTAL_RENT", SqlDbType.Decimal)
            param(50).Value = IIf(txttotalrent.Text = "", 0, Convert.ToDecimal(txttotalrent.Text))
            param(51) = New SqlParameter("@TENURE", SqlDbType.VarChar)
            param(51).Value = ddlTenure.SelectedValue

            'Other Details
            param(52) = New SqlParameter("@COMPETE_VISCINITY", SqlDbType.VarChar)
            param(52).Value = ""
            param(53) = New SqlParameter("@ROLLING_SHUTTER", SqlDbType.VarChar)
            param(53).Value = "No"
            param(54) = New SqlParameter("@OFFCIE_EQUIPMENTS", SqlDbType.VarChar)
            param(54).Value = ""

            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas

            'If fu1.PostedFiles IsNot Nothing Then
            '    For Each File In fu1.PostedFiles
            '        If File.ContentLength > 0 Then
            '            Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
            '            Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
            '            File.SaveAs(filePath)
            '            IC = New ImageClas()
            '            IC.Filename = File.FileName
            '            IC.FilePath = Upload_Time & "_" & File.FileName
            '            Imgclass.Add(IC)
            '        End If
            '    Next
            'End If

            param(55) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(55).Value = UtilityService.ConvertToDataTable(Imgclass)
            param(56) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(56).Value = Session("Uid").ToString
            param(57) = New SqlParameter("@PPT_CODE", SqlDbType.VarChar)
            'param(57).Value = txtPropCode.Text
            param(57).Value = ""
            param(58) = New SqlParameter("@POA_SIGNED", SqlDbType.VarChar)
            param(58).Value = ddlAgreementbyPOA.SelectedValue
            param(59) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(59).Value = lblLeaseReqId.Text

            'POA Details
            param(60) = New SqlParameter("@POA_NAME", SqlDbType.VarChar)
            param(60).Value = txtPOAName.Text
            param(61) = New SqlParameter("@POA_ADDRESS", SqlDbType.VarChar)
            param(61).Value = txtPOAAddress.Text
            param(62) = New SqlParameter("@POA_MOBILE", SqlDbType.VarChar)
            param(62).Value = txtPOAMobile.Text
            param(63) = New SqlParameter("@POA_EMAIL", SqlDbType.VarChar)
            param(63).Value = txtPOAEmail.Text

            'Rent Revision
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            For Each i As RepeaterItem In rpRevision.Items
                rr_obj = New RentRevision()
                Dim txtRevision As TextBox = DirectCast(i.FindControl("txtRevision"), TextBox)
                Dim lblRevYear As Label = DirectCast(i.FindControl("lblRevYear"), Label)
                If txtRevision IsNot Nothing Then
                    rr_obj.RR_Year = lblRevYear.Text
                    rr_obj.RR_Percentage = IIf(txtRevision.Text = "", 0, txtRevision.Text)
                    RR_lst.Add(rr_obj)
                End If
            Next

           
            param(64) = New SqlParameter("@REVISIONLIST", SqlDbType.Structured)
            param(64).Value = UtilityService.ConvertToDataTable(RR_lst)

            param(65) = New SqlParameter("@LEASEEXPENSESLIST", SqlDbType.VarChar)
            param(65).Value = ""
            param(66) = New SqlParameter("@COST_TYPE", SqlDbType.VarChar)
            param(66).Value = rblCostType.SelectedValue
            param(67) = New SqlParameter("@SEAT_COST", SqlDbType.Decimal)
            param(67).Value = IIf(txtSeatCost.Text = "", 0.0, txtSeatCost.Text)

            param(68) = New SqlParameter("@NO_OF_LL", SqlDbType.Int)
            param(68).Value = Convert.ToInt32(txtNoLanlords.Text)


            'Charges section extra parameter

            param(69) = New SqlParameter("@OTHER_TAX", SqlDbType.Decimal)
            param(69).Value = IIf(txtOtherTax.Text = "", 0, Convert.ToDecimal(txtOtherTax.Text))

            param(70) = New SqlParameter("@EMAIL_ADDRESS", SqlDbType.VarChar)
            param(70).Value = txtReminderEmail.Text

            'SaveDocs()

            param(71) = New SqlParameter("@LEGAL_DOCS", SqlDbType.Structured)
            param(71).Value = UtilityService.ConvertToDataTable(legalDocs)


            'Office Connectivity Section

            param(72) = New SqlParameter("@PM_OC_TEL_SP_BUILDING", SqlDbType.VarChar)
            param(72).Value = ""

            param(73) = New SqlParameter("@PM_OC_TEL_SP_FIRST_OCCUPANT", SqlDbType.VarChar)
            param(73).Value = ""

            param(74) = New SqlParameter("@PM_OC_LEASE_LINE_SP_BUILD", SqlDbType.VarChar)
            param(74).Value = ""

            param(75) = New SqlParameter("@PM_OC_LEASE_LINE_SP_FIRST_OCCPNT", SqlDbType.VarChar)
            param(75).Value = ""

            param(76) = New SqlParameter("@PM_OC_TEL_VEN_FEASIBILITY", SqlDbType.VarChar)
            param(76).Value = ""

            param(77) = New SqlParameter("@PM_OC_IT_VEN_FEASIBILITY", SqlDbType.VarChar)
            param(77).Value = ""

            'Civil

            param(78) = New SqlParameter("@PM_C_PUNNG_WALLS", SqlDbType.VarChar)
            param(78).Value = ""

            param(79) = New SqlParameter("@PM_C_CRACKS", SqlDbType.VarChar)
            param(79).Value = ""

            param(80) = New SqlParameter("@PM_C_TOILET", SqlDbType.VarChar)
            param(80).Value = ""

            param(81) = New SqlParameter("@PM_C_PANTRY", SqlDbType.VarChar)
            param(81).Value = ""

            param(82) = New SqlParameter("@PM_C_DRAINAGE_DRAWNGS", SqlDbType.VarChar)
            param(82).Value = ""

            param(83) = New SqlParameter("@PM_C_WINDOW_HEIGHT", SqlDbType.VarChar)
            param(83).Value = ""

            param(84) = New SqlParameter("@PM_C_WATER_LEAKAGE", SqlDbType.VarChar)
            param(84).Value = ""

            param(85) = New SqlParameter("@PM_C_STAIRCASE", SqlDbType.VarChar)
            param(85).Value = ""

            param(86) = New SqlParameter("@PM_C_EMRG_EXIT", SqlDbType.VarChar)
            param(86).Value = ""

            param(87) = New SqlParameter("@PM_C_HEIGH_AVAILABILITY", SqlDbType.VarChar)
            param(87).Value = ""

            param(88) = New SqlParameter("@PM_C_ADD_CIVIL_WORK", SqlDbType.VarChar)
            param(88).Value = ""


            'Outstanding amounts / Damages

            param(89) = New SqlParameter("@PM_D_ANY_DAMAGES", SqlDbType.VarChar)
            param(89).Value = ddlOutOrDamagesAmount.SelectedValue

            param(90) = New SqlParameter("@PM_D_EXTND_STAY", SqlDbType.VarChar)
            param(90).Value = ddlDamangesExtStay.SelectedValue

            param(91) = New SqlParameter("@PM_D_HAND_OVER", SqlDbType.VarChar)
            param(91).Value = ddlPossessionHandover.SelectedValue

            param(92) = New SqlParameter("@PM_D_COMP_VICINITY", SqlDbType.VarChar)
            param(92).Value = ""

            param(93) = New SqlParameter("@PM_D_OFFC_EQUIPMNTS", SqlDbType.VarChar)
            param(93).Value = ""

            param(94) = New SqlParameter("@PM_D_WRKNG_HRS_CONSTRUCTION", SqlDbType.VarChar)
            param(94).Value = ""

            param(95) = New SqlParameter("@PM_D_USAGE_TOILET", SqlDbType.VarChar)
            param(95).Value = ""

            param(96) = New SqlParameter("@PM_D_USAGE_LIFT", SqlDbType.VarChar)
            param(96).Value = ""

            param(97) = New SqlParameter("@PM_D_SECURITY_SYSTEM", SqlDbType.VarChar)
            param(97).Value = ""

            'utility power extra fields start

            param(98) = New SqlParameter("@PM_LUP_PATH_ELEC_CABLE", SqlDbType.VarChar)
            param(98).Value = ""

            param(99) = New SqlParameter("@PM_LUP_GROUND_EARTH", SqlDbType.VarChar)
            param(99).Value = ""

            param(100) = New SqlParameter("@PM_LUP_EARTH_TYPE", SqlDbType.VarChar)
            param(100).Value = ""

            param(101) = New SqlParameter("@PM_LUP_LIFT_POWERCUT", SqlDbType.VarChar)
            param(101).Value = txtLiftPowerCut.Text

            param(102) = New SqlParameter("@PM_LUP_TRANSFORMER_LOCATION", SqlDbType.VarChar)
            param(102).Value = ""

            param(103) = New SqlParameter("@PM_LUP_WATER_CONNECTION", SqlDbType.VarChar)
            param(103).Value = txtWaterConnection.Text
            'utility power extra fields end


            'other service extra fields start
            param(104) = New SqlParameter("@PM_LO_LIFT_YES_NO", SqlDbType.VarChar)
            param(104).Value = ddlLift.Text

            param(105) = New SqlParameter("@PM_LO_MAKE", SqlDbType.VarChar)
            param(105).Value = txtMake.Text

            param(106) = New SqlParameter("@PM_LO_CAPACITY", SqlDbType.VarChar)
            param(106).Value = txtCapacity.Text

            param(107) = New SqlParameter("@PM_LO_MAIN_STAIRCASE_WIDTH", SqlDbType.VarChar)
            param(107).Value = txtStairCaseWidth.Text

            param(108) = New SqlParameter("@PM_LO_FIRE_STAIRCASE", SqlDbType.VarChar)
            param(108).Value = txtFireStaircase.Text

            param(109) = New SqlParameter("@PM_LO_AC_OUTDOOR_SPACE", SqlDbType.VarChar)
            param(109).Value = txtACOutdoor.Text

            param(110) = New SqlParameter("@PM_LO_ACS_AT_LOCATION", SqlDbType.VarChar)
            param(110).Value = ""

            param(111) = New SqlParameter("@PM_LO_SIGNAGE_BRANDING", SqlDbType.VarChar)
            param(111).Value = txtsignOrbrand.Text

            param(112) = New SqlParameter("@PM_LO_FIRE_EXIT_STAIRS", SqlDbType.VarChar)
            param(112).Value = ""

            param(113) = New SqlParameter("@PM_LO_FIRE_FIGHTING_SYSTEM", SqlDbType.VarChar)
            param(113).Value = ""

            param(114) = New SqlParameter("@PM_LO_LIFTS_AVAILABLE_LICENSE", SqlDbType.VarChar)
            param(114).Value = txtLiftNLicense.Text

            param(115) = New SqlParameter("@PM_LO_MAINT_AGENCY_LIFT", SqlDbType.VarChar)
            param(115).Value = txtMaintAgencyLift.Text

            'other service extra fields end

            'property or building section input fields start
            param(116) = New SqlParameter("@PM_SHOP_NUMBER_TO_OCCUPY", SqlDbType.VarChar)
            param(116).Value = txtshopnumberoccupy.Text

            param(117) = New SqlParameter("@PM_TERMS_OF_LEASE", SqlDbType.VarChar)
            param(117).Value = txtTermofLease.Text

            param(118) = New SqlParameter("@PM_ROLLING_SHUTTER", SqlDbType.VarChar)
            param(118).Value = "No"

            param(119) = New SqlParameter("@RENEW_STATUS", SqlDbType.Int)
            param(119).Value = 1
            'property or building section input fields end

            ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
            ViewState("LES_TOT_RENT") = Convert.ToDecimal(txttotalrent.Text)
            ViewState("LES_SEC_DEPOSIT") = Convert.ToDecimal(txtpay.Text)




            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_ADD_LEASE", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        'lblMsg.Text = "Lease Request Raised Successfully : " + lblLeaseReqId.Text
                        lblLeaseReqId.Text = ""
                        Session("ID") = sdr("ID").ToString()
                        Session("REQ_ID") = sdr("REQUEST_ID").ToString()
                        BindReqId()
                        panPOA.Visible = False
                        ClearTextBox(Me)
                        AssigTextValues()
                        ClearDropdown(Me)
                        Landlord.Visible = True
                        AddLeaseDetails.Visible = False
                        lblMsg.Text = ""
                        lblMsg.Text = "Renewal Of Lease Is Done Waiting For Approval" + ":" + Session("REQ_ID")
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetLandlords()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORDS")
        sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        sp.Command.AddParameter("@ID", hdnLSNO.Value, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvlandlordItems.DataSource = ds
        gvlandlordItems.DataBind()

    End Sub

    Protected Sub gvlandlordItems_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvlandlordItems.RowCancelingEdit
        gvlandlordItems.EditIndex = -1
        GetLandlords()
    End Sub

    Protected Sub gvlandlordItems_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvlandlordItems.RowEditing
        gvlandlordItems.EditIndex = e.NewEditIndex
        GetLandlords()
    End Sub

    Protected Sub gvlandlordItems_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles gvlandlordItems.RowUpdating
        Dim lblsno As String = DirectCast(gvlandlordItems.Rows(e.RowIndex) _
                                    .FindControl("lblsno"), Label).Text
        Dim txtRentAmount As String = DirectCast(gvlandlordItems.Rows(e.RowIndex).FindControl("txtRentAmount"), TextBox).Text
        Dim lblRentAmount As Label = DirectCast(gvlandlordItems.Rows(e.RowIndex).FindControl("lblRentAmount"), Label)
        Dim txtsecamount As String = DirectCast(gvlandlordItems.Rows(e.RowIndex).FindControl("txtsecamount"), TextBox).Text
        Dim lblsecamount As Label = DirectCast(gvlandlordItems.Rows(e.RowIndex).FindControl("lblsecamount"), Label)
        'lblLeaseReqId.Text
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_UPDATE_LANDLORD_RENT_DETAILS")
        sp.Command.AddParameter("@REQ_ID", lblLeaseReqId.Text, DbType.String)
        sp.Command.AddParameter("@ID", lblsno, DbType.Int32)
        sp.Command.AddParameter("@LAN_RENT", txtRentAmount, DbType.Currency)
        sp.Command.AddParameter("@LAN_SECURITY", txtsecamount, DbType.Currency)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Execute()
        gvlandlordItems.EditIndex = -1
        GetLandlords()
    End Sub

    Public Sub AssigTextValues()
        'txtInteriorCost.Text = 0
        txtfurniture.Text = 0
        'txtbrokerage.Text = 0
        'txtpfees.Text = 0
        txttotalrent.Text = 0
        txttotalrent.Text = 0
        'txtFirstYear.Text = 0
        'txtSecondYear.Text = 0
        'txtThirdYear.Text = 0
        txtlock.Text = 0
        txtLeasePeiodinYears.Text = 0
        txtNotiePeriod.Text = 0
        txtAvailablePower.Text = 0
        txtAdditionalPowerKWA.Text = 0
        txtNoOfTwoWheelerParking.Text = 0
        txtNoOfCarsParking.Text = 0
        'txtDistanceFromAirPort.Text = 0
        'txtDistanceFromRailwayStation.Text = 0
        'txtDistanceFromBustop.Text = 0
        txtpmonthrent.Text = 0
        txtpsecdep.Text = 0
        'txtbrkamount.Text = 0
        'txtbrkamount.Text = 0
        'txtbrkmob.Text = 0
    End Sub

    Public Sub ClearTextBox(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearTextBox(ctrl)
            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Text = ""
            End If
        Next ctrl
    End Sub

    Public Sub ClearDropdown(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearDropdown(ctrl)
            If TypeOf ctrl Is DropDownList Then
                CType(ctrl, DropDownList).ClearSelection()
            End If
        Next ctrl
    End Sub

    Protected Sub CancelEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvlandlordItems.RowCancelingEdit
        gvLeaseExpences.EditIndex = -1
        BindLeaseExpences()
    End Sub

    Protected Sub EditLeaseExpense(sender As Object, e As GridViewEditEventArgs)
        gvLeaseExpences.EditIndex = e.NewEditIndex
        BindLeaseExpences()
    End Sub

    Protected Sub UpdateLeaseExpense(sender As Object, e As GridViewUpdateEventArgs)
        Dim ddlServiceProvider As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlServiceProvider"), DropDownList).SelectedItem.Value
        Dim ddliptype As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddliptype"), DropDownList).SelectedItem.Value
        Dim ddlPaidBy As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlPaidBy"), DropDownList).SelectedItem.Value
        Dim PM_EXP_SNO As String = gvLeaseExpences.DataKeys(e.RowIndex).Value.ToString()
        Dim code As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("lblServiceID"), Label).Text
        Dim compValue As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("txtCompValue"), TextBox).Text
        Dim Flag As Integer
        Dim param(9) As SqlParameter
        'Area &Cost
        param(0) = New SqlParameter("@PM_EXP_SERV_PROVIDER", SqlDbType.VarChar)
        param(0).Value = ddlServiceProvider
        param(1) = New SqlParameter("@PM_EXP_INP_TYPE", SqlDbType.VarChar)
        param(1).Value = ddliptype
        param(2) = New SqlParameter("@PM_EXP_COMP_LES_VAL", SqlDbType.VarChar)
        param(2).Value = compValue
        param(3) = New SqlParameter("@PM_EXP_PAID_BY", SqlDbType.VarChar)
        param(3).Value = ddlPaidBy

        param(4) = New SqlParameter("@PM_EXP_SNO", SqlDbType.VarChar)
        param(4).Value = PM_EXP_SNO

        If PM_EXP_SNO = "" Then
            Flag = 2 ' Insert
        Else
            Flag = 1 ' Update
        End If

        param(5) = New SqlParameter("@FLAG", SqlDbType.VarChar)
        param(5).Value = Flag

        param(6) = New SqlParameter("@PM_EXP_PM_LES_SNO", SqlDbType.VarChar)
        param(6).Value = hdnLSNO.Value

        param(7) = New SqlParameter("@PM_EXP_HEAD", SqlDbType.VarChar)
        param(7).Value = code

        param(8) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
        param(8).Value = Session("Uid").ToString

        param(9) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
        param(9).Value = Session("REQ_ID")

        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_UPDATE_LEASE_EXPENSES", param)
            While sdr.Read()
                If sdr("result").ToString() = "SUCCESS" Then
                    lblMsg.Text = "Lease Expenses Updated Successfully : " + Session("REQ_ID")
                    BindLeaseExpences()
                Else
                    lblMsg.Text = "Something went wrong. Please try again later."
                End If
            End While
            sdr.Close()
        End Using
    End Sub
    Protected Sub RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow AndAlso gvLeaseExpences.EditIndex = e.Row.RowIndex Then
            Dim ddlSerpr As DropDownList = DirectCast(e.Row.FindControl("ddlServiceProvider"), DropDownList)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SERVICE_PROVIDER_DDL")
            ddlSerpr.DataSource = sp.GetDataSet().Tables(0)
            ddlSerpr.DataTextField = "PM_SP_NAME"
            ddlSerpr.DataValueField = "PM_SP_SNO"
            ddlSerpr.DataBind()

            If Not TryCast(e.Row.FindControl("lblspno"), Label).Text = "" Then
                ddlSerpr.Items.FindByValue(TryCast(e.Row.FindControl("lblspno"), Label).Text).Selected = True
            End If

            If Not TryCast(e.Row.FindControl("lblipname"), Label).Text = "" Then
                Dim ddliptype As DropDownList = DirectCast(e.Row.FindControl("ddliptype"), DropDownList)
                ddliptype.Items.FindByValue(TryCast(e.Row.FindControl("lblipname"), Label).Text).Selected = True
            End If

            If Not TryCast(e.Row.FindControl("lblPaidby"), Label).Text = "" Then

                Dim ddlPaidBy As DropDownList = DirectCast(e.Row.FindControl("ddlPaidBy"), DropDownList)
                ddlPaidBy.Items.FindByValue(TryCast(e.Row.FindControl("lblPaidby"), Label).Text).Selected = True
            End If
        End If
    End Sub
    Public Sub fillgridOnTenantCodeSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_RENEWAL_FILTER_DETAILS")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)

            sp2.Command.AddParameter("@SEARCH", txtSearch.Text, DbType.String)
            'Session("dataset") = sp2.GetDataSet()
            Dim ds As New DataTable()
            ds = Nothing
            gvLDetails_Lease.DataSource = ds
            gvLDetails_Lease.DataSource = sp2.GetDataSet()
            gvLDetails_Lease.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            If txtSearch.Text = "" Then
                lblMsg02.Visible = True
                lblMsg02.Text = "Please Search By Lease Id/Property Name/City/Location"
            Else
                lblMsg02.Visible = False
                lblMsg.Visible = False
                fillgridOnTenantCodeSearch()
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub gvLegalDocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLegalDocs.RowCommand
    '    Try
    '        If e.CommandName = "DownloadLeaseDoc" Then

    '            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            Dim RowIndex As Integer = row.RowIndex
    '            Dim filepath As String = DirectCast(row.FindControl("ldocPath"), LinkButton).Text.ToString()
    '            filepath = "~\Images\Property_Images\" & filepath
    '            Response.ContentType = "application/CSV"
    '            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filepath, "~\Images\Property_Images", "") & """")
    '            Response.TransmitFile(Server.MapPath(filepath))
    '            Response.[End]()
    '        ElseIf e.CommandName = "DeleteLeaseDoc" Then
    '            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            Dim RowIndex As Integer = row.RowIndex
    '            Dim docSNO As String = DirectCast(row.FindControl("lblLdocSNO"), Label).Text.ToString()
    '            param = New SqlParameter(0) {}
    '            param(0) = New SqlParameter("@PM_LDOC_SNO", SqlDbType.NVarChar, 50)
    '            param(0).Value = docSNO
    '            ObjSubSonic.GetSubSonicDataSet("PM_DELETE_LEASE_DOCS", param)
    '            BindLegalDocuments(hdnLSNO.Value)
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    Protected Sub ddlState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlState.SelectedIndexChanged
        BindCity()
    End Sub

    'Protected Sub gvboard_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvboard.RowCommand
    '    Try
    '        If e.CommandName = "Download" Then
    '            Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
    '            Response.ContentType = ContentType
    '            Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
    '            Response.WriteFile(imgPath)
    '            Response.End()
    '        Else
    '            Dim gvbrd As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            gvboard.Rows(gvbrd.RowIndex).Visible = False
    '            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_DELETE_DOCS")
    '            sp.Command.AddParameter("@DOC_ID", e.CommandArgument, DbType.String)
    '            sp.Command.AddParameter("@TYPE", "BOARDDOC", DbType.String)
    '            sp.ExecuteScalar()
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

   
End Class

Public Class DocClas
    Private _fn As String

    Public Property Filename() As String
        Get
            Return _fn
        End Get
        Set(ByVal value As String)
            _fn = value
        End Set
    End Property

    Private _Fpath As String

    Public Property FilePath() As String
        Get
            Return _Fpath
        End Get
        Set(ByVal value As String)
            _Fpath = value
        End Set
    End Property


    Private _DocSNO As Integer

    Public Property Doc_SNO() As String
        Get
            Return _DocSNO
        End Get
        Set(ByVal value As String)
            _DocSNO = value
        End Set
    End Property

End Class

Public Class ImageClas
    Private _fn As String

    Public Property Filename() As String
        Get
            Return _fn
        End Get
        Set(ByVal value As String)
            _fn = value
        End Set
    End Property

    Private _Fpath As String

    Public Property FilePath() As String
        Get
            Return _Fpath
        End Get
        Set(ByVal value As String)
            _Fpath = value
        End Set
    End Property

End Class
