<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmModifyPropertyDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmModifyPropertyDetails"
    MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='http://localhost:51165/fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        /*body
        {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }*/

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #f5f5f5;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>ZFM-Property Options
                            </legend>
                        </fieldset>
                        <h3 class="panel">
                            <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        </h3>
                        <form id="form1" class="well" runat="server">
                            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />
                            <div class="panel panel-default " role="tab" id="div0">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#General">Location Details</a>
                                    </h4>
                                </div>
                                <div id="General" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Request ID</label>
                                                    <asp:TextBox ID="txtReqID" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">


                                            <div class="fprm-group col-sm-3 col-xs-6">
                                                <div class="form-group">
                                                    <label>Request Type<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlReqType" runat="server" ControlToValidate="ddlReqType"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Request Type"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control selectpicker with-search" onchange="clearMsg()" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="fprm-group col-sm-3 col-xs-6">
                                                <div class="form-group">
                                                    <label>Property Nature<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlPprtNature" runat="server" ControlToValidate="ddlPprtNature"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlPprtNature" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                        <asp:ListItem Value="1">Lease</asp:ListItem>
                                                        <asp:ListItem Value="2">Own</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="fprm-group col-sm-3 col-xs-6">
                                                <div class="form-group">
                                                    <label>Acquisition Through<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlAcqThr" runat="server" ControlToValidate="ddlAcqThr"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Acquisition Through"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlAcqThr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                        <asp:ListItem Value="1">Purchase</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>City<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">


                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Location<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                        data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Tower<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlTower" runat="server" ControlToValidate="ddlTower"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Tower" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlTower" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                        data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Floor<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlFloor" runat="server" ControlToValidate="ddlFloor"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Floor" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Type Of Office<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rvpropertyType" runat="server" ControlToValidate="ddlPropertyType"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Type Of Office"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <div>
                                                        <asp:DropDownList ID="ddlPropertyType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Property Name<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="revPropIDName" runat="server" ControlToValidate="txtPropIDName"
                                                        Display="None" ErrorMessage="Please Enter Property Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revpropertyname" runat="server" ControlToValidate="txtPropIDName"
                                                        Display="None" ErrorMessage="Invalid Property Name" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropIDName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Property Address<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtPropDesc"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Property Address"></asp:RequiredFieldValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Address with maximum 250 characters')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropDesc" runat="server" CssClass="form-control"
                                                                Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>ESTD(Year)</label>
                                                    <asp:RegularExpressionValidator ID="rgetxtESTD" runat="server" ControlToValidate="txtESTD"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid ESTD." ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter Numericals With Maximum length 4')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtESTD" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="4"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Age</label>
                                                    <div>
                                                        <div>
                                                            <asp:TextBox ID="txtAge" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <%--From Lease--%>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Competitors in Vicinity</label>
                                                    <asp:TextBox ID="txtCompetitorsVicinity" runat="server" CssClass="form-control" MaxLength="12"></asp:TextBox>
                                                </div>
                                            </div>
                                            <%--From Lease--%>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Type Of Agreement<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlOffice"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Type Of Agreement" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlOffice" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default " role="tab" id="div2">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Area">Commercial Details</a>
                                    </h4>
                                </div>
                                <div id="Area" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Super Built Up Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtSuperBulArea" runat="server" ControlToValidate="txtSuperBulArea"
                                                        Display="None" ErrorMessage="Please Enter Super Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txtUsableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Super Built Up Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtSuperBulArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Carpet Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfCarpetarea" runat="server" ControlToValidate="txtCarpetArea"
                                                        Display="None" ErrorMessage="Please Enter Carpet Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revCarpetarea" runat="server" ControlToValidate="txtOptCapacity"
                                                        Display="None" ErrorMessage="Invalid Carpet Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Total No. Of Floors</label>
                                                    <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Built Up Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                        Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                        Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtBuiltupArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Common Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvCommonarea" runat="server" ControlToValidate="txtCommonArea"
                                                        Display="None" ErrorMessage="Please Enter Common Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revCommon" runat="server" ControlToValidate="txtCommonArea"
                                                        Display="None" ErrorMessage="Invalid Common Area" ValidationExpression="^[0-9]*\.?[0-9]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtCommonArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Rentable Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvRentable" runat="server" ControlToValidate="txtRentableArea"
                                                        Display="None" ErrorMessage="Please Enter Rentable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRentableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Rentable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtRentableArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Usable Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvUsable" runat="server" ControlToValidate="txtUsableArea"
                                                        Display="None" ErrorMessage="Please Enter Usable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revUsable" runat="server" ControlToValidate="txtUsableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Usable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtUsableArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Plot Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtPlotArea" runat="server" ControlToValidate="txtPlotArea"
                                                        Display="None" ErrorMessage="Please Enter Plot Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="txtUsableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Plot Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPlotArea" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Floor to Ceiling Height(ft) </label>
                                                    <asp:RegularExpressionValidator ID="rfvtxtCeilingHight" runat="server" ControlToValidate="txtCeilingHight"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Ceiling Hight" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtCeilingHight" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Floor to Beam Bottom Height(ft) </label>
                                                    <asp:RegularExpressionValidator ID="rfvtxtBeamBottomHight" runat="server" ControlToValidate="txtBeamBottomHight"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Beam Bottom Height" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtBeamBottomHight" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Maximum Capacity<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvMaxCapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                        Display="None" ErrorMessage="Please Enter Maximum Capacity" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revmaxcapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                        Display="None" ErrorMessage="Invalid Maximum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtMaxCapacity" runat="server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Optimum Capacity </label>
                                                    <asp:RegularExpressionValidator ID="revOptCapacity" runat="server" ControlToValidate="txtOptCapacity"
                                                        Display="None" ErrorMessage="Invalid Optimum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtOptCapacity" runat="Server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Seating Capacity</label>
                                                    <asp:RegularExpressionValidator ID="rfvtxtSeatingCapacity" runat="server" ControlToValidate="txtSeatingCapacity"
                                                        Display="None" ErrorMessage="Invalid Seating Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtSeatingCapacity" runat="Server" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Flooring Type</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlFlooringType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>FSI (Floor Space Index)</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlFSI" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-3 col-xs-6" runat="server" id="divFSI">
                                                <div class="form-group">
                                                    <label>FSI Ratio (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                        Display="None" ErrorMessage="Please Enter FSI Ratio" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid FSI Ratio" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtFSI" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Preferred Efficiency (%) </label>
                                                    <asp:RegularExpressionValidator ID="revtxtEfficiency" runat="server" ControlToValidate="txtEfficiency"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Preferred Efficiency" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtEfficiency" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--From Lease--%>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>No of Two Wheelers Parking</label>
                                                    <asp:TextBox ID="txtNoOfTwoWheelerParking" runat="server" CssClass="form-control" TabIndex="82"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>No Of  Cars Parking</label>
                                                    <asp:TextBox ID="txtNoOfCarsParking" runat="server" CssClass="form-control" TabIndex="83"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Additional Recurring Cost</label>
                                                    <asp:TextBox ID="txtRecurringCost" runat="server" CssClass="form-control" TabIndex="83"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Available Power (KWA)</label>
                                                    <asp:TextBox ID="txtAvailablePower" runat="server" CssClass="form-control" TabIndex="79"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Basic Rent<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea" Display="None" ValidationGroup="Val1"
                                                        ErrorMessage="Please Enter Basic Rent"></asp:RequiredFieldValidator>
                                                    <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtInvestedArea" runat="server" CssClass="form-control" MaxLength="15" TabIndex="4"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Maintenance Charges<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtmain1" runat="server" ControlToValidate="txtmain1"
                                                        Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Maintenance Charges "></asp:RequiredFieldValidator>
                                                    <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtmain1" runat="server" CssClass="form-control" MaxLength="15" TabIndex="16"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--From Lease--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default " role="tab" id="div8">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Recom">Recommendation Details</a>
                                    </h4>
                                </div>
                                <div id="Recom" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Recommended/Priority</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlRecommended" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12" runat="server" id="divRecommanded">
                                                <div class="form-group">
                                                    <label>Recommended/Priority Remarks<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtRecmRemarks" runat="server" ControlToValidate="txtRecmRemarks"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Recommended/Priority Remarks"></asp:RequiredFieldValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Remarks with maximum 500 characters')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtRecmRemarks" runat="server" CssClass="form-control"
                                                                Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default " role="tab" id="div7" runat="server" visible="true">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#genDetails">General Details</a>
                                    </h4>
                                </div>
                                <div id="genDetails" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>No. Of Toilet Blocks</label>
                                                    <asp:RegularExpressionValidator ID="revtxtToilet" runat="server" ControlToValidate="txtToilet"
                                                        Display="None" ErrorMessage="Invalid No. Of Toilet Blocks" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtToilet" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Society Name</label>
                                                    <div>
                                                        <div>
                                                            <asp:TextBox ID="txtSocityName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Latitude</label>

                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtlat" ValidationGroup="Val1"
                                                        Display="None" ErrorMessage="Invalid Latitude" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <asp:TextBox ID="txtlat" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Longitude</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtlong" ValidationGroup="Val1"
                                                        Display="None" ErrorMessage="Invalid Longitude" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <asp:TextBox ID="txtlong" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Owner Scope Of Work</label>
                                                    <div>
                                                        <asp:TextBox ID="txtOwnScopeWork" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Signage Location</label>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Signage Location with maximum 250 characters')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtSignageLocation" runat="server" CssClass="form-control"
                                                                Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="divNewOfficeRelocation" runat="server" visible="true">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#genOtherDetails">General Other Details</a>
                                    </h4>
                                </div>
                                <div id="genOtherDetails" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Existing monthly rental </label>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtExistMonthRent" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed Area (for agency)</label>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtProposedAreaForAgency" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed Area (Other channels , if any)</label>


                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPropAreaOtherChannels" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed Monthly rental  </label>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropMonthlyRental" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed per sq.ft rental</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtPurPrice"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Purchase Price" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropsedPerSqftrental" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Is Board approval in place  </label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPprtNature"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlBoardAppr" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                        <asp:ListItem Value="">--Select--</asp:ListItem>
                                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                        <asp:ListItem Value="No">No</asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                            </div>


                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Cost Type On<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType" Display="None" ValidationGroup="Val1"
                                                        ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                    <asp:RadioButtonList ID="rblCostType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                        <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                        <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">

                                            <div id="Costype1" runat="server" visible="false">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Rent Per Sq.ft (On Carpet)<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftCarpet" runat="server" ControlToValidate="txtRentPerSqftCarpet" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Rent Per Sq.ft (On BUA)<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftBUA" runat="server" ControlToValidate="txtRentPerSqftBUA" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Costype2" runat="server" visible="false">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Seat Cost<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">.</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div1">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Owner">Owner Details</a>
                                    </h4>
                                </div>
                                <div id="Owner" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Owner Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvownname" runat="server" ControlToValidate="txtownrname"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Owner Name"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtownrname" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Phone Number </label>
                                                    <asp:RegularExpressionValidator ID="REVPHNO" runat="server" ControlToValidate="txtphno"
                                                        ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" Display="None"
                                                        ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtphno" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Previous Owner Name </label>
                                                    <asp:TextBox ID="txtPrvOwnName" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Previous Owner Phone Number</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtPrvOwnPhNo" Display="None"
                                                        ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPrvOwnPhNo" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Owner Email</label>
                                                    <asp:RegularExpressionValidator ID="rfv" runat="server" ControlToValidate="txtOwnEmail"
                                                        ValidationGroup="Val1" ErrorMessage="Please Enter Valid Email" Display="None"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                    </asp:RegularExpressionValidator>
                                                    <asp:TextBox ID="txtOwnEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="panel panel-default " role="tab" id="div3">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Purchase">Purchase Details</a>
                                    </h4>
                                </div>
                                <div id="Purchase" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Purchase Price </label>
                                                    <asp:RegularExpressionValidator ID="revtxtPurPrice" runat="server" ControlToValidate="txtPurPrice"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Purchase Price" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPurPrice" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Purchase Date</label>
                                                    <div>
                                                        <div class='input-group date' id='PurDate'>
                                                            <asp:TextBox ID="txtPurDate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('PurDate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Market Value </label>
                                                    <asp:RegularExpressionValidator ID="revtxtMarketValue" runat="server" ControlToValidate="txtMarketValue"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Market Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtMarketValue" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div4">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Govt">Govt. Details</a>
                                    </h4>
                                </div>
                                <div id="Govt" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>IRDA for opening new office?</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlIRDA" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No">No</asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>PC Code (Penal Code)</label>
                                                    <div>
                                                        <asp:TextBox ID="txtPCcode" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Government Property Code</label>
                                                    <asp:RegularExpressionValidator ID="revgovt" runat="server" ControlToValidate="txtGovtPropCode"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Government Property Code"
                                                        ValidationExpression="^[A-Za-z0-9//(/)\s/-]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtGovtPropCode" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>UOM CODE (Unit Of Measure)</label>
                                                    <div>
                                                        <asp:TextBox ID="txtUOM_CODE" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div5">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Insurance">Insurance Details</a>
                                    </h4>
                                </div>
                                <div id="Insurance" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Type</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlInsuranceType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Vendor</label>
                                                    <div>
                                                        <asp:TextBox ID="txtInsuranceVendor" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Amount</label>
                                                    <div>
                                                        <asp:TextBox ID="txtInsuranceAmt" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Policy Number</label>
                                                    <div>
                                                        <asp:TextBox ID="txtInsurancePolNum" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Start Date</label>
                                                    <div>
                                                        <div class='input-group date' id='fromdate'>
                                                            <asp:TextBox ID="txtInsuranceStartdate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>End Date</label>
                                                    <div>
                                                        <div class='input-group date' id='todate'>
                                                            <asp:TextBox ID="txtInsuranceEnddate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div6">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#PropDocs">Documents</a>
                                    </h4>
                                </div>
                                <div id="PropDocs" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-inline">
                                            <div class="form-group col-md-12">
                                                <h5>Property Documents</h5>
                                                <asp:GridView ID="gvPropdocs" runat="server" EmptyDataText="No Property Details Found."
                                                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Document Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblImgNam" runat="server" Text='<%#Eval("PM_IMG_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <%--<a href="../../images/Property_Images/<%#Eval("PM_IMG_PATH")%>">Download</a>--%>
                                                                <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" CommandArgument='<%#Eval("PM_IMG_PATH")%>'
                                                                    CommandName="Download"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandArgument='<%#Eval("PM_IMG_SNO")%>'
                                                                    CommandName="Delete"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                </asp:GridView>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <h5>Board Approval Documents</h5>
                                                <asp:GridView ID="gvboard" runat="server" EmptyDataText="No Board Approval Documents Found."
                                                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Board Approval Document">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblImgNam" runat="server" Text='<%#Eval("BRD_IMG_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" CommandArgument='<%#Eval("BRD_IMG_PATH")%>'
                                                                    CommandName="Download"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandArgument='<%#Eval("PM_PPT_SNO")%>'
                                                                    CommandName="Delete"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label for="txtcode">Upload Additional Property Images <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>

                                                    <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fu1"
                                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                        ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$">
                                                    </asp:RegularExpressionValidator>
                                                    <div class="btn btn-primary btn-mm">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" id="divBoardApprDocument" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label for="txtcode">Upload Board Approval Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Display="None" ControlToValidate="fuBoardApprlDoc"
                                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                        ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg|.doc|.docx|.pdf)$">
                                                    </asp:RegularExpressionValidator>

                                                    <div class="btn btn-primary btn-mm">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="fuBoardApprlDoc" runat="Server" Width="90%" AllowMultiple="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" id="divIRDADoc" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label for="txtcode">Upload IRDA Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" Display="None" ControlToValidate="fu1"
                                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                        ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg|.doc|.docx|.pdf)$">
                                                    </asp:RegularExpressionValidator>

                                                    <div class="btn btn-primary btn-mm">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="irdaAppr" runat="Server" Width="90%" AllowMultiple="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-12 text-right">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Update" CausesValidation="true" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };

        $('.closeall').click(function () {
            $('.panel-collapse.in')
              .collapse('hide');
        });
        $('.openall').click(function () {
            $('.panel-collapse:not(".in")')
              .collapse('show');
        });
    </script>
</body>
</html>



