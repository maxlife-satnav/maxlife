Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmPropertyLevelApproval
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            fillReqs()
            BindRequestTypes()
            BindPropType()
            BindInsuranceType()
            BindUserCities()
            BindFlooringTypes()
        End If
    End Sub

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select Request Type--", 0))
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", 0))
    End Sub

    Private Sub BindInsuranceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select Insurance Type--", 0))
    End Sub

    Private Sub GetLocationsbyCity(ByVal city As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", city)
        sp2.Command.AddParameter("@USR_ID", Session("uid"))
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Protected Sub DisableAllFunctions()
        ddlReqType.Enabled = False
        ddlPprtNature.Enabled = False
        ddlAcqThr.Enabled = False
        ddlCity.Enabled = False
        ddlLocation.Enabled = False
        ddlTower.Enabled = False
        txtFloor.Enabled = False
        ddlFloor.Enabled = False
        txtToilet.Enabled = False
        ddlPropertyType.Enabled = False
        txtPropIDName.Enabled = False
        txtESTD.Enabled = False
        txtAge.Enabled = False
        txtPropDesc.Enabled = False
        txtSignageLocation.Enabled = False
        txtSocityName.Enabled = False
        txtlat.Enabled = False
        txtlong.Enabled = False
        txtOwnScopeWork.Enabled = False
        txtRecmRemarks.Enabled = False

        txtownrname.Enabled = False
        txtphno.Enabled = False
        txtPrvOwnName.Enabled = False
        txtPrvOwnPhNo.Enabled = False
        txtOwnEmail.Enabled = False
        txtCompetitorsVicinity.Enabled = False
        txtCarpetArea.Enabled = False
        txtBuiltupArea.Enabled = False
        txtCommonArea.Enabled = False
        txtRentableArea.Enabled = False
        txtUsableArea.Enabled = False
        txtSuperBulArea.Enabled = False
        txtPlotArea.Enabled = False
        txtCeilingHight.Enabled = False
        txtBeamBottomHight.Enabled = False
        txtMaxCapacity.Enabled = False
        txtOptCapacity.Enabled = False
        txtSeatingCapacity.Enabled = False
        ddlFlooringType.Enabled = False
        txtFSI.Enabled = False
        txtEfficiency.Enabled = False

        txtPurPrice.Enabled = False
        txtPurDate.Enabled = False
        txtMarketValue.Enabled = False

        ddlIRDA.Enabled = False
        txtPCcode.Enabled = False
        txtGovtPropCode.Enabled = False
        txtUOM_CODE.Enabled = False
        ddlOffice.Enabled = False
        ddlInsuranceType.Enabled = False
        txtInsuranceVendor.Enabled = False
        txtInsuranceAmt.Enabled = False
        txtInsurancePolNum.Enabled = False
        txtInsuranceStartdate.Enabled = False
        txtInsuranceEnddate.Enabled = False
        ddlFSI.Enabled = False
        txtNoOfTwoWheelerParking.Enabled = False
        txtNoOfCarsParking.Enabled = False
        txtRecurringCost.Enabled = False
        txtAvailablePower.Enabled = False
        txtInvestedArea.Enabled = False
        txtmain1.Enabled = False
        ddlRecommended.Enabled = False
        txtExistMonthRent.Enabled = False
        txtProposedAreaForAgency.Enabled = False
        txtPropAreaOtherChannels.Enabled = False
        txtPropMonthlyRental.Enabled = False
        txtPropsedPerSqftrental.Enabled = False
        txtExistMonthRent.Enabled = False
        ddlBoardAppr.Enabled = False
        rblCostType.Enabled = False
        txtRentPerSqftCarpet.Enabled = False
        txtRentPerSqftBUA.Enabled = False
        txtSeatCost.Enabled = False
        txtSeatCost.Enabled = False
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            GetLocationsbyCity(ddlCity.SelectedValue)
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Private Sub GetTowerbyLoc(ByVal loc As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
        sp2.Command.AddParameter("@LCMID", loc, DbType.String)
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlTower.DataSource = sp2.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select Tower--", "0"))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            GetTowerbyLoc(ddlLocation.SelectedValue)
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Private Sub GetFloorsbyTwr(ByVal tower As String, ByVal cty As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
        sp2.Command.AddParameter("@TWR_ID", tower, DbType.String)
        sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@FLR_CITY_CODE", cty, DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlFloor.DataSource = sp2.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select Floor--", "0"))
        txtFloor.Text = ddlFloor.Items.Count - 1
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            GetFloorsbyTwr(ddlTower.SelectedValue, ddlCity.SelectedValue)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub

    Protected Sub txtESTD_TextChanged(sender As Object, e As EventArgs) Handles txtESTD.TextChanged
        If Integer.TryParse(txtESTD.Text, 0) Then
            txtAge.Text = DateTime.Now.Year - Convert.ToInt32(txtESTD.Text)
        Else
            txtAge.Text = ""
        End If
    End Sub

    Protected Sub ddlRecommended_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRecommended.SelectedIndexChanged
        divRecommanded.Visible = IIf(ddlRecommended.SelectedValue = "1", True, False)
    End Sub

    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    'Private Sub fillReqs()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTY_REQS_FORAPPROVAL")
    '    sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)

    '    Dim ds As New DataSet
    '    ds = sp.GetDataSet
    '    gvReqs.DataSource = ds
    '    gvReqs.DataBind()
    '    Session("Reqs") = ds
    'End Sub
    Private Sub fillReqs()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ZFM_APPR_DETAILS")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvReqs.DataSource = ds
        gvReqs.DataBind()
        Session("Reqs") = ds
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            panel3.Visible = True
        Else
            panel3.Visible = False
        End If
    End Sub

    Protected Sub gvReqs_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvReqs.PageIndexChanging
        gvReqs.PageIndex = e.NewPageIndex()
        gvReqs.DataSource = Session("Reqs")
        gvReqs.DataBind()
    End Sub

    Protected Sub gvReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqs.RowCommand
        Try
            If e.CommandName = "GetProperties" Then
                hdnSno.Value = e.CommandArgument
                'GetProperties(ReqID)
                BindDetails(hdnSno.Value)
            End If
            panel2.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Private Sub GetProperties(ByVal Reqid As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTS_FOR_APPROVAL")
    '    sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '    sp.Command.AddParameter("@REQ_ID", Reqid, DbType.String)

    '    Dim ds As New DataSet
    '    ds = sp.GetDataSet
    '    gvrReqProperties.DataSource = ds
    '    gvrReqProperties.DataBind()
    '    Session("reqDetails") = ds
    '    If ds.Tables.Count > 0 Then
    '        remarks.Visible = True
    '    Else
    '        remarks.Visible = False
    '    End If

    'End Sub

    'Protected Sub gvrReqProperties_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvrReqProperties.PageIndexChanging
    '    gvrReqProperties.PageIndex = e.NewPageIndex()
    '    gvrReqProperties.DataSource = Session("reqDetails")
    '    gvrReqProperties.DataBind()
    'End Sub

    Private Sub BindOfficeType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_OFFICE_TYPE")
        ddlOffice.DataSource = sp3.GetDataSet()
        ddlOffice.DataTextField = "PM_TOO"
        ddlOffice.DataValueField = "PM_OID"
        ddlOffice.DataBind()
        ddlOffice.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    'Protected Sub gvrReqProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvrReqProperties.RowCommand
    '    Try
    '        If e.CommandName = "ViewDetails" Then
    '            panel1.Visible = True
    '            hdnSno.Value = e.CommandArgument
    '            BindDetails(hdnSno.Value)

    '            'bind curr loc details
    '            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTY_CURR_LOC_SUMMARY")
    '            sp.Command.AddParameter("@PROP_ID", hdnSno.Value, DbType.Int32)
    '            Dim ds As New DataSet
    '            ds = sp.GetDataSet()
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                'txtLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
    '                'txtpptsCount.Text = ds.Tables(0).Rows(0).Item("TOTPPTS")
    '                'txtMonth.Text = ds.Tables(0).Rows(0).Item("TOTRENT")
    '                'txtsqft.Text = ds.Tables(0).Rows(0).Item("TOTAREA")
    '            End If

    '            panel2.Visible = True
    '            ' ClientScript.RegisterStartupScript(Me.[GetType](), "none", "ShowPopup()", True)
    '        Else
    '            panel2.Visible = False
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Private Sub BindDetails(ByVal sno As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PRPTY_DETAILS")
        sp.Command.AddParameter("@PROP_ID", sno, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'General Details

            txtReqID.Text = ds.Tables(0).Rows(0).Item("PM_PPT_PM_REQ_ID")
            ddlReqType.ClearSelection()
            ddlReqType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE")).Selected = True
            'ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE") = ddlReqType.SelectedValue
            ddlPprtNature.ClearSelection()
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_NATURE")).Selected = True
            ddlAcqThr.ClearSelection()
            ddlAcqThr.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_ACQ_THR")).Selected = True
            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE")).Selected = True
            GetLocationsbyCity(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
            GetTowerbyLoc(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE"))
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")).Selected = True
            GetFloorsbyTwr(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE"), ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlFloor.ClearSelection()
            ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_FLR_CODE")).Selected = True
            txtToilet.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_TOI")  'IIf(txtToilet.Text = "", 0, txtToilet.Text)
            ddlPropertyType.ClearSelection()
            ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_PPT_TYPE")
            txtPropIDName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NAME")

            txtESTD.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ESTD")  'IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            txtAge.Text = ds.Tables(0).Rows(0).Item("PM_PPT_AGE")   'IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            txtPropDesc.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
            txtSignageLocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SIGN_LOC")

            txtSocityName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SOC_NAME")
            txtlat.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LAT")
            txtlong.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LONG")
            txtOwnScopeWork.Text = ds.Tables(0).Rows(0).Item("PM_PPT_OWN_SCOPE")
            ddlRecommended.ClearSelection()
            ddlRecommended.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED") = True, 1, 2)
            'General Other Details
            txtExistMonthRent.Text = ds.Tables(0).Rows(0).Item("OD_EXISTING_MONTH_RENT")
            txtProposedAreaForAgency.Text = ds.Tables(0).Rows(0).Item("OD_PROPOSED_AREA")
            txtPropAreaOtherChannels.Text = ds.Tables(0).Rows(0).Item("OD_PROPOSED_AREA_OTHER_CHANELS")
            txtPropMonthlyRental.Text = ds.Tables(0).Rows(0).Item("OD_PROP_MONTHLY_RENT")
            txtPropsedPerSqftrental.Text = ds.Tables(0).Rows(0).Item("OD_PROP_SQFT_RENT")
            ddlBoardAppr.ClearSelection()
            ddlBoardAppr.Items.FindByValue(ds.Tables(0).Rows(0).Item("OD_BOARD_APPR")).Selected = True
            BindOfficeType()
            ddlOffice.Items.FindByValue(ds.Tables(0).Rows(0).Item("OFFICE")).Selected = True
            Dim CostTypeOn As String
            CostTypeOn = ds.Tables(0).Rows(0).Item("OD_COST_TYPE")
            rblCostType.Items.FindByValue(CostTypeOn).Selected = True
            If (CostTypeOn = "Seat") Then
                Costype2.Visible = True
                txtSeatCost.Text = ds.Tables(0).Rows(0).Item("OD_SEAT_COST")
            Else
                Costype1.Visible = True
                txtRentPerSqftCarpet.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_CARPET")
                txtRentPerSqftBUA.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_BUA")
            End If

            txtRentPerSqftCarpet.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_CARPET")
            txtRentPerSqftBUA.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_BUA")
            txtSeatCost.Text = ds.Tables(0).Rows(0).Item("OD_SEAT_COST")
            'If ds.Tables(2).Rows.Count > 0 Then
            '    If (ds.Tables(2).Rows(0)("BRD_IMG_NAME").ToString <> "") Then
            '        gvboard.DataSource = ds.Tables(2)
            '        gvboard.DataBind()
            '    Else
            '        gvboard.Visible = False
            '    End If
            'End If

            'Owner Details
            txtownrname.Text = ds.Tables(0).Rows(0).Item("PM_OWN_NAME")
            txtphno.Text = ds.Tables(0).Rows(0).Item("PM_OWN_PH_NO")
            txtPrvOwnName.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_NAME")
            txtPrvOwnPhNo.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_PH_NO")

            txtOwnEmail.Text = ds.Tables(0).Rows(0).Item("PM_OWN_EMAIL")

            'Area Details
            txtCarpetArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA"))
            txtBuiltupArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA"))
            txtCommonArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_COM_AREA"))
            txtRentableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_RENT_AREA"))

            txtUsableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_USABEL_AREA"))
            txtSuperBulArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_SBU_AREA"))
            txtPlotArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_PLOT_AREA"))
            txtCeilingHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT"))

            txtBeamBottomHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT"))
            txtMaxCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_MAX_CAP"))
            txtOptCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_OPT_CAP"))
            txtSeatingCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_SEATING_CAP"))
            ddlFlooringType.ClearSelection()
            ddlFlooringType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE")).Selected = True
            ddlFSI.ClearSelection()
            ddlFSI.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_AR_FSI") = True, 1, 2)
            txtFSI.Text = ds.Tables(0).Rows(0).Item("PM_AR_FSI_RATIO")
            txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")

            'PURCHASE DETAILS
            txtPurPrice.Text = Format(ds.Tables(0).Rows(0).Item("PM_PUR_PRICE"), "0.00")
            txtPurDate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("PM_PUR_DATE"))
            txtMarketValue.Text = Format(ds.Tables(0).Rows(0).Item("PM_PUR_MARKET_VALUE"), "0.00")

            'GOVT DETAILS
            ddlIRDA.ClearSelection()
            If ds.Tables(0).Rows(0).Item("PM_GOV_IRDA") = "" Then
                ddlIRDA.SelectedValue = "0"
            Else
                ddlIRDA.SelectedValue = ds.Tables(0).Rows(0).Item("PM_GOV_IRDA")
            End If
            txtPCcode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PC_CODE")
            txtGovtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PROP_CODE")
            txtUOM_CODE.Text = ds.Tables(0).Rows(0).Item("PM_GOV_UOM_CODE")

            'INSURANCE DETAILS
            ddlInsuranceType.ClearSelection()
            ddlInsuranceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_INS_TYPE")).Selected = True
            txtInsuranceVendor.Text = ds.Tables(0).Rows(0).Item("PM_INS_VENDOR")
            txtInsuranceAmt.Text = Format(ds.Tables(0).Rows(0).Item("PM_INS_AMOUNT"), "0.00")
            txtInsurancePolNum.Text = ds.Tables(0).Rows(0).Item("PM_INS_PNO")

            txtInsuranceStartdate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("PM_INS_START_DT"))
            txtInsuranceEnddate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("PM_INS_END_DT"))

            'If ds.Tables(1).Rows.Count > 0 Then
            '    gvPropdocs.DataSource = ds.Tables(1)
            '    gvPropdocs.DataBind()
            'End If

            txtCompetitorsVicinity.Text = ds.Tables(0).Rows(0).Item("PM_VICINITY")
            txtNoOfTwoWheelerParking.Text = ds.Tables(0).Rows(0).Item("PM_TWO_PARK")
            txtNoOfCarsParking.Text = ds.Tables(0).Rows(0).Item("PM_FOUR_PARK")
            txtRecurringCost.Text = ds.Tables(0).Rows(0).Item("PM_RECUR_COST")
            txtAvailablePower.Text = ds.Tables(0).Rows(0).Item("PM_KWA")
            txtInvestedArea.Text = ds.Tables(0).Rows(0).Item("PM_BASIC_RENT")
            txtmain1.Text = ds.Tables(0).Rows(0).Item("PM_MAINT_CHRG")
            DisableAllFunctions()
        End If
    End Sub

    'Protected Sub gvPropdocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPropdocs.RowCommand
    '    Try
    '        If e.CommandName = "Download" Then
    '            Response.ContentType = ContentType
    '            Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
    '            Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
    '            Response.WriteFile(imgPath)
    '            Response.End()
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'multi approval
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            If txtMultiRemarks.Text <> Nothing Then
                Dim ds As New DataSet
                For Each row As GridViewRow In gvReqs.Rows
                    Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                    Dim lblPropSno As Label = DirectCast(row.FindControl("lblID"), Label)

                    Dim param As SqlParameter() = New SqlParameter(4) {}
                    param(0) = New SqlParameter("@PROP_SNO", lblPropSno.Text)

                    param(2) = New SqlParameter("@AUR_ID", Session("uid"))
                    param(3) = New SqlParameter("@REMARKS", txtMultiRemarks.Text)
                    param(4) = New SqlParameter("@REQ_ID", hdnSno.Value)

                    If chkselect.Checked = True Then
                        param(1) = New SqlParameter("@STA_ID", 4002)
                    Else
                        param(1) = New SqlParameter("@STA_ID", 4003)
                    End If
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_MULTI_PROP_APP_REJ", param)
                Next

                lblmsg.Text = "Property Approved Successfully"
                panel2.Visible = False
                panel3.Visible = False
                txtMultiRemarks.Text = String.Empty
                fillReqs()
            End If
        Catch ex As Exception

        End Try
    End Sub

    'multi rejection
    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            If txtMultiRemarks.Text <> Nothing Then
                Dim ds As New DataSet
                For Each row As GridViewRow In gvReqs.Rows
                    Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                    Dim lblPropSno As Label = DirectCast(row.FindControl("lblID"), Label)

                    Dim param As SqlParameter() = New SqlParameter(4) {}
                    param(0) = New SqlParameter("@PROP_SNO", lblPropSno.Text)
                    param(1) = New SqlParameter("@STA_ID", 4003)
                    param(2) = New SqlParameter("@AUR_ID", Session("uid"))
                    param(3) = New SqlParameter("@REMARKS", txtMultiRemarks.Text)
                    param(4) = New SqlParameter("@REQ_ID", hdnSno.Value)

                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_MULTI_PROP_APP_REJ", param)
                Next

                lblmsg.Text = "Property Rejected Successfully"
                panel2.Visible = False
                txtMultiRemarks.Text = String.Empty
                fillReqs()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearchRequest_Click(sender As Object, e As EventArgs) Handles btnSearchRequest.Click
        If txtFilterText.Text = "" Then
            lblmsg.Visible = True
            lblmsg.Text = "Please enter Property Name/Location/Recommended/Owner Name to search"
        Else
            lblmsg.Visible = False
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_ZFM_APPR_FILTER")
            sp.Command.AddParameter("@ZFM", txtFilterText.Text, DbType.String)

            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvReqs.DataSource = ds
            gvReqs.DataBind()

        End If
    End Sub
End Class
