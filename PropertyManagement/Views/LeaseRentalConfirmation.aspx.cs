﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;
using System.Globalization;

public partial class PropertyManagement_LeaseRentalConfirmation : System.Web.UI.Page
{
    Decimal TotalRent = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uid"].ToString() == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        if (!IsPostBack)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string host = HttpContext.Current.Request.Url.Host;

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 10);
            param[0].Value = Session["UID"];
            param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
            param[1].Value = path;
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
                }
            }


            SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_CHECK_SYS_PREFERENCES_DATES");
            sp1.Command.AddParameter("@TODAY_DATE", System.DateTime.Now, DbType.DateTime);
            int ChkScreen = 0;
            ChkScreen = Convert.ToInt32(sp1.ExecuteScalar());

            if (ChkScreen == 1)
            {
                BindGrid();
            }
            else
            {
                gvItems.DataSource = null;
                gvItems.DataBind();
                Msg.Text = "This window is closed.";
                Div1.Visible = false;
            }
            panel1.Visible = false;
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (txtReqId.Text == "")
        {
            BindGrid();
        }
        else
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA_SEARCH_BY");
            sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.String);
            gvItems.DataSource = sp.GetDataSet();
            gvItems.DataBind();
        }
    }
    protected void txtreset_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtReqId.Text = "";
    }
    private void BindGrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();

    }

    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindGrid();
        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataBind();
        lblMsg.Text = "";
        Msg.Text = "";
    }
    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblMsg.Visible = false;
        Msg.Visible = false;
        txtRemarks.Focus();
        if (e.CommandName == "LeaseRent")
        {
            txtRemarks.Focus();
            DataSet ds;
            lblMsg.Text = string.Empty;
            Msg.Text = string.Empty;
            //txtWithhold.Text = string.Empty;
            ddlMonths.Items.Clear();
            ddlYears.Items.Clear();
            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblLseName = (Label)gvRow.FindControl("lbllname");
            Label lbldisplayname = (Label)gvRow.FindControl("lbldisplayname");
            Label lblRent = (Label)gvRow.FindControl("lblRent");
            Label TotalRentAmt = (Label)gvRow.FindControl("lblTotalRent");
            Label PayTerm = (Label)gvRow.FindControl("lblPayTerm");
            Label lblSertax = (Label)gvRow.FindControl("lblSertax");
            Label startDt = (Label)gvRow.FindControl("lblsdate");
            Label ToDt = (Label)gvRow.FindControl("lblEdate");
            Label lblSNO = (Label)gvRow.FindControl("lblSNO");



            panel1.Visible = true;
            txtCost.Text = String.Format("{0:0.##}", Convert.ToDouble(lblRent.Text));
            txtSerTax.Text = lblSertax.Text;
            txtPaymentTerm.Text = PayTerm.Text;
            PaymentDate.Text = DateTime.Now.ToString("MM/10/yyyy");
            txtLeaseId.Text = lbldisplayname.Text;
            LoadMonth(txtPaymentTerm.Text, startDt.Text);
            DateTime today = DateTime.Now;
            DateTime addonemonth = today.AddMonths(1);
            DateTime addoneyear = today.AddYears(1);
            ddlMonths.SelectedValue = addonemonth.Month.ToString();
            string sMonth = DateTime.Now.ToString("MM");
            LoadYear(startDt.Text, ToDt.Text);
            if (Convert.ToInt32(sMonth) == 12)
            {
                ddlYears.SelectedValue = addoneyear.Year.ToString();
            }
            else
            {
                ddlYears.SelectedValue = DateTime.Now.Year.ToString();
            }
            Session["TotalAmount"] = 0;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_BIND_LANDLORDS_PAYMENT");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@LEASE_ID", lblLseName.Text, DbType.String);
            sp.Command.AddParameter("@SNO", lblSNO.Text, DbType.String);

            gvLandLord.DataSource = sp.GetDataSet();
            gvLandLord.DataBind();
            ds = sp.GetDataSet();
            ddlpay.ClearSelection();
            if (ds.Tables[0].Rows[0]["PM_RENT_PAY_VALID"].ToString() == "") { ddlpay.Items.FindByValue("0").Selected = true; }
            else
            {
                ddlpay.Items.FindByValue(ds.Tables[0].Rows[0]["PM_RENT_PAY_VALID"].ToString()).Selected = true;
            }
            //gvLandLord.Focus();
        }
        else
        {
            panel1.Visible = false;
        }

    }
    //private void LoadMonth()
    //{       
    //    var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
    //    for (int i = 0; i < months.Length - 1; i++)
    //    {
    //        ddlMonths.Items.Add(new ListItem(months[i], (i + 1).ToString()));
    //    }
    //}

    private void LoadMonth(string Pterms, string StartMonth)
    {
        Dictionary<int, string> mnth = new Dictionary<int, string>();
        mnth.Add(1, "January");
        mnth.Add(2, "February");
        mnth.Add(3, "March");
        mnth.Add(4, "April");
        mnth.Add(5, "May");
        mnth.Add(6, "June");
        mnth.Add(7, "July");
        mnth.Add(8, "August");
        mnth.Add(9, "September");
        mnth.Add(10, "October");
        mnth.Add(11, "November");
        mnth.Add(12, "December");

        // Pterms = "Yearly";
        int dt = Convert.ToDateTime(StartMonth).Month;
        DateTime datetimeAd = Convert.ToDateTime(StartMonth);
        if (Pterms == "Quaterly")
        {
            int cnt = mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Key;
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Value, cnt.ToString()));
            for (int i = 0; i < 3; i++)
            {
                datetimeAd = datetimeAd.AddMonths(3);
                ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Value, mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Key.ToString()));
            }
        }

        else if (Pterms == "Monthly")
        {
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            for (int i = 0; i < months.Length - 1; i++)
            {
                ddlMonths.Items.Add(new ListItem(months[i], (i + 1).ToString()));
            }
        }

        else if (Pterms == "HalfYearly")
        {
            int cnt = mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Key;
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Value, cnt.ToString()));
            datetimeAd = datetimeAd.AddMonths(6);
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Value, mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Key.ToString()));
        }
        else
        {
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Value, mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Key.ToString()));
        }
    }

    private void LoadYear(string StartMonth, string EndMonth)
    {
        //int i = DateTime.Now.Year;
        //for (i = i - 1; i <= DateTime.Now.Year + 1; i++)
        //    ddlYears.Items.Add(Convert.ToString(i));
        int i = Convert.ToDateTime(StartMonth).Year;
        int j = Convert.ToDateTime(EndMonth).Year;
        for (; i <= j; i++)
            ddlYears.Items.Add(Convert.ToString(i));
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        txtRemarks.Focus();
        try
        {

            int ValidateCode = 0;
            ValidateCode = Convert.ToInt32(ValidateRent());
            if (ValidateCode == 0)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Rent Payment Already Done For The Selected Criteria";

            }
            else if (ValidateCode == 2)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Please Select The Month Between Lease Start And End Date";
            }
            else if (ValidateCode == 1)
            {
                InsertRecord();
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void InsertRecord()
    {

        try
        {
            foreach (GridViewRow row in gvLandLord.Rows)
            {

                Label LANDLORD_SNO = (Label)row.FindControl("llId");
                Label requisition_Sno = (Label)row.FindControl("requisition_Sno");
                Label lblreqid = (Label)row.FindControl("lblLeaseId");
                Label lblWHAmt = (Label)row.FindControl("lblWHAmt");
                Label lblLLName = (Label)row.FindControl("lblLseName");
                TextBox txtTDSAmt = (TextBox)row.FindControl("txtTDSAmt");
                TextBox txtWithhold = (TextBox)row.FindControl("txtWithhold");
                Label lblLLTotRent = (Label)row.FindControl("lblTLLRent");
                Label lblLLBasicRent = (Label)row.FindControl("lblRentAmt");
                Label lblPayable = (Label)row.FindControl("lblPayable");
                Label lblTotalRent = (Label)row.FindControl("lblTotalRent");
                Label lblDue = (Label)row.FindControl("lblDue");

                SqlParameter[] param = new SqlParameter[16];
                param[0] = new SqlParameter("@LEASEID", lblreqid.Text);
                param[1] = new SqlParameter("@PAY_TERMS", txtPaymentTerm.Text);
                param[2] = new SqlParameter("@MONTH", ddlMonths.SelectedValue);
                param[3] = new SqlParameter("@YEAR", ddlYears.SelectedValue);
                param[4] = new SqlParameter("@PAY_DATE", PaymentDate.Text);
                param[5] = new SqlParameter("@WITHHOLD_AMT", lblWHAmt.Text);
                param[6] = new SqlParameter("@LANDLORD_NAME", lblLLName.Text);
                param[7] = new SqlParameter("@TDS_AMT", txtTDSAmt.Text);
                param[8] = new SqlParameter("@LL_TOTAL_RENT", lblPayable.Text);
                param[9] = new SqlParameter("@LL_BASIC_RENT", lblLLBasicRent.Text);
                param[10] = new SqlParameter("@AUR_ID", Session["UID"].ToString());
                param[11] = new SqlParameter("@REMARKS", txtRemarks.Text);
                param[12] = new SqlParameter("@SNO", LANDLORD_SNO.Text);
                param[13] = new SqlParameter("@DUE_AMT", lblDue.Text);
                param[14] = new SqlParameter("@RPVALID", ddlpay.SelectedValue);
                param[15] = new SqlParameter("@LEASE_SNO", requisition_Sno.Text);
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_INSERT_LL_PAYMENT_DETAILS", param);

                Msg.Visible = true;
                Msg.Text = "Rent Payment Confirmed Successfully";
                txtRemarks.Text = string.Empty;
                txtWithhold.Text = string.Empty;
                lblPayable.Text = "0";
                lblWHAmt.Text = "0";
                panel1.Visible = false;
                BindGrid();
            }


        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void gvLandLord_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvLandLord.PageIndex = e.NewPageIndex;
        gvLandLord.DataBind();
        lblMsg.Text = "";
        Msg.Text = "";
    }

    protected void txtTDSAmt_TextChanged(object sender, EventArgs e)
    {


        //Decimal TDS = Convert.ToDecimal(txtTDS.Text);
        //Decimal WH = Convert.ToDecimal(lblWH.Text);
        //Decimal LLRentAmt = Convert.ToDecimal(lblLLRentAmt.Text);
        //Decimal Due = Convert.ToDecimal(lblDue.Text);

        //Decimal TDSAmt = ((LLRentAmt * TDS) / 100);

        //lblTotal.Text = (LLRentAmt - TDSAmt + Due).ToString();
        //lblPayable.Text = Convert.ToDecimal((LLRentAmt - TDSAmt + Due) - WH).ToString();

        //updatefooter();

        lblMsg.Text = "";
        Msg.Text = "";
        TextBox txt = (TextBox)sender;
        GridViewRow gvRow = (GridViewRow)txt.Parent.Parent;
        TextBox txtTDS = (TextBox)gvRow.FindControl("txtTDSAmt");
        TextBox txtWithhold = (TextBox)gvRow.FindControl("txtWithhold");
        Label lblTotal = (Label)gvRow.FindControl("lblTLLRent");
        Label lblWH = (Label)gvRow.FindControl("lblWHAmt");
        Label lblLLRentAmt = (Label)gvRow.FindControl("lblRentAmt");
        Label lblPayable = (Label)gvRow.FindControl("lblPayable");
        Label lblDue = (Label)gvRow.FindControl("lblDue");
        Double TDS = Convert.ToDouble((txtTDS.Text == "" ? "0" : txtTDS.Text));
        Double WithHold = Convert.ToDouble((txtWithhold.Text == "" ? "0" : txtWithhold.Text));
        Double Servicetax = Convert.ToDouble(txtSerTax.Text);
        Double LLRentAmt = Convert.ToDouble(lblLLRentAmt.Text);
        Double Due = Convert.ToDouble(lblDue.Text);
        Double Total = Convert.ToDouble(lblTotal.Text);
        Double TotPayable = Convert.ToDouble(lblPayable.Text);
        Double TDSAmt = ((LLRentAmt * TDS) / 100);

        Total = LLRentAmt - TDSAmt + Due;
        Double WithholdAmount = ((Total * WithHold) / 100);
        lblWH.Text = WithholdAmount.ToString();


        TotPayable = ((LLRentAmt - TDSAmt + Due) - WithholdAmount);
        Servicetax = ((TotPayable * Servicetax) / 100);
        lblPayable.Text = Convert.ToDecimal(((LLRentAmt - TDSAmt + Due) - WithholdAmount) + Servicetax).ToString();
        updatefooter();
    }
    protected void txtWithhold_TextChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        Msg.Text = "";
        txtRemarks.Focus();
        for (int i = 0; i < gvLandLord.Rows.Count; i++)
        {

            Label lblWHAmt = (Label)gvLandLord.Rows[i].FindControl("lblWHAmt");
            Label lblRentAmt = (Label)gvLandLord.Rows[i].FindControl("lblRentAmt");
            Label lblTotal = (Label)gvLandLord.Rows[i].FindControl("lblTLLRent");
            TextBox txtTDS = (TextBox)gvLandLord.Rows[i].FindControl("txtTDSAmt");
            TextBox txtWithhold = (TextBox)gvLandLord.Rows[i].FindControl("txtWithhold");
            Label lblPayable = (Label)gvLandLord.Rows[i].FindControl("lblPayable");
            Label lblDue = (Label)gvLandLord.Rows[i].FindControl("lblDue");
            Double TDS = Convert.ToDouble((txtTDS.Text == "" ? "0" : txtTDS.Text));
            Double WithHold = Convert.ToDouble((txtWithhold.Text == "" ? "0" : txtWithhold.Text));
            Double RentAmt = Convert.ToDouble(lblRentAmt.Text);
            Double WHAmt = Convert.ToDouble(lblWHAmt.Text);
            Double Due = Convert.ToDouble(lblDue.Text);
            Double Total = Convert.ToDouble(lblTotal.Text);
            Double TotPayable = Convert.ToDouble(lblPayable.Text);
            Double Servicetax = Convert.ToDouble(txtSerTax.Text);

            Double TDSAmt = ((RentAmt * TDS) / 100);
            Total = RentAmt - TDSAmt + Due;

            Double WithholdAmount = ((Total * WithHold) / 100);
            lblWHAmt.Text = WithholdAmount.ToString();

            TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
            Servicetax = ((TotPayable * Servicetax) / 100);
            lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due) - WithholdAmount) + Servicetax).ToString();

        }

        updatefooter();

    }
    protected void txtSerTax_TextChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        Msg.Text = "";
        txtRemarks.Focus();
        for (int i = 0; i < gvLandLord.Rows.Count; i++)
        {

            Label lblWHAmt = (Label)gvLandLord.Rows[i].FindControl("lblWHAmt");
            Label lblRentAmt = (Label)gvLandLord.Rows[i].FindControl("lblRentAmt");
            Label lblTotal = (Label)gvLandLord.Rows[i].FindControl("lblTLLRent");
            TextBox txtTDS = (TextBox)gvLandLord.Rows[i].FindControl("txtTDSAmt");
            TextBox txtWithhold = (TextBox)gvLandLord.Rows[i].FindControl("txtWithhold");
            Label lblPayable = (Label)gvLandLord.Rows[i].FindControl("lblPayable");
            Label lblDue = (Label)gvLandLord.Rows[i].FindControl("lblDue");
            Double TDS = Convert.ToDouble((txtTDS.Text == "" ? "0" : txtTDS.Text));
            Double WithHold = Convert.ToDouble((txtWithhold.Text == "" ? "0" : txtWithhold.Text));
            Double RentAmt = Convert.ToDouble(lblRentAmt.Text);
            Double WHAmt = Convert.ToDouble(lblWHAmt.Text);
            Double Due = Convert.ToDouble(lblDue.Text);
            Double Total = Convert.ToDouble(lblTotal.Text);
            Double TotPayable = Convert.ToDouble(lblPayable.Text);
            Double Servicetax = Convert.ToDouble(txtSerTax.Text);


            Double TDSAmt = ((RentAmt * TDS) / 100);
            Total = RentAmt - TDSAmt + Due;

            Double WithholdAmount = ((Total * WithHold) / 100);
            lblWHAmt.Text = WithholdAmount.ToString();

            TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
            Servicetax = ((TotPayable * Servicetax) / 100);
            lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due) - WithholdAmount) + Servicetax).ToString();

        }

        updatefooter();
    }


    public void updatefooter()
    {
        decimal dblTotalAmount = 0;
        foreach (GridViewRow row in gvLandLord.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lblPayable = (Label)row.FindControl("lblPayable");
                dblTotalAmount += Decimal.Parse(lblPayable.Text);
            }
        }
        Label lblTotalRent = gvLandLord.FooterRow.FindControl("lblTotalRent") as Label;
        lblTotalRent.Text = dblTotalAmount.ToString();
    }

    protected void gvLandLord_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        decimal dblTotalAmount = Convert.ToDecimal(Session["TotalAmount"]);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblLLTotal = (Label)e.Row.FindControl("lblTLLRent");
            Label lblRentAmt = (Label)e.Row.FindControl("lblRentAmt");
            Label lblPayable = (Label)e.Row.FindControl("lblPayable");
            Label lblDue = (Label)e.Row.FindControl("lblDue");
            Double TotPayable = Convert.ToDouble(lblPayable.Text);

            Double Due = Convert.ToDouble(lblDue.Text);
            Double Rent = Convert.ToDouble(lblRentAmt.Text);
            Double Servicetax = Convert.ToDouble(txtSerTax.Text);


            lblLLTotal.Text = (Due + Rent).ToString();
            TotPayable = (Due + Rent);
            TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
            lblPayable.Text = TotPayable.ToString();
            dblTotalAmount += Decimal.Parse(lblPayable.Text);
            Session["TotalAmount"] = dblTotalAmount;
            TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotalPrice = (Label)e.Row.FindControl("lblTotalRent");
            lblTotalPrice.Text = Session["TotalAmount"].ToString();
        }
    }

    public object ValidateRent()
    {
        int ValidateCode = 0;
        string LeaseId = "";
        string requisition_Sno = "";

        foreach (GridViewRow row in gvLandLord.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {

                Label lblreqid = (Label)row.FindControl("lblLeaseId");
                Label requisition = (Label)row.FindControl("requisition_Sno");
                LeaseId = lblreqid.Text;
                requisition_Sno = requisition.Text;
            }
        }
        SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_VALIDATE_LEASE_RENT");
        sp1.Command.AddParameter("@PAY_TERM", txtPaymentTerm.Text, DbType.String);
        sp1.Command.AddParameter("@LEASE_ID", LeaseId, DbType.String);
        sp1.Command.AddParameter("@REQ_SNO", requisition_Sno, DbType.String);
        sp1.Command.AddParameter("@PAY_MONTH", ddlMonths.SelectedValue, DbType.String);
        sp1.Command.AddParameter("@PAY_YEAR", ddlYears.SelectedValue, DbType.String);

        ValidateCode = Convert.ToInt32(sp1.ExecuteScalar());
        return ValidateCode;

    }
    protected void ddlGST_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtRemarks.Focus();
        if (ddlGST.SelectedValue == "Yes")
        {
            divgst.Visible = true;
        }
        else
        {
            divgst.Visible = false;
        }

    }

    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        Msg.Text = "";
        txtRemarks.Focus();
        foreach (GridViewRow row in gvLandLord.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtWithhold = (TextBox)row.FindControl("txtWithhold");
                Label txtttoal = (Label)row.FindControl("lblTotal");
                Label txtWithholdamt = (Label)row.FindControl("lblWHAmt");
                Label lblPayable = (Label)row.FindControl("lblPayable");
                Label lblWHAmt = (Label)row.FindControl("lblWHAmt");

                txtWithhold.Text = "0";
                lblPayable.Text = "0";
                lblWHAmt.Text = "0";
                txtWithhold.Text = Convert.ToInt32("0").ToString();
                //txtttoal.Text = "0";
            }
        }
    }
    protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        Msg.Text = "";
        txtRemarks.Focus();
        foreach (GridViewRow row in gvLandLord.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtWithhold = (TextBox)row.FindControl("txtWithhold");

                Label txtttoal = (Label)row.FindControl("lblTotal");
                Label txtWithholdamt = (Label)row.FindControl("lblWHAmt");
                Label lblPayable = (Label)row.FindControl("lblPayable");
                Label lblWHAmt = (Label)row.FindControl("lblWHAmt");
                txtWithhold.Text = "0";
                txtWithhold.Text = Convert.ToInt32("0").ToString();
                lblPayable.Text = "0";
                lblWHAmt.Text = "0";
                //txtttoal.Text = "0";
            }
        }

    }
}

