<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewPropertyComments_Documents.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmViewPropertyComments_Documents" Title="Property Comments & Document" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>View Property Documents
                            </legend>
                        </fieldset>
                        
                            <form id="form1" class="well" runat="server">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Property Type<span style="color: red;">*</span></label>
                                            <asp:DropDownList ID="ddlPropertyType" CssClass="selectpicker" runat="server" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Property<span style="color: red;">*</span></label>
                                            <asp:DropDownList ID="ddlProperty" CssClass="selectpicker" runat="server" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--  <asp:Label ID="lblgridheading" runat="server" Text="Property Remarks" Visible="false" Font-Size="14"></asp:Label>--%>

                                <div class="clearfix">
                                    
                                        <asp:GridView ID="gvRemarks" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                            AllowPaging="True" PageSize="5" EmptyDataText="No Property Document Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                            Style="font-size: 12px;">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Commented by">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" Text='<%# Eval("ID") %>' Visible="false" runat="server"></asp:Label>
                                                        <asp:Label ID="lblAUR_FIRST_NAME" Text='<%# Eval("AUR_FIRST_NAME") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comments">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCOMMENTS" Text='<%# Eval("COMMENTS") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Commented Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" Text='<%# Eval("COMMENT_DATE") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Document">
                                                    <ItemTemplate>
                                                        <a href='<%=Page.ResolveUrl("~/UploadFiles") %>/<%# Eval("PROPERTY_DOC") %>' target="_blank">
                                                            <%# Eval("PROPERTY_DOC") %>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    
                                </div>
                            </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
