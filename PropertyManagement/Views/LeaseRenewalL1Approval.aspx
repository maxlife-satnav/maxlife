﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeaseRenewalL1Approval.aspx.vb" Inherits="PropertyManagement_Views_LeaseRenewalL1Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

            <%--check box validation--%>
        function validateCheckBoxesMyReq(flag) {
            var gridView = document.getElementById("<%=gvLeases.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    if (flag == "Approve")
                        return true;
                    else
                        return confirm('Are you sure you want to reject this Lease(s)?');
                }
            }
            alert("Please select atleast one Lease");
            return false;
        }
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Approve/Reject Lease Renewal
                            </legend>
                        </fieldset>
                        <form id="form1" class="well" runat="server">
                            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" />
                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Val2" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" />
                            <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="Val3" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" />

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix" style="padding-top: 10px;">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-6 control-label">Search by Propery Name/Code/City/Location<span style="color: red;">*</span></label>

                                            <asp:RequiredFieldValidator ID="rfvTxtEmpId" runat="server" ControlToValidate="txtSearch" Display="None" ErrorMessage="Please Enter Tenant Code/Tenant Name/Property Name"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtSearch" runat="Server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-12">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                                        <asp:Button ID="btnReset" runat="server" CausesValidation="false" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                    </div>
                                </div>
                            </div>

                            <div style="margin-top: 10px">
                                <div class="clearfix">

                                    <asp:GridView ID="gvLeases" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                        AllowPaging="True" PageSize="10" EmptyDataText="No lease(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    Select All
                                                           <input id="chkSelect1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease ID">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblReqID" runat="server" Text='<%#Eval("PM_LES_PM_LR_REQ_ID_DISPLAYNAME")%>' CommandArgument='<%#Eval("PM_LES_SNO")%>' CommandName="GetLeaseDetails"></asp:LinkButton>
                                                    <asp:Label ID="lblLeaseID" runat="server" Visible="false" Text='<%#Eval("PM_LES_PM_LR_REQ_ID")%>'></asp:Label>
                                                    <asp:Label ID="lblLesSno" runat="server" Visible="false" Text='<%#Eval("PM_LES_SNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCode" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="City">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLocation" runat="server" CssClass="clsLabel" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Total Rent">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRent" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LES_TOT_RENT","{0:c2}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created By">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>

                                </div>
                                <asp:HiddenField ID="hdnLSNO" runat="server" />
                                <div id="pnlBulk" class="clearfix" runat="server" style="margin-top: 10px">
                                    <div class="col-md-5 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12">Remark<span style="color: red;">*</span> </label>
                                            <asp:RequiredFieldValidator ID="rfvtxtRemarks" runat="server" ControlToValidate="txtRemarks" Display="None" ErrorMessage="Please Enter Remarks"
                                                ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%"
                                                    runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-8 col-xs-12" style="margin-top: 20px">
                                        <div class="form-group">
                                            <asp:Button ID="btnAppall" Text="Approve" runat="server" Class="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq('Approve');" ValidationGroup="Val2"></asp:Button>
                                            <asp:Button ID="btnRejAll" Text="Reject" runat="server" Class="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq('Reject');" ValidationGroup="Val2"></asp:Button>
                                        </div>
                                    </div>
                                </div>


                                <br />
                                <hr />
                                <h3>Approved / Rejected Requests</h3>
                                <br />
                                <div class="clearfix">

                                    <asp:GridView ID="gvLeasesApprRej" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                        AllowPaging="True" PageSize="5" EmptyDataText="No lease(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>

                                            <asp:TemplateField HeaderText="Lease ID">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblReqID" runat="server" Text='<%#Eval("PM_LES_PM_LR_REQ_ID_DISPLAYNAME")%>' CommandArgument='<%#Eval("PM_LES_SNO")%>' CommandName="GetLeaseDetails"></asp:LinkButton>
                                                    <asp:Label ID="lblLeaseID" runat="server" Visible="false" Text='<%#Eval("PM_LES_PM_LR_REQ_ID")%>'></asp:Label>
                                                    <asp:Label ID="lblLesSno" runat="server" Visible="false" Text='<%#Eval("PM_LES_SNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCode" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="City">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLocation" runat="server" CssClass="clsLabel" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created By">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>

                                </div>


                                <div id="updatepanel" runat="server" visible="false">
                                    <div ba-panel ba-panel-title="Add Lease" ba-panel-class="with-scroll">
                                        <div class="panel">
                                            <div class="panel-heading" style="height: 41px;">
                                                <h3 class="panel-title">View Lease Details <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus " aria-hidden="true"></i></a>
                                                </h3>
                                            </div>
                                            <div class="panel-body" style="padding-right: 10px;">
                                                <div id="AddLeaseDetails" runat="server">
                                                    <div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseLandlordDetails">Landlord Details</a>


                                                                </h4>
                                                            </div>
                                                            <div id="collapseLandlordDetails" class="panel-collapse collapse in">
                                                                <div class="panel-body color">

                                                                    <div class="row">
                                                                        <asp:HiddenField ID="hdnLandlordSNO" runat="server" />
                                                                        <div class="col-md-12 col-sm-3 col-xs-12">
                                                                            <div class="form-group">
                                                                                <asp:GridView ID="gvlandlordItems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                                                    AllowPaging="True" PageSize="10" EmptyDataText="No Landlord Details Found."
                                                                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                                    <PagerSettings Mode="NumericFirstLast" />
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="LSNO" Visible="false">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblsno" runat="server" Text='<%#Eval("PM_LL_SNO")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Landlord Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="lnksurrender" runat="server" Text='<%#Eval("PM_LL_NAME")%>' CommandArgument='<%#Eval("PM_LL_SNO")%>'
                                                                                                    CommandName="GetLandlordDetails"></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Address 1">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblLLAddress1" runat="server" Text='<%#Eval("PM_LL_ADDRESS1")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="PAN No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblLLPAN" runat="server" Text='<%#Eval("PM_LL_PAN")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Rent">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblRentAmount" runat="server" Text='<%# Eval("PM_LL_MON_RENT_PAYABLE", "{0:c2}")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:TextBox ID="txtRentAmount" runat="server" Text='<%# Eval("PM_LL_MON_RENT_PAYABLE")%>'></asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Security Deposit Amount">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblsecamount" runat="server" Text='<%# Eval("PM_LL_SECURITY_DEPOSIT","{0:c2}") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:TextBox ID="txtsecamount" runat="server" Text='<%# Eval("PM_LL_SECURITY_DEPOSIT")%>'></asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                                    <PagerStyle CssClass="pagination-ys" />
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:ValidationSummary ID="ValidationSummary4" runat="server" ForeColor="Red" ValidationGroup="ValLandlord" DisplayMode="List" />
                                                                <div id="Landlord" visible="false" runat="server">
                                                                    <div class="panel-body color">
                                                                        <div class="row">
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Name <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" ControlToValidate="txtName"
                                                                                        ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Landlord1 Name"></asp:RequiredFieldValidator>
                                                                                    <asp:TextBox ID="txtName" Enabled="false" runat="server" CssClass="form-control" TabIndex="30"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Address 1<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtAddress" runat="server" ControlToValidate="txtAddress"
                                                                                        ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Landlord1 Address 1"></asp:RequiredFieldValidator>
                                                                                    <asp:TextBox ID="txtAddress" Enabled="false" runat="server" CssClass="form-control" TabIndex="31"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Address 2</label>
                                                                                    <asp:TextBox ID="txtAddress2" Enabled="false" runat="server" TabIndex="32" CssClass="form-control"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Address 3</label>
                                                                                    <asp:TextBox ID="txtAddress3" Enabled="false" runat="server" CssClass="form-control" TabIndex="33"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>State<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtState" runat="server" ControlToValidate="txtL1State"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord1 State"></asp:RequiredFieldValidator>
                                                                                    <asp:TextBox ID="txtL1State" Enabled="false" runat="server" CssClass="form-control" TabIndex="33"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>City<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                    <asp:DropDownList ID="ddlCity" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="35">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>PIN CODE<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvld1pin" runat="server" ControlToValidate="txtld1Pin"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord1 Pin number"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtld1Pin" runat="server" TargetControlID="txtld1Pin" FilterType="Numbers" ValidChars="0123456789" />
                                                                                    <div onmouseover="Tip('Enter PIN No with maximum length 6')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtld1Pin" Enabled="false" runat="server" CssClass="form-control" TabIndex="36" MaxLength="6"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>PAN No<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtPAN" runat="server" ControlToValidate="txtPAN"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord1 PAN No"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="revtxtPAN" runat="server" ControlToValidate="txtPAN" Display="None" ValidationGroup="Val1"
                                                                                        ErrorMessage="Please Enter Landlord1 Pan number in Alphanumerics only" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                                                                    <asp:RegularExpressionValidator ID="revtxtPAN1" runat="server" ControlToValidate="txtPAN" Display="None" ValidationGroup="Val1"
                                                                                        ErrorMessage="Landlord1 Pan card Minimum length should be 10" ValidationExpression=".{10}.*" />
                                                                                    <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtPAN" Enabled="false" runat="server" CssClass="form-control" TabIndex="37" MaxLength="10"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <%--  <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Service Tax Applicable<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtServiceTaxApplicable" runat="server" ControlToValidate="ddlServiceTaxApplicable"
                                                                                    ValidationGroup="ValLandlord" Display="None" InitialValue="--Select--" ErrorMessage="Please Enter Landlord1 Service Tax Applicable"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlServiceTaxApplicable" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="38" AutoPostBack="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div id="Serv1" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Service Tax<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtServiceTaxlnd" runat="server" ControlToValidate="txtServiceTaxlnd"
                                                                                    ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Landlord1 Service Tax"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtServiceTaxApplicable" runat="server" TargetControlID="txtServiceTaxlnd" FilterMode="ValidChars" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtServiceTaxlnd" runat="server" CssClass="form-control" MaxLength="12" TabIndex="39"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Property Tax Applicable<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvddlPropertyTaxApplicable" runat="server" ControlToValidate="ddlPropertyTaxApplicable"
                                                                                    ValidationGroup="ValLandlord" Display="None" InitialValue="--Select--" ErrorMessage="Please Enter Landlord1 Property Tax Applicable"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlPropertyTaxApplicable" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="40" AutoPostBack="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div id="Property1" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Property Tax<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtPropertyTax" runat="server" ControlToValidate="txtPropertyTax"
                                                                                    ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Landlord1 Property Tax"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtPropertyTax" runat="server" TargetControlID="txtPropertyTax" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtPropertyTax" runat="server" CssClass="form-control" TabIndex="41"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>
                                                                        <div class="row">
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Contact Details<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtContactDetails" runat="server" ControlToValidate="txtContactDetails"
                                                                                        ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Lanlord1 Contact Details"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtContactDetails" runat="server" TargetControlID="txtContactDetails" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox ID="txtContactDetails" Enabled="false" runat="server" CssClass="form-control" TabIndex="42"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Email</label>
                                                                                    <asp:RegularExpressionValidator ID="revldemail" runat="server" ControlToValidate="txtldemail"
                                                                                        ErrorMessage="Please Enter valid Email of Landlord1" Display="None" ValidationGroup="Val1"
                                                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                    <asp:TextBox ID="txtldemail" Enabled="false" runat="server" CssClass="form-control" TabIndex="43" MaxLength="50"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Amount In<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="RFVddlAmountIn" runat="server" ControlToValidate="ddlAmountIn" Display="None" ValidationGroup="ValLandlord"
                                                                                        ErrorMessage="Please Select Amount In" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                    <asp:DropDownList ID="ddlAmountIn" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                        <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                        <asp:ListItem Value="Percentage">Percentage</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Rent Payable</label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtpmonthrent" runat="server" ControlToValidate="txtpmonthrent"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Lanlord1 Monthly Rent Payable"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtpmonthrent" runat="server" TargetControlID="txtpmonthrent" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox ID="txtpmonthrent" Enabled="false" runat="server" CssClass="form-control" MaxLength="15" TabIndex="44"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Security Deposit</label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtpsecdep" runat="server" ControlToValidate="txtpsecdep"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Lanlord1 Security Deposit"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtpsecdep" runat="server" TargetControlID="txtpsecdep" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox ID="txtpsecdep" Enabled="false" runat="server" TabIndex="45" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>PaymentMode<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvddlpaymentmode" runat="server" ControlToValidate="ddlpaymentmode" Display="None" ValidationGroup="ValLandlord"
                                                                                        ErrorMessage="Please Select Payment Mode" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                    <asp:DropDownList ID="ddlpaymentmode" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True" TabIndex="48">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div id="panel1" runat="server">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Bank Name <span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                                                            Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revBankName" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtBankName"
                                                                                            ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtBankName" Enabled="false" runat="server" TabIndex="49" CssClass="form-control"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Account Number<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                                                            Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtAccNo" runat="server" TargetControlID="txtAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtAccNo" Enabled="false" runat="server" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" id="panel2" runat="server">
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Bank Name <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftBank" runat="server" ControlToValidate="txtNeftBank"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="revtxtNeftBank" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtNeftBank"
                                                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtNeftBank" Enabled="false" runat="server" TabIndex="51" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Account Number <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftAccNo" runat="server" ControlToValidate="txtNeftAccNo"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtNeftAccNo" runat="server" TargetControlID="txtNeftAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtNeftAccNo" Enabled="false" runat="server" TabIndex="52" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Branch Name <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftBrnch" runat="server" ControlToValidate="txtNeftBrnch"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="revtxtNeftBrnch" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtNeftBrnch"
                                                                                        ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtNeftBrnch" Enabled="false" runat="server" TabIndex="53" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>IFSC Code <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftIFSC" runat="server" ControlToValidate="txtNeftIFSC"
                                                                                        Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter IFSC Code"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="revtxtNeftIFSC" Display="None" ValidationGroup="Val1"
                                                                                        runat="server" ControlToValidate="txtNeftIFSC" ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtNeftIFSC" Enabled="false" runat="server" TabIndex="54" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 text-right">
                                                                            <asp:Button ID="btnLandlord" Enabled="false" runat="server" ValidationGroup="ValLandlord" CssClass="btn btn-primary custom-button-color " Text="Update"></asp:Button>
                                                                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color " Text="Hide"></asp:Button>
                                                                            <asp:Button ID="btnDelLandlord" Enabled="false" runat="server" CssClass="btn btn-primary custom-button-color " Text="Delete"></asp:Button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class=" panel panel-default" runat="server" id="divspace">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Property/Building Details </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                                <div class="panel-body color">

                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Property Name<span style="color: red;">*</span></label>
                                                                                <asp:TextBox ID="txtPropIDName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Floor and unit/shop numbers proposed to occupy:</label>
                                                                                <asp:TextBox ID="txtshopnumberoccupy" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Age</label>
                                                                                <div>
                                                                                    <asp:TextBox ID="txtAge" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White" Enabled="false"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Built Up Area(Sqft)<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                                    Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                                    Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                <div>
                                                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtBuiltupArea" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Type Of Office</label>
                                                                                <div>
                                                                                    <asp:TextBox ID="txtOfficeType" Enabled="false" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Floor to Ceiling Height(ft)</label>

                                                                                <asp:TextBox ID="txtCeilingHight" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Floor to Beam Bottom Height(ft) </label>
                                                                                <asp:TextBox Enabled="false" ID="txtBeamBottomHight" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>No. Of Toilet Blocks</label>

                                                                                <asp:TextBox ID="txtToilet" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Carpet Area(Sqft)<span style="color: red;">*</span></label>

                                                                                <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Preferred Efficiency(%) </label>

                                                                                <asp:TextBox ID="txtEfficiency" runat="server" CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Term of Lease  (9 years preferable)</label>

                                                                                <asp:TextBox ID="txtTermofLease" runat="server" CssClass="form-control" MaxLength="14" Enabled="False"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Total No. Of Floors</label>
                                                                                <asp:TextBox ID="txtNoofFloors" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Flooring Type</label>
                                                                                <div>
                                                                                    <asp:TextBox ID="TxtFlooringType" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Signage Location</label>
                                                                                <div>
                                                                                    <asp:TextBox ID="txtSignageLocation" runat="server" CssClass="form-control" Enabled="false"
                                                                                        Rows="3" TextMode="Multiline" MaxLength="500" Height="30%"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Rolling Shutter<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlRollingShutter"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Rolling Shutter"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlRollingShutter" runat="server" Enabled="False" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Competitors in Vicinity</label>
                                                                                <asp:TextBox ID="txtCompetitorsVicinity" Enabled="False" runat="server" CssClass="form-control" MaxLength="12"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseCommercials">Commercials</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseCommercials" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Cost Type On<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                                                <asp:RadioButtonList ID="rblCostType" Enabled="false" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                                                    <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                                                    <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                                                </asp:RadioButtonList>
                                                                            </div>
                                                                        </div>
                                                                        <div id="Costype1" runat="server" visible="false">
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Rent Per Sq.ft (On Carpet)<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftCarpet" runat="server" ControlToValidate="txtRentPerSqftCarpet" Display="None" ValidationGroup="Val1"
                                                                                        ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftbetxtRentPerSqftCarpet" runat="server" TargetControlID="txtRentPerSqftCarpet" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox Enabled="false" ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">.</asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Rent Per Sq.ft (On BUA)<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftBUA" runat="server" ControlToValidate="txtRentPerSqftBUA" Display="None" ValidationGroup="Val1"
                                                                                        ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtRentPerSqftBUA" runat="server" TargetControlID="txtRentPerSqftBUA" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox Enabled="false" ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9">.</asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="Costype2" runat="server" visible="false">
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Seat Cost<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                                                        ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtSeatCost" runat="server" TargetControlID="txtSeatCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox Enabled="false" ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Basic Rent<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter Basic Rent"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtInvestedArea" runat="server" TargetControlID="txtInvestedArea" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                    <asp:TextBox Enabled="false" ID="txtInvestedArea" runat="server" CssClass="form-control" MaxLength="15" TabIndex="4" AutoPostBack="true"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Maintenance Charges<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtmain1" runat="server" ControlToValidate="txtmain1"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Maintenance Charges "></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtmain1" runat="server" TargetControlID="txtmain1" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                    <asp:TextBox Enabled="false" ID="txtmain1" runat="server" CssClass="form-control" MaxLength="15" TabIndex="16" AutoPostBack="true"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>
                                                                                    Total Rent (Maintenance Cost + Basic Rent)</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxttotalrent" runat="server" TargetControlID="txttotalrent" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txttotalrent" runat="server" CssClass="form-control" Enabled="false" TabIndex="23"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Service Tax (In %)<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtservicetax" runat="server" ControlToValidate="txtservicetax"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Service Tax "></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtservicetax" runat="server" TargetControlID="txtservicetax" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtservicetax" Enabled="False" runat="server" CssClass="form-control" MaxLength="15" TabIndex="17" AutoPostBack="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Property Tax (In %)<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtproptax" runat="server" ControlToValidate="txtproptax"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Property Tax "></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtproptax" runat="server" TargetControlID="txtproptax" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtproptax" Enabled="False" runat="server" CssClass="form-control" MaxLength="15" TabIndex="18"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Rent Free Period(In Days)<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtRentFreePeriod" runat="server" ControlToValidate="txtRentFreePeriod" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter Rent Free Period"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtRentFreePeriod" runat="server" TargetControlID="txtRentFreePeriod" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtRentFreePeriod" Enabled="False" runat="server" CssClass="form-control" MaxLength="50" TabIndex="7">.</asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Label ID="lblLeaseReqId" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="false"></asp:Label>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseUtilityDetails">Utility/Power Back Up</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseUtilityDetails" class="panel-collapse collapse in" enabled="False">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>DG Set</label>
                                                                                <asp:RequiredFieldValidator ID="rfvddlDgSet" runat="server" ControlToValidate="ddlDgSet" InitialValue="--Select--"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select DG Set"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlDgSet" runat="server" Enabled="False" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="72" AutoPostBack="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                    <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div id="Dgset" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>DG Set Commercials (If Provided by Landlord) Per Unit<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtDgSetPerUnit" runat="server" ControlToValidate="txtDgSetPerUnit"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter DG Set Commercials (If Provided by Landlord) Per Unit"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="FTEtxtDgSetPerUnit" runat="server" TargetControlID="txtDgSetPerUnit" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtDgSetPerUnit" Enabled="False" runat="server" CssClass="form-control" TabIndex="73"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>DG Set Location</label>
                                                                                <asp:TextBox ID="txtDgSetLocation" Enabled="False" runat="server" TabIndex="74" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Space For Servo Stabilizer</label>
                                                                                <asp:TextBox ID="txtSpaceServoStab" Enabled="False" runat="server" CssClass="form-control" TabIndex="75"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Electrical Meter<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvddlElectricalMeter" runat="server" ControlToValidate="ddlElectricalMeter"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Electrical Meter"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlElectricalMeter" Enabled="False" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="76" AutoPostBack="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div id="Meter" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Meter Location<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtMeterLocation" runat="server" ControlToValidate="txtMeterLocation"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Meter Location"></asp:RequiredFieldValidator>
                                                                                <asp:TextBox ID="txtMeterLocation" Enabled="False" runat="server" CssClass="form-control" TabIndex="77"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Earthing Pit</label>
                                                                                <asp:TextBox ID="txtEarthingPit" Enabled="False" runat="server" TabIndex="78" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Available Power (KWA)</label>
                                                                                <asp:TextBox ID="txtAvailablePower" Enabled="False" runat="server" CssClass="form-control" TabIndex="79"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Additional Power Required (KWA)</label>
                                                                                <asp:TextBox ID="txtAdditionalPowerKWA" Enabled="False" runat="server" CssClass="form-control" TabIndex="80"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Power Specification</label>
                                                                                <asp:TextBox ID="txtPowerSpecification" Enabled="False" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Pathways for electrical cable </label>
                                                                                <asp:TextBox ID="txtpathElecCable" Enabled="False" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Proper Grounding And Earthling </label>
                                                                                <asp:TextBox ID="txtgroundEarthling" Enabled="False" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Type of earthing  </label>
                                                                                <asp:TextBox ID="txtearthType" Enabled="False" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lift working (in case of power cut) </label>
                                                                                <asp:TextBox ID="txtLiftPowerCut" Enabled="False" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Transformer ( Location )</label>
                                                                                <asp:TextBox ID="txtTransLocation" Enabled="False" runat="server" CssClass="form-control" TabIndex="80"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Water Connection</label>
                                                                                <asp:TextBox ID="txtWaterConnection" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherServices">Other Services</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOtherServices" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>No Of Two Wheelers Parking</label>
                                                                                <asp:TextBox ID="txtNoOfTwoWheelerParking" Enabled="False" runat="server" CssClass="form-control" TabIndex="82"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>No Of  Cars Parking</label>
                                                                                <asp:TextBox ID="txtNoOfCarsParking" Enabled="False" runat="server" CssClass="form-control" TabIndex="83"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lift</label>

                                                                                <asp:DropDownList ID="ddlLift" runat="server" Enabled="False" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" runat="server" id="divLift" visible="false">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lift Make</label>
                                                                                <asp:TextBox ID="txtMake" Enabled="False" runat="server" CssClass="form-control" TabIndex="87"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lift Capacity</label>
                                                                                <asp:TextBox ID="txtCapacity" Enabled="False" runat="server" CssClass="form-control" TabIndex="88"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Main Staircase (Width)</label>
                                                                                <asp:TextBox ID="txtStairCaseWidth" Enabled="False" runat="server" CssClass="form-control" TabIndex="89"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Fire Staircase(No,Size) </label>
                                                                                <asp:TextBox ID="txtFireStaircase" Enabled="False" runat="server" CssClass="form-control" TabIndex="90"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="row">


                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>AC outdoor space </label>
                                                                                <asp:TextBox ID="txtACOutdoor" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Any existing Acs at location</label>
                                                                                <asp:TextBox ID="anyACSatloc" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Signage / Branding</label>
                                                                                <asp:TextBox ID="txtsignOrbrand" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Fire Exit stairs</label>
                                                                                <asp:TextBox ID="txtFireExitstairs" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Provision for fire fighting system</label>
                                                                                <asp:TextBox ID="txtFireFightingSystem" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lifts available and License</label>
                                                                                <asp:TextBox ID="txtLiftNLicense" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Maintenance agency ( For Lift)</label>
                                                                                <asp:TextBox ID="txtMaintAgencyLift" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDistanceMapping">Distance Mapping</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseDistanceMapping" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Distance From Airport</label>
                                                                                <asp:TextBox ID="txtDistanceFromAirPort" Enabled="False" runat="server" CssClass="form-control" MaxLength="12" TabIndex="84"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Distance From Railway Station</label>
                                                                                <asp:TextBox ID="txtDistanceFromRailwayStation" Enabled="False" runat="server" TabIndex="85" MaxLength="17" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Distance From Bus Stop</label>
                                                                                <asp:TextBox ID="txtDistanceFromBustop" Enabled="False" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseAreaCostDtls">Area & Cost Details</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseAreaCostDtls" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>CTS Number<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtLnumber" runat="server" ControlToValidate="txtLnumber" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter CTS Number"></asp:RequiredFieldValidator>
                                                                                <div onmouseover="Tip('Enter Alphabets,Numbers and some special characters like /-\ with maximum length 50')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtLnumber" Enabled="False" runat="server" CssClass="form-control" MaxLength="50" TabIndex="2"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Entitled Lease Amount<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="txtentitle" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter Entitled Lease Amount"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftbetxtentitle" runat="server" TargetControlID="txtentitle" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtentitle" Enabled="False" runat="server" CssClass="form-control" TabIndex="3">0</asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Security Deposit<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="txtpay"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Security Deposit"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtpay" runat="server" TargetControlID="txtpay" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtpay" Enabled="False" runat="server" CssClass="form-control" TabIndex="5" MaxLength="26"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Security Deposited Months<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="ddlSecurityDepMonths" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Select Property Type" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                <asp:RequiredFieldValidator ID="rfvddlSecurityDepMonths" runat="server" ControlToValidate="ddlSecurityDepMonths" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Select Security Deposited Months" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlSecurityDepMonths" Enabled="False" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="6">
                                                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                                                    <asp:ListItem Value="6">6</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Interior Cost (Approx)</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtInteriorCost" runat="server" TargetControlID="txtInteriorCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtInteriorCost" Enabled="False" runat="server" CssClass="form-control" MaxLength="50" TabIndex="10"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Tenure</label>
                                                                                <asp:DropDownList ID="ddlTenure" Enabled="False" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="24"></asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseBrokegeDetails">Brokerage Details</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseBrokegeDetails" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Amount Of Brokerage Paid</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtbrkamount" runat="server" TargetControlID="txtbrkamount" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtbrkamount" Enabled="false" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Broker Name</label>
                                                                                <asp:TextBox ID="txtbrkname" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Broker PAN Number</label>
                                                                                <asp:RegularExpressionValidator ID="regpanbrk" runat="server" ControlToValidate="txtbrkpan"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Broker Pan number in Alphanumerics only"
                                                                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                                                                <asp:RegularExpressionValidator ID="regExTextBox1" runat="server" ControlToValidate="txtbrkpan"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Broker Pan card Minimum length is 10"
                                                                                    ValidationExpression=".{10}.*" />
                                                                                <asp:TextBox ID="txtbrkpan" Enabled="false" runat="server" TabIndex="68" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Contact Details</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtbrkmob" runat="server" TargetControlID="txtbrkmob" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtbrkmob" Enabled="false" runat="server" CssClass="form-control" TabIndex="69" Width="97%" MaxLength="15"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Broker Email</label>
                                                                                <asp:RegularExpressionValidator ID="revbrkremail" runat="server" ControlToValidate="txtbrkremail"
                                                                                    ErrorMessage="Please Enter valid Email" Display="None" ValidationGroup="Val1"
                                                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                <asp:TextBox ID="txtbrkremail" Enabled="false" runat="server" CssClass="form-control" MaxLength="50" TabIndex="70"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Broker Address</label>
                                                                                <asp:TextBox ID="txtbrkaddr" Enabled="false" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" TabIndex="71" Rows="5"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseCharges">Charges</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseCharges" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Registration Charges<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtregcharges" runat="server" ControlToValidate="txtregcharges" ValidationGroup="Val1" Display="None"
                                                                                    ErrorMessage="Please Enter Registration Charges"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtregcharges" runat="server" TargetControlID="txtregcharges" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtregcharges" Enabled="false" runat="server" CssClass="form-control" TabIndex="11"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Stamp Duty Charges<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtsduty" runat="server" ControlToValidate="txtsduty" ValidationGroup="Val1" Display="None"
                                                                                    ErrorMessage="Please Enter Stamp Duty Charges"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtsduty" runat="server" TargetControlID="txtsduty" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtsduty" Enabled="false" runat="server" CssClass="form-control" MaxLength="12" TabIndex="12"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Furniture & Fixtures Charges</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtfurniture" runat="server" TargetControlID="txtfurniture" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtfurniture" Enabled="false" runat="server" TabIndex="13" MaxLength="17" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Consultancy/Brokerage Charges</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtbrokerage" runat="server" TargetControlID="txtbrokerage" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtbrokerage" Enabled="false" runat="server" CssClass="form-control" TabIndex="14"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Professional Fees</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtpfees" runat="server" TargetControlID="txtpfees" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtpfees" Enabled="false" runat="server" CssClass="form-control" MaxLength="12" TabIndex="15"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>
                                                                                    Other Taxes</label>
                                                                                <cc1:FilteredTextBoxExtender ID="FtetxtOtherTax" runat="server" TargetControlID="txtOtherTax" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox ID="txtOtherTax" Enabled="false" runat="server" TabIndex="13" CssClass="form-control" MaxLength="17"></asp:TextBox>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseLeaseExpenses">Other Expenses</a>


                                                                </h4>
                                                            </div>
                                                            <div id="collapseLeaseExpenses" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">

                                                                        <div class="col-md-12 col-sm-3 col-xs-12">
                                                                            <div class="form-group">

                                                                                <asp:GridView ID="gvLeaseExpences" DataKeyNames="PM_EXP_SNO" runat="server" AutoGenerateColumns="false"
                                                                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Service Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblsername" runat="server" Text='<%# Eval("NAME")%>'></asp:Label>
                                                                                                <asp:Label ID="lblcode" runat="server" Text='<%# Eval("CODE")%>' Visible="false"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Service Provide Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblSPNAME" runat="server" Text='<%# Eval("PM_SP_NAME")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:Label ID="lblspno" runat="server" Text='<%# Eval("PM_SP_SNO")%>' Visible="false"></asp:Label>
                                                                                                <asp:DropDownList ID="ddlServiceProvider" CssClass="form-control selectpicker with-search" data-live-search="true" runat="server">
                                                                                                </asp:DropDownList>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Input Type">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbliptype" runat="server" Text='<%# Eval("PM_EXP_INP_TYPE")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:Label ID="lblipname" runat="server" Text='<%# Eval("PM_EXP_INP_TYPE")%>' Visible="false"></asp:Label>
                                                                                                <asp:DropDownList ID="ddliptype" CssClass="form-control selectpicker with-search" data-live-search="true" runat="server">
                                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                    <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                                    <asp:ListItem Value="Percentage">Percentage(%)</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Component of the Lease Value">
                                                                                            <ItemStyle Width="10%" />
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblCompValue" runat="server" Text='<%# Eval("PM_EXP_COMP_LES_VAL")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:TextBox ID="txtCompValue" CssClass="form-control" runat="server" Text='<%# Eval("PM_EXP_COMP_LES_VAL")%>'></asp:TextBox>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Paid by">
                                                                                            <ItemStyle Width="10%" />
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblPaidbyname" runat="server" Text='<%# Eval("PM_EXP_PAID_BY")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <EditItemTemplate>
                                                                                                <asp:Label ID="lblPaidby" runat="server" Text='<%# Eval("PM_EXP_PAID_BY")%>' Visible="false"></asp:Label>
                                                                                                <asp:DropDownList ID="ddlPaidBy" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                    <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                                    <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                                                </asp:DropDownList>
                                                                                            </EditItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseAgrmntDetails">Agreement Details</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseAgrmntDetails" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Effective Date Of Agreement<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvsdate" runat="server" ControlToValidate="txtsdate"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Effective Date of Agreement"></asp:RequiredFieldValidator>
                                                                                <div class='input-group date' id='effdate'>
                                                                                    <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtsdate" runat="server" Enabled="false" CssClass="form-control" TabIndex="55" AutoPostBack="true"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Expiry Date Of Agreement<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvedate" runat="server" ControlToValidate="txtedate"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Expiry Date of Agreement"></asp:RequiredFieldValidator>
                                                                                <div class='input-group date' id='fromdate'>
                                                                                    <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtedate" Enabled="false" runat="server" CssClass="form-control" TabIndex="56" AutoPostBack="true"> </asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lock in Period (In Months)</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtlock" runat="server" TargetControlID="txtlock" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtlock" Enabled="false" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lease Period (In Years)</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtLeasePeiodinYears" runat="server" TargetControlID="txtLeasePeiodinYears" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtLeasePeiodinYears" Enabled="false" runat="server" CssClass="form-control" TabIndex="58"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Notice Period(In Months)</label>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtNotiePeriod" runat="server" TargetControlID="txtNotiePeriod" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtNotiePeriod" Enabled="false" runat="server" CssClass="form-control" MaxLength="12" TabIndex="59"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Agreement To be Signed By POA<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvddlAgreementbyPOA" runat="server" ControlToValidate="ddlAgreementbyPOA" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Select Agreement To be Signed By POA" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlAgreementbyPOA" Enabled="false" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12 ">
                                                                            <label>Reminder Before<span style="color: red;">*</span></label>
                                                                            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one reminder in Lease Escalation details."
                                                                                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" Display="None" runat="server" ValidationGroup="Val1" />
                                                                            <div class="bootstrap-tagsinput">
                                                                                <asp:CheckBoxList ID="ReminderCheckList" Enabled="false" runat="server" CellPadding="25" CellSpacing="100" RepeatColumns="3" RepeatLayout="Table" RepeatDirection="Vertical" CausesValidation="True" TabIndex="65">
                                                                                    <asp:ListItem Value="365">1 Year </asp:ListItem>
                                                                                    <asp:ListItem Value="60">60 Days</asp:ListItem>
                                                                                    <asp:ListItem Value="10">10 Days</asp:ListItem>
                                                                                    <asp:ListItem Value="2">2 Days</asp:ListItem>
                                                                                    <asp:ListItem Value="210">7 Months </asp:ListItem>
                                                                                    <asp:ListItem Value="30">30 Days</asp:ListItem>
                                                                                    <asp:ListItem Value="7">7 Days</asp:ListItem>
                                                                                    <asp:ListItem Value="1">1 Day</asp:ListItem>
                                                                                    <asp:ListItem Value="90">90 Days</asp:ListItem>
                                                                                    <asp:ListItem Value="20">20 Days</asp:ListItem>
                                                                                    <asp:ListItem Value="3">3 Days</asp:ListItem>
                                                                                </asp:CheckBoxList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Email address</label>
                                                                                <div>
                                                                                    <asp:TextBox ID="txtReminderEmail" Enabled="false" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" MaxLength="1000" TabIndex="63"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="RentRevisionPanel" runat="server" class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseRentRevision">Rent Revision (Variable % for every year)</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseRentRevision" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <asp:Panel ID="Panel10" runat="server"></asp:Panel>
                                                                    <asp:Repeater ID="rpRevision" runat="server">
                                                                        <ItemTemplate>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <asp:Label ID="lblRevYear" runat="server" Text='<%# Eval("RR_Year")%>'> </asp:Label>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtRevision" runat="server" TargetControlID="txtRevision" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox ID="txtRevision" Enabled="false" class="fa-percent" runat="server" CssClass="form-control" MaxLength="12" Text='<%# Eval("RR_Percentage")%>'></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="panPOA" runat="server" visible="false" class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsepanPOA">Power of Attorney Details</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsepanPOA" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Name<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvPOAName" runat="server" ControlToValidate="txtPOAName"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Name of Power of Attorney"></asp:RequiredFieldValidator>
                                                                                <asp:TextBox ID="txtPOAName" Enabled="false" runat="server" CssClass="form-control" TabIndex="32" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Address<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvPOAAddress" runat="server" ControlToValidate="txtPOAAddress"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Address of Power of Attorney"></asp:RequiredFieldValidator>
                                                                                <asp:TextBox ID="txtPOAAddress" Enabled="false" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" Rows="4" TabIndex="33"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Contact Details<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Contact Details of Power of Attorney"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtPOAMobile" runat="server" TargetControlID="txtPOAMobile" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtPOAMobile" Enabled="false" runat="server" CssClass="form-control" MaxLength="12" TabIndex="34"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Email-ID</label>
                                                                                <asp:RegularExpressionValidator ID="revPOAEmail" runat="server" ControlToValidate="txtPOAEmail"
                                                                                    ErrorMessage="Please Enter valid Email of Power of Attorney" Display="None" ValidationGroup="Val1"
                                                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                <asp:TextBox ID="txtPOAEmail" Enabled="false" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="35"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseleaseEscDetails">Lease Escalation Details</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseleaseEscDetails" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Do You Wish To Enter Lease Escalation<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvesc" runat="server" ControlToValidate="ddlesc"
                                                                                    Display="None" ValidationGroup="Val1" InitialValue="--Select--" ErrorMessage="Please Select Lease Escalation"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlesc" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="60">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lease Escalation Type<span style="color: red;">*</span></label>
                                                                                <asp:DropDownList ID="ddlLeaseEscType" Enabled="false" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="61">
                                                                                    <asp:ListItem Value="FLT">Flat Amount</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lease Hold Improvements<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtLeaseHoldImprovements" runat="server" ControlToValidate="txtLeaseHoldImprovements"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Lease Hold Improvements"></asp:RequiredFieldValidator>
                                                                                <asp:TextBox ID="txtLeaseHoldImprovements" Enabled="false" runat="server" TabIndex="62" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lease Comments</label>
                                                                                <div onmouseover="Tip('Enter Comments with maximum 750 Characters')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtComments" Enabled="false" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" MaxLength="1000" TabIndex="63"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Due Diligence certification<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvddlDueDilegence" runat="server" ControlToValidate="ddlDueDilegence" Display="None" ValidationGroup="Val1"
                                                                                    InitialValue="--Select--" ErrorMessage="Please Select Due Diligence certification"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlDueDilegence" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="64">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>







                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOfficeConnectivity">Office connectivity</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOfficeConnectivity" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Telecom - service providers (in the building)</label>
                                                                                <asp:TextBox ID="txtTeleSPB" Enabled="false" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Telecom - service providers (incase first-occupant)</label>
                                                                                <asp:TextBox ID="txtTeleSPFO" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lease line service providers (in the building)</label>
                                                                                <asp:TextBox ID="txtLeaseSPB" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Lease line service providers (incase first-occupant)</label>
                                                                                <asp:TextBox ID="txtLeaseSPFO" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Telecom vendor feasibility report</label>
                                                                                <asp:TextBox ID="txtTeleVenFR" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>IT Vendor feasibility report</label>
                                                                                <asp:TextBox ID="txtITVenFR" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseCivil">Civil</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseCivil" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Existing punning on walls</label>
                                                                                <asp:TextBox ID="txtPunnWall" Enabled="false" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Cracks on external glazing</label>
                                                                                <asp:TextBox ID="txtCracks" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Washrooms /Toilet </label>
                                                                                <asp:TextBox ID="txtWashToilet" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Pantry </label>
                                                                                <asp:TextBox ID="txtPantry" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Drainage line drawings </label>
                                                                                <asp:TextBox ID="txtDrainagelinedrawings" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Window Height</label>
                                                                                <asp:TextBox ID="txtWindowHeight" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Any Water leakage</label>
                                                                                <asp:TextBox ID="txtWaterLeak" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Staircase</label>
                                                                                <asp:TextBox ID="txtStaircase" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>



                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Emergency Exit </label>
                                                                                <asp:TextBox ID="txtEmergExit" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Height Availability </label>
                                                                                <asp:TextBox ID="txtHeightAvailability" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Any additional civil work (whether in case of landlord or Max Life)</label>
                                                                                <asp:TextBox ID="txtAddtnlCivilWork" Enabled="false" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherDetails">Outstanding amounts/ Damages</a>
                                                                </h4>
                                                            </div>
                                                            <div id="Div1" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Any Outstanding amounts/ Damages  </label>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlOutOrDamagesAmount"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Lift"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlOutOrDamagesAmount" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Damages for extended Stay  </label>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlOutOrDamagesAmount"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Lift"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlDamangesExtStay" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Possession hand-over in as is whereis basis  </label>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlPossessionHandover"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Lift"></asp:RequiredFieldValidator>
                                                                                <asp:DropDownList ID="ddlPossessionHandover" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Competitors in Vicinity</label>
                                                                                <asp:TextBox ID="TextBox1" runat="server" Enabled="false" CssClass="form-control" MaxLength="12"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>No.Of Landlords<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtNoLanlords" runat="server" ControlToValidate="txtNoLanlords" InitialValue="0"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter No.Of Landlords"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtNoLanlords" runat="server" TargetControlID="txtNoLanlords" FilterType="Numbers" ValidChars="0123456789" />
                                                                                <asp:TextBox ID="txtNoLanlords" Enabled="false" runat="server" CssClass="form-control" TabIndex="30" AutoPostBack="true"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Office Equipments</label>
                                                                                <asp:TextBox ID="txtOfficeEquipments" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Working Hrs. of Construction </label>
                                                                                <asp:TextBox ID="txtWorkHrs" runat="server" CssClass="form-control" TabIndex="30" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Restriction on usage of common toilet </label>

                                                                                <asp:TextBox ID="txtCommonToilet" runat="server" CssClass="form-control" TabIndex="30" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">





                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Restriction on usage of service lift  </label>

                                                                                <asp:TextBox ID="txtServiceLift" runat="server" CssClass="form-control" TabIndex="30" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Security System   </label>

                                                                                <asp:TextBox ID="txtSecSystem" runat="server" CssClass="form-control" TabIndex="30" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-11">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <asp:Label ID="lblMsgLL" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                                        </asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherDetails">Property Images</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOtherDetails" class="panel-collapse collapse in">
                                                                <div class="panel-body color">
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                                            <div class="form-group">
                                                                                <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                                                </asp:Label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-sm-3 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div id="tblGridDocs" runat="server">
                                                                                    <asp:DataGrid ID="grdDocs" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" DataKeyField="PM_IMG_SNO"
                                                                                        EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                                                        <Columns>
                                                                                            <asp:BoundColumn Visible="False" DataField="PM_IMG_SNO" HeaderText="ID"></asp:BoundColumn>
                                                                                            <asp:BoundColumn DataField="PM_IMG_PATH" HeaderText="Document Name">
                                                                                                <HeaderStyle></HeaderStyle>
                                                                                            </asp:BoundColumn>
                                                                                            <asp:BoundColumn DataField="PM_IMG_CREATED_DT" HeaderText="Document Date">
                                                                                                <HeaderStyle></HeaderStyle>
                                                                                            </asp:BoundColumn>
                                                                                            <asp:ButtonColumn Text="Download" CommandName="Download">
                                                                                                <HeaderStyle></HeaderStyle>
                                                                                            </asp:ButtonColumn>
                                                                                            <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                                                                <HeaderStyle></HeaderStyle>
                                                                                            </asp:ButtonColumn>
                                                                                        </Columns>
                                                                                        <HeaderStyle ForeColor="white" BackColor="Black" />
                                                                                        <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                                                    </asp:DataGrid>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="pnlSingle" runat="server" style="margin-top: 10px">
                                                        <div class="col-md-5 col-sm-8 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="col-md-12">Remarks<span style="color: red;">*</span> </label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtRemarksSingle" runat="server" ControlToValidate="txtRemarksSingle" Display="None" ErrorMessage="Please Enter Remarks"
                                                                    ValidationGroup="Val3"></asp:RequiredFieldValidator>
                                                                <div class="col-md-12">
                                                                    <asp:TextBox ID="txtRemarksSingle" CssClass="form-control" Width="100%" Height="30%"
                                                                        runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-8 col-xs-12" style="margin-top: 20px">
                                                            <div class="form-group">
                                                                <asp:Button ID="btnApprove" Text="Approve" runat="server" Class="btn btn-primary custom-button-color" ValidationGroup="Val3"></asp:Button>
                                                                <asp:Button ID="btnReject" Text="Reject" runat="server" Class="btn btn-primary custom-button-color" ValidationGroup="Val3"></asp:Button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script>
    $('.closeall').click(function () {
        $('.panel-collapse.in')
          .collapse('hide');
    });
    $('.openall').click(function () {
        $('.panel-collapse:not(".in")')
          .collapse('show');
    });
</script>


