﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="PreValidation_Modify.aspx.vb" Inherits="PropertyManagement_Views_PreValidation_Modify" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='/../fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->


</head>

<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Pre - Validation Fact Sheet Modify
                            </legend>
                        </fieldset>
                        <form id="form1" class="well" runat="server">
                            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                            <div class="clearfix">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="clearfix">
                                            <label class="col-md-12 control-label">Search by Property / Lease / Location Name<span style="color: red;">*</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="clearfix">
                                            <div class="col-md-4">
                                                <asp:RequiredFieldValidator ID="rfvEmpID" runat="server" ControlToValidate="txtReqId"
                                                    Display="None" ErrorMessage="Please Search by Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtReqId" runat="Server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn btn-primary custom-button-color" Text="Search"
                                                    ValidationGroup="Val1" />
                                                <asp:Button ID="btnReset" runat="server" OnClick="txtreset_Click" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdnLSNO" runat="server" />
                                <div class="clearfix" style="padding-top: 10px;">

                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                        AllowPaging="True" PageSize="10" EmptyDataText="No Approved Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                        Style="font-size: 12px;" OnPageIndexChanging="gvItems_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("LEASE_ID") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Id">
                                                <ItemTemplate>
                                                    <%--<asp:LinkButton ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_ID")%>' CommandName="Tax"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lblReqID" runat="server" Text='<%#Eval("PM_LR_REQ_ID_DISPLAYNAME")%>' CommandArgument='<%#Eval("PM_LES_SNO")%>'
                                                        CommandName="GetLeaseDetails"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="City">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Rent">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRent" Text='<%#Eval("PM_LES_TOT_RENT","{0:c2}")%>' runat="server" CssClass="lblRent"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>

                                </div>
                                <div id="updatepanel" runat="server" visible="false">
                                    <div class="clearfix">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                    <div ba-panel ba-panel-title="Add Lease" ba-panel-class="with-scroll">
                                        <div id="AddLeaseDetails" runat="server" class="panel">
                                            <div class="panel-heading" style="height: 41px;">
                                                <h3 class="panel-title">View Details
                                                                        <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus " aria-hidden="true"></i></a>
                                                </h3>
                                            </div>
                                            <div class="panel-body" style="padding-right: 10px;">
                                                <div>
                                                    <div class=" panel panel-default" runat="server" id="divspace">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Property/Building Details </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in">
                                                            <div class="panel-body color">
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Property Name<span style="color: red;">*</span></label>
                                                                            <asp:TextBox ID="txtPropIDName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Floor and unit/shop numbers proposed to occupy:</label>
                                                                            <asp:TextBox ID="txtshopnumberoccupy" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Age</label>
                                                                            <div>
                                                                                <asp:TextBox ID="txtAge" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Built Up Area(Sqft)<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                                Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                            <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                                Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                            <div>
                                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtBuiltupArea" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Type Of Office</label>
                                                                            <div>
                                                                                <asp:TextBox ID="txtOfficeType" Enabled="false" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Floor to Ceiling Height(ft)</label>

                                                                            <asp:TextBox ID="txtCeilingHight" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Floor to Beam Bottom Height(ft) </label>
                                                                            <asp:TextBox Enabled="false" ID="txtBeamBottomHight" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>No. Of Toilet Blocks</label>

                                                                            <asp:TextBox ID="txtToilet" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Carpet Area(Sqft)<span style="color: red;">*</span></label>

                                                                            <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Preferred Efficiency(%) </label>

                                                                            <asp:TextBox ID="txtEfficiency" runat="server" CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Term of Lease  (9 years preferable)</label>

                                                                            <asp:TextBox ID="txtTermofLease" runat="server" CssClass="form-control" MaxLength="14" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Total No. Of Floors</label>
                                                                            <asp:TextBox ID="txtNoofFloors" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Flooring Type</label>
                                                                            <div>
                                                                                <asp:TextBox ID="TxtFlooringType" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Signage Location</label>
                                                                            <div>
                                                                                <asp:TextBox ID="txtSignageLocation" runat="server" CssClass="form-control" Enabled="false"
                                                                                    Rows="3" TextMode="Multiline" MaxLength="500" Height="30%"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Rolling Shutter<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlRollingShutter"
                                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Rolling Shutter"></asp:RequiredFieldValidator>
                                                                            <asp:DropDownList ID="ddlRollingShutter" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="true">
                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Competitors in Vicinity</label>
                                                                            <asp:TextBox ID="txtCompetitorsVicinity" runat="server" CssClass="form-control" MaxLength="12" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCommercials">Commercials</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseCommercials" class="panel-collapse collapse in">
                                                            <div class="panel-body color">
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Cost Type On<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType" Display="None" ValidationGroup="Val1"
                                                                                ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                                            <asp:RadioButtonList ID="rblCostType" Enabled="false" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                                                <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                                                <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                                            </asp:RadioButtonList>
                                                                        </div>
                                                                    </div>
                                                                    <div id="Costype1" runat="server" visible="false">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Rent Per Sq.ft (On Carpet)<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftCarpet" runat="server" ControlToValidate="txtRentPerSqftCarpet" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftbetxtRentPerSqftCarpet" runat="server" TargetControlID="txtRentPerSqftCarpet" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox Enabled="false" ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">.</asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Rent Per Sq.ft (On BUA)<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftBUA" runat="server" ControlToValidate="txtRentPerSqftBUA" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtRentPerSqftBUA" runat="server" TargetControlID="txtRentPerSqftBUA" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox Enabled="false" ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9">.</asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="Costype2" runat="server" visible="false">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Seat Cost<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtSeatCost" runat="server" TargetControlID="txtSeatCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                                <asp:TextBox Enabled="false" ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">.</asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Basic Rent<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea" Display="None" ValidationGroup="Val1"
                                                                                ErrorMessage="Please Enter Basic Rent"></asp:RequiredFieldValidator>
                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtInvestedArea" runat="server" TargetControlID="txtInvestedArea" FilterType="Numbers" ValidChars="0123456789." />
                                                                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                <asp:TextBox Enabled="false" ID="txtInvestedArea" runat="server" CssClass="form-control" MaxLength="15" TabIndex="4" AutoPostBack="true"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Maintenance Charges<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtmain1" runat="server" ControlToValidate="txtmain1"
                                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Maintenance Charges "></asp:RequiredFieldValidator>
                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtmain1" runat="server" TargetControlID="txtmain1" FilterType="Numbers" ValidChars="0123456789." />
                                                                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                <asp:TextBox Enabled="false" ID="txtmain1" runat="server" CssClass="form-control" MaxLength="15" TabIndex="16" AutoPostBack="true"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Total Rent (Maintenance Cost + Basic Rent)</label>
                                                                            <cc1:FilteredTextBoxExtender ID="ftetxttotalrent" runat="server" TargetControlID="txttotalrent" FilterType="Numbers" ValidChars="0123456789." />
                                                                            <asp:TextBox ID="txttotalrent" runat="server" CssClass="form-control" Enabled="false" TabIndex="23"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Service Tax (In %)<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtservicetax" runat="server" ControlToValidate="txtservicetax"
                                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Service Tax "></asp:RequiredFieldValidator>
                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtservicetax" runat="server" TargetControlID="txtservicetax" FilterType="Numbers" ValidChars="0123456789." />
                                                                            <asp:TextBox ID="txtservicetax" runat="server" CssClass="form-control" Enabled="true" MaxLength="15" TabIndex="17" AutoPostBack="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Property Tax (In %)<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtproptax" runat="server" ControlToValidate="txtproptax"
                                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Property Tax "></asp:RequiredFieldValidator>
                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtproptax" runat="server" TargetControlID="txtproptax" FilterType="Numbers" ValidChars="0123456789." />
                                                                            <asp:TextBox ID="txtproptax" runat="server" CssClass="form-control" Enabled="true" MaxLength="15" TabIndex="18"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Rent Free Period(In Days)<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtRentFreePeriod" runat="server" ControlToValidate="txtRentFreePeriod" Display="None" ValidationGroup="Val1"
                                                                                ErrorMessage="Please Enter Rent Free Period"></asp:RequiredFieldValidator>
                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtRentFreePeriod" runat="server" TargetControlID="txtRentFreePeriod" FilterType="Numbers" ValidChars="0123456789" />
                                                                            <asp:TextBox ID="txtRentFreePeriod" runat="server" Enabled="true" CssClass="form-control" MaxLength="50" TabIndex="7">.</asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Car Park Charge(Monthly)</label>
                                                                            <asp:TextBox ID="txtcarpark" runat="server" CssClass="form-control" MaxLength="15" TabIndex="17" AutoPostBack="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="clearfix">

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Exit Options</label>
                                                                            <asp:TextBox ID="txtExit" runat="server" CssClass="form-control" MaxLength="15" TabIndex="17" AutoPostBack="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Other Allowances</label>
                                                                            <asp:TextBox ID="txtAllow" runat="server" CssClass="form-control" MaxLength="15" TabIndex="17" AutoPostBack="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Estimated Cost For Additional Power</label>
                                                                            <asp:TextBox ID="txtAddpwr" runat="server" CssClass="form-control" MaxLength="15" TabIndex="17" AutoPostBack="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:Label ID="lblLeaseReqId" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="false"></asp:Label>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUtilityDetails">Utility/Power Back Up</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseUtilityDetails" class="panel-collapse collapse in">
                                                            <div class="panel-body color">
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>DG Set</label>
                                                                            <asp:RequiredFieldValidator ID="rfvddlDgSet" runat="server" ControlToValidate="ddlDgSet" InitialValue="--Select--"
                                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select DG Set"></asp:RequiredFieldValidator>
                                                                            <asp:DropDownList ID="ddlDgSet" runat="server" Enabled="true" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="72" AutoPostBack="true">
                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div id="Dgset" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>DG Set Commercials (If Provided by Landlord) Per Unit<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtDgSetPerUnit" runat="server" ControlToValidate="txtDgSetPerUnit"
                                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter DG Set Commercials (If Provided by Landlord) Per Unit"></asp:RequiredFieldValidator>
                                                                            <cc1:FilteredTextBoxExtender ID="FTEtxtDgSetPerUnit" runat="server" TargetControlID="txtDgSetPerUnit" FilterType="Numbers" ValidChars="0123456789" />
                                                                            <asp:TextBox ID="txtDgSetPerUnit" Enabled="true" runat="server" CssClass="form-control" TabIndex="73"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>DG Set Location</label>
                                                                            <asp:TextBox ID="txtDgSetLocation" Enabled="true" runat="server" TabIndex="74" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Space For Servo Stabilizer</label>
                                                                            <asp:TextBox ID="txtSpaceServoStab" Enabled="true" runat="server" CssClass="form-control" TabIndex="75"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Electrical Meter<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvddlElectricalMeter" runat="server" ControlToValidate="ddlElectricalMeter"
                                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Electrical Meter"></asp:RequiredFieldValidator>
                                                                            <asp:DropDownList ID="ddlElectricalMeter" Enabled="true" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="76" AutoPostBack="true">
                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div id="Meter" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Meter Location<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtMeterLocation" runat="server" ControlToValidate="txtMeterLocation"
                                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Meter Location"></asp:RequiredFieldValidator>
                                                                            <asp:TextBox ID="txtMeterLocation" Enabled="true" runat="server" CssClass="form-control" TabIndex="77"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Earthing Pit</label>
                                                                            <asp:TextBox ID="txtEarthingPit" Enabled="true" runat="server" TabIndex="78" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Available Power (KWA)</label>
                                                                            <asp:TextBox ID="txtAvailablePower" Enabled="true" runat="server" CssClass="form-control" TabIndex="79"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Additional Power Required (KWA)</label>
                                                                            <asp:TextBox ID="txtAdditionalPowerKWA" Enabled="true" runat="server" CssClass="form-control" TabIndex="80"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Power Specification</label>
                                                                            <asp:TextBox ID="txtPowerSpecification" Enabled="true" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Pathways for electrical cable </label>
                                                                            <asp:TextBox ID="txtpathElecCable" Enabled="true" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Proper Grounding And Earthling </label>
                                                                            <asp:TextBox ID="txtgroundEarthling" Enabled="true" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Type of earthing  </label>
                                                                            <asp:TextBox ID="txtearthType" Enabled="true" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lift working (in case of power cut) </label>
                                                                            <asp:TextBox ID="txtLiftPowerCut" Enabled="true" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Transformer ( Location )</label>
                                                                            <asp:TextBox ID="txtTransLocation" Enabled="true" runat="server" CssClass="form-control" TabIndex="80"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Water Connection</label>
                                                                            <asp:TextBox ID="txtWaterConnection" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherServices">Other Services</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOtherServices" class="panel-collapse collapse in">
                                                            <div class="panel-body color">
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>No Of Two Wheelers Parking</label>
                                                                            <asp:TextBox ID="txtNoOfTwoWheelerParking" Enabled="true" runat="server" CssClass="form-control" TabIndex="82"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>No Of  Cars Parking</label>
                                                                            <asp:TextBox ID="txtNoOfCarsParking" Enabled="true" runat="server" CssClass="form-control" TabIndex="83"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lift</label>

                                                                            <asp:DropDownList ID="ddlLift" Enabled="true" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix" runat="server" id="divLift" visible="false">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lift Make</label>
                                                                            <asp:TextBox ID="txtMake" Enabled="true" runat="server" CssClass="form-control" TabIndex="87"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lift Capacity</label>
                                                                            <asp:TextBox ID="txtCapacity" Enabled="true" runat="server" CssClass="form-control" TabIndex="88"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Main Staircase (Width)</label>
                                                                            <asp:TextBox ID="txtStairCaseWidth" Enabled="true" runat="server" CssClass="form-control" TabIndex="89"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Fire Staircase(No,Size) </label>
                                                                            <asp:TextBox ID="txtFireStaircase" Enabled="true" runat="server" CssClass="form-control" TabIndex="90"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="clearfix">


                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>AC outdoor space </label>
                                                                            <asp:TextBox ID="txtACOutdoor" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Any existing Acs at location</label>
                                                                            <asp:TextBox ID="anyACSatloc" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Signage / Branding</label>
                                                                            <asp:TextBox ID="txtsignOrbrand" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Fire Exit stairs</label>
                                                                            <asp:TextBox ID="txtFireExitstairs" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Provision for fire fighting system</label>
                                                                            <asp:TextBox ID="txtFireFightingSystem" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lifts available and License</label>
                                                                            <asp:TextBox ID="txtLiftNLicense" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Maintenance agency ( For Lift)</label>
                                                                            <asp:TextBox ID="txtMaintAgencyLift" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-3 col-xs-12">
                                                                        <div class="form-group">
                                                                            
                                                                                <asp:GridView ID="gvLeaseExpences" DataKeyNames="PM_EXP_SNO" runat="server" AutoGenerateColumns="false"
                                                                                OnRowEditing="EditLeaseExpense" OnRowDataBound="RowDataBound" OnRowUpdating="UpdateLeaseExpense"
                                                                                OnRowCancelingEdit="CancelEdit" CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Service Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblsername" runat="server" Text='<%# Eval("NAME")%>'></asp:Label>
                                                                                            <asp:Label ID="lblcode" runat="server" Text='<%# Eval("CODE")%>' Visible="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Service Provide Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblSPNAME" runat="server" Text='<%# Eval("PM_SP_NAME")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblspno" runat="server" Text='<%# Eval("PM_SP_SNO")%>' Visible="false"></asp:Label>
                                                                                            <asp:DropDownList ID="ddlServiceProvider" CssClass="form-control selectpicker with-search" data-live-search="true" runat="server">
                                                                                            </asp:DropDownList>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Input Type">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lbliptype" runat="server" Text='<%# Eval("PM_EXP_INP_TYPE")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblipname" runat="server" Text='<%# Eval("PM_EXP_INP_TYPE")%>' Visible="false"></asp:Label>
                                                                                            <asp:DropDownList ID="ddliptype" CssClass="form-control selectpicker with-search" data-live-search="true" runat="server">
                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                                <asp:ListItem Value="Percentage">Percentage(%)</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Component of the Lease Value">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCompValue" runat="server" Text='<%# Eval("PM_EXP_COMP_LES_VAL")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:TextBox ID="txtCompValue" CssClass="form-control" runat="server" Text='<%# Eval("PM_EXP_COMP_LES_VAL")%>'></asp:TextBox>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Paid by">
                                                                                        <ItemStyle Width="10%" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblPaidbyname" runat="server" Text='<%# Eval("PM_EXP_PAID_BY")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                        <EditItemTemplate>
                                                                                            <asp:Label ID="lblPaidby" runat="server" Text='<%# Eval("PM_EXP_PAID_BY")%>' Visible="false"></asp:Label>
                                                                                            <asp:DropDownList ID="ddlPaidBy" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                                <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </EditItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:CommandField ShowEditButton="True" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseDistanceMapping">Distance Mapping</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseDistanceMapping" class="panel-collapse collapse in">
                                                            <div class="panel-body color">
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Distance From Airport</label>
                                                                            <asp:TextBox ID="txtDistanceFromAirPort" Enabled="true" runat="server" CssClass="form-control" MaxLength="12" TabIndex="84"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Distance From Railway Station</label>
                                                                            <asp:TextBox ID="txtDistanceFromRailwayStation" Enabled="true" runat="server" TabIndex="85" MaxLength="17" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Distance From Bus Stop</label>
                                                                            <asp:TextBox ID="txtDistanceFromBustop" Enabled="true" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOfficeConnectivity">Office connectivity</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOfficeConnectivity" class="panel-collapse collapse in">
                                                            <div class="panel-body color">
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Telecom - service providers (in the building)</label>
                                                                            <asp:TextBox ID="txtTeleSPB" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Telecom - service providers (incase first-occupant)</label>
                                                                            <asp:TextBox ID="txtTeleSPFO" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lease line service providers (in the building)</label>
                                                                            <asp:TextBox ID="txtLeaseSPB" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lease line service providers (incase first-occupant)</label>
                                                                            <asp:TextBox ID="txtLeaseSPFO" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Telecom vendor feasibility report</label>
                                                                            <asp:TextBox ID="txtTeleVenFR" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>IT Vendor feasibility report</label>
                                                                            <asp:TextBox ID="txtITVenFR" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCivil">Civil</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseCivil" class="panel-collapse collapse in">
                                                            <div class="panel-body color">
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Existing punning on walls</label>
                                                                            <asp:TextBox ID="txtPunnWall" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Cracks on external glazing</label>
                                                                            <asp:TextBox ID="txtCracks" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Washrooms /Toilet </label>
                                                                            <asp:TextBox ID="txtWashToilet" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Pantry </label>
                                                                            <asp:TextBox ID="txtPantry" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Drainage line drawings </label>
                                                                            <asp:TextBox ID="txtDrainagelinedrawings" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Window Height</label>
                                                                            <asp:TextBox ID="txtWindowHeight" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Any Water leakage</label>
                                                                            <asp:TextBox ID="txtWaterLeak" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Staircase</label>
                                                                            <asp:TextBox ID="txtStaircase" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>



                                                                </div>

                                                                <div class="clearfix">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Emergency Exit </label>
                                                                            <asp:TextBox ID="txtEmergExit" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Height Availability </label>
                                                                            <asp:TextBox ID="txtHeightAvailability" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Any additional civil work (whether in case of landlord or Max Life)</label>
                                                                            <asp:TextBox ID="txtAddtnlCivilWork" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix">
                                                        <div class="col-md-11">
                                                            <div class="form-group">
                                                                <div class="clearfix">
                                                                    <asp:Label ID="lblMsgLL" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                                    </asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 text-right">
                                                        <%--<asp:Button ID="btnUpdateLease" runat="server" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color " Text="Update"></asp:Button>--%>
                                                        <asp:Button ID="btnModify" runat="server" CssClass="btn btn-primary custom-button-color " Text="Modify"></asp:Button>
                                                        <%--<asp:Button ID="btnReject" runat="server" CssClass="btn btn-primary custom-button-color " Text="Reject"></asp:Button>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script>
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
    $('.closeall').click(function () {
        $('.panel-collapse.in')
          .collapse('hide');
    });
    $('.openall').click(function () {
        $('.panel-collapse:not(".in")')
          .collapse('show');
    });
</script>
