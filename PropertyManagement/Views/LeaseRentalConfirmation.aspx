﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaseRentalConfirmation.aspx.cs" Inherits="PropertyManagement_LeaseRentalConfirmation" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='/../fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
    <%--<style type="text/css">
        .table-striped {
            height: 161px;
        }
    </style>--%>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Rental Payment
                            </legend>
                        </fieldset>

                        <form id="form1" class="well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />

                            <div class="clearfix" style="padding-top: 10px" id="Div1" runat="server">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-10 control-label">Search By Property / Lease / Location Name<span style="color: red;">*</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-5">
                                                <asp:Button ID="btnsearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                    CausesValidation="true" TabIndex="2" OnClick="btnsearch_Click" />
                                                <asp:Button ID="txtreset" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reset" ValidationGroup="Val1"
                                                    CausesValidation="true" TabIndex="2" OnClick="txtreset_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Msg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="padding-top: 10px;">
                                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" PageSize="50" EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                    Style="font-size: 12px;" OnPageIndexChanging="gvItems_PageIndexChanging" OnRowCommand="gvItems_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcity" runat="server" CssClass="lblcity" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSNO" runat="server" Visible="false" Text='<%#Eval("PM_LES_SNO")  %>'></asp:Label>
                                                <asp:Label ID="lbllname" runat="server" Text='<%#Eval("LEASE_ID") %>'></asp:Label>
                                                <asp:Label ID="lbldisplayname" runat="server" Text='<%#Eval("PM_LR_REQ_ID_DISPLAYNAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Name & Id">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblextend" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_LR_REQ_ID_DISPLAYNAME")%>' CommandName="LeaseRent"
                                                    CommandArgument='<%#Eval("LEASE_ID") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Property Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="Lease Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" CssClass="lblstartdate" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" CssClass="lblExpiryDate" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRentRent" Text='<%#Eval("PM_LES_TOT_RENT","{0:c2}")%>' runat="server" CssClass="lblRentRent"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="Lease Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" CssClass="lbluser" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="Service Tax" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSertax" Text='<%#Eval("PM_LC_SERVICE_TAX")%>' runat="server" CssClass="lblSertax"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Rent" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRent" Text='<%#Eval("PM_LES_TOT_RENT")%>' runat="server" CssClass="lblRent"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Term" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPayTerm" runat="server" CssClass="lbluser" Text='<%#Eval("PM_PT_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>

                            <div class="clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                            <div id="panel1" runat="Server">
                                <div class="clearfix" style="padding-top: 10px">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Lease Id</label>
                                            <asp:TextBox ID="txtLeaseId" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Payment Terms</label>
                                            <asp:TextBox ID="txtPaymentTerm" runat="server" CssClass="form-control" Enabled="false">                                                    
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Months</label>

                                            <asp:RequiredFieldValidator ID="rfvmonth" runat="server" ControlToValidate="ddlMonths"
                                                Display="none" ErrorMessage="Please Select Month" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlMonths" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                ToolTip="--Select--" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Years</label>
                                            <asp:RequiredFieldValidator ID="rfvyear" runat="server" ControlToValidate="ddlYears"
                                                Display="none" ErrorMessage="Please Select Year" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlYears" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                ToolTip="--Select--" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Payment Date</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="PaymentDate"
                                                Display="none" ErrorMessage="Please Select Payment Date" ValidationGroup="Val2">
                                            </asp:RequiredFieldValidator>
                                            <div class='input-group date' id='PayDate'>
                                                <asp:TextBox ID="PaymentDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('PayDate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Rent Amount</label>
                                            <asp:TextBox ID="txtCost" TextMode="Number" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Rental Payment Validation</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlpay"
                                                Display="none" ErrorMessage="Please Select Month" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlpay" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--">
                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                <asp:ListItem Value="0">Release</asp:ListItem>
                                                <asp:ListItem Value="1">Hold</asp:ListItem>
                                                <asp:ListItem Value="2">Stop</asp:ListItem>
                                                <asp:ListItem Value="3">Reduction</asp:ListItem>
                                                <asp:ListItem Value="4">Others</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>GST</label>
                                            <asp:RequiredFieldValidator ID="rfvgst" runat="server" ControlToValidate="ddlGST"
                                             Display="none" ErrorMessage="Please Select GST" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlGST" runat="server" CssClass="form-control selectpicker"  data-live-search="true"
                                              ToolTip="--Select--" OnSelectedIndexChanged="ddlGST_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Value="No">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix" visible="false" id="divgst" runat="server">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>GST (%)</label>
                                            <asp:TextBox ID="txtSerTax" TextMode="Number" min="0" runat="server" CssClass="form-control" Enabled="TRUE" OnTextChanged="txtSerTax_TextChanged" AutoPostBack="True">0</asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">

                                    <asp:GridView ID="gvLandLord" runat="server" AllowPaging="True" AutoGenerateColumns="false" ShowFooter="true"
                                        EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="20"
                                        OnPageIndexChanging="gvLandLord_PageIndexChanging" OnRowDataBound="gvLandLord_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Lease Id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="requisition_Sno" runat="server" Visible="false" Text='<%#Eval("PM_LL_PM_LES_SNO") %>'></asp:Label>
                                                    <asp:Label ID="lblLeaseId" runat="server" Text='<%#Eval("LEASE_ID") %>' CommandName="LandlordRent"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Landlord">
                                                <ItemTemplate>
                                                    <asp:Label ID="llId" runat="server" Visible="false" Text='<%#Eval("SNO") %>'></asp:Label>
                                                    <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("PM_LL_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="City">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCity" Text='<%#Eval("CTY_NAME")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAdd" Text='<%#Eval("PM_LL_ADDRESS1")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rent Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRentAmt" Text='<%#Eval("PM_LL_MON_RENT_PAYABLE")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TDS (%)" Visible="false">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtTDSAmt" TextMode="Number" runat="server" OnTextChanged="txtTDSAmt_TextChanged" AutoPostBack="true" min="0"
                                                        Enabled='<%# Eval("PM_LL_TDS").ToString() == "1" ? true : false %>'>0</asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Due">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDue" Text='<%#Eval("PM_LP_DUE_AMT")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Current Rent Amt (INR) (A)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCurrent" Text='<%#Eval("PM_LL_CUR_RENT")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Maintance Amt (B)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMaintance" Text='<%#Eval("PM_LL_MAINTENANCE")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Other Charges Amt (C)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOther" Text='<%#Eval("PM_LL_CONT_PAY")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="Total Landlord Rent">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTLLRent" Text="0" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:TemplateField HeaderText="Type Of Agreement">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAgr" runat="server" CssClass="lblExpiryDate" Text='<%#Eval("PM_TOO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Withold (%)">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtWithhold" TextMode="Number" runat="server" data-live-search="true" AutoPostBack="True" OnTextChanged="txtWithhold_TextChanged" min="0">0</asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Withhold Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblWHAmt" runat="server">0</asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblTotal" Text="Total" runat="server" Font-Bold="True"></asp:Label>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Payable(A+B+C)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPayable" runat="server">0</asp:Label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lblTotalRent" runat="server" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>

                                </div>


                                <div class="clearfix" style="padding-top: 10px;">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <div onmouseover="Tip('Enter Asset Description')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                        <div class="form-group">
                                            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Pay" CausesValidation="true" ValidationGroup="Val2" OnClick="btnSubmit_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
