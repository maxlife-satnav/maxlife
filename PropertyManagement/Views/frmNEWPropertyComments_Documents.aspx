<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmNEWPropertyComments_Documents.aspx.vb" Inherits="WorkSpace_frmNEWPropertyComments_Documents"
    Title="Property Comments & Document" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Upload Property Document
                            </legend>
                        </fieldset>
                        
                            <form id="form1" class="well" runat="server">
                                <div class="clearfix">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                    </div>
                                </div>
                                
                                <div class="clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Property Type<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvddproptype" ControlToValidate="ddlPropertyType" InitialValue="0"
                                                ErrorMessage="Please Select Property Type" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlPropertyType" CssClass="selectpicker" runat="server" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Property<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvprop" ControlToValidate="ddlProperty" InitialValue="--Select--"
                                                ErrorMessage="Please Select Property" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlProperty" CssClass="selectpicker" runat="server" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Browse Document<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Select Document"
                                                ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="btn btn-default" style="display: block!important;">
                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-right: 10px;">
                                        <div class="form-group">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="clearfix">
                                    
                                        <asp:GridView ID="gvRemarks" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                            OnRowDeleting="gvRemarks_RowDeleting" OnRowCommand="gvRemarks_RowCommand1"
                                            AllowPaging="True" PageSize="5" EmptyDataText="No Records Found" CssClass="table table-condensed table-bordered table-hover table-striped"
                                            Style="font-size: 12px;">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Commented by">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" Text='<%# Eval("ID") %>' Visible="false" runat="server"></asp:Label>
                                                        <asp:Label ID="lblAUR_FIRST_NAME" Text='<%# Eval("AUR_FIRST_NAME") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comments">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCOMMENTS" Text='<%# Eval("COMMENTS") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Commented Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label2" Text='<%# Eval("COMMENT_DATE") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Uploaded Document">
                                                    <ItemTemplate>
                                                        <a href='<%=Page.ResolveUrl("~/UploadFiles") %>/<%# Eval("PROPERTY_DOC") %>' target="_blank">
                                                            <%# Eval("PROPERTY_DOC") %>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" ID="lnkDelete" Text="Delete" CommandArgument='<%# Eval("id") %>'
                                                            CommandName="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    
                                </div>
                            </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    
</body>
</html>
