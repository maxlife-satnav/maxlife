Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper

Partial Class WorkSpace_SMS_Webfiles_frmHRAddLease
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet

    Private Sub BindReqId()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GENERATE_REQUESTID_ADD_LEASE")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDLEASE", DbType.String)
        Dim dt = DateTime.Now.ToString("MMddyyyyHHmm")
        lblLeaseReqId.Text = dt + "/LESREQ/" + Convert.ToString(sp3.ExecuteScalar())
    End Sub

    Private Sub BindOfficeType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_OFFICE_TYPE")
        ddlOffice.DataSource = sp3.GetDataSet()
        ddlOffice.DataTextField = "PM_TOO"
        ddlOffice.DataValueField = "PM_OID"
        ddlOffice.DataBind()
        ddlOffice.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            ClearTextBox(Me)
            AssigTextValues()
            ClearDropdown(Me)
            BindOfficeType()
            '  BindProperty()
            BindState()
            BindCity()
            BindLeaseExpences()
            BindLegalDocuments()
            BindTenure()
            BindPayMode()
            BindReqId()
            lblMsg.Text = ""
            panel1.Visible = False
            panel2.Visible = False
            txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")
            BindRentRevision()
            PropertyDetails(Request("Rid"))

        End If
        txtsdate.Attributes.Add("readonly", "readonly")
        txtedate.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub BindRentRevision()
        Dim rowCount As Integer = 0
        rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
        If rowCount > 1 Then
            RentRevisionPanel.Visible = True
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            For i As Integer = 1 To rowCount - 1
                rr_obj = New RentRevision()
                rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
                rr_obj.RR_Percentage = "0"
                RR_lst.Add(rr_obj)
            Next
            rpRevision.DataSource = RR_lst
            rpRevision.DataBind()
        Else
            RentRevisionPanel.Visible = False
        End If
    End Sub

    'Private Sub BindProperty()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_BIND_PROPERTIES_FOR_LEASE")
    '    sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
    '    ddlproperty.DataSource = sp.GetDataSet()
    '    ddlproperty.DataTextField = "PN_NAME"
    '    ddlproperty.DataValueField = "PM_PPT_SNO"
    '    ddlproperty.DataBind()
    '    ddlproperty.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    'End Sub

    Private Sub BindLeaseExpences()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SERVICE_TYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        gvLeaseExpences.DataSource = sp.GetDataSet()
        gvLeaseExpences.DataBind()
        For i As Integer = 0 To gvLeaseExpences.Rows.Count - 1
            Dim ddlServiceProvider As DropDownList = CType(gvLeaseExpences.Rows(i).FindControl("ddlServiceProvider"), DropDownList)
            ObjSubSonic.Binddropdown(ddlServiceProvider, "PM_GET_SERVICE_PROVIDER", "NAME", "CODE")
        Next
    End Sub

    Private Sub BindTenure()
        ObjSubSonic.Binddropdown(ddlTenure, "PM_GET_PAYMENT_TERMS", "PM_PT_NAME", "PM_PT_SNO")
        ddlTenure.Items.RemoveAt(0)
    End Sub

    Private Sub BindPayMode()
        ObjSubSonic.Binddropdown(ddlpaymentmode, "PM_GET_PAYMENT_MODE", "NAME", "CODE")
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY_BY_STATE_CODE")
        sp.Command.AddParameter("@STELST", ddlState.SelectedValue, DbType.String)
        sp.Command.AddParameter("@MODE", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindState()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_STATE")
        sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
        sp.Command.AddParameter("@MODE", 1, DbType.Int32)
        ddlState.DataSource = sp.GetDataSet()
        ddlState.DataTextField = "STE_NAME"
        ddlState.DataValueField = "STE_CODE"
        ddlState.DataBind()
        ddlState.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub

    Public Sub ClearTextBox(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearTextBox(ctrl)
            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Text = ""
            End If
        Next ctrl
    End Sub

    Public Sub ClearDropdown(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearDropdown(ctrl)
            If TypeOf ctrl Is DropDownList Then
                CType(ctrl, DropDownList).ClearSelection()
            End If
        Next ctrl
    End Sub

    Public Sub AssigTextValues()
        txtInteriorCost.Text = 0
        txtfurniture.Text = 0
        txtbrokerage.Text = 0
        txtpfees.Text = 0
        txttotalrent.Text = 0
        txttotalrent.Text = 0
        txtlock.Text = 0
        txtLeasePeiodinYears.Text = 0
        txtNotiePeriod.Text = 0
        txtAvailablePower.Text = 0
        txtAdditionalPowerKWA.Text = 0
        txtNoOfTwoWheelerParking.Text = 0
        txtNoOfCarsParking.Text = 0
        txtDistanceFromAirPort.Text = 0
        txtDistanceFromRailwayStation.Text = 0
        txtDistanceFromBustop.Text = 0
        txtpmonthrent.Text = 0
        txtpsecdep.Text = 0
        txtbrkamount.Text = 0
        txtbrkamount.Text = 0
        txtbrkmob.Text = 0
        txtNoLanlords.Text = 0
    End Sub

    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            Else
                panel1.Visible = False
                panel2.Visible = False
            End If
        End If
    End Sub

    Public Sub Send_Mail(ByVal MailTo As String, ByVal subject As String, ByVal msg As String, ByVal strRMEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_LEASE_INSERT_AMTMAIL")
        sp.Command.AddParameter("@VC_ID", ID, DbType.String)
        sp.Command.AddParameter("@VC_MSG", msg, DbType.String)
        sp.Command.AddParameter("@VC_MAIL", MailTo, DbType.String)
        sp.Command.AddParameter("@VC_SUB", subject, DbType.String)
        sp.Command.AddParameter("@DT_MAILTIME", Date.Today(), DbType.String)
        sp.Command.AddParameter("@VC_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@VC_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@VC_MAIL_CC", strRMEmail, DbType.String)
        sp.ExecuteScalar()
    End Sub

    'Protected Sub ddlproperty_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproperty.SelectedIndexChanged
    '    If ddlproperty.SelectedIndex > 0 Then
    '        param = New SqlParameter(0) {}
    '        param(0) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar, 200)
    '        param(0).Value = ddlproperty.SelectedValue
    '        ds = New DataSet
    '        ds = ObjSubSonic.GetSubSonicDataSet("BIND_DETAILS_BY_PROP", param)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            txtPropAddedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
    '            txtApprovedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
    '            txtPropCode.Focus()
    '            'txtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_PPT_CODE").ToString()
    '        End If
    '    Else
    '        txtPropAddedBy.Text = ""
    '        txtApprovedBy.Text = ""
    '    End If
    'End Sub

    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub

    'Protected Sub ddlServiceTaxApplicable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceTaxApplicable.SelectedIndexChanged
    '    AddLeaseDetails.Visible = False
    '    Landlord.Visible = True
    '    If ddlServiceTaxApplicable.SelectedValue = "Yes" Then
    '        Serv1.Visible = True
    '        txtServiceTaxlnd.Text = ""
    '        'RentRevisionPanel.Visible = False
    '    Else
    '        Serv1.Visible = False
    '        txtServiceTaxlnd.Text = "0"
    '    End If
    'End Sub

    'Protected Sub ddlPropertyTaxApplicable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPropertyTaxApplicable.SelectedIndexChanged
    '    AddLeaseDetails.Visible = False
    '    Landlord.Visible = True
    '    If ddlPropertyTaxApplicable.SelectedValue = "Yes" Then
    '        Property1.Visible = True
    '        txtPropertyTax.Text = ""
    '    Else
    '        Property1.Visible = False
    '        txtPropertyTax.Text = "0"
    '    End If
    'End Sub

    Protected Sub ddlElectricalMeter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlElectricalMeter.SelectedIndexChanged
        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If
    End Sub

    Protected Sub txtInvestedArea_TextChanged(sender As Object, e As EventArgs) Handles txtInvestedArea.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
        txtpay.Focus()
    End Sub

    Protected Sub txtmain1_TextChanged(sender As Object, e As EventArgs) Handles txtmain1.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
        txtservicetax.Focus()
    End Sub

    'Protected Sub txtservicetax_TextChanged(sender As Object, e As EventArgs) Handles txtservicetax.TextChanged
    '    txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtservicetax.Text = "", 0, txtservicetax.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
    '    txtproptax.Focus()
    'End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim LE_lst As New List(Of LeaseExpenses)()
            Dim LE_obj As New LeaseExpenses()
            For Each row As GridViewRow In gvLeaseExpences.Rows
                LE_obj = New LeaseExpenses()
                Dim lblServiceID As Label = DirectCast(row.FindControl("lblServiceID"), Label)
                Dim ddlServiceProvider As DropDownList = DirectCast(row.FindControl("ddlServiceProvider"), DropDownList)
                Dim ddlInputType As DropDownList = DirectCast(row.FindControl("ddlInputType"), DropDownList)
                Dim txtValue As TextBox = DirectCast(row.FindControl("txtValue"), TextBox)
                Dim ddlPaidBy As DropDownList = DirectCast(row.FindControl("ddlPaidBy"), DropDownList)
                If ddlServiceProvider.SelectedIndex > 0 Then
                    If ddlInputType.SelectedIndex > 0 Then
                        If txtValue.Text <> "" Or txtValue.Text <> String.Empty Then
                            If ddlPaidBy.SelectedIndex > 0 Then
                                LE_obj.PM_EXP_HEAD = lblServiceID.Text
                                LE_obj.PM_EXP_SERV_PROVIDER = ddlServiceProvider.SelectedValue
                                LE_obj.PM_EXP_INP_TYPE = ddlInputType.SelectedValue
                                LE_obj.PM_EXP_LES_VAL = IIf(txtValue.Text = "", 0, txtValue.Text)
                                LE_obj.PM_EXP_PAID_BY = ddlPaidBy.SelectedValue
                                LE_lst.Add(LE_obj)
                            Else
                                lblMsg.Text = "Please select paid by against service provider in Lease Expenses"
                                Exit Sub
                            End If
                        Else
                            lblMsg.Text = "Please enter Component of the lease value against service provider in Lease Expenses"
                            Exit Sub
                        End If
                    Else
                        lblMsg.Text = "Please select input type against service provider in Lease Expenses"
                        Exit Sub
                    End If
                End If
            Next

            Dim param(118) As SqlParameter
            'Area &Cost
            param(0) = New SqlParameter("@CTS_NUMBER", SqlDbType.VarChar)
            param(0).Value = txtLnumber.Text
            param(1) = New SqlParameter("@ENTITLE_LEASE_AMOUNT", SqlDbType.Decimal)
            param(1).Value = IIf(txtentitle.Text = "", 0, txtentitle.Text)
            param(2) = New SqlParameter("@LEASE_RENT", SqlDbType.Decimal)
            param(2).Value = IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)
            param(3) = New SqlParameter("@SECURITY_DEPOSIT", SqlDbType.Decimal)
            param(3).Value = IIf(txtpay.Text = "", 0, txtpay.Text)
            param(4) = New SqlParameter("@SECURITY_DEP_MONTHS", SqlDbType.Int)
            param(4).Value = Convert.ToInt32(ddlSecurityDepMonths.SelectedValue)
            param(5) = New SqlParameter("@RENT_FREE_PRD", SqlDbType.Int)
            param(5).Value = Convert.ToInt32(txtRentFreePeriod.Text)
            param(6) = New SqlParameter("@RENT_SFT_CARPET", SqlDbType.Decimal)
            param(6).Value = IIf(txtRentPerSqftCarpet.Text = "", 0, txtRentPerSqftCarpet.Text)
            param(7) = New SqlParameter("@RENT_SFT_BUA", SqlDbType.Decimal)
            param(7).Value = IIf(txtRentPerSqftBUA.Text = "", 0, txtRentPerSqftBUA.Text)
            param(8) = New SqlParameter("@INTERIORCOST", SqlDbType.Decimal)
            param(8).Value = IIf(txtInteriorCost.Text = "", 0, txtInteriorCost.Text)

            'Charges
            param(9) = New SqlParameter("@REGISTRATION_CHARGES", SqlDbType.Decimal)
            param(9).Value = IIf(txtregcharges.Text = "", 0, txtregcharges.Text)
            param(10) = New SqlParameter("@STAMP_DUTY", SqlDbType.Decimal)
            param(10).Value = IIf(txtsduty.Text = "", 0, txtsduty.Text)
            param(11) = New SqlParameter("@FURNITURE", SqlDbType.Decimal)
            param(11).Value = IIf(txtfurniture.Text = "", 0, txtfurniture.Text)
            param(12) = New SqlParameter("@PROFESSIONAL_FEES", SqlDbType.Decimal)
            param(12).Value = IIf(txtpfees.Text = "", 0, txtpfees.Text)
            param(13) = New SqlParameter("@MAINTENANCE_CHARGES", SqlDbType.Decimal)
            param(13).Value = IIf(txtmain1.Text = "", 0, txtmain1.Text)
            param(14) = New SqlParameter("@SERVICE_TAX", SqlDbType.Decimal)
            param(14).Value = IIf(txtservicetax.Text = "", 0, txtservicetax.Text)
            param(15) = New SqlParameter("@PROPERTY_TAX", SqlDbType.Decimal)
            param(15).Value = IIf(txtproptax.Text = "", 0, txtproptax.Text)
            param(16) = New SqlParameter("@BROKERAGE_AMOUNT_CHARGE", SqlDbType.Decimal)
            param(16).Value = IIf(txtbrokerage.Text = "", 0, txtbrokerage.Text)

            'AGREEMENT DETAILS 
            param(17) = New SqlParameter("@EFF_DOA", SqlDbType.DateTime)
            param(17).Value = Convert.ToDateTime(txtsdate.Text)
            param(18) = New SqlParameter("@EXP_DOA", SqlDbType.DateTime)
            param(18).Value = Convert.ToDateTime(txtedate.Text)
            param(19) = New SqlParameter("@PPT_SNO", SqlDbType.Int)
            'param(19).Value = ddlproperty.SelectedValue
            param(19).Value = Request("Rid")
            param(20) = New SqlParameter("@LOCKINPERIOD", SqlDbType.Int)
            param(20).Value = Convert.ToInt32(txtlock.Text)
            param(21) = New SqlParameter("@LEASEPERIOD", SqlDbType.Int)
            param(21).Value = Convert.ToInt32(txtLeasePeiodinYears.Text)
            param(22) = New SqlParameter("@NOTICEPERIOD", SqlDbType.Int)
            param(22).Value = Convert.ToInt32(txtNotiePeriod.Text)

            'BROKAGE DETAILS
            param(23) = New SqlParameter("@BROKERAGE_AMOUNT", SqlDbType.Decimal)
            param(23).Value = IIf(txtbrkamount.Text = "", 0, txtbrkamount.Text)
            param(24) = New SqlParameter("@BROKER_NAME", SqlDbType.VarChar)
            param(24).Value = txtbrkname.Text
            param(25) = New SqlParameter("@BROKER_ADDR", SqlDbType.VarChar)
            param(25).Value = txtbrkaddr.Text
            param(26) = New SqlParameter("@BROKER_PAN", SqlDbType.VarChar)
            param(26).Value = txtbrkpan.Text
            param(27) = New SqlParameter("@BROKER_EMAIL", SqlDbType.VarChar)
            param(27).Value = txtbrkremail.Text
            param(28) = New SqlParameter("@BROKER_CONTACT", SqlDbType.VarChar)
            param(28).Value = txtbrkmob.Text

            'UTILITY/POWER BACKUP DETAILS
            param(29) = New SqlParameter("@DGSET", SqlDbType.VarChar)
            param(29).Value = ddlDgSet.SelectedValue
            param(30) = New SqlParameter("@DGSET_COM_UNIT", SqlDbType.VarChar)
            param(30).Value = txtDgSetPerUnit.Text
            param(31) = New SqlParameter("@DGSET_LOCATION", SqlDbType.VarChar)
            param(31).Value = txtDgSetLocation.Text
            param(32) = New SqlParameter("@SPACE_SERVO_STAB", SqlDbType.VarChar)
            param(32).Value = txtSpaceServoStab.Text
            param(33) = New SqlParameter("@ELEC_METER", SqlDbType.VarChar)
            param(33).Value = ddlElectricalMeter.SelectedValue
            param(34) = New SqlParameter("@METER_LOC", SqlDbType.VarChar)
            param(34).Value = txtMeterLocation.Text
            param(35) = New SqlParameter("@EARTHING_PIT", SqlDbType.VarChar)
            param(35).Value = txtEarthingPit.Text
            param(36) = New SqlParameter("@AVAIL_POWER", SqlDbType.Float)
            param(36).Value = IIf(txtAvailablePower.Text = "", 0, txtAvailablePower.Text)
            param(37) = New SqlParameter("@ADDITIONAL_POWER", SqlDbType.Float)
            param(37).Value = IIf(txtAdditionalPowerKWA.Text = "", 0, txtAdditionalPowerKWA.Text)
            param(38) = New SqlParameter("@POWER_SPEC", SqlDbType.VarChar)
            param(38).Value = txtPowerSpecification.Text

            'OTHER SERVICES
            param(39) = New SqlParameter("@TWO_WHL_PARK", SqlDbType.Int)
            param(39).Value = Convert.ToInt32(txtNoOfTwoWheelerParking.Text)
            param(40) = New SqlParameter("@CAR_PARK", SqlDbType.Int)
            param(40).Value = Convert.ToInt32(txtNoOfCarsParking.Text)
            param(41) = New SqlParameter("@AIRPRT_DIST", SqlDbType.Float)
            param(41).Value = IIf(txtDistanceFromAirPort.Text = "", 0, txtDistanceFromAirPort.Text)
            param(42) = New SqlParameter("@RAIL_DIST", SqlDbType.Float)
            param(42).Value = IIf(txtDistanceFromRailwayStation.Text = "", 0, txtDistanceFromRailwayStation.Text)
            param(43) = New SqlParameter("@RAIL_BUSTOP", SqlDbType.Float)
            param(43).Value = IIf(txtDistanceFromBustop.Text = "", 0, txtDistanceFromBustop.Text)

            'LEASE ESCALATION DETAILS   
            param(44) = New SqlParameter("@OPTION_LEASE_ESC", SqlDbType.VarChar)
            param(44).Value = ddlesc.SelectedValue
            param(45) = New SqlParameter("@ESC_TYPE", SqlDbType.VarChar)
            param(45).Value = ddlLeaseEscType.SelectedValue
            param(46) = New SqlParameter("@LEASEHOLD_IMPROVEMENTS", SqlDbType.VarChar)
            param(46).Value = txtLeaseHoldImprovements.Text
            param(47) = New SqlParameter("@LEASE_COMMENTS", SqlDbType.VarChar)
            param(47).Value = txtComments.Text
            param(48) = New SqlParameter("@DUE_DILIGENCE", SqlDbType.VarChar)
            param(48).Value = ddlDueDilegence.SelectedValue
            Dim selectedItems As String = [String].Join(",", ReminderCheckList.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
            param(49) = New SqlParameter("@REMINDER_BEFORE", SqlDbType.VarChar)
            param(49).Value = selectedItems

            'Rent Revision
            param(50) = New SqlParameter("@TOTAL_RENT", SqlDbType.Decimal)
            param(50).Value = IIf(txttotalrent.Text = "", 0, txttotalrent.Text)
            param(51) = New SqlParameter("@TENURE", SqlDbType.VarChar)
            param(51).Value = ddlTenure.SelectedValue

            'Other Details
            param(52) = New SqlParameter("@COMPETE_VISCINITY", SqlDbType.VarChar)
            param(52).Value = txtCompetitorsVicinity.Text
            param(53) = New SqlParameter("@ROLLING_SHUTTER", SqlDbType.VarChar)
            param(53).Value = ddlRollingShutter.SelectedValue
            param(54) = New SqlParameter("@OFFCIE_EQUIPMENTS", SqlDbType.VarChar)
            param(54).Value = txtOfficeEquipments.Text

            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas

            If fu1.PostedFiles IsNot Nothing Then
                For Each File In fu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            param(55) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(55).Value = UtilityService.ConvertToDataTable(Imgclass)
            param(56) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(56).Value = Session("Uid").ToString
            param(57) = New SqlParameter("@PPT_CODE", SqlDbType.VarChar)
            'param(57).Value = txtPropCode.Text
            param(57).Value = ""
            param(58) = New SqlParameter("@POA_SIGNED", SqlDbType.VarChar)
            param(58).Value = ddlAgreementbyPOA.SelectedValue
            param(59) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(59).Value = lblLeaseReqId.Text

            'POA Details
            param(60) = New SqlParameter("@POA_NAME", SqlDbType.VarChar)
            param(60).Value = txtPOAName.Text
            param(61) = New SqlParameter("@POA_ADDRESS", SqlDbType.VarChar)
            param(61).Value = txtPOAAddress.Text
            param(62) = New SqlParameter("@POA_MOBILE", SqlDbType.VarChar)
            param(62).Value = txtPOAMobile.Text
            param(63) = New SqlParameter("@POA_EMAIL", SqlDbType.VarChar)
            param(63).Value = txtPOAEmail.Text

            'Rent Revision
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            For Each i As RepeaterItem In rpRevision.Items
                rr_obj = New RentRevision()
                Dim txtRevision As TextBox = DirectCast(i.FindControl("txtRevision"), TextBox)
                Dim lblRevYear As Label = DirectCast(i.FindControl("lblRevYear"), Label)
                If txtRevision IsNot Nothing Then
                    rr_obj.RR_Year = lblRevYear.Text
                    rr_obj.RR_Percentage = IIf(txtRevision.Text = "", 0, txtRevision.Text)
                    RR_lst.Add(rr_obj)
                End If
            Next
            param(64) = New SqlParameter("@REVISIONLIST", SqlDbType.Structured)
            param(64).Value = UtilityService.ConvertToDataTable(RR_lst)

            'param(65) = New SqlParameter("@LEASEEXPENSESLIST", SqlDbType.Structured)
            'param(65).Value = UtilityService.ConvertToDataTable(LE_lst)
            param(65) = New SqlParameter("@LEASEEXPENSESLIST", SqlDbType.VarChar)
            param(65).Value = ""
            param(66) = New SqlParameter("@COST_TYPE", SqlDbType.VarChar)
            param(66).Value = rblCostType.SelectedValue
            param(67) = New SqlParameter("@SEAT_COST", SqlDbType.Decimal)
            param(67).Value = IIf(txtSeatCost.Text = "", 0, txtSeatCost.Text)

            param(68) = New SqlParameter("@NO_OF_LL", SqlDbType.Int)
            param(68).Value = Convert.ToInt32(txtNoLanlords.Text)


            'Charges section extra parameter

            param(69) = New SqlParameter("@OTHER_TAX", SqlDbType.Decimal)
            param(69).Value = IIf(txtOtherTax.Text = "", 0, txtOtherTax.Text)

            param(70) = New SqlParameter("@EMAIL_ADDRESS", SqlDbType.VarChar)
            param(70).Value = txtReminderEmail.Text

            SaveDocs()

            param(71) = New SqlParameter("@LEGAL_DOCS", SqlDbType.Structured)
            param(71).Value = UtilityService.ConvertToDataTable(legalDocs)


            'Office Connectivity Section

            param(72) = New SqlParameter("@PM_OC_TEL_SP_BUILDING", SqlDbType.VarChar)
            param(72).Value = txtTeleSPB.Text

            param(73) = New SqlParameter("@PM_OC_TEL_SP_FIRST_OCCUPANT", SqlDbType.VarChar)
            param(73).Value = txtTeleSPFO.Text

            param(74) = New SqlParameter("@PM_OC_LEASE_LINE_SP_BUILD", SqlDbType.VarChar)
            param(74).Value = txtLeaseSPB.Text

            param(75) = New SqlParameter("@PM_OC_LEASE_LINE_SP_FIRST_OCCPNT", SqlDbType.VarChar)
            param(75).Value = txtLeaseSPFO.Text

            param(76) = New SqlParameter("@PM_OC_TEL_VEN_FEASIBILITY", SqlDbType.VarChar)
            param(76).Value = txtTeleVenFR.Text

            param(77) = New SqlParameter("@PM_OC_IT_VEN_FEASIBILITY", SqlDbType.VarChar)
            param(77).Value = txtITVenFR.Text

            'Civil

            param(78) = New SqlParameter("@PM_C_PUNNG_WALLS", SqlDbType.VarChar)
            param(78).Value = txtPunnWall.Text

            param(79) = New SqlParameter("@PM_C_CRACKS", SqlDbType.VarChar)
            param(79).Value = txtCracks.Text

            param(80) = New SqlParameter("@PM_C_TOILET", SqlDbType.VarChar)
            param(80).Value = txtWashToilet.Text

            param(81) = New SqlParameter("@PM_C_PANTRY", SqlDbType.VarChar)
            param(81).Value = txtPantry.Text

            param(82) = New SqlParameter("@PM_C_DRAINAGE_DRAWNGS", SqlDbType.VarChar)
            param(82).Value = txtDrainagelinedrawings.Text

            param(83) = New SqlParameter("@PM_C_WINDOW_HEIGHT", SqlDbType.VarChar)
            param(83).Value = txtWindowHeight.Text

            param(84) = New SqlParameter("@PM_C_WATER_LEAKAGE", SqlDbType.VarChar)
            param(84).Value = txtWaterLeak.Text

            param(85) = New SqlParameter("@PM_C_STAIRCASE", SqlDbType.VarChar)
            param(85).Value = txtStaircase.Text

            param(86) = New SqlParameter("@PM_C_EMRG_EXIT", SqlDbType.VarChar)
            param(86).Value = txtEmergExit.Text

            param(87) = New SqlParameter("@PM_C_HEIGH_AVAILABILITY", SqlDbType.VarChar)
            param(87).Value = txtHeightAvailability.Text

            param(88) = New SqlParameter("@PM_C_ADD_CIVIL_WORK", SqlDbType.VarChar)
            param(88).Value = txtAddtnlCivilWork.Text


            'Outstanding amounts / Damages

            param(89) = New SqlParameter("@PM_D_ANY_DAMAGES", SqlDbType.VarChar)
            param(89).Value = ddlOutOrDamagesAmount.SelectedValue

            param(90) = New SqlParameter("@PM_D_EXTND_STAY", SqlDbType.VarChar)
            param(90).Value = ddlDamangesExtStay.SelectedValue

            param(91) = New SqlParameter("@PM_D_HAND_OVER", SqlDbType.VarChar)
            param(91).Value = ddlPossessionHandover.SelectedValue

            param(92) = New SqlParameter("@PM_D_COMP_VICINITY", SqlDbType.VarChar)
            param(92).Value = txtCompetitorsVicinity.Text

            param(93) = New SqlParameter("@PM_D_OFFC_EQUIPMNTS", SqlDbType.VarChar)
            param(93).Value = txtOfficeEquipments.Text

            param(94) = New SqlParameter("@PM_D_WRKNG_HRS_CONSTRUCTION", SqlDbType.VarChar)
            param(94).Value = txtWorkHrs.Text

            param(95) = New SqlParameter("@PM_D_USAGE_TOILET", SqlDbType.VarChar)
            param(95).Value = txtCommonToilet.Text

            param(96) = New SqlParameter("@PM_D_USAGE_LIFT", SqlDbType.VarChar)
            param(96).Value = txtServiceLift.Text

            param(97) = New SqlParameter("@PM_D_SECURITY_SYSTEM", SqlDbType.VarChar)
            param(97).Value = txtSecSystem.Text

            'utility power extra fields start

            param(98) = New SqlParameter("@PM_LUP_PATH_ELEC_CABLE", SqlDbType.VarChar)
            param(98).Value = txtpathElecCable.Text

            param(99) = New SqlParameter("@PM_LUP_GROUND_EARTH", SqlDbType.VarChar)
            param(99).Value = txtgroundEarthling.Text

            param(100) = New SqlParameter("@PM_LUP_EARTH_TYPE", SqlDbType.VarChar)
            param(100).Value = txtearthType.Text

            param(101) = New SqlParameter("@PM_LUP_LIFT_POWERCUT", SqlDbType.VarChar)
            param(101).Value = txtLiftPowerCut.Text

            param(102) = New SqlParameter("@PM_LUP_TRANSFORMER_LOCATION", SqlDbType.VarChar)
            param(102).Value = txtTransLocation.Text

            param(103) = New SqlParameter("@PM_LUP_WATER_CONNECTION", SqlDbType.VarChar)
            param(103).Value = txtWaterConnection.Text
            'utility power extra fields end


            'other service extra fields start
            param(104) = New SqlParameter("@PM_LO_LIFT_YES_NO", SqlDbType.VarChar)
            param(104).Value = ddlLift.Text

            param(105) = New SqlParameter("@PM_LO_MAKE", SqlDbType.VarChar)
            param(105).Value = txtMake.Text

            param(106) = New SqlParameter("@PM_LO_CAPACITY", SqlDbType.VarChar)
            param(106).Value = txtCapacity.Text

            param(107) = New SqlParameter("@PM_LO_MAIN_STAIRCASE_WIDTH", SqlDbType.VarChar)
            param(107).Value = txtStairCaseWidth.Text

            param(108) = New SqlParameter("@PM_LO_FIRE_STAIRCASE", SqlDbType.VarChar)
            param(108).Value = txtFireStaircase.Text

            param(109) = New SqlParameter("@PM_LO_AC_OUTDOOR_SPACE", SqlDbType.VarChar)
            param(109).Value = txtACOutdoor.Text

            param(110) = New SqlParameter("@PM_LO_ACS_AT_LOCATION", SqlDbType.VarChar)
            param(110).Value = anyACSatloc.Text

            param(111) = New SqlParameter("@PM_LO_SIGNAGE_BRANDING", SqlDbType.VarChar)
            param(111).Value = txtsignOrbrand.Text

            param(112) = New SqlParameter("@PM_LO_FIRE_EXIT_STAIRS", SqlDbType.VarChar)
            param(112).Value = txtFireExitstairs.Text

            param(113) = New SqlParameter("@PM_LO_FIRE_FIGHTING_SYSTEM", SqlDbType.VarChar)
            param(113).Value = txtFireFightingSystem.Text

            param(114) = New SqlParameter("@PM_LO_LIFTS_AVAILABLE_LICENSE", SqlDbType.VarChar)
            param(114).Value = txtLiftNLicense.Text

            param(115) = New SqlParameter("@PM_LO_MAINT_AGENCY_LIFT", SqlDbType.VarChar)
            param(115).Value = txtMaintAgencyLift.Text

            'other service extra fields end

            'property or building section input fields start
            param(116) = New SqlParameter("@PM_SHOP_NUMBER_TO_OCCUPY", SqlDbType.VarChar)
            param(116).Value = txtshopnumberoccupy.Text

            param(117) = New SqlParameter("@PM_TERMS_OF_LEASE", SqlDbType.VarChar)
            param(117).Value = txtTermofLease.Text

            param(118) = New SqlParameter("@PM_ROLLING_SHUTTER", SqlDbType.VarChar)
            param(118).Value = ddlRollingShutter.Text

            'property or building section input fields end

            ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
            ViewState("LES_TOT_RENT") = Convert.ToDecimal(txttotalrent.Text)
            ViewState("LES_SEC_DEPOSIT") = Convert.ToDecimal(txtpay.Text)




            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_ADD_LEASE", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        'lblMsg.Text = "Lease Request Raised Successfully : " + lblLeaseReqId.Text
                        lblLeaseReqId.Text = ""
                        Session("ID") = sdr("ID").ToString()
                        Session("REQ_ID") = sdr("REQUEST_ID").ToString()
                        BindReqId()
                        panPOA.Visible = False
                        ClearTextBox(Me)
                        AssigTextValues()
                        ClearDropdown(Me)
                        Landlord.Visible = True
                        AddLeaseDetails.Visible = False
                        BindOfficeType()
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnLandlord_Click(sender As Object, e As EventArgs) Handles btnLandlord.Click
        lblMsgLL.Text = ""
        lblMsg.Text = ""
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
            sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp1.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
            Dim ds As New DataSet
            ds = sp1.GetDataSet

            'To check the lanlord count
            If ds.Tables(0).Rows(0).Item("LL_COUNT") >= ViewState("NO_OF_LL") Then
                lblMsgLL.Text = "You have already reached maximum number of landlords " + ViewState("NO_OF_LL").ToString()
                Exit Sub
            End If

            'To check the total rent and security deposit if it is last lanlord.
            If ViewState("NO_OF_LL") = ds.Tables(0).Rows(0).Item("LL_COUNT") + 1 Then

                Dim TOT_LL_RENT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_RENT")) + Convert.ToDecimal(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text))
                Dim TOT_LL_SEC_DEPOSIT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT")) + Convert.ToDecimal(IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text))

                If TOT_LL_RENT <> ViewState("LES_TOT_RENT") Then
                    lblMsgLL.Text = "Landlord Rent Payable Should be " + (Math.Round(ViewState("LES_TOT_RENT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_RENT"))).ToString() + ", Should Match With Lease Total Rent " + Math.Round(ViewState("LES_TOT_RENT"), 0).ToString()
                    Exit Sub
                End If
                If TOT_LL_SEC_DEPOSIT <> ViewState("LES_SEC_DEPOSIT") Then
                    lblMsgLL.Text = "Landlord Security Deposit Should be " + (Math.Round(ViewState("LES_SEC_DEPOSIT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT"))).ToString() + ", Should Match With Lease Total Security Deposit " + Math.Round(ViewState("LES_SEC_DEPOSIT"), 0).ToString()
                    Exit Sub
                End If
                btnLandlord.Enabled = False
            Else
                ' To check the total rent and security deposit if it is not last lanlord.
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim TOT_LL_RENT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_RENT")) + Convert.ToDecimal(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text))
                    Dim TOT_LL_SEC_DEPOSIT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT")) + Convert.ToDecimal(IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text))
                    If TOT_LL_RENT > ViewState("LES_TOT_RENT") Then
                        'lblMsgLL.Text = "Landlord Rent Payable " + Math.Round(TOT_LL_RENT, 0).ToString() + " Should Not Be More Than Lease Total Rent " + Math.Round(ViewState("LES_TOT_RENT"), 0).ToString()
                        lblMsgLL.Text = "Landlord Rent Payable can't be more than " + (Math.Round(ViewState("LES_TOT_RENT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_RENT"))).ToString() + ", Should Match With Lease Total Rent " + Math.Round(ViewState("LES_TOT_RENT"), 0).ToString()
                        Exit Sub
                    End If
                    If TOT_LL_SEC_DEPOSIT > ViewState("LES_SEC_DEPOSIT") Then
                        'lblMsgLL.Text = "Landlord Security Deposit " + Math.Round(TOT_LL_SEC_DEPOSIT, 0).ToString() + " Should Not Be More Than Lease Security Deposit " + Math.Round(ViewState("LES_SEC_DEPOSIT"), 0).ToString()
                        lblMsgLL.Text = "Landlord Security Deposit can't be more than " + (Math.Round(ViewState("LES_SEC_DEPOSIT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT"))).ToString() + ", Should Match With Lease Total Security Deposit " + Math.Round(ViewState("LES_SEC_DEPOSIT"), 0).ToString()
                        Exit Sub
                    End If
                End If
            End If

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_ADD_LANDLORD_DETAILS")
            sp.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
            sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp.Command.AddParameter("@LAN_NAME", txtName.Text, DbType.String)
            sp.Command.AddParameter("@LAN_ADDRESS1", txtAddress.Text, DbType.String)
            sp.Command.AddParameter("@LAN_ADDRESS2", txtAddress2.Text, DbType.String)
            sp.Command.AddParameter("@LAN_ADDRESS3", txtAddress3.Text, DbType.String)
            sp.Command.AddParameter("@LAN_STATE", ddlState.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LAN_CITY", ddlCity.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LAN_PINCODE", txtld1Pin.Text, DbType.String)
            sp.Command.AddParameter("@LAN_PAN", txtPAN.Text, DbType.String)
            'sp.Command.AddParameter("@LAN_SERV_TAX_APP", ddlServiceTaxApplicable.SelectedValue, DbType.String)
            'sp.Command.AddParameter("@LAN_SERV_TAX", IIf(txtServiceTaxlnd.Text = "", 0, txtServiceTaxlnd.Text), DbType.Decimal)
            'sp.Command.AddParameter("@LAN_PROP_TAX_APP", ddlPropertyTaxApplicable.SelectedValue, DbType.String)
            'sp.Command.AddParameter("@LAN_PROP_TAX", IIf(txtPropertyTax.Text = "", 0, txtPropertyTax.Text), DbType.Decimal)
            sp.Command.AddParameter("@LAN_CONTACT_DET", txtContactDetails.Text, DbType.String)
            sp.Command.AddParameter("@LAN_EMAIL", txtldemail.Text, DbType.String)
            sp.Command.AddParameter("@LAN_AMOUNT_IN", ddlAmountIn.SelectedValue, DbType.String)
            sp.Command.AddParameter("@TOA", ddlOffice.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LAN_RENT", IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text), DbType.Decimal)
            sp.Command.AddParameter("@LAN_SECURITY", IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text), DbType.Decimal)
            sp.Command.AddParameter("@LAN_PAY_MODE", ddlpaymentmode.SelectedValue, DbType.Int32)
            If ddlpaymentmode.SelectedIndex > 0 Then
                lblMsg.Visible = False
                If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                    sp.Command.AddParameter("@LAN_BANK", txtBankName.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_ACCNO", txtAccNo.Text, DbType.String)
                ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                    sp.Command.AddParameter("@LAN_BANK", txtNeftBank.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_ACCNO", txtNeftAccNo.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_NEFT_BRANCH", txtNeftBrnch.Text, DbType.String)
                    sp.Command.AddParameter("@LAN_NEFT_IFSC", txtNeftIFSC.Text, DbType.String)
                End If
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please Select Payment Mode"
            End If
            sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
            Dim res As String = sp.ExecuteScalar()
            If res = "SUCCESS" Then
                ddlAmountIn.Enabled = False
                btnFinalize.Enabled = True
                GetLandlords()
                'btnBack.Enabled = True
                lblMsg.Text = "Landlord Details Added Successfully"
                ClearLandlord()
                txtName.Focus()
                panel1.Visible = False
                panel2.Visible = False
            Else
                lblMsg.Text = "Something went wrong. Please try again later."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub ClearLandlord()
        txtName.Text = ""
        txtAddress.Text = ""
        txtAddress2.Text = ""
        txtAddress3.Text = ""
        ddlState.ClearSelection()
        ddlCity.ClearSelection()
        txtld1Pin.Text = ""
        txtPAN.Text = ""
        'ddlServiceTaxApplicable.ClearSelection()
        'txtServiceTaxlnd.Text = "0"
        'ddlPropertyTaxApplicable.ClearSelection()
        'txtPropertyTax.Text = "0"
        txtContactDetails.Text = ""
        txtldemail.Text = ""
        'ddlAmountIn.ClearSelection()
        txtpmonthrent.Text = "0"
        txtpsecdep.Text = "0"
        ddlpaymentmode.ClearSelection()
        txtBankName.Text = ""
        txtAccNo.Text = ""
        txtNeftBank.Text = ""
        txtNeftAccNo.Text = ""
        txtNeftBrnch.Text = ""
        txtNeftIFSC.Text = ""

    End Sub

    'Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
    '    AddLeaseDetails.Visible = True
    '    Landlord.Visible = False
    '    ddlAmountIn.Enabled = True
    'End Sub

    Protected Sub btnFinalize_Click(sender As Object, e As EventArgs) Handles btnFinalize.Click
        lblMsgLL.Text = ""
        lblMsg.Text = ""
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
        sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        sp1.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
        Dim ds As New DataSet
        ds = sp1.GetDataSet
        If ds.Tables(0).Rows(0).Item("LL_COUNT") <> ViewState("NO_OF_LL") Then
            lblMsgLL.Text = "Please add " + (ViewState("NO_OF_LL") - ds.Tables(0).Rows(0).Item("LL_COUNT")).ToString() + " more landlords"
            Exit Sub
        Else
            lblMsg.Text = "Lease Request Raised Successfully : " + Session("REQ_ID").ToString
            AddLeaseDetails.Visible = True
            Landlord.Visible = False
            RentRevisionPanel.Visible = False
            ddlAmountIn.Enabled = True
        End If

    End Sub

    Protected Sub ddlDgSet_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDgSet.SelectedIndexChanged
        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If
    End Sub

    Protected Sub txtedate_TextChanged(sender As Object, e As EventArgs) Handles txtedate.TextChanged
        BindRentRevision()
    End Sub

    Protected Sub txtsdate_TextChanged(sender As Object, e As EventArgs) Handles txtsdate.TextChanged
        BindRentRevision()
    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub

    Private Sub GetLandlords()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORDS")
        sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        sp.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count = 1 Then
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SEND_MAIL_ADD_LEASE")
            sp1.Command.AddParameter("@REQ_BY", Session("Uid").ToString, DbType.String)
            sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp1.ExecuteScalar()
        End If

        If ds.Tables(0).Rows.Count > 0 Then
            TotalLandlords.Visible = True
            gvLandlords.DataSource = ds
            gvLandlords.DataBind()
        Else
            TotalLandlords.Visible = False
        End If
    End Sub

    Private Sub BindLegalDocuments()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEGAL_DOCS_NAMES")
        gvLegalDocs.DataSource = sp.GetDataSet()
        gvLegalDocs.DataBind()

    End Sub


    Public Class DocClas
        Private _fn As String

        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String

        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property


        Private _DocSNO As Integer

        Public Property Doc_SNO() As String
            Get
                Return _DocSNO
            End Get
            Set(ByVal value As String)
                _DocSNO = value
            End Set
        End Property

    End Class

    Dim FileUploadName As String
    ' PROPERTY IMAGES UPLOAD
    Dim legalDocs As List(Of DocClas) = New List(Of DocClas)
    Dim IC As DocClas
    Private Sub SaveDocs()
        For Each row As GridViewRow In gvLegalDocs.Rows
            'if using TemplateField columns then you may need to use FindControl method

            Dim fu As FileUpload = DirectCast(row.FindControl("fuLegalDoc"), FileUpload)

            Dim lblDocSNO As Label = DirectCast(row.FindControl("lblDocSNO"), Label)


            If fu.PostedFiles IsNot Nothing Then
                For Each File In fu.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        FileUploadName = Upload_Time & "_" & File.FileName
                        IC = New DocClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        IC.Doc_SNO = lblDocSNO.Text
                        legalDocs.Add(IC)
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub ddlLift_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLift.SelectedIndexChanged
        If ddlLift.SelectedValue = "Yes" Then
            divLift.Visible = True
        Else
            divLift.Visible = False
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/PropertyManagement/Views/PropertiesForLease.aspx")
    End Sub


    Public Sub PropertyDetails(PM_PPT_SNO As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTY_DETAILS_BY_SNO")
        sp1.Command.AddParameter("@PM_PPT_SNO", PM_PPT_SNO, DbType.String)
        Dim ds As New DataSet
        ds = sp1.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then

            txtAge.Text = ds.Tables(0).Rows(0).Item("PM_PPT_AGE")
            txtPropIDName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NAME")
            txtCarpetArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA")
            txtBuiltupArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA")
            txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")
            txtNoofFloors.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_FLRS")
            txtCeilingHight.Text = ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT")
            txtBeamBottomHight.Text = ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT")
            TxtFlooringType.Text = ds.Tables(0).Rows(0).Item("PM_FT_TYPE")
            txtSignageLocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SIGN_LOC")
            txtToilet.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_TOI")

            txtInvestedArea.Text = ds.Tables(0).Rows(0).Item("PM_BASIC_RENT")
            txtmain1.Text = ds.Tables(0).Rows(0).Item("PM_MAINT_CHRG")

            txttotalrent.Text = ds.Tables(0).Rows(0).Item("TOTALRENT")
            txtCompetitorsVicinity.Text = ds.Tables(0).Rows(0).Item("PM_VICINITY")


            Dim costtype As String
            costtype = ds.Tables(0).Rows(0).Item("OD_COST_TYPE")
            rblCostType.SelectedValue = costtype
            If costtype = "Sqft" Then
                Costype1.Visible = True
                Costype2.Visible = False
                txtRentPerSqftCarpet.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_BUA")
                txtRentPerSqftBUA.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_CARPET")

            ElseIf costtype = "Seat" Then
                Costype1.Visible = False
                Costype2.Visible = True
                txtSeatCost.Text = ds.Tables(0).Rows(0).Item("OD_SEAT_COST")
            End If
            txtOfficeType.Text = ds.Tables(0).Rows(0).Item("PM_OFFICE_TYPE")
        End If
    End Sub

    Protected Sub ddlState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlState.SelectedIndexChanged
        BindCity()
    End Sub
End Class
