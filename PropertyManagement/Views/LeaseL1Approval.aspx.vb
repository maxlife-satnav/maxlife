Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper

Partial Class PropertyManagement_Views_LeaseL1Approval
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable


    Private Sub BindRentRevision()
        Dim rowCount As Integer = 0
        rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
        If rowCount > 1 Then
            RentRevisionPanel.Visible = True
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            For i As Integer = 1 To rowCount - 1
                rr_obj = New RentRevision()
                rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
                rr_obj.RR_Percentage = "0"
                RR_lst.Add(rr_obj)
            Next
            rpRevision.DataSource = RentRevision
            rpRevision.DataBind()
        Else
            RentRevisionPanel.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindGrid()
            BindGridApprRej()
            BindCity()
            BindPayMode()
            BindTenure()
            txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")

            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY_ADM")
        sp.Command.AddParameter("@MODE", 2, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Private Sub BindPayMode()
        ObjSubSonic.Binddropdown(ddlpaymentmode, "PM_GET_PAYMENT_MODE", "NAME", "CODE")
    End Sub

    Private Sub BindLeaseExpences()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXPENSES_OF_LEASE")
        sp.Command.AddParameter("@PM_LES_SNO", hdnLSNO.Value, DbType.String)
        gvLeaseExpences.DataSource = sp.GetDataSet()
        gvLeaseExpences.DataBind()
    End Sub

    Private Sub BindTenure()
        ObjSubSonic.Binddropdown(ddlTenure, "PM_GET_PAYMENT_TERMS", "PM_PT_NAME", "PM_PT_SNO")
        ddlTenure.Items.RemoveAt(0)
    End Sub

    Private Sub BindGrid()
        Try
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_LEVEL1_APROVAL")
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_FOR_APROVAL_BY_ROLE")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                gvLeases.DataSource = ds
                gvLeases.DataBind()
                pnlBulk.Visible = True

            Else
                gvLeases.DataSource = ds
                gvLeases.DataBind()
                pnlBulk.Visible = False

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Private Sub BindGridApprRej()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASES_APROVED")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ds = sp.GetDataSet()
            gvLeasesApprRej.DataSource = ds
            gvLeasesApprRej.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Private Sub BindGridApprRejRemarks(ByVal les_sno As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_APPR_REMARKS")

            sp.Command.AddParameter("@les_sno", les_sno, DbType.String)
            ds = sp.GetDataSet()
            gvApprRemarks.DataSource = ds
            gvApprRemarks.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_LEVEL1_APROVAL_BY_SEARCH")
        sp.Command.AddParameter("@SEARCH_BY", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            gvLeases.DataSource = ds
            gvLeases.DataBind()
            pnlBulk.Visible = True
            updatepanel.Visible = False
        Else
            gvLeases.DataSource = ds
            gvLeases.DataBind()
            pnlBulk.Visible = False
            updatepanel.Visible = False
        End If
    End Sub

    Protected Sub gvLeases_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLeases.RowCommand
        Try
            If e.CommandName = "GetLeaseDetails" Then

                hdnLSNO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim ReqID As String = DirectCast(row.FindControl("lblReqID"), LinkButton).Text.ToString()
                Session("REQ_ID") = ReqID
                BindGridApprRejRemarks(hdnLSNO.Value)
                GetSelectedLeaseDetails(hdnLSNO.Value)
                BindPropertyImages()
                BindLegalDocuments(hdnLSNO.Value)
                divl1Remarks.Visible = True
                apprejButtons.Visible = True
            Else
                pnlBulk.Visible = True
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Protected Sub gvLeasesApprRej_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLeasesApprRej.RowCommand
        Try
            If e.CommandName = "GetLeaseDetails" Then

                hdnLSNO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim ReqID As String = DirectCast(row.FindControl("lblReqID"), LinkButton).Text.ToString()
                Session("REQ_ID") = ReqID
                BindGridApprRejRemarks(hdnLSNO.Value)
                GetSelectedLeaseDetails(hdnLSNO.Value)
                BindDocuments()
                pnlBulk.Visible = True
                pnlSingle.Visible = False
            Else
                pnlBulk.Visible = True
                pnlSingle.Visible = False
            End If
            pnlBulk.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetSelectedLeaseDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GRID_VIEW_MODIFY")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvlandlordItems.DataSource = ds.Tables(1)
        gvlandlordItems.DataBind()
        SetLeaseValuesToUpdate(ds)
        UpdatePanel.Visible = True
    End Sub

    Private Sub SetLeaseValuesToUpdate(ByVal leaseAllSections As DataSet)
        Dim LeaseNAreaNDetails As DataTable
        Dim charges As DataTable
        Dim agreementDetails As DataTable
        Dim brokerageDt As DataTable
        Dim UtilityDt As DataTable
        Dim OtherServicesDt As DataTable
        Dim OtherDetailsDt As DataTable
        Dim LeaseDetailsDt As DataTable
        Dim LeaseExpDt As DataTable
        Dim POA As DataTable
        Dim PropertyDetails As DataTable
        Dim OfficeConnectivity As DataTable
        Dim Civil As DataTable
        Dim Damages As DataTable

        LeaseNAreaNDetails = leaseAllSections.Tables(0)
        charges = leaseAllSections.Tables(2)
        agreementDetails = leaseAllSections.Tables(3)
        brokerageDt = leaseAllSections.Tables(4)
        UtilityDt = leaseAllSections.Tables(5)
        OtherServicesDt = leaseAllSections.Tables(6)
        OtherDetailsDt = leaseAllSections.Tables(7)
        LeaseDetailsDt = leaseAllSections.Tables(8)
        LeaseExpDt = leaseAllSections.Tables(9)
        POA = leaseAllSections.Tables(10)
        RentRevision = leaseAllSections.Tables(11)
        PropertyDetails = leaseAllSections.Tables(12)
        OfficeConnectivity = leaseAllSections.Tables(13)
        Civil = leaseAllSections.Tables(14)
        Damages = leaseAllSections.Tables(15)
        BindLeaseExpences()

        'Property Details section start
        txtAge.Text = PropertyDetails.Rows(0).Item("PM_PPT_AGE")
        txtPropIDName.Text = PropertyDetails.Rows(0).Item("PM_PPT_NAME")
        txtCarpetArea.Text = PropertyDetails.Rows(0).Item("PM_AR_CARPET_AREA")
        txtBuiltupArea.Text = PropertyDetails.Rows(0).Item("PM_AR_BUA_AREA")
        txtEfficiency.Text = PropertyDetails.Rows(0).Item("PM_AR_PREF_EFF")
        txtNoofFloors.Text = PropertyDetails.Rows(0).Item("PM_PPT_TOT_FLRS")
        txtCeilingHight.Text = PropertyDetails.Rows(0).Item("PM_AR_FTC_HIGHT")
        txtBeamBottomHight.Text = PropertyDetails.Rows(0).Item("PM_AR_FTBB_HIGHT")
        TxtFlooringType.Text = PropertyDetails.Rows(0).Item("PM_FT_TYPE")
        txtSignageLocation.Text = PropertyDetails.Rows(0).Item("PM_PPT_SIGN_LOC")
        txtToilet.Text = PropertyDetails.Rows(0).Item("PM_PPT_TOT_TOI")

        txtInvestedArea.Text = PropertyDetails.Rows(0).Item("PM_BASIC_RENT")
        txtmain1.Text = PropertyDetails.Rows(0).Item("PM_MAINT_CHRG")

        txttotalrent.Text = PropertyDetails.Rows(0).Item("TOTALRENT")

        Dim costtype As String
        costtype = PropertyDetails.Rows(0).Item("OD_COST_TYPE")
        rblCostType.SelectedValue = costtype
        If costtype = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtRentPerSqftCarpet.Text = PropertyDetails.Rows(0).Item("OD_RENT_PER_SQFT_BUA")
            txtRentPerSqftBUA.Text = PropertyDetails.Rows(0).Item("OD_RENT_PER_SQFT_CARPET")

        ElseIf costtype = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtSeatCost.Text = PropertyDetails.Rows(0).Item("OD_SEAT_COST")
        End If
        txtOfficeType.Text = PropertyDetails.Rows(0).Item("PM_OFFICE_TYPE")

        txtshopnumberoccupy.Text = PropertyDetails.Rows(0).Item("PM_SHOP_NUMBER_TO_OCCUPY")
        txtTermofLease.Text = PropertyDetails.Rows(0).Item("PM_TERMS_OF_LEASE")

        ddlRollingShutter.ClearSelection()
        Dim rolShutter As String = PropertyDetails.Rows(0)("PM_ROLLING_SHUTTER")
        ddlRollingShutter.Items.FindByValue(rolShutter).Selected = True
        'proerty details section end


        'Lease Details Section Start
        If LeaseNAreaNDetails.Rows(0)("PM_LES_STA_ID") = 4024 Then
            lblMsg.Text = "Please add at least one landlord to add lease successfully"
        Else
            lblMsg.Text = ""
        End If
        txtNoLanlords.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_NO_OF_LL")
        'Lease Details Section End

        'Area & Cost Details Section start
        ddlSecurityDepMonths.ClearSelection()
        Dim secrtyDepositMonths As String = LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEP_MONTHS")
        ddlSecurityDepMonths.Items.FindByValue(secrtyDepositMonths).Selected = True
        ddlTenure.ClearSelection()
        Dim tenure As String = LeaseNAreaNDetails.Rows(0)("PM_LES_TENURE")
        ddlTenure.Items.FindByValue(tenure).Selected = True
        'Dim costType As String = LeaseNAreaNDetails.Rows(0)("PM_LES_COST_TYPE")
        'rblCostType.Items.FindByValue(costType).Selected = True
        'If String.Equals(costType, "Seat") Then
        '    Costype1.Visible = False
        '    Costype2.Visible = True

        'Else
        '    Costype1.Visible = True
        '    Costype2.Visible = False

        'End If

        'txtSeatCost.Text = IIf(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST") = 0, 0, Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST")))
        'txtRentPerSqftCarpet.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_CARPET"))
        'txtRentPerSqftBUA.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_BUA"))

        txtLnumber.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_CTS_NO")
        txtentitle.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_ENTITLED_AMT"))
        txtInvestedArea.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_BASIC_RENT"))
        txtpay.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEPOSIT"))
        txtRentFreePeriod.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_FREE_PERIOD")
        txtInteriorCost.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_INTERIOR_COST"))
        'Area & Cost Details Section end

        'Charges Section start
        txtregcharges.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_REGS_CHARGES"))
        txtsduty.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_STMP_DUTY_CHARGES"))
        txtfurniture.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_FF_CHARGES"))
        txtbrokerage.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_CONSLBROKER_CHARGES"))
        txtpfees.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROF_CHARGES"))
        txtmain1.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_MAINT_CHARGES"))
        txtservicetax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_SERVICE_TAX"))
        txtproptax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROPERTY_TAX"))

        txtOtherTax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_OTHER_TAX"))

        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
        'Charges Section end

        'Agreement Details Section start
        txtsdate.Text = agreementDetails.Rows(0)("PM_LAD_EFFE_DT_AGREEMENT")
        txtedate.Text = agreementDetails.Rows(0)("PM_LAD_EXP_DT_AGREEMENT")
        txtlock.Text = agreementDetails.Rows(0)("PM_LAD_LOCK_INPERIOD")
        txtLeasePeiodinYears.Text = agreementDetails.Rows(0)("PM_LAD_LEASE_PERIOD")
        txtNotiePeriod.Text = agreementDetails.Rows(0)("PM_LAD_NOTICE_PERIOD")
        ddlAgreementbyPOA.ClearSelection()
        Dim AgreementbyPOA As String = agreementDetails.Rows(0)("PM_LES_SIGNED_POA")
        ddlAgreementbyPOA.Items.FindByValue(AgreementbyPOA).Selected = True
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
            txtPOAName.Text = POA.Rows(0)("PM_POA_NAME")
            txtPOAAddress.Text = POA.Rows(0)("PM_POA_ADDRESS")
            txtPOAMobile.Text = POA.Rows(0)("PM_POA_PHONE_NO")
            txtPOAEmail.Text = POA.Rows(0)("PM_POA_EMAIL")
        Else
            panPOA.Visible = False
        End If
        txtReminderEmail.Text = agreementDetails.Rows(0)("EMAIL_ADDRESS")
        'Agreement Details Section end
        BindRentRevision()

        'Brokerage Section start
        txtbrkamount.Text = Convert.ToDouble(brokerageDt.Rows(0)("PM_LBD_PAID_AMOUNT"))
        txtbrkname.Text = brokerageDt.Rows(0)("PM_LBD_NAME")
        txtbrkpan.Text = brokerageDt.Rows(0)("PM_LBD_PAN_NO")
        txtbrkmob.Text = brokerageDt.Rows(0)("PM_LBD_PHONE_NO")
        txtbrkremail.Text = brokerageDt.Rows(0)("PM_LBD_EMAIL")
        txtbrkaddr.Text = brokerageDt.Rows(0)("PM_LBD_ADDRESS")
        'Brokerage Section end

        'Utility/Power back up Details Section start
        ddlDgSet.ClearSelection()
        Dim dgSetVal As String = UtilityDt.Rows(0)("PM_LUP_DGSET")
        ddlDgSet.Items.FindByValue(dgSetVal).Selected = True

        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If

        txtDgSetPerUnit.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_COMMER_PER_UNIT")
        txtDgSetLocation.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_LOCATION")
        txtSpaceServoStab.Text = UtilityDt.Rows(0)("PM_LUP_SPACE_SERVO_STABILIZER")

        ddlElectricalMeter.ClearSelection()
        Dim eleMeter As String = UtilityDt.Rows(0)("PM_LUP_ELE_METER")
        ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        'ele meter yes start

        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If

        txtMeterLocation.Text = UtilityDt.Rows(0)("PM_LUP_MET_LOCATION")
        txtEarthingPit.Text = UtilityDt.Rows(0)("PM_LUP_EARTHING_PIT")
        txtAvailablePower.Text = UtilityDt.Rows(0)("PM_LUP_AVAIL_POWER")
        txtAdditionalPowerKWA.Text = UtilityDt.Rows(0)("PM_LUP_ADDITIONAL_POWER")
        txtPowerSpecification.Text = UtilityDt.Rows(0)("PM_LUP_POWER_SPECIFICATION")


        txtpathElecCable.Text = UtilityDt.Rows(0)("PM_LUP_PATH_ELEC_CABLE")
        txtgroundEarthling.Text = UtilityDt.Rows(0)("PM_LUP_GROUND_EARTH")
        txtearthType.Text = UtilityDt.Rows(0)("PM_LUP_EARTH_TYPE")
        txtLiftPowerCut.Text = UtilityDt.Rows(0)("PM_LUP_LIFT_POWERCUT")
        txtTransLocation.Text = UtilityDt.Rows(0)("PM_LUP_TRANSFORMER_LOCATION")
        txtWaterConnection.Text = UtilityDt.Rows(0)("PM_LUP_WATER_CONNECTION")



        ' ele meter yes end

        'Utility/Power back up Details Section end

        'Other Services Section start
        txtNoOfTwoWheelerParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_TWO_PARK")
        txtNoOfCarsParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_CAR_PARK")
        txtDistanceFromAirPort.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_AIRPT")
        txtDistanceFromRailwayStation.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_RAIL")
        txtDistanceFromBustop.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_BUS")


        ddlLift.SelectedValue = OtherServicesDt.Rows(0)("PM_LO_LIFT_YES_NO")

        If ddlLift.SelectedValue = "Yes" Then
            divLift.Visible = True

            txtMake.Text = OtherServicesDt.Rows(0)("PM_LO_MAKE")
            txtCapacity.Text = OtherServicesDt.Rows(0)("PM_LO_CAPACITY")
            txtStairCaseWidth.Text = OtherServicesDt.Rows(0)("PM_LO_MAIN_STAIRCASE_WIDTH")
            txtFireStaircase.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_STAIRCASE")


        Else
            divLift.Visible = False

            txtACOutdoor.Text = OtherServicesDt.Rows(0)("PM_LO_AC_OUTDOOR_SPACE")
            anyACSatloc.Text = OtherServicesDt.Rows(0)("PM_LO_ACS_AT_LOCATION")
            txtsignOrbrand.Text = OtherServicesDt.Rows(0)("PM_LO_SIGNAGE_BRANDING")
            txtFireExitstairs.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_EXIT_STAIRS")
            txtFireFightingSystem.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_FIGHTING_SYSTEM")
            txtLiftNLicense.Text = OtherServicesDt.Rows(0)("PM_LO_LIFTS_AVAILABLE_LICENSE")
            txtMaintAgencyLift.Text = OtherServicesDt.Rows(0)("PM_LO_MAINT_AGENCY_LIFT")

        End If


        'Other Services Section end

        'Other Details Section start
        txtCompetitorsVicinity.Text = OtherDetailsDt.Rows(0)("PM_LO_COMP_VICINITY")

        txtOfficeEquipments.Text = OtherDetailsDt.Rows(0)("PM_LO_OFF_EQUIPMENTS")
        'Other Details Section end

        'Lease Escalation Details Section start
        ddlesc.ClearSelection()
        Dim leaseEsc As String = LeaseDetailsDt.Rows(0)("PM_LD_LES_ESCALATON")
        ddlesc.Items.FindByValue(leaseEsc).Selected = True
        ddlLeaseEscType.ClearSelection()
        Dim leaseEscType As String = LeaseDetailsDt.Rows(0)("PM_LD_ESCALTION_TYPE")
        ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
        txtLeaseHoldImprovements.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_IMPROV")
        txtComments.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_COMMENTS")
        ddlDueDilegence.ClearSelection()
        Dim dueDilegence As String = LeaseDetailsDt.Rows(0)("PM_LD_DUE_DILIGENCE_CERT")
        ddlDueDilegence.Items.FindByValue(dueDilegence).Selected = True

        Dim reminderBefore As String
        reminderBefore = LeaseDetailsDt.Rows(0).Item("PM_LRE_REMINDER_BEFORE")
        Dim parts As String() = reminderBefore.Split(New Char() {","c})
        For i As Integer = 0 To parts.Length - 1
            For j As Integer = 0 To ReminderCheckList.Items.Count - 1
                If ReminderCheckList.Items(j).Value = parts(i) Then
                    ReminderCheckList.Items(j).Selected = True
                End If
            Next
        Next
        'Lease Escalation Details Section end


        'office connectivity start
        txtTeleSPB.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_SP_BUILDING")
        txtTeleSPFO.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_SP_FIRST_OCCUPANT")
        txtLeaseSPB.Text = OfficeConnectivity.Rows(0)("PM_OC_LEASE_LINE_SP_BUILD")
        txtLeaseSPFO.Text = OfficeConnectivity.Rows(0)("PM_OC_LEASE_LINE_SP_FIRST_OCCPNT")
        txtTeleVenFR.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_VEN_FEASIBILITY")
        txtITVenFR.Text = OfficeConnectivity.Rows(0)("PM_OC_IT_VEN_FEASIBILITY")
        'office connectivity end

        'CIVIL SECTION START
        txtPunnWall.Text = Civil.Rows(0)("PM_C_PUNNG_WALLS")
        txtCracks.Text = Civil.Rows(0)("PM_C_CRACKS")
        txtWashToilet.Text = Civil.Rows(0)("PM_C_TOILET")
        txtPantry.Text = Civil.Rows(0)("PM_C_PANTRY")
        txtDrainagelinedrawings.Text = Civil.Rows(0)("PM_C_DRAINAGE_DRAWNGS")
        txtWindowHeight.Text = Civil.Rows(0)("PM_C_WINDOW_HEIGHT")

        txtWaterLeak.Text = Civil.Rows(0)("PM_C_WATER_LEAKAGE")
        txtStaircase.Text = Civil.Rows(0)("PM_C_STAIRCASE")
        txtEmergExit.Text = Civil.Rows(0)("PM_C_EMRG_EXIT")
        txtHeightAvailability.Text = Civil.Rows(0)("PM_C_HEIGH_AVAILABILITY")
        txtAddtnlCivilWork.Text = Civil.Rows(0)("PM_C_ADD_CIVIL_WORK")
        'CIVIL SECTION END

        'out standing amounts/damages start
        ddlOutOrDamagesAmount.SelectedValue = Damages.Rows(0)("PM_D_ANY_DAMAGES")
        ddlDamangesExtStay.SelectedValue = Damages.Rows(0)("PM_D_EXTND_STAY")
        ddlPossessionHandover.SelectedValue = Damages.Rows(0)("PM_D_HAND_OVER")
        txtOfficeEquipments.Text = Damages.Rows(0)("PM_D_OFFC_EQUIPMNTS")
        txtWorkHrs.Text = Damages.Rows(0)("PM_D_WRKNG_HRS_CONSTRUCTION")
        txtCommonToilet.Text = Damages.Rows(0)("PM_D_USAGE_TOILET")
        txtServiceLift.Text = Damages.Rows(0)("PM_D_USAGE_LIFT")
        txtSecSystem.Text = Damages.Rows(0)("PM_D_SECURITY_SYSTEM")
        'out standing amounts/damages end

        ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
        ViewState("LES_TOT_RENT") = Convert.ToDecimal(txttotalrent.Text)
        ViewState("LES_SEC_DEPOSIT") = Convert.ToDecimal(txtpay.Text)

        gvlandlordItems.DataSource = leaseAllSections.Tables(1)
        gvlandlordItems.DataBind()
        If leaseAllSections.Tables(1).Rows.Count >= ViewState("NO_OF_LL") Then
            btnAddNewLandlord.Enabled = False
        Else
            btnAddNewLandlord.Enabled = True
            txtName.Focus()
        End If

    End Sub

    Protected Sub gvLeases_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLeases.PageIndexChanging
        gvLeases.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub



    Protected Sub gvApprRemarks_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvApprRemarks.PageIndexChanging
        gvApprRemarks.PageIndex = e.NewPageIndex()
        BindGridApprRejRemarks(hdnLSNO.Value)
    End Sub

    Protected Sub gvLeasesApprRej_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLeasesApprRej.PageIndexChanging
        gvLeasesApprRej.PageIndex = e.NewPageIndex()
        BindGridApprRej()
    End Sub

    Protected Sub gvlldata_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvlandlordItems.RowCommand
        Try
            If e.CommandName = "GetLandlordDetails" Then
                Landlord.Visible = True
                hdnLandlordSNO.Value = e.CommandArgument
                GetSelectedLandlordDetails(hdnLandlordSNO.Value)
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetSelectedLandlordDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORD_DETAILS_BY_ID")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_LL_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        UpdatePanel.Visible = True
        Landlord.Visible = True
        SetLandlordValsToUpdate(ds.Tables(0))
    End Sub

    Private Sub SetLandlordValsToUpdate(ByVal landlorddt As DataTable)
        txtName.Text = landlorddt.Rows(0)("PM_LL_NAME")
        txtAddress.Text = landlorddt.Rows(0)("PM_LL_ADDRESS1")
        txtAddress2.Text = landlorddt.Rows(0)("PM_LL_ADDRESS2")
        txtAddress3.Text = landlorddt.Rows(0)("PM_LL_ADDRESS3")
        txtL1State.Text = landlorddt.Rows(0)("PM_LL_STATE")
        ddlCity.ClearSelection()
        Dim selectedCity As String = landlorddt.Rows(0)("PM_LL_CITY_CODE")
        ddlCity.Items.FindByValue(selectedCity).Selected = True
        txtld1Pin.Text = landlorddt.Rows(0)("PM_LL_PINCODE")
        txtPAN.Text = landlorddt.Rows(0)("PM_LL_PAN")
        txtldemail.Text = landlorddt.Rows(0)("PM_LL_EMAIL")
      
        txtContactDetails.Text = landlorddt.Rows(0)("PM_LL_PHONE_NO")
        ddlAmountIn.ClearSelection()
        Dim amountIn As String = landlorddt.Rows(0)("PM_LL_AMOUNT_IN")
        ddlAmountIn.Items.FindByValue(amountIn).Selected = True
        txtpmonthrent.Text = Convert.ToDouble(landlorddt.Rows(0)("PM_LL_MON_RENT_PAYABLE"))
        txtpsecdep.Text = Convert.ToDouble(landlorddt.Rows(0)("PM_LL_SECURITY_DEPOSIT"))
        ddlpaymentmode.ClearSelection()
        Dim paymentMode As String = landlorddt.Rows(0)("PM_LL_PAYMENT_MODE")
        ddlpaymentmode.Items.FindByValue(paymentMode).Selected = True
        txtBankName.Text = landlorddt.Rows(0)("PM_LL_BANK")
        txtAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")

        'NEFT Transfer
        txtNeftBank.Text = landlorddt.Rows(0)("PM_LL_BANK")
        txtNeftAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")
        txtNeftBrnch.Text = landlorddt.Rows(0)("PM_LL_BRANCH")
        txtNeftIFSC.Text = landlorddt.Rows(0)("PM_LL_IFSC")
        txtNeftAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")

        If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
            panel1.Visible = True
            panel2.Visible = False
        ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
            panel1.Visible = False
            panel2.Visible = True
        Else
            panel1.Visible = False
            panel2.Visible = False
        End If

    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub

    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub

    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            Else
                panel1.Visible = False
                panel2.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtSearch.Text = String.Empty
        updatepanel.Visible = False
        BindGrid()
    End Sub

    Private Sub MultiApproval()
        Try
            Dim LES_APPR_lst As New List(Of L1Approval)()
            Dim LES_APPR_obj As New L1Approval()
            For Each row As GridViewRow In gvLeases.Rows
                LES_APPR_obj = New L1Approval()
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                Dim lblLeaseID As Label = DirectCast(row.FindControl("lblLeaseID"), Label)
                Dim lblLesSno As Label = DirectCast(row.FindControl("lblLesSno"), Label)
                If chkselect.Checked = True Then
                    LES_APPR_obj.Lease_ID = lblLeaseID.Text
                    LES_APPR_obj.Lease_SNO = lblLesSno.Text
                    LES_APPR_obj.L1_REMARKS = txtRemarks.Text
                    LES_APPR_lst.Add(LES_APPR_obj)
                End If
            Next

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@LEASEAPPRLIST", SqlDbType.Structured)
            param(0).Value = UtilityService.ConvertToDataTable(LES_APPR_lst)
            param(1) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(1).Value = Session("Uid").ToString
            
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_APPROVE_MULTI_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then

                        Dim mailParam(4) As SqlParameter
                        For i As Integer = 0 To LES_APPR_lst.Count - 1
                            mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                            mailParam(0).Value = Session("UID").ToString()
                            mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                            mailParam(1).Value = "Approved"
                            mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                            mailParam(2).Value = "Level1 "
                            mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                            mailParam(3).Value = txtRemarks.Text
                            mailParam(4) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                            mailParam(4).Value = LES_APPR_lst(i).Lease_ID
                            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        Next

                        lblMsg.Text = "Lease Request Approved Successfully "
                        txtRemarks.Text = ""
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
            BindGridApprRej()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnAppall_Click(sender As Object, e As EventArgs) Handles btnAppall.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        MultiApproval()
    End Sub

    Private Sub MultiReject()
        Try
            Dim LES_APPR_lst As New List(Of L1Approval)()
            Dim LES_APPR_obj As New L1Approval()
            For Each row As GridViewRow In gvLeases.Rows
                LES_APPR_obj = New L1Approval()
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                Dim lblLeaseID As Label = DirectCast(row.FindControl("lblLeaseID"), Label)
                Dim lblLesSno As Label = DirectCast(row.FindControl("lblLesSno"), Label)
                If chkselect.Checked = True Then
                    LES_APPR_obj.Lease_ID = lblLeaseID.Text
                    LES_APPR_obj.Lease_SNO = lblLesSno.Text
                    LES_APPR_obj.L1_REMARKS = txtRemarks.Text
                    LES_APPR_lst.Add(LES_APPR_obj)
                End If
            Next

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@LEASEAPPRLIST", SqlDbType.Structured)
            param(0).Value = UtilityService.ConvertToDataTable(LES_APPR_lst)
            param(1) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(1).Value = Session("Uid").ToString

            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_REJECT_MULTI_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        Dim mailParam(4) As SqlParameter
                        For i As Integer = 0 To LES_APPR_lst.Count - 1
                            mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                            mailParam(0).Value = Session("UID").ToString()
                            mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                            mailParam(1).Value = "Rejected"
                            mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                            mailParam(2).Value = "Level1 "
                            mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                            mailParam(3).Value = txtRemarks.Text
                            mailParam(4) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                            mailParam(4).Value = LES_APPR_lst(i).Lease_ID
                            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        Next
                        lblMsg.Text = "Lease Request Rejected Successfully "
                        txtRemarks.Text = ""
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
            BindGridApprRej()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnRejAll_Click(sender As Object, e As EventArgs) Handles btnRejAll.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        MultiReject()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Landlord.Visible = False
    End Sub

    Private Sub SingleApproval()
        Try
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@L1_REMARKS", SqlDbType.VarChar)
            param(0).Value = txtRemarksSingle.Text
            param(1) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
            param(1).Value = Session("REQ_ID").ToString
            param(2) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(2).Value = Session("Uid").ToString
            param(3) = New SqlParameter("@LES_SNO", SqlDbType.Int)
            param(3).Value = hdnLSNO.Value


            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_APPROVE_SINGLE_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        Dim mailParam(4) As SqlParameter
                        mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                        mailParam(0).Value = Session("UID").ToString()
                        mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                        mailParam(1).Value = "Approved"
                        mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                        mailParam(2).Value = "Level1 "
                        mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                        mailParam(3).Value = txtRemarksSingle.Text
                        mailParam(4) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                        mailParam(4).Value = Session("REQ_ID")
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        lblMsg.Text = "Lease Request Approved Successfully: " + Session("REQ_ID").ToString
                        txtRemarksSingle.Text = ""
                        updatepanel.Visible = False
                        pnlSingle.Visible = False
                        pnlBulk.Visible = True
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
            BindGridApprRej()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        SingleApproval()
    End Sub

    Private Sub SingleReject()
        Try
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@L1_REMARKS", SqlDbType.VarChar)
            param(0).Value = txtRemarksSingle.Text
            param(1) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
            param(1).Value = Session("REQ_ID").ToString
            param(2) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(2).Value = Session("Uid").ToString
            param(3) = New SqlParameter("@LES_SNO", SqlDbType.Int)
            param(3).Value = hdnLSNO.Value

            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_REJECT_SINGLE_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        Dim mailParam(4) As SqlParameter
                        mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                        mailParam(0).Value = Session("UID").ToString()
                        mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                        mailParam(1).Value = "Rejected"
                        mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                        mailParam(2).Value = "Level1 "
                        mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                        mailParam(3).Value = txtRemarksSingle.Text
                        mailParam(4) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                        mailParam(4).Value = Session("REQ_ID")
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        lblMsg.Text = "Lease Request Rejected Successfully: " + Session("REQ_ID").ToString
                        txtRemarksSingle.Text = ""
                        updatepanel.Visible = False
                        pnlSingle.Visible = False
                        pnlBulk.Visible = True
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
            BindGridApprRej()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        SingleReject()
    End Sub

    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim pm_ldoc_sno = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = "~\Images\Property_Images\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\Images\Property_Images", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub

    Public Sub BindDocuments()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PM_LDOC_PM_LES_SNO", SqlDbType.NVarChar, 50)
        param(0).Value = hdnLSNO.Value
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_LEASE_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub

    Protected Sub CancelEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvlandlordItems.RowCancelingEdit
        gvLeaseExpences.EditIndex = -1
        BindLeaseExpences()
    End Sub

    Protected Sub EditLeaseExpense(sender As Object, e As GridViewEditEventArgs)
        gvLeaseExpences.EditIndex = e.NewEditIndex
        BindLeaseExpences()
    End Sub
    Protected Sub UpdateLeaseExpense(sender As Object, e As GridViewUpdateEventArgs)
        Dim ddlServiceProvider As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlServiceProvider"), DropDownList).SelectedItem.Value
        Dim ddliptype As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddliptype"), DropDownList).SelectedItem.Value
        Dim ddlPaidBy As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlPaidBy"), DropDownList).SelectedItem.Value
        Dim PM_EXP_SNO As String = gvLeaseExpences.DataKeys(e.RowIndex).Value.ToString()
        Dim code As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("lblcode"), Label).Text
        Dim compValue As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("txtCompValue"), TextBox).Text
        Dim Flag As Integer
        Dim param(9) As SqlParameter
        'Area &Cost
        param(0) = New SqlParameter("@PM_EXP_SERV_PROVIDER", SqlDbType.VarChar)
        param(0).Value = ddlServiceProvider
        param(1) = New SqlParameter("@PM_EXP_INP_TYPE", SqlDbType.VarChar)
        param(1).Value = ddliptype
        param(2) = New SqlParameter("@PM_EXP_COMP_LES_VAL", SqlDbType.VarChar)
        param(2).Value = compValue
        param(3) = New SqlParameter("@PM_EXP_PAID_BY", SqlDbType.VarChar)
        param(3).Value = ddlPaidBy

        param(4) = New SqlParameter("@PM_EXP_SNO", SqlDbType.VarChar)
        param(4).Value = PM_EXP_SNO

        If PM_EXP_SNO = "" Then
            Flag = 2 ' Insert
        Else
            Flag = 1 ' Update
        End If

        param(5) = New SqlParameter("@FLAG", SqlDbType.VarChar)
        param(5).Value = Flag

        param(6) = New SqlParameter("@PM_EXP_PM_LES_SNO", SqlDbType.VarChar)
        param(6).Value = hdnLSNO.Value

        param(7) = New SqlParameter("@PM_EXP_HEAD", SqlDbType.VarChar)
        param(7).Value = code

        param(8) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
        param(8).Value = Session("Uid").ToString

        param(9) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
        param(9).Value = Session("REQ_ID")

        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_UPDATE_LEASE_EXPENSES", param)
            While sdr.Read()
                If sdr("result").ToString() = "SUCCESS" Then
                    lblMsg.Text = "Lease Expenses Updated Successfully : " + Session("REQ_ID")
                    BindLeaseExpences()
                Else
                    lblMsg.Text = "Something went wrong. Please try again later."
                End If
            End While
            sdr.Close()
        End Using
    End Sub


    Protected Sub RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow AndAlso gvLeaseExpences.EditIndex = e.Row.RowIndex Then
            Dim ddlSerpr As DropDownList = DirectCast(e.Row.FindControl("ddlServiceProvider"), DropDownList)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SERVICE_PROVIDER_DDL")
            ddlSerpr.DataSource = sp.GetDataSet().Tables(0)
            ddlSerpr.DataTextField = "PM_SP_NAME"
            ddlSerpr.DataValueField = "PM_SP_SNO"
            ddlSerpr.DataBind()

            If Not TryCast(e.Row.FindControl("lblspno"), Label).Text = "" Then
                ddlSerpr.Items.FindByValue(TryCast(e.Row.FindControl("lblspno"), Label).Text).Selected = True
            End If

            If Not TryCast(e.Row.FindControl("lblipname"), Label).Text = "" Then
                Dim ddliptype As DropDownList = DirectCast(e.Row.FindControl("ddliptype"), DropDownList)
                ddliptype.Items.FindByValue(TryCast(e.Row.FindControl("lblipname"), Label).Text).Selected = True
            End If

            If Not TryCast(e.Row.FindControl("lblPaidby"), Label).Text = "" Then

                Dim ddlPaidBy As DropDownList = DirectCast(e.Row.FindControl("ddlPaidBy"), DropDownList)
                ddlPaidBy.Items.FindByValue(TryCast(e.Row.FindControl("lblPaidby"), Label).Text).Selected = True
            End If
        End If
    End Sub
    Private Sub BindLegalDocuments(ByVal PM_LES_SNO As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEGAL_DOCS_OF_LEASE_BY_SNO")
        sp.Command.AddParameter("@PM_LES_SNO", PM_LES_SNO, DbType.Int32)
        gvLegalDocs.DataSource = sp.GetDataSet()
        gvLegalDocs.DataBind()

    End Sub
    Public Sub BindPropertyImages()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PM_LES_SNO", SqlDbType.NVarChar, 50)
        param(0).Value = hdnLSNO.Value
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_PROEPRTY_IMAGES", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Property Images Available"
        End If
        dtDocs = Nothing
    End Sub
    Protected Sub gvLegalDocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLegalDocs.RowCommand
        Try
            If e.CommandName = "DownloadLeaseDoc" Then

                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim filepath As String = DirectCast(row.FindControl("ldocPath"), LinkButton).Text.ToString()
                filepath = "~\Images\Property_Images\" & filepath
                Response.ContentType = "application/CSV"
                Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\Images\Property_Images", "") & """")
                Response.TransmitFile(Server.MapPath(filePath))
                Response.[End]()
            ElseIf e.CommandName = "DeleteLeaseDoc" Then
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim docSNO As String = DirectCast(row.FindControl("lblLdocSNO"), Label).Text.ToString()
                param = New SqlParameter(0) {}
                param(0) = New SqlParameter("@PM_LDOC_SNO", SqlDbType.NVarChar, 50)
                param(0).Value = docSNO
                ObjSubSonic.GetSubSonicDataSet("PM_DELETE_LEASE_DOCS", param)
                BindLegalDocuments(hdnLSNO.Value)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
