﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_TenantRenew
    Inherits System.Web.UI.Page

    Private Sub BindProperties()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_PROPERTIES_DETAILS")
        ddlBuilding.DataSource = sp.GetDataSet()
        ddlBuilding.DataTextField = "PM_PPT_NAME"
        ddlBuilding.DataValueField = "PM_PPT_SNO"
        ddlBuilding.DataBind()
        ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub
    Private Sub BindPaymentTerms()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PAYMENT_TERMS")
            ddlPaymentTerms.DataSource = sp.GetDataSet()
            ddlPaymentTerms.DataTextField = "PM_PT_NAME"
            ddlPaymentTerms.DataValueField = "PM_PT_SNO"
            ddlPaymentTerms.DataBind()
            ddlPaymentTerms.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub

    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindUser()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENUSER")
            sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindCity()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_CITY")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@USR_ID", Session("UID"), DbType.String)
            ddlCity.DataSource = sp.GetDataSet()
            ddlCity.DataTextField = "CTY_NAME"
            ddlCity.DataValueField = "CTY_CODE"
            ddlCity.DataBind()
            ddlCity.Items.Insert(0, New ListItem("--Select City--", "--Select City--"))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub BindLocationByCity(ByVal cty As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", cty)
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VIEW_TENANT_DETAILS")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()

            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub EnableDisableFields()
        ddlproptype.Enabled = False
        ddlCity.Enabled = False
        ddlLocation.Enabled = False
        ddlBuilding.Enabled = False
        txtTenantOccupiedArea.Enabled = False
        txtSubject.Enabled = False
        txtPurpose.Enabled = False
        txtrenew.Text = False
        txtTenFromDt.Enabled = False
        txtTenEndDt.Enabled = False
        ddluser.Enabled = False
        txttcode.Enabled = False
        txtNoofparking.Enabled = False
        txtRent.Enabled = False
        txtSecurityDeposit.Enabled = False
        txtDate.Enabled = False
        ddlPaymentTerms.Enabled = False
        txtAddnFee.Enabled = False
        txtfees.Enabled = False
        txtamount.Enabled = False
        TntRem.Enabled = False
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            btnSubmit.OnClientClick = "fn();return false;"
            btnApproval.OnClientClick = "fn();return false;"
            btnRejection.OnClientClick = "fn();return false;"
            txtrenew.Text = "1"
            BindPaymentTerms()
            BindPropType()
            BindCity()
            BindCityLoc()
            BindDetails(Request.QueryString("Renewal"))
            If Request.QueryString("Type") = "1" Then
                EnableDisableFields()
                btnApproval.Visible = True
                btnRejection.Visible = True
                btnSubmit.Visible = False
            Else
                btnApproval.Visible = False
                btnRejection.Visible = False
                btnSubmit.Visible = True
            End If

            ' This is invoked when clicked on view details in tenant extension requisition 
            If Request.QueryString("ExType") = "1" Then
                EnableDisableFields()
                btnApproval.Visible = False
                btnRejection.Visible = False
                btnSubmit.Visible = False
            End If

        End If
    End Sub

    Private Sub BindDetails(ByVal Renew As String)
        Try
            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENANT_DETAILS")
            sp4.Command.AddParameter("@SNO", Renew, DbType.Int32)
            sp4.Command.AddParameter("@flag", 1, DbType.Int32)
            Dim ds As New DataSet
            ds = sp4.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                BindPropType()
                Dim li4 As ListItem = Nothing
                li4 = ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPERTY_TYPE"))
                If Not li4 Is Nothing Then
                    li4.Selected = True
                End If
                BindCity()
                Dim li2 As ListItem = Nothing
                li2 = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_CITY_CODE"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                    'ddlCity.Enabled = False
                End If
                BindCityLoc()
                Dim li6 As ListItem = Nothing
                li6 = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_LOC_CODE"))
                If Not li6 Is Nothing Then
                    li6.Selected = True
                    'ddlCity.Enabled = False
                End If

                BindProperties()
                Dim li7 As ListItem = Nothing
                li7 = ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPRTY"))
                If Not li7 Is Nothing Then
                    li7.Selected = True
                End If

                BindUser()
                Dim li3 As ListItem = Nothing
                li3 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_NAME"))
                If Not li3 Is Nothing Then
                    li3.Selected = True
                End If

                BindPaymentTerms()
                Dim li8 As ListItem = Nothing
                li8 = ddlPaymentTerms.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TD_PAY_TERMS"))
                If Not li8 Is Nothing Then
                    li8.Selected = True
                End If
                txtTenantOccupiedArea.Text = ds.Tables(0).Rows(0).Item("PM_TEN_OCCUP_AREA")
                txtSubject.Text = ds.Tables(0).Rows(0).Item("PM_TEN_SUB_OF_AGREE")
                txtPurpose.Text = ds.Tables(0).Rows(0).Item("PM_TEN_PUR_OF_AGREE")
                txtRent.Text = ds.Tables(0).Rows(0).Item("PM_TD_RENT")
                txtDate.Text = ds.Tables(0).Rows(0).Item("PM_TD_JOIN_DT")
                txtSecurityDeposit.Text = ds.Tables(0).Rows(0).Item("PM_TD_SECURITY_DEPOSIT")
                ddlPaymentTerms.SelectedValue = ds.Tables(0).Rows(0).Item("PM_TD_PAY_TERMS")
                txtTenFromDt.Text = ds.Tables(0).Rows(0).Item("PM_TEN_FRM_DT")
                txtTenEndDt.Text = ds.Tables(0).Rows(0).Item("PM_TEN_TO_DT")
                txttcode.Text = ds.Tables(0).Rows(0).Item("PM_TEN_CODE")
                txtNoofparking.Text = ds.Tables(0).Rows(0).Item("PM_TEN_NO_OF_PARKING")
                txtfees.Text = ds.Tables(0).Rows(0).Item("PM_TD_MAINT_FEES")
                txtamount.Text = ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT")
                txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_TD_REMARKS"))
                txtrenew.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Years"))
                Dim s As String = ds.Tables(0).Rows(0).Item("PM_TEN_REMINDER_BEFORE").ToString()
                Dim values As String() = s.Split(","c).[Select](Function(sValue) sValue.Trim()).ToArray()
                For i As Integer = 0 To TntRem.Items.Count - 1
                    For j As Integer = 0 To values.Length - 1
                        If TntRem.Items(i).Value = values.GetValue(j) Then
                            TntRem.Items(i).Selected = True
                        End If
                    Next
                Next
                
            End If
            'THIS
            txtTenFromDt.Text = Convert.ToDateTime(txtTenFromDt.Text).AddDays(1)
            Dim tdt As DateTime = Convert.ToDateTime(txtTenEndDt.Text).AddYears(1).AddDays(-1)
            txtTenEndDt.Text = tdt
            txtTenFromDt.Enabled = False
            txtTenEndDt.Enabled = False

            GetDateIntervals(Renew)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim RenewId As String
            RenewId = Request.QueryString("Renewal")
            Dim Id As Integer = Convert.ToInt32(RenewId)
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_INSERT_EXTENSION_DETAILS")
            sp2.Command.AddParameter("@SNO", Id, DbType.Int32)
            sp2.Command.AddParameter("@PROPERTYTYPE", ddlproptype.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@PROPERTY", ddlBuilding.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@OCCUP_AREA", txtTenantOccupiedArea.Text, DbType.String)
            sp2.Command.AddParameter("@LEASE_SUBJ", txtSubject.Text, DbType.String)
            sp2.Command.AddParameter("@LEASE_PURP", txtPurpose.Text, DbType.String)
            sp2.Command.AddParameter("@YEARS", txtrenew.Text, DbType.String)
            sp2.Command.AddParameter("@FROMDATE", txtTenFromDt.Text, DbType.Date)
            sp2.Command.AddParameter("@TODATE", txtTenEndDt.Text, DbType.Date)
            sp2.Command.AddParameter("@TEN_NAME", ddluser.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@TEN_CODE", txttcode.Text, DbType.String)
            sp2.Command.AddParameter("@PARKING_SPACES", txtNoofparking.Text, DbType.String)
            sp2.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
            sp2.Command.AddParameter("@CREATEDBY", Session("uid"), DbType.String)
            sp2.Command.AddParameter("@TEN_RENT_PER_SFT", txtRent.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_SECURITY_DEPOSIT", txtSecurityDeposit.Text, DbType.Decimal)
            sp2.Command.AddParameter("@JOIN_DATE", txtDate.Text, DbType.Date)
            sp2.Command.AddParameter("@TEN_PAYMENTTERMS", ddlPaymentTerms.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@ADD_PARKING", txtAddnFee.Text, DbType.String)
            sp2.Command.AddParameter("@TEN_MAINT_FEE", IIf(txtfees.Text = "", 0, txtfees.Text), DbType.Decimal)
            sp2.Command.AddParameter("@TEN_OUTSTANDING_AMOUNT", txtamount.Text, DbType.Decimal)
            Dim selectedItems As String = [String].Join(",", TntRem.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
            sp2.Command.AddParameter("@TEM_REM", selectedItems, DbType.String)
            sp2.ExecuteScalar()
            lblmsg.Visible = True
            lblmsg.Text = "Tenant Renewal Requisition Submitted Succesfully"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub txtRent_TextChanged(sender As Object, e As EventArgs) Handles txtRent.TextChanged
        If txtRent.Text <> Nothing Then
            Dim MainF As Decimal
            If txtfees.Text = "" Then
                MainF = 0
            Else
                MainF = Convert.ToDecimal(txtfees.Text)
            End If

            Dim AddF As Decimal             'Additional car parking fee
            If txtAddnFee.Text = "" Then
                AddF = 0
            Else
                AddF = Convert.ToDecimal(txtAddnFee.Text)
            End If

            txtamount.ReadOnly = False
            txtamount.Text = MainF + Convert.ToDecimal(txtRent.Text) + AddF
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub

    Protected Sub txtAddnFee_TextChanged(sender As Object, e As EventArgs) Handles txtAddnFee.TextChanged
        Dim MainF As Decimal
        If txtfees.Text = "" Then
            MainF = 0
        Else
            MainF = Convert.ToDecimal(txtfees.Text)
        End If

        Dim RentF As Decimal
        If txtRent.Text = "" Then
            RentF = 0
        Else
            RentF = Convert.ToDecimal(txtRent.Text)
        End If

        Dim AddF As Decimal             'Additional car parking fee
        If txtAddnFee.Text = "" Then
            AddF = 0
        Else
            AddF = Convert.ToDecimal(txtAddnFee.Text)
        End If

        txtamount.ReadOnly = False
        txtamount.Text = MainF + RentF + AddF
        txtamount.ReadOnly = True
    End Sub

    Protected Sub txtfees_TextChanged(sender As Object, e As EventArgs) Handles txtfees.TextChanged
        If txtRent.Text <> Nothing Then
            Dim MainF As Decimal
            If txtfees.Text = "" Then
                MainF = 0
            Else
                MainF = Convert.ToDecimal(txtfees.Text)
            End If

            Dim AddF As Decimal             'Additional car parking fee
            If txtAddnFee.Text = "" Then
                AddF = 0
            Else
                AddF = Convert.ToDecimal(txtAddnFee.Text)
            End If

            txtamount.ReadOnly = False
            txtamount.Text = Convert.ToDecimal(txtRent.Text) + MainF + AddF
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub

    Protected Sub txtrenew_TextChanged(sender As Object, e As EventArgs) Handles txtrenew.TextChanged
        If txtrenew.Text = "" Then
            txtrenew.Text = "1"
        Else
            Dim num As Integer
            Dim isNum As Boolean = Int32.TryParse(txtrenew.Text, num)
            If isNum Then
                Dim tdt As DateTime = Convert.ToDateTime(txtTenFromDt.Text).AddYears(Convert.ToInt32(txtrenew.Text)).AddDays(-1)
                txtTenEndDt.Text = tdt
            Else
                lblmsg.Visible = True
                lblmsg.Text = "Please Enter Valid Number of years"
            End If

        End If
    End Sub
    Protected Sub GetDateIntervals(Renew As Integer)
        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_DATE_INTERVALS")
        sp4.Command.AddParameter("@SNO", Renew, DbType.Int32)
        Dim ds As New DataSet
        ds = sp4.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            Session("FromDate") = ds.Tables(0).Rows(0).Item("PM_RT_START_DT")
            Session("ToDate") = ds.Tables(0).Rows(0).Item("PM_RT_END_DT")
        End If
    End Sub
    Protected Sub btnApproval_Click(sender As Object, e As EventArgs) Handles btnApproval.Click
        lblmsg.Text = ""
        Dim Sno As String = Request.QueryString("Renewal")
        Dim Id As Integer = Sno
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_UPDATE_TENANT_RENEWAL")
        sp.Command.AddParameter("@SNO", Id, DbType.String)
        sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@USER", Session("Uid"), DbType.String)
        sp.Command.AddParameter("@FROM", Session("FromDate"), DbType.String)
        sp.Command.AddParameter("@TO", Session("ToDate"), DbType.String)
        sp.ExecuteScalar()
        lblmsg.Text = "Tenant Renewal Approved Successfully"
    End Sub

    Protected Sub btnRejection_Click(sender As Object, e As EventArgs) Handles btnRejection.Click
        lblmsg.Text = ""
        Dim Sno As String = Request.QueryString("Renewal")
        Dim Id As Integer = Sno
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_TENANT_RENEWAL_REJECETION")
        sp.Command.AddParameter("@SNO", Id, DbType.Int32)
        sp.ExecuteScalar()
        lblmsg.Text = "Tenant Renewal Rejected Successfully"
    End Sub
End Class
