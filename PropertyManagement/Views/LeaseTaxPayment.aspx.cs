﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;

public partial class PropertyManagement_Views_LeaseTaxPayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (Session["uid"].ToString() == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        if (!IsPostBack)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string host = HttpContext.Current.Request.Url.Host;

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 10);
            param[0].Value = Session["UID"];
            param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
            param[1].Value = path;
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
                }
            }
            BindGrid();
            panel1.Visible = false;

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA_SEARCH_BY");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();
    }

    protected void txtreset_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtReqId.Text = "";
    }

    private void BindGrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();

    }

    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindGrid();
        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataBind();
        lblMsg.Text = "";
    }
    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblMsg.Visible = false;
        btnSubmit.Focus();
        if (e.CommandName == "Tax")
        {
            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblLseName = (Label)gvRow.FindControl("lblLseName");

            panel1.Visible = true;

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_BIND_LANDLORDS");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@LEASE_ID", lblLseName.Text, DbType.String);
            gvLandlords.DataSource = sp.GetDataSet();
            gvLandlords.DataBind();
        }
        else
        {
            panel1.Visible = false;
        }

    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow row in gvLandlords.Rows)
            {

                Label lblreqid = (Label)row.FindControl("lbllname");
                DropDownList ddlTDS = (DropDownList)row.FindControl("ddlTDS");
                Label lblSNO = (Label)row.FindControl("LBLSNO");

                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@LEASEID", lblreqid.Text);
                param[1] = new SqlParameter("@TDS", ddlTDS.SelectedItem.Value);
                param[2] = new SqlParameter("@TDS_REMARKS", txtRemarks.Text);
                param[3] = new SqlParameter("@AUR_ID", Session["UID"].ToString());
                param[4] = new SqlParameter("@SNO", lblSNO.Text);
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_LEASE_TAX_PAYMENT", param);


                lblMsg.Visible = true;
                lblMsg.Text = "TDS for Landlord(s) Confirmed Successfully";
                panel1.Visible = false;
            }


        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

}