Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmApprovalLease
    Inherits System.Web.UI.Page
    Shared refund As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            BindGrid()
            If gvItems.Rows.Count > 0 Then
                lblMessage.Visible = True
            Else
                lblMessage.Visible = False
            End If
            'LeaseSurrenderAmt()
            pnl1.Visible = False
            txtsurrender.Text = Date.Today.ToString("MM/dd/yyyy")
        End If
        txtsurrender.Attributes.Add("readonly", "readonly")
        txtrefund.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub LeaseSurrenderAmt()
        txtAmtPaidLesse.Text = 0
        txtAmtPaidLessor.Text = 0
        If CInt(txtSDamount.Text) > (CInt(txtDmgAmt.Text) + CInt(txtOutAmt.Text)) Then
            txtAmtPaidLesse.Text = CInt(txtSDamount.Text) - (CInt(txtDmgAmt.Text) + CInt(txtOutAmt.Text))

        Else
            txtAmtPaidLessor.Text = (CInt(txtDmgAmt.Text) + CInt(txtOutAmt.Text)) - CInt(txtSDamount.Text)

        End If

    End Sub
    Private Sub Clear()
        txtOutAmt.Text = ""
        txtAmtPaidLessor.Text = ""
        txtAmtPaidLesse.Text = ""
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_GRID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "Surrender" Then

            Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
            Dim lblLseName As Label = DirectCast(gvRow.FindControl("lblLeaseName"), Label)
            txtstore.Text = lblLseName.Text

            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SURRENDER_LEASE_DETAILS")
            sp4.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp4.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtSDamount.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_LES_SEC_DEPOSIT"), 2, MidpointRounding.AwayFromZero)
                txtOutAmt.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_LES_TOT_RENT"), 2, MidpointRounding.AwayFromZero)
            End If
            LeaseSurrenderAmt()
            pnl1.Visible = True
        Else
            pnl1.Visible = False
        End If
        txtremarks.Focus()
    End Sub

    Private Sub InsertSurrenderDetails()

        Dim orgfilename As String = BrowsePossLtr.FileName
        Dim filename As String = ""

        Try
            If (BrowsePossLtr.HasFile) Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(BrowsePossLtr.FileName)
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
                BrowsePossLtr.PostedFile.SaveAs(filePath)
                filename = orgfilename
                'lblMsg.Visible = True
                'lblMsg.Text = "File upload successfully"

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_INSERT_LEASE_SURRENDER_DETAILS")
            sp.Command.AddParameter("@LEASEID", txtstore.Text, DbType.String)
            sp.Command.AddParameter("@SURRENDER_DATE", txtsurrender.Text, DbType.Date)
            sp.Command.AddParameter("@REFUND_DT", txtrefund.Text, DbType.Date)
            sp.Command.AddParameter("@SECU_DEPO", txtSDamount.Text, DbType.Decimal)
            sp.Command.AddParameter("@DAMAGES", txtDmg.Text, DbType.String)
            sp.Command.AddParameter("@DAMAGE_AMT", txtDmgAmt.Text, DbType.Decimal)
            sp.Command.AddParameter("@LESSE_AMT", txtAmtPaidLesse.Text, DbType.Decimal)
            sp.Command.AddParameter("@LESSOR_AMT", txtAmtPaidLessor.Text, DbType.Decimal)
            sp.Command.AddParameter("@OUTSTAND_AMT", txtOutAmt.Text, DbType.Decimal)
            sp.Command.AddParameter("@REMARKS", txtremarks.Text, DbType.String)
            sp.Command.AddParameter("@POSSESS_LETTER_UPLOAD", filename, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim Result As Integer = sp.ExecuteScalar()
            If Result = 1 Then
                lblMsg.Visible = True
                lblMsg.Text = "Lease Request Already Surrendered..!"
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Lease Request Surrendered Successfully"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        If (Convert.ToDecimal(txtSDamount.Text) = 0) Then
            'lblMsg.Text = "the refund amount should be less or equal to" + refund.ToString() + " and greater than 0"
            lblMsg.Text = "The Security Deposit Should Be Greater Than 0"
            Exit Sub
        Else
            InsertSurrenderDetails()
        End If
        'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=47")
        pnl1.Visible = False

    End Sub

    Protected Sub txtDmgAmt_TextChanged(sender As Object, e As EventArgs) Handles txtDmgAmt.TextChanged

        txtAmtPaidLesse.Text = 0
        txtAmtPaidLessor.Text = 0
        LeaseSurrenderAmt()

    End Sub

    Public Sub fillgridOnTenantCodeSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_FILTER_DETAILS")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@SEARCH", txtReqId.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvItems.DataSource = Session("dataset")
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        Try
            fillgridOnTenantCodeSearch()
            pnl1.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtReqId.Text = ""
        BindGrid()
        pnl1.Visible = False
    End Sub
End Class
