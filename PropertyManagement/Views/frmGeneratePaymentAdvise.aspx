<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmGeneratePaymentAdvise.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmGeneratePaymentAdvise" Title="Generate Payment Advise" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
   
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Update Work Payment Details
                            </legend>
                        </fieldset>
                       
                            <form id="form1" class="well" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                                <div class="clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Request <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                                                Display="None" ErrorMessage="Please Select Work Request" ValidationGroup="Val1"
                                                InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >City</label>
                                            <div  >
                                                <asp:TextBox ID="txtBuilding" runat="server" CssClass="form-control"
                                                    MaxLength="50" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Property</label>
                                            <div  >
                                                <asp:TextBox ID="txtproperty" runat="server" CssClass="form-control"
                                                    MaxLength="50" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Title</label>
                                            <div  >
                                                <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                                                    MaxLength="50" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Specifications</label>
                                            <div  >
                                                <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control"
                                                    Rows="3" TextMode="SingleLine" MaxLength="1000" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Estimated Amount</label>
                                            <div  >
                                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Vendor Name</label>
                                            <div  >
                                                <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control"
                                                    Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Status</label>
                                            <div  >
                                                <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true" Enabled="False">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Payment Mode <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="ddlpay"
                                                Display="None" ErrorMessage="Please Select Payment Mode" ValidationGroup="Val1"
                                                InitialValue="--Select Payment Mode--" Enabled="true"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:DropDownList ID="ddlpay" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                                    <asp:ListItem>--Select Payment Mode--</asp:ListItem>
                                                    <asp:ListItem Value="1">Cheque</asp:ListItem>
                                                    <asp:ListItem Value="2">Cash</asp:ListItem>
                                                    <asp:ListItem Value="3">Bank Transfer</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Remarks</label>
                                            <div  >
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" MaxLength="250"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div id="panel1" runat="server">
                                    <div class="panel panel-default " role="tab" runat="server" id="div3">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Cheque Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse in">
                                            <div class="panel-body color">
                                                <div class="form-group">
                                                    <div class="clearfix">
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label  >Cheque No<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvCheque" runat="server" ControlToValidate="txtCheque"
                                                                    Display="None" ErrorMessage="Please Enter Cheque Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="rvcheque" Display="None" runat="server" ControlToValidate="txtCheque"
                                                                    ErrorMessage="Invalid Cheque Number" ValidationExpression="^[A-Za-z0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                <div  >
                                                                    <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtCheque" runat="Server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label  >Bank Name<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                                    Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                <div  >
                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                        onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label  >Account Number<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                                    Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                                    ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                <div  >
                                                                    <div onmouseover="Tip('Enter  Numbers only and No Special Characters allowed ')"
                                                                        onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtAccNo" runat="Server" CssClass="form-control"></asp:TextBox>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="panel2" runat="Server">
                                    <div class="panel panel-default " role="tab" runat="server" id="div1">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Bank Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse in">
                                            <div class="panel-body color">
                                                <div class="form-group">
                                                    <div class="clearfix">
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label  >Issuing Bank Name<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                                                                    Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                                    ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                <div  >
                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                        onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtIBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label  >Deposited Bank<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                                                                    Display="None" ErrorMessage="Please Enter Deposited Bank" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtDeposited"
                                                                    ErrorMessage="Enter Valid Bank Branch" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                <div  >
                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                        onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtDeposited" runat="Server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label  >IFSC Code<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvIFCB" runat="server" ControlToValidate="txtIFCB"
                                                                    Display="none" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="REVIFCB" Display="None" runat="server" ControlToValidate="txtIFCB"
                                                                    ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                <div  >
                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                        onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtIFCB" runat="Server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">

                                            <asp:Button ID="btnsubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                                CausesValidation="true" />

                                        </div>
                                    </div>
                                </div>

                            </form>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



