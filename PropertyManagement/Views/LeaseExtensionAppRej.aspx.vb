﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_LeaseExtensionAppRej
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Request.QueryString("id") <> "" Then
            Session("id") = Request.QueryString("rid")
        End If
       
        If Not IsPostBack Then
            BindDetails()

        End If
        txttodate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXTENSION_DETAILS")
            sp.Command.AddParameter("@LEASE", Request.QueryString("ID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtstore.Text = Request.QueryString("ID")
                txtfromdate.Text = ds.Tables(0).Rows(0).Item("PM_LE_START_DT")
                txtrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                lblstrtdate.Text = ds.Tables(0).Rows(0).Item("START_DATE").ToString()
                lblenddate.Text = ds.Tables(0).Rows(0).Item("END_DATE").ToString()
                txtsdep.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                txttodate.Text = ds.Tables(0).Rows(0).Item("PM_LE_END_DT")
            End If
            pnl1.Visible = True

        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/PropertyManagement/Views/frmApproveLeaseExtension.aspx")
    End Sub


    Protected Sub btnapprove_Click(sender As Object, e As EventArgs) Handles btnapprove.Click
        Try
            If CDate(txttodate.Text) < CDate(txtfromdate.Text) Then
                lblMsg.Text = "Lease End Date should be greater than Start date"
                Exit Sub
            Else
                lblMsg.Text = ""
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_APPROVE")
                sp.Command.AddParameter("@LEASEID", Request.QueryString("ID"), DbType.String)
                sp.Command.AddParameter("@EXTENDED_DATE", txttodate.Text, DbType.Date)
                sp.Command.AddParameter("@APPRV_REMARKS", txtremarks.Text, DbType.String)
                sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                sp.ExecuteScalar()
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=60", False)


                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Approved Successfully"

            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnreject_Click(sender As Object, e As EventArgs) Handles btnreject.Click
        Try
            If CDate(txttodate.Text) < CDate(txtfromdate.Text) Then
                lblMsg.Text = "Lease End Date should be greater than Start date"
                Exit Sub
            Else
                lblMsg.Text = ""
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_REJECT")
                sp.Command.AddParameter("@LEASEID", Request.QueryString("ID"), DbType.String)
                sp.Command.AddParameter("@APPRV_REMARKS", txtremarks.Text, DbType.String)
                sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                sp.ExecuteScalar()
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=60", False)


                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Rejected Successfully"

            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub
End Class
