﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PropertyManagement_Views_TenExtensionApproval : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uid"].ToString() == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        if (!IsPostBack)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string host = HttpContext.Current.Request.Url.Host;

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 10);
            param[0].Value = Session["UID"];
            param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
            param[1].Value = path;
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
                }
            }
            BindGrid();
        }
    }

    private void BindGrid()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TENANT_DETAILS");
            sp.Command.AddParameter("@USER", Session["uid"], DbType.String);
            sp.Command.AddParameter("@COMPANY", Session["COMPANYID"], DbType.String);
            sp.Command.AddParameter("@FLAG", 8, DbType.Int32);
            gvLDetails_Lease.DataSource = sp.GetDataSet();
            gvLDetails_Lease.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void gvLDetails_Lease_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "ViewDetails")
            {
                // CALL SP AND FILL 3 TAB VALUES
                panel1.Visible = true;
                   GridViewRow gvRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = gvRow.RowIndex;
                hdnExtSNO.Value = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblExtSNO")).Text;
                hdnExtFrmDt.Value = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblExtsdate")).Text;
                hdnExtToDt.Value = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblExtEdate")).Text;    

                hdnPropCode.Value = Convert.ToString(e.CommandArgument);
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TENANT_DETAILS");
                sp.Command.AddParameter("@USER", Session["uid"], DbType.String);
                sp.Command.AddParameter("@PN_NAME", e.CommandArgument, DbType.String);
                sp.Command.AddParameter("@FLAG", 9, DbType.Int32);
                
                DataSet ds = sp.GetDataSet();

                lblproptype.Text = Convert.ToString(ds.Tables[0].Rows[0]["PN_PROPERTYTYPE"]);
                lblcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["CTY_NAME"]);
                lblLocation.Text = Convert.ToString(ds.Tables[0].Rows[0]["LCM_NAME"]);
                lblProperty.Text = Convert.ToString(ds.Tables[0].Rows[0]["PROPERTY_NAME"]);
                lblOccArea.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_OCCUP_AREA"]);
                lblTenFromDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_FRM_DT"]);
                lblTenToDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_TO_DT"]);

                lblTenant.Text = Convert.ToString(ds.Tables[0].Rows[0]["TENANT"]);
                lblTenCode.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_CODE"]);
                txtNoofparking.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_NO_OF_PARKING"]);

                txtRent.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_RENT"]);
                txtSecurityDeposit.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_SECURITY_DEPOSIT"]);
                txtDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_JOIN_DT"]);
                txtpayterms.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_PT_NAME"]);
                txtfees.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_ADDITION_PARKING_FEE"]);
                txtcar.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_MAINT_FEES"]);
                txtamount.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_TOT_RENT"]);
                txtReqRemarks.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TE_REMARKS"]);
            }
            else
            {
                panel1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void txtfees_TextChanged(object sender, EventArgs e)
    {
        if (txtRent.Text != null & txtfees.Text != null)
        {
            txtamount.ReadOnly = false;
            txtamount.Text = Convert.ToString(Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtfees.Text));
            txtamount.ReadOnly = true;
        }
        else
        {
            txtamount.Text = null;
        }
    }

    protected void txtRent_TextChanged(object sender, EventArgs e)
    {
        if (txtfees.Text != null & txtRent.Text != null)
        {
            txtamount.ReadOnly = false;
            txtamount.Text = Convert.ToString(Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text));
            txtamount.ReadOnly = true;
        }
        else
        {
            txtamount.Text = null;
        }
    }

    protected void gvLDetails_Lease_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        gvLDetails_Lease.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        Extension_AppRej(5002);
        lblmsg.Text = "Tenant Extension Approved Successfully";
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        Extension_AppRej(5003);
        lblmsg.Text = "Tenant Extension Rejected Successfully";
    }

    private void Extension_AppRej(int status)
    {
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_TEN_EXTENSION_APPROVAL");
        sp3.Command.AddParameter("@TEN_SNO", hdnPropCode.Value, DbType.String);
        sp3.Command.AddParameter("@EXTSNO", hdnExtSNO.Value, DbType.String);
        sp3.Command.AddParameter("@EXT_FROMDATE", hdnExtFrmDt.Value, DbType.Date);
        sp3.Command.AddParameter("@EXT_TODATE", hdnExtToDt.Value, DbType.Date);
        sp3.Command.AddParameter("@RENT", Convert.ToDecimal(txtRent.Text), DbType.Decimal);
        sp3.Command.AddParameter("@SEC_DEPOSIT", Convert.ToDecimal(txtSecurityDeposit.Text), DbType.String);
        sp3.Command.AddParameter("@ADD_FEE", Convert.ToDecimal(txtcar.Text), DbType.Decimal);
        sp3.Command.AddParameter("@MAIN_FEE", Convert.ToDecimal(txtfees.Text), DbType.Decimal);
        sp3.Command.AddParameter("@REMS", txtRemarks.Text, DbType.String);
        sp3.Command.AddParameter("@STATUS", status, DbType.String);
        sp3.Command.AddParameter("@CREATED_BY", Session["uid"], DbType.String);
        sp3.Command.AddParameter("@CMP_ID", Session["COMPANYID"], DbType.String);
        sp3.ExecuteScalar();
        BindGrid();
        panel1.Visible = false;
    }    
}