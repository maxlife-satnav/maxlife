Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmApproveLeaseExtension
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            BindGrid()
            'BindLeaseHistory()
        End If
    End Sub

    Private Sub BindGrid()
        Try
            lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_APPROVAL_GRID")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@LEASE_ID", txtLeaseId.Text, DbType.String)
            gvLDetailsLease.DataSource = sp.GetDataSet()
            gvLDetailsLease.DataBind()
            If gvLDetailsLease.DataSource.Tables(0).Rows.Count > 0 Then
                pnlbutton.Visible = True
            Else
                pnlbutton.Visible = False
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        BindGrid()
    End Sub

    Protected Sub gvLDetailsLease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetailsLease.PageIndexChanging
        gvLDetailsLease.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    'Protected Sub gvitems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
    '    gvitems1.PageIndex = e.NewPageIndex()
    '    BindLeaseHistory()
    'End Sub
    Private Sub UpdateLEAll(ByVal STA_ID As Integer)
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvLDetailsLease.Rows
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lbllname"), Label)
                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(3) {}
                    param(0) = New SqlParameter("@LEASEID", lblreqid.Text)
                    param(1) = New SqlParameter("@STA_ID", STA_ID)
                    param(2) = New SqlParameter("@APPRV_REMARKS", txtRemarks.Text)
                    param(3) = New SqlParameter("@AUR_ID", Session("UID").ToString())
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_LEASE_EXTENSION_APPROVE_REJECT", param)
                Else
                    ' lblMsg.Text = "Please Select Atleast One Checkbox"
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnAppall_Click(sender As Object, e As EventArgs) Handles btnAppall.Click

        For Each row As GridViewRow In gvLDetailsLease.Rows
            Dim chkselect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
            If chkselect.Checked = True Then
                UpdateLEAll(4018)

                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Approved Successfully"
            Else
                lblMsg.Text = "Please Select Atleast One Checkbox"
            End If
        Next

    End Sub

    Protected Sub btnRejAll_Click(sender As Object, e As EventArgs) Handles btnRejAll.Click

        For Each row As GridViewRow In gvLDetailsLease.Rows
            Dim chkselect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
            If chkselect.Checked = True Then
                UpdateLEAll(4019)

                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Rejected Successfully"
            Else
                'lblMsg.Text = "Please Select Atleast One Checkbox"
            End If
        Next

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtLeaseId.Text = ""
        BindGrid()
    End Sub
End Class