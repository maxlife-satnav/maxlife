Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmApproval
    Inherits System.Web.UI.Page
    Dim code As String
    Dim TotalRent As Decimal
    Dim TotalRentPaid As Decimal
    Dim ClosingBalance As Decimal
    Dim SecurityDeposit As Decimal
    Dim Flag As Integer
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_TENANT_MASTER_DETAILS")
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindUser()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENUSER")
            sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VIEW_TENANT_DETAILS")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()

            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETTNTDTL")
            sp.Command.AddParameter("@dummy", hdnSNO.Value, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                Dim li4 As ListItem = Nothing
                li4 = ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_PROP_TYPE"))
                If Not li4 Is Nothing Then
                    li4.Selected = True
                End If
                Dim li2 As ListItem = Nothing
                li2 = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_ADM_CODE"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                End If


                BindCityLoc()


                Dim liLoc As ListItem = Nothing
                liLoc = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_ID"))
                If Not liLoc Is Nothing Then
                    liLoc.Selected = True
                End If


                BindProp()
                Dim li As ListItem = Nothing
                li = ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PN_NAME"))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim LI5 As ListItem = Nothing
                'LI5 = ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("TEN_STATUS"))
                'If Not LI5 Is Nothing Then
                '    LI5.Selected = True
                'End If

                Dim li3 As ListItem = Nothing
                li3 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("Aur_Id"))
                If Not li3 Is Nothing Then
                    li3.Selected = True
                End If

                txtcode.Text = ds.Tables(0).Rows(0).Item("TEN_CODE")
                txtname.Text = ds.Tables(0).Rows(0).Item("TEN_NAME")
                txtTenantOccupiedArea.Text = ds.Tables(0).Rows(0).Item("TEN_OCCUPIEDAREA")
                txtRent.Text = ds.Tables(0).Rows(0).Item("TEN_RENT_PER_SFT")
                txtDate.Text = ds.Tables(0).Rows(0).Item("TEN_COMMENCE_DATE")
                txtSecurityDeposit.Text = ds.Tables(0).Rows(0).Item("TEN_SECURITY_DEPOSIT")

                ddlPaymentTerms.SelectedValue = ds.Tables(0).Rows(0).Item("TEN_PAYMENTTERMS")
                txtPayableDate.Text = ds.Tables(0).Rows(0).Item("TEN_NEXTPAYABLEDATE")

                txtNoofparking.Text = ds.Tables(0).Rows(0).Item("TEN_NO_PARKING")
                txtfees.Text = ds.Tables(0).Rows(0).Item("TEN_MAINT_FEE")
                txtamount.Text = ds.Tables(0).Rows(0).Item("TEN_OUTSTANDING_AMOUNT")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("TEN_REMARKS")
                txtSurrenderDt.Text = ds.Tables(0).Rows(0).Item("SurrenderDate")

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Private Sub UpdateStatus()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_TENANT_MASTER_REJECT")
            'sp.Command.AddParameter("@TEN_CODE", hdnSNO.Value, DbType.String)
            sp.Command.AddParameter("@SNO", hdnSNO.Value, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdateUserStat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_STA_AU")
        sp.Command.AddParameter("@AUR_ID", ddluser.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Public Sub SearchfilterGrid(Flag)
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@FLAG", Flag, DbType.Int32)
            sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvItems.DataSource = Session("dataset")
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPaymentTerms()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PAYMENT_TERMS")
            ddlPaymentTerms.DataSource = sp.GetDataSet()
            ddlPaymentTerms.DataTextField = "PM_PT_NAME"
            ddlPaymentTerms.DataValueField = "PM_PT_SNO"
            ddlPaymentTerms.DataBind()
            ddlPaymentTerms.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdatePropertyStat(Flag)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_UPDATE_PROPERTY_STATUS")
        sp.Command.AddParameter("@SNO", Convert.ToInt32(HttpContext.Current.Session("Tenant_Count")), DbType.Int32)
        If Not txtPayableDate.Text = "" Then
            sp.Command.AddParameter("@PM_TS_NXT_PAY", txtPayableDate.Text, DbType.DateTime)
        End If
        sp.Command.AddParameter("@REMARKS", txtApprRemarks.Text, DbType.String)
        sp.Command.AddParameter("@FLAG", Flag, DbType.Int32)
        sp.Command.AddParameter("@ApprovedBy", HttpContext.Current.Session("Uid"), DbType.String)
        sp.ExecuteScalar()
    End Sub
    Public Sub GetTenantClosingBalance()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_CLOSING_BALANCE")
        sp.Command.AddParameter("@SNO", hdnSNO.Value, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            TotalRent = ds.Tables(0).Rows(0).Item("TotalRent")
            TotalRentPaid = ds.Tables(0).Rows(0).Item("TotalRentPaid")
            ClosingBalance = ds.Tables(0).Rows(0).Item("ClosingBalance")
            SecurityDeposit = ds.Tables(0).Rows(0).Item("TEN_SECURITY_DEPOSIT")
        End If
    End Sub
    Private Sub UpdateSurenderProperty(ByVal PNAME As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_LOCATIONS_PROPERTY")
        sp.Command.AddParameter("@PN_NAME", PNAME, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            panel1.Visible = False
            BindGrid()
            BindPaymentTerms()
            txtDate.Text = getoffsetdate(Date.Today)
        End If
        txtDate.Attributes.Add("readonly", "readonly")
        txtPayableDate.Attributes.Add("readonly", "readonly")
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            UpdatePropertyStat(1)
            lblmsg.Visible = True
            lblmsg.Text = "Request Has Been Approved Successfully."
            panel1.Visible = False
            BindGrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            'BindProp()
            BindCityLoc()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub
    Protected Sub ddlproptype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlproptype.SelectedIndexChanged
        If ddlproptype.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            BindProp()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub
    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        BindUser()
        BindPropType()
        BindCity()
        Dim lnkApprove As LinkButton = DirectCast(e.CommandSource, LinkButton)
        Dim gvRow As GridViewRow = DirectCast(lnkApprove.NamingContainer, GridViewRow)
        Dim lbltenantcode As Label = DirectCast(gvRow.FindControl("lbltenantcode"), Label)
        Dim pname As Label = DirectCast(gvRow.FindControl("lblpropertyname"), Label)
        Dim SNO As Label = DirectCast(gvRow.FindControl("lblUniqueNo"), Label)
        Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
        If chkSelect.Checked = True Then
            lblmsg.Text = ""
            hdnPN.Value = pname.Text
            hdnSNO.Value = SNO.Text
            Dim code As String = lbltenantcode.Text
            If e.CommandName = "Approve" Then
                panel1.Visible = True
                Dim currentpageindex As Integer = 0
                If Session("CurrentPageIndex") <> Nothing Then
                    currentpageindex = (Integer.Parse(Session("CurrentPageIndex")))
                End If
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString()) - (currentpageindex * 5)
                Dim lblid As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblsno"), Label)
                Dim Serial As Integer = lblid.Text
                Session("Tenant_Count") = Serial
                Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENANT_DETAILS")
                sp4.Command.AddParameter("@SNO", Serial, DbType.Int32)
                sp4.Command.AddParameter("@flag", 2, DbType.Int32)
                Dim ds As New DataSet
                ds = sp4.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    txtcode.Text = ds.Tables(0).Rows(0).Item("PM_TEN_CODE")
                    txtname.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                    Dim li0 As ListItem = Nothing
                    li0 = ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPERTY_TYPE"))
                    If Not li0 Is Nothing Then
                        li0.Selected = True
                    End If
                    Dim li1 As ListItem = Nothing
                    li1 = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_CITY_CODE"))
                    If Not li1 Is Nothing Then
                        li1.Selected = True
                    End If
                    BindCityLoc()
                    Dim li2 As ListItem = Nothing
                    li2 = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_LOC_CODE"))
                    If Not li2 Is Nothing Then
                        li2.Selected = True
                    End If
                    BindProp()
                    Dim li3 As ListItem = Nothing
                    li3 = ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPRTY"))
                    If Not li3 Is Nothing Then
                        li3.Selected = True
                    End If
                    txtTenantOccupiedArea.Text = ds.Tables(0).Rows(0).Item("PM_TEN_OCCUP_AREA")
                    txtRent.Text = ds.Tables(0).Rows(0).Item("PM_TD_RENT")
                    txtDate.Text = ds.Tables(0).Rows(0).Item("PM_TD_JOIN_DT")
                    txtSecurityDeposit.Text = ds.Tables(0).Rows(0).Item("PM_TD_SECURITY_DEPOSIT")
                    ddlPaymentTerms.SelectedValue = ds.Tables(0).Rows(0).Item("PM_TD_PAY_TERMS")
                    txtNoofparking.Text = ds.Tables(0).Rows(0).Item("PM_TEN_NO_OF_PARKING")
                    txtfees.Text = ds.Tables(0).Rows(0).Item("PM_TD_MAINT_FEES")
                    txtamount.Text = ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("PM_TD_REMARKS")
                    txtSurrenderDt.Text = ds.Tables(0).Rows(0).Item("PM_TS_SURRENDER_DT")
                    txtTotalRent.Text = ds.Tables(0).Rows(0).Item("PM_TS_TOT_RENT_AMT")
                    txtRentPaid.Text = ds.Tables(0).Rows(0).Item("PM_TS_RENT_AMT_PAID")
                    txtRentPaid.Text = ds.Tables(0).Rows(0).Item("PM_TS_RENT_AMT_PAID")
                    BindUser()
                    Dim li7 As ListItem = Nothing
                    li7 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_NAME"))
                    If Not li7 Is Nothing Then
                        li7.Selected = True
                    End If
                    Dim Rent As Double = ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT")
                    Dim SecurityDetails As Double = ds.Tables(0).Rows(0).Item("PM_TD_SECURITY_DEPOSIT")
                    Dim TotalAmount As Double = ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT")
                    Dim resultamount As Integer = ((SecurityDetails + Rent) - TotalAmount)
                    Session("result_amount") = resultamount
                    txtTenClosingBal.Text = Session("result_amount")
                End If
            Else
                panel1.Visible = False
                UpdateStatus()
                lblmsg.Visible = True
                lblmsg.Text = "Surrender Request Rejected Successfully"
                'BindGrid()
            End If
        Else
            lblmsg.Text = "Please Select Atleast One Checkbox"
        End If
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            SearchfilterGrid(5)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click

        UpdatePropertyStat(2)
        lblmsg.Visible = True
        lblmsg.Text = "Request Has Been Rejected Successfully."
    End Sub
End Class

