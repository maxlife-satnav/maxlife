﻿Imports System.Data
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports System.Configuration
Imports SubSonic
Imports System.IO
Imports System.Net
Imports System

Partial Class PropertyManagement_Views_PreValidation
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            BindGrid()
        End If
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") + "." + "PM_GET_LEASE_DATA_ON_L4_APPROVAL")
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session("UID").ToString(), DbType.[String])
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()

    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") + "." + "PM_GET_LEASE_DATA_SEARCH_BY")
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session("UID").ToString(), DbType.[String])
        sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.[String])
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
    End Sub

    Protected Sub txtreset_Click(sender As Object, e As EventArgs)
        BindGrid()
        txtReqId.Text = ""
    End Sub
    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        BindGrid()
        gvItems.PageIndex = e.NewPageIndex
        gvItems.DataBind()
    End Sub
    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "GetLeaseDetails" Then
                AddLeaseDetails.Visible = True

                hdnLSNO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim ReqID As String = DirectCast(row.FindControl("lblReqID"), LinkButton).Text.ToString()
                Session("REQ_ID") = ReqID
                '  btnSearch.Text = "Add more properties to " + hdnLSNO.Value
                lblMsg.Text = ""
                GetSelectedLeaseDetails(hdnLSNO.Value)
                'BindPropertyImages()
                'BindLegalDocuments(hdnLSNO.Value)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    'Private Sub BindLegalDocuments(ByVal PM_LES_SNO As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEGAL_DOCS_OF_LEASE_BY_SNO")
    '    sp.Command.AddParameter("@PM_LES_SNO", PM_LES_SNO, DbType.Int32)
    '    gvLegalDocs.DataSource = sp.GetDataSet()
    '    gvLegalDocs.DataBind()

    'End Sub
    'Public Sub BindPropertyImages()
    '    Dim dtDocs As New DataTable("Documents")
    '    param = New SqlParameter(0) {}
    '    param(0) = New SqlParameter("@PM_LES_SNO", SqlDbType.NVarChar, 50)
    '    param(0).Value = hdnLSNO.Value
    '    Dim ds As New DataSet
    '    ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_PROEPRTY_IMAGES", param)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        grdDocs.DataSource = ds
    '        grdDocs.DataBind()
    '        tblGridDocs.Visible = True
    '        lblDocsMsg.Text = ""
    '    Else
    '        tblGridDocs.Visible = False
    '        lblMsg.Visible = True
    '        lblDocsMsg.Text = "No Property Images Available"
    '    End If
    '    dtDocs = Nothing
    'End Sub

    Private Sub GetSelectedLeaseDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GRID_VIEW_MODIFY")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        SetLeaseValuesToUpdate(ds)
        updatepanel.Visible = True
    End Sub
    Private Sub SetLeaseValuesToUpdate(ByVal leaseAllSections As DataSet)
        Dim LeaseNAreaNDetails As DataTable
        Dim charges As DataTable
        Dim agreementDetails As DataTable
        Dim brokerageDt As DataTable
        Dim UtilityDt As DataTable
        Dim OtherServicesDt As DataTable
        Dim OtherDetailsDt As DataTable
        Dim LeaseDetailsDt As DataTable
        Dim LeaseExpDt As DataTable
        Dim POA As DataTable
        Dim PropertyDetails As DataTable
        Dim OfficeConnectivity As DataTable
        Dim Civil As DataTable
        Dim Damages As DataTable

        LeaseNAreaNDetails = leaseAllSections.Tables(0)
        charges = leaseAllSections.Tables(2)
        agreementDetails = leaseAllSections.Tables(3)
        brokerageDt = leaseAllSections.Tables(4)
        UtilityDt = leaseAllSections.Tables(5)
        OtherServicesDt = leaseAllSections.Tables(6)
        OtherDetailsDt = leaseAllSections.Tables(7)
        LeaseDetailsDt = leaseAllSections.Tables(8)
        LeaseExpDt = leaseAllSections.Tables(9)
        POA = leaseAllSections.Tables(10)
        RentRevision = leaseAllSections.Tables(11)
        PropertyDetails = leaseAllSections.Tables(12)
        OfficeConnectivity = leaseAllSections.Tables(13)
        Civil = leaseAllSections.Tables(14)
        Damages = leaseAllSections.Tables(15)
        'BindLeaseExpences()
        BindLeaseExpences()
        'Property Details section start
        txtAge.Text = PropertyDetails.Rows(0).Item("PM_PPT_AGE")
        txtPropIDName.Text = PropertyDetails.Rows(0).Item("PM_PPT_NAME")
        txtCarpetArea.Text = PropertyDetails.Rows(0).Item("PM_AR_CARPET_AREA")
        txtBuiltupArea.Text = PropertyDetails.Rows(0).Item("PM_AR_BUA_AREA")
        txtEfficiency.Text = PropertyDetails.Rows(0).Item("PM_AR_PREF_EFF")
        txtNoofFloors.Text = PropertyDetails.Rows(0).Item("PM_PPT_TOT_FLRS")
        txtCeilingHight.Text = PropertyDetails.Rows(0).Item("PM_AR_FTC_HIGHT")
        txtBeamBottomHight.Text = PropertyDetails.Rows(0).Item("PM_AR_FTBB_HIGHT")
        TxtFlooringType.Text = PropertyDetails.Rows(0).Item("PM_FT_TYPE")
        txtSignageLocation.Text = PropertyDetails.Rows(0).Item("PM_PPT_SIGN_LOC")
        txtToilet.Text = PropertyDetails.Rows(0).Item("PM_PPT_TOT_TOI")

        txtInvestedArea.Text = PropertyDetails.Rows(0).Item("PM_BASIC_RENT")
        txtmain1.Text = PropertyDetails.Rows(0).Item("PM_MAINT_CHRG")

        txttotalrent.Text = PropertyDetails.Rows(0).Item("TOTALRENT")

        Dim costtype As String
        costtype = PropertyDetails.Rows(0).Item("OD_COST_TYPE")
        rblCostType.SelectedValue = costtype
        If costtype = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtRentPerSqftCarpet.Text = PropertyDetails.Rows(0).Item("OD_RENT_PER_SQFT_BUA")
            txtRentPerSqftBUA.Text = PropertyDetails.Rows(0).Item("OD_RENT_PER_SQFT_CARPET")

        ElseIf costtype = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtSeatCost.Text = PropertyDetails.Rows(0).Item("OD_SEAT_COST")
        End If
        txtOfficeType.Text = PropertyDetails.Rows(0).Item("PM_OFFICE_TYPE")

        txtshopnumberoccupy.Text = PropertyDetails.Rows(0).Item("PM_SHOP_NUMBER_TO_OCCUPY")
        txtTermofLease.Text = PropertyDetails.Rows(0).Item("PM_TERMS_OF_LEASE")

        ddlRollingShutter.ClearSelection()
        Dim rolShutter As String = OtherDetailsDt.Rows(0)("PM_LO_ROL_SHUTTER")
        ddlRollingShutter.Items.FindByValue(rolShutter).Selected = True
        'proerty details section end


        'Lease Details Section Start
        If LeaseNAreaNDetails.Rows(0)("PM_LES_STA_ID") = 4024 Then
            lblMsg.Text = "Plese add at least one landlord to add lease successfully"
        Else
            lblMsg.Text = ""
        End If
        'txtNoLanlords.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_NO_OF_LL")
        'Lease Details Section End

        'Area & Cost Details Section start
        'ddlSecurityDepMonths.ClearSelection()
        'Dim secrtyDepositMonths As String = LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEP_MONTHS")
        'ddlSecurityDepMonths.Items.FindByValue(secrtyDepositMonths).Selected = True

        'ddlTenure.ClearSelection()
        'BindTenure()
        'Dim tenure As String = LeaseNAreaNDetails.Rows(0)("PM_LES_TENURE")
        'ddlTenure.Items.FindByValue(tenure).Selected = True
        'Dim costType As String = LeaseNAreaNDetails.Rows(0)("PM_LES_COST_TYPE")
        'rblCostType.Items.FindByValue(costType).Selected = True
        'If String.Equals(costType, "Seat") Then
        '    Costype1.Visible = False
        '    Costype2.Visible = True

        'Else
        '    Costype1.Visible = True
        '    Costype2.Visible = False

        'End If

        'txtSeatCost.Text = IIf(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST") = 0, 0, Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST")))
        'txtRentPerSqftCarpet.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_CARPET"))
        'txtRentPerSqftBUA.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_BUA"))

        'txtLnumber.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_CTS_NO")
        'txtentitle.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_ENTITLED_AMT"))
        'txtInvestedArea.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_BASIC_RENT"))
        'txtpay.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEPOSIT"))
        txtRentFreePeriod.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_FREE_PERIOD")
        'txtInteriorCost.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_INTERIOR_COST"))
        'Area & Cost Details Section end

        'Charges Section start
        'txtregcharges.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_REGS_CHARGES"))
        'txtsduty.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_STMP_DUTY_CHARGES"))
        'txtfurniture.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_FF_CHARGES"))
        'txtbrokerage.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_CONSLBROKER_CHARGES"))
        'txtpfees.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROF_CHARGES"))
        txtmain1.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_MAINT_CHARGES"))
        txtservicetax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_SERVICE_TAX"))
        txtproptax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROPERTY_TAX"))

        'txtOtherTax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_OTHER_TAX"))

        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
        'Charges Section end

        'Agreement Details Section start
        'txtsdate.Text = agreementDetails.Rows(0)("PM_LAD_EFFE_DT_AGREEMENT")
        'txtedate.Text = agreementDetails.Rows(0)("PM_LAD_EXP_DT_AGREEMENT")
        'txtlock.Text = agreementDetails.Rows(0)("PM_LAD_LOCK_INPERIOD")
        'txtLeasePeiodinYears.Text = agreementDetails.Rows(0)("PM_LAD_LEASE_PERIOD")
        'txtNotiePeriod.Text = agreementDetails.Rows(0)("PM_LAD_NOTICE_PERIOD")
        'ddlAgreementbyPOA.ClearSelection()
        'Dim AgreementbyPOA As String = agreementDetails.Rows(0)("PM_LES_SIGNED_POA")
        'ddlAgreementbyPOA.Items.FindByValue(AgreementbyPOA).Selected = True
        'If ddlAgreementbyPOA.SelectedValue = "Yes" Then
        '    panPOA.Visible = True
        '    txtPOAName.Text = POA.Rows(0)("PM_POA_NAME")
        '    txtPOAAddress.Text = POA.Rows(0)("PM_POA_ADDRESS")
        '    txtPOAMobile.Text = POA.Rows(0)("PM_POA_PHONE_NO")
        '    txtPOAEmail.Text = POA.Rows(0)("PM_POA_EMAIL")
        'Else
        '    panPOA.Visible = False
        'End If
        'txtReminderEmail.Text = agreementDetails.Rows(0)("EMAIL_ADDRESS")
        'Agreement Details Section end
        'BindRentRevision()

        'Brokerage Section start
        'txtbrkamount.Text = Convert.ToDouble(brokerageDt.Rows(0)("PM_LBD_PAID_AMOUNT"))
        'txtbrkname.Text = brokerageDt.Rows(0)("PM_LBD_NAME")
        'txtbrkpan.Text = brokerageDt.Rows(0)("PM_LBD_PAN_NO")
        'txtbrkmob.Text = brokerageDt.Rows(0)("PM_LBD_PHONE_NO")
        'txtbrkremail.Text = brokerageDt.Rows(0)("PM_LBD_EMAIL")
        'txtbrkaddr.Text = brokerageDt.Rows(0)("PM_LBD_ADDRESS")
        'Brokerage Section end

        'Utility/Power back up Details Section start
        ddlDgSet.ClearSelection()
        Dim dgSetVal As String = UtilityDt.Rows(0)("PM_LUP_DGSET")
        ddlDgSet.Items.FindByValue(dgSetVal).Selected = True

        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If

        txtDgSetPerUnit.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_COMMER_PER_UNIT")
        txtDgSetLocation.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_LOCATION")
        txtSpaceServoStab.Text = UtilityDt.Rows(0)("PM_LUP_SPACE_SERVO_STABILIZER")

        ddlElectricalMeter.ClearSelection()
        Dim eleMeter As String = UtilityDt.Rows(0)("PM_LUP_ELE_METER")
        ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        'ele meter yes start

        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If

        txtMeterLocation.Text = UtilityDt.Rows(0)("PM_LUP_MET_LOCATION")
        txtEarthingPit.Text = UtilityDt.Rows(0)("PM_LUP_EARTHING_PIT")
        txtAvailablePower.Text = UtilityDt.Rows(0)("PM_LUP_AVAIL_POWER")
        txtAdditionalPowerKWA.Text = UtilityDt.Rows(0)("PM_LUP_ADDITIONAL_POWER")
        txtPowerSpecification.Text = UtilityDt.Rows(0)("PM_LUP_POWER_SPECIFICATION")


        txtpathElecCable.Text = UtilityDt.Rows(0)("PM_LUP_PATH_ELEC_CABLE")
        txtgroundEarthling.Text = UtilityDt.Rows(0)("PM_LUP_GROUND_EARTH")
        txtearthType.Text = UtilityDt.Rows(0)("PM_LUP_EARTH_TYPE")
        txtLiftPowerCut.Text = UtilityDt.Rows(0)("PM_LUP_LIFT_POWERCUT")
        txtTransLocation.Text = UtilityDt.Rows(0)("PM_LUP_TRANSFORMER_LOCATION")
        txtWaterConnection.Text = UtilityDt.Rows(0)("PM_LUP_WATER_CONNECTION")



        ' ele meter yes end

        'Utility/Power back up Details Section end

        'Other Services Section start
        txtNoOfTwoWheelerParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_TWO_PARK")
        txtNoOfCarsParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_CAR_PARK")
        txtDistanceFromAirPort.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_AIRPT")
        txtDistanceFromRailwayStation.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_RAIL")
        txtDistanceFromBustop.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_BUS")


        ddlLift.SelectedValue = OtherServicesDt.Rows(0)("PM_LO_LIFT_YES_NO")

        If ddlLift.SelectedValue = "Yes" Then
            divLift.Visible = True

            txtMake.Text = OtherServicesDt.Rows(0)("PM_LO_MAKE")
            txtCapacity.Text = OtherServicesDt.Rows(0)("PM_LO_CAPACITY")
            txtStairCaseWidth.Text = OtherServicesDt.Rows(0)("PM_LO_MAIN_STAIRCASE_WIDTH")
            txtFireStaircase.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_STAIRCASE")


        Else
            divLift.Visible = False

            txtACOutdoor.Text = OtherServicesDt.Rows(0)("PM_LO_AC_OUTDOOR_SPACE")
            anyACSatloc.Text = OtherServicesDt.Rows(0)("PM_LO_ACS_AT_LOCATION")
            txtsignOrbrand.Text = OtherServicesDt.Rows(0)("PM_LO_SIGNAGE_BRANDING")
            txtFireExitstairs.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_EXIT_STAIRS")
            txtFireFightingSystem.Text = OtherServicesDt.Rows(0)("PM_LO_FIRE_FIGHTING_SYSTEM")
            txtLiftNLicense.Text = OtherServicesDt.Rows(0)("PM_LO_LIFTS_AVAILABLE_LICENSE")
            txtMaintAgencyLift.Text = OtherServicesDt.Rows(0)("PM_LO_MAINT_AGENCY_LIFT")

        End If


        'Other Services Section end

        'Other Details Section start
        txtCompetitorsVicinity.Text = OtherDetailsDt.Rows(0)("PM_LO_COMP_VICINITY")

        'txtOfficeEquipments.Text = OtherDetailsDt.Rows(0)("PM_LO_OFF_EQUIPMENTS")
        'Other Details Section end

        'Lease Escalation Details Section start
        'ddlesc.ClearSelection()
        'Dim leaseEsc As String = LeaseDetailsDt.Rows(0)("PM_LD_LES_ESCALATON")
        'ddlesc.Items.FindByValue(leaseEsc).Selected = True
        'ddlLeaseEscType.ClearSelection()
        'Dim leaseEscType As String = LeaseDetailsDt.Rows(0)("PM_LD_ESCALTION_TYPE")
        'ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
        'txtLeaseHoldImprovements.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_IMPROV")
        'txtComments.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_COMMENTS")
        'ddlDueDilegence.ClearSelection()
        'Dim dueDilegence As String = LeaseDetailsDt.Rows(0)("PM_LD_DUE_DILIGENCE_CERT")
        'ddlDueDilegence.Items.FindByValue(dueDilegence).Selected = True

        'Dim reminderBefore As String
        'reminderBefore = LeaseDetailsDt.Rows(0).Item("PM_LRE_REMINDER_BEFORE")
        'Dim parts As String() = reminderBefore.Split(New Char() {","c})
        'For i As Integer = 0 To parts.Length - 1
        '    For j As Integer = 0 To ReminderCheckList.Items.Count - 1
        '        If ReminderCheckList.Items(j).Value = parts(i) Then
        '            ReminderCheckList.Items(j).Selected = True
        '        End If
        '    Next
        'Next
        'Lease Escalation Details Section end


        'office connectivity start
        txtTeleSPB.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_SP_BUILDING")
        txtTeleSPFO.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_SP_FIRST_OCCUPANT")
        txtLeaseSPB.Text = OfficeConnectivity.Rows(0)("PM_OC_LEASE_LINE_SP_BUILD")
        txtLeaseSPFO.Text = OfficeConnectivity.Rows(0)("PM_OC_LEASE_LINE_SP_FIRST_OCCPNT")
        txtTeleVenFR.Text = OfficeConnectivity.Rows(0)("PM_OC_TEL_VEN_FEASIBILITY")
        txtITVenFR.Text = OfficeConnectivity.Rows(0)("PM_OC_IT_VEN_FEASIBILITY")
        'office connectivity end

        'CIVIL SECTION START
        txtPunnWall.Text = Civil.Rows(0)("PM_C_PUNNG_WALLS")
        txtCracks.Text = Civil.Rows(0)("PM_C_CRACKS")
        txtWashToilet.Text = Civil.Rows(0)("PM_C_TOILET")
        txtPantry.Text = Civil.Rows(0)("PM_C_PANTRY")
        txtDrainagelinedrawings.Text = Civil.Rows(0)("PM_C_DRAINAGE_DRAWNGS")
        txtWindowHeight.Text = Civil.Rows(0)("PM_C_WINDOW_HEIGHT")

        txtWaterLeak.Text = Civil.Rows(0)("PM_C_WATER_LEAKAGE")
        txtStaircase.Text = Civil.Rows(0)("PM_C_STAIRCASE")
        txtEmergExit.Text = Civil.Rows(0)("PM_C_EMRG_EXIT")
        txtHeightAvailability.Text = Civil.Rows(0)("PM_C_HEIGH_AVAILABILITY")
        txtAddtnlCivilWork.Text = Civil.Rows(0)("PM_C_ADD_CIVIL_WORK")
        'CIVIL SECTION END

        'out standing amounts/damages start
        'ddlOutOrDamagesAmount.SelectedValue = Damages.Rows(0)("PM_D_ANY_DAMAGES")
        'ddlDamangesExtStay.SelectedValue = Damages.Rows(0)("PM_D_EXTND_STAY")
        'ddlPossessionHandover.SelectedValue = Damages.Rows(0)("PM_D_HAND_OVER")
        'txtOfficeEquipments.Text = Damages.Rows(0)("PM_D_OFFC_EQUIPMNTS")
        'txtWorkHrs.Text = Damages.Rows(0)("PM_D_WRKNG_HRS_CONSTRUCTION")
        'txtCommonToilet.Text = Damages.Rows(0)("PM_D_USAGE_TOILET")
        'txtServiceLift.Text = Damages.Rows(0)("PM_D_USAGE_LIFT")
        'txtSecSystem.Text = Damages.Rows(0)("PM_D_SECURITY_SYSTEM")
        'out standing amounts/damages end

        'ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
        'ViewState("LES_TOT_RENT") = Convert.ToDecimal(txttotalrent.Text)
        ''ViewState("LES_SEC_DEPOSIT") = Convert.ToDecimal(txtpay.Text)

        'gvlandlordItems.DataSource = leaseAllSections.Tables(1)
        'gvlandlordItems.DataBind()
        'If leaseAllSections.Tables(1).Rows.Count >= ViewState("NO_OF_LL") Then
        '    btnAddNewLandlord.Enabled = False
        'Else
        '    btnAddNewLandlord.Enabled = True
        '    txtName.Focus()
        'End If

    End Sub

    'Protected Sub EditLeaseExpense(sender As Object, e As GridViewEditEventArgs)
    '    gvLeaseExpences.EditIndex = e.NewEditIndex
    '    BindLeaseExpences()
    'End Sub
    'Private Sub BindLeaseExpences()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXPENSES_OF_LEASE")
    '    sp.Command.AddParameter("@PM_LES_SNO", hdnLSNO.Value, DbType.String)
    '    gvLeaseExpences.DataSource = sp.GetDataSet()
    '    gvLeaseExpences.DataBind()
    'End Sub
    'Protected Sub UpdateLeaseExpense(sender As Object, e As GridViewUpdateEventArgs)
    '    Dim ddlServiceProvider As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlServiceProvider"), DropDownList).SelectedItem.Value
    '    Dim ddliptype As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddliptype"), DropDownList).SelectedItem.Value
    '    Dim ddlPaidBy As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlPaidBy"), DropDownList).SelectedItem.Value
    '    Dim PM_EXP_SNO As String = gvLeaseExpences.DataKeys(e.RowIndex).Value.ToString()
    '    Dim code As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("lblcode"), Label).Text
    '    Dim compValue As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("txtCompValue"), TextBox).Text
    '    Dim Flag As Integer
    '    Dim param(9) As SqlParameter
    '    'Area &Cost
    '    param(0) = New SqlParameter("@PM_EXP_SERV_PROVIDER", SqlDbType.VarChar)
    '    param(0).Value = ddlServiceProvider
    '    param(1) = New SqlParameter("@PM_EXP_INP_TYPE", SqlDbType.VarChar)
    '    param(1).Value = ddliptype
    '    param(2) = New SqlParameter("@PM_EXP_COMP_LES_VAL", SqlDbType.VarChar)
    '    param(2).Value = compValue
    '    param(3) = New SqlParameter("@PM_EXP_PAID_BY", SqlDbType.VarChar)
    '    param(3).Value = ddlPaidBy

    '    param(4) = New SqlParameter("@PM_EXP_SNO", SqlDbType.VarChar)
    '    param(4).Value = PM_EXP_SNO

    '    If PM_EXP_SNO = "" Then
    '        Flag = 2 ' Insert
    '    Else
    '        Flag = 1 ' Update
    '    End If

    '    param(5) = New SqlParameter("@FLAG", SqlDbType.VarChar)
    '    param(5).Value = Flag

    '    param(6) = New SqlParameter("@PM_EXP_PM_LES_SNO", SqlDbType.VarChar)
    '    param(6).Value = hdnLSNO.Value

    '    param(7) = New SqlParameter("@PM_EXP_HEAD", SqlDbType.VarChar)
    '    param(7).Value = code

    '    param(8) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
    '    param(8).Value = Session("Uid").ToString

    '    param(9) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
    '    param(9).Value = Session("REQ_ID")

    '    Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_UPDATE_LEASE_EXPENSES", param)
    '        While sdr.Read()
    '            If sdr("result").ToString() = "SUCCESS" Then
    '                lblMsg.Text = "Lease Expenses Updated Successfully : " + Session("REQ_ID")
    '                BindLeaseExpences()
    '            Else
    '                lblMsg.Text = "Something went wrong. Please try again later."
    '            End If
    '        End While
    '        sdr.Close()
    '    End Using
    'End Sub
    'Protected Sub RowDataBound(sender As Object, e As GridViewRowEventArgs)
    '    If e.Row.RowType = DataControlRowType.DataRow AndAlso gvLeaseExpences.EditIndex = e.Row.RowIndex Then
    '        Dim ddlSerpr As DropDownList = DirectCast(e.Row.FindControl("ddlServiceProvider"), DropDownList)
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SERVICE_PROVIDER_DDL")
    '        ddlSerpr.DataSource = sp.GetDataSet().Tables(0)
    '        ddlSerpr.DataTextField = "PM_SP_NAME"
    '        ddlSerpr.DataValueField = "PM_SP_SNO"
    '        ddlSerpr.DataBind()

    '        If Not TryCast(e.Row.FindControl("lblspno"), Label).Text = "" Then
    '            ddlSerpr.Items.FindByValue(TryCast(e.Row.FindControl("lblspno"), Label).Text).Selected = True
    '        End If

    '        If Not TryCast(e.Row.FindControl("lblipname"), Label).Text = "" Then
    '            Dim ddliptype As DropDownList = DirectCast(e.Row.FindControl("ddliptype"), DropDownList)
    '            ddliptype.Items.FindByValue(TryCast(e.Row.FindControl("lblipname"), Label).Text).Selected = True
    '        End If

    '        If Not TryCast(e.Row.FindControl("lblPaidby"), Label).Text = "" Then

    '            Dim ddlPaidBy As DropDownList = DirectCast(e.Row.FindControl("ddlPaidBy"), DropDownList)
    '            ddlPaidBy.Items.FindByValue(TryCast(e.Row.FindControl("lblPaidby"), Label).Text).Selected = True
    '        End If
    '    End If
    'End Sub
    'Private Sub BindRentRevision()
    '    Dim rowCount As Integer = 0
    '    rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
    '    If rowCount > 1 Then
    '        RentRevisionPanel.Visible = True
    '        Dim RR_lst As New List(Of RentRevision)()
    '        Dim rr_obj As New RentRevision()
    '        For i As Integer = 1 To rowCount - 1
    '            rr_obj = New RentRevision()
    '            rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '            rr_obj.RR_Percentage = "0"
    '            RR_lst.Add(rr_obj)
    '        Next
    '        rpRevision.DataSource = RentRevision
    '        rpRevision.DataBind()
    '    Else
    '        RentRevisionPanel.Visible = False
    '    End If
    'End Sub

    'Protected Sub CancelEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvlandlordItems.RowCancelingEdit
    '    gvLeaseExpences.EditIndex = -1
    '    BindLeaseExpences()
    'End Sub
    'Private Sub BindTenure()
    '    ObjSubSonic.Binddropdown(ddlTenure, "PM_GET_PAYMENT_TERMS", "PM_PT_NAME", "PM_PT_SNO")
    '    ddlTenure.Items.RemoveAt(0)
    'End Sub

    Private Sub BindLeaseExpences()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXPENSES_OF_LEASE")
        sp.Command.AddParameter("@PM_LES_SNO", hdnLSNO.Value, DbType.String)
        gvLeaseExpences.DataSource = sp.GetDataSet()
        gvLeaseExpences.DataBind()
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PRE_VALIDATION_APPROVE")
        sp.Command.AddParameter("@LEASE_ID", hdnLSNO.Value, DbType.String)
        sp.Command.AddParameter("@LEASE_REQ_ID", Session("REQ_ID"), DbType.String)
        'PROPERTY
        sp.Command.AddParameter("@SHOP_NO_OCC", txtshopnumberoccupy.Text, DbType.String)
        sp.Command.AddParameter("@TERM_OF_LEASE", txtTermofLease.Text, DbType.String)
        sp.Command.AddParameter("@COMP_VICI", txtCompetitorsVicinity.Text, DbType.String)
        sp.Command.AddParameter("@DDL_ROLL", ddlRollingShutter.Text, DbType.String)

        'COMMERCIALS
        sp.Command.AddParameter("@PROP_TAX", txtproptax.Text, DbType.String)
        sp.Command.AddParameter("@SERV_TAX", txtservicetax.Text, DbType.String)
        sp.Command.AddParameter("@RENT_FREE", txtRentFreePeriod.Text, DbType.String)
        sp.Command.AddParameter("@PM_CAR_PARK_FEES", txtcarpark.Text, DbType.String)
        sp.Command.AddParameter("@PM_EXIT_OPTS", txtExit.Text, DbType.String)
        sp.Command.AddParameter("@PM_ALLOW", txtAllow.Text, DbType.String)
        sp.Command.AddParameter("@PM_EST_COST", txtAddpwr.Text, DbType.String)

        'UTILITY/POWER BACK UP
        sp.Command.AddParameter("@DG_SET", ddlDgSet.Text, DbType.String)
        sp.Command.AddParameter("@DG_SET_COM", txtDgSetPerUnit.Text, DbType.String)
        sp.Command.AddParameter("@DG_LOC", txtDgSetLocation.Text, DbType.String)
        sp.Command.AddParameter("@SPC_SERVO", txtSpaceServoStab.Text, DbType.String)
        sp.Command.AddParameter("@DDL_METRO", ddlElectricalMeter.Text, DbType.String)
        sp.Command.AddParameter("@MET_LOC", txtMeterLocation.Text, DbType.String)
        sp.Command.AddParameter("@EARTH_PIT", txtEarthingPit.Text, DbType.String)
        sp.Command.AddParameter("@AVAIL_PWR", txtAvailablePower.Text, DbType.String)
        sp.Command.AddParameter("@ADD_POWER", txtAdditionalPowerKWA.Text, DbType.String)
        sp.Command.AddParameter("@PWR_SPEC", txtPowerSpecification.Text, DbType.String)
        sp.Command.AddParameter("@PATH_ELEC_CAB", txtpathElecCable.Text, DbType.String)
        sp.Command.AddParameter("@GND_EARTH", txtgroundEarthling.Text, DbType.String)
        sp.Command.AddParameter("@EARTH_TYPE", txtearthType.Text, DbType.String)
        sp.Command.AddParameter("@LIFT_PWR_CUT", txtLiftPowerCut.Text, DbType.String)
        sp.Command.AddParameter("@TRANS_LOC", txtTransLocation.Text, DbType.String)
        sp.Command.AddParameter("@WATER_CONN", txtWaterConnection.Text, DbType.String)

        'OTHER SERVICES

        sp.Command.AddParameter("@TWO_WHEEL", txtNoOfTwoWheelerParking.Text, DbType.String)
        sp.Command.AddParameter("@CAR_PARK", txtNoOfCarsParking.Text, DbType.String)
        sp.Command.AddParameter("@LIFT", ddlLift.Text, DbType.String)
        sp.Command.AddParameter("@MAKE", txtMake.Text, DbType.String)
        sp.Command.AddParameter("@CAPACITY", txtCapacity.Text, DbType.String)
        sp.Command.AddParameter("@STAIR_WIDTH", txtStairCaseWidth.Text, DbType.String)
        sp.Command.AddParameter("@FIRE_STAIR", txtFireStaircase.Text, DbType.String)
        sp.Command.AddParameter("@AC_OUT_DOOR", txtACOutdoor.Text, DbType.String)
        sp.Command.AddParameter("@ANY_AC", anyACSatloc.Text, DbType.String)
        sp.Command.AddParameter("@SIGNAGE_BAND", txtsignOrbrand.Text, DbType.String)
        sp.Command.AddParameter("@FIRE_EXIT", txtFireExitstairs.Text, DbType.String)
        sp.Command.AddParameter("@FIR_FIGHT", txtFireFightingSystem.Text, DbType.String)
        sp.Command.AddParameter("@LIFT_N", txtLiftNLicense.Text, DbType.String)
        sp.Command.AddParameter("@MAINT_AGENCY", txtMaintAgencyLift.Text, DbType.String)

        'DISTANCE MAPPING
        sp.Command.AddParameter("@FROM_AIR", txtDistanceFromAirPort.Text, DbType.String)
        sp.Command.AddParameter("@FROM_RAIL", txtDistanceFromRailwayStation.Text, DbType.String)
        sp.Command.AddParameter("@FROM_BUS", txtDistanceFromBustop.Text, DbType.String)

        'OFFICE COONECTIVITY
        sp.Command.AddParameter("@TeleSPB", txtTeleSPB.Text, DbType.String)
        sp.Command.AddParameter("@TeleSPFO", txtTeleSPFO.Text, DbType.String)
        sp.Command.AddParameter("@LeaseSPB", txtLeaseSPB.Text, DbType.String)
        sp.Command.AddParameter("@LeaseSPFO", txtLeaseSPFO.Text, DbType.String)
        sp.Command.AddParameter("@TeleVenFR", txtTeleVenFR.Text, DbType.String)
        sp.Command.AddParameter("@ITVenFR", txtITVenFR.Text, DbType.String)

        'CIVIL
        sp.Command.AddParameter("@txtPunnWall", txtPunnWall.Text, DbType.String)
        sp.Command.AddParameter("@txtCracks", txtCracks.Text, DbType.String)
        sp.Command.AddParameter("@txtWashToilet", txtWashToilet.Text, DbType.String)
        sp.Command.AddParameter("@txtPantry", txtPantry.Text, DbType.String)
        sp.Command.AddParameter("@DRAINAGE", txtDrainagelinedrawings.Text, DbType.String)
        sp.Command.AddParameter("@WIN_HT", txtWindowHeight.Text, DbType.String)
        sp.Command.AddParameter("@txtWaterLeak", txtWaterLeak.Text, DbType.String)
        sp.Command.AddParameter("@txtStaircase", txtStaircase.Text, DbType.String)
        sp.Command.AddParameter("@txtEmergExit", txtEmergExit.Text, DbType.String)
        sp.Command.AddParameter("@HT_AVAIL", txtHeightAvailability.Text, DbType.String)
        sp.Command.AddParameter("@ADD_CIVIL", txtAddtnlCivilWork.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.ExecuteScalar()
        lblMsg.Visible = True
        lblMsg.Text = "Pre Validation Approved Successfully"

    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PRE_VALIDATION_REJECT")
        sp.Command.AddParameter("@LEASE_ID", hdnLSNO.Value, DbType.String)
        sp.Command.AddParameter("@LEASE_REQ_ID", Session("REQ_ID"), DbType.String)
        sp.ExecuteScalar()
        lblMsg.Visible = True
        lblMsg.Text = "Pre Validation Rejected Successfully"
    End Sub

    Protected Sub gvItems_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvItems.SelectedIndexChanged

    End Sub
End Class
