<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddWorkRequest.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmAddWorkRequest" Title="Add Work Request" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div  class="col-md-12">
                        <fieldset>
                            <legend>Add Work Request
                            </legend>
                        </fieldset>
                      
                            <form id="form1" class="well" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" ForeColor="Red" />
                                <div class="clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Property Type <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                                Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                                                InitialValue="0"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >City <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="--Select City--"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Location <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Property <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlproperty"
                                                Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:DropDownList ID="ddlproperty" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Title<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfTitle" runat="server" ControlToValidate="txtWorkTitle"
                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Work Title"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                                                    MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Specifications<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvspec" runat="server" ControlToValidate="txtWorkSpec"
                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Work Specification"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control" Height="30%"
                                                    TextMode="MultiLine" MaxLength="250"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Estimated Amount<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvamount" runat="server" ControlToValidate="txtamount"
                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Estimated Amount"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revamount" Display="None" runat="server" ControlToValidate="txtamount"
                                                ValidationGroup="Val1" ErrorMessage="Invalid Amount" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                            <div  >
                                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Vendor<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvVendor" runat="server" ControlToValidate="ddlVendor"
                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Vendor"></asp:RequiredFieldValidator>
                                            <div  >
                                               
                                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Vendor Phone Number</label>
                                            <div  >
                                                <asp:TextBox ID="txtPhno" runat="server" CssClass="form-control" MaxLength="12" Enabled="false" ></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Vendor Address</label>
                                            <div  >
                                                <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" Enabled="false" ></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Remarks</label>
                                            <div  >
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                                    MaxLength="250"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 19px; padding-left: 30px">
                                        <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                            CausesValidation="true" />&nbsp;            
                                    </div>
                                </div>
                            </form>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

