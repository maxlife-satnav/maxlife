﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaseTaxPayment.aspx.cs" Inherits="PropertyManagement_Views_LeaseTaxPayment" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Tax Payment
                            </legend>
                        </fieldset>

                        <form id="form1" class="well" runat="server">
                            <div class="col-md-12">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                            </div>
                            <div class="clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>

                                    </div>
                                </div>
                            </div>
                            <div class="row" id="Tr1" runat="server" visible="false">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-12 control-label">Select Lease Type </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                                    Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-12 control-label">Search by Property / Lease / Location Name<span style="color: red;">*</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:RequiredFieldValidator ID="rfvEmpID" runat="server" ControlToValidate="txtReqId"
                                                    Display="None" ErrorMessage="Please Search by Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtReqId" runat="Server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn btn-primary custom-button-color" Text="Search"
                                                    ValidationGroup="Val1" />
                                                <asp:Button ID="btnReset" runat="server" OnClick="txtreset_Click" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="padding-top: 10px;">

                                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" PageSize="10" EmptyDataText="No Approved Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                    Style="font-size: 12px;" OnPageIndexChanging="gvItems_PageIndexChanging" OnRowCommand="gvItems_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("LEASE_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Id">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LR_REQ_ID_DISPLAYNAME")%>' CommandName="Tax" CommandArgument='<%#Eval("LEASE_ID")%>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRent" Text='<%#Eval("PM_LES_TOT_RENT","{0:c2}")%>' runat="server" CssClass="lblRent"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Sub Lease Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSLfrmdt" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_SLA_AGREE_ST_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                        <%-- <asp:TemplateField HeaderText="Sub Lease End Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSLEdDT" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_SLA_AGREE_END_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>

                            </div>
                            <br />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                            <div id="panel1" runat="Server">
                                <div class="row" style="padding-top: 10px;">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvLandlords" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                            AllowPaging="True" PageSize="5" EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                            Style="font-size: 12px;">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Lease Id" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LES_PM_LR_REQ_ID_DISPLAYNAME")%>' CommandArgument='<%#Eval("LEASE_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Landlord Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LBLSNO" runat="server" CssClass="lblCODE" Text='<%#Eval("SNO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Landlord Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_LL_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--           <asp:TemplateField HeaderText="State">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("STE_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="City">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Address">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("PM_LL_ADDRESS1")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Basic Rent">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LL_MON_RENT_PAYABLE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Security Deposit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LL_SECURITY_DEPOSIT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Declare TDS">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlTDS" runat="server" CssClass="form-control selectpicker" data-live-search="true" SelectedValue='<%# Eval("PM_LL_TDS")%>'
                                                            ToolTip="Select TDS">
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                        <div class="clearfix" id="pnlbutton" runat="server" style="padding-top: 10px;">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Remarks</label>
                                                    <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                                <div class="form-group">
                                                    <asp:Button ID="btnSubmit" Text="Submit" runat="server" CssClass="btn btn-primary custom-button-color"
                                                        ValidationGroup="Val2" OnClick="btnSubmit_Click"></asp:Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lease Application form</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
