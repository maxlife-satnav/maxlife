<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddPropertyDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmAddPropertyDetails"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='http://localhost:49466/fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>

<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <%--<div class="panel-heading" style="height: 41px;">--%>
                        <fieldset>
                            <legend>ZFM-Property Options
                            </legend>
                        </fieldset>

                        <form id="form1" class="well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />
                            <h3 class="clearfix">
                                <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            </h3>
                            <div class="panel panel-default " role="tab" id="LocDetails">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Loc">Location Details</a></h4>
                                </div>
                                <div id="Loc" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Request ID</label>
                                                    <asp:Label ID="txtReqID" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">


                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Request Type<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlReqType" runat="server" ControlToValidate="ddlReqType"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Request Type"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlReqType" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" onchange="clearMsg()" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Property Nature<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlPprtNature" runat="server" ControlToValidate="ddlPprtNature"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlPprtNature" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                        <asp:ListItem Value="1">Lease</asp:ListItem>
                                                        <asp:ListItem Value="2">Own</asp:ListItem>
                                                        <%-- <asp:ListItem Value="3">Farm House</asp:ListItem>
                                                                <asp:ListItem Value="3">Software Park</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12" hidden>
                                                <div class="form-group">
                                                    <label>Acquisition Through<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlAcqThr" runat="server" ControlToValidate="ddlAcqThr"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Acquisition Through"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlAcqThr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        <asp:ListItem Value="1">Purchase</asp:ListItem>
                                                        <%--<asp:ListItem Value="2">Testametary Succession</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>City<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Location<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                        data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">


                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Tower<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlTower" runat="server" ControlToValidate="ddlTower"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Tower" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlTower" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                        data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Floor<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvddlFloor" runat="server" ControlToValidate="ddlFloor"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Floor" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Property Type<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rvpropertyType" runat="server" ControlToValidate="ddlPropertyType"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Type"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <div>
                                                        <asp:DropDownList ID="ddlPropertyType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Property Name<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="revPropIDName" runat="server" ControlToValidate="txtPropIDName"
                                                        Display="None" ErrorMessage="Please Enter Property Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revpropertyname" runat="server" ControlToValidate="txtPropIDName"
                                                        Display="None" ErrorMessage="Invalid Property Name" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropIDName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Property Address</label>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Address with maximum 500 characters')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropDesc" runat="server" CssClass="form-control" Height="30%"
                                                                Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>ESTD(Year)</label>
                                                    <asp:RegularExpressionValidator ID="rgetxtESTD" runat="server" ControlToValidate="txtESTD"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid ESTD." ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter Numericals With Maximum length 4')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtESTD" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="4">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Age</label>
                                                    <div>
                                                        <div>
                                                            <asp:TextBox ID="txtAge" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White" Enabled="false">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Competitors in Vicinity</label>
                                                    <asp:TextBox ID="txtCompetitorsVicinity" runat="server" CssClass="form-control" MaxLength="12"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Type Of Agreement<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlOffice"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Type Of Agreement" InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlOffice" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div2">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Area">Commercial Details</a>
                                    </h4>
                                </div>
                                <div id="Area" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Super Built Up Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtSuperBulArea" runat="server" ControlToValidate="txtSuperBulArea"
                                                        Display="None" ErrorMessage="Please Enter Super Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txtUsableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Super Built Up Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtSuperBulArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Carpet Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfCarpetarea" runat="server" ControlToValidate="txtCarpetArea"
                                                        Display="None" ErrorMessage="Please Enter Carpet Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revCarpetarea" runat="server" ControlToValidate="txtOptCapacity"
                                                        Display="None" ErrorMessage="Invalid Carpet Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Built Up Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                        Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                        Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtBuiltupArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Common Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvCommonarea" runat="server" ControlToValidate="txtCommonArea"
                                                        Display="None" ErrorMessage="Please Enter Common Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revCommon" runat="server" ControlToValidate="txtCommonArea"
                                                        Display="None" ErrorMessage="Invalid Common Area" ValidationExpression="^[0-9]*\.?[0-9]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtCommonArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Rentable Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvRentable" runat="server" ControlToValidate="txtRentableArea"
                                                        Display="None" ErrorMessage="Please Enter Rentable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRentableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Rentable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtRentableArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Usable Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvUsable" runat="server" ControlToValidate="txtUsableArea"
                                                        Display="None" ErrorMessage="Please Enter Usable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revUsable" runat="server" ControlToValidate="txtUsableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Usable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtUsableArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Plot Area (Sqft)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtPlotArea" runat="server" ControlToValidate="txtPlotArea"
                                                        Display="None" ErrorMessage="Please Enter Plot Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="txtUsableArea"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Plot Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPlotArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Total No. Of Floors</label>
                                                    <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control" BorderColor="White" Enabled="false">0</asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Floor to Ceiling Height(ft)</label>
                                                    <asp:RegularExpressionValidator ID="rfvtxtCeilingHight" runat="server" ControlToValidate="txtCeilingHight"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Ceiling Hight" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtCeilingHight" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Floor to Beam Bottom Height(ft) </label>
                                                    <asp:RegularExpressionValidator ID="rfvtxtBeamBottomHight" runat="server" ControlToValidate="txtBeamBottomHight"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Beam Bottom Height" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtBeamBottomHight" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Maximum Capacity</label>
                                                    <asp:RegularExpressionValidator ID="revmaxcapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                        Display="None" ErrorMessage="Invalid Maximum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtMaxCapacity" runat="server" CssClass="form-control" MaxLength="6">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Optimum Capacity</label>
                                                    <asp:RegularExpressionValidator ID="revOptCapacity" runat="server" ControlToValidate="txtOptCapacity"
                                                        Display="None" ErrorMessage="Invalid Optimum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtOptCapacity" runat="Server" CssClass="form-control" MaxLength="6">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Seating Capacity</label>
                                                    <asp:RegularExpressionValidator ID="rfvtxtSeatingCapacity" runat="server" ControlToValidate="txtSeatingCapacity"
                                                        Display="None" ErrorMessage="Invalid Seating Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtSeatingCapacity" runat="Server" CssClass="form-control" MaxLength="6">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Flooring Type</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlFlooringType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>FSI (Floor Space Index)</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlFSI" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3 col-xs-6" runat="server" id="divFSI" visible="false">
                                                <div class="form-group">
                                                    <label>FSI Ratio (Sqft)<span style="color: red;">*</span></label>
                                                    <%--                                                            <asp:RequiredFieldValidator ID="rfvtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                                Display="None" ErrorMessage="Please Enter FSI Ratio" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                    <asp:RegularExpressionValidator ID="revtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid FSI Ratio" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtFSI" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Preferred Efficiency (%) </label>
                                                    <asp:RegularExpressionValidator ID="revtxtEfficiency" runat="server" ControlToValidate="txtEfficiency"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Preferred Efficiency" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtEfficiency" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <%--From Lease--%>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>No Of Two Wheelers Parking</label>
                                                    <asp:TextBox ID="txtNoOfTwoWheelerParking" runat="server" CssClass="form-control" TabIndex="82">0</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>No Of  Cars Parking</label>
                                                    <asp:TextBox ID="txtNoOfCarsParking" runat="server" CssClass="form-control" TabIndex="83">0</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Additional Recurring Cost</label>
                                                    <asp:TextBox ID="txtRecurringCost" runat="server" CssClass="form-control" TabIndex="83">0</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Available Power (KWA)</label>
                                                    <asp:TextBox ID="txtAvailablePower" runat="server" CssClass="form-control" TabIndex="79">0</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Basic Rent<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea" Display="None" ValidationGroup="Val1"
                                                        ErrorMessage="Please Enter Basic Rent"></asp:RequiredFieldValidator>
                                                    <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtInvestedArea" runat="server" CssClass="form-control" MaxLength="15" TabIndex="4">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Maintenance Charges<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtmain1" runat="server" ControlToValidate="txtmain1"
                                                        Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Maintenance Charges "></asp:RequiredFieldValidator>
                                                    <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtmain1" runat="server" CssClass="form-control" MaxLength="15" TabIndex="16">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--From Lease--%>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div6">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Recom">Recommendation Details</a>
                                    </h4>
                                </div>
                                <div id="Recom" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Recommended/Priority</label>
                                                    <asp:DropDownList ID="ddlRecommended" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="2">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12" runat="server" id="divRecommanded" visible="false">
                                                <div class="form-group">
                                                    <label>Recommended/Priority Remarks<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtRecmRemarks" runat="server" ControlToValidate="txtRecmRemarks"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Recommended/Priority Remarks"></asp:RequiredFieldValidator>
                                                    <div onmouseover="Tip('Enter Remarks with maximum 500 characters')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtRecmRemarks" runat="server" CssClass="form-control" Height="30%"
                                                            Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div0">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#General">General Details</a>
                                    </h4>
                                </div>
                                <div id="General" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>No. Of Toilet Blocks</label>
                                                    <asp:RegularExpressionValidator ID="revtxtToilet" runat="server" ControlToValidate="txtToilet"
                                                        Display="None" ErrorMessage="Invalid No. Of Toilet Blocks" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtToilet" runat="server" CssClass="form-control">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Society Name</label>
                                                    <div>
                                                        <div>
                                                            <asp:TextBox ID="txtSocityName" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Latitude</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtlat" ValidationGroup="Val1"
                                                        Display="None" ErrorMessage="Invalid Latitude" ValidationExpression="^[0-9\.\-\/]+$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <asp:TextBox ID="txtlat" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Longitude</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtlong" ValidationGroup="Val1"
                                                        Display="None" ErrorMessage="Invalid Longitude" ValidationExpression="^[0-9\.\-\/]+$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <asp:TextBox ID="txtlong" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Owner Scope Of Work</label>
                                                    <div>
                                                        <asp:TextBox ID="txtOwnScopeWork" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Signage Location</label>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Signage Location with maximum 500 characters')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtSignageLocation" runat="server" CssClass="form-control"
                                                                Rows="3" TextMode="Multiline" MaxLength="500" Height="30%">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="divNewOfficeRelocation" runat="server" visible="true">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#genOtherDetails">General Other Details</a>
                                    </h4>
                                </div>
                                <div id="genOtherDetails" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Existing monthly rental </label>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtExistMonthRent" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed Area (for agency)</label>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtProposedAreaForAgency" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed Area (Other channels , if any)</label>


                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPropAreaOtherChannels" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed Monthly rental  </label>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropMonthlyRental" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Proposed per sq.ft rental</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtPropsedPerSqftrental"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Purchase Price" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPropsedPerSqftrental" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Is Board approval in place  </label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPprtNature"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                        InitialValue="0"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlBoardAppr" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                        <asp:ListItem Value="">--Select--</asp:ListItem>
                                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                        <asp:ListItem Value="No">No</asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                            </div>


                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Cost Type On</label>
                                                    <asp:RadioButtonList ID="rblCostType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                        <asp:ListItem Value="Sqft" Text="Sq.ft"  Selected />
                                                        <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">

                                            <div id="Costype1" runat="server" visible="false">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Rent Per Sq.ft (On Carpet)</label>
                                                        <asp:TextBox ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">0</asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Rent Per Sq.ft (On BUA)</label>
                                                        <asp:TextBox ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Costype2" runat="server" visible="false">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Seat Cost<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div1">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Owner">Owner Details</a>
                                    </h4>
                                </div>
                                <div id="Owner" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Owner Name<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvownname" runat="server" ControlToValidate="txtownrname"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Owner Name"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtownrname" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <asp:RegularExpressionValidator ID="REVPHNO" runat="server" ControlToValidate="txtphno"
                                                        ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" Display="None"
                                                        ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtphno" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Previous Owner Name</label>
                                                    <asp:TextBox ID="txtPrvOwnName" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Previous Owner Phone Number</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtPrvOwnPhNo" Display="None"
                                                        ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPrvOwnPhNo" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Owner Email</label>
                                                    <asp:RegularExpressionValidator ID="rfv" runat="server" ControlToValidate="txtOwnEmail"
                                                        ValidationGroup="Val1" ErrorMessage="Please Enter Valid Email" Display="None"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                    </asp:RegularExpressionValidator>
                                                    <asp:TextBox ID="txtOwnEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div3">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Purchase">Purchase Details</a>
                                    </h4>
                                </div>
                                <div id="Purchase" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Allocated Budget </label>
                                                    <asp:RegularExpressionValidator ID="revtxtPurPrice" runat="server" ControlToValidate="txtPurPrice"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Purchase Price" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtPurPrice" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Purchase Date</label>
                                                    <div>
                                                        <div class='input-group date' id='PurDate'>
                                                            <asp:TextBox ID="txtPurDate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('PurDate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Current Market Rate </label>
                                                    <asp:RegularExpressionValidator ID="revtxtMarketValue" runat="server" ControlToValidate="txtMarketValue"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Market Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtMarketValue" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div4">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Govt">Govt. Details</a>
                                    </h4>
                                </div>
                                <div id="Govt" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>IRDA (Insurance Regulatory and Development)</label>
                                                            <div>
                                                                <asp:TextBox ID="txtIRDA" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>--%>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>IRDA for opening new office?</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlIRDA" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No">No</asp:ListItem>

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>PC Code (Penal Code)</label>
                                                    <div>
                                                        <asp:TextBox ID="txtPCcode" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Government Property Code</label>
                                                    <asp:RegularExpressionValidator ID="revgovt" runat="server" ControlToValidate="txtGovtPropCode"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Government Property Code"
                                                        ValidationExpression="^[A-Za-z0-9//(/)\s/-]*$"></asp:RegularExpressionValidator>
                                                    <div>
                                                        <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtGovtPropCode" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>UOM CODE (Unit of Measurement)</label>
                                                    <div>
                                                        <asp:TextBox ID="txtUOM_CODE" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default " role="tab" id="div5">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Insurance">Insurance Details</a>
                                    </h4>
                                </div>
                                <div id="Insurance" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Type</label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlInsuranceType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Vendor</label>
                                                    <div>
                                                        <asp:TextBox ID="txtInsuranceVendor" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Amount</label>
                                                    <div>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtInsuranceAmt"
                                                            ErrorMessage="Please enter Insurance Amount in numericals with decimals only" ValidationGroup="Val1" Display="None"
                                                            ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                        <asp:TextBox ID="txtInsuranceAmt" runat="server" CssClass="form-control">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Policy Number</label>
                                                    <div>
                                                        <asp:TextBox ID="txtInsurancePolNum" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>Start Date</label>
                                                    <div>
                                                        <div class='input-group date' id='fromdate'>
                                                            <asp:TextBox ID="txtInsuranceStartdate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>End Date</label>
                                                    <div>
                                                        <div class='input-group date' id='todate'>
                                                            <asp:TextBox ID="txtInsuranceEnddate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div class="clearfix">
                                                        <label for="txtcode">Upload Property Images <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                        <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fu1"
                                                            ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                            ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$">
                                                        </asp:RegularExpressionValidator>

                                                        <div class="btn btn-primary btn-mm">
                                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                                            <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12" id="divBoardApprDocument" runat="server" visible="false">
                                                <div class="form-group">
                                                    <div class="clearfix">
                                                        <label for="txtcode">Upload Board Approval Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Display="None" ControlToValidate="fuBoardApprlDoc"
                                                            ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                            ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg|.doc|.docx|.pdf)$">
                                                        </asp:RegularExpressionValidator>

                                                        <div class="btn btn-primary btn-mm">
                                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                                            <asp:FileUpload ID="fuBoardApprlDoc" runat="Server" Width="90%" AllowMultiple="True" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12" id="divIRDADoc" runat="server" visible="false">
                                                <div class="form-group">
                                                    <div class="clearfix">
                                                        <label for="txtcode">Upload IRDA Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" Display="None" ControlToValidate="fu1"
                                                            ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                            ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg|.doc|.docx|.pdf)$">
                                                        </asp:RegularExpressionValidator>

                                                        <div class="btn btn-primary btn-mm">
                                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                                            <asp:FileUpload ID="irdaAppr" runat="Server" Width="90%" AllowMultiple="True" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">
                                <asp:GridView ID="gvboard" runat="server" EmptyDataText="No Documents Found."
                                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Board Document">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDownload1" runat="server" Text="Click To Download Board Document" CommandArgument='<%#Eval("BOARD_DOC")%>'
                                                    CommandName="BoardDoc"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="IRDA Document">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDownload2" runat="server" Text="Click To Download IRDA Document" CommandArgument='<%#Eval("IRDA")%>'
                                                    CommandName="IRDADoc"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle />
                                    <PagerStyle CssClass="pagination-ys" />
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                </asp:GridView>
                            </div>

                            <div class="clearfix">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-12 text-right" style="padding-top: 5px">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
                                </div>
                            </div>
                            <%--</div>--%>
                        </form>
                        <%--</div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script>
    $('.closeall').click(function () {
        $('.panel-collapse.in')
          .collapse('hide');
    });
    $('.openall').click(function () {
        $('.panel-collapse:not(".in")')
          .collapse('show');
    });

    function clearMsg() {
        document.getElementById("AddPropertyDetails1_lblmsg").innerHTML = '';
    }
    function maxLength(s, args) {
        if (args.Value.length >= 500)
            args.IsValid = false;
    }
    function CheckDate() {
        var dtFrom = document.getElementById("txtInsuranceStartdate").Value;
        var dtTo = document.getElementById("txtInsuranceEnddate").Value;
        if (dtFrom < dtTo) {
            alert("Invalid Dates");
        }
    }
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
