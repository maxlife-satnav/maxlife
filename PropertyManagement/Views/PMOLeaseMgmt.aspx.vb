﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic


Partial Class PropertyManagement_Views_PMOLeaseMgmt
    Inherits System.Web.UI.Page
    Dim BoardApprovalDoc As String
    Dim IRDAApprovalDoc As String

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Public Shared Function GetCurrentFinancialYear() As String
        Dim CurrentYear As Integer = DateTime.Today.Year
        Dim PreviousYear As Integer = DateTime.Today.Year - 1
        Dim NextYear As Integer = DateTime.Today.Year + 1
        Dim PreYear As String = PreviousYear.ToString()
        Dim NexYear As String = NextYear.ToString()
        Dim CurYear As String = CurrentYear.ToString()
        Dim FinYear As String = "FY"
        If DateTime.Today.Month > 3 Then
            FinYear = Convert.ToString((FinYear & CurYear) + "-") & NexYear
        Else
            FinYear = Convert.ToString((FinYear & PreYear) + "-") & CurYear
        End If
        Return FinYear.Trim()
    End Function

    Private Shared Function GetSerialNumber() As String
        Dim sp3 As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GENERATE_REQUESTID")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDPROPERTY", DbType.String)
        Return Convert.ToString(sp3.ExecuteScalar())
    End Function

    Private Sub GetZoneAndStateByCityCode()
        Dim Zone, StateCode, CityCode As String
        Dim ds As DataSet
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ZONE_STATE_BY_CITY_CODE")
        sp3.Command.Parameters.Add("@CITY", ddlCity.SelectedValue, DbType.String)
        sp3.Command.AddParameter("@USR_ID", Session("uid"))
        ds = sp3.GetDataSet()

        Zone = ds.Tables(0).Rows(0).Item("CTY_ZN_ID").ToString()
        StateCode = ds.Tables(0).Rows(0).Item("CTY_STATE_ID").ToString()
        CityCode = ds.Tables(0).Rows(0).Item("CTY_CODE").ToString()

        txtReqID.Text = Zone + "/" + StateCode + "/" + CityCode + "/" + GetCurrentFinancialYear() + "/" + GetSerialNumber()
    End Sub

    Private Sub Cleardata()
        ddlReqType.SelectedIndex = 0
        ddlPprtNature.SelectedIndex = 0
        ddlAcqThr.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlPropertyType.SelectedIndex = 0
        txtExistMonthRent.Text = ""
        txtProposedAreaForAgency.Text = ""
        txtPropAreaOtherChannels.Text = ""
        txtPropMonthlyRental.Text = ""
        txtPropsedPerSqftrental.Text = ""
        ddlBoardAppr.SelectedValue = ""
        txtRentPerSqftCarpet.Text = ""
        txtRentPerSqftBUA.Text = ""
        txtSeatCost.Text = ""
        txtPropDesc.Text = ""
        txtOffice.Text = ""
        ddlIRDA.SelectedValue = 0

    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp2.Command.AddParameter("@CITY", ddlCity.SelectedValue)
            sp2.Command.AddParameter("@USR_ID", Session("uid"))
            ddlLocation.DataSource = sp2.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlTower.Items.Clear()
            GetZoneAndStateByCityCode()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
            sp2.Command.AddParameter("@LCMID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlTower.DataSource = sp2.GetDataSet()
            ddlTower.DataTextField = "TWR_NAME"
            ddlTower.DataValueField = "TWR_CODE"
            ddlTower.DataBind()
            ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_DETAILS")
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedValue, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                txtExistMonthRent.Text = ds.Tables(0).Rows(0).Item("PM_LES_TOT_RENT")
                txtRentPerSqftCarpet.Text = ds.Tables(0).Rows(0).Item("FLR_CARPET_AREA")
                txtRentPerSqftBUA.Text = ds.Tables(0).Rows(0).Item("FLR_AREA")
                End If

            Else
                ddlTower.Items.Clear()
            End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
            sp2.Command.AddParameter("@TWR_ID", ddlTower.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_CITY_CODE", ddlCity.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlFloor.DataSource = sp2.GetDataSet()
            ddlFloor.DataTextField = "FLR_NAME"
            ddlFloor.DataValueField = "FLR_CODE"
            ddlFloor.DataBind()
            ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
            txtFloor.Text = ddlFloor.Items.Count - 1
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub

    'Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
    '    If ddlFloor.SelectedIndex > 0 Then
    '        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_RELOCATION_AREA")
    '        sp2.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedValue, DbType.String)
    '        'ddlFloor.DataSource = sp2.GetDataSet()
    '        txtRentPerSqftCarpet.Text = "FLR_CARPET_AREA"
    '        txtRentPerSqftBUA.Text = "FLR_AREA"
    '    Else
    '        txtRentPerSqftCarpet.Text = Nothing
    '        txtRentPerSqftBUA.Text = Nothing
    '    End If
    'End Sub


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindRequestTypes()
            BindPropType()
            BindUserCities()
        End If
    End Sub

    Protected Sub ddlBoardAppr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBoardAppr.SelectedIndexChanged
        If ddlBoardAppr.SelectedValue = "Yes" Then
            divBoardApprDocument.Visible = True
        Else
            divBoardApprDocument.Visible = False
        End If
    End Sub

    Protected Sub ddlIRDA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIRDA.SelectedIndexChanged
        If ddlIRDA.SelectedValue = "Yes" Then
            divIRDADoc.Visible = True
        Else
            divIRDADoc.Visible = False
        End If
    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub

    Protected Sub ddlReqType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReqType.SelectedIndexChanged
        lblmsg.Text = ""

        If ddlReqType.SelectedValue = 1 Then
            IRDA.Visible = False
            MonRent.Visible = True
            PropArea.Visible = True
            PropAreaOtr.Visible = True
            Address.Visible = True
            CostType.Visible = True
            txtLocation.Visible = False
            txtTower.Visible = False
            txtddlFloor.Visible = False
            ddlLocation.Visible = True
            RequiredFieldValidator15.Visible = True
            ddlTower.Visible = True
            rfvddlTower.Visible = True
            ddlFloor.Visible = True
            rfvddlFloor.Visible = True
            'Costype1.Visible = True
        Else
            MonRent.Visible = False
            PropArea.Visible = False
            PropAreaOtr.Visible = False
            Address.Visible = False
            IRDA.Visible = True
            CostType.Visible = False
            txtLocation.Visible = True
            txtTower.Visible = True
            txtddlFloor.Visible = True
            ddlLocation.Visible = False
            RequiredFieldValidator15.Visible = False
            ddlTower.Visible = False
            rfvddlTower.Visible = False
            ddlFloor.Visible = False
            rfvddlFloor.Visible = False
            Costype1.Visible = False
            Costype2.Visible = False
        End If

    End Sub

    Public Sub InsertPMODetails()
        Try
            Dim param As SqlParameter() = New SqlParameter(25) {}
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = txtReqID.Text
            param(1) = New SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar)
            param(1).Value = ddlReqType.SelectedValue
            param(2) = New SqlParameter("@PROP_NATURE", SqlDbType.VarChar)
            param(2).Value = ddlPprtNature.SelectedValue
            param(3) = New SqlParameter("@ACQ_TRH", SqlDbType.VarChar)
            param(3).Value = ddlAcqThr.SelectedValue

            param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
            param(4).Value = ddlCity.SelectedValue

            param(5) = New SqlParameter("@OD_EXISTING_MONTH_RENT", SqlDbType.VarChar)
            param(5).Value = IIf(txtExistMonthRent.Text = "", DBNull.Value, txtExistMonthRent.Text)

            param(6) = New SqlParameter("@OD_PROPOSED_AREA", SqlDbType.VarChar)
            param(6).Value = IIf(txtProposedAreaForAgency.Text = "", DBNull.Value, txtProposedAreaForAgency.Text)

            param(7) = New SqlParameter("@OD_PROPOSED_AREA_OTHER_CHANELS", SqlDbType.VarChar)
            param(7).Value = IIf(txtPropAreaOtherChannels.Text = "", DBNull.Value, txtPropAreaOtherChannels.Text)

            param(8) = New SqlParameter("@OD_PROP_MONTHLY_RENT", SqlDbType.VarChar)
            param(8).Value = IIf(txtPropMonthlyRental.Text = "", DBNull.Value, txtPropMonthlyRental.Text)

            param(9) = New SqlParameter("@OD_PROP_SQFT_RENT", SqlDbType.VarChar)
            param(9).Value = IIf(txtPropsedPerSqftrental.Text = "", DBNull.Value, txtPropsedPerSqftrental.Text)

            param(10) = New SqlParameter("@OD_BOARD_APPR", SqlDbType.VarChar)
            param(10).Value = ddlBoardAppr.SelectedValue

            param(11) = New SqlParameter("@IRDA", SqlDbType.VarChar)
            param(11).Value = ddlIRDA.SelectedValue

            param(12) = New SqlParameter("@OD_COST_TYPE", SqlDbType.VarChar)
            param(12).Value = rblCostType.SelectedValue

            param(13) = New SqlParameter("@OD_RENT_PER_SQFT_CARPET", SqlDbType.VarChar)
            param(13).Value = IIf(txtRentPerSqftCarpet.Text = "", DBNull.Value, txtRentPerSqftCarpet.Text)

            param(14) = New SqlParameter("@OD_RENT_PER_SQFT_BUA", SqlDbType.VarChar)
            param(14).Value = IIf(txtRentPerSqftBUA.Text = "", DBNull.Value, txtRentPerSqftBUA.Text)

            param(15) = New SqlParameter("@OD_SEAT_COST", SqlDbType.VarChar)
            param(15).Value = IIf(txtSeatCost.Text = "", DBNull.Value, txtSeatCost.Text)

            If fuBoardApprlDoc.PostedFiles IsNot Nothing Then
                For Each File In fuBoardApprlDoc.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        BoardApprovalDoc = Upload_Time & "_" & File.FileName

                    End If
                Next
            End If

            param(16) = New SqlParameter("@OD_BOARD_APPR_DOC", SqlDbType.VarChar)
            param(16).Value = BoardApprovalDoc

            If irdaAppr.PostedFiles IsNot Nothing Then
                For Each File In irdaAppr.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IRDAApprovalDoc = Upload_Time & "_" & File.FileName

                    End If
                Next
            End If

            param(17) = New SqlParameter("@OD_IRDA_APPR_DOC", SqlDbType.VarChar)
            param(17).Value = IRDAApprovalDoc

            param(18) = New SqlParameter("@PM_OFFICE", SqlDbType.VarChar)
            param(18).Value = txtOffice.Text

            Dim Location As String
            If ddlLocation.SelectedValue = "0" Then
                Location = txtLocation.Text
            Else
                Location = ddlLocation.SelectedValue
            End If
            param(19) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
            param(19).Value = Location

            Dim Tower As String
            If ddlTower.SelectedValue = "" Then
                Tower = txtTower.Text
            Else
                Tower = ddlTower.SelectedValue
            End If
            param(20) = New SqlParameter("@TOWER", SqlDbType.VarChar)
            param(20).Value = Tower

            Dim Floor As String
            If ddlFloor.SelectedValue = "" Then
                Floor = txtddlFloor.Text
            Else
                Floor = ddlFloor.SelectedValue
            End If
            param(21) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
            param(21).Value = Floor

            param(22) = New SqlParameter("@PROP_TYPE", SqlDbType.VarChar)
            param(22).Value = ddlPropertyType.SelectedValue

            param(23) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(23).Value = HttpContext.Current.Session("UID")

            param(24) = New SqlParameter("@ADDRESS", SqlDbType.VarChar)
            param(24).Value = txtPropDesc.Text

            param(25) = New SqlParameter("@TOTALFLOOR", SqlDbType.VarChar)
            param(25).Value = txtFloor.Text
            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_ADD_PROPERTY_DETAILS", param)
            If res = "SUCCESS" Then
                lblmsg.Visible = True
                lblmsg.Text = "PMO Added Successfully"
                txtLocation.Text = ""
                txtTower.Text = ""
                txtddlFloor.Text = ""
            Else
                lblmsg.Visible = True
                lblmsg.Text = "Something went wrong. Please try again later."
            End If

        Catch
            Throw
        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        InsertPMODetails()
        Cleardata()
    End Sub
    Protected Sub btnback_Click(sender As Object, e As EventArgs)
        Response.Redirect("PMO.aspx")
    End Sub

    Protected Sub txtProposedAreaForAgency_TextChanged(sender As Object, e As EventArgs) Handles txtProposedAreaForAgency.TextChanged
        If txtProposedAreaForAgency.Text <> "" And txtPropMonthlyRental.Text <> "" Then

            txtPropsedPerSqftrental.Text = Val(txtPropMonthlyRental.Text) / Val(txtProposedAreaForAgency.Text)

        End If
    End Sub

    Protected Sub txtPropMonthlyRental_TextChanged(sender As Object, e As EventArgs) Handles txtPropMonthlyRental.TextChanged
        If txtProposedAreaForAgency.Text <> "" And txtPropMonthlyRental.Text <> "" Then

            txtPropsedPerSqftrental.Text = Val(txtPropMonthlyRental.Text) / Val(txtProposedAreaForAgency.Text)
        End If
    End Sub
End Class


