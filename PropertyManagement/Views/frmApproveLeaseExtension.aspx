<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApproveLeaseExtension.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApproveLeaseExtension"
    Title="Approve Lease Extension" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvLDetailsLease.Columns.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }


            function ChildClick(CheckBox) {
                //Get target base & child control.
                var TargetBaseControl = document.getElementById('<%= Me.gvLDetailsLease.ClientID%>');
                var TargetChildControl = "chkItem";
                //Get all the control of the type INPUT in the base control.
                var Inputs = TargetBaseControl.getElementsByTagName("input");
                // check to see if all other checkboxes are checked
                for (var n = 0; n < Inputs.length; ++n)
                    if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                        // Whoops, there is an unchecked checkbox, make sure
                        // that the header checkbox is unchecked
                        if (!Inputs[n].checked) {
                            Inputs[0].checked = false;
                            return;
                        }
                    }
                // If we reach here, ALL GridView checkboxes are checked
                Inputs[0].checked = true;
            }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvLDetailsLease.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Approve / Reject Lease Extension
                            </legend>
                        </fieldset>

                        <form id="form1" class="well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" id="Tr1" runat="server" visible="false">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-12 control-label">Select Lease Type </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                                    Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <label>Search by Property / Lease / Location Name<span style="color: red;">*</span></label>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="clearfix">
                                        <div class="col-md-5">
                                            <asp:RequiredFieldValidator ID="rfvEmpID" runat="server" ControlToValidate="txtLeaseId"
                                                Display="None" ErrorMessage="Please Search By Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtLeaseId" runat="Server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" ValidationGroup="Val1" />
                                            <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="clearfix" style="padding-top: 10px;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <asp:GridView ID="gvLDetailsLease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="10" EmptyDataText="No Approved Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                Style="font-size: 12px;">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" TextAlign="Right" onclick="javascript:CheckAllDataGridCheckBoxes('chkItem', this.checked);"
                                                ToolTip="Click to check all" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkItem" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                        </ItemTemplate>
                                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("LEASE_ID", "~/PropertyManagement/Views/LeaseExtensionAppRej.aspx?id={0}")%>'
                                                Text='<%# Eval("LEASE_ID")%> '></asp:HyperLink>
                                            <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_ID")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Start Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Expiry Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Extended Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLL" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LE_END_DT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Created By">
                                        <ItemTemplate>
                                            <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                            <br />
                            <div class="clearfix" id="pnlbutton" runat="server">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Remarks </label>
                                        <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                    <div class="form-group">
                                        <asp:Button ID="btnAppall" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                        <asp:Button ID="btnRejAll" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>
                        
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lease Application form</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
