﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorkRequest.aspx.cs" Inherits="PropertyManagement_Views_WorkRequest" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                orientation: 'bottom'
            });
        };
    </script>
    <style>
       
        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

    
    </style>
</head>
<body data-ng-controller="WorkRequestController" class="amantra">
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Work Request
                            </legend>
                        </fieldset>
                        
                         
                            <form id="form1" class="well" name="WorkReq" runat="server">
                                <div id="TotalRequest" data-ng-show="GridVisiblity">
                                    <input id="filtertxt" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridOptions" style="height: 300px;" class="ag-blue"></div>
                                </div>
                                <br />
                                <br />
                                <div id="PropertyReq" data-ng-show="PropertyDetailsVisible">
                                    <input id="filtertxt1" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridOptionsProperty" style="height: 300px;" class="ag-blue"></div>
                                </div>
                            </form>
                        

                        <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal" data-backdrop="false">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="panel-group box box-primary" id="Div2">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div class="panel-heading ">
                                                <h4 class="panel-title modal-header-primary" style="padding-left: 17px" data-target="#collapseTwo">Work Request Details</h4>
                                                <form role="form" name="form2" id="form3">
                                                    <div class="row">
                                                        <div class="col-md-12" id="table2">
                                                            <br />
                                                            <a data-ng-click="GenReport(WorkRequest,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                                            <a data-ng-click="GenReport(WorkRequest,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                                            <a data-ng-click="GenReport(WorkRequest,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="box">
                                                                <div class="box-danger table-responsive">
                                                                    <div data-ag-grid="PopOptions" class="ag-blue" style="height: 305px; width: 100%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
     <script>
         var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

         var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../JS/WorkRequest.js"></script>
    <script src="../../SMViews/Utility.js"></script>
</body>
</html>
