<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmHRAddLease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmHRAddLease" Title="Add Lease HR" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        /*body {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }*/

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #eeeeee;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=ReminderCheckList.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Add Lease" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Property Term Sheet 
                                <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus " aria-hidden="true"></i></a>
                            </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">

                            <form id="form1" class="well" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <div>
                                    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                        </Triggers>
                                        <ContentTemplate>--%>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <div class="row">
                                                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />

                                    <div id="AddLeaseDetails" runat="server">
                                        <div class=" panel panel-default" runat="server" id="divspace">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Property/Building Details </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body color">

                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Property Name<span style="color: red;">*</span></label>
                                                                <asp:TextBox ID="txtPropIDName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Floor and unit/shop numbers proposed to occupy:</label>
                                                                <asp:TextBox ID="txtshopnumberoccupy" runat="server" CssClass="form-control" Enabled="True"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Age</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtAge" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Built Up Area (Sqft)<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                    Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                    Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                <div>
                                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtBuiltupArea" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Type Of Office</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtOfficeType" Enabled="false" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Floor to Ceiling Height(ft)</label>

                                                                <asp:TextBox ID="txtCeilingHight" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Floor to Beam Bottom Height(ft) </label>
                                                                <asp:TextBox Enabled="false" ID="txtBeamBottomHight" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>No. Of Toilet Blocks</label>

                                                                <asp:TextBox ID="txtToilet" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Carpet Area (Sqft)<span style="color: red;">*</span></label>

                                                                <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Preferred Efficiency (%) </label>

                                                                <asp:TextBox ID="txtEfficiency" runat="server" CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Term of Lease  (9 years preferable)</label>

                                                                <asp:TextBox ID="txtTermofLease" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Total No. of Floors</label>
                                                                <asp:TextBox ID="txtNoofFloors" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Flooring Type</label>
                                                                <div>
                                                                    <asp:TextBox ID="TxtFlooringType" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Signage Location</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtSignageLocation" runat="server" CssClass="form-control" Enabled="false"
                                                                        Rows="3" TextMode="Multiline" MaxLength="500" Height="30%"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Rolling Shutter </label>
                                                                <asp:RequiredFieldValidator ID="rfvddlRollingShutter" runat="server" ControlToValidate="ddlRollingShutter"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Rolling Shutter"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlRollingShutter" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Competitors in Vicinity</label>
                                                                <asp:TextBox ID="txtCompetitorsVicinity" runat="server" CssClass="form-control" MaxLength="12"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:Label ID="lblLeaseReqId" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="false"></asp:Label>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseCommercials">Commercials</a>
                                                </h4>
                                            </div>
                                            <div id="collapseCommercials" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Cost Type On<span style="color: red;">*</span></label>
                                                                <%-- <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>--%>
                                                                <asp:RadioButtonList ID="rblCostType" runat="server" Enabled="false" RepeatDirection="Horizontal" AutoPostBack="true">
                                                                    <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                                    <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>
                                                        <div id="Costype1" runat="server" visible="false">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label>Rent Per Sqft (On Carpet)<span style="color: red;">*</span></label>
                                                                    <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftCarpet" runat="server" ControlToValidate="txtRentPerSqftCarpet" Display="None" ValidationGroup="Val1"
                                                                        ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="ftbetxtRentPerSqftCarpet" runat="server" TargetControlID="txtRentPerSqftCarpet" FilterType="Numbers" ValidChars="0123456789." />
                                                                    <asp:TextBox ID="txtRentPerSqftCarpet" Enabled="false" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">.</asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label>Rent Per Sqft (On BUA)<span style="color: red;">*</span></label>
                                                                    <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftBUA" runat="server" ControlToValidate="txtRentPerSqftBUA" Display="None" ValidationGroup="Val1"
                                                                        ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtRentPerSqftBUA" runat="server" TargetControlID="txtRentPerSqftBUA" FilterType="Numbers" ValidChars="0123456789." />
                                                                    <asp:TextBox ID="txtRentPerSqftBUA" Enabled="false" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9">.</asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="Costype2" runat="server" visible="false">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label>Seat Cost<span style="color: red;">*</span></label>
                                                                    <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                                        ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtSeatCost" runat="server" TargetControlID="txtSeatCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                    <asp:TextBox ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">.</asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Basic Rent<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Enter Basic Rent"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtInvestedArea" runat="server" TargetControlID="txtInvestedArea" FilterType="Numbers" ValidChars="0123456789." />
                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtInvestedArea" Enabled="false" runat="server" CssClass="form-control" MaxLength="15" TabIndex="4" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Maintenance Charges<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtmain1" runat="server" ControlToValidate="txtmain1"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Maintenance Charges "></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtmain1" runat="server" TargetControlID="txtmain1" FilterType="Numbers" ValidChars="0123456789." />
                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtmain1" Enabled="false" runat="server" CssClass="form-control" MaxLength="15" TabIndex="16" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>
                                                                    Total Rent (Basic Rent + Maint. Cost)</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxttotalrent" runat="server" TargetControlID="txttotalrent" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txttotalrent" runat="server" CssClass="form-control" Enabled="false" TabIndex="23"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Property Tax (In %)<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtproptax" runat="server" ControlToValidate="txtproptax"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Property Tax "></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtproptax" runat="server" TargetControlID="txtproptax" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtproptax" runat="server" CssClass="form-control" MaxLength="15" TabIndex="18"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Service Tax (In %)<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtservicetax" runat="server" ControlToValidate="txtservicetax"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Service Tax "></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtservicetax" runat="server" TargetControlID="txtservicetax" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtservicetax" runat="server" CssClass="form-control" MaxLength="15" TabIndex="17" AutoPostBack="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Rent Free Period(In Days)<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtRentFreePeriod" runat="server" ControlToValidate="txtRentFreePeriod" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Enter Rent Free Period"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtRentFreePeriod" runat="server" TargetControlID="txtRentFreePeriod" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtRentFreePeriod" runat="server" CssClass="form-control" MaxLength="50" TabIndex="7">.</asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseUtilityDetails">Utility/Power Back Up</a>
                                                </h4>
                                            </div>
                                            <div id="collapseUtilityDetails" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>DG Set <span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvddlDgSet" runat="server" ControlToValidate="ddlDgSet" InitialValue="--Select--"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select DG Set"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlDgSet" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="72" AutoPostBack="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                    <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div id="Dgset" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>DG Set Commercials (If Provided by Landlord) Per Unit<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtDgSetPerUnit" runat="server" ControlToValidate="txtDgSetPerUnit"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter DG Set Commercials (If Provided by Landlord) Per Unit"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="FTEtxtDgSetPerUnit" runat="server" TargetControlID="txtDgSetPerUnit" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtDgSetPerUnit" runat="server" CssClass="form-control" TabIndex="73"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>DG Set Location</label>
                                                                <asp:TextBox ID="txtDgSetLocation" runat="server" TabIndex="74" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Space For Servo Stabilizer</label>
                                                                <asp:TextBox ID="txtSpaceServoStab" runat="server" CssClass="form-control" TabIndex="75"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Electrical Meter<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvddlElectricalMeter" runat="server" ControlToValidate="ddlElectricalMeter"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Electrical Meter"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlElectricalMeter" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="76" AutoPostBack="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div id="Meter" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Meter Location<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtMeterLocation" runat="server" ControlToValidate="txtMeterLocation"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Meter Location"></asp:RequiredFieldValidator>
                                                                <asp:TextBox ID="txtMeterLocation" runat="server" CssClass="form-control" TabIndex="77"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Earthing Pit</label>
                                                                <asp:TextBox ID="txtEarthingPit" runat="server" TabIndex="78" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <%--Add Into Property--%>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Available Power (KWA)</label>
                                                                <asp:TextBox ID="txtAvailablePower" runat="server" CssClass="form-control" TabIndex="79"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <%--Add Into Property--%>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Additional Power Required (KWA)</label>
                                                                <asp:TextBox ID="txtAdditionalPowerKWA" runat="server" CssClass="form-control" TabIndex="80"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Power Specification</label>
                                                                <asp:TextBox ID="txtPowerSpecification" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Pathways for electrical cable </label>
                                                                <asp:TextBox ID="txtpathElecCable" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Proper Grounding And Earthling </label>
                                                                <asp:TextBox ID="txtgroundEarthling" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Type of earthing  </label>
                                                                <asp:TextBox ID="txtearthType" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lift working (in case of power cut) </label>
                                                                <asp:TextBox ID="txtLiftPowerCut" runat="server" CssClass="form-control" TabIndex="81"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Transformer ( Location )</label>
                                                                <asp:TextBox ID="txtTransLocation" runat="server" CssClass="form-control" TabIndex="80"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Water Connection</label>
                                                                <asp:TextBox ID="txtWaterConnection" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherServices">Other Services</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOtherServices" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <%--Add Into Property--%>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>No Of Two Wheelers Parking</label>
                                                                <asp:TextBox ID="txtNoOfTwoWheelerParking" runat="server" CssClass="form-control" TabIndex="82"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>No Of  Cars Parking</label>
                                                                <asp:TextBox ID="txtNoOfCarsParking" runat="server" CssClass="form-control" TabIndex="83"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lift</label>

                                                                <asp:DropDownList ID="ddlLift" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" runat="server" id="divLift" visible="false">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lift Make</label>
                                                                <asp:TextBox ID="txtMake" runat="server" CssClass="form-control" TabIndex="87"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lift Capacity</label>
                                                                <asp:TextBox ID="txtCapacity" runat="server" CssClass="form-control" TabIndex="88"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Main Staircase (Width)</label>
                                                                <asp:TextBox ID="txtStairCaseWidth" runat="server" CssClass="form-control" TabIndex="89"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Fire Staircase(No,Size) </label>
                                                                <asp:TextBox ID="txtFireStaircase" runat="server" CssClass="form-control" TabIndex="90"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">


                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>AC outdoor space </label>
                                                                <asp:TextBox ID="txtACOutdoor" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Any existing Acs at location</label>
                                                                <asp:TextBox ID="anyACSatloc" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Signage / Branding</label>
                                                                <asp:TextBox ID="txtsignOrbrand" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Fire Exit stairs</label>
                                                                <asp:TextBox ID="txtFireExitstairs" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Provision for fire fighting system</label>
                                                                <asp:TextBox ID="txtFireFightingSystem" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lifts available and License</label>
                                                                <asp:TextBox ID="txtLiftNLicense" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Maintenance agency ( For Lift)</label>
                                                                <asp:TextBox ID="txtMaintAgencyLift" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDistanceMapping">Distance Mapping</a>
                                                </h4>
                                            </div>
                                            <div id="collapseDistanceMapping" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Distance From Airport</label>
                                                                <asp:TextBox ID="txtDistanceFromAirPort" runat="server" CssClass="form-control" MaxLength="12" TabIndex="84"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Distance From Railway Station</label>
                                                                <asp:TextBox ID="txtDistanceFromRailwayStation" runat="server" TabIndex="85" MaxLength="17" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Distance From Bus Stop</label>
                                                                <asp:TextBox ID="txtDistanceFromBustop" runat="server" CssClass="form-control" TabIndex="86"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>





                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseAreaCostDtls">Area & Cost Details</a>
                                                </h4>
                                            </div>
                                            <div id="collapseAreaCostDtls" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>CTS (City Survey) Number<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtLnumber" runat="server" ControlToValidate="txtLnumber" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Enter CTS Number"></asp:RequiredFieldValidator>
                                                                <div onmouseover="Tip('Enter Alphabets,Numbers and some special characters like /-\ with maximum length 50')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtLnumber" runat="server" CssClass="form-control" MaxLength="50" TabIndex="2"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Entitled Lease Amount<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="txtentitle" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Enter Entitled Lease Amount"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftbetxtentitle" runat="server" TargetControlID="txtentitle" FilterType="Numbers" ValidChars="0123456789." />
                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtentitle" runat="server" CssClass="form-control" TabIndex="3">0</asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Security Deposit<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="txtpay"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Security Deposit"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtpay" runat="server" TargetControlID="txtpay" FilterType="Numbers" ValidChars="0123456789." />
                                                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtpay" runat="server" CssClass="form-control" TabIndex="5" MaxLength="26"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Security Deposited Months<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="ddlSecurityDepMonths" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Select Property Type" InitialValue="0"></asp:RequiredFieldValidator>
                                                                <asp:RequiredFieldValidator ID="rfvddlSecurityDepMonths" runat="server" ControlToValidate="ddlSecurityDepMonths" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Select Security Deposited Months" InitialValue="0"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlSecurityDepMonths" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="6">
                                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                                    <asp:ListItem Value="6">6</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Interior Cost (Approx)</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtInteriorCost" runat="server" TargetControlID="txtInteriorCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtInteriorCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="10"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Rent Cycle</label>
                                                                <asp:DropDownList ID="ddlTenure" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="24"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseBrokegeDetails">Brokerage Details</a>
                                                </h4>
                                            </div>
                                            <div id="collapseBrokegeDetails" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Amount Of Brokerage Paid</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtbrkamount" runat="server" TargetControlID="txtbrkamount" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtbrkamount" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Broker Name</label>
                                                                <asp:TextBox ID="txtbrkname" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Broker PAN Number</label>
                                                                <asp:RegularExpressionValidator ID="regpanbrk" runat="server" ControlToValidate="txtbrkpan"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Broker Pan number in Alphanumerics only"
                                                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                                                <asp:RegularExpressionValidator ID="regExTextBox1" runat="server" ControlToValidate="txtbrkpan"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Broker Pan card Minimum length is 10"
                                                                    ValidationExpression=".{10}.*" />
                                                                <asp:TextBox ID="txtbrkpan" runat="server" TabIndex="68" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Contact Details</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtbrkmob" runat="server" TargetControlID="txtbrkmob" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtbrkmob" runat="server" CssClass="form-control" TabIndex="69" Width="97%" MaxLength="15"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Broker Email</label>
                                                                <asp:RegularExpressionValidator ID="revbrkremail" runat="server" ControlToValidate="txtbrkremail"
                                                                    ErrorMessage="Please Enter valid Email" Display="None" ValidationGroup="Val1"
                                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                <asp:TextBox ID="txtbrkremail" runat="server" CssClass="form-control" MaxLength="50" TabIndex="70"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Broker Address</label>
                                                                <asp:TextBox ID="txtbrkaddr" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" TabIndex="71" Rows="5"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseCharges">Charges</a>
                                                </h4>
                                            </div>
                                            <div id="collapseCharges" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Registration Charges<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtregcharges" runat="server" ControlToValidate="txtregcharges" ValidationGroup="Val1" Display="None"
                                                                    ErrorMessage="Please Enter Registration Charges"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtregcharges" runat="server" TargetControlID="txtregcharges" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtregcharges" runat="server" CssClass="form-control" TabIndex="11"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Stamp Duty Charges<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtsduty" runat="server" ControlToValidate="txtsduty" ValidationGroup="Val1" Display="None"
                                                                    ErrorMessage="Please Enter Stamp Duty Charges"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtsduty" runat="server" TargetControlID="txtsduty" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtsduty" runat="server" CssClass="form-control" MaxLength="12" TabIndex="12"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Furniture & Fixtures Charges</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtfurniture" runat="server" TargetControlID="txtfurniture" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtfurniture" runat="server" TabIndex="13" MaxLength="17" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Consultancy/Brokerage Charges</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtbrokerage" runat="server" TargetControlID="txtbrokerage" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtbrokerage" runat="server" CssClass="form-control" TabIndex="14"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Professional Fees</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtpfees" runat="server" TargetControlID="txtpfees" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtpfees" runat="server" CssClass="form-control" MaxLength="12" TabIndex="15"></asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>
                                                                    Other Taxes</label>
                                                                <cc1:FilteredTextBoxExtender ID="FtetxtOtherTax" runat="server" TargetControlID="txtOtherTax" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtOtherTax" runat="server" TabIndex="13" CssClass="form-control" MaxLength="17"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseLeaseExpenses">Other Expenses</a>
                                                </h4>
                                            </div>
                                            <div id="collapseLeaseExpenses" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="clearfix">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="gvLeaseExpences" runat="server" AllowPaging="false" AutoGenerateColumns="false" EmptyDataText="No Record(s) Found."
                                                                CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Service Type">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblServiceType" runat="server" Text='<%#Eval("NAME")%>'></asp:Label>
                                                                            <asp:Label ID="lblServiceID" Visible="false" runat="server" Text='<%#Eval("CODE")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Service Provider">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlServiceProvider" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Input Type">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlInputType" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                <asp:ListItem Value="Percentage">Percentage(%)</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Component of the lease value">
                                                                        <ItemStyle Width="10%" />
                                                                        <ItemTemplate>
                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtValue" runat="server" TargetControlID="txtValue" FilterType="Numbers" ValidChars=".0123456789." />
                                                                            <asp:TextBox ID="txtValue" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Paid by">
                                                                        <ItemStyle Width="10%" />
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlPaidBy" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseAgrmntDetails">Agreement Details</a>
                                                </h4>
                                            </div>
                                            <div id="collapseAgrmntDetails" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Effective Date of Agreement<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvsdate" runat="server" ControlToValidate="txtsdate"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Effective Date of Agreement"></asp:RequiredFieldValidator>
                                                                <div class='input-group date' id='effdate'>
                                                                    <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtsdate" runat="server" CssClass="form-control" TabIndex="55" AutoPostBack="true"></asp:TextBox>
                                                                    </div>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Expiry Date of Agreement<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvedate" runat="server" ControlToValidate="txtedate"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Expiry Date of Agreement"></asp:RequiredFieldValidator>
                                                                <div class='input-group date' id='fromdate'>
                                                                    <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                        <asp:TextBox ID="txtedate" runat="server" CssClass="form-control" TabIndex="56" AutoPostBack="true"> </asp:TextBox>
                                                                    </div>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lock in Period (In Months)</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtlock" runat="server" TargetControlID="txtlock" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtlock" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lease Period (In Years)</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtLeasePeiodinYears" runat="server" TargetControlID="txtLeasePeiodinYears" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtLeasePeiodinYears" runat="server" CssClass="form-control" TabIndex="58"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Notice Period (In Months)</label>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtNotiePeriod" runat="server" TargetControlID="txtNotiePeriod" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtNotiePeriod" runat="server" CssClass="form-control" MaxLength="12" TabIndex="59"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Agreement To be Signed By POA<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvddlAgreementbyPOA" runat="server" ControlToValidate="ddlAgreementbyPOA" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Select Agreement To be Signed By POA" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlAgreementbyPOA" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <label>Reminder Before<span style="color: red;">*</span></label>
                                                            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one reminder in Lease Escalation details."
                                                                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" Display="None" runat="server" ValidationGroup="Val1" />
                                                            <div class="bootstrap-tagsinput">
                                                                <asp:CheckBoxList ID="ReminderCheckList" runat="server" CellPadding="25" CellSpacing="100" RepeatColumns="3" RepeatLayout="Table" RepeatDirection="Vertical" CausesValidation="True" TabIndex="65">
                                                                    <asp:ListItem Value="365">1 Year </asp:ListItem>
                                                                    <asp:ListItem Value="60">60 Days</asp:ListItem>
                                                                    <asp:ListItem Value="10">10 Days</asp:ListItem>
                                                                    <asp:ListItem Value="2">2 Days</asp:ListItem>
                                                                    <asp:ListItem Value="210">7 Months </asp:ListItem>
                                                                    <asp:ListItem Value="30">30 Days</asp:ListItem>
                                                                    <asp:ListItem Value="7">7 Days</asp:ListItem>
                                                                    <asp:ListItem Value="1">1 Day</asp:ListItem>
                                                                    <asp:ListItem Value="90">90 Days</asp:ListItem>
                                                                    <asp:ListItem Value="20">20 Days</asp:ListItem>
                                                                    <asp:ListItem Value="3">3 Days</asp:ListItem>

                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Email address</label>
                                                                <%-- <div onmouseover="Tip('Enter Comments with maximum 750 Characters')" onmouseout="UnTip()">--%>
                                                                <div>
                                                                    <asp:TextBox ID="txtReminderEmail" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" MaxLength="1000" TabIndex="63"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="RentRevisionPanel" runat="server" class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseRentRevision">Rent Revision (Variable % for every year)</a>
                                                </h4>
                                            </div>
                                            <div id="collapseRentRevision" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <asp:Panel ID="Panel10" runat="server"></asp:Panel>
                                                    <asp:Repeater ID="rpRevision" runat="server">
                                                        <ItemTemplate>
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <asp:Label ID="lblRevYear" runat="server" Text='<%# Eval("RR_Year")%>'> </asp:Label>
                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtRevision" runat="server" TargetControlID="txtRevision" FilterType="Numbers" ValidChars="0123456789." />
                                                                    <asp:TextBox ID="txtRevision" class="fa-percent" runat="server" CssClass="form-control" MaxLength="12" Text='<%# Eval("RR_Percentage")%>'></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="panPOA" runat="server" visible="false" class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsepanPOA">Power of Attorney Details</a>
                                                </h4>
                                            </div>
                                            <div id="collapsepanPOA" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Name<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvPOAName" runat="server" ControlToValidate="txtPOAName"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Name of Power of Attorney"></asp:RequiredFieldValidator>
                                                                <asp:TextBox ID="txtPOAName" runat="server" CssClass="form-control" TabIndex="32" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Address<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvPOAAddress" runat="server" ControlToValidate="txtPOAAddress"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Address of Power of Attorney"></asp:RequiredFieldValidator>
                                                                <asp:TextBox ID="txtPOAAddress" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" Rows="4" TabIndex="33"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Contact Details<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Contact Details of Power of Attorney"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtPOAMobile" runat="server" TargetControlID="txtPOAMobile" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtPOAMobile" runat="server" CssClass="form-control" MaxLength="12" TabIndex="34"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Email-ID</label>
                                                                <asp:RegularExpressionValidator ID="revPOAEmail" runat="server" ControlToValidate="txtPOAEmail"
                                                                    ErrorMessage="Please Enter valid Email of Power of Attorney" Display="None" ValidationGroup="Val1"
                                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                <asp:TextBox ID="txtPOAEmail" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="35"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseleaseEscDetails">Lease Escalation Details</a>
                                                </h4>
                                            </div>
                                            <div id="collapseleaseEscDetails" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Do You Wish To Enter Lease Escalation<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvesc" runat="server" ControlToValidate="ddlesc"
                                                                    Display="None" ValidationGroup="Val1" InitialValue="--Select--" ErrorMessage="Please Select Lease Escalation"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlesc" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="60">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lease Escalation Type<span style="color: red;">*</span></label>
                                                                <asp:DropDownList ID="ddlLeaseEscType" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="61">
                                                                    <asp:ListItem Value="FLT">Flat Amount</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lease Hold Improvements<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtLeaseHoldImprovements" runat="server" ControlToValidate="txtLeaseHoldImprovements"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Lease Hold Improvements"></asp:RequiredFieldValidator>
                                                                <asp:TextBox ID="txtLeaseHoldImprovements" runat="server" TabIndex="62" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lease Comments</label>
                                                                <div onmouseover="Tip('Enter Comments with maximum 750 Characters')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" MaxLength="1000" TabIndex="63"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Due Diligence certification<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvddlDueDilegence" runat="server" ControlToValidate="ddlDueDilegence" Display="None" ValidationGroup="Val1"
                                                                    InitialValue="--Select--" ErrorMessage="Please Select Due Diligence certification"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlDueDilegence" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="64">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOfficeConnectivity">Office connectivity</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOfficeConnectivity" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Telecom - service providers (in the building)</label>
                                                                <asp:TextBox ID="txtTeleSPB" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Telecom - service providers (incase first-occupant)</label>
                                                                <asp:TextBox ID="txtTeleSPFO" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lease line service providers (in the building)</label>
                                                                <asp:TextBox ID="txtLeaseSPB" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Lease line service providers (incase first-occupant)</label>
                                                                <asp:TextBox ID="txtLeaseSPFO" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Telecom vendor feasibility report</label>
                                                                <asp:TextBox ID="txtTeleVenFR" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>IT Vendor feasibility report</label>
                                                                <asp:TextBox ID="txtITVenFR" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseCivil">Civil</a>
                                                </h4>
                                            </div>
                                            <div id="collapseCivil" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Existing punning on walls</label>
                                                                <asp:TextBox ID="txtPunnWall" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Cracks on external glazing</label>
                                                                <asp:TextBox ID="txtCracks" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Washrooms /Toilet </label>
                                                                <asp:TextBox ID="txtWashToilet" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Pantry </label>
                                                                <asp:TextBox ID="txtPantry" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Drainage line drawings </label>
                                                                <asp:TextBox ID="txtDrainagelinedrawings" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Window Height</label>
                                                                <asp:TextBox ID="txtWindowHeight" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Any Water leakage</label>
                                                                <asp:TextBox ID="txtWaterLeak" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Staircase</label>
                                                                <asp:TextBox ID="txtStaircase" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>



                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Emergency Exit </label>
                                                                <asp:TextBox ID="txtEmergExit" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Height Availability </label>
                                                                <asp:TextBox ID="txtHeightAvailability" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Any additional civil work (whether in case of landlord or Max Life)</label>
                                                                <asp:TextBox ID="txtAddtnlCivilWork" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                        </div>





                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherDetails">Outstanding amounts/ Damages</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOtherDetails" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Any Outstanding amounts/ Damages  </label>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlOutOrDamagesAmount"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Lift"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlOutOrDamagesAmount" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Damages for extended Stay  </label>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlOutOrDamagesAmount"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Lift"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlDamangesExtStay" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Possession hand-over in as is whereis basis  </label>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlPossessionHandover"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Lift"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="ddlPossessionHandover" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>No.Of Landlords<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtNoLanlords" runat="server" ControlToValidate="txtNoLanlords" InitialValue="0"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter No.Of Landlords"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtNoLanlords" runat="server" TargetControlID="txtNoLanlords" FilterType="Numbers" ValidChars="0123456789" />
                                                                <asp:TextBox ID="txtNoLanlords" runat="server" CssClass="form-control" TabIndex="30"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Office Equipment</label>
                                                                <asp:TextBox ID="txtOfficeEquipments" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Working Hrs. of Construction </label>
                                                                <asp:TextBox ID="txtWorkHrs" runat="server" CssClass="form-control" TabIndex="30"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Restriction on usage of common toilet </label>

                                                                <asp:TextBox ID="txtCommonToilet" runat="server" CssClass="form-control" TabIndex="30"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">





                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Restriction on usage of service lift  </label>

                                                                <asp:TextBox ID="txtServiceLift" runat="server" CssClass="form-control" TabIndex="30"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Security System   </label>

                                                                <asp:TextBox ID="txtSecSystem" runat="server" CssClass="form-control" TabIndex="30"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>



                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseLeaseDocs">Building & Legal Documents</a>
                                                </h4>
                                            </div>
                                            <div id="collapseLeaseDocs" class="panel-collapse collapse in">
                                                <div class="panel-body color">
                                                    <div class="clearfix">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="gvLegalDocs" runat="server" AllowPaging="false" AutoGenerateColumns="false" EmptyDataText="No Record(s) Found."
                                                                CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Document Title" ItemStyle-Width="50%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDocType" runat="server" Text='<%#Eval("PM_DOC_TITLE")%>'></asp:Label>
                                                                            <asp:Label ID="lblDocSNO" Visible="false" runat="server" Text='<%#Eval("PMLD_DOC_SNO")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="Upload Document" ItemStyle-Width="90%">
                                                                        <ItemStyle Width="10%" />
                                                                        <ItemTemplate>
                                                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                                                            <asp:FileUpload ID="fuLegalDoc" runat="Server" AllowMultiple="False" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Upload Files/Images</label>
                                                    <div class="btn btn-primary btn-mm">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color " Text="Submit"></asp:Button>
                                                <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
                                            </div>
                                        </div>
                                    </div>

                                    <div id="Landlord" visible="false" runat="server" class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseLandLordDetails">Add Landlord Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseLandLordDetails" class="panel-collapse collapse in">
                                            <div class="panel-body color">

                                                <div class="row" id="TotalLandlords" runat="server" visible="false" style="padding-bottom: 10px;">
                                                    <div class="col-md-12 col-sm-3 col-xs-12">
                                                        <div class="form-group">
                                                            <asp:GridView ID="gvLandlords" runat="server" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True"
                                                                PageSize="15" EmptyDataText="Landlord details not exists." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                <PagerSettings Mode="NumericFirstLast" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="S.No">
                                                                        <ItemTemplate>
                                                                            <%#Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="LSNO" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblsno" runat="server" Text='<%#Eval("PM_LL_SNO")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Landlord Name">
                                                                        <ItemTemplate>
                                                                            <%--<asp:LinkButton ID="lnksurrender" runat="server" Text='<%#Eval("PM_LL_NAME")%>' CommandArgument='<%#Eval("PM_LL_SNO")%>'
                                                                            CommandName="GetLandlordDetails"></asp:LinkButton>--%>
                                                                            <asp:Label ID="lblLLName" runat="server" Text='<%#Eval("PM_LL_NAME")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Address 1">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLLAddress1" runat="server" Text='<%#Eval("PM_LL_ADDRESS1")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="PAN No.">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLLPAN" runat="server" Text='<%#Eval("PM_LL_PAN")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Rent">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRentAmount" runat="server" Text='<%# Eval("PM_LL_MON_RENT_PAYABLE", "{0:c2}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Security Deposit Amount">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblsecamount" runat="server" Text='<%# Eval("PM_LL_SECURITY_DEPOSIT","{0:c2}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Name<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" ControlToValidate="txtName"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Landlord1 Name"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" TabIndex="30"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Address 1<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtAddress" runat="server" ControlToValidate="txtAddress"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Landlord1 Address 1"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TabIndex="31"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Address 2</label>
                                                            <asp:TextBox ID="txtAddress2" runat="server" TabIndex="32" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Address 3</label>
                                                            <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control" TabIndex="33"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>State<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtState" runat="server" ControlToValidate="txtL1State"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Landlord1 State"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtL1State" runat="server" CssClass="form-control" TabIndex="33"></asp:TextBox>
                                                        </div>
                                                    </div>--%>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>State<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtState" runat="server" ControlToValidate="ddlState"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select State" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True" >
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>City<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="35">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>PIN CODE<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvld1pin" runat="server" ControlToValidate="txtld1Pin"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Landlord1 Pin number"></asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="ftetxtld1Pin" runat="server" TargetControlID="txtld1Pin" FilterType="Numbers" ValidChars="0123456789" />
                                                            <div onmouseover="Tip('Enter PIN No with maximum length 10')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtld1Pin" runat="server" CssClass="form-control" TabIndex="36" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>PAN No<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtPAN" runat="server" ControlToValidate="txtPAN"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Landlord1 PAN No"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revtxtPAN" runat="server" ControlToValidate="txtPAN" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Enter Landlord1 Pan number in Alphanumerics only" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                                            <asp:RegularExpressionValidator ID="revtxtPAN1" runat="server" ControlToValidate="txtPAN" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Landlord1 Pan card Minimum length should be 10" ValidationExpression=".{10}.*" />
                                                            <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtPAN" runat="server" CssClass="form-control" TabIndex="37" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Contact Details<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtContactDetails" runat="server" ControlToValidate="txtContactDetails"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Lanlord1 Contact Details"></asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="ftetxtContactDetails" runat="server" TargetControlID="txtContactDetails" FilterType="Numbers" ValidChars="0123456789." />
                                                            <asp:TextBox ID="txtContactDetails" runat="server" CssClass="form-control" TabIndex="42"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Email</label>
                                                            <asp:RegularExpressionValidator ID="revldemail" runat="server" ControlToValidate="txtldemail"
                                                                ErrorMessage="Please Enter valid Email of Landlord1" Display="None" ValidationGroup="Val1"
                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                            <asp:TextBox ID="txtldemail" runat="server" CssClass="form-control" TabIndex="43" MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Amount In<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="RFVddlAmountIn" runat="server" ControlToValidate="ddlAmountIn" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Amount In" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlAmountIn" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                <asp:ListItem Value="Percentage">Percentage</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rent Payable</label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtpmonthrent" runat="server" ControlToValidate="txtpmonthrent"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Lanlord1 Monthly Rent Payable"></asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="ftetxtpmonthrent" runat="server" TargetControlID="txtpmonthrent" FilterType="Numbers" ValidChars="0123456789." />
                                                            <asp:TextBox ID="txtpmonthrent" runat="server" CssClass="form-control" MaxLength="15" TabIndex="44"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Security Deposit <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtpsecdep" runat="server" ControlToValidate="txtpsecdep"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Lanlord1 Security Deposit"></asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="ftetxtpsecdep" runat="server" TargetControlID="txtpsecdep" FilterType="Numbers" ValidChars="0123456789." />
                                                            <asp:TextBox ID="txtpsecdep" runat="server" TabIndex="45" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Type Of Agreement<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlOffice"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Type Of Agreement" InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlOffice" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>PaymentMode<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvddlpaymentmode" runat="server" ControlToValidate="ddlpaymentmode" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Payment Mode" InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlpaymentmode" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True" TabIndex="48">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div id="panel1" runat="server">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Bank Name <span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revBankName" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtBankName"
                                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtBankName" runat="server" TabIndex="49" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Account Number<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtAccNo" runat="server" TargetControlID="txtAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtAccNo" runat="server" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row" id="panel2" runat="server">

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Bank Name <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtNeftBank" runat="server" ControlToValidate="txtNeftBank"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revtxtNeftBank" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtNeftBank"
                                                                ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtNeftBank" runat="server" TabIndex="51" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Account Number <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtNeftAccNo" runat="server" ControlToValidate="txtNeftAccNo"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="ftetxtNeftAccNo" runat="server" TargetControlID="txtNeftAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtNeftAccNo" runat="server" TabIndex="52" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Branch Name <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtNeftBrnch" runat="server" ControlToValidate="txtNeftBrnch"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revtxtNeftBrnch" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtNeftBrnch"
                                                                ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtNeftBrnch" runat="server" TabIndex="53" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>IFSC Code <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtNeftIFSC" runat="server" ControlToValidate="txtNeftIFSC"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter IFSC Code"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revtxtNeftIFSC" Display="None" ValidationGroup="Val1"
                                                                runat="server" ControlToValidate="txtNeftIFSC" ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtNeftIFSC" runat="server" TabIndex="54" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 text-right">
                                                    <asp:Button ID="btnLandlord" runat="server" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color " Text="Add More Landlord"></asp:Button>
                                                    <asp:Button ID="btnFinalize" Enabled="false" runat="server" CssClass="btn btn-primary custom-button-color " Text="Close"></asp:Button>
                                                    <%--<asp:Button ID="btnBack" Enabled="false" runat="server" CssClass="btn btn-primary custom-button-color " Text="Back"></asp:Button>--%>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <asp:Label ID="lblMsgLL" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                                </asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--</ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script>
    $('.closeall').click(function () {
        $('.panel-collapse.in')
          .collapse('hide');
    });
    $('.openall').click(function () {
        $('.panel-collapse:not(".in")')
          .collapse('show');
    });
</script>

