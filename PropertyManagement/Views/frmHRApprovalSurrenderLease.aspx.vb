Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmSurrenderLease
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            BindGrid()
            panel.Visible = False
            panel2.Visible = False
            panel3.Visible = False
            PaymentMode()
        End If
        txtTdate.Attributes.Add("readonly", "readonly")

    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_APPROVE_GRID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "Surrender" Then

            Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
            Dim lblLseName As Label = DirectCast(gvRow.FindControl("lblLeaseName"), Label)
            txtLeaseId.Text = lblLseName.Text

            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_FILL_DETAILS")
            SP.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            SP.Command.AddParameter("@LEASE_ID", lblLseName.Text, DbType.String)
            Dim dr As SqlDataReader = SP.GetReader()
            If dr.Read() Then

                txtsurrender.Text = dr("SURRENDER_DT")
                txtrefund.Text = dr("PM_SL_REFUND_DT")
                txtAmtPaidLesse.Text = Format(dr("LESSE_AMT"), "0.00")
                txtAmtPaidLessor.Text = Format(dr("LESSOR_AMT"), "0.00")
                txtSDamount.Text = Format(dr("SECURITY_AMT"), "0.00")
                txtOutAmt.Text = Format(dr("OUT_AMT"), "0.00")
                txtDmg.Text = dr("PM_SL_DAMAGES")
                txtDmgAmt.Text = Format(dr("DAMAGE_AMT"), "0.00")
                If dr("DOC_NAME") = "" Then
                    DocLink.Text = "No Possession Letter Found"
                Else
                    DocLink.Text = dr("DOC_NAME")
                    Session("Document") = DocLink.Text
                End If
                txtsurrender.Enabled = False
                txtrefund.Enabled = False
            End If
            panel.Visible = True
        Else
            panel.Visible = False
        End If
    End Sub
    Protected Sub ddlPaymentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub PaymentMode()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PAYMENT_MODE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlPaymentType.DataSource = sp.GetDataSet()
        ddlPaymentType.DataTextField = "NAME"
        ddlPaymentType.DataValueField = "CODE"
        ddlPaymentType.DataBind()
        ddlPaymentType.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click

        Dim orgfilename As String = BrowsePossLtr.FileName
        Dim filename As String = ""

        Try
            If (BrowsePossLtr.HasFile) Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(BrowsePossLtr.FileName)
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
                BrowsePossLtr.PostedFile.SaveAs(filePath)
                filename = orgfilename
                'lblMsg.Visible = True
                'lblMsg.Text = "File upload successfully"

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_APPROVE")
            sp.Command.AddParameter("@LEASE_ID", txtLeaseId.Text, DbType.String)
            sp.Command.AddParameter("@APPRV_REMARKS", txtRemarks.Text, DbType.String)
            sp.Command.AddParameter("@PAY_MODE", ddlPaymentType.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@PAY_DATE", txtTdate.Text, DbType.Date)
            sp.Command.AddParameter("@DOC_NAME", filename, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)

            If ddlPaymentType.SelectedItem.Value = "2" Then

                sp.Command.AddParameter("@ACC_SERIAL_NO", txtAccNo.Text, DbType.String)
                sp.Command.AddParameter("@BANK_NAME", txtBankName.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
                sp.Command.AddParameter("@IFSC", "", DbType.String)

            ElseIf ddlPaymentType.SelectedItem.Value = "3" Then

                sp.Command.AddParameter("@ACC_SERIAL_NO", txtAccTwo.Text, DbType.String)
                sp.Command.AddParameter("@BANK_NAME", txtBankTwo.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", txtbrnch.Text, DbType.String)
                sp.Command.AddParameter("@IFSC", txtIFSC.Text, DbType.String)

            ElseIf ddlPaymentType.SelectedItem.Value = "4" Then

                sp.Command.AddParameter("@ACC_SERIAL_NO", txtAccNo.Text, DbType.String)
                sp.Command.AddParameter("@BANK_NAME", txtBankName.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
                sp.Command.AddParameter("@IFSC", "", DbType.String)

            End If
            sp.ExecuteScalar()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        lblMsg.Visible = True
        lblMsg.Text = "Surrender Lease Approved Successfully"

    End Sub

    Protected Sub ddlPaymentType_SelectedIndexChanged1(sender As Object, e As EventArgs) Handles ddlPaymentType.SelectedIndexChanged
        If ddlPaymentType.SelectedIndex > 0 Then
            If ddlPaymentType.SelectedItem.Value = "1" Then
                panel2.Visible = False
                panel3.Visible = False
            ElseIf ddlPaymentType.SelectedItem.Value = "2" Then
                panel2.Visible = True
                panel3.Visible = False
            ElseIf ddlPaymentType.SelectedItem.Value = "3" Then
                panel2.Visible = False
                panel3.Visible = True
            ElseIf ddlPaymentType.SelectedItem.Value = "4" Then
                panel2.Visible = True
                panel3.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnsearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        Try
            fillgridOnSearch()
            panel.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub fillgridOnSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_APPROVE_GRID_BY_SEARCH")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@SEARCH", txtReqId.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvItems.DataSource = Session("dataset")
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtReqId.Text = ""
        BindGrid()
        panel.Visible = False
    End Sub

    Protected Sub DocLink_Click(sender As Object, e As EventArgs) Handles DocLink.Click
        Try
            Response.ContentType = ContentType
            Dim DocPath As String = "~/UploadFiles/" + Session("Document")
            Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(DocPath)))
            Response.WriteFile(DocPath)
            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
End Class
