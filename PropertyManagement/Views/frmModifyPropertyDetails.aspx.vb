Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmModifyPropertyDetails
    Inherits System.Web.UI.Page
    Dim BoardApprovalDoc As String
    Dim IRDAApprovalDoc As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtReqID.Text = Request.QueryString("id")
            BindRequestTypes()
            BindPropType()
            BindInsuranceType()
            BindUserCities()
            BindFlooringTypes()
            BindDetails()
            If Request.QueryString("staid") <> "4001" Then  'If Approved/Reject then readonly all control values and hide submit btn
                EnableControls(Me.Page.Form.Controls, False)
                btnSubmit.Visible = False
            End If

        End If
    End Sub

    Private Sub BindOfficeType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_OFFICE_TYPE")
        ddlOffice.DataSource = sp3.GetDataSet()
        ddlOffice.DataTextField = "PM_TOO"
        ddlOffice.DataValueField = "PM_OID"
        ddlOffice.DataBind()
        ddlOffice.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Public Sub EnableControls(ctrl As ControlCollection, isEnable As Boolean)
        For Each item As Control In ctrl
            If item.[GetType]() = GetType(Panel) OrElse item.[GetType]() = GetType(HtmlGenericControl) Then
                EnableControls(item.Controls, isEnable)
            ElseIf item.[GetType]() = GetType(DropDownList) Then
                DirectCast(item, DropDownList).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(TextBox) Then
                DirectCast(item, TextBox).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(FileUpload) Then
                DirectCast(item, FileUpload).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(HtmlInputButton) Then
                DirectCast(item, HtmlInputButton).Disabled = Not isEnable
            End If
        Next
    End Sub

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub

    Private Sub BindReqId()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GENERATE_REQUESTID")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDPROPERTY", DbType.String)
        Dim dt = DateTime.Now.ToString("MMddyyyyHHmm")
        txtReqID.Text = dt + "/PRPREQ/" + Convert.ToString(sp3.ExecuteScalar())
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select Request Type--", 0))
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", 0))
    End Sub

    Private Sub BindInsuranceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select Insurance Type--", 0))
    End Sub

    Private Sub GetLocationsbyCity(ByVal city As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", city)
        sp2.Command.AddParameter("@USR_ID", Session("uid"))
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            GetLocationsbyCity(ddlCity.SelectedValue)
            GetZoneAndStateByCityCode()
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Private Sub GetTowerbyLoc(ByVal loc As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
        sp2.Command.AddParameter("@LCMID", loc, DbType.String)
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlTower.DataSource = sp2.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select Tower--", "0"))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            GetTowerbyLoc(ddlLocation.SelectedValue)
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Private Sub GetFloorsbyTwr(ByVal tower As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
        sp2.Command.AddParameter("@TWR_ID", tower, DbType.String)
        sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@FLR_CITY_CODE", ddlCity.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlFloor.DataSource = sp2.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select Floor--", "0"))
        txtFloor.Text = ddlFloor.Items.Count - 1
    End Sub


    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            GetFloorsbyTwr(ddlTower.SelectedValue)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub


    Protected Sub txtESTD_TextChanged(sender As Object, e As EventArgs) Handles txtESTD.TextChanged
        If Integer.TryParse(txtESTD.Text, 0) Then
            txtAge.Text = DateTime.Now.Year - Convert.ToInt32(txtESTD.Text)
        Else
            txtAge.Text = ""
        End If
    End Sub

    Protected Sub ddlRecommended_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRecommended.SelectedIndexChanged
        divRecommanded.Visible = IIf(ddlRecommended.SelectedValue = "1", True, False)
    End Sub

    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    Private Sub Cleardata()
        ddlReqType.SelectedIndex = 0
        ddlPprtNature.SelectedIndex = 0
        ddlAcqThr.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlLocation.Items.Clear()
        ddlTower.Items.Clear()
        txtFloor.Text = ""
        ddlFloor.Items.Clear()
        txtToilet.Text = ""
        ddlPropertyType.SelectedIndex = 0
        txtPropIDName.Text = ""
        txtESTD.Text = ""
        txtAge.Text = ""
        txtPropDesc.Text = ""
        txtSignageLocation.Text = ""
        txtSocityName.Text = ""
        txtlat.Text = ""
        txtlong.Text = ""
        txtOwnScopeWork.Text = ""
        txtRecmRemarks.Text = ""

        txtownrname.Text = ""
        txtphno.Text = ""
        txtPrvOwnName.Text = ""
        txtPrvOwnPhNo.Text = ""
        txtOwnEmail.Text = ""

        txtCarpetArea.Text = ""
        txtBuiltupArea.Text = ""
        txtCommonArea.Text = ""
        txtRentableArea.Text = ""
        txtUsableArea.Text = ""
        txtSuperBulArea.Text = ""
        txtPlotArea.Text = ""
        txtCeilingHight.Text = ""
        txtBeamBottomHight.Text = ""
        txtMaxCapacity.Text = ""
        txtOptCapacity.Text = ""
        txtSeatingCapacity.Text = ""
        ddlFlooringType.SelectedIndex = 0
        txtFSI.Text = ""
        txtEfficiency.Text = ""

        txtPurPrice.Text = ""
        txtPurDate.Text = ""
        txtMarketValue.Text = ""

        ddlIRDA.SelectedValue = "0"
        txtPCcode.Text = ""
        txtGovtPropCode.Text = ""
        txtUOM_CODE.Text = ""
        ddlOffice.SelectedIndex = 0
        ddlInsuranceType.SelectedIndex = 0
        txtInsuranceVendor.Text = ""
        txtInsuranceAmt.Text = ""
        txtInsurancePolNum.Text = ""
        txtInsuranceStartdate.Text = ""
        txtInsuranceEnddate.Text = ""

        gvPropdocs.DataSource = Nothing
        gvPropdocs.DataBind()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than builtup area');</SCRIPT>", False)
        ElseIf CInt(txtCommonArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtCommonArea.Text) > CInt(txtBuiltupArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Common Area cannot be more than builtup area and Carpet');</SCRIPT>", False)

        ElseIf CInt(txtRentableArea.Text) > CInt(txtBuiltupArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtUsableArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Rentable area cannot be more than built up, carpet area and usable area');</SCRIPT>", False)
        Else
            UpdatepptyDetails()
            'Cleardata()
            'BindReqId()
        End If
    End Sub

    Public Sub UpdatepptyDetails()
        Try
            Dim param As SqlParameter() = New SqlParameter(80) {}

            'General Details
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = txtReqID.Text
            param(1) = New SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar)
            param(1).Value = ddlReqType.SelectedValue
            param(2) = New SqlParameter("@PROP_NATURE", SqlDbType.VarChar)
            param(2).Value = ddlPprtNature.SelectedValue
            param(3) = New SqlParameter("@ACQ_TRH", SqlDbType.VarChar)
            param(3).Value = ddlAcqThr.SelectedValue

            param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
            param(4).Value = ddlCity.SelectedValue
            param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
            param(5).Value = ddlLocation.SelectedValue
            param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
            param(6).Value = ddlTower.SelectedValue

            param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
            param(7).Value = ddlFloor.SelectedValue
            param(8) = New SqlParameter("@TOI_BLKS", SqlDbType.Int)
            param(8).Value = IIf(txtToilet.Text = "", 0, txtToilet.Text)
            param(9) = New SqlParameter("@PROP_TYPE", SqlDbType.VarChar)
            param(9).Value = ddlPropertyType.SelectedValue
            param(10) = New SqlParameter("@PROP_NAME", SqlDbType.VarChar)
            param(10).Value = txtPropIDName.Text

            param(11) = New SqlParameter("@ESTD_YR", SqlDbType.VarChar)
            param(11).Value = IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            param(12) = New SqlParameter("@AGE", SqlDbType.VarChar)
            param(12).Value = IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            param(13) = New SqlParameter("@PROP_ADDR", SqlDbType.VarChar)
            param(13).Value = txtPropDesc.Text
            param(14) = New SqlParameter("@SING_LOC", SqlDbType.VarChar)
            param(14).Value = txtSignageLocation.Text

            param(15) = New SqlParameter("@SOC_NAME", SqlDbType.VarChar)
            param(15).Value = txtSocityName.Text
            param(16) = New SqlParameter("@LATITUDE", SqlDbType.VarChar)
            param(16).Value = txtlat.Text
            param(17) = New SqlParameter("@LONGITUDE", SqlDbType.VarChar)
            param(17).Value = txtlong.Text
            param(18) = New SqlParameter("@SCOPE_WK", SqlDbType.VarChar)
            param(18).Value = txtOwnScopeWork.Text

            param(19) = New SqlParameter("@RECM_PROP", SqlDbType.VarChar)
            param(19).Value = ddlRecommended.SelectedValue
            param(20) = New SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar)
            param(20).Value = txtRecmRemarks.Text

            'Owner Details
            param(21) = New SqlParameter("@OWN_NAME", SqlDbType.VarChar)
            param(21).Value = txtownrname.Text
            param(22) = New SqlParameter("@OWN_PH", SqlDbType.VarChar)
            param(22).Value = txtphno.Text
            param(23) = New SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar)
            param(23).Value = txtPrvOwnName.Text
            param(24) = New SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar)
            param(24).Value = txtPrvOwnPhNo.Text

            param(25) = New SqlParameter("@OWN_EMAIL", SqlDbType.VarChar)
            param(25).Value = txtOwnEmail.Text

            'Area Details
            param(26) = New SqlParameter("@CARPET_AREA", SqlDbType.Decimal)
            param(26).Value = Convert.ToDecimal(txtCarpetArea.Text)
            param(27) = New SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal)
            param(27).Value = Convert.ToDecimal(txtBuiltupArea.Text)
            param(28) = New SqlParameter("@COMMON_AREA", SqlDbType.Decimal)
            param(28).Value = Convert.ToDecimal(txtCommonArea.Text)
            param(29) = New SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal)
            param(29).Value = Convert.ToDecimal(txtRentableArea.Text)

            param(30) = New SqlParameter("@USABLE_AREA", SqlDbType.Decimal)
            param(30).Value = Convert.ToDecimal(txtUsableArea.Text)
            param(31) = New SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal)
            param(31).Value = Convert.ToDecimal(txtSuperBulArea.Text)
            param(32) = New SqlParameter("@PLOT_AREA", SqlDbType.Decimal)
            param(32).Value = Convert.ToDecimal(txtPlotArea.Text)
            param(33) = New SqlParameter("@FTC_HIGHT", SqlDbType.Decimal)
            param(33).Value = IIf(txtCeilingHight.Text = "", 0, txtCeilingHight.Text)

            param(34) = New SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal)
            param(34).Value = IIf(txtBeamBottomHight.Text = "", 0, txtBeamBottomHight.Text)
            param(35) = New SqlParameter("@MAX_CAPACITY", SqlDbType.Int)
            param(35).Value = IIf(txtMaxCapacity.Text = "", 0, txtMaxCapacity.Text)
            param(36) = New SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int)
            param(36).Value = IIf(txtOptCapacity.Text = "", 0, txtOptCapacity.Text)
            param(37) = New SqlParameter("@SEATING_CAPACITY", SqlDbType.Int)
            param(37).Value = IIf(txtSeatingCapacity.Text = "", 0, txtSeatingCapacity.Text)

            param(38) = New SqlParameter("@FLR_TYPE", SqlDbType.VarChar)
            param(38).Value = ddlFlooringType.SelectedValue
            param(39) = New SqlParameter("@FSI", SqlDbType.VarChar)
            param(39).Value = ddlFSI.SelectedValue
            param(40) = New SqlParameter("@FSI_RATIO", SqlDbType.Decimal)
            param(40).Value = IIf(txtFSI.Text = "", 0, txtFSI.Text)
            param(41) = New SqlParameter("@PFR_EFF", SqlDbType.VarChar)
            param(41).Value = txtEfficiency.Text

            'PURCHASE DETAILS
            param(42) = New SqlParameter("@PUR_PRICE", SqlDbType.Decimal)
            param(42).Value = IIf(txtPurPrice.Text = "", 0, txtPurPrice.Text)
            param(43) = New SqlParameter("@PUR_DATE", SqlDbType.Date)
            param(43).Value = IIf(txtPurDate.Text = "", DBNull.Value, txtPurDate.Text)
            param(44) = New SqlParameter("@MARK_VALUE", SqlDbType.Decimal)
            param(44).Value = IIf(txtMarketValue.Text = "", 0, txtMarketValue.Text)

            'GOVT DETAILS
            param(45) = New SqlParameter("@IRDA", SqlDbType.VarChar)
            param(45).Value = ddlIRDA.SelectedValue
            param(46) = New SqlParameter("@PC_CODE", SqlDbType.VarChar)
            param(46).Value = txtPCcode.Text
            param(47) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar)
            param(47).Value = txtGovtPropCode.Text
            param(48) = New SqlParameter("@UOM_CODE", SqlDbType.VarChar)
            param(48).Value = txtUOM_CODE.Text

            'INSURANCE DETAILS
            param(49) = New SqlParameter("@IN_TYPE", SqlDbType.VarChar)
            param(49).Value = ddlInsuranceType.SelectedValue
            param(50) = New SqlParameter("@IN_VENDOR", SqlDbType.VarChar)
            param(50).Value = txtInsuranceVendor.Text
            param(51) = New SqlParameter("@IN_AMOUNT", SqlDbType.Decimal)
            param(51).Value = IIf(txtInsuranceAmt.Text = "", 0, txtInsuranceAmt.Text)
            param(52) = New SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar)
            param(52).Value = txtInsurancePolNum.Text

            param(53) = New SqlParameter("@IN_SDATE", SqlDbType.VarChar)
            param(53).Value = IIf(txtInsuranceStartdate.Text = "", DBNull.Value, txtInsuranceStartdate.Text)
            param(54) = New SqlParameter("@IN_EDATE", SqlDbType.VarChar)
            param(54).Value = IIf(txtInsuranceEnddate.Text = "", DBNull.Value, txtInsuranceEnddate.Text)

            param(55) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(55).Value = Session("uid")

            ' PROPERTY IMAGES UPLOAD
            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas

            If fu1.PostedFiles IsNot Nothing Then
                For Each File In fu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            param(56) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(56).Value = UtilityService.ConvertToDataTable(Imgclass)

            param(57) = New SqlParameter("@OD_EXISTING_MONTH_RENT", SqlDbType.VarChar)
            param(57).Value = IIf(txtExistMonthRent.Text = "", DBNull.Value, txtExistMonthRent.Text)

            param(58) = New SqlParameter("@OD_PROPOSED_AREA", SqlDbType.VarChar)
            param(58).Value = IIf(txtProposedAreaForAgency.Text = "", DBNull.Value, txtProposedAreaForAgency.Text)

            param(59) = New SqlParameter("@OD_PROPOSED_AREA_OTHER_CHANELS", SqlDbType.VarChar)
            param(59).Value = IIf(txtPropAreaOtherChannels.Text = "", DBNull.Value, txtPropAreaOtherChannels.Text)

            param(60) = New SqlParameter("@OD_PROP_MONTHLY_RENT", SqlDbType.VarChar)
            param(60).Value = IIf(txtPropMonthlyRental.Text = "", DBNull.Value, txtPropMonthlyRental.Text)

            param(61) = New SqlParameter("@OD_PROP_SQFT_RENT", SqlDbType.VarChar)
            param(61).Value = IIf(txtPropsedPerSqftrental.Text = "", DBNull.Value, txtPropsedPerSqftrental.Text)

            param(62) = New SqlParameter("@OD_BOARD_APPR", SqlDbType.VarChar)
            param(62).Value = ddlBoardAppr.SelectedValue

            param(63) = New SqlParameter("@OD_COST_TYPE", SqlDbType.VarChar)
            param(63).Value = rblCostType.SelectedValue

            param(64) = New SqlParameter("@OD_RENT_PER_SQFT_CARPET", SqlDbType.VarChar)
            param(64).Value = IIf(txtRentPerSqftCarpet.Text = "", DBNull.Value, txtRentPerSqftCarpet.Text)

            param(65) = New SqlParameter("@OD_RENT_PER_SQFT_BUA", SqlDbType.VarChar)
            param(65).Value = IIf(txtRentPerSqftBUA.Text = "", DBNull.Value, txtRentPerSqftBUA.Text)

            param(66) = New SqlParameter("@OD_SEAT_COST", SqlDbType.VarChar)
            param(66).Value = IIf(txtSeatCost.Text = "", DBNull.Value, txtSeatCost.Text)

            If fuBoardApprlDoc.PostedFiles IsNot Nothing Then
                For Each File In fuBoardApprlDoc.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        BoardApprovalDoc = Upload_Time & "_" & File.FileName

                    End If
                Next
            End If

            param(67) = New SqlParameter("@OD_BOARD_APPR_DOC", SqlDbType.VarChar)
            param(67).Value = BoardApprovalDoc


            If irdaAppr.PostedFiles IsNot Nothing Then
                For Each File In irdaAppr.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IRDAApprovalDoc = Upload_Time & "_" & File.FileName

                    End If
                Next
            End If

            param(68) = New SqlParameter("@OD_IRDA_APPR_DOC", SqlDbType.VarChar)
            param(68).Value = IRDAApprovalDoc

            param(69) = New SqlParameter("@PM_VICINITY", SqlDbType.VarChar)
            param(69).Value = txtCompetitorsVicinity.Text

            param(70) = New SqlParameter("@PM_TWO_PARK", SqlDbType.VarChar)
            param(70).Value = txtNoOfTwoWheelerParking.Text

            param(71) = New SqlParameter("@PM_FOUR_PARK", SqlDbType.VarChar)
            param(71).Value = txtNoOfCarsParking.Text

            param(72) = New SqlParameter("@PM_RECUR_COST", SqlDbType.VarChar)
            param(72).Value = txtRecurringCost.Text

            param(73) = New SqlParameter("@PM_KWA", SqlDbType.VarChar)
            param(73).Value = txtAvailablePower.Text

            param(74) = New SqlParameter("@PM_BASIC_RENT", SqlDbType.VarChar)
            param(74).Value = txtInvestedArea.Text

            param(75) = New SqlParameter("@PM_MAINT_CHRG", SqlDbType.VarChar)
            param(75).Value = txtmain1.Text

            param(76) = New SqlParameter("@TOT_FLOORS", SqlDbType.VarChar)
            param(76).Value = txtFloor.Text

            param(77) = New SqlParameter("@OFFICE_TYPE", SqlDbType.VarChar)
            param(77).Value = ddlOffice.SelectedItem.Value

            param(78) = New SqlParameter("@PPTY_ID", SqlDbType.Int)
            param(78).Value = Request.QueryString("id")

            param(79) = New SqlParameter("@STA_ID", SqlDbType.Int)
            param(79).Value = 4001

            param(80) = New SqlParameter("@APP_REJ", SqlDbType.VarChar)
            param(80).Value = ""

            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_UPDATE_PROPERTY_DETAILS", param)
            If res = "SUCCESS" Then
                lblmsg.Text = "ZFM Property- Options updated Successfully"
            Else
                lblmsg.Text = "Something went wrong. Please try again later."
            End If
            Cleardata()
        Catch ex As Exception

        End Try
    End Sub

    Public Class ImageClas
        Private _fn As String
        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String
        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class
    Public Sub EnableDisableSelections()
        ddlReqType.Enabled = False
        ddlPprtNature.Enabled = False
        ddlAcqThr.Enabled = False
        ddlCity.Enabled = False
        ddlLocation.Enabled = False
        ddlTower.Enabled = False
        ddlFloor.Enabled = False
        ddlPropertyType.Enabled = False
        txtExistMonthRent.Enabled = False
        txtProposedAreaForAgency.Enabled = False
        txtPropAreaOtherChannels.Enabled = False
        txtPropMonthlyRental.Enabled = False
        txtPropsedPerSqftrental.Enabled = False
        ddlBoardAppr.Enabled = False
        'rblCostType.Enabled = False
        'txtSeatCost.Enabled = False
        'txtRentPerSqftCarpet.Enabled = False
        'txtRentPerSqftBUA.Enabled = False

    End Sub
    Private Sub BindDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PRPTY_DETAILS")
        sp.Command.AddParameter("@PROP_ID", Request.QueryString("id"), DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'General Details

            txtReqID.Text = ds.Tables(0).Rows(0).Item("PM_PPT_PM_REQ_ID")
            ddlReqType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE")).Selected = True
            'ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE") = ddlReqType.SelectedValue
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_NATURE")).Selected = True
            ddlAcqThr.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_ACQ_THR")).Selected = True

            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE")).Selected = True
            GetLocationsbyCity(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
            GetTowerbyLoc(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE"))
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")).Selected = True

            GetFloorsbyTwr(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE"))
            ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_FLR_CODE")).Selected = True
            txtToilet.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_TOI")  'IIf(txtToilet.Text = "", 0, txtToilet.Text)
            ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_PPT_TYPE")
            txtPropIDName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NAME")

            txtESTD.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ESTD")  'IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            txtAge.Text = ds.Tables(0).Rows(0).Item("PM_PPT_AGE")   'IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            txtPropDesc.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
            txtSignageLocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SIGN_LOC")

            txtSocityName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SOC_NAME")
            txtlat.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LAT")
            txtlong.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LONG")
            txtOwnScopeWork.Text = ds.Tables(0).Rows(0).Item("PM_PPT_OWN_SCOPE")
            ddlRecommended.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED") = True, 1, 0)
            'ddlRecommended.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED")).Selected = True
            If ddlRecommended.SelectedValue = 1 Then
                txtRecmRemarks.Visible = True
                txtRecmRemarks.Text = ds.Tables(0).Rows(0).Item("PM_PPT_RECO_REM")
            Else
                divRecommanded.Visible = False
            End If

            'General Other Details
            txtExistMonthRent.Text = ds.Tables(0).Rows(0).Item("OD_EXISTING_MONTH_RENT")
            txtProposedAreaForAgency.Text = ds.Tables(0).Rows(0).Item("OD_PROPOSED_AREA")
            txtPropAreaOtherChannels.Text = ds.Tables(0).Rows(0).Item("OD_PROPOSED_AREA_OTHER_CHANELS")
            txtPropMonthlyRental.Text = ds.Tables(0).Rows(0).Item("OD_PROP_MONTHLY_RENT")
            txtPropsedPerSqftrental.Text = ds.Tables(0).Rows(0).Item("OD_PROP_SQFT_RENT")
            ddlBoardAppr.Items.FindByValue(ds.Tables(0).Rows(0).Item("OD_BOARD_APPR")).Selected = True
            BindOfficeType()
            ddlOffice.Items.FindByValue(ds.Tables(0).Rows(0).Item("OFFICE")).Selected = True
            If (ds.Tables(0).Rows(0).Item("OD_BOARD_APPR") = "Yes") Then
                divBoardApprDocument.Visible = True
            End If
            Dim CostTypeOn As String
            If Not ds.Tables(0).Rows(0).Item("OD_COST_TYPE") = "" Then
                CostTypeOn = ds.Tables(0).Rows(0).Item("OD_COST_TYPE")
                rblCostType.Items.FindByValue(CostTypeOn).Selected = True
                If (CostTypeOn = "Seat") Then
                    Costype2.Visible = True
                    txtSeatCost.Text = ds.Tables(0).Rows(0).Item("OD_SEAT_COST")
                Else
                    Costype1.Visible = True
                    txtRentPerSqftCarpet.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_CARPET")
                    txtRentPerSqftBUA.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_BUA")
                End If
            End If
            txtRentPerSqftCarpet.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_CARPET")
            txtRentPerSqftBUA.Text = ds.Tables(0).Rows(0).Item("OD_RENT_PER_SQFT_BUA")
            txtSeatCost.Text = ds.Tables(0).Rows(0).Item("OD_SEAT_COST")
            If ds.Tables(2).Rows.Count > 0 Then
                If (ds.Tables(2).Rows(0)("BRD_IMG_NAME").ToString <> "") Then
                    gvboard.DataSource = ds.Tables(2)
                    gvboard.DataBind()
                Else
                    gvboard.Visible = False
                End If
            End If

            'Owner Details
            txtownrname.Text = ds.Tables(0).Rows(0).Item("PM_OWN_NAME")
            txtphno.Text = ds.Tables(0).Rows(0).Item("PM_OWN_PH_NO")
            txtPrvOwnName.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_NAME")
            txtPrvOwnPhNo.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_PH_NO")

            txtOwnEmail.Text = ds.Tables(0).Rows(0).Item("PM_OWN_EMAIL")

            'Area Details
            txtCarpetArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA"))
            txtBuiltupArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA"))
            txtCommonArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_COM_AREA"))
            txtRentableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_RENT_AREA"))

            txtUsableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_USABEL_AREA"))
            txtSuperBulArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_SBU_AREA"))
            txtPlotArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_PLOT_AREA"))
            txtCeilingHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT"))

            txtBeamBottomHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT"))
            txtMaxCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_MAX_CAP"))
            txtOptCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_OPT_CAP"))
            txtSeatingCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_SEATING_CAP"))

            ddlFlooringType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE")).Selected = True
            ddlFSI.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_AR_FSI") = True, 1, 0)
            txtFSI.Text = ds.Tables(0).Rows(0).Item("PM_AR_FSI_RATIO")
            txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")

            'PURCHASE DETAILS
            txtPurPrice.Text = Format(ds.Tables(0).Rows(0).Item("PM_PUR_PRICE"), "0.00")
            txtPurDate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("PM_PUR_DATE"))
            txtMarketValue.Text = Format(ds.Tables(0).Rows(0).Item("PM_PUR_MARKET_VALUE"), "0.00")

            'GOVT DETAILS
            ddlIRDA.SelectedValue = ds.Tables(0).Rows(0).Item("PM_GOV_IRDA")
            txtPCcode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PC_CODE")
            txtGovtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PROP_CODE")
            txtUOM_CODE.Text = ds.Tables(0).Rows(0).Item("PM_GOV_UOM_CODE")

            'INSURANCE DETAILS
            ddlInsuranceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_INS_TYPE")).Selected = True
            txtInsuranceVendor.Text = ds.Tables(0).Rows(0).Item("PM_INS_VENDOR")
            txtInsuranceAmt.Text = Format(ds.Tables(0).Rows(0).Item("PM_INS_AMOUNT"), "0.00")
            txtInsurancePolNum.Text = ds.Tables(0).Rows(0).Item("PM_INS_PNO")

            txtInsuranceStartdate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("PM_INS_START_DT"))
            txtInsuranceEnddate.Text = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("PM_INS_END_DT"))

            If ds.Tables(1).Rows.Count > 0 Then
                gvPropdocs.DataSource = ds.Tables(1)
                gvPropdocs.DataBind()
            End If

            txtCompetitorsVicinity.Text = ds.Tables(0).Rows(0).Item("PM_VICINITY")
            txtNoOfTwoWheelerParking.Text = ds.Tables(0).Rows(0).Item("PM_TWO_PARK")
            txtNoOfCarsParking.Text = ds.Tables(0).Rows(0).Item("PM_FOUR_PARK")
            txtRecurringCost.Text = ds.Tables(0).Rows(0).Item("PM_RECUR_COST")
            txtAvailablePower.Text = ds.Tables(0).Rows(0).Item("PM_KWA")
            txtInvestedArea.Text = ds.Tables(0).Rows(0).Item("PM_BASIC_RENT")
            txtmain1.Text = ds.Tables(0).Rows(0).Item("PM_MAINT_CHRG")
            EnableDisableSelections()
        End If
    End Sub

    Protected Sub gvPropdocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPropdocs.RowCommand
        Try
            If e.CommandName = "Download" Then
                Response.ContentType = ContentType
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            Else
                'DELETE
                Dim gvr As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                'gvPropdocs.DeleteRow(gvr.RowIndex)
                gvPropdocs.Rows(gvr.RowIndex).Visible = False
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_DELETE_DOCS")
                sp.Command.AddParameter("@DOC_ID", e.CommandArgument, DbType.String)
                sp.Command.AddParameter("@TYPE", "PROPERTY", DbType.String)
                sp.ExecuteScalar()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvPropdocs_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvPropdocs.RowDeleting
      
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/PropertyManagement/Views/frmEditPropertyDetails.aspx?back=" + txtReqID.Text)
    End Sub

    Public Shared Function GetCurrentFinancialYear() As String
        Dim CurrentYear As Integer = DateTime.Today.Year
        Dim PreviousYear As Integer = DateTime.Today.Year - 1
        Dim NextYear As Integer = DateTime.Today.Year + 1
        Dim PreYear As String = PreviousYear.ToString()
        Dim NexYear As String = NextYear.ToString()
        Dim CurYear As String = CurrentYear.ToString()
        Dim FinYear As String = "FY"
        If DateTime.Today.Month > 3 Then
            FinYear = Convert.ToString((FinYear & CurYear) + "-") & NexYear
        Else
            FinYear = Convert.ToString((FinYear & PreYear) + "-") & CurYear
        End If
        Return FinYear.Trim()
    End Function

    Private Sub GetZoneAndStateByCityCode()
        Dim Zone, StateCode, CityCode As String
        Dim ds As DataSet
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ZONE_STATE_BY_CITY_CODE")
        sp3.Command.Parameters.Add("@CITY", ddlCity.SelectedValue, DbType.String)
        sp3.Command.AddParameter("@USR_ID", Session("uid"))
        ds = sp3.GetDataSet()

        Zone = ds.Tables(0).Rows(0).Item("CTY_ZN_ID").ToString()
        StateCode = ds.Tables(0).Rows(0).Item("CTY_STATE_ID").ToString()
        CityCode = ds.Tables(0).Rows(0).Item("CTY_CODE").ToString()

        txtReqID.Text = Zone + "/" + StateCode + "/" + CityCode + "/" + GetCurrentFinancialYear() + "/" + Request.QueryString("id")
    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub
    Protected Sub ddlBoardAppr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBoardAppr.SelectedIndexChanged
        If ddlBoardAppr.SelectedValue = "Yes" Then
            divBoardApprDocument.Visible = True
        Else
            divBoardApprDocument.Visible = False
        End If
    End Sub

    Protected Sub ddlIRDA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIRDA.SelectedIndexChanged
        If ddlIRDA.SelectedValue = "Yes" Then
            divIRDADoc.Visible = True
        Else
            divIRDADoc.Visible = False
        End If
    End Sub

    Protected Sub gvboard_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvboard.RowCommand
        Try
            If e.CommandName = "Download" Then
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.ContentType = ContentType
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            Else
                Dim gvbrd As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                gvboard.Rows(gvbrd.RowIndex).Visible = False
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_DELETE_DOCS")
                sp.Command.AddParameter("@DOC_ID", e.CommandArgument, DbType.String)
                sp.Command.AddParameter("@TYPE", "BOARDDOC", DbType.String)
                sp.ExecuteScalar()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvboard_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvboard.RowDeleting

    End Sub
End Class
