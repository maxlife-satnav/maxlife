Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_EditPropertyDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            fillReqs()
            If Request.QueryString("back") <> Nothing Then
                panel1.Visible = True
                btnSubmit.Text = "Add more properties to " + Request.QueryString("back")
                GetProperties(Request.QueryString("back"))
            Else
                panel1.Visible = False
            End If

        End If
    End Sub

    Private Sub fillReqs()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTY_REQS")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)

        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvReqs.DataSource = ds
        gvReqs.DataBind()
        Session("Reqs") = ds
    End Sub

    Protected Sub gvReqs_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvReqs.PageIndexChanging
        gvReqs.PageIndex = e.NewPageIndex()
        gvReqs.DataSource = Session("Reqs")
        gvReqs.DataBind()
    End Sub

    'Protected Sub gvrReqProperties_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvrReqProperties.PageIndexChanging
    '    gvrReqProperties.PageIndex = e.NewPageIndex()
    '    gvrReqProperties.DataSource = Session("reqDetails")
    '    gvrReqProperties.DataBind()
    'End Sub

    Private Sub GetProperties(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES_BY_REQID_01")
        sp.Command.AddParameter("@REQ_ID", Reqid, DbType.String)

        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub

    Protected Sub gvReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqs.RowCommand
        Try

            If e.CommandName = "GetProperties" Then
                panel1.Visible = True
                hdnReqid.Value = e.CommandArgument
                btnSubmit.Text = "Add more properties to " + hdnReqid.Value
                GetProperties(hdnReqid.Value)
                Session("req") = hdnReqid.Value
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        gvrReqProperties.Focus()

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        'Response.Redirect("~/PropertyManagement/Views/frmAddPropertyDetails.aspx?id=" + Session("req"))
        Response.Redirect("~/PropertyManagement/Views/frmAddPropertyDetails.aspx?id=" + Session("req"))
        'Response.Redirect("page2.aspx?myname=" + Server.UrlEncode(txtName.Text));
        'Response.Redirect("page2.aspx?myname="+Server.UrlEncode(txtName.Text)+"&myage="+txtAge.Text);


    End Sub
    Protected Sub gvReqs_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvReqs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbl As Label = e.Row.FindControl("lblStatusId")
            Dim sts As String = DirectCast(e.Row.DataItem, System.Data.DataRowView).Row.ItemArray(8)
            'e.Row.Cells(9).Enabled = IIf(sts <> "1", True, False)

            If sts = "4001" Then
                btnSubmit.Enabled = True
                ' e.Row.Cells(11).Text = "Edit"

            Else

                btnSubmit.Enabled = False
                ' e.Row.Cells(11).Text = "View"
            End If
        End If
        gvrReqProperties.Focus()
    End Sub

    'Protected Sub gvrReqProperties_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvrReqProperties.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim lbl As Label = e.Row.FindControl("lblStsValue")
    '        Dim sts As String = DirectCast(e.Row.DataItem, System.Data.DataRowView).Row.ItemArray(13)
    '        End If
    'End Sub

    'Protected Sub gvrReqProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvrReqProperties.RowCommand
    '    Try
    '        If e.CommandName = "VIEW" Then
    '            Dim lnkView As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '            Dim dealId As String = lnkView.CommandArgument
    '            Dim sts = gvrReqProperties.Rows(0).Cells(10).Text
    '        Else

    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Protected Sub btnSearchRequest_Click(sender As Object, e As EventArgs) Handles btnSearchRequest.Click
        If txtFilterText.Text = "" Then
            'lblmsg.Visible = True
            'lblmsg.Text = "Please enter Request Id/Location/Tower/Floor to search"
            fillReqs()
        Else
            lblmsg.Visible = False
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTY_REQS_SEARCH")
            sp.Command.AddParameter("@PROP_NAME", txtFilterText.Text, DbType.String)

            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvReqs.DataSource = ds
            gvReqs.DataBind()

        End If
        panel1.Visible = False

    End Sub

End Class
