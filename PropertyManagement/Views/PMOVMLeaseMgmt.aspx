﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PMOVMLeaseMgmt.aspx.vb" Inherits="PropertyManagement_Views_PMOVMLeaseMgmt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='http://localhost:49466/fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>View & Modify - PMO
                            </legend>
                        </fieldset>
                        <form id="form1" class="well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />
                            <div class="clearfix">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-6 control-label">Search by Request Id/Location/Tower/Floor<span style="color: red;">*</span></label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtFilterText" runat="Server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-12">
                                        <asp:Button ID="btnSearchRequest" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" />
                                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" OnClick="btnback_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" PageSize="10" EmptyDataText="PMO Details Not Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                    Style="font-size: 12px;" OnPageIndexChanging="gvItems_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Request Id" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("REQ_ID") %>' CommandName="PMO"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Id">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnksurrender" runat="server" Text='<%#Eval("REQ_ID")%>' CommandArgument='<%#Eval("PM_PPT_SNO")%>'
                                                    CommandName="PMO"></asp:LinkButton>
                                                 <asp:Label ID="lblPropSno" runat="server" Visible="false" Text='<%#Eval("PM_PPT_SNO")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("REQ_TYPE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CITY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LOCATION")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tower">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("TOWER")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Floor">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("FLOOR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Proposed Monthly Rental">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMonthly" runat="server" CssClass="clsLabel" Text='<%#Eval("MNTLY_RENT","{0:c}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqStatus" runat="server" CssClass="clsLabel" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Updated By">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUpdatedBy" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Updated Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUpdatedDate" runat="server" CssClass="clsLabel" Text='<%#Eval("UPT_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>

                            <div id="pnl" runat="server" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="clearfix">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Request ID </label>
                                                <asp:Label ID="txtReqID" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="clearfix" id="divtxtSno" runat="server">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Request Sno </label>
                                                <asp:Label ID="txtSno" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Request Type<span style="color: red;">*</span></label>

                                                <asp:RequiredFieldValidator ID="rfvddlReqType" runat="server" ControlToValidate="ddlReqType"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Request Type"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" OnSelectedIndexChanged="ddlReqType_SelectedIndexChanged" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Property Nature<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlPprtNature" runat="server" ControlToValidate="ddlPprtNature"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlPprtNature" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="1">Lease</asp:ListItem>
                                                    <asp:ListItem Value="2">Own</asp:ListItem>
                                                    <%-- <asp:ListItem Value="3">Farm House</asp:ListItem>
                                                                <asp:ListItem Value="3">Software Park</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12" hidden>
                                            <div class="form-group">
                                                <label>Acquisition Through<span style="color: red;">*</span></label>
                                                <asp:DropDownList ID="ddlAcqThr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                    <asp:ListItem Value="1">Purchase</asp:ListItem>
                                                    <%--<asp:ListItem Value="2">Testametary Succession</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>City<span style="color: red;">*</span></label>

                                                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" Enabled="false"></asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Location<span style="color: red;">*</span></label>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                    data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Tower<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlTower" runat="server" ControlToValidate="ddlTower"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Tower" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlTower" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                    data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Floor<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlFloor" runat="server" ControlToValidate="ddlFloor"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Floor" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Type Of Office<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rvpropertyType" runat="server" ControlToValidate="ddlPropertyType"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Type Of Office"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <div>
                                                    <asp:DropDownList ID="ddlPropertyType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="MonRent" runat="server" class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Existing monthly rental</label>
                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtExistMonthRent" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">


                                        <div id="PropArea" runat="server" class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Proposed Area (for agency)</label>
                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtProposedAreaForAgency" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                        <div id="PropAreaOtr" runat="server" class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Proposed Area (Other channels , if any)</label>


                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtPropAreaOtherChannels" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Proposed Monthly rental  </label>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPropMonthlyRental" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="Address" runat="server" class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Property Address<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtPropDesc"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Property Address"></asp:RequiredFieldValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Address with maximum 500 characters')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPropDesc" runat="server" CssClass="form-control" Height="30%"
                                                            Rows="4" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Proposed per sq.ft rental</label>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtPropsedPerSqftrental"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Purchase Price" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPropsedPerSqftrental" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Is Board approval in place<span style="color: red;">*</span> </label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPprtNature"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Select Board Approval"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlBoardAppr" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                    <asp:ListItem Value="No">No</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div id="IRDA" runat="server" class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>IRDA for opening new office?<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlIRDA"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Select Board IRDA"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlIRDA" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true" data-live-search="true">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                    <asp:ListItem Value="No">No</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div id="CostType" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Cost Type On<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                <asp:RadioButtonList ID="rblCostType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                    <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                    <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">

                                        <div id="Costype1" runat="server" visible="false">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Rent Per Sq.ft (On Carpet)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRentPerSqftCarpet" Display="None" ValidationGroup="Val1"
                                                        ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Rent Per Sq.ft (On BUA)<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRentPerSqftBUA" Display="None" ValidationGroup="Val1"
                                                        ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Costype2" runat="server" visible="false">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Seat Cost<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                        ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Office Code</label>
                                                <div onmouseover="Tip('Enter Office Code')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtOffice" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12" id="divBoardApprDocument" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label for="txtcode">Upload Board Approval Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Display="None" ControlToValidate="fuBoardApprlDoc"
                                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                        ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg|.doc|.docx|.pdf|.xls|.xlsx)$">
                                                    </asp:RegularExpressionValidator>

                                                    <div class="btn btn-primary btn-mm">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="fuBoardApprlDoc" runat="Server" Width="90%" AllowMultiple="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" id="divIRDADoc" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="clearfix">
                                                    <label for="txtcode">Upload IRDA Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" Display="None" ControlToValidate="irdaAppr"
                                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                        ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg|.doc|.docx|.pdf|.xls|.xlsx)$">
                                                    </asp:RegularExpressionValidator>

                                                    <div class="btn btn-primary btn-mm">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="irdaAppr" runat="Server" Width="90%" AllowMultiple="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:GridView ID="gvboard" runat="server" EmptyDataText="No Documents Found."
                                            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                            AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Board Approval Document">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDownload1" runat="server" Text="Click To Download Board Document" CommandArgument='<%#Eval("BOARD_DOC")%>'
                                                            CommandName="BoardDoc"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="IRDA Document">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkDownload2" runat="server" Text="Click To Download IRDA Document" CommandArgument='<%#Eval("IRDA")%>'
                                                            CommandName="IRDADoc"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                            <PagerStyle CssClass="pagination-ys" />
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        </asp:GridView>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-12 text-right" style="padding-top: 5px">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Update" CausesValidation="true" ValidationGroup="Val1" />
                                            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" OnClick="btnback_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
