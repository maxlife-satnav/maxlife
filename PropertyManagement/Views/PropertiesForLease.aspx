﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PropertiesForLease.aspx.vb" Inherits="PropertyManagement_Views_PropertiesForLease" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='http://localhost:51165/fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Property Term Sheet
                            </legend>
                        </fieldset>
                        <form id="form1" runat="server" class="well">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <div class="row">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Val2" runat="server" />

                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse">Upload Lease Details</a>
                                </h4>
                            </div>
                            <div class="panel-body color">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:HyperLink ID="hyp" runat="server" Text=" Click here to view the template" NavigateUrl="~/Masters/Mas_Webfiles/LeaseTemplate.xlsx"></asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" ControlToValidate="fpBrowseDoc"
                                            ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                                            ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> </asp:RegularExpressionValidator>
                                        <div class="col-md-6 text-left">
                                            <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                        </div>
                                        <div class="col-md-1 text-left">
                                            <asp:Button ID="Btnupload" runat="server" CssClass="btn btn-primary custom-button-color " Text="Upload"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-11" style="overflow: auto;">
                                    <asp:GridView ID="GridView1" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="row">
                                <div class="col-md-11" style="overflow: auto;">
                                    <asp:GridView ID="GridView2" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <br />
                            <br />





                            <div class="clearfix">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-6 control-label">Search by Property Name/Location/Recommended/Owner Name<span style="color: red;">*</span></label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtFilterText" runat="Server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-12">
                                        <asp:Button ID="btnSearchRequest" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" />
                                    </div>
                                </div>
                            </div>



                            <div class="clearfix">
                                <asp:GridView ID="gvReqs" runat="server" EmptyDataText="No Records Found." AllowPaging="True" PageSize="50" AutoGenerateColumns="false"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Request Id">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnksurrender" runat="server" Text='<%#Eval("PM_REQ_ID")%>' CommandArgument='<%#Eval("PM_PPT_SNO")%>'
                                                    CommandName="GetProperties"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requested Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedDt" runat="server" Text='<%#Eval("PM_CREATED_DT")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPropCode" runat="server" Text='<%#Eval("STA_TITLE")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPropType" runat="server" Text='<%#Eval("PN_PROPERTYTYPE")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPropName" runat="Server" Text='<%#Eval("PM_PPT_NAME")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="Server" Text='<%#Eval("CTY_NAME")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoc" runat="Server" Text='<%#Eval("LCM_NAME")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                            <br />
                            <br />
                            <asp:HiddenField ID="hdnReqid" runat="server" />

                            <div id="panel1" runat="Server" visible="false">


                                <div class="clearfix">
                                    <asp:GridView ID="gvrReqProperties" runat="server" EmptyDataText="No Property Details Found."
                                        AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Request Id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblID" runat="server" Text='<%#Eval("PM_PPT_SNO")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPropName" runat="Server" Text='<%#Eval("PM_PPT_NAME")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPropType" runat="server" Text='<%#Eval("PN_PROPERTYTYPE")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Plot Area">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblplotArea" runat="Server" Text='<%#Eval("PM_AR_PLOT_AREA")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Address">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPropDesc" runat="Server" Text='<%#Eval("PM_PPT_ADDRESS")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoc" runat="Server" Text='<%#Eval("LCM_NAME")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Owner Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOwnName" runat="Server" Text='<%#Eval("PM_OWN_NAME")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phone No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPhno" runat="Server" Text='<%#Eval("PM_OWN_PH_NO")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" runat="Server" Text='<%#Eval("PM_OWN_EMAIL")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Request Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReqSts" runat="Server" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Request Status" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStsValue" runat="Server" Text='<%#Eval("STA_VALUE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%--<a href='frmModifyPropertyDetails.aspx?id=<%#Eval("PM_PPT_SNO")%>'>EDIT</a>--%>
                                                    <%--<asp:LinkButton ID="linkbtn" runat="server" CommandArgument='<%#Eval("PM_PPT_SNO") %>' CommandName="VIEW" Text="View"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="linkbtn" runat="server" CommandArgument='<%#Eval("PM_PPT_SNO") %>' CommandName="VIEW" Text='<%# If(Eval("STA_VALUE").ToString() = "4001", "Edit", "View") %>'
                                                        PostBackUrl='<%# String.Format("frmModifyPropertyDetails.aspx?id={0}&staid={1}", Eval("PM_PPT_SNO").ToString(), Eval("STA_VALUE").ToString())%>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle />
                                        <PagerStyle CssClass="pagination-ys" />
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    </asp:GridView>
                                </div>

                            </div>

                        </form>
                        <%--</div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>







