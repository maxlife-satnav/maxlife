﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TenantExtension.aspx.cs" Inherits="PropertyManagement_Views_TenantExtension" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
  
</head>
<script type="text/javascript">
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Tenant Extension
                            </legend>
                        </fieldset>

                        <form id="form1" class="well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />

                            <div class="clearfix">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="form-group col-sm-12 col-xs-6">
                                    <label>Search by Property Name/Location/TenantId/TenantCode</label>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search"></asp:TextBox>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" OnClick="btnSearch_Click" />
                                </div>
                            </div>

                            <div class="clearfix">

                                <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" EmptyDataText="No Current Tenant Details Found."
                                    CssClass="table table-condensed table-bordered table-hover table-striped" OnPageIndexChanging="gvLDetails_Lease_PageIndexChanging"
                                    OnRowCommand="gvLDetails_Lease_RowCommand" DataKeyNames="PROPERTY_NAME, PM_TEN_TO_DT">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Property Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpropTenSNO" runat="server" Text='<%#Eval("PROPERTY_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpname" runat="server" Text='<%#Eval("PROPERTY_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant">
                                            <ItemTemplate>
                                                <asp:Label ID="lblten" runat="server" Text='<%#Eval("TENANT_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Created By">
                                            <ItemTemplate>
                                                <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="PAY TERMS" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="LblPAYTName" runat="server" Text='<%#Eval("PM_PT_NAME")%>'></asp:Label>
                                                <asp:Label ID="lblPAYid" runat="server" Text='<%#Eval("PM_TD_PAY_TERMS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="LblTenRent" runat="server" Text='<%#Eval("PM_TD_RENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkExtn" runat="server" Text="Extension" CommandArgument='<%#Eval("PROPERTY_CODE")%>'
                                                    CommandName="Extension"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="View Details">
                                            <ItemTemplate>
                                                <a href="#" onclick="showPopWin('<%# Eval("SNO") %>')">
                                                    <asp:Label ID="lbltencode" runat="server" Text="View Details"></asp:Label>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>

                            </div>
                            <br />
                            <br />
                            <asp:HiddenField ID="hdnPropCode" runat="server" />
                            <asp:HiddenField ID="hdnpayId" runat="server" />
                            <div id="panel1" runat="server" visible="false">
                                <h4>Tenant Agreement Extension Details     </h4>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Property Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rftPN" runat="server" ControlToValidate="txtPname"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Property Name"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtPname" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Payment Terms<span style="color: red;">*</span></label>
                                            <%--<asp:RequiredFieldValidator ID="rfvPt" runat="server" ControlToValidate="txtPterms"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Pa"></asp:RequiredFieldValidator>--%>
                                            <asp:TextBox ID="txtPterms" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">No of Payment Terms <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvPaycnt" runat="server" ControlToValidate="txtpayCount"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter No of Payment Terms"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtpayCount" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtpayCount_TextChanged"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Rent Amount<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvRent" runat="server" ControlToValidate="txtRent"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Rent Amount"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtRent" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tenant From Date <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvSt" runat="server" ControlToValidate="TntFrm"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Tenant From Date"></asp:RequiredFieldValidator>
                                            <div class='input-group date' id='TFrm'>
                                                <asp:TextBox ID="TntFrm" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                <%--  <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('TFrm')"></span>
                                            </span>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tenant To Date <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txttoDt"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Tenant To Date"></asp:RequiredFieldValidator>
                                            <div class='input-group date' id='Tto'>
                                                <asp:TextBox ID="txttoDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('Tto')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Security Deposit <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                Display="None" ErrorMessage="Please Enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                Display="None" ErrorMessage="Please Enter Valid Security Deposit in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                            <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvtxtRecmRemarks" runat="server" ControlToValidate="txtRemarks"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Extension Remarks"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Remarks with maximum 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%"
                                                    Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="clearfix">
                                    <div class="col-md-12 col-sm-6 col-xs-12 text-right" style="padding-top: 5px">
                                        <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" OnClick="btnSubmit_Click" />
                                    </div>
                                </div>
                            </div>
                            <br />

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Tenant Renewal Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="450px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "TenantRenew.aspx?Renewal=" + id + "&ExType=1");
            $("#myModal").modal().fadeIn();
        }

        // No of Payments
        jQuery('#txtpayCount').keyup(function () {
            this.value = this.value.replace(/[^1-9]/g, '');
        });

    </script>
</body>
</html>
