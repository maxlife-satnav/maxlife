﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;
public partial class PropertyManagement_Views_PreValidation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uid"].ToString() == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    private void BindGrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA_ON_L4_APPROVAL");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA_SEARCH_BY");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();
    }

    protected void txtreset_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtReqId.Text = "";
    }
    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindGrid();
        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataBind();
    }
}