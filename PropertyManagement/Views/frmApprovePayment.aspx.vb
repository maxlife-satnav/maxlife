Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmApprovePayment
    Inherits System.Web.UI.Page
    'Public Sub fillgrid()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETWDETAIL")
    '    sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
    '    gvwrequest.DataSource = sp.GetDataSet()
    '    gvwrequest.DataBind()
    '    For i As Integer = 0 To gvwrequest.Rows.Count - 1
    '        Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblstatus"), Label)
    '        If lblstatus.Text = "0" Then
    '            lblstatus.Text = "Pending"
    '        ElseIf lblstatus.Text = "1" Then
    '            lblstatus.Text = "InProgress"
    '        Else
    '            lblstatus.Text = "2"
    '            lblstatus.Text = "Completed"
    '        End If

    '        Dim lblworktype As Label = CType(gvwrequest.Rows(i).FindControl("lblworktype"), Label)
    '        If lblworktype.Text = "0" Then
    '            lblworktype.Text = "Rejected"
    '        Else
    '            lblworktype.Text = "Approved"
    '        End If
    '    Next
    '    btnApprove.Enabled = True
    '    btnReject.Enabled = True
    'End Sub
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        txtStartDate.Text = ""
        txtExpiryDate.Text = ""
        txtpamount.Text = ""
        txtoamount.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtRemarks.Text = ""
        ddlwstatus.SelectedIndex = -1
        btnApprove.Enabled = False
        btnReject.Enabled = False
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETWORK1REQ")
        sp.Command.AddParameter("@User", Session("UID"), DbType.String)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select WorkRequest--", "0"))
    End Sub
    Public Sub GetWorkStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_WORK_STATUS")
        ddlwstatus.DataSource = sp.GetDataSet()
        ddlwstatus.DataTextField = "STA_NAME"
        ddlwstatus.DataValueField = "STA_CODE"
        ddlwstatus.DataBind()
        ddlwstatus.Items.Insert(0, New ListItem("--Select Work Status--", "0"))
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            getrequest()
            GetWorkStatus()
        End If
        txtStartDate.Attributes.Add("readonly", "readonly")
        txtExpiryDate.Attributes.Add("readonly", "readonly")
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_ADDWR1")
        sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp1.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp1.ExecuteScalar()
        lblmsg.Text = "Work Request Payment Rejected Successfully"
        Cleardata()

    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_ADDWR")
        sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp1.Command.AddParameter("@Remarks", Remarks.Text, DbType.String)
        sp1.ExecuteScalar()
        lblmsg.Text = "Work Request Payment Approved Succesfully"
        Cleardata()

    End Sub

    Public Sub ValidatePaymentDetails()
        Dim ds As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VALIDATE_PAYMENT_DETAILS")
        sp.Command.AddParameter("@VALIDATE_WR", ddlWorkRequest.SelectedValue, DbType.String)
        ds = sp.GetDataSet
        If ds.Tables(0).Rows(0).Item("VALIDATE_COUNT") = 1 Then
            lblmsg.Text = "Work Request Payment Has Already Been Approved/ Rejected"
            btnApprove.Enabled = False
            btnReject.Enabled = False
        Else
            WorkRequestPayment_Change()
        End If
    End Sub
    Public Sub WorkRequestPayment_Change()
        If ddlWorkRequest.SelectedIndex > 0 Then

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETWORKDETAILS")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT")
                'Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                ddlwstatus.SelectedValue = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETWORKREDETAILS")
                sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
                Dim ds1 As New DataSet()
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    txtStartDate.Text = ds1.Tables(0).Rows(0).Item("START_DATE")
                    txtExpiryDate.Text = ds1.Tables(0).Rows(0).Item("END_DATE")
                    txtpamount.Text = ds1.Tables(0).Rows(0).Item("PAID_AMOUNT")
                    txtoamount.Text = ds1.Tables(0).Rows(0).Item("OUTSTANDING_AMOUNT")
                    txtRemarks.Text = ds1.Tables(0).Rows(0).Item("Remarks")
                End If
            End If
            btnApprove.Visible = True
            btnReject.Visible = True
        Else
            txtStartDate.Text = ""
            txtExpiryDate.Text = ""
            txtpamount.Text = ""
            txtoamount.Text = ""
            txtWorkTitle.Text = ""
            txtWorkSpec.Text = ""
            txtamount.Text = ""
            txtRemarks.Text = ""
            ddlwstatus.SelectedIndex = -1
        End If
    End Sub
    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        ValidatePaymentDetails()
    End Sub
End Class