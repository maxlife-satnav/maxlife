﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PMO.aspx.cs" Inherits="PropertyManagement_Views_PMO" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
    <style>
        .btn {
            border-radius: 4px;
            background-color: #3A618F;
        }
         hr
        {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }
    </style>
</head>
<body>
    <script src="../../WorkSpace/Scripts/wz_tooltip.js"></script>
    <div id="page-wrapper" class="row">
        <div class="row ng-scope">
            <div class="box box-primary">
                <div class="col-md-12">
                    <fieldset>
                        <legend>PMO</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="LeaseMgmtPMO" runat="server">
                                    <asp:HyperLink ID="PMO" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/PMOLeaseMgmt.aspx">Lease Management - PMO</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="ModifyLease" runat="server">
                                    <asp:HyperLink ID="MLease" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/PMOVMLeaseMgmt.aspx">View & Modify - PMO</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/bootstrap.min.js"></script>
</body>
</html>
