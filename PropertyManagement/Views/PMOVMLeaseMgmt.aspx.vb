﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class PropertyManagement_Views_PMOVMLeaseMgmt
    Inherits System.Web.UI.Page
    Dim BoardApprovalDoc As String
    Dim IRDAApprovalDoc As String

    Public Sub BindPMOGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VM_PMO")
        sp.Command.AddParameter("@FLAG", 1, DbType.Int32)
        sp.Command.AddParameter("@PMOREQ", "", DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindPMOGrid()
            BindRequestTypes()
            BindUserCities()
            BindPropType()
            pnl.Visible = False
            divtxtSno.visible = False
        End If
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ' ddlCity.Items.Insert(0, New ListItem("--Select City--", 0))
    End Sub

    Private Sub GetLocationsbyCity(ByVal city As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", city)
        sp2.Command.AddParameter("@USR_ID", Session("uid"))
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Private Sub GetTowerbyLoc(ByVal loc As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
        sp2.Command.AddParameter("@LCMID", loc, DbType.String)
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlTower.DataSource = sp2.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select Tower--", "0"))
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select Request Type--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", 0))
    End Sub

    Private Sub GetFloorsbyTwr(ByVal tower As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
        sp2.Command.AddParameter("@TWR_ID", tower, DbType.String)
        sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@FLR_CITY_CODE", ddlCity.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlFloor.DataSource = sp2.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select Floor--", "0"))
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        BindPMOGrid()
        gvItems.PageIndex = e.NewPageIndex
        gvItems.DataBind()
    End Sub

    Public Sub PMODetails(Sno As String)
        lblmsg.Text = ""
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VM_PMO")
        sp1.Command.AddParameter("@FLAG", 2, DbType.Int32)
        sp1.Command.AddParameter("@PMOREQ", Sno, DbType.String)
        Dim ds As New DataSet
        ds = sp1.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            If (ds.Tables(0).Rows(0).Item("REQ_TYPE") = 1) Then
                IRDA.Visible = False
                MonRent.Visible = True
                PropArea.Visible = True
                PropAreaOtr.Visible = True
                Address.Visible = True
                CostType.Visible = True
            Else
                MonRent.Visible = False
                PropArea.Visible = False
                PropAreaOtr.Visible = False
                Address.Visible = False
                IRDA.Visible = True
                CostType.Visible = False
            End If
            BindUserCities()
            ddlReqType.ClearSelection()
            txtReqID.Text = ds.Tables(0).Rows(0).Item("REQ_ID")
            ddlReqType.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_TYPE")).Selected = True
            ddlPprtNature.ClearSelection()
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_NATURE")).Selected = True

            ddlAcqThr.ClearSelection()
            ddlAcqThr.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_ACQ")).Selected = True
            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CITY")).Selected = True
            GetLocationsbyCity(ds.Tables(0).Rows(0).Item("CITY"))
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("LOCATION")).Selected = True
            GetTowerbyLoc(ds.Tables(0).Rows(0).Item("LOCATION"))
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("TOWER")).Selected = True
            GetFloorsbyTwr(ds.Tables(0).Rows(0).Item("TOWER"))
            ddlFloor.ClearSelection()
            ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("FLOOR")).Selected = True
            ddlPropertyType.ClearSelection()
            ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_REQ_TYPE")
            txtExistMonthRent.Text = ds.Tables(0).Rows(0).Item("RENT")
            txtProposedAreaForAgency.Text = ds.Tables(0).Rows(0).Item("AREA")
            txtPropAreaOtherChannels.Text = ds.Tables(0).Rows(0).Item("OTR_CHNL")
            txtPropMonthlyRental.Text = ds.Tables(0).Rows(0).Item("MNTLY_RENT")
            txtPropsedPerSqftrental.Text = ds.Tables(0).Rows(0).Item("SQFT_RENT")
            ddlBoardAppr.ClearSelection()
            ddlBoardAppr.Items.FindByValue(ds.Tables(0).Rows(0).Item("BOARD_APPR")).Selected = True

            If (ds.Tables(0).Rows(0).Item("BOARD_APPR").ToString = "Yes") Then
                divBoardApprDocument.Visible = True
            Else
                divBoardApprDocument.Visible = False
            End If


            Dim CostTypeOn As String
            CostTypeOn = ds.Tables(0).Rows(0).Item("COST_TYP")
            If Not CostTypeOn = "" Then
                rblCostType.Items.FindByValue(CostTypeOn).Selected = True
                If (CostTypeOn = "Seat") Then
                    Costype2.Visible = True
                    txtSeatCost.Text = ds.Tables(0).Rows(0).Item("OD_SEAT_COST")
                Else
                    Costype1.Visible = True
                    txtRentPerSqftCarpet.Text = ds.Tables(0).Rows(0).Item("SQFT_CARPET")
                    txtRentPerSqftBUA.Text = ds.Tables(0).Rows(0).Item("BUA")
                End If
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0)("BOARD_DOC").ToString <> "") Then
                    ddlIRDA.SelectedValue = "Yes"
                    gvboard.DataSource = ds.Tables(0)
                    gvboard.DataBind()
                    gvboard.Columns(0).Visible = True
                Else
                    gvboard.Columns(0).Visible = False
                End If
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0)("IRDA").ToString <> "") Then
                    divIRDADoc.Visible = True
                    gvboard.DataSource = ds.Tables(0)
                    gvboard.DataBind()
                    gvboard.Columns(1).Visible = True
                    ddlIRDA.SelectedValue = "Yes"
                Else
                    divIRDADoc.Visible = False
                    gvboard.Columns(1).Visible = False
                    ddlIRDA.SelectedValue = "No"
                End If
            End If
            txtOffice.Text = ds.Tables(0).Rows(0).Item("PROP_CODE")
            txtPropDesc.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
            txtSno.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SNO")
            'txtCarpetArea.Text = ds.Tables(0).Rows(0).Item("OD_SEAT_COST")
            '   ddlIRDA.SelectedValue = ds.Tables(0).Rows(0).Item("IRDA")
        End If
    End Sub

    Public Shared Function GetCurrentFinancialYear() As String
        Dim CurrentYear As Integer = DateTime.Today.Year
        Dim PreviousYear As Integer = DateTime.Today.Year - 1
        Dim NextYear As Integer = DateTime.Today.Year + 1
        Dim PreYear As String = PreviousYear.ToString()
        Dim NexYear As String = NextYear.ToString()
        Dim CurYear As String = CurrentYear.ToString()
        Dim FinYear As String = "FY"
        If DateTime.Today.Month > 3 Then
            FinYear = Convert.ToString((FinYear & CurYear) + "-") & NexYear
        Else
            FinYear = Convert.ToString((FinYear & PreYear) + "-") & CurYear
        End If
        Return FinYear.Trim()
    End Function

    Private Sub GetZoneAndStateByCityCode()
        Dim Zone, StateCode, CityCode As String
        Dim ds As DataSet
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ZONE_STATE_BY_CITY_CODE")
        sp3.Command.Parameters.Add("@CITY", ddlCity.SelectedValue, DbType.String)
        sp3.Command.AddParameter("@USR_ID", Session("uid"))
        ds = sp3.GetDataSet()

        Zone = ds.Tables(0).Rows(0).Item("CTY_ZN_ID").ToString()
        StateCode = ds.Tables(0).Rows(0).Item("CTY_STATE_ID").ToString()
        CityCode = ds.Tables(0).Rows(0).Item("CTY_CODE").ToString()

        txtReqID.Text = Zone + "/" + StateCode + "/" + CityCode + "/" + GetCurrentFinancialYear() + "/" + Request.QueryString("id")
    End Sub

    Public Sub UpdatepptyDetails()
        Try
            Dim param As SqlParameter() = New SqlParameter(25) {}
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = txtReqID.Text
            param(1) = New SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar)
            param(1).Value = ddlReqType.SelectedValue
            param(2) = New SqlParameter("@PROP_NATURE", SqlDbType.VarChar)
            param(2).Value = ddlPprtNature.SelectedValue
            param(3) = New SqlParameter("@ACQ_TRH", SqlDbType.VarChar)
            param(3).Value = ddlAcqThr.SelectedValue

            param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
            param(4).Value = ddlCity.SelectedValue

            param(5) = New SqlParameter("@OD_EXISTING_MONTH_RENT", SqlDbType.VarChar)
            param(5).Value = IIf(txtExistMonthRent.Text = "", DBNull.Value, txtExistMonthRent.Text)

            param(6) = New SqlParameter("@OD_PROPOSED_AREA", SqlDbType.VarChar)
            param(6).Value = IIf(txtProposedAreaForAgency.Text = "", DBNull.Value, txtProposedAreaForAgency.Text)

            param(7) = New SqlParameter("@OD_PROPOSED_AREA_OTHER_CHANELS", SqlDbType.VarChar)
            param(7).Value = IIf(txtPropAreaOtherChannels.Text = "", DBNull.Value, txtPropAreaOtherChannels.Text)

            param(8) = New SqlParameter("@OD_PROP_MONTHLY_RENT", SqlDbType.VarChar)
            param(8).Value = IIf(txtPropMonthlyRental.Text = "", DBNull.Value, txtPropMonthlyRental.Text)

            param(9) = New SqlParameter("@OD_PROP_SQFT_RENT", SqlDbType.VarChar)
            param(9).Value = IIf(txtPropsedPerSqftrental.Text = "", DBNull.Value, txtPropsedPerSqftrental.Text)

            param(10) = New SqlParameter("@OD_BOARD_APPR", SqlDbType.VarChar)
            param(10).Value = ddlBoardAppr.SelectedValue

            param(11) = New SqlParameter("@IRDA", SqlDbType.VarChar)
            param(11).Value = ddlIRDA.SelectedValue

            param(12) = New SqlParameter("@OD_COST_TYPE", SqlDbType.VarChar)
            param(12).Value = rblCostType.SelectedValue

            param(13) = New SqlParameter("@OD_RENT_PER_SQFT_CARPET", SqlDbType.VarChar)
            param(13).Value = IIf(txtRentPerSqftCarpet.Text = "", DBNull.Value, txtRentPerSqftCarpet.Text)

            param(14) = New SqlParameter("@OD_RENT_PER_SQFT_BUA", SqlDbType.VarChar)
            param(14).Value = IIf(txtRentPerSqftBUA.Text = "", DBNull.Value, txtRentPerSqftBUA.Text)

            param(15) = New SqlParameter("@OD_SEAT_COST", SqlDbType.VarChar)
            param(15).Value = IIf(txtSeatCost.Text = "", DBNull.Value, txtSeatCost.Text)

            If fuBoardApprlDoc.PostedFiles IsNot Nothing Then
                For Each File In fuBoardApprlDoc.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        BoardApprovalDoc = Upload_Time & "_" & File.FileName

                    End If
                Next
            End If

            param(16) = New SqlParameter("@OD_BOARD_APPR_DOC", SqlDbType.VarChar)
            param(16).Value = BoardApprovalDoc

            If irdaAppr.PostedFiles IsNot Nothing Then
                For Each File In irdaAppr.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IRDAApprovalDoc = Upload_Time & "_" & File.FileName

                    End If
                Next
            End If

            param(17) = New SqlParameter("@OD_IRDA_APPR_DOC", SqlDbType.VarChar)
            param(17).Value = IRDAApprovalDoc

            param(18) = New SqlParameter("@PM_OFFICE", SqlDbType.VarChar)
            param(18).Value = txtOffice.Text

            param(19) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
            param(19).Value = ddlLocation.SelectedValue

            param(20) = New SqlParameter("@TOWER", SqlDbType.VarChar)
            param(20).Value = ddlTower.SelectedValue

            param(21) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
            param(21).Value = ddlFloor.SelectedValue

            param(22) = New SqlParameter("@PROP_TYPE", SqlDbType.VarChar)
            param(22).Value = ddlPropertyType.SelectedValue

            param(23) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(23).Value = HttpContext.Current.Session("UID")

            param(24) = New SqlParameter("@ADDRESS", SqlDbType.VarChar)
            param(24).Value = txtPropDesc.Text

            param(25) = New SqlParameter("@SNO", SqlDbType.Int)
            param(25).Value = txtSno.Text

            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_UPDATE_PMO_DETAILS", param)
            If res = "SUCCESS" Then
                lblmsg.Visible = True
                lblmsg.Text = "PMO Modified Successfully"
                pnl.Visible = False
            Else
                lblmsg.Visible = True
                lblmsg.Text = "Something went wrong. Please try again later."
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Cleardata()
        ddlReqType.SelectedIndex = 0
        ddlPprtNature.SelectedIndex = 0
        ddlAcqThr.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlPropertyType.SelectedIndex = 0
        txtExistMonthRent.Text = ""
        txtProposedAreaForAgency.Text = ""
        txtPropAreaOtherChannels.Text = ""
        txtPropMonthlyRental.Text = ""
        txtPropsedPerSqftrental.Text = ""
        ddlBoardAppr.SelectedValue = ""
        txtRentPerSqftCarpet.Text = ""
        txtRentPerSqftBUA.Text = ""
        txtSeatCost.Text = ""
        txtPropDesc.Text = ""
        txtOffice.Text = ""
        ddlIRDA.SelectedValue = 0

    End Sub
    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        Dim Sno As String
        Try
            If e.CommandName = "PMO" Then
                btnSubmit.Focus()
                pnl.Visible = True
                Sno = e.CommandArgument
                PMODetails(Sno)
            Else
                pnl.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        btnSubmit.Focus()
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub

    Protected Sub ddlReqType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReqType.SelectedIndexChanged
        btnSubmit.Focus()

        If ddlReqType.SelectedValue = 1 Then
            IRDA.Visible = False
            MonRent.Visible = True
            PropArea.Visible = True
            PropAreaOtr.Visible = True
            Address.Visible = True
            CostType.Visible = True
            ddlLocation.Visible = True
            RequiredFieldValidator15.Visible = True
            ddlTower.Visible = True
            rfvddlTower.Visible = True
            ddlFloor.Visible = True
            rfvddlFloor.Visible = True
            Costype1.Visible = True
        Else
            MonRent.Visible = False
            PropArea.Visible = False
            PropAreaOtr.Visible = False
            Address.Visible = False
            IRDA.Visible = True
            CostType.Visible = False
            Costype1.Visible = False

        End If
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        btnSubmit.Focus()
        If ddlCity.SelectedIndex > 0 Then
            GetLocationsbyCity(ddlCity.SelectedValue)
            GetZoneAndStateByCityCode()
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        btnSubmit.Focus()
        If ddlLocation.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
            sp2.Command.AddParameter("@LCMID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlTower.DataSource = sp2.GetDataSet()
            ddlTower.DataTextField = "TWR_NAME"
            ddlTower.DataValueField = "TWR_CODE"
            ddlTower.DataBind()
            ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        btnSubmit.Focus()
        If ddlTower.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
            sp2.Command.AddParameter("@TWR_ID", ddlTower.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_CITY_CODE", ddlCity.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlFloor.DataSource = sp2.GetDataSet()
            ddlFloor.DataTextField = "FLR_NAME"
            ddlFloor.DataValueField = "FLR_CODE"
            ddlFloor.DataBind()
            ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
            'txtFloor.Text = ddlFloor.Items.Count - 1
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub

    Protected Sub ddlBoardAppr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBoardAppr.SelectedIndexChanged
        btnSubmit.Focus()
        If ddlBoardAppr.SelectedValue = "Yes" Then
            divBoardApprDocument.Visible = True
        Else
            divBoardApprDocument.Visible = False
        End If
    End Sub

    Protected Sub ddlIRDA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIRDA.SelectedIndexChanged
        btnSubmit.Focus()
        If ddlIRDA.SelectedValue = "Yes" Then
            divIRDADoc.Visible = True
        Else
            divIRDADoc.Visible = False
        End If
    End Sub

    Protected Sub gvboard_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvboard.RowCommand
        Try
            If e.CommandName = "BoardDoc" Then
                Response.ContentType = ContentType
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            Else
                Response.ContentType = ContentType
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        UpdatepptyDetails()
        Cleardata()
    End Sub

    Protected Sub btnSearchRequest_Click(sender As Object, e As EventArgs) Handles btnSearchRequest.Click
        If txtFilterText.Text = "" Then
            'lblmsg.Visible = True
            'lblmsg.Text = "Please enter Property Name/Location/Recommended/Owner Name to search"
            BindPMOGrid()
        Else
            lblmsg.Visible = False
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ZFM_FILTER")
            sp.Command.AddParameter("@ZFM", txtFilterText.Text, DbType.String)

            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvItems.DataSource = ds
            gvItems.DataBind()

        End If
    End Sub
    Protected Sub btnback_Click(sender As Object, e As EventArgs)
        Response.Redirect("PMO.aspx")
    End Sub
End Class

