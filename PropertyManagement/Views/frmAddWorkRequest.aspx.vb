Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_frmAddWorkRequest
    Inherits System.Web.UI.Page

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlcity.DataSource = sp.GetDataSet()
        ddlcity.DataTextField = "CTY_NAME"
        ddlcity.DataValueField = "CTY_CODE"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Public Sub Cleardata()
        ddlProperty.SelectedValue = 0
        ddlVendor.SelectedValue = 0
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtPhno.Text = ""
        txtRemarks.Text = ""
        txtAddress.Text = ""
    End Sub
    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROP_tenant")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            ddlproperty.DataSource = sp.GetDataSet()
            ddlproperty.DataTextField = "PN_NAME"
            ddlproperty.DataValueField = "BDG_ID"
            ddlproperty.DataBind()
            ddlproperty.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlcity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", ""))

        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindPropertyType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Vendors()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_VENDORS")
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub VendorChangeEvent()
        Dim ds As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_VENDORS_DETAILS")
        sp.Command.AddParameter("@VENDOR", ddlVendor.SelectedValue, DbType.String)
        ds = sp.GetDataSet()
        txtPhno.Text = ds.Tables(0).Rows(0).Item("AVR_PHNO")
        txtAddress.Text = ds.Tables(0).Rows(0).Item("AVR_ADDR")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            BindPropertyType()
            BindCity()
            Vendors()
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim WorkReq As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMdd") + "/" + Session("uid")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_ADD_WORK_REQUEST")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", WorkReq, DbType.String)
        sp.Command.AddParameter("@Prop_Type", ddlproptype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PN_CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PN_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PN_PROP", ddlproperty.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@WORK_TITLE", txtWorkTitle.Text, DbType.String)
        sp.Command.AddParameter("@WORK_SPECIFICATIONS", txtWorkSpec.Text, DbType.String)
        sp.Command.AddParameter("@ESTIMATED_AMOUNT", txtamount.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_NAME", ddlVendor.SelectedValue, DbType.String)
        sp.Command.AddParameter("@VENDOR_PHONE", txtPhno.Text, DbType.String)
        sp.Command.AddParameter("@VENDOR_DETAILS", txtAddress.Text, DbType.String)
        sp.Command.AddParameter("@REQUEST_RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=10")
        lblmsg.Text = "Work Request Added Succesfully"
        Cleardata()

    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        BindCityLoc()
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlcity.SelectedIndex > 0 Then
            BindProp()
        Else
            ddlProperty.Items.Clear()
            ddlProperty.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlProperty.SelectedIndex = 0
        End If
    End Sub
    Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendor.SelectedIndexChanged
        VendorChangeEvent()
    End Sub

    Protected Sub ddlproptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproptype.SelectedIndexChanged
        ddlCity.ClearSelection()
        ddlLocation.ClearSelection()
    End Sub
End Class
