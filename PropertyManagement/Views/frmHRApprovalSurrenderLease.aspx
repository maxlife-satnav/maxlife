<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmHRApprovalSurrenderLease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSurrenderLease" Title="Approve Surrender Lease" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Termination/Surrender Lease Approve/Reject
                            </legend>
                        </fieldset>

                        <form id="form1" class="well" runat="server">
                            <div class="clearfix">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                     <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                                </div>
                            </div>
                           
                            
                            <div class="clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label"></label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="padding-top: 10px">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-10 control-label">Search By Property / Lease / Location Name<span style="color: red;">*</span></label>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="clearfix">
                                            <div class="col-md-4">
                                                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                    Display="none" ErrorMessage="Please Search By Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-5">
                                                <asp:Button ID="btnsearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                    CausesValidation="true" TabIndex="2" />
                                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="padding-top: 10px">

                                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
                                    AllowPaging="True" PageSize="10" EmptyDataText=" No Surrender Records Found" CssClass="table table-condensed table-bordered table-hover table-striped"
                                    Style="font-size: 12px;">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLeaseName" runat="server" Text='<%#Eval("LEASE_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Id">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lblLseName" runat="server" Text='<%#Eval("LEASE_ID")%>' CommandName="Surrender"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPC" runat="server" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPN" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLCM" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLsdate" runat="server" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLedate" runat="server" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>

                            </div>

                            <div id="panel" runat="Server" style="padding-top: 10px;">
                                <div class="clearfix">

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Lease Id</label>
                                            <asp:TextBox ID="txtLeaseId" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Surrender Date<span style="color: red;">*</span></label>
                                            <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtsurrender" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Refund Date<span style="color: red;">*</span></label>
                                            <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtrefund" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Security Deposit<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtSDamount"
                                                ErrorMessage="Please Enter Valid Security Deposit " Display="None" ValidationGroup="Val2"
                                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="Server" ControlToValidate="txtSDamount"
                                                ErrorMessage="Please Enter Security Deposit" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtSDamount" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Damages</label>
                                            <asp:TextBox ID="txtDmg" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Damage Amount</label>
                                            <asp:TextBox ID="txtDmgAmt" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Outstanding Amount</label>
                                            <asp:TextBox ID="txtOutAmt" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Amount To Be Paid To Lesse</label>
                                            <asp:TextBox ID="txtAmtPaidLesse" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Amount To Be Paid To Lessor</label>
                                            <asp:TextBox ID="txtAmtPaidLessor" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                    <%--      <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Payee Name</label>
                <asp:TextBox ID="txtPayeeName" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
            </div>
        </div>--%>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Payment Mode<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvpaytype" runat="server" ControlToValidate="ddlPaymentType"
                                                Display="none" ErrorMessage="Please Select Payment Mode" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"
                                                ToolTip="--Select--">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Payment Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTdate"
                                                Display="None" ErrorMessage="Please Select Payment Date" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <div class='input-group date' id='effdate'>
                                                <asp:TextBox ID="txtTdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="panel2" runat="Server">
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Account / Serial Number <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                    Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                    ErrorMessage="Enter Valid Account Number" ValidationExpression="^[a-zA-Z0-9, ]*$"
                                                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control">0</asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Bank Name <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                    Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9, ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div id="panel3" runat="Server" visible="false">
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Account / Serial Number <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAccTwo"
                                                    Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                    ControlToValidate="txtAccTwo" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtAccTwo" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Bank Name <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtBankTwo"
                                                    Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtBankTwo"
                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtBankTwo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Branch Name <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvl3brnch" runat="server" ControlToValidate="txtbrnch"
                                                    Display="None" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revl3brnch" Display="None" runat="server" ControlToValidate="txtbrnch"
                                                    ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtbrnch" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>IFSC Code <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvIFsc" runat="server" ControlToValidate="txtIFSC"
                                                    Display="None" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="REVIFsc" Display="None" runat="server" ControlToValidate="txtIFSC"
                                                    ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtIFSC" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Download Possession Letter</label>

                                            <asp:LinkButton ID="DocLink" runat="server" CssClass="form-control"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Upload Possession Letter</label>
                                            <asp:RegularExpressionValidator ID="rfvupserv" Display="None" ControlToValidate="BrowsePossLtr"
                                                ValidationGroup="Val2" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                                ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                                            </asp:RegularExpressionValidator>
                                            <div class="btn-default">
                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                <asp:FileUpload ID="BrowsePossLtr" runat="Server" onchange="showselectedfiles(this)" Width="90%" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Approver Remarks</label>
                                            <div onmouseover="Tip('Please Enter Remarks')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="multiLine" MaxLength="1000" Height="30%"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                        <div class="form-group">
                                            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" CausesValidation="true" />
                                            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" CausesValidation="true" />
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="modal fade" id="myModal" tabindex='-1'>
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Lease Application form</h4>
                                        </div>
                                        <div class="modal-body" id="modelcontainer">
                                            <%-- Content loads here --%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
