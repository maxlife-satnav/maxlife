﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;


public partial class WorkSpace_SMS_Webfiles_SubLeaseApprove : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_SEARCH_SUB_LEASE_APPROVE_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();
    }

    protected void txtreset_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtReqId.Text = "";
    }

    private void BindGrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SUB_LEASE_APPROVE_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        gvItems.DataSource = sp.GetDataSet();
        gvItems.DataBind();

    }

    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindGrid();
        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataBind();
        lblMsg.Text = "";
    }

    private void UpdateSLAll(int STA_ID)
    {
        try
        {
            DataSet ds = new DataSet();
            foreach (GridViewRow row in gvItems.Rows)
            {
                CheckBox chkselect = (CheckBox)row.FindControl("chkItem");
                Label lblreqid = (Label)row.FindControl("lbllname");
                if (chkselect.Checked == true)
                {
                    SqlParameter[] param = new SqlParameter[5];
                    param[0] = new SqlParameter("@SUB_LEASE_ID", lblreqid.Text);
                    param[1] = new SqlParameter("@STA_ID", STA_ID);
                    param[2] = new SqlParameter("@APPRV_REMARKS", txtRemarks.Text);
                    param[3] = new SqlParameter("@AUR_ID", Session["UID"].ToString());
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SUB_LEASE_APPROVE_REJECT", param);
                }
                else
                {
                    lblMsg.Text = "Please Select Atleast One Checkbox";
                }
            }


        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void btnAppall_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in gvItems.Rows)
        {
            CheckBox chkselect = (CheckBox)row.FindControl("chkItem");
            if (chkselect.Checked == true)
            {
                UpdateSLAll(4010);

                lblMsg.Visible = true;
                lblMsg.Text = "Sub Lease Agreement Approved Successfully";
            }
            else
            {
                lblMsg.Text = "Please Select Atleast One Checkbox";
            }
        }

    }

    protected void btnRejAll_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in gvItems.Rows)
        {
            CheckBox chkselect = (CheckBox)row.FindControl("chkItem");
            if (chkselect.Checked == true)
            {
                UpdateSLAll(4011);

                lblMsg.Visible = true;
                lblMsg.Text = "Sub Lease Agreement Rejected Successfully";
            }
            else
            {
                lblMsg.Text = "Please Select Atleast One Checkbox";
            }
        }

    }


}