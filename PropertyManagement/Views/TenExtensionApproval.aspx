﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TenExtensionApproval.aspx.cs" Inherits="PropertyManagement_Views_TenExtensionApproval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<script type="text/javascript">
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Tenant Extension Approve/Reject
                            </legend>
                        </fieldset>
                        <form id="form1" class="well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />

                            <div class="clearfix">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" EmptyDataText="No Current Tenant Details Found."
                                        CssClass="table table-condensed table-bordered table-hover table-striped" OnPageIndexChanging="gvLDetails_Lease_PageIndexChanging"
                                        OnRowCommand="gvLDetails_Lease_RowCommand" DataKeyNames="PROPERTY_NAME, PM_TEN_TO_DT">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Property Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpropTenSNO" runat="server" Text='<%#Eval("SNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpname" runat="server" Text='<%#Eval("PROPERTY_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Requested Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExtsdate" runat="server" Text='<%#Eval("PM_TE_START_DT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Requested To Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExtEdate" runat="server" Text='<%#Eval("PM_TE_END_DT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblten" runat="server" Text='<%#Eval("TENANT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Extension Requested By">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("PM_TE_CREATED_BY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Requested Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblRequestesDt" runat="server" Text='<%#Eval("PM_TE_CREATED_DT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExtSNO" runat="server" Text='<%#Eval("PM_TE_SNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkExtn" runat="server" Text="ViewDetails" CommandArgument='<%#Eval("PM_TE_SNO")%>' CommandName="ViewDetails"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <br />
                            <br />
                            <asp:HiddenField ID="hdnPropCode" runat="server" />
                            <asp:HiddenField ID="hdnExtSNO" runat="server" />
                            <asp:HiddenField ID="hdnExtFrmDt" runat="server" />
                            <asp:HiddenField ID="hdnExtToDt" runat="server" />
                            <div id="panel1" runat="server" visible="false">
                                <h4>Tenant Agreement Extension Details</h4>
                                <div class="panel panel-default " role="tab" runat="server" id="div0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-multiselectable="true">Property Details</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body color">
                                            <div class="clearfix">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Property Type <span style="color: red;">*</span></label>
                                                        <div class="col-md-12">
                                                            <asp:Label ID="lblproptype" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">City <span style="color: red;">*</span></label>

                                                        <div class="col-md-12">
                                                            <asp:Label ID="lblcity" runat="server"></asp:Label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Location <span style="color: red;">*</span></label>

                                                        <div class="col-md-12">

                                                            <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Property <span style="color: red;">*</span></label>

                                                        <div class="col-md-12">

                                                            <asp:Label ID="lblProperty" runat="server"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="clearfix">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant Occupied Area (Sqft)<span style="color: red;">*</span></label>

                                                        <div class="col-md-12">

                                                            <asp:Label ID="lblOccArea" runat="server"></asp:Label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant From Date</label>
                                                        <div class="col-md-12">
                                                            <asp:Label ID="lblTenFromDt" runat="server"></asp:Label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant End Date</label>
                                                        <div class="col-md-12">
                                                            <asp:Label ID="lblTenToDate" runat="server"></asp:Label>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default " role="tab" runat="server" id="div1">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Tenant Details</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse out">
                                        <div class="panel-body color">
                                            <div class="clearfix">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant <span style="color: red;">*</span></label>

                                                        <div class="col-md-12">

                                                            <asp:Label ID="lblTenant" runat="server"></asp:Label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant Code <span style="color: red;">*</span></label>

                                                        <div class="col-md-12">
                                                            <asp:Label ID="lblTenCode" runat="server"></asp:Label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Parking Spaces</label>

                                                        <div class="col-md-12">

                                                            <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control" ReadOnly></asp:TextBox>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default " role="tab" runat="server" id="div2">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Payment Details</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse out">
                                        <div class="panel-body color">
                                            <div class="clearfix">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant Rent <span style="color: red;">*</span></label>

                                                        <asp:RequiredFieldValidator ID="rfRent" runat="server" ControlToValidate="txtRent"
                                                            Display="None" ErrorMessage="Please Enter Tenant Rent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRent"
                                                            Display="None" ErrorMessage="Please Enter Valid Tenant Rent in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                            ValidationExpression="((\d+)((\.\d{1,4})?))$"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter tenant rent in numbers upto 2 decimal places.')"
                                                                onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtRent" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20" OnTextChanged="txtRent_TextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Security Deposit <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                            Display="None" ErrorMessage="Please Enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                            Display="None" ErrorMessage="Please Enter Valid Security Deposit in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                            ValidationExpression="((\d+)((\.\d{1,4})?))$"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')"
                                                                onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Joining Date <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Joining Date"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">

                                                            <asp:TextBox ID="txtDate" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Payment Terms <span style="color: red;">*</span></label>

                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtpayterms" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Maintenance Fees</label>

                                                        <asp:RegularExpressionValidator ID="revfees" runat="server" ControlToValidate="txtfees"
                                                            Display="none" ErrorMessage="Please Enter Valid Maintenance fees in Numbers or Decimal Number with 2 Decimal Places." ValidationExpression="((\d+)((\.\d{1,4})?))$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter Maintenance Fees in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtfees" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20" OnTextChanged="txtfees_TextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Additional Car Parking Fees </label>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtcar" runat="server" CssClass="form-control" Rows="3" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Total Rent Amount <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvamt" runat="server" ControlToValidate="txtamount"
                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount."></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regexpamount" runat="server" ControlToValidate="txtamount" Display="None"
                                                            ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount in Numbers or Decimal Number with 2 Decimal Places" ValidationExpression="((\d+)((\.\d{1,4})?))$">

                                                        </asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter Total Rent Amount in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Requestor Remarks <span style="color: red;">*</span></label>

                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtReqRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" Rows="3" ReadOnly></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Remarks <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                                                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                                                Rows="3" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="clearfix">
                                    <div class="col-md-12 text-right" style="padding-top: 5px">
                                        <asp:Button ID="btnApprove" CssClass="btn btn-primary custom-button-color" runat="server" Text="Approve" ValidationGroup="Val1" OnClick="btnApprove_Click" />
                                        <asp:Button ID="btnReject" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reject" ValidationGroup="Val1" OnClick="btnReject_Click" />
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
