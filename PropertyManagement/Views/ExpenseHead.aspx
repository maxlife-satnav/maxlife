﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
            /*border-bottom: 1px solid #eee;*/
            /*background-color: #428bca;*/
            /*-webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }
    </style>

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>

<body data-ng-controller="ExpHeadController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                   <fieldset>
                        <legend>Expense Head</legend>
                    </fieldset>
                    <div class="well">
                      
                        <%--<div class="panel-body" style="padding-right: 10px;">--%>
                            <form role="form" id="form1" name="frmExpensesHead" data-valid-submit="SaveData()" novalidate>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Expense Head Code<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmExpensesHead.$submitted && frmExpensesHead.EXP_CODE.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers)" onmouseout="UnTip()">
                                                <input id="EXP_CODE" type="text" name="EXP_CODE" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="ExpHead.EXP_CODE" autofocus class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmExpensesHead.$submitted && frmExpensesHead.EXP_CODE.$invalid" style="color: red">Please enter valid expense head code </span>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Expense Head Name<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmExpensesHead.$submitted && frmExpensesHead.EXP_NAME.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers)" onmouseout="UnTip()">
                                                <input id="EXP_NAME" type="text" name="EXP_NAME" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="ExpHead.EXP_NAME" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmExpensesHead.$submitted && frmExpensesHead.EXP_NAME.$invalid" style="color: red">Please enter valid expense head name </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Remarks</label>
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <textarea id="EXP_REM" runat="server" data-ng-model="ExpHead.EXP_REM" class="form-control" style="height: 30%" maxlength="500"></textarea>
                                            </div>
                                        </div>
                                    </div>


                                   <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-show="ActionStatus==1">
                                            <label class="control-label">Status<span style="color: red;">*</span></label>
                                            <div>
                                                <select id="EXP_STATUS" name="EXP_STATUS" data-ng-model="ExpHead.EXP_STATUS" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="clearfix">
                                    <div class="box-footer text-right">
                                        <div class="form-group">
                                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="padding-left: 20px;">
                                    <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" style="height: 250px; width: 98%;" class="ag-blue"></div>
                                </div>
                            </form>
                       <%--</div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/ExpenseHead.js"></script>
</body>
</html>
