<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApproveRequest.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApproveRequest" Title="Approve Request" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div  class="col-md-12">
                        <fieldset>
                            <legend>Approve Work Request
                            </legend>
                        </fieldset>
                        
                            <form id="form1" class="well" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" ForeColor="Red" />
                                <div class="clearfix">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Request<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                                                Display="None" ErrorMessage="Please Select Work Request" ValidationGroup="Val1"
                                                InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >City <span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" MaxLength="50"
                                                    Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Location <span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" MaxLength="50"
                                                    Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Property Type <span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtproptype" runat="server" CssClass="form-control"
                                                    MaxLength="50" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Property<span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtproperty" runat="server" CssClass="form-control"
                                                    MaxLength="50" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Title<span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                                                    MaxLength="50" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Specifications<span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control" Height="30%"
                                                    TextMode="MultiLine" MaxLength="500" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Estimated Amount<span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Vendor<span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control"
                                                    Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Work Status<span style="color: red;">*</span></label>
                                            <div  >
                                                <asp:TextBox ID="txtstatus" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label  >Remarks<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                                                Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div  >
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                                    MaxLength="250"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 25px; padding-left: 35px">
                                        <asp:Button ID="btnApprove" CssClass="btn btn-primary custom-button-color" runat="server" Text="Approve" ValidationGroup="Val1" CausesValidation="true" />
                                        <asp:Button ID="btnReject" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reject" ValidationGroup="Val1" CausesValidation="true" />
                                    </div>
                                </div>

                            </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

