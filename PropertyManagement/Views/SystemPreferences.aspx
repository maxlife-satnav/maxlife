﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body data-ng-controller="SystemPreferencesController" class="amantra" onload="setDateVals()">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>System Preferences</legend>
                    </fieldset>
                    <div class="well">

                        <form role="form"  name="frmSYS" data-valid-submit="SaveDetails()" novalidate >

                            <div class="clearfix">


                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSYS.$submitted && frmSYS.AST_SYSP_CODE.$invalid}">
                                        <label class="control-label">Month <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Monthlist" data-output-model="System.Monthlist" data-button-label="icon AST_SYSP_CODE"
                                            data-item-label="icon AST_SYSP_CODE maker" data-on-item-click="MonthChanged()" data-on-select-all="MonthSelectAll()" data-on-select-none="MonthSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="System.Monthlist[0]" name="AST_SYSP_CODE" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSYS.$submitted && frmSYS.AST_SYSP_CODE.$invalid">Please select Month </span>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSYS.$submitted && frmSYS.SYSP_FROM_DATE.$invalid}">
                                        <label class="control-label">From Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="SystemPref.SYSP_FROM_DATE" id="SYSP_FROM_DATE" name="SYSP_FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSYS.$submitted && frmSYS.SYSP_FROM_DATE.$invalid">Please select from date </span>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSYS.$submitted && frmSYS.SYSP_TO_DATE.$invalid}">
                                        <label class="control-label">To Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="SystemPref.SYSP_TO_DATE" id="SYSP_TO_DATE" name="SYSP_TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSYS.$submitted && frmSYS.SYSP_TO_DATE.$invalid">Please select to date </span>
                                    </div>
                                </div>
                                <br />

                                <div class="col-md-2 col-sm-6 col-xs-12 text-right">

                                    <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Modify</button>
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 30px;">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" style="height: 340px; width: 100%;" class="ag-blue"></div>
                            </div>


                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script>
    var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

    function setDateVals() {
        $('#SYSP_FROM_DATE').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('#SYSP_TO_DATE').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    }

</script>
<script src="../../Scripts/Lodash/lodash.min.js"></script>
<script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
<script src="../../SMViews/Utility.js"></script>
<script src="../JS/SystemPreferences.js"></script>
</html>
