﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TenantRenewal.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_TenantRenewal" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
</head>
<script type="text/javascript">
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="clearfix">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Tenant Renewal
                            </legend>
                        </fieldset>

                       
                            <form id="form1" class="well" runat="server">
                                <asp:HiddenField ID="hdnSNO" runat="server" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                <div id="PNLCONTAINER" runat="server">
                                    <div class="clearfix">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix" id="Tr1" runat="server" visible="false">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">Lease Type<span style="color: red;">*</span></label>
                                                    <div class="col-md-7">
                                                        <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                                            Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                                            InitialValue="0"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="panel1" runat="server">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            
                                                <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="True" PageSize="5" EmptyDataText="No Current Tenant Details Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Property Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpname" runat="server" Text='<%#Eval("PROPERTY_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Tenant Start Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Tenant Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Tenant">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblten" runat="server" Text='<%#Eval("TENANT_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Created By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="View Details">
                                                            <ItemTemplate>
                                                                <a href="#" onclick="showPopWin('<%# Eval("SNO") %>')">
                                                                    <asp:Label ID="lbltencode" runat="server" Text="View Details"></asp:Label>
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                                <%--  </asp:Panel>--%>
                                            
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </form>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Tenant Renewal Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" src="#" width="100%" height="450px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--<a href="../../PropertyManagement/Views/TenantRenew.aspx">../../PropertyManagement/Views/TenantRenew.aspx</a>--%>
    <script>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "../../PropertyManagement/Views/TenantRenew.aspx?Renewal=" + id);
            $("#myModal").modal().fadeIn();
        }
    </script>
</body>
</html>
