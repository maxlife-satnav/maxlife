﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>

<body data-ng-controller="TenantCustomizedReportController" class="amantra">

    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Tenant Customizable Report
                            </legend>
                        </fieldset>

                        <form id="form1" name="CustomizedReport" class="well" data-valid-submit="LoadData(2)" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid}">
                                        <label>Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getZonebyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.ZN_NAME.$invalid}">
                                        <label>Zone<span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Zone" data-output-model="Customized.Zone" data-button-label="icon ZN_NAME"
                                            data-item-label="icon ZN_NAME maker" data-on-item-click="ZoneChanged()" data-on-select-all="ZoneChangeAll()" data-on-select-none="ZoneSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Zone[0]" name="ZN_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ZN_NAME.$invalid" style="color: red">Please Select Zone </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.STE_NAME.$invalid}">
                                        <label>State<span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="State" data-output-model="Customized.State" data-button-label="icon STE_NAME"
                                            data-item-label="icon STE_NAME maker" data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.State[0]" name="STE_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.STE_NAME.$invalid" style="color: red">Please Select State </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid}">
                                        <label>City<span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid" style="color: red">Please Select City </span>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid}">
                                        <label>Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.Columns.$invalid}">
                                        <label>Select Columns<span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Cols" data-output-model="COL" button-label="icon COL" item-label="icon COL" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="3">
                                        </div>
                                        <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Columns.$invalid" style="color: red">Please Select Columns </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="txtcode">Quick Select</label>
                                    <br />
                                    <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                        <option value="TODAY">Today</option>
                                        <option value="YESTERDAY">Yesterday</option>
                                        <option value="7">Last 7 Days</option>
                                        <option value="30">Last 30 Days</option>
                                        <option value="THISMONTH">This Month</option>
                                        <option value="LASTMONTH">Last Month</option>
                                        <option value="THISYEAR">This Year</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="txtcode">From Date</label>
                                    <div class="input-group date" id='FromDate'>
                                        <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                </div>

                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="txtcode">To Date</label>
                                    <div class="input-group date" id='ToDate'>
                                        <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('ToDate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" style="padding-right: 15px; padding-top: 15px">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                            <%-- <div class="row" style="padding-top: 15px">
                                    <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>--%>
                            <%-- </form>
                            <form id="form2" style="padding-top: 10px">--%>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="clearfix" data-ng-show="GridVisiblity">
                                        <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix" data-ng-show="GridVisiblity2">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 390px; width: auto"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../Dashboard/C3/c3.min.js"></script>
    <link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../JS/TenantCustomizedReport.js"></script>
    <script src="../../SMViews/Utility.js"></script>
    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });
    </script>

</body>
</html>
