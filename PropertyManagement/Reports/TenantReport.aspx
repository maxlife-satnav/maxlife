﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
  

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style type="text/css">
           #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }
    </style>
</head>

<body data-ng-controller="TenantReportController" class="amantra">
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Tenant Report
                            </legend>
                        </fieldset>

                        <form role="form" id="form1" class="well" name="TenantReport" data-valid-submit="LoadData()" novalidate>
                            <div class="clearfix">
                                       <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': TenantReport.$submitted && TenantReport.ZN_NAME.$invalid}">
                                            <label for="txtcode">Zone <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Zonelst" data-output-model="TenantRep.selectedZone" data-button-label="icon ZN_NAME" data-item-label="icon ZN_NAME maker"
                                                data-on-item-click="ZonChanged()" data-on-select-all="ZonChangeAll()" data-on-select-none="ZonSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="TenantRep.selectedZone" name="ZN_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="TenantReport.$submitted && TenantReport.ZN_NAME.$invalid" style="color: red">Please select Zone </span>
                                        </div>
                                    </div>
                                     <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': TenantReport.$submitted && TenantReport.STE_NAME.$invalid}">
                                            <label for="txtcode">State <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Statelst" data-output-model="TenantRep.selectedState" data-button-label="icon STE_NAME" data-item-label="icon STE_NAME maker"
                                                data-on-item-click="SteChanged()" data-on-select-all="SteChangeAll()" data-on-select-none="SteSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="TenantRep.selectedState" name="STE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="TenantReport.$submitted && TenantReport.STE_NAME.$invalid" style="color: red">Please select State </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': TenantReport.$submitted && TenantReport.CTY_NAME.$invalid}">
                                            <label for="txtcode">City <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Citylst" data-output-model="TenantRep.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                                data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="TenantRep.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="TenantReport.$submitted && TenantReport.CTY_NAME.$invalid " style="color: red">Please select city </span>
                                        </div>
                                    </div>

                                 <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group"  data-ng-class="{'has-error': TenantReport.$submitted && TenantReport.LCM_NAME.$invalid}">
                                        <label class="control-label">Location</label>
                                        <div isteven-multi-select data-input-model="Loclist" data-output-model="TenantRep.selectedLoc" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="LocSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="TenantRep.selectedLoc" name="LCM_NAME" style="display: none" required="" />
                                         <span class="error" data-ng-show="TenantReport.$submitted && TenantReport.LCM_NAME.$invalid " style="color: red">Please select location </span>
                                    </div>
                                </div>
                                   
                            </div>
                            <div class="clearfix">

                               

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Property Type</label>
                                        <div isteven-multi-select data-input-model="PrpTypelist" data-output-model="TenantRep.selectedPrpType" data-button-label="icon PN_PROPERTYTYPE" data-item-label="icon PN_PROPERTYTYPE maker"
                                            data-on-item-click="PrpChanged()" data-on-select-all="PrpChangeAll()" data-on-select-none="PrpSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="TenantRep.selectedPrpType" name="PN_PROPERTYTYPE" style="display: none" required="" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Property Name</label>
                                        <div isteven-multi-select data-input-model="PrpNamelist" data-output-model="TenantRep.selectedPrpName" data-button-label="icon PM_PPT_NAME" data-item-label="icon PM_PPT_NAME maker"
                                            data-on-item-click="PrpNameChanged()" data-on-select-all="PrpNameChangeAll()" data-on-select-none="PrpNameSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="TenantRep.selectedPrpName" name="PM_PPT_NAME" style="display: none" required="" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 17px;">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                            
                            <%--<div class="row" style="padding-left: 18px">
                                    <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>--%>

                            <div class="row" style="padding-left: 18px">
                                <div class="box-footer text-right" id="table2" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    <br />
                                    <a data-ng-click="GenReport(TenantRep,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(TenantRep,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(TenantRep,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>

                            <div class="row" style="padding-left: 20px;">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../JS/TenantReport.js"></script>
    <script src="../../SMViews/Utility.js"></script>
    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });
    </script>

</body>
</html>




