﻿//$('#<%= btnSubmit.ClientID %>').click(function() {  
 
function TowerWiseUtilization(LCM_CODE,TWR_CODE)
{ alert(LCM_CODE);
   $("#UsersGrid").jqGrid(
   {    url: '../WorkSpace/Reports/ctrl_handlers/TowerwiseUtilization.ashx?lcm_code='+LCM_CODE+'&twr_code='+TWR_CODE,
        datatype: 'json',
        height: 250,
        
        colNames: ['City','Location','Tower','Project','Floor','Wing','Cost Center','WST','FCB','HCB','TOTAL'],
        colModel: [
                           { name: 'City', width: 100, sortable: true },
                           { name: 'LOCATION_CODE', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true }, 
                           { name: 'Project', index: 'Project', width: 120,   sortable: true },
                           { name: 'Floor', index: 'Floor',  width: 75,  sortable: true },
                           { name: 'Wing', index: 'Wing', width: 50,   sortable: true },
                           { name: 'VERTICAL', index: 'VERTICAL',  width: 100,  sortable: true },
                           { name: 'WST', index: 'WST', width: 50,   sortable: true },
                           { name: 'FCB', index: 'FCB', width: 50,   sortable: true },
                           { name: 'HCB', index: 'HCB',  width: 50,  sortable: true },
                           { name: 'TOTAL', index: 'TOTAL',  width: 50,  sortable: true }
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'Project',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#UsersGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UsersGrid").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'exportexcel.aspx?Mcity='+ city +'&MLCM_CODE='+ brn + ''; jQuery("#UsersGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 
    