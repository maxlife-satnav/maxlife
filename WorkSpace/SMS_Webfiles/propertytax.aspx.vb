﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_propertytax
    Inherits System.Web.UI.Page

    Dim objsubsonic As New clsSubSonicCommonFunctions
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SERVICE_TAX", SqlDbType.Decimal)
        param(0).Value = CDbl(txtservicetax.Text)
        param(1) = New SqlParameter("@MAINTENANCE_TAX", SqlDbType.Decimal)
        param(1).Value = CDbl(txtmaintenance.Text)
        param(2) = New SqlParameter("@sno", SqlDbType.Decimal)
        param(2).Value = 1
        param(3) = New SqlParameter("@ADD", SqlDbType.NVarChar, 50)
        param(3).Value = "ADD"
        objsubsonic.GetSubSonicExecute("ADD_MODIFY_PROPERTYTAX", param)
        lblMsg.Text = "Property Tax added succesfully"
        fillgrid()
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvPropType.PageIndex = 0
            fillgrid()
            gvPropType.Visible = True
            btnmodify.Visible = False
            btnSubmit.Visible = True
        End If
    End Sub
    Private Sub fillgrid()
        objsubsonic.BindGridView(gvPropType, "GET_PROEPRTY_TAX")
    End Sub
    Protected Sub btnmodify_Click(sender As Object, e As EventArgs) Handles btnmodify.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SERVICE_TAX", SqlDbType.Decimal)
        param(0).Value = CDbl(txtservicetax.Text)
        param(1) = New SqlParameter("@MAINTENANCE_TAX", SqlDbType.Decimal)
        param(1).Value = CDbl(txtmaintenance.Text)
        param(2) = New SqlParameter("@sno", SqlDbType.Decimal)
        param(2).Value = txtstore.Text
        param(3) = New SqlParameter("@ADD", SqlDbType.NVarChar, 50)
        param(3).Value = "MODIFY"
        Try
            objsubsonic.GetSubSonicExecute("ADD_MODIFY_PROPERTYTAX", param)
            lblMsg.Text = "Property Tax Modified succesfully"
        Catch ex As Exception
            lblMsg.Text = ex.Message()
        End Try


        fillgrid()
        btnmodify.Visible = False
        btnSubmit.Visible = True
        txtservicetax.Text = ""
        txtmaintenance.Text = ""
    End Sub
    Protected Sub gvPropType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvPropType.PageIndexChanging
        gvPropType.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub
    Protected Sub gvPropType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPropType.RowCommand
        If e.CommandName = "EDIT" Then
            lblMsg.Text = ""
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblID"), Label)
            Dim lblservicetax As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblservicetax"), Label)
            Dim lblmaintenance As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblmaintenance"), Label)
            txtstore.Text = lblID.Text
            txtservicetax.Text = lblservicetax.Text
            txtmaintenance.Text = lblmaintenance.Text
            btnSubmit.Visible = False
            btnmodify.Visible = True
        End If
    End Sub
    Protected Sub gvPropType_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvPropType.RowEditing

    End Sub
End Class
