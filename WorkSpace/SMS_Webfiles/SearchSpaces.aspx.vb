
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_SearchSpaces
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""
        lblSelVertical.Text = Session("Parent")
        'rfvVTl.ErrorMessage = "Please Select " + lblSelVertical.Text
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")

        If Not Page.IsPostBack Then
            'Try
            Filter.Visible = False
            btnSubmit.Visible = False
            btnViewInMap.Visible = False
            Dim sp(0) As SqlParameter
            sp(0) = New SqlParameter("@VC_SESSION", SqlDbType.NVarChar, 50)
            sp(0).Value = Session("Uid").ToString().Trim()
            Dim intCount As Integer = ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_LA_ROLE_COUNT", sp)
            txtFrmDate.Text = getoffsetdate(Date.Today)
            txtToDate.Text = getoffsetdate(Date.Today)
            Dim myList As New System.Collections.ArrayList()
            myList.Add("Group C1")
            myList.Add("Group C2")
            myList.Add("Group D1")
            myList.Add("Group D2")
            myList.Add("Group E")

            If intCount = 0 Then
                Dim k As Integer = myList.IndexOf(ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_GRADE", sp))
                If k = -1 Then
                    PopUpMessage("You are not Authorized to Request for Vertical", Me)
                    btnSubmit.Enabled = False
                    btnViewInMap.Enabled = False

                End If
            End If

            loadVertical()
            LoadCountry()
            LoadCity()
            BindTowers()
            BindFloors()


            ObjSubsonic.Binddropdown(ddlspacetype, "GETSPACETYPE_NOTBCP", "SPACE_TYPE", "SNO")

            'Dim dt As New DataTable
            'Dim i As Integer
            'dt.Columns.Add("sno")
            'For i = 0 To 0
            '    Dim dr As DataRow = dt.NewRow
            '    dr(0) = i + 1
            '    dt.Rows.Add(dr)
            'Next
            'gvEnter.DataSource = dt
            'gvEnter.DataBind()
            ''Fillyears()
            'gvEnter.Visible = True
            RIDDS = ObjSubsonic.RIDGENARATION("VerticalReq")
            'Catch ex As Exception
            '    Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
            'End Try

        End If
    End Sub
    Public Sub cleardata()
        ddlSelectLocation.SelectedItem.Value = -1
        ddlVertical.SelectedItem.Value = -1
        'txtRemarks.Text = String.Empty
    End Sub

    Public Sub LoadCountry()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlCountry, "GET_ACTIVECOUNTRIES_BY_USRID", "CNY_NAME", "CNY_CODE", param)
    End Sub

    Public Sub LoadCity()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE", param)
        loadlocation()
    End Sub
    Public Sub loadlocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlSelectLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Public Sub BindTowers()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlTower, "GET_TOWERS_BY_USRID", "TWR_NAME", "TWR_CODE", param)
    End Sub
    Public Sub BindFloors()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlFloor, "GET_FLOORS_BY_USRID", "FLR_NAME", "FLR_CODE", param)
    End Sub
    Public Sub loadVertical()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlVertical, "GET_ALL_VERTICALS", "Ver_Name", "Ver_Code", param)
        'ObjSubsonic.Binddropdown(ddlVertical, "GET_ALL_VERTICALSBYUSER", "Ver_Name", "Ver_Code")
    End Sub

    Public Sub loadshifts()
        Dim param(0) As SqlParameter

        param(0) = New SqlParameter("@location", SqlDbType.NVarChar, 50)
        param(0).Value = ddlSelectLocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlshift, "usp_getActiveshiftsbylocation", "sh_name", "sh_code", param)

        If (ddlshift.Items.Count < 1) Then
            lblMsg.Text = "You have No Shifts Alloted for this Location"
        End If

        'ObjSubSonic.Binddropdown(ddlshift, "usp_getActiveshiftsbylocation", "sh_name", "sh_code")
    End Sub
    Public Sub loadShifttimings()
        Dim cmd As New SqlCommand
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        Dim dr As SqlDataReader
        cmd.Connection = cn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = Session("TENANT") & "." & "usp_getActiveShifttiming_BYLOC"
        cmd.Parameters.AddWithValue("@sh_code", ddlshift.SelectedItem.Value)
        cmd.Parameters.AddWithValue("@LOC", ddlSelectLocation.SelectedItem.Value)
        cn.Open()
        dr = cmd.ExecuteReader()
        If dr.Read() Then

            starttimehr.SelectedValue = dr("SH_FRM_HRS").ToString()
            starttimemin.SelectedValue = dr("SH_FRM_MINS").ToString()
            endtimehr.SelectedValue = dr("SH_TO_HRS").ToString()
            endtimemin.SelectedValue = dr("SH_TO_MINS").ToString()
        End If
        cn.Close()

    End Sub

    Public Sub loadfloor()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@location", SqlDbType.NVarChar, 50)
        param(0).Value = ddlSelectLocation.SelectedItem.Value
        param(1) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(1).Value = Session("Uid").ToString().Trim()
        'Dim param As SqlParameter = New SqlParameter("@location", SqlDbType.VarChar)
        'param(0).Value = ddlSelectLocation.SelectedItem.Text
        ddlFloor.Items.Clear()
        ObjSubsonic.Binddropdown(ddlFloor, "usp_getActiveFloors_forshifts", "flr_name", "FLR_ID", param)

    End Sub

    Private Sub loadgrid()

        Dim ftime As String = ""
        Dim ttime As String = ""

        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"


        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value = "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value = "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If

        If ddlspacetype.SelectedItem.Value = 1 Then
            ftime = "00:00"
            ttime = "23:30"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If
        Dim param(6) As SqlParameter

        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = ddlSelectLocation.SelectedValue
        param(1) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50)
        param(1).Value = ddlTower.SelectedValue
        param(2) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50)
        param(2).Value = ddlFloor.SelectedValue
        param(3) = New SqlParameter("@FDATE", SqlDbType.DateTime)
        param(3).Value = Convert.ToDateTime(txtFrmDate.Text)
        param(4) = New SqlParameter("@TDATE", SqlDbType.DateTime)
        param(4).Value = Convert.ToDateTime(txtToDate.Text)
        param(5) = New SqlParameter("@FTIME", SqlDbType.DateTime)
        param(5).Value = ftime
        param(6) = New SqlParameter("@TTIME", SqlDbType.DateTime)
        param(6).Value = ttime

        ObjSubsonic.BindGridView(gdavail, "SMS_GET_VACANT_SPACES", param)

    End Sub

    '    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
    '        REQID = ObjSubsonic.REQGENARATION_REQ(ddlVertical.SelectedItem.Value.Trim(), "SVR_ID", Session("TENANT") & ".", "SMS_VERTICAL_REQUISITION")
    '        Dim verticalreqid As String = REQID
    '        Dim cntWst As Integer = 0
    '        Dim cntHCB As Integer = 0
    '        Dim cntFCB As Integer = 0
    '        Dim twr As String = ""
    '        Dim flr As String = ""
    '        Dim wng As String = ""
    '        Dim intCount As Int16 = 0
    '        Dim ftime As String = ""
    '        Dim ttime As String = ""
    '        Dim StartHr As String = "00"
    '        Dim EndHr As String = "00"
    '        Dim StartMM As String = "00"
    '        Dim EndMM As String = "00"
    '        If starttimehr.SelectedItem.Value <> "Hr" Then
    '            StartHr = starttimehr.SelectedItem.Text
    '        End If
    '        If starttimemin.SelectedItem.Value = "Min" Then
    '            StartMM = starttimemin.SelectedItem.Text
    '        End If
    '        If endtimehr.SelectedItem.Value <> "Hr" Then
    '            EndHr = endtimehr.SelectedItem.Text
    '        End If
    '        If endtimemin.SelectedItem.Value = "Min" Then
    '            EndMM = endtimemin.SelectedItem.Text
    '        End If

    '        If ddlspacetype.SelectedItem.Value = 1 Then
    '            ftime = "00:00"
    '            ttime = "23:59"
    '        Else
    '            ftime = StartHr + ":" + StartMM
    '            ttime = EndHr + ":" + EndMM
    '        End If
    '        If CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
    '            lblMsg.Text = "You can't request for the past month"
    '            Exit Sub
    '        ElseIf CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year = getoffsetdatetime(DateTime.Now).Year Then
    '            lblMsg.Text = "You can't request for the past month"
    '            Exit Sub
    '        ElseIf CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now).Date Then
    '            lblMsg.Text = "Please Select Valid Date From Date Cannot be less than Current Date"
    '            Exit Sub
    '        ElseIf CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
    '            lblMsg.Text = "Selected To Date Cannot be less than From Date"
    '            Exit Sub
    '        End If

    '        If CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now).Date Or CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
    '            lblMsg.Text = "Please Select Valid Date, From Date Cannot be less than Current Date/To Date Cannot be less than From Date"
    '            Exit Sub
    '        End If
    '        'Start Page submit
    '        Dim chkcnt As Integer
    '        For Each row As GridViewRow In gdavail.Rows
    '            Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
    '            Dim lblSpaceType As Label = DirectCast(row.FindControl("lblSpaceType"), Label)
    '            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
    '            lblMsg.Text = ""

    '            If chkSelect.Checked = True Then
    '                'twr = returntower(lblspcid.Text, 1)
    '                'flr = returntower(lblspcid.Text, 2)
    '                'wng = returntower(lblspcid.Text, 3)

    '                chkcnt = chkcnt + 1

    '            End If

    '        Next
    '        If chkcnt <> Integer.Parse(txtNumberofReq.Text) Then
    '            ScriptManager.RegisterStartupScript(Me, [GetType](), "Message", "alert('Please Select " & txtNumberofReq.Text & " seat(s)');", True)
    '            Exit Sub

    '        End If

    '        'End Page Submit


    '        Dim sta As Integer = 6
    '        RIDDS = RIDGENARATION("VerticalReq")
    '        Dim param(0) As SqlParameter
    '        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
    '        param(0).Value = RIDDS

    '        If ObjSubsonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
    '            lblMsg.Text = "Request is already raised "
    '            Exit Sub
    '        Else
    '            For Each row As GridViewRow In gdavail.Rows
    '                Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
    '                Dim lblSpaceType As Label = DirectCast(row.FindControl("lblSpaceType"), Label)
    '                Dim lblSpacelyr As Label = DirectCast(row.FindControl("lblSpacelyr"), Label)

    '                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
    '                lblMsg.Text = ""
    '                If chkSelect.Checked = True Then
    '                    'twr = returntower(lblspcid.Text, 1)
    '                    'flr = returntower(lblspcid.Text, 2)
    '                    'wng = returntower(lblspcid.Text, 3)

    '                    If lblSpacelyr.Text = "WORK STATION" Then
    '                        cntWst += 1

    '                    End If
    '                    If lblSpacelyr.Text = "HALF CABIN" Then
    '                        cntHCB += 1

    '                    End If
    '                    If lblSpacelyr.Text = "FULL CABIN" Then
    '                        cntFCB += 1
    '                    End If

    '                End If

    '            Next
    '            Dim param2(15) As SqlParameter

    '            param2(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
    '            param2(0).Value = REQID
    '            param2(1) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
    '            param2(1).Value = ddlVertical.SelectedItem.Value
    '            param2(2) = New SqlParameter("@WSTREQCOUNT", SqlDbType.Int)
    '            param2(2).Value = cntWst
    '            param2(3) = New SqlParameter("@FCREQCOUNT", SqlDbType.Int)
    '            param2(3).Value = cntFCB
    '            param2(4) = New SqlParameter("@HCREQCOUNT", SqlDbType.Int)
    '            param2(4).Value = cntHCB
    '            param2(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
    '            param2(5).Value = txtFrmDate.Text
    '            param2(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
    '            param2(6).Value = txtToDate.Text
    '            param2(7) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
    '            param2(7).Value = ftime
    '            param2(8) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
    '            param2(8).Value = ttime
    '            param2(9) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
    '            param2(9).Value = Session("uid").ToString().Trim()
    '            param2(10) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 50)
    '            param2(10).Value = ddlSelectLocation.SelectedItem.Value
    '            param2(11) = New SqlParameter("@CITY", SqlDbType.NVarChar, 50)
    '            param2(11).Value = ddlCity.SelectedItem.Value
    '            param2(12) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
    '            param2(12).Value = ddlspacetype.SelectedValue
    '            param2(13) = New SqlParameter("@BCPTYPE", SqlDbType.NVarChar, 50)
    '            'param2(13).Value = ddlbcp.SelectedValue
    '            param2(13).Value = ""
    '            param2(14) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
    '            param2(14).Value = sta
    '            param2(15) = New SqlParameter("@REMARKS", SqlDbType.NVarChar, 200)
    '            param2(15).Value = txtRemarks.Text
    '            ObjSubsonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART1", param2)
    '            Dim cnt As Int32 = 0
    '            For Each row As GridViewRow In gdavail.Rows
    '                Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)

    '                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
    '                lblMsg.Text = ""
    '                If chkSelect.Checked = True Then
    '                    verticalreqid = ObjSubsonic.REQGENARATION_REQ(ddlVertical.SelectedItem.Value.Trim(), "ssa_id", Session("TENANT") & ".", "SMS_space_allocation")

    '                    Dim param3(12) As SqlParameter

    '                    param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
    '                    param3(0).Value = verticalreqid
    '                    param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
    '                    param3(1).Value = REQID
    '                    param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
    '                    param3(2).Value = ddlVertical.SelectedItem.Value
    '                    'param3(3) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
    '                    'param3(3).Value = ddlSelectLocation.SelectedItem.Value
    '                    'param3(4) = New SqlParameter("@TOWER", SqlDbType.NVarChar, 200)
    '                    'param3(4).Value = twr
    '                    'param3(5) = New SqlParameter("@FLRID", SqlDbType.NVarChar, 200)
    '                    'param3(5).Value = flr
    '                    'param3(6) = New SqlParameter("@WNGID", SqlDbType.NVarChar, 200)
    '                    'param3(6).Value = wng
    '                    param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
    '                    param3(3).Value = lblspcid.Text
    '                    param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
    '                    param3(4).Value = Session("UID")
    '                    param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
    '                    param3(5).Value = txtFrmDate.Text
    '                    param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
    '                    param3(6).Value = txtToDate.Text
    '                    param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
    '                    param3(7).Value = ddlspacetype.SelectedItem.Value
    '                    param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
    '                    param3(8).Value = ftime
    '                    param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
    '                    param3(9).Value = ttime
    '                    param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
    '                    param3(10).Value = sta

    '                    If ddlspacetype.SelectedValue = 1 Then
    '                        param3(11) = New SqlParameter("@shiftid", SqlDbType.NVarChar, 200)
    '                        param3(11).Value = 1
    '                    Else
    '                        param3(11) = New SqlParameter("@shiftid", SqlDbType.NVarChar, 200)
    '                        param3(11).Value = ddlshift.SelectedItem.Value
    '                    End If


    '                    param3(12) = New SqlParameter("@costcenter", SqlDbType.NVarChar, 200)
    '                    param3(12).Value = ""



    '                    ObjSubsonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART2", param3)

    '                    If ddlspacetype.SelectedIndex = 1 Then
    '                        UpdateRecord(lblspcid.Text, sta, lblspcid.Text)
    '                    Else
    '                        Dim param4(0) As SqlParameter

    '                        param4(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
    '                        param4(0).Value = lblspcid.Text
    '                        Dim count As Integer = 0
    '                        count = ObjSubsonic.GetSubSonicExecuteScalar("Shift_SeatRequsition", param4)
    '                        If count = 1 Then
    '                            UpdateRecord(lblspcid.Text, 11, lblspcid.Text) ' completely allocated
    '                        Else
    '                            UpdateRecord(lblspcid.Text, 10, lblspcid.Text)  ' partially allocated
    '                        End If
    '                    End If
    '                End If
    '            Next
    '        End If

    '        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("6") & "&rid=" & clsSecurity.Encrypt(REQID)
    'GVColor:


    '        If strRedirect <> String.Empty Then
    '            Response.Redirect(strRedirect, False)
    '        End If

    '        SendMail(REQID)


    '    End Sub

    Private Sub SendMail(ByVal REQ_ID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@strReqID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ_ID
        ObjSubsonic.GetSubSonicExecute("SEND_MAIL_VERTCALALLOCATION", param)
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        ddlCity.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@CNYID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlCountry.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlCity, "GET_CITIES_BY_CNYID", "CTY_NAME", "CTY_CODE", param)
        If ddlCity.Items.Count = 0 Then
            lblMsg.Text = "No Cities Available"
        End If
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        ddlSelectLocation.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlCity.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
        If ddlSelectLocation.Items.Count = 0 Then
            lblMsg.Text = "No Locations Available"
        End If

    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSelectLocation.SelectedIndexChanged
        ddlshift.Items.Clear()
        ddlTower.Items.Clear()
        'If ddlSelectLocation.SelectedIndex <> 0 Then
        '    BindTowers()
        '    If ddlTower.Items.Count = 0 Then
        '        lblMsg.Text = "No Floors Available"
        '    End If

        '    loadshifts()

        'End If
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@LCMID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlSelectLocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlTower, "GET_TOWERSBYLCMID", "TWR_NAME", "TWR_CODE", param)
        If ddlTower.Items.Count = 0 Then
            lblMsg.Text = "No Towers Available"
        End If

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ddlFloor.Items.Clear()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@TWRID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlTower.SelectedItem.Value
        param(2) = New SqlParameter("@LOCID", SqlDbType.NVarChar, 200)
        param(2).Value = ddlSelectLocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlFloor, "GET_FLRS_BY_TWR_LOC", "FLR_NAME", "FLR_CODE", param)
        If ddlTower.Items.Count = 0 Then
            lblMsg.Text = "No Floors Available"
        End If
    End Sub

    Protected Sub btnViewInMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInMap.Click
        Dim spcid As String = ""
        Dim count1 As Integer = 0
        For Each row As GridViewRow In gdavail.Rows
            Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                count1 = count1 + 1
                spcid = spcid & lblspcid.Text & ";"
            End If
        Next

        If count1 = 0 Then
            lblMsg.Text = "Please select any of the checkbox to view the map"
        Else

            ScriptManager.RegisterStartupScript(Page, GetType(Page), "OpenWindow", "window.open('SpacesViewMap.aspx?spcid=" & spcid.ToString() & "');", True)

            'Response.Redirect("SpacesViewMap.aspx?spcid=" & spcid)
        End If

    End Sub

    Protected Sub btnavail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnavail.Click

        If Not Page.IsValid Then
            Exit Sub
        End If

        If ddlCountry.SelectedIndex > 0 Then

            If ddlCity.SelectedIndex > 0 Then

                If ddlSelectLocation.SelectedIndex > 0 Then

                    If ddlTower.SelectedIndex > 0 Then

                        If ddlFloor.SelectedIndex > 0 Then

                            If txtFrmDate.Text <> "" And txtToDate.Text <> "" Then

                                loadgrid()

                                If gdavail.Rows.Count > 0 Then
                                    If gdavail.Rows.Count >= 0 Then
                                        Filter.Visible = True
                                        gdavail.Visible = True
                                        btnSubmit.Visible = True
                                        btnViewInMap.Visible = True

                                    Else
                                        Dim ic As New Integer
                                        ic = Convert.ToInt32(gdavail.Rows.Count)
                                        Filter.Visible = False
                                        gdavail.Visible = False
                                        btnSubmit.Visible = False
                                        btnViewInMap.Visible = False
                                        lblMsg.Text = "No of seats required should be less than or equal to total no of seats in selected location - " & ic
                                        lblMsg.Visible = True

                                    End If
                                    btnSubmit.Visible = True
                                    Filter.Visible = True
                                    BindFilters()
                                End If
                            Else

                                lblMsg.Text = "Please Select From Date and To Date"
                            End If
                        Else

                            lblMsg.Text = "Please Select Floor"
                        End If
                    Else

                        lblMsg.Text = "Please Select Tower"
                    End If
                Else

                    lblMsg.Text = "Please Select Preferred Location"
                End If
            Else

                lblMsg.Text = "Please Select City"
            End If
        Else

            lblMsg.Text = "Please Select Country"
        End If
    End Sub

    Private Sub BindFilters()

        Try
            ObjSubsonic.Binddropdown(ddlSpaceTypeFilter, "USP_GET_ACTIVE_SPACE_TYPES", "SPC_TYPE_NAME", "SPC_TYPE_CODE")
            ObjSubsonic.Binddropdown(ddlSpaceSubTypeFilter, "GET_ACTIVE_SPACE_SUB_TYPES", "SST_NAME", "SST_CODE")

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ddlspacetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlspacetype.SelectedIndexChanged
        If ddlspacetype.SelectedItem.Value = "1" Then
            trshift.Visible = False
            trTimeSlot.Visible = False
            starttimehr.SelectedIndex = 0
            starttimemin.SelectedIndex = 0
            endtimehr.SelectedIndex = 0
            endtimemin.SelectedIndex = 0
        Else
            trshift.Visible = True
            trTimeSlot.Visible = True
        End If
    End Sub

    Protected Sub gdavail_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gdavail.PageIndexChanging
        gdavail.PageIndex = e.NewPageIndex
        loadgrid()
    End Sub


   

    Protected Sub btncheck_Click(sender As Object, e As EventArgs) Handles btncheck.Click
        'getfiltergrid()
    End Sub

    Protected Sub ddlshift_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlshift.SelectedIndexChanged
        If ddlshift.SelectedIndex <> 0 Then
            loadShifttimings()


        Else
            starttimehr.SelectedIndex = 0
            starttimemin.SelectedIndex = 0
            endtimehr.SelectedIndex = 0
            endtimemin.SelectedIndex = 0
        End If
        gdavail.DataSource = Nothing
        gdavail.DataBind()
    End Sub

    

    'Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
    '    ddlshift.Items.Clear()
    '    If ddlFloor.SelectedIndex <> 0 Then
    '        Dim param(1) As SqlParameter
    '        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
    '        param(0).Value = Session("Uid").ToString().Trim()
    '        param(1) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50)
    '        param(1).Value = ddlFloor.SelectedItem.Value

    '        '   ObjSubsonic.Binddropdown(ddlFloor, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)

    '        Dim ds As DataSet()
    '        ddlCity.SelectedValue = ds.Tables[0].rows["city_code"][0];


    '    End If
    'End Sub


End Class


