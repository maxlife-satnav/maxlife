﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_conference_ApprovalDtls
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub btnapprove_Click(sender As Object, e As EventArgs) Handles btnapprove.Click
        ApproveConference(7)
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Conference Room Booked Succesfully ');window.location.href='conference_Approval.aspx'</SCRIPT>", False)

    End Sub
    Private Sub ApproveConference(ByVal staid As Int32)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CONFERENCE_ROOM_STATUS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("@STATUS", staid, DbType.Int32)
        sp.Command.AddParameter("@CROOM", ddlConference.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FDATE", txtFrmDate.Text, DbType.DateTime)
        sp.ExecuteScalar()


         For Each row As GridViewRow In gvassets.Rows
            Dim lblassetcode As Label = DirectCast(row.FindControl("lblassetcode"), Label)
            lblMsg.Text = ""
            Dim param5(4) As SqlParameter
            param5(0) = New SqlParameter("@CONF_DATE", SqlDbType.DateTime)
            param5(0).Value = txtFrmDate.Text
            param5(1) = New SqlParameter("@ASSET_ID", SqlDbType.NVarChar, 200)
            param5(1).Value = lblassetcode.Text

            param5(2) = New SqlParameter("@CONF_ID", SqlDbType.NVarChar, 200)
            param5(2).Value = ddlConference.SelectedItem.Value
            param5(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param5(3).Value = staid
            param5(4) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param5(4).Value = Request.QueryString("id")

            ObjSubSonic.GetSubSonicExecute("APPROVE_ASSETS", param5)
        Next




    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblMsg.Text = ""

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If

        If Not Page.IsPostBack Then
            LoadCity()
            Binddetails()
            loadgrid()
        End If
    End Sub
    Private Sub loadgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RAISED_ASSETS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        gvassets.DataSource = sp.GetDataSet()
        gvassets.DataBind()

    End Sub
    Public Sub LoadCity()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub
    Private Sub Binddetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SPACE_GET_CONFERENCE_DETAILS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CITY")).Selected = True
            BindLocation(ddlCity.SelectedValue)
            ddlSelectLocation.ClearSelection()
            ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("SSA_BDG_ID")).Selected = True
            BindTower(ddlSelectLocation.SelectedValue)
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("SSA_TWR_ID")).Selected = True
            BindFloor(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue)
            ddlFloor.ClearSelection()
            ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("SSA_FLR_ID")).Selected = True
            BindConferenceRooms(ddlSelectLocation.SelectedValue, ddlTower.SelectedValue, ddlFloor.SelectedValue)
            ddlConference.ClearSelection()
            ddlConference.Items.FindByValue(ds.Tables(0).Rows(0).Item("SSA_SPC_ID")).Selected = True
            txtFrmDate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
            starttimehr.ClearSelection()
            starttimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_HOUR")).Selected = True
            starttimemin.ClearSelection()
            starttimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_MINUTE")).Selected = True
            endtimehr.ClearSelection()
            endtimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_HOUR")).Selected = True
            endtimemin.ClearSelection()
            endtimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_MINUTE")).Selected = True
            lblEmpSearchId.Text = ds.Tables(0).Rows(0).Item("SSA_EMP_MAP")
            GetEmployeeSearchDetails(lblEmpSearchId.Text)
        End If

    End Sub
    Private Sub GetEmployeeSearchDetails(ByVal EmpCode As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = EmpCode
        Dim dsEmpDetails As New DataSet
        dsEmpDetails = objsubsonic.GetSubSonicDataSet("GETEMPLOYEEDETAILS", param)
        If dsEmpDetails.Tables(0).Rows.Count > 0 Then
            lblEmpSearchId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_ID")
            lblEmpName.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
            lblEmpEmailId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_EMAIL")
            lblReportingTo.Text = dsEmpDetails.Tables(0).Rows(0).Item("REPORTTO")
            lblAUR_BDG_ID.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_BDG_ID")
            lblAUR_VERT_CODE.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_VERT_CODE")
            lblCity.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_CITY")
            lblDepartment.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DEP_ID")
            lblDesignation.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            lblEmpExt.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_EXTENSION")
            lblEmpFloor.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_FLOOR")
            lblPrjCode.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_PRJ_CODE")
            lblResNumber.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            lblState.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_STATE")
            lblEmpTower.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_TOWER")
            lblDoj.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DOJ")
            hypSpaceId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_LAST_NAME")
            If hypSpaceId.Text = "" Then
                lblSpaceAllocated.Text = "Space not allocated to " & lblEmpName.Text
                hypSpaceId.Text = ""
                hypSpaceId.Visible = False

            Else
                lblSpaceAllocated.Text = "Space ID "
                hypSpaceId.Visible = True

            End If
        Else
            lblmsg.Text = "Employee not found."
            empdetails.Visible = False
        End If

    End Sub

    Private Sub BindLocation(ByVal city As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = city
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Private Sub BindTower(ByVal location As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
        param(0).Value = location
        ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)
    End Sub
    Private Sub BindFloor(ByVal tower As String, ByVal location As String)
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforFloors(tower, location)
        ddlFloor.DataSource = verDTO
        ddlFloor.DataTextField = "Floorname"
        ddlFloor.DataValueField = "Floorcode"
        ddlFloor.DataBind()

    End Sub
    Private Sub BindConferenceRooms(ByVal loc As String, ByVal twr As String, ByVal flr As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
        param(0).Value = loc
        param(1) = New SqlParameter("@TOWER", SqlDbType.NVarChar, 200)
        param(1).Value = twr
        param(2) = New SqlParameter("@FLOOR", SqlDbType.NVarChar, 200)
        param(2).Value = flr
        ObjSubSonic.Binddropdown(ddlConference, "SPACE_GET_CONFERENCEIDS", "spc_name", "spc_id", param)
    End Sub

    Protected Sub btnreject_Click(sender As Object, e As EventArgs) Handles btnreject.Click
        ApproveConference(6)
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Conference Room Booking Rejected ');window.location.href='conference_Approval.aspx'</SCRIPT>", False)
    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("conference_Approval.aspx")
    End Sub

    Protected Sub gvassets_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvassets.PageIndexChanging
        gvassets.PageIndex = e.NewPageIndex()
        loadgrid()
    End Sub
End Class
