Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmThanks
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MESSAGE")
            sp.Command.AddParameter("@id", Request.QueryString("id"), DbType.Int32)
            sp.Command.AddParameter("@cuser", Session("UID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lbl1.Text = ds.Tables(0).Rows(0).Item("Message1")
            End If
            sp.ExecuteScalar()
        End If
    End Sub
End Class
