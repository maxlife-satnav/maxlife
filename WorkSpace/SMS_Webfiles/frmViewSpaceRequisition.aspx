<%@ Page Language="vb" AutoEventWireup="false" Inherits="frmViewSpaceRequisition"
    CodeFile="frmViewSpaceRequisition.aspx.vb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <asp:TextBox ID="txttoday" runat="server" Visible="False"></asp:TextBox>
    <asp:TextBox ID="Textbox1" runat="server" Visible="False"></asp:TextBox>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View Space Requisition </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="dvSpaceRequistions" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                    OnRowCreated="dvSpaceRequistions_OnRowCreated" CellPadding="3"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Space Requisition ID">
                                            <ItemTemplate>
                                                <div onmouseover="TagToTip('spn<%# Eval("Space_Requistion_ID")%>')" onmouseout="UnTip()">
                                                    <asp:LinkButton runat="server" ID="lnkSpaceReqId" Text='<% #Bind("Space_Requistion_ID")%>'
                                                        OnClick="lnkSpaceReq_click"></asp:LinkButton>
                                                </div>
                                                <span id='spn<%# Eval("Space_Requistion_ID")%>'>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <asp:Label runat="server" ID="LinkButton1" Text='<% #Bind("Space_Requistion_ID")%>' class="col-md-5 control-label"></asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requisition Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblDate" Text='<% #Bind("Requistion_Date")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblFromDate" Text='<% #Bind("From_Date")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblToDate" Text='<% #Bind("To_Date")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblStatus" Text='<% #Bind("Status")%>'></asp:Label>
                                                <asp:Label runat="server" ID="lblStatusID" Text='<% #Bind("StatusID")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requested by">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblRequestedby" Text='<% #Bind("Requested_by")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lbllocation" Text='<% #Bind("Location")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tower">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblTower" Text='<% #Bind("Tower")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Work stations">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblWS" Text='<% #Bind("Work_stations")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Half Cabins">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblHC" Text='<% #Bind("Half_Cabins")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Cabins">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblFC" Text='<% #Bind("Full_Cabins")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSpace" runat="server" Text="" Font-Bold="true" class="col-md-8 control-label"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
