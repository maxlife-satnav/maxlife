Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_TotalSpaceReport
    Inherits System.Web.UI.Page
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGridSummary()
        End If
    End Sub

    Private Sub BindGridSummary()
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GET_TOTALSPACEREPORT")
        Dim ds As New DataSet
        ds = sp.GetDataSet

        Dim rds As New ReportDataSource()
        rds.Name = "TotalSpaceRptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/TotalSpaceReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub

    'Private Sub BindTot()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOTALSPACE_TOT")
    '    Dim ds As New DataSet()
    '    ds = sp.GetDataSet()
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        lbltot.Text = ds.Tables(0).Rows(0).Item("AVLBL").ToString()
    '        lbltot.Font.Bold = True
    '    End If
    'End Sub
End Class