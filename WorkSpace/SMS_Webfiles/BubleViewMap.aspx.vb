Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Threading.Tasks
Partial Class WorkSpace_SMS_Webfiles_BubleViewMap
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("item") IsNot Nothing Then
                GetSearchMap()
            Else
                GetMap()
            End If

        End If
    End Sub

    Private Sub GetMap()

        '?lcm_code=PSN&twr_code=NA&flr_code=GF&spc_id=''&Date=12/15/2012

        Dim LCM_CODE As String = ""
        Dim TWR_CODE As String = ""
        Dim FLR_CODE As String = ""
        Dim FLR_USR_MAP As String = ""
        Dim spc_id As String = ""
        'Dim Date1 As Date

        LCM_CODE = Request.QueryString("lcm_code")
        TWR_CODE = Request.QueryString("twr_code")
        FLR_CODE = Request.QueryString("flr_code")
       
        spc_id = Request.QueryString("spc_id")
 
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = LCM_CODE
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = TWR_CODE
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = FLR_CODE
        param(3) = New SqlParameter("@TID", SqlDbType.NVarChar, 200)
        param(3).Value = Session("Tenant")
        Dim ds As New DataSet
        'ds = objsubsonic.GetSubSonicDataSet("SP_BBox", param)

        ds = objsubsonic.GetSubSonicDataSet("SP_BBox_WITHMAPTXT", param)
        Dim strspaces As String = ""
        Dim strbox_bounds As String = ""
        'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
        Dim rowcount As Integer
        Dim spcid As String = ""
        rowcount = ds.Tables(0).Rows.Count
        'Parallel.For(0, rowcount - 1, Sub(i)


        '                                  'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1 And ds.Tables(0).Rows.Count > 0
        '                                  If rowcount > 0 Then

        '                                      spcid = ds.Tables(0).Rows(i).Item("spc_id")

        '                                      strspaces = strspaces & "{""longlat"":""" & ds.Tables(0).Rows(i).Item("lat") & "," & ds.Tables(0).Rows(i).Item("lon") & """,""space_id"":""<a  target='_blank' href='../GIS/ShowDetails.aspx?id=" & spcid & "'>" & ds.Tables(0).Rows(i).Item("spc_display") & "</a>"",""imgurl"":""" & ds.Tables(0).Rows(i).Item("IMGURL") & """},"
        '                                      strbox_bounds = ds.Tables(0).Rows(0).Item("BBOX")
        '                                      ' Next

        '                                  End If
        '                              End Sub)

        'If rowcount > 0 Then
        '    For Each dr As DataRow In ds.Tables(0).Rows
        '        spcid = dr.Item("spc_id")

        '        strspaces = strspaces & "{""longlat"":""" & dr.Item("lat") & "," & dr.Item("lon") & """,""space_id"":""<a  target='_blank' href='../GIS/ShowDetails.aspx?id=" & spcid & "'>" & dr.Item("spc_display") & "</a>"",""imgurl"":""" & dr.Item("IMGURL") & """},"
        '        strbox_bounds = dr.Item("BBOX")
        '    Next
        'End If

        'If rowcount > 0 Then
        '    strspaces = ds.Tables(0).Rows(0).Item("SPACES")
        '    strbox_bounds = ds.Tables(0).Rows(0).Item("BOUNDS")
        'End If

        'Dim strResponse As String = ""
        'If File.Exists(MapPath("ViewMap.txt")) Then
        '    strResponse = File.ReadAllText(MapPath("ViewMap.txt"))
        'End If
        'strResponse = Replace(strResponse, "@@data", "[" & strspaces & "]")
        'strResponse = Replace(strResponse, "@@box_bounds", strbox_bounds)




        Dim strResponse As String = ""
        If rowcount > 0 Then
            strResponse = ds.Tables(0).Rows(0).Item("MAP")
        End If
        litmap.Text = strResponse




    End Sub

    Private Sub GetSearchMap()
        Dim LCM_CODE As String = ""
        Dim TWR_CODE As String = ""
        Dim FLR_CODE As String = ""
        Dim item As String = ""
        Dim subcode As String = ""
        'Dim Date1 As Date

        LCM_CODE = Request.QueryString("lcm_code")
        TWR_CODE = Request.QueryString("twr_code")
        FLR_CODE = Request.QueryString("flr_code")
        item = Request.QueryString("item")
        subcode = Request.QueryString("subitem")

        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = LCM_CODE
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = TWR_CODE
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = FLR_CODE
        param(3) = New SqlParameter("@ITEM", SqlDbType.NVarChar, 200)
        param(3).Value = item
        param(4) = New SqlParameter("@SUBITEM", SqlDbType.NVarChar, 200)
        param(4).Value = subcode
        param(5) = New SqlParameter("@TENANT_ID", SqlDbType.NVarChar, 200)
        param(5).Value = Session("Tenant")
        Dim ds As New DataSet

        ds = ObjSubsonic.GetSubSonicDataSet("SMS_GET_ALL_DATA_FLITER_SM", param)

        Dim strResponse As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            strResponse = ds.Tables(0).Rows(0).Item("MAP")
        End If
        litmap.Text = strResponse

    End Sub


End Class
