﻿
Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.Services

Partial Class WorkSpace_SMS_Webfiles_Costcenter_SFT_Cost_Report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            'txtdate.Attributes.Add("onClick", "displayDatePicker('" + txtdate.ClientID + "')")
            'txtdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            Get_Project()
            ddlproject.Items(0).Text = "--All " & Session("Child") & "(s)--"
            BindGrid()
            'rfvloc.ErrorMessage = "Please Select " + Session("Child")
            'btnExport.Visible = False

        End If
    End Sub

    Private Sub Get_Project()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROJECT")
        ddlproject.DataSource = sp.GetDataSet()
        ddlproject.DataTextField = "PROJECT_NAME"
        ddlproject.DataValueField = "PROJECT_CODE"
        ddlproject.DataBind()
        ddlproject.Items(0).Text = "--All " & Session("Child") & "(s)--"
        ddlproject.Items(0).Value = ""
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        BindGrid()

    End Sub
    Private Sub BindGrid()

        Dim project As String = ""
        Dim tilldate As String = ""

        If ddlproject.SelectedValue = "--All " & Session("Child") & "(s)--" Then
            project = ""
        Else
            project = ddlproject.SelectedValue
        End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COSTCENTER_SFT__COST")
        sp.Command.AddParameter("@PROJECT", project, DbType.String)
        sp.Command.AddParameter("@DATE", tilldate, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()

        If ds.Tables(0).Rows.Count > 0 Then
            btnexcel.Visible = True
        Else
            btnexcel.Visible = False
        End If
        gvEmp.DataSource = ds
        gvEmp.DataBind()

        'rds.Value = ds.Tables(0)

    End Sub


    Protected Sub ddlproject_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproject.SelectedIndexChanged
        'ReportViewer1.Visible = False
    End Sub

    Protected Sub gvEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmp.PageIndexChanging
        Try
            gvEmp.PageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
        End Try


    End Sub

    Protected Sub btnexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ExportToExcelEmployeeAllocationReport()
    End Sub

    Private Sub ExportToExcelEmployeeAllocationReport()

        Dim project As String = ""
        Dim tilldate As String = ""
        If ddlproject.SelectedValue = "--All " & Session("Child") & "(s)--" Then
            project = ""
        Else
            project = ddlproject.SelectedValue
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COSTCENTER_SFT__COST")
        sp.Command.AddParameter("@PROJECT", project, DbType.String)
        sp.Command.AddParameter("@DATE", tilldate, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export(Session("Child") & " Wise Seat Cost Report.xls", gv)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

End Class
