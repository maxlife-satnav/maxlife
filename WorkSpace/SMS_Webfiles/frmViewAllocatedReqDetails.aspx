<%@ Page Language="VB" MasterPageFile="~/MasterPage.master"  AutoEventWireup="false" CodeFile="frmViewAllocatedReqDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmViewAllocatedReqDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div>
        <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">  <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label>
              </td>
                        </tr>
                        </table>
                        
        
         
            
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="90%">
                <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                        <td  colspan="3" align="left">
          <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
          
          </td>
                        </tr>
                    <tr>
                        <td>
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td width="100%" class="tableHEADER" align="left">
                           <strong> Allocated Vertical Requisition Details</strong></td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                        </td>
                        <td align="left">
                             <asp:ValidationSummary ID="vsSM" runat="server" CssClass="divmessagebackground" ForeColor="" />
                            <br />
                            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" Text="" CssClass="clsMessage"></asp:Label>
                            <table id="table2" cellspacing="1" cellpadding="1" style="width: 100%;" border="1">
                                <tr id="Tr1" runat="server">
                                    <td colspan="1" style="height: 26px">
                                        <strong>Vertical Requisition ID</strong></td>
                                    <td align="left" colspan="1" style="height: 26px">
                                        <asp:Label ID="lblVerReqID" runat="server" CssClass="clsLabel"></asp:Label>
                                        </td>
                               <td colspan="2" style="height: 26px" align="left">
                                        <strong>Status of the Requisition : </strong>
                                        <asp:Label ID="lblReqStatus" runat="server" CssClass="clsLabel"></asp:Label></td>
                                 </tr>
                            <tr id="trCity" runat="server">
                                            <td align="left" class="clslabel" colspan="2" style="width: 25%; height: 26px;">
                                                City<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                                    Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td  colspan="2" style="width: 25%; height: 26px;">
                                                <asp:DropDownList ID="ddlCity" runat="server" Width="98%" CssClass="clsComboBox" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                <tr id="trLName" runat="server">
                                    <td align="left" class="clslabel" style="width: 25%; height: 26px;">
                                        Preferred Location<font class="clsNote">*</font>&nbsp;
                                    </td>
                                    <td style="width: 25%; height: 26px;">
                                        <asp:DropDownList ID="ddlSelectLocation" runat="server" Width="98%" CssClass="clsComboBox" Enabled="False">
                                        </asp:DropDownList></td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Vertical<font class="clsNote">*</font>
                                    </td>
                                    <td style="height: 26px">
                                        <asp:DropDownList ID="ddlVertical" runat="server" Width="98%" CssClass="clsComboBox" Enabled="False">
                                        </asp:DropDownList></td>
                                </tr>
                                 <tr>
                                           <td align="left" class="clslabel" style="width: 25%; height: 26px;">
                                                &nbsp;Tower<font class="clsNote">*</font>&nbsp;
                                                
                                                </td>
                                           <td style="width: 25%; height: 26px;">
                                                <asp:DropDownList ID="ddlTower" runat="server" Width="98%" CssClass="clsComboBox" Enabled="False">
                                                </asp:DropDownList></td>
                                              <td align="left" style="width: 25%; height: 26px;">
                                                &nbsp;Floor<font class="clsNote">*</font>&nbsp;
                                                </td>
                                            <td style="height: 26px">
                                                <asp:DropDownList ID="ddlFloor" runat="server" Width="98%" CssClass="clsComboBox" Enabled="False">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                             <td align="left" class="clslabel" style="width: 25%; height: 26px;">
                                                &nbsp;Wing<font class="clsNote">*</font>&nbsp;
                                     </td>
                                             <td style="width: 25%; height: 26px;">
                                                <asp:DropDownList ID="ddlWing" runat="server" Width="98%" CssClass="clsComboBox" Enabled="False">
                                                </asp:DropDownList>
                                            </td>
                                          <td style="width: 25%; height: 26px;">  &nbsp; </td>
                    <td style="height: 26px"> &nbsp;  </td>
                                        </tr>
                                <tr id="Tr2" runat="server" visible="false">
                                    <td align="left" class="clslabel" style="width: 25%; height: 17px">
                                        From Date<font class="clsNote">*</font>&nbsp;
                                    </td>
                                    <td style="width: 25%; height: 17px">
                                        <asp:TextBox ID="txtFrmDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField" Enabled="False"></asp:TextBox>&nbsp;
                                    </td>
                                    <td align="left" style="width: 25%; height: 17px">
                                        To Date<font class="clsNote">*</font>&nbsp;
                                    </td>
                                    <td style="height: 17px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField" Enabled="False"></asp:TextBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table class="table" width="100%" border="1">
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvEnter" runat="server" AutoGenerateColumns="False" Width="100%"
                                            PageSize="1">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Month">
                                                    <ItemTemplate>
                                                        <asp:DropDownList Visible="FALSE" ID="ddlMonth" runat="server" CssClass="clsComboBox" Width="100%" Enabled="False">
                                                            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="January"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="February"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="March"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="April"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                                            <asp:ListItem Value="6" Text="June"></asp:ListItem>
                                                            <asp:ListItem Value="7" Text="July"></asp:ListItem>
                                                            <asp:ListItem Value="8" Text="August"></asp:ListItem>
                                                            <asp:ListItem Value="9" Text="September"></asp:ListItem>
                                                            <asp:ListItem Value="10" Text="October"></asp:ListItem>
                                                            <asp:ListItem Value="11" Text="November"></asp:ListItem>
                                                            <asp:ListItem Value="12" Text="December"></asp:ListItem>
                                                        </asp:DropDownList>
                                                         <asp:TextBox ID="txtMonth" runat="server" Enabled="false" CssClass="clsTextField"></asp:TextBox>
                                                      
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year">
                                                    <ItemTemplate>
                                                        <asp:DropDownList Visible="FALSE" ID="ddlYear" runat="server" CssClass="clsComboBox" Width="100%" Enabled="False">
                                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                        </asp:DropDownList> 
                                                        <asp:TextBox ID="txtYear" runat="server" Enabled="false" CssClass="clsTextField"></asp:TextBox>                                                    
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Work Stations Required">
                                                    <ItemTemplate>
                                                        
                                                            <asp:TextBox ID="txtWorkstations" CssClass="clsTextField" runat="server" MaxLength="5" Enabled="False"></asp:TextBox>
                                                        </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Half Cabins Required">
                                                    <ItemTemplate>
                                                      
                                                            <asp:TextBox ID="txtHalfcabinsrequired" CssClass="clsTextField" runat="server" MaxLength="5" Enabled="False"></asp:TextBox>
                                                         </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Full Cabins Required">
                                                    <ItemTemplate>
                                                     
                                                            <asp:TextBox ID="txtFullCabinsRequired" CssClass="clsTextField" runat="server" MaxLength="5" Enabled="False"></asp:TextBox>
                                                       </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Work Stations Approved">
                                                    <ItemTemplate>
                                                      
                                                            <asp:TextBox ID="txtWorkstationsA" CssClass="clsTextField" runat="server" MaxLength="5" Enabled="False"></asp:TextBox>
                                                        </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Half Cabins Approved">
                                                    <ItemTemplate>
                                                       
                                                            <asp:TextBox ID="txtHalfcabinsrequiredA" CssClass="clsTextField" runat="server" MaxLength="5" Enabled="False"></asp:TextBox>
                                                         </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Full Cabins Approved">
                                                    <ItemTemplate>
                                                      
                                                            <asp:TextBox ID="txtFullCabinsRequiredA" CssClass="clsTextField" runat="server" MaxLength="5" Enabled="False"></asp:TextBox>
                                                       </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            
                          
                                <tr>
                                    <td>
                                        <table id="table10"  border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width: 100%">
                                            <tr>
                                                <td align="left" style="width: 25%; height: 26px">
                                                    Space Preference in Sft (if any)</td>
                                                <td style="width: 25%; height: 26px">
                                                    <asp:TextBox ID="txtLabSpace" runat="server" CssClass="clsTextField" MaxLength="10"
                                                        Width="100%" Enabled="False"></asp:TextBox></td>
                                            </tr>
                                            
                                            <tr>                                             
                                                <td style="height: 26px; width: 25%;" align="left">
                                                    Vertical Allocatorís Remarks
                                                    </td>
                                                <td style="height: 26px; width: 25%">
                                                    <asp:Label ID="Label1" runat="server"  Text="Label"></asp:Label></td>
                                            </tr>
                                            
                                            <tr>                                             
                                                <td style="height: 26px; width: 25%;" align="left">
                                                   Remarks
                                                    </td>
                                                <td style="height: 26px; width: 25%">
                                                    <asp:Label ID="Label2" runat="server"  Text="Label"></asp:Label></td>
                                            </tr>
                                            
                                            <tr>                                             
                                                <td style="height: 26px; width: 25%;" align="left">
                                                    Remarks<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvRem" runat="server" ControlToValidate="txtRemarks"
                                                        Display="None" ErrorMessage="Please Enter Remarks" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                                                <td style="height: 26px; width: 25%">
                                                    <div onmouseover="Tip('Enter remarks up to 500 characters')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" TextMode="MultiLine"
                                                            MaxLength="500" Width="100%" Height="35px" Enabled="true"></asp:TextBox></div>
                                                </td>
                                            </tr>
                                            <tr id="trWC" runat="server" height="50%">
                                                <td align="left" style="width: 25%; height: 28px">
                                                    Work Stations</td>
                                                <td style="width: 25%; height: 28px" align="left">
                                                    <asp:GridView ID="gvWorkStations" runat="server" CellPadding="3" Height="127%" ShowHeader="False"
                                                        Width="100%">
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr id="trHC" runat="server">
                                                <td align="left" style="width: 25%; height: 26px">
                                                    Half Cabins</td>
                                                <td style="width: 25%; height: 26px" align="left">
                                                    <asp:GridView ID="gvHalfCabins" runat="server" CellPadding="3" Height="100%" ShowHeader="False"
                                                        Width="100%">
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr id="trFC" runat="server">
                                                <td align="left" style="width: 25%; height: 26px">
                                                    Full Cabins</td>
                                                <td style="width: 25%; height: 26px" align="left">
                                                    <asp:GridView ID="gvFullCabins" runat="server" CellPadding="3" Height="100%" ShowHeader="False"
                                                        Width="100%">
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 17px;" align="center" colspan="2">
                                        <asp:Button ID="btnUpdate" runat="server" Width="76px" CssClass="button" Text="Confirm" OnClick=" btnUpdate_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Width="76px" CssClass="button" Text="Cancel" />
                                        <asp:Button ID="btnBack" runat="server" Width="76px" CssClass="button" Text="Back" CausesValidation="False" /></td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
          
        </div>
    </asp:Content>