﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Cost_per_seat.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Cost_per_seat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
    
    <div>
            <table id="table4" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="center" width="100%">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                            ForeColor="Black">Cost Per seat
             <hr align="center" width="60%" /></asp:Label>
                    </td>
                </tr>
            </table>
            <table id="table5" cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Cost Per seat</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table width="100%" cellpadding="1" cellspacing="0" align="center" border="1">
                            <tr >
                               <td align="left" >
                               Search by Lease Name
                               </td>
                               <td align="left">
                               <asp:TextBox ID="txtsearch" runat="server" CssClass="clsTextField" Width="99%"></asp:TextBox>
                               </td>
                               </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button"  />
                                        
                                </td>
                            </tr>
                        </table>
                        <table id="table1" runat="server" cellspacing="1" cellpadding="1" width="100%" border="0">
                            
                            <tr>
                            <td align="left">
                            <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export to Excel" />
                            </td>
                            </tr>
                            <tr>
                                <td align="LEFT" style="height: 20px">
                               <cc1:ExportPanel ID="exportpanel2" runat="server" Width="100%">
                                    <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                        AllowPaging="True" Width="100%" PageSize="5" EmptyDataText="No Records Found" OnPageIndexChanging="gvitems_PageIndexChanging">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                           
                                           <asp:TemplateField HeaderText="State">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblempid" runat="server"  Text='<%#Eval("STATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                         
                                           <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLesseName" runat="server"  Text='<%#Eval("LOCATION")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="ADDRESS">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllsename" runat="server"  Text='<%#Eval("ADDRESS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="PINCODE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLSDate" runat="server"  Text='<%#Eval("PINCODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Lockinperiod">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLEDate" runat="server"  Text='<%#Eval("LOCKINPERIOD")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Notice of Termination (Months)">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLcost" runat="server"  Text='<%#Eval("NOTICE_OF_TERMINATION_MONTHS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="Date Execution of Agreement">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsecamount" runat="server" Text='<%#Eval("DATE_EXECUTION_OF_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Tenure of Agreement">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblbrkname" runat="server" Text='<%#Eval("TENURE_OF_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Rent Escalation">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblbrkfee" runat="server" Text='<%#Eval("RENT_ESCALATION")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Area IN SQFT">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblldname" runat="server" Text='<%#Eval("AREA_IN_SQFT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Security Deposit">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblldrent" runat="server" Text='<%#Eval("SECURITY_DEPOSIT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rent Per month">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblldsecdeposit" runat="server" Text='<%#Eval("RENT_PER_MONTH")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Maintenance Per month">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblempacc" runat="server" Text='<%#Eval("MAINTENANCE_PER_MONTH")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                          
                                            <asp:TemplateField HeaderText="Property Tax">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblrecfdate" runat="server" Text='<%#Eval("PROPERTY_TAX")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Interst on Deposit @10%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblrectodate" runat="server" Text='<%#Eval("INTEREST_ON_DEPOSIT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rent Interest On Deposit">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblemprem" runat="server" Text='<%#Eval("RENT_INTEREST_ON_DEPOSIT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>




<%--                                            
                                           <asp:TemplateField HeaderText="Depreciation cost on Fixed Assets Excluding IT">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server"  Text='<%#Eval("DEPRECIATION_COST_ON_FIXED_ASSETS_EXCL_IT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                         
                                           <asp:TemplateField HeaderText="Electricity Charges">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server"  Text='<%#Eval("ELECTRICITY_CHARGES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="DG Charges">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server"  Text='<%#Eval("DG_CHARGES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Telephone EXPENSES">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server"  Text='<%#Eval("TELEPHONE_EXPENSES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Postage Courier expenses">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server"  Text='<%#Eval("POSTAGE_COURIER_EXPENSES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pantry Expenses">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server"  Text='<%#Eval("PANTRY_EXPENSES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="Housekeeping Charges">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("HOUSEKEEPING_CHARGES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Security Charges">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("SECURITY_CHARGES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Repairs Maintenance">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label9" runat="server" Text='<%#Eval("REPAIRS_MAINTENANCE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Photocopying Charges">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label10" runat="server" Text='<%#Eval("PHOTOCOPYING_CHARGES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Printing Stationery">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Text='<%#Eval("PRINTING_STATIONERY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AMC Charges">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label12" runat="server" Text='<%#Eval("AMC_CHARGES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SE Fee">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label13" runat="server" Text='<%#Eval("S_E_FEE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Insurance Premium">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label14" runat="server" Text='<%#Eval("INSURANCE_PREMIUM")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="IT Running Cost">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label15" runat="server" Text='<%#Eval("IT_RUNNING_COST")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Cost">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label16" runat="server" Text='<%#Eval("TOTAL_COST")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Opex Cost">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label17" runat="server" Text='<%#Eval("OPEX_COST")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="Total cost for associates">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label18" runat="server"  Text='<%#Eval("TOTAL_COST_FOR_ASSOCIATES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Total capacity">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label19" runat="server"  Text='<%#Eval("TOTAL_CAPACITY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Per Seat Cost">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label20" runat="server"  Text='<%#Eval("PER_sEAT_COST")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="Per Seat cost to Associates">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label21" runat="server" Text='<%#Eval("PER_SEAT_COST_TO_ASSOCIATES")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            
                                        </Columns>
                                        <FooterStyle CssClass="GVFixedFooter" />
                                        <HeaderStyle CssClass="GVFixedHeader" />
                                    </asp:GridView>
                                    </cc1:ExportPanel>
                                </td>
                            </tr>
                        </table>
                    
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </div>

</asp:Content>
 
