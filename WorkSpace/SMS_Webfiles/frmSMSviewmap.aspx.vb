#Region " Document Details "
'**************************************************************************************
'Class Name                 : frmSmsViewMap.aspx.vb
'Purpose                    : Using this page, User can View the map
'Developer                  : Phanindra
'Creation Date              : 27th August 2008
'Last Revised               : 27th August 2008
'**************************************************************************************
'Revision History
'Modified By             Date Modified          Reason for Modification
'**************************************************************************************
#End Region

Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions

Public Class frmSMSviewmap
    Inherits System.Web.UI.Page
    Dim obj As New clsWorkSpace
    Dim objMsater As New clsMasters

    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("Logout"))
        End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            LoadCity()
            ddlCity.ClearSelection()
            ddlLocation.Items.Add("--Select--")
            'objMsater.Bindlocation(ddlLocation)
            ddlTower.Items.Add("--Select--")
            ddlFloor.Items.Add("--Select--")

        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex <> -1 And ddlLocation.SelectedIndex <> 0 Then
                objMsater.BindTowerLoc(ddlTower, ddlLocation.SelectedItem.Value)

            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
            If ddlTower.Items.Count = 1 Then
                lblMsg.Text = "No Tower Available"
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub
    Dim verBll As New VerticalBLL()
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
             
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "--Select--")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If
            If ddlFloor.Items.Count = 1 Then
                lblMsg.Text = "No Floor Available"
            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub
    Public Sub cleardata()
        ddlTower.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0

    End Sub


    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        If Page.IsValid Then

            Dim BDG_ID As String = String.Empty
            Dim FLR_CODE As String = String.Empty


            'Dim spFlrID As New SqlParameter("@VCFLR_CODE", SqlDbType.NVarChar, 50)
            'Dim spTwrID As New SqlParameter("@VCTWR_CODE", SqlDbType.NVarChar, 50)
            'Dim spLocID As New SqlParameter("@VCBDG_CODE", SqlDbType.NVarChar, 50)
            'spFlrID.Value = ddlFloor.SelectedItem.Value
            'spTwrID.Value = ddlTower.SelectedValue
            'spLocID.Value = ddlLocation.SelectedValue

            'ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, Session("TENANT") & "."  & "USP_GET_FLOOR_BDGCODE", spFlrID, spTwrID, spLocID)

            'If ObjDR.Read Then
            '    BDG_ID = ObjDR("FLR_BDG_ID").ToString
            '    FLR_CODE = ObjDR("FLR_Code").ToString
            'End If

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@VCFLR_CODE", SqlDbType.NVarChar, 50)
            param(0).Value = ddlFloor.SelectedItem.Value
            param(1) = New SqlParameter("@VCTWR_CODE", SqlDbType.NVarChar, 50)
            param(1).Value = ddlTower.SelectedItem.Value
            param(2) = New SqlParameter("@VCBDG_CODE", SqlDbType.NVarChar, 50)
            param(2).Value = ddlLocation.SelectedItem.Value

            Dim ds As New DataSet
            ds = objsubsonic.GetSubSonicDataSet("USP_GET_FLOOR_BDGCODE", param)
            If ds.Tables(0).Rows.Count > 0 Then
                BDG_ID = ds.Tables(0).Rows(0).Item("FLR_BDG_ID").ToString
                FLR_CODE = ds.Tables(0).Rows(0).Item("FLR_Code").ToString
            End If

            Dim flag As Integer = 0
            flag = ObjSubsonic.GetSubSonicExecuteScalar("USP_CHECK_FLOOR_CODE", param)
            If flag <> 0 Then


                Session("FLR_CODE") = FLR_CODE
                Session("BDG_ID") = BDG_ID
                Session("Twr_Code") = ddlTower.SelectedValue
                Session("LOC_ID") = ddlLocation.SelectedValue
                Session("Twr_name") = ddlTower.SelectedItem.Text
                Session("Loc_name") = ddlLocation.SelectedItem.Text
                Session("Flr_name") = ddlFloor.SelectedItem.Text

                Session("Cty_Id") = ddlCity.SelectedItem.Value

                'Response.Write("<script language=javascript>javascript:window.open('../GIS/Map_User.aspx','MapWindow','scrollbars=no,toolbar=no,statusbar=no,height=650,width=850,top=0,left=0')</script>")
                'Response.Redirect("Map.aspx")
                ' Response.Redirect("Map.aspx?lcm_code=" & ddlLocation.SelectedValue & "&twr_code=" & ddlTower.SelectedValue & "&flr_code=" & FLR_CODE)

                Response.Redirect(ResolveUrl("~/SMViews/frmSpaceFloormap.aspx") & "?lcm_code=" & ddlLocation.SelectedValue & "&twr_code=" & ddlTower.SelectedValue & "&flr_code=" & FLR_CODE, False)
                'Response.Redirect("frmusermapfloorlist_temp.aspx?lcm_code=" & ddlLocation.SelectedValue & "&twr_code=" & ddlTower.SelectedValue & "&flr_code=" & FLR_CODE & "&spc_id=''&Date=" & getoffsetdate(Date.Today) & "")
            Else
                lblMsg.Text = "There are no maps for the selected floor"
            End If

        End If
    End Sub
    Public Sub LoadCity()
        objMsater.BindVerticalCity(ddlCity)
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            ddlTower.Items.Clear()
            ddlFloor.Items.Clear()
            ddlLocation.Items.Clear()
            If Session("uid") = "" Then
                Response.Redirect(AppSettings("logout"))
            End If
            'strSQL = "select lcm_code,LCM_name +'('+ Lcm_code +')' LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
            'BindCombo(strSQL, ddlLocation, "LCM_name", "lcm_code")
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 50)
            param(0).Value = ddlCity.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATION_BYFLOORMAP", "LCM_name", "LCM_CODE", param)
            
            If ddlLocation.Items.Count = 0 Then
                lblMsg.Text = "No Locations Available"
                lblMsg.Visible = True
            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
