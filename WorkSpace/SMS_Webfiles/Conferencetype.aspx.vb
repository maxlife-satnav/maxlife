﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class WorkSpace_SMS_Webfiles_Conferencetype
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                'obj.Block_LoadGrid(gvItem)
                trLName.Visible = False
                rbActions.Checked = True
                obj.BindConfType(ddlConftype)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "ConferenceMasterr", "Page_Load", exp)
        End Try
    End Sub

    Protected Sub ddlConftype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlConftype.SelectedIndexChanged
        Try
            If ddlConftype.SelectedItem.Value <> "--Select--" Then
                'obj.Block_SelectedIndex_Changed(ddlBName, ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry)
                txtConfTypeCode.Text = ddlConftype.SelectedValue
                txtConfTyepName.Text = ddlConftype.SelectedItem.ToString()
            Else
                cleardata()
                obj.BindConfType(ddlConftype)
            End If
            txtConfTyepName.Focus()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "ConferenceMasterr", "ddlBName_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtConfTypeCode.Text = String.Empty Or txtConfTyepName.Text = String.Empty Then
                    ' PopUpMessage("Please Enter Mandatory fields", Me)
                    lblMsg.Text = "Please Enter Mandatory Fields"
                    lblMsg.Visible = True
                Else
                    Insertdata()
                End If
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If txtConfTypeCode.Text = String.Empty Or txtConfTyepName.Text = String.Empty Or ddlConftype.SelectedItem.Text = "--Select--" Then
                    ' PopUpMessage("Please Enter Mandatory fields", Me)
                    lblMsg.Text = "Please Enter Mandatory Fields"
                    lblMsg.Visible = True

                Else
                    Modifydata()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "ConferenceMasterr", "btnSubmit_Click", exp)
        End Try
    End Sub

    Private Sub Insertdata()
        Try
            'Inserting the data into SPACE Table
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_CONFERENCE_TYPE")
            sp.Command.AddParameter("@SPC_CR_TYPE_CODE", txtConfTypeCode.Text, DbType.String)
            sp.Command.AddParameter("@SPC_CR_TYPE_NAME", txtConfTyepName.Text, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Visible = True
            lblMsg.Text = "New conference added successfully"
            cleardata()
            obj.BindConfType(ddlConftype)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Private Sub Modifydata()
        Try
            'Inserting the data into SPACE Table
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MODIFY_CONFERENCE_TYPE")
            sp.Command.AddParameter("@SPC_CR_TYPE_CODE", txtConfTypeCode.Text, DbType.String)
            sp.Command.AddParameter("@SPC_CR_TYPE_NAME", txtConfTyepName.Text, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Visible = True
            lblMsg.Text = "Conference Type Modified Successfully"
            cleardata()
            obj.BindConfType(ddlConftype)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub cleardata()
        txtConfTypeCode.Text = String.Empty
        txtConfTyepName.Text = String.Empty
        ddlConftype.SelectedIndex = 0
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trLName.Visible = False
                txtConfTypeCode.ReadOnly = False
                btnSubmit.Text = "Submit"
                cleardata()
            Else
                trLName.Visible = True
                txtConfTypeCode.ReadOnly = True
                btnSubmit.Text = "Modify"
                obj.BindConfType(ddlConftype)
                cleardata()

            End If

        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "ConferenceMasterr", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

End Class
