﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.ComponentModel

Partial Class WorkSpace_SMS_Webfiles_assetlocation
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocation()

            getassetcategory()
            TryCast(ddlAssetCategory, IPostBackDataHandler).RaisePostDataChangedEvent()

            BindGrid()
            'btnExport.Visible = False
        End If
    End Sub
    'Private Sub Get_Building()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EFM_SRQ_BUILDING")
    '    sp.Command.AddParameter("@dummy", 1, DbType.Int32)
    '    ddlloc.DataSource = sp.GetDataSet()
    '    ddlloc.DataTextField = "BDG_NAME"
    '    ddlloc.DataValueField = "BDG_ADM_CODE"
    '    ddlloc.DataBind()
    '    ddlloc.Items.Insert(0, "--All--")
    'End Sub

    'Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
    '    BindGrid()

    'End Sub
    Private Sub BindGrid()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_ASSETS1")
        'sp.Command.AddParameter("@LOCATION", ddlloc.SelectedItem.Value, DbType.String)
        'gvitems.DataSource = sp.GetDataSet()
        'gvitems.DataBind()
        Dim AstCat As String = ""
        If ddlAssetCategory.SelectedValue = "--All--" Then
            AstCat = ""
        Else
            AstCat = ddlAssetCategory.SelectedValue
        End If

        Dim AstSubCat As String = ""
        If ddlAstSubCat.SelectedValue = "--All--" Then
            AstSubCat = ""
        Else
            AstSubCat = ddlAstSubCat.SelectedValue
        End If

        Dim AstBrand As String = ""
        If ddlAstBrand.SelectedValue = "--All--" Then
            AstBrand = ""
        Else
            AstBrand = ddlAstBrand.SelectedValue
        End If

        Dim AstModel As String = ""
        If ddlModel.SelectedValue = "--All--" Then
            AstModel = ""
        Else
            AstModel = ddlModel.SelectedValue
        End If

        Dim Location As String = ""
        If ddlLocation.SelectedValue = "--All--" Then
            Location = ""
        Else
            Location = ddlLocation.SelectedValue
        End If
        Dim frmDate As String = FromDate.Text


        Dim toDate As String = ""
        toDate = txtToDate.Text


        Dim param(7) As SqlParameter


        param(0) = New SqlParameter("@Category", SqlDbType.NVarChar, 200)
        param(0).Value = AstCat
        param(1) = New SqlParameter("@SubCategory", SqlDbType.NVarChar, 200)
        param(1).Value = AstSubCat
        param(2) = New SqlParameter("@Brand", SqlDbType.NVarChar, 200)
        param(2).Value = AstBrand
        param(3) = New SqlParameter("@Model", SqlDbType.NVarChar, 200)
        param(3).Value = AstModel
        param(4) = New SqlParameter("@Location", SqlDbType.NVarChar, 200)
        param(4).Value = Location
        param(5) = New SqlParameter("@FromDate", SqlDbType.DateTime, 200)
        param(5).Value = FromDate.Text
        param(6) = New SqlParameter("@ToDate", SqlDbType.DateTime, 200)
        param(6).Value = txtToDate.Text
        param(7) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(7).Value = Session("Uid").ToString

        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_LOCATION_ASSETS1", param)

        'Dim ds As New DataSet
        'ds = sp.GetDataSet()

        Dim rds As New ReportDataSource()
        rds.Name = "AssetAllocRptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetAllocReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub

    'Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_ASSETS1")
    '    sp.Command.AddParameter("@LOCATION", ddlloc.SelectedItem.Value, DbType.String)
    '    Dim gv As New GridView

    '    gv.DataSource = sp.GetDataSet()
    '    gv.DataBind()
    '    Export("Asset_Allocation_report.xls", gv)
    'End Sub


    'Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
    '    gvitems.PageIndex = e.NewPageIndex()
    '    BindGrid()
    'End Sub

    '#Region "Export"
    '    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
    '        HttpContext.Current.Response.ContentType = "application/ms-excel"
    '        Dim sw As StringWriter = New StringWriter
    '        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '        '  Create a form to contain the grid
    '        Dim table As Table = New Table
    '        table.GridLines = gv.GridLines
    '        '  add the header row to the table
    '        If (Not (gv.HeaderRow) Is Nothing) Then
    '            PrepareControlForExport(gv.HeaderRow)
    '            table.Rows.Add(gv.HeaderRow)
    '        End If
    '        '  add each of the data rows to the table
    '        For Each row As GridViewRow In gv.Rows
    '            PrepareControlForExport(row)
    '            table.Rows.Add(row)
    '        Next
    '        '  add the footer row to the table
    '        If (Not (gv.FooterRow) Is Nothing) Then
    '            PrepareControlForExport(gv.FooterRow)
    '            table.Rows.Add(gv.FooterRow)
    '        End If
    '        '  render the table into the htmlwriter
    '        table.RenderControl(htw)
    '        '  render the htmlwriter into the response
    '        HttpContext.Current.Response.Write(sw.ToString)
    '        HttpContext.Current.Response.End()
    '    End Sub
    '    ' Replace any of the contained controls with literals
    '    Private Shared Sub PrepareControlForExport(ByVal control As Control)
    '        Dim i As Integer = 0
    '        Do While (i < control.Controls.Count)
    '            Dim current As Control = control.Controls(i)
    '            If (TypeOf current Is LinkButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
    '            ElseIf (TypeOf current Is ImageButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
    '            ElseIf (TypeOf current Is HyperLink) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
    '            ElseIf (TypeOf current Is DropDownList) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
    '            ElseIf (TypeOf current Is TextBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
    '            ElseIf (TypeOf current Is CheckBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
    '                'TODO: Warning!!!, inline IF is not supported ?
    '            End If
    '            If current.HasControls Then
    '                PrepareControlForExport(current)
    '            End If
    '            i = (i + 1)
    '        Loop
    '    End Sub
    '#End Region
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        'ddlAssetCategory.Items.Insert(0, "--All--")
        ddlAssetCategory.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        'ddlAstSubCat.Items.Insert(0, "--All--")
        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        'getconsumbles()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        TryCast(ddlAstBrand, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ' ddlModel.Items.Insert(0, "--All--")
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))

    End Sub
    Private Sub BindLocation()
        'Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        'param(0).Value = 1
        'param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 100)
        'param(0).Value = Session("Uid").ToString
        'ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
        sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        'ddlLocation.Items.Insert(0, "--All--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "ALL"))


    End Sub
    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        'If ddlAssetCategory.SelectedIndex <> 0 Then
        'ddlAstBrand.Items.Clear()
        'ddlModel.Items.Clear()
        'ddlAstBrand.SelectedIndex = 0
        'ddlModel.SelectedIndex = 0
        'ddlLocation.SelectedIndex = 0
        ' ddlAstSubCat.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()

        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        TryCast(ddlAstSubCat, IPostBackDataHandler).RaisePostDataChangedEvent()

        'End If
    End Sub



    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        'If ddlAstSubCat.SelectedIndex <> 0 Then
        'ddlModel.Items.Clear()
        'ddlModel.SelectedIndex = 0
        'ddlLocation.SelectedIndex = 0
        ddlModel.Items.Clear()
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        'End If

    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        getmakebycatsubcat()
    End Sub


    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        BindGrid()
    End Sub
End Class
