﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmconferencetimeslot.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmconferencetimeslot" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        //function maxLength(s, args) {
        //    if (args.Value.length >= 500)
        //        args.IsValid = false;
        //}

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Conference Booking Availability 
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" />
                            <asp:Label ID="lblmsg" ForeColor="RED" class="col-md-12 control-label" runat="server"></asp:Label><br />
                            <div id="Panel1" runat="server" visible="true">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    City<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCity" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Please Select City" InitialValue="--Select--">
                                                </asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Location<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--">
                                                </asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlSelectLocation" AutoPostBack="true" runat="server"
                                                        CssClass="selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Tower<span style="color: red;">*</span></label>

                                                <asp:CompareValidator ID="cmpflr" runat="server" ControlToValidate="ddlTower" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Tower " ValueToCompare="--Select--" Operator="NotEqual">cmpflr</asp:CompareValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Floor<span style="color: red;">*</span></label>

                                                <asp:CompareValidator ID="cmptwr" runat="server" ControlToValidate="ddlFloor" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Floor " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select Capacity<span style="color: red;">*</span></label>

                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlCapacity" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Capacity" ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlCapacity" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Conference Room<span style="color: red;">*</span></label>

                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlConf" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Conference Room " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlConf" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-bottom: 20px">
                                    <div class="col-md-6">
                                        <label class="btn btn-default pull-right">
                                            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true" />
                                            OneTime
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="btn btn-default" style="margin-left: 25px">
                                            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsrecurring" GroupName="rbActions" AutoPostBack="true" />
                                            Recurring
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select From Date" Display="None" ControlToValidate="txtFdate" __designer:wfdid="w49">
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFdate" ValidationExpression="^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$"
                                                    ErrorMessage="Not Valid Date" ValidationGroup="Val1" Display="None"></asp:RegularExpressionValidator>
                                                <div class="col-md-7">
                                                    <div class='input-group date' id='Fromdate'>
                                                        <asp:TextBox ID="txtFdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('Fromdate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div id="Tr1" runat="server" visible="false">
                                                    <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select To Date" Display="None" ControlToValidate="txtTdate" __designer:wfdid="w49">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTdate" ValidationExpression="^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$"
                                                        ErrorMessage="Not Valid Date" ValidationGroup="Val1" Display="None"></asp:RegularExpressionValidator>
                                                    <div class="col-md-7">
                                                        <div class='input-group date' id='Todate'>
                                                            <asp:TextBox ID="txtTdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('Todate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" Visible="False"
                                    BorderWidth="0px"></asp:TextBox>

                                <div id="rectimes" runat="server" class="row" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">From Time (HH):<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlstarttime"
                                                    Display="None" ErrorMessage="Please Enter From Time" InitialValue="HH" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <%--  <asp:DropDownList ID="ddlstarttime" runat="server" CssClass="selectpicker" data-live-search="true" />--%>
                                                    <asp:DropDownList ID="ddlstarttime" runat="server" CssClass="selectpicker" data-live-search="true">
                                                        <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">To Time (HH):<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlendtime"
                                                    Display="None" ErrorMessage="Please Enter To Time " InitialValue="HH" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlendtime" runat="server" CssClass="selectpicker" data-live-search="true">
                                                        <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="AttendeesMail" runat="server" class="row" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    External Attendees <span style="color: red;">(Please Enter Mail IDs With Comma (,) Separation)</span></label>
                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtAttendees" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                                </asp:RegularExpressionValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtAttendees" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ToolTip="Please Enter Mail IDs With Comma (,) Separtion"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Internal Attendees<span style="color: red;"></span></label>
                                                <div class="col-md-7">
                                                    <asp:ListBox ID="lstInternal" runat="server" SelectionMode="Multiple" CssClass="form-control" Rows="5"></asp:ListBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div id="InternalAttendees" runat="server" class="row" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Description<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription"
                                                    Display="None" ErrorMessage="Please Enter Description" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">

                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Check Availability" CausesValidation="true" ValidationGroup="Val1" />
                                        <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="View Search" />
                                        <asp:Button ID="btncheck" runat="server" CssClass="btn btn-primary custom-button-color" Text="Book" CausesValidation="true" ValidationGroup="Val1" />

                                    </div>
                                </div>
                            </div>

                            <div id="gvPanel" runat="server">
                                <div class="col-md-9 pull-right">
                                    <fieldset>
                                        <legend>
                                            <asp:Label runat="server" ID="lblHeader"></asp:Label>
                                        </legend>
                                    </fieldset>
                                </div>

                                <div class="form-group"></div>
                                <div class="row">
                                    <div class="col-md-11 pull-right">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Location Name<span style="color: red;">*</span></label>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblBuilding1">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-11 pull-right">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Conference Room Name<span style="color: red;">*</span></label>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblConfName1">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-11 pull-right">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Click on date for multiple slot booking <span style="color: red;">*</span></label>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" Text="Click on green slot for single slot booking" ID="Label5">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11 pull-right">
                                    <div class="row">
                                        <div class="form-group">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="../../images/available.gif" />
                                            <asp:Image ID="Image3" runat="server" ImageUrl="../../images/Booked.gif" />
                                            <asp:Image ID="Image2" runat="server" ImageUrl="../../images/Withhold.gif" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvspacereport" runat="server" EmptyDataText="No Conference Booking Found." GridLines="None" TabIndex="5"
                                        CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                        <RowStyle BackColor="#DEDFDE" ForeColor="Black" Wrap="False" />
                                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#33276A" />
                                    </asp:GridView>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblconfalert" ForeColor="RED" class="col-md-12 control-label" runat="server" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" EmptyDataText="No Records Found."
                                        CssClass="table table-condensed table-bordered table-hover table-striped" Visible="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Booked From Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBookedfromdate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_FROM_DATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booked To Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBookedtodate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_TO_DATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="From Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfromtime" runat="server" CssClass="bodyText" Text='<%#(DateTime.Parse(Eval("FTIME").ToString()).ToShortTimeString())%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltotime" runat="server" CssClass="bodyText" Text='<%#(DateTime.Parse(Eval("TTIME").ToString()).ToShortTimeString())%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus" runat="server" CssClass="bodyText" Text='<%#Eval("ConfStatus")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



