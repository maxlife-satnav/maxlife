Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_repVerticalwiseAllocation
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lbl1.Visible = False
        lblSelParent1.Text = Session("Parent")
        If Not Page.IsPostBack Then
            ReportViewer1.Visible = False
            objsubsonic.Binddropdown(ddlTower, "GET_ALLCOSTCENTER_AURID_ALLOC", "COST_CENTER_NAME", "COST_CENTER_CODE")
            ddlTower.Items(0).Text = "--All " & Session("Parent") & "(s)--"
            lblSelParent.Text = Session("Parent")
            'RequiredFieldValidator2.ErrorMessage = "Please Select " + lblSelParent.Text

        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        If ddlTower.SelectedIndex = 0 Then
            lbl1.Visible = True
            Exit Sub
        End If

        Dim Mver As String = ""
        If ddlTower.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            Mver = ""

        Else
            Mver = ddlTower.SelectedItem.Value

        End If

        Dim spVertical As New SqlParameter("@vc_ver_name", SqlDbType.NVarChar, 50)
        spVertical.Value = Mver

        Dim dtVerticalA As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_get_vertical_Wise_ALLOC_Report", spVertical)

        Dim rds As New ReportDataSource()
        rds.Name = "VerticalAllocationDS"
        rds.Value = dtVerticalA
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/VerticalAllocationReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

    End Sub
    
    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
        If ddlTower.SelectedIndex > 0 Then
            lbl1.Visible = False
        Else
            lbl1.Visible = True
        End If
    End Sub

End Class
