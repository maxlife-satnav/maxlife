﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repLocationwiseutilization
    Inherits System.Web.UI.Page

     Dim objMasters As clsMasters
    Dim obj As New clsReports
    Dim MBdgId As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False
        Try
            If Not Page.IsPostBack Then
                binddata()
                'ReportViewer1.Visible = False
                objMasters = New clsMasters()
                objMasters.Bindlocation(ddllocation)
                ddllocation.Items(0).Text = "--All Locations--"
                
                lbltotal_util.ForeColor = Drawing.Color.Black
                lbltotal_util.Font.Bold = True
            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Dim dtReport, dtTemp As New DataTable
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        If ddllocation.SelectedValue = "--All Locations--" Then
            MBdgId = ""
        Else
            MBdgId = ddllocation.SelectedItem.Value

        End If
        binddata()
        tutility.Visible = True

    End Sub

    Public Sub binddata()
        Try

            Dim rds As New ReportDataSource()
            rds.Name = "LocationUtilizationDS"
            'This refers to the dataset name in the RDLC file
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/LocationUtilizationReport.rdlc")
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True


            If ddllocation.SelectedValue = "--All Locations--" Then
                MBdgId = ""
            Else
                MBdgId = ddllocation.SelectedValue
            End If


            Dim sp4 As New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 50)
            sp4.Value = MBdgId

            Dim sp5 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
            sp5.Value = "BUILDING"

            Dim sp6 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
            sp6.Value = "ASC"


            dtTemp = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_LOCATIONSEAT_REPORT", sp4, sp5, sp6)
            rds.Value = dtTemp


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_UTI_COUNT")
            sp.Command.AddParameter("@BDG_ID", MBdgId, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()

            If ds.Tables(0).Rows.Count > 0 Then
                lbltotal_util.Text = ds.Tables(0).Rows(0).Item("util") & "%"
                tutility.Visible = True
            Else
                tutility.Visible = False
            End If



        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddllocation.SelectedIndexChanged

        ReportViewer1.Visible = False
        tutility.Visible = False
    End Sub

End Class

