'StatusID's For vertical ***********************
' 5   - Requsted
' 6   - Allocated
' 166 - Partially Allocated
' 8   - Cancelled  
'***********************

Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_ViewReservedBlockedSeats
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If

        If Not Page.IsPostBack Then
            Try

                'Dim sp As SqlParameter = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
                'sp.Value = Session("uid").ToString().Trim()
                'Dim dt As DataSet
                'dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_VERTICAL_REQ_COUNT", sp)
                'dvVerticalReq.DataSource = dt
                'dvVerticalReq.DataBind()

                BindGridView()

                If dvVerticalReq.Rows.Count = 0 Then
                    lblSpace.Text = "No Requisitions in this Month"
                Else
                    lblSpace.Text = ""
                    Session("View") = "ViewVertical"
                End If

            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)

            Finally
            End Try
        End If
    End Sub


    Public Sub BindGridView()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("uid").ToString().Trim()

        ObjSubSonic.BindGridView(dvVerticalReq, "USP_VERTICAL_REQ_COUNT_ReseverSeats", param)

    End Sub
    Dim strRedirect As String = String.Empty
    Protected Sub lnk_view(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkSender As LinkButton = sender
            For i As Int16 = 0 To dvVerticalReq.Rows.Count - 1
                Dim lnk As LinkButton = CType(dvVerticalReq.Rows(i).FindControl("lnkVerReq"), LinkButton)
                If lnk.ClientID = lnkSender.ClientID Then
                    Dim lblStatus As Label = CType(dvVerticalReq.Rows(i).FindControl("lblStatusID"), Label)
                    Dim lblMon As Label = CType(dvVerticalReq.Rows(i).FindControl("lblMon"), Label)
                    Dim lblYear As Label = CType(dvVerticalReq.Rows(i).FindControl("lblYear"), Label)



                    Dim str As String = Server.MapPath("ReservedBlockedSeatsDetails.aspx")

                    If lnk.Text.Contains("&") = True Then
                        lnk.Text = lnk.Text.Replace("&", ":")
                    End If


                    strRedirect = "ReservedBlockedSeatsDetails.aspx?ReqID=" & lnk.Text.Trim() & "&Month=" & lblMon.Text.Trim() & "&Year=" & lblYear.Text.Trim() & "&StaID=" & lblStatus.Text.Trim()


                End If
            Next
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)

        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If


    End Sub

    Protected Sub dvVerticalReq_OnRowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'If e.Row.RowState = DataControlRowState.Alternate Then
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='White';")
            'Else
            '    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            '    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            'End If
        End If
    End Sub

    Protected Sub dvVerticalReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        dvVerticalReq.PageIndex = e.NewPageIndex


        'Dim sp As SqlParameter = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
        'sp.Value = Session("uid").ToString().Trim()
        'Dim dt As DataSet
        'dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_VERTICAL_REQ_COUNT", sp)
        'dvVerticalReq.DataSource = dt
        'dvVerticalReq.DataBind()

        BindGridView()


        If dvVerticalReq.Rows.Count = 0 Then
            lblSpace.Text = "No Requisitions in this Month"
        Else
            lblSpace.Text = ""
            Session("View") = "ViewVertical"
        End If
    End Sub

    Protected Sub dvVerticalReq_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles dvVerticalReq.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(6).Text = Session("Parent")
        End If
    End Sub
End Class
