Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient


Partial Class WorkSpace_SMS_Webfiles_frmUSERMAPS
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            GetLocationURL()
        End If
    End Sub

    Private Sub GetLocationURL()
        Dim LCM_CODE As String = ""
        Dim TWR_CODE As String = ""
        Dim FLR_CODE As String = ""
        Dim FLR_USR_MAP As String = ""
        Dim spc_id As String = ""

        LCM_CODE = Request.QueryString("lcm_code")
        TWR_CODE = Request.QueryString("twr_code")
        FLR_CODE = Request.QueryString("flr_code")

        spc_id = Request.QueryString("spc_id")


        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = LCM_CODE
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = TWR_CODE
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = FLR_CODE

        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_URL_LOCATION", param)
        If ds.Tables(0).Rows.Count > 0 Then
            FLR_USR_MAP = ds.Tables(0).Rows(0).Item("FLR_USR_MAP")
        End If

        'midFrame.Attributes("src") = "../gis/ViewMap_User.aspx" 'FLR_USR_MAP
        'midFrame.Attributes("src") = "../gis/ViewMap_User.aspx?lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE
        'midFrame.Attributes("src") = "../gis/UserLocationMap.aspx?lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE
        'midFrame.Attributes("src") = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE
        Dim URL As String

        If spc_id = "''" Or spc_id Is Nothing Then
            '  URL = "../../PGMap/frmpgMap.aspx?flr_id=AX0040-NA-GF&box=94019.2734375,8746.4140625,95555.28125,9349.4150390625&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&spc_id=''"
            URL = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&spc_id=''"
        Else

            '   URL = "../../PGMap/frmpgMap.aspx?flr_id=AX0040-NA-GF&box=94019.2734375,8746.4140625,95555.28125,9349.4150390625&spc_id=" & Request.QueryString("spc_id") & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE
            URL = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&spc_id=" & Request.QueryString("spc_id")
        End If
     
        'midFrame.Attributes("src") = URL
        midFrame.Attributes("src") = "../gis/UserLocationMap.aspx?lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE
    End Sub
End Class
