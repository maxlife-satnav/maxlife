﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmAssetThanks.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmAssetThanks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:Panel ID="pnlMain" runat="server" Width="100%" GroupingText="Asset Requisition Status" height="100%">
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Asset Requisition Status
                        <hr align="center" width="75%" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                align="center" border="0">
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong><asp:Label ID="lblSubHead" runat="server" Text="Asset Requisition Status"></asp:Label></strong>
                    </td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                    </td>
                    <td align="center">
                        <br />
                        <table id="table2" cellspacing="0" cellpadding="5" width="100%" border="1" style="border-collapse: collapse">
                            <tr>
                                <td>
                                  <strong>&nbsp;  <asp:Label ID="lblMsg" runat="server"></asp:Label></strong>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>



</asp:Content>

