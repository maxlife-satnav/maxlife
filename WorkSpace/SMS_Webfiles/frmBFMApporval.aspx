<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmBFMApporval.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmBFMApporval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Level 2 Approval
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="vsSM" runat="server" CssClass="alert alert-danger" ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">

                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Request ID to Approve<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvREQid" runat="server" ControlToValidate="ddlReqid"
                                            Display="None" ErrorMessage="Please Select Request ID to Approve / Reject"
                                            InitialValue="--Select--" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlReqid" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlReqid_SelectedIndexChanged"
                                                CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trCName" runat="server">
                                        <label class="col-md-5 control-label">Requestors Name</label>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter Name in alphabets and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtReqname" runat="server" ReadOnly="True" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Tower</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtPB1" runat="server" ReadOnly="True" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="trDep" visible="false" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Department</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtDept" runat="server" ReadOnly="True" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">
                                            Work Stations Required</label>
                                        <asp:RequiredFieldValidator ID="rfvWs" runat="server" ControlToValidate="txtWSreq"
                                            Display="None" ErrorMessage="Please Enter Number Of Work Stations Required"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvWS" runat="server" ControlToValidate="txtWSreq" Display="None"
                                            ErrorMessage="Please Enter Valid Work Stations Required In Numbers" Operator="DataTypeCheck"
                                            Type="Integer"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtWSreq" runat="server" MaxLength="5" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Half Cabins Required</label>
                                        <asp:RequiredFieldValidator ID="rfvCub" runat="server" ControlToValidate="txtCubreq"
                                            Display="None" ErrorMessage="Please Enter Number of Half Cabins Required !"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvCubicals" runat="server" ControlToValidate="txtCubreq"
                                            Display="None" ErrorMessage="Please Enter Valid Half Cabins Required In Numbers"
                                            Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCubreq" runat="server" MaxLength="5" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Full Cabins Required</label>
                                        <asp:RequiredFieldValidator ID="rfvcab" runat="server" ControlToValidate="txtCabreq"
                                            Display="None" ErrorMessage="Please Enter Number of Full Cabins Required !"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvCab" runat="server" ControlToValidate="txtCabreq" Display="None"
                                            ErrorMessage="Please Enter Valid Full Cabins Required in Numbers" Operator="DataTypeCheck"
                                            Type="Integer"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCabreq" runat="server" MaxLength="5" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requestor's Remarks</label>
                                        <asp:CompareValidator ID="cvFdate" runat="server" ControlToValidate="txtFdate" Display="None"
                                            ErrorMessage="Please enter valid from date !" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtReqRemarks" runat="server" TextMode="MultiLine" ReadOnly="True"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFdate" Display="None" ErrorMessage="Please Enter Valid From Date " Operator="DataTypeCheck" Type="Date"> </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtEDate" Display="None"
                                            ErrorMessage="Please enter valid To date !" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtEDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Level 1 Remarks</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtRMrem" runat="server" TextMode="MultiLine" CssClass="form-control"
                                                MaxLength="500" ReadOnly="true">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Level 2 Remarks<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtBFMRemarks"
                                            Display="None" ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvrem" runat="server" ClientValidationFunction="maxLength"
                                            ControlToValidate="txtBFMRemarks" Display="None" ErrorMessage="Please enter remarks in less than or equal to 500 characters"></asp:CustomValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtBFMRemarks" runat="server" MaxLength="500" TextMode="MultiLine"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" />
                                    <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" CausesValidation="False" />
                                    <asp:TextBox ID="txtCubreqHid" runat="server" CssClass="form-control" MaxLength="5"
                                        Visible="False" Width="5px"></asp:TextBox><asp:TextBox ID="txtCabreqHid" runat="server"
                                            CssClass="clsTextField" MaxLength="5" Visible="False" Width="5px"></asp:TextBox><asp:TextBox
                                                ID="txtWSreqHid" runat="server" CssClass="clsTextField" MaxLength="5" Visible="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
