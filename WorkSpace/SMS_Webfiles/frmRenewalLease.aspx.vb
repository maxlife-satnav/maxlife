Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmRenewalLease
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'BindLease_Type()
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            BindGrid()
            BindRejected()

        End If
    End Sub
    Private Sub BindLease_Type()
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
            sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlLtype.DataSource = sp3.GetDataSet()
            ddlLtype.DataTextField = "PN_LEASE_TYPE"
            ddlLtype.DataValueField = "PN_LEASE_ID"
            ddlLtype.DataBind()
            ddlLtype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            'lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LEASES_HR_MODIFY_LEASE1")

            sp.Command.AddParameter("@EMP_ID", Session("uid"), DbType.String)
            gvLDetails_Lease.DataSource = sp.GetDataSet()
            gvLDetails_Lease.DataBind()
            For i As Integer = 0 To gvLDetails_Lease.Rows.Count - 1
                Dim lblLstatus As Label = CType(gvLDetails_Lease.Rows(i).FindControl("lblLstatus"), Label)

                If lblLstatus.Text = 1003 Or lblLstatus.Text = 1010 Then
                    lblLstatus.Text = "In Process of Approval with HR "

                ElseIf lblLstatus.Text = 1006 Then
                    lblLstatus.Text = "Competent Authority Rejected"

                ElseIf lblLstatus.Text = 1004 Then
                    lblLstatus.Text = "Supervisor Rejected"

                ElseIf lblLstatus.Text = 1008 Then
                    lblLstatus.Text = "In-Principal Approval Rejected"

                ElseIf lblLstatus.Text = 1005 Then
                    lblLstatus.Text = "Lease Approved"

                ElseIf lblLstatus.Text = 1001 Or lblLstatus.Text = 1009 Then
                    lblLstatus.Text = "In Process of Approval with Supervisor"

                ElseIf lblLstatus.Text = 1007 Or lblLstatus.Text = 1011 Then
                    lblLstatus.Text = "In Process of Approval with Competant Authority"

                End If

            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRejected()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LEASES_HR_MODIFY_LEASE1_RENEWAL")
        sp.Command.AddParameter("@EMP_ID", Session("uid"), DbType.String)
        gvrejected.DataSource = sp.GetDataSet()
        gvrejected.DataBind()
        For i As Integer = 0 To gvrejected.Rows.Count - 1
            Dim lblLstatus As Label = CType(gvrejected.Rows(i).FindControl("lblLstatus"), Label)

            If lblLstatus.Text = 1003 Or lblLstatus.Text = 1010 Then
                lblLstatus.Text = "In Process of Approval with HR "

            ElseIf lblLstatus.Text = 1006 Then
                lblLstatus.Text = "Competent Authority Rejected"

            ElseIf lblLstatus.Text = 1004 Then
                lblLstatus.Text = "Supervisor Rejected"

            ElseIf lblLstatus.Text = 1008 Then
                lblLstatus.Text = "In-Principal Approval Rejected"

            ElseIf lblLstatus.Text = 1005 Then
                lblLstatus.Text = "Lease Approved"

            ElseIf lblLstatus.Text = 1001 Or lblLstatus.Text = 1009 Then
                lblLstatus.Text = "In Process of Approval with Supervisor"

            ElseIf lblLstatus.Text = 1007 Or lblLstatus.Text = 1011 Then
                lblLstatus.Text = "In Process of Approval with Competant Authority"

            End If

        Next
    End Sub

  

    Protected Sub gvLDetails_Lease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetails_Lease.PageIndexChanging
        gvLDetails_Lease.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvrejected_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvrejected.PageIndexChanging
        gvrejected.PageIndex = e.NewPageIndex()
        BindRejected()
    End Sub
End Class
