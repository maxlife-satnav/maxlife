<%@ Page Language="vb" AutoEventWireup="false"
    CodeFile="frmSMSviewmap.aspx.vb" Inherits="frmSMSviewmap" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">


        function ViewMap() {
            alert(document.frmSMSAvb.cmbFloor.value);
            var flR_id = document.frmSMSAvb.cmbFloor.value;
            alert(flR_id);
            window.open('Map_User.aspx?flR_id=' + flR_id, 'MapWindow', 'scrollbars=no,toolbar=no,statusbar=no,height=540,width=790,top=0,left=0');
        }
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View Maps
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="vsSM" runat="server" CssClass="alert alert-danger" ForeColor="Red" />

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trCName" runat="server">
                                        <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCity"
                                            runat="server" ErrorMessage="Please Select City" ControlToValidate="ddlCity"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" TabIndex="1" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvLoc"
                                            runat="server" ErrorMessage="Please Select Location" ControlToValidate="ddlLocation"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" TabIndex="1" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="Div1" runat="server">
                                        <label class="col-md-5 control-label">Tower<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTower"
                                            runat="server" ErrorMessage="Please Select Tower" ControlToValidate="ddlTower"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Floor<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvFloor"
                                            runat="server" ErrorMessage="Please Select Floor" ControlToValidate="ddlFloor"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="View"></asp:Button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

