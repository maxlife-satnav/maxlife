﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class WorkSpace_SMS_Webfiles_Dashboard
    Inherits System.Web.UI.Page

    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim mode As Integer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        'txtdate.Attributes.Add("onClick", "displayDatePicker('" + txtdate.ClientID + "')")
        'txtdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txtdate.Text = Now.Date
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If


        If Not Page.IsPostBack Then
            Try
                gvSpace.Columns(7).HeaderText = Session("Parent")
                gvSpace.Columns(8).HeaderText = Session("Child")
                txtmode.Text = 0


                verreqs.Visible = False
                spcreqs.Visible = False
                'panel4.GroupingText = Session("Parent")
                'panel5.GroupingText = Session("Child")
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@dummy", SqlDbType.Int)
                param(0).Value = 0
                objsubsonic.Binddropdown(ddllocation, "GET_BDG_LOCATION", "LCM_NAME", "LCM_CODE", param)
                ddllocation.SelectedValue = "PSN"
                ddllocation_SelectedIndexChanged(sender, e)
                Dim sp As SqlParameter = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
                sp.Value = Session("uid")
                Dim dt As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_GET_DETAILS", sp)
                'dvDetails.DataSource = dt
                'dvDetails.DataBind()
                dt.Clear()
                dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_VERTICAL_REQ_COUNT", sp)
                dvVerticalReq.DataSource = dt
                dvVerticalReq.DataBind()

                If dvVerticalReq.Rows.Count = 0 Then
                    lblVertical.Text = "No Requisitions in this Month or Previous Month"
                Else
                    lblVertical.Text = ""
                End If

                dt.Clear()
                '  Dim dvSpaceRequistions As GridView = CType(ac.FindControl("dvSpaceRequistions"), GridView)
                dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_SPACE_REQ_COUNT", sp)
                dvSpaceRequistions.DataSource = dt
                dvSpaceRequistions.DataBind()
                If dvSpaceRequistions.Rows.Count = 0 Then
                    lblSpace.Text = "No Requisitions in this Month or Previous Month"
                Else
                    lblSpace.Text = ""
                    Session("View") = "DashBoard"
                End If

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_MYSPACE_DETAILS")
                sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
                gvSpace.DataSource = sp1.GetDataSet()
                gvSpace.DataBind()

            Catch ex As Exception
            Finally
            End Try
        End If
    End Sub
    Private Sub Binddashboard()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_DASHBOARD_DATA")
        sp.Command.AddParameter("@LOCATION", ddllocation.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@FDATE", txtdate.Text, DbType.Date)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltotavail.Text = "Total seats : " & ds.Tables(0).Rows(0).Item("TOTAL_AVAILABILITY")
            lbltotws.Text = "Workstations : " & ds.Tables(0).Rows(0).Item("TOTAL_WORKSTATIONS")
            lbltotfc.Text = "Full cabins : " & ds.Tables(0).Rows(0).Item("TOTAL_FULL_CABINS")
            lbltothc.Text = "Half cabins : " & ds.Tables(0).Rows(0).Item("TOTAL_HALF_CABINS")
            lblfirstoccupied.Text = "Occupied seats : " & ds.Tables(0).Rows(0).Item("OCCUPIED")
            lblfirstallocated.Text = "Allocated seats : " & ds.Tables(0).Rows(0).Item("ALLOCATED")
            lblfirstallocatedNOT.Text = "Allocated but not occupied seats : " & ds.Tables(0).Rows(0).Item("ALLOCATED_BUT_NOT_OCCUPIED")
            lblgrossarea.Text = "Total Area : " & ds.Tables(0).Rows(0).Item("GROSS_SPACE")
            lblworkspace.Text = "Workable Area : " & ds.Tables(0).Rows(0).Item("WORK_SPACE")
            '& ds.Tables(0).Rows(0).Item("WORK_SPACE")
            'lbloccspace.Text = ds.Tables(0).Rows(0).Item("")
        End If
    End Sub
    Protected Sub dvVerticalReq_OnRowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dvVerticalReq.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            'If e.Row.RowState = DataControlRowState.Alternate Then
            'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B28E99';")
            'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#B28E99';")
            'Else
            '    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            '    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            'End If
        End If
    End Sub

    Sub CustomersGridView_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

        ' Get the currently selected row using the SelectedRow property.
        ' Dim row As GridViewRow = CustomersGridView.SelectedRow

        ' Display the company name from the selected row.
        ' In this example, the third column (index 2) contains
        ' the company name.
        'Message.Text = "You selected " & row.Cells(2).Text & "."

    End Sub
    Sub CustomersGridView_SelectedIndexChanging(ByVal sender As Object, ByVal e As GridViewSelectEventArgs)

        ' Get the currently selected row. Because the SelectedIndexChanging event
        ' occurs before the select operation in the GridView control, the
        ' SelectedRow property cannot be used. Instead, use the Rows collection
        ' and the NewSelectedIndex property of the e argument passed to this 
        ' event handler.
        ' Dim row As GridViewRow = CustomersGridView.Rows(e.NewSelectedIndex)

        ' You can cancel the select operation by using the Cancel
        ' property. For this example, if the user selects a customer with 
        ' the ID "ANATR", the select operation is canceled and an error message
        ' is displayed.
        'If row.Cells(1).Text = "ANATR" Then

        'e.Cancel = True
        'Message.Text = "You cannot select " + row.Cells(2).Text & "."

        'End If

    End Sub

    Protected Sub lnk_view(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkSender As LinkButton = sender
        Dim str As String
        For i As Int16 = 0 To dvVerticalReq.Rows.Count - 1
            Dim lnk As LinkButton = CType(dvVerticalReq.Rows(i).FindControl("lnkVerReq"), LinkButton)
            If lnk.ClientID = lnkSender.ClientID Then
                Dim lblStatus As Label = CType(dvVerticalReq.Rows(i).FindControl("lblStatusID"), Label)
                Dim lblMon As Label = CType(dvVerticalReq.Rows(i).FindControl("lblMon"), Label)
                Dim lblYear As Label = CType(dvVerticalReq.Rows(i).FindControl("lblYear"), Label)
                If lblStatus.Text = "5" Or lblStatus.Text = "6" Or lblStatus.Text = "166" Or lblStatus.Text = "8" Then
                    Session("View") = "DashBoard"
                    str = Server.MapPath("frmDashBoardViewVerticalReq.aspx")
                    If lnk.Text.Contains("&") = True Then
                        lnk.Text = lnk.Text.Replace("&", ":")
                    End If
                    Response.Redirect("frmDashBoardViewVerticalReq.aspx?ReqID=" & lnk.Text.Trim() & "&Month=" & lblMon.Text.Trim() & "&Year=" & lblYear.Text.Trim() & "&StaID=" & lblStatus.Text.Trim())
                    Exit Sub
                ElseIf lblStatus.Text = "167" Then
                    Session("View") = "DashBoard"
                    str = Server.MapPath("frmDashBoardViewVerticalReq.aspx")
                    Response.Redirect("frmViewAllocatedReqDetails.aspx?ReqID=" & lnk.Text.Trim() & "&Month=" & lblMon.Text.Trim() & "&Year=" & lblYear.Text.Trim() & "&StaID=" & lblStatus.Text.Trim())
                    Exit Sub
                Else
                    'PopUpMessage("Request -1", Me)
                    Exit Sub
                End If
            End If
        Next
    End Sub
    Protected Sub lnkSpaceReq_click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkSender As LinkButton = sender
        For i As Int16 = 0 To dvSpaceRequistions.Rows.Count - 1
            Dim lnk As LinkButton = CType(dvSpaceRequistions.Rows(i).FindControl("lnkSpaceReqId"), LinkButton)
            If lnk.ClientID = lnkSender.ClientID Then
                Dim lblStatus As Label = CType(dvSpaceRequistions.Rows(i).FindControl("lblStatusID"), Label)
                Dim str As String = Server.MapPath("frmDashBoardViewVerticalReq.aspx")
                Response.Redirect("frmDashBoardViewSpaceRequisition.aspx?ReqID=" & lnk.Text.Trim() & "&StaID=" & lblStatus.Text.Trim())
            End If
        Next
    End Sub
    Protected Sub dvSpaceRequistions_OnRowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dvSpaceRequistions.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            'If e.Row.RowState = DataControlRowState.Alternate Then
            'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B28E99';")
            'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#B28E99';")
            'Else
            '    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            '    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            'End If
        End If
    End Sub

    Private Sub Vertical_Barchart()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddllocation.SelectedItem.Value
        'param(1) = New SqlParameter("@FDATE", SqlDbType.DateTime)
        'param(1).Value = txtdate.Text
        ds = objsubsonic.GetSubSonicDataSet("GET_BARGRAPH_DATA_OCCUPIED_VERTICAL", param)

        Dim strDataCategories As String = ""
        Dim strTotalOccupied As String = ""
        Dim strTotalAlloted As String = ""
        Dim strTotalBlocked As String = ""
        Dim strTotalReserved As String = ""
        Dim strTotalAllocvertical As String = ""
        Dim strTotalCancelled As String = ""

        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                strDataCategories = strDataCategories & "'" & ds.Tables(0).Rows(i).Item("VERTICAL") & "',"
                strTotalOccupied = strTotalOccupied & ds.Tables(0).Rows(i).Item("OCCUPIED_SEATS") & ","
                strTotalAlloted = strTotalAlloted & ds.Tables(0).Rows(i).Item("ALLOTED_SEATS") & ","
                strTotalBlocked = strTotalBlocked & ds.Tables(0).Rows(i).Item("BLOCKED") & ","
                strTotalReserved = strTotalReserved & ds.Tables(0).Rows(i).Item("RESERVED") & ","
                strTotalAllocvertical = strTotalAllocvertical & ds.Tables(0).Rows(i).Item("ALLOC_VERT") & ","
                strTotalCancelled = strTotalCancelled & ds.Tables(0).Rows(i).Item("CANCELLED") & ","

            Next

            If strDataCategories.EndsWith(",") = True Then
                strDataCategories = strDataCategories.Substring(0, strDataCategories.Length - 1)
            End If

            If strTotalOccupied.EndsWith(",") = True Then
                strTotalOccupied = strTotalOccupied.Substring(0, strTotalOccupied.Length - 1)
            End If

            If strTotalAlloted.EndsWith(",") = True Then
                strTotalAlloted = strTotalAlloted.Substring(0, strTotalAlloted.Length - 1)
            End If

            Dim FILE_NAME As String = Server.MapPath("~/GraphicalReport/sources/BarChart_VerticalReport.txt")
            Dim TextLine As String = ""
            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim objReader As New System.IO.StreamReader(FILE_NAME)
                Do While objReader.Peek() <> -1
                    TextLine = TextLine & objReader.ReadLine().Replace("<Alloc not Occ>", strTotalAlloted.ToString).Replace("<Occ>", strTotalOccupied.ToString).Replace("<blck>", strTotalBlocked.ToString).Replace("<DataCategories>", strDataCategories.ToString)
                Loop
                litvertbarchart.Text = TextLine.Replace("<vertical>", Session("Parent"))
                objReader.Close()
                objReader.Dispose()
            End If

        Else
            litvertbarchart.Text = "No Data available"



        End If
    End Sub
    Private Sub Department_Barchart()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddllocation.SelectedItem.Value
        'param(1) = New SqlParameter("@FDATE", SqlDbType.DateTime)
        'param(1).Value = txtdate.Text
        ds = objsubsonic.GetSubSonicDataSet("GET_BARGRAPH_DATA_OCCUPIED_DEPARTMENT", param)
        Dim strDataCategories As String = ""
        Dim strTotalOccupied As String = ""


        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                strDataCategories = strDataCategories & "'" & ds.Tables(0).Rows(i).Item("DEPARTMENT") & "',"
                strTotalOccupied = strTotalOccupied & ds.Tables(0).Rows(i).Item("OCCUPIED") & ","

            Next
            If strDataCategories.EndsWith(",") = True Then
                strDataCategories = strDataCategories.Substring(0, strDataCategories.Length - 1)
            End If
            If strTotalOccupied.EndsWith(",") = True Then
                strTotalOccupied = strTotalOccupied.Substring(0, strTotalOccupied.Length - 1)
            End If


            Dim FILE_NAME As String = Server.MapPath("~/GraphicalReport/sources/BarChart_DepartmentReport.txt")
            Dim TextLine As String = ""
            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim objReader As New System.IO.StreamReader(FILE_NAME)
                Do While objReader.Peek() <> -1
                    TextLine = TextLine & objReader.ReadLine().Replace("<Occ>", strTotalOccupied.ToString).Replace("<DataCategories>", strDataCategories.ToString)
                Loop
                litdepbarchart.Text = TextLine.Replace("<Cost Center>", Session("Child"))
                objReader.Close()
                objReader.Dispose()
            End If
        Else
            litdepbarchart.Text = "No Data available"
        End If
    End Sub
    Private Sub Piechart_week(ByVal i As Integer)
        Dim ds As New DataSet
        Dim FILE_NAME As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Get_week_data_space")
        sp.Command.AddParameter("@Data", i, DbType.Int32)
        sp.Command.AddParameter("@Location", ddllocation.SelectedItem.Value, DbType.String)
        ds = sp.GetDataSet
        Dim strdata As String = ""
        Dim strColor As String = "["
        Dim strpiedata As String = ""
        Dim Occupied As String = ""
        Dim Allocated As String = ""
        Dim Blocked As String = ""
        Dim Reserved As String = ""
        Dim Allocvertical As String = ""
        Dim Cancelled As String = ""


        If ds.Tables(0).Rows.Count > 0 Then
            Occupied = ds.Tables(0).Rows(0)("OccUPIED")
            Reserved = ds.Tables(0).Rows(0)("Res")
            Blocked = ds.Tables(0).Rows(0)("Block")
            'Allocvertical = ds.Tables(0).Rows(0)("Alloc_other_vertical")
            'Cancelled = ds.Tables(0).Rows(0)("Cancld")
            Allocated = ds.Tables(0).Rows(0)("Allocated")

            If Occupied = "0" And Allocated = "0" And Blocked = "0" Then
                Litweek.Text = "NO Data available "
            Else
                strpiedata = "['Occ'," & Occupied & "],['Alloc'," & Allocated & "],['Block'," & Blocked & "]"
            End If
            If strpiedata.EndsWith(",") = True Then
                strpiedata = strpiedata.Substring(0, strpiedata.Length - 1)
            End If
        End If
        If i = 1 Then
            FILE_NAME = Server.MapPath("~/GraphicalReport/sources/PieChart_weekdata.txt")
        Else
            FILE_NAME = Server.MapPath("~/GraphicalReport/sources/PieChart_month.txt")
        End If
        Dim TextLine As String = ""
        If System.IO.File.Exists(FILE_NAME) = True Then
            Dim objReader As New System.IO.StreamReader(FILE_NAME)
            Do While objReader.Peek() <> -1
                TextLine = TextLine & objReader.ReadLine().Replace("<PieFloorArea>", strpiedata.ToString)
            Loop
            If i = 1 Then
                Litweek.Text = TextLine
            Else
                Litmonth.Text = TextLine

            End If
            objReader.Close()
            objReader.Dispose()
        End If
    End Sub


    'toggle button events

    Protected Sub lnkbtntoggle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtntoggle.Click
        If txtmode.Text = 1 Then
            '  empdetails.Visible = False
            verreqs.Visible = False
            spcreqs.Visible = False
            dashboard.Visible = True
            dashboard_content.Visible = True
            txtmode.Text = 0
        Else
            'empdetails.Visible = True
            verreqs.Visible = True
            spcreqs.Visible = True
            dashboard.Visible = False
            dashboard_content.Visible = False
            txtmode.Text = 1
        End If
        'dashboard.Visible = False
        'dashboard_content.Visible = False
    End Sub
    Protected Sub lnkbtnshow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnshow.Click
        If txtmode.Text = 0 Then
            ' empdetails.Visible = True
            verreqs.Visible = True
            spcreqs.Visible = True
            txtmode.Text = 1
        End If
        dashboard.Visible = False
        dashboard_content.Visible = False
    End Sub
    Protected Sub lnkbtndashboard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtndashboard.Click
        ' empdetails.Visible = False
        verreqs.Visible = False
        spcreqs.Visible = False
        dashboard.Visible = True
        dashboard_content.Visible = False
        txtmode.Text = 0
        ddllocation.SelectedIndex = 0
    End Sub



    Protected Sub ddllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddllocation.SelectedIndexChanged
        If ddllocation.SelectedIndex > 0 Then
            dashboard_content.Visible = True
            Binddashboard()
            Piechart_week(1)
            Piechart_week(2)
            Vertical_Barchart()
            Department_Barchart()

        Else
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please select Location');", True)
            dashboard_content.Visible = False
        End If

        'empdetails.Visible = False
        'verreqs.Visible = True
        'spcreqs.Visible = True
    End Sub
End Class
