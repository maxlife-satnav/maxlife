﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Text
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports Google.Apis.Calendar.v3.Data

Partial Class WorkSpace_SMS_Webfiles_frmconferencetimeslot
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty
    Dim Conf_REQ_ID As String

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click

        If CDate(txtFdate.Text) >= getoffsetdate(Date.Today) Then
            txtTdate.Text = LastDayOfMonth(txtFdate.Text)
            If CDate(txtFdate.Text) <= CDate(txtTdate.Text) Then
                bindtimeslots()
                lblmsg.Visible = False
                'hpViewDetails.Visible = True
                Panel1.Visible = False
                btnView.Visible = True
                btnsubmit.Visible = False

            Else
                lblmsg.Visible = True
                lblmsg.Text = "To Date should be greater than From Date"
                'hpViewDetails.Visible = False

            End If
        Else
            lblmsg.Visible = True
            lblmsg.Text = "From Date should be greater than Today's Date"
            'hpViewDetails.Visible = False

        End If
    End Sub

    Private Sub cleardata()
        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        'ddlConference.Items.Clear()
        ddlCapacity.Items.Clear()
        ddlConf.Items.Clear()
        txtFdate.Text = String.Empty
        txtTdate.Text = String.Empty
        ddlstarttime.ClearSelection()
        ddlendtime.ClearSelection()
        btnsubmit.Visible = False
        ' gvDaily.Visible = False
        'gvassets.Visible = False
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function

    'Get the first day of the month
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblmsg.Text = ""
        lblconfalert.Text = ""
        txtTdate.Attributes.Add("readonly", "readonly")
        txtFdate.Attributes.Add("readonly", "readonly")

        If Not Page.IsPostBack Then
            'hpViewDetails.Visible = False
            gvspacereport.Visible = False
            gvPanel.Visible = False
            btnView.Visible = False
            btncheck.Visible = False
            txtFdate.Text = getoffsetdate(Date.Today)
            txtTdate.Text = getoffsetdate(Date.Today)
            lblmsg.Visible = False
            LoadCity()
            BindMail()
            txtDate.Text = getoffsetdatetime(DateTime.Now).Date
        End If
        lblmsg.Text = ""
    End Sub
    Private Sub BindMail()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ALL_MAIL_IDS")
        lstInternal.DataSource = sp.GetDataSet()
        lstInternal.DataTextField = "AUR_ID"
        lstInternal.DataValueField = "AUR_EMAIL"
        lstInternal.DataBind()
    End Sub

    Public Sub LoadCity()
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub

    Public Sub bindtimeslots()
        Try
            gvspacereport.Visible = True
            gvPanel.Visible = True
            btnView.Visible = True
            lblBuilding1.Text = ddlSelectLocation.SelectedItem.Text
            lblConfName1.Text = ddlConf.SelectedItem.Text
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CONFERENCE_TIME_SLOT")
            lblHeader.Text = "Availability Status for  " + CDate(txtFdate.Text).ToString("MMMM - yyyy")
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CONF_NAME", ddlConf.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@StartDate", FirstDayOfMonth(txtFdate.Text), DbType.Date)
            sp.Command.AddParameter("@EndDate", txtTdate.Text, DbType.Date)
            Dim ds As DataSet
            ds = sp.GetDataSet
            gvspacereport.DataSource = ds
            gvspacereport.DataBind()
            'hpViewDetails.Visible = True

            Dim i As Integer = 0
            Dim j As Integer = 0
            For i = 0 To gvspacereport.Rows.Count - 1
                For j = 1 To 24
                    'If gvspacereport.Rows(i).Cells(j).Text = "1" Then
                    If gvspacereport.Rows(i).Cells(j).Text.Contains("1;") Then

                        If gvspacereport.Rows(i).Cells(0).Text >= getoffsetdate(Date.Today) Then
                            If CInt(j) <= getoffsetdatetime(DateTime.Now).Hour And gvspacereport.Rows(i).Cells(0).Text = getoffsetdate(Date.Today) Then

                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                                gvspacereport.Rows(i).Cells(j).Text = ""
                            Else
                                'gvspacereport.Rows(i).Cells(j).Text = ""
                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.Red
                                gvspacereport.Rows(i).Cells(j).ToolTip = "Booked by " & Replace(gvspacereport.Rows(i).Cells(j).Text, "1;", "")
                                gvspacereport.Rows(i).Cells(j).Text = ""

                            End If
                        Else
                            gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                            gvspacereport.Rows(i).Cells(j).Text = ""
                            'gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&FROM_TIME=" & j - 1 & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" & ddlSelectLocation.SelectedItem.Value & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CONF_CODE=" & ddlConf.SelectedValue & ">A </a>"
                        End If


                    ElseIf gvspacereport.Rows(i).Cells(j).Text = "0" Then
                        If gvspacereport.Rows(i).Cells(0).Text >= getoffsetdate(Date.Today) Then
                            If CInt(j) <= getoffsetdatetime(DateTime.Now).Hour And gvspacereport.Rows(i).Cells(0).Text = getoffsetdate(Date.Today) Then

                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                                gvspacereport.Rows(i).Cells(j).Text = ""
                            Else

                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.Green
                                gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&FROM_TIME=" & j - 1 & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" + HttpUtility.UrlEncode(ddlSelectLocation.SelectedItem.Value) & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CAPACITY=" & ddlCapacity.SelectedValue & "&CONF_CODE=" & ddlConf.SelectedValue & "><img src='../../images/chair_yellow.gif' /> </a>"
                                gvspacereport.Rows(i).Cells(j).ToolTip = "Click here to book the room between " & TimeSpan.FromHours(j - 1).ToString() & " and " & TimeSpan.FromHours(j).ToString()
                            End If
                        Else
                            gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                            gvspacereport.Rows(i).Cells(j).Text = ""
                            'gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&FROM_TIME=" & j - 1 & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" & ddlSelectLocation.SelectedItem.Value & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CONF_CODE=" & ddlConf.SelectedValue & ">A </a>"
                        End If

                        'gvspacereport.Rows(i).Cells(j).Text = ""
                        'ElseIf gvspacereport.Rows(i).Cells(j).Text = "5" Then
                    ElseIf gvspacereport.Rows(i).Cells(j).Text.Contains("5;") Then
                        If gvspacereport.Rows(i).Cells(0).Text >= getoffsetdate(Date.Today) Then
                            If CInt(j) <= getoffsetdatetime(DateTime.Now).Hour And gvspacereport.Rows(i).Cells(0).Text = getoffsetdate(Date.Today) Then

                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                                gvspacereport.Rows(i).Cells(j).Text = ""
                            Else
                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.Black
                                'gvspacereport.Rows(i).Cells(j).Text = ""
                                gvspacereport.Rows(i).Cells(j).ToolTip = "Withhold by " & Replace(gvspacereport.Rows(i).Cells(j).Text, "5;", "")
                                gvspacereport.Rows(i).Cells(j).Text = ""

                            End If
                        Else
                            gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                            gvspacereport.Rows(i).Cells(j).Text = ""
                            'gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&FROM_TIME=" & j - 1 & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" & ddlSelectLocation.SelectedItem.Value & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CONF_CODE=" & ddlConf.SelectedValue & ">A </a>"
                        End If
                    End If
                Next
            Next
            For i = 0 To gvspacereport.Rows.Count - 1
                gvspacereport.Rows(i).Cells(0).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" + HttpUtility.UrlEncode(ddlSelectLocation.SelectedItem.Value) & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CAPACITY=" & ddlCapacity.SelectedValue & "&CONF_CODE=" & ddlConf.SelectedValue & ">" + gvspacereport.Rows(i).Cells(0).Text + "</a>"

            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub gvspacereport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Cells(2).Attributes.Add("onclick", String.Format("document.getElementById('{0}').innerHTML='{1}';", e.Row.Cells(1).ClientID, "H"))
    '        e.Row.Cells(3).Attributes.Add("onclick", String.Format("document.getElementById('{0}').innerHTML='{1}';", e.Row.Cells(1).ClientID, "A"))
    '        e.Row.Cells(4).Attributes.Add("onclick", String.Format("document.getElementById('{0}').innerHTML='{1}';", e.Row.Cells(1).ClientID, "P"))
    '    End If
    'End Sub

    'Protected Sub gvspacereport_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowCreated
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Cells[0].Visible = false 
    '        e.Row.Cells(0).Visible = False
    '    End If
    'End Sub

    'Protected Sub gvspacereport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowDataBound


    'End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged

        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCapacity.Items.Clear()
        ddlConf.Items.Clear()
        gvItem.Visible = False
        'txtFdate.Text = String.Empty
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSelectLocation.SelectedIndexChanged
        Try
            'usp_getActiveTower_LOC
            ddlTower.Items.Clear()
            ddlFloor.Items.Clear()
            ddlCapacity.Items.Clear()
            ddlConf.Items.Clear()
            gvItem.Visible = False
            'txtFdate.Text = String.Empty
            If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
                ' objMsater.BindTowerLoc(ddlTower, ddlSelectLocation.SelectedItem.Value)
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
                param(0).Value = ddlSelectLocation.SelectedItem.Value
                ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)


                'Else
                '    ddlTower.Items.Clear()
                '    ddlFloor.Items.Clear()
                '    ddlTower.Items.Add("--Select--")
                '    ddlFloor.Items.Add("--Select--")
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            ddlFloor.Items.Clear()
            ddlCapacity.Items.Clear()
            ddlConf.Items.Clear()
            gvItem.Visible = False
            'txtFdate.Text = String.Empty
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "--Select--")
                'Else
                '    ddlFloor.Items.Clear()
                '    ddlFloor.Items.Add("--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")
        'txtFdate.Text = String.Empty

    End Sub

    Protected Sub ddlCapacity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCapacity.SelectedIndexChanged
        'GET_SPACE_CONFERENCE

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)

        If ddlCapacity.SelectedItem.Value <> "--Select--" Then
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
            'txtFdate.Text = String.Empty
        Else
            ddlConf.Items.Clear()
        End If

    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        Panel1.Visible = True
        gvspacereport.Visible = False
        btnView.Visible = False
        btnsubmit.Visible = True
        gvPanel.Visible = False

    End Sub

    Protected Sub gvspacereport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowDataBound
        e.Row.Cells(25).Visible = False
    End Sub

    Protected Sub rbActions_CheckedChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsrecurring.CheckedChanged
        If rbActionsrecurring.Checked = True Then
            btnsubmit.Visible = False
            btncheck.Visible = True
            Tr1.Visible = True
            rectimes.Visible = True
            AttendeesMail.Visible = True
            InternalAttendees.Visible = True
            gvItem.Visible = False
            txtFdate.Text = getoffsetdate(Date.Today)
            txtTdate.Text = getoffsetdate(Date.Today)
            ddlstarttime.ClearSelection()
            ddlendtime.ClearSelection()

        Else
            Tr1.Visible = False
            rectimes.Visible = False
            AttendeesMail.Visible = False
            InternalAttendees.Visible = False
            btnsubmit.Visible = True
            btncheck.Visible = False
            gvItem.Visible = False
            ddlstarttime.ClearSelection()
            ddlendtime.ClearSelection()
        End If
    End Sub

    Private Sub BindGrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONFERENCE_BOOKED_SLOTS")
        sp.Command.AddParameter("@REQ_ID", "", DbType.String)
        sp.Command.AddParameter("@SPC_ID", ddlConf.SelectedValue, DbType.String)
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", ddlstarttime.SelectedValue, DbType.String)
        sp.Command.AddParameter("@TO_TIME", ddlendtime.SelectedValue, DbType.String)

        'sp.Command.AddParameter("@S", 1, DbType.Int16)
        'sp.Command.AddParameter("@E", 10, DbType.Int16)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()

        If (ds.Tables(0).Rows.Count > 0) Then
            gvItem.Visible = True
            lblconfalert.Visible = True
            lblconfalert.Text = "Note: selected time interval is already booked, please select other available slots."

        ElseIf (txtFdate.Text >= getoffsetdate(Date.Today).Date) And (txtTdate.Text > txtFdate.Text) Then
            'If ddlstarttime.SelectedValue < ddlendtime.SelectedValue Or (ddlstarttime.SelectedValue > ddlendtime.SelectedValue And ddlstarttime.SelectedValue = "23") Then

            Dim cntWst As Integer = 0
            Dim cntHCB As Integer = 0
            Dim cntFCB As Integer = 0
            Dim twr As String = ""
            Dim flr As String = ""
            Dim wng As String = ""
            Dim intCount As Int16 = 0
            Dim ftime As String = ""
            Dim ttime As String = ""
            Dim StartHr As String = "00"
            Dim EndHr As String = "00"
            Dim StartMM As String = "00"
            Dim EndMM As String = "00"

            If ddlstarttime.SelectedItem.Value <> "Hr" Then
                StartHr = ddlstarttime.SelectedItem.Text
            End If

            If ddlendtime.SelectedItem.Value <> "Hr" Then
                If ddlendtime.SelectedItem.Value = "24" Then
                    EndHr = "00"
                Else
                    EndHr = ddlendtime.SelectedItem.Text
                End If

            End If

            'If StartHr = "23" And EndHr <> "00" Then
            '    lblmsg.Text = "To date Should not be more than 00 hrs"
            '    Exit Sub
            'End If

            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM

            Dim param(7) As SqlParameter
            param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
            param(0).Value = ddlSelectLocation.SelectedItem.Value
            param(1) = New SqlParameter("@TOWER", SqlDbType.NVarChar, 200)
            param(1).Value = ddlTower.SelectedItem.Value
            param(2) = New SqlParameter("@FLOOR", SqlDbType.NVarChar, 200)
            param(2).Value = ddlFloor.SelectedItem.Value
            param(3) = New SqlParameter("@FROM_TIME", SqlDbType.DateTime)
            param(3).Value = ftime
            param(4) = New SqlParameter("@TO_TIME", SqlDbType.DateTime)
            param(4).Value = ttime
            param(5) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
            param(5).Value = Convert.ToDateTime(txtFdate.Text)
            param(6) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
            param(6).Value = Convert.ToDateTime(txtTdate.Text)
            param(7) = New SqlParameter("@CONF_CODE", SqlDbType.DateTime)
            param(7).Value = ddlConf.SelectedValue

            'ObjSubSonic.Binddropdown(ddlConference, "usp_get_CONFERENCE_Seats_PART1", "spc_name", "spc_id", param)
            'confroom.Visible = True

            'If Request.QueryString("mode") = 2 Then
            '    emp.Visible = True
            'Else
            '    emp.Visible = False
            'End If
            If CDate(txtFdate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtFdate.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
                lblmsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                Exit Sub
            End If

            SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 2)
            lblmsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
            cleardata()



            'Else
            '    lblMsg.Text = "Please enter employee code."
            'End If
            'Else
            '    lblmsg.Visible = True
            '    lblmsg.Text = "Start Time Must Be Less Than End Time"
            'End If
        Else
            lblmsg.Visible = True
            lblmsg.Text = "To Date Must Be Greater than From date"
        End If

    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btncheck_Click(sender As Object, e As EventArgs) Handles btncheck.Click
        gvItem.Visible = False
        Try
            Dim intFromDate As Date = CType(txtFdate.Text, Date)
        Catch ex As Exception
            lblmsg.Text = "Please enter vaild From Date "
            lblmsg.Visible = True
            Exit Sub
        End Try

        Try
            Dim intToDate As Date = CType(txtTdate.Text, Date)
        Catch ex As Exception
            lblmsg.Text = "Please enter vaild To Date "
            lblmsg.Visible = True
            Exit Sub
        End Try
        If CDate(txtTdate.Text) <= CDate(txtFdate.Text) Then
            lblmsg.Text = "To date should be greater than from date "
            lblmsg.Visible = True
            Exit Sub
        ElseIf CDate(txtFdate.Text) < CDate(txtDate.Text) Then
            lblmsg.Text = " Please enter valid from date that has to be greater than today's date "
            lblmsg.Visible = True
            Exit Sub
        End If

        Dim InternalCount As Integer = 0
        Dim InternalAttendees As String = ""
        For Each li As ListItem In lstInternal.Items
            If li.Selected = True Then
                InternalCount = 1
            End If
        Next

        If txtAttendees.Text = "" And InternalCount = 0 Then
            lblmsg.Text = "Please enter or select at least one Attendees Email"
            lblmsg.Visible = True
            Exit Sub
        End If

        BindGrid()

    End Sub

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)

        REQID = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "SVR_ID", Session("TENANT") & ".", "SMS_VERTICAL_REQUISITION")
        Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If ddlstarttime.SelectedItem.Value <> "Hr" Then
            StartHr = ddlstarttime.SelectedItem.Text
        End If
        'If starttimemin.SelectedItem.Value <> "Min" Then
        '    StartMM = starttimemin.SelectedItem.Text
        'End If
        If ddlendtime.SelectedItem.Value <> "Hr" Then
            If ddlendtime.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = ddlendtime.SelectedItem.Text
            End If
        End If

        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM

        Dim sta As Integer = status

        RIDDS = RIDGENARATION("VerticalReq")

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubSonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblmsg.Text = "Request is already raised "
            Exit Sub
        Else


            cntWst = 1

            Dim param2(15) As SqlParameter

            param2(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param2(0).Value = REQID
            param2(1) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param2(1).Value = strVerticalCode
            param2(2) = New SqlParameter("@WSTREQCOUNT", SqlDbType.Int)
            param2(2).Value = cntWst
            param2(3) = New SqlParameter("@FCREQCOUNT", SqlDbType.Int)
            param2(3).Value = cntFCB
            param2(4) = New SqlParameter("@HCREQCOUNT", SqlDbType.Int)
            param2(4).Value = cntHCB
            param2(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param2(5).Value = txtFdate.Text
            param2(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param2(6).Value = txtTdate.Text
            param2(7) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param2(7).Value = ftime
            param2(8) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param2(8).Value = ttime
            param2(9) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
            param2(9).Value = strAurId
            param2(10) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 50)
            param2(10).Value = ddlSelectLocation.SelectedItem.Value 'GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
            param2(11) = New SqlParameter("@CITY", SqlDbType.NVarChar, 50)
            param2(11).Value = ddlCity.SelectedValue
            param2(12) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param2(12).Value = "2" 'GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param2(13) = New SqlParameter("@BCPTYPE", SqlDbType.NVarChar, 50)
            param2(13).Value = "2"
            param2(14) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param2(14).Value = sta
            param2(15) = New SqlParameter("@REMARKS", SqlDbType.NVarChar, 200)
            param2(15).Value = remarks
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART1", param2)
            Dim cnt As Int32 = 0
            ' For Each row As GridViewRow In gdavail.Rows
            Dim lblspcid As String   ' DirectCast(row.FindControl("lblspcid"), Label)
            lblspcid = ddlConf.SelectedItem.Value
            'Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblmsg.Text = ""
            ' If chkSelect.Checked = True Then
            verticalreqid = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "ssa_id", Session("TENANT") & ".", "SMS_space_allocation")
            Conf_REQ_ID = verticalreqid

            Dim param3(10) As SqlParameter

            param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param3(0).Value = verticalreqid
            param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
            param3(1).Value = REQID
            param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param3(2).Value = strVerticalCode

            param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
            param3(3).Value = lblspcid
            param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
            param3(4).Value = strAurId
            param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param3(5).Value = txtFdate.Text
            param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param3(6).Value = txtTdate.Text
            param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param3(7).Value = "2" ' GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param3(8).Value = ftime
            param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param3(9).Value = ttime
            param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param3(10).Value = sta
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART3", param3)
            'Addusers.Occupiedspace(Trim(strAurId), LTrim(RTrim(lblspcid)))
            ' UpdateRecord(LTrim(RTrim(lblspcid)), sta, Trim(strAurId) & "/" & lblDepartment.Text)
            'End If
            'Next
            '-------------------SEND MAIL-----------------

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_CONF_BOOKING_REQUEST")
            sp.Command.AddParameter("@REQID", verticalreqid, DbType.String)
            sp.ExecuteScalar()
        End If

        strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("2") & "&rid=" & clsSecurity.Encrypt(verticalreqid)
GVColor:
        Try
            BookEventOnGoogleCalendar()
        Catch ex As Exception
            Throw ex
        End Try

        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect, False)
        End If


    End Sub

    Private Function BookEventOnGoogleCalendar() As Boolean
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        If ddlstarttime.SelectedItem.Value <> "Hr" Then
            StartHr = ddlstarttime.SelectedItem.Text
        End If
        'If starttimemin.SelectedItem.Value <> "Min" Then
        '    StartMM = starttimemin.SelectedItem.Text
        'End If
        If ddlendtime.SelectedItem.Value <> "Hr" Then
            If ddlendtime.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = ddlendtime.SelectedItem.Text
            End If
        End If

        If EndHr = "00" And StartHr = "23" Then
            EndHr = "23:59"
        End If

        ftime = txtFdate.Text + " " + StartHr + ":" + StartMM
        ttime = txtFdate.Text + " " + EndHr + ":" + EndMM

        Dim RecCount As Integer
        Dim FromDate As DateTime = txtFdate.Text
        Dim ToDate As DateTime = txtTdate.Text
        RecCount = (ToDate - FromDate).TotalDays
        RecCount = RecCount + 1

        Dim calendarservice As New clsCalendarService
        With calendarservice
            .Summary = txtDescription.Text
            .Location = ddlSelectLocation.SelectedItem.Text + "," + ddlTower.SelectedItem.Text + "," + ddlFloor.SelectedItem.Text + "," + ddlConf.SelectedItem.Text
            .Description = txtDescription.Text
            .Start = New EventDateTime
            With calendarservice.Start
                .DateTime = ftime
                '.TimeZone = "Asia/Kolkata"
                .TimeZone = Session("useroffset")
            End With

            .End = New EventDateTime
            With .End
                .DateTime = ttime
                .TimeZone = Session("useroffset")
            End With

            Dim ndx As Integer = 0

            Dim attendees() = txtAttendees.Text.Split(",")
            .Attendees = New EventAttendee(attendees.Length - 1) {}
            For i = 0 To attendees.Length - 1
                .Attendees(ndx) = New EventAttendee
                With .Attendees(ndx)
                    .Email = (attendees(ndx)).Trim()
                End With
                ndx = ndx + 1
            Next
            For i = 0 To lstInternal.Items.Count - 1
                If lstInternal.Items(i).Selected Then
                    ReDim Preserve .Attendees(ndx + 1)
                    .Attendees(ndx) = New EventAttendee
                    With .Attendees(ndx)
                        .Email = lstInternal.Items(i).Value
                    End With
                    ndx = ndx + 1
                End If
            Next
            .Recurrence = New String() {"RRULE:FREQ=DAILY;COUNT=" + RecCount.ToString()}
            .Reminders = New Google.Apis.Calendar.v3.Data.Event.RemindersData()
            With .Reminders
                .UseDefault = False
                .Overrides = New EventReminder() {
                    New EventReminder() With {.Method = "email", .Minutes = 24 * 60},
                    New EventReminder() With {.Method = "popup", .Minutes = 10}
                }
            End With
        End With
        'Return calendarservice.PushEvent()

        Dim Event_ID As String = calendarservice.PushEvent()

        Dim InternalAttendees As String = ""
        For Each li As ListItem In lstInternal.Items
            If li.Selected = True Then
                InternalAttendees = InternalAttendees + li.Value + ","
            End If
        Next

        InternalAttendees = InternalAttendees.TrimEnd(",")

        Dim param_Event(6) As SqlParameter
        param_Event(0) = New SqlParameter("@CONF_REQ_ID", SqlDbType.NVarChar, 1000)
        param_Event(0).Value = Conf_REQ_ID
        param_Event(1) = New SqlParameter("@CONF_EVENT_ID", SqlDbType.NVarChar, 1000)
        param_Event(1).Value = Event_ID
        param_Event(2) = New SqlParameter("@CONF_EVENT_STATUS", SqlDbType.Int)
        param_Event(2).Value = 1
        param_Event(3) = New SqlParameter("@CONF_DESCRIPTION", SqlDbType.NVarChar, 1000)
        param_Event(3).Value = txtDescription.Text
        param_Event(4) = New SqlParameter("@CREATED_BY", SqlDbType.NVarChar, 100)
        param_Event(4).Value = Session("UID")
        param_Event(5) = New SqlParameter("@EXTERNAL_ATTENDEES", txtAttendees.Text)
        param_Event(6) = New SqlParameter("@INTERNAL_ATTENDEES", InternalAttendees)
        ObjSubSonic.GetSubSonicExecute("CONF_GOOGLE_NEW_EVENT_CREATE", param_Event)

    End Function

End Class
