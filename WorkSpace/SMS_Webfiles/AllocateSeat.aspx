<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AllocateSeat.aspx.vb" Inherits="WorkSpace_GIS_AllocateSeat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Space Allocation</title>

    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <script language="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">  
             <hr align="center" width="60%" />Space Allocation</asp:Label></td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
            <tr>
                <td align="left" width="100%" colspan="3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                        width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong></strong>
                </td>
                <td style="width: 16px">
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                        width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;</td>
                <td align="left">
                    <div>
                        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%">
                            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                                <tr>
                                    <td align="left">
                                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="clsMessage"
                                            ForeColor="" />
                                        <br />
                                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                                        <br />
                                        <table id="table4" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                                            border="1">
                                            <tr id="Tr3" runat="server">
                                                <td align="left" class="clslabel" style="width: 25%; height: 17px">
                                                    From Date<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                                        Display="None" ErrorMessage="Please Enter From Date ">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                                        Display="None" ErrorMessage="Please Enter Valid From Date " Operator="DataTypeCheck"
                                                        Type="Date">
                                                    </asp:CompareValidator></td>
                                                <td style="width: 25%; height: 17px">
                                                    <asp:TextBox ID="txtFrmDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                                                    </asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 25%; height: 17px">
                                                    To Date<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvtodate" runat="server" ControlToValidate="txtToDate"
                                                        Display="None" ErrorMessage="Please Enter To Date ">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtToDate"
                                                        Display="None" ErrorMessage="Please Enter Valid To Date " Operator="DataTypeCheck"
                                                        Type="Date">
                                                    </asp:CompareValidator></td>
                                                <td style="height: 17px">
                                                    <asp:TextBox ID="txtToDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trTimeSlot" runat="server" visible="false">
                                                <td>
                                                    From:<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr"
                                                        Display="None" ErrorMessage="Please Enter From Time " InitialValue="Hr">
                                                    </asp:RequiredFieldValidator>
                                                    <font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimemin"
                                                        Display="None" ErrorMessage="Please Enter From Minutes" InitialValue="Min">
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="starttimehr" runat="server" Width="60">
                                                        <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="starttimemin" runat="server" Width="60">
                                                        <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    To:<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr"
                                                        Display="None" ErrorMessage="Please Enter To Time " InitialValue="Hr">
                                                    </asp:RequiredFieldValidator>
                                                    <font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimemin"
                                                        Display="None" ErrorMessage="Please Enter To Minutes" InitialValue="Min">
                                                    </asp:RequiredFieldValidator></td>
                                                <td>
                                                    <asp:DropDownList ID="endtimehr" runat="server" Width="60">
                                                        <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="endtimemin" runat="server" Width="60">
                                                        <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td>
                                                    Enter Employee Code
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmployeeCode" runat="server"></asp:TextBox></td>
                                                <td>
                                                    <asp:Button ID="btnallocate" runat="server" CssClass="clsButton" Text="Allocate Now" /></td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr runat="Server" id="empdetails" visible="false">
                                                <td colspan="4">
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Employee Id
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblEmpSearchId" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Employee Name
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblEmpName" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Email Id
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblEmpEmailId" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Reporting To
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblReportingTo" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Designation
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblDesignation" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Department
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblDepartment" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Employee Extension
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblEmpExt" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Contact Number
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblResNumber" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                City
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblCity" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                State
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblState" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Project
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblPrjCode" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Vertical
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblAUR_VERT_CODE" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Building
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblAUR_BDG_ID" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Tower
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblEmpTower" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Floor
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblEmpFloor" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Date of Joining
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:Label ID="lblDoj" runat="server" CssClass="bodytext"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left" class="bodytext">
                                                                Space
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <asp:HyperLink ID="hypSpaceId" runat="server" CssClass="BODYTEXT"></asp:HyperLink>
                                                            </td>
                                                            <td valign="top" align="left" class="bodytext">
                                                                <asp:Label ID="lblSpaceAllocated" runat="server" CssClass="bodytext" Font-Bold="True"
                                                                    Font-Names="Arial" Font-Size="Medium" Font-Underline="True" ForeColor="Red"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="Center" colspan="4">
                                                                &nbsp;<asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr runat="server" visible="false">
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gdavail" runat="server" AutoGenerateColumns="False" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" Checked="true" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SPC_BDG_ID" HeaderText="Location Name" SortExpression="SPC_BDG_ID" />
                                                <asp:TemplateField HeaderText="Spaces Available">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblspcid" runat="server" Text='<%# Eval("spc_id").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Space Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSpacelyr" runat="server" Text='<%# Eval("spc_layer").ToString()%>'>  </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Seat Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSpaceType" runat="server" Text='<%# Eval("SPACE_TYPE").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Carved status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcarved" runat="server" Text='<%# Eval("SPACE_CARVED").ToString()%>'>  </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Port Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblportstatus" runat="server" Text='<%# Eval("SPACE_PORT_STATUS").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Space Port Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblporttype" runat="server" Text='<%# Eval("SPACE_PORT_TYPE").ToString()%>'>  </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="BCP Seat Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblbcpseattype" runat="server" Text='<%# Eval("SPACE_BCP_SEAT_TYPE").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MCP ">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmcp" runat="server" Text='<%# Eval("SPACE_MCP_SEAT").ToString()%>'>  </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PORT Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblportnumber" runat="server" Text='<%# Eval("SPACE_PORT_NUMBER").ToString()%>'>  </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Area Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblareatype" runat="server" Text='<%# Eval("SPACE_AREA_TYPE").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="height: 17px;" colspan="2" align="center">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 16px;
                    height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" /></td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" /></td>
                <td style="height: 17px; width: 16px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
        <!--#INCLUDE File=../../includes/modal_divs.aspx-->
    </form>
</body>
</html>
