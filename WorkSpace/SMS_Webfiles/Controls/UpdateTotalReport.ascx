<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateTotalReport.ascx.vb"
    Inherits="WorkSpace_SMS_Webfiles_Controls_UpdateTotalReport" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black"> Update Total Report 
                    </asp:Label>
                    <hr align="center" width="60%" />
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                        width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong> Update Total Report </strong>
                </td>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                        width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;</td>
                <td align="left">
                    <table id="table1" cellspacing="2" cellpadding="2" width="100%" border="1" style="border-collapse: collapse">
                        <tr>
                            <td valign="top" align="left" class="bodytext" colspan="2">
                                <asp:Label ID="lblmsg" ForeColor="RED" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                                Select City
                            </td>
                            <td valign="top" align="left">
                                <asp:DropDownList ID="ddlCity" CssClass="bodytext" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                                Select Location
                            </td>
                            <td valign="top" align="left">
                                <asp:DropDownList ID="ddlLocation" CssClass="bodytext" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                                Select Loc Type
                            </td>
                            <td valign="top" align="left">
                                <asp:DropDownList ID="ddlLocType" CssClass="bodytext" AutoPostBack="True" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                                Select Tower
                            </td>
                            <td valign="top" align="left">
                                <asp:DropDownList ID="ddlTower" AutoPostBack="True" CssClass="bodytext" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                                Select Department
                            </td>
                            <td valign="top" align="left">
                                <asp:DropDownList ID="ddlDepartment" AutoPostBack="true" CssClass="bodytext" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                                Total Available
                            </td>
                            <td valign="top" align="left">
                                <asp:TextBox ID="txtTot_Available" CssClass="bodytext" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                                Total Occupied
                            </td>
                            <td valign="top" align="left">
                                <asp:TextBox ID="txtTot_Occupied" CssClass="bodytext" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" class="bodytext">
                            </td>
                            <td valign="top" align="left">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="clsButton" Text="Submit" OnClick="btnSubmit_Click" />
                                &nbsp;
                                <asp:Button ID="btnDelete" runat="server" CssClass="clsButton" Text="Delete" OnClick="btnDelete_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                    height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" /></td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
