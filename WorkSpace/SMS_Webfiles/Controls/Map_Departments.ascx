<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Map_Departments.ascx.vb"
    Inherits="WorkSpace_GIS_Controls_Map_Departments" %>
<table class="TABLE" cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
    <tr> 
        <td>
            <table class="table" cellspacing="0" cellpadding="0" align="center" border="0">
                <tr align="left">
                    <td bgcolor="#4863A0" colspan="2" style="padding-left: 15px;" height="15">
                        <asp:LinkButton ID="LinkButton1" runat="server" Style="color: Blue; font-weight: bold;">Departments</asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <div style="height: 10em; width: 400; overflow: scroll; border: solid gray 1px; padding: 0.2em 0.2em;">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="0"
                    ForeColor="#333333" ShowFooter="false" GridLines="None" Width="100%">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <ItemTemplate>
                                <table border="0">
                                    <tr>
                                        <td align="left" nowrap="NOWRAP" valign="top">
                                             
                                            <asp:LinkButton ID="lnkDept" runat="server" CommandName="select" CommandArgument='<%#Eval("cost_center_code").ToString()%>' Text='<%#Eval("DEP_NAME").ToString()%>'></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No. of Seats">
                            <ItemTemplate>
                                <table border="0">
                                    <tr>
                                        <td align="left" valign="top">
                                            <%--TOTAL_TOTAL----%>
                                            <%#Eval("total_seats").ToString()%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <table border="0">
                                    <tr>
                                        <td align="left" valign="top">
                                            <%--TOTAL_TOTAL----%>
                                            <%#Eval("STATUS").ToString()%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left"
                        VerticalAlign="Top" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
            </div>
        </td>
    </tr>
</table>
