<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SelectLocation.ascx.vb"
    Inherits="WorkSpace_GIS_Controls_SelectLocation" %>

<script language="javascript" type="text/javascript">
		
			function submitall()
			{		
						    	
				var flm=document.form1.drpdwnFlm_Id.value;
				var tlr=document.form1.drpdwnTwm_Id.value;
//				document.Form1.target="DisplayFloors";
//				document.Form1.action="DisFloors.aspx?FLR_ID="+flm;
//				document.Form1.submit();
				
				document.form1.target="SelectFloors";
				document.form1.action="SelFloors.aspx?FLR_CODE="+flm;
				document.form1.submit();
								 //alert(parent.hidden.document.f1.User.value);
				if (parent.hidden.document.f1.User.value=="Admin")
				{
				
					document.form1.target="main";
					document.form1.action="ViewMap_Admin.aspx?FLR_CODE="+flm;
					document.form1.submit();
					
//					document.form1.target="Reports";
//					document.form1.action="Reports2.aspx?FLR_CODE="+flm;
//					document.form1.submit();
					
					document.form1.target="maptools";
					document.form1.action="maptools_admin.aspx?FLR_CODE="+flm;
					document.form1.submit();
					
					document.form1.target="query";
					document.form1.action="query.aspx?FLR_CODE="+flm;
					document.form1.submit();
					
					top.Themes.location = "Themes.htm";
					top.Themes_Show.location = "Themes_Hide.htm";
				}
				else if (parent.hidden.document.f1.User.value=="Edit")
				{
					
					document.form1.target="main";
					document.form1.action="ViewMap_Edit.aspx?FLR_CODE="+flm;
					document.form1.submit();
					
					document.form1.target="maptools";
					document.form1.action="maptools_edit.aspx?FLR_CODE="+flm;
					document.form1.submit();

					document.form1.target="Line";
					document.form1.action="Line.aspx?FLR_CODE="+flm;
					document.form1.submit();					
					
					document.form1.target="Point";
					document.form1.action="Point.aspx?FLR_CODE="+flm;
					document.form1.submit();	
					
					document.form1.target="Polygon";
					document.form1.action="Polygon.aspx?FLR_CODE="+flm;
					document.form1.submit();									
					
					document.form1.target="Asset";
					document.form1.action="Asset.aspx?FLR_ID="+flm;
					document.form1.submit();
				}
				else
				{
				    
					document.form1.target="main";
					document.form1.action="ViewMap_User.aspx?FLR_CODE="+flm;
					document.form1.submit();
					
					document.form1.target="query";
					document.form1.action="query.aspx?FLR_CODE="+flm;
					document.form1.submit();
					
					 
			
					document.form1.target="Departments";
					 document.form1.action="Map_Departments.aspx?FLR_CODE="+flm+"&TWR_CODE="+tlr;
					 document.form1.submit();

                    document.form1.target="Reports2";
					document.form1.action="Reports2.aspx?FLR_CODE="+flm+"&TWR_CODE="+tlr;
					document.form1.submit();
					
					
										
				}	
				
			}				
			
</script>

<asp:TextBox ID="txtFlr" Style="z-index: 109; left: 525px; position: absolute; top: 328px"
    runat="server" Height="23px" Visible="false" Width="125px"></asp:TextBox>
<table class="table" cellspacing="0" width="100%" cellpadding="0" align="center" border="0">
    <tr align="left">
        <td bgcolor="#4863A0" colspan="2" style="padding-left: 15px;" height="15">
            <b><span style="color: white">Select</span></b></td>
    </tr>
</table>
<table cellspacing="0" cellpadding="2"   width="100%" align="center" border="0">
    <tr>
        <td style="height: 15px">
            Location</td>
        <td style="height: 15px" align="center">
            <asp:DropDownList ID="drpdwnLocation" runat="server" Width="151px" CssClass="clsComboBox"
                AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td style="height: 15px">
            Tower</td>
        <td style="height: 15px" align="center">
            <asp:DropDownList ID="drpdwnTwm_Id" runat="server" Width="151px" CssClass="clsComboBox"
                AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td>
            Floor</td>
        <td align="center">
            <asp:DropDownList ID="drpdwnFlm_Id" runat="server" Width="150px" CssClass="clsComboBox">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <table cellspacing="0" cellpadding="2" width="150" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="cursor: hand" align="center">
                        
                          <asp:ImageButton ID="imgbtnView" runat="server" Height="16px" Width="41px" ImageUrl="~/WorkSpace/GIS/Images/VIEW.jpg">
                            </asp:ImageButton>
                            <%--<a href="#">
                                <img alt="" onclick="submitall()" src="<%=Page.ResolveUrl("~/WorkSpace/GIS/Images/VIEW.JPG")%>"
                                    border="0" id="IMG1" /></a>--%></td>
                        <% 
                            If txtMode.Text = "EDIT" Then%>
                        <td style="cursor: hand" align="center">
                            <a href="#">
                                <img alt="" onclick="doGetKey()" src="<%=Page.ResolveUrl("~/WorkSpace/GIS/Images/getid1.JPG")%>"
                                    border="0" /></a>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="16px" Width="41px" ImageUrl="~/WorkSpace/GIS/Images/submit.jpg">
                            </asp:ImageButton></td>
                        <% End If%>
                    </tr>
                </tbody>
            </table>
        </td>
        <td align="center">
        </td>
    </tr>
</table>
&nbsp;&nbsp; &nbsp;&nbsp;
<input id="TxtKey1" type="text" size="50" visible="false" name="TxtKey1" runat="server" />
<asp:TextBox ID="txtMode" runat="server" Height="19px" Width="185px" Visible="False"></asp:TextBox>
