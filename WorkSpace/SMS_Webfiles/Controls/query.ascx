<%@ Control Language="VB" AutoEventWireup="false" CodeFile="query.ascx.vb" Inherits="WorkSpace_GIS_Controls_query" %>
<table class="table" cellspacing="0" cellpadding="0"  width="100%" align="center" border="0">
    <tr align="left">
        <td bgcolor="#4863A0" colspan="2" style="padding-left: 15px;" height="15">
            <b><span style="color: white">Query Analyzer</span></b></td>
    </tr>
</table>
<table cellspacing="0" cellpadding="2" align="center" width="100%" border="0">
    <tr>
        <td>
            Field</td>
        <td align="center">
            <asp:DropDownList ID="drpdwnCategory" runat="server" CssClass="clsComboBox" AutoPostBack="True"
                Height="20px" Width="150px">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td>
            Operator</td>
        <td align="center">
            <asp:DropDownList ID="drpdwnOpt" runat="server" CssClass="clsComboBox" AutoPostBack="True"
                Height="20px" Width="150px">
                <asp:ListItem Value="=" Selected="True">=</asp:ListItem>
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td style="height: 15px">
            Value</td>
        <td style="height: 15px" align="center">
            <asp:DropDownList ID="drpdwnValue" runat="server" CssClass="clsComboBox" AutoPostBack="True"
                Height="20px" Width="150px">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:ImageButton ID="ImgFetch" runat="server" ImageUrl='~/WorkSpace/GIS/Images/Search.jpg'>
            </asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:ImageButton ID="ImgZoom" runat="server" Visible="false" ImageUrl='<%=Page.ResolveUrl("~/WorkSpace/GIS/Images/Zoom.jpg")%>'>
            </asp:ImageButton></td>
    </tr>
</table>
