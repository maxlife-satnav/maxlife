<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SearchEmployee.ascx.vb"
    Inherits="WorkSpace_SMS_Webfiles_Controls_SearchEmployee" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Search Type <span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:RadioButtonList ID="radSearch" AutoPostBack="true" runat="server">
                        <asp:ListItem Text="Employee Id" Value="0" Enabled="true" Selected="True">
                        </asp:ListItem>
                        <asp:ListItem Text="Employee Name" Value="1" Enabled="true" Selected="false">
                        </asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="pnlempid" runat="server" visible="true">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Employee Id<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="frvEmpSearch" runat="server" ControlToValidate="txtEmpId"
                        Display="None" ErrorMessage="Please Enter Employee Id to Search " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="pnlEmailid" runat="server" visible="false">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Employee name<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmpName"
                        Display="None" ErrorMessage="Please Enter Employee Name to Search " ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtEmpName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <div class="row">
                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-md-12">
         <div class="form-group">
        <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" PageSize="1" EmptyDataText="No Employee Found." ShowHeaderWhenEmpty="True"
            CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblLoc" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_BDG_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tower" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblTwr" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_TWR_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Floor" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <asp:Label ID="lblFlr" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_FLR_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Space Id">
                    <ItemTemplate>
                        <asp:Label ID="lblSpaceId" runat="server" CssClass="bodyText" Visible="false" Text='<%#Eval("AUR_LAST_NAME")%>'></asp:Label>
                        <a href="#" onclick="showPopWin('<%#Eval("AUR_LAST_NAME")%>','<%#Eval("aur_id")%>')"><%#Eval("AUR_LAST_NAME")%></a>
                        <%--<a href="#" onclick="showPopWin('SpacesViewMapSearchEmployee.aspx?spcid=<%#Eval("AUR_LAST_NAME")%>&aur_id=<%#Eval("aur_id")%>',1100,600,null)">
                            <%#Eval("AUR_LAST_NAME")%></a>--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Space Type">
                    <ItemTemplate>
                        <asp:Label ID="lblSpcType" runat="server" CssClass="bodyText" Text='<%#Eval("SPC_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Name" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpName" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Extension Number" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblExtension" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_EXTENSION")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div></div>
</div>
<div class="modal fade" id="myModal" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title"><label id="lblModalHead" class="control-label"></label></h4>
            </div>
            <div class="modal-body" id="modelcontainer">
                <%-- Content loads here --%>
                <iframe id="modalcontentframe" src="#" width="100%" height="520px" style="border: none"></iframe>
            </div>
        </div>
    </div>
</div>

<script>
    function showPopWin(spcid, aurid) {
        //$("#modelcontainer").load("SpacesViewMapSearchEmployee.aspx?spcid=" + spcid + "&aur_id=" + aurid, function (responseTxt, statusTxt, xhr) {
        //    $("#myModal").modal().fadeIn();
        //});
        $("#modalcontentframe").attr("src", "SpacesViewMapSearchEmployee.aspx?spcid=" + spcid + "&aur_id=" + aurid);
        $("#myModal").modal().fadeIn();
        return false;
    }
</script>
