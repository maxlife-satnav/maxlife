<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AnalysisRpt.ascx.vb"
    Inherits="WorkSpace_SMS_Webfiles_Controls_AnalysisRpt" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Analysis Report
                    </asp:Label>
                    <hr align="center" width="60%" />
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                        width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Analysis Report</strong>
                </td>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                        width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;</td>
                <td align="left">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 238px" class="clslabel" align="center" colspan="2">
                                <asp:Label ID="lblmsg" CssClass="error" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 238px" class="clslabel">
                                Select Vertical :</td>
                            <td>
                                <asp:DropDownList ID="ddlVertical" CssClass="clsComboBox" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 238px" class="clslabel">
                                No. of Seats :</td>
                            <td>
                                <asp:TextBox ID="txtNoofSeats" runat="server" CssClass="bodytext"></asp:TextBox>
                                <asp:Button ID="btnSubmit" CssClass="clsButton" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                        <tr id="trVertOcc" runat="server" visible="false">
                            <td style="width: 238px" class="clslabel" align="center" colspan="2">
                                <fieldset>
                                    <legend>Vertical Occupied Summary </legend>
                                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                                <asp:Label ID="lblTotalWST" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                                <asp:Label ID="lblTotalHCB" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                                <asp:Label ID="lblTotalFCB" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr id="trVacantSummary" runat="server" visible="false">
                            <td style="width: 238px" class="clslabel" align="center" colspan="2">
                                <fieldset>
                                    <legend>Total Vacant Summary </legend>
                                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                                <asp:Label ID="lblVacant_WST" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                                <asp:Label ID="lblVacant_HCB" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                                <asp:Label ID="lblVacant_FCB" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr id="tr1" runat="server" visible="false">
                            <td colspan="3">
                                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                            Total Allocated Seats : <b>
                                                <asp:Label ID="lblScatteredSeats" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                            Total Occupied Seats : <b>
                                                <asp:Label ID="lbloccupied" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                            Total Vacant Seats : <b>
                                                <asp:Label ID="lblVacantSeats" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 238px" class="clslabel" align="left" colspan="2">
                                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                    height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" /></td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
