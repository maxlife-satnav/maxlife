Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_GIS_Controls_query
    Inherits System.Web.UI.UserControl
    Public Shared Space_Id As String = String.Empty
    Public Shared Layer_Id As String = String.Empty


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim selfloor As String = String.Empty
        Dim selTower As String = String.Empty
        Dim selBld As String = String.Empty

        selfloor = "'" & Request.QueryString("FLR_CODE") & "'"
        Session("FLR_CODE") = Request.QueryString("FLR_CODE")
        selTower = Session("Twr_Code")
        selBld = Session("BDG_ID")

        If Not IsPostBack() Then
            Dim query As String = String.Empty

            strSQL = "select smq_Id, smq_field from  " & Session("TENANT") & "."  & "SMS_MAP_QUERY "
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

            drpdwnCategory.DataValueField = "smq_Id"
            drpdwnCategory.DataTextField = "smq_field"
            drpdwnCategory.DataSource = ObjDR
            drpdwnCategory.DataBind()
            ObjDR.Close()

            drpdwnCategory.SelectedIndex = 0

            strSQL = "select smq_query from  " & Session("TENANT") & "."  & "SMS_MAP_QUERY where smq_Id= " & drpdwnCategory.SelectedItem.Value & ""
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            If ObjDR.Read() Then
                query = ObjDR("smq_query").ToString()
            End If
            ObjDR.Close()


            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, query & selfloor & "and ssa_Twr_id = '" & selTower & "' and ssa_bdg_id = '" & selBld & "' Order by DisValue")

            'Response.Write(query & selfloor & "and ssa_Twr_id = '" & selTower & "' and ssa_bdg_id = '" & selBld & "' Order by DisValue")

            drpdwnValue.DataSource = ObjDR
            drpdwnValue.DataValueField = "Value"
            drpdwnValue.DataTextField = "DisValue"
            drpdwnValue.DataBind()
            'ObjDR.Close()
            If ObjDR.IsClosed = False Then
                ObjDR.Close()

            End If
            If drpdwnValue.Items.Count = 0 Then
                drpdwnValue.Items.Clear()
                drpdwnValue.Items.Insert("0", "No Data")
            End If
        End If
    End Sub

    Private Sub drpdwnCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpdwnCategory.SelectedIndexChanged
        Dim objdata As SqlDataReader
        Dim query As String = String.Empty
        Dim selfloor As String
        Dim selTower As String = String.Empty
        Dim selBld As String = String.Empty

        selfloor = "'" & Session("FLR_CODE") & "'"
        selTower = Session("Twr_Code")
        selBld = Session("BDG_ID")

        'ObjComm = New SqlCommand("select smq_query from  " & AppSettings("APPDB") & "SMS_MAP_QUERY where smq_Id= " & drpdwnCategory.SelectedItem.Value & "", ObjCon)
        strSQL = "select smq_query from  " & Session("TENANT") & "."  & "SMS_MAP_QUERY where smq_Id= " & drpdwnCategory.SelectedItem.Value & ""
        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        If objdata.Read() Then
            query = objdata("smq_query").ToString()
        End If
        objdata.Close()
        If drpdwnCategory.SelectedValue.ToString().Trim() = "2" Then
            query = query & selfloor & " and SPC_TWR_ID = '" & selTower & "' and SPC_BDG_ID = '" & selBld & "' Order by DisValue"
        Else
            query = query & selfloor & " and SSA_TWR_ID = '" & selTower & "' and SSA_BDG_ID = '" & selBld & "' Order by DisValue"
        End If


        objdata = SqlHelper.ExecuteReader(CommandType.Text, query)
        drpdwnValue.DataSource = objdata
        drpdwnValue.DataValueField = "Value"
        drpdwnValue.DataTextField = "DisValue"
        drpdwnValue.DataBind()
        objdata.Close()

        If drpdwnValue.Items.Count = 0 Then
            drpdwnValue.Items.Clear()
            drpdwnValue.Items.Insert("0", "No Data")
        End If
    End Sub

    Private Sub ImgFetch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgFetch.Click
        Dim objdata As SqlDataReader

        If (drpdwnCategory.Items.Count = 0) Then
            Response.Write("<script language=javascript>alert(""No Items to Fetch"")</script>")
        Else
            If (drpdwnCategory.SelectedItem.Value = "") Or (drpdwnValue.SelectedItem.Value = "No Data") Then
                Response.Write("<script language=javascript>alert(""No Items to Fetch"")</script>")
            Else
                Dim q1, q2, mainquery, opt
                q1 = drpdwnCategory.SelectedItem.Value
                opt = drpdwnOpt.SelectedItem.Value
                q2 = drpdwnValue.SelectedItem.Value

                strSQL = "select smq_result from  " & Session("TENANT") & "."  & "SMS_MAP_QUERY where smq_Id = '" & q1 & "'"

                objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                If objdata.Read() Then
                    mainquery = objdata("smq_result").ToString()
                End If

                objdata.Close()

                If q1 = 2 Or q1 = 3 Or q1 = 4 Then
                    'ObjComm = New SqlCommand(mainquery & opt & "'" & q2 & "'", ObjCon)
                    strSQL = mainquery & opt & "'" & q2 & "'"
                Else
                    'ObjComm = New SqlCommand(mainquery & opt & q2, ObjCon)
                    strSQL = mainquery & opt & "'" & q2 & "'"
                End If

                'Response.Write(strSQL)
                objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)


                If objdata.Read() Then
                    If q1 = 1 Or q1 = 3 Or q1 = 4 Or q1 = 5 Or q1 = 6 Then
                        Space_Id = objdata("ssa_spc_Id").ToString()
                        Layer_Id = objdata("spc_layer").ToString()
                    Else
                        Space_Id = objdata("SPC_ID").ToString()
                        Layer_Id = objdata("spc_layer").ToString()
                    End If


                End If

                objdata.Close()

                'Response.Write("<SCRIPT language=JavaScript src=""Scripts/mapcontrols.js""></SCRIPT>")
                'Response.Write("<script>GetSelection(""" & Trim(Space_Id) & """ , """ & Trim(Layer_Id) & """)</script>")
                Response.Write("<Script language=Javascript>window.open(""../GIS/Query_Results.aspx?q1=" & drpdwnCategory.SelectedItem.Value & "&opt= " & drpdwnOpt.SelectedItem.Value & "&q2=" & drpdwnValue.SelectedItem.Value & """,""QueryResults"",""Top=0,left=470,height=300,width=320,scrollbars=no"")</Script>")
            End If
        End If
    End Sub

    Private Sub ImgZoom_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgZoom.Click
        strSQL = "Select SPC_ID, SPC_LAYER from " & Session("TENANT") & "."  & "SPACE where spc_ID = '" & drpdwnValue.SelectedValue & "'"
        Dim objdata As SqlDataReader

        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        If objdata.Read Then
            Space_Id = objdata("SPC_ID").ToString()
            Layer_Id = objdata("spc_layer").ToString()
        End If
        Response.Write("<SCRIPT language=JavaScript src=""Scripts/mapcontrols.js""></SCRIPT>")
        Response.Write("<script>GetSelection(""" & Trim(Space_Id) & """ , """ & Trim(Layer_Id) & """)</script>")
    End Sub
End Class

