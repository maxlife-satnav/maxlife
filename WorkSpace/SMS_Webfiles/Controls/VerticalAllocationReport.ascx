<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VerticalAllocationReport.ascx.vb"
    Inherits="WorkSpace_SMS_Webfiles_Controls_VerticalAllocationReport" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-6">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select <%= Session("Parent")%>
                </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlVertical"
                    Display="None" ErrorMessage="Please Select Verticals" InitialValue="--Select--" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlVertical" CssClass="selectpicker" data-live-search="true" AutoPostBack="false" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="row">
                        <div class="col-md-5 control-label">
                            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="View Report" ValidationGroup="Val1" />
                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/WorkSpace/SMS_WebFiles/frmRepMasters.aspx"
                                Text="Back" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="trVertOcc" runat="server">
    <div class="row text-center">
        <div class="col-md-6">
            <h4><%= Session("Parent")%>  Occupied Summary </h4>
        </div>

        <%--<div class="col-md-6">
            <h4>Total Vacant Summary </h4>
        </div>--%>
    </div>
    <div class="row">
        <div class="form-group">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Workstations (WST) </label>
                    <div class="col-md-7">
                        <asp:Label ID="lblTotalWST" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <%--<div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Workstations (WST):</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblVacant_WST" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>--%>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Half Cabins (HCB):</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblTotalHCB" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <%-- <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Half Cabins (HCB): </label>
                    <div class="col-md-7">
                        <asp:Label ID="lblVacant_HCB" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Full Cabins (FCB): </label>
                    <div class="col-md-7">
                        <asp:Label ID="lblTotalFCB" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <%-- <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Full Cabins (FCB): </label>
                    <div class="col-md-7">
                        <asp:Label ID="lblVacant_FCB" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
    </div>
</div>



