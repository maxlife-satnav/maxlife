Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_GIS_Controls_Map_Departments
    Inherits System.Web.UI.UserControl
    Public Shared Space_Id As String = String.Empty
    Public Shared Layer_Id As String = String.Empty
    Public Shared flr_code As String = String.Empty
    Dim strSQL As String = String.Empty
    Public Shared spcids As String
    Dim objMsater As New clsMasters

    Dim objSubSonic As New clsSubSonicCommonFunctions


    Dim selfloor As String = String.Empty
    Dim selTower As String = String.Empty
    Dim selBld As String = String.Empty

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Put user code to initialize the page here


        selfloor = Request.QueryString("flr_code") 'Session("FLR_CODE")
        selTower = Request.QueryString("twr_code") 'Session("Twr_Code")
        selBld = Request.QueryString("lcm_code")

        If Not IsPostBack() Then

            Dim spCount1 As New SqlParameter("@SSA_TWR_ID", SqlDbType.VarChar, 500)
            Dim spCount2 As New SqlParameter("@SSA_BDG_ID", SqlDbType.VarChar, 500)
            Dim spCount3 As New SqlParameter("@SSA_FLR_ID", SqlDbType.VarChar, 500)
            spCount1.Value = selTower
            spCount2.Value = selBld
            spCount3.Value = selfloor

            'Response.Write(selTower & "<br/>" & selBld & "<br>" & selfloor)

            Dim MCount As Integer = 0
            Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"USER_GETCONSOLIDATED_Dep_Report", spCount1, spCount2, spCount3)

            GridView1.DataSource = ds
            GridView1.DataBind()

        End If
    End Sub
 
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Response.Write("<script language=javascript>javascript:window.open('../GIS/map_departments.aspx?FLR_CODE=" & Request.QueryString("flr_code") & "&TWR_CODE=" & Request.QueryString("twr_code") & "&BDG_ID=" & Request.QueryString("LCM_CODE") & "','MapWindow','scrollbars=no,toolbar=no,statusbar=no,height=650,width=850,top=0,left=0')</script>")
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "select" Then
             
            Dim index As String = e.CommandArgument
 
            Dim ds As New DataSet
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@DEPT", SqlDbType.NVarChar, 200)
            param(0).Value = index
            param(1) = New SqlParameter("@lcm_code", SqlDbType.NVarChar, 200)
            param(1).Value = Request.QueryString("lcm_code")
            param(2) = New SqlParameter("@twr_code", SqlDbType.NVarChar, 200)
            param(2).Value = Request.QueryString("twr_code")
            param(3) = New SqlParameter("@flr_code", SqlDbType.NVarChar, 200)
            param(3).Value = Request.QueryString("flr_code")

            ds = objsubsonic.GetSubSonicDataSet("GETSPACEID_DEPT", param)
            Dim mspc_id As String


            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    mspc_id = mspc_id & ds.Tables(0).Rows(i).Item("SSA_SPC_ID").ToString & ","
                Next
            End If

            If mspc_id.EndsWith(",") Then
                mspc_id = mspc_id.Remove(mspc_id.Length - 1, 1)
            End If
            selfloor = Request.QueryString("flr_code")
            selTower = Request.QueryString("twr_code")
            selBld = Request.QueryString("lcm_code")
            Response.Redirect("frmUSERMAPS.aspx?lcm_code=" & selBld & "&twr_code=" & selTower & "&flr_code=" & selfloor & "&spc_id=" & mspc_id)
        End If
    End Sub

    'Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim lnkDept As LinkButton = DirectCast(e.Row.FindControl("lnkDept"), LinkButton)
    '        lnkDept.Attributes.Add("Onclick", "GetDeptSeat('AX0040/NA/GF/NA/FCB-003','AX0040-NA-GF')")
    '        ' lnkDept.Attributes.Add("Onclick", "alert('AX0040/NA/GF/NA/FCB-003')")
    '    End If
    'End Sub
End Class