Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_Controls_VerticalAllocationReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'ReportViewer1.Visible = False
            BindVerticals()
            trVertOcc.Visible = False
            BindVerticalLocation()

        End If
    End Sub

    Private Sub BindVerticals()
        ObjSubSonic.Binddropdown(ddlVertical, "GET_ALLCOSTCENTER_AURID_ALLOC", "COST_CENTER_NAME", "COST_CENTER_CODE")
        ddlVertical.Items.Insert(0, "--All--")
        ddlVertical.Items.RemoveAt(1)
    End Sub

 
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindVerticalLocation()
    End Sub

    Private Sub BindVerticalLocation()
        Dim strVertical As String = ""
        'If ddlVertical.SelectedItem.Value = "--Select--" Then
        '    trVertOcc.Visible = False
        '    Exit Sub
        'ElseIf ddlVertical.SelectedItem.Value = "--All--" Then
        '    strVertical = ""
        '    lblmsg.Text = ""
        'Else
        '    strVertical = ddlVertical.SelectedItem.Value
        '    lblmsg.Text = ""
        'End If

        If ddlVertical.SelectedItem.Value = "--All--" Then
            strVertical = ""
        Else
            strVertical = ddlVertical.SelectedItem.Value
        End If

        Dim ds As New DataSet
        Dim grdTotal_WST As Integer = 0
        Dim grdTotal_HCB As Integer = 0
        Dim grdTotal_FCB As Integer = 0

        Dim grdTotal_Vacant_WST As Integer = 0
        Dim grdTotal_Vacant_HCB As Integer = 0
        Dim grdTotal_Vacant_FCB As Integer = 0


        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SSA_VERTICAL", SqlDbType.NVarChar, 200)
        param(0).Value = strVertical
        ds = ObjSubSonic.GetSubSonicDataSet("USP_VERTICAL_ALLOCATION_REPORT1", param)

        Dim rds As New ReportDataSource()
        rds.Name = "VerticalAllocationSummaryDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/VerticalAllocationSummaryReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        Dim param1(0) As SqlParameter
        param1(0) = New SqlParameter("@SSA_VERTICAL", SqlDbType.NVarChar, 200)
        param1(0).Value = strVertical
        Dim ds1 As DataSet
        ds1 = objsubsonic.GetSubSonicDataSet("USP_VERTICAL_ALLOCATION_REPORT1", param1)
        If ds1.Tables(0).Rows.Count > 0 Then
            trVertOcc.Visible = True
            lblTotalWST.Text = ds.Tables(1).Rows(0).Item("OCCUPIED_WORK_STATION")
            lblTotalHCB.Text = ds.Tables(1).Rows(0).Item("OCCUPIED_HALF_CABIN")
            lblTotalFCB.Text = ds.Tables(1).Rows(0).Item("OCCUPIED_full_cabin")
            'lblVacant_WST.Text = ds.Tables(1).Rows(0).Item("VACANT_WORK_STATION")
            'lblVacant_HCB.Text = ds.Tables(1).Rows(0).Item("VACANT_HALF_CABIN")
            'lblVacant_FCB.Text = ds.Tables(1).Rows(0).Item("VACANT_FULL_CABIN")
        Else
            trVertOcc.Visible = False
        End If

    End Sub
    Protected Sub ddlVertical_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVertical.SelectedIndexChanged
        ReportViewer1.Visible = False
        trVertOcc.Visible = False
        If ddlVertical.SelectedIndex <> 0 Then
            lblMsg.Text = ""
        End If
    End Sub
End Class
