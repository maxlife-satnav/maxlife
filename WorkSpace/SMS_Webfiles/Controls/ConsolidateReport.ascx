<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConsolidateReport.ascx.vb"
    Inherits="WorkSpace_SMS_Webfiles_Controls_ConsolidateReport" %>
<table cellspacing="0" width="100%" cellpadding="0" align="center" border="0">
    <tr>
        <td>
            <table class="table" cellspacing="0" cellpadding="0" align="center" border="0">
                <tr align="left">
                    <td bgcolor="#4863A0" colspan="2" style="padding-left: 15px;" height="15">
                      <asp:LinkButton ID="LinkButton1" runat="server" style="color:Blue;font-weight:bold;">Consolidated Report</asp:LinkButton> 
                       </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div style="height: 10em; width: 400; overflow: scroll; border: solid gray 1px; padding: 0.2em 0.2em;">
                <asp:DataList ID="dlWings" runat="server">
                    <ItemTemplate>
                        <h3>
                            <asp:Label ID="lblwing" Text='<%# Eval("SPC_WNG_ID") %>' runat="server" Visible="false"></asp:Label></h3>
                        <asp:DataList ID="dlSeatSummaryByWing" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="lblsummary" runat="server" Text='<%# Eval("summary") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:DataList>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </td>
    </tr>
</table>
