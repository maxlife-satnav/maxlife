Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_Controls_UpdateTotalReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim UID As String = Session("uid")
        End If
        If Not IsPostBack Then
            BindCity()
        End If
    End Sub

    Private Sub BindCity()
        ObjSubSonic.Binddropdown(ddlCity, "GET_USPREPORT_CITY", "CTY_NAME", "CTY_CODE")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlLocation, "GET_USPREPORT_LOCATION", "LCM_NAME", "LCM_CODE", param)

        ddlLocType.Items.Clear()
        ddlLocType.Items.Insert("0", "--Select--")
        ddlTower.Items.Clear()
        ddlTower.Items.Insert("0", "--Select--")
        ddlDepartment.Items.Clear()
        ddlDepartment.Items.Insert("0", "--Select--")

        txtTot_Available.Text = ""
        txtTot_Occupied.Text = ""
        lblmsg.Text = ""

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
     
        ObjSubSonic.Binddropdown(ddlLocType, "GET_USPREPORT_LOCTYPE", "LOC_TYPE_NAME", "LOC_TYPE", param)

        ddlTower.Items.Clear()
        ddlTower.Items.Insert("0", "--Select--")
        ddlDepartment.Items.Clear()
        ddlDepartment.Items.Insert("0", "--Select--")

        txtTot_Available.Text = ""
        txtTot_Occupied.Text = ""

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        param(2) = New SqlParameter("@LOC_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlLocType.SelectedItem.Value
        param(3) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(3).Value = ddlTower.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlDepartment, "GET_USPREPORT_DEPT", "DEPT_NAME", "DEPT", param)

       

        txtTot_Available.Text = ""
        txtTot_Occupied.Text = ""

    End Sub




    Protected Sub ddlLocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocType.SelectedIndexChanged
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        param(2) = New SqlParameter("@LOC_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlLocType.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlTower, "GET_USPREPORT_TOWER", "TWR_NAME", "TWR_CODE", param)

        ddlDepartment.Items.Clear()
        ddlDepartment.Items.Insert("0", "--Select--")

        txtTot_Available.Text = ""
        txtTot_Occupied.Text = ""

    End Sub

    Protected Sub ddlDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged

        Dim ds As New DataSet

        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        param(2) = New SqlParameter("@LOC_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlLocType.SelectedItem.Value
        param(3) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(3).Value = ddlTower.SelectedItem.Value
        param(4) = New SqlParameter("@DEPT", SqlDbType.NVarChar, 200)
        param(4).Value = ddlDepartment.SelectedItem.Value
        ds = objsubsonic.GetSubSonicDataSet("GET_USPREPORT_AVAILABLE_OCCUPIED", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtTot_Available.Text = ds.Tables(0).Rows(0).Item("AVLBL_FOR_ALLOCATION")
            txtTot_Occupied.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_SEATS")
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        param(2) = New SqlParameter("@LOC_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlLocType.SelectedItem.Value
        param(3) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(3).Value = ddlTower.SelectedItem.Value
        param(4) = New SqlParameter("@DEPT", SqlDbType.NVarChar, 200)
        param(4).Value = ddlDepartment.SelectedItem.Value
        param(5) = New SqlParameter("@OCCUPIED_SEATS", SqlDbType.Int)
        param(5).Value = txtTot_Occupied.Text
        param(6) = New SqlParameter("@AVLBL_FOR_ALLOCATION", SqlDbType.Int)
        param(6).Value = txtTot_Available.Text
        ObjSubSonic.GetSubSonicExecute("GET_USPREPORT_AVAILABLE_OCCUPIED_UPDATE", param)
        lblmsg.Text = "Succesfully updated."
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        param(2) = New SqlParameter("@LOC_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlLocType.SelectedItem.Value
        param(3) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(3).Value = ddlTower.SelectedItem.Value
        param(4) = New SqlParameter("@DEPT", SqlDbType.NVarChar, 200)
        param(4).Value = ddlDepartment.SelectedItem.Value
        param(5) = New SqlParameter("@OCCUPIED_SEATS", SqlDbType.Int)
        param(5).Value = txtTot_Occupied.Text
        param(6) = New SqlParameter("@AVLBL_FOR_ALLOCATION", SqlDbType.Int)
        param(6).Value = txtTot_Available.Text
        ObjSubSonic.GetSubSonicExecute("GET_USPREPORT_AVAILABLE_OCCUPIED_DELETE", param)

        lblmsg.Text = "Succesfully Deleted."
        Response.Redirect("frmUpdateTotalReport.aspx")
    End Sub
End Class
