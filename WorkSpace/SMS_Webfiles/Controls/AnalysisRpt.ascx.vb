Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.io
Partial Class WorkSpace_SMS_Webfiles_Controls_AnalysisRpt
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindVerticals()
        End If
    End Sub

    Private Sub BindVerticals()
        ObjSubSonic.Binddropdown(ddlVertical, "GET_ALLCOSTCENTER_AURID_ALLOC", "COST_CENTER_NAME", "COST_CENTER_CODE")
        ddlVertical.Items.Insert(1, "--All--")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BindAnalysisReport()
    End Sub

    Private Sub BindAnalysisReport()

        Dim strVertical As String

        If ddlVertical.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Please Select Vertical."
            tr1.visible = False
            Exit Sub
        ElseIf ddlVertical.SelectedItem.Value = "--All--" Then
            lblmsg.Text = ""
        Else
            strVertical = ddlVertical.SelectedItem.Value
            lblmsg.Text = ""
        End If

        If IsNumeric(txtNoofSeats.Text) = False Then
            lblmsg.Text = "Invalid number of seat(s)."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If

        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@SSA_VERTICAL", SqlDbType.NVarChar, 200)
        param(0).Value = strVertical
        param(1) = New SqlParameter("@CONSOL_NUMBER", SqlDbType.Int)
        param(1).Value = CInt(txtNoofSeats.Text)
        ds = objsubsonic.GetSubSonicDataSet("AUTO_ANALYSIS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            tr1.Visible = True

            lblScatteredSeats.Text = ds.Tables(0).Rows(0).Item("ALLOTED_NOT_OCCUPIED")
            lbloccupied.Text = ds.Tables(0).Rows(0).Item("PARTIAL_OCCUPANCY")
            lblVacantSeats.Text = ds.Tables(0).Rows(0).Item("SEATS_TO_FILL")
            lblMessage.Text = ds.Tables(0).Rows(0).Item("MESSAGESTRING")
        End If


    End Sub
End Class
