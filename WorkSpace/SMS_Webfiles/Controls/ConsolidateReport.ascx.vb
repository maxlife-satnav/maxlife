Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_Controls_ConsolidateReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        FillWings()
        'End If
    End Sub

    Public Sub FillWings()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 100)
        param(0).Value = Request.QueryString("LCM_CODE")
        param(1) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Request.QueryString("TWR_CODE")
        param(2) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 100)
        param(2).Value = Request.QueryString("FLR_CODE")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GET_WNGSFORSPACE", param)
        dlWings.DataSource = ds
        dlWings.DataBind()


        For i As Integer = 0 To dlWings.Items.Count - 1
            Dim dlSeatSummaryByWing As DataList = CType(dlWings.Items(i).FindControl("dlSeatSummaryByWing"), DataList)
            Dim lblwing As Label = CType(dlWings.Items(i).FindControl("lblwing"), Label)
            'lblwing.Text = ds.Tables(0).Rows(i).Item("SPC_WNG_ID")

            Dim param1(3) As SqlParameter
            param1(0) = New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 100)
            param1(0).Value = Request.QueryString("LCM_CODE")
            param1(1) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 100)
            param1(1).Value = Request.QueryString("TWR_CODE")
            param1(2) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 100)
            param1(2).Value = Request.QueryString("FLR_CODE")
            param1(3) = New SqlParameter("@WNG_ID", SqlDbType.NVarChar, 100)
            param1(3).Value = lblwing.Text 'ds.Tables(0).Rows(i).Item("SPC_WNG_ID")
            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GET_ALLOCATION_SUMMARY", param1)
            dlSeatSummaryByWing.DataSource = ds1
            dlSeatSummaryByWing.DataBind()

        Next


    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Response.Write("<script language=javascript>javascript:window.open('../GIS/Reports2.aspx?FLR_CODE=" & Request.QueryString("flr_code") & "&LCM_CODE=" & Request.QueryString("LCM_CODE").Trim & "&TWR_CODE=" & Request.QueryString("twr_code") & "','MapWindow','scrollbars=no,toolbar=no,statusbar=no,height=650,width=850,top=0,left=0')</script>")
    End Sub
End Class


