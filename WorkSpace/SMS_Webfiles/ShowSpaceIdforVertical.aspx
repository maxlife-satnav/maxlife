<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowSpaceIdforVertical.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_ShowSpaceIdforVertical" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Allocated Spaces</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black"> Allocated Spaces
                    </asp:Label><hr align="center" width="60%" />
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                        width="9" />
                </td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Allocated Spaces</strong>
                </td>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                        width="16" />
                </td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;
                </td>
                <td align="left">
                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                        <tr>
                            <td>
                                <asp:GridView ID="gvToday" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vertical" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                            ItemStyle-Width="20">
                                            <ItemTemplate>
                                                <asp:Label ID="lblssa_vertical" runat="server" Text='<%# Eval("ssa_vertical") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Space Id" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                            ItemStyle-Width="20">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSSA_SPC_ID" runat="server" Text='<%# Eval("SSA_SPC_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                    height: 100%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" />
                </td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" />
                </td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
