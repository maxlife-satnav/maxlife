<%@ Page Language="VB"  MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmSpaceConfirmation.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSpaceConfirmation" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script language="javascript" type="text/javascript">
		
		function ViewMap()
		{
		alert(document.frmSMSAvb.cmbFloor.value);
		var flR_id=document.frmSMSAvb.cmbFloor.value;
		alert(flR_id);
		window.open('Map_User.aspx?flR_id='+flR_id,'MapWindow','scrollbars=no,toolbar=no,statusbar=no,height=540,width=790,top=0,left=0');
		}
    </script>

   
        <div>
        
        <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">   <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="88%" Font-Underline="False" ForeColor="Black"
                Height="24px">Space Connect 
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
                        
        
      
          
         
           
           
                
            <asp:Panel ID="panelmain" runat="server" Width="95%" Height="100%">
             
                <table id="Table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                        <td  colspan="3" align="left">
           <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
          </td>
                        </tr>
                        
                    <tr>
                        <td >
                            <img height="27" src="../../Images/table_left_top_corner.gif"></td>
                        <td class="tableHEADER" align="left">
                            <strong>&nbsp; Employee Space Confirmation&nbsp;</strong></td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 9px">
                        </td>
                        <td align="Left">
<asp:ValidationSummary ID="vsSM" runat="server" CssClass="divmessagebackground" ForeColor="" />
                            &nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <br />
                            <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 95%" align="center"
                                border="1">
                                
                                <tr>
                                    <td style="width: 50%; height: 5px">
                                        &nbsp;City<font class="clsnote">*</font></td>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlCity"
                                        Display="None" ErrorMessage="Select   City " ValueToCompare="--Select--"
                                        Operator="NotEqual"></asp:CompareValidator><td style="width: 50%; height: 5px">
                                            <asp:DropDownList ID="ddlCity" TabIndex="1" runat="server" CssClass="clsComboBox"
                                                Width="100%" AutoPostBack="true">
                                            </asp:DropDownList></td>
                                </tr>
                                
                                <tr>
                                    <td style="width: 50%; height: 5px">
                                        &nbsp;Location<font class="clsnote">*</font></td>
                                    <asp:CompareValidator ID="cvCmbPremise" runat="server" ControlToValidate="ddlLocation"
                                        Display="None" ErrorMessage="Select   Location " ValueToCompare="--Select--"
                                        Operator="NotEqual">cmpLocation</asp:CompareValidator><td style="width: 50%; height: 5px">
                                            <asp:DropDownList ID="ddlLocation" TabIndex="1" runat="server" CssClass="clsComboBox"
                                                Width="100%" AutoPostBack="true">
                                            </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 5px">
                                        &nbsp;<asp:Label ID="Label1" runat="server" Width="92px" Height="18px">Tower<font class="clsnote">
												*</font></asp:Label>
                                        <asp:CompareValidator ID="cmpflr" runat="server" ControlToValidate="ddlTower" Display="None"
                                            ErrorMessage="Select Tower " ValueToCompare="--Select--" Operator="NotEqual">cmpTower</asp:CompareValidator></td>
                                    <td align="left" style="width: 50%; height: 5px">
                                        <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="clsComboBox"
                                            Width="100%" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 5px">
                                        &nbsp;<asp:Label ID="Label2" runat="server" Width="92px" Height="18px">Floor<font class="clsnote">
												*</font></asp:Label>
                                        <asp:CompareValidator ID="cmptwr" runat="server" ControlToValidate="ddlFloor" Display="None"
                                            ErrorMessage="Select Floor " ValueToCompare="--Select--" Operator="NotEqual">cmpFloor</asp:CompareValidator></td>
                                    <td style="width: 50%; height: 5px">
                                        <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="clsComboBox"
                                            Width="100%" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 5px">
                                        &nbsp;<asp:Label ID="Label4" runat="server" Width="92px" Height="18px">Space Type<font class="clsnote">
												*</font></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlSpacetype" Display="None"
                                            ErrorMessage="Select Space Type " ValueToCompare="--Select--" Operator="NotEqual">cmpSpaceType</asp:CompareValidator></td>
                                    <td style="width: 50%; height: 5px">
                                        <asp:DropDownList ID="ddlSpacetype" TabIndex="5" runat="server" CssClass="clsComboBox"
                                            Width="100%" AutoPostBack="true">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 50%; height: 5px">
                                        &nbsp;<asp:Label ID="Label5" runat="server" Width="92px" Height="18px">Space<font class="clsnote">
												*</font></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlSpaces" Display="None"
                                            ErrorMessage="Select Space " ValueToCompare="--Select--" Operator="NotEqual">cmpSpace</asp:CompareValidator></td>
                                    <td style="width: 50%; height: 5px">
                                        <asp:DropDownList ID="ddlSpaces" TabIndex="5" runat="server" CssClass="clsComboBox"
                                            Width="100%">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" style="height: 23px">
                                        <asp:Button ID="btnView" runat="server" CssClass="clsButton" Text="View"></asp:Button></td>
                                </tr>
                                
                                
                                  <tr>
                                    <td align="center" colspan="2" style="height: 23px">
                                    <a onclick="return getCal('Form1','txtFromDate');" href="#"></a></td>
                                </tr>
                                
                                  <tr>
                                    <td align="center" colspan="2" style="height: 23px">
                                        <asp:TextBox ID="txtCtrlVal" runat="server" Width="19px" Visible="False">--Select--</asp:TextBox></td>
                                </tr>
                                
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9px" /></td>
                        <td style="height: 17px;" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" /></td>
                        <td style="height: 17px;">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" /></td>
                    </tr>
                </table>
            </asp:Panel>
            
  </div>
  </asp:Content>