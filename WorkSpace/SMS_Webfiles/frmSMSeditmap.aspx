<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmSMSeditmap.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSMSeditmap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
		<script language="javascript" type="text/javascript">
		
		function ViewMap()
		{
		alert(document.frmSMSAvb.cmbFloor.value);
		var flR_id=document.frmSMSAvb.cmbFloor.value;
		alert(flR_id);
		window.open('Map_User.aspx?flR_id='+flR_id,'MapWindow','scrollbars=no,toolbar=no,statusbar=no,height=540,width=790,top=0,left=0');
		}
		</script>
		
		
        <div>
         <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center"> <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="88%" Font-Underline="False" ForeColor="Black" Height="24px">Space Connect 
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
  		
            <asp:Panel ID="panelmain"  runat="server" Width="95%" Height="100%">
             
                <table id="Table3" cellspacing="0" cellpadding="0"  width="95%"
                    align="center" border="0">
                    <tr>
                        <td  colspan="3" align="left">
             <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
          </td>
                        </tr>
                         <tr>
                        <td  colspan="3" align="left">
           <asp:validationsummary id="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="true"></asp:validationsummary>
          </td>
                        </tr>
                         
                        
                    <tr>
                        <td style="width: 10px">
                            <img height="27" src="../../Images/table_left_top_corner.gif" ></td>
                        <td class="tableHEADER" align="left" >
                            <strong>&nbsp; Edit Map </strong>
                        </td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif" ></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        </td>
                        <td align="center">
							<table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                border="1">
								<tr>
									<td align="left" style="WIDTH: 50%; HEIGHT: 5px">Location<font class="clsnote">*</font></td>
										<asp:comparevalidator id="cvCmbPremise" runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Select   Location "
											ValueToCompare="--Select--" Operator="NotEqual">cmpPrm</asp:comparevalidator><td style="WIDTH: 50%; HEIGHT: 5px">
										<asp:dropdownlist id="ddlLocation" tabIndex="1" runat="server" CssClass="clsComboBox" Width="100%"
											AutoPostBack="true"></asp:dropdownlist></td>
								</tr>
															
								
								<tr>
									<td align="left"  style="WIDTH: 50%; HEIGHT: 5px">
										<asp:label id="Label1" runat="server"  Width="92px" Height="18px" >Tower<font class="clsnote">*</font></asp:label>
										<asp:comparevalidator id="cmpflr" runat="server" ControlToValidate="ddlTower" Display="None" ErrorMessage="Select Tower "
											ValueToCompare="--Select--" Operator="NotEqual">cmpflr</asp:comparevalidator></td>
									<td align="left" style="WIDTH: 50%; HEIGHT: 5px">
										<asp:dropdownlist id="ddlTower" tabIndex="5" runat="server" CssClass="clsComboBox" Width="100%" AutoPostBack="true"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td align="left"  style="WIDTH: 50%; HEIGHT: 5px">
										<asp:label id="Label2" runat="server"  Width="92px" Height="18px">Floor<font class="clsnote">*</font></asp:label>
										<asp:comparevalidator id="cmptwr" runat="server" ControlToValidate="ddlFloor" Display="None" ErrorMessage="Select Floor "
											ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:comparevalidator></td>
									<td style="WIDTH: 50%; HEIGHT: 5px">
										<asp:dropdownlist id="ddlFloor" tabIndex="5" runat="server" CssClass="clsComboBox" Width="100%" AutoPostBack="true"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td align="center" colspan="2" style="height: 23px">
										<asp:button id="btnView" runat="server" CssClass="clsButton" Text="View"></asp:button></td>
								</tr>
																<tr runat="server" visible="false">
									<td align="center" colspan="2" style="height: 23px">
										 <a onclick="return getCal('Form1','txtFromDate');" href="#"></a></td>
								</tr>
								
																<tr runat="server" visible="false">
									<td align="center" colspan="2" style="height: 23px">
								<asp:textbox id="txtCtrlVal" runat="server" Width="19px" Visible="False" >--Select--</asp:textbox>
							</td>
							</tr>
							</table>
					           </td>
                       <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 17px; width: 10px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif"  /></td>
                        <td style="height: 17px;" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" /></td>
                        <td style="height: 17px; ">
                            <img height="17" src="../../Images/table_right_bot_corner.gif"  /></td>
                    </tr>
                </table>
			</asp:Panel>
			
</div>

</asp:Content>