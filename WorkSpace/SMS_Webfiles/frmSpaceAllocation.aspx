<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSpaceAllocation.aspx.vb"
    Inherits="SpaceManagement_frmSpaceVerticalAllocations" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
        function colorRow(e) {

            // Annoying code to get the desired event handler from the browser.
            if (!e) e = window.event;
            var srcEl = e.target || e.srcElement;

            var curElement = srcEl;
            while (curElement && !(curElement.tagName == "TR")) {
                // Keep going until we reach the parent table row
                curElement = curElement.parentNode;
            }
            if (curElement != srcEl) {
                if (srcEl.checked) {
                    // Modify your colors here for a selected object

                    curElement.style.backgroundColor = "#DEDFDE";
                }
                else {
                    // Modify your colors for a deselected object (read: Normal)

                    curElement.style.backgroundColor = "white";
                }
            }
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                var gvName = elm.name
                if (gvName.indexOf('$') != -1) {
                    gvName = gvName.substring(0, gvName.indexOf('$'))
                    // alert(gvName)
                    re = new RegExp('chk')
                    //alert(elm.name);
                    if (gvName == "gvWs") {
                        if (elm.type == 'checkbox') {
                            if (re.test('chk')) {
                                if (elm.checked == true) {
                                    k = k + 1;
                                }
                            }
                        }
                    }
                }

            }

            document.getElementById('lblSelectedWs').innerText = "Selected Workstation count is : " + k;
        }
        function colorRowHC(e) {
            // Annoying code to get the desired event handler from the browser.
            if (!e) e = window.event;
            var srcEl = e.target || e.srcElement;

            var curElement = srcEl;
            while (curElement && !(curElement.tagName == "TR")) {
                // Keep going until we reach the parent table row
                curElement = curElement.parentNode;
            }
            if (curElement != srcEl) {
                if (srcEl.checked) {
                    // Modify your colors here for a selected object

                    curElement.style.backgroundColor = "#DEDFDE";
                }
                else {
                    // Modify your colors for a deselected object (read: Normal)

                    curElement.style.backgroundColor = "white";
                }
            }
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                re = new RegExp('chkHC')
                //alert(elm.name);
                var gvName = elm.name
                if (gvName.indexOf('$') != -1) {
                    gvName = gvName.substring(0, gvName.indexOf('$'))
                    if (gvName == "gvHC") {
                        if (elm.type == 'checkbox') {
                            if (re.test('chkHC')) {
                                if (elm.checked == true) {
                                    k = k + 1;
                                }
                            }
                        }
                    }
                }
            }
            document.getElementById('lblSelectedHC').innerText = "Selected Half Cabin count is : " + k;
        }
        function colorRowFC(e) {
            // Annoying code to get the desired event handler from the browser.
            if (!e) e = window.event;
            var srcEl = e.target || e.srcElement;

            var curElement = srcEl;
            while (curElement && !(curElement.tagName == "TR")) {
                // Keep going until we reach the parent table row
                curElement = curElement.parentNode;
            }
            if (curElement != srcEl) {
                if (srcEl.checked) {
                    // Modify your colors here for a selected object

                    curElement.style.backgroundColor = "#DEDFDE";
                }
                else {
                    // Modify your colors for a deselected object (read: Normal)

                    curElement.style.backgroundColor = "white";
                }
            }
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]

                var gvName = elm.name
                if (gvName.indexOf('$') != -1) {
                    gvName = gvName.substring(0, gvName.indexOf('$'))
                    if (gvName == "gvFC") {
                        if (elm.type == 'checkbox') {
                            if (elm.checked == true) {
                                k = k + 1;
                            }

                        }
                    }
                }
            }
            document.getElementById('lblSelecetedFC').innerText = "Selected Full Cabin count is : " + k;
        }
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Allocation
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:Label ID="lblTower" runat="server" Visible="False"></asp:Label>
                        <asp:TextBox ID="txtVertical" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txrDept" Visible="false" runat="server" Enabled="False"
                            CssClass="form-control">
                        </asp:TextBox>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="red" />

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requisition ID<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlReq"
                                            Display="None" ErrorMessage="Please Select Requisition ID" SetFocusOnError="True" InitialValue="--Select--">
                                        </asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlReq" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requestor Name<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtReuname" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelVertical" runat="server" Text="" class="col-md-5 control-label"></asp:Label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="lblPrjName" runat="server" Font-Bold="False" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            <asp:Label ID="lblvercode" runat="server" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelCostcenter" runat="server" Text="" class="col-md-5 control-label"></asp:Label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="lblVertName" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Work Stations Required</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtWSreq" runat="server" MaxLength="5" CssClass="form-control"
                                                ReadOnly="True">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Work Stations Allocated<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtWSAllocate"
                                            Display="None" ErrorMessage="Please Enter Work Stations To Allocate" SetFocusOnError="True" InitialValue=""></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvwrkstations" runat="server" ControlToValidate="txtWSAllocate"
                                            Display="None" ErrorMessage="Please Enter Numerics in Work Stations  Allocated"
                                            Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtWSAllocate" runat="server" CssClass="form-control"
                                                MaxLength="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Half Cabins Required</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCubreq" runat="server" Enabled="False" CssClass="form-control"
                                                ReadOnly="True">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Half Cabins Allocated<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCuballocate"
                                            Display="None" ErrorMessage="Please Enter Half Cabins To Allocate" SetFocusOnError="True" InitialValue=""></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCuballocate"
                                            Display="None" ErrorMessage="Please Enter Numerics in Half Cabins Allocated"
                                            Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCuballocate" runat="server" CssClass="form-control"
                                                MaxLength="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Full Cabins Required</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCabReq" runat="server" Enabled="False" CssClass="form-control"
                                                ReadOnly="True">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Full Cabins Allocated<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCabAllocate"
                                            Display="None" ErrorMessage="Please Enter Full Cabins To Allocate" SetFocusOnError="True" InitialValue=""></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtCabAllocate"
                                            Display="None" ErrorMessage="Please Enter Numerics in Full Cabins To Allocated "
                                            Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCabAllocate" runat="server" CssClass="form-control"
                                                MaxLength="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date</label>
                                        <asp:RequiredFieldValidator ID="rfvFromdate" runat="server" ControlToValidate="txtFdate"
                                            Display="None" ErrorMessage="Please Select From Date" SetFocusOnError="True">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFdate" Enabled="false" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date</label>
                                        <asp:RequiredFieldValidator ID="rfvTodate" runat="server" ControlToValidate="txtTodate"
                                            Display="None" ErrorMessage="Please Select To  Date" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtTodate" runat="server" Enabled="false" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requestors Remarks</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtReqrem" runat="server" TextMode="MultiLine" CssClass="form-control"
                                                Enabled="False" ReadOnly="True">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Admin Remarks</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAdminrem" runat="server" TextMode="MultiLine" CssClass="form-control"
                                                ReadOnly="True">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-bordered table-hover table-striped"
                                    AllowPaging="True" PageSize="3">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Available Full Cabin(s)/Half Cabin(s)/Work Station"></asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server">View Map</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">
                                            Available Work Stations<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:Label ID="lblSelectedWs" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lblWSCount" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvWs" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            OnRowDataBound="gvWs_RowDataBound" EmptyDataText="No Work Stations are allocated to this vertical"
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk', this.checked)">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <input id="chk" type="checkbox" runat="server" value='<% #Bind("SSA_SPC_ID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblSpace" Text='<% #Bind("SSA_SPC_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblShiftId" Text='<% #Bind("SHIFTID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblReqid" Text='<% #Bind("SSA_SRNREQ_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Available Half Cabins<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblSelectedHC" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                                <asp:Label ID="lblHCCount" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:GridView ID="gvHC" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                OnRowDataBound="gvHC_RowDataBound" EmptyDataText="No Half Cabins are allocated to this vertical"
                                                CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk', this.checked)">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <input id="chkHC" type="checkbox" runat="server" value='<% #Bind("SSA_SPC_ID")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSpace" runat="server" Text='<% #Bind("SSA_SPC_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblShiftId" Text='<% #Bind("SHIFTID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblReqid" Text='<% #Bind("SSA_SRNREQ_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Available Full Cabins<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:Label ID="lblSelecetedFC" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lblFCCount" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvFC" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            OnRowDataBound="gvFC_RowDataBound" EmptyDataText="No Full Cabins are allocated to this vertical"
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk', this.checked)">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <input id="chkFC" type="checkbox" runat="server" value='<% #Bind("SSA_SPC_ID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSpace" runat="server" Text='<% #Bind("SSA_SPC_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblShiftId" Text='<% #Bind("SHIFTID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblReqid" Text='<% #Bind("SSA_SRNREQ_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                            <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                                ControlToValidate="TextBox5" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters !">
                                            </asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="TextBox5"
                                                Display="None" ErrorMessage="Please Enter Valid Remarks">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row text-right">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:CheckBox ID="chkForcibleAllocate" runat="server" Text="Request is Completed" />
                                        <asp:Button ID="btnAllo" runat="server" Text="Allocate" CssClass="btn btn-primary custom-button-color" OnClick="btnAllo_Click"></asp:Button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

