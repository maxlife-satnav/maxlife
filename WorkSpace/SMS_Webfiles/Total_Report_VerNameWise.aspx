<%@ Page Language="VB"  MasterPageFile="~/MasterPage.master"  AutoEventWireup="false" CodeFile="Total_Report_VerNameWise.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_Total_Report_VerNameWise" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <div>
        <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">  <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
        
        
          
            
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%">
                <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                        <td  colspan="3" align="left">
               <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
          </td>
                        </tr>
                    <tr>
                        <td>
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td width="100%" class="tableHEADER">
                            &nbsp;<strong> Consolidated Space Report</strong></td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                        </td>
                        <td align="left">
                            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                        </td>
                        <td>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:Button ID="Button1" runat="server" Text="Export To Excel" CssClass="clsButton"
                                Width="136px" />
                            <br />
                            <br />
                            <br />
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                           
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:DropDownList ID="ddlCity" Visible="false" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            <br />
                            <br />
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:DropDownList ID="ddlLocation" Visible="false" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            <br />
                            <br />
                              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:Label ID="Label3" Text="Select Vertical Name" runat="server"></asp:Label>
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
                            <asp:DropDownList ID="ddlVerNameList" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            <br />
                            <br />
                            <cc1:ExportPanel ID="ExportPanel1" runat="server">
                                &nbsp;<asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                &nbsp;<asp:Label ID="lblReport" runat="server"></asp:Label>
                                <br />
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    DataSourceID="SqlDataSource1" ForeColor="#333333" ShowFooter="true" GridLines="None">
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <Columns>
                                        <asp:BoundField DataField="TWR_Name" HeaderText="TWR_Name" SortExpression="TWR_Name" />
                                        <asp:BoundField DataField="FLR_Name" HeaderText="FLR_Name" SortExpression="FLR_Name" />
                                        <asp:BoundField DataField="WNG_NAME" HeaderText="WNG_NAME" SortExpression="WNG_NAME" />
                                        <asp:BoundField DataField="VER_NAME" HeaderText="VER_NAME" SortExpression="VER_NAME" />
                                        <asp:BoundField DataField="SSA_PROJECT" HeaderText="SSA_PROJECT" SortExpression="SSA_PROJECT" />
                                        <asp:TemplateField HeaderText="TOTAL_WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_WS" runat="server" Text='<%# Eval("TOTAL_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TOTAL_FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_FC" runat="server" Text='<%# Eval("TOTAL_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TOTAL_HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_HC" runat="server" Text='<%# Eval("TOTAL_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TOTAL_TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_TOTAL" runat="server" Text='<%# Eval("TOTAL_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CAP_WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_WS" runat="server" Text='<%# Eval("CAP_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CAP_FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_FC" runat="server" Text='<%# Eval("CAP_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CAP_HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_HC" runat="server" Text='<%# Eval("CAP_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CAP_TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_TOTAL" runat="server" Text='<%# Eval("CAP_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC_WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_WS" runat="server" Text='<%# Eval("OCC_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC_FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_FC" runat="server" Text='<%# Eval("OCC_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC_HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_HC" runat="server" Text='<%# Eval("OCC_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC_TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_TOTAL" runat="server" Text='<%# Eval("OCC_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT_WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_WS" runat="server" Text='<%# Eval("VACANT_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT_FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_FC" runat="server" Text='<%# Eval("VACANT_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT_HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_HC" runat="server" Text='<%# Eval("VACANT_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT_TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_TOTAL" runat="server" Text='<%# Eval("VACANT_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left"
                                        VerticalAlign="Top" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AmantraAxisframework_ConnectionString %>"
                                    SelectCommand="usp_TotalReport_ByVerNameWise" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="ddlVerNameList" DefaultValue="Support" Name="VER_NAME"
                                            PropertyName="SelectedValue" Type="String" />
                                         
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </cc1:ExportPanel>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
           
        </div>
 </asp:Content>