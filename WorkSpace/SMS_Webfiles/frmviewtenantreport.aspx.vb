﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class WorkSpace_SMS_Webfiles_frmviewtenantreport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocations()
            BindPropertyTypes(ddlLocation.SelectedItem.Value.ToString())
            BindPropNames()
            FillData()
            'LBTN1.Visible = False
        End If
        lblMsg.Text = String.Empty
    End Sub

    Protected Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATIONS_BY_PROPERTY_ADMIN")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        'ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
        ddlLocation.Items.Insert(0, "--All--")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindPropertyTypes(ddlLocation.SelectedItem.Value.ToString())
        ddlpropname.Items.Clear()
        txtSearch.Text = String.Empty
        ReportViewer1.Visible = False
        ddlpropname.Items.Insert(0, "--All--")
        'LBTN1.Visible = False
    End Sub


    Private Sub BindPropertyTypes(ByVal loc_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTYTYPES_BY_lOCID")
        sp.Command.AddParameter("@LOC_ID", loc_code, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count <> 0 Then
            ddlproptype.DataSource = ds
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PROPERTY_TYPE"
            ddlproptype.DataBind()
            'ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlproptype.Items.Insert(0, "--All--")
        Else
            ddlproptype.DataSource = ""
            ddlproptype.DataBind()
        End If
    End Sub

    Protected Sub ddlproptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproptype.SelectedIndexChanged
        'If ddlLocation.SelectedIndex > 0 And ddlproptype.SelectedIndex > 0 Then
        BindPropNames()
        txtSearch.Text = String.Empty
        ReportViewer1.Visible = False
        ' ddlpropname.Items.Insert(0, "--All--")
        ' LBTN1.Visible = False
        'End If
        'If ddlproptype.SelectedIndex > 0 And ddlLocation.SelectedIndex = 0 Then
        '    lblMsg.Text = "Select Location"
        '    ddlproptype.SelectedIndex = 0
        'End If
    End Sub

    Protected Sub ddlpropname_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlpropname.SelectedIndexChanged
        'btnexporttoexcel.Visible = False
        txtSearch.Text = String.Empty
        ReportViewer1.Visible = False
        ' LBTN1.Visible = False
    End Sub
     Private Sub BindPropNames()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTYNAMES_BY_LOCID_PROPERTYTYPE")
        sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
        sp.Command.AddParameter("@PROP_TYPE", ddlproptype.SelectedValue.ToString(), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count <> 0 Then
            ddlpropname.DataSource = ds
            ddlpropname.DataTextField = "PN_NAME"
            ddlpropname.DataValueField = "PN_NAME"
            ddlpropname.DataBind()
            'ddlpropname.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlpropname.Items.Insert(0, "--All--")
            'FillData()
        Else
            ddlpropname.DataSource = ""
            ddlpropname.DataBind()
            lblMsg.Text = "No Properties Available"
        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        ReportViewer1.Visible = True
        'LBTN1.Visible = True
        'ddlLocation.SelectedIndex = 0
        'ddlproptype.Items.Clear()
        ' ddlpropname.Items.Clear()

        '-REC = "0"
        ' If txtSearch.Text <> String.Empty Then
        ' REC = "1"
        'End If
        'Session("val") = REC
        GetTenantDetails()
        'btnexporttoexcel.Enabled = True
    End Sub

    'Protected Sub gvPropType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvPropType.PageIndexChanging
    '    gvPropType.PageIndex = e.NewPageIndex
    '    GetTenantDetails(Session("val").ToString())
    'End Sub

    'Protected Sub btnexporttoexcel_Click(sender As Object, e As EventArgs) Handles btnexporttoexcel.Click
    '    exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
    '    exportpanel1.FileName = "Tenant Details Report"
    'End Sub

    'Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
    '    If (ddlLocation.SelectedIndex > 0 And ddlproptype.SelectedIndex > 0 And ddlpropname.SelectedIndex > 0) Then
    '        REC = "0"
    '        Session("val") = REC
    '        GetTenantDetails(REC)
    '    End If
    'End Sub
    'Dim REC As String
    Private Sub GetTenantDetails()
        ReportViewer1.Visible = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_DETAILS_BY_LOC_PROPERTYDETAILS_NEW")
        sp.Command.AddParameter("@TEN_NAME", txtSearch.Text, DbType.String)
        sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
        sp.Command.AddParameter("@PROPERTY_TYPE", ddlproptype.SelectedValue.ToString(), DbType.String)
        sp.Command.AddParameter("@PN_NAME", ddlpropname.SelectedValue.ToString(), DbType.String)
        sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
        'sp.Command.AddParameter("@GET_ALL_REC", REC, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet



        'If ds.Tables(0).Rows.Count <> 0 Then
        Dim rds As New ReportDataSource()
        rds.Name = "ViewtenantreportDS"
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/rptViewtenantreport.rdlc")
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("Currencyparam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()

        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        'Else
        ' ReportViewer1.Visible = False
        ' End If
    End Sub

    'Protected Sub LBTN1_Click(sender As Object, e As EventArgs) Handles LBTN1.Click

    ' tab.Visible = True
    'txtSearch.Text = ""
    'btnexporttoexcel.Enabled = True
    ' End Sub

    Public Sub FillData()
        ReportViewer1.Visible = False
        If Not IsPostBack Then
            'gvPropType.PageIndex = 0
        End If
        ' REC = "1"
        ' Session("val") = REC
        txtSearch.Text = String.Empty
        GetTenantDetails()
        ReportViewer1.Visible = True
    End Sub

End Class