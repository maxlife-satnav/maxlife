Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repTWROccupancy
    Inherits System.Web.UI.Page

    Dim obj As New clsReports
     Dim objMasters As clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If

        If Not Page.IsPostBack Then

            'ReportViewer1.Visible = False
            'obj.bindLocation(ddlBuildings)
            objMasters = New clsMasters()
            objMasters.Bindlocation(ddlBuildings)
            ddlBuildings.Items(0).Text = "--All Locations--"
            ddlReqID.Items.Insert(0, "--All Towers--")
            ddlReqID.SelectedIndex = 0
            binddata()

        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        If ddlReqID.SelectedIndex = 0 Then
            'lbl2.Visible = False
        End If
        binddata()
    End Sub

    Public Sub binddata()
        Dim MTower As String = ""
        Dim MBdg As String = ""

        If ddlBuildings.SelectedValue = "--All Locations--" Then
            MBdg = ""
        Else
            MBdg = ddlBuildings.SelectedItem.Value
        End If

        If ddlReqID.SelectedValue = "--All Towers--" Then
            MTower = ""
        Else
            MTower = ddlReqID.SelectedItem.Value
        End If
        Dim sp1 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = MTower

        Dim sp2 As New SqlParameter("@vc_BuildingId", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = MBdg

        Dim sp3 As New SqlParameter("@sortexp", SqlDbType.NVarChar, 50)
        sp3.Value = "SPACE_ID"

        Dim sp4 As New SqlParameter("@sortdir", SqlDbType.NVarChar, 50)
        sp4.Value = "ASC"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_TowerWise", sp1, sp2, sp3, sp4)

        Dim rds As New ReportDataSource()
        rds.Name = "TowerOccupancyDS"
        rds.Value = dt
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/TowerOccupancyReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMPLOYEETOWER_COUNT")
        sp.Command.AddParameter("@LCM_CODE", MBdg, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", MTower, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltot.Visible = True
            lbltot.Text = "Total Employees : " & ds.Tables(0).Rows(0).Item("EMPLOYEE_ID")
            lbltot.ForeColor = Drawing.Color.Black
            lbltot.Font.Bold = True

        End If
        lbltot.Visible = True
        t2.Visible = True
    End Sub

    Protected Sub ddlBuildings_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBuildings.SelectedIndexChanged
        obj.bindTower_Locationwise(ddlReqID, ddlBuildings.SelectedValue.ToString)
        ReportViewer1.Visible = False
        t2.Visible = False
        ddlReqID.Items(0).Text = "--All Towers--"
    End Sub

    Protected Sub ddlReqID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReqID.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class