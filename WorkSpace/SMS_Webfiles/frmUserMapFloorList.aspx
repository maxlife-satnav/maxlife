<%@ Page Language="VB" AutoEventWireup="false" Title="User Floor List" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body class="amantra">
    <div id="page-wrapper" class="row" data-ng-controller="FloorLstController">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View All Maps
                        </legend>
                    </fieldset>
                    <div class="well">

                        <form id="form1">
                            <div class="row" style="padding-left: 18px">
                                <div class="box-footer text-right" id="table2" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    <br />
                                    <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div style="height: 400px;">
                                        <input id="txtCountFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                        <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script>
        app.service('FloorLstService', function ($http, $q, UtilityService) {

            this.getFloorLst = function (result) {
                var deferred = $q.defer();
                return $http.get(UtilityService.path + '/api/MaploaderAPI/GetFloorLst')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

        });
        app.controller('FloorLstController', function ($scope, $q, $http, FloorLstService, $filter, UtilityService) {

            $scope.columDefsAlloc = [
            { headerName: "Zone", field: "ZN_NAME", width: 80, cellClass: 'grid-align', filter: 'set' },
            { headerName: "State", field: "STE_NAME", width: 150, cellClass: 'grid-align', filter: 'set' },
            { headerName: "City", field: "CTY_NAME", width: 150, cellClass: 'grid-align', filter: 'set' },
            { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: "grid-align" },
            { headerName: "Tower", field: "TWR_NAME", width: 150, cellClass: "grid-align" },
            { headerName: "Floor", width: 150, cellClass: "grid-align", field: "FLR_NAME" },

            { headerName: "Last Validated On", width: 150, cellClass: "grid-align", field: "FLR_VAL_DT" },
            { headerName: "Last Valideted By", width: 150, cellClass: "grid-align", field: "FLR_VAL_BY" },
            {
                headerName: "View Map", cellClass: "grid-align", filter: 'set', pinned: 'right',
                template: "<a href='../../SMViews/Map/Maploader.aspx?lcm_code={{data.LCM_CODE}}&twr_code={{data.TWR_CODE}}&flr_code={{data.FLR_CODE}}'>View Map</a>"
            },
            //{
            //    headerName: "Release", width: 80, cellClass: "grid-align", filter: 'set',
            //    template: "<a class='btn btn-primary btn-xs custom-button-color' ng-show='data.STATUS != 1 && data.SSA_SRNREQ_ID != \"\" ' data-ng-click ='Release(data)'>Release</a>"
            //},

            ];

            var URLparams = { URL: location.pathname }
            UtilityService.ValidatePagePath(URLparams).then(function (data) {
                if (data.data == 1) //0
                {
                    window.location = "/maxlifeuat/login.aspx";
                }
            });

            $scope.gridOptions = {
                columnDefs: $scope.columDefsAlloc,
                rowData: null,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableColResize: true,
                enableCellSelection: false,
            };
            FloorLstService.getFloorLst().then(function (response) {
                if (response.data != null) {
                    $scope.gridOptions.api.setRowData(response.data);

                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            });

            $("#txtCountFilter").change(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).keydown(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).keyup(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).bind('paste', function () {
                onReq_SelSpacesFilterChanged($(this).val());
            });

            function onReq_SelSpacesFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

            $scope.GenReport = function () {
                progress(0, 'Loading...', true);

                if ($scope.gridOptions.api.isAnyFilterPresent($scope.columDefsAlloc)) {
                    $scope.GenerateFilterExcel();
                }
                else {
                    $http({
                        url: UtilityService.path + '/api/MaploaderAPI/GetViewMaps',
                        method: 'POST',
                        responseType: 'arraybuffer'

                    }).success(function (data, status, headers, config) {
                        var file = new Blob([data], {
                            type: 'application/' + 'xls'
                        });

                        //trick to download store a file having its URL
                        var fileURL = URL.createObjectURL(file);

                        $("#reportcontainer").attr("src", fileURL);
                        var a = document.createElement('a');
                        a.href = fileURL;
                        a.target = '_blank';
                        a.download = 'ViewAllMaps.' + 'xls';
                        document.body.appendChild(a);
                        a.click();
                        progress(0, '', false);
                    }).error(function (data, status, headers, config) {

                    });
                };
            }
        });
    </script>
</body>
</html>

