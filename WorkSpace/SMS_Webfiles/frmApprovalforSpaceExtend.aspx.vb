Imports System.Net
Imports System.Text
Imports Amantra.RequisitionDTON
Imports Amantra.RequisitionDALN
Imports Amantra.RequisitionBLLN
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports clsSubSonicCommonFunctions

Partial Class SpaceManagement_frmSpaceVerticalAllocations
    Inherits System.Web.UI.Page
    Dim reqBll As New RequisitionBLL()
    Dim rDTO As New RequisitionDTO()
    Dim objExtendRelease As New clsExtenedRelease()
    Dim dt As New DataTable
    Dim dtRej As New DataTable
    Dim obj As New clsMasters()
    Dim strRedirect As String = String.Empty
    Dim Email As String = String.Empty
    Dim strEmail1 As String = String.Empty
    Dim strRM As String = String.Empty
    Dim strRR As String = String.Empty
    Dim strFMG As String = String.Empty
    Dim strBUHead As String = String.Empty
    Dim strVertical As String = String.Empty
    Dim strsql As String = String.Empty
    Dim stVrm As String = String.Empty
    Dim dr As SqlDataReader
    Dim dr1 As SqlDataReader
    Dim iQry As Integer = 0
    Dim strVRM As String = String.Empty
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Visible = False
        If Not Page.IsPostBack Then
            Try
                BindCity()
                'obj.Bindlocation(ddlLocation)
                ddlTower.Items.Insert(0, "--Select--")
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Extend", "Load", ex)
            End Try

        End If
    End Sub

    Private Sub BindCity()
        ObjSubSonic.Binddropdown(ddlCity, "GETALLCITIES", "CTY_NAME", "CTY_CODE")
    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

        Try
            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)

                If chk.Checked = True Then
                    intCheckCount += 1
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    Dim txtExtendDate As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblextensionDate"), Label)
                    Dim lblspace As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblSpaceID"), Label)
                    Dim lblReqID As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblReqID"), Label)

                    If txtRemarks.Text.Trim.Length > 500 Then
                        lblMsg.Text = "Please enter remarks in less than or equal to 500 characters"
                        lblMsg.Visible = True
                        Exit Sub
                    End If


                    Dim extend As Integer
                    extend = CheckExtens(txtExtendDate.Text, lblspace.Text, lblReqID.Text, 2)
                    If extend = 1 Then
                        lblMsg.Text = "Space already Allocated for this Date"
                        lblMsg.Visible = True
                        Exit Sub



                    End If


                    'If CType(gvSpaceExtension.Rows(i).Cells(9).Text, Date) <= CType(txtExtendDate.Text, Date) Then
                    '    Dim extend As Integer
                    '    extend = CheckExtens(txtExtendDate.Text, lblspace.Text, lblReqID.Text, 1)
                    '    If extend = 1 Then
                    '        lblMsg.Text = "Space already Allocated for this Date"
                    '        lblMsg.Visible = True
                    '        Exit Sub

                    '    End If

                    'End If

                    If txtRemarks.Text.Trim.Length = 0 Then
                        lblMsg.Text = "Please enter remarks"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atlease one"
                lblMsg.Visible = True
                Exit Sub
            End If

            dt.Columns.Add("Sno", GetType(Integer))
            dt.Columns.Add("SpaceID", GetType(String))
            dt.Columns.Add("ExtDate", GetType(Date))
            dt.Columns.Add("FromDate", GetType(Date))
            dt.Columns.Add("ToDate", GetType(Date))
            dt.Columns.Add("Remarks", GetType(String))
            dt.Columns.Add("Vertical", GetType(String))
            Dim drNew As DataRow
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)
                Dim lblMail As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblAllocEmpMail"), Label)

                If chk.Checked = True Then
                    Dim lblSpaceID As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblSpaceID"), Label)
                    Dim lblReqID As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblReqID"), Label)
                    Dim lblExt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblextensionDate"), Label)
                    Dim lblToDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblToDate"), Label)
                    Dim lblFDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label9"), Label)
                    Dim lblTower As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label3"), Label)
                    Dim lblFloor As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label4"), Label)
                    Dim lblWing As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label5"), Label)
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    Dim lblVertical As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblVercode"), Label)



                    objExtendRelease.updateSpaceExtensionApprove(lblSpaceID.Text, txtRemarks.Text.Trim().Replace("'", "''"), lblExt.Text, lblReqID.Text, 4)

                    drNew = dt.NewRow
                    drNew(0) = dt.Rows.Count + 1
                    drNew(1) = lblSpaceID.Text
                    drNew(2) = lblExt.Text.Trim()
                    drNew(3) = lblFDt.Text.Trim()
                    drNew(4) = lblToDt.Text.Trim()
                    drNew(5) = txtRemarks.Text.Trim()
                    drNew(6) = lblVertical.Text.Trim()
                    dt.Rows.Add(drNew)
                    Email = lblMail.Text
                    sendmail(lblReqID.Text)
                    'sendMail(dt, lblTower.Text.Trim, lblFloor.Text.Trim(), lblWing.Text.Trim(), lblMail.Text, "Approve", strVertical)
                  
                End If

            Next
           
            If dt.Rows.Count > 0 Then
                Session("ReleaseData") = dt
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("170") & ""
            End If

            If gvSpaceExtension.Rows.Count = 0 Then
                btnApprove.Visible = False
                btnReject.Visible = False
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Approving data.", "Default", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub
    Private Function CheckExtens(ByVal dtdate As Date, ByVal space As String, ByVal req As String, ByVal mode As Integer) As Integer
        Dim extend As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECK_EXTEND_DATE")
        sp.Command.AddParameter("@DATE", dtdate, DbType.Date)
        sp.Command.AddParameter("@SPACE", space, DbType.String)
        sp.Command.AddParameter("@REQ", req, DbType.String)
        sp.Command.AddParameter("@MODE", mode, DbType.Int32)
        extend = sp.ExecuteScalar
        Return extend
    End Function
    Private Sub sendmail(reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEEXTENSIONREQAPPROVAL")
        sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@REQID", reqid, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)
                If chk.Checked = True Then
                    intCheckCount += 1
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    If txtRemarks.Text.Trim.Length > 500 Then
                        lblMsg.Text = "Please enter remarks in less than or equal to 500 characters"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If txtRemarks.Text.Trim.Length = 0 Then
                        lblMsg.Text = "Please enter remarks"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atleast one space"
                lblMsg.Visible = True
                Exit Sub
            End If
            dtRej.Columns.Add("Sno", GetType(Integer))
            dtRej.Columns.Add("SpaceID", GetType(String))
            dtRej.Columns.Add("ExtDate", GetType(Date))
            dtRej.Columns.Add("FromDate", GetType(Date))
            dtRej.Columns.Add("ToDate", GetType(Date))
            dtRej.Columns.Add("Remarks", GetType(String))
            dtRej.Columns.Add("Vertical", GetType(String))
            Dim drNew As DataRow
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)
                Dim lblMail As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblAllocEmpMail"), Label)
                If chk.Checked = True Then
                    Dim lblSpaceID As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblSpaceID"), Label)
                    Dim lblReqID As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblReqID"), Label)
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    Dim lblTower As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label3"), Label)
                    Dim lblFloor As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label4"), Label)
                    Dim lblWing As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label5"), Label)
                    Dim lblVertical As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblVercode"), Label)
                    Dim lblExt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblextensionDate"), Label)
                    Dim lblToDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblToDate"), Label)
                    Dim lblFDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label9"), Label)

                    objExtendRelease.updateSpaceExtensionApprove(lblSpaceID.Text, txtRemarks.Text.Trim().Replace("'", "''"), CType(gvSpaceExtension.Rows(i).FindControl("lblToDate"), Label).Text, lblReqID.Text, 7)

                    drNew = dtRej.NewRow
                    drNew(0) = dtRej.Rows.Count + 1
                    drNew(1) = lblSpaceID.Text
                    drNew(2) = lblExt.Text.Trim()
                    drNew(3) = lblFDt.Text.Trim()
                    drNew(4) = lblToDt.Text.Trim()
                    drNew(5) = txtRemarks.Text.Trim()
                    drNew(6) = lblVertical.Text.Trim()
                    dtRej.Rows.Add(drNew)
                    Email = lblMail.Text
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEEXTENSIONREQREJECT")
                    sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
                    sp.Command.AddParameter("@REQID", lblReqID.Text, DbType.String)
                    sp.ExecuteScalar()


                    'sendMail(dtRej, lblTower.Text, lblFloor.Text, lblWing.Text, lblMail.Text, "Reject", strVertical)


                    'strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & lblVertical.Text & "'"
                    'iQry = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL1))
                    'If iQry > 0 Then
                    '    strSQL1 = "Select VER_VRM from VERTICAL where VER_CODE='" & lblVertical.Text & "'"
                    '    dr1 = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
                    '    While dr1.Read
                    '        stVrm = dr1("VER_VRM").ToString()
                    '    End While
                    '    strsql = "Select aur_email from amantra_user where aur_id='" & stVrm & "'"
                    '    dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
                    '    While dr.Read
                    '        strVRM = dr("aur_email").ToString()
                    '    End While
                    'Else
                    '    strVRM = ConfigurationManager.AppSettings("AmantraEmailId").ToString
                    'End If

                    'If (strVertical = String.Empty) Then

                    '    strVertical = strVRM
                    'Else
                    '    strVertical = strVertical + "," + strVRM
                    'End If

                    'sendMail(lblSpaceID.Text, gvSpaceExtension.Rows(i).Cells(8).Text, gvSpaceExtension.Rows(i).Cells(7).Text, gvSpaceExtension.Rows(i).Cells(3).Text, gvSpaceExtension.Rows(i).Cells(4).Text, gvSpaceExtension.Rows(i).Cells(5).Text, Email)
                End If
            Next
            'For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
            '    Dim lblTower As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label3"), Label)
            '    Dim lblFloor As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label4"), Label)
            '    Dim lblWing As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label5"), Label)
            '    sendMail(dt, lblTower.Text.Trim, lblFloor.Text.Trim(), lblWing.Text.Trim(), Email, "Reject", strVertical)

            'Next
            If dtRej.Rows.Count > 0 Then
                Session("ReleaseData") = dtRej
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("202") & ""
            End If
            If gvSpaceExtension.Rows.Count = 0 Then
                btnApprove.Visible = False
                btnReject.Visible = False
                ddlTower.SelectedIndex = 0
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Rejecting data.", "Approval for Space Extend", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            objExtendRelease.getSpaceIDforExtension(gvSpaceExtension, ddlTower.SelectedValue)
            If gvSpaceExtension.Rows.Count = 0 Then
                btnApprove.Visible = False
                btnReject.Visible = False
                lblMsg.Text = "No requisition for approval"
                lblMsg.Visible = True
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Extend", "Load", ex)
        End Try


    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try

            objExtendRelease.getSpaceIDforExtension(gvSpaceExtension, ddlTower.SelectedValue)
            If gvSpaceExtension.Rows.Count = 0 Then
                btnApprove.Visible = False
                btnReject.Visible = False
                lblMsg.Text = "No requisition for approval"
                lblMsg.Visible = True
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Extend", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex > 0 Then
                obj.BindTowerLoc(ddlTower, ddlLocation.SelectedValue.Trim())
            Else
                gvSpaceExtension.Visible = False
                btnApprove.Visible = False
                btnReject.Visible = False
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Extend", "Load", ex)
        End Try

    End Sub

    'Private Sub sendMail(ByVal strSpaceId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal strTower As String, ByVal strFloor As String, ByVal strWing As String, ByVal strEmail As String)
    Private Sub sendMail(ByVal dt As DataTable, ByVal strTower As String, ByVal strFloor As String, ByVal strWing As String, ByVal strEmail As String, ByVal strFlag As String, ByVal strVertical As String)
        Try
            Dim to_mail As String = strEmail
            Dim cc_mail As String = Session("uemail")
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim objData As SqlDataReader

            Dim user As String = String.Empty
            'Dim objDataCurrentUser As SqlDataReader
            'Dim strCurrentUser As String = "select aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
            'objDataCurrentUser = SqlHelper.ExecuteReader(CommandType.Text, strCurrentUser)
            'While objDataCurrentUser.Read
            '    user = objDataCurrentUser("aur_known_as")
            'End While
            Dim strKnownas As String = String.Empty
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
            sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            objData = sp1.GetReader()
            While objData.Read
                strRR = objData("aur_reporting_to").ToString
                strRM = objData("aur_reporting_email").ToString
                strKnownas = objData("aur_known_as").ToString
                cc_mail = objData("aur_email").ToString
                'BCC = objData("BCC").ToString
            End While

            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS2")
            sp2.Command.AddParameter("@AUR_Email", strEmail, DbType.String)
            Dim dr As SqlDataReader
            dr = sp2.GetReader()
            While (dr.Read)
                user = dr("aur_known_as").ToString
                to_mail = dr("aur_email").ToString
            End While

            body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
            If strFlag = "Approve" Then
                body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam</b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Project space timeline extension request has been Approved by " + strKnownas + ". The details are as follows</td></tr></table>&nbsp;<br />"
            Else
                body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam</b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Project space timeline extension request has been Rejected by " + strKnownas + ".The details are as follows.</td></tr></table>&nbsp;<br />"
            End If
            body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
            body = body & "<table align='center'>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Floor Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strFloor & "</td></tr>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Wing Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strWing & "</td></tr>"
            body = body & "</table>"
            body = body & "</td></tr></table><br>"
            body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
            body = body & "<table align='center'>"
            Dim j As Integer = 3

            If dtRej.Rows.Count > 0 Then
                For i As Integer = 0 To dtRej.Rows.Count - 1
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dtRej.Rows(i)(1).ToString() & "</td></tr> "
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Extended Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dtRej.Rows(i)(2).ToString()).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dtRej.Rows(i)(3).ToString()).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dtRej.Rows(i)(4).ToString()).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dtRej.Rows(i)(5).ToString() & "</td></tr>"
                    j = j + 5
                Next
            ElseIf dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows(i)(1).ToString() & "</td></tr> "
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Extended Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dt.Rows(i)(2).ToString()).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dt.Rows(i)(3).ToString()).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dt.Rows(i)(4).ToString()).ToShortDateString() & "</td></tr>"
                    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows(i)(5).ToString() & "</td></tr>"
                    j = j + 5
                Next
            End If
            body = body & "</table>"

            body = body & "</td></tr></table>"
            body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
            body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"

            body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "

            body = body & "</table>"
            'If to_mail = String.Empty Then
            '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
            'End If


            Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
            Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
            Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
            Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
            Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
            Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
            Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

            parms10.Value = "Abcd"
            parms11.Value = body
            parms12.Value = to_mail
            If strFlag = "Approve" Then
                parms13.Value = "Project Timeline Extension Approved"
            Else
                parms13.Value = "Project Timeline Extension Rejected"
            End If
            parms14.Value = getoffsetdatetime(DateTime.Now)
            parms15.Value = "Request Submitted"
            parms16.Value = "Normal Mail"
            parms17.Value = cc_mail
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)


            Dim parms19 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
            Dim parms20 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
            Dim parms21 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
            Dim parms22 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
            Dim parms23 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
            Dim parms24 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
            Dim parms25 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim parms26 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

            parms19.Value = "Abcd"
            parms20.Value = body
            parms21.Value = strRM
            If strFlag = "Approve" Then
                parms22.Value = "Project Timeline Extension Approved"
            Else
                parms22.Value = "Project Timeline Extension Rejected"
            End If
            parms23.Value = getoffsetdatetime(DateTime.Now)
            parms24.Value = "Request Submitted"
            parms25.Value = "Normal Mail"
            parms26.Value = String.Empty

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms19, parms20, parms21, parms22, parms23, parms24, parms25, parms26)



        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
        End Try


    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'obj.Bindlocation(ddlLocation)
        Dim cty_code As String = ""
        If ddlCity.SelectedItem.Text = "--Select--" Then
            lblMsg.Text = "Please select city."
            Exit Sub
        Else
            cty_code = ddlCity.SelectedItem.Value
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = cty_code

        ObjSubSonic.Binddropdown(ddlLocation, "GETACTIVELOCATIONBY_CCODE", "LCM_NAME", "LCM_CODE", param)

    End Sub
End Class
