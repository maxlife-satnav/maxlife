<%@ Page Language="VB" AutoEventWireup="false" CodeFile="repLocationwiseOccupancy.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_repLocationwiseOccupancy" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Location Wise Occupancy Report</legend>
                    </fieldset>

                    <form id="Form1" runat="server" class="form-horizontal well">

                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="true" ValidationGroup="Val1"
                            CssClass="alert alert-danger" ForeColor="Red" />
                        <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="row" id="trCName" runat="server">
                                        <label class="col-md-5 control-label">
                                            Select Location
                                        </label>
                                        <asp:RequiredFieldValidator ID="rfvLoc" runat="server" ErrorMessage="Please Select Location" ControlToValidate="ddlReqID"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlReqID" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <asp:Button ID="btnViewReport" runat="server" Text="View Report" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/WorkSpace/SMS_WebFiles/frmRepMasters.aspx"
                                        Text="Back" />
                                </div>

                            </div>
                        </div>
                        <div id="t2" runat="server">
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lbltot" runat="server" Visible="False"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row table table table-condensed table-responsive">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</body>
</html>
