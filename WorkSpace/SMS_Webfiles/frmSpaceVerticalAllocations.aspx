<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="frmSpaceVerticalAllocations.aspx.vb" Inherits="SpaceManagement_frmSpaceVerticalAllocations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript"> 
 function maxLength(s,args)
     { 
     //alert(args.Value.length);
     if(args.Value.length >= 500)
     args.IsValid=false;
     }

function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal)
        {
            re = new RegExp(aspCheckBoxID)
            
            var theForm = document.forms['aspnetForm'];

            
            for(i = 0; i < theForm.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox')
                {
                    if (re.test(elm.name))
                    elm.checked = checkVal
                }
           }
        }
        function CheckDataGrid()
        {
            var k=0;
              var theForm = document.forms['aspnetForm'];
            for(i = 0; i < theForm.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox')
                {
                    if (elm.checked == true)
                    {
                            k=k+1;
                    }
                }
           } 
           document.getElementById('lblSelectedWs').innerText= "Count is : " + k + ".";

        }
   
        
        function colorRow(e)
					{
					
					
					var theForm = document.forms['aspnetForm'];
					
					// Annoying code to get the desired event handler from the browser.
					if (!e) e = window.event; 
					var srcEl = e.target || e.srcElement;

					var curElement = srcEl;
					while (curElement && !(curElement.tagName == "TR"))
					{
					// Keep going until we reach the parent table row
					curElement = curElement.parentNode;
					}
					if (curElement != srcEl)
					{
					if (srcEl.checked)
					{
					// Modify your colors here for a selected object
					
					curElement.style.backgroundColor = "#DEDFDE";
					}
					else
					{
					// Modify your colors for a deselected object (read: Normal)
				
					curElement.style.backgroundColor = "white";
					}
					}
					 var k=0;
            for(i = 0; i < theForm.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
               var gvName=elm.name
              if( gvName.indexOf('$')!=-1)
              {
                  gvName=gvName.substring(0,gvName.indexOf('$'))
                  // alert(gvName)
                     re = new RegExp('chk')
                   //alert(elm.name);
                   if (gvName=="gvWs")
                   {
                        if (elm.type == 'checkbox')
                        {
                            if (re.test('chk'))
                            {
                                if (elm.checked == true)
                                {
                                        k=k+1;
                                }
                            }
                        }
                    }
              }
               
           } 
           document.getElementById('lblSelectedWs').innerText= "Selected Workstation count is : " + k;
					}
function colorRowHC(e)
					{
					
					
					var theForm = document.forms['aspnetForm'];
					
					// Annoying code to get the desired event handler from the browser.
					if (!e) e = window.event; 
					var srcEl = e.target || e.srcElement;

					var curElement = srcEl;
					while (curElement && !(curElement.tagName == "TR"))
					{
					// Keep going until we reach the parent table row
					curElement = curElement.parentNode;
					}
					if (curElement != srcEl)
					{
					if (srcEl.checked)
					{
					// Modify your colors here for a selected object
					
					curElement.style.backgroundColor = "#DEDFDE";
					}
					else
					{
					// Modify your colors for a deselected object (read: Normal)
				
					curElement.style.backgroundColor = "white";
					}
					}
					 var k=0;
            for(i = 0; i < theForm.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
               re = new RegExp('chkHC')
               //alert(elm.name);
                  var gvName=elm.name
              if( gvName.indexOf('$')!=-1)
              {
                      gvName=gvName.substring(0,gvName.indexOf('$'))
                      if (gvName=="gvHC")
                      {
                        if (elm.type == 'checkbox')
                        {
                            if (re.test('chkHC'))
                            {
                                if (elm.checked == true)
                                {
                                        k=k+1;
                                }
                            }
                        }
                     }
                }
           } 
           document.getElementById('lblSelectedHC').innerText= "Selected Half Cabin count is : " + k ;
					}
					function colorRowFC(e)
					{
					// Annoying code to get the desired event handler from the browser.
					if (!e) e = window.event; 
					var srcEl = e.target || e.srcElement;

					var curElement = srcEl;
					while (curElement && !(curElement.tagName == "TR"))
					{
					// Keep going until we reach the parent table row
					curElement = curElement.parentNode;
					}
					if (curElement != srcEl)
					{
					if (srcEl.checked)
					{
					// Modify your colors here for a selected object
					
					curElement.style.backgroundColor = "#DEDFDE";
					}
					else
					{
					// Modify your colors for a deselected object (read: Normal)
				
					curElement.style.backgroundColor = "white";
					}
					}
					 var k=0;
            for(i = 0; i < form1.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
              
                  var gvName=elm.name
              if( gvName.indexOf('$')!=-1)
              {
                      gvName=gvName.substring(0,gvName.indexOf('$'))
                      if (gvName=="gvFC")
                      {
                        if (elm.type == 'checkbox')
                        {
                             if (elm.checked == true)
                                {
                                        k=k+1;
                                }
                           
                        }
                     }
                }
           } 
           document.getElementById('lblSelecetedFC').innerText= "Selected Full Cabin count is : " + k ;
					}
    </script>

    <%-- <style type="text/css" >.off { BACKGROUND-COLOR: #fffff }
	.on { BACKGROUND-COLOR: #fff }
		</style>--%>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                align="center" border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong> Vertical &nbsp;wise Space Allocation</strong></td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="vsSM" runat="server" EnableTheming="True" CssClass="clsMessage" />
                        &nbsp; &nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                        <table id="tbl" style="width: 100%" border="1" cellpadding="0" cellspacing="0">
                            <tr visible="false">
                                <td align="left" height="24" style="width: 50%">
                                    &nbsp;Select Vertical <strong><span style="font-size: 8pt; color: #ff0000">*<asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVertical" Display="None"
                                        ErrorMessage="Please select Vertical" InitialValue="--Select--"></asp:RequiredFieldValidator></span></strong></td>
                                <td align="left" style="width: 50%">
                                    <asp:DropDownList ID="ddlVertical" runat="server" Width="95%" AutoPostBack="True"
                                        CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 50%">
                                    &nbsp;Select Request Id to Allocate<font class="clsNote">* </font>&nbsp;<asp:RequiredFieldValidator
                                        ID="rfvReqid" runat="server" ControlToValidate="ddlReqid" Display="None" ErrorMessage="Please Select Request Id To Allocate "
                                        InitialValue="-- Select --"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" height="24" style="width: 50%">
                                    <asp:DropDownList ID="ddlReqid" runat="server" Width="95%" CssClass="clsComboBox"
                                        AutoPostBack="True">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr runat="server" visible="false">
                                <td align="left" height="24" style="width: 25%">
                                    &nbsp;Select Month<%-- <asp:RequiredFieldValidator ID="rfvMonth" runat="server" ControlToValidate="ddlMonth"
                                                        Display="None" ErrorMessage="Please Select Month " InitialValue="-- Select --"></asp:RequiredFieldValidator>--%></td>
                                <td align="left" class="label" style="width: 25%">
                                    &nbsp;<asp:TextBox ID="txtMonth" runat="server" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td align="left" height="24" style="width: 25%">
                                    &nbsp;Select Year &nbsp;
                                </td>
                                <td align="left" height="24" style="width: 25%">
                                    <asp:TextBox ID="txtYear" runat="server" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4" height="24">
                                    &nbsp;<asp:Button ID="btnGet" runat="server" Text="Get" CssClass="clsButton" Width="35px"
                                        Visible="False" />&nbsp;</td>
                            </tr>
                        </table>
                        <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 100%" align="center"
                            border="1" runat="server">
                            <tr>
                                <td colspan="4">
                                </td>
                            </tr>
                            <tr id="trCity" runat="server">
                                <td align="left" class="clslabel" colspan="2" style="width: 25%; height: 26px;">
                                    &nbsp;City<font class="clsNote">*</font>
                                </td>
                                <td colspan="2" style="width: 25%; height: 26px;">
                                    <asp:TextBox ID="TxtCity" Width="95%" runat="server" ReadOnly="True" CssClass="clsTextField"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Requestor's Name<font class="clsNote"><asp:RequiredFieldValidator ID="rfvRN"
                                        runat="server" ControlToValidate="txtRequesterName" Display="None" ErrorMessage="Please Enter Requestor's Name"></asp:RequiredFieldValidator></font>
                                </td>
                                <td style="width: 25%" class="label" align="left">
                                    <asp:TextBox ID="txtRequesterName" Width="95%" runat="server" ReadOnly="True" CssClass="clsTextField"></asp:TextBox>
                                </td>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Vertical
                                    <asp:RequiredFieldValidator ID="rfvVertical" runat="server" ControlToValidate="txtVertical"
                                        Display="None" ErrorMessage="Please Enter vertical"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtVertical" runat="server" CssClass="clsTextField" Visible="False"
                                        Width="5px"></asp:TextBox></td>
                                <td style="width: 25%" height="24" align="left">
                                    <asp:TextBox ID="txtVerticalName" Width="95%" runat="server" CssClass="clsTextField"
                                        ReadOnly="True"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 25%; height: 24px;" align="left">
                                    &nbsp;Preferred Location<asp:TextBox ID="txtHidLocCode" runat="server" CssClass="clsTextField"
                                        Visible="False" Width="5px"></asp:TextBox></td>
                                <td style="width: 25%; height: 24px;" class="label" align="left">
                                    <asp:TextBox ID="txtLocation" runat="server" ReadOnly="True" Width="95%" CssClass="clsTextField"></asp:TextBox></td>
                                <td style="width: 25%; height: 24px;" align="left">
                                    &nbsp;Tower<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvTower" runat="server" ControlToValidate="ddlTower"
                                        Display="None" ErrorMessage="Please Select Tower" InitialValue="-- Select --"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%; height: 24px;" align="left">
                                    <asp:DropDownList ID="ddlTower" runat="server" Width="95%" CssClass="clsComboBox"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%; height: 24px;" align="left">
                                    &nbsp;Floor<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvFlr" runat="server" ControlToValidate="ddlFloor"
                                        Display="None" ErrorMessage="Please Select Floor" InitialValue="-- Select --"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%; height: 24px;" class="label" align="left">
                                    <asp:DropDownList ID="ddlFloor" runat="server" Width="97%" CssClass="clsComboBox"
                                        AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td style="width: 25%; height: 24px;" align="left">
                                    &nbsp;Wing<font class="clsNote">*
                                        <asp:RequiredFieldValidator ID="rfvWing" runat="server" ControlToValidate="ddlWing"
                                            Display="None" ErrorMessage="Please Select Wing" InitialValue="-- Select --"></asp:RequiredFieldValidator></font></td>
                                <td style="width: 25%; height: 24px;" align="left">
                                    <asp:DropDownList ID="ddlWing" runat="server" Width="95%" CssClass="clsComboBox"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Work Stations Required
                                    <asp:RequiredFieldValidator ID="rfvWSR" runat="server" ControlToValidate="txtWSRequired"
                                        Display="None" ErrorMessage="Please Enter Work Stations Required"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%" class="label" align="left">
                                    <asp:TextBox ID="txtWSRequired" Width="95%" runat="server" ReadOnly="True" CssClass="clsTextField"></asp:TextBox>
                                </td>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Work Stations Allocated<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvWSA" runat="server" ControlToValidate="txtWSAllocated"
                                        Display="None" ErrorMessage="Please Enter Work Stations Allocated"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                            ID="revws" runat="server" ControlToValidate="txtWSAllocated" Display="None" ErrorMessage="Please Enter Work Stations Allocated  in Numbers!"
                                            ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator><asp:CompareValidator
                                                ID="cvws" runat="server" ControlToCompare="txtWSRequired" ControlToValidate="txtWSAllocated"
                                                Display="None" ErrorMessage="Please Allocate Work stations that are less than or equal to requested!"
                                                Operator="LessThanEqual" Type="Integer"></asp:CompareValidator>&nbsp;
                                    <asp:CompareValidator ID="cvwrkstations" runat="server" ControlToValidate="txtWSAllocated"
                                        Display="None" ErrorMessage="Please Enter Numerics in Work Stations " Operator="DataTypeCheck"
                                        Type="Integer"></asp:CompareValidator>&nbsp;
                                </td>
                                <td style="width: 25%" height="24" align="left">
                                    <div onmouseover="Tip('Enter number of  workstations allocated')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtWSAllocated" Width="95%" runat="server" MaxLength="5" CssClass="clsTextField"></asp:TextBox></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Half Cabins Required
                                    <asp:RequiredFieldValidator ID="rfvHCR" runat="server" ControlToValidate="txtHalfCabin"
                                        Display="None" ErrorMessage="Please Enter Half Cabins Required"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%; height: 24px;" class="label" align="left">
                                    <asp:TextBox ID="txtHalfCabin" Width="95%" runat="server" ReadOnly="True" CssClass="clsTextField"></asp:TextBox></td>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Half Cabins Allocated<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvHCA" runat="server" Display="None" ErrorMessage="Please Enter Half Cabins Allocated"
                                        ControlToValidate="txtHCAllocated"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                            ID="revhc" runat="server" ControlToValidate="txtHCAllocated" Display="None" ErrorMessage="Please Enter half cabins Allocated in numbers !"
                                            Height="16px" ValidationExpression="^[0-9]*" Width="72px"></asp:RegularExpressionValidator><asp:CompareValidator
                                                ID="cvhc" runat="server" ControlToCompare="txtHalfCabin" ControlToValidate="txtHCAllocated"
                                                Display="None" ErrorMessage="Please Allocate Half cabins that are less than or equal to requested!"
                                                Height="16px" Operator="LessThanEqual" Type="Integer" Width="72px"></asp:CompareValidator>&nbsp;
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtHCAllocated"
                                        Display="None" ErrorMessage="Please Enter Numerics in Half Cabins" Operator="DataTypeCheck"
                                        Type="Integer"></asp:CompareValidator></td>
                                <td style="width: 25%" height="24" align="left">
                                    <div onmouseover="Tip('Enter number of  workstations allocated')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtHCAllocated" Width="95%" runat="server" MaxLength="5" CssClass="clsTextField"></asp:TextBox></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%; height: 29px;" align="left">
                                    &nbsp;Full Cabins Required
                                    <asp:RequiredFieldValidator ID="rfvFCR" runat="server" ControlToValidate="txtFullCabin"
                                        Display="None" ErrorMessage="Please Enter Full Cabins required"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%; height: 29px;" class="label" align="left">
                                    <asp:TextBox ID="txtFullCabin" Width="95%" runat="server" ReadOnly="True" CssClass="clsTextField"></asp:TextBox>
                                </td>
                                <td style="width: 25%; height: 29px;" align="left">
                                    &nbsp;Full Cabins Allocated<font class="clsNote">*</font>&nbsp;
                                    <asp:RequiredFieldValidator ID="rfvFCA" runat="server" Display="None" ErrorMessage="Please Enter Full Cabins Allocated"
                                        ControlToValidate="txtFCAllocated"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regfc" runat="server" ControlToValidate="txtFCAllocated"
                                        Display="None" ErrorMessage="Please Enter number of Full cabins Allocated in numbers!"
                                        ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="cvFC" runat="server" ControlToCompare="txtFullCabin" ControlToValidate="txtFCAllocated"
                                        ErrorMessage="Please Allocate Full cabins that are less than or equal to requested!"
                                        Operator="LessThanEqual" Type="Integer" Display="None"></asp:CompareValidator>&nbsp;
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtFCAllocated"
                                        Display="None" ErrorMessage="Please Enter Numerics in Full Cabins" Operator="DataTypeCheck"
                                        Type="Integer"></asp:CompareValidator></td>
                                <td style="width: 25%; height: 29px;" align="left">
                                    <div onmouseover="Tip('Enter number of  full cabins allocated')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtFCAllocated" Width="95%" runat="server" MaxLength="5" CssClass="clsTextField"></asp:TextBox></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%; height: 24px;" align="left">
                                    &nbsp;From Date
                                    <asp:RequiredFieldValidator ID="rfvFD" runat="server" Display="None" ErrorMessage="Please Enter From Date"
                                        ControlToValidate="txtFromDate"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%; height: 24px;" class="label" align="left">
                                    <asp:TextBox ID="txtFromDate" Width="95%" runat="server" ReadOnly="True" CssClass="clsTextField"></asp:TextBox></td>
                                <td style="width: 25%; height: 24px;" align="left">
                                    &nbsp;To Date
                                    <asp:RequiredFieldValidator ID="rfvTD" runat="server" Display="None" ErrorMessage="Please Enter To Date"
                                        ControlToValidate="txtToDate"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%; height: 24px;" align="left">
                                    <asp:TextBox ID="txtToDate" Width="95%" runat="server" ReadOnly="True" CssClass="clsTextField"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%" align="left" height="24" colspan="2">
                                    &nbsp;Available Work Stations<font class="clsNote">* </font>
                                    <br />
                                    <asp:Label ID="lblAvail_Occp_Count_WST" CssClass="clsMessage" runat="server"></asp:Label>
                                    &nbsp;&nbsp;<br />
                                    <br />
                                    &nbsp;<asp:Label ID="lblSelectedWs" runat="server" CssClass="clsMessage"></asp:Label>
                                    <asp:Label ID="lblWSCount" runat="server" CssClass="clsMessage"></asp:Label></td>
                                <td align="left" colspan="3" height="24" valign="top">
                                    <div onmouseover="Tip('Select number of  workstations allocated')" onmouseout="UnTip()">
                                        <asp:Panel ID="panWS" runat="server" Height="100px" ScrollBars="Vertical" Width="100%">
                                            <asp:GridView ID="gvWs" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                OnRowDataBound="gvWs_RowDataBound" Height="98%" Width="90%" EmptyDataText="No Work Stations Available ">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk',this.checked)">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <input id="chk" type="checkbox" runat="server" value='<% #bind("SpaceID") %>' />
                                                            <%-- <asp:CheckBox runat="server" ID="chk" />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblSpace" Text='<% #bind("SpaceID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    &nbsp;Available Half Cabins&nbsp;<br />
                                    <br />
                                    <asp:Label ID="lblAvail_Occp_Count_HCB" CssClass="clsMessage" runat="server"></asp:Label>
                                    &nbsp;&nbsp;<br />
                                    <br />
                                    <asp:Label ID="lblSelectedHC" runat="server" CssClass="clsMessage"></asp:Label>&nbsp;
                                    <asp:Label ID="lblHCCount" runat="server" CssClass="clsMessage"></asp:Label></td>
                                <td align="left" height="24" colspan="2" valign="top">
                                    <div onmouseover="Tip('Select number of  halfcabins allocated')" onmouseout="UnTip()">
                                        <asp:Panel ID="panHC" runat="server" Height="100px" ScrollBars="Vertical" Width="100%">
                                            <asp:GridView ID="gvHC" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                OnRowDataBound="gvHC_RowDataBound" Height="98%" Width="90%" EmptyDataText="No Half Cabins Available">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk',this.checked)">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <input id="chkHC" type="checkbox" runat="server" value='<% #bind("SpaceID") %>' />
                                                            <%-- <asp:CheckBox runat="server" ID="chk" />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSpace" runat="server" Text='<% #bind("SpaceID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    &nbsp;Available Full Cabins &nbsp;
                                    <br />
                                    <br />
                                    <asp:Label ID="lblAvail_Occp_Count_FCB" CssClass="clsMessage" runat="server"></asp:Label>
                                    &nbsp;&nbsp;<br />
                                    <br />
                                    &nbsp;<asp:Label ID="lblSelecetedFC" runat="server" CssClass="clsMessage"></asp:Label>
                                    <asp:Label ID="lblFCCount" runat="server" CssClass="clsMessage"></asp:Label></td>
                                <td align="left" class="label" colspan="2" valign="top">
                                    <div onmouseover="Tip('Select number of  full cabins allocated')" onmouseout="UnTip()">
                                        <asp:Panel ID="panFC" runat="server" Height="100px" ScrollBars="Vertical" Width="98%">
                                            <asp:GridView ID="gvFC" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                OnRowDataBound="gvFC_RowDataBound" Height="98%" Width="90%" EmptyDataText="No Full Cabins Available">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk',this.checked)">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <input id="chkFC" type="checkbox" runat="server" value='<% #bind("SpaceID") %>' />
                                                            <%-- <asp:CheckBox runat="server" ID="chk" />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSpace" runat="server" Text='<% #bind("SpaceID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    &nbsp;Space Preference in Sft (if any)
                                    <asp:CompareValidator ID="cvLab" runat="server" ControlToValidate="txtLabSpace" Display="None"
                                        ErrorMessage="Please Enter Numerics in Lab Space" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator></td>
                                <td align="center" class="label" colspan="2">
                                    <asp:TextBox ID="txtLabSpace" runat="server" CssClass="clsTextField" Width="95%">0</asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Requestor's Remarks
                                    <asp:RequiredFieldValidator ID="rfvRRem" runat="server" Display="None" ErrorMessage="Please Enter Requestor's Remarks"
                                        ControlToValidate="txtRequestorRemarks"></asp:RequiredFieldValidator></td>
                                <td style="width: 25%" class="label" align="left">
                                    <asp:TextBox ID="txtRequestorRemarks" runat="server" TextMode="multiLine" Width="95%"
                                        ReadOnly="True" CssClass="clsTextField" Height="35px"></asp:TextBox>
                                </td>
                                <td style="width: 25%" align="left" height="24">
                                    &nbsp;Remarks<font class="clsNote">*<asp:RequiredFieldValidator ID="rfvAR" runat="server"
                                        Display="None" ErrorMessage="Please Enter Remarks" ControlToValidate="txtAdminRemarks"></asp:RequiredFieldValidator></font>
                                    <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                        ControlToValidate="txtAdminRemarks" Display="None" ErrorMessage="Remarks Should be less than 500 characters !"></asp:CustomValidator>
                                </td>
                                <td style="width: 25%" height="24" class="label" align="left">
                                    <div onmouseover="Tip('Enter remarks in 500 characters')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="multiLine" Width="95%"
                                            CssClass="clsTextField" Height="35px"></asp:TextBox></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="label" colspan="4" height="24">
                                    <asp:CheckBox ID="chkClsRequest" runat="server" Text="Request is Completed. " /></td>
                            </tr>
                            <tr id="Tr1" runat="server" visible="false">
                                <td style="width: 25%" height="24" align="left">
                                    &nbsp;Allocate this Space To</td>
                                <td style="width: 25%" height="24" align="left" class="label">
                                    <asp:DropDownList ID="ddlAllocSpaceTo" runat="server" Width="95%" CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4" headers="24px">
                                    <asp:Button ID="btnAllocate" runat="server" Text="Allocate" CssClass="clsButton" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
                <tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
