<%@ Page Language="VB" AutoEventWireup="false" ValidateRequest="true" CodeFile="frmDashBoardViewSpaceRequisition.aspx.vb"
    Inherits="frmDashBoardViewSpaceRequisition" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Cost Center Wise Space Requisition
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="vsSM" runat="server" CssClass="alert alert-danger" ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" Font-Bold="True" Visible="False" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <h6>
                                            <label class="col-md-5 control-label">Space Requisition</label></h6>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="lblSpaceReqID" runat="server" class="col-md-5 control-label" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <h6>
                                            <label class="col-md-5 control-label">Status of the Requisition</label></h6>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="lblReqStatus" runat="server" class="col-md-5 control-label" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trCName" runat="server">
                                        <label class="col-md-5 control-label">City<span style="color: red;">*</span> </label>
                                        <asp:RequiredFieldValidator
                                            ID="rfvCity" runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLoc" Display="None"
                                            ErrorMessage="Please Select Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLoc" runat="server" CssClass="selectpicker" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlLoc_SelectedIndexChanged" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Tower<span style="color: red;">*</span> </label>
                                        <asp:RequiredFieldValidator ID="rfvT1"
                                            runat="server" ErrorMessage="Please Select Tower" ControlToValidate="ddlTower1"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlTower1" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Country</label>
                                        <asp:TextBox ID="txtCity1" runat="server" Width="2px" ReadOnly="True" CssClass="clsTextField"
                                            Visible="False"></asp:TextBox>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCountry1" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtLocation1" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <h5>
                                            <label class="col-md-12 control-label">Full Cabins / Half Cabins / Work Stations Required</label></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelVertical" runat="server" Text="" class="col-md-5 control-label"><span style="color: red;">*</span></asp:Label>
                                        <asp:Label ID="lblVertical" runat="server" Text="Label" class="col-md-5 control-label"></asp:Label>
                                        <div class="col-md-7">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelCostcenter" runat="server" Text="" class="col-md-5 control-label"><span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                            runat="server" ErrorMessage="Please Select Cost Center " ControlToValidate="ddlCostcenter"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCostcenter" runat="server" CssClass="selectpicker" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="trDep" runat="server" visible="false" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Department<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            runat="server" ErrorMessage="Please Select Department " ControlToValidate="ddlDept"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvFD" runat="server" ErrorMessage="Please Enter From Date"
                                            ControlToValidate="txtFromdate" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFromdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTD" runat="server" ErrorMessage="Please Enter Todate"
                                            ControlToValidate="txtTodate" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Work Stations<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvWS" runat="server" ErrorMessage="Please Enter Work Stations in Numbers"
                                            ControlToValidate="txtWstations" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtWstations" runat="server" CssClass="form-control"
                                                MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Half Cabins<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="rfvHC" runat="server" ErrorMessage="Please Enter number of   Half Cabins in Numbers"
                                            ControlToValidate="txtHcabins" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtHcabins" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Full Cabins<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please  Enter Full Cabins in Numbers"
                                            ControlToValidate="txtFcabins" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtFcabins" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label><%--<asp:CustomValidator ID="cvRemarks"
                                            runat="server" ClientValidationFunction="maxLength" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Remarks Shoud be less than 500 characters !"></asp:CustomValidator>--%>
                                        <asp:RequiredFieldValidator ID="rfvRem" runat="server" ErrorMessage="Please Enter Remarks"
                                            Display="None" ControlToValidate="txtRemarks"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter remarks in 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" MaxLength="500"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Update" CssClass="btn btn-primary custom-button-color" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary custom-button-color" />
                                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color"
                                        CausesValidation="False" />
                                    <asp:TextBox ID="txtDate" runat="server" Visible="False"
                                        BorderWidth="0px"></asp:TextBox>
                                    <asp:Label ID="lblVertcode" Visible="false" runat="server" Text="Label"></asp:Label>
                                    <input id="Hidden1" runat="server" type="hidden" />
                                    <input id="hdnPrjName" runat="server" type="hidden" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
