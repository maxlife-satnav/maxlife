﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Utility_Payments.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Utility_Payments" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>
            <div>
                <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                        <td align="center" width="100%">
                            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                                ForeColor="Black">Add Utility Payments
             <hr align="center" width="60%" /></asp:Label></td>
                    </tr>
                </table>
                <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                    <table id="table4" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                        <tr>
                            <td align="left" width="100%" colspan="3">
                                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                            <td width="100%" class="tableHEADER" align="left">
                                <strong>&nbsp;Add Utility Payments</strong>
                            </td>
                            <td>
                                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                        </tr>
                        <tr>
                            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                            <td align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                                    ForeColor="" ValidationGroup="Val1" />
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                                <asp:TextBox ID="txtstore" runat="server" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtstore1" runat="server" Visible="false"></asp:TextBox>
                                <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                                    <tr>
                                        <td align="left" style="height: 16px; width: 50%;">&nbsp;Select  Utility <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="ddlutility"
                                                Display="none" ErrorMessage="Please Select Utility  !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                        </td>
                                        <td align="left" style="height: 16px; width: 50%;">
                                            <asp:DropDownList ID="ddlutility" runat="server" CssClass="clsComboBox" Width="99%"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clslabel" style="width: 50%; height: 16px">&nbsp;Select Location<font class="clsNote">*<asp:RequiredFieldValidator ID="rfvLocation"
                                            runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator></font></td>
                                        <td align="left" style="width: 43%; height: 16px">
                                            <asp:DropDownList ID="ddlLocation" runat="server" Width="97%" AutoPostBack="True"
                                                CssClass="clsComboBox">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="clslabel" style="width: 50%; height: 16px">&nbsp;Select Tower <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvTower" runat="server" ControlToValidate="ddlTower"
                                                Display="None" ErrorMessage="Please Select Tower " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                        <td align="left" style="width: 43%; height: 16px">
                                            <asp:DropDownList ID="ddlTower" runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                            </asp:DropDownList></td>
                                    </tr>

                                    <tr runat="server">
                                        <td align="left" style="height: 11px; width: 50%;">&nbsp;Select Floor
                                        <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlFloor"
                                                Display="none" ErrorMessage="Please Select Floor  !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>


                                        </td>
                                        <td align="left" style="width: 43%; height: 11px">
                                            <asp:DropDownList ID="ddlFloor" runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox"
                                                ToolTip="Select the Floor">
                                            </asp:DropDownList></td>
                                    </tr>

                                    <tr>
                                        <td align="left" style="height: 16px; width: 50%;">&nbsp;Enter Units <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtunits"
                                                Display="none" ErrorMessage="Please Enter Units!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtunits"
                                                Display="None" ErrorMessage="Invalid Units" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="height: 16px; width: 50%;">
                                            <asp:TextBox ID="txtunits" runat="server" CssClass="clsTextField" Width="99%" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td align="left" style="height: 16px; width: 50%;">&nbsp;Enter Amount <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtutility"
                                                Display="none" ErrorMessage="Please Enter Utility Amount  !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revmaxcapacity" runat="server" ControlToValidate="txtutility"
                                                Display="None" ErrorMessage="Invalid Amount" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="height: 16px; width: 50%;">
                                            <asp:TextBox ID="txtutility" runat="server" CssClass="clsTextField" Width="99%" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>

                                       <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    From Date <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFdate"
                                                Display="none" ErrorMessage="Please Select From Date  !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtFdate" runat="server" CssClass="clsTextField"></asp:TextBox>
                                   <%-- <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFdate"
                                        Format="dd-MMM-yyyy">
                                    </cc1:CalendarExtender>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    To Date <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtTdate"
                                                Display="none" ErrorMessage="Please Select To Date  !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtTdate" runat="server" CssClass="clsTextField"></asp:TextBox>
                                   <%-- <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTdate"
                                        Format="dd-MMM-yyyy">
                                    </cc1:CalendarExtender>--%>
                                </td>
                            </tr>





                                    <tr>
                                        <td colspan="3" align="center" style="height: 39px">
                                            <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Add" ValidationGroup="Val1"
                                                CausesValidation="true" />
                                            <asp:Button ID="btnmodify" CssClass="button" runat="server" Text="Modify" ValidationGroup="Val1"
                                                CausesValidation="true" />

                                        </td>
                                    </tr>
                                </table>

                                <table id="Table5" width="100%" runat="Server" cellpadding="0" cellspacing="0" border="1">
                                    <tr>
                                        <td align="center" style="height: 20px">
                                            <asp:GridView ID="gvPropType" runat="server" AllowPaging="True" AllowSorting="False"
                                                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                                PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Records Found">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>

                                                       <asp:TemplateField Visible="false" >
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsno" runat="server"  Text='<%#Eval("SNO")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="false" >
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblID" runat="server"  Text='<%#Eval("UTILITY_ID")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Utility">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblutility" runat="server"  Text='<%#Eval("PN_UTILITY")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblloc" runat="server"  Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                              <asp:Label ID="lblloccode" runat="server" Visible="false"   Text='<%#Eval("UTILITY_BDG")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tower">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbltwr" runat="server"  Text='<%#Eval("TWR_NAME")%>'></asp:Label>
                                                              <asp:Label ID="lbltwrcode" runat="server" Visible="false"   Text='<%#Eval("UTILITY_TWR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Floor">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblflr" runat="server"  Text='<%#Eval("FLR_NAME")%>'></asp:Label>
                                                              <asp:Label ID="lblflrcode" runat="server" Visible="false"   Text='<%#Eval("UTILITY_FLR")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Units">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblunits" runat="server"  Text='<%#Eval("UTILITY_UNITS")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblamount" runat="server"  Text='<%#Eval("UTILITY_AMOUNT")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="From Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblfdate" runat="server" Text='<%#Bind("FROM_DATE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                       <asp:TemplateField HeaderText="To Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbltdate" runat="server" Text='<%#Bind("TO_DATE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:ButtonField Text="EDIT" CommandName="EDIT" />
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 10px; height: 17px;">
                                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                            <td style="height: 17px">
                                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

