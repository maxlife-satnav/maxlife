<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmUtilizationdetails.aspx.vb" Inherits="SpaceManagement_frmSpaceVerticalAllocations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
    <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">   <asp:Label ID="lblHead" runat="server" CssClass="clsHead"  Width="95%" Font-Underline="False" ForeColor="Black">Space Connect</asp:Label></td>
                        </tr>
                        </table>

     
    
        
<asp:Panel ID="PNLCONTAINER"  runat="server" Width="95%" Height="100%">
             
           <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align:top;" width="95%"  align="center" border="0">
                    <tr>
						<td style="height: 27px"><img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
						<td width="100%"  class="tableHEADER" style="height: 27px" align="left"  ><strong> Utilization Report</strong></td>
						<td style="height: 27px"><img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
					</tr>
								
                <tr>
            			<td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
            			<td align="center">
            			<table id ="tblDetails" cellspacing="0" cellpadding="0" style ="width :98%"                            align="center" border="0">
            	<tr>
            	</tr>		
                        <tr>
                        <td style="width :78%; height: 24px;" align ="left" >
                            Building wise Space Utilization Report</td>
                         <td style="width :24%; height: 24px;" class ="label">
                             <asp:Button ID="Button1" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                        </tr>
                         <tr>
                        <td style="width :78%; height: 24px;" align ="left" >
                            Building wise Space Occupancy Report</td>
                         <td style="width :24%; height: 24px;" class ="label" >
                             <asp:Button ID="Button2" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                        </tr>
                        <tr>
                            <td style="width :78%; height: 23px;" align ="left" >
                                Building wise Space Extended Report</td>
                             <td style="width :24%; height: 23px;" class ="label" >
                                 <asp:Button ID="Button3" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                        </tr>
                       <tr>
                        <td style="width :78%" align ="left" height ="24px" >
                            Tower wise Space Utilization Report</td>
                         <td style="width :24%" class ="label">
                             <asp:Button ID="Button4" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                        </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Building wise Charge Back Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button5" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                        <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Tower wise Occupancy Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button6" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 78%; height: 24px;">
                                    Floor wise Occupancy Report</td>
                                <td class="label" style="width: 24%; height: 24px;">
                                    <asp:Button ID="Button7" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Wing wise Occupancy Report</td>
                                <td class="label" style="width: 24%; height: 24px;">
                                    <asp:Button ID="Button8" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Location wise Occupancy Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button9" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Vertical wise Occupancy Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button10" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Vertical wise Allocation Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button11" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Building wise Allocation Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button12" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Building wise Requisition Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button13" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                            <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Location wise Requisition Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button14" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                                <tr>
                                <td align="left" height="24" style="width: 78%">
                                    Offshore And Onsite Employee Report</td>
                                <td class="label" style="width: 24%">
                                    <asp:Button ID="Button15" runat="server" CssClass="clsButton" Text="View" Width="83%" /></td>
                            </tr>
                        
                        
                
					</table> 						
					              </td>  
					                <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                       </td>
           </tr>    
                  <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>   
           
		                                 <tr>
                       
               
              </table>
           
        </asp:Panel>
       
    </div>
    </asp:Content>