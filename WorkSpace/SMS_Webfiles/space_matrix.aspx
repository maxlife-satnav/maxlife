<%@ Page Language="VB" AutoEventWireup="false" CodeFile="space_matrix.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_space_matrix" Title="Escalation Matrix for Space" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Matrix</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-md-6">
                                <label class="col-md-2 btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true" />
                                    Add</label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true" />
                                    Modify</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Space Requisition<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="SpcReq" runat="server" ControlToValidate="ddlSpc"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Space Requisition" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlSpc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Level 1 Approval<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="lvl1" runat="server" ControlToValidate="ddllvl1"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Level 1 Approval" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddllvl1" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Level 2 Approval<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="lvl2" runat="server" ControlToValidate="ddllvl2"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Level 2 Approval" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddllvl2" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Space Allocation<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="allc" runat="server" ControlToValidate="ddlalloc"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Space Allocation" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlalloc" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Employee Mapping<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="maps" runat="server" ControlToValidate="ddlmap"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Employee Mapping" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlmap" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Space Release<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rls" runat="server" ControlToValidate="ddlrls"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Space Release" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlrls" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Space Extension<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="Ext" runat="server" ControlToValidate="ddlext"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Space Extension" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlext" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="stat" runat="server" ControlToValidate="ddlsta"
                                            InitialValue="--Select--" Display="None" ErrorMessage="Please Select Status" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlsta" runat="server" Visible="True" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtrem" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true"
                                        ValidationGroup="Val1" />
                                    <asp:Button ID="BtnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" CausesValidation="false" />
                                    <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="false" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvitems" runat="server" AllowSorting="true" AllowPaging="true"
                                    AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Space Requisition">
                                            <ItemTemplate>
                                                <asp:Label ID="empid" runat="server" Text='<%#Eval("SM_SPC_REQ")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Level 1 Approval">
                                            <ItemTemplate>
                                                <asp:Label ID="level1appr" runat="server" Text='<%#Eval("SM_L1_APPR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText=" Level 2 Approval">
                                            <ItemTemplate>
                                                <asp:Label ID="level2appr" runat="server" Text='<%#Eval("SM_L2_APPR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Space Allocation ">
                                            <ItemTemplate>
                                                <asp:Label ID="spaceallocation" runat="server" Text='<%#Eval("SM_SPC_ALLOC_APPR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Mapping">
                                            <ItemTemplate>
                                                <asp:Label ID="empmap" runat="server" Text='<%#Eval("SM_EMP_MAP_APPR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Space Release">
                                            <ItemTemplate>
                                                <asp:Label ID="spacerel" runat="server" Text='<%#Eval("SM_SPC_REL_APPR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Space Extension">
                                            <ItemTemplate>
                                                <asp:Label ID="spcext" runat="server" Text='<%#Eval("SM_SPC_EXTN_APPR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="status" runat="server" Text='<%#Eval("STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
