﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class WorkSpace_SMS_Webfiles_Report
    Inherits System.Web.UI.Page

    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            txtFdate.Text = getoffsetdatetime(DateTime.Now).AddYears(-1).Date
            txtTdate.Text = getoffsetdatetime(DateTime.Now).Date
            BindGrid()

        End If
        txtTdate.Attributes.Add("readonly", "readonly")
        txtFdate.Attributes.Add("readonly", "readonly")
    End Sub
    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click


        If CDate(txtFdate.Text) > CDate(txtTdate.Text) Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('To Date should be greater than from date');", True)


        End If
        BindGrid()

    End Sub

    Private Sub BindGrid()

        
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SEAT_COST")
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.Date)
        Dim ds As DataSet = sp.GetDataSet()


        Dim rds As New ReportDataSource()
        rds.Name = "TotalCostDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/TotalCostReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p2 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

    End Sub
End Class
