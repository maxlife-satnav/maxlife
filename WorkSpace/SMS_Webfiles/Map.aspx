<%@ Page Language="VB" MasterPageFile="~/MapMasterPage.master" AutoEventWireup="false"
    CodeFile="Map.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Map" Title="View Map" %>

<%@ Register Src="Controls/query.ascx" TagName="query" TagPrefix="uc1" %>
<%@ Register Src="Controls/Map_Departments.ascx" TagName="Map_Departments" TagPrefix="uc2" %>
<%@ Register Src="Controls/ConsolidateReport.ascx" TagName="ConsolidateReport" TagPrefix="uc3" %>
<%@ Register Src="Controls/SelectLocation.ascx" TagName="SelectLocation" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
        <tr>
            <td colspan="2" align="left">
                <table cellspacing="0" cellpadding="0" width="58" border="0">
                    <tr valign="top">
                        <td width="58" >
                            <table cellspacing="2" cellpadding="2" border="0">
                                <tr>
                                    <td>
                                        <b>Legend</b></td>
                                </tr>
                            </table>
                            <table cellspacing="1" cellpadding="1" border="0" align="left">
                                <tr>
								<td>
                                        Allocated</td>
                                    <td width="50">
                                        &nbsp;</td>
                                    <td bgcolor="#FFFF00">
                                        &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        Occupied</td>
                                    <td width="50">
                                        &nbsp;</td>
                                    <td bgcolor="#FF0000">
                                        &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                   <td>
                                        Vacant</td>
                                    <td width="50">
                                        &nbsp;</td>
                                    <td bgcolor="#00FF00">
                                        &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="70%" valign="top" style="height: 260px">
                <iframe id="midFrame" height="500px" width="97%" frameborder="0" scrolling="no" runat="server">
                </iframe>
            </td>
            <td width="30%" align="left" valign="top" style="height: 260px">
                <table width="100%" border="1" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <uc4:SelectLocation ID="SelectLocation1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <uc1:query ID="Query1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <uc2:Map_Departments ID="Map_Departments1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            &nbsp;
                            <uc3:ConsolidateReport ID="ConsolidateReport1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
