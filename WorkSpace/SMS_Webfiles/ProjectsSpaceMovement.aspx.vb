﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader

Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_ProjectsSpaceMovement
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim verticalreqid As String
    Dim strRedirect As String
    Dim cntwst As Int32 = 0
    Dim cnthcb As Int32 = 0
    Dim cntfcb As Int32 = 0

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        'txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        'txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")



        If Not IsPostBack Then
            trgrid.Visible = False
            trbutton.Visible = False
            loadVertical()
            objsubsonic.Binddropdown(ddlCity, "GET_ALLCITY", "CTY_NAME", "CTY_CODE")
            'objsubsonic.Binddropdown(ddltocity, "GET_ALLCITY", "CTY_NAME", "CTY_CODE")
        End If
    End Sub

    Public Sub loadVertical()
        Dim UID As String = ""
        UID = Session("uid")
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = UID
        objsubsonic.Binddropdown(ddlfrmPrj, "GET_COSTCENTER_AURID", "COST_CENTER_NAME", "COST_CENTER_CODE", param)
        objsubsonic.Binddropdown(ddltoprj, "GET_COSTCENTER_AURID", "COST_CENTER_NAME", "COST_CENTER_CODE", param)
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCity.SelectedItem.Value
            objsubsonic.Binddropdown(ddlfrmloc, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
        End If
    End Sub

    'Protected Sub ddltocity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltocity.SelectedIndexChanged
    '    If ddltocity.SelectedIndex > 0 Then
    '        Dim param(0) As SqlParameter
    '        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
    '        param(0).Value = ddltocity.SelectedItem.Value
    '        objsubsonic.Binddropdown(ddltoloc, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    '    End If
    'End Sub

    Protected Sub ddlfrmLoc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlfrmloc.SelectedIndexChanged
        If ddlfrmloc.SelectedIndex > 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@FLR_CTY_ID", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCity.SelectedItem.Value
            param(1) = New SqlParameter("@FLR_LOC_ID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlfrmloc.SelectedItem.Value
            objsubsonic.Binddropdown(ddlfrmfloor, "GET_FLOOR_BYLOCATIONCITY", "FLR_NAME", "FLR_CODE", param)
        End If
    End Sub

    'Protected Sub ddltoloc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltoloc.SelectedIndexChanged
    '    If ddltoloc.SelectedIndex > 0 Then
    '        Dim param(1) As SqlParameter
    '        param(0) = New SqlParameter("@FLR_CTY_ID", SqlDbType.NVarChar, 200)
    '        param(0).Value = ddltocity.SelectedItem.Value
    '        param(1) = New SqlParameter("@FLR_LOC_ID", SqlDbType.NVarChar, 200)
    '        param(1).Value = ddltoloc.SelectedItem.Value
    '        objsubsonic.Binddropdown(ddltofloor, "GET_FLOOR_BYLOCATIONCITY", "FLR_NAME", "FLR_CODE", param)
    '    End If
    'End Sub

    Protected Sub btnavail_Click(sender As Object, e As EventArgs) Handles btnavail.Click
        If ddlCity.SelectedIndex > 0 And ddlfrmloc.SelectedIndex > 0 Then
            BindAvailspaces()
            trgrid.Visible = True
        End If
    End Sub

    Private Sub BindAvailspaces()


        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlfrmloc.SelectedItem.Value
        param(2) = New SqlParameter("@flr_code", SqlDbType.NVarChar, 200)
        param(2).Value = ddlfrmfloor.SelectedItem.Value

        param(3) = New SqlParameter("@PROJECT", SqlDbType.NVarChar, 200)
        param(3).Value = ddlfrmPrj.SelectedItem.Value

        objsubsonic.BindGridView(gdavail, "GET_AVAIL_SPACES_FOR_PROJECT", param)
        If gdavail.Rows.Count > 0 Then
            lblMsg.Text = ""
            trbutton.Visible = True
            'For i As Integer = 0 To gdavail.Rows.Count - 1
            '    Dim ddlavail As DropDownList = CType(gdavail.Rows(i).FindControl("ddlavail"), DropDownList)
            '    Dim ds As New DataSet
            '    Dim sp1 As New SqlParameter("@LOC", SqlDbType.NVarChar, 50, ParameterDirection.Input)
            '    sp1.Value = ddltoloc.SelectedItem.Value
            '    'Dim sp2 As New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 50, ParameterDirection.Input)
            '    'sp2.Value = ddlvertical.SelectedItem.Value
            '    'Dim sp3 As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            '    'sp3.Value = txtFrmDate.Text
            '    'Dim sp4 As New SqlParameter("@TODATE", SqlDbType.DateTime)
            '    'sp4.Value = txtToDate.Text
            '    'Dim sp5 As New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            '    'sp5.Value = ftime
            '    'Dim sp6 As New SqlParameter("@TOTIME", SqlDbType.DateTime)
            '    'sp6.Value = ttime

            '    ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_VACANT_SPACES", sp1)
            '    ddlavail.DataSource = ds
            '    ddlavail.DataTextField = "SPC_ID"
            '    ddlavail.DataValueField = "SPC_ID"
            '    ddlavail.DataBind()
            '    ddlavail.Items.Insert(0, "--Select--")
            'Next

        Else
            lblMsg.Text = "No seats found"
            trbutton.Visible = False
        End If
    End Sub

  
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim count As Integer
        verticalreqid = objsubsonic.REQGENARATION_REQ(ddlfrmloc.SelectedItem.Value, "ID", Session("TENANT") & "." , "project_movement")
        For Each row As GridViewRow In gdavail.Rows
            Dim spc_layer As Label = CType(row.FindControl("lblspacetype"), Label)
            Dim chkSelect As CheckBox = CType(row.FindControl("chkSelect"), CheckBox)
            lblMsg.Text = ""

            If chkSelect.Checked = True Then
                count = 1
                If spc_layer.Text = "WORK STATION" Then
                    cntwst += 1
                End If
                If spc_layer.Text = "HALF CABIN" Then
                    cnthcb += 1
                End If
                If spc_layer.Text = "FULL CABIN" Then
                    cntfcb += 1
                End If
            End If
           
        Next
        If count = 0 Then
            lblMsg.Text = "Please select atleast one seat"
            Exit Sub
        End If

        RaiseProjectMovementreq(verticalreqid, cntwst, cntfcb, cnthcb)

        For i As Integer = 0 To gdavail.Rows.Count - 1
            Dim lblspcoccu As Label = CType(gdavail.Rows(i).FindControl("lblspcoccu"), Label)
            Dim status As Label = CType(gdavail.Rows(i).FindControl("lblstatus"), Label)
            Dim chkSelect As CheckBox = CType(gdavail.Rows(i).FindControl("chkSelect"), CheckBox)
            Dim intCount As Int16 = 0
            If chkSelect.Checked = True Then
                intCount = 0

                RaiseProjectMovementreq1(verticalreqid, lblspcoccu.Text, ddlfrmPrj.SelectedValue, status.Text)

            End If
        Next
        'lblMsg.Text = "Project moved successfully"
        'Exit Sub
        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("1112") & "&rid=" & HttpUtility.UrlEncode(verticalreqid)

GVColor:
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub
    Private Sub RaiseProjectMovementreq(ByVal verticalreqid As String, ByVal cntwst As Int32, ByVal cntfcb As Int32, ByVal cnthcb As Int32)

        Dim param3(9) As SqlParameter

        param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        param3(0).Value = verticalreqid
        param3(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param3(1).Value = Session("uid")
        param3(2) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param3(2).Value = ddlCity.SelectedItem.Value
        param3(3) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
        param3(3).Value = ddlfrmloc.SelectedItem.Value
        param3(4) = New SqlParameter("@frmfloor", SqlDbType.DateTime)
        param3(4).Value = ddlfrmfloor.SelectedValue
        param3(5) = New SqlParameter("@frmproject", SqlDbType.NVarChar, 200)
        param3(5).Value = ddlfrmPrj.SelectedItem.Value
        'param3(6) = New SqlParameter("@TOCITY", SqlDbType.NVarChar, 200)
        'param3(6).Value = ddltocity.SelectedItem.Value
        'param3(7) = New SqlParameter("@TOLOC", SqlDbType.NVarChar, 200)
        'param3(7).Value = ddltoloc.SelectedItem.Value
        'param3(8) = New SqlParameter("@tofloor", SqlDbType.NVarChar, 200)
        'param3(8).Value = ddltofloor.SelectedItem.Value
        param3(6) = New SqlParameter("@toproject", SqlDbType.NVarChar, 200)
        param3(6).Value = ddltoprj.SelectedItem.Value

        param3(7) = New SqlParameter("@WST_COUNT", SqlDbType.Int)
        param3(7).Value = cntwst

        param3(8) = New SqlParameter("@HCB_COUNT", SqlDbType.Int)
        param3(8).Value = cnthcb

        param3(9) = New SqlParameter("@FCB_COUNT", SqlDbType.Int)
        param3(9).Value = cntfcb

        objsubsonic.GetSubSonicExecute("PROJECT_MOVEMENT_REQUEST", param3)
    End Sub
    Private Sub RaiseProjectMovementreq1(ByVal verticalreqid As String, ByVal spcid As String, ByVal prjid As String, ByVal status As String)
        Dim param3(4) As SqlParameter
        param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        param3(0).Value = verticalreqid
        param3(1) = New SqlParameter("@SPACE_ID", SqlDbType.NVarChar, 200)
        param3(1).Value = spcid
        param3(2) = New SqlParameter("@project_id", SqlDbType.NVarChar, 200)
        param3(2).Value = prjid
        param3(3) = New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)
        param3(3).Value = status
        'param3(4) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 200)
        'param3(4).Value = ddltocity.SelectedItem.Value
        'param3(5) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        'param3(5).Value = ddltoloc.SelectedItem.Value
        'param3(6) = New SqlParameter("@flr_code", SqlDbType.NVarChar, 200)
        'param3(6).Value = ddltofloor.SelectedItem.Value
        param3(4) = New SqlParameter("@project", SqlDbType.NVarChar, 200)
        param3(4).Value = ddltoprj.SelectedItem.Value
        objsubsonic.GetSubSonicExecute("PROJECT_MOVEMENT_REQUEST_DTLS", param3)

    End Sub

End Class
