<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master"
    CodeFile="AllocateSeatsByVertical.aspx.vb" Inherits="WorkSpace_GIS_AllocateSeatsByVertical" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="http://code.jquery.com/ui/1.9.0/themes/ui-darkness/jquery-ui.css" rel="stylesheet">

    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.min.js"></script>

    <script language="javascript" type="text/javascript"> 
  function popupClosing(lcm_code,twr_code,flr_code,date1,spc_id) 
  {
  window.open('','_self','');
 window.close();
  window.location = '../../WorkSpace/sms_Webfiles/frmusermapfloorlist_temp.aspx?lcm_code='+ lcm_code+'&twr_code='+ twr_code+'&flr_code='+ flr_code + '&spc_id='+ spc_id +'&Date='+date1;
  parent.parent.GB_hide();
  }
    </script>

    <script>
		
		
		
		
		
		$(function(){
		
			$('#txtEmployeeCode').keyboard();
			
			
		});
		function CheckDate()
     {
         var dtFrom=document.getElementById("txtFromdate").Value;
         var dtTo=document.getElementById("txtTodate").Value;
         if (dtFrom < dtTo)
         {
            alert("Invalid Dates");
         }
     }
		
	
    </script>

    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">  
             <hr align="center" width="60%" />Vertical Requisition</asp:Label></td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                <strong></strong>
            </td>
            <td style="width: 16px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <table cellpadding="0" cellspacing="0" border="0" width="90%" align="center">
                    <tr>
                        <td align="left" colspan="2" style="width: 100%" valign="top">
                            <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Red" Text="Please Note : Booking from Kiosk is only for Today"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100%" valign="top" colspan="2">
                            <asp:Panel ID="emppanel" runat="server" Width="100%" GroupingText="Search by Employee id">
                                <table cellpadding="1" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblSearchMsg" ForeColor="red" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="bodytext" valign="top">
                                            Seat Type</td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="ddlSeatType" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="bodytext" valign="top">
                                            Shifts</td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="ddlShifts" runat="server">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left" class="bodytext">
                                            From Date<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfromdate"
                                                Display="None" ErrorMessage="Please Enter From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top" align="left">
                                            <asp:TextBox ID="txtfromdate" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left" class="bodytext">
                                            To Date<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttodate"
                                                Display="None" ErrorMessage="Please Enter To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top" align="left">
                                            <asp:TextBox ID="txttodate" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="bodytext" valign="top">
                                            Number of Seats</td>
                                        <td align="left" valign="top">
                                            <asp:TextBox ID="txtNumberofSeats" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="bodytext" valign="top">
                                            Verticals</td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="ddlVerticals" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="bodytext" valign="top">
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="clsButton" Text="Submit" />
                                        </td>
                                        <td align="center" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="center" colspan="2">
                                            <asp:Label ID="lblmsg" ForeColor="RED" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <asp:GridView ID="gvitems" runat="server" AllowPaging="true" AllowSorting="true"
                                                AutoGenerateColumns="false" EmptyDataText="No Space records found" Width="100%"
                                                PageSize="20">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select Seat">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkId" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Space Id">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSPC_ID" runat="server" Text='<%#Eval("SPC_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Space Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblemail" runat="server" Text='<%#Eval("SPC_NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="View Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblspaceID" runat="server" Text='<%#Eval("SPC_VIEW_NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" valign="top">
                                            <asp:Button ID="btnAllocate" runat="server" Visible="false" CssClass="clsButton"
                                                Text="Allocate" />
                                        </td>
                                        <td align="center" valign="top">
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 16px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 16px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</asp:Content>
