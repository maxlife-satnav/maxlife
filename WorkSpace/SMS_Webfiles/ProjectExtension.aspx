﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ProjectExtension.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_ProjectExtension" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <script language="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < aspnetForm.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function CheckDataGrid() {
            var k = 0;
            for (i = 0; i < aspnetForm.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                window.alert('Please Select atleast one');
                return false;
            }
            else {
                var input = confirm("Are you sure you want to extend?");
                if (input == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript">
    
    </script>

 <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
 <ContentTemplate>
    <div runat="server" id="divw">
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Project Extension Details
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
            align="center" border="0">
            <tr>
                <td colspan="3" align="left">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 27px">
                    <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                <td class="tableHEADER" align="left">
                    <strong>Project Extension Details</strong></td>
                <td style="height: 27px">
                    <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                </td>
                <td align="left" width="962px">
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="clsMessage" />
                    <br />
                    <br />
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                    <br />
                    <table id="tblDetails" cellspacing="0" cellpadding="0" align="center" border="1"
                        style="border-collapse: collapse; width: 98%">
                        <tr>
                            <td align="left" style="width: 50%;">
                                &nbsp;Select City <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                
                                <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please select City"
                                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                                
                                </td>
                            <td align="center" style="height: 11px" width="50%">
                                <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="clsComboBox"
                                    Width="99%" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 50%;">
                                &nbsp;Select Location <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                <asp:RequiredFieldValidator ID="rfvLoc"
                                    runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please select Location"
                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                            </td>
                            <td align="center" style="height: 11px" width="50%">
                                <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="clsComboBox"
                                    OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" Width="99%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 50%;">
                                &nbsp;Select Tower<font class="clsNote">*
                                    <asp:RequiredFieldValidator
                                        ID="rfvTower" runat="server" ControlToValidate="ddlTower" Display="None" ErrorMessage="Please select Tower"
                                        InitialValue="--Select--"></asp:RequiredFieldValidator></font></td>
                            <td align="center" style="height: 11px" width="50%">
                                <asp:DropDownList ID="ddlTower" runat="server" AutoPostBack="true" Width="99%" CssClass="clsComboBox"
                                    OnSelectedIndexChanged="ddlTower_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 13px" width="50%">
                                &nbsp;Select Floor <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlFloor" Display="None" 
                                    ErrorMessage="Please select Floor" InitialValue="-- Select --"></asp:RequiredFieldValidator>
                            </td>
                            <td align="center" style="height: 13px" width="50%">
                                <asp:DropDownList ID="ddlFloor" runat="server" Width="99%" CssClass="clsComboBox">
                                </asp:DropDownList></td>
                        </tr>
                       <%-- <tr>
                            <td align="left" style="height: 13px" width="50%">
                                &nbsp;Select Wing <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                    <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlWing" Display="None" 
                                    ErrorMessage="Please select Wing" InitialValue="--Select--"></asp:RequiredFieldValidator>
                            </td>
                            <td align="center" style="height: 13px" width="50%">
                                <asp:DropDownList ID="ddlWing" AutoPostBack="True" OnSelectedIndexChanged="ddlWing_SelectedIndexChanged"
                                    runat="server" Width="99%" CssClass="clsComboBox">
                                </asp:DropDownList></td>
                        </tr>--%>
                        <%--<tr>
                            <td align="left" style="height: 13px" width="50%">
                                &nbsp;Select vertical <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                <asp:RequiredFieldValidator ID="rfvVertical" runat="server" ControlToValidate="ddlVertical"
                                    Display="None" ErrorMessage="Please select vertical" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>&nbsp;
                            </td>
                            <td align="center" style="height: 13px" width="50%">
                                <asp:DropDownList ID="ddlVertical" runat="server" CssClass="clsComboBox" Width="99%"
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>--%>
                       <%-- <tr>
                            <td align="left" style="height: 13px" width="50%">
                                &nbsp;Select Cost Center <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                <asp:RequiredFieldValidator ID="rfvCost" runat="server" ControlToValidate="ddlVertical"
                                    Display="None" ErrorMessage="Please select Cost Center" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>&nbsp;
                            </td>
                            <td align="center" style="height: 13px" width="50%">
                                <asp:DropDownList ID="ddlCostCenter" runat="server" CssClass="clsComboBox" Width="99%"
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" width="50%" style="height: 13px">
                                &nbsp;Select Project <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                <asp:RequiredFieldValidator ID="rfvProject" runat="server" ControlToValidate="ddlProject"
                                    Display="None" ErrorMessage="Please select Project" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="center" width="50%" style="height: 13px">
                                <asp:DropDownList ID="ddlProject" runat="server" CssClass="clsComboBox" Width="99%">
                                </asp:DropDownList></td>
                        </tr>--%>
                        <tr>
                            <td align="center" colspan="2" style="height: 22px">
                                <asp:Button ID="btnView" runat="server" CssClass="clsButton" Text="View" /></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:GridView ID="gvSpaceExtend" runat="server" AutoGenerateColumns="False" Width="100%"
                                    AllowPaging="True" PageSize="25">
                                    <columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkEmp" runat="server" />
                                                    <asp:Label runat="server" ID="lblReqID" Visible="false" Text='<%#Bind("Prj_code")%>'></asp:Label> 
                                                   
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkEmp', this.checked)">
                                                </HeaderTemplate>
                                                <ItemStyle Width="1px" />
                                            </asp:TemplateField>
                                            
                                            <asp:BoundField HeaderText="Tower" DataField="Tower" />
                                            <asp:BoundField HeaderText="Floor" DataField="Floor" />
                                            <asp:BoundField HeaderText="Projects" DataField="Prj_code" />                                        
                                            <asp:BoundField HeaderText="From Date" DataField="FROMDATE" />
                                            <asp:BoundField HeaderText="To Date" DataField="TODATE" />
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtExtendDate" MaxLength="10" runat="server" CssClass="clsTextField"
                                                        Width="100px"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtExtendDate"
                                        Format="MM/dd/yyyy">
                                    </cc1:CalendarExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reason to Change">
                                                <ItemTemplate>
                                                    <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                                        ControlToValidate="txtExtendReason" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters !"></asp:CustomValidator>
                                                    <div onmouseover="Tip('Enter reason for Changing TimeLine')" onmouseout="UnTip()">
                                                        <asp:TextBox runat="server" ID="txtExtendReason" TextMode="MultiLine" CssClass="clsTextField"
                                                            Height="35px" Width="150px" MaxLength="500"></asp:TextBox></div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                           <%-- <asp:BoundField HeaderText="EmpID" DataField="SSA_EMP_MAP" Visible="false" />--%>
                                        </columns>
                                    
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" headers="24px" style="height: 24px">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="return CheckDataGrid()"
                                    CssClass="clsButton" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                </td>
            </tr>
            <tr>
                <td style="height: 17px;">
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px; width: 962px;" background="../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px">
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

