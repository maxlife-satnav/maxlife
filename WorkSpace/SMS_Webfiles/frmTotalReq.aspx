<%@ Page Language="VB"  MasterPageFile="~/MasterPage.master"  AutoEventWireup="false" CodeFile="frmTotalReq.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmTotalReq" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>
    <script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>
    <script language="javascript" type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal)
        {
            re = new RegExp(aspCheckBoxID)
            for(i = 0; i < form1.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox')
                {
                    if (re.test(elm.name))
                    elm.checked = checkVal
                }
           }
        }
    </script>
       <div>
       <table id="table4" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">  <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>

            
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="100%"
                    align="center" border="0">
                    <tr>
                        <td  colspan="3" align="left">
                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
          
          </td>
                        </tr>
                    <tr>
                        <td style="height: 27px">
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td class="tableHEADER" align="left">
                            <strong>&nbsp;Vertical wise Requisition Report</strong></td>
                        <td style="height: 27px">
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                            &nbsp;</td>
                        <td align="left" style="width: 962px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage" ForeColor="" />
                            &nbsp;<br />
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                            <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                border="1" runat="server">
                                
                                <tr>
                                    <td align="left" width="50%">
                                        &nbsp;Select Vertical&nbsp;<font class="clsNote">*<asp:RequiredFieldValidator ID="rfvTower"
                                            runat="server" ControlToValidate="ddlReqID" Display="None" ErrorMessage="Please select Vertical"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator></font></td>
                                    <td align="center" width="50%">
                                        <asp:DropDownList ID="ddlReqID" runat="server" Width="99%" AutoPostBack="True" CssClass="clsComboBox">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        &nbsp;Select Criteria&nbsp;<font class="clsNote">*</font></td>
                                    <td align="center" width="50%">
                                        <asp:DropDownList ID="ddlCriteria" runat="server" Width="99%" AutoPostBack="True"
                                            CssClass="clsComboBox">
                                            <asp:ListItem Value="0">--Select Criteria--</asp:ListItem>
                                            <asp:ListItem Value="2">On Date</asp:ListItem>
                                            <asp:ListItem Value="1">Between Dates</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trOnDate" runat="server">
                                    <td align="left" width="50%">
                                        &nbsp;Select Date&nbsp;<font class="clsNote">*</font></td>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtOnDate" CssClass="clsTextField" runat="server" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trFromDate" runat="server">
                                    <td align="left" width="50%">
                                        &nbsp;Select From Date&nbsp;<font class="clsNote">*</font></td>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtFromDate" CssClass="clsTextField" runat="server" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trToDate" runat="server">
                                    <td align="left" width="50%">
                                        &nbsp;Select To Date&nbsp;<font class="clsNote">*</font></td>
                                    <td align="left" width="50%">
                                        <asp:TextBox ID="txtToDate" CssClass="clsTextField" runat="server" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table id="Table2" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                border="1" runat="server">
                                <tr>
                                    <td align="center" colspan="3" style="height: 24px">
                                        <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="clsButton"
                                            Width="136px" OnClick="btnExport_Click" />
                                        &nbsp;<asp:Button ID="btnViewReport" runat="server" Text="View Report" CssClass="clsButton"
                                            Width="136px" />
                                        <asp:Button ID="btnViewCriteria" runat="server" Text="View Criteria" CssClass="clsButton"
                                            OnClick="btnViewCriteria_Click" />
                                        <input type="button" value="Print" onclick="javascript:window.print();"
                                            class="button" id="btnPrint" style="width: 120px" runat="server" /></td>
                                </tr>
                            </table>
                            <table id="Table1" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                border="1" runat="server">
                                <tr>
                                    <td align="center" colspan="4" headers="24px" style="height: 24px">
                                        
                                            <div id="printdiv">
                                                <asp:GridView ID="gvSpaceExtend" runat="server" AutoGenerateColumns="False" Width="100%"
                                                    AllowPaging="True" PageSize="25">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Requisition ID" DataField="SVR_REQ_ID" />
                                                        <asp:BoundField HeaderText="Requested By" DataField="Req_by" />
                                                        <asp:BoundField HeaderText="Requisition Date" DataField="Requisition_Date" />
                                                        <asp:BoundField HeaderText="Vertical Name" DataField="VER_NAME" />
                                                        <asp:BoundField HeaderText="Location Name" DataField="LCM_NAME" />
                                                        <asp:BoundField HeaderText="From Date" DataField="Month" />
                                                        <asp:BoundField HeaderText="To Date" DataField="SVD_YEAR" />
                                                        <asp:BoundField HeaderText="Work Stations" DataField="SVD_WSTNO" />
                                                        <asp:BoundField HeaderText="Half Cabins" DataField="SVD_HCNO" />
                                                        <asp:BoundField HeaderText="Cabins" DataField="SVD_FCNO" />
                                                        <asp:BoundField HeaderText="Status" DataField="status" />
                                                    </Columns>
                                                    
                                                </asp:GridView>
                                            </div>
                                            <cc1:ExportPanel ID="ExportPanel1" runat="server">
                                                  <asp:GridView ID="gvExcel" runat="server" AutoGenerateColumns="False" Width="100%"
                                                    Visible="false">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Requisition ID" DataField="SVR_REQ_ID" />
                                                        <asp:BoundField HeaderText="Requested By" DataField="Req_by" />
                                                        <asp:BoundField HeaderText="Requisition Date" DataField="Requisition_Date" />
                                                        <asp:BoundField HeaderText="Vertical Name" DataField="VER_NAME" />
                                                        <asp:BoundField HeaderText="Location Name" DataField="LCM_NAME" />
                                                        <asp:BoundField HeaderText="From Date" DataField="Month" />
                                                        <asp:BoundField HeaderText="To Date" DataField="SVD_YEAR" />
                                                        <asp:BoundField HeaderText="Work Stations" DataField="SVD_WSTNO" />
                                                        <asp:BoundField HeaderText="Half Cabins" DataField="SVD_HCNO" />
                                                        <asp:BoundField HeaderText="Cabins" DataField="SVD_FCNO" />
                                                        <asp:BoundField HeaderText="Status" DataField="status" />
                                                    </Columns>
                                                    
                                                </asp:GridView>
                                        </cc1:ExportPanel>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px; width: 962px;" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                </table>
            </asp:Panel>
          
        </div>
   </asp:Content>