Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmSMSeditmap
    Inherits System.Web.UI.Page
    Dim obj As New clsWorkSpace

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            Try
                obj.loadlocation(ddlLocation)
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Edit Map", "Load", ex)
            End Try

        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            obj.BindTower(ddlTower, ddlLocation.SelectedItem.Value)
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Edit Map", "Load", ex)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            obj.BindFloor(ddlFloor, ddlTower.SelectedItem.Value)

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Edit Map", "Load", ex)

        End Try
    End Sub
    Public Sub cleardata()
        ddlTower.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0

    End Sub


    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            If Page.IsValid Then
                Dim strSQL As String
                Dim BDG_ID As String = String.Empty
                Dim FLR_CODE As String = String.Empty

                strSQL = "Select Distinct FLR_Code,FLR_BDG_ID,FLR_CTY_ID from " & Session("TENANT") & "."  & "Floor where FLR_code = '" & ddlFloor.SelectedItem.Value & "' AND FLR_BDG_ID='" & ddlLocation.SelectedItem.Value & "' AND FLR_TWR_ID='" & ddlTower.SelectedItem.Value & "'"
                ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

                If ObjDR.Read Then
                    BDG_ID = ObjDR("FLR_BDG_ID").ToString
                    FLR_CODE = ObjDR("FLR_Code").ToString
                    Session("Cty_Id") = ObjDR("FLR_CTY_ID").ToString
                End If
                Session("FLR_CODE") = FLR_CODE
                Session("BDG_ID") = BDG_ID
                Session("Twr_Code") = ddlTower.SelectedValue
                Session("LOC_ID") = ddlLocation.SelectedValue
                ObjDR.Close()

                Response.Write("<script language=javascript>javascript:window.open('../GIS/Map_Admin.aspx','MapWindow','scrollbars=no,toolbar=no,statusbar=no,height=650,width=850,top=0,left=0')</script>")
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Edit Map", "Load", ex)
        End Try

    End Sub
End Class
