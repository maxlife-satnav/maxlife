<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmLocationAvailableEdit.aspx.vb" Inherits="Masters_Mas_Webfiles_frmLocationAvailableEdit"
    Title="Modify Available and Occupied Locations " %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Modify Available and Occupied Locations
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
            <tr>
                <td colspan="3" align="left">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 36px">
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Available Locations Master</strong>&nbsp;</td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                </td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <asp:Panel id="pnlgrid" runat="Server" width="100%" >
                        <asp:GridView id="gvitems" runat="server" Allowpaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" Width="100%">
                            <columns>
                <asp:templatefield visible="false">
                <itemtemplate>
                <asp:label id="lblloc" runat="server" Text=<%#Eval("LOCATION_CODE") %>></asp:label> />
                </itemtemplate>
                </asp:templatefield>
                <asp:BoundField DataField="LOCATION_NAME" HeaderText="LOCATION" />
                <asp:BoundField DataField="AVAILABLE_WT" HeaderText="Available WT" />
                <asp:BoundField DataField="OCCUPIED_WT" HeaderText="Occupied WT" />
                <asp:BoundField DataField="AVAILABLE_WI" HeaderText="Available WI" />
                <asp:BoundField DataField="OCCUPIED_WI" HeaderText="Occupied WI" />
                <asp:BoundField DataField="AVAILABLE_WBPO" HeaderText="Available WBPO" />
                <asp:BoundField DataField="OCCUPIED_WBPO" HeaderText="Occupied WBPO" />
                <asp:BoundField DataField="AVAILABLE_WSTL" HeaderText="Available WSTL" />
                <asp:BoundField DataField="OCCUPIED_WSTL" HeaderText="Occupied WSTL" />
             <%--    <asp:ButtonField Text="EDIT" CommandName="EDIT" />--%>
                 <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                </columns>
                        </asp:GridView>
                    </asp:Panel>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px; width: 861px;" background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
