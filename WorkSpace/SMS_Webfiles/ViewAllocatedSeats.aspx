<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="ViewAllocatedSeats.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_ViewAllocatedSeats"
    Title="View Allocated seats" %>

<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View Allocated Seats
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="vsSM" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-3 control-label">City<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-9">
                                            <asp:DropDownList ID="ddlCity" AutoPostBack="true" CssClass="selectpicker" data-live-search="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Select Location<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlLocation" CssClass="selectpicker" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                           <div class="col-md-12">                              
                                    <asp:GridView ID="gvitemsSameLocation" runat="server" AllowPaging="true" AllowSorting="true"
                                        AutoGenerateColumns="false" EmptyDataText="No Space Records Found."
                                        PageSize="20" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Tower">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTower" runat="server" Text='<%#Eval("TWR_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblTwrCode" Visible="false" runat="server" Text='<%#Eval("SSA_TWR_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Floor">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFloor" runat="server" Text='<%#Eval("Flr_name") %>'></asp:Label>
                                                    <asp:Label ID="lblFlr_code" Visible="false" runat="server" Text='<%#Eval("SSA_FLR_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Allocated">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkAllocated" Text='<%#Eval("Allocated") %>' CommandArgument='<%#Eval("RowNumber") %>'
                                                        CommandName="ViewAllocated" runat="server"></asp:LinkButton>
                                                    <asp:Label ID="lblAllocated" runat="server" Text=""></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>                            
                        </div>

                        <div id="trSpaceDetails" class="row" runat="server" visible="false" style="margin-top: 10px">
                           <div class="col-md-12">                             
                                    <asp:GridView ID="gvSpaceDetails" runat="server" AllowPaging="true" AllowSorting="true"
                                        AutoGenerateColumns="false" EmptyDataText="No Space records found" Width="100%"
                                        PageSize="20" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Space Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSSA_SPC_ID" runat="server" Text='<%#Eval("SSA_SPC_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vertical Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSSA_VERTICAL" runat="server" Text='<%#Eval("SSA_VERTICAL") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="From Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSSA_FROM_DATE" runat="server" Text='<%#Eval("SSA_FROM_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSSA_TO_DT" runat="server" Text='<%#Eval("SSA_TO_DT") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="From Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFROM_TIME" runat="server" Text='<%#Eval("FROM_TIME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTO_TIME" runat="server" Text='<%#Eval("TO_TIME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>                         
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
