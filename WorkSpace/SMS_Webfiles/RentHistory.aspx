﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="RentHistory.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_RentHistory" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Rent History
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvzone" runat="server" Display="none" ErrorMessage="Please Select From Date"
                                            ValidationGroup="Val1" ControlToValidate="txtFdate"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                            ErrorMessage="Please Select To Date" ValidationGroup="Val1" ControlToValidate="txtTdate"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtTdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">

                                    <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"></asp:Button>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <h3>
                                        <asp:Label ID="lblunpaid" runat="server" Text="UN PAID" CssClass="col-md-12 control-label"></asp:Label></h3>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvitems" runat="server" AllowPaging="True" AllowSorting="False"
                                    PageSize="20" AutoGenerateColumns="false"
                                    CssClass="table table-condensed table-bordered table-hover table-striped" EmptyDataText="No Un-paid Rent Histroy Found.">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Property">
                                            <ItemTemplate>
                                                <asp:Label ID="lblproperty" runat="server" Text='<%#Eval("PROPERTY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllease" runat="server" Text='<%#Bind("LEASE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("LESSE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldate" runat="server" Text='<%#Eval("PAIDDATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Basic Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrent" runat="server" Text='<%# Eval("RENT_AMOUNT", "{0:c2}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Maintenance Cost">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmain" runat="server" Text='<%# Eval("MAINTENANCE_COST","{0:c2}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltot" runat="server" Text='<%# Eval("TOTAL_RENT", "{0:c2}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstat" runat="server" Text='<%#Eval("STAT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CHKSELECT" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">

                                    <asp:Button ID="btnsubmit" runat="server" Text="Un Paid" CssClass="btn btn-primary custom-button-color"></asp:Button>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <h3>
                                        <asp:Label ID="lblpaid" runat="server" Text="PAID" CssClass="col-md-12 control-label"></asp:Label></h3>

                                </div>
                            </div>
                        </div>


                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvpaid" runat="server" AllowPaging="True" AllowSorting="False"
                                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                    PageSize="20" AutoGenerateColumns="false" EmptyDataText="No Paid Rent History Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Property">
                                            <ItemTemplate>
                                                <asp:Label ID="lblproperty" runat="server" Text='<%#Eval("PROPERTY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllease" runat="server" Text='<%#Bind("LEASE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("LESSE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldate" runat="server" Text='<%#Eval("PAIDDATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Basic Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrent" runat="server" Text='<%# Eval("RENT_AMOUNT","{0:c2}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Maintenance Cost">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmain" runat="server" Text='<%# Eval("MAINTENANCE_COST","{0:c2}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltot" runat="server" Text='<%# Eval("TOTAL_RENT","{0:c2}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstat" runat="server" Text='<%#Eval("STAT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CHKSELECT" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">

                                    <asp:Button ID="btnpaid" runat="server" CssClass="btn btn-primary custom-button-color" Text="Paid" />

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
