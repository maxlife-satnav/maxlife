Imports Amantra.RequisitionDTON
Imports Amantra.RequisitionDALN
Imports Amantra.RequisitionBLLN
Imports System.Collections.Generic
Imports System.Net.Mail
Imports System.Data
Imports System.Data.SqlClient


Partial Class SpaceManagement_frmApprovalSpaceRequisition
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim reqBll As New RequisitionBLL()
    Dim rDTO As New RequisitionDTO()
    Dim Email As String = String.Empty
    Dim dtSpaceReqDetails As DataTable
    Dim strRedirect As String = String.Empty
    Dim strBranch As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Visible = False
        txtFdate.Attributes.Add("readonly", "readonly")
        txtEDate.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            Try
                BindReqidtoDropDown()
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Requisition", "Load", ex)
            End Try

        End If
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If Not IsValid Then
            Exit Sub
        End If

        Try
            If ddlReqid.SelectedIndex <> -1 And ddlReqid.SelectedIndex <> 0 Then
                Dim iSeats As Integer
                iSeats = Convert.ToInt32(txtCabreq.Text) + Convert.ToInt32(txtCubreq.Text) + Convert.ToInt32(txtWSreq.Text)
                If (iSeats < 50) Then
                    rDTO.RequestID = ddlReqid.SelectedValue
                    rDTO.getStatus = 147
                    rDTO.getRMRemarks = txtRMrem.Text
                    Dim iCount As Integer = reqBll.updateRequestIdStatus(rDTO)
                    Dim bol As Boolean = reqBll.insertRMReqDetails(ddlReqid.SelectedValue, txtWSreqHid.Text, txtCabreqHid.Text, txtCubreqHid.Text, txtWSreq.Text, txtCabreq.Text, txtCubreq.Text, txtFdate.Text, txtEDate.Text, Session("uid").ToString(), txtRMrem.Text.Trim().Replace("'", "''"), "RM")
                    If (iCount = 1) And bol = True Then

                        Dim sp1 As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
                        sp1.Value = ddlReqid.SelectedValue
                        Email = SqlHelper.ExecuteScalar(Data.CommandType.StoredProcedure, "USP_SPACE_GET_APPROVEDREQ", sp1)
                        ' sendMail_Approved_Lessthan50(ddlReqid.SelectedValue.ToString().Trim(), txtFdate.Text, txtEDate.Text, txtPB1.Text, txtWSreqHid.Text, txtCubreqHid.Text, txtCabreqHid.Text, txtWSreq.Text, txtCubreq.Text, txtCabreq.Text, txtDept.Text, 147, Email)
                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_SPACE_REQ_MAIL")
                        'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                        'sp.Command.AddParameter("@REQID", ddlReqid.SelectedValue.ToString().Trim(), DbType.String)
                        'sp.Command.AddParameter("@MAILSTATUS", 10, DbType.Int32)
                        'sp.ExecuteScalar()


                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEREQUISITIONAPPROVELEVEL1")
                        sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
                        sp.Command.AddParameter("@REQID", ddlReqid.SelectedValue, DbType.String)
                        sp.ExecuteScalar()

                        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("147") & "&rid=" & clsSecurity.Encrypt(ddlReqid.SelectedValue)
                    Else
                        ' PopUpMessage("RM Apporved Failed ", Me)
                        lblMsg.Text = "RM Approved Failed"
                        lblMsg.Visible = True
                    End If
                    BindReqidtoDropDown()
                    clear()
                    ddlReqid.SelectedIndex = -1
                Else
                    rDTO.RequestID = ddlReqid.SelectedValue
                    rDTO.getStatus = 149
                    rDTO.getRMRemarks = txtRMrem.Text
                    Dim iCount As Integer = reqBll.updateRequestIdStatus(rDTO)
                    Dim bol As Boolean = reqBll.insertRMReqDetails(ddlReqid.SelectedValue, txtWSreqHid.Text, txtCabreqHid.Text, txtCubreqHid.Text, txtWSreq.Text, txtCabreq.Text, txtCubreq.Text, txtFdate.Text, txtEDate.Text, Session("uid").ToString(), txtRMrem.Text.Trim().Replace("'", "''"), "RM")
                    If (iCount = 1) And bol = True Then
                        ' PopUpMessage("Request Sent to BFM Apporval because it requires more than 50 Seats ", Me)
                        '----------------------------------------
                        ''USP_SPACE_GET_APPROVEDREQ
                        '----------------------------------------
                        'strSQL = "SELECT (SELECT AUR_EMAIL FROM AMANTRA_USER WHERE AUR_ID=SRN_AUR_ID) AS EMAIL " & _
                        '        " FROM " & Session("TENANT") & "."  & "sms_spacerequisition WHERE srn_req_id='" & ddlReqid.SelectedValue.Trim() & "'"
                        'Email = SqlHelper.ExecuteScalar(Data.CommandType.Text, strSQL)
                        Dim sp1 As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
                        sp1.Value = ddlReqid.SelectedValue
                        Email = SqlHelper.ExecuteScalar(Data.CommandType.StoredProcedure, "USP_SPACE_GET_APPROVEDREQ", sp1)
                        ' sendMail_Approved_Above50(ddlReqid.SelectedValue.ToString().Trim(), txtFdate.Text, txtEDate.Text, txtPB1.Text, txtWSreqHid.Text, txtCubreqHid.Text, txtCabreqHid.Text, txtWSreq.Text, txtCubreq.Text, txtCabreq.Text, txtDept.Text, 147, Email)

                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_SPACE_REQ_MAIL")
                        'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                        'sp.Command.AddParameter("@REQID", ddlReqid.SelectedValue.ToString().Trim(), DbType.String)
                        'sp.Command.AddParameter("@MAILSTATUS", 10, DbType.Int32)
                        'sp.ExecuteScalar()
                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEREQUISITIONAPPROVELEVEL1")
                        sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
                        sp.Command.AddParameter("@REQID", ddlReqid.SelectedValue.ToString().Trim(), DbType.String)
                        sp.ExecuteScalar()
                        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("149") & "&rid=" & clsSecurity.Encrypt(ddlReqid.SelectedValue)
                    Else
                        'PopUpMessage(" Failed to send BFM Apporval  ", Me)
                        lblMsg.Text = "Failed to send BFM Approval"
                        lblMsg.Visible = True
                    End If

                    BindReqidtoDropDown()
                    clear()
                    ddlReqid.SelectedIndex = -1
                End If
            Else
                ' PopUpMessage("please select request id to Apporve ", Me)
                lblMsg.Text = "Please select Request Id to Approve "
                lblMsg.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Approving data.", "Approval for Space Requisition", "Load", ex)
        End Try
        If (strRedirect <> String.Empty) Then
            Response.Redirect(strRedirect)
        End If

    End Sub

    Public Sub BindRequsitionDeatils(ByVal req_id As String, ByVal status As Integer, ByVal user_id As String)
        Try

            Dim reqDTO As IList(Of RequisitionDTO) = New List(Of RequisitionDTO)()
            reqDTO = reqBll.GetRequisitionData(req_id, status, user_id)
            txtReqname.Text = reqDTO(0).getUserName
            txtDept.Text = reqDTO(0).getDept
            txtPB1.Text = reqDTO(0).getBuildingOne
            txtCabreq.Text = reqDTO(0).getCabins
            txtCubreq.Text = reqDTO(0).getCubicals
            txtWSreq.Text = reqDTO(0).getWrkStations
            txtFdate.Text = reqDTO(0).getFromDate
            txtEDate.Text = reqDTO(0).getToDate
            '   txtRM.Text = reqDTO(0).getRptMgrName
            txtReqRemarks.Text = reqDTO(0).getRemarks
            Label1.Text = reqDTO(0).getRMRemarks
            txtCabreqHid.Text = txtCabreq.Text
            txtCubreqHid.Text = txtCubreq.Text
            txtWSreqHid.Text = txtWSreq.Text

            Dim sp As SqlParameter = New SqlParameter("@req_id", SqlDbType.NVarChar, 50)
            Dim sp1 As SqlParameter = New SqlParameter("@sta_id", SqlDbType.Int)
            Dim sp2 As SqlParameter = New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
            sp.Value = req_id
            sp1.Value = status
            sp2.Value = user_id
            dtSpaceReqDetails = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_Wipro_getRequisitionData", sp, sp1, sp2)
            lblPrjName.Text = dtSpaceReqDetails.Rows(0)("Prjname").ToString().Trim()
            lblVerName.Text = dtSpaceReqDetails.Rows(0)("Vertical").ToString().Trim()

        Catch ex As System.Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Requisition", "Load", ex)
        End Try
    End Sub

    Public Sub BindReqidtoDropDown()
        Try
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@Aur_id", SqlDbType.NVarChar, 200)
            param(0).Value = Session("uid")
            param(1) = New SqlParameter("@sta_id", SqlDbType.Int)
            param(1).Value = 5
            objsubsonic.Binddropdown(ddlReqid, "GET_SPACE_REQUISITIONS", "SRN_REQ_ID1", "srn_req_id", param)
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Requisition", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlReqid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqid.SelectedIndexChanged
        Try

            If ddlReqid.SelectedIndex <> 0 And ddlReqid.SelectedIndex <> -1 Then
                clear()
                BindRequsitionDeatils(ddlReqid.SelectedValue, 5, Session("uid").ToString())
            Else
                clear()
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Requisition", "Load", ex)
        End Try

    End Sub

    Protected Sub clear()
        txtReqname.Text = String.Empty
        txtDept.Text = String.Empty
        txtPB1.Text = String.Empty
        txtCabreqHid.Text = ""
        txtCubreqHid.Text = ""
        txtWSreqHid.Text = ""
        txtCabreq.Text = String.Empty
        txtCubreq.Text = String.Empty
        txtWSreq.Text = String.Empty
        txtFdate.Text = String.Empty
        txtEDate.Text = String.Empty
        'txtRM.Text = String.Empty
        txtRMrem.Text = String.Empty
        txtReqRemarks.Text = String.Empty
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim strRedirect As String = String.Empty
        Try

            If ddlReqid.SelectedIndex <> -1 And ddlReqid.SelectedIndex <> 0 Then
                rDTO.RequestID = ddlReqid.SelectedValue
                rDTO.getStatus = 148
                rDTO.getRMRemarks = txtRMrem.Text.Trim().Replace("'", "''")
                Dim iCount As Integer = reqBll.updateRequestIdStatus(rDTO)
                If (iCount = 1) Then
                    'PopUpMessage("RM Rejected Sucessfully ", Me)
                    lblMsg.Text = "RM Rejected Successfully"
                    lblMsg.Visible = True
                    strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("148") & "&rid=" & clsSecurity.Encrypt(ddlReqid.SelectedValue)
                Else
                    'PopUpMessage("RM Rejected Failed ", Me)
                    lblMsg.Text = "RM Rejected Failed"
                    lblMsg.Visible = True
                    strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("148-2") & "&rid=" & clsSecurity.Encrypt(ddlReqid.SelectedValue)

                End If
                '----------------------------------------
                ''USP_SPACE_GET_APPROVEDREQ
                '----------------------------------------
                'strSQL = "SELECT (SELECT AUR_EMAIL FROM AMANTRA_USER WHERE AUR_ID=SRN_AUR_ID) AS EMAIL " & _
                '                 " FROM " & Session("TENANT") & "."  & "sms_spacerequisition WHERE srn_req_id='1/SPCREQ/0000001'"
                'Email = SqlHelper.ExecuteScalar(Data.CommandType.Text, strSQL)
                Dim sp1 As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
                sp1.Value = ddlReqid.SelectedValue
                Email = SqlHelper.ExecuteScalar(Data.CommandType.StoredProcedure, "USP_SPACE_GET_APPROVEDREQ", sp1)
                ' sendMail_Reject(ddlReqid.SelectedValue.ToString().Trim(), txtFdate.Text, txtEDate.Text, txtPB1.Text, txtWSreqHid.Text, txtCubreqHid.Text, txtCabreqHid.Text, txtWSreq.Text, txtCubreq.Text, txtCabreq.Text, txtDept.Text, 147, Email)
                'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_SPACE_REQ_MAIL")
                'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                'sp.Command.AddParameter("@REQID", ddlReqid.SelectedValue.ToString().Trim(), DbType.String)
                'sp.Command.AddParameter("@MAILSTATUS", 11, DbType.Int32)
                'sp.ExecuteScalar()
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEREQUISITIONREJECTLEVEL1")
                sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
                sp.Command.AddParameter("@REQID", ddlReqid.SelectedValue.ToString().Trim(), DbType.String)
                sp.ExecuteScalar()
                BindReqidtoDropDown()
                clear()
                ddlReqid.SelectedIndex = -1
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Rejecting data.", "Approval for Space Requisition", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    'Private Sub sendMail_Approved_Lessthan50(ByVal strReqId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal strTower As String, ByVal reqWs As Int16, ByVal reqHC As Int16, ByVal reqFC As Int16, ByVal AllocWs As Int16, ByVal AllocHC As Int16, ByVal AllocFC As Int16, ByVal strDept As String, ByVal intStatusID As Int16, ByVal strEmail As String)
    '    Try
    '        Dim to_mail As String = strEmail
    '        Dim cc_mail As String = Session("uemail")
    '        Dim body As String = String.Empty
    '        Dim user As String = String.Empty
    '        Dim objDataCurrentUser As SqlDataReader

    '        'Dim strCurrentUser As String = "select aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objDataCurrentUser = SqlHelper.ExecuteReader(CommandType.Text, strCurrentUser)
    '        'While objDataCurrentUser.Read
    '        '    user = objDataCurrentUser("aur_known_as")
    '        'End While
    '        Dim objData As SqlDataReader
    '        Dim strRR As String = ""
    '        Dim strRM As String = ""
    '        Dim strKnownas As String = ""

    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '        sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '        objData = sp1.GetReader()
    '        While objData.Read
    '            strRR = objData("aur_reporting_to").ToString
    '            strRM = objData("aur_reporting_email").ToString
    '            strKnownas = objData("aur_known_as").ToString
    '            to_mail = objData("aur_email").ToString
    '            'BCC = objData("BCC").ToString
    '        End While

    '        body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />"
    '        body = body & "Project space request has been Approved by " + user + " and pending for allocation.The details are as follows."

    '        Dim strCC As String = String.Empty
    '        Dim strEmail1 As String = String.Empty
    '        ' Dim strRM As String = String.Empty
    '        Dim strFMG As String = String.Empty
    '        Dim strBUHead As String = String.Empty
    '        'Dim strRR As String = String.Empty
    '        'Dim objData As SqlDataReader

    '        body = body & "The details are as follows</td></tr></table>&nbsp;<br /> "
    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"

    '        body = body & "<table align='center'>"
    '        '<tr ><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Sno</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Field</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Value</td></tr> "
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strReqId & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblPrjName.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblVerName.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & FromDate & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ToDate & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqWs & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocWs & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqHC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocHC & "</td> </tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtReqRemarks.Text.Trim & "</td></tr></table>"

    '        body = body & "</td></tr></table>"
    '        body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
    '        body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
    '        body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
    '        body = body & "</table>"
    '        'If to_mail = String.Empty Then
    '        '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If
    '        'If cc_mail = String.Empty Then
    '        '    cc_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If
    '        Dim strVRM As String = String.Empty

    '        Dim strsql As String = String.Empty
    '        Dim stVrm As String = String.Empty
    '        Dim stVrm1 As String = String.Empty
    '        Dim dr As SqlDataReader
    '        Dim dr1 As SqlDataReader
    '        Dim iQry As Integer = 0

    '        'strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & Label1.Text.Trim() & "'"
    '        'iQry = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL1))
    '        'If iQry > 0 Then
    '        '    strSQL1 = "Select VER_VRM from VERTICAL where VER_CODE='" & Label1.Text.Trim() & "'"
    '        '    dr1 = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
    '        '    While dr1.Read
    '        '        stVrm = dr1("VER_VRM").ToString()
    '        '    End While
    '        '    strsql = "Select aur_email from amantra_user where aur_id='" & stVrm & "'"
    '        '    dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
    '        '    While dr.Read
    '        '        stVrm1 = dr("aur_email").ToString()
    '        '    End While
    '        'Else
    '        '    stVrm1 = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If

    '        Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        Dim parms2 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '        Dim parms3 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        Dim parms4 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        Dim parms5 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        Dim parms6 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        Dim parms7 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        Dim parms8 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        parms.Value = "Abcd"
    '        parms2.Value = body
    '        parms3.Value = to_mail
    '        parms4.Value = "Space Approval Details"
    '        parms5.Value = getoffsetdatetime(DateTime.Now)
    '        parms6.Value = "Request Submitted"
    '        parms7.Value = "Normal Mail"
    '        parms8.Value = strRM

    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms2, parms3, parms4, parms5, parms6, parms7, parms8)

    '        'Dim parms9 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
    '        'parms9(0).Value = 3
    '        'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms9)
    '        'If objData.HasRows Then
    '        '    While objData.Read
    '        '        strBUHead = objData("aur_email")
    '        '        Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        '        Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '        '        Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        '        Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        '        Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        '        Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        '        Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        '        Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        '        parms10.Value = "Abcd"
    '        '        parms11.Value = body
    '        '        parms12.Value = strBUHead
    '        '        parms13.Value = "Space Approval Details"
    '        '        parms14.Value = getoffsetdatetime(DateTime.Now)
    '        '        parms15.Value = "Request Submitted"
    '        '        parms16.Value = "Normal Mail"
    '        '        parms17.Value = String.Empty
    '        '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)

    '        '    End While
    '        'End If
    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Requisition", "Load", ex)
    '    End Try


    'End Sub

    'Private Sub sendMail_Approved_Above50(ByVal strReqId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal strTower As String, ByVal reqWs As Int16, ByVal reqHC As Int16, ByVal reqFC As Int16, ByVal AllocWs As Int16, ByVal AllocHC As Int16, ByVal AllocFC As Int16, ByVal strDept As String, ByVal intStatusID As Int16, ByVal strEmail As String)
    '    Try
    '        Dim to_mail As String = strEmail
    '        Dim cc_mail As String = Session("uemail")
    '        Dim body As String = String.Empty
    '        Dim user As String = String.Empty
    '        'Dim objDataCurrentUser As SqlDataReader
    '        'Dim strCurrentUser As String = "select aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objDataCurrentUser = SqlHelper.ExecuteReader(CommandType.Text, strCurrentUser)
    '        'While objDataCurrentUser.Read
    '        '    user = objDataCurrentUser("aur_known_as")
    '        'End While

    '        Dim strCC As String = String.Empty
    '        Dim strEmail1 As String = String.Empty
    '        Dim strRM As String = String.Empty
    '        Dim strFMG As String = String.Empty
    '        Dim strBUHead As String = String.Empty
    '        Dim strRR As String = String.Empty
    '        Dim objData As SqlDataReader
    '        Dim strKnownas As String = ""

    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '        sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '        objData = sp1.GetReader()
    '        While objData.Read
    '            strRR = objData("aur_reporting_to").ToString
    '            strRM = objData("aur_reporting_email").ToString
    '            strKnownas = objData("aur_known_as").ToString
    '            to_mail = objData("aur_email").ToString
    '            'BCC = objData("BCC").ToString
    '        End While

    '        body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />"
    '        body = body & "Project space request has been Approved by " + user + " and pending for allocation.The details are as follows."

    '        body = body & "The details are as follows</td></tr></table>&nbsp;<br /> "
    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '        body = body & "<table align='center'>"
    '        '<tr ><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Sno</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Field</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Value</td></tr> "
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strReqId & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblPrjName.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblVerName.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & FromDate & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ToDate & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqWs & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocWs & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqHC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocHC & "</td> </tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtReqRemarks.Text.Trim & "</td></tr></table>"
    '        body = body & "</td></tr></table>"
    '        body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
    '        body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
    '        body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
    '        body = body & "</table>"
    '        'If to_mail = String.Empty Then
    '        '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If
    '        'If cc_mail = String.Empty Then
    '        '    cc_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If

    '        Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        Dim parms2 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '        Dim parms3 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        Dim parms4 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        Dim parms5 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        Dim parms6 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        Dim parms7 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        Dim parms8 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        parms.Value = "Abcd"
    '        parms2.Value = body
    '        parms3.Value = to_mail
    '        parms4.Value = "Space Approval Details"
    '        parms5.Value = getoffsetdatetime(DateTime.Now)
    '        parms6.Value = "Request Submitted"
    '        parms7.Value = "Normal Mail"
    '        parms8.Value = strRM

    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms2, parms3, parms4, parms5, parms6, parms7, parms8)
    '        Dim parms9 As SqlParameter() = {New SqlParameter("@Requisition", SqlDbType.VarChar, 250)}
    '        parms9(0).Value = ddlReqid.SelectedValue
    '        objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[USP_GET_BFMEMAIL]", parms9)
    '        If objData.HasRows Then
    '            While objData.Read
    '                strBUHead = objData("aur_email")
    '                Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '                Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '                Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '                Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '                Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '                Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '                Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '                Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '                parms10.Value = "Abcd"
    '                parms11.Value = body
    '                parms12.Value = strBUHead
    '                parms13.Value = "Space Approval Details"
    '                parms14.Value = getoffsetdatetime(DateTime.Now)
    '                parms15.Value = "Request Submitted"
    '                parms16.Value = "Normal Mail"
    '                parms17.Value = String.Empty
    '                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)

    '            End While
    '        End If
    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Requisition", "Load", ex)
    '    End Try


    'End Sub

    'Private Sub sendMail_Reject(ByVal strReqId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal strTower As String, ByVal reqWs As Int16, ByVal reqHC As Int16, ByVal reqFC As Int16, ByVal AllocWs As Int16, ByVal AllocHC As Int16, ByVal AllocFC As Int16, ByVal strDept As String, ByVal intStatusID As Int16, ByVal strEmail As String)
    '    Try
    '        Dim to_mail As String = strEmail
    '        Dim cc_mail As String = Session("uemail")
    '        Dim body As String = String.Empty
    '        Dim user As String = String.Empty
    '        'Dim objDataCurrentUser As SqlDataReader
    '        'Dim strCurrentUser As String = "select aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objDataCurrentUser = SqlHelper.ExecuteReader(CommandType.Text, strCurrentUser)
    '        'While objDataCurrentUser.Read
    '        '    user = objDataCurrentUser("aur_known_as")
    '        'End While
    '        Dim strKnownas As String = String.Empty
    '        Dim strRM As String = String.Empty
    '        Dim strRR As String = String.Empty
    '        Dim strFMG As String = String.Empty
    '        Dim strBUHead As String = String.Empty
    '        Dim BCC As String = ""

    '        Dim objData As SqlDataReader
    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '        sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '        objData = sp1.GetReader()
    '        While objData.Read
    '            strRR = objData("aur_reporting_to").ToString
    '            strRM = objData("aur_reporting_email").ToString
    '            strKnownas = objData("aur_known_as").ToString
    '            to_mail = objData("aur_email").ToString
    '            'BCC = objData("BCC").ToString
    '        End While

    '        body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />"

    '        body = body & "Project space request has been Rejected by " + user + ". The details are as follows."

    '        Dim strCC As String = String.Empty
    '        Dim strEmail1 As String = String.Empty

    '        'strCC = "select aur_reporting_to from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strCC)
    '        'While objData.Read
    '        '    strRR = objData("aur_reporting_to")
    '        'End While
    '        'strEmail1 = " select aur_email from amantra_user where aur_id='" & strRR & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strEmail1)

    '        'While objData.Read
    '        '    strRM = objData("aur_email")
    '        'End While
    '        body = body & "The details are as follows</td></tr></table>&nbsp;<br /> "
    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"

    '        body = body & "<table align='center'>"
    '        '<tr ><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Sno</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Field</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Value</td></tr> "
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strReqId & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblPrjName.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblVerName.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & FromDate & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ToDate & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqWs & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocWs & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqHC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocHC & "</td> </tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtReqRemarks.Text.Trim & "</td></tr></table>"

    '        body = body & "</td></tr></table>"
    '        body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
    '        body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
    '        body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
    '        body = body & "</table>"
    '        If to_mail = String.Empty Then
    '            to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        End If
    '        If cc_mail = String.Empty Then
    '            cc_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        End If
    '        Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        Dim parms2 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '        Dim parms3 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        Dim parms4 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        Dim parms5 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        Dim parms6 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        Dim parms7 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        Dim parms8 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        parms.Value = "Abcd"
    '        parms2.Value = body
    '        parms3.Value = to_mail
    '        parms4.Value = "Space Rejection Details"
    '        parms5.Value = getoffsetdatetime(DateTime.Now)
    '        parms6.Value = "Request Submitted"
    '        parms7.Value = "Normal Mail"
    '        parms8.Value = strRM

    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms2, parms3, parms4, parms5, parms6, parms7, parms8)


    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Requisition", "Load", ex)
    '    End Try


    'End Sub

End Class
