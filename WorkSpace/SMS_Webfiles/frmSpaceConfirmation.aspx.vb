Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Partial Class WorkSpace_SMS_Webfiles_frmSpaceConfirmation
    Inherits System.Web.UI.Page
    Dim obj As New clsWorkSpace
    Dim objMsater As New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("uid") = "" Then
        '    Response.Redirect(AppSettings("logout"))
        'End If
        If Not Page.IsPostBack Then
            objMsater.Bindlocation(ddlLocation)
            ddlTower.Items.Add("--Select--")
            ddlFloor.Items.Add("--Select--")
            LoadCity()
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex <> -1 And ddlLocation.SelectedIndex <> 0 Then
                objMsater.BindTowerLoc(ddlTower, ddlLocation.SelectedItem.Value)
            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub
    Dim verBll As New VerticalBLL()
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub
    Public Sub cleardata()
        ddlTower.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0

    End Sub


    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click

        strSQL = "insert into " & Session("TENANT") & "."  & "emp_space_confirm values('" & ddlSpaces.SelectedItem.Value.Trim() & "','" & ddlSpaces.SelectedItem.Text.Trim() & "','" & Session("uid") & "','',getdate())"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        Response.Redirect("../../WebFiles/frmAMTwelcome.aspx")

    End Sub
    Public Sub LoadCity()
        objMsater.BindVerticalCity(ddlCity)
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex <> -1 Or ddlCity.SelectedIndex = 0 Then
            strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
            BindCombo(strSQL, ddlLocation, "LCM_name", "lcm_code")
        Else
            ddlLocation.Items.Clear()
            ddlLocation.Items.Add("--Select--")
        End If
       
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        If ddlFloor.SelectedIndex <> -1 Or ddlFloor.SelectedIndex = 0 Then
            strSQL = "select distinct spc_type as 'type',spc_type as 'id' from space order by spc_type"
            BindCombo(strSQL, ddlSpacetype, "type", "id")
        Else
            ddlSpacetype.Items.Clear()
            ddlSpacetype.Items.Add("--Select--")
        End If
     
    End Sub

    Protected Sub ddlSpacetype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSpacetype.SelectedIndexChanged
        If ddlSpacetype.SelectedIndex <> -1 Or ddlSpacetype.SelectedIndex = 0 Then
            Dim spFlrID As New SqlParameter("@FLR", SqlDbType.NVarChar, 50)
            Dim spTwrID As New SqlParameter("@TWR", SqlDbType.NVarChar, 50)
            Dim spLocID As New SqlParameter("@LOC", SqlDbType.NVarChar, 50)
            Dim spType As New SqlParameter("@TYPE", SqlDbType.NVarChar, 50)
            spFlrID.Value = ddlFloor.SelectedItem.Value
            spTwrID.Value = ddlTower.SelectedValue
            spLocID.Value = ddlLocation.SelectedValue
            spType.Value = ddlSpacetype.SelectedItem.Text.Trim()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"USP_GET_ACTIVESPACES", spFlrID, spTwrID, spLocID, spType)
            BindCombo(ds, ddlSpaces, "name", "id")
        Else
            ddlSpaces.Items.Clear()
            ddlSpaces.Items.Add("--Select--")
        End If
        
    End Sub
End Class
