<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmERBFac_Participants.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmERBFac_Participants" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script language="JavaScript" type="text/javascript" src="../../EmpSearchSuggest.js"></script>

    <style type="text/css">	
       
	   .suggest_link 
	   {
	   background-color: #FFFFFF;
	   padding: 2px 6px 2px 6px;
	   }	
	   .suggest_link_over
	   {
	   background-color: #3366CC;
	   padding: 2px 6px 2px 6px;	
	   }	
	   #search_suggest 
	   {
	   position: absolute;
	   background-color: #FFFFFF;
	   text-align: left;
	   border: 1px solid #000000;			
	   }
	</style>
</head>

<script type="text/javascript">
 
 
function querySt(ji) {
hu = window.location.search.substring(1);
gy = hu.split("&");
for (i=0;i<gy.length;i++) {
ft = gy[i].split("=");
if (ft[0] == ji) {
return ft[1];
}
}
}
 
//var flr_id = querySt("FLR_ID");
//var box = querySt("box");
 // var spc_id = querySt("spc_id");
</script>

<script language="javascript" type="text/javascript"> 
  function popupClosing() 
  {
  window.parent.location.href = 'ConferenceBookingFacilities.aspx?req_id=' + querySt("req_id");
  parent.parent.GB_hide();
    }

</script>

<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblHead" runat="server" Width="100%" CssClass="clsMessage" Visible="False"> Select Participants and Facilities</asp:Label>
        <asp:Panel ID="pnlhead" Width="95%" runat="server">
            <fieldset>
                <legend>Search Employee by Employee Id and Name</legend>
                <table class="tbl" id="tblmain1" cellspacing="0" cellpadding="0" width="100%" align="center">
                    <tr>
                        <td class="clsTbl" width="30%">
                            &nbsp;Select Participants</td>
                    </tr>
                </table>
                <table id="table1" cellspacing="2" cellpadding="2" width="100%" border="1" style="border-collapse: collapse">
                    <tr>
                        <td valign="top" align="left" class="bodytext">
                            Enter Employee Code/Name
                        </td>
                        <td valign="top" align="left">
                            <asp:TextBox ID="txtSearch" runat="server" alt="Search Criteria" onkeyup="searchSuggest(event);"
                                autocomplete="off"></asp:TextBox>
                            <div id="search_suggest" style="z-index: 2; visibility: hidden; position: absolute;">
                            </div>
                            <asp:Button ID="btnSearch" runat="server" Text="Add" CssClass="clsButton" Width="150" />
                        </td>
                    </tr>
                    <tr>
                        <td width="50%" colspan="2">
                            </td>
                    </tr>
                </table>
                            <asp:DataGrid ID="dgParticipantsSearch" Width="100%" runat="server" PageSize="5"
                                AllowPaging="True" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="SNo" Visible="false">
                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="Label2" runat="server" Text="<%# Container.ItemIndex+1 %>" Height="11px">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID">
                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Name" HeaderText="Name">
                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="depname" HeaderText="Department">
                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Remarks" HeaderText="Remarks" Visible="false">
                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn Text="Delete" HeaderText="Remove" CommandName="Delete">
                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                    </asp:ButtonColumn>
                                </Columns>
                                <PagerStyle VerticalAlign="Bottom" NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right">
                                </PagerStyle>
                            </asp:DataGrid>
            </fieldset>
            <br>
            <fieldset>
                <legend>Search Employee by Department </legend>
                <table class="tbl" id="tblmain" cellspacing="0" cellpadding="0" width="100%" align="center">
                    <tr>
                        <td align="center" width="100%">
                            <asp:Panel ID="pnlmain" Width="100%" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td class="label" style="width: 456px" align="left" width="400">
                                            Filter :</td>
                                        <td>
                                            <asp:DropDownList ID="cboFilter" runat="server" CssClass="clsComboBox" Width="100%"
                                                AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="Label1" runat="server" Font-Bold="True">Do You want Your name to be included in the Participants?</asp:Label>&nbsp;<strong>:</strong></td>
                                        <td valign="middle">
                                            <asp:RadioButtonList ID="radSelection" runat="server" Width="112px" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="0">Yes</asp:ListItem>
                                                <asp:ListItem Value="1" Selected="true">No</asp:ListItem>
                                            </asp:RadioButtonList></td>
                                    </tr>
                                </table>
                                <br>
                                <table width="100%">
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:DataGrid ID="dgParticipants" Width="100%" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="True" BorderStyle="None">
                                                <Columns>
                                                    <asp:BoundColumn Visible="False" DataField="aur_id" HeaderText="ID">
                                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="aur_known_as" HeaderText="Name">
                                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="depname" HeaderText="Department">
                                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn Visible="False" HeaderText="Remarks">
                                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtRemPar" Width="100%" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Select">
                                                        <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                        <%-- <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="True" ToolTip="Select/Deselect All">
                                                        </asp:CheckBox>
                                                    </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelectPar" runat="server" AutoPostBack="false"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                                <PagerStyle VerticalAlign="Bottom" NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right">
                                                </PagerStyle>
                                            </asp:DataGrid>
                                            <asp:Button ID="btnSubmit" runat="server" Visible="False" CssClass="clsButton" Text="Submit"
                                                CausesValidation="false" OnClientClick="popupClosing()"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label ID="lblmsg" runat="server" CssClass="clsmessage"></asp:Label>
                                <asp:Label ID="lblError" runat="server" CssClass="clsNote"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
    </form>
</body>
</html>
