﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repEmployeeUnoccupied
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim objMasters As clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        If Not Page.IsPostBack Then

            BindCity()
            BindLocation("--All--")
            ddlLocation.Items.Insert(0, "--All--")
            ddlLocation.Items.RemoveAt(1)
            ddlTower.Items.Insert(0, "--All--")
            ddlFloor.Items.Insert(0, "--All--")
            BindEmployeeData()
        End If
    End Sub
    Public Sub BindCity()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")
        ddlCity.Items.Insert(0, "--All--")
        ddlCity.Items.RemoveAt(1)
    End Sub
    Public Sub BindLocation(ByVal MCity As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = MCity
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONBY_CTY", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        BindEmployeeData()
    End Sub
    Public Sub BindEmployeeData()
        Dim rds As New ReportDataSource()
        rds.Name = "EmployeeUnoccupiedDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/EmployeeUnoccupiedReport.rdlc")


        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True


        Dim MCity As String = ""
        Dim MLocation As String = ""
        Dim MTower As String = ""
        Dim MFloor As String = ""

        If ddlCity.SelectedValue = "--All--" Then
            MCity = ""
        Else
            MCity = ddlCity.SelectedValue
        End If

        If ddlLocation.SelectedValue = "--All--" Then
            MLocation = ""
        Else
            MLocation = ddlLocation.SelectedValue
        End If

        If ddlTower.SelectedValue = "--All--" Then
            MTower = ""
        Else
            MTower = ddlTower.SelectedValue
        End If

        If ddlFloor.SelectedValue = "--All--" Then
            MFloor = ""
        Else
            MFloor = ddlFloor.SelectedValue
        End If

        Dim sp1 As New SqlParameter("@CITY_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = MCity
        Dim sp2 As New SqlParameter("@LCM_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = MLocation
        Dim sp3 As New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = MTower
        Dim sp4 As New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp4.Value = MFloor
        Dim sp5 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
        sp5.Value = "AUR_ID"
        Dim sp6 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
        sp6.Value = "ASC"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_EMPLOYEEUNOCCUPIED_REPORT", sp1, sp2, sp3, sp4, sp5, sp6)
        rds.Value = dt

    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        ReportViewer1.Visible = False
        ddlLocation.Items.Clear()
        BindLocation(ddlCity.SelectedItem.Value)

        If ddlLocation.Items.Count = 0 Then
            lblMsg.Text = "No Locations Available for Selected City"
            lblMsg.Visible = True
        Else
            ddlLocation.Items(0).Text = "--All--"
            lblMsg.Visible = False
        End If

        obj.bindTower_Locationwise(ddlTower, ddlLocation.SelectedValue)
        ddlTower.Items(0).Text = "--All--"
        If ddlTower.Items.Count = 2 Then
            ddlTower.SelectedIndex = 0
        End If
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
        ddlFloor.Items(0).Text = "--All--"
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
        End If

    End Sub
    
    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        ReportViewer1.Visible = False
        obj.bindTower_Locationwise(ddlTower, ddlLocation.SelectedValue)
        ddlTower.Items(0).Text = "--All--"
        If ddlTower.Items.Count = 2 Then
            ddlTower.SelectedIndex = 0
        End If
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
        ddlFloor.Items(0).Text = "--All--"
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
        ddlFloor.Items(0).Text = "--All--"
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
        End If
    End Sub
End Class
