Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports Npgsql
Partial Class WorkSpace_SMS_Webfiles_frmusermapfloorlist_temp
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim space_id, Layer_id As String
    Dim stringdate As String
    Dim ObjPostGres As New cls_OLEDB_postgresSQL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            stringdate = Request.QueryString("Date")
            If stringdate.Contains("%2f") Then
                stringdate.Replace("%2f", "/")
            End If

            GetLocationURL()
            Bind_Department()
            BindConsolidate()
            BindSpaceType()
            'BindQueryanalyzer()
            'BindShiftWiseView()

            'If Session("ddldwnval") IsNot Nothing Then
            '    If (Not drpdwnValue.Items.FindByValue(Session("ddldwnval")) Is Nothing) Then
            '        drpdwnValue.Items.FindByValue(Session("ddldwnval")).Selected = True
            '    End If
            'End If
        End If
    End Sub

    Public Sub BindSpaceType()
        'Start get the FLR_ADM_MAP (flr id) from the floor table
        Dim LCM_CODE As String = ""
        Dim TWR_CODE As String = ""
        Dim FLR_CODE As String = ""
        Dim FLR_ADM_MAP As String = ""
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("lcm_code")
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("twr_code")
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = Request.QueryString("flr_code")
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_URL_LOCATION", param)
        If ds.Tables(0).Rows.Count > 0 Then
            FLR_ADM_MAP = ds.Tables(0).Rows(0).Item("FLR_ADM_MAP")
        Else
            FLR_ADM_MAP = ""
        End If
        'End get the FLR_ADM_MAP (flr id) from the floor table

        Dim param1(1) As Npgsql.NpgsqlParameter
        param1(0) = New Npgsql.NpgsqlParameter("@flr_id", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param1(0).Value = FLR_ADM_MAP
        param1(1) = New Npgsql.NpgsqlParameter("@tenantid", NpgsqlTypes.NpgsqlDbType.Text, 200)
        param1(1).Value = Session("tenant")
        Dim ds1 As DataSet = ObjPostGres.PostGresGetDataSet("get_spacecount_spacetypebyflrid", param1)


        dlspacetype.DataSource = ds1
        dlspacetype.DataBind()

        For i As Integer = 0 To dlspacetype.Rows.Count - 1
            Dim lblspctype As Label = CType(dlspacetype.Rows(i).FindControl("lblspctype"), Label)

            If lblspctype.Text = 1 Then
                lblspctype.Text = "Dedicated"
            ElseIf lblspctype.Text = 2 Then
                lblspctype.Text = "Shared"
            ElseIf lblspctype.Text = 3 Then
                lblspctype.Text = "Shared"
            ElseIf lblspctype.Text = 4 Then
                lblspctype.Text = "BCP"
            End If


        Next
    End Sub

    Private Sub Bind_Department()
        Dim spCount1 As New SqlParameter("@SSA_TWR_ID", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@SSA_BDG_ID", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@SSA_FLR_ID", SqlDbType.VarChar, 500)
        Dim spCount4 As New SqlParameter("@SSA_DATE", SqlDbType.Date)
        spCount1.Value = Request.QueryString("twr_code")
        spCount2.Value = Request.QueryString("lcm_code")
        spCount3.Value = Request.QueryString("flr_code")
        spCount4.Value = CDate(stringdate)

        'Response.Write(selTower & "<br/>" & selBld & "<br>" & selfloor)

        Dim MCount As Integer = 0
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USER_GETCONSOLIDATED_Dep_Report_TEMP", spCount1, spCount2, spCount3, spCount4)

        GridView1.DataSource = ds
        GridView1.DataBind()

    End Sub

    Private Sub GetLocationURL()

        Dim LCM_CODE As String = ""
        Dim TWR_CODE As String = ""
        Dim FLR_CODE As String = ""
        Dim FLR_USR_MAP As String = ""
        Dim spc_id As String = ""
        Dim BBOX As String = ""
        Dim Date1 As Date

        LCM_CODE = Request.QueryString("lcm_code")
        TWR_CODE = Request.QueryString("twr_code")
        FLR_CODE = Request.QueryString("flr_code")
        Date1 = Request.QueryString("Date")

        Dim Hparam(1) As SqlParameter
        Hparam(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
        Hparam(0).Value = LCM_CODE
        Hparam(1) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        Hparam(1).Value = FLR_CODE
        'Hparam(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        'Hparam(2).Value = FLR_CODE

        Dim Hds As New DataSet
        Hds = ObjSubsonic.GetSubSonicDataSet("GET_MAP_HEADING", Hparam)
        If Hds.Tables(0).Rows.Count > 0 Then
            lblHead.Text = Hds.Tables(0).Rows(0).Item("HEADING")
            GetFloors_Map(Hds.Tables(0).Rows(0).Item("FLR_ID"))
        End If

        'lblHead.Text = LCM_CODE & " / " & TWR_CODE & " / " & FLR_CODE


        spc_id = Request.QueryString("spc_id")
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = LCM_CODE
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = TWR_CODE
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = FLR_CODE

        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_URL_LOCATION", param)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("FLR_USR_MAP") <> "" Then
                FLR_USR_MAP = ds.Tables(0).Rows(0).Item("FLR_USR_MAP")
                BBOX = ds.Tables(0).Rows(0).Item("BBOX")
            Else
                Response.Write("<script language=javascript>alert(""User map path is not defined"")</script>")

                Exit Sub
            End If
        End If


        Dim URL As String

        If spc_id = "''" Or spc_id Is Nothing Then

            URL = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&spc_id=''"

        Else


            URL = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&spc_id=" & Request.QueryString("spc_id")

        End If

        midFrame.Attributes("src") = "BubleViewMap.aspx?lcm_code=" & LCM_CODE & "&bbox=" & BBOX & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE
        'midFrame.Attributes("src") = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE

    End Sub

    Public Sub GetFloors_Map(FLRID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar)
        param(0).Value = Session("UID").ToString()
        ObjSubsonic.Binddropdown(ddlfloors, "GET_FLOORS_FOR_MAP", "FLR_NAME", "FLR_ID", param)
        ddlfloors.Items.FindByValue(FLRID).Selected = True
    End Sub

    'Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
    '    If e.CommandName = "select" Then

    '        Dim index As String = e.CommandArgument
    '        Dim ds As New DataSet
    '        Dim param(3) As SqlParameter
    '        param(0) = New SqlParameter("@DEPT", SqlDbType.NVarChar, 200)
    '        param(0).Value = index
    '        param(1) = New SqlParameter("@lcm_code", SqlDbType.NVarChar, 200)
    '        param(1).Value = Request.QueryString("lcm_code")
    '        param(2) = New SqlParameter("@twr_code", SqlDbType.NVarChar, 200)
    '        param(2).Value = Request.QueryString("twr_code")
    '        param(3) = New SqlParameter("@flr_code", SqlDbType.NVarChar, 200)
    '        param(3).Value = Request.QueryString("flr_code")

    '        ds = ObjSubsonic.GetSubSonicDataSet("GETSPACEID_DEPT", param)
    '        Dim mspc_id As String


    '        If ds.Tables(0).Rows.Count > 0 Then
    '            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '                mspc_id = mspc_id & ds.Tables(0).Rows(i).Item("SSA_SPC_ID").ToString & ","
    '            Next
    '        End If

    '        If mspc_id.EndsWith(",") Then
    '            mspc_id = mspc_id.Remove(mspc_id.Length - 1, 1)
    '        End If
    '        Response.Redirect("frmusermapfloorlist_temp.aspx?lcm_code=" & Request.QueryString("lcm_code") & "&twr_code=" & Request.QueryString("twr_code") & "&flr_code=" & Request.QueryString("flr_code") & "&spc_id=" & mspc_id & "&Date=" & CDate(stringdate).ToString().Replace("%2f", "/"))
    '    End If

    'End Sub

    Public Sub BindConsolidate()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 100)
        param(0).Value = Request.QueryString("LCM_CODE")
        param(1) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Request.QueryString("TWR_CODE")
        param(2) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 100)
        param(2).Value = Request.QueryString("FLR_CODE")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_WNGSFORSPACE", param)
        dlWings.DataSource = ds
        dlWings.DataBind()

        For i As Integer = 0 To dlWings.Items.Count - 1
            Dim dlSeatSummaryByWing As GridView = CType(dlWings.Items(i).FindControl("dlSeatSummaryByWing"), GridView)
            Dim lblwing As Label = CType(dlWings.Items(i).FindControl("lblwing"), Label)
            'lblwing.Text = ds.Tables(0).Rows(i).Item("SPC_WNG_ID")

            Dim param1(4) As SqlParameter
            param1(0) = New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 100)
            param1(0).Value = Request.QueryString("LCM_CODE")
            param1(1) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 100)
            param1(1).Value = Request.QueryString("TWR_CODE")
            param1(2) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 100)
            param1(2).Value = Request.QueryString("FLR_CODE")
            param1(3) = New SqlParameter("@WNG_ID", SqlDbType.NVarChar, 100)
            param1(3).Value = lblwing.Text
            param1(4) = New SqlParameter("@DATE", SqlDbType.Date)
            param1(4).Value = CDate(stringdate)
            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_ALLOCATION_SUMMARY_TEMP", param1)
            dlSeatSummaryByWing.DataSource = ds1
            dlSeatSummaryByWing.DataBind()

        Next


    End Sub

    'Private Sub BindQueryanalyzer()


    '    Dim selfloor As String = String.Empty
    '    Dim selTower As String = String.Empty
    '    Dim selBld As String = String.Empty
    '    Dim sdate As Date

    '    selfloor = "'" & Request.QueryString("FLR_CODE") & "'"
    '    Session("FLR_CODE") = Request.QueryString("FLR_CODE")
    '    selTower = Request.QueryString("Twr_Code")
    '    Session("Twr_Code") = selTower
    '    selBld = Request.QueryString("LCM_CODE")
    '    Session("BDG_ID") = selBld


    '    If Not IsPostBack() Then
    '        Dim query As String = String.Empty

    '        strSQL = "select smq_Id, smq_field from  " & Session("TENANT") & "." & "SMS_MAP_QUERY  where smq_sta_id=1"
    '        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

    '        drpdwnCategory.DataValueField = "smq_Id"
    '        drpdwnCategory.DataTextField = "smq_field"
    '        drpdwnCategory.DataSource = ObjDR
    '        drpdwnCategory.DataBind()
    '        ObjDR.Close()

    '        drpdwnCategory.SelectedIndex = 0

    '        strSQL = "select smq_query from  " & Session("TENANT") & "." & "SMS_MAP_QUERY where smq_Id= " & drpdwnCategory.SelectedItem.Value & ""
    '        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    '        If ObjDR.Read() Then
    '            query = ObjDR("smq_query").ToString()
    '            query = String.Format(query, Session("TENANT") & ".")

    '        End If
    '        ObjDR.Close()


    '        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, query & selfloor & "and ssa_Twr_id = '" & selTower & "' and ssa_bdg_id = '" & selBld & "' Order by DisValue")

    '        'Response.Write(query & selfloor & "and ssa_Twr_id = '" & selTower & "' and ssa_bdg_id = '" & selBld & "' Order by DisValue")

    '        drpdwnValue.DataSource = ObjDR
    '        drpdwnValue.DataValueField = "Value"
    '        drpdwnValue.DataTextField = "DisValue"
    '        drpdwnValue.DataBind()
    '        'ObjDR.Close()
    '        If ObjDR.IsClosed = False Then
    '            ObjDR.Close()

    '        End If
    '        If drpdwnValue.Items.Count = 0 Then
    '            drpdwnValue.Items.Clear()
    '            drpdwnValue.Items.Insert("0", "No Data")
    '        End If
    '    End If
    'End Sub

    'Protected Sub drpdwnCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpdwnCategory.SelectedIndexChanged
    '    Dim objdata As SqlDataReader
    '    Dim query As String = String.Empty
    '    Dim selfloor As String
    '    Dim selTower As String = String.Empty
    '    Dim selBld As String = String.Empty

    '    selfloor = "'" & Session("FLR_CODE") & "'"
    '    selTower = Session("Twr_Code")
    '    selBld = Session("BDG_ID")

    '    'ObjComm = New SqlCommand("select smq_query from  " & AppSettings("APPDB") & "SMS_MAP_QUERY where smq_Id= " & drpdwnCategory.SelectedItem.Value & "", ObjCon)
    '    strSQL = "select smq_query from  " & Session("TENANT") & "." & "SMS_MAP_QUERY where smq_Id= " & drpdwnCategory.SelectedItem.Value & ""
    '    objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    '    If objdata.Read() Then
    '        query = objdata("smq_query").ToString()
    '        query = String.Format(query, Session("TENANT") & ".")
    '    End If
    '    objdata.Close()
    '    If drpdwnCategory.SelectedValue.ToString().Trim() = "2" Then
    '        query = query & selfloor & " and SPC_TWR_ID = '" & selTower & "' and SPC_BDG_ID = '" & selBld & "' Order by DisValue"
    '    Else
    '        query = query & selfloor & " and SSA_TWR_ID = '" & selTower & "' and SSA_BDG_ID = '" & selBld & "' Order by DisValue"
    '    End If


    '    objdata = SqlHelper.ExecuteReader(CommandType.Text, query)
    '    drpdwnValue.DataSource = objdata
    '    drpdwnValue.DataValueField = "Value"
    '    drpdwnValue.DataTextField = "DisValue"
    '    drpdwnValue.DataBind()
    '    objdata.Close()

    '    If drpdwnValue.Items.Count = 0 Then
    '        drpdwnValue.Items.Clear()
    '        drpdwnValue.Items.Insert("0", "No Data")
    '    End If
    'End Sub

    'Protected Sub ImgFetch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgFetch.Click
    '    Dim spc_id As String = ""
    '    Dim selfloor As String
    '    Dim selTower As String = String.Empty
    '    Dim selBld As String = String.Empty
    '    Session("ddldwnval") = drpdwnValue.SelectedValue
    '    selfloor = "'" & Session("FLR_CODE") & "'"
    '    selTower = Session("Twr_Code")
    '    selBld = Session("lcm_code")
    '    drpdwnValue.Items.FindByValue(Session("ddldwnval")).Selected = True
    '    Dim dsSpacedetails As New DataSet

    '    Dim param(0) As SqlParameter

    '    param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '    param(0).Value = Session("ddldwnval")
    '    dsSpacedetails = ObjSubsonic.GetSubSonicDataSet("GETSPACEDETAILS_AURID", param)
    '    If dsSpacedetails.Tables(0).Rows.Count > 0 Then
    '        spc_id = dsSpacedetails.Tables(0).Rows(0).Item("SSA_SPC_ID")

    '        Dim count1 As Integer = 0
    '        midFrame.Attributes("src") = "SpaceViewmap_queryanly.aspx?spcid=" & spc_id.ToString() & "&aur_id= " & Session("ddldwnval")

    '    End If
    '    'Dim objdata As SqlDataReader

    '    'If (drpdwnCategory.Items.Count = 0) Then
    '    '    Response.Write("<script language=javascript>alert(""No Items to Fetch"")</script>")
    '    'Else
    '    '    If (drpdwnCategory.SelectedItem.Value = "") Or (drpdwnValue.SelectedItem.Value = "No Data") Then
    '    '        Response.Write("<script language=javascript>alert(""No Items to Fetch"")</script>")
    '    '    Else
    '    '        Dim q1, q2, mainquery, opt, q3
    '    '        q1 = drpdwnCategory.SelectedItem.Value
    '    '        opt = drpdwnOpt.SelectedItem.Value
    '    '        q2 = drpdwnValue.SelectedItem.Text
    '    '        q3 = txtdate.Text

    '    '        strSQL = "select smq_result from  " & Session("TENANT") & "."  & "SMS_MAP_QUERY where smq_Id = '" & q1 & "'"

    '    '        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    '    '        If objdata.Read() Then
    '    '            mainquery = objdata("smq_result").ToString()
    '    '        End If

    '    '        objdata.Close()

    '    '        If q1 = 2 Then

    '    '            mainquery = mainquery & opt & "'" & q2 & "'"
    '    '        ElseIf q3 = String.Empty Then
    '    '            mainquery = mainquery & opt & "'" & q2 & "'"
    '    '        Else
    '    '            mainquery = mainquery & opt & "'" & q2 & "' and '" & q3 & "' Between SSA_FROM_DATE AND SSA_TO_DT "
    '    '        End If

    '    '        'Response.Write(strSQL)
    '    '        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, mainquery)


    '    '        If ObjDR.Read() Then
    '    '            If q1 = "1" Or q1 = "3" Or q1 = "4" Or q1 = "5" Or q1 = "6" Then
    '    '                space_id = ObjDR("ssa_spc_Id").ToString()
    '    '                Layer_id = ObjDR("spc_layer").ToString()
    '    '            Else
    '    '                space_id = ObjDR("SPC_ID").ToString()
    '    '                Layer_id = ObjDR("spc_layer").ToString()
    '    '            End If


    '    '        End If

    '    '        ObjDR.Close()

    '    '        Response.Write("<Script language=Javascript>window.open(""../GIS/Query_Results.aspx?q1=" & drpdwnCategory.SelectedItem.Value & "&Date=" & txtdate.Text & "&opt= " & drpdwnOpt.SelectedItem.Value & "&q2=" & drpdwnValue.SelectedItem.Value & """,""QueryResults"",""Top=0,left=470,height=300,width=320,scrollbars=no"")</Script>")
    '    '    End If
    '    'End If
    'End Sub

    'Protected Sub btn1_Click(sender As Object, e As EventArgs) Handles btn1.Click
    '    ' Response.Redirect(Request.RawUrl)
    '    ddlsearchforsub.ClearSelection()
    '    GetLocationURL()
    'End Sub

    'Protected Sub BindShiftWiseView()
    '    'Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GET_ACTIVE_SHIFTS")
    '    'ddlsearchforsub.DataSource = ds
    '    'ddlsearchforsub.DataValueField = "Code"
    '    'ddlsearchforsub.DataTextField = "Name"
    '    'ddlsearchforsub.DataBind()
    '    'ddlsearchforsub.Items.Insert(0, New ListItem("--All--", "0"))
    '    Dim Hparam(0) As SqlParameter
    '    Hparam(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
    '    Hparam(0).Value = Request.QueryString("lcm_code")
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_ACTIVE_SHIFTS_LOCTIONWISE", Hparam)
    '    ddlsearchforsub.DataSource = ds
    '    ddlsearchforsub.DataValueField = "Code"
    '    ddlsearchforsub.DataTextField = "Name"
    '    ddlsearchforsub.DataBind()
    '    ddlsearchforsub.Items.Insert(0, New ListItem("--All--", "0"))
    'End Sub

    'Protected Sub ddlsearchforsub_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlsearchforsub.SelectedIndexChanged
    '    If ddlsearchforsub.SelectedItem.Value.Equals("0") Then
    '        GetLocationURL()
    '    Else
    '        GetLocationURL_Shift(ddlsearchforsub.SelectedItem.Value)
    '    End If
    'End Sub

    'Private Sub GetLocationURL_Shift(ByVal shft As String)
    '    Dim LCM_CODE As String = ""
    '    Dim TWR_CODE As String = ""
    '    Dim FLR_CODE As String = ""
    '    Dim FLR_USR_MAP As String = ""
    '    Dim spc_id As String = ""
    '    Dim BBOX As String = ""
    '    Dim Date1 As Date

    '    LCM_CODE = Request.QueryString("lcm_code")
    '    TWR_CODE = Request.QueryString("twr_code")
    '    FLR_CODE = Request.QueryString("flr_code")
    '    Date1 = Request.QueryString("Date")

    '    Dim Hparam(1) As SqlParameter
    '    Hparam(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
    '    Hparam(0).Value = LCM_CODE
    '    Hparam(1) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
    '    Hparam(1).Value = FLR_CODE
    '    'Hparam(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
    '    'Hparam(2).Value = FLR_CODE

    '    Dim Hds As New DataSet
    '    Hds = ObjSubsonic.GetSubSonicDataSet("GET_MAP_HEADING", Hparam)
    '    If Hds.Tables(0).Rows.Count > 0 Then
    '        lblHead.Text = Hds.Tables(0).Rows(0).Item("HEADING")
    '    End If

    '    'lblHead.Text = LCM_CODE & " / " & TWR_CODE & " / " & FLR_CODE


    '    spc_id = Request.QueryString("spc_id")
    '    Dim param(2) As SqlParameter
    '    param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
    '    param(0).Value = LCM_CODE
    '    param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
    '    param(1).Value = TWR_CODE
    '    param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
    '    param(2).Value = FLR_CODE

    '    Dim ds As New DataSet
    '    ds = ObjSubsonic.GetSubSonicDataSet("GET_URL_LOCATION", param)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        FLR_USR_MAP = ds.Tables(0).Rows(0).Item("FLR_USR_MAP")
    '        BBOX = ds.Tables(0).Rows(0).Item("BBOX")
    '    End If


    '    Dim URL As String

    '    If spc_id = "''" Or spc_id Is Nothing Then

    '        URL = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&spc_id=''"

    '    Else

    '        URL = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&spc_id=" & Request.QueryString("spc_id")

    '    End If

    '    midFrame.Attributes("src") = "BubbleViewMap_Shift.aspx?lcm_code=" & LCM_CODE & "&bbox=" & BBOX & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE & "&shift_code=" & shft
    '    'midFrame.Attributes("src") = FLR_USR_MAP & "&lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE

    'End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(0).Text = Session("Parent")
        End If
    End Sub
End Class
