﻿Imports System.Data
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_spacetype
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim objpgsql As New cls_OLEDB_postgresSQL

    Protected Sub btnassign_Click(sender As Object, e As EventArgs) Handles btnassign.Click



        Dim rstat As Int32 = AddSpacedata()
        If rstat = 0 Then
            lblMsg.Text = "Port Number already exists"
        ElseIf rstat = 1 Then
            lblMsg.Text = "Space Data added succesfully"
        Else
            UpdateSpaceType(lblspace.Text, ddlspctype.SelectedItem.Value)
            lblMsg.Text = "Space Data Modified succesfully"

            ' Cleardata()
        End If




    End Sub
    Private Sub Cleardata()
        txtportnumber.Text = ""
        ddlcarved.SelectedIndex = 0
        ddlarea.SelectedIndex = 0
        ddlportstatus.SelectedIndex = 0
        ddlporttype.SelectedIndex = 0
        ddlbcpseattype.SelectedIndex = 0
        ddlmcp.SelectedIndex = 0
    End Sub

    Private Function AddSpacedata()
        Dim rstat As Int32
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SPACE_DATA")
        sp.Command.AddParameter("@SPACE_ID", lblspace.Text, DbType.String)
        sp.Command.AddParameter("@PORT_NUMBER", txtportnumber.Text, DbType.String)
        sp.Command.AddParameter("@CARVED", ddlcarved.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AREA_TYPE", ddlarea.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PORT_STATUS", ddlportstatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PORT_TYPE", ddlporttype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@BCP_TYPE", ddlbcpseattype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@MCP_SEAT", ddlmcp.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SEAT_TYPE", ddlspctype.SelectedItem.Value, DbType.String)
        rstat = sp.ExecuteScalar()
        Return rstat
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim space_id As String = Request.QueryString("SPC_ID")
            lblspace.Text = space_id
            BindDropdowns()
            BindSpaceType()
            GetSpaceData()
        End If

    End Sub
    Private Sub BindSpaceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TYPE_SPACE")
        sp.Command.AddParameter("@SPACE_ID", Request.QueryString("spc_id"), DbType.String)
        Dim ds1 As DataSet = sp.GetDataSet()
        If ds1.Tables(0).Rows.Count > 0 Then
            ddlspctype.Items.FindByValue(ds1.Tables(0).Rows(0).Item("SPACE_TYPE")).Selected = True
        Else
            ddlspctype.SelectedIndex = 0
        End If

    End Sub
    Private Sub GetSpaceData()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_CARVED_DATA")
        sp.Command.AddParameter("@SPACE_ID", Request.QueryString("spc_id"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then

            txtportnumber.Text = ds.Tables(0).Rows(0).Item("SPACE_PORT_NUMBER")
            ddlcarved.ClearSelection()
            ddlcarved.Items.FindByValue(ds.Tables(0).Rows(0).Item("SPACE_CARVED")).Selected = True
            ddlarea.ClearSelection()
            ddlarea.Items.FindByValue(ds.Tables(0).Rows(0).Item("SPACE_AREA_TYPE")).Selected = True
            ddlportstatus.ClearSelection()
            ddlportstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("SPACE_PORT_STATUS")).Selected = True
            ddlporttype.ClearSelection()
            ddlporttype.Items.FindByValue(ds.Tables(0).Rows(0).Item("SPACE_PORT_TYPE")).Selected = True
            ddlbcpseattype.ClearSelection()
            ddlbcpseattype.Items.FindByValue(ds.Tables(0).Rows(0).Item("SPACE_BCP_SEAT_TYPE")).Selected = True
            ddlmcp.ClearSelection()
            ddlmcp.Items.FindByValue(ds.Tables(0).Rows(0).Item("SPACE_MCP_SEAT")).Selected = True
            ddlspctype.ClearSelection()
            ddlspctype.Items.FindByValue(ds.Tables(0).Rows(0).Item("SPACE_TYPE")).Selected = True
        Else
            Cleardata()
        End If

    End Sub
    Private Sub BindDropdowns()
        objsubsonic.Binddropdown(ddlcarved, "GET_CARVED", "NAME", "SNO")
        objsubsonic.Binddropdown(ddlarea, "GET_AREA", "NAME", "SNO")
        objsubsonic.Binddropdown(ddlportstatus, "GET_PORTSTATUS", "NAME", "SNO")
        objsubsonic.Binddropdown(ddlporttype, "GET_PORTTYPE", "NAME", "SNO")
        objsubsonic.Binddropdown(ddlbcpseattype, "GET_BCPSEATTYPE", "NAME", "SNO")
        objsubsonic.Binddropdown(ddlmcp, "GET_MCP", "NAME", "SNO")
        objsubsonic.Binddropdown(ddlspctype, "GET_SPACE_TYPE", "SPACE_TYPE", "SNO")
    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("Spacetypemaster.aspx")
    End Sub
End Class
