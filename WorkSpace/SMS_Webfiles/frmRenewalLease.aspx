<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmRenewalLease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmRenewalLease" Title="Renewal Lease" %>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>    
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Renewal Lease</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div id="PNLCONTAINER" runat="server">
                            
                            <div class="row" id="Tr1" runat="server" visible="false">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Select Lease Type<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                                    Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                        
                            <div id="panel1" runat="server">                                
                                    <h4>Current (Renewal)Lease</h4>                                
                                <div class="row">
                                  <div class="col-md-12">
                                        <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                            AllowPaging="True" PageSize="5" EmptyDataText="No Current Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Lease No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllname" runat="server" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CTS Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="City">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcity" runat="server" Text='<%#Eval("CITY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLesseName" runat="server" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Expiry Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLstatus" runat="server" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemTemplate>
                                                        <a href='frmModifyLeaseRenewal.aspx?id=<%#HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>'>Renewal of Lease</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>                            
                            <div id="panel2" runat="server">
                                
                                    <h4>Rejected (Renewal)Lease</h4>
                                
                                <div class="row" style="margin-top: 10px">
                                  <div class="col-md-12">
                                        <asp:GridView ID="gvrejected" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                            AllowPaging="True" PageSize="5" EmptyDataText="No Rejected Lease Details Found."
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>

                                                <asp:TemplateField HeaderText="Lease No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllname" runat="server" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CTS Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="City">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcity" runat="server" Text='<%#Eval("CITY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLesseName" runat="server" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Expiry Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLstatus" runat="server" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemTemplate>
                                                        <%--<a href="#" onclick="showPopWin('frmViewLeaseDetailsUser.aspx?id=<%#Eval("LEASE_NAME")%>',800,620,null)">View Details</a>--%>
                                                        <a href="#" onclick="showPopWin('<%# HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>')">View Details</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body" id="modelcontainer"> 
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>

   <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsUser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>
</body>
</html>
