
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_EmployeeMapping
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If

            If Not IsPostBack Then
                trLOC.Visible = True
                combofill()
            End If
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub combofill()
        If CHECK_ADMIN_ROLE(Session("UID")) Then
            strSQL = "SELECT DISTINCT  SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SRN_BGT_ID)+'/'+SRN_BGT_ID AS LOC  from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and srn_req_id not in  (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION) AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')  union SELECT DISTINCT SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SRN_BGT_ID)+'/'+SRN_BGT_ID AS LOC from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and  srn_req_id in (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where ssa_sta_id in (7)) AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')  UNION SELECT DISTINCT SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SSE_LCM_ID)+'/'+SRN_BGT_ID AS LOC FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SRN_STA_ID In (21)  and SSE_EMP_ID NOT IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID in (6,4,103)) AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')  UNION SELECT DISTINCT SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID)+'/'+SRN_BGT_ID AS LOC  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS," & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID AND SSE_EMP_ID  IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID IN (7)) AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')   order by LOC "
        Else
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
            arParms(0).Value = Session("uid")
            arParms(1) = New SqlParameter("@TYPE", SqlDbType.NVarChar, 10)
            arParms(1).Value = "LOC"
            Dim varloc As String = ""
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, Session("TENANT") & "."  & "ADMIN_CREATE", arParms)
            While ObjDR.Read
                If varloc = "" Then
                    varloc = "'" & ObjDR("CODE") & "'"
                Else
                    varloc = varloc & ",'" & ObjDR("CODE") & "'"
                End If
            End While
            ObjDR.Close()
            strSQL = "SELECT DISTINCT SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SRN_BGT_ID)+'/'+SRN_BGT_ID AS LOC   from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and srn_req_id not in  (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION) and SRN_APP_BDG in (" & varloc & ") AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')  union SELECT DISTINCT  SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SRN_BGT_ID)+'/'+SRN_BGT_ID AS LOC from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and  srn_req_id in (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where ssa_sta_id in (7)) and SRN_APP_BDG in (" & varloc & ") AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')   UNION SELECT DISTINCT SRN_BGT_ID,(select  DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID)+'/'+SRN_BGT_ID AS LOC FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SRN_STA_ID In (21)  and SSE_EMP_ID NOT IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID in (6,4,103)) and SRN_APP_BDG in (" & varloc & ") AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')  UNION SELECT DISTINCT SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID)+'/'+SRN_BGT_ID AS LOC  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SSE_EMP_ID  IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID IN (7)) and SRN_APP_BDG in (" & varloc & ") AND SRN_BGT_ID IS NOT NULL AND SRN_BGT_ID NOT IN ('NA','0')  order by LOC "
        End If
        cboLOC.Items.Clear()
        BindCombo(strSQL, cboLOC, "LOC", "SRN_BGT_ID")
    End Sub
    Public Function CHECK_ADMIN_ROLE(ByVal UID As String) As Integer

        Dim objdata As SqlDataReader
        Dim strSQL As String
        strSQL = "SELECT URL_USR_ID,ROL_ACRONYM,URL_SCOPEMAP_ID " & _
          "FROM  " & Session("TENANT") & "."  & "USER_ROLE, " & Session("TENANT") & "."  & "ROLE " & _
          "WHERE URL_ROL_ID=ROL_ID AND ROL_ACRONYM IN ('GADMIN','NADMIN') AND URL_USR_ID = '" & UID & "' ORDER BY ROL_ACRONYM"
        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        Dim locs As String = "'0'"
        Dim rol As String
        Dim userid As String
        While objdata.Read
            locs = locs & ",'" & objdata("URL_scopemap_ID").ToString & "'"
            userid = objdata("url_usr_id").ToString
            If rol = "" Then
                rol = objdata("rol_acronym").ToString
            Else
                rol = rol + "," + objdata("rol_acronym").ToString
            End If
        End While
        objdata.Close()

        If UCase(rol) = "GADMIN" Or UCase(rol) = "NADMIN" Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Private Sub BindData()
        If CHECK_ADMIN_ROLE(Session("UID")) Then
            strSQL = "SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SRN_BGT_ID) AS LOC  from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and srn_req_id not in  (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION) union SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SRN_BGT_ID) AS LOC from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and  srn_req_id in (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where ssa_sta_id in (7)) UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SSE_LCM_ID) AS LOC FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SRN_STA_ID In (21)  and SSE_EMP_ID NOT IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID in (6,4,103)) UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID) AS LOC  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS," & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID AND SSE_EMP_ID  IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID IN (7)) order by LOC "
        Else
            'query changed by rajkumar on 1 Apr 2008
            strSQL1 = "SELECT LCM_CODE FROM  " & Session("TENANT") & "."  & "ADMIN_LOCATION_VW WHERE AUR_ID='" & Session("uid") & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
            Dim varloc As String = ""
            While ObjDR.Read
                If varloc = "" Then
                    varloc = "'" & ObjDR("LCM_CODE") & "'"
                Else
                    varloc = varloc & ",'" & ObjDR("LCM_CODE") & "'"
                End If
            End While
            ObjDR.Close()
            strSQL = "SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SRN_BGT_ID) AS LOC   from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and srn_req_id not in  (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION) and SRN_APP_BDG in (" & varloc & ") union SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SRN_BGT_ID) AS LOC from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and  srn_req_id in (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where ssa_sta_id in (7)) and SRN_APP_BDG in (" & varloc & ")  UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select  DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID) AS LOC FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SRN_STA_ID In (21)  and SSE_EMP_ID NOT IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID in (6,4,103)) and SRN_APP_BDG in (" & varloc & ") UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID) AS LOC  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SSE_EMP_ID  IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID IN (7)) and SRN_APP_BDG in (" & varloc & ") order by LOC "
        End If
        BindGridSet(strSQL, grdReqList)

        If grdReqList.Rows.Count > 0 Then
            Dim i As Integer
            grdReqList.Visible = True
            For i = 0 To grdReqList.Rows.Count - 1
                If grdReqList.Rows(i).Cells(3).Text = 1 Then
                    grdReqList.Rows(i).Cells(2).Text = "Employee(s)"
                ElseIf grdReqList.Rows(i).Cells(3).Text = 2 Then
                    grdReqList.Rows(i).Cells(2).Text = "Non Employee(s)"
                Else
                    grdReqList.Rows(i).Cells(2).Text = "Na"
                End If
            Next
        Else
            grdReqList.Visible = False
            Response.Write("<p align=center class=clsmessage><br><br><br><br><br><br><br><br><br><br><br><br>No Requisitions For Employee Mapping</p>")
        End If
    End Sub

    Private Sub cboLOC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLOC.SelectedIndexChanged
        Try
            visibleTF(1)
            grdReqList.PageIndex = 0
            If cboLOC.SelectedItem.Text <> "--All--" And cboLOC.SelectedItem.Text <> "--Select--" Then
                BindData1()
            End If
            visibleTF(0)
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Private Sub BindData1()
        Dim varuid As String
        varuid = Session("uid")
        strSQL = "select url_usr_id,rol_acronym,URL_scopemap_ID " & _
                 " from  " & Session("TENANT") & "."  & "user_role, " & Session("TENANT") & "."  & "role " & _
                 " where url_rol_id=rol_id " & _
                 " and url_usr_id='" & Session("uid") & "'"
        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Dim locs As String = 0
        Dim rol As String = ""
        Dim userid As String
        While ObjDR.Read
            locs = locs & "," & ObjDR("URL_scopemap_ID").ToString
            userid = ObjDR("url_usr_id").ToString
            If rol = "" Then
                rol = ObjDR("rol_acronym").ToString
            Else
                rol = rol + "," + ObjDR("rol_acronym").ToString
            End If

        End While
        ObjDR.Close()
        If CHECK_ADMIN_ROLE(Session("UID")) Then
            strSQL = "SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SRN_BGT_ID) AS LOC  from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and srn_req_id not in  (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION) and SRN_APP_BDG='" & cboLOC.SelectedItem.Value & "' union SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SRN_BGT_ID) AS LOC from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and  srn_req_id in (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where ssa_sta_id in (7)) and SRN_APP_BDG='" & cboLOC.SelectedItem.Value & "' UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_code=SSE_LCM_ID) AS LOC FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SRN_STA_ID In (21)  and SSE_EMP_ID NOT IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID in (6,4,103)) and SRN_APP_BDG='" & cboLOC.SelectedItem.Value & "' UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID) AS LOC  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS," & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID AND SSE_EMP_ID  IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID IN (7)) and SRN_APP_BDG='" & cboLOC.SelectedItem.Value & "'order by SRN_REQ_ID  "
        Else
            strSQL = "SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SRN_BGT_ID) AS LOC   from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and srn_req_id not in  (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION) and SRN_APP_BDG='" & cboLOC.SelectedValue & "' union SELECT DISTINCT srn_type_id,SRN_REQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SRN_BGT_ID) AS LOC from  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SRN_STA_ID In (21) and  srn_req_id in (select distinct ssa_srnreq_id from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where ssa_sta_id in (7)) and SRN_APP_BDG='" & cboLOC.SelectedValue & "'  UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select  DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID) AS LOC FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SRN_STA_ID In (21)  and SSE_EMP_ID NOT IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID in (6,4,103)) and SRN_APP_BDG='" & cboLOC.SelectedValue & "' UNION SELECT DISTINCT srn_type_id,SSE_SRNREQ_ID,SRN_BGT_ID,(select DISTINCT TOP 1 lcm_name from  " & Session("TENANT") & "."  & "LOCATION WHERE lcm_code=SSE_LCM_ID) AS LOC  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION WHERE SSE_SRNREQ_ID=SRN_REQ_ID and SSE_EMP_ID  IN (select distinct ssa_AUR_ID from  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE  SSA_STA_ID IN (7)) and SRN_APP_BDG='" & cboLOC.SelectedValue & "' order by SRN_REQ_ID  "
        End If
        BindGridSet(strSQL, grdReqList)
        If grdReqList.Rows.Count > 0 Then
            Dim i As Integer
            grdReqList.Visible = True
            For i = 0 To grdReqList.Rows.Count - 1
                If grdReqList.Rows(i).Cells(3).Text = "1" Then
                    grdReqList.Rows(i).Cells(2).Text = "Employee(s)"
                ElseIf grdReqList.Rows(i).Cells(3).Text = "2" Then
                    grdReqList.Rows(i).Cells(2).Text = "Non Employee(s)"
                Else
                    grdReqList.Rows(i).Cells(2).Text = "Na"
                End If
            Next
        Else
            grdReqList.Visible = False
            Response.Write("<p align=center class=clsmessage><br><br><br><br><br><br><br><br><br><br><br><br>No Requisitions For Employee Mapping</p>")
        End If
    End Sub

    Protected Sub grdReqList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdReqList.PageIndexChanging
        Try
            visibleTF(1)
            If cboLOC.SelectedItem.Text = "--All--" Then
                grdReqList.PageIndex = e.NewPageIndex
                BindData()
            ElseIf cboLOC.SelectedItem.Text <> "--All--" And cboLOC.SelectedItem.Text <> "--Select--" Then
                grdReqList.PageIndex = e.NewPageIndex
                BindData1()
            End If
            visibleTF(0)
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub visibleTF(ByVal status As Integer)
        If status = 1 Then
            grdReqList.Columns(3).Visible = True
        Else
            grdReqList.Columns(3).Visible = False
        End If
    End Sub
End Class
