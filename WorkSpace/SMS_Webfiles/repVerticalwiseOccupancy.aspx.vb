Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repVerticalwiseOccupancy
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim MVer As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False
        lblSelVer.Text = Session("Parent")

        If Not Page.IsPostBack Then
            binddata_verticalwise(MVer, 7)
            objsubsonic.Binddropdown(ddlReqID, "GET_ALLCOSTCENTER_AURID_OCC", "COST_CENTER_NAME", "COST_CENTER_CODE")
            ddlReqID.Items(0).Text = "--All " & Session("Parent") & "(s)--"
            ddlReqID.Items(0).Value = " "

            lblSelParent.Text = lblSelParent.Text + " " + Session("Parent")
        End If

    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click



        If ddlReqID.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            MVer = ""

        Else
            MVer = ddlReqID.SelectedItem.Value

        End If
        binddata_verticalwise(MVer, 7)

    End Sub

    Public Sub binddata_verticalwise(ByVal vc_VerticalName As String, ByVal vc_StatusID As Integer)

        Dim sp1 As New SqlParameter("@vc_VerticalName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = vc_VerticalName
        Dim sp2 As New SqlParameter("@vc_StatusID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp2.Value = vc_StatusID
        Dim sp3 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
        sp3.Value = "SSA_SPC_ID"
        Dim sp4 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
        sp4.Value = "ASC"
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_VerticalWise", sp1, sp2, sp3, sp4)

        Dim rds As New ReportDataSource()
        rds.Name = "VerticalOccupancyDS"
        rds.Value = dt
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/VerticalOccupancyReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMPLOYEE_VERTICAL_COUNT")
        sp.Command.AddParameter("@VER_CODE", vc_VerticalName, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltot.Visible = True
            lbltot.Text = "Total Employees : " & ds.Tables(0).Rows(0).Item("EMPLOYEE_ID")
            lbltot.ForeColor = Drawing.Color.Black
            lbltot.Font.Bold = True

        End If
        lbltot.Visible = True
        t2.Visible = True
    End Sub


    Protected Sub ddlReqID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReqID.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class