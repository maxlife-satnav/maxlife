﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repFloorwiseutilization
    Inherits System.Web.UI.Page
    Dim objMasters As clsMasters
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False
        Try
            If Not Page.IsPostBack Then
                binddata()
                'ReportViewer1.Visible = False
                objMasters = New clsMasters()
                objMasters.BindTower(ddlTower)
                ddlTower.Items(0).Text = "--All Towers--"
                ddlFloor.Items.Insert(0, "--All Floors--")
                ddlFloor.SelectedIndex = 0
                lbltotal_util.ForeColor = Drawing.Color.Black
                lbltotal_util.Font.Bold = True
            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Dim dtReport, dtTemp As New DataTable
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click



        If ddlFloor.SelectedIndex = 0 Then
            lbl2.Visible = False
            'Exit Sub
        End If
        binddata()
        tutility.Visible = True


    End Sub

    Public Sub binddata()
        Try

            Dim rds As New ReportDataSource()
            rds.Name = "FloorUtilizationDS"
            'This refers to the dataset name in the RDLC file
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/FloorUtilizationReport.rdlc")
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True


            Dim MTowerId As String = ""
            Dim MFloorId As String = ""

            If ddlTower.SelectedValue = "--All Towers--" Then
                MTowerId = ""
            Else
                MTowerId = ddlTower.SelectedValue
            End If

            If ddlFloor.SelectedValue = "--All Floors--" Then
                MFloorId = ""
            Else
                MFloorId = ddlFloor.SelectedValue
            End If

            Dim sp3 As New SqlParameter("@vc_twrID", SqlDbType.NVarChar, 50)
            sp3.Value = MTowerId
            Dim sp4 As New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50)
            sp4.Value = MFloorId

            Dim sp5 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
            sp5.Value = "FLOOR"

            Dim sp6 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
            sp6.Value = "ASC"
            dtTemp = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_FLOORUTILITY_REPORT", sp3, sp4, sp5, sp6)



            rds.Value = dtTemp


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_FLOOR_UTI_COUNT")
            sp.Command.AddParameter("@TWR_ID", MTowerId, DbType.String)
            sp.Command.AddParameter("@FLR_ID ", MFloorId, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()

            If ds.Tables(0).Rows.Count > 0 Then
                lbltotal_util.Text = ds.Tables(0).Rows(0).Item("util") & "%"
                tutility.Visible = True
            Else
                tutility.Visible = False
            End If



        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        obj = New clsReports()
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)

        ddlFloor.Items(0).Text = "--All Floors--"
        ReportViewer1.Visible = False
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0

        End If
        tutility.Visible = False
    End Sub


End Class
