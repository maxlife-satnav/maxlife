﻿<%@ Page Title="" Language="VB" AutoEventWireup="false"
    CodeFile="empswap.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_empswap" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" src="<%=Page.ResolveUrl("~/EmpSearchSuggestswap.js")%>"></script>
    <script type="text/javascript" src="<%=Page.ResolveUrl("~/EmpSearchSuggestswap1.js")%>"></script>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>
    <style type="text/css">
        .suggest_link {
            background-color: #FFFFFF;
            padding: 2px 6px 2px 6px;
        }

        .suggest_link_over {
            background-color: #3366CC;
            padding: 2px 6px 2px 6px;
        }

        #search_suggest {
            position: absolute;
            background-color: #FFFFFF;
            text-align: left;
            border: 1px solid #000000;
        }

        .suggest_link1 {
            background-color: #FFFFFF;
            padding: 2px 6px 2px 6px;
        }

        .suggest_link_over1 {
            background-color: #3366CC;
            padding: 2px 6px 2px 6px;
        }

        #search_suggest1 {
            position: absolute;
            background-color: #FFFFFF;
            text-align: left;
            border: 1px solid #000000;
        }
    </style>
</head>
<body>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Employee Swapping
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                            ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Enter Employee Id<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtempswap"
                                            ErrorMessage="Please Enter First Employee Id" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtempswap" runat="Server" CssClass="form-control" onkeyup="searchSuggest(event);"
                                                autocomplete="off">
                                            </asp:TextBox>
                                            <div id="search_suggest" style="z-index: 2; visibility: hidden; position: absolute;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Enter Employee Id to Swap<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtemp2swap"
                                            runat="server" ErrorMessage="Please Enter Employee Id To Swap" Display="None"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtemp2swap" runat="Server" CssClass="form-control"
                                                onkeyup="searchSuggest1(event);" autocomplete="off">
                                            </asp:TextBox>
                                            <div id="search_suggest1" style="z-index: 2; visibility: hidden; position: absolute;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">

                                    <asp:Button ID="btnsubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" CausesValidation="true" />

                                </div>
                            </div>
                        </div>

                        <div id="swap" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row" id="trCName" runat="server">
                                            <label class="col-md-5 control-label">Employee Id</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpempid" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Employee Id</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpempid2" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row" id="Div2" runat="server">
                                            <label class="col-md-5 control-label">Employee Name</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpempname" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Employee Name</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpempname2" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row" id="Div3" runat="server">
                                            <label class="col-md-5 control-label">Email Id</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpemail" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Email Id</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpemail2" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row" id="Div1" runat="server">
                                            <label class="col-md-5 control-label">Department</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpdep" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Department</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpdep2" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row" id="Div4" runat="server">
                                            <label class="col-md-5 control-label">Space Id</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpspace" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Space Id</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblswpspace2" runat="server" CssClass="bodytext"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnswapping" runat="server" Text="Swap" CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
