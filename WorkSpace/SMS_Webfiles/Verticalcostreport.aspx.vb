﻿Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class WorkSpace_SMS_Webfiles_Verticalcostreport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            
            Get_Vertical()
            'ddlvertical.Items(0).Text = "--All " & Session("Parent") & "(s)--"
            BindGrid()
            'rfvloc.ErrorMessage = "Please Select " + Session("Child")
            'btnExport.Visible = False
        End If
    End Sub
    Private Sub Get_Vertical()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VERTICAL_COST")
        ddlvertical.DataSource = sp.GetDataSet()
        ddlvertical.DataTextField = "VERTICAL_NAME"
        ddlvertical.DataValueField = "VERTICAL_CODE"
        ddlvertical.DataBind()
        ddlvertical.Items.Insert(0, "--All " & Session("Parent") & "(s)--")
        'ddlvertical.Items(0).Value = ""
    End Sub
    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        BindGrid()
    End Sub
    Private Sub BindGrid()

        Dim rds As New ReportDataSource()
        rds.Name = "VerticalCostDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/VerticalCostReport.rdlc")
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p2 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p2)

        'Setting Header Column value dynamically
        'Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        'ReportViewer1.LocalReport.SetParameters(p1)

        'Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        'ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim project As String = ""
        Dim tilldate As String = ""

        If ddlvertical.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            project = ""
        Else
            project = ddlvertical.SelectedValue
        End If

        

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VERTICAL_WISE_COST")
        sp.Command.AddParameter("@VERTICAL", project, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        rds.Value = ds.Tables(0)

    End Sub
    Protected Sub ddlvertical_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlvertical.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class
