Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_SpaceReport
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim TArea As Decimal = 0
    Dim WArea As Decimal = 0
    Dim CArea As Decimal = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            'ddlCity.Items(0).Text = "--All--"
            'ddlLocation.Items.Insert(0, "--All--")
            BindCities()
            BindLocation("--All--")
            BindLocationWise()
            BindArea()
            lbltotarea.Visible = True
            t1.Visible = True
        End If
    End Sub

    Public Sub BindCities()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")
        ddlCity.Items.Insert(0, "--All--")
        lbltotarea.Visible = True
        t1.Visible = True
        ddlCity.Items.RemoveAt(1)
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        BindLocation(ddlCity.SelectedItem.Value)
        BindLocationWise()
        BindArea()
        lbltotarea.Visible = True
        t1.Visible = True
        
    End Sub
  
    Public Sub BindLocation(ByVal strCity As String)
        lblMsg.Visible = False
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strCity
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONBY_CTY", "LCM_NAME", "LCM_CODE", param)
        If ddlLocation.Items.Count > 0 Then
            ReportViewer1.Visible = False
            lbltotarea.Visible = True
            t1.Visible = True
            ddlLocation.Items.Insert(0, "--All--")
            ddlLocation.Items.RemoveAt(1)
        ElseIf ddlLocation.Items.Count = 0 Then
            lblMsg.Text = "no locations available"
            ddlLocation.Items.Insert(0, "--All--")
            ReportViewer1.Visible = False
            lbltotarea.Visible = False
        End If


    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindArea()
        BindLocationWise()

        t1.Visible = True
    End Sub
    Private Sub BindArea()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETFLOORAREA_TOTAL")
        If ddlCity.SelectedItem.Text = "--All--" Then
            sp.Command.AddParameter("@CTY_ID", "", DbType.String)
        Else
            sp.Command.AddParameter("@CTY_ID", ddlCity.SelectedValue, DbType.String)
        End If
        If ddlLocation.SelectedItem.Text = "--All--" Then
            sp.Command.AddParameter("@LCM_CODE", "", DbType.String)
        Else
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedValue, DbType.String)
        End If
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltotarea.Visible = True
            lbltotarea.Text = "Total Area  " & ds.Tables(0).Rows(0).Item("FLOOR_AREA") & "  Sqft."
            lbltotarea.ForeColor = Drawing.Color.Black
            lbltotarea.Font.Bold = True
        End If
    End Sub
    Public Sub BindLocationWise()
        lbltotarea.Visible = True
        t1.Visible = True
        Dim strCity As String = ""
        Dim strLocation As String = ""

        'If ddlCity.SelectedItem.Value <> "--Select--" Then
        '    strCity = ddlCity.SelectedItem.Value
        'End If
        If ddlCity.SelectedItem.Text = "--All--" Then
            strCity = ""
        Else
            strCity = ddlCity.SelectedValue
        End If

        'If ddlLocation.SelectedItem.Value <> "--Select--" Then
        '    strLocation = ddlLocation.SelectedItem.Value
        'End If
        If ddlLocation.SelectedItem.Text = "--All--" Then
            strLocation = ""
        Else
            strLocation = ddlLocation.SelectedValue
        End If


        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strCity

        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = strLocation

        param(2) = New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 200)
        param(2).Value = "CITY_NAME"

        param(3) = New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 200)
        param(3).Value = "ASC"
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GETFLOORAREA", param)

        Dim rds As New ReportDataSource()
        rds.Name = "AreaReportDS"
        rds.Value = dt
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/AreaReport.rdlc")

        'Setting Header Column value dynamically
        'Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        'ReportViewer1.LocalReport.SetParameters(p1)

        'Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        'ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True


    End Sub


End Class
