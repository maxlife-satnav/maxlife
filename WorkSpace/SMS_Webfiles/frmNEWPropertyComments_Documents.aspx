<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmNEWPropertyComments_Documents.aspx.vb" Inherits="WorkSpace_frmNEWPropertyComments_Documents"
    Title="Property Comments & Document" %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="page-wrapper">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Upload Property Document</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ShowSummary="true" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Property Type<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvddproptype" ControlToValidate="ddlPropertyType" InitialValue="0"
                                                ErrorMessage="Please Select Property Type" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlPropertyType" CssClass="selectpicker" runat="server" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Property<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvprop" ControlToValidate="ddlProperty" InitialValue="--Select--"
                                                ErrorMessage="Please Select Property" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlProperty" CssClass="selectpicker" runat="server" AutoPostBack="True" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvRemearks" ControlToValidate="txtRemarks"
                                                ErrorMessage="Please Enter Remarks" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Browse Document<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Select Document"
                                            ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class="btn btn-default" style="display:block!important;">
                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                            </div>
                        </div>      
                        <asp:Label ID="lblgridheading" runat="server" Text="Property Remarks" Visible="false" Font-Size="14"></asp:Label>                  
                            <%--<h4>Property Remarks</h4>--%>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvRemarks" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                        OnRowDeleting="gvRemarks_RowDeleting" OnRowCommand="gvRemarks_RowCommand1"
                                        AllowPaging="True" PageSize="5" EmptyDataText="No Remarks Found" CssClass="table table-condensed table-bordered table-hover table-striped"
                                        Style="font-size: 12px;">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Commented by">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblID" Text='<%# Eval("ID") %>' Visible="false" runat="server"></asp:Label>
                                                    <asp:Label ID="lblAUR_FIRST_NAME" Text='<%# Eval("AUR_FIRST_NAME") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Comments">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCOMMENTS" Text='<%# Eval("COMMENTS") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Commented Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" Text='<%# Eval("COMMENT_DATE") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Uploaded Document">
                                                <ItemTemplate>
                                                    <a href='<%=Page.ResolveUrl("~/UploadFiles") %>/<%# Eval("PROPERTY_DOC") %>' target="_blank">
                                                        <%# Eval("PROPERTY_DOC") %>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lnkDelete" Text="Delete" CommandArgument='<%# Eval("id") %>'
                                                        CommandName="Delete"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>                      
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--<script src="../../BootStrapCSS/Scripts/bootstrap.file-input.js"></script>--%>
    <%--    <script>
        $(document).ready(function () { $('input[type=file]').bootstrapFileInput(); });
    </script>--%>
</body>
</html>
