<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmTenantDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmTenantDetails" Title="Tenant Details" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script lang="javascript" type="text/javascript">
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
   <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <form id="form1" class="well" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >Tenant Code<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >Tenant<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:DropDownList ID="ddluser" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <%--  <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >Tenant Name<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >Property Type<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >City<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                        AutoPostBack="True" Enabled="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >Location<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                        AutoPostBack="True" Enabled="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >Property<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True" Enabled="false">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >Tenant Occupied Area<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control"
                                        MaxLength="5" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Tenant Rent<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtRent" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Joining Date<span style="color: red;">*</span></label>
                                <div  >
                                    <div class='input-group date' id='fromdate'>
                                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Next Payable Date<span style="color: red;">*</span></label>
                                <div  >
                                    <div class='input-group date' id='todate'>
                                        <asp:TextBox ID="txtPayableDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Security Deposit<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control"
                                        MaxLength="5" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Payment Terms<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                        <asp:ListItem>-- Payment Terms--</asp:ListItem>
                                        <asp:ListItem>Weekly</asp:ListItem>
                                        <asp:ListItem>Monthly</asp:ListItem>
                                        <asp:ListItem>Half-Yearly</asp:ListItem>
                                        <asp:ListItem>Annual</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Tenant Status<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                        <asp:ListItem>-- Status--</asp:ListItem>
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Number of Parking<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Maintenance Fees<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtfees" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Outstanding amount<span style="color: red;">*</span></label>
                                <div >
                                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label  >
                                    Remarks<span style="color: red;">*</span></label>
                                <div  >
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                        Rows="3" MaxLength="500" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

