Imports System
Imports System.Net
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports cls_OLEDB_postgresSQL
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_frmSpaceVerticalAllocations
    Inherits System.Web.UI.Page
    Dim obj As New clsRelease
    Dim clsObj As New clsMasters
    Dim ds As DataSet
    Dim objMaster As New clsMasters()
    Dim objEmp As New clsEmpMapping()
    Dim objExtedSpace As New clsExtenedRelease()
    Dim strRedirect As String = String.Empty
    Dim Email As String = String.Empty
    Dim dt As New DataTable

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("logout"))
            End If
           
            lblMsg.Text = ""
            If Not Page.IsPostBack Then
                Try

                    btnSubmit.Visible = False
                    Dim obj1 As New clsRelease
                    Dim sta As Integer = 7
                    'objMaster.BindTower(ddlReqID)
                    'objMaster.BindVerticalAllocationData(ddlVertical)
                    'objMaster.BindCostCenter(ddlVertical)
                    BindCity()
                    '    objMaster.Bindlocation(ddlLocation)
                    ResetDropDown()
                    lblSelVertical.Text = Session("Parent")
                    RequiredFieldValidator6.ErrorMessage = "Please Select " + lblSelVertical.Text
                    lblSelCostcenter.Text = Session("Child")
                    RequiredFieldValidator7.ErrorMessage = "Please Select " + lblSelCostcenter.Text
                    'ddlReqID.Items.Insert(0, "--Select--")
                    'ddlFloor.Items.Insert(0, "--Select--")
                    'ddlWing.Items.Insert(0, "--Select--")
                    'ddlVertical.Items.Insert(0, "--Select--")
                    'ddlCostCenter.Items.Insert(0, "--Select--")
                    'ddlProject.Items.Insert(0, "--Select--")
                Catch ex As Exception
                    Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
                End Try

            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub


    Private Sub BindCity()
        ObjSubSonic.Binddropdown(ddlCity, "GETALLCITIES", "CTY_NAME", "CTY_CODE")
    End Sub


    Public Sub ResetDropDown()
        ddlLocation.Items.Insert(0, "--Select--")
        ddlReqID.Items.Insert(0, "--Select--")
        ddlFloor.Items.Insert(0, "--Select--")
        ddlWing.Items.Insert(0, "--Select--")
        ddlVertical.Items.Insert(0, "--Select--")
        ddlCostCenter.Items.Insert(0, "--Select--")
        'ddlProject.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try

            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chkEmp As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)
                If chkEmp.Checked = True Then
                    intCheckCount += 1
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atleast one Space to Release"
                Exit Sub
            End If

            dt.Columns.Add("Sno", GetType(Integer))
            dt.Columns.Add("SpaceID", GetType(String))
            dt.Columns.Add("FromDate", GetType(Date))
            dt.Columns.Add("ToDate", GetType(Date))
            'dt.Columns.Add("Floor", GetType(String))
            'dt.Columns.Add("Tower", GetType(String))
            'dt.Columns.Add("Wing", GetType(String))
            Dim drNew As DataRow

            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chkEmp As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)
                Dim lblSpaceReqID As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblSpaceID"), Label)
                Dim lblMail As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblAllocEmpMail"), Label)
                Dim txtfromDT As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblFromDT"), Label)
                Dim txtToDT As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblToDT"), Label)
                Dim lblstaid As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblstaid"), Label)
                If chkEmp.Checked = True Then
                    Try
                        Dim REQID As String = obj.updateSpace(gvSpaceExtend.Rows(i).Cells(4).Text, lblSpaceReqID.Text)
                        If REQID = "0" Then
                            lblMsg.Text = "Error while updating into database."
                            Exit Sub
                        Else
                            drNew = dt.NewRow
                            drNew(0) = dt.Rows.Count + 1
                            drNew(1) = gvSpaceExtend.Rows(i).Cells(4).Text
                            drNew(2) = txtfromDT.Text.Trim()
                            drNew(3) = txtToDT.Text.Trim()
                            'drNew(4) = gvSpaceExtend.Rows(i).Cells(2).Text
                            'drNew(5) = gvSpaceExtend.Rows(i).Cells(1).Text
                            'drNew(6) = gvSpaceExtend.Rows(i).Cells(3).Text
                            dt.Rows.Add(drNew)
                            Email = lblMail.Text
                            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACERELEASE")
                            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                            sp.Command.AddParameter("@REQID", lblSpaceReqID.Text, DbType.String)
                            sp.Command.AddParameter("@RELREQID", REQID, DbType.String)
                            sp.Command.AddParameter("@STA_ID", lblstaid.Text, DbType.String)
                            sp.ExecuteScalar()
                            UpdateRecord(gvSpaceExtend.Rows(i).Cells(4).Text, 1, gvSpaceExtend.Rows(i).Cells(4).Text)
                        End If

                        If GETSPACETYPE(gvSpaceExtend.Rows(i).Cells(4).Text).Tables(0).Rows(0).Item("SPACE_TYPE") <> 2 Then
                            UpdateRecord(gvSpaceExtend.Rows(i).Cells(4).Text, 1, "")
                        Else
                            Dim param4(0) As SqlParameter
                            param4(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
                            param4(0).Value = gvSpaceExtend.Rows(i).Cells(4).Text
                            Dim count As Integer = 0
                            count = ObjSubSonic.GetSubSonicExecuteScalar("Shift_SeatRequsition", param4)
                            If count = 1 Then
                                UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), 11, "") ' completely allocated
                            ElseIf count = -1 Then
                                UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), 1, "") ' vacant
                            Else
                                UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), 10, "") ' partially allocated
                            End If
                        End If




                        'Email = lblMail.Text
                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_SPACE_EMP_MAP")
                        'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                        'sp.Command.AddParameter("@REQID", lblSpaceReqID.Text, DbType.String)
                        'sp.Command.AddParameter("@MAILSTATUS", 14, DbType.Int32)
                        'sp.ExecuteScalar()
                        ' sendMail(dt, gvSpaceExtend.Rows(i).Cells(1).Text, gvSpaceExtend.Rows(i).Cells(2).Text, gvSpaceExtend.Rows(i).Cells(3).Text, lblMail.Text)
                    Catch ex As Exception
                        lblMsg.Text = "unable to Release"
                        Exit Sub
                    End Try
                End If
            Next
            'For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
            '    sendMail(dt, gvSpaceExtend.Rows(i).Cells(1).Text, gvSpaceExtend.Rows(i).Cells(2).Text, gvSpaceExtend.Rows(i).Cells(3).Text, Email)
            'Next
            If dt.Rows.Count > 0 Then
                Session("ReleaseData") = dt
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("4") & ""
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub
    Private Function GETSPACETYPE(ByVal strSpaceId As String) As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETSPACETYPE")
        sp.Command.AddParameter("@SPC_ID", strSpaceId, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Return ds
    End Function
    '    Private Sub sendMail(ByVal strSpaceId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal strTower As String, ByVal strFloor As String, ByVal strWing As String, ByVal strEmail As String)
    Private Sub sendMail(ByVal dt As DataTable, ByVal strTower As String, ByVal strFloor As String, ByVal strWing As String, ByVal strEmail As String)
        Try
            Dim to_mail As String = strEmail
            Dim cc_mail As String = Session("uemail")
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail1 As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim objData As SqlDataReader
            Dim user As String = String.Empty
            'Dim objDataCurrentUser As SqlDataReader
            'Dim strCurrentUser As String = "select aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
            'objDataCurrentUser = SqlHelper.ExecuteReader(CommandType.Text, strCurrentUser)
            'While objDataCurrentUser.Read
            '    user = objDataCurrentUser("aur_known_as")
            'End While
            Dim strKnownas As String = String.Empty
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
            sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            objData = sp1.GetReader()
            While objData.Read
                strRR = objData("aur_reporting_to").ToString
                strRM = objData("aur_reporting_email").ToString
                strKnownas = objData("aur_known_as").ToString
                cc_mail = objData("aur_email").ToString
                'BCC = objData("BCC").ToString
            End While

            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS2")
            sp2.Command.AddParameter("@AUR_Email", strEmail, DbType.String)
            Dim dr As SqlDataReader
            dr = sp2.GetReader()
            While (dr.Read)
                user = dr("aur_known_as").ToString
                to_mail = dr("aur_email").ToString
            End While

            body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "

            body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'> Dear <b> Sir/Madam&nbsp;</b> , </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br /> Project space has been released by " + strKnownas + " The details are as follows.</td></tr></table>&nbsp;<br /> "
            body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
            body = body & "<table align='center'>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Floor Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strFloor & "</td></tr>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Wing Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strWing & "</td></tr> "
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlVertical.SelectedItem.Text & "</td></tr> "
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Cost Center</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlCostCenter.SelectedItem.Text & "</td></tr> "
            'body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlProject.SelectedItem.Text & "</td></tr> "
            body = body & "</table>"
            body = body & "</td></tr></table><br/>"
            body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
            body = body & "<table align='center'>"
            Dim j As Integer = 3
            For i As Integer = 0 To dt.Rows.Count - 1
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows(i)(1).ToString() & "</td></tr> "
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dt.Rows(i)(2).ToString()).ToShortDateString() & "</td></tr>"
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dt.Rows(i)(3).ToString()).ToShortDateString() & "</td></tr>"
                j = j + 3
            Next
            body = body & "</table>"
            body = body & "</td></tr></table>"

            body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
            body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
            body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
            body = body & "</table>"
            'If to_mail = String.Empty Then
            '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
            'End If


            Dim strsql As String = String.Empty
            Dim stVrm As String = String.Empty
            Dim stVrm1 As String = String.Empty
            ' Dim dr As SqlDataReader
            'Dim dr1 As SqlDataReader
            'Dim iQry As Integer = 0
            'strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
            ''strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
            'iQry = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL1))
            'If iQry > 0 Then
            '    strSQL1 = "Select VER_VRM from VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
            '    dr1 = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
            '    While dr1.Read
            '        stVrm = dr1("VER_VRM").ToString()
            '    End While
            '    strsql = "Select aur_email from amantra_user where aur_id='" & stVrm & "'"
            '    dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            '    While dr.Read
            '        stVrm1 = dr("aur_email").ToString()
            '    End While
            'Else
            '    stVrm1 = ConfigurationManager.AppSettings("AmantraEmailId").ToString
            'End If

            Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
            Dim parms2 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
            Dim parms3 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
            Dim parms4 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
            Dim parms5 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
            Dim parms6 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
            Dim parms7 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim parms8 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

            parms.Value = "Abcd"
            parms2.Value = body
            parms3.Value = to_mail
            parms4.Value = "Space Release Details"
            parms5.Value = getoffsetdatetime(DateTime.Now)
            parms6.Value = "Request Submitted"
            parms7.Value = "Normal Mail"
            parms8.Value = cc_mail

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms2, parms3, parms4, parms5, parms6, parms7, parms8)

            'Dim parms9 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
            'parms9(0).Value = 3
            'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms9)
            'If objData.HasRows Then
            '    While objData.Read
            '        strBUHead = objData("aur_email")
            '        Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
            '        Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
            '        Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
            '        Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
            '        Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
            '        Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
            '        Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            '        Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

            '        parms10.Value = "Abcd"
            '        parms11.Value = body
            '        parms12.Value = strBUHead
            '        parms13.Value = "Space Release Details"
            '        parms14.Value = getoffsetdatetime(DateTime.Now)
            '        parms15.Value = "Request Submitted"
            '        parms16.Value = "Normal Mail"
            '        parms17.Value = String.Empty
            '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)

            '    End While
            'End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
        End Try


    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            objMaster.BindTowerLoc(ddlReqID, ddlLocation.SelectedValue.Trim())
            If ddlReqID.Items.Count = 1 Then
                lblMsg.Text = "No Cities Available"
            End If
            If ddlLocation.SelectedItem.Text = "--Select--" Then
                ResetTowerDropDown()
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, "--Select--")
                ddlWing.Items.Clear()
                ddlWing.Items.Insert(0, "--Select--")
                ddlVertical.Items.Clear()
                ddlVertical.Items.Insert(0, "--Select--")
                ddlCostCenter.Items.Clear()
                ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
        End Try
    End Sub




    Public Sub ResetTowerDropDown()
        ddlReqID.Items.Clear()
        ddlReqID.Items.Insert(0, "--Select--")
        ddlFloor.Items.Clear()
        ddlFloor.Items.Insert(0, "--Select--")
        ddlWing.Items.Clear()
        ddlWing.Items.Insert(0, "--Select--")
        ddlVertical.Items.Clear()
        ddlVertical.Items.Insert(0, "--Select--")
        ddlCostCenter.Items.Clear()
        ddlCostCenter.Items.Insert(0, "--Select--")
        'ddlProject.Items.Clear()
        'ddlProject.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try

            If ddlVertical.SelectedIndex = 0 Or ddlCostcenter.SelectedIndex = 0 Then
                'If ddlVertical.SelectedIndex = 0 Then
                gvSpaceExtend.DataSource = Nothing
                gvSpaceExtend.DataBind()
                btnSubmit.Visible = False
                Exit Sub
            End If

            ' objExtedSpace.bindDataForRelease(ddlReqID.SelectedValue, gvSpaceExtend, "7,4", ddlLocation.SelectedValue.ToString().Trim())
            '            objExtedSpace.bindDataForRelease(ddlVertical.SelectedValue, gvSpaceExtend, "7,4", ddlProject.SelectedValue.ToString().Trim())
            'objExtedSpace.bindDataForRelease(ddlVertical.SelectedValue, gvSpaceExtend, "7,4", ddlProject.SelectedValue.ToString().Trim())
            BindGrid()

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
        End Try

    End Sub

    Public Sub BindGrid()

        



        Dim P1 As New SqlParameter("@AUR_VERT_CODE", SqlDbType.NVarChar, 50)
        Dim P2 As New SqlParameter("@AUR_PRJ_CODE", SqlDbType.NVarChar, 50)
        Dim P3 As New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50)
        Dim P4 As New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 50)
        Dim P5 As New SqlParameter("@WNG_CODE", SqlDbType.NVarChar, 50)
        Dim P6 As New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 50)
        Dim P7 As New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)

        Dim dsgvSpaceExtend As New Data.DataSet
        'P1.Value = ddlVertical.SelectedItem.Value
        'If rbActions.Items(1).Value = "Add" Then
        '    P2.Value = ""
        'ElseIf rbActions.Items(1).Value = "Modify" Then
        '    P2.Value = ddlCostcenter.SelectedItem.Value
        'End If
        P1.Value = ddlVertical.SelectedItem.Value
        P2.Value = ddlCostcenter.SelectedItem.Value
        P3.Value = ddlReqID.SelectedItem.Value
        P4.Value = ddlFloor.SelectedItem.Value
        P5.Value = ddlWing.SelectedItem.Value
        P6.Value = ddlLocation.SelectedItem.Value
        P7.Value = Session("UID")


        dsgvSpaceExtend = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GET_SPACEREALEASEDETAILS", P1, P2, P3, P4, P5, P6, P7)
        gvSpaceExtend.DataSource = dsgvSpaceExtend
        gvSpaceExtend.DataBind()

        If gvSpaceExtend.Rows.Count > 0 Then
            btnSubmit.Visible = True
            gvSpaceExtend.Visible = True
        Else
            ddlLocation.SelectedIndex = 0
            ddlReqID.SelectedIndex = 0
            ddlFloor.SelectedIndex = 0
            ddlWing.SelectedIndex = 0
            ddlVertical.SelectedIndex = 0
            ddlCostcenter.SelectedIndex = 0
            'ddlProject.SelectedIndex = 0

            'ddlReqID.SelectedIndex = 0
            btnSubmit.Visible = False
            lblMsg.Text = "No Spaces to Release in this Vertical"
        End If



    End Sub
    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        Try
            If (ddlVertical.SelectedIndex > 0) Then
                Dim sp1 As New SqlParameter("@TWR_ID", SqlDbType.VarChar, 250)
                Dim sp2 As New SqlParameter("@FLR_ID", SqlDbType.VarChar, 250)
                Dim sp3 As New SqlParameter("@WNG_ID", SqlDbType.VarChar, 250)
                Dim sp4 As New SqlParameter("@BDG_ID", SqlDbType.VarChar, 250)
                Dim sp5 As New SqlParameter("@COSTCENTER", SqlDbType.VarChar, 250)

                sp1.Value = ddlReqID.SelectedValue
                sp2.Value = ddlFloor.SelectedValue
                sp3.Value = ddlWing.SelectedValue
                sp4.Value = ddlLocation.SelectedValue
                sp5.Value = ddlVertical.SelectedValue

                ddlCostCenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_PROJECTCOSTCENTER", sp1, sp2, sp3, sp4, sp5)
                ddlCostCenter.DataTextField = "cost_center_name"
                ddlCostCenter.DataValueField = "cost_center_code"
                ddlCostCenter.DataBind()
                ddlCostcenter.Items.Insert(0, "--Select--")
                If ddlCostcenter.Items.Count = 1 Then
                    lblMsg.Text = "No Costcenters Available"
                End If


                'Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
                'sp1.Value = ddlVertical.SelectedValue
                'ddlCostCenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvscostcenter", sp1)
                'ddlCostCenter.DataTextField = "cost_center_name"
                'ddlCostCenter.DataValueField = "cost_center_code"
                'ddlCostCenter.DataBind()
                'ddlCostCenter.Items.Insert(0, "--Select--")


                
                 
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            Else
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")


               
                ddlCostCenter.Items.Clear()
                ddlCostCenter.Items.Insert(0, "--Select--")
              

                'ddlProject.SelectedIndex = 0
                'ddlCostCenter.SelectedIndex = 0
                'ddlCostCenter.Items.Clear()
                'ddlProject.Items.Clear()
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
        End Try

        'Try
        '    objMaster.BindCostVertical(ddlCostCenter, ddlVertical.SelectedValue.Trim())
        '    ddlCostCenter.SelectedIndex = 0
        '    ddlProject.SelectedIndex = 0
        '    gvSpaceExtend.Visible = False
        'Catch ex As Exception
        '    Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
        'End Try
    End Sub

    Protected Sub ddlCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCenter.SelectedIndexChanged
        Try



            'objMaster.BindProjectCost(ddlProject, ddlVertical.SelectedValue.Trim(), ddlCostCenter.SelectedValue.Trim())
            'ddlProject.SelectedIndex = 0
            'gvSpaceExtend.Visible = False

            If ddlCostCenter.SelectedIndex > 0 Then
                Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
                sp1.Value = ddlVertical.SelectedValue
                Dim sp2 As New SqlParameter("@vc_costcenter", SqlDbType.VarChar, 250)
                sp2.Value = ddlCostCenter.SelectedValue
                'ddlProject.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvcproject", sp1, sp2)
                'ddlProject.DataTextField = "prj_name"
                'ddlProject.DataValueField = "prj_code"
                'ddlProject.DataBind()
                'ddlProject.Items.Insert(0, "--Select--")
            Else
                'ddlProject.Items.Clear()
            End If


        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
        End Try
    End Sub


    Dim verBll As New VerticalBLL()
    Protected Sub ddlReqID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqID.SelectedIndexChanged
        Try
            If ddlReqID.SelectedIndex <> -1 And ddlReqID.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlReqID.SelectedValue, ddlLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
                If ddlFloor.Items.Count = 1 Then
                    lblMsg.Text = "No Floors Available"
                End If

                ddlWing.Items.Clear()
                ddlWing.Items.Insert(0, "--Select--")
                ddlVertical.Items.Clear()
                ddlVertical.Items.Insert(0, "--Select--")
                ddlCostCenter.Items.Clear()
                ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")



                ddlWing.Items.Clear()
                ddlWing.Items.Insert(0, "--Select--")
                ddlVertical.Items.Clear()
                ddlVertical.Items.Insert(0, "--Select--")
                ddlCostCenter.Items.Clear()
                ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Public Sub ResetFloorDropDown()
        ddlFloor.Items.Clear()
        ddlFloor.Items.Insert(0, "--Select--")

        'ddlFloor.Items.Insert(0, "--Select--")
        'ddlWing.Items.Insert(0, "--Select--")
        'ddlVertical.Items.Insert(0, "--Select--")
        'ddlCostCenter.Items.Insert(0, "--Select--")
        'ddlProject.Items.Insert(0, "--Select--")
    End Sub

    Dim obj1 As New clsReports
    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        Try
            obj1.bindwing(ddlWing, ddlFloor.SelectedValue, ddlLocation.SelectedValue, ddlReqID.SelectedValue)
            If ddlWing.Items.Count = 1 Then
                lblMsg.Text = "No Wings Available"
            End If
            ddlWing.Items(0).Text = "--Select--"
            'If ddlWing.Items.Count = 2 Then
            '    ddlWing.SelectedIndex = 1

            'End If


            'If ddlFloor.SelectedItem.Text = "--Select--" Then
            '    ddlWing.Items.Clear()
            '    ddlWing.Items.Insert(0, "--Select--")




            '    ddlVertical.Items.Clear()
            '    ddlVertical.Items.Insert(0, "--Select--")
            '    ddlCostCenter.Items.Clear()
            '    ddlCostCenter.Items.Insert(0, "--Select--")
            '    ddlProject.Items.Clear()
            '    ddlProject.Items.Insert(0, "--Select--")
            'Else

            '    ddlVertical.Items.Clear()
            '    ddlVertical.Items.Insert(0, "--Select--")
            '    ddlCostCenter.Items.Clear()
            '    ddlCostCenter.Items.Insert(0, "--Select--")
            '    ddlProject.Items.Clear()
            '    ddlProject.Items.Insert(0, "--Select--")
            'End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseOccupancy", "ddlFloor_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub ddlWing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWing.SelectedIndexChanged
        'objMaster.BindCostCenter(ddlVertical)

        Dim sp1 As New SqlParameter("@BDG_ID", SqlDbType.VarChar, 250)
        Dim sp2 As New SqlParameter("@TWR_ID", SqlDbType.VarChar, 250)
        Dim sp3 As New SqlParameter("@FLR_ID", SqlDbType.VarChar, 250)
        Dim sp4 As New SqlParameter("@WNG_ID", SqlDbType.VarChar, 250)
        Dim sp5 As New SqlParameter("@AUR_ID", SqlDbType.VarChar, 250)

        sp1.Value = ddlLocation.SelectedValue
        sp2.Value = ddlReqID.SelectedValue
        sp3.Value = ddlFloor.SelectedValue
        sp4.Value = ddlWing.SelectedValue
        sp5.Value = Session("UID")


        ddlVertical.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GETCOSTCENTERBYDETAILS", sp1, sp2, sp3, sp4, sp5)
        ddlVertical.DataTextField = "COST_CENTER_NAME"
        ddlVertical.DataValueField = "COST_CENTER_CODE"
        ddlVertical.DataBind()
        ddlVertical.Items.Insert(0, "--Select--")
        If ddlVertical.Items.Count = 1 Then
            lblMsg.Text = "No Verticals Available"
        End If


        If ddlWing.SelectedItem.Text = "--Select--" Then
            ddlVertical.Items.Clear()
            ddlVertical.Items.Insert(0, "--Select--")


          
            ddlCostCenter.Items.Clear()
            ddlCostCenter.Items.Insert(0, "--Select--")
            'ddlProject.Items.Clear()
            'ddlProject.Items.Insert(0, "--Select--")
        Else

            ddlCostCenter.Items.Clear()
            ddlCostCenter.Items.Insert(0, "--Select--")
            'ddlProject.Items.Clear()
            'ddlProject.Items.Insert(0, "--Select--")
        End If
    End Sub
    Protected Sub gvSpaceExtend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpaceExtend.PageIndexChanging
        gvSpaceExtend.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'obj.Bindlocation(ddlLocation)
        Dim cty_code As String = ""
        If ddlCity.SelectedItem.Text = "--Select--" Then
            lblMsg.Text = "Please select city."
            Exit Sub
        Else
            cty_code = ddlCity.SelectedItem.Value
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = cty_code

        ObjSubSonic.Binddropdown(ddlLocation, "GETACTIVELOCATIONBY_CCODE", "LCM_NAME", "LCM_CODE", param)
        If ddlLocation.Items.Count = 1 Then
            lblMsg.Text = "No Locations Available"
        End If

    End Sub

    
End Class

