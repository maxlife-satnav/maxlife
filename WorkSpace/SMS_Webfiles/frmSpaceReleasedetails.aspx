<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSpaceReleasedetails.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmSpaceVerticalAllocations" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {

            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function CheckDataGrid() {
            var k = 0;
            for (i = 0; i < aspnetForm.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                window.alert('Please Select atleast one');
                return false;
            }
            else {
                var input = confirm("Are you sure you want to Release?");
                if (input == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Release Details
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server"
                            CssClass="alert alert-danger"
                            ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select City <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select  City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Please Select  Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Tower  <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlReqID"
                                            Display="None" ErrorMessage="Please Select Tower" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlReqID" runat="server" OnSelectedIndexChanged="ddlReqID_SelectedIndexChanged"
                                                CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Floor <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlFloor"
                                            Display="None" ErrorMessage="Please Select  Floor" InitialValue="-- Select --"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlFloor" OnSelectedIndexChanged="ddlFloor_SelectedIndexChanged" AutoPostBack="True" runat="server"
                                                CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Wing  <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlWing"
                                            Display="None" ErrorMessage="Please Select  Wing" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlWing" AutoPostBack="True" OnSelectedIndexChanged="ddlWing_SelectedIndexChanged" runat="server"
                                                CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelVertical" class="col-md-5 control-label" runat="server" Text=""><span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                            runat="server" ControlToValidate="ddlVertical"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlVertical" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelCostcenter" class="col-md-5 control-label" runat="server" Text=""><span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                            runat="server" ControlToValidate="ddlCostcenter"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCostcenter" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnView_Click"
                                        Text="View" />
                                </div>
                            </div>
                        </div>
                        <div class="row">&nbsp</div>
                        <div id="Table2">

                            <div class="col-md-12">
                                <asp:GridView ID="gvSpaceExtend" runat="server" AutoGenerateColumns="False" EmptyDataText="No Space Release Details Found."
                                    AllowPaging="True" PageSize="25" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEmp" runat="server" />
                                                <asp:Label runat="server" ID="lblSpaceID" Visible="false" Text='<%#bind("SpaceReq") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblstaid" Visible="false" Text='<%#bind("ssa_sta_id") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblAllocEmpMail" Visible="false" Text='<%#bind("Email") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkEmp', this.checked)">
                                            </HeaderTemplate>
                                            <ItemStyle Width="1px" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Tower" DataField="Tower" />
                                        <asp:BoundField HeaderText="Floor" DataField="Floor" />
                                        <asp:BoundField HeaderText="Wing" DataField="Wing" />
                                        <asp:BoundField HeaderText="Space" DataField="Space" />
                                        <asp:TemplateField HeaderText="From Date" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblFromDT" Text='<%#bind("FROMDATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblToDT" Text='<%#bind("TODATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="From Date" DataField="FROMDATE" />
                                        <asp:BoundField HeaderText="To Date" DataField="TODATE" />
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" OnClientClick="return CheckDataGrid()" Text="Release"
                                        CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
