Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_LeaseExtension
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindGrid()

            pnl1.Visible = False
        End If
        txttodate.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_GRID")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
        lblMsg.Text = ""

    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "Extension" Then
            lblMsg2.Text = ""
            Dim lnkExtend As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkExtend.NamingContainer, GridViewRow)
            Dim lbllname As Label = DirectCast(gvRow.FindControl("lbllname"), Label)
            Dim lblEdate As Label = DirectCast(gvRow.FindControl("lblEdate"), Label)
            txtstore.Text = lbllname.Text
            'txtstore1.Text = lblEdate.Text
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LEASE_EXPIRY_DATE")
            sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtfromdate.Text = ds.Tables(0).Rows(0).Item("EXPIRY_DATE")
                txtrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                lblstrtdate.Text = ds.Tables(0).Rows(0).Item("START_DATE").ToString()
                lblenddate.Text = ds.Tables(0).Rows(0).Item("END_DATE").ToString()
                txtsdep.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
            End If
            pnl1.Visible = True
        Else
            txtfromdate.Text = ""
            pnl1.Visible = False
        End If
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim ValidateCode As Integer
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_COUNT_VALIDATION")
        sp1.Command.AddParameter("@LEASE_REQ_ID", txtstore.Text, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        If ValidateCode <> 0 Then
            lblMsg2.Visible = True
            lblMsg2.Text = "Lease Extension Request Already Raised And Waiting For Approval"
            pnl1.Visible = False
            Exit Sub
        End If

        Dim orgfilename As String = Replace(Replace(BrowsePossLtr.FileName, " ", "_"), "&", "_")
        Dim repdocdatetime As String = ""
        Try
            If (BrowsePossLtr.HasFile) Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(BrowsePossLtr.FileName)
                repdocdatetime = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & orgfilename
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime
                BrowsePossLtr.PostedFile.SaveAs(filePath)
                'lblMsg.Visible = True
                'lblMsg.Text = "File upload successfully"

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Try
            If CDate(txttodate.Text) < CDate(txtfromdate.Text) Then
                lblMsg2.Text = "Lease End Date should be greater than Start date"
                Exit Sub
            Else
                lblMsg2.Text = ""
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_INSERT_LEASE_EXTENSION_DETAILS")
                sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
                sp.Command.AddParameter("@FROM_DATE", txtfromdate.Text, DbType.Date)
                sp.Command.AddParameter("@TO_DATE", txttodate.Text, DbType.Date)
                sp.Command.AddParameter("@RENT_AMOUNT", txtrent.Text, DbType.Decimal)
                sp.Command.AddParameter("@SECURITY_DEPOSIT", txtsdep.Text, DbType.Decimal)
                sp.Command.AddParameter("@REMARKS", txtremarks.Text, DbType.String)
                sp.Command.AddParameter("@AURID", Session("UID"), DbType.String)
                sp.Command.AddParameter("@DOC", repdocdatetime, DbType.String)
                sp.ExecuteScalar()
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=60", False)


                lblMsg2.Visible = True
                lblMsg2.Text = "Lease Extension Request Submitted Successfully"
                Cleardata()
                pnl1.Visible = False
                BindGrid()
            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub Cleardata()
        txtfromdate.Text = ""
        txttodate.Text = ""
        txtremarks.Text = ""
    End Sub

    Public Sub fillgridOnSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_FILTER_GRID")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@SEARCH", txtReqId.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvitems.DataSource = Session("dataset")
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        Try
            fillgridOnSearch()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtReqId.Text = ""
        BindGrid()
    End Sub

End Class