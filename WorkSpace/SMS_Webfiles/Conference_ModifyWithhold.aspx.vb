﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL

Partial Class WorkSpace_SMS_Webfiles_Conference_ModifyWithhold
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        'txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
        lblMsg.Text = String.Empty
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            LoadCity()
            Dim id As String = Request.QueryString("id")
            Binddetails(id)
            'confroom.Visible = False
            'btnsubmit.Visible = False


        End If
    End Sub

    Private Sub Binddetails(ByVal id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VIEW_CONFERENCE_WITHHOLD")
        sp.Command.AddParameter("@REQ_ID", id, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("status") = 8 Then
                txtFrmDate.Enabled = False
                txtToDate.Enabled = False
                starttimehr.Enabled = False
                endtimehr.Enabled = False
                btnsubmit.Visible = False
            End If
            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_CITY")).Selected = True
            BindLocation(ddlCity.SelectedItem.Value)
            ddlSelectLocation.ClearSelection()
            ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_LOC")).Selected = True
            BindTower(ddlSelectLocation.SelectedItem.Value)
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_TOWER")).Selected = True
            BindFloor(ddlTower.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value.ToString().Trim())
            ddlFloor.ClearSelection()
            ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_FLOOR")).Selected = True
            BindCapacity()
            ddlCapacity.ClearSelection()
            ddlCapacity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CAPACITY")).Selected = True
            txtFrmDate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
            txtToDate.Text = ds.Tables(0).Rows(0).Item("TO_DATE")

            If txtToDate.Text >= getoffsetdate(Date.Today) And ds.Tables(0).Rows(0).Item("status") <> 8 Then

                txtFrmDate.Enabled = True
                txtToDate.Enabled = True
                starttimehr.Enabled = True
                endtimehr.Enabled = True
                btnsubmit.Visible = True
            Else
                txtFrmDate.Enabled = False
                txtToDate.Enabled = False
                starttimehr.Enabled = False
                endtimehr.Enabled = False
                btnsubmit.Visible = False
            End If

            starttimehr.ClearSelection()
            starttimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_HOUR")).Selected = True
            'starttimemin.ClearSelection()
            'starttimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_MINUTE")).Selected = True
            endtimehr.ClearSelection()

            If ds.Tables(0).Rows(0).Item("ETIME") = "23:59:00" Then
                endtimehr.Items.FindByValue("24").Selected = True
            Else
                endtimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_HOUR")).Selected = True
            End If


            'endtimemin.ClearSelection()
            'endtimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_MINUTE")).Selected = True
            'BindConference(ddlSelectLocation.SelectedItem.Value.ToString().Trim(), ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value)
            BindConference_ddl()
            ddlConf.ClearSelection()
            ddlConf.Items.FindByValue(ds.Tables(0).Rows(0).Item("SSA_SPC_ID")).Selected = True
            ' txtEmployeeCode.Text = ds.Tables(0).Rows(0).Item("SSA_EMP_MAP")
            ' BindassetGrid()
            'GetEmployeeSearchDetails(txtEmployeeCode.Text)
            'loadgrid()
        End If
    End Sub

    Public Sub LoadCity()
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        BindLocation(ddlCity.SelectedItem.Value)
    End Sub

    Private Sub BindFloor(ByVal tower As String, ByVal loc As String)
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforFloors(tower, loc.Trim())
        ddlFloor.DataSource = verDTO
        ddlFloor.DataTextField = "Floorname"
        ddlFloor.DataValueField = "Floorcode"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "-- Select --")
    End Sub

    Private Sub BindTower(ByVal loc As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
        param(0).Value = loc
        ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)
    End Sub

    Private Sub BindLocation(ByVal city As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = city
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Private Sub BindCapacity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)

        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

    End Sub

    Public Sub BindConference_ddl()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectLocation.SelectedIndexChanged

        Try
            'usp_getActiveTower_LOC

            If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
                ' objMsater.BindTowerLoc(ddlTower, ddlSelectLocation.SelectedItem.Value)
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
                param(0).Value = ddlSelectLocation.SelectedItem.Value
                ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)


            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try

            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If (txtFrmDate.Text >= getoffsetdate(Date.Today)) Then
            If txtToDate.Text >= txtFrmDate.Text Then

                If starttimehr.SelectedItem.Text < endtimehr.SelectedItem.Text Then
                    'Dim cntWst As Integer = 0
                    'Dim cntHCB As Integer = 0
                    'Dim cntFCB As Integer = 0
                    'Dim twr As String = ""
                    'Dim flr As String = ""
                    'Dim wng As String = ""
                    'Dim intCount As Int16 = 0
                    'Dim ftime As String = ""
                    'Dim ttime As String = ""
                    'Dim StartHr As String = "00"
                    'Dim EndHr As String = "00"
                    'Dim StartMM As String = "00"
                    'Dim EndMM As String = "00"

                    'If starttimehr.SelectedItem.Value <> "Hr" Then
                    '    StartHr = starttimehr.SelectedItem.Text
                    'End If
                    ''If starttimemin.SelectedItem.Value <> "Min" Then
                    ''    StartMM = starttimemin.SelectedItem.Text
                    ''End If
                    'If endtimehr.SelectedItem.Value <> "Hr" Then
                    '    If endtimehr.SelectedItem.Value = "24" Then
                    '        EndHr = "00"
                    '    Else
                    '        EndHr = endtimehr.SelectedItem.Text
                    '    End If
                    'End If
                    ''If endtimemin.SelectedItem.Value <> "Min" Then
                    ''    EndMM = endtimemin.SelectedItem.Text
                    ''End If
                    'ftime = StartHr + ":" + StartMM
                    'ttime = EndHr + ":" + EndMM
                    'Dim param(6) As SqlParameter
                    'param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
                    'param(0).Value = ddlSelectLocation.SelectedItem.Value
                    'param(1) = New SqlParameter("@TOWER", SqlDbType.NVarChar, 200)
                    'param(1).Value = ddlTower.SelectedItem.Value
                    'param(2) = New SqlParameter("@FLOOR", SqlDbType.NVarChar, 200)
                    'param(2).Value = ddlFloor.SelectedItem.Value
                    'param(3) = New SqlParameter("@FROM_TIME", SqlDbType.DateTime)
                    'param(3).Value = ftime
                    'param(4) = New SqlParameter("@TO_TIME", SqlDbType.DateTime)
                    'param(4).Value = ttime
                    'param(5) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
                    'param(5).Value = Convert.ToDateTime(txtFrmDate.Text)
                    'param(6) = New SqlParameter("@CONF_CODE", SqlDbType.DateTime)
                    'param(6).Value = ddlConf.SelectedValue


                    ''ObjSubSonic.Binddropdown(ddlConference, "usp_get_CONFERENCE_Seats_PART1", "spc_name", "spc_id", param)
                    ''confroom.Visible = True

                    ''If Request.QueryString("mode") = 2 Then
                    ''    emp.Visible = True
                    ''Else
                    ''    emp.Visible = False
                    ''End If

                    ''added by praveen on 13/02/2014
                    'Dim flag As Integer
                    'flag = ObjSubSonic.GetSubSonicExecuteScalar("usp_get_CONFERENCE_Seats_PART1", param)
                    'If flag = 1 Then 'added a new flag case.
                    SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 7)
                    lblMsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                    cleardata()
                    strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("7") & "&rid=" & clsSecurity.Encrypt(REQID)
                    'Else
                    '    PopUpMessage("Already conference is alloted for this date.Please select another date ", Me)
                    'End If


GVColor:


                    If strRedirect <> String.Empty Then
                        Response.Redirect(strRedirect, False)
                    End If

                Else
                    lblMsg.Text = "Start Time Must Be Less Than End Time"
                End If
            Else
                lblMsg.Text = "To Date Must Greater than Today's date"
            End If
        Else
            lblMsg.Text = "Date Must Be Greater than Today's date"
        End If

    End Sub

    Private Sub cleardata()
        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlConf.Items.Clear()
        btnsubmit.Visible = False
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)

        'REQID = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "SVR_ID", Session("TENANT") & "." , "SMS_VERTICAL_REQUISITION")
        'Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "24"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If starttimehr.SelectedItem.Value <> "HH" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        'If starttimemin.SelectedItem.Value <> "Min" Then
        '    StartMM = starttimemin.SelectedItem.Text
        'End If
        If endtimehr.SelectedItem.Value <> "HH" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If
        'If endtimemin.SelectedItem.Value <> "Min" Then
        '    EndMM = endtimemin.SelectedItem.Text
        'End If




        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM


        'If CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month Then ' And CDate(txtToDate.Text).Year < getoffsetdatetime(DateTime.Now).Year 
        '    lblMsg.Text = "You cant request for the past month"
        '    Exit Sub

        'End If



        Dim sta As Integer = status



        'RIDDS = RIDGENARATION("VerticalReq")

        'Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        'param(0).Value = RIDDS

        'If ObjSubSonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
        '    lblMsg.Text = "Request is already raised "
        '    Exit Sub
        'Else
        '    cntWst = 1

        '    Dim param2(15) As SqlParameter

        '    param2(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        '    param2(0).Value = REQID
        '    param2(1) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
        '    param2(1).Value = strVerticalCode
        '    param2(2) = New SqlParameter("@WSTREQCOUNT", SqlDbType.Int)
        '    param2(2).Value = cntWst
        '    param2(3) = New SqlParameter("@FCREQCOUNT", SqlDbType.Int)
        '    param2(3).Value = cntFCB
        '    param2(4) = New SqlParameter("@HCREQCOUNT", SqlDbType.Int)
        '    param2(4).Value = cntHCB
        '    param2(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        '    param2(5).Value = txtFrmDate.Text
        '    param2(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        '    param2(6).Value = txtToDate.Text
        '    param2(7) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        '    param2(7).Value = ftime
        '    param2(8) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        '    param2(8).Value = ttime
        '    param2(9) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
        '    param2(9).Value = strAurId
        '    param2(10) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 50)
        '    param2(10).Value = ddlSelectLocation.SelectedItem.Value 'GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
        '    param2(11) = New SqlParameter("@CITY", SqlDbType.NVarChar, 50)
        '    param2(11).Value = ddlCity.SelectedValue
        '    param2(12) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
        '    param2(12).Value = "2" 'GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
        '    param2(13) = New SqlParameter("@BCPTYPE", SqlDbType.NVarChar, 50)
        '    param2(13).Value = "2"
        '    param2(14) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
        '    param2(14).Value = sta
        '    param2(15) = New SqlParameter("@REMARKS", SqlDbType.NVarChar, 200)
        '    param2(15).Value = remarks
        '    ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART1", param2)
        '    Dim cnt As Int32 = 0
        '    ' For Each row As GridViewRow In gdavail.Rows
        '    Dim lblspcid As String   ' DirectCast(row.FindControl("lblspcid"), Label)
        '    lblspcid = ddlConf.SelectedItem.Value
        '    'Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    lblMsg.Text = ""
        '    ' If chkSelect.Checked = True Then
        '    verticalreqid = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "ssa_id", Session("TENANT") & "." , "SMS_space_allocation")

        Dim param3(5) As SqlParameter

        param3(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param3(0).Value = Request.QueryString("ID")
        param3(1) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
        param3(1).Value = txtFrmDate.Text
        param3(2) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
        param3(2).Value = txtToDate.Text
        param3(3) = New SqlParameter("@FROM_TIME", SqlDbType.DateTime)
        param3(3).Value = ftime
        param3(4) = New SqlParameter("@TO_TIME", SqlDbType.DateTime)
        param3(4).Value = ttime
        param3(5) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
        param3(5).Value = sta
        ObjSubSonic.GetSubSonicExecute("CONF_UPDATE_WITHHOLD", param3)
        ' Addusers.Occupiedspace(Trim(strAurId), LTrim(RTrim(lblspcid)))
        ' UpdateRecord(LTrim(RTrim(lblspcid)), sta, Trim(strAurId) & "/" & lblDepartment.Text)
        'End If
        'Next




    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        'GET_SPACE_CONFERENCE
        If ddlFloor.SelectedIndex <> 0 Then
            BindConference()
        End If

    End Sub

    Public Sub BindConference()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

End Class
