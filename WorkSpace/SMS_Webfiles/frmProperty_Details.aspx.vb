Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmProperty_Details
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindPropType("")
            BindCity("")
            BindPropertyDetails()
        End If
        lblMsg.Visible = False
    End Sub

    Private Sub BindPropertyDetails()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SNO", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("pid")
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_PRPTYPOPUP_DETAILS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblpropcode.Text = ds.Tables(0).Rows(0).Item("BDG_ID")
            lblPropIDName.Text = ds.Tables(0).Rows(0).Item("PN_NAME")
            lblMaxCapacity.Text = ds.Tables(0).Rows(0).Item("MAX_CAPACITY")
            lblOptCapacity.Text = ds.Tables(0).Rows(0).Item("OPTIMUM_CAPACITY")
            lblRentableArea.Text = ds.Tables(0).Rows(0).Item("RENTABLE_AREA")
            lblUsableArea.Text = ds.Tables(0).Rows(0).Item("USABLE_AREA")
            lblUOM_CODE.Text = ds.Tables(0).Rows(0).Item("UOM_CODE")
            lblCarpetArea.Text = ds.Tables(0).Rows(0).Item("CARPET_AREA")
            lblBuiltupArea.Text = ds.Tables(0).Rows(0).Item("BUILTUP_AREA")
            lblCommonArea.Text = ds.Tables(0).Rows(0).Item("COMMON_AREA")
            lblGovtPropCode.Text = ds.Tables(0).Rows(0).Item("GOVT_PROPERTY_CODE")
            lblPropDesc.Text = ds.Tables(0).Rows(0).Item("PROPERTY_DESCRIPTION")
            lblemail.Text = ds.Tables(0).Rows(0).Item("EMAIL")
            lblownrname.Text = ds.Tables(0).Rows(0).Item("OWNERNAME")
            lblphno.Text = ds.Tables(0).Rows(0).Item("PHNO")
            lblstatus.Text = ds.Tables(0).Rows(0).Item("STA_REM")
            Dim lblinstype As String = ds.Tables(0).Rows(0).Item("IN_TYPE")
            If lblinstype = "1" Then
                txtInsType.Text = "Building only"
            ElseIf lblinstype = "2" Then
                txtInsType.Text = "Content only"
            ElseIf lblinstype = "3" Then
                txtInsType.Text = "Building & Content"
            Else
                txtInsType.Text = "NA"
            End If
            'txtInsType.Text = ds.Tables(0).Rows(0).Item("IN_TYPE")
            txtInsVendor.Text = ds.Tables(0).Rows(0).Item("IN_VENDOR")
            txtInsAmt.Text = ds.Tables(0).Rows(0).Item("IN_AMOUNT")
            txtInsPolNum.Text = ds.Tables(0).Rows(0).Item("IN_POLICY_NUMBER")
            txtInsSdate.Text = ds.Tables(0).Rows(0).Item("IN_START_DATE").ToString()
            txtInsEdate.Text = ds.Tables(0).Rows(0).Item("IN_END_DATE").ToString()
            BindPropType(ds.Tables(0).Rows(0).Item("PN_PROPERTYTYPE"))
            BindCity(ds.Tables(0).Rows(0).Item("BDG_ADM_CODE"))
        End If
    End Sub

    Private Sub BindPropType(ByVal PropertyType As String)
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", "--Select Property Type--"))
        ddlPropertyType.ClearSelection()
        If PropertyType <> "" Then
            ddlPropertyType.Items.FindByText(PropertyType).Selected = True
            ddlPropertyType.Enabled = False
        End If
    End Sub

    Private Sub BindCity(ByVal strCity As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", "--Select City--"))
        ddlCity.ClearSelection()
        If strCity <> "" Then
            ddlCity.Items.FindByText(strCity).Selected = True
            ddlCity.Enabled = False
        End If
    End Sub

   

    Protected Sub btnApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproval.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SNO", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("PID")
        param(1) = New SqlParameter("@BTN_STATUS", SqlDbType.NVarChar, 200)
        param(1).Value = "APPROVED"
        param(2) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(2).Value = Session("uid")
        param(3) = New SqlParameter("@COMMENTS", SqlDbType.NVarChar, 200)
        param(3).Value = txtComments.Text
        ObjSubsonic.GetSubSonicExecuteScalar("UPDATE_PRPTY_STATUS", param)

        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=58")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SNO", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("PID")
        param(1) = New SqlParameter("@BTN_STATUS", SqlDbType.NVarChar, 200)
        param(1).Value = "REJECT"
        param(2) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(2).Value = Session("uid")
        param(3) = New SqlParameter("@COMMENTS", SqlDbType.NVarChar, 200)
        param(3).Value = txtComments.Text
        ObjSubsonic.GetSubSonicExecuteScalar("UPDATE_PRPTY_STATUS", param)
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=59")
        'Response.Redirect("frmPropertyLevelApproval.aspx")
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("frmPropertyLevelApproval.aspx")
    End Sub
End Class
