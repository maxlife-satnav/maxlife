﻿Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient
Partial Class WorkSpace_SMS_Webfiles_empswap
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Not IsPostBack Then

            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else

                swap.Visible = False
                btnswapping.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        lblMsg.Text = ""
        If txtemp2swap.Text <> "" And txtempswap.Text <> "" Then
            binddata()
            bindswapdata()

            swap.Visible = True

            If lblswpspace.Text <> "" And lblswpspace2.Text <> "" Then
                btnswapping.Visible = True
            Else
                btnswapping.Visible = False
            End If
        Else
            swap.Visible = False
            btnswapping.Visible = False

        End If
    End Sub
    Private Sub bindswapdata()
        Dim Emp1 As String
        Dim emp As Array
        emp = txtemp2swap.Text.Split("(")
        Emp1 = Trim(emp(0))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_EMP_DETAILS")
        sp.Command.AddParameter("@AUR_ID", Emp1, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblswpempid2.Text = ds.Tables(0).Rows(0).Item("AUR_ID")
            lblswpempname2.Text = ds.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
            lblswpemail2.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            lblswpdep2.Text = ds.Tables(0).Rows(0).Item("DEP_NAME")
            lblswpspace2.Text = ds.Tables(0).Rows(0).Item("SSA_SPC_ID")
        Else
            lblMsg.Text = "Employee not found."
        End If
    End Sub
    Private Sub binddata()
        Dim Emp1 As String
        Dim emp As Array
        emp = txtempswap.Text.Split("(")
        Emp1 = Trim(emp(0))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_EMP_DETAILS")
        sp.Command.AddParameter("@AUR_ID", Emp1, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblswpempid.Text = ds.Tables(0).Rows(0).Item("AUR_ID")
            lblswpempname.Text = ds.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
            lblswpemail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            lblswpdep.Text = ds.Tables(0).Rows(0).Item("DEP_NAME")
            lblswpspace.Text = ds.Tables(0).Rows(0).Item("SSA_SPC_ID")
        Else
            lblMsg.Text = "Employee not found."
        End If
    End Sub

    Protected Sub btnswapping_Click(sender As Object, e As EventArgs) Handles btnswapping.Click

        Dim Emp_1 As String
        Dim emp1 As Array
        emp1 = txtempswap.Text.Split("(")
        Emp_1 = Trim(emp1(0))

        Dim Emp_2 As String
        Dim emp2 As Array
        emp2 = txtemp2swap.Text.Split("(")
        Emp_2 = Trim(emp2(0))

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SWAP_EMPLOYEE1")
        sp.Command.AddParameter("@OEMPID", Emp_1, DbType.String)
        sp.Command.AddParameter("@NEMPID", Emp_2, DbType.String)
        sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
        sp.ExecuteScalar()
        lblMsg.Text = "Swapping done successfully"
        Cleardata()
    End Sub

    Private Sub cleardata()
        swap.Visible = False
        btnswapping.Visible = False
        txtemp2swap.Text = ""
        txtempswap.Text = ""
    End Sub
End Class
