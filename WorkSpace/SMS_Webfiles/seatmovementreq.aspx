﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="seatmovementreq.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_seatmovementreq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <script language="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
 <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
 <ContentTemplate>
    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Space Movement Requisition
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>Space Movement Requisition</strong>&nbsp;</td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif"></td>
                    <td align="left">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="clsMessage"
                            ForeColor="" />
                        <br />

                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                        <br />
                             <br />
                             <br />
                        <table id="table2" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                            border="1">
                    
                           
                            <tr>
                                <td align="left" class="clslabel" style="width: 25%; height: 26px;">Select From  City<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                        Display="None" ErrorMessage="Please Select From City" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td  style="width: 25%; height: 26px;">
                                    <asp:DropDownList ID="ddlCity" runat="server" Width="98%" CssClass="clsComboBox"
                                        AutoPostBack="True">
                                    </asp:DropDownList></td>


                                 <td align="left" class="clslabel" style="width: 25%; height: 26px;">Select To  City<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddltocity"
                                        Display="None" ErrorMessage="Please Select To City" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 25%; height: 26px;">
                                    <asp:DropDownList ID="ddltocity" runat="server" Width="98%" CssClass="clsComboBox"
                                        AutoPostBack="True">
                                    </asp:DropDownList></td>


                            </tr>
                            <tr>
                                <td align="left" class="clslabel" style="width: 25%; height: 26px;">Select From Location<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                        Display="None" ErrorMessage="Please Select From Location" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" class="clslabel" style="width: 25%; height: 26px;">
                                    <asp:DropDownList ID="ddlSelectLocation" runat="server" Width="98%" CssClass="clsComboBox">
                                    </asp:DropDownList></td>

                                 <td align="left" class="clslabel" style="width: 25%; height: 26px;">Select To Location<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddltoloc"
                                        Display="None" ErrorMessage="Please Select  to Location" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" class="clslabel" style="width: 25%; height: 26px;">
                                    <asp:DropDownList ID="ddltoloc" runat="server" Width="98%" CssClass="clsComboBox">
                                    </asp:DropDownList></td>

                            </tr>

                            <tr>
                                <td align="left" colspan="2" class="clslabel" style="width: 50%; height: 26px;">Select Vertical<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlvertical"
                                        Display="None" ErrorMessage="Please Select Vertical" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" class="clslabel" colspan="2" style="width: 50%; height: 26px;">
                                    <asp:DropDownList ID="ddlvertical" runat="server" Width="98%" CssClass="clsComboBox">
                                    </asp:DropDownList></td>

                            </tr>

                                <tr id="Tr1" runat="server"  >
                                <td align="left" class="clslabel" style="width: 25%; height: 17px">From Date<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                        Display="None" ErrorMessage="Please Enter From Date ">
                                    </asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                        Display="None" ErrorMessage="Please Enter Valid From Date " Operator="DataTypeCheck"
                                        Type="Date">
                                    </asp:CompareValidator></td>
                                <td style="width: 25%; height: 17px">
                                    <asp:TextBox ID="txtFrmDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                                    </asp:TextBox>
                                </td>
                                <td align="left" style="width: 25%; height: 17px">To Date<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvtodate" runat="server" ControlToValidate="txtToDate"
                                        Display="None" ErrorMessage="Please Enter To Date ">
                                    </asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtToDate"
                                        Display="None" ErrorMessage="Please Enter Valid To Date " Operator="DataTypeCheck"
                                        Type="Date">
                                    </asp:CompareValidator></td>
                                <td style="height: 17px">
                                    <asp:TextBox ID="txtToDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr id="trTimeSlot" runat="server" >
                                <td>From Time:<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimehr"
                                        Display="None" ErrorMessage="Please Enter From Time " InitialValue="Hr">
                                    </asp:RequiredFieldValidator>

                                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="starttimemin"
                                        Display="None" ErrorMessage="Please Enter From Minutes" InitialValue="Min">
                                    </asp:RequiredFieldValidator>

                                </td>
                                <td>
                                    <asp:DropDownList ID="starttimehr" runat="server" Width="60">
                                        <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="starttimemin" runat="server" Width="60">
                                        <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                        
                                    </asp:DropDownList>
                                    
                                </td>

                                <td>To time:<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimehr"
                                        Display="None" ErrorMessage="Please Enter To Time " InitialValue="Hr">
                                    </asp:RequiredFieldValidator>

                                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="endtimemin"
                                        Display="None" ErrorMessage="Please Enter To Minutes" InitialValue="Min">
                                    </asp:RequiredFieldValidator></td>
                                <td>
                                    <asp:DropDownList ID="endtimehr" runat="server" Width="60">
                                        <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="endtimemin" runat="server" Width="60">
                                        <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                      
                                    </asp:DropDownList>
                                    
                                </td>
                            </tr>


                            <tr>
                                <td style="height: 17px;" colspan="4" align="center">

                                    <asp:Button ID="btnavail" runat="server" Text="Check Availability" CssClass="button" />
                                </td>
                            </tr>


                            <tr runat="server" id="trgrid">
                                <td colspan="4" align="left">

                                    <asp:Panel ID="panelspace" runat="server" Width="100%">



                                        <asp:GridView ID="gdavail" runat="server" AutoGenerateColumns="False" AllowSorting="true" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                  <asp:TemplateField Visible="false" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQ_ID").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                  <asp:TemplateField HeaderText="Seats" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblspcoccu" runat="server" Text='<%# Eval("SPC_ID").ToString()%>'>
                                                        </asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Location" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblloc" runat="server" Text='<%# Eval("LOC").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Tower" >
                                                    <ItemTemplate>

                                                        <asp:Label ID="lbltower" runat="server" Text='<%# Eval("TWR").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Floor" >
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblfloor" runat="server" Text='<%# Eval("FLR").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Wing" >
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblwing" runat="server" Text='<%# Eval("WNG").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Vertical" >
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblvertical" runat="server" Text='<%# Eval("VER").ToString()%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                              
                                                <asp:TemplateField HeaderText="seats" >
                                                    <ItemTemplate>
                                                         <asp:DropDownList ID="ddlavail" runat="server" Width="300px" CssClass="clsComboBox"
                                                        AutoPostBack="True" >
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtavailspace" runat="server" Width="300px" Visible="false"></asp:TextBox>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                               





                                            </Columns>
                                        </asp:GridView>

                                    </asp:Panel>

                                </td>
                            </tr>
                            <tr runat="server" id="trbutton">
                                <td align="center" colspan="4">

                                    <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit" />
                                </td>
                            </tr>
                        </table>

 </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

