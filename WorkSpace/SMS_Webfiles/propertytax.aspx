﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="propertytax.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_propertytax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="center" width="100%">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                            ForeColor="Black">Add Property Tax
             <hr align="center" width="60%" /></asp:Label></td>
                </tr>
            </table>
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                <table id="table4" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER" align="left">
                            <strong>&nbsp; Add Property Tax</strong>
                        </td>
                        <td>
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                            &nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                          <asp:TextBox ID="txtstore" runat="server"  Visible="false"></asp:TextBox>
                              <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                                <tr>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        Enter Service tax % <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtservicetax"
                                            Display="none" ErrorMessage="Please enter Service tax  !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                          <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtservicetax"
                                                    ErrorMessage="Please Enter Valid Service tax" Display="None"
                                                    ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                    
                                    </td>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        <div onmouseover="Tip('Enter servicetax  in decimals')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtservicetax" runat="server" CssClass="clsTextField" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        Enter Maintenance %<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtmaintenance"
                                            Display="none" ErrorMessage="Please enter Maintenance tax  !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtmaintenance"
                                                    ErrorMessage="Please Enter Valid Maintenance tax" Display="None"
                                                    ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                    </td>
                                      <td align="left" style="height: 16px; width: 50%;">
                                        <div onmouseover="Tip('Enter maintenance tax  in decimals')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtmaintenance" runat="server" CssClass="clsTextField" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td colspan="3" align="center" style="height: 39px">
                                        <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Add" ValidationGroup="Val1"
                                            CausesValidation="true" />
                                          <asp:Button ID="btnmodify" CssClass="button" runat="server" Text="Modify" ValidationGroup="Val1"
                                            CausesValidation="true" />
                                      
                                    </td>
                                </tr>
                            </table>
                         
                            <table id="Table5" width="100%" runat="Server" cellpadding="0" cellspacing="0" border="1">
                                <tr>
                                    <td align="center" style="height: 20px">
                                        <asp:GridView ID="gvPropType" runat="server" AllowPaging="True" AllowSorting="False"
                                            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Records Found">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("SNO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Service Tax">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblservicetax" runat="server" CssClass="lblASTCode" Text='<%#Eval("SERVICE_TAX")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText=" Maintenance Tax">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmaintenance" runat="server" CssClass="lblStatus" Text='<%#Bind("MAINTENANCE_CHARGE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:ButtonField Text="EDIT" CommandName="EDIT" />
                                              
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                </table> 
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

