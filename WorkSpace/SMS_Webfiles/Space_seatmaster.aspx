<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Space_seatmaster.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Space_seatmaster"
    Title="Space seat type master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Seat Type Mapping Master
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
            <tr>
                <td align="left" width="100%" colspan="3">
                    <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong>&nbsp;Seat Type Mapping Master</strong>
                </td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <br />
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <br />
                    <table id="tab" runat="server" cellspacing="0" cellpadding="1" border="1" width="100%">
                        <tr>
                            <td align="left" width="50%">
                                Select City <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlcity"
                                    Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:DropDownList ID="ddlcity" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%" style="height: 26px">
                                Select Building <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvBDG" runat="server" ControlToValidate="ddlBDG"
                                    Display="None" ErrorMessage="Please Select Building" ValidationGroup="Val1" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%; height: 26px;">
                                <asp:DropDownList ID="ddlBDG" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                Select Tower <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ControlToValidate="ddlTWR"
                                    Display="None" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:DropDownList ID="ddlTWR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                Select Floor <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvFLR" runat="server" ControlToValidate="ddlFLR"
                                    Display="None" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:DropDownList ID="ddlFLR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                Select Wing <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="frvWNG" runat="server" ControlToValidate="ddlWNG"
                                    Display="None" ErrorMessage="Please Select Wing" ValidationGroup="Val1" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:DropDownList ID="ddlWNG" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                Select Space <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlspace"
                                    Display="None" ErrorMessage="Please Select Space" ValidationGroup="Val1" InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:DropDownList ID="ddlspace" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                Select Seat Type <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlseattype"
                                    Display="None" ErrorMessage="Please Select Seat Type" ValidationGroup="Val1"
                                    InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:DropDownList ID="ddlseattype" runat="server" Width="97%" CssClass="clsComboBox">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="button" CausesValidation="true"
                                    ValidationGroup="Val1" />
                                     <asp:Button ID="btnback" runat="server" CssClass="button" Text="Back" CausesValidation="false"
                                     />
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
