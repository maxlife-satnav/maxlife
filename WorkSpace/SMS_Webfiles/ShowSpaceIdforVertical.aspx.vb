
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports System.Threading
Partial Class WorkSpace_SMS_Webfiles_ShowSpaceIdforVertical
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))
    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB", False)

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If

        If Not Page.IsPostBack Then
            loadgrid()
        End If
    End Sub


    Private Sub loadgrid()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@req", SqlDbType.Date)
        param(0).Value = Request.QueryString("req") 'CDate("12-04-2012") '
       
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GETSPACE_VERTICALINTIME", param)
      
        gvToday.DataSource = ds
        gvToday.DataBind()
    End Sub
End Class
