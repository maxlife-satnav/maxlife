Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_VPHRApprovalLeaseDetails
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim maintper As Decimal = 0
    Dim servper As Decimal = 0
    'added by praveen for the Bind agreement details
    Dim startdate As String = ""
    Dim enddate As String = ""
    Dim basicamount As Decimal = 0
    Dim secDeposit As Decimal = 0
    Dim Remark As String = ""
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If Page.IsValid = True Then
            lblMsg.Text = ""

            Dim brkamount As Decimal = 0
            Dim ld3rent As Decimal = 0
            Dim ld2rent As Decimal = 0
            Dim ld1rent As Decimal = 0
            Dim ld3deposit As Decimal = 0
            Dim ld2deposit As Decimal = 0
            Dim ld1deposit As Decimal = 0


            If txtbrkamount.Text <> "" Then
                brkamount = CDbl(txtbrkamount.Text)
            End If
            If txtld3rent.Text <> "" Then
                ld3rent = CDbl(txtld3rent.Text)
            End If

            If txtld2rent.Text <> "" Then
                ld2rent = CDbl(txtld2rent.Text)
            End If

            If txtpmonthrent.Text <> "" Then
                ld1rent = CDbl(txtpmonthrent.Text)
            End If

            If txtld3sd.Text <> "" Then
                ld3deposit = CDbl(txtld3sd.Text)
            End If

            If txtld2sd.Text <> "" Then
                ld2deposit = CDbl(txtld2sd.Text)
            End If

            If txtpsecdep.Text <> "" Then
                ld1deposit = CDbl(txtpsecdep.Text)
            End If

            If ddlesc.SelectedItem.Value = "Yes" Then




                If CDate(txtEscalationDate.Text) >= CDate(txtsdate.Text) And (txtesctodate1.Text) <= CDate(txtedate.Text) Then



                    If CDbl(txtInvestedArea.Text) <> (ld3rent + ld2rent + ld1rent) Then
                        lblMsg.Text = "Landlord(s) Month Rent Not Matched with the  Lease Rent Amount Entered"
                        'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord(s) Month Rent Not Matched With LeaseRent Entered');", True)
                        Exit Sub
                    ElseIf CDbl(txtpay.Text) <> (ld3deposit + ld2deposit + ld1deposit) Then
                        lblMsg.Text = "Landlord(s) Security Deposit Not Matched with the Lease Security Deposit Amount Entered"
                        'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord(s) Security Deposit Not Matched With  Lease Security Deposit Entered');", True)
                        Exit Sub
                    ElseIf brkamount > CDbl(txtentitle.Text) Then
                        lblMsg.Text = "Broker Amount Should not be more than Entitle Amount"
                        Exit Sub
                    Else
                        If ddlleaseld.SelectedItem.Value = "1" Then
                            Dim LandLord1 As Integer
                            LandLord1 = ValidateLandlord1()
                            If LandLord1 = 0 Then
                                UpdateLandLord1()
                            End If
                        ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                            Dim Landlord2 As Integer
                            Landlord2 = ValidateLandlord2()
                            If Landlord2 = 0 Then
                                UpdateLandLord1()
                                UpdateLandLord2()
                            Else
                                UpdateLandLord1()
                                Landlord2Add()
                            End If
                        ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                            Dim Landlord3 As Integer
                            Landlord3 = ValidateLandlord3()
                            If Landlord3 = 0 Then
                                UpdateLandLord1()
                                UpdateLandLord2()
                                UpdateLandLord3()
                            Else
                                UpdateLandLord1()
                                UpdateLandLord2()
                                Landlord3Add()
                            End If
                        End If
                        ModifyLease()
                        ModifyEscalationDetails()
                        ModifyAgreementDetails()
                        ModifyRemarks()
                        ModifyBrokerageDetails()

                        If ddlpoa.SelectedItem.Value = "No" Then
                            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_update_REPORT_DOCUMENT1")
                            sp2.Command.AddParameter("@AXIS_SNO", 3, DbType.Int32)
                            sp2.Command.AddParameter("@_STATUS", "N", DbType.String)
                            sp2.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
                            sp2.Command.AddParameter("@SETS", "SET2", DbType.String)
                            sp2.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                            sp2.ExecuteScalar()
                        ElseIf ddlpoa.SelectedItem.Value = "Yes" Then
                            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_update_REPORT_DOCUMENT1")
                            sp2.Command.AddParameter("@AXIS_SNO", 3, DbType.Int32)
                            sp2.Command.AddParameter("@_STATUS", "Y", DbType.String)
                            sp2.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
                            sp2.Command.AddParameter("@SETS", "SET2", DbType.String)
                            sp2.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                            sp2.ExecuteScalar()
                        End If

                        If CDbl(txtbrkamount.Text) <> 0 And txtbrkamount.Text <> "" Then
                            Update_Axislabelmaster2()
                        Else
                            Update_AxislabelmasterBroker()
                        End If
                        ApproveLease()
                        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETLEASEREQUEST_vp_HRDETAILS")
                        'sp1.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
                        'Dim ds1 As New DataSet()
                        'ds1 = sp1.GetDataSet()
                        'Dim Lease_Date As String = ds1.Tables(0).Rows(0).Item("creation_Date")
                        'Dim stremail As String = ds1.Tables(0).Rows(0).Item("AUR_EMAIL")
                        'Dim StrApproved As String = ds1.Tables(0).Rows(0).Item("StrApproved")
                        'Dim sthrid As String = ds1.Tables(0).Rows(0).Item("VP_HR_ID")
                        'Dim sthrloc As String = ds1.Tables(0).Rows(0).Item("Strhrloc")
                        'Dim strCirclehr_Email As String = ds1.Tables(0).Rows(0).Item("strCirclehr_Email")
                        'Dim strHR_Email As String = ds1.Tables(0).Rows(0).Item("STRHR_Email")
                        'Dim strrm_email As String = ds1.Tables(0).Rows(0).Item("strrm_email")
                        'Dim strCCMail As String = strrm_email & ";" & strCirclehr_Email & ";" & strHR_Email
                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_LEASEREQUEST_EMPLOYEEvp_HR_MESSAGE")
                        'Dim ds As New DataSet()
                        'ds = sp.GetDataSet()
                        'Dim strsubj As String = ds.Tables(0).Rows(0).Item("MAIL_SUBJECT")
                        'Dim strmsg As String = ds.Tables(0).Rows(0).Item("MAIL_BODY")
                        'strmsg = strmsg.Replace("@@EmpNamee", ddlLesse.SelectedItem.Text)
                        'strmsg = strmsg.Replace("@@LeaseName", Request.QueryString("id"))
                        'strmsg = strmsg.Replace("@@Date", Lease_Date)
                        'strmsg = strmsg.Replace("@@Approved", StrApproved)
                        'strmsg = strmsg.Replace("@@loc", sthrloc)
                        'strmsg = strmsg.Replace("@@AppID", sthrid)
                        ''Response.Write(strmsg)
                        'Dim subj As String = "Leave and Licence Agreement"
                        'Send_Mail(stremail, strsubj, strmsg, strCCMail)
                        'Dim genword As String
                        ' genword = Generateword()
                        'Send_Mail(stremail, subj, genword, strHR_Email)

                        Dim dcc As Decimal = 0
                        'Dim dcc1 As Decimal = 0

                        If txtEmpRcryAmt.Text = "" Then
                            dcc = 0
                        Else
                            dcc = CDbl(txtEmpRcryAmt.Text)
                        End If

                        'If txtrcramt1.Text <> "" Or CDbl(txtEmpRcryAmt.Text) <> 0 Then
                        '    dcc1 = 0
                        'End If

                        If dcc > 0 Then
                            Dim SP9 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ALERT_RECOVERY_MAIL_COMPETANT")
                            SP9.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                            SP9.Command.AddParameter("@RECOVERY_AMOUNT", dcc, DbType.Decimal)
                            SP9.Command.AddParameter("@RECOVERY_FROMDATE", txtrcryfromdate.Text, DbType.DateTime)
                            SP9.Command.AddParameter("@RECOVERY_TODATE", txtrcrytodate.Text, DbType.DateTime)
                            SP9.ExecuteScalar()
                        End If

                        'If txtrcramt1.Text <> "" Or CDbl(txtEmpRcryAmt.Text) <> 0 Then
                        '    Dim SP10 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ALERT_RECOVERY_MAIL")
                        '    SP10.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                        '    SP10.Command.AddParameter("@RECOVERY_AMOUNT", dcc, DbType.Decimal)
                        '    SP10.ExecuteScalar()
                        'End If
                        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=49")

                        'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Lease Approved Succesfully..You can now print the Lease Details by clicking on PRINT button ! ')</SCRIPT>", False)
                        'trprint.Visible = True
                        'l1.Visible = True

                        'lblsysdate.Text = Date.Today.ToString("DD-MM-YYYY")
                        'lblsysdate.Visible = True
                        'btnprint.Enabled = True


                    End If

                Else
                    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Escalation Dates should be between Effective agreement date and Expiry agreement date!');", True)
                    Exit Sub
                End If
            ElseIf ddlesc.SelectedItem.Value = "No" Then
                If CDbl(txtInvestedArea.Text) <> (ld3rent + ld2rent + ld1rent) Then
                    lblMsg.Text = "Landlord(s) Month Rent Not Matched with the  LeaseRent Amount Entered"
                    'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord(s) Month Rent Not Matched With LeaseRent Entered');", True)
                    Exit Sub
                ElseIf CDbl(txtpay.Text) <> (ld3deposit + ld2deposit + ld1deposit) Then
                    lblMsg.Text = "Landlord(s) Security Deposit Not Matched with the Lease Security Deposit Amount Entered"
                    'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord(s) Security Deposit Not Matched With  Lease Security Deposit Entered');", True)
                    Exit Sub
                ElseIf brkamount > CDbl(txtentitle.Text) Then
                    lblMsg.Text = "Broker Amount Should not be more than Entitle Amount"
                    Exit Sub
                Else
                    If ddlleaseld.SelectedItem.Value = "1" Then
                        Dim LandLord1 As Integer
                        LandLord1 = ValidateLandlord1()
                        If LandLord1 = 0 Then
                            UpdateLandLord1()
                        End If
                    ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                        Dim Landlord2 As Integer
                        Landlord2 = ValidateLandlord2()
                        If Landlord2 = 0 Then
                            UpdateLandLord1()
                            UpdateLandLord2()
                        Else
                            UpdateLandLord1()
                            Landlord2Add()
                        End If
                    ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                        Dim Landlord3 As Integer
                        Landlord3 = ValidateLandlord3()
                        If Landlord3 = 0 Then
                            UpdateLandLord1()
                            UpdateLandLord2()
                            UpdateLandLord3()
                        Else
                            UpdateLandLord1()
                            UpdateLandLord2()
                            Landlord3Add()
                        End If
                    End If
                    ModifyLease()
                    ModifyEscalationDetails()
                    ModifyAgreementDetails()
                    ModifyRemarks()
                    ModifyBrokerageDetails()

                    If ddlpoa.SelectedItem.Value = "No" Then
                        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_update_REPORT_DOCUMENT1")
                        sp2.Command.AddParameter("@AXIS_SNO", 3, DbType.Int32)
                        sp2.Command.AddParameter("@_STATUS", "N", DbType.String)
                        sp2.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
                        sp2.Command.AddParameter("@SETS", "SET2", DbType.String)
                        sp2.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                        sp2.ExecuteScalar()
                    ElseIf ddlpoa.SelectedItem.Value = "Yes" Then
                        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_update_REPORT_DOCUMENT1")
                        sp2.Command.AddParameter("@AXIS_SNO", 3, DbType.Int32)
                        sp2.Command.AddParameter("@_STATUS", "Y", DbType.String)
                        sp2.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
                        sp2.Command.AddParameter("@SETS", "SET2", DbType.String)
                        sp2.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                        sp2.ExecuteScalar()
                    End If

                    If CDbl(txtbrkamount.Text) <> 0 And txtbrkamount.Text <> "" Then
                        Update_Axislabelmaster2()
                    Else
                        Update_AxislabelmasterBroker()
                    End If
                    ApproveLease()

                    'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETLEASEREQUEST_vp_HRDETAILS")
                    'sp1.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
                    'Dim ds1 As New DataSet()
                    'ds1 = sp1.GetDataSet()
                    'Dim Lease_Date As String = ds1.Tables(0).Rows(0).Item("creation_Date")
                    'Dim stremail As String = ds1.Tables(0).Rows(0).Item("AUR_EMAIL")
                    'Dim StrApproved As String = ds1.Tables(0).Rows(0).Item("StrApproved")
                    'Dim sthrid As String = ds1.Tables(0).Rows(0).Item("VP_HR_ID")
                    'Dim sthrloc As String = ds1.Tables(0).Rows(0).Item("Strhrloc")
                    'Dim strCirclehr_Email As String = ds1.Tables(0).Rows(0).Item("strCirclehr_Email")
                    'Dim strHR_Email As String = ds1.Tables(0).Rows(0).Item("STRHR_Email")
                    'Dim strrm_email As String = ds1.Tables(0).Rows(0).Item("strrm_email")
                    'Dim strCCMail As String = strrm_email & ";" & strCirclehr_Email & ";" & strHR_Email
                    'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_LEASEREQUEST_EMPLOYEEvp_HR_MESSAGE")
                    'Dim ds As New DataSet()
                    'ds = sp.GetDataSet()
                    'Dim strsubj As String = ds.Tables(0).Rows(0).Item("MAIL_SUBJECT")
                    'Dim strmsg As String = ds.Tables(0).Rows(0).Item("MAIL_BODY")
                    'strmsg = strmsg.Replace("@@EmpNamee", ddlLesse.SelectedItem.Text)
                    'strmsg = strmsg.Replace("@@LeaseName", Request.QueryString("id"))
                    'strmsg = strmsg.Replace("@@Date", Lease_Date)
                    'strmsg = strmsg.Replace("@@Approved", StrApproved)
                    'strmsg = strmsg.Replace("@@loc", sthrloc)
                    'strmsg = strmsg.Replace("@@AppID", sthrid)
                    ''Response.Write(strmsg)
                    'Dim subj As String = "Leave and Licence Agreement"
                    'Send_Mail(stremail, strsubj, strmsg, strCCMail)

                    Dim dcc As Decimal = 0
                    'Dim dcc1 As Decimal = 0

                    If txtEmpRcryAmt.Text = "" Then
                        dcc = 0
                    Else
                        dcc = CDbl(txtEmpRcryAmt.Text)
                    End If

                    'If txtrcramt1.Text <> "" Or CDbl(txtEmpRcryAmt.Text) <> 0 Then
                    '    dcc1 = 0
                    'Else
                    '    dcc1 = CDbl(txtEmpRcryAmt.Text)
                    'End If

                    If dcc > 0 Then
                        Dim SP9 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ALERT_RECOVERY_MAIL_COMPETANT")
                        SP9.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                        SP9.Command.AddParameter("@RECOVERY_AMOUNT", dcc, DbType.Decimal)
                        SP9.Command.AddParameter("@RECOVERY_FROMDATE", txtrcryfromdate.Text, DbType.DateTime)
                        SP9.Command.AddParameter("@RECOVERY_TODATE", txtrcrytodate.Text, DbType.DateTime)
                        SP9.ExecuteScalar()
                    End If

                    'If dcc1 <> 0 Then
                    '    Dim SP10 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ALERT_RECOVERY_MAIL")
                    '    SP10.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
                    '    SP10.Command.AddParameter("@RECOVERY_AMOUNT", dcc, DbType.Decimal)
                    '    SP10.ExecuteScalar()
                    'End If

                    'Dim genword As String
                    ' genword = Generateword()
                    'Send_Mail(stremail, subj, genword, strHR_Email)


                    Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=49", False)
                    'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Lease Approved Succesfully..You can now print the Lease Details by clicking on PRINT button ! ')</SCRIPT>", False)
                    'trprint.Visible = True
                    'l1.Visible = True
                    'lblsysdate.Text = Date.Today.ToString("dd-MMM-yyyy")
                    'lblsysdate.Visible = True
                    'btnprint.Enabled = True

                End If
                'Else
                'Response.Write("<script language=javascript> alert('Please Check the Data You Entered or Fill the Mandatory Fields')</SCRIPT>")
                ''lblMsg.Text = "Please Check the Data You Entered or Fill the Mandatory Fields"
            End If
        End If
    End Sub
    Protected Sub btntotal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btntotal.Click
        onetimecost()
        maintenancecost()

        Dim rent As Decimal = 0
        Dim esc1 As Decimal = 0
        Dim esc2 As Decimal = 0
        If txtservicetax.Text <> "" Then
            rent = CDbl(txtservicetax.Text)
        End If
        If txtmain.Text <> "" Then
            esc1 = CDbl(txtmain.Text)
        End If
        If txtInvestedArea.Text <> "" Then
            esc2 = CDbl(txtInvestedArea.Text)
        End If
        If Request.QueryString("ren") IsNot Nothing Then
            esc2 = basicamount
        End If

        txttotalrent.Text = rent + esc1 + esc2

        BindRecoveryDetails()


        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            panafteresc1.Visible = True

        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            txtrcramt1.Text = ""
            panafteresc1.Visible = False

        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            panrecwiz.Visible = False
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                panafteresc1.Visible = True
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
            Else
                panafteresc1.Visible = False
                txtrcramt1.Text = ""
            End If
        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
            Clear_Recovery_Details()
            panrecwiz.Visible = False
            panafteresc1.Visible = False

        End If


        If ddlleaseld.SelectedItem.Value = "1" Then
            txtpmonthrent.Text = txtInvestedArea.Text
            txtpsecdep.Text = txtpay.Text
            txtpmonthrent.ReadOnly = True
            txtpsecdep.ReadOnly = True
            txtld2rent.Text = 0
            txtld2sd.Text = 0
            txtld3rent.Text = 0
            txtld3sd.Text = 0
            panld1.Visible = True
            pnlld2.Visible = False
            pnlld3.Visible = False
        ElseIf ddlleaseld.SelectedItem.Value = "2" Then
            txtld2rent.Text = 0
            txtld2sd.Text = 0
            txtld3rent.Text = 0
            txtld3sd.Text = 0
            txtpmonthrent.Text = 0
            txtpsecdep.Text = 0
            txtpmonthrent.ReadOnly = False
            txtpsecdep.ReadOnly = False
            panld1.Visible = True
            pnlld2.Visible = True
            pnlld3.Visible = False
        ElseIf ddlleaseld.SelectedItem.Value = "3" Then
            txtld2rent.Text = 0
            txtld2sd.Text = 0
            txtld3rent.Text = 0
            txtld3sd.Text = 0
            txtpmonthrent.Text = 0
            txtpsecdep.Text = 0
            txtpmonthrent.ReadOnly = False
            txtpsecdep.ReadOnly = False
            panld1.Visible = True
            pnlld2.Visible = True
            pnlld3.Visible = True
        End If



    End Sub
    'Private Function Generateword()

    '    Dim moncount As String = ""
    '    Dim leasecost As Decimal
    '    Dim adv As Decimal = 0
    '    Dim GENDATE As String = ""
    '    Dim lease_code As String = Request.QueryString("id")
    '    Dim builtup As Decimal
    '    Dim strFileName As String = ""
    '    Dim lesrAdr As String = ""
    '    Dim Words As String = ""
    '    Dim Advwords As String = ""
    '    Dim LANDLORD_COST_WORDS As String = ""
    '    Dim LANDLORD_COST As Decimal = 0
    '    Dim LANDLORD_COUNT As Integer
    '    Dim LANDLORD_NAME As String = ""
    '    Dim Sum As String = ""
    '    Dim Sumwords As String = ""
    '    Dim paiddate As String = ""
    '    Dim month As String = ""
    '    Dim cheque As String = ""
    '    Dim txtLesr As String = ""
    '    Dim txtday As String = ""
    '    Dim txtmn As String = ""
    '    Dim txtcno As String = ""
    '    Dim txtLeaseaddr As String = ""
    '    Dim txtLesse As String = ""
    '    Dim txtLsdate As String = ""
    '    Dim txtLEdate As String = ""
    '    Dim TXTLSNAME As String = ""
    '    Dim txtlestype As String = ""
    '    Dim TXTADV As String = ""
    '    Dim txtcity As String = ""


    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LESSORS_GETDETAILS")
    '    sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
    '    Dim dr As SqlDataReader = sp.GetReader()
    '    While (dr.Read())
    '        txtLesr = txtLesr & dr("lessor_name").ToString() & ","
    '        txtPAN.Text = txtPAN.Text & dr("PAN").ToString() & ","
    '        txtday = dr("DAY1").ToString()
    '        txtmn = dr("mon").ToString()
    '        lesrAdr = dr("LESSOR_ADDRESS").ToString()
    '        GENDATE = dr("GENDATE").ToString()
    '        txtcno = txtcno & dr("PHNO").ToString() & ","
    '    End While
    '    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Axis_pn_leases_All")
    '    sp1.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
    '    Dim ds As New DataSet
    '    ds = sp1.GetDataSet()
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        txtLeaseaddr = ds.Tables(0).Rows(0).Item("ADDRESS")
    '        txtLesse = ds.Tables(0).Rows(0).Item("LESSE")
    '        txtLsdate = ds.Tables(0).Rows(0).Item("LEASE_START_DATE")
    '        txtLEdate = ds.Tables(0).Rows(0).Item("LEASE_EXPIRY_DATE")
    '        TXTLSNAME = ds.Tables(0).Rows(0).Item("LEASE_NAME")
    '        txtlestype = ds.Tables(0).Rows(0).Item("LEASE_TYPE")
    '        moncount = ds.Tables(0).Rows(0).Item("MONCOUNT")
    '        leasecost = ds.Tables(0).Rows(0).Item("LEASE_COST")
    '        TXTADV = ds.Tables(0).Rows(0).Item("ADVANCE")
    '        builtup = ds.Tables(0).Rows(0).Item("BUILTUP")
    '        strFileName = ds.Tables(0).Rows(0).Item("LEASE_NAME") + ".html"
    '        Words = ds.Tables(0).Rows(0).Item("WORDS")
    '        Advwords = ds.Tables(0).Rows(0).Item("WORDSADVANCE")
    '        txtcity = ds.Tables(0).Rows(0).Item("CITY")
    '    End If
    '    Dim SP2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GET_LANDLORD_COUNT")
    '    SP2.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
    '    Dim DS2 As New DataSet
    '    DS2 = SP2.GetDataSet()
    '    LANDLORD_NAME = DS2.Tables(0).Rows(0).Item("LANDLORD_NAME")
    '    LANDLORD_COST = DS2.Tables(0).Rows(0).Item("LANDLORD_COST")
    '    LANDLORD_COUNT = DS2.Tables(0).Rows.Count 'DS2.Tables(0).Rows(0).Item("LANDLORD_COUNT")
    '    LANDLORD_COST_WORDS = DS2.Tables(0).Rows(0).Item("LANDLORD_COST_WORDS")
    '    Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_EMPLOYEE_GET_PAYMENTDETAILS")
    '    sp4.Command.AddParameter("LEASE", Request.QueryString("id"), DbType.String)
    '    Dim ds4 As New DataSet()
    '    ds4 = sp4.GetDataSet()
    '    If ds4.Tables(0).Rows.Count > 0 Then


    '        Sum = ds4.Tables(0).Rows(0).Item("sum")
    '        Sumwords = ds4.Tables(0).Rows(0).Item("sumwords")
    '        paiddate = ds4.Tables(0).Rows(0).Item("paiddate")
    '        month = ds4.Tables(0).Rows(0).Item("month")
    '        cheque = ds4.Tables(0).Rows(0).Item("cheque")
    '    End If
    '    Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GET_LEAVELICENCEAGREEMENT")
    '    Dim ds3 As New DataSet
    '    ds3 = sp3.GetDataSet()
    '    Dim stragreement As String = ds3.Tables(0).Rows(0).Item("format_body")
    '    stragreement = stragreement.Replace("@@txtLesr.Text", txtLesr)
    '    stragreement = stragreement.Replace("@@txtLeaseaddr.Text", txtLeaseaddr)
    '    stragreement = stragreement.Replace("@@txtLsdate.Text", txtLsdate)
    '    stragreement = stragreement.Replace("@@txtLEdate.Text", txtLEdate)
    '    stragreement = stragreement.Replace("@@txtday.Text", txtday)
    '    stragreement = stragreement.Replace("@@txtmn.Text", txtmn)
    '    stragreement = stragreement.Replace("@@TXTLSNAME.Tex", TXTLSNAME)
    '    stragreement = stragreement.Replace("@@txtlestype.Text", txtlestype)
    '    stragreement = stragreement.Replace("@@txtLesse.Text", txtLesse)
    '    stragreement = stragreement.Replace("@@moncount", moncount)
    '    stragreement = stragreement.Replace("@@LANDLORD_NAME", LANDLORD_NAME)
    '    stragreement = stragreement.Replace("@@LANDLORD_COST", LANDLORD_COST)
    '    stragreement = stragreement.Replace("@@LANDLORD_COST_WORDS", LANDLORD_COST_WORDS)
    '    stragreement = stragreement.Replace("@@TXTADV.Text", TXTADV)
    '    stragreement = stragreement.Replace("@@GENDATE", GENDATE)
    '    stragreement = stragreement.Replace("@@lease_code ", lease_code)
    '    stragreement = stragreement.Replace("@@txtLeaseaddr.Text", txtLeaseaddr)
    '    stragreement = stragreement.Replace("@@lesrAdr ", lesrAdr)
    '    stragreement = stragreement.Replace("@@Cheque", cheque)
    '    stragreement = stragreement.Replace("@@PAIDDATE ", paiddate)
    '    stragreement = stragreement.Replace("@@MONTH", month)
    '    stragreement = stragreement.Replace("@@AmountWords", Sumwords)
    '    stragreement = stragreement.Replace("@@Sum", Sum)
    '    Return stragreement
    'End Function
    Private Sub Update_Axislabelmaster2()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_UPDATE_REPORT_DOCUMENT2")
        sp.Command.AddParameter("@AXIS_SNO", 1, DbType.Int32)
        sp.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp.Command.AddParameter("@SETS", "SET3", DbType.String)
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        sp.ExecuteScalar()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_UPDATE_REPORT_DOCUMENT2")
        sp1.Command.AddParameter("@AXIS_SNO", 2, DbType.Int32)
        sp1.Command.AddParameter("@_STATUS", "Y", DbType.String)
        sp1.Command.AddParameter("@SETS", "SET3", DbType.String)
        sp1.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        sp1.ExecuteScalar()
    End Sub
    Private Sub Update_AxislabelmasterBroker()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_UPDATE_REPORT_DOCUMENT2")
        sp.Command.AddParameter("@AXIS_SNO", 1, DbType.Int32)
        sp.Command.AddParameter("@_STATUS", "N", DbType.String)
        sp.Command.AddParameter("@SETS", "SET3", DbType.String)
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        sp.ExecuteScalar()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_UPDATE_REPORT_DOCUMENT2")
        sp1.Command.AddParameter("@AXIS_SNO", 2, DbType.Int32)
        sp1.Command.AddParameter("@_STATUS", "N", DbType.String)
        sp1.Command.AddParameter("@SETS", "SET3", DbType.String)
        sp1.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        sp1.ExecuteScalar()
    End Sub
    
    Public Sub Send_Mail(ByVal MailTo As String, ByVal subject As String, ByVal msg As String, ByVal strRMEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LEASE_INSERT_AMTMAIL")
        sp.Command.AddParameter("@VC_ID", ID, DbType.String)
        sp.Command.AddParameter("@VC_MSG", msg, DbType.String)
        sp.Command.AddParameter("@VC_MAIL", MailTo, DbType.String)
        sp.Command.AddParameter("@VC_SUB", subject, DbType.String)
        sp.Command.AddParameter("@DT_MAILTIME", Date.Today(), DbType.String)
        sp.Command.AddParameter("@VC_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@VC_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@VC_MAIL_CC", strRMEmail, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnreject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreject.Click
        Try
            If Page.IsValid = True Then
                lblMsg.Text = ""

                Dim ld3rent As Decimal = 0
                Dim ld2rent As Decimal = 0
                Dim ld1rent As Decimal = 0
                Dim ld3deposit As Decimal = 0
                Dim ld2deposit As Decimal = 0
                Dim ld1deposit As Decimal = 0


                If txtld3rent.Text <> "" Then
                    ld3rent = CDbl(txtld3rent.Text)
                End If

                If txtld2rent.Text <> "" Then
                    ld2rent = CDbl(txtld2rent.Text)
                End If

                If txtpmonthrent.Text <> "" Then
                    ld1rent = CDbl(txtpmonthrent.Text)
                End If

                If txtld3sd.Text <> "" Then
                    ld3deposit = CDbl(txtld3sd.Text)
                End If

                If txtld2sd.Text <> "" Then
                    ld2deposit = CDbl(txtld2sd.Text)
                End If

                If txtpsecdep.Text <> "" Then
                    ld1deposit = CDbl(txtpsecdep.Text)
                End If

                If CDbl(txtInvestedArea.Text) <> (ld3rent + ld2rent + ld1rent) Then
                    lblMsg.Text = "Landlord(s) Month Rent Not Matched with the  LeaseRent Amount Entered"
                    'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord(s) Month Rent Not Matched With LeaseRent Entered');", True)
                    Exit Sub
                ElseIf CDbl(txtpay.Text) <> (ld3deposit + ld2deposit + ld1deposit) Then
                    lblMsg.Text = "Landlord(s) Security Deposit Not Matched with the Lease Security Deposit Amount Entered"
                    'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord(s) Security Deposit Not Matched With  Lease Security Deposit Entered');", True)
                    Exit Sub
                Else

                    If ddlleaseld.SelectedItem.Value = "1" Then
                        Dim LandLord1 As Integer
                        LandLord1 = ValidateLandlord1()
                        If LandLord1 = 0 Then
                            UpdateLandLord1()
                        End If
                    ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                        Dim Landlord2 As Integer
                        Landlord2 = ValidateLandlord2()
                        If Landlord2 = 0 Then
                            UpdateLandLord1()
                            UpdateLandLord2()
                        Else
                            UpdateLandLord1()
                            Landlord2Add()
                        End If
                    ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                        Dim Landlord3 As Integer
                        Landlord3 = ValidateLandlord3()
                        If Landlord3 = 0 Then
                            UpdateLandLord1()
                            UpdateLandLord2()
                            UpdateLandLord3()
                        Else
                            UpdateLandLord1()
                            UpdateLandLord2()
                            Landlord3Add()
                        End If
                    End If
                    ModifyLease()
                    ModifyEscalationDetails()
                    ModifyAgreementDetails()
                    ModifyBrokerageDetails()
                    ModifyRemarks()
                    If CDbl(txtbrkamount.Text) <> 0 And txtbrkamount.Text <> "" Then
                        Update_Axislabelmaster2()
                    End If
                    RejectLease()
                    'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETLEASEREQUEST_vp_HRDETAILS")
                    'sp1.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
                    'Dim ds1 As New DataSet()
                    'ds1 = sp1.GetDataSet()
                    'Dim Lease_Date As String = ds1.Tables(0).Rows(0).Item("creation_Date")
                    'Dim stremail As String = ds1.Tables(0).Rows(0).Item("AUR_EMAIL")
                    'Dim strApproved As String = ds1.Tables(0).Rows(0).Item("StrApproved")
                    'Dim strCirclehr_Email As String = ds1.Tables(0).Rows(0).Item("strCirclehr_Email")
                    'Dim strHR_Email As String = ds1.Tables(0).Rows(0).Item("STRHR_Email")
                    'Dim strCCMail As String = strCirclehr_Email & ";" & strHR_Email

                    'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_LEASEREQUEST_EMPLOYEEvp_HR_MESSAGE1")
                    'Dim ds As New DataSet()
                    'ds = sp.GetDataSet()
                    'Dim strsubj As String = ds.Tables(0).Rows(0).Item("MAIL_SUBJECT")
                    'Dim strmsg As String = ds.Tables(0).Rows(0).Item("MAIL_BODY")
                    'strmsg = strmsg.Replace("@@EmpNamee", ddlLesse.SelectedItem.Text)
                    'strmsg = strmsg.Replace("@@LeaseName", Request.QueryString("id"))
                    'strmsg = strmsg.Replace("@@Date", Lease_Date)
                    'strmsg = strmsg.Replace("@@Rejected", strApproved)
                    ''Response.Write(strmsg)
                    'Send_Mail(stremail, strsubj, strmsg, strCCMail)

                    Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=46", False)
                End If
                'Else
                'Response.Write("<script language=javascript> alert('Please Check the Data You Entered or Fill the Mandatory Fields')</SCRIPT>")
                ''lblMsg.Text = "Please Check the Data You Entered or Fill the Mandatory Fields"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ModifyRemarks()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"LEASE_MODIFY_LEASE_VP_HR_REMARKS")
            sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@VP_HR_REMARKS", txtvphrremarks.Text, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateLandlord1()
        Dim Landlord1 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD1")
        sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
        Landlord1 = sp.ExecuteScalar()
        Return Landlord1
    End Function
    Private Function ValidateLandlord2()
        Dim Landlord2 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD2")
        sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
        Landlord2 = sp.ExecuteScalar()
        Return Landlord2
    End Function
    Private Function ValidateLandlord3()
        Dim Landlord3 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD3")
        sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
        Landlord3 = sp.ExecuteScalar()
        Return Landlord3
    End Function
    Private Sub RejectLease()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LEASES_VP_HR_REJECTLEASE")
        sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@STATUS", txtstore2.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub ApproveLease()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LEASES_VP_HR_APPROVELEASE")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@STATUS", txtstore2.Text, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        txtsdate.Attributes.Add("readonly", "readonly")
        txtedate.Attributes.Add("readonly", "readonly")
        txtescfromdate2.Attributes.Add("readonly", "readonly")
        txtesctodate2.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then

            trprint.Visible = False
            l1.Visible = False
            lblsysdate.Visible = False
            btnprint.Enabled = False
            ClearTextBox(Me)
            txtldemail.Text = ""
            txtescfromdate2.Text = ""
            txtesctodate2.Text = ""
            txtInvestedArea.Text = 0
            txtpay.Text = 0
            txtOccupiedArea.Text = 0
            txtagreeamt.Text = 0
            txtregamt.Text = 0
            txtnotice.Text = 0
            txtlock.Text = 0
            txtmob.Text = 0
            txtpmonthrent.Text = 0
            txtpsecdep.Text = 0
            txtEmpRcryAmt.Text = ""
            ddlStatus.SelectedItem.Value = "1"
            ddlStatus.Enabled = False
            txtfirstesc.Text = 0
            txtld2email.Text = ""
            txtld2mob.Text = 0
            txtld2rent.Text = 0
            txtld2sd.Text = 0
            txtld3email.Text = ""
            txtld3mob.Text = 0
            txtld3rent.Text = 0
            txtld3sd.Text = 0
            txtbrkamount.Text = 0
            BindPropertyType()
            BindProperty()

            BindLeaseType()
            BindCity()
            BindLandlordstate()

            ddlproperty.Enabled = False
            ddlLesse.Enabled = False
            lblMsg.Text = ""
            ddlMode.SelectedValue = "2"
            ddlMode.Enabled = False
            ddlproptype.SelectedValue = "1"
            ddlproptype.Enabled = False
            ddlLeaseType.SelectedValue = "2"
            ddlLeaseType.Enabled = False
            panrecwiz.Visible = False
            txtrcryfromdate.Text = ""
            txtrcrytodate.Text = ""
            txtEmpRcryAmt.ReadOnly = True
            txtentitle.ReadOnly = True
            ddlesctype.SelectedValue = "FLT"
            ddlesctype.Enabled = False

            trregagree.Visible = False
            panel1.Visible = True
            panel2.Visible = True
            panel3.Visible = True
            panel4.Visible = True
            panel5.Visible = True
            panel6.Visible = True
            pnlesc1.Visible = True
            pnlesc2.Visible = True

            panld1.Visible = True
            pnlld2.Visible = True
            pnlld3.Visible = True
            'landlord.Visible = False
            panPOA.Visible = True
            panrecwiz.Visible = True
            'Dim ValidBroker As Integer
            'ValidBroker = ValidBroker1()
            'If ValidBroker = 0 Then
            '    panbrk.Visible = True
            '    txtbrkamount.Text = 0
            '    txtbrkmob.Text = 0
            'Else
            '    panbrk.Visible = False
            'End If

            'btn1Next.Visible = True
            ' btn2prev.Visible = False
            ' btn2Next.Visible = False
            'btn3Prev.Visible = False
            btnApprove.Visible = True
            btnreject.Visible = True
            txtEmpAccNo.ReadOnly = True
            txtEmpBankName.ReadOnly = True
            txtEmpBranch.ReadOnly = True
            txtrcryfromdate.ReadOnly = True
            txtrcrytodate.ReadOnly = True
            panafteresc1.Visible = False
            panafteresc2.Visible = False
            txtrcramt1.ReadOnly = True
            txtrcramt2.ReadOnly = True
            txtrcrfrmdate1.ReadOnly = True
            txtrcrfrmdate2.ReadOnly = True
            txtrcrtodate1.ReadOnly = True
            txtrcrtodate2.ReadOnly = True

            txtPOAMobile.Text = 0
            txtPOAEmail.Text = ""


            panel1.Visible = False
            panel2.Visible = False
            panelL12.Visible = False
            panel3.Visible = False
            panel4.Visible = False
            pnll22.Visible = False
            panel5.Visible = False
            panel6.Visible = False
            pnll32.Visible = False

            lblHead.Text = "Approval by Competent Authority"
            Dim id As String = Request.QueryString("id")
            GetEntitle(id)
            BindLeaseDetails(id)
            BindGrid()
            BindEscalationDetails(id)
            BindAgreementDetails(id)

            BindBrokerageDetails(id)
            BindRecoveryDetails()
            GetRMName()
            GetName()

            BindLandLordDetails(Request.QueryString("id"))

            If ddlleaseld.SelectedItem.Value = "1" Then
                panld1.Visible = True
                pnlld2.Visible = False
                pnlld3.Visible = False

            ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = False

            ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = True

            End If





            If ddlpoa.SelectedValue = "Yes" Then
                panPOA.Visible = True
            Else
                panPOA.Visible = False
            End If
            Dim Interest As Double = ((CDbl(txtpay.Text) * 9) / 100) / 12
            txtstore1.Text = Interest


            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                panafteresc1.Visible = True

            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                txtrcramt1.Text = ""
                panafteresc1.Visible = False


            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                panrecwiz.Visible = False

                If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                    panafteresc1.Visible = True
                    txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                Else
                    panafteresc1.Visible = False
                    txtrcramt1.Text = ""
                End If
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                Clear_Recovery_Details()
                panrecwiz.Visible = False
                panafteresc1.Visible = False

            End If

            Dim dt As DateTime = Convert.ToDateTime(txtsdate.Text).ToString("dd-MMM-yyyy")
            txtrcryfromdate.Text = txtsdate.Text
            If ddlesc.SelectedItem.Value = "Yes" Then
                txtrcrytodate.Text = dt.AddYears(1).ToString("dd-MMM-yyyy")
            Else
                txtrcrytodate.Text = txtedate.Text
            End If
            txtrcrfrmdate1.Text = txtEscalationDate.Text
            txtrcrtodate1.Text = txtesctodate1.Text
            txtrcrfrmdate2.Text = txtescfromdate2.Text
            txtrcrtodate2.Text = txtesctodate2.Text



            If ddlleaseld.SelectedItem.Value = "1" Then
                txtpmonthrent.ReadOnly = True
                txtpsecdep.ReadOnly = True
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
            ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
                txtld3rent.Text = 0
                txtld3sd.Text = 0
            ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
            End If


            If ddlesc.SelectedItem.Value = "Yes" Then
                If txtescfromdate2.Text <> "" And txtEscalationDate.Text <> "" Then
                    pnlesc1.Visible = True
                    pnlesc2.Visible = True
                ElseIf txtescfromdate2.Text = "" And txtEscalationDate.Text <> "" Then
                    pnlesc1.Visible = True
                    pnlesc2.Visible = False
                End If
            Else
                pnlesc1.Visible = False
                pnlesc2.Visible = False
            End If
            onetimecost()
            maintenancecost()
            Dim rent As Decimal = 0
            Dim esc1 As Decimal = 0
            Dim esc2 As Decimal = 0
            If txtservicetax.Text <> "" Then
                rent = CDbl(txtservicetax.Text)
            End If
            If txtmain.Text <> "" Then
                esc1 = CDbl(txtmain.Text)
            End If
            If txtInvestedArea.Text <> "" Then
                esc2 = CDbl(txtInvestedArea.Text)
            End If

            txttotalrent.Text = rent + esc1 + esc2
          

            btnprint.Attributes.Add("onclick", "printPartOfPage('Div1')")
        End If

        txtmain1.Attributes.Add("readonly", "readonly")
        txtbasic.Attributes.Add("readonly", "readonly")
        txtmain.Attributes.Add("readonly", "readonly")
        txttotalrent.Attributes.Add("readonly", "readonly")
        txtAgreedate.Attributes.Add("readonly", "readonly")
        txtsdate.Attributes.Add("readonly", "readonly")
        txtedate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindProperty()
        ddlproperty.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_PROPERTY_LEASE")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlproperty.DataSource = sp.GetDataSet()
        ddlproperty.DataTextField = "pn_name"
        ddlproperty.DataValueField = "BDG_ID"
        ddlproperty.DataBind()
        ddlproperty.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub GetName()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"LEASE_GET_NAME_HR")
        sp.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()

        If (ds.Tables(0).Rows(0).Item("RM_DATE")) Is Nothing Then
            lblrmdate.Text = ds.Tables(0).Rows(0).Item("RM_DATE")

        Else
            lblrmdate.Text = ""
        End If
        If ds.Tables(0).Rows(0).Item("HR_NAME") Is Nothing Then
            lblhr.Text = ds.Tables(0).Rows(0).Item("HR_NAME")
        Else
            lblhr.Text = ""
        End If
        If ds.Tables(0).Rows(0).Item("HR_DATE") Is Nothing Then
            lblhrdate.Text = ds.Tables(0).Rows(0).Item("HR_DATE")
        Else
            lblhrdate.Text = ""
        End If
    End Sub
    Private Sub GetRMName()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_NAME")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblRMname.Text = ds.Tables(0).Rows(0).Item("RM_NAME")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindRecoveryDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"LEASE_EMPLOYEE_ACCOUNT_DETAILS")
            sp.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
            Dim DS As New DataSet()
            DS = sp.GetDataSet()
            If DS.Tables(0).Rows.Count > 0 Then
                txtEmpAccNo.Text = DS.Tables(0).Rows(0).Item("AUR_ACCOUNT_NUMBER")
                txtEmpBankName.Text = DS.Tables(0).Rows(0).Item("AUR_BANK_NAME")
                txtEmpBranch.Text = DS.Tables(0).Rows(0).Item("AUR_BRANCH_NAME")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
   
    Private Function BindLandlord2(ByVal id As String)
        Dim l2 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LANDLORD2")
        sp.Command.AddParameter("@LEASE", id, DbType.String)
        l2 = sp.ExecuteScalar()
        Return l2
    End Function
    Public Sub ClearTextBox(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearTextBox(ctrl)
            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Text = ""
            End If
        Next ctrl
    End Sub
    Private Function BindLandlord3(ByVal id As String)
        Dim l3 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LANDLORD3")
        sp.Command.AddParameter("@LEASE", id, DbType.String)
        l3 = sp.ExecuteScalar()
        Return l3
    End Function
    Private Function ValidateBroker(ByVal id As String)
        Dim validbrk As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALID_BROKER_DETAILS")
        sp.Command.AddParameter("LEASE", id, DbType.String)
        validbrk = sp.ExecuteScalar()
        Return validbrk
    End Function
    Private Sub GetEntitle(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETENTITLE_USER_RM")
            sp.Command.AddParameter("@LEASE", ID, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                lblmaxrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                lblmaxsd.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                'txtstore1.Text = ds.Tables(0).Rows(0).Item("RECOVERY")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GET_EMPDETAILS_RMAPPROVAL")
        sp.Command.AddParameter("@LESSE_ID", txtstore.Text, DbType.String)
        gvEmpDetails.DataSource = sp.GetDataSet()
        gvEmpDetails.DataBind()
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY_ENTITLE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select City--"))

        'ddlld1city.DataSource = sp.GetDataSet()
        'ddlld1city.DataTextField = "CTY_NAME"
        'ddlld1city.DataValueField = "CTY_CODE"
        'ddlld1city.DataBind()
        'ddlld1city.Items.Insert(0, New ListItem("--Select--", "--Select City--"))

        'ddlld2city.DataSource = sp.GetDataSet()
        'ddlld2city.DataTextField = "CTY_NAME"
        'ddlld2city.DataValueField = "CTY_CODE"
        'ddlld2city.DataBind()
        'ddlld2city.Items.Insert(0, New ListItem("--Select--", "--Select City--"))

        'ddlld3city.DataSource = sp.GetDataSet()
        'ddlld3city.DataTextField = "CTY_NAME"
        'ddlld3city.DataValueField = "CTY_CODE"
        'ddlld3city.DataBind()
        'ddlld3city.Items.Insert(0, New ListItem("--Select--", "--Select City--"))
    End Sub
    Private Sub BindPropertyType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("UID"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLandlordstate()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY_STATE")

        ddlstate.DataSource = sp.GetDataSet()
        ddlstate.DataTextField = "STE_NAME"
        ddlstate.DataValueField = "STE_CODE"
        ddlstate.DataBind()
        ddlstate.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        ddlld2state.DataSource = sp.GetDataSet()
        ddlld2state.DataTextField = "STE_NAME"
        ddlld2state.DataValueField = "STE_CODE"
        ddlld2state.DataBind()
        ddlld2state.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        ddlld3state.DataSource = sp.GetDataSet()
        ddlld3state.DataTextField = "STE_NAME"
        ddlld3state.DataValueField = "STE_CODE"
        ddlld3state.DataBind()
        ddlld3state.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub BindLeaseType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlLeaseType.DataSource = sp3.GetDataSet()
        ddlLeaseType.DataTextField = "PN_LEASE_TYPE"
        ddlLeaseType.DataValueField = "PN_LEASE_ID"
        ddlLeaseType.DataBind()
        ddlLeaseType.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindLeaseDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_LEASES_GETDETAILS")
            sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()

            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CITY")).Selected = True
            ddlproperty.ClearSelection()
            ddlproperty.Items.FindByValue(ds.Tables(0).Rows(0).Item("PROPERTY_ADDRESS1")).Selected = True
            txtBuilding.Text = ds.Tables(0).Rows(0).Item("PROPERTY_ADDRESS2")
            txtprop3.Text = ds.Tables(0).Rows(0).Item("STATE")
            txtregion.Text = ds.Tables(0).Rows(0).Item("REGION")
            txtsduty.Text = ds.Tables(0).Rows(0).Item("STAMP_DUTY")
            txtregcharges.Text = ds.Tables(0).Rows(0).Item("REGISTRATION_CHARGES")
            txtpfees.Text = ds.Tables(0).Rows(0).Item("PROFESSIONAL_FEES")
            txtbrokerage.Text = ds.Tables(0).Rows(0).Item("BROKERAGE_AMOUNT")
            txtbasic.Text = ds.Tables(0).Rows(0).Item("BASIC_rENT")
            txtmain.Text = ds.Tables(0).Rows(0).Item("MAINTENANCE_CHARGES")
            txtdg.Text = ds.Tables(0).Rows(0).Item("DG_BACKUP_CHARGES")


            txtproptax.Text = ds.Tables(0).Rows(0).Item("PROPERTY_TAX")
            txtimp.Text = ds.Tables(0).Rows(0).Item("LEASEHOLDIMPROVEMENTS")
            txtrentrev.Text = ds.Tables(0).Rows(0).Item("RENT_REVISION")


            txttenure.Text = ds.Tables(0).Rows(0).Item("TENURE_AGREEMENT")
            txtfurniture.Text = ds.Tables(0).Rows(0).Item("FURNITURE")
            txtofcequip.Text = ds.Tables(0).Rows(0).Item("OFFICE_EQUIPMENTS")


            txtLnumber.Text = ds.Tables(0).Rows(0).Item("CTS_NUMBER")
            ddlLesse.ClearSelection()
            ddlLesse.Items.Insert(0, ds.Tables(0).Rows(0).Item("LESSE_ID"))
            txtstore.Text = ds.Tables(0).Rows(0).Item("LESSE")
            'txtEscalationDate.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATION_DATE")
            'txtEmail1.Text = ds.Tables(0).Rows(0).Item("ESCALATED_EMAIL")
            txtInvestedArea.Text = ds.Tables(0).Rows(0).Item("LEASE_RENT")
            txtOccupiedArea.Text = ds.Tables(0).Rows(0).Item("BUILTUP_AREA")
            bindpropertytax()
            txtmain1.Text = maintper * CDbl(txtOccupiedArea.Text)
            txtservicetax.Text = servper * CDbl(txtOccupiedArea.Text)
            ddlStatus.ClearSelection()
            ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("LEASE_STA_ID")).Selected = True
            txtpay.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")

            txtpincode.Text = ds.Tables(0).Rows(0).Item("PINCODE")
            ddlpoa.ClearSelection()
            ddlpoa.Items.FindByValue(ds.Tables(0).Rows(0).Item("POA")).Selected = True

            ddlleaseld.ClearSelection()
            ddlleaseld.Items.FindByValue(ds.Tables(0).Rows(0).Item("LANDLORDS")).Selected = True

            ddlesc.ClearSelection()
            ddlesc.Items.FindByValue(ds.Tables(0).Rows(0).Item("LEASE_ESCALATION")).Selected = True

            Dim stat1 As String = ds.Tables(0).Rows(0).Item("LEASE_ESCALATION")
            If stat1 = "Yes" Then
                pnlesc1.Visible = True
                pnlesc2.Visible = True
            Else
                pnlesc1.Visible = False
                pnlesc2.Visible = False
            End If
            ddlesctype.ClearSelection()
            ddlesctype.Items.FindByValue(ds.Tables(0).Rows(0).Item("ESCALATION")).Selected = True
            txtentitle.Text = ds.Tables(0).Rows(0).Item("ENTITLE_LEASE_AMOUNT")
            txtComments.Text = ds.Tables(0).Rows(0).Item("LEASE_COMMENTS")
            txtrmremarks.Text = ds.Tables(0).Rows(0).Item("RM_REMARKS")
            txthrremarks.Text = ds.Tables(0).Rows(0).Item("HR_REMARKS")
            txtstore2.Text = ds.Tables(0).Rows(0).Item("LEASE_STATUS")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindEscalationDetails(ByVal id As String)

        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_ESCALATION_DETAILS_GETDETAILS")
            sp.Command.AddParameter("@LEASE", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            txtEscalationDate.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_FROM_DATE1")
            txtesctodate1.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_TO_DATE1")
            txtescfromdate2.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_FROM_DATE2")
            txtesctodate2.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_TO_DATE2")
            txtfirstesc.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_AMOUNT1")
            txtsecondesc.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_AMOUNT2")
            txtEmpAccNo.Text = ds.Tables(0).Rows(0).Item("EMPLOYEE_ACCOUNT_NUMBER")
            txtEmpBankName.Text = ds.Tables(0).Rows(0).Item("EMPLOYEE_BANK_NAME")
            txtEmpBranch.Text = ds.Tables(0).Rows(0).Item("EMPLOYEE_BRANCH_NAME")
            txtEmpRcryAmt.Text = ds.Tables(0).Rows(0).Item("RECOVERY_AMOUNT")
            txtrcrytodate.Text = ds.Tables(0).Rows(0).Item("RECOVERY_TODATE")
            txtrcryfromdate.Text = ds.Tables(0).Rows(0).Item("RECOVERY_FROMDATE")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLandLordDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_LANDLORDS_GETDETAILS")
            sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtldname.Text = ds.Tables(0).Rows(0).Item("LANDLORD_NAME")
                txtldaddr.Text = ds.Tables(0).Rows(0).Item("LANDLORD_aDDRESS")
                txtld1addr2.Text = ds.Tables(0).Rows(0).Item("LANDLORD_ADDERSS2")
                txtld1addr3.Text = ds.Tables(0).Rows(0).Item("LANDLORD_ADDRESS3")
                txtld1Pin.Text = ds.Tables(0).Rows(0).Item("LANDLORD_PIN")
                'ddlld1city.ClearSelection()
                'ddlld1city.Items.FindByValue(ds.Tables(0).Rows(0).Item("LANDLORD_CITY")).Selected = True
                ddlld1city.Text = ds.Tables(0).Rows(0).Item("LANDLORD_CITY")
                ddlstate.ClearSelection()
                ddlstate.Items.FindByValue(ds.Tables(0).Rows(0).Item("LANDLORD_STATE")).Selected = True
                txtPAN.Text = ds.Tables(0).Rows(0).Item("LANDLORD_PAN")
                txtldemail.Text = ds.Tables(0).Rows(0).Item("LANDLORD_EMAIL")
                txtmob.Text = ds.Tables(0).Rows(0).Item("LANDLORD_MOBILE")
                txtpfromdate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
                txtptodate.Text = ds.Tables(0).Rows(0).Item("TO_DATE")
                txtpmonthrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                txtpsecdep.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                ddlpaymentmode.ClearSelection()
                ddlpaymentmode.Items.FindByValue(ds.Tables(0).Rows(0).Item("PAYMENT_MODE")).Selected = True
                ' txtCheque.Text = ds.Tables(0).Rows(0).Item("CHEQUE")
                'txtBankName.Text = ds.Tables(0).Rows(0).Item("ISSUING_BANK")
                txtL12Accno.Text = ds.Tables(0).Rows(0).Item("ACCOUNT_NUMBER")
                txtAccNo.Text = ds.Tables(0).Rows(0).Item("ACCOUNT_NUMBER")
                txtIBankName.Text = ds.Tables(0).Rows(0).Item("ACCOUNT_NUMBER")
                txtBankName.Text = ds.Tables(0).Rows(0).Item("DEPOSITED_BANK")
                txtDeposited.Text = ds.Tables(0).Rows(0).Item("DEPOSITED_BANK")
                txtIFSC.Text = ds.Tables(0).Rows(0).Item("IFSC_CODE")
                txtbrnch.Text = ds.Tables(0).Rows(0).Item("BRANCH_NAME")
                Dim paymode As String = ds.Tables(0).Rows(0).Item("PAYMENT_MODE")
                If paymode = "1" Then
                    panel1.Visible = True
                    panel2.Visible = False
                    panelL12.Visible = False

                ElseIf paymode = "2" Then
                    panel1.Visible = False
                    panel2.Visible = False
                    panelL12.Visible = True
                Else
                    panel1.Visible = False
                    panel2.Visible = True
                    panelL12.Visible = False
                End If

            End If
            If ds.Tables(1).Rows.Count > 0 Then
                txtld1name.Text = ds.Tables(1).Rows(0).Item("LANDLORD_NAME")
                txtld2addr.Text = ds.Tables(1).Rows(0).Item("LANDLORD_aDDRESS")
                txtld2pan.Text = ds.Tables(1).Rows(0).Item("LANDLORD_PAN")
                txtld2email.Text = ds.Tables(1).Rows(0).Item("LANDLORD_EMAIL")
                txtld2mob.Text = ds.Tables(1).Rows(0).Item("LANDLORD_MOBILE")
                txtld2frmdate.Text = ds.Tables(1).Rows(0).Item("FROM_DATE")
                txtld2todate.Text = ds.Tables(1).Rows(0).Item("TO_DATE")
                txtld2rent.Text = ds.Tables(1).Rows(0).Item("RENT_AMOUNT")
                txtld2sd.Text = ds.Tables(1).Rows(0).Item("SECURITY_DEPOSIT")
                ddlld2mode.ClearSelection()
                ddlld2mode.Items.FindByValue(ds.Tables(1).Rows(0).Item("PAYMENT_MODE")).Selected = True
                'txtld2cheque.Text = ds.Tables(1).Rows(0).Item("CHEQUE")
                'txtld2bankname.Text = ds.Tables(1).Rows(0).Item("ISSUING_BANK")
                txtld2addr2.Text = ds.Tables(1).Rows(0).Item("LANDLORD_ADDERSS2")
                txtld2addr3.Text = ds.Tables(1).Rows(0).Item("LANDLORD_ADDRESS3")
                txtld2Pin.Text = ds.Tables(1).Rows(0).Item("LANDLORD_PIN")
                'ddlld2city.ClearSelection()
                'ddlld2city.Items.FindByValue(ds.Tables(1).Rows(0).Item("LANDLORD_CITY")).Selected = True
                ddlld2city.Text = ds.Tables(1).Rows(0).Item("LANDLORD_CITY")
                ddlld2state.ClearSelection()
                ddlld2state.Items.FindByValue(ds.Tables(1).Rows(0).Item("LANDLORD_STATE")).Selected = True
                txtl22accno.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtld2accno.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtld2IBankName.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtld2bankname.Text = ds.Tables(1).Rows(0).Item("DEPOSITED_BANK")
                txtld2Deposited.Text = ds.Tables(1).Rows(0).Item("DEPOSITED_BANK")
                txtld2IFSC.Text = ds.Tables(1).Rows(0).Item("IFSC_CODE")
                txtl2brnchname.Text = ds.Tables(1).Rows(0).Item("BRANCH_NAME")
                Dim paymode1 As String = ds.Tables(1).Rows(0).Item("PAYMENT_MODE")
                If paymode1 = "1" Then
                    panel3.Visible = False
                    panel4.Visible = False
                    pnll22.Visible = False
                ElseIf paymode1 = "2" Then
                    panel3.Visible = False
                    panel4.Visible = False
                    pnll22.Visible = True
                Else
                    panel3.Visible = False
                    panel4.Visible = True
                    pnll22.Visible = False
                End If
            End If
            If ds.Tables(2).Rows.Count > 0 Then

                txtld3name.Text = ds.Tables(2).Rows(0).Item("LANDLORD_NAME")
                txtld3addr.Text = ds.Tables(2).Rows(0).Item("LANDLORD_aDDRESS")
                txtld3pan.Text = ds.Tables(2).Rows(0).Item("LANDLORD_PAN")
                txtld3email.Text = ds.Tables(2).Rows(0).Item("LANDLORD_EMAIL")
                txtld3mob.Text = ds.Tables(2).Rows(0).Item("LANDLORD_MOBILE")
                txtld3fromdate.Text = ds.Tables(2).Rows(0).Item("FROM_DATE")
                txtld3todate.Text = ds.Tables(2).Rows(0).Item("TO_DATE")
                txtld3rent.Text = ds.Tables(2).Rows(0).Item("RENT_AMOUNT")
                txtld3sd.Text = ds.Tables(2).Rows(0).Item("SECURITY_DEPOSIT")
                ddlld3mode.ClearSelection()
                ddlld3mode.Items.FindByValue(ds.Tables(2).Rows(0).Item("PAYMENT_MODE")).Selected = True
                'txtld3cheque.Text = ds.Tables(2).Rows(0).Item("CHEQUE")
                ' txtld3bankname.Text = ds.Tables(2).Rows(0).Item("ISSUING_BANK")
                txtl32accno.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtld3addr2.Text = ds.Tables(2).Rows(0).Item("LANDLORD_ADDERSS2")
                txtld3addr3.Text = ds.Tables(2).Rows(0).Item("LANDLORD_ADDRESS3")
                txtld3Pin.Text = ds.Tables(2).Rows(0).Item("LANDLORD_PIN")
                'ddlld3city.ClearSelection()
                'ddlld3city.Items.FindByValue(ds.Tables(2).Rows(0).Item("LANDLORD_CITY")).Selected = True
                ddlld3city.Text = ds.Tables(2).Rows(0).Item("LANDLORD_CITY")
                ddlld3state.ClearSelection()
                ddlld3state.Items.FindByValue(ds.Tables(2).Rows(0).Item("LANDLORD_STATE")).Selected = True
                txtLd3acc.Text = ds.Tables(2).Rows(0).Item("ACCOUNT_NUMBER")
                txtld3IBankName.Text = ds.Tables(2).Rows(0).Item("ACCOUNT_NUMBER")
                txtld3bankname.Text = ds.Tables(1).Rows(0).Item("DEPOSITED_BANK")
                txtld3Deposited.Text = ds.Tables(2).Rows(0).Item("DEPOSITED_BANK")
                txtld3ifsc.Text = ds.Tables(2).Rows(0).Item("IFSC_CODE")
                txtl3brnch.Text = ds.Tables(2).Rows(0).Item("BRANCH_NAME")
                Dim paymode2 As String = ds.Tables(2).Rows(0).Item("PAYMENT_MODE")
                If paymode2 = "1" Then
                    panel5.Visible = False
                    panel6.Visible = False
                    pnll32.Visible = False
                ElseIf paymode2 = "2" Then
                    panel5.Visible = False
                    panel6.Visible = False
                    pnll32.Visible = True
                Else
                    panel5.Visible = False
                    panel6.Visible = True
                    pnll32.Visible = False
                End If


            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindAgreementDetails(ByVal id As String)
        Try
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_AGREEMENT_DETAILS_GETDETAILS")
                sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
                Dim ds As New DataSet()
                ds = sp.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    txtAgreedate.Text = ds.Tables(0).Rows(0).Item("AGREEMENT_EXECUTION_DATE")

                    txtagreeamt.Text = ds.Tables(0).Rows(0).Item("AMOUNT_OF_STAMP_DUTY_PAID")
                    txtregamt.Text = ds.Tables(0).Rows(0).Item("REGISTRATION_AMOUNT")
                    txtsdate.Text = ds.Tables(0).Rows(0).Item("EFFECTIVE_AGREEMENT_DATE")
                    txtedate.Text = ds.Tables(0).Rows(0).Item("EXPIRY_AGREEMENT_DATE")
                    ddlagreeres.ClearSelection()
                    ddlagreeres.Items.FindByValue(ds.Tables(0).Rows(0).Item("AGREEMENT_REGISTERED")).Selected = True
                    txtagreeregdate.Text = ds.Tables(0).Rows(0).Item("REGISTRATION_DATE")
                    txtagreesub.Text = ds.Tables(0).Rows(0).Item("SUB_REGISTARS_OFFICE_NAME")
                    txtPOAName.Text = ds.Tables(0).Rows(0).Item("POA_NAME")
                    txtPOAMobile.Text = ds.Tables(0).Rows(0).Item("POA_MOBILE")
                    txtPOAEmail.Text = ds.Tables(0).Rows(0).Item("POA_EMAIL")
                    txtPOAAddress.Text = ds.Tables(0).Rows(0).Item("POA_ADDRESS")
                    Dim res As String = ds.Tables(0).Rows(0).Item("AGREEMENT_REGISTERED")
                    If res = "Yes" Then
                        trregagree.Visible = True
                    Else
                        trregagree.Visible = False
                    End If
                    txtnotice.Text = ds.Tables(0).Rows(0).Item("TERMINATION_NOTICE")
                    txtlock.Text = ds.Tables(0).Rows(0).Item("LOCKIN_PERIOD_OF_AGREEMENT")
                End If
            If Request.QueryString("ren") IsNot Nothing Then
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_AGREEMENT_EXTENTION_DETAILS_GETDETAILS")
                sp1.Command.AddParameter("@LEASE_NAME", id, DbType.String)
                Dim ds1 As New DataSet()
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    startdate = txtsdate.Text
                    txtsdate.Text = ds1.Tables(0).Rows(0).Item("EXTESNION_FROMDATE")
                    enddate = txtedate.Text
                    txtedate.Text = ds1.Tables(0).Rows(0).Item("EXTESNION_TODATE")
                    basicamount = txtInvestedArea.Text
                    txtInvestedArea.Text = ds1.Tables(0).Rows(0).Item("RENT_AMOUNT")
                    secDeposit = txtpay.Text
                    txtpay.Text = ds1.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                    Remark = txtimp.Text
                    txtimp.Text = ds1.Tables(0).Rows(0).Item("EXTESNION_REMARKS")
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindBrokerageDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_BROKERAGE_DETAILS_GETDETAILS")
            sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtbrkamount.Text = ds.Tables(0).Rows(0).Item("AMOUNT_OF_BROKERAGE_PAID")
                txtbrkname.Text = ds.Tables(0).Rows(0).Item("BROKER_NAME")
                txtbrkaddr.Text = ds.Tables(0).Rows(0).Item("BROKER_aDDRESS")
                txtbrkpan.Text = ds.Tables(0).Rows(0).Item("PAN_NO")
                txtbrkremail.Text = ds.Tables(0).Rows(0).Item("EMAIL")
                txtbrkmob.Text = ds.Tables(0).Rows(0).Item("MOBILE_NO")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdateLandLord1()
        Try

            Dim ld1rent As Decimal = 0
            Dim ld1deposit As Decimal = 0

            If txtpmonthrent.Text <> "" Then
                ld1rent = CDbl(txtpmonthrent.Text)
            End If

            If txtpsecdep.Text <> "" Then
                ld1deposit = CDbl(txtpsecdep.Text)
            End If
            txtpfromdate.Text = txtsdate.Text
            txtptodate.Text = txtedate.Text

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_MODIFY_LANDLORD")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtldname.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtldaddr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld1addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld1addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld1city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlstate.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld1Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtPAN.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtldemail.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtmob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD1", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtpfromdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtptodate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", ld1rent, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", ld1deposit, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlpaymentmode.SelectedItem.Value, DbType.String)

            If ddlpaymentmode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtBankName.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtAccNo.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", "", DbType.String)
            ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then

                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtL12Accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", "", DbType.String)

            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtDeposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtIBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtIFSC.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", txtbrnch.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdateLandLord2()
        Try

            Dim ld2rent As Decimal = 0
            Dim ld2deposit As Decimal = 0

            If txtld2rent.Text <> "" Then
                ld2rent = CDbl(txtld2rent.Text)
            End If

            If txtld2sd.Text <> "" Then
                ld2deposit = CDbl(txtld2sd.Text)
            End If
            txtld2frmdate.Text = txtsdate.Text
            txtld2todate.Text = txtedate.Text

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_MODIFY_LANDLORD")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtld1name.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtld2addr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld2addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld2addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld2city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlld2state.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld2Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtld2pan.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtld2email.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtld2mob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD2", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtld2frmdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtld2todate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", ld2rent, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", ld2deposit, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlld2mode.SelectedItem.Value, DbType.String)
            If ddlld2mode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld2bankname.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld2accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", "", DbType.String)
            ElseIf ddlld2mode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtl22accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", "", DbType.String)
            ElseIf ddlld2mode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld2Deposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld2IBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtld2IFSC.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", txtl2brnchname.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdateLandLord3()
        Try

            Dim ld3rent As Decimal = 0
            Dim ld3deposit As Decimal = 0

            If txtld3rent.Text <> "" Then
                ld3rent = CDbl(txtld3rent.Text)
            End If

            If txtld3sd.Text <> "" Then
                ld3deposit = CDbl(txtld3sd.Text)
            End If
            txtld3fromdate.Text = txtsdate.Text
            txtld3todate.Text = txtedate.Text
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_MODIFY_LANDLORD")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtld3name.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtld3addr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld3addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld3addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld3city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlld3state.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld3Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtld3pan.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtld3email.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtld3mob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD3", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtld3fromdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtld3todate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", ld3rent, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", ld3deposit, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlld3mode.SelectedItem.Value, DbType.String)
            If ddlld3mode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld3bankname.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtLd3acc.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", "", DbType.String)
            ElseIf ddlld3mode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtl32accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", "", DbType.String)
            ElseIf ddlld3mode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld3Deposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld3IBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtld3ifsc.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH_NAME", txtl3brnch.Text, DbType.String)


            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ModifyBrokerageDetails()
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@LEASE_NAME", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("id")
        param(1) = New SqlParameter("@AMOUNT_OF_BROKERAGE_PAID", SqlDbType.Float)
        If txtbrkamount.Text = "" Then
            param(1).Value = 0
        Else

            param(1).Value = txtbrkamount.Text
        End If

        param(2) = New SqlParameter("@BROKER_NAME", SqlDbType.NVarChar, 50)
        If txtbrkname.Text = "" Then
            param(2).Value = ""
        Else
            param(2).Value = txtbrkname.Text
        End If

        param(3) = New SqlParameter("@BROKER_ADDRESS", SqlDbType.NVarChar, 1000)
        If txtbrkaddr.Text = "" Then
            param(3).Value = ""
        Else
            param(3).Value = txtbrkaddr.Text
        End If

        param(4) = New SqlParameter("@PAN_NO", SqlDbType.NVarChar, 50)
        If txtbrkpan.Text = "" Then
            param(4).Value = ""
        Else
            param(4).Value = txtbrkpan.Text
        End If

        param(5) = New SqlParameter("@EMAIL", SqlDbType.NVarChar, 50)
        If txtbrkremail.Text = "" Then
            param(5).Value = ""
        Else
            param(5).Value = txtbrkremail.Text
        End If

        param(6) = New SqlParameter("@MOBILE_NO", SqlDbType.NVarChar, 15)
        If txtbrkmob.Text = "" Then
            param(6).Value = 0
        Else
            param(6).Value = txtbrkmob.Text
        End If
        ObjSubSonic.GetSubSonicExecute("UPDATE_BROKERAGE_DETAILS", param)
    End Sub
    Private Sub ModifyAgreementDetails()
        Dim param(14) As SqlParameter
        param(0) = New SqlParameter("@LEASE_NAME", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("ID")

        param(1) = New SqlParameter("@AGREEMENT_EXECUTION_DATE", SqlDbType.DateTime)
        If txtAgreedate.Text = "" Then
            param(1).Value = DBNull.Value
        Else
            param(1).Value = txtAgreedate.Text
        End If
        param(2) = New SqlParameter("@AMOUNT_OF_STAMP_DUTY_PAID", SqlDbType.Float)
        param(2).Value = txtagreeamt.Text
        param(3) = New SqlParameter("@REGISTRATION_AMOUNT", SqlDbType.Float)
        param(3).Value = txtregamt.Text
        param(4) = New SqlParameter("@EFFECTIVE_AGREEMENT_DATE", SqlDbType.DateTime)
        param(4).Value = txtsdate.Text

        param(5) = New SqlParameter("@EXPIRY_AGREEMENT_DATE", SqlDbType.DateTime)
        param(5).Value = txtedate.Text
        ' added by praveen 
        If Request.QueryString("ren") IsNot Nothing Then
            ' COMMENTED BY NETAJI, TO NOT DISTURB AGREEMENT DATE WHILE EXTENSION
            'param(4) = New SqlParameter("@EFFECTIVE_AGREEMENT_DATE", SqlDbType.DateTime)
            'param(4).Value = startdate

            param(5) = New SqlParameter("@EXPIRY_AGREEMENT_DATE", SqlDbType.DateTime)
            param(5).Value = enddate
        End If
        'ends here
        param(6) = New SqlParameter("@AGREEMENT_REGISTERED", SqlDbType.NVarChar, 200)
        param(6).Value = ddlagreeres.SelectedItem.Value
        param(7) = New SqlParameter("@REGISTRATION_DATE", SqlDbType.DateTime)
        If txtagreeregdate.Text = "" Then
            param(7).Value = DBNull.Value
        Else
            param(7).Value = txtagreeregdate.Text
        End If

        param(8) = New SqlParameter("@SUB_REGISTARS_OFFICE_NAME", SqlDbType.NVarChar, 200)
        param(8).Value = txtagreesub.Text
        param(9) = New SqlParameter("@TERMINATION_NOTICE", SqlDbType.NVarChar, 200)
        param(9).Value = txtnotice.Text
        param(10) = New SqlParameter("@LOCKIN_PERIOD_OF_AGREEMENT", SqlDbType.NVarChar, 200)
        param(10).Value = txtlock.Text
        param(11) = New SqlParameter("@POA_NAME", SqlDbType.NVarChar, 200)
        param(11).Value = txtPOAName.Text
        param(12) = New SqlParameter("@POA_MOBILE", SqlDbType.NVarChar, 200)
        param(12).Value = txtPOAMobile.Text
        param(13) = New SqlParameter("@POA_EMAIL", SqlDbType.NVarChar, 200)
        param(13).Value = txtPOAEmail.Text
        param(14) = New SqlParameter("@POA_ADDRESS", SqlDbType.NVarChar, 1000)
        param(14).Value = txtPOAAddress.Text
        ObjSubSonic.GetSubSonicExecute("UPDATE_LEASE_AGREEMENT_DETAILS", param)
    End Sub
    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then


            If ddlpaymentmode.SelectedItem.Value = "1" Then
                panel1.Visible = True
                panel2.Visible = False
                panell12.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then
                panel1.Visible = False
                panel2.Visible = False
                panell12.Visible = True
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
                panell12.Visible = False
            End If
        End If
    End Sub
    Private Sub ModifyLease()
        Try
            Dim dec_Flat As Decimal = 0
            Dim dec_Percent As Decimal = 0
            Dim Builtup As Decimal = 0
            If txtOccupiedArea.Text <> "" Then
                Builtup = CDbl(txtOccupiedArea.Text)
            End If


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_LEASES_UPDATE")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@PROPERTY_TYPE", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@PROPERTY_ADDRESS1", ddlproperty.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@PROPERTY_ADDRESS2", txtBuilding.Text, DbType.String)
            sp.Command.AddParameter("@PROPERTY_ADDRESS3", txtprop3.Text, DbType.String)
            sp.Command.AddParameter("@CTS_NUMBER", txtLnumber.Text, DbType.String)
            sp.Command.AddParameter("@LEASE_TYPE", ddlLeaseType.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LESSE", txtstore.Text, DbType.String)

            sp.Command.AddParameter("@LEASE_ESCALATION", ddlesc.SelectedItem.Value, DbType.String)
            'If txtEscalationDate.Text = "" Then
            '    sp.Command.AddParameter("@LEASE_ESCALATION_DATE", Date.Today(), DbType.Date)
            'Else
            '    sp.Command.AddParameter("@LEASE_ESCALATION_DATE", txtEscalationDate.Text, DbType.Date)
            'End If

            sp.Command.AddParameter("@ESCALATED_EMAIL", "", DbType.String)
            sp.Command.AddParameter("@LEASE_RENT", txtInvestedArea.Text, DbType.Decimal)
            sp.Command.AddParameter("@BUILTUP_AREA", Builtup, DbType.Decimal)
            sp.Command.AddParameter("@LEASE_STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)

            'added byb praveen
            If Request.QueryString("ren") Is Nothing Then
                sp.Command.AddParameter("@SECURITY_DEPOSIT", txtpay.Text, DbType.Decimal)
            Else
                'sp.Command.AddParameter("@SECURITY_DEPOSIT", secDeposit, DbType.Decimal)
                sp.Command.AddParameter("@SECURITY_DEPOSIT", txtpay.Text, DbType.Decimal)
            End If
            'ends here

            sp.Command.AddParameter("@LEASE_ESC_TYPE", ddlesctype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@PERCENTAGE", dec_Percent, DbType.Decimal)
            sp.Command.AddParameter("@FLAT_AMOUNT", dec_Flat, DbType.Decimal)
            sp.Command.AddParameter("@ENTITLE_LEASE_AMOUNT", txtentitle.Text, DbType.Decimal)
            sp.Command.AddParameter("@LEASE_COST_PER", ddlMode.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LEASE_COMMENTS", txtComments.Text, DbType.String)
            sp.Command.AddParameter("@PIN_CODE", txtpincode.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORDS", ddlleaseld.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@POA", ddlpoa.SelectedItem.Value, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub LandLord1History()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtldname.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtpfromdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtptodate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtpmonthrent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD1", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub Landlord2History()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtld1name.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtld2frmdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtld2todate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtld2rent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD2", DbType.String)
        sp.ExecuteScalar()

    End Sub
    Private Sub Landlord3History()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtld3name.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtld3fromdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtld3todate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtld3rent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD3", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub Landlord1HistoryAdd()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", txtstore.Text, DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtldname.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtpfromdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtptodate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtpmonthrent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD1", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub Landlord2HistoryAdd()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", txtstore.Text, DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtld1name.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtld2frmdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtld2todate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtld2rent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD2", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub Landlord3HistoryAdd()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LANDLORD_HISTORY")
        sp.Command.AddParameter("LEASE_NAME", txtstore.Text, DbType.String)
        sp.Command.AddParameter("LANDLORD_NAME", txtld3name.Text, DbType.String)
        sp.Command.AddParameter("FROM_DATE", txtld3fromdate.Text, DbType.Date)
        sp.Command.AddParameter("TO_DATE", txtld3todate.Text, DbType.Date)
        sp.Command.AddParameter("LEASE_RENT", txtld3rent.Text, DbType.Decimal)
        sp.Command.AddParameter("LANDLORD_NUMBER", "LANDLORD3", DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Function ValidateleaseL()
        Dim validatelease As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LEASE")
        sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
        validatelease = sp.ExecuteScalar()
        Return validatelease
    End Function
    Private Function ValidateLeaseL2()
        Dim validatelandlord2 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD2")
        sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
        validatelandlord2 = sp.ExecuteScalar()
        Return validatelandlord2
    End Function
    Private Function ValidateLeaseL3()
        Dim validatelandlord3 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD3")
        sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
        validatelandlord3 = sp.ExecuteScalar()
        Return validatelandlord3
    End Function
    Private Sub Landlord1Add()
        Try
            lblMsg.Text = ""
            Dim dec_ld2rent As Decimal = 0
            Dim dec_ld3rent As Decimal = 0
            Dim dec_ld2sd As Decimal = 0
            Dim dec_ld3sd As Decimal = 0
            If txtld2rent.Text <> "" Then
                dec_ld2rent = CDbl(txtld2rent.Text)
            End If
            If txtld3rent.Text <> "" Then
                dec_ld3rent = CDbl(txtld3rent.Text)
            End If
            If txtld2sd.Text <> "" Then
                dec_ld2sd = CDbl(txtld2sd.Text)
            End If
            If txtld3sd.Text <> "" Then
                dec_ld3sd = CDbl(txtld3sd.Text)
            End If
            'Dim validatelease As Integer
            'validatelease = ValidateleaseL()
            'If validatelease = 0 Then
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord1 Details already there for this Lease!');", True)
            'ElseIf CDbl(txtpmonthrent.Text) > CDbl(CDbl(txtInvestedArea.Text) - (CDbl(dec_ld2rent) + CDbl(dec_ld3rent))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Month Rent exceeded!');", True)
            'ElseIf CDbl(txtpsecdep.Text) > CDbl(CDbl(txtpay.Text) - (CDbl(dec_ld2sd) + CDbl(dec_ld3sd))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Security Deposit exceeded!');", True)
            ' Else

            AddLandLord1()
            Landlord1History()
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('LandLord1 Details Added Succesfully!');", True)
            ' End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Landlord2Add()
        Try
            lblMsg.Text = ""
            Dim dec_ld1rent As Decimal = 0
            Dim dec_ld3rent As Decimal = 0
            Dim dec_ld1sd As Decimal = 0
            Dim dec_ld3sd As Decimal = 0

            Dim dec_ld2sd As Decimal = 0
            Dim dec_ld2rent As Decimal = 0

            If txtld2sd.Text <> "" Then
                dec_ld2sd = CDbl(txtld2sd.Text)
            End If

            If txtld2rent.Text <> "" Then
                dec_ld2rent = CDbl(txtld2rent.Text)
            End If

            If txtpmonthrent.Text <> "" Then
                dec_ld1rent = CDbl(txtpmonthrent.Text)
            End If
            If txtld3rent.Text <> "" Then
                dec_ld3rent = CDbl(txtld3rent.Text)
            End If
            If txtpsecdep.Text <> "" Then
                dec_ld1sd = CDbl(txtpsecdep.Text)
            End If
            If txtld3sd.Text <> "" Then
                dec_ld3sd = CDbl(txtld3sd.Text)
            End If
            'Dim validateLandLord2 As Integer
            'validateLandLord2 = ValidateLeaseL2()
            'If validateLandLord2 = 0 Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord2 Details already there for this Lease');", True)
            'ElseIf dec_ld2rent > CDbl(CDbl(txtInvestedArea.Text) - (CDbl(dec_ld1rent) + CDbl(dec_ld3rent))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Month Rent exceeded!');", True)
            'ElseIf dec_ld2sd > CDbl(CDbl(txtpay.Text) - (CDbl(dec_ld1sd) + CDbl(dec_ld3sd))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Security Deposit exceeded!');", True)
            'ElseIf txtld1name.Text <> "" And txtld2sd.Text <> "" And txtld2rent.Text <> "" And txtld2frmdate.Text <> "" And txtld2todate.Text <> "" Then
            '    lblMsg.Text = ""
            AddLandLord2()
            Landlord2History()
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('LandLord2 Details Added Succesfully!');", True)
            'End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Landlord3Add()
        Try
            lblMsg.Text = ""
            Dim dec_ld1rent As Decimal = 0
            Dim dec_ld2rent As Decimal = 0
            Dim dec_ld3rent As Decimal = 0
            Dim dec_ld1sd As Decimal = 0
            Dim dec_ld2sd As Decimal = 0
            Dim dec_ld3sd As Decimal = 0

            If txtld3rent.Text <> "" Then
                dec_ld3rent = CDbl(txtld3rent.Text)
            End If

            If txtld3sd.Text <> "" Then
                dec_ld3sd = CDbl(txtld3sd.Text)
            End If


            If txtpmonthrent.Text <> "" Then
                dec_ld1rent = CDbl(txtpmonthrent.Text)
            End If

            If txtld2rent.Text <> "" Then
                dec_ld2rent = CDbl(txtld2rent.Text)
            End If
            If txtpsecdep.Text <> "" Then
                dec_ld1sd = CDbl(txtpsecdep.Text)
            End If
            If txtld2sd.Text <> "" Then
                dec_ld2sd = CDbl(txtld2sd.Text)
            End If
            'Dim validateLandlord3 As Integer
            'validateLandlord3 = ValidateLeaseL3()
            'If validateLandlord3 = 0 Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord3 Details already there for this Lease');", True)
            'ElseIf dec_ld3rent > CDbl(CDbl(txtInvestedArea.Text) - (CDbl(dec_ld1rent) + CDbl(dec_ld2rent))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Month Rent Exceeded');", True)
            'ElseIf dec_ld3sd > CDbl(CDbl(txtpay.Text) - (CDbl(dec_ld1sd) + CDbl(dec_ld2sd))) Then
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Security Deposit Exceeded');", True)
            'ElseIf txtld1name.Text <> "" And txtld3sd.Text <> "" And txtld3rent.Text <> "" And txtld3fromdate.Text <> "" And txtld3todate.Text <> "" Then
            '    lblMsg.Text = ""
            AddLandLord3()
            Landlord3History()
            '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Landlord3 Details Added Succesfully');", True)
            '    End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub AddLandLord1()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LANDLORD1")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtldname.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtldaddr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld1addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld1addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld1city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlstate.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld1Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtPAN.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtldemail.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtmob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD1", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtpfromdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtptodate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", txtpmonthrent.Text, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", txtpsecdep.Text, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlpaymentmode.SelectedItem.Value, DbType.String)
            If ddlpaymentmode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtBankName.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtAccNo.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtL12Accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtDeposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtIBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtIFSC.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", txtbrnch.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub AddLandLord2()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LANDLORD1")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtld1name.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtld2addr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld2addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld2addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld2city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlld2state.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld2Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtld2pan.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtld2email.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtld2mob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD2", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtld2frmdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtld2todate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", txtld2rent.Text, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", txtld2sd.Text, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlld2mode.SelectedItem.Value, DbType.String)
            If ddlld2mode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld2bankname.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld2accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlld2mode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtl22accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlld2mode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld2Deposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld2IBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtld2IFSC.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", txtl2brnchname.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub AddLandLord3()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LANDLORD1")
            sp.Command.AddParameter("@LEASE_NAME", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@LANDLORD_NAME", txtld3name.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS", txtld3addr.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS2", txtld3addr2.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_ADDRESS3", txtld3addr3.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_CITY", ddlld3city.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_STATE", ddlld3state.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PIN", txtld3Pin.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_PAN", txtld3pan.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_EMAIL", txtld3email.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_MOB", txtld3mob.Text, DbType.String)
            sp.Command.AddParameter("@LANDLORD_NUMBER", "LANDLORD3", DbType.String)
            sp.Command.AddParameter("@FROM_DATE", txtld3fromdate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtld3todate.Text, DbType.Date)
            sp.Command.AddParameter("@RENT_AMOUNT", txtld3rent.Text, DbType.Decimal)
            sp.Command.AddParameter("@SECURITY_DEPOSIT", txtld3sd.Text, DbType.Decimal)
            sp.Command.AddParameter("@PAYMENT_MODE", ddlld3mode.SelectedItem.Value, DbType.String)
            If ddlld3mode.SelectedItem.Value = "1" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld3bankname.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtLd3acc.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)
            ElseIf ddlld3mode.SelectedItem.Value = "2" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", "", DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtl32accno.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", "", DbType.String)
                sp.Command.AddParameter("@BRANCH", "", DbType.String)

            ElseIf ddlld3mode.SelectedItem.Value = "3" Then
                sp.Command.AddParameter("@CHEQUE", "", DbType.String)
                sp.Command.AddParameter("@ISSUING_BANK", "AXIS", DbType.String)
                sp.Command.AddParameter("@DEPOSITED_BANK", txtld3Deposited.Text, DbType.String)
                sp.Command.AddParameter("@ACCOUNT_NUMBER", txtld3IBankName.Text, DbType.String)
                sp.Command.AddParameter("@IFSC_CODE", txtld3ifsc.Text, DbType.String)
                sp.Command.AddParameter("@BRANCH", txtl3brnch.Text, DbType.String)

            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlld3mode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld3mode.SelectedIndexChanged
        If ddlld3mode.SelectedIndex > 0 Then
            If ddlld3mode.SelectedItem.Value = "1" Then
                panel5.Visible = False
                panel6.Visible = False
                pnll32.Visible = False
            ElseIf ddlld3mode.SelectedItem.Value = "2" Then
                panel5.Visible = False
                panel6.Visible = False
                pnll32.Visible = True
            ElseIf ddlld3mode.SelectedItem.Value = "3" Then
                panel5.Visible = False
                panel6.Visible = True
                pnll32.Visible = False
            End If
        End If
    End Sub
    Protected Sub ddlld2mode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld2mode.SelectedIndexChanged
        If ddlld2mode.SelectedIndex > 0 Then


            If ddlld2mode.SelectedItem.Value = "1" Then
                panel3.Visible = False
                panel4.Visible = False
                pnll22.Visible = False
            ElseIf ddlld2mode.SelectedItem.Value = "2" Then
                panel3.Visible = False
                panel4.Visible = False
                pnll22.Visible = True
            ElseIf ddlld2mode.SelectedItem.Value = "3" Then
                panel3.Visible = False
                panel4.Visible = True
                pnll22.Visible = False
            End If
        End If
    End Sub
    Protected Sub ddlagreeres_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlagreeres.SelectedIndexChanged
        If ddlagreeres.SelectedValue = "Yes" Then
            trregagree.Visible = True
        Else
            trregagree.Visible = False
        End If
    End Sub


    Private Sub ModifyEscalationDetails()
        Try
            Dim decrcryAmt As Decimal = 0
            If txtEmpRcryAmt.Text <> "" Then
                decrcryAmt = txtEmpRcryAmt.Text
            End If

            Dim param(12) As SqlParameter
            param(0) = New SqlParameter("@LEASE_NAME", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("id")
            If ddlesc.SelectedValue = "Yes" Then
                param(1) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE1", SqlDbType.DateTime)
                param(1).Value = txtEscalationDate.Text
                param(2) = New SqlParameter("@LEASE_ESCALATED_TO_DATE1", SqlDbType.DateTime)
                param(2).Value = txtesctodate1.Text
                param(3) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE2", SqlDbType.DateTime)
                If txtescfromdate2.Text = "" Then
                    param(3).Value = DBNull.Value
                Else
                    param(3).Value = txtescfromdate2.Text
                End If
                param(4) = New SqlParameter("@LEASE_ESCALATED_TO_DATE2", SqlDbType.DateTime)
                If txtesctodate2.Text = "" Then
                    param(4).Value = DBNull.Value
                Else
                    param(4).Value = txtesctodate2.Text
                End If
                param(5) = New SqlParameter("@LEASE_ESCALATED_AMOUNT1", SqlDbType.Float)
                param(5).Value = txtfirstesc.Text
                param(6) = New SqlParameter("@LEASE_ESCALATED_AMOUNT2", SqlDbType.Float)
                If txtsecondesc.Text = "" Then
                    param(6).Value = 0
                Else

                    param(6).Value = txtsecondesc.Text
                End If

            Else
                param(1) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE1", SqlDbType.DateTime)
                param(1).Value = DBNull.Value
                param(2) = New SqlParameter("@LEASE_ESCALATED_TO_DATE1", SqlDbType.DateTime)
                param(2).Value = DBNull.Value
                param(3) = New SqlParameter("@LEASE_ESCALATED_FROM_DATE2", SqlDbType.DateTime)
                param(3).Value = DBNull.Value
                param(4) = New SqlParameter("@LEASE_ESCALATED_TO_DATE2", SqlDbType.DateTime)
                param(4).Value = DBNull.Value
                param(5) = New SqlParameter("@LEASE_ESCALATED_AMOUNT1", SqlDbType.Float)
                param(5).Value = 0
                param(6) = New SqlParameter("@LEASE_ESCALATED_AMOUNT2", SqlDbType.Float)
                param(6).Value = 0
            End If
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) Then
                param(7) = New SqlParameter("@RECOVERY_FROMDATE", SqlDbType.DateTime)
                param(7).Value = txtrcryfromdate.Text
                param(8) = New SqlParameter("@RECOVERY_TODATE", SqlDbType.DateTime)
                param(8).Value = txtrcrytodate.Text
                param(9) = New SqlParameter("@RECOVERY_AMOUNT", SqlDbType.Float)
                param(9).Value = decrcryAmt
                param(10) = New SqlParameter("@EMPLOYEE_ACCOUNT_NUMBER", SqlDbType.NVarChar)
                param(10).Value = txtEmpAccNo.Text
                param(11) = New SqlParameter("@EMPLOYEE_BANK_NAME", SqlDbType.NVarChar)
                param(11).Value = txtEmpBankName.Text
                param(12) = New SqlParameter("@EMPLOYEE_BRANCH_NAME", SqlDbType.NVarChar)
                param(12).Value = txtEmpBranch.Text
            Else
                param(7) = New SqlParameter("@RECOVERY_FROMDATE", SqlDbType.DateTime)
                param(7).Value = DBNull.Value
                param(8) = New SqlParameter("@RECOVERY_TODATE", SqlDbType.DateTime)
                param(8).Value = DBNull.Value
                param(9) = New SqlParameter("@RECOVERY_AMOUNT", SqlDbType.Float)
                param(9).Value = decrcryAmt
                param(10) = New SqlParameter("@EMPLOYEE_ACCOUNT_NUMBER", SqlDbType.NVarChar)
                param(10).Value = ""
                param(11) = New SqlParameter("@EMPLOYEE_BANK_NAME", SqlDbType.NVarChar)
                param(11).Value = ""
                param(12) = New SqlParameter("@EMPLOYEE_BRANCH_NAME", SqlDbType.NVarChar)
                param(12).Value = ""

            End If

            ObjSubSonic.GetSubSonicExecute("MODIFY_ESCALATION_DETAILS", param)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlesc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlesc.SelectedIndexChanged
        If ddlesc.SelectedIndex > 0 Then
            If ddlesc.SelectedValue = "Yes" Then
                Dim diff As Integer = DateDiff("yyyy", txtsdate.Text, txtedate.Text)
                If diff = 1 Then
                    Dim diff1 As Integer = DateDiff("m", CDate(txtsdate.Text), CDate(txtedate.Text))
                    If diff1 <= 12 Then
                        lblMsg.Text = "Escalation will takes place only if the Difference of Effective and Expiry Agreement Date is more than a Year "
                        ddlesc.SelectedIndex = 0
                        Exit Sub
                    ElseIf diff1 > 12 And diff1 <= 24 Then
                        lblMsg.Text = ""
                        pnlesc1.Visible = True
                        pnlesc2.Visible = False
                    ElseIf diff1 > 24 And diff1 <= 36 Then
                        lblMsg.Text = ""
                        pnlesc1.Visible = True
                        pnlesc2.Visible = True
                    End If
                ElseIf diff = 2 Then
                    lblMsg.Text = ""
                    pnlesc1.Visible = True
                    pnlesc2.Visible = False
                Else
                    lblMsg.Text = ""
                    pnlesc1.Visible = True
                    pnlesc2.Visible = True
                End If

                Dim dt As DateTime = Convert.ToDateTime(txtsdate.Text)
                txtEscalationDate.Text = dt.AddYears(1).ToString("dd-MMM-yyyy")
                Dim dt1 As DateTime = Convert.ToDateTime(txtEscalationDate.Text)
                Dim todate1 As DateTime = dt1.AddYears(1).ToString("dd-MMM-yyyy")
                txtesctodate1.Text = todate1.AddDays(-1).ToString("dd-MMM-yyyy")
                txtesctodate2.Text = Convert.ToString(DBNull.Value)
                txtescfromdate2.Text = Convert.ToString(DBNull.Value)
                txtfirstesc.Text = 0
                txtsecondesc.Text = 0
            ElseIf ddlesc.SelectedValue = "No" Then
                lblMsg.Text = ""
                pnlesc1.Visible = False
                pnlesc2.Visible = False
                txtEscalationDate.Text = ""
                txtesctodate1.Text = ""
                txtesctodate2.Text = ""
                txtescfromdate2.Text = ""
                txtEscalationDate.Text = Convert.ToString(DBNull.Value)
                txtesctodate1.Text = Convert.ToString(DBNull.Value)
                txtesctodate2.Text = Convert.ToString(DBNull.Value)
                txtescfromdate2.Text = Convert.ToString(DBNull.Value)
                txtfirstesc.Text = ""
                txtsecondesc.Text = ""
            End If
        Else
            pnlesc1.Visible = False
            pnlesc2.Visible = False
            txtEscalationDate.Text = Convert.ToString(DBNull.Value)
            txtesctodate1.Text = Convert.ToString(DBNull.Value)
            txtesctodate2.Text = Convert.ToString(DBNull.Value)
            txtescfromdate2.Text = Convert.ToString(DBNull.Value)
            txtfirstesc.Text = ""
            txtsecondesc.Text = ""
        End If
    End Sub

    Private Sub Clear_Recovery_Details()
        txtEmpBankName.Text = ""
        txtEmpBranch.Text = ""
        txtEmpAccNo.text = ""
        txtEmpRcryAmt.Text = ""
        txtrcrytodate.Text = Date.Today
        txtrcryfromdate.Text = Date.Today
    End Sub
    Protected Sub btn1Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn1Next.Click
        tr_HR.Visible = False
        tr_RM.Visible = False
        tr_VP_HR.Visible = False
        Dim dt As DateTime = Convert.ToDateTime(txtsdate.Text).ToString("dd-MMM-yyyy")
        txtrcryfromdate.Text = txtsdate.Text
        txtrcrytodate.Text = dt.AddDays(365).Date().ToString("dd-MMM-yyyy")
        txtrcrfrmdate1.Text = txtEscalationDate.Text
        txtrcrtodate1.Text = txtesctodate1.Text
        txtrcrfrmdate2.Text = txtescfromdate2.Text
        txtrcrtodate2.Text = txtesctodate2.Text
        Dim Interest As Double = ((CDbl(txtpay.Text) * 9) / 100) / 12
        txtstore1.Text = Interest
        'If Page.IsValid = True Then
        'panwiz1.Visible = False
        btn1Next.Visible = False
        If ddlpoa.SelectedItem.Value = "Yes" Then
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                panafteresc1.Visible = True
                lblHead.Text = "Lease Details"
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                txtrcramt1.Text = ""
                panafteresc1.Visible = False
                lblHead.Text = "Lease Details"
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                panrecwiz.Visible = False
                If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                    panafteresc1.Visible = True
                    txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                Else
                    panafteresc1.Visible = False
                    txtrcramt1.Text = ""
                End If
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                Clear_Recovery_Details()
                panrecwiz.Visible = False
                panafteresc1.Visible = False
                lblHead.Text = "Lease Details"
            End If
            panPOA.Visible = True
            panld1.Visible = False
            pnlld2.Visible = False
            pnlld3.Visible = False
            btn2Next.Visible = True
            btn2prev.Visible = True
            btn3Prev.Visible = False
            btnApprove.Visible = False
            btnreject.Visible = False

        ElseIf ddlpoa.SelectedItem.Value = "No" Then
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                panafteresc1.Visible = True
                panrecwiz.Visible = True
                btn2Next.Visible = True
                btn2prev.Visible = True
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                panld1.Visible = False
                pnlld2.Visible = False
                pnlld3.Visible = False
                lblHead.Text = "Lease Details"

            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                txtrcramt1.Text = ""
                panafteresc1.Visible = False
                btn2Next.Visible = True
                btn2prev.Visible = True
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                panld1.Visible = False
                pnlld2.Visible = False
                pnlld3.Visible = False
                lblHead.Text = "Lease Details"
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                panrecwiz.Visible = False
                If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                    panafteresc1.Visible = True
                    txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                    btn2Next.Visible = True
                    btn2prev.Visible = True
                    btn3Prev.Visible = False
                    btnApprove.Visible = False
                    btnreject.Visible = False
                    panld1.Visible = False
                    pnlld2.Visible = False
                    pnlld3.Visible = False
                Else
                    panafteresc1.Visible = False
                    txtrcramt1.Text = ""
                    btn2Next.Visible = False
                    btn2prev.Visible = False
                    btn3Prev.Visible = True
                    btnApprove.Visible = True
                    btnreject.Visible = True
                    panld1.Visible = True
                    pnlld2.Visible = True
                    pnlld3.Visible = True
                End If

            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                panrecwiz.Visible = False
                panafteresc1.Visible = False
                If ddlleaseld.SelectedItem.Value = "1" Then
                    panld1.Visible = True
                    pnlld2.Visible = False
                    pnlld3.Visible = False
                ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                    panld1.Visible = True
                    pnlld2.Visible = True
                    pnlld3.Visible = False
                ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                    panld1.Visible = True
                    pnlld2.Visible = True
                    pnlld3.Visible = True
                End If

                btn2Next.Visible = False
                btn2prev.Visible = False
                btn3Prev.Visible = True
                btnApprove.Visible = True
                btnreject.Visible = True
                lblHead.Text = "Landlord Details"
                panPOA.Visible = False
            End If

        End If
        ' panbrk.Visible = True

        ' panwiz3.Visible = True
        'Else
        '    ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please Check the Data You Entered or Fill the Mandatory Fields!');", True)
        'End If
    End Sub
    Protected Sub btn2Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn2Next.Click
        tr_HR.Visible = True
        tr_RM.Visible = True
        tr_VP_HR.Visible = True
        panafteresc1.Visible = False
        'If Page.IsValid = True Then
        'panwiz1.Visible = False
        panPOA.Visible = False
        panrecwiz.Visible = False
        panwiz3.Visible = False
        panbrk.Visible = False
        If ddlleaseld.SelectedItem.Value = "1" Then
            panld1.Visible = True
            pnlld2.Visible = False
            pnlld3.Visible = False
        ElseIf ddlleaseld.SelectedItem.Value = "2" Then
            panld1.Visible = True
            pnlld2.Visible = True
            pnlld3.Visible = False
        ElseIf ddlleaseld.SelectedItem.Value = "3" Then
            panld1.Visible = True
            pnlld2.Visible = True
            pnlld3.Visible = True
        End If
        btn1Next.Visible = False
        btn2prev.Visible = False
        btn2Next.Visible = False
        btn3Prev.Visible = True
        btnApprove.Visible = True
        btnreject.Visible = True
        lblHead.Text = "Landlord Details"
        BindLandLordDetails(Request.QueryString("id"))
        ' End If
    End Sub

    Protected Sub btn2prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn2prev.Click
        tr_HR.Visible = False
        tr_RM.Visible = False
        tr_VP_HR.Visible = False
        panafteresc1.Visible = False
        panPOA.Visible = False
        panrecwiz.Visible = False
        panwiz3.Visible = False
        panbrk.Visible = False
        'panwiz1.Visible = True
        btn1Next.Visible = True
        btn2prev.Visible = False
        btn2Next.Visible = False
        btn3Prev.Visible = False
        btnApprove.Visible = False
        btnreject.Visible = False
        panld1.Visible = False
        pnlld2.Visible = False
        pnlld3.Visible = False
        lblHead.Text = "Lease Details"
    End Sub

    Protected Sub btn3Prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn3Prev.Click
        tr_HR.Visible = False
        tr_RM.Visible = False
        tr_VP_HR.Visible = False
        ' panbrk.Visible = True
        ' panwiz3.Visible = True
        If ddlpoa.SelectedItem.Value = "Yes" Then
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                panafteresc1.Visible = True
                btn2Next.Visible = True
                btn2prev.Visible = True
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                btn1Next.Visible = False
                lblHead.Text = "Lease Details"
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                txtrcramt1.Text = ""
                panafteresc1.Visible = False
                btn2Next.Visible = True
                btn2prev.Visible = True
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                btn1Next.Visible = False
                lblHead.Text = "Lease Details"
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                panrecwiz.Visible = False
                If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                    panafteresc1.Visible = True
                    txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                    btn2Next.Visible = True
                    btn2prev.Visible = True
                    btn3Prev.Visible = False
                    btnApprove.Visible = False
                    btnreject.Visible = False
                    btn1Next.Visible = False
                Else
                    panafteresc1.Visible = False
                    txtrcramt1.Text = ""
                    btn2Next.Visible = True
                    btn2prev.Visible = True
                    btn3Prev.Visible = False
                    btnApprove.Visible = False
                    btnreject.Visible = False
                    btn1Next.Visible = False
                End If
            Else
                Clear_Recovery_Details()
                panrecwiz.Visible = False
                panafteresc1.Visible = False
                btn2Next.Visible = True
                btn2prev.Visible = True
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                btn1Next.Visible = False
                lblHead.Text = "Lease Details"
            End If
            panPOA.Visible = True
            panld1.Visible = False
            pnlld2.Visible = False
            pnlld3.Visible = False
            'panwiz1.Visible = False

        ElseIf ddlpoa.SelectedItem.Value = "No" Then
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                ' txtrcramt2.Text = CDbl(txtstore3.Text) - CDbl(txtentitle.Text)
                panafteresc1.Visible = True
                panrecwiz.Visible = True
                btn2Next.Visible = True
                btn2prev.Visible = True
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                btn1Next.Visible = False
                'panwiz1.Visible = False
                lblHead.Text = " Lease Details"
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
                panrecwiz.Visible = True
                txtrcramt1.Text = ""
                panafteresc1.Visible = False
                btn2Next.Visible = True
                btn2prev.Visible = True
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                btn1Next.Visible = False
                'panwiz1.Visible = False
                lblHead.Text = "Lease Details"
            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
                panrecwiz.Visible = False
                If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                    panafteresc1.Visible = True
                    txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
                    btn2Next.Visible = True
                    btn2prev.Visible = True
                    btn3Prev.Visible = False
                    btnApprove.Visible = False
                    btnreject.Visible = False
                    btn1Next.Visible = False
                    'panwiz1.Visible = False
                    lblHead.Text = "Lease Details"
                Else
                    panafteresc1.Visible = False
                    txtrcramt1.Text = ""
                    btn2Next.Visible = False
                    btn2prev.Visible = False
                    btn3Prev.Visible = False
                    btnApprove.Visible = False
                    btnreject.Visible = False
                    btn1Next.Visible = True
                    'panwiz1.Visible = True
                    lblHead.Text = "Lease Details"
                End If


            ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
                panrecwiz.Visible = False
                panafteresc1.Visible = False
                'panwiz1.Visible = True
                btn2Next.Visible = False
                btn2prev.Visible = False
                btn3Prev.Visible = False
                btnApprove.Visible = False
                btnreject.Visible = False
                btn1Next.Visible = True
                lblHead.Text = "Lease Details"
                panPOA.Visible = False
            End If

            panld1.Visible = False
            pnlld2.Visible = False
            pnlld3.Visible = False
        End If
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            GetEntitle()
            lblMsg.Text = ""
        Else
            txtentitle.Text = 0
            lblmaxrent.Text = 0
            lblmaxsd.Text = 0
            lblMsg.Text = "Please Select City"
        End If
    End Sub
    Private Sub GetEntitle()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETENTITLE_USER")
            sp.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
            sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtentitle.Text = ds.Tables(0).Rows(0).Item("ENTITLEMENT")
                lblmaxrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                lblmaxsd.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                'txtstore1.Text = ds.Tables(0).Rows(0).Item("RECOVERY")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlleaseld_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlleaseld.SelectedIndexChanged
        If ddlleaseld.SelectedIndex > 0 Then

            If ddlleaseld.SelectedItem.Value = "1" Then
                txtpmonthrent.Text = txtInvestedArea.Text
                txtpsecdep.Text = txtpay.Text
                txtpmonthrent.ReadOnly = True
                txtpsecdep.ReadOnly = True
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
                panld1.Visible = True
                pnlld2.Visible = False
                pnlld3.Visible = False
            ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
                txtpmonthrent.Text = 0
                txtpsecdep.Text = 0
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = False
            ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
                txtpmonthrent.Text = 0
                txtpsecdep.Text = 0
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = True
            End If
        End If
    End Sub

    Protected Sub ddlpoa_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpoa.SelectedIndexChanged
        If ddlpoa.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please wait while an application is getting processed ');", True)
        End If
    End Sub

    Protected Sub ddlstate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlstate.SelectedIndexChanged
        'If ddlstate.SelectedIndex > 0 Then
        '    ddlld1city.Items.Clear()
        '    BindCity(ddlstate.SelectedItem.Value)
        'Else
        '    ddlld1city.Items.Clear()
        'End If
    End Sub
    'Private Sub BindCity(ByVal state As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LANDLORD1_CITY")
    '    sp.Command.AddParameter("@STATE", state, DbType.String)
    '    ddlld1city.DataSource = sp.GetDataSet()
    '    ddlld1city.DataTextField = "CTY_NAME"
    '    ddlld1city.DataValueField = "CTY_CODE"
    '    ddlld1city.DataBind()
    '    ddlld1city.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    'End Sub
    Protected Sub ddlld2state_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld2state.SelectedIndexChanged
        'If ddlld2state.SelectedIndex > 0 Then
        '    ddlld2city.Items.Clear()
        '    BindCity2(ddlld2state.SelectedItem.Value)
        'Else
        '    ddlld2city.Items.Clear()
        'End If
    End Sub
    'Private Sub BindCity2(ByVal state As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LANDLORD2_CITY")
    '    sp.Command.AddParameter("@STATE", state, DbType.String)
    '    ddlld2city.DataSource = sp.GetDataSet()
    '    ddlld2city.DataTextField = "CTY_NAME"
    '    ddlld2city.DataValueField = "CTY_CODE"
    '    ddlld2city.DataBind()
    '    ddlld2city.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    'End Sub

    Protected Sub ddlld3state_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlld3state.SelectedIndexChanged
        'If ddlld3state.SelectedIndex > 0 Then
        '    ddlld3city.Items.Clear()
        '    BindCity3(ddlld3state.SelectedItem.Value)
        'Else
        '    ddlld3city.Items.Clear()
        'End If

    End Sub
    'Private Sub BindCity3(ByVal state As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LANDLORD3_CITY")
    '    sp.Command.AddParameter("@STATE", state, DbType.String)
    '    ddlld3city.DataSource = sp.GetDataSet()
    '    ddlld3city.DataTextField = "CTY_NAME"
    '    ddlld3city.DataValueField = "CTY_CODE"
    '    ddlld3city.DataBind()
    '    ddlld3city.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    'End Sub

    Protected Sub btnprint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprint.Click
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=49")
    End Sub
    Protected Sub txtpay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtpay.TextChanged
        If ddlleaseld.SelectedIndex > 0 Then

            If ddlleaseld.SelectedItem.Value = "1" Then
                txtpmonthrent.Text = txtInvestedArea.Text
                txtpsecdep.Text = txtpay.Text
                txtpmonthrent.ReadOnly = True
                txtpsecdep.ReadOnly = True
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
            ElseIf ddlleaseld.SelectedItem.Value = "2" Or ddlleaseld.SelectedItem.Value = "3" Then
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
                txtpmonthrent.Text = 0
                txtpsecdep.Text = 0
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
            End If

        End If
    End Sub

    Protected Sub txtInvestedArea_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvestedArea.TextChanged
        If ddlleaseld.SelectedIndex > 0 Then

            If ddlleaseld.SelectedItem.Value = "1" Then
                txtpmonthrent.Text = txtInvestedArea.Text
                txtpsecdep.Text = txtpay.Text
                txtpmonthrent.ReadOnly = True
                txtpsecdep.ReadOnly = True
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
            ElseIf ddlleaseld.SelectedItem.Value = "2" Or ddlleaseld.SelectedItem.Value = "3" Then
                txtld2rent.Text = 0
                txtld2sd.Text = 0
                txtld3rent.Text = 0
                txtld3sd.Text = 0
                txtpmonthrent.Text = 0
                txtpsecdep.Text = 0
                txtpmonthrent.ReadOnly = False
                txtpsecdep.ReadOnly = False
            End If

        End If
    End Sub

    Private Sub onetimecost()

        Dim sduty As Decimal = 0
        Dim regcharges As Decimal = 0
        Dim professionalfees As Decimal = 0
        Dim brokeragefees As Decimal = 0

        If txtsduty.Text = "" Then
            sduty = 0
        Else
            sduty = CDbl(txtsduty.Text)
        End If

        If txtregcharges.Text = "" Then
            regcharges = 0
        Else
            regcharges = CDbl(txtregcharges.Text)
        End If

        If txtpfees.Text = "" Then
            professionalfees = 0
        Else
            professionalfees = CDbl(txtpfees.Text)
        End If

        If txtbrokerage.Text = "" Then
            brokeragefees = 0
        Else
            brokeragefees = CDbl(txtbrokerage.Text)
        End If

        txtbasic.Text = sduty + regcharges + professionalfees + brokeragefees




    End Sub
    Private Sub maintenancecost()
        Dim dgbackup As Decimal = 0
        Dim furniture As Decimal = 0
        Dim office As Decimal = 0
        Dim maintenance As Decimal = 0

        If txtdg.Text = "" Then
            dgbackup = 0
        Else
            dgbackup = CDbl(txtdg.Text)
        End If

        If txtfurniture.Text = "" Then
            furniture = 0
        Else
            furniture = CDbl(txtfurniture.Text)
        End If

        If txtofcequip.Text = "" Then
            office = 0
        Else
            office = CDbl(txtofcequip.Text)
        End If

        If txtmain1.Text = "" Then
            maintenance = 0
        Else
            maintenance = CDbl(txtmain1.Text)
        End If

        txtmain.Text = dgbackup + furniture + office + maintenance
    End Sub


    Protected Sub txtOccupiedArea_TextChanged(sender As Object, e As EventArgs) Handles txtOccupiedArea.TextChanged
        If txtOccupiedArea.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please Enter Area in SQFT!');", True)
            Exit Sub
        ElseIf (IsNumeric(txtOccupiedArea.Text)) Then
            bindpropertytax()
            txtmain1.Text = maintper * CDbl(txtOccupiedArea.Text)
            txtservicetax.Text = servper * CDbl(txtOccupiedArea.Text)
        Else
            lblMsg.Text = "Please enter the valid data in Builtup area"
            lblMsg.Visible = True
        End If
    End Sub
    Private Sub bindpropertytax()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROEPRTY_TAX")
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            maintper = ds.Tables(0).Rows(0).Item("MAINTENANCE_CHARGE")
            SERVPER = ds.Tables(0).Rows(0).Item("SERVICE_TAX")
        End If
    End Sub
    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmVPHRLeaseRenewal.aspx")
    End Sub
End Class
