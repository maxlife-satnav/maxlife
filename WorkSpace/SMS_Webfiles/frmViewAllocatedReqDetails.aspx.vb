'***********************
' 5   - Requsted
' 6   - Allocated
' 166 - Partially Allocated
' 8   - Cancelled  
'***********************

Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Data.SqlClient
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports System.Net.Mail
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_frmViewAllocatedReqDetails
    Inherits System.Web.UI.Page

    Dim objclsViewCancelChange As clsViewCancelChange
    Dim objpgsql As New cls_OLEDB_postgresSQL
    Dim verBll As New VerticalBLL()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim obj As clsMasters
    Dim REQID As String
    Dim RIDDS As String
    Dim dtVerReqDetails As DataTable
    Dim drReader As SqlDataReader

    Dim strReqID As String = String.Empty
    Dim strMonth As String = String.Empty
    Dim strSatusID As String = String.Empty
    Dim intMonth As Integer = 0
    Dim intYear As Integer = 0
    Dim i As Integer = 0
    Dim strRedirect As String = String.Empty


    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            Try
                obj = New clsMasters()
                GetFormVariables()
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Allocated Vertical Req", "Load", ex)
            End Try


        End If
    End Sub

    Protected Sub GetFormVariables()
        strReqID = Request.QueryString("ReqID").ToString().Trim()
        Dim strMonth As String = CType(Request.QueryString("Month").ToString().Trim(), String)
        strSatusID = Request.QueryString("StaID").ToString().Trim()
        Dim dt1 As DateTime = Convert.ToDateTime(strMonth)
        Dim strYear As String = CType(Request.QueryString("Year").ToString().Trim(), String)
        Dim dtYear As DateTime = Convert.ToDateTime(strYear)
        Dim dr1 As SqlDataReader
        lblVerReqID.Text = strReqID
        loadlocation()
        loaddepartment()
        LoadCity()
        loadtowers()

        obj = New clsMasters
        Dim dt As New DataTable

        dt.Columns.Add("sno")
        For i = 0 To 0
            Dim dr As DataRow = dt.NewRow
            dr(0) = i + 1
            dt.Rows.Add(dr)
        Next
        gvEnter.DataSource = dt
        gvEnter.DataBind()
        Fillyears()
        gvEnter.Visible = True
        RIDDS = obj.RIDGENARATION("VerticalReq")
        Dim sp1 As SqlParameter = New SqlParameter("@VerReqID", SqlDbType.NVarChar, 50)
        sp1.Value = strReqID
        Dim sp2 As SqlParameter = New SqlParameter("@mon", SqlDbType.DateTime)
        sp2.Value = dt1.ToShortDateString()
        Dim sp3 As SqlParameter = New SqlParameter("@yer", SqlDbType.DateTime)
        sp3.Value = dtYear.ToShortDateString()

        strSQL = "select SMS_REM from " & Session("TENANT") & "." & "SMS_VERTICAL_ALLOCATION where SMS_ALLOC_REQ = '" & strReqID & "'"
        drReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        While (drReader.Read())
            Label2.Text = drReader.Item("SMS_REM").ToString().Trim()
        End While
        dtVerReqDetails = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_get_BUREQDetails", sp1, sp2, sp3)
        ViewState("dtVerReqDetails") = dtVerReqDetails
        ddlSelectLocation.SelectedIndex = ddlSelectLocation.Items.IndexOf(ddlSelectLocation.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_LOC_ID").ToString().Trim()))
        ddlSelectLocation.Enabled = False
        ddlCity.Enabled = False
        ddlVertical.Enabled = False
        ddlTower.Enabled = False
        ddlFloor.Enabled = False
        ddlWing.Enabled = False

        ddlVertical.SelectedIndex = ddlVertical.Items.IndexOf(ddlVertical.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_VERTICAL").ToString().Trim()))

        ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_CTY_ID").ToString().Trim()))
        ddlTower.SelectedIndex = ddlTower.Items.IndexOf(ddlTower.Items.FindByValue(dtVerReqDetails.Rows(0)("TWR_CODE").ToString().Trim()))

        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, dtVerReqDetails.Rows(0)("SVR_LOC_ID").ToString().Trim())
        ddlFloor.DataSource = verDTO
        ddlFloor.DataTextField = "Floorname"
        ddlFloor.DataValueField = "Floorcode"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "-- Select --")

        ddlFloor.SelectedIndex = ddlFloor.Items.IndexOf(ddlFloor.Items.FindByValue(dtVerReqDetails.Rows(0)("FLR_CODE").ToString().Trim()))

        'Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforWings(ddlFloor.SelectedValue, ddlTower.SelectedValue.ToString().Trim(), dtVerReqDetails.Rows(0)("SVR_LOC_ID").ToString().Trim())
        ddlWing.DataSource = verDTO
        ddlWing.DataTextField = "WingName"
        ddlWing.DataValueField = "WingCode"
        ddlWing.DataBind()
        ddlWing.Items.Insert(0, "-- Select --")
        ddlWing.SelectedIndex = ddlWing.Items.IndexOf(ddlWing.Items.FindByValue(dtVerReqDetails.Rows(0)("WNG_CODE").ToString().Trim()))


        Label1.Text = dtVerReqDetails.Rows(0)("SVR_REM").ToString().Trim()
        Dim txtMonth As TextBox = CType(gvEnter.Rows(0).FindControl("txtMonth"), TextBox)
        Dim txtYear As TextBox = CType(gvEnter.Rows(0).FindControl("txtYear"), TextBox)
        Dim txtWorkstations As TextBox = CType(gvEnter.Rows(0).FindControl("txtWorkstations"), TextBox)
        Dim txtHalfcabinsrequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtHalfcabinsrequired"), TextBox)
        Dim txtFullCabinsRequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtFullCabinsRequired"), TextBox)
        Dim txtWorkstationsA As TextBox = CType(gvEnter.Rows(0).FindControl("txtWorkstationsA"), TextBox)
        Dim txtHalfcabinsrequiredA As TextBox = CType(gvEnter.Rows(0).FindControl("txtHalfcabinsrequiredA"), TextBox)
        Dim txtFullCabinsRequiredA As TextBox = CType(gvEnter.Rows(0).FindControl("txtFullCabinsRequiredA"), TextBox)
        txtMonth.Text = Convert.ToDateTime(dtVerReqDetails.Rows(0)("SVD_FROM").ToString().Trim()).ToShortDateString()
        txtYear.Text = Convert.ToDateTime(dtVerReqDetails.Rows(0)("SVD_TO").ToString().Trim()).ToShortDateString()
        txtWorkstations.Text = dtVerReqDetails.Rows(0)("SVD_WSTNO").ToString().Trim()
        txtHalfcabinsrequired.Text = dtVerReqDetails.Rows(0)("SVD_HCNO").ToString().Trim()
        txtFullCabinsRequired.Text = dtVerReqDetails.Rows(0)("SVD_FCNO").ToString().Trim()
        txtWorkstationsA.Text = dtVerReqDetails.Rows(0)("SMS_WST_ALLOC").ToString().Trim()
        txtHalfcabinsrequiredA.Text = dtVerReqDetails.Rows(0)("SMS_CUB_ALLOC").ToString().Trim()
        txtFullCabinsRequiredA.Text = dtVerReqDetails.Rows(0)("SMS_CAB_ALLOC").ToString().Trim()
        lblReqStatus.Text = dtVerReqDetails.Rows(0)("status").ToString().Trim()
        txtLabSpace.Text = dtVerReqDetails.Rows(0)("SVR_LABSPACE").ToString().Trim()
        Dim spReqID As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
        Dim spType As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        Dim spStatusID As New SqlParameter("@VC_STAID", SqlDbType.NVarChar, 10)
        spReqID.Value = strReqID
        spType.Value = "WORK STATION"
        spStatusID.Value = 0
        Dim dtSpace As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_ALLOCATED_SPACES", spReqID, spType, spStatusID)
        gvWorkStations.DataSource = dtSpace
        gvWorkStations.DataBind()
        spType.Value = "HALF CABIN"
        dtSpace = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_ALLOCATED_SPACES", spReqID, spType, spStatusID)
        gvHalfCabins.DataSource = dtSpace
        gvHalfCabins.DataBind()
        spType.Value = "FULL CABIN"
        dtSpace = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_ALLOCATED_SPACES", spReqID, spType, spStatusID)
        gvFullCabins.DataSource = dtSpace
        gvFullCabins.DataBind()
        If gvWorkStations.Rows.Count = 0 And gvHalfCabins.Rows.Count = 0 And gvFullCabins.Rows.Count = 0 Then
            btnUpdate.Enabled = False
            btnCancel.Enabled = False
        End If
        If gvWorkStations.Rows.Count = 0 Then
            trWC.Visible = False
        End If
        If gvFullCabins.Rows.Count = 0 Then
            trFC.Visible = False
        End If
        If gvHalfCabins.Rows.Count = 0 Then
            trHC.Visible = False
        End If
        If strSatusID = 6 Then
            btnUpdate.Enabled = False
            btnCancel.Enabled = False
            txtRemarks.Enabled = False
            txtRemarks.Text = Label1.Text
        ElseIf strSatusID = 167 Then
            btnUpdate.Enabled = True
            btnCancel.Enabled = True
            txtRemarks.Text = "NA"
        End If
    End Sub
    Public Sub LoadCity()
        obj.BindVerticalCity(ddlCity)
    End Sub
    Public Sub loadtowers()
        obj.BindTower(ddlTower)
    End Sub
    Public Sub loadlocation()
        obj.Bindlocation(ddlSelectLocation)
        'strSQL = "select lcm_code,lcm_name from " & Session("TENANT") & "."  & "location where lcm_sta_id=1 order by lcm_name"
        'BindCombo(strSQL, ddlSelectLocation, "LCM_NAME", "LCM_CODE")
    End Sub
    Public Sub bindfloor(ByVal towerid)
        Dim sp1 As New SqlParameter("", SqlDbType.NVarChar, 50)
        sp1.Value = towerid
        Dim sp2 As New SqlParameter("", SqlDbType.NVarChar, 50)
        sp1.Value = towerid
        Dim ds As New DataSet


    End Sub
    Public Sub loaddepartment()
        'obj.BindCostCenter(ddlVertical)
        '   obj.BindVertical(ddlVertical)
        'strSQL = "select VER_code,VER_NAME FROM " & Session("TENANT") & "."  & "VERTICAL WHERE VER_STA_ID=1 ORDER BY VER_NAME"
        'BindCombo(strSQL, ddlVertical, "VER_NAME", "VER_code")



        Dim UID As String = ""
        UID = Session("uid")

        Dim param(0) As SqlParameter

        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = UID

        ObjSubSonic.Binddropdown(ddlVertical, "GET_COSTCENTER", "COST_CENTER_NAME", "COST_CENTER_CODE")
        ', param)




    End Sub
    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function
    Private Sub Fillyears()
        For i As Integer = 0 To gvEnter.Rows.Count - 1
            Dim ddlYear As DropDownList = CType(gvEnter.Rows(i).FindControl("ddlYear"), DropDownList)
            For iYear As Integer = getoffsetdatetime(DateTime.Now).Year() To getoffsetdatetime(DateTime.Now).Year + 3
                ddlYear.Items.Add(iYear.ToString())
            Next
        Next
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            strRedirect = "frmViewAllocatedRequisition.aspx"
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while redirecting", "Space Vertical Requisition", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim allocatedtooltip As String = ""
            allocatedtooltip = " Allocatedto " & "/" & ddlVertical.SelectedItem.Value


            Dim spReqID As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
            Dim spType As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim spStatusID As New SqlParameter("@VC_STAID", SqlDbType.NVarChar, 10)
            Dim spSpaceID As New SqlParameter("@VC_SPC_ID", SqlDbType.NVarChar, 100)
            Dim spRemarks As New SqlParameter("@VC_REMARKS", SqlDbType.NVarChar, 256)
            spReqID.Value = lblVerReqID.Text.Trim()
            spStatusID.Value = "6"
            spType.Value = "WORK STATION"
            spRemarks.Value = txtRemarks.Text.Trim()
            For i As Int16 = 0 To gvWorkStations.Rows.Count - 1

                spSpaceID.Value = gvWorkStations.Rows(i).Cells(0).Text.Trim()
                'strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION SET SSA_STA_ID = '6'  WHERE SSA_SRNREQ_ID = '" & lblVerReqID.Text.Trim() & "' AND SSA_SPC_TYPE = 'WORK STATION'  and SSA_SPC_ID='" & gvWorkStations.Rows(i).Cells(0).Text.Trim() & "' "
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ALLOCATED_SPACES", spReqID, spType, spStatusID, spSpaceID, spRemarks)
                UpdateSpacestatus(gvWorkStations.Rows(i).Cells(0).Text.Trim(), 4, allocatedtooltip)

            Next
            spType.Value = "HALF CABIN"
            For i As Int16 = 0 To gvHalfCabins.Rows.Count - 1

                spSpaceID.Value = gvHalfCabins.Rows(i).Cells(0).Text.Trim()
                ' strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION SET SSA_STA_ID = '6' WHERE SSA_SRNREQ_ID = '" & lblVerReqID.Text.Trim() & "' AND SSA_SPC_TYPE = 'HALF CABIN' and SSA_SPC_ID='" & gvHalfCabins.Rows(i).Cells(0).Text.Trim() & "' "
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ALLOCATED_SPACES", spReqID, spType, spStatusID, spSpaceID, spRemarks)
                UpdateSpacestatus(gvWorkStations.Rows(i).Cells(0).Text.Trim(), 4, allocatedtooltip)
            Next
            spType.Value = "FULL CABIN"
            For i As Int16 = 0 To gvFullCabins.Rows.Count - 1

                spSpaceID.Value = gvFullCabins.Rows(i).Cells(0).Text.Trim()
                ' strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION SET SSA_STA_ID = '6' WHERE SSA_SRNREQ_ID = '" & lblVerReqID.Text.Trim() & "' AND SSA_SPC_TYPE = 'FULL CABIN' and SSA_SPC_ID='" & gvFullCabins.Rows(i).Cells(0).Text.Trim() & "' "
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ALLOCATED_SPACES", spReqID, spType, spStatusID, spSpaceID, spRemarks)
                UpdateSpacestatus(gvWorkStations.Rows(i).Cells(0).Text.Trim(), 4, allocatedtooltip)
            Next
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_VER_ALLOC_MAIL")
            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            sp.Command.AddParameter("@REQID", lblVerReqID.Text.Trim(), DbType.String)
            sp.Command.AddParameter("@MAILSTATUS", 5, DbType.Int32)
            sp.ExecuteScalar()
            'sendMail(lblVerReqID.Text.Trim(), "1", ddlSelectLocation.SelectedItem.Text.Trim(), ddlVertical.SelectedItem.Text.Trim())
            strRedirect = "finalpage.aspx?sta=167&reqid=" & lblVerReqID.Text.Trim()
        Catch ex As Exception
            ' Throw New Amantra.Exception.DataException("This error has been occured while Updating data", "Space Vertical Requisition", "Load", ex)
            Throw (ex)

        End Try
        If (strRedirect <> "") Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Dim spReqID As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
            Dim spType As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim spStatusID As New SqlParameter("@VC_STAID", SqlDbType.NVarChar, 10)
            Dim spSpaceID As New SqlParameter("@VC_SPC_ID", SqlDbType.NVarChar, 100)
            Dim spRemarks As New SqlParameter("@VC_REMARKS", SqlDbType.NVarChar, 256)
            spReqID.Value = lblVerReqID.Text.Trim()
            spStatusID.Value = "8"
            spType.Value = "WORK STATION"
            spRemarks.Value = txtRemarks.Text.Trim()
            For i As Int16 = 0 To gvWorkStations.Rows.Count - 1
                ' strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION SET SSA_STA_ID = '166' WHERE SSA_SRNREQ_ID = '" & lblVerReqID.Text.Trim() & "' AND SSA_SPC_TYPE = 'WORK STATION' and SSA_SPC_ID='" & gvWorkStations.Rows(i).Cells(0).Text.Trim() & "' "
                spSpaceID.Value = gvWorkStations.Rows(i).Cells(0).Text.Trim()
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ALLOCATED_SPACES", spReqID, spType, spStatusID, spSpaceID, spRemarks)
            Next

            spType.Value = "HALF CABIN"
            For i As Int16 = 0 To gvHalfCabins.Rows.Count - 1
                spSpaceID.Value = gvHalfCabins.Rows(i).Cells(0).Text.Trim()
                ' strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION SET SSA_STA_ID = '6' WHERE SSA_SRNREQ_ID = '" & lblVerReqID.Text.Trim() & "' AND SSA_SPC_TYPE = 'HALF CABIN' and SSA_SPC_ID='" & gvHalfCabins.Rows(i).Cells(0).Text.Trim() & "' "
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ALLOCATED_SPACES", spReqID, spType, spStatusID, spSpaceID, spRemarks)

            Next
            spType.Value = "FULL CABIN"
            For i As Int16 = 0 To gvFullCabins.Rows.Count - 1
                spSpaceID.Value = gvFullCabins.Rows(i).Cells(0).Text.Trim()
                ' strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION SET SSA_STA_ID = '6' WHERE SSA_SRNREQ_ID = '" & lblVerReqID.Text.Trim() & "' AND SSA_SPC_TYPE = 'FULL CABIN' and SSA_SPC_ID='" & gvFullCabins.Rows(i).Cells(0).Text.Trim() & "' "
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_ALLOCATED_SPACES", spReqID, spType, spStatusID, spSpaceID, spRemarks)
            Next
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_VER_ALLOC_MAIL")
            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            sp.Command.AddParameter("@REQID", lblVerReqID.Text.Trim(), DbType.String)
            sp.Command.AddParameter("@MAILSTATUS", 6, DbType.Int32)
            sp.ExecuteScalar()
            ' sendMail(lblVerReqID.Text.Trim(), "2", ddlSelectLocation.SelectedItem.Text.Trim(), ddlVertical.SelectedItem.Text.Trim())
            strRedirect = "finalpage.aspx?sta=168&reqid=" & lblVerReqID.Text.Trim()
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)

        End Try
        If (strRedirect <> "") Then
            Response.Redirect(strRedirect)
        End If
    End Sub

   
End Class
