﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="conference_Approval.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_conference_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">  
             <hr align="center" width="60%" />Conference Approval</asp:Label>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                    width="9" />
            </td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp; <strong>Conference Approval</strong>
            </td>
            <td style="width: 16px">
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                    width="16" />
            </td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;
            </td>
            <td align="left">
                <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="clsMessage"
                    ForeColor="" />
                <br />
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                <br />
                <asp:Panel ID="panl" runat="server" Width="100%">
                    <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="False" Width="100%"
                        AllowPaging="True" PageSize="25">
                        <Columns>
                            <asp:HyperLinkField DataNavigateUrlFields="REQ_ID"
                                DataNavigateUrlFormatString="conference_Approvaldtls.aspx?id={0}"
                                DataTextField="REQ_ID" />


                            <asp:BoundField HeaderText="Space Name" DataField="SPC_NAME" />
                            <asp:BoundField HeaderText="Date" DataField="FROM_DATE" />
                            <asp:BoundField HeaderText="From Time" DataField="FROM_TIME" />
                            <asp:BoundField HeaderText="To Time" DataField="TO_TIME" />
                        </Columns>
                       
                    </asp:GridView>
                </asp:Panel>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 16px;
                height: 100%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" />
            </td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" />
            </td>
            <td style="height: 17px; width: 16px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" />
            </td>
        </tr>
    </table>
</asp:Content>
