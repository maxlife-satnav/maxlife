<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReserveBlockedSeatsRequisitions.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_ReserveBlockedSeatsRequisitions" Title="Reserve Blocked Seats" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Reserve Blocked Seats Requisitions
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:TextBox ID="txttoday" runat="server" Visible="False" CssClass="form-control"></asp:TextBox>
                        <asp:TextBox ID="Textbox1" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>

                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="dvVerticalReq" runat="server" CellPadding="3" 
                                    AutoGenerateColumns="false" OnRowCreated="dvVerticalReq_OnRowCreated" AllowPaging="True"
                                    OnPageIndexChanging="dvVerticalReq_PageIndexChanging"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Requisition ID">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" Text='<% #Bind("Requisition_ID")%>' ID="lnkVerReq"
                                                    OnClick="lnk_view"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requested by">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("Requested_by")%>' ID="lblReqBy"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Vertical">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("Vertical")%>' ID="lblVertical"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("location")%>' ID="lbllocation"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requested Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("Requested_Date")%>' ID="lblReqDate"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Work Stations Required">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("Work_Stations_Required")%>' ID="lblWSR"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Half Cabins Required">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("Half_Cabnis_Required")%>' ID="lblHCR"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Cabins Required">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("Full_Cabins_Required")%>' ID="lblFCR"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lab Space(Sqft)">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("SVR_LABSPACE")%>' ID="lbllabspace"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("Status")%>' ID="lblStatus"></asp:Label>
                                                <asp:Label runat="server" Text='<% #Bind("StatusID")%>' ID="lblStatusID" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("MON")%>' ID="lblMon"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<% #Bind("YER")%>' ID="lblYear"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <asp:Label ID="lblSpace" runat="server" class="col-md-12 control-label" Text="" Font-Bold="true"></asp:Label>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
