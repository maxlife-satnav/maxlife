Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports ConsolidateRpt
Imports clsSubSonicCommonFunctions
Imports System.io

Partial Class Reports_exportexcel
    Inherits System.Web.UI.Page
    Dim objSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ExportToExcelEmployeeAllocationReport()
        End If
    End Sub


    Private Sub ExportToExcelEmployeeAllocationReport()
        'Dim param(6) As SqlParameter
        'param(0) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        'param(0).Value = Request.QueryString("Mcity")
        'param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        'param(1).Value = Request.QueryString("MLCM_CODE")
        'param(2) = New SqlParameter("@PageIndex", SqlDbType.NVarChar, 200)
        'param(2).Value = 1
        'param(3) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        'param(3).Value = "City"
        'param(4) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        'param(4).Value = "asc"
        'param(5) = New SqlParameter("@NumberOfRows", SqlDbType.NVarChar, 200)
        'param(5).Value = 90000
        'param(6) = New SqlParameter("@TotalRecords", SqlDbType.NVarChar, 200)
        'param(6).Value = 0
        Dim strCity As String = Request("Mcity")


        If strCity = "--All--" Then
            strCity = ""
        End If
        Dim strLCM_CODE As String = Request("MLCM_CODE")


        If strLCM_CODE = "--All--" Then
            strLCM_CODE = ""
        End If

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(0).Value = strCity
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = strLCM_CODE
        param(2) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(2).Value = 1
        param(3) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(3).Value = "City"
        param(4) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(4).Value = "ASC"
        param(5) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(5).Value = 90000
        param(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(6).Value = 0
        param(6).Direction = ParameterDirection.Output
        Dim dsEmployeeAllocationReport As New DataSet
        dsEmployeeAllocationReport = objsubsonic.GetSubSonicDataSet("GETCONSOLIDATERPT", param)
        'Dim dsEmployeeAllocationReport As New DataSet
        'dsEmployeeAllocationReport = objsubsonic.GetSubSonicDataSet("GETCONSOLIDATERPT_old", param)

        dsEmployeeAllocationReport.Tables(0).Columns(1).ColumnName = "Project"
        dsEmployeeAllocationReport.Tables(0).Columns(2).ColumnName = "City"
        dsEmployeeAllocationReport.Tables(0).Columns(3).ColumnName = "Tower"
        dsEmployeeAllocationReport.Tables(0).Columns(4).ColumnName = "Location Code"
        dsEmployeeAllocationReport.Tables(0).Columns(5).ColumnName = "Floor"
        dsEmployeeAllocationReport.Tables(0).Columns(6).ColumnName = "Wing"
        dsEmployeeAllocationReport.Tables(0).Columns(7).ColumnName = "Vertical"
        dsEmployeeAllocationReport.Tables(0).Columns(8).ColumnName = "WST"
        dsEmployeeAllocationReport.Tables(0).Columns(9).ColumnName = "FCB"
        dsEmployeeAllocationReport.Tables(0).Columns(10).ColumnName = "HCB"
        dsEmployeeAllocationReport.Tables(0).Columns(11).ColumnName = "Total"



        Dim gv As New GridView
        gv.DataSource = dsEmployeeAllocationReport
        gv.DataBind()
        Export("ConsolidateSpaceReport.xls", gv)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

End Class
