Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports ConsolidateRpt
Imports clsSubSonicCommonFunctions
Imports System.io
Partial Class WorkSpace_SMS_Webfiles_Exports_LocationWiseRPTExport
    Inherits System.Web.UI.Page
    Dim objSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ExportToExcelEmployeeAllocationReport()
        End If
    End Sub
    Private Sub ExportToExcelEmployeeAllocationReport()
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("Mcity")
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("MLCM_CODE")
        param(2) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(2).Value = 1
        param(3) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(3).Value = "EMPLOYEE_ID"
        param(4) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(4).Value = "ASC"
        param(5) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(5).Value = 0
        param(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(6).Value = 0

        'param(6).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_ALLAMANTRA_USERBYLOC_CODE_EXPORT", param)

        'ds.Tables(0).Columns(1).ColumnName = "EMPLOYEE_ID"
        'ds.Tables(0).Columns(2).ColumnName = "EMPLOYEE_NAME"
        'ds.Tables(0).Columns(3).ColumnName = "SPACEID"
        'ds.Tables(0).Columns(4).ColumnName = "DEPARTMENT"
      



        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("EmployeeOccupancyReport.xls", gv)
    End Sub


   

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

End Class
