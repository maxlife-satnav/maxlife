Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repTowerwiseRequisition
    Inherits System.Web.UI.Page
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        If Not Page.IsPostBack Then

            obj.bindLocation(ddlBuildings)
            ddlBuildings.Items(0).Text = "--All Locations--"
            ddlTower.Items.Insert(0, "--All Towers--")
            binddata_requistions()
            ddlTower.SelectedIndex = 0
        End If
    End Sub
    
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        If ddlTower.SelectedIndex = 0 Then

        End If
        binddata_requistions()
    End Sub

    Public Sub binddata_requistions()

        Dim rds As New ReportDataSource()
        rds.Name = "TowerRequisitionDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/TowerRequisitionReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim MTower As String = ""
        Dim MLocation As String = ""
        If ddlBuildings.SelectedValue = "--All Locations--" Then
            MLocation = " "
        Else
            MLocation = ddlBuildings.SelectedValue
        End If
        If ddlTower.SelectedValue = "--All Towers--" Then
            MTower = " "
        Else
            MTower = ddlTower.SelectedValue
        End If

        
        Dim sp1 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = MTower
        Dim sp2 As New SqlParameter("@vc_building_id", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = MLocation

        Dim sp3 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
        sp3.Value = "srn_req_id"

        Dim sp4 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
        sp4.Value = "ASC"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Requests_TowerWise", sp1, sp2, sp3, sp4)
        rds.Value = dt
        
    End Sub
    
    Protected Sub ddlBuildings_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBuildings.SelectedIndexChanged
        ReportViewer1.Visible = False
        obj.bindTower_Locationwise(ddlTower, ddlBuildings.SelectedValue.ToString)
        ddlTower.Items(0).Text = "--All Towers--"
        If ddlTower.Items.Count = 2 Then
            ddlTower.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class
