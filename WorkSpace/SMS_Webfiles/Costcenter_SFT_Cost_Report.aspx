﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Costcenter_SFT_Cost_Report.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Costcenter_SFT_Cost_Report" Title="Costcenter Sft cost Report" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <%-- <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />--%>
    <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .c3-grid line {
            stroke: none;
        }

        .c3-axis-x > path.domain, .tick > line[x2="-6"] {
            visibility: hidden;
        }
    </style>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend><%= Session("Child")%> Wise Seat Cost Report  
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <asp:Label ID="Label2" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <div id="Div1" class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select <%= Session("Child")%> </label>
                                        <asp:RequiredFieldValidator ID="rfvloc" runat="server" ValidationGroup="Val1" InitialValue="0" Display="None" ControlToValidate="ddlproject">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlproject" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <asp:Button ID="btnsubmit" runat="server" Text="View Report" CausesValidation="true" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"></asp:Button>
                                        <asp:Button ID="btnexcel" OnClick="btnexcel_Click" Visible="false" runat="server" Text="Export to Excel" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="col-sm-12">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small"
                                    data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="row tabularC" id="Tabular">
                            <div class="col-md-12">
                                <asp:GridView ID="gvEmp" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped"
                                    AutoGenerateColumns="False" CellPadding="3" AllowSorting="True" AllowPaging="True" EmptyDataText="No Records Found">
                                    <Columns>
                                        <asp:BoundField DataField="LCM_NAME" HeaderText="Location"></asp:BoundField>
                                        <asp:BoundField DataField="FLR_NAME" HeaderText="Floor"></asp:BoundField>
                                        <asp:BoundField DataField="VERTICAL" HeaderText="Verticals"></asp:BoundField>
                                        <asp:BoundField DataField="costcenter" HeaderText="Cost Centers"></asp:BoundField>
                                        <asp:BoundField DataField="seatcount" HeaderText="Allocated Count"></asp:BoundField>
                                        <asp:BoundField DataField="seatpercost" HeaderText="Per Seat Cost" DataFormatString="{0:c2}"></asp:BoundField>
                                        <asp:BoundField DataField="totalseatcost" HeaderText="Total Seat Cost" DataFormatString="{0:c2}"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div id="Graphicaldiv">
                            <div id="CostGraph">&nbsp</div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script src="../../Dashboard/C3/d3.v3.min.js"></script>
<script src="../../Dashboard/C3/c3.min.js"></script>
<link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
<script src="../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
<script src="../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
<script src="../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#Graphicaldiv").fadeIn();
        $("#Tabular").fadeOut();
        $("#Div1").hide();
        var chart;
        funfillCostcenterCost();

        function funfillCostcenterCost() {

            chart = c3.generate({
                data: {
                    x: 'x',
                    columns: [
                        //['x', "a", "b", "c", "d", "e", "f"],
                        //['y', 130, 100, 140, 200, 150, 50],
                        //['z', 130, 200, 120, 200, 150, 50]
                    ],
                    type: 'bar',
                    empty: { label: { text: "Sorry, No Data Found" } },
                },
                legend: {
                    position: 'top'
                },
                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            rotate: -40,
                            multiline: false
                        },
                        height: 130
                    }

                },
                width:
                {
                    ratio: 0.5 // this makes bar width 50% of length between ticks
                }
                //or
                //width: 100 // this makes bar width 100px
            });


            $.ajax({
                url: '../../api/CostcenterSeatCostAPI/GetCostcenterChart/' + $("#ddlproject").val(),
                type: 'POST',
                success: function (result) {
                    chart.load({
                        columns: result
                    });
                }
            });
            setTimeout(function () {
                $("#CostGraph").append(chart.element);
            }, 700);

        }

        $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                $("#Tabular").fadeOut(function () {
                    $("#Graphicaldiv").fadeIn();
                    $("#Div1").hide();
                    if (chart) {
                        chart.flush();
                    }
                });
            }
            else {
                $("#Graphicaldiv").fadeOut(function () {
                    $("#Tabular").fadeIn();
                    $("#Div1").show();
                   
                });
            }
        });
    });
</script>
