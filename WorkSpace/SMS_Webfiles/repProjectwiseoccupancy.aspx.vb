﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repProjectwiseoccupancy
    Inherits System.Web.UI.Page

    Dim obj As New clsReports
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""
        Label1.Text = Session("Child")

        If Not Page.IsPostBack Then

            obj.bindVertical(ddlverticals)
            lblSelParent.Text = Session("Parent")
            'RequiredFieldValidator2.ErrorMessage = "Please Select " + lblSelParent.Text
            lblSelChild1.Text = Session("Child")
            ddlverticals.Items(0).Text = "--All " & Session("Parent") & "(s)--"
            ddlproject.Items.Insert(0, "--All " & Session("Child") & "(s)--")
            ddlverticals.SelectedIndex = 0
            'ddlverticals.Items.RemoveAt(1)
            binddata_floorwise()
            'lbl1.Text = lbl1.Text + " " + Session("Parent")
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click


        binddata_floorwise()

    End Sub
    Public Sub binddata_floorwise()


        Dim rds As New ReportDataSource()
        rds.Name = "ProjectOccupancyDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/ProjectOccupancyReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim Mver As String = ""
        Dim Mprj As String = ""

        If ddlverticals.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            Mver = ""
        Else
            Mver = ddlverticals.SelectedValue
        End If

        If ddlproject.SelectedValue = "--All " & Session("Child") & "(s)--" Then
            Mprj = ""
        Else
            Mprj = ddlproject.SelectedValue
        End If
        Dim sp1 As New SqlParameter("@VC_VERTICALNAME", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = Mver
        Dim sp2 As New SqlParameter("@VC_PROJECTNAME", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = Mprj

        Dim sp3 As New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = "SSA.SSA_SPC_ID"

        Dim sp4 As New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp4.Value = "Asc"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_ProjectWise", sp1, sp2, sp3, sp4)

        rds.Value = dt

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMPLOYEE_PROJECT_COUNT")
        sp.Command.AddParameter("@VER_CODE", Mver, DbType.String)
        sp.Command.AddParameter("@PRJ_CODE", Mprj, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltot.Visible = True
            lbltot.Text = "Total Employees : " & ds.Tables(0).Rows(0).Item("EMPLOYEE_ID")
            lbltot.ForeColor = Drawing.Color.Black
            lbltot.Font.Bold = True

        End If
        lbltot.Visible = True
        t2.Visible = True

    End Sub

   
    Protected Sub ddlverticals_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlverticals.SelectedIndexChanged
        lbl1.Visible = False
        ReportViewer1.Visible = False
        t2.Visible = False
        If ddlverticals.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            lbl1.Visible = False
            'Else
            '    lbl1.Visible = True
        End If


        Dim sp1 As New SqlParameter("@VC_VERTICALNAME", SqlDbType.NVarChar, 50)
        If ddlverticals.SelectedIndex = 1 Then
            sp1.Value = ""
        Else
            sp1.Value = ddlverticals.SelectedValue
        End If

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_VERTICALWISE_PROJECT", sp1)
        BindCombo(ds, ddlproject, "PRJ_NAME", "PRJ_CODE")
        ddlproject.Items(0).Text = "--All " & Session("Child") & "(s)--"

        ddlproject.SelectedIndex = 0
    End Sub
    
    
End Class
