﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Dashboard" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

</head>
<body>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/Scripts/highcharts.js"></script>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Dashboard 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div id="Table1">


                            <div id="Table2" runat="server">
                                <div class="row">
                                    <div class="col-md-4 text-right">
                                        <div class="form-group">
                                            <asp:LinkButton ID="lnkbtntoggle" runat="server" Text="Toggle All Reports"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <div class="form-group">
                                            <asp:LinkButton ID="lnkbtnshow" runat="server" Text="Show All Reports"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <div class="form-group">
                                            <asp:LinkButton ID="lnkbtndashboard" runat="server" Text="Dashboard"></asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div id="verreqs" runat="server">
                                <fieldset>
                                    <legend>Vertical Requisitions</legend>
                                </fieldset>
                                <div class="row">
                                    <div class="col-md-11 pull-center">
                                        <div class="form-group">
                                            <asp:GridView ID="dvVerticalReq" runat="server" AutoGenerateColumns="false" EmptyDataText="No Vertical Requisition Found."
                                                CssClass ="table table-condensed table-bordered table-hover table-striped" OnRowCreated="dvVerticalReq_OnRowCreated" 
                                                OnSelectedIndexChanged="CustomersGridView_SelectedIndexChanged" OnSelectedIndexChanging="CustomersGridView_SelectedIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Requisition ID">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkVerReq" runat="server" OnClick="lnk_view" Text='<% #Bind("Requisition_ID")%>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Requested by">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReqBy" runat="server" Text='<% #Bind("Requested_by")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Requested Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReqDate" runat="server" Text='<% #Bind("Requested_Date")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Work Stations Required">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblWSR" runat="server" Text='<% #Bind("Work_Stations_Required")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Half Cabins Required">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblHCR" runat="server" Text='<% #Bind("Half_Cabnis_Required")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Full Cabins Required">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFCR" runat="server" Text='<% #Bind("Full_Cabins_Required")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text='<% #Bind("Status")%>'></asp:Label>
                                                            <asp:Label ID="lblStatusID" runat="server" Text='<% #Bind("StatusID")%>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="From Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMon" runat="server" Text='<% #Bind("MON")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="To Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblYear" runat="server" Text='<% #Bind("YER")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <asp:Label ID="lblVertical" runat="server" Font-Bold="true" Text=""></asp:Label>

                            </div>
                            <div id="spcreqs" runat="server">
                                <fieldset>
                                    <legend>Space Requisitions</legend>
                                </fieldset>
                                <div class="row">
                                    <div class="col-md-11 pull-center">
                                        <div class="form-group">
                                            <asp:GridView ID="dvSpaceRequistions" runat="server" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped" 
                                               EmptyDataText="No Space Requisition Found." OnRowCreated="dvSpaceRequistions_OnRowCreated">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Space Requisitions ID">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkSpaceReqId" runat="server" OnClick="lnkSpaceReq_click" Text='<% #Bind("Space_Requistion_ID")%>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Requested by">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRequestedby" runat="server" Text='<% #Bind("Requested_by")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Requisition Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDate" runat="server" Text='<% #Bind("Requistion_Date")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="From Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFromDate" runat="server" Text='<% #Bind("From_Date")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="To Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblToDate" runat="server" Text='<% #Bind("To_Date")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tower">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTower" runat="server" Text='<% #Bind("Tower")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Work stations">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblWS" runat="server" Text='<% #Bind("Work_stations")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Half Cabins">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblHC" runat="server" Text='<% #Bind("Half_Cabins")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Full Cabins">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFC" runat="server" Text='<% #Bind("Full_Cabins")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text='<% #Bind("Status")%>'></asp:Label>
                                                            <asp:Label ID="lblStatusID" runat="server" Text='<% #Bind("StatusID")%>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <asp:Label ID="lblSpace" runat="server" Font-Bold="true" Text=""></asp:Label>
                            </div>

                            <div id="dashboard" runat="server">
                                <div id="tab" runat="server">
                                    <div class="row">
                                        <label class="col-md-2 control-label">
                                            Select Location
                                        </label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddllocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div id="dashboard_content" runat="server">

                                        <div id="Table4" runat="server">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <fieldset>
                                                        <legend>Week Wise Allocation Data</legend>
                                                    </fieldset>
                                                    <div class="form-group">
                                                        <asp:Literal ID="Litweek" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <fieldset>
                                                        <legend>Month Wise Allocation Data</legend>
                                                    </fieldset>
                                                    <div class="form-group">
                                                        <asp:Literal ID="Litmonth" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <fieldset>
                                                    <legend>Total seats</legend>
                                                </fieldset>
                                                <div id="tabfirst" runat="server">
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lbltotavail" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lbltotws" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lbltotfc" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lbltothc" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblfirstallocated" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblfirstoccupied" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblfirstallocatedNOT" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset>
                                                    <legend>Space Area</legend>
                                                </fieldset>
                                                <div id="Table6" runat="server">
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblgrossarea" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-left">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblworkspace" runat="server" CssClass="clsLabel"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <fieldset>
                                            <legend>My Space</legend>
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-md-11 pull-center">
                                                <div class="form-group">
                                                    <asp:GridView ID="gvSpace" runat="server" AutoGenerateColumns="False" PageSize="1" EmptyDataText="No Space Found." ShowHeaderWhenEmpty="True" 
                                                        CssClass="table table-condensed table-bordered table-hover table-striped">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Employee Name" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpName" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Employee Id" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpId" runat="server" CssClass="bodyText" Text='<%#Eval("EMPLOYEE_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLoc" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_BDG_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Tower" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTwr" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_TWR_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Floor" ItemStyle-Width="3%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFlr" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_FLR_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Space Id">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSpaceId" runat="server" CssClass="bodyText" Visible="false" Text='<%#Eval("SPACE_ID")%>'></asp:Label>
                                                                    <a href="#" onclick="showPopWin('<%#Eval("SPACE_ID")%>','<%#Eval("EMPLOYEE_ID")%>')">
                                                                        <%#Eval("SPACE_ID")%></a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Space Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSpcType" runat="server" CssClass="bodyText" Text='<%#Eval("SPC_TYPE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblVertical" runat="server" CssClass="bodyText" Text='<%#Eval("VERTICAL")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpName" runat="server" CssClass="bodyText" Text='<%#Eval("COSTCENTER")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatus" runat="server" CssClass="bodyText" Text='<%#Eval("STATUS")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                        <PagerStyle CssClass="pagination-ys" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:TextBox ID="txtmode" runat="Server" Visible="false"></asp:TextBox>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <fieldset>
                                                    <legend><%= Session("Parent")%></legend>
                                                </fieldset>
                                                <div class="form-group">
                                                    <asp:Literal ID="litvertbarchart" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <fieldset>
                                                    <legend><%= Session("Child")%></legend>
                                                </fieldset>
                                                <div class="form-group">
                                                    <asp:Literal ID="litdepbarchart" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <%-- <h4 class="modal-title">View Property Details</h4>--%>
                </div>
                <div class="modal-body" id="modelcontainer">
                </div>
            </div>
        </div>
    </div>
    <script>
        function showPopWin(id, aurid) {
            $("#modelcontainer").load("SpacesViewMapSearchEmployee.aspx?id=" + id + "&aurid=" + aurid, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>
</body>
</html>
