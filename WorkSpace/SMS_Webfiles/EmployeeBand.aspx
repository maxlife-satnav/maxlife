﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align {text-align: center;}
        a:hover {cursor: pointer; }
    </style>
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BandController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Employee Band Master</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frm" class="form-horizontal well" data-valid-submit="Save()" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Code.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Band Code<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <input id="txtcode" name="Code" type="text" data-ng-model="Band.Code" data-ng-pattern="codepattern" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.Code.$invalid" style="color: red">Please enter valid code </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Name.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Band Name<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <input id="txtCName" name="Name" type="text" data-ng-model="Band.Name" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.Name.$invalid" style="color: red">Please enter valid name</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <textarea id="txtremarks" data-ng-model="Band.Remarks" class="form-control" maxlength="500"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.Status.$invalid}">
                                        <label class="col-md-4 control-label">Status<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div>
                                                <select id="ddlsta" name="Status" data-ng-model="Band.Status" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.Status.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                                    <%--<input type="button" value="Back" class='btn btn-primary' onclick="window.location = 'frmMASMasters.aspx'" />--%>
                                     <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="height: 340px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 330px;width:100%"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/aggrid/ag-grid.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
        
        //app.directive('validSubmit', ['$parse', function ($parse) {
        //    return {
        //        require: 'form',
        //        link: function (scope, element, iAttrs, form) {
        //            form.$submitted = false;
        //            var fn = $parse(iAttrs.validSubmit);
        //            element.on('submit', function (event) {
        //                scope.$apply(function () {
        //                    form.$submitted = true;
        //                    if (form.$valid) {
        //                        fn(scope, { $event: event });
        //                        form.$submitted = false;
        //                    }
        //                });
        //            });
        //        }
        //    };
        //}
        //])
        app.service("BandService", function ($http, $q, UtilityService) {
            var deferred = $q.defer();
            this.getBand = function () {
                return $http.get('../../../api/Band')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.CreateBand = function (Band) {
                deferred = $q.defer();
                return $http.post('../../../api/Band/Create', Band)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.UpdateBand = function (Band) {
                deferred = $q.defer();
                return $http.post('../../../api/Band/UpdateBandData', Band)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.GetGriddata = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Band/GetGridData')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

        });

        app.controller('BandController', function ($scope, $q, BandService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.Band = {};
            $scope.Banddata = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.Save = function () {
                progress(0, 'Loading...', true);
                if ($scope.IsInEdit) {
                    BandService.UpdateBand($scope.Band).then(function (Band) {
                        $scope.ActionStatus = 0;
                        //$scope.ShowMessage = true;
                        //$scope.Success = "Data Updated Successfully";
                        progress(0, 'Loading...', false);                     
                        showNotification('success', 8, 'bottom-right', "Data Updated Successfully");
                        var savedobj = {};
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.IsInEdit = false;                       
                        $scope.LoadData();
                        $scope.Band = {};
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.Band.Status = "1";
                    BandService.CreateBand($scope.Band).then(function (response) {
                        //$scope.ShowMessage = true;                       
                        //$scope.Success = "Data Inserted Successfully";
                        progress(0, 'Loading...', false);
                        showNotification('success', 8, 'bottom-right', "Data Inserted Successfully");
                        var savedobj = {};
                        angular.copy($scope.Band, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);                       
                        $scope.Band = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            var columnDefs = [
                { headerName: "Band Code", field: "Code", width: 190, cellClass: 'grid-align' },
                { headerName: "Band Name", field: "Name", width: 280, cellClass: 'grid-align' },
                { headerName: "Status", template: "{{ShowStatus(data.Status)}}", width: 170, cellClass: 'grid-align' },
                { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110,suppressMenu: true,suppressSorting:true }];

            $scope.LoadData = function () {
                BandService.GetGriddata().then(function (data) {
                    $scope.gridata = data;
                    //$scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });
            }

            $scope.pageSize = '10';

            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                enableCellSelection: false,
                rowData: null,
                enableFilter: true,
                enableSorting: true,
                angularCompileRows: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }

            };
            $scope.LoadData();

            $scope.EditData = function (data) {
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                angular.copy(data, $scope.Band);
            }

            $scope.EraseData = function () {
                $scope.Band = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
        });
    </script>
        <script src="../../SMViews/Utility.js"></script>

</body>
</html>
