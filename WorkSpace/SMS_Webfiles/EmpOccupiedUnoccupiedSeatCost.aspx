﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmpOccupiedUnoccupiedSeatCost.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_EmpOccupiedUnoccupiedSeatCost" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <%-- <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />--%>
    <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Occupied Unoccupied Seat Cost</legend>
                    </fieldset>
                    <form id="Form1" name="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Floor</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="col-sm-12">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small"
                                    data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />
                                <%-- data-label-text="<span class='fa fa-youtube fa-lg'></span>"--%>
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="row tabularC">
                            <div class="col-md-12">
                                <div class="row" id="tblsummary">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <asp:Button ID="btnexcel" OnClick="btnexcel_Click" runat="server" Text="Export to Excel" CssClass="btn btn-primary custom-button-color"></asp:Button>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                AllowPaging="True" PageSize="10" EmptyDataText="No Records Found."
                                                CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <PagerSettings Mode="NumericFirstLast" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Location" Visible="True">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblempid" runat="server" Text='<%#Eval("locationname")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tower" Visible="True">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLesseName" runat="server" Text='<%#Eval("Towername")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Floor" Visible="True">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbllsename" runat="server" Text='<%#Eval("Floorname")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Seats">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLSDate" runat="server" Text='<%#Eval("totalseats")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Occupied Seats">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLEDate" runat="server" Text='<%#Eval("TotalOccupiedSeats")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Unoccupied Seats">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLEDate" runat="server" Text='<%#Eval("TotalUnoccupied")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Per Seat Cost">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLcost" runat="server" Text='<%# Eval("SPC_FCLTY_COST", "{0:c2}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Occupied Cost">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsecamount" runat="server" Text='<%# Eval("TotalOccupiedCost", "{0:c2}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Unoccupied Cost">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblbrkfee" runat="server" Text='<%#Eval("TotalUnoccupiedCost", "{0:c2}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>


                                <div class="row" id="chartdiv">
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <i class="fa fa-circle-o"></i>Total Occupied - Unoccupied Seats Count
                                            </div>
                                            <div class="panel-body">
                                                <div id="SeatsCount"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <i class="fa fa-bar-chart-o fa-fw"></i>Total Occupied - Unoccupied Seats Cost
                                            </div>
                                            <div class="panel-body">
                                                <div id="SeatsCost">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

<script src="../../Dashboard/C3/d3.v3.min.js"></script>
<script src="../../Dashboard/C3/c3.min.js"></script>
<link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
<script src="../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
<script src="../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
<script src="../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
<script type="text/javascript">
    var chart;
    var chart2;
    $("#chartdiv").fadeIn();
    $("#tblsummary").fadeOut();

    funfillAllSeats();
    FunBindAmounts(); 

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#tblsummary").fadeOut(function () {
                $("#chartdiv").fadeIn();
                if (chart2)
                    chart2.flush();
                if (chart)
                    chart.flush();
            });

        }
        else {
            $("#chartdiv").fadeOut(function () {
                $("#tblsummary").fadeIn();

                
            });
        }
    });
  
    function funfillAllSeats() {        
        chart = c3.generate({
            data: {
                columns: [],
                type: 'donut',

                empty: { label: { text: "Sorry, No Data Found" } },
            },
            donut: {
                expand: false,
                label: {
                    format: function (value, ratio, id) {
                        return d3.format('')(value);
                    }
                }

            },
            axis: {
                x: {
                    position: 'outer-center',
                },
            }
        });
        var Model = { LOCATION: $("#ddlLocation").val(), FLOOR: $("#ddlFloor").val() };
        $.ajax({
            url: '../../api/EmpSeatCostAPI/GetEmpSeatCount',
            type: 'POST',
            data: Model,
            success: function (result) {
                setTimeout(function () {
                    chart.load({ columns: result });
                }, 1000);
                $("#SeatsCount").append(chart.element);
                
              
            }
        });
    
      

       
    }
   
    function FunBindAmounts() {
        var Model = { LOCATION: $("#ddlLocation").val(), FLOOR: $("#ddlFloor").val() };
        $.ajax({
            url: '../../api/EmpSeatCostAPI/GetEmpSeatCost',
            type: 'POST',
            data: Model,
            success: function (summarydata) {
                chart2 = c3.generate({
                    data: {
                        columns: [["Cost", summarydata.ChartData[0].TotalSeats, summarydata.ChartData[1].OccSeats, summarydata.ChartData[2].UnoccSeats]],
                        type: 'bar',

                        empty: { label: { text: "Sorry, No Data Found" } },
                    },
                    bar: {
                        width: {
                            ratio: 0.5
                        }
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: ["Total Seats Cost", "Occupied Seats Cost", "Unoccupied Seats Cost"],
                        },
                        y: {
                            show: true,
                            label: {
                                text: 'Amount',
                                position: 'outer-middle'
                            }
                        },

                    },
                    bindto: '#SeatsCost'

                });

            }

        });
    }   
</script>
