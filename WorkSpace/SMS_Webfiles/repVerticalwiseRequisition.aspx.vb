Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_repVerticalwiseRequisition
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""
        ' Try
        If Not Page.IsPostBack Then
            ReportViewer1.Visible = True
            BindVertical()
            binddata_requistions()
        End If
       
    End Sub
    Dim dt As DataTable
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        binddata_requistions()

    End Sub
    Public Sub binddata_requistions()

        Dim rds As New ReportDataSource()
        rds.Name = "VerticalRequisitionDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/VerticalRequisitionReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True


        Dim Mver As String = ""
        If ddlVertical.SelectedValue = "--All--" Then
            Mver = " "
        Else
            Mver = ddlVertical.SelectedValue
        End If
       
        Dim sp1 As New SqlParameter("@VC_VERTICAL_NAME", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = Mver
      
        Dim sp2 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
        sp2.Value = "SRN_REQ_ID"

        Dim sp3 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
        sp3.Value = "Asc"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_VERTICAL_REQUISITIONS", sp1, sp2, sp3)
        rds.Value = dt


    End Sub
    Public Sub BindVertical()
        objsubsonic.Binddropdown(ddlVertical, "GET_VERTICAL_REQUISITION", "COST_CENTER_NAME", "COST_CENTER_CODE")
        ddlVertical.Items(0).Text = "--All--"
        'ddlVertical.Items.RemoveAt(1)
    End Sub
    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged

        ReportViewer1.Visible = False

    End Sub
End Class
