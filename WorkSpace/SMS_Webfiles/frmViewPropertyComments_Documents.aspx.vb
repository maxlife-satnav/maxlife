Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmViewPropertyComments_Documents
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            Try
                BindPropType()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", "--Select Property Type--"))
    End Sub

    Protected Sub ddlPropertyType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPropertyType.SelectedIndexChanged
        lblgridheading.Visible = False
        gvRemarks.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTIES")
        sp.Command.AddParameter("@PROPERTYTYPE", ddlPropertyType.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        ddlProperty.DataSource = sp.GetDataSet()
        ddlProperty.DataTextField = "PM_PPT_NAME"
        ddlProperty.DataValueField = "PM_PPT_SNO"
        ddlProperty.DataBind()
        ddlProperty.Items.Insert(0, "--Select--")

        gvRemarks.DataSource = Nothing
        gvRemarks.DataBind()
    End Sub

    Protected Sub ddlProperty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProperty.SelectedIndexChanged
        BindInnerGridView()
        gvRemarks.Visible = True
    End Sub

    Private Sub BindInnerGridView()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@PROPERTY", SqlDbType.NVarChar, 200)
        param(0).Value = ddlProperty.SelectedItem.Value
        ObjSubSonic.BindGridView(gvRemarks, "GET_PROPERTY_COMMENTS_AUR_ID", param)
        If gvRemarks.Rows.Count > 0 Then
            lblgridheading.Visible = True
        Else
            lblgridheading.Visible = False
        End If
    End Sub
    Protected Sub gvRemarks_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRemarks.PageIndexChanging
        gvRemarks.PageIndex = e.NewPageIndex()
        BindInnerGridView()
    End Sub
    Protected Sub gvRemarks_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvRemarks.RowEditing

    End Sub

End Class
