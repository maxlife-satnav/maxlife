Imports Microsoft.VisualBasic
Imports System
Imports System.io
Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports cls_OLEDB_postgresSQL

Partial Class WorkSpace_SMS_Webfiles_SiteLocationMap
    Inherits System.Web.UI.Page
    Dim filepath As String
    Dim strtxt As String
    Dim Subsonic As clsSubSonicCommonFunctions = New clsSubSonicCommonFunctions

    Dim strResponse As String = ""
    Dim strpoints As String = ""
    Dim strResponse1 As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblmsg.Text = ""
        If Not IsPostBack Then
            Subsonic.Binddropdown(ddlcity, "USP_GETACTIVECITIES_ALLOCATED", "CTY_NAME", "CTY_CODE")
            ddlcity.Items.Insert(1, New ListItem("ALL", "ALL"))
            lblmsg.Text = ""
            ' BindMap("")
            ' ConvertDataTabletoString()
            GetData("")


        End If

    End Sub

    Private Sub GetData(city As String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_GOOGLE_MAP")
        sp.Command.AddParameter("@CTY_ID", city, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)


        Dim ds As DataSet = sp.GetDataSet
        rptMarkers.DataSource = ds
        rptMarkers.DataBind()

    End Sub

    

    'Private Sub BindMap(ByVal city As String)
    '    LBLMSG.Text = ""

    '    Dim strMainPoints As String
    '    strMainPoints = ""

    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_GOOGLE_MAP")
    '    sp.Command.AddParameter("@CTY_ID", city, DbType.String)


    '    Dim dr As SqlDataReader = sp.GetReader
    '    Dim lat As Double = "0"
    '    Dim lon As Double = "0"
    '    Dim icnt As Integer = 1
    '    Dim AVAILABLE_WT As Double = 0
    '    Dim OCCUPIED_WT As Double = 0

    '    Dim AVAILABLE_WI As Double = 0
    '    Dim OCCUPIED_WI As Double = 0

    '    Dim AVAILABLE_WBPO As Double = 0
    '    Dim OCCUPIED_WBPO As Double = 0



    '    Dim strText As String = ""

    '    While dr.Read()

    '        lat = CDbl(dr("LAT"))
    '        lon = CDbl(dr("LONG"))
    '        AVAILABLE_WT = CDbl(dr("AVAILABLE_WT"))
    '        OCCUPIED_WT = CDbl(dr("OCCUPIED_WT"))

    '        'AVAILABLE_WI = CDbl(dr("AVAILABLE_WI"))
    '        'OCCUPIED_WI = CDbl(dr("OCCUPIED_WI"))

    '        'AVAILABLE_WBPO = CDbl(dr("AVAILABLE_WBPO"))
    '        'OCCUPIED_WBPO = CDbl(dr("OCCUPIED_WBPO"))



    '        'strText = CStr(dr("LCM_NAME")) & "<br> <b>Address:</b> " & CStr(dr("LCM_NAME")) & "(" & CStr(dr("LCM_CODE")) & ") , " & CStr(dr("CTY_NAME")) & ", " & CStr(dr("CTY_STE_ID")) & ", " & CStr(dr("CTY_CNY_ID")) & "<br/>" & "<b>Total Available Seats:- WT :(" & CStr(AVAILABLE_WT) & ") ,WI :(" & CStr(AVAILABLE_WI) & "),WBPO :(" & CStr(AVAILABLE_WBPO) & ")</b>" & "<br/><b>Total Occupied Seats:- WT :(" & CStr(OCCUPIED_WT) & ") ,WI :(" & CStr(OCCUPIED_WI) & "),WBPO :(" & CStr(OCCUPIED_WBPO) & ")</b>"
    '        'strText = "<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1>" & CStr(dr("LCM_NAME")) & "<br> <b>Address:</b> " & CStr(dr("LCM_NAME")) & "(" & CStr(dr("LCM_CODE")) & ") , " & CStr(dr("CTY_NAME")) & ", " & CStr(dr("CTY_STE_ID")) & ", " & CStr(dr("CTY_CNY_ID")) & "<br/>" & "<b>Total Available Seats:-</b> WST :(" & CStr(AVAILABLE_WT) & ") ,HCB :(" & CStr(AVAILABLE_WI) & "),FCB :(" & CStr(AVAILABLE_WBPO) & ")" & "<br/><b>Total Occupied Seats:-</b> WST :(" & CStr(OCCUPIED_WT) & ") ,HCB :(" & CStr(OCCUPIED_WI) & "),FCB :(" & CStr(OCCUPIED_WBPO) & ")</font>"
    '        strText = "<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1>" & CStr(dr("LCM_NAME")) & "<br> <b>Address:</b> " & CStr(dr("LCM_NAME")) & "(" & CStr(dr("LCM_CODE")) & ") , " & CStr(dr("CTY_NAME")) & ", " & CStr(dr("CTY_STE_ID")) & ", " & CStr(dr("CTY_CNY_ID")) & "<br/>" & "<b>Total Available Seats:-</b> :(" & CStr(AVAILABLE_WT) & ") " & "<br/><b>Total Occupied Seats:-</b> :(" & CStr(OCCUPIED_WT) & ")</font>"




    '        Dim str As String = ""
    '        'str = "<item>"
    '        'str = str + "<title>" & CStr(dr("LCM_NAME")) & "</title><link></link><guid></guid>"
    '        'str = str + "<description>"
    '        'str = str + "<![CDATA["
    '        'str =  

    '        'str = str + "]]>"
    '        'str = str + "</description>"
    '        'str = str + "<georss:line>" & lat & " " & lon & "</georss:line>"
    '        'str = str + "</item>"


    '        strpoints = strText
    '        icnt += 1
    '        ' ['',lat,lon,1]
    '        strMainPoints = strMainPoints & " ['" & strpoints & "'" & "," & lat & "," & lon & ", 1],"
    '    End While





    '    If File.Exists(MapPath("GoogleMap.txt")) Then
    '        strResponse = File.ReadAllText(MapPath("GoogleMap.txt"))
    '        strResponse = strResponse.Replace("@@points", strMainPoints)
    '    End If
    '    'File.Delete(MapPath("MapGoogleSiteLocationMap.html"))
    '    'File.WriteAllText(MapPath("MapGoogleSiteLocationMap.html"), strResponse)

    '    litmap1.Text = strResponse

    '    'If File.Exists(MapPath("MapGoogleSiteLocationMap.html")) Then
    '    '    strResponse1 = File.ReadAllText(MapPath("MapGoogleSiteLocationMap.html"))
    '    '    'Dim strcity As String = ""
    '    '    'Dim strMapCenter As String = ""
    '    '    'If ddlcity.SelectedItem.Value = "--Select--" Then
    '    '    '    strcity = ""
    '    '    'ElseIf ddlcity.SelectedItem.Value = "ALL" Then
    '    '    '    strcity = ""
    '    '    'Else
    '    '    '    strcity = ddlcity.SelectedItem.Value
    '    '    'End If
    '    '    'If strcity = "" Then
    '    '    '    strMapCenter = "map.setCenter(new OpenLayers.LonLat(78.411776,17.499027),3);"
    '    '    'Else
    '    '    '    strMapCenter = "map.setCenter(new OpenLayers.LonLat(" & lon & "," & lat & "),10);"
    '    '    'End If

    '    '    'strResponse1 = strResponse1.Replace("@@Zoom", strMapCenter)

    '    '    'Literal1.Text = strResponse1

    '    'End If
    'End Sub





    'Private Sub GetMap()
    '    Dim strResponse As String = ""
    '    Dim strResponse1 As String = ""
    '    If File.Exists(MapPath("MapXMLFormat.txt")) Then
    '        strResponse = File.ReadAllText(MapPath("MapXMLFormat.txt"))

    '        Dim str As String = ""
    '        str = "<item>"
    '        str = str + "<title>png_routes_uid</title><link></link><guid></guid>"
    '        str = str + "<description>"
    '        str = str + "<![CDATA["
    '        str = str + "BELAPUR (BPO)  <br/><b>Address:</b> BELAPUR (BPO) (BELAPUR)"
    '        str = str + "Total Available Seats:- WT :() ,WI :(),WBPO :()</b>"
    '        str = str + "<br/><b>Total Occupied Seats:- WT :() ,WI :(),WBPO :()</b>"
    '        str = str + "]]>"
    '        str = str + "</description>"
    '        str = str + "<georss:line>19.019259	73.039362</georss:line>"
    '        str = str + "</item>"

    '        strResponse = strResponse.Replace("@@item", str)
    '        File.Delete(MapPath("stpl-png_routes_uid.xml"))
    '        File.WriteAllText(MapPath("stpl-png_routes_uid.xml"), strResponse)

    '    End If

    '    If File.Exists(MapPath("index.html")) Then
    '        strResponse1 = File.ReadAllText(MapPath("index.html"))

    '        Literal1.Text = strResponse1

    '    End If


    'End Sub

     

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strcity As String = ""
        If ddlcity.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Please Select City"
            lblMsg.Visible = False
            strcity = ""

        ElseIf ddlcity.SelectedItem.Value = "ALL" Then
            strcity = ""
        Else
            strcity = ddlcity.SelectedItem.Value
        End If
        'ConvertDataTabletoString()
        GetData(strcity)
    End Sub
End Class

