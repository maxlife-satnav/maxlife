<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmusermapfloorlist_temp.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmusermapfloorlist_temp"
    Title="View Map" %>

<%@ Register Src="Controls/query.ascx" TagName="query" TagPrefix="uc1" %>
<%@ Register Src="Controls/Map_Departments.ascx" TagName="Map_Departments" TagPrefix="uc2" %>
<%@ Register Src="Controls/ConsolidateReport.ascx" TagName="ConsolidateReport" TagPrefix="uc3" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function querySt(ji) {
            hu = window.location.search.substring(1);
            gy = hu.split("&");
            for (i = 0; i < gy.length; i++) {
                ft = gy[i].split("=");
                if (ft[0] == ji) {
                    return ft[1];
                }
            }
        }
        var flr_id = querySt("flr_id");
        var box = querySt("box");
        var spc_id = querySt("spc_id");
    </script>
    <%--   <script type="text/javascript">

        $(window).load(function () {
            $('#loading').hide();
        });
        $(function () {
            //When an <li> element in your myMenu area is clicked
            $('#divli li').click(function () {
                //Removes the active class from any <li> elements
                $('li.active').removeClass('active');
                //Adds it to the current element
                $(this).addClass('active');
            });
        });

    </script>
    <style type="text/css">
        #loading {
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            position: fixed;
            display: block;
            opacity: 0.7;
            background-color: #fff;
            z-index: 99;
            text-align: center;
        }

        #loading-image {
            position: absolute;
            left: 34%;
            top: 32%;
            z-index: 100;
            display: block;
        }
    </style>--%>
</head>
<body>
    <script type="text/javascript" src="tabcontent.js"></script>
    <link href="tabcontent.css" rel="stylesheet" type="text/css" />

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <form id="form1" runat="server">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-primary" runat="server" id="divspace">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">
                                                        <asp:Label ID="lblHead" runat="server" class="control-label"></asp:Label>
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="float: right"><span class='glyphicon glyphicon-search'></span>Advanced Search</a>
                                                        <%--<button type="button" class="btn" id="spacePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                                            style="clear: both; float: right; margin-top: -5px">
                                                        <i class="fa fa-search" style="float:right"></i>
                                                        </button>--%>
                                                        
                                                        <a id="btn1"  style="float: right;padding-right:5px;text-decoration:none !important;"><span class='glyphicon glyphicon-refresh'></span> Refresh </a>

                                                    </h3>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse out">
                                                    <div class="panel-body">
                                                        <label class="col-md-1 control-label">Floor:</label>
                                                        <div class="col-md-4">
                                                            <asp:DropDownList ID="ddlfloors" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                                                        </div>

                                                        <label class="col-md-1 control-label">Search By:</label>
                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="ddlsearchfor" runat="server" CssClass="selectpicker" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <label class="col-md-1 control-label">Sub-Item:</label>
                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="ddlsearchforsub" runat="server" CssClass="selectpicker" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div>
                                                            <button id="btnSearch" value="Filter" style="margin-left: -5px;" class="btn btn-primary custom-button-color"> <span class='glyphicon glyphicon-refresh'></span> Submit </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <iframe id="midFrame" height="500px" width="100%" style='margin: 0; margin-left: 0px'
                                    frameborder="0" scrolling="no" runat="server"></iframe>

                            </div>
                        </div>

                        <div style="background: #ffffff;">
                            <div id="flowertabs1" class="modernbricksmenu2">
                                <ul>
                                    <li><a href="#" rel="tcontent1" class="selected">Legends</a></li>
                                    <li><a href="#" rel="tcontent2" class="selected">Seat Type Information </a></li>
                                    <li><a href="#" rel="tcontent3" class="selected">Search Employee</a></li>
                                    <li><a href="#" rel="tcontent4" class="selected">Vertical Details</a></li>
                                    <li><a href="#" rel="tcontent5" class="selected">Consolidate Report</a></li>
                                </ul>
                                <div style="clear: both; width: 100%;">
                                </div>
                            </div>
                            <div id="tcontent1" class="tabcontent_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="col-md-12 control-label">Status</label>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image8" ImageUrl="../../images/chair_green.gif" runat="server" />
                                                            Un-Allocated                                                          
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image11" ImageUrl="../../images/Chair_Black.gif" runat="server" />
                                                            Vacant but Reserved
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image12" ImageUrl="../../images/Chair_Blue.gif" runat="server" />
                                                            Allocated Vacant
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image13" ImageUrl="../../images/chair_red.gif" runat="server" />
                                                            Allocated Occupied
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image14" ImageUrl="../../images/chair_blink.gif" runat="server" />
                                                            Duplicate Values
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 text-left">
                                                    <label>Seat Type</label>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image9" ImageUrl="http://projects.a-mantra.com:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=UHCSeatType"
                                                                runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 text-left">
                                                    <label>Space Type</label>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image10" ImageUrl="http://projects.a-mantra.com:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=floormaps_UHC_spacetype"
                                                                runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 text-left">
                                                    <label class="col-md-12 control-label">Assets</label>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image1" ImageUrl="http://devv4.a-mantra.com/images/p1.png" runat="server" />
                                                            Printer                                                         
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image2" ImageUrl="http://devv4.a-mantra.com/images/com.png" runat="server" />
                                                            Computer
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 control-label">
                                                            <asp:Image ID="Image3" ImageUrl="http://devv4.a-mantra.com/images/tel.png" runat="server" />
                                                            Telephone
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tcontent2" class="tabcontent_1">

                                <asp:GridView ID="dlspacetype" runat="server" EmptyDataText="No Records Found" AllowPaging="true"
                                    AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Seat Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspctype" runat="Server" Text='<%#Eval("space_type")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Count">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspacename" runat="Server" Text='<%#Eval("space_Count")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="tcontent4" class="tabcontent_1">
                                <asp:GridView ID="GridView1" runat="server" EmptyDataText="No Records Found" AllowPaging="true"
                                    AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vertical">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspctype1" runat="Server" Text='<%#Eval("DEP_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Occupied Count">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspctype2" runat="Server" Text='<%#Eval("OCCUPIED")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allocated Count">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspctype2" runat="Server" Text='<%#Eval("ALLOCATED")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="tcontent3" class="tabcontent_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-3 control-label">Field</label>
                                                <div class="col-md-3">
                                                    <select ID="drpdwnCategory" Class="selectpicker" data-live-search="true">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-3 control-label">Operator</label>
                                                <div class="col-md-3">
                                                    <select ID="drpdwnOpt" Class="selectpicker" data-live-search="true">
                                                        <option Value="=" >=</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-3 control-label">Value</label>
                                                <div class="col-md-3">
                                                    <select ID="drpdwnValue" Class="selectpicker" data-live-search="true">
                                                    </select>
                                                  <a ID="ImgFetch" > <img src='../../WorkSpace/GIS/Images/Search.jpg' /> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tcontent5" class="tabcontent_1">
                                <asp:DataList ID="dlWings" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <ItemTemplate>
                                        <h3>
                                            <asp:Label ID="lblwing" Text='<%# Eval("SPC_WNG_ID") %>' class="col-md-6 control-label" runat="server" Visible="false"></asp:Label></h3>
                                        <asp:GridView ID="dlSeatSummaryByWing" runat="server" EmptyDataText="No Records Found" AllowPaging="true"
                                            AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No. of Seats">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsummary" runat="server" Text='<%# Eval("summary") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </div>

                        <script type="text/javascript">
                            var myflowers = new ddtabcontent("flowertabs1")
                            myflowers.setpersist(true)
                            myflowers.setselectedClassTarget("link") //"link" or "linkparent"
                            myflowers.init()
                        </script>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        function GetParameterValues(param) {
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {
                var urlparam = url[i].split('=');
                if (urlparam[0] == param) {
                    return urlparam[1];
                }
            }
        }

        $(document).ready(function () {
            $.getJSON('../../api/Space_mapAPI/GetallFilterbyItem', function (result) {
                $('#<%= ddlsearchfor.ClientID %> option').remove()
                $('#<%= ddlsearchfor.ClientID %>').append($("<option></option>").val("--Select--").html("--Select--"));
                $.each(result, function (key, value) {
                    $('#<%= ddlsearchfor.ClientID %>').append($("<option></option>").val(value.SSQ_ID).html(value.SSQ_NAME));
                });
                $('#<%= ddlsearchfor.ClientID %>').selectpicker('refresh');
            });
            $('#<%= ddlsearchfor.ClientID %>').on('change', function (e) {
                var dataObject = { Item: $('#<%= ddlsearchfor.ClientID %>').val(), location: GetParameterValues('lcm_code'), flrid: GetParameterValues('flr_code') };

                $.post('../../api/Space_mapAPI/GetallFilterbySubItem', dataObject, function (result) {
                    $('#<%= ddlsearchforsub.ClientID %> option').remove()
                    $('#<%= ddlsearchforsub.ClientID %>').append($("<option></option>").val("--Select--").html("--Select--"));

                    $.each(result, function (key, value) {
                        $('#<%= ddlsearchforsub.ClientID %>').append($("<option></option>").val(value.SUBITEM_ID).html(value.SUBITEM_NAME));
                    });
                    $('#<%= ddlsearchforsub.ClientID %>').selectpicker('refresh');
                });
            });
            $('#btn1').on('click', function (e) {
                var url = "BubleViewMap.aspx?lcm_code=" + GetParameterValues('lcm_code') + "&bbox=8155667.5,4548958,8267027.5,4630498.5&twr_code=" + GetParameterValues('twr_code') + "&flr_code=" + GetParameterValues('flr_code');
                $('#<%= midFrame.ClientID %>').attr("src", url);
                return false;
            });

            $('#btnSearch').on('click', function (e) {
                alert('heloo');
                var url = "BubleViewMap.aspx?lcm_code=" + GetParameterValues('lcm_code') + "&bbox=8155667.5,4548958,8267027.5,4630498.5&twr_code=" + GetParameterValues('twr_code') + "&flr_code=" + $('#<%= ddlfloors.ClientID %>').val() + "&item=" + $('#<%= ddlsearchfor.ClientID %>').val() + "&subitem=" + $('#<%= ddlsearchforsub.ClientID %>').val();
                $('#<%= midFrame.ClientID %>').attr("src", url);
                return false;
            });

            $.getJSON('../../api/Space_mapAPI/GetQueryAnalycat', function (result) {
                $('#drpdwnCategory option').remove()
                //$('#drpdwnCategory').append($("<option></option>").val("--Select--").html("--Select--"));
                $.each(result, function (key, value) {
                    $('#drpdwnCategory').append($("<option></option>").val(value.smq_Id).html(value.smq_field));
                });
                $('#drpdwnCategory').selectpicker('refresh');
                bindvaluesbycategory();
            });
            function bindvaluesbycategory() {
                
                var ddlcategory = { category: $('#drpdwnCategory').val(), location: GetParameterValues('lcm_code'), flrid: GetParameterValues('flr_code'), twrid: GetParameterValues('twr_code') };
                
                $.post('../../api/Space_mapAPI/GetQueryAnalyValuebyCat', ddlcategory, function (result) {
                    $('#drpdwnValue option').remove()
                    //$('#drpdwnCategory').append($("<option></option>").val("--Select--").html("--Select--"));
                    $.each(result, function (key, value) {
                        $('#drpdwnValue').append($("<option></option>").val(value.Value).html(value.DisValue));
                    });
                    $('#drpdwnValue').selectpicker('refresh');
                });
            }
            $('#drpdwnCategory').on('change', function (e) {
                bindvaluesbycategory();
            });


            $('#ImgFetch').on('click', function (e) {
                alert('heloo');
                $.post('../../api/Space_mapAPI/GetspcidbyEmp', { CatValue: $('#drpdwnValue').val() }, function (result) {
                    var url 
                    $.each(result, function (key, value) {
                        alert(value.SSA_SPC_ID);
                        url = "SpaceViewmap_queryanly.aspx?spcid=" + value.SSA_SPC_ID + "&aur_id= " + $('#drpdwnValue').val();
                        alert(url);
                    });
                    $('#<%= midFrame.ClientID %>').attr("src", url);
                    return false;
                });
                
              });


        });
    </script>
</body>
</html>
