﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_repVerticalRelease
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim objMasters As clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""
        ' Try
        If Not Page.IsPostBack Then
            ReportViewer1.Visible = True
            obj.bindVertical(ddlVertical)
            ddlVertical.Items(0).Text = "--All--"
            'ddlVertical.Items.RemoveAt(1)
            BindReleaseData()
        End If

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        BindReleaseData()
    End Sub
    Public Sub BindReleaseData()
        Dim rds As New ReportDataSource()
        rds.Name = "VerticalWiseReleaseDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/VerticalReleaseReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True


        Dim Mvertical As String = ""
       
        If ddlVertical.SelectedItem.Value = "--All--" Then
            Mvertical = ""
        Else
            Mvertical = ddlVertical.SelectedItem.Value
        End If


        Dim sp1 As New SqlParameter("@VER_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = Mvertical
        Dim sp2 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
        sp2.Value = "ver_name"
        Dim sp3 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
        sp3.Value = "ASC"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "VERTICAL_RELEASE_REPORT", sp1, sp2, sp3)
        rds.Value = dt

    End Sub
    Protected Sub ddlVertical_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVertical.SelectedIndexChanged

    End Sub

    
End Class
