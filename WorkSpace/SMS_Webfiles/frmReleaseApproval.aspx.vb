Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmReleaseApproval
    Inherits System.Web.UI.Page
    Private dvProducts As DataView
    Public strMsg As String
    Public strErrorMsg As String
    Dim ObjComm As New SqlCommand
    Dim strSQL As String


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            visibleTF(1)
            Binddata()
            visibleTF(0)
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Public Function CHECK_ADMIN_ROLE(ByVal UID As String) As Integer

        Dim objdata As SqlDataReader
        Dim strSQL As String
        strSQL = "SELECT URL_USR_ID,ROL_ACRONYM,URL_SCOPEMAP_ID " & _
          "FROM  " & Session("TENANT") & "."  & "USER_ROLE, " & Session("TENANT") & "."  & "ROLE " & _
          "WHERE URL_ROL_ID=ROL_ID AND ROL_ACRONYM IN ('GADMIN','NADMIN') AND URL_USR_ID = '" & UID & "' ORDER BY ROL_ACRONYM"
        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        Dim locs As String = "'0'"
        Dim rol As String
        Dim userid As String
        While objdata.Read
            locs = locs & ",'" & objdata("URL_scopemap_ID").ToString & "'"
            userid = objdata("url_usr_id").ToString
            If rol = "" Then
                rol = objdata("rol_acronym").ToString
            Else
                rol = rol + "," + objdata("rol_acronym").ToString
            End If
        End While
        objdata.Close()

        If UCase(rol) = "GADMIN" Or UCase(rol) = "NADMIN" Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Public Function CHECK_ANY_ROLE(ByVal UID As String, ByVal ROLE As String) As Integer

        Dim objdata As SqlDataReader
        Dim strSQL As String
        strSQL = "SELECT URL_USR_ID,ROL_ACRONYM,URL_SCOPEMAP_ID " & _
          "FROM  " & Session("TENANT") & "."  & "USER_ROLE, " & Session("TENANT") & "."  & "ROLE " & _
          "WHERE URL_ROL_ID=ROL_ID AND ROL_ACRONYM IN ('" & ROLE & "') AND URL_USR_ID = '" & UID & "' ORDER BY ROL_ACRONYM"
        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        Dim locs As String = "'0'"
        Dim rol As String
        Dim userid As String
        While objdata.Read
            locs = locs & ",'" & objdata("URL_scopemap_ID").ToString & "'"
            userid = objdata("url_usr_id").ToString
            If rol = "" Then
                rol = objdata("rol_acronym").ToString
            Else
                rol = rol + "," + objdata("rol_acronym").ToString
            End If
        End While
        objdata.Close()

        If UCase(rol) = UCase(ROLE) Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Private Sub Binddata()
        Try

            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
            arParms(0).Value = Session("uid")

            If CHECK_ADMIN_ROLE(Session("UID")) Then
                arParms(1) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
                arParms(1).Value = 1
            ElseIf CHECK_ANY_ROLE(Session("UID"), "OSSA") Or CHECK_ANY_ROLE(Session("UID"), "OSSI") Or CHECK_ANY_ROLE(Session("UID"), "ADMIN") Then
                arParms(1) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
                arParms(1).Value = 2
            Else
                arParms(1) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
                arParms(1).Value = 3
            End If

            Dim capDS As New DataTable
            capDS = SqlHelper.ExecuteDataTable(CommandType.StoredProcedure, AppSettings("APPDB") & "SPACE_RELEASEAPP_BINDDATA_SP", arParms)
            ReleaseList.DataSource = capDS
            ReleaseList.DataBind()


         
            If ReleaseList.Rows.Count > 0 Then
                Dim i As Integer
                ReleaseList.Visible = True
                For i = 0 To ReleaseList.Rows.Count - 1
                    If ReleaseList.Rows(i).Cells(4).Text = 1 Then
                        ReleaseList.Rows(i).Cells(3).Text = "Employee(s)"
                    ElseIf ReleaseList.Rows(i).Cells(4).Text = 2 Then
                        ReleaseList.Rows(i).Cells(3).Text = "Non Employee(s)"
                    Else
                        ReleaseList.Rows(i).Cells(3).Text = "Na"
                    End If
                Next
            Else
                ReleaseList.Visible = False
                Response.Write("<p align=center class=clsmessage><br><br><br><br><br><br><br><br><br><br><br><br><br>No requests for approving the release of space </p>")
            End If
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            ' fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Private Sub ReleaseList_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ReleaseList.PageIndexChanging
        Try
            ReleaseList.PageIndex = e.NewPageIndex
            visibleTF(1)
            Binddata()
            visibleTF(1)
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub visibleTF(ByVal status As Integer)
        If status = 1 Then
            ReleaseList.Columns(4).Visible = True
        Else
            ReleaseList.Columns(4).Visible = False
        End If
    End Sub
End Class
