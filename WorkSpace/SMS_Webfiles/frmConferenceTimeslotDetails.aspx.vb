﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmConferenceTimeslotDetails
    Inherits System.Web.UI.Page
    Dim fdate As Date = ""
    Dim tdate As Date = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
           
           
            lblMsg.Visible = False

        End If
    End Sub
    Public Sub bindtimeslots()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CONFERENCE_TIME_SLOTDETAILS")
        sp.Command.AddParameter("@StartDate", fdate, DbType.Date)
        sp.Command.AddParameter("@EndDate", tdate, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        gvspacereport.DataSource = ds
        gvspacereport.DataBind()


    End Sub
End Class
