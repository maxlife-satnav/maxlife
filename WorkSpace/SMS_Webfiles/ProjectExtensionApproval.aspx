﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ProjectExtensionApproval.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_ProjectExtensionApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        //function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        //    re = new RegExp(aspCheckBoxID)
        //    for (i = 0; i < form1.elements.length; i++) {
        //        elm = document.forms[0].elements[i]
        //        if (elm.type == 'checkbox') {
        //            if (re.test(elm.name))
        //                elm.checked = checkVal
        //        }
        //    }
        //}
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < aspnetForm.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function CheckDataGrid() {
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                window.alert('Please Select atleast one');
                return false;
            }
            else {
                var input = confirm("Are you sure you want to extend?");
                if (input == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td width="100%" align="center">
                            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                                ForeColor="Black">Approval for Project Extension
             <hr align="center" width="60%" /></asp:Label></td>
                    </tr>
                </table>
                <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                    <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                        align="center" border="0">
                        <tr>
                            <td colspan="3" align="left">
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px">
                                <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                            <td width="100%" class="tableHEADER" style="height: 27px" align="left">
                                <strong>&nbsp; Approval for Project Extension</strong></td>
                            <td style="height: 27px">
                                <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                        </tr>
                        <tr>
                            <td background="../../Images/table_left_mid_bg.gif"></td>
                            <td align="left">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="divmessagebackground" />
                                <br />
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                                <table id="tblReqids" cellpadding="0" cellspacing="0" border="1" style="width: 98%">
                                    <tr>
                                        <td align="left" style="width: 50%">&nbsp;Select City <span style="color: #ff0000">*</span>
                                            <asp:RequiredFieldValidator ID="rfvLoc"
                                                runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please select City"
                                                InitialValue="--Select--"></asp:RequiredFieldValidator>

                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="clsComboBox"
                                                Width="99%" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%">&nbsp;Select Location <span style="color: #ff0000">*</span>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please select Location"
                                                InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="clsComboBox"
                                                OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" Width="99%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%">&nbsp;Select Tower <span style="color: red">*<asp:RequiredFieldValidator
                                            ID="rfvTower" runat="server" ControlToValidate="ddlTower" Display="None" ErrorMessage="Please select Tower"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator><font class="clsNote"></font></span></td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlTower" runat="server" CssClass="clsComboBox" Width="99%" AutoPostBack="True">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 13px" width="50%">&nbsp;Select Floor <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong>
                                            <asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlFloor" Display="None"
                                                ErrorMessage="Please select Floor" InitialValue="-- Select --"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="center" style="height: 13px" width="50%">
                                            <asp:DropDownList ID="ddlFloor" runat="server" Width="99%" CssClass="clsComboBox">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" style="height: 15px">
                                            <asp:Button ID="btnView" runat="server" CssClass="clsButton" Text="View" /></td>
                                    </tr>
                                </table>
                                <table id="Table1" cellpadding="0" cellspacing="0" border="1" style="width: 98%">
                                    <tr>
                                        <td align="left" colspan="2">
                                            <asp:GridView ID="gvSpaceExtension" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="Server" ID="chk" />
                                                            <asp:Label runat="server" ID="lblReqID" Text='<% #Bind("SSA_VERTICAL")%>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk', this.checked)">
                                                        </HeaderTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Project">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblVercode" Text='<% #Bind("SSA_VERTICAL")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tower Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="Label3" Text='<% #Bind("Tower_Name")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Floor Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="Label4" Text='<% #Bind("FloorName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Wing Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="Label5" Text='<% #Bind("WingName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="To Date">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblToDate" Text='<% #Bind("To_Date")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="From Date">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="Label9" Text='<% #Bind("From_Date")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Extension Date">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblextensionDate" Text='<% #Bind("Extend_Date")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Extension Reason">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="Label11" Text='<% #Bind("SRN_EXT_REMARKS")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Admin Remarks">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" MaxLength="500" ID="txtRem" CssClass="clsTextField"></asp:TextBox>
                                                            <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                                                ControlToValidate="txtRem" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters !"></asp:CustomValidator>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- <asp:TemplateField HeaderText="EmpId" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblempid" Text='<% #bind("SSA_EMP_MAP") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:Button runat="server" ID="btnApprove" OnClientClick="return CheckDataGrid()"
                                                Text="Approve" CssClass="button" OnClick="btnApprove_Click" Visible="False" />
                                            <asp:Button runat="server" ID="btnReject" Text="Reject" CssClass="button" OnClick="btnReject_Click"
                                                Visible="False" />
                                        </td>
                                    </tr>
                                </table>
                                &nbsp;
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;"></td>
                        </tr>
                        <tr>
                            <td style="height: 17px;">
                                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                            <td style="height: 17px">
                                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

