﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_RepBuildingwisereport
    Inherits System.Web.UI.Page

    Dim obj As clsMasters = New clsMasters
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Text = String.Empty
            lblVert.Text = "Select " & Session("Parent") & "<span style=""color: red;"">*</span>"
            If Not Page.IsPostBack Then
                tblGrid.Visible = False
                getcity()
                bindVertical()
                bindseattype()
                btnExport.Visible = False
                pnlsummary.Visible = False
                ddlLocation.Items.Insert(0, "--All--")
                ddlstatus.Items.Insert(0, "--All--")
                bindreport()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "Load", exp)
        End Try
    End Sub
    Public Sub getcity()
        'Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        'param(0).Value = Session("Uid")
        objsubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
        'objsubsonic.Binddropdown(ddlCity, "GET_CITY", "CTY_NAME", "CTY_CODE")
        If ddlCity.Items.Count > 3 Then
            ddlCity.Items.Insert(0, "--All--")
            ddlCity.Items.RemoveAt(1)
        End If
    End Sub

    Public Sub bindVertical()
        objsubsonic.Binddropdown(ddlVertical, "usp_getVerticalDetails", "ver_name", "ver_code")
        ddlVertical.Items.Insert(0, "--All--")
        ddlVertical.Items.RemoveAt(1)
    End Sub
    Public Sub bindseattype()
        objsubsonic.Binddropdown(ddlseattype, "GET_SEAT_TYPE", "SEAT_NAME", "SEAT_CODE")
        ddlseattype.Items.Insert(0, "--All--")
        ddlseattype.Items.RemoveAt(1)
    End Sub
    Public Sub bindlocation()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        objsubsonic.Binddropdown(ddlLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
        If ddlLocation.Items.Count = 0 Then
            lblMsg.Text = "No Locations Available"
        End If

    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        liitems.ClearSelection()
        ddlLocation.ClearSelection()
        ddlseattype.ClearSelection()
        pnlsummary.Visible = False
        tblGrid.Visible = False
        If ddlCity.SelectedIndex <> 0 Then
            bindlocation()
            ddlLocation.Items.Insert(0, "--All--")
            ddlLocation.Items.RemoveAt(1)

            If ddlLocation.Items.Count = 0 Then
                lblMsg.Text = "There are no locations for the selected city"
            End If
        End If
    End Sub


    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        ddlCity.ClearSelection()
        ddlVertical.ClearSelection()
        ddlLocation.ClearSelection()
        ddlseattype.ClearSelection()
        ddlstatus.ClearSelection()
        liitems.ClearSelection()
        tblGrid.Visible = False
        btnExport.Visible = False
        pnlsummary.Visible = False
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVertical.SelectedIndexChanged
        liitems.ClearSelection()
        ddlCity.ClearSelection()
        ddlLocation.ClearSelection()
        ddlseattype.ClearSelection()
        pnlsummary.Visible = False
        tblGrid.Visible = False
    End Sub
    Public Sub bindGrid(ByVal gv As GridView)
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.VarChar, 100)
        param(0).Value = ddlCity.SelectedItem.Text
        param(1) = New SqlParameter("@VERT_CODE", SqlDbType.VarChar, 100)
        param(1).Value = ddlVertical.SelectedItem.Value
        param(2) = New SqlParameter("@LCM_CODE", SqlDbType.VarChar, 100)
        param(2).Value = ddlLocation.SelectedItem.Value
        param(3) = New SqlParameter("@SEAT_TYPE", SqlDbType.VarChar, 100)
        param(3).Value = ddlseattype.SelectedItem.Value
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_SEATTYPE_CONSOLIDATEREPORT", param)
        gv.DataSource = ds.Tables(0)
        gv.DataBind()
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        bindreport()

    End Sub
    Public Sub bindreport()
        ' Page.ClientScript.RegisterStartupScript(Me.GetType(), "alert", "showDisplay();", True)
        Dim selectedIndex As String = String.Empty
        tblGrid.Visible = True
        For Each item As ListItem In liitems.Items
            If item.Selected Then
                If selectedIndex = "" Then
                    selectedIndex = item.Value & " [" & item.Text & "]"
                Else
                    selectedIndex += ", " & item.Value & " [" & item.Text & "]"
                End If

            End If
        Next
        Dim query As String
        query = selectedIndex.Replace("'", "''")

        If ddlLocation.Items.Count <> 0 Then
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@CTY_CODE", SqlDbType.VarChar, 100)
            param(0).Value = ddlCity.SelectedItem.Text
            param(1) = New SqlParameter("@VERT_CODE", SqlDbType.VarChar, 100)
            param(1).Value = ddlVertical.SelectedItem.Value
            param(2) = New SqlParameter("@LCM_CODE", SqlDbType.VarChar, 100)
            param(2).Value = ddlLocation.SelectedItem.Value
            param(3) = New SqlParameter("@SEAT_TYPE", SqlDbType.VarChar, 100)
            param(3).Value = ddlseattype.SelectedItem.Value
            param(4) = New SqlParameter("@QUERY", SqlDbType.VarChar, 100)
            param(4).Value = query
            param(5) = New SqlParameter("@STATUS", SqlDbType.VarChar, 100)
            param(5).Value = ddlstatus.SelectedItem.Value
            Dim ds As New DataSet
            ds = objsubsonic.GetSubSonicDataSet("GET_SEATTYPE_CONSOLIDATEREPORT", param)
            If ddlseattype.SelectedItem.Value = "WS" Then
                lblA.Text = "WorkStation Seats"
            ElseIf ddlseattype.SelectedItem.Value = "HC" Then
                lblA.Text = "Half Cabin Seats"
            ElseIf ddlseattype.SelectedItem.Value = "FC" Then
                lblA.Text = "Full Cabin Seats"
            End If
            gvItem1.DataSource = ds.Tables(0)
            gvItem1.DataBind()
            btnExport.Visible = True

            If ddlseattype.SelectedItem.Value = "--All--" Then
                gvItem2.DataSource = ds.Tables(1)
                gvItem2.DataBind()
                lblA.Text = "WorkStation Seats"
                gvItem3.DataSource = ds.Tables(2)
                gvItem3.DataBind()
                tdC.Visible = True
                tdB.Visible = True
                'thB.Visible = True
                'thC.Visible = True
            Else
                Dim dt As New DataTable
                gvItem2.DataSource = dt
                gvItem2.DataBind()
                gvItem3.DataSource = dt
                gvItem3.DataBind()

                tdB.Visible = False
                tdC.Visible = False
                'thB.Visible = False
                'thC.Visible = False

            End If

            'Dim Mver As String = ""
            'Dim Mcity As String = ""
            'Dim Mloc As String = ""
            'If ddlCity.SelectedItem.Value = "--All--" Then
            '    Mcity = ""
            'Else
            '    Mcity = ddlCity.SelectedItem.Value
            'End If

            'If ddlLocation.SelectedItem.Value = "--All--" Then
            '    Mloc = ""
            'Else
            '    Mloc = ddlLocation.SelectedItem.Value
            'End If

            'If ddlVertical.SelectedItem.Value = "--All--" Then
            '    Mver = ""
            'Else
            '    Mver = ddlVertical.SelectedItem.Value
            'End If

            Dim param1(4) As SqlParameter
            param1(0) = New SqlParameter("@CTY_CODE", SqlDbType.VarChar, 100)
            param1(0).Value = ddlCity.SelectedItem.Value
            param1(1) = New SqlParameter("@VERT_CODE", SqlDbType.VarChar, 100)
            param1(1).Value = ddlVertical.SelectedItem.Value
            param1(2) = New SqlParameter("@LCM_CODE", SqlDbType.VarChar, 100)
            param1(2).Value = ddlLocation.SelectedItem.Value
            param1(3) = New SqlParameter("@SEAT_TYPE", SqlDbType.VarChar, 100)
            param1(3).Value = ddlseattype.SelectedItem.Value
            param1(4) = New SqlParameter("@STATUS", SqlDbType.VarChar, 100)
            param1(4).Value = ddlstatus.SelectedValue
            Dim dr As SqlDataReader
            dr = objsubsonic.GetSubSonicDataReader("GET_SEATTYPE_CONSOLIDATEREPORT_SUMMARY", param1)
            If dr.Read() Then
                lblavl.Text = "Total No of Seats    : " & dr("TOTAL").ToString()
                lblalloc.Text = "Allocated Seats    : " & CInt(dr("ALLOCOCC")) + CInt(dr("ALLOCVACN"))
                lblallocc.Text = "Allocated Occupied    : " & dr("ALLOCOCC").ToString()
                lblallvac.Text = "Allocated Vacant      : " & dr("ALLOCVACN").ToString()
                lblvacant.Text = "Vacant Seats      : " & dr("VACANT").ToString()
                pnlsummary.Visible = True
            End If

        Else
            lblMsg.Text = "There are no locations for the selected city"
        End If
    End Sub
    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click

        exportpanel1.Style.Clear()
        exportpanel1.BackColor = Drawing.Color.White
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel1.FileName = "BuildingWiseDetails"
        'exportpanel1.OpenInBrowser = True
        'Dim gv As New GridView
        'bindGrid(gv)
        'Export("BuildingWiseDeatils.xls", gv)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            gv.HeaderRow.BackColor = Drawing.Color.White
            gv.HeaderRow.BorderStyle = BorderStyle.Solid
            gv.HeaderRow.BorderWidth = 1
            PrepareControlForExport(gv.HeaderRow)

            table.Rows.Add(gv.HeaderRow)

        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            row.BorderWidth = 1
            row.BackColor = Drawing.Color.White
            row.BorderStyle = BorderStyle.Solid
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    'Protected Sub gvItem1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem1.PageIndexChanging
    '    gvItem1.PageIndex = e.NewPageIndex
    '    bindGrid(gvItem1)
    'End Sub

End Class
