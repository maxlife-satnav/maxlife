<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmEmployeemapping.aspx.vb" Inherits="SpaceManagement_frmSpaceVerticalAllocations" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function CheckDataGrid() {
            var k = 0;
            for (i = 0; i < aspnetForm.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                window.alert('Please select at least one checkbox');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Employee Mapping
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requisition ID <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlReq" Display="None"
                                            ErrorMessage="Please Select Requisition ID" InitialValue="--Select--">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlReq" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row col-md-6">
                                <div class="col-md-12">
                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="View" OnClick="btnView_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" EmptyDataText="No Employee Mapping Found."
                                    PageSize="2" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEmp" runat="server" />
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkEmp', this.checked)">
                                            </HeaderTemplate>
                                            <ItemStyle Width="5px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Floor">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblFloor"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Wing">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblWing"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="False">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblvertical" Text='<% #Bind("SSA_VERTICAL")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblSpace" Text='<% #Bind("SSA_SPC_ID")%>'></asp:Label>
                                                <asp:Label runat="server" ID="lblSpaceType" Text='<% #Bind("SSA_SPC_TYPE")%>' Visible="false"></asp:Label>
                                                <asp:Label runat="server" ID="lblReqID" Text='<% #Bind("SSA_SRNREQ_ID")%>' Visible="false"></asp:Label>
                                                <asp:Label runat="server" ID="lblSpcReqid" Text='<% #Bind("SSA_sPC_REQID")%>' Visible="false"></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Type">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlEmpType" runat="server" CssClass="selectpicker"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlEmpType_SelectedIndexChanged">
                                                    <asp:ListItem Text="Permanent" Value="P"></asp:ListItem>

                                                    <asp:ListItem Text="Contract" Value="C"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlEmp_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtEmpName" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <h6>
                                            <label class="col-md-12 control-label" style="text-align: left;">*One space for one employee</label></h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnAssign" runat="server" Text="Assign" OnClientClick="return CheckDataGrid()"
                                        CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
