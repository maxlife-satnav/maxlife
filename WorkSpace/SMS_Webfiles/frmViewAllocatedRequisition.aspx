<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmViewAllocatedRequisition.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmViewAllocatedRequisition" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div>
        
         <asp:TextBox ID="txttoday" runat="server" Visible="False" Width="0px"></asp:TextBox>
          
            <asp:TextBox ID="Textbox1"  runat="server" Width="0px" Visible="False"></asp:TextBox>
                
            <asp:Panel ID="panelmain"  runat="server" Width="95%" Height="100%">           
                        <table id="Table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                            align="center" border="0">
                            <tr>
                                <td>
                                    <img height="27" src="../../Images/table_left_top_corner.gif"></td>
                                <td class="tableHEADER" align="left">
                                    <strong>&nbsp; Allocated vertical Requisitions&nbsp;</strong></td>
                                <td>
                                    <img height="27" src="../../Images/table_right_top_corner.gif"></td>
                            </tr>
                            <tr>
                                <td background="../../Images/table_left_mid_bg.gif" style="width: 1px">
                                </td>
                                <td align="left" style="padding: 20px 20px 20px 20px">
                                    <center>
                                        <asp:GridView ID="dvVerticalReq" runat="server" Height="50px" Width="95%" CellPadding="3"
                                            AutoGenerateColumns="false" OnRowCreated="dvVerticalReq_OnRowCreated">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Requisition ID">
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" Text='<% #bind("Requisition_ID") %>' ID="lnkVerReq"
                                                            OnClick="lnk_view"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested by">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("Requested_by") %>' ID="lblReqBy"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("Requested_Date") %>' ID="lblReqDate"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Work Stations Required">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("Work_Stations_Required") %>' ID="lblWSR"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Half Cabins Required">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("Half_Cabnis_Required") %>' ID="lblHCR"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Full Cabins Required">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("Full_Cabins_Required") %>' ID="lblFCR"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("Status") %>' ID="lblStatus"></asp:Label>
                                                        <asp:Label runat="server" Text='<% #bind("StatusID") %>' ID="lblStatusID" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                 <asp:TemplateField HeaderText="Lab Space(Sft)">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("svr_labspace") %>' ID="lbllabspace"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                
                                                 <asp:TemplateField HeaderText="vertical">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("Vertical") %>' ID="lblVertical"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField HeaderText="From Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("MON") %>' ID="lblMon"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<% #bind("YER") %>' ID="lblYear"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:Label ID="lblVertical" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    </center>
                                </td>
                                <td background="../../Images/table_right_mid_bg.gif" style="width: 1px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img height="17" src="../../Images/table_left_bot_corner.gif" /></td>
                                <td background="../../Images/table_bot_mid_bg.gif">
                                    <img height="17" src="../../Images/table_bot_mid_bg.gif" /></td>
                                <td style="height: 17px;">
                                    <img height="17" src="../../Images/table_right_bot_corner.gif" /></td>
                            </tr>
                        </table>         
            </asp:Panel>

           
        </div>
   </asp:Content>