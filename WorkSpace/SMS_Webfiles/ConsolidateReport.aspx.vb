Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_ConsolidateReport
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim TWST, TFCB, THCB, Ttotal As Integer
    Dim mWST As Integer = 0
    Dim mHCB As Integer = 0
    Dim mFCB As Integer = 0


    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        ReportViewer1.Visible = False
        lblFWST.Text = ""
        lblFHCB.Text = ""
        lblFFCB.Text = ""
        lblWFHSeats.Text = ""
        ddlLocation.Items.Clear()
        BindLocation(ddlCity.SelectedItem.Value)
    End Sub
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        lblFWST.Visible = False
        lblFHCB.Visible = False
        lblFFCB.Visible = False
        lblWFHSeats.Visible = False
        totsumry.Visible = False
        If Not IsPostBack Then
            ReportViewer1.Visible = False
            BindCities()

            lblFWST.ForeColor = Drawing.Color.Black
            lblFWST.Font.Bold = True

            lblFHCB.ForeColor = Drawing.Color.Black
            lblFHCB.Font.Bold = True

            lblFFCB.ForeColor = Drawing.Color.Black
            lblFFCB.Font.Bold = True

            lblWFHSeats.ForeColor = Drawing.Color.Black
            lblWFHSeats.Font.Bold = True

        End If
    End Sub
    Public Sub BindCities()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")
        ddlCity.Items.Insert(1, "--All--")
    End Sub
    Public Sub BindLocation(ByVal strCity As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strCity
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONBY_CTY", "LCM_NAME", "LCM_CODE", param)
        If ddlLocation.Items.Count > 0 Then

            ddlLocation.Items.Insert(1, "--All--")

        Else
            ReportViewer1.Visible = False
        End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        If ddlLocation.SelectedValue = Nothing Then
            lblMsg.Text = "No Locations Available"
            Exit Sub
        End If
        bindgrid()
        totsumry.Visible = True

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETCONSOLIDATERPT_TOTAL")
        If ddlCity.SelectedItem.Value = "--All--" Then
            sp.Command.AddParameter("@CITY", "", DbType.String)
        Else
            sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        End If

        If ddlLocation.SelectedItem.Value = "--All--" Then
            sp.Command.AddParameter("@LCM_CODE", "", DbType.String)
        Else
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        End If
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            mWST = ds.Tables(0).Rows(0).Item("WST")
            mHCB = ds.Tables(0).Rows(0).Item("HCB")
            mFCB = ds.Tables(0).Rows(0).Item("FCB")
            lblFWST.Text = ds.Tables(0).Rows(0).Item("WST")
            lblFHCB.Text = ds.Tables(0).Rows(0).Item("HCB")
            lblFFCB.Text = ds.Tables(0).Rows(0).Item("FCB")
            lblWFHSeats.Text = CInt(mWST) + CInt(mHCB) + CInt(mFCB)
            lblWFHSeats.Visible = True
            lblFHCB.Visible = True
            lblFFCB.Visible = True
            lblFWST.Visible = True

        End If


        'Page.RegisterStartupScript("SCRIPTNAME", "<script language='javascript'>dg_reportc();</script>")
    End Sub

    Public Sub bindgrid()
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedValue
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedValue
        param(2) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(2).Value = 1
        param(3) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(3).Value = "Project"
        param(4) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(4).Value = "asc"
        param(5) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(5).Value = 10
        param(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(6).Value = 0
        param(6).Direction = ParameterDirection.Output
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GETConsolidateRPT", param)

        Dim rds As New ReportDataSource()
        rds.Name = "CostCenterAllocationSummaryDS"
        rds.Value = dt
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/CostCenterAllocationSummaryReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
      

    End Sub

End Class
