Imports System.Data.SqlClient
Imports System.Data
Imports System.io
Imports ControlFreak
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web.Configuration
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_Location
    Inherits System.Web.UI.Page
    Dim filepath As String
    Dim strtxt As String
    Dim Subsonic As clsSubSonicCommonFunctions = New clsSubSonicCommonFunctions

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If UCase(ddlcity.SelectedItem.Value) = "ALL" Then
            BindMap("")
        Else
            BindMap(ddlcity.SelectedItem.Value)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Subsonic.Binddropdown(ddlcity, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")
            ddlcity.Items.Insert(1, New ListItem("ALL", "ALL"))
            LBLMSG.Text = ""
            BindMap("")
        End If
    End Sub
    Private Sub BindMap(ByVal city As String)
        LBLMSG.Text = ""
       ' Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_LAT_LONG_BY_CITY")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_GOOGLE_LOCATION")
        sp.Command.AddParameter("@CTY_ID", city, DbType.String)
        GoogleMap1.Markers.Clear()
        GoogleMap1.Polylines.Clear()
        Dim PolyLine As New Artem.Web.UI.Controls.GooglePolyline()
        PolyLine.Color = Drawing.Color.Blue
        PolyLine.Opacity = 1
        PolyLine.Weight = 2
        PolyLine.IsGeodesic = False
        Dim dr As SqlDataReader = sp.GetReader
        Dim lat As Double = "0"
        Dim lon As Double = "0"
        Dim icnt As Integer = 1
        Dim AVAILABLE_WT As Double = 0
        Dim OCCUPIED_WT As Double = 0

        Dim AVAILABLE_WI As Double = 0
        Dim OCCUPIED_WI As Double = 0

        Dim AVAILABLE_WBPO As Double = 0
        Dim OCCUPIED_WBPO As Double = 0


        'Dim letter As Byte = 65
        Dim strText As String = ""
        'lstItems.Items.Clear()
        While dr.Read()
            Dim Markers As New Artem.Web.UI.Controls.GoogleMarker()
            Markers.IconUrl = Page.ResolveUrl("~/images/icon_wipro.png") ' "http://localhost/AmantraWipro/images/icon_wipro.png"
            Markers.Bouncy = True
            Markers.IconSize = New Artem.Web.UI.Controls.GoogleSize(32, 32)
            lat = CDbl(dr("LAT"))
            lon = CDbl(dr("LONG"))
            AVAILABLE_WT = CDbl(dr("AVAILABLE_WT"))
            OCCUPIED_WT = CDbl(dr("OCCUPIED_WT"))

            AVAILABLE_WI = CDbl(dr("AVAILABLE_WI"))
            OCCUPIED_WI = CDbl(dr("OCCUPIED_WI"))

            AVAILABLE_WBPO = CDbl(dr("AVAILABLE_WBPO"))
            OCCUPIED_WBPO = CDbl(dr("OCCUPIED_WBPO"))


            'strText = CStr(dr("LCM_NAME")) & "<br/><b>Address:</b> " & CStr(dr("LCM_NAME")) & "(" & CStr(dr("LCM_CODE")) & ") , " & CStr(dr("CTY_NAME")) & ", " & CStr(dr("CTY_STE_ID")) & ", " & CStr(dr("CTY_CNY_ID")) & "<br/><b>Total Available Seats (WT) :(" & CStr(AVAILABLE_WT) & ")</b>" & "<br/><b>Total Occupied Seats (WT) :(" & CStr(OCCUPIED_WT) & ")" & "<br/><b>Total Available Seats (WI) :(" & CStr(AVAILABLE_WI) & ")</b>" & "<br/><b>Total Occupied Seats (WI) :(" & CStr(OCCUPIED_WI) & ")" & "<br/><b>Total Available Seats (WBPO) :(" & CStr(AVAILABLE_WBPO) & ")</b>" & "<br/><b>Total Occupied Seats (WBPO) :(" & CStr(OCCUPIED_WBPO) & ")"


            strText = CStr(dr("LCM_NAME")) & "<br/><b>Address:</b> " & CStr(dr("LCM_NAME")) & "(" & CStr(dr("LCM_CODE")) & ") , " & CStr(dr("CTY_NAME")) & ", " & CStr(dr("CTY_STE_ID")) & ", " & CStr(dr("CTY_CNY_ID")) & "<br/>" & "<b>Total Available Seats:- WT :(" & CStr(AVAILABLE_WT) & ") ,WI :(" & CStr(AVAILABLE_WI) & "),WBPO :(" & CStr(AVAILABLE_WBPO) & ")</b>" & "<br/><b>Total Occupied Seats:- WT :(" & CStr(OCCUPIED_WT) & ") ,WI :(" & CStr(OCCUPIED_WI) & "),WBPO :(" & CStr(OCCUPIED_WBPO) & ")</b>"


            'LBLMSG.Text = strText
            'lstItems.Items.Add(CStr(dr("LCM_NAME")) & "(" & CStr(dr("LCM_CODE")) & ") , " & CStr(dr("CTY_NAME")) & ", " & CStr(dr("CTY_STE_ID")) & ", " & CStr(dr("CTY_CNY_ID")))
            Markers.Text = strText
            Markers.Latitude = lat
            Markers.Longitude = lon
            Markers.Address = strText

            GoogleMap1.Markers.Add(Markers)
            icnt += 1
        End While
        If icnt = 1 Then
            lblMsg.Text = "No such data found."
            GoogleMap1.Visible = False
        Else
            GoogleMap1.Latitude = lat
            GoogleMap1.Longitude = lon
            If String.IsNullOrEmpty(Trim(city)) Then
                GoogleMap1.Zoom = 4
            Else
                GoogleMap1.Zoom = 9
            End If
            'GoogleMap1.ZoomPanType = Artem.Web.UI.Controls.ZoomPanType.Large3D
            GoogleMap1.Visible = True
        End If
    End Sub
End Class
