﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="RepSpaceAllocation.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_RepBuildingwisereport" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Customized Report
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select City</label>
                                        <asp:RequiredFieldValidator ID="rfvcty" runat="server"
                                            ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Location"
                                            ControlToValidate="ddlLocation" Display="None" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Seat Type</label>
                                        <asp:RequiredFieldValidator ID="rfvCode12" runat="server" ControlToValidate="ddlseattype"
                                            Display="None" ErrorMessage="Please Select Seat Type " ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlseattype" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Status</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlstatus"
                                            Display="None" ErrorMessage="Please Select Status" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true">
                                                <%--<asp:ListItem Text="--Select--" Value="--Select--"></asp:ListItem>--%>
                                                <%--<asp:ListItem Text="--All--" Value="--All--"></asp:ListItem>--%>
                                                <asp:ListItem Text="VACANT" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ALLOCATED VACANT" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="ALLOCATED OCCUPIED" Value="7"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Columns</label>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="liitems" Display="None" ErrorMessage="Please Select Column(s)" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">

                                            <asp:ListBox ID="liitems" runat="server" SelectionMode="Multiple" Rows="6" CssClass="form-control">
                                                <asp:ListItem Value="LOCATION" Text="City" Selected="true"></asp:ListItem>
                                                <%-- <asp:ListItem Value="LCM_CODE" Text="Building Code" Selected =" true"></asp:ListItem>--%>
                                                <asp:ListItem Value="LCM_NAME" Text="Location" Selected="true"></asp:ListItem>
                                                <asp:ListItem Value="SPACE_ID" Text="Seat No" Selected=" true"></asp:ListItem>
                                                <asp:ListItem Value="SPACE_AREA" Text="Seat Area" Selected="true"></asp:ListItem>
                                                <asp:ListItem Value="VERTICAL" Text="Vertical" Selected="true"></asp:ListItem>
                                                <asp:ListItem Value="COST_CENTER" Text="Costcenter" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="DEPT_NAME" Text="Department" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="STATUS" Text="Status" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="EMP_ID" Text="Employee ID" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="EMP_NAME" Text="Employee Name" Selected="True"></asp:ListItem>
                                                <%--<asp:ListItem Value="EMP_NO" Text="Employee SignumID"></asp:ListItem>--%>
                                                <asp:ListItem Value="EMP_EMAIL" Text="Employee Email" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="MNGR_ID" Text="Manager's ID" Selected="True"></asp:ListItem>
                                                <%--<asp:ListItem Value="MNGR_NO" Text="Manage's SignumID"></asp:ListItem>--%>
                                                <asp:ListItem Value="MNGR_EMAIL" Text="Manager's Email" Selected="True"></asp:ListItem>

                                            </asp:ListBox>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <asp:Label ID="lblVert" runat="server" class="col-md-5 control-label" Visible=" false">Vertical<span style="color: red;">*</span> </asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                        ControlToValidate="ddlVertical" Display="None" ErrorMessage="Please Select Vertical"
                                        InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="ddlVertical" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Visible="false">
                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-primary custom-button-color" CausesValidation="False" />
                                    <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary custom-button-color" CausesValidation="False" />
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <ul class="nav nav-tabs" id="myTab">
                                            <li class="active"><a data-target="#consolidatedreport" data-toggle="tab">Consolidated Report</a></li>
                                            <li><a data-target="#workstations" data-toggle="tab">Workstations</a></li>
                                            <li><a data-target="#halfcabins" data-toggle="tab">Half Cabins</a></li>
                                            <li><a data-target="#fullcabins" data-toggle="tab">Full Cabins</a></li>
                                        </ul>
                                        <cc1:ExportPanel ID="exportpanel1" runat="server">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="consolidatedreport">
                                                    <div class="row">
                                                        <div class="form-group">
                                                        </div>
                                                    </div>
                                                    <div id="pnlsummary" runat="server" class="row">
                                                        <div class="row">
                                                            <div class="col-md-6 text=left">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-7 text=left">
                                                                            <asp:Label ID="lblavl" runat="server" Text="" CssClass="col-md-12 control-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 text=left">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-5 text=left">
                                                                            <asp:Label ID="lblalloc" runat="server" Text="" CssClass="col-md-12 control-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-7 text=left">
                                                                            <asp:Label ID="lblallocc" runat="server" Text="" CssClass="col-md-12 control-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-7">
                                                                            <asp:Label ID="lblallvac" runat="server" Text="" CssClass="col-md-12 control-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-7">
                                                                            <asp:Label ID="lblvacant" runat="server" Text="" CssClass="col-md-12 control-label"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                                <div class="tab-pane" id="workstations">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <%-- <div class="form-group">--%>
                                                            <div id="tblGrid" runat="server" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <fieldset>
                                                                            <legend>
                                                                                <asp:Label ID="lblA" runat="server" Text="WorkStation Seats"></asp:Label>
                                                                            </legend>
                                                                        </fieldset>
                                                                        <asp:GridView ID="gvItem1" runat="server" AllowSorting="False"
                                                                            RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                                                            PageSize="10" AutoGenerateColumns="true" EmptyDataText="No WorkStation Summary Report Found." ShowHeaderWhenEmpty="True"
                                                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                            <PagerStyle CssClass="pagination-ys" />
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <%-- </div>--%>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane" id="halfcabins">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="tdB" runat="server" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <fieldset>
                                                                            <legend>
                                                                                <asp:Label ID="lblB" runat="server" Text="Half Cabin Seats"></asp:Label>
                                                                            </legend>
                                                                        </fieldset>
                                                                        <asp:GridView ID="gvItem2" runat="server" AllowSorting="False"
                                                                            RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                                                            PageSize="10" AutoGenerateColumns="true" EmptyDataText="No Half Cabin Summary Report Found."
                                                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                            <PagerSettings Mode="NumericFirstLast" />

                                                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                            <PagerStyle CssClass="pagination-ys" />
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="fullcabins">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div id="tdC" runat="server" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <fieldset>
                                                                            <legend>
                                                                                <asp:Label ID="lblC" runat="server" Text="Full Cabin Seats"></asp:Label>
                                                                            </legend>
                                                                        </fieldset>
                                                                        <asp:GridView ID="gvItem3" runat="server" AllowSorting="False"
                                                                            RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                                                            PageSize="10" AutoGenerateColumns="true" EmptyDataText="No Full Cabin Summary Report Found."
                                                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                            <PagerStyle CssClass="pagination-ys" />

                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </cc1:ExportPanel>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery('#txtNumberofReq').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });

            //jQuery(function () {
            //    jQuery('#myTab a:last').tab('show')
            //})
        });
    </script>
</body>
</html>

