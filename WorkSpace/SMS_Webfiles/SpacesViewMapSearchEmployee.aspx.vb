﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports System.IO
Partial Class WorkSpace_SMS_Webfiles_SpacesViewMapSearchEmployee
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetMap()
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
            param(0).Value = Replace(Request.QueryString("spcid"), ";", "")
            Dim ds As New DataSet
            ds = ObjSubsonic.GetSubSonicDataSet("GET_SEARCHEMP_HEADING", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "myFunction", "setHead('" & ds.Tables(0).Rows(0).Item("SEMPTITLE").ToString() & "');", True)
            End If
        End If
    End Sub

    Private Sub GetMap()
        Dim strspaces As String
        Dim strbox_bounds As String
        Dim spcid As String
        Dim filter As String
        spcid = Request.QueryString("spcid")
        ' spcid = spcid.ToString.Substring(0, spcid.Length - 1)


        Dim spaces As Array
        spaces = spcid.Split(";")
        For i As Integer = 0 To spaces.Length - 1
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
            param(0).Value = spaces(i)
            param(1) = New SqlParameter("@aur_id", SqlDbType.NVarChar, 200)
            param(1).Value = Request.QueryString("aur_id")
            Dim ds As New DataSet
            'ds = objsubsonic.GetSubSonicDataSet("getSpaceDetails_space", param)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    'strspaces = strspaces & "{""longlat"":""" & ds.Tables(0).Rows(0).Item("lat") & "," & ds.Tables(0).Rows(0).Item("lon") & """,""space_id"":""<table id=table1 border=1 bgcolor=#FFFFE0 width=100%><tr><td width=100%><center><b><font color=red>Seat Information for Allocation/Deallocation</font></b></center></td></tr><tr><td width=50%><center>" & ds.Tables(0).Rows(0).Item("spc_id") & "</center></td></tr></table>"",""imgurl"":""" & Replace(ds.Tables(0).Rows(0).Item("IMGURL"), "chair_red", "chair_red_blink") & """},"
            '    strspaces = strspaces & "{""longlat"":""" & ds.Tables(0).Rows(0).Item("lat") & "," & ds.Tables(0).Rows(0).Item("lon") & """,""space_id"":""<table id=table1 border=1 bgcolor=#FFFFE0 width=100%><tr><td width=100%><center><b><font color=red>Seat Information</font></b></center></td></tr><tr><td width=50%><center><a href='' onclick=getspace('../GIS/SearchEmployeeGetData.aspx?spc_id=" & spcid & "')>" & ds.Tables(0).Rows(0).Item("spc_id") & "</a></center></td></tr></table>"",""imgurl"":""" & Replace(ds.Tables(0).Rows(0).Item("IMGURL"), "chair_red", "chair_red_blink") & """},"


            '    '<a href='' onclick=getspace()>Occupied By Amita Maheshwari</a>
            '    strbox_bounds = ds.Tables(0).Rows(0).Item("BBOX")
            'End If
            ds = ObjSubsonic.GetSubSonicDataSet("getSpaceDetails_space_queryanaly", param)
            If ds.Tables(0).Rows.Count > 0 Then
                strspaces = strspaces & "{""longlat"":""" & ds.Tables(0).Rows(0).Item("lat") & "," & ds.Tables(0).Rows(0).Item("lon") & """,""space_id"":"""
                strspaces = strspaces & "<table align=center cellSpacing=0 cellPadding=0 border=0 width=300><tr align=center><td bgcolor=#4863A0><span style='color:white'><b>Seat Information</b></span></td></tr></table><table style='font-size:12px' cellpadding=2 cellspacing=0 border=1 align=center width=300><tr>"
                strspaces = strspaces & "<td valign><b>Space Id</b></td><td>" & ds.Tables(0).Rows(0).Item("spc_id") & "</td></tr><tr><td valign><b>Site</b></td><td>" & ds.Tables(0).Rows(0).Item("lcm_name") & "</td></tr><tr><td valign=top><b>Tower/Building</b></td><td>" & ds.Tables(0).Rows(0).Item("TWR_NAME") & "</td></tr><tr><td valign=top><b>Floor</b></td><td> " & ds.Tables(0).Rows(0).Item("flr_name") & "</td></tr> <TR><td><b>Employee Id</b></td><TD>" & ds.Tables(0).Rows(0).Item("AUR_ID") & "</TD></tr>"
                strspaces = strspaces & "  <TR><td><b>Name</b></td><TD>" & ds.Tables(0).Rows(0).Item("aur_known_as") & "</TD></tr><TR><td><b>Email</b></td><TD>" & ds.Tables(0).Rows(0).Item("AUR_EMAIL") & "</TD></tr><TR><td><b>Department</b></td><TD>" & ds.Tables(0).Rows(0).Item("AUR_PRJ_CODE") & "</TD></tr></table>"""
                strspaces = strspaces & ",""imgurl"":""" & ds.Tables(0).Rows(0).Item("IMGURL") & """},"

                strbox_bounds = ds.Tables(0).Rows(0).Item("BBOX")
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                filter = ds.Tables(1).Rows(0).Item("flr_id")
            End If
        Next
        strspaces = strspaces.ToString.Substring(0, strspaces.Length - 1)
        Dim strResponse As String = ""
        If File.Exists(MapPath("ViewMapSE.txt")) Then
            strResponse = File.ReadAllText(MapPath("ViewMapSE.txt"))
        End If
        strResponse = Replace(strResponse, "@@data", "[" & strspaces & "]")
        strResponse = Replace(strResponse, "@@box_bounds", strbox_bounds)
        strResponse = Replace(strResponse, "@@filter", filter)
        strResponse = Replace(strResponse, "@@tenant", Session("tenant"))
        litmap.Text = strResponse
    End Sub
End Class

