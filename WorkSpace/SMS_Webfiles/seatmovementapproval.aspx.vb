﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_seatmovementapproval
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim verticalreqid As String
    Dim strRedirect As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If

        If Not IsPostBack Then
            approvaldetails.Visible = False
            BindSpacedetails()
            loadVertical()
            objsubsonic.Binddropdown(ddlCity, "GET_ALLCITY", "CTY_NAME", "CTY_CODE")
            objsubsonic.Binddropdown(ddltocity, "GET_ALLCITY", "CTY_NAME", "CTY_CODE")
        End If
    End Sub
    Public Sub loadVertical()


        Dim UID As String = ""
        UID = Session("uid")

        Dim param(0) As SqlParameter

        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = UID

        ObjSubsonic.Binddropdown(ddlVertical, "GET_COSTCENTER_AURID", "COST_CENTER_NAME", "COST_CENTER_CODE", param)


    End Sub
    Private Sub BindSpacedetails()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")
        objsubsonic.BindGridView(gvitems, "GET_SEAT_MOVEMENT_APPROVAL", param)
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindSpacedetails()
    End Sub

    Protected Sub gvitems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "View" Then
            approvaldetails.Visible = True
            Dim lnkreq As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkreq.NamingContainer, GridViewRow)
            lblstore.Text = lnkreq.Text
            Binddetails(lblstore.Text)
            BindAvailspaces(lblstore.Text)
        End If
    End Sub
    Private Sub Binddetails(ByVal reqid As String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SEAT_MOVEMENT_APPROVAL_REQID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CITY")).Selected = True
            BindLocation(ddlCity.SelectedItem.Value, ddlSelectLocation)
            ddlSelectLocation.ClearSelection()
            ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("LOCATION")).Selected = True
            ddltocity.ClearSelection()
            ddltocity.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_CITY")).Selected = True
            BindLocation(ddltocity.SelectedItem.Value, ddltoloc)
            ddltoloc.ClearSelection()
            ddltoloc.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_LOCATION")).Selected = True
            ddlvertical.ClearSelection()
            ddlvertical.Items.FindByValue(ds.Tables(0).Rows(0).Item("VERTICAL")).Selected = True
            txtFrmDate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
            txtToDate.Text = ds.Tables(0).Rows(0).Item("TO_DATE")
            starttimehr.ClearSelection()
            starttimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_HOUR")).Selected = True
            starttimemin.ClearSelection()
            starttimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_MINUTE")).Selected = True
            endtimehr.ClearSelection()
            endtimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_HOUR")).Selected = True
            endtimemin.ClearSelection()
            endtimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_MINUTE")).Selected = True
        End If


    End Sub
    Private Sub BindLocation(ByVal city As String, ByVal ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = city
        objsubsonic.Binddropdown(ddl, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Private Sub BindAvailspaces(ByVal reqid As String)
        Dim param(1) As SqlParameter

        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(1).Value = reqid

        objsubsonic.BindGridView(gdavail, "GET_SEAT_MOVEMENT_APPROVAL_REQID", param)
    End Sub


    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        updateseatmovement(6)

        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("214") & "&rid=" & clsSecurity.Encrypt(lblstore.Text)
GVColor:


        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If


    End Sub
    Private Sub updateseatmovement(ByVal mode As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"APPROVE_sEAT_MOVEMENT")
        sp.Command.AddParameter("@REQ_ID", lblstore.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@STA_ID", mode, DbType.Int32)
        sp.ExecuteScalar()



    End Sub

    Protected Sub btnreject_Click(sender As Object, e As EventArgs) Handles btnreject.Click
        updateseatmovement(2)
        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("187") & "&rid=" & clsSecurity.Encrypt(verticalreqid)
GVColor:


        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs) Handles btncancel.Click
        approvaldetails.Visible = False
        BindSpacedetails()
    End Sub
End Class
