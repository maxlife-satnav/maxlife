﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="repEmployeeUnoccupied.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_repEmployeeUnoccupied" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
        
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Location Wise Un-occupied Employee Report
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                         <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server"
                            CssClass="alert alert-danger"
                            ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select City</label>
                                        <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select  City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location</label>
                                        <asp:RequiredFieldValidator ID="rfvlocation" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Please Select  Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>     
                                       <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Tower</label>
                                        <asp:RequiredFieldValidator ID="rfvtower" runat="server" ControlToValidate="ddlTower"
                                            Display="None" ErrorMessage="Please Select Tower" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlTower" runat="server" 
                                                CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Floor</label>
                                        <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlFloor"
                                            Display="None" ErrorMessage="Please Select  Floor" InitialValue="-- Select --"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>         

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="View Report" ValidationGroup="Val1" />
                                                
                                </div>
                            </div>
                        </div>
                        <div class="row">&nbsp</div>
                         <div class="form-group">
                                <div class="col-md-12">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                </div>
                            </div>
                       
                      
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</body>
</html>
