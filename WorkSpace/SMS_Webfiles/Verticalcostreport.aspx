﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Verticalcostreport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Verticalcostreport" Title="Vertical cost Report" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('.date').datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend><%= Session("Parent")%> Cost Report  
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <asp:Label ID="Label2" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select <%= Session("Parent")%> </label>
                                        <asp:RequiredFieldValidator ID="rfvver" runat="server" ValidationGroup="Val1" InitialValue="0" Display="None" ControlToValidate="ddlvertical">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlvertical" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="row">
                                        <asp:Button ID="btnsubmit" runat="server" Text="View Report" CausesValidation="true" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"></asp:Button>
                                        <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/WorkSpace/SMS_WebFiles/frmRepMasters.aspx"
                                            Text="Back" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>&nbsp</div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
