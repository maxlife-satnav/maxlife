Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_Vertical_Escalation
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Text = ""

        lblHeader.Text = Session("Parent")
        lblVertical.Text = Session("Parent") + " Block Requisition By"
        rfvrm.ErrorMessage = "Please Select " + lblVertical.Text

        lblVerticalconfirm.Text = Session("parent") + " Block Confirmation By"
        rfvhr.ErrorMessage = "Please Select " + lblVerticalconfirm.Text

        lblVerticalrelse.Text = Session("parent") + " Release By"
        rfvhr.ErrorMessage = "Please Select " + lblVerticalrelse.Text

        If Not IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            lblMsg.Text = ""
            BindGrid()
            Binddropdown()
            btnModify.Visible = False
        End If
    End Sub

    Private Sub Binddropdown()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ROLES_VERTICAL_MATRIX")
        Dim Ds As New DataSet
        Ds = sp.GetDataSet
        RoleDropDowns(ddlVerticalBlockRequest, Ds, "ROL_DESCRIPTION", "ROL_ID")
        RoleDropDowns(ddlVerticalBlockConfirmation, Ds, "ROL_DESCRIPTION", "ROL_ID")
        RoleDropDowns(ddlVerticalRelease, Ds, "ROL_DESCRIPTION", "ROL_ID")
    End Sub

    Private Function Submitdata() As Integer


        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@VERT_REQ", SqlDbType.NVarChar, 200)
        param(0).Value = ddlVerticalBlockRequest.SelectedValue
        param(1) = New SqlParameter("@VERT_BLOC_CONF", SqlDbType.NVarChar, 200)
        param(1).Value = ddlVerticalBlockConfirmation.SelectedValue
        param(2) = New SqlParameter("@VERT_RELEASE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlVerticalRelease.SelectedValue
        param(3) = New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)
        param(3).Value = ddlStatus.SelectedValue
        param(4) = New SqlParameter("@REM", SqlDbType.NVarChar, 200)
        param(4).Value = txtRemarks.Text
        param(5) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
        param(5).Value = Session("Uid").ToString().Trim()
        Dim Flag As Integer = ObjSubsonic.GetSubSonicExecute("INSERT_VERTICAL_MATRIX_DETAILS", param)
        If Flag = 1 Then
            lblMsg.Text = "Selected " + Session("parent") + " Requisition Already Exists"
        ElseIf Flag = 0 Then
            lblMsg.Text = "Data Inserted Succesfully"


        End If
        Return Flag


    End Function

    Private Sub Modifydata()
        Try
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@VERT_REQ", SqlDbType.NVarChar, 200)
            param(0).Value = ddlVerticalBlockRequest.SelectedValue
            param(1) = New SqlParameter("@VERT_BLOC_CONF", SqlDbType.NVarChar, 200)
            param(1).Value = ddlVerticalBlockConfirmation.SelectedValue
            param(2) = New SqlParameter("@VERT_RELEASE", SqlDbType.NVarChar, 200)
            param(2).Value = ddlVerticalRelease.SelectedValue
            param(3) = New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)
            param(3).Value = ddlStatus.SelectedValue
            param(4) = New SqlParameter("@REM", SqlDbType.NVarChar, 200)
            param(4).Value = txtRemarks.Text
            param(5) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
            param(5).Value = Session("Uid").ToString().Trim()
            ObjSubsonic.GetSubSonicExecute("UPDATE_VERTICAL_MATRIX", param)
            lblMsg.Text = "Data Updated Successully"

        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindGrid()
        ObjSubsonic.BindGridView(gvitems, "GET_VERTICAL_MATRIX_BINDGRID")
    End Sub

    Private Sub Cleardata()
        ddlVerticalBlockRequest.SelectedIndex = 0
        ddlVerticalBlockConfirmation.SelectedIndex = 0
        ddlVerticalRelease.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        txtRemarks.Text = ""
        'lblMsg.Text = ""
    End Sub

    Private Sub RoleDropDowns(ByRef cboCombo As DropDownList, ByVal reader As DataSet, ByVal TxtField As String, ByVal ValField As String)
        cboCombo.DataSource = reader
        cboCombo.DataTextField = "ROL_DESCRIPTION"
        cboCombo.DataValueField = "ROL_ID"
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Submitdata()
        BindGrid()
        Cleardata()
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx")
    End Sub

    Protected Sub gvitems_SelectedIndexChanged(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        Modifydata()
        BindGrid()
        Cleardata()

    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Cleardata()
    End Sub

    Protected Sub rbActions_CheckedChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            lblMsg.Text = ""
            Cleardata()
            btnsubmit.Visible = True
            btnModify.Visible = False
        Else
            lblMsg.Text = ""
            Cleardata()
            btnsubmit.Visible = False
            btnModify.Visible = True
        End If
    End Sub

    Protected Sub ddlVerticalBlockRequest_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVerticalBlockRequest.SelectedIndexChanged
        Try
            lblMsg.Text = ""
            If rbActionsModify.Checked = True Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_DROPDOWN_BY_VER_BLOCK_REQ")
                sp.Command.AddParameter("@REQ_ID", ddlVerticalBlockRequest.SelectedValue, DbType.String)
                Dim Ds As New DataSet
                Ds = sp.GetDataSet
                ddlVerticalBlockConfirmation.SelectedValue = Ds.Tables(0).Rows(0)("VBA")
                ddlVerticalRelease.SelectedValue = Ds.Tables(0).Rows(0)("VR")
                ddlStatus.SelectedValue = Ds.Tables(0).Rows(0)("VM_STA_ID")
                txtRemarks.Text = Ds.Tables(0).Rows(0)("VM_REM")
            Else
                ''Cleardata()
                'Binddropdown()

            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
            lblMsg.Text = "No data Found with the Request"
        End Try
    End Sub

    Protected Sub gvItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvitems.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            'e.Row.Cells(0).Text = Session("Child") + " Name"

            e.Row.Cells(0).Text = Session("Parent") + " Block Requisition By"
            e.Row.Cells(1).Text = Session("Parent") + " Block Confirmation By "
            e.Row.Cells(2).Text = Session("Parent") + " Release By"
            e.Row.Cells(3).Text = " Status"
        End If
    End Sub

End Class
