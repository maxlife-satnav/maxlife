﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Threading.Tasks
Partial Class WorkSpace_SMS_Webfiles_BubbleViewMap_Shift
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetMap()
        End If
    End Sub

    Private Sub GetMap()
        '?lcm_code=PSN&twr_code=NA&flr_code=GF&spc_id=''&Date=12/15/2012

        Dim LCM_CODE As String = ""
        Dim TWR_CODE As String = ""
        Dim FLR_CODE As String = ""
        Dim SHIFT_CODE As String = ""
        Dim FLR_USR_MAP As String = ""
        Dim spc_id As String = ""

        LCM_CODE = Request.QueryString("lcm_code")
        TWR_CODE = Request.QueryString("twr_code")
        FLR_CODE = Request.QueryString("flr_code")
        SHIFT_CODE = Request.QueryString("shift_code")

        spc_id = Request.QueryString("spc_id")

        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = LCM_CODE
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = TWR_CODE
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = FLR_CODE
        param(3) = New SqlParameter("@SHIFT_CODE", SqlDbType.NVarChar, 200)
        param(3).Value = SHIFT_CODE
        param(4) = New SqlParameter("@TID", SqlDbType.NVarChar, 200)
        param(4).Value = Session("Tenant")
        Dim ds As New DataSet
        'ds = objsubsonic.GetSubSonicDataSet("SP_BBox", param)

        ds = objsubsonic.GetSubSonicDataSet("SP_BBox_WITHMAPTXT_ShiftWise", param)
        Dim strspaces As String = ""
        Dim strbox_bounds As String = ""
        'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
        Dim rowcount As Integer
        Dim spcid As String = ""
        rowcount = ds.Tables(0).Rows.Count

        Dim strResponse As String = ""
        If rowcount > 0 Then
            strResponse = ds.Tables(0).Rows(0).Item("MAP")
        End If
        litmap.Text = strResponse
    End Sub
End Class
