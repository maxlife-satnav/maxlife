<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="ConsolidateReport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_ConsolidateReport"
    Title="Consolidated Space Report" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend><%= Session("Child")%>  Allocation Report  
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" CssClass="col-md-12 control-label" ForeColor="Red" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtstore" runat="server" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="txtstore1" runat="server" Visible="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select City<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvloc" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">

                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                                </div>
                            </div>
                        </div>
                        <div id="totsumry" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Total Workstations :</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblFWST" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Total Half Cabins : </label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblFHCB" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Total Full Cabins : </label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblFFCB" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Total</label>
                                            <div class="col-md-7">
                                                <asp:Label ID="lblWFHSeats" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

