Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repTowerwiseExtension
    Inherits System.Web.UI.Page
    Dim objMasters As clsMasters
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            objMasters = New clsMasters()
            objMasters.Bindlocation(ddlBuildings)
            ddlBuildings.Items(0).Text = "--All Locations--"
            ddlTower.Items.Insert(0, "--All Towers--")
            bindextdata()
            ddlTower.SelectedIndex = 0
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        bindextdata()

    End Sub
    Public Sub bindextdata()
        Dim rds As New ReportDataSource()
        rds.Name = "TowerExtensionDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/TowerExtensionReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim Mtower As String = ""
        Dim Mbdg As String = ""


        If ddlTower.SelectedValue = "--All Towers--" Then
            Mtower = " "
        Else
            Mtower = ddlTower.SelectedItem.Value
        End If

        If ddlBuildings.SelectedValue = "--All Locations--" Then
            Mbdg = " "
        Else
            Mbdg = ddlBuildings.SelectedValue
        End If

        Dim spVertical As New SqlParameter("@VC_TWRID", SqlDbType.NVarChar, 50)
        spVertical.Value = Mtower

        Dim spbuilding As New SqlParameter("@VC_BUILDING_ID", SqlDbType.NVarChar, 50)
        spbuilding.Value = Mbdg

        Dim spsortexp As New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 50)
        spsortexp.Value = "SE.ssa_spc_id"

        Dim spsortdir As New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 50)
        spsortdir.Value = "ASC"


        Dim dtProjectA As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_REPORTS_TOWER_EXTENSION", spVertical, spbuilding, spsortexp, spsortdir)

        rds.Value = dtProjectA
    End Sub
    Protected Sub ddlBuildings_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBuildings.SelectedIndexChanged
        ReportViewer1.Visible = False
        obj.bindTower_Locationwise(ddlTower, ddlBuildings.SelectedValue.ToString)
        ddlTower.Items(0).Text = "--All Towers--"
        'ddlTower.Items(0).Value = ""
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class