<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CalendarReportDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_CalendarReportDetails"
    Title="Calendar Report in detail " %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
.hidden
{
    display: none;
} 
/*GridView---------------------------------------------*/

.gridview
{
width: 100%;
border: 1px solid black;
background: white;
text-align: center;

}
.gridview th
{
text-align: center;
background: #013b82;
color: white;
}
.gridview .pager
{
text-align: center;
background: #013b82;
color: White;
font-weight: bold;
border: 1px solid #013b82;
}
.gridview .pager a
{
color: #666;
}

.gridview a
{
text-decoration: none;
color: White;
}
.gridview a:hover
{
color: Silver;
}
.gridview .sortedasc
{
background-color: #336699;
}
.gridview .sortedasc a
{
padding-right: 15px;
background-image :url(../images/up_arrow.png);
background-repeat: no-repeat;
background-position: right center;
}
.gridview .sortedasc a:hover
{
color:White;
}
.gridview .sorteddesc
{
background-color: #336699;
}
.gridview .sorteddesc a
{
padding-right: 15px;
background-image :url(../images/down_arrow.png);
background-repeat: no-repeat;
background-position: right center;
}
.gridview .sorteddesc a:hover
{
color:White;
}
.gridview .alternating
{
background: #d8d8d8;
}

/*-----------------------------------------------------*/

/*SubGridView------------------------------------------*/
.subgridview
{
width:80%;
border: none;
text-align:left;
margin: 0px 0px 5px 25px;
background: whitesmoke;
}
.subgridview th
{
background: silver;
color: Black;
}
/*-----------------------------------------------------*/ 
</style>

    <script type="text/javascript">

  function gvrowtoggle(row) {
    try {
      row_num = row; //row to be hidden
      ctl_row = row - 1; //row where show/hide button was clicked
      rows = document.getElementById('<%= gvWeekly.ClientID %>').rows; 
     
      rowElement = rows[ctl_row]; //elements in row where show/hide button was clicked
      img = rowElement.cells[0].firstChild; //the show/hide button

      if (rows[row_num].className !== 'hidden') //if the row is not currently hidden 
						//(default)...
      {
        rows[row_num].className = 'hidden'; //hide the row
        img.src = '../../Images/detail.gif'; //change the image for the show/hide button
      } 
      else {
     
        rows[row_num].className = ''; //set the css class of the row to default 
				//(to make it visible)
        img.src = '../../Images/close.gif'; //change the image for the show/hide button
      } 
    } 
    catch (ex) {alert(ex) }
  }
    </script>

    <style type="text/css">
        .Initial
        {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../../Images/InitialImage.png") no-repeat right top;
            color: Black;
            font-weight: bold;
        }
        .Initial:hover
        {
            color: White;
            background: url("../../Images/SelectedButton.png") no-repeat right top;
        }
        .Clicked
        {
            float: left;
            display: block;
            background: url("../../Images/SelectedButton.png") no-repeat right top;
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }
    </style>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Calendar Report in detail
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Calendar Report in detail</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
            
				
				   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
				
				<table width="100%" align="center">
                    <tr>
                        <td>
                            <asp:Button Text="Today" BorderStyle="None" ID="btnToday" CssClass="Initial" runat="server"
                                OnClick="btnToday_Click" />
                            <asp:Button Text="Daily" BorderStyle="None" ID="btnDaily" CssClass="Initial" runat="server"
                                OnClick="btnDaily_Click" />
                            <asp:Button Text="Weekly" BorderStyle="None" ID="btnWeekly" CssClass="Initial" runat="server"
                                OnClick="btnWeekly_Click" />
                            <asp:Button Text="Monthly" BorderStyle="None" Visible="false" ID="btnMonthly" CssClass="Initial"
                                runat="server" OnClick="btnMonthly_Click" />
                            <asp:MultiView ID="MainView" runat="server">
                                <asp:View ID="View1" runat="server">
                                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvToday" runat="server"  EmptyDatatext="No records found." AutoGenerateColumns="False" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="From Time" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                                            ItemStyle-Width="20">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTime" runat="server" Text='<%# Eval("FROM_TIME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="To Time" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                                            ItemStyle-Width="20">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTOTIME" runat="server" Text='<%# Eval("TO_TIME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTOTIME" runat="server" Text='<%# Eval("SSA_VERTICAL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Space(s) Allocated">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCNT" runat="server" Text='<%# Eval("CNT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                        <tr>
                                            <td align="left" bgcolor="#cccccc" style="border-width: 1px; border-color: #666;
                                                border-style: solid">
                                                <table width="20%" align="left">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:ImageButton ID="PrevDay" ToolTip="Previous Day" ImageUrl='~/Images/triangle_blue_left.gif'
                                                                runat="server" />
                                                        </td>
                                                        <td align="center" nowrap="nowrap">
                                                            <b>
                                                                <asp:Label ID="lbldate"  runat="server"></asp:Label></b>
                                                        </td>
                                                        <td align="left">
                                                            <asp:ImageButton ID="NextDay" ToolTip="Next Day" ImageUrl='~/Images/triangle_blue_right.gif'
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvDaily" runat="server"   EmptyDatatext="No records found."  AutoGenerateColumns="False" Width="100%">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="From Time" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                                            ItemStyle-Width="20">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTime" runat="server" Text='<%# Eval("FROM_TIME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="To Time" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                                            ItemStyle-Width="20">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTOTIME" runat="server" Text='<%# Eval("TO_TIME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vertical">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTOTIME" runat="server" Text='<%# Eval("SSA_VERTICAL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Space(s) Allocated">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCNT" runat="server" Text='<%# Eval("CNT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View3" runat="server">
                                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                        <tr>
                                            <td align="left" bgcolor="#cccccc" style="border-width: 1px; border-color: #666;
                                                border-style: solid">
                                                <table width="20%" align="left">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:ImageButton ID="PrevWeek" ToolTip="Previous Week" ImageUrl='~/Images/triangle_blue_left.gif'
                                                                runat="server" />
                                                        </td>
                                                        <td align="center" nowrap="nowrap">
                                                            <b>
                                                                <asp:Label ID="lblWeek" runat="server"></asp:Label><asp:Label ID="lblweekday" Visible="false"
                                                                    runat="server"></asp:Label></b>
                                                        </td>
                                                        <td align="left">
                                                            <asp:ImageButton ID="NextWeek" ToolTip="Next Week" ImageUrl='~/Images/triangle_blue_right.gif'
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvWeekly" runat="server" AutoGenerateColumns="false" Width="100%"
                                                    AlternatingRowStyle-CssClass="alternating" FooterStyle-CssClass="footer"   EmptyDatatext="No records found.">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="1" ItemStyle-Width="5">
                                                            <ItemTemplate>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Day" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                                            ItemStyle-Width="20">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDay" runat="server" Text='<%#Eval("Value") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Date" ItemStyle-Wrap="false" ItemStyle-BackColor="#cccccc"
                                                            HeaderStyle-Width="20" ItemStyle-Width="20">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbldate1" runat="server" Text='<%#Eval("text") %>'></asp:Label>
                                                                 
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View4" runat="server">
                                    <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                        <tr>
                                            <td>
                                                <br />
                                                <br />
                                                <h3>
                                                    Monthly
                                                </h3>
                                                <br />
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                </table>

	 </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</asp:Content>
