Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class WorkSpace_SMS_Webfiles_BuildingCost
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Get_Building()
            ddlloc.Items(0).Text = "--All Locations--"
            'txtdate.Text = DateTime.Now.Date
            BindGrid()
            'btnExport.Visible = False
        End If
        'txtdate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub Get_Building()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EFM_SRQ_BUILDING")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlloc.DataSource = sp.GetDataSet()
        ddlloc.DataTextField = "BDG_NAME"
        ddlloc.DataValueField = "BDG_ADM_CODE"
        ddlloc.DataBind()
        ddlloc.Items(0).Text = "--All Locations--"
        ddlloc.Items(0).Value = ""
        
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        BindGrid()

        
    End Sub
    Private Sub BindGrid()

        Dim rds As New ReportDataSource()
        rds.Name = "LocationCostDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/LocationCostReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p2 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim location As String = ""
        Dim tilldate As String = ""

        If ddlloc.SelectedValue = "--All Locations--" Then
            location = ""
        Else
            location = ddlloc.SelectedValue
        End If



        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_BUILDING_WISE_COST")
        sp.Command.AddParameter("@BUILDING", location, DbType.String)
        sp.Command.AddParameter("@DATE", tilldate, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        rds.Value = ds.Tables(0)
    End Sub


    Protected Sub ddlloc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlloc.SelectedIndexChanged
        ReportViewer1.Visible = False

    End Sub
End Class
