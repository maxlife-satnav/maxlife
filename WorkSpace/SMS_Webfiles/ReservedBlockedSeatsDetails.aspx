<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReservedBlockedSeatsDetails.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_ReservedBlockedSeatsDetails" Title="Allocate Reserve Seats" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Allocate Reserve Seats Details
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="Label1" runat="server" Text="" class="col-md-5 control-label"></asp:Label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="lblVerReqID" runat="server" class="col-md-5 control-label" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Status of the Requisition</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="lblReqStatus" runat="server" class="col-md-5 control-label" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select seat Type </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlspacetype"
                                            Display="None" ErrorMessage="Please Select Space Type" InitialValue="--Select--">
                                        </asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlspacetype" runat="server" CssClass="selectpicker"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">City</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" runat="server">
                                        <label class="col-md-5 control-label">Location</label>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlSelectLocation" runat="server" CssClass="selectpicker">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelVertical" runat="server" Text="" class="col-md-5 control-label"></asp:Label>
                                        <asp:RequiredFieldValidator ID="rfvVTl" runat="server"
                                            ControlToValidate="ddlVertical" Display="None" ErrorMessage="Please Select Vertical"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlVertical" runat="server" CssClass="selectpicker">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="trTimeSlot" runat="server" visible="false" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="starttimehr" runat="server" CssClass="selectpicker">
                                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                            </asp:DropDownList><asp:DropDownList ID="starttimemin" runat="server" CssClass="selectpicker">
                                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList Enabled="false" ID="endtimehr" runat="server" CssClass="selectpicker">
                                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                            </asp:DropDownList><asp:DropDownList ID="endtimemin" runat="server" CssClass="selectpicker">
                                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvEnter" runat="server" AutoGenerateColumns="False"
                                    PageSize="1" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="From Date">
                                            <ItemTemplate>
                                                <asp:DropDownList Visible="false" ID="ddlMonth" runat="server" CssClass="selectpicker">
                                                    <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="January"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="February"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="March"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="April"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="June"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="July"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="August"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="September"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="October"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="November"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="December"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtMonth" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <ItemTemplate>
                                                <asp:DropDownList Visible="false" ID="ddlYear" runat="server" CssClass="selectpicker">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtYear" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Work Stations Required">
                                            <ItemTemplate>
                                                <div onmouseover="Tip('Enter number of  workstations required')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtWorkstations" CssClass="form-control" runat="server" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="cvwrkstations" runat="server" ControlToValidate="txtWorkstations"
                                                    Display="None" ErrorMessage="Please enter numerics in Work stations !" Operator="DataTypeCheck"
                                                    Type="Integer"></asp:CompareValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Half Cabins Required">
                                            <ItemTemplate>
                                                <div onmouseover="Tip('Enter number of  Halfcabins required/For every 60 Work Stations 4 Half cabins entitled')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtHalfcabinsrequired" CssClass="form-control" runat="server" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="cvhc" runat="server" ControlToValidate="txtHalfcabinsrequired"
                                                    Display="None" ErrorMessage="Please enter numerics in half cabins !" Operator="DataTypeCheck"
                                                    Type="Integer"></asp:CompareValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Cabins Required">
                                            <ItemTemplate>
                                                <div onmouseover="Tip('Enter number of  full cabins required/For every 60 Work Stations 1 Full cabin entitled')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtFullCabinsRequired" CssClass="form-control" runat="server" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="cvfc" runat="server" ControlToValidate="txtFullCabinsRequired"
                                                    Display="None" ErrorMessage="Please enter numerics in full cabins !" Operator="DataTypeCheck"
                                                    Type="Integer"></asp:CompareValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Space Preference in Sqft (if any)</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtLabSpace" Enabled="false" runat="server" CssClass="form-control" MaxLength="10">0</asp:TextBox>
                                            <asp:CompareValidator ID="cvfc" runat="server" ControlToValidate="txtLabSpace" Display="None"
                                                ErrorMessage="Please Enter Numerics in Lab Space" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <h4>
                                            <label class="col-md-12 control-label">Seats from same location</label></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gdavail" runat="server" AllowPaging="true" PageSize="5" AutoGenerateColumns="False"
                                    CssClass="table table-condensed table-bordered table-hover table-striped" EmptyDataText="No Seats found">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" Checked="true" runat="server" Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SPC_BDG_ID" HeaderText="Location Name" SortExpression="SPC_BDG_ID" />
                                        <asp:TemplateField HeaderText="Blocked Spaces">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspcid" runat="server" Text='<%# Eval("spc_id").ToString()%>'>
                                                </asp:Label>
                                                <asp:Label ID="lblSpaceType" runat="server" Visible="false" Text='<%# Eval("spc_layer").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFROMTIME" runat="server" Text='<%# Eval("FROMTIME").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTIME" runat="server" Text='<%# Eval("TOTIME").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <h4>
                                            <label class="col-md-12 control-label">Allocate to other Vertical</label></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvSpaceAllocatedtoOtherVertical" EmptyDataText="No Seats found"
                                    runat="server" AllowPaging="true" PageSize="5" AutoGenerateColumns="False"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" Checked="true" Enabled="false" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SPC_BDG_ID" HeaderText="Location Name" SortExpression="SPC_BDG_ID" />
                                        <asp:TemplateField HeaderText="Blocked Spaces">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspcid" runat="server" Text='<%# Eval("spc_id").ToString()%>'>
                                                </asp:Label>
                                                <asp:Label ID="lblSpaceType" runat="server" Visible="false" Text='<%# Eval("spc_layer").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks</label>
                                        <asp:CustomValidator ID="cvRemarks" runat="server"
                                            ClientValidationFunction="maxLength" ControlToValidate="txtRemarks" Display="None"
                                            ErrorMessage="Remarks Shoud be less than 500 characters !"></asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please  enter Remarks !"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter remarks up to 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" Enabled="false" runat="server" CssClass="form-control"
                                                    TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary custom-button-color"
                                        Text="Allocate Reserve Seats" />
                                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"
                                        CausesValidation="False" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


