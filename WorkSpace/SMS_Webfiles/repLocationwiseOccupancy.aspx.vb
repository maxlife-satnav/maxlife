Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repLocationwiseOccupancy
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim MLocation As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") Is Nothing Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False
        If Not Page.IsPostBack Then
            ReportViewer1.Visible = False
            obj.bindLocation(ddlReqID)
            ddlReqID.Items(0).Text = "--All Locations--"
            binddata_Locationwise(MLocation, 7)
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        If ddlReqID.SelectedValue = "--All Locations--" Then
            MLocation = ""
        Else
            MLocation = ddlReqID.SelectedItem.Value

        End If
        binddata_Locationwise(MLocation, 7)
    End Sub

    Public Sub binddata_Locationwise(ByVal vc_LocationName As String, ByVal vc_StatusID As Integer)
        Dim sp1 As New SqlParameter("@vc_LocationName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = vc_LocationName
        Dim sp2 As New SqlParameter("@vc_StatusID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp2.Value = vc_StatusID
        Dim sp3 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
        sp3.Value = "SSA.SSA_SPC_ID"
        Dim sp4 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
        sp4.Value = "Asc"
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_LocationWise", sp1, sp2, sp3, sp4)
        
        Dim rds As New ReportDataSource()
        rds.Name = "LocationOccupancyDS"
        rds.Value = dt
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/LocationOccupancyReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMPLOYEELOC_COUNT")
        sp.Command.AddParameter("@LCM_CODE", vc_LocationName, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltot.Visible = True
            lbltot.Text = "Total Employees : " & ds.Tables(0).Rows(0).Item("EMPLOYEE_ID")
            lbltot.ForeColor = Drawing.Color.Black
            lbltot.Font.Bold = True

        End If
        lbltot.Visible = True
    End Sub

    Protected Sub ddlReqID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReqID.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class