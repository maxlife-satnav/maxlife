Imports clsEmpMapping
Imports System.Configuration
Imports System.Data
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class SpaceManagement_frmSpaceVerticalAllocations
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim objEmp As New clsEmpMapping
    Dim Email As String = String.Empty
    Dim strTower As String = String.Empty
    Dim strRedirect As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Visible = False
        ' lblMsg.Text = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
        ' lblMsg.Visible = True
        txtFdate.Attributes.Add("readonly", "readonly")
        txtTodate.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            Try
                lblSelVertical.Text = Session("Parent")

                lblSelCostcenter.Text = Session("Child")
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
                param(0).Value = Session("uid")
                objsubsonic.Binddropdown(ddlReq, "usp_bindSpaceData_ALLOCATE", "SRN_REQ_ID1", "SRN_REQ_ID", param)
                'objEmp.BindData(ddlReq)
                
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Allocation", "Load", ex)
            End Try
        End If
        For Each _listItem As ListItem In Me.ddlReq.Items
            _listItem.Attributes.Add("title", _listItem.Text)
        Next
    End Sub

    Protected Sub ddlReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReq.SelectedIndexChanged
        Try
            If ddlReq.SelectedIndex = 0 Then
                clearFields()
                gvWs.DataBind()
                gvHC.DataBind()
                gvFC.DataBind()
                lblFCCount.Visible = False
                lblHCCount.Visible = False
                lblWSCount.Visible = False
                lblSelecetedFC.Visible = False
                lblSelectedHC.Visible = False
                lblSelectedWs.Visible = False
                lblPrjName.Text = String.Empty
                lblVertName.Text = String.Empty
                Exit Sub
            End If
            ObjDR = objEmp.getReqDetails(ddlReq.SelectedValue, Session("uid").ToString())
            If ObjDR.HasRows() Then
                If ObjDR.Read Then
                    txtReuname.Text = ObjDR("usr_name").ToString()
                    txrDept.Text = ObjDR("dep_name").ToString()
                    txtCabReq.Text = ObjDR("Cabins_A").ToString()
                    txtCubreq.Text = ObjDR("Cubicals_A").ToString()
                    txtWSreq.Text = ObjDR("Work_Stations_A").ToString()
                    txtReqrem.Text = ObjDR("srn_rem").ToString()
                    txtAdminrem.Text = ObjDR("srn_rm_rem").ToString()
                    txtFdate.Text = ObjDR("srn_frm_dt").ToString()
                    txtTodate.Text = ObjDR("srn_to_dt").ToString()
                    txtVertical.Text = ObjDR("Vertical").ToString()
                    lblPrjName.Text = ObjDR("Prjname").ToString()
                    lblVertName.Text = ObjDR("Vertical").ToString()
                    lblvercode.Text = ObjDR("ver_code").ToString()
                    txtWSAllocate.Text = ""
                    txtCabAllocate.Text = ""
                    txtCuballocate.Text = ""
                    lblTower.Text = ObjDR("srn_bdg_one").ToString()
                End If
            End If
            lblFCCount.Visible = True
            lblHCCount.Visible = True
            lblWSCount.Visible = True
            lblSelecetedFC.Visible = True
            lblSelectedHC.Visible = True
            lblSelectedWs.Visible = True
            lblSelectedWs.Text = "Selected Work Station count is : 0"
            lblSelectedHC.Text = "Selected Half Cabin count is : 0"
            lblSelecetedFC.Text = "Selected Full Cabin count is : 0"


            'objEmp.getReqSpaces(txtVertical.Text, "WORK STATION", lstWorkStations, txtFdate.Text, txtTodate.Text)
            'objEmp.getReqSpaces(txtVertical.Text, "HALF CABIN", lstCabins, txtFdate.Text, txtTodate.Text)
            'objEmp.getReqSpaces(txtVertical.Text, "CABIN", lstFullCabins, txtFdate.Text, txtTodate.Text)

            objEmp.getReqSpaces(lblvercode.Text, "WORK STATION", gvWs, txtFdate.Text, txtTodate.Text, ddlReq.SelectedItem.Value)
            objEmp.getReqSpaces(lblvercode.Text, "HALF CABIN", gvHC, txtFdate.Text, txtTodate.Text, ddlReq.SelectedItem.Value)
            objEmp.getReqSpaces(lblvercode.Text, "FULL CABIN", gvFC, txtFdate.Text, txtTodate.Text, ddlReq.SelectedItem.Value)

            lblWSCount.Text = "of  " & gvWs.Rows.Count & ""
            lblHCCount.Text = "of  " & gvHC.Rows.Count & ""
            lblFCCount.Text = "of  " & gvFC.Rows.Count & ""
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Allocation", "Load", ex)

        End Try

    End Sub

    Private Sub clearFields()
        txtReuname.Text = ""
        txrDept.Text = ""
        txtCabReq.Text = ""
        txtCubreq.Text = ""
        txtWSreq.Text = ""
        txtReqrem.Text = ""
        txtAdminrem.Text = ""
        txtFdate.Text = ""
        txtTodate.Text = ""
        txtVertical.Text = ""
        txtWSAllocate.Text = ""
        txtCabAllocate.Text = ""
        txtCuballocate.Text = ""
        'lstWorkStations.Items.Clear()
        'lstCabins.Items.Clear()
        'lstFullCabins.Items.Clear()
    End Sub

    Protected Sub btnAllo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllo.Click
        '  Try
        If ddlReq.SelectedIndex <= 0 Then
            lblMsg.Text = "Please Select Requisition ID"
            lblMsg.Visible = True
            Exit Sub
        End If
        If CDate(txtTodate.Text) < CDate(txtFdate.Text) Then
            lblMsg.Text = "To date should be greater than from date "
            lblMsg.Visible = True
            Exit Sub
        End If
        If txtWSAllocate.Text = String.Empty And txtCuballocate.Text = String.Empty And txtCabAllocate.Text = String.Empty Then
            lblMsg.Text = "Please Enter No.Of Work Stations/Half Cabins/Full Cabins Allocated"
            lblMsg.Visible = True
            Exit Sub
        End If
        If Convert.ToInt32(txtWSAllocate.Text) <= 0 And Convert.ToInt32(txtCuballocate.Text) <= 0 And Convert.ToInt32(txtCabAllocate.Text) <= 0 Then
            lblMsg.Text = "Any one of the space should be more than zero"
            lblMsg.Visible = True
            Exit Sub
        End If
        Dim iWSCountP, iFCCountP, iHCCountP As Integer
        iWSCountP = Convert.ToInt32(txtWSreq.Text)
        iFCCountP = Convert.ToInt32(txtCabReq.Text)
        iHCCountP = Convert.ToInt32(txtCubreq.Text)
        Dim iWSCount, iFCCount, iHCCount As Integer
        If txtWSAllocate.Text <> "" Then
            iWSCount = Convert.ToInt32(txtWSAllocate.Text)
        Else
            iWSCount = 0
        End If
        If txtCabAllocate.Text <> "" Then
            iFCCount = Convert.ToInt32(txtCabAllocate.Text)
        Else
            iFCCount = 0
        End If
        If txtCuballocate.Text <> "" Then
            iHCCount = Convert.ToInt32(txtCuballocate.Text)
        Else
            iHCCount = 0
        End If
        Dim iWSCountL, iFCCountL, iHCCountL As Integer
        iWSCountL = 0
        iFCCountL = 0
        iHCCountL = 0
        Dim iWSCountT As Integer = Convert.ToInt32(txtWSreq.Text)
        If iWSCount > iWSCountT Then
            lblMsg.Text = "You can Allocate maximum of " & iWSCountT & " Work Stations of " & txtWSAllocate.Text & " because remaining " & iWSCountP & " allocated already !"
            lblMsg.Visible = True
            Exit Sub
        End If
        Dim iFCCountT As Integer = Convert.ToInt32(txtCabReq.Text)
        If iFCCount > iFCCountT Then
            lblMsg.Text = "You can Allocate maximum of " & iFCCountT & " Full Cabins of " & txtCabReq.Text & " because remaining " & iFCCountP & " allocated already !"
            lblMsg.Visible = True
            Exit Sub
        End If
        Dim iHCCountT As Integer = Convert.ToInt32(txtCubreq.Text)
        If iHCCount > iHCCountT Then
            lblMsg.Text = "You can Allocate maximum of " & iHCCountT & " Half Cabins of" & txtCubreq.Text & " because remaining " & iHCCountP & " allocated already !"
            lblMsg.Visible = True
            Exit Sub
        End If

        For iWs As Integer = 0 To gvWs.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvWs.Rows(iWs).FindControl("chk"), HtmlInputCheckBox)
            If chk.Checked Then
                iWSCountL += 1
            End If

        Next
        For iWs As Integer = 0 To gvFC.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvFC.Rows(iWs).FindControl("chkFC"), HtmlInputCheckBox)
            If chk.Checked Then
                iFCCountL += 1
            End If

        Next
        For iWs As Integer = 0 To gvHC.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvHC.Rows(iWs).FindControl("chkHC"), HtmlInputCheckBox)
            If chk.Checked Then
                iHCCountL += 1
            End If

        Next
        If iWSCount <> iWSCountL Then
            lblMsg.Text = "Please select Exact no of Work Stations as Allocated "
            lblMsg.Visible = True
            GoTo GVColor
            Exit Sub
        End If
        If iFCCount <> iFCCountL Then
            lblMsg.Text = "Please select Exact no of Full Cabins as Allocated "
            lblMsg.Visible = True
            GoTo GVColor
            Exit Sub
        End If
        If iHCCount <> iHCCountL Then
            lblMsg.Text = "Please select Exact no of Half Cabins as Allocated "
            lblMsg.Visible = True
            GoTo GVColor
            Exit Sub
        End If
        Dim iCount As Integer = 0
        Dim intStatusID As Integer

        For iWs As Integer = 0 To gvWs.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvWs.Rows(iWs).FindControl("chk"), HtmlInputCheckBox)
            Dim lblRedid1 As Label = CType(gvWs.Rows(iWs).FindControl("lblReqid"), Label)
            If chk.Checked = True Then
                Dim sp1 As New SqlParameter("@vc_SpcID", SqlDbType.NVarChar, 50)
                sp1.Value = chk.Value
                Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_getSelectedSpaceDetails", sp1)

                iCount = iCount + 1
                Dim TEMPVAR As String
                TEMPVAR = iCount
                TEMPVAR = TEMPVAR + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
                'objEmp.insertSpacesForProject(ddlReq.SelectedValue + "/" + TEMPVAR, chk.Value, Session("uid"), getoffsetdatetime(DateTime.Now), txtFdate.Text, txtVertical.Text, "WORK STATION", txtTodate.Text, chk.Value, dt.Rows(0)("SPC_BDG_ID").ToString().Trim(), dt.Rows(0)("SPC_TWR_ID").ToString().Trim(), dt.Rows(0)("SPC_FLR_ID").ToString().Trim(), dt.Rows(0)("SPC_WNG_ID").ToString().Trim(), ddlReq.SelectedValue)
                objEmp.insertSpacesForProject(ddlReq.SelectedValue + "/" + TEMPVAR, chk.Value, Session("uid"), getoffsetdatetime(DateTime.Now), txtFdate.Text, lblvercode.Text, "WORK STATION", txtTodate.Text, chk.Value, dt.Rows(0)("SPC_BDG_ID").ToString().Trim(), dt.Rows(0)("SPC_TWR_ID").ToString().Trim(), dt.Rows(0)("SPC_FLR_ID").ToString().Trim(), dt.Rows(0)("SPC_WNG_ID").ToString().Trim(), ddlReq.SelectedValue, lblRedid1.Text)
            End If
        Next

        For iWs As Integer = 0 To gvFC.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvFC.Rows(iWs).FindControl("chkFC"), HtmlInputCheckBox)
            Dim lblRedid1 As Label = CType(gvFC.Rows(iWs).FindControl("lblReqid"), Label)
            'Dim lblRedid1 As Label = CType(gvWs.Rows(iWs).FindControl("lblReqid"), Label)
            If chk.Checked Then
                Dim sp1 As New SqlParameter("@vc_SpcID", SqlDbType.NVarChar, 50)
                sp1.Value = chk.Value
                Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_getSelectedSpaceDetails", sp1)
                iCount = iCount + 1
                Dim TEMPVAR As String
                TEMPVAR = iCount
                TEMPVAR = TEMPVAR + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
                'objEmp.insertSpacesForProject(ddlReq.SelectedValue + "/" + TEMPVAR, chk.Value, Session("uid"), getoffsetdatetime(DateTime.Now), txtFdate.Text, txtVertical.Text, "FULL CABIN", txtTodate.Text, chk.Value, dt.Rows(0)("SPC_BDG_ID").ToString().Trim(), dt.Rows(0)("SPC_TWR_ID").ToString().Trim(), dt.Rows(0)("SPC_FLR_ID").ToString().Trim(), dt.Rows(0)("SPC_WNG_ID").ToString().Trim(), ddlReq.SelectedValue)
                objEmp.insertSpacesForProject(ddlReq.SelectedValue + "/" + TEMPVAR, chk.Value, Session("uid"), getoffsetdatetime(DateTime.Now), txtFdate.Text, lblvercode.Text, "WORK STATION", txtTodate.Text, chk.Value, dt.Rows(0)("SPC_BDG_ID").ToString().Trim(), dt.Rows(0)("SPC_TWR_ID").ToString().Trim(), dt.Rows(0)("SPC_FLR_ID").ToString().Trim(), dt.Rows(0)("SPC_WNG_ID").ToString().Trim(), ddlReq.SelectedValue, lblRedid1.Text)
            End If
        Next
        For iWs As Integer = 0 To gvHC.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvHC.Rows(iWs).FindControl("chkHC"), HtmlInputCheckBox)
            Dim lblRedid1 As Label = CType(gvHC.Rows(iWs).FindControl("lblReqid"), Label)
            If chk.Checked Then
                Dim sp1 As New SqlParameter("@vc_SpcID", SqlDbType.NVarChar, 50)
                sp1.Value = chk.Value
                Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_getSelectedSpaceDetails", sp1)
                iCount = iCount + 1
                Dim TEMPVAR As String
                TEMPVAR = iCount
                TEMPVAR = TEMPVAR + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
                'objEmp.insertSpacesForProject(ddlReq.SelectedValue + "/" + TEMPVAR, chk.Value, Session("uid"), getoffsetdatetime(DateTime.Now), txtFdate.Text, txtVertical.Text, "HALF CABIN", txtTodate.Text, chk.Value, dt.Rows(0)("SPC_BDG_ID").ToString().Trim(), dt.Rows(0)("SPC_TWR_ID").ToString().Trim(), dt.Rows(0)("SPC_FLR_ID").ToString().Trim(), dt.Rows(0)("SPC_WNG_ID").ToString().Trim(), ddlReq.SelectedValue)
                objEmp.insertSpacesForProject(ddlReq.SelectedValue + "/" + TEMPVAR, chk.Value, Session("uid"), getoffsetdatetime(DateTime.Now), txtFdate.Text, lblvercode.Text, "WORK STATION", txtTodate.Text, chk.Value, dt.Rows(0)("SPC_BDG_ID").ToString().Trim(), dt.Rows(0)("SPC_TWR_ID").ToString().Trim(), dt.Rows(0)("SPC_FLR_ID").ToString().Trim(), dt.Rows(0)("SPC_WNG_ID").ToString().Trim(), ddlReq.SelectedValue, lblRedid1.Text)
            End If
        Next
        If iCount = 0 Then
            lblMsg.Text = "Please select atleast one Space!"
            lblMsg.Visible = True
            Exit Sub
        End If
        Dim sp3 As New SqlParameter("@reqid", SqlDbType.NVarChar, 50)
        sp3.Value = ddlReq.SelectedValue
        Dim dt1 As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "validatereqidprojectdetails", sp3)
        If dt1.Rows(0)("RETURNSTATUS").ToString().Trim() = "0" Then
            'If (iHCCountL + iFCCountL + iWSCountL) = iCount Then
            iCount = objEmp.insertSpacesForProjectDetails(ddlReq.SelectedValue, txtWSreq.Text, txtCabReq.Text, txtCubreq.Text, iWSCount, iFCCount, iHCCount, txtAdminrem.Text.Trim().Replace("'", "''"), Session("uid"))
            'Else
            '    iCount = objEmp.UPDATESpacesForProjectDetails(ddlReq.SelectedValue, iWSCount, iFCCount, iHCCount, txtAdminrem.Text.Trim().Replace("'", "''"), Session("uid"))
            'End If
        Else
            iCount = objEmp.UPDATESpacesForProjectDetails(ddlReq.SelectedValue, iWSCount, iFCCount, iHCCount, txtAdminrem.Text.Trim().Replace("'", "''"), Session("uid"))
        End If

        If iCount = 1 Then
            If chkForcibleAllocate.Checked = True Then
                intStatusID = 6
                iCount = objEmp.updateSpaceReq(ddlReq.SelectedValue, intStatusID)
            ElseIf txtWSreq.Text = txtWSAllocate.Text And txtCubreq.Text = txtCuballocate.Text And txtCabReq.Text = txtCabAllocate.Text Then
                intStatusID = 6
                iCount = objEmp.updateSpaceReq(ddlReq.SelectedValue, intStatusID)
            End If
        End If
        If iCount = 1 Then
            Dim sp1 As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
            sp1.Value = ddlReq.SelectedValue
            Email = SqlHelper.ExecuteScalar(Data.CommandType.StoredProcedure, "USP_SPACE_GET_APPROVEDREQ", sp1)
            ' sendMail(ddlReq.SelectedValue.ToString().Trim(), txtFdate.Text, txtTodate.Text, txtWSreq.Text, txtCubreq.Text, txtCabReq.Text, txtWSAllocate.Text, txtCuballocate.Text, txtCabAllocate.Text, txrDept.Text, Email)

            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_SPACE_ALLOC_MAIL")
            'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            'sp.Command.AddParameter("@REQID", ddlReq.SelectedValue.ToString().Trim(), DbType.String)
            'sp.Command.AddParameter("@MAILSTATUS", 12, DbType.Int32)
            'sp.ExecuteScalar()
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_SPACEREQUISITIONALLOCATION")
            sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@REQID", ddlReq.SelectedValue, DbType.String)
            sp.ExecuteScalar()
            strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("6") & "&rid=" & clsSecurity.Encrypt(ddlReq.SelectedValue)
        End If
GVColor:
        For iWs As Integer = 0 To gvFC.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvFC.Rows(iWs).FindControl("chkFC"), HtmlInputCheckBox)
            If chk.Checked Then
                gvFC.Rows(iWs).BackColor = Drawing.Color.LightGray
            Else
                gvFC.Rows(iWs).BackColor = Drawing.Color.White
            End If
        Next
        lblSelecetedFC.Text = "Selected Full Cabin count is : " & iFCCountL

        For iWs As Integer = 0 To gvWs.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvWs.Rows(iWs).FindControl("chk"), HtmlInputCheckBox)
            If chk.Checked Then
                gvWs.Rows(iWs).BackColor = Drawing.Color.LightGray
            Else
                gvWs.Rows(iWs).BackColor = Drawing.Color.White
            End If
        Next
        lblSelectedWs.Text = "Selected Work Station count is : " & iWSCountL

        For iWs As Integer = 0 To gvHC.Rows.Count - 1
            Dim chk As HtmlInputCheckBox = CType(gvHC.Rows(iWs).FindControl("chkHC"), HtmlInputCheckBox)
            If chk.Checked Then
                gvHC.Rows(iWs).BackColor = Drawing.Color.LightGray
            Else
                gvHC.Rows(iWs).BackColor = Drawing.Color.White
            End If
        Next
        lblSelectedHC.Text = "Selected  Half Cabin count is : " & iHCCountL

        '   Catch ex As Exception
        '  Throw New Amantra.Exception.DataException("This error has been occured while Updating data to Allocate Spaces.", "Space Allocation", "Load", ex)
        'Throw New Exception(ex)

        '    End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub

    'Private Sub sendMail(ByVal strReqId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal reqWs As Int16, ByVal reqHC As Int16, ByVal reqFC As Int16, ByVal AllocWs As Int16, ByVal AllocHC As Int16, ByVal AllocFC As Int16, ByVal strDept As String, ByVal strEmail As String)
    '    ' Try
    '    Dim to_mail As String = strEmail
    '    Dim cc_mail As String = Session("uemail")
    '    Dim body As String = String.Empty
    '    Dim strCC As String = String.Empty
    '    Dim strEmail1 As String = String.Empty
    '    Dim strRM As String = String.Empty
    '    Dim strRR As String = String.Empty
    '    Dim strFMG As String = String.Empty
    '    Dim objData As SqlDataReader
    '    Dim user As String = String.Empty
    '    Dim strKnownas As String = String.Empty
    '    'Dim objDataCurrentUser As SqlDataReader
    '    'Dim strCurrentUser As String = String.Empty

    '    'strCurrentUser = "select aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
    '    'objDataCurrentUser = SqlHelper.ExecuteReader(CommandType.Text, strCurrentUser)
    '    'While objDataCurrentUser.Read
    '    '    user = objDataCurrentUser("aur_known_as")
    '    'End While
    '    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '    sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '    objData = sp1.GetReader()
    '    While objData.Read
    '        strRR = objData("aur_reporting_to").ToString
    '        strRM = objData("aur_reporting_email").ToString
    '        strKnownas = objData("aur_known_as").ToString
    '        to_mail = objData("aur_email").ToString
    '        'BCC = objData("BCC").ToString
    '    End While

    '    'strCC = "select aur_reporting_to from amantra_user where aur_id= '" & Session("uid") & "'"
    '    'objData = SqlHelper.ExecuteReader(CommandType.Text, strCC)
    '    'While objData.Read
    '    '    strRR = objData("aur_reporting_to")
    '    'End While
    '    'strEmail1 = " select aur_email from amantra_user where aur_id='" & strRR & "'"
    '    'objData = SqlHelper.ExecuteReader(CommandType.Text, strEmail1)

    '    'While objData.Read
    '    '    strRM = objData("aur_email")
    '    'End While

    '    body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
    '    body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br /> Requested Project spaces has been allotted by " + user + ". The details are as follows.</td></tr></table>&nbsp;<br /> "
    '    body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '    body = body & "<table align='center'> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlReq.SelectedItem.Text & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Project Name </td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblPrjName.Text.Trim() & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Vertical Name </td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblVertName.Text.Trim() & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Tower Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblTower.Text.Trim() & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Requested Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtFdate.Text.Trim() & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & FromDate & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ToDate & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtReqrem.Text & "</td></tr></table>"
    '    body = body & "</td></tr></table><br>"
    '    body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '    body = body & "<table align='center'>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqWs & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocWs & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqHC & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Half Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocHC & "</td> </tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqFC & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Approved Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocFC & "</td></tr>"
    '    body = body & "</table>"


    '    body = body & "</td></tr></table>"
    '    body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
    '    body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
    '    body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
    '    body = body & "</table>"
    '    'If to_mail = String.Empty Then
    '    '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '    'End If
    '    'If cc_mail = String.Empty Then
    '    '    cc_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '    'End If

    '    Dim strsql As String = String.Empty
    '    Dim stVrm As String = String.Empty
    '    Dim stVrm1 As String = String.Empty
    '    Dim dr As SqlDataReader
    '    Dim dr1 As SqlDataReader
    '    Dim iQry As Integer = 0

    '    'strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & txtVertical.Text.Trim() & "'"
    '    'iQry = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL1))
    '    'If iQry > 0 Then
    '    '    strSQL1 = "Select VER_VRM from VERTICAL where VER_CODE='" & txtVertical.Text.Trim() & "'"
    '    '    dr1 = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
    '    '    While dr1.Read
    '    '        stVrm = dr1("VER_VRM").ToString()
    '    '    End While
    '    '    strsql = "Select aur_email from amantra_user where aur_id='" & stVrm & "'"
    '    '    dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
    '    '    While dr.Read
    '    '        stVrm1 = dr("aur_email").ToString()
    '    '    End While
    '    'Else
    '    '    stVrm1 = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '    'End If

    '    Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '    Dim parms1 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '    Dim parms2 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '    Dim parms3 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '    Dim parms4 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '    Dim parms5 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '    Dim parms6 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '    Dim parms7 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '    parms.Value = "Abcd"
    '    parms1.Value = body
    '    parms2.Value = to_mail
    '    parms3.Value = "Space Allocation Details"
    '    parms4.Value = getoffsetdatetime(DateTime.Now)
    '    parms5.Value = "Request Submitted"
    '    parms6.Value = "Normal Mail"
    '    parms7.Value = strRM

    '    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms1, parms2, parms3, parms4, parms5, parms6, parms7)

    '    'Dim parms9 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
    '    'parms9(0).Value = 3
    '    'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms9)
    '    'If objData.HasRows Then
    '    '    While objData.Read
    '    '        strFMG = objData("aur_email")
    '    '        Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '    '        Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '    '        Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '    '        Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '    '        Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '    '        Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '    '        Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '    '        Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '    '        parms10.Value = "Abcd"
    '    '        parms11.Value = body
    '    '        parms12.Value = strFMG
    '    '        parms13.Value = "Space Allocation Details"
    '    '        parms14.Value = getoffsetdatetime(DateTime.Now)
    '    '        parms15.Value = "Request Submitted"
    '    '        parms16.Value = "Normal Mail"
    '    '        parms17.Value = String.Empty
    '    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)

    '    '    End While
    '    'End If

    '    ' Catch ex As Exception
    '    'Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Allocation", "Load", ex)
    '    '  End Try

    'End Sub

    Protected Sub gvWs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim chkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("chk"), HtmlInputCheckBox)

            chkSelect.Attributes.Add("OnClick", "javascript:colorRow(event)")

            ''If e.Row.RowState = DataControlRowState.Alternate Then
            'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#B4CDCD';")
            ''Else
            ''    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            ''    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            ''End If
        End If
    End Sub

    Protected Sub gvHC_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim chkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("chkHC"), HtmlInputCheckBox)

            chkSelect.Attributes.Add("OnClick", "javascript:colorRowHC(event)")

            ''If e.Row.RowState = DataControlRowState.Alternate Then
            'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#B4CDCD';")
            ''Else
            ''    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            ''    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            ''End If
        End If
    End Sub

    Protected Sub gvFC_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim chkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("chkFC"), HtmlInputCheckBox)

            chkSelect.Attributes.Add("OnClick", "javascript:colorRowFC(event)")

            ''If e.Row.RowState = DataControlRowState.Alternate Then
            'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#B4CDCD';")
            ''Else
            ''    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            ''    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            ''End If
        End If
    End Sub

   
End Class
