Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmApproveLeaseExtension
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindLease_Type()
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            BindGrid()
            BindLeaseHistory()
        End If
    End Sub

    Private Sub BindLeaseHistory()
        lblMsg.Text = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LEASE_HISTORY_VPHR")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        'sp.Command.AddParameter("@EMP_ID", txtempid.Text, DbType.String)
        gvitems1.DataSource = sp.GetDataSet()
        gvitems1.DataBind()

    End Sub

    Private Sub BindLease_Type()
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
            sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlLtype.DataSource = sp3.GetDataSet()
            ddlLtype.DataTextField = "PN_LEASE_TYPE"
            ddlLtype.DataValueField = "PN_LEASE_ID"
            ddlLtype.DataBind()
            ddlLtype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LEASES_VP_HR_APPROVAL_GETDETAILSext")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@EMP_ID", txtempid.Text, DbType.String)
            gvLDetails_Lease.DataSource = sp.GetDataSet()
            gvLDetails_Lease.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        BindGrid()
    End Sub

    Protected Sub gvLDetails_Lease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetails_Lease.PageIndexChanging
        gvLDetails_Lease.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex()
        BindLeaseHistory()
    End Sub

End Class