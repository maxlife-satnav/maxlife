﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repflooravailability
    Inherits System.Web.UI.Page
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") Is Nothing Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If

        If Not Page.IsPostBack Then

            'ReportViewer1.Visible = True
            obj.bindTower(ddlReqID)
            ddlReqID.Items(0).Text = "--All Towers--"
            ddlFloor.Items.Insert(0, "--All Floors--")
            ddlFloor.SelectedIndex = 0
            binddata_floorwise()
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        binddata_floorwise()
    End Sub

    Public Sub binddata_floorwise()


        Dim rds As New ReportDataSource()
        rds.Name = "FloorAvailabilityDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/FloorAvailableReport.rdlc")

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim MTower As String = ""
        Dim MFloor As String = ""

        If ddlReqID.SelectedItem.Value = "--All Towers--" Then
            MTower = ""
        Else
            MTower = ddlReqID.SelectedItem.Value
        End If

        If ddlFloor.SelectedItem.Value = "--All Floors--" Then
            MFloor = ""
        Else
            MFloor = ddlFloor.SelectedItem.Value
        End If
        Dim sp1 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = MTower
        Dim sp2 As New SqlParameter("@vc_floorname", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = MFloor

        Dim sp3 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = "Locationname"

        Dim sp4 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp4.Value = "Asc"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Availability_Floorwise", sp1, sp2, sp3, sp4)

        rds.Value = dt
       

    End Sub

    Protected Sub ddlReqID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        obj.bindfloor(ddlFloor, ddlReqID.SelectedValue)
        ReportViewer1.Visible = False
        ddlFloor.Items(0).Text = "--All Floors--"
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class
