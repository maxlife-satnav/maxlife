<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmWorkRequestDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmWorkRequestDetails" %>

<%@ Register Src="../../Controls/WorkRequestDetails.ascx" TagName="WorkRequestDetails"
    TagPrefix="uc1" %>

<!DOCTYPE html>

<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">    
        <uc1:WorkRequestDetails ID="WorkRequestDetails1" runat="server" />       
    </form>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
