Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Net.Mail
Imports System.Web.Services
Imports System.Web
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions

Partial Class frmSpaceRequisition
    Inherits System.Web.UI.Page
    Dim objData As SqlDataReader
    Dim obj As New clsWorkSpace
    Dim clsObj As New clsMasters
    Dim dtProject As DataTable
    Dim strRedirect As String = String.Empty
    Dim strQuery As String = String.Empty

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Text = ""
        txtFromdate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            ' obj.loadtower1(ddlTower1)
            ' obj.loadrm(ddlRM)
            Try
                'obj.loadvertical(ddlVertical)
                btnSubmit.Attributes.Add("onclick", "CheckDate()")
                txtDate.Text = getoffsetdatetime(DateTime.Now).Date

                lblSelVertical.Text = Session("Parent")
                RequiredFieldValidator3.ErrorMessage = "Please Select " + lblSelVertical.Text
                lblSelCostcenter.Text = Session("Child")
                RequiredFieldValidator4.ErrorMessage = "Please Select " + lblSelCostcenter.Text
                LoadCity()
                loadVertical()
                'ddlProject.Items.Insert(0, "--Select--")
                ddlCostcenter.Items.Insert(0, "--Select--")
                txtFromdate.Attributes.Add("onClick", "displayDatePicker('" + txtFromdate.ClientID + "')")
                txtFromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                txtTodate.Attributes.Add("onClick", "displayDatePicker('" + txtTodate.ClientID + "')")
                txtTodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

                'CompareValidator1.ValueToCompare = getoffsetdatetime(DateTime.Now)
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Requisition", "Load", ex)
            End Try


        End If
    End Sub
    Public Sub LoadCity()
        clsObj.BindVerticalCity(ddlCity)
    End Sub
    Public Sub loadVertical()
       
        ObjSubSonic.Binddropdown(ddlVertical, "GET_ALL_VERTICALS", "Ver_Name", "Ver_Code")

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If

        Try
            Dim intHC As Integer = CType(txtHcabins.Text, Integer)
        Catch ex As Exception
            lblMsg.Text = "Only numbers are allowed in Half Cabins "
            lblMsg.Visible = True
            Exit Sub
        End Try
        Try
            Dim intFC As Integer = CType(txtFcabins.Text, Integer)
        Catch ex As Exception
            lblMsg.Text = "Only numbers are allowed in Full Cabins "
            lblMsg.Visible = True
            Exit Sub
        End Try

        Try
            Dim intWS As Integer = CType(txtWstations.Text, Integer)
        Catch ex As Exception
            lblMsg.Text = "Only numbers are allowed in Work Stations "
            lblMsg.Visible = True
            Exit Sub
        End Try

        If Convert.ToInt32(txtWstations.Text) <= 0 And Convert.ToInt32(txtHcabins.Text) <= 0 And Convert.ToInt32(txtFcabins.Text) <= 0 Then
            lblMsg.Text = "Any one of the space should be more than zero"
            lblMsg.Visible = True
            Exit Sub
        End If
        Dim spVC_Code As New SqlParameter("@VC_PRJ_CODE", SqlDbType.NVarChar, 50)
        'spVC_Code.Value = ddlProject.SelectedValue
        dtProject = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SP_GET_PROJECT_DETAILS", spVC_Code)

        Try
            Dim intFromDate As Date = CType(txtFromdate.Text, Date)
        Catch ex As Exception
            lblMsg.Text = "Please enter vaild From Date "
            lblMsg.Visible = True
            Exit Sub
        End Try

        Try
            Dim intToDate As Date = CType(txtTodate.Text, Date)
        Catch ex As Exception
            lblMsg.Text = "Please enter vaild To Date "
            lblMsg.Visible = True
            Exit Sub
        End Try

        If CDate(txtTodate.Text) < CDate(txtFromdate.Text) Then
            lblMsg.Text = "To date should be greater than from date "
            lblMsg.Visible = True
            Exit Sub
        ElseIf CDate(txtFromdate.Text) < CDate(txtDate.Text) Then
            lblMsg.Text = " Please enter valid from date that has to be greater than today's date "
            lblMsg.Visible = True
            Exit Sub
        ElseIf CDate(txtTodate.Text) < CDate(txtDate.Text) Then
            lblMsg.Text = "Please enter valid To date that has to be greater than today's date"
            lblMsg.Visible = True
            Exit Sub
        Else
            If txtHcabins.Text = "" Then
                txtHcabins.Text = "0"
            End If
            If txtFromdate.Text = String.Empty Or txtTodate.Text = String.Empty Or txtRemarks.Text = String.Empty Then
                lblMsg.Text = " Please Enter the Mandatory Fields"
                lblMsg.Visible = True
                Exit Sub
            End If
            If txtFcabins.Text = String.Empty Then
                txtFcabins.Text = 0
            End If
            If txtWstations.Text = String.Empty Then
                txtWstations.Text = 0
            End If
            If gvCount.Rows.Count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "No seats are allocated to the selected vertical."
                Exit Sub


            End If
            Dim wscnt As Integer
            Dim fccnt As Integer
            Dim hccnt As Integer

            Dim i As Integer = 0
            For Each row As GridViewRow In gvCount.Rows
                Dim lblws As Label = DirectCast(row.FindControl("lblWS"), Label)
                Dim lblhc As Label = DirectCast(row.FindControl("lblHC"), Label)
                Dim lblfc As Label = DirectCast(row.FindControl("lblFC"), Label)
                wscnt = CInt(lblws.Text)
                fccnt = CInt(lblfc.Text)
                hccnt = CInt(lblhc.Text)
            Next

            If CInt(txtWstations.Text) > wscnt Then
                lblMsg.Visible = True
                lblMsg.Text = "Please enter the available no.of work station(s)."
                Exit Sub
            End If
            If CInt(txtHcabins.Text) > hccnt Then
                lblMsg.Visible = True
                lblMsg.Text = "Please enter the available no.of Half cabins(s)."
                Exit Sub
            End If
            If CInt(txtFcabins.Text) > fccnt Then
                lblMsg.Visible = True
                lblMsg.Text = "Please enter the available no.of Full cabin(s)."
                Exit Sub
            End If

            Try

                Dim strdept As String = ddlDept.SelectedValue
                Dim strHcab As String = txtHcabins.Text
                Dim strFcab As String = txtFcabins.Text
                Dim strWS As String = txtWstations.Text
                '       Dim strproj As String = ddlProject.SelectedItem.Value
                Dim strtower As String = ddlTower1.SelectedValue
                Dim strloc As String = ddlLoc.SelectedValue.ToString()
                Dim strCostCenter As String = ddlCostcenter.SelectedValue
                Dim strReqId As String = obj.INSERTSPACEREQUISITION_Values(strdept, strHcab, strFcab, strWS, "", Page, CType(txtFromdate.Text, Date), CType(txtTodate.Text, Date), strtower, txtRemarks.Text.Trim().Replace("'", "''"), ddlVertical.SelectedItem.Value, ddlLoc.SelectedItem.Value, strCostCenter)
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_SPACEREQUISITIONINSERT")
                sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
                sp.Command.AddParameter("@REQID", strReqId, DbType.String)
                sp.ExecuteScalar()
                'cleardata()

                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("5") & "&rid=" & clsSecurity.Encrypt(strReqId)
            Catch ex As Exception
                '  Throw New Amantra.Exception.DataException("This error has been occured while Inserting data ", "Space Requisition", "Load", ex)
                Throw (ex)
            End Try

            If strRedirect <> String.Empty Then
                Response.Redirect(strRedirect)
            End If
        End If
    End Sub

    Sub cleardata()
        ddlTower1.SelectedIndex = -1
        txtCountry1.Text = String.Empty
        txtCity1.Text = String.Empty
        txtLocation1.Text = String.Empty
        txtFromdate.Text = String.Empty
        txtTodate.Text = String.Empty
        txtHcabins.Text = String.Empty
        txtFcabins.Text = String.Empty
        txtWstations.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlDept.SelectedIndex = -1

    End Sub

    Private Sub loadgrid()

        Dim fdate As Date
        Dim tdate As Date
        fdate = CDate(txtFromdate.Text)
        tdate = CDate(txtTodate.Text)
        If fdate > tdate Then
            lblMsg.Text = "To date should be more than from date "
            Exit Sub
        End If

        Dim pcount As SqlParameter() = {New SqlParameter("@VC_VERTICAL", SqlDbType.VarChar, 150), New SqlParameter("@VC_LOC", SqlDbType.VarChar, 150), New SqlParameter("@vc_Tower", SqlDbType.VarChar, 150), New SqlParameter("@from_date", SqlDbType.Date), New SqlParameter("@to_date", SqlDbType.Date)}
        pcount(0).Value = ddlVertical.SelectedValue
        pcount(1).Value = HiddenField1.Value
        pcount(2).Value = ddlTower1.SelectedValue
        pcount(3).Value = fdate
        pcount(4).Value = tdate

        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"USP_project_requisition_space", pcount)
        gvCount.DataSource = ds.Tables(0)
        gvCount.DataBind()
        If (gvCount.Rows.Count > 0) Then
            gvCount.Visible = True
            For ICnt As Integer = 0 To gvCount.Rows.Count - 1
                If gvCount.Rows(ICnt).Cells(1).Text = "0" And gvCount.Rows(ICnt).Cells(2).Text = "0" And gvCount.Rows(ICnt).Cells(3).Text = "0" Then
                    gvCount.Rows(ICnt).Visible = False
                End If
            Next
        Else
            lblMsg.Visible = True
            lblMsg.Text = "There is no seats available for this vertical..."
            gvCount.Visible = False
        End If

        For ICnt1 As Integer = 0 To gvCount.Rows.Count - 1
            If gvCount.Rows.Count = 1 And gvCount.Rows(ICnt1).Cells(1).Text = "0" And gvCount.Rows(ICnt1).Cells(2).Text = "0" And gvCount.Rows(ICnt1).Cells(3).Text = "0" Then
                gvCount.Visible = False
            End If
        Next
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        '' ddlLoc.Items.Clear()
        'ddlTower1.Items.Clear()
        txtCity1.Text = ""
        txtCountry1.Text = ""
        txtLocation1.Text = ""
        'ddlCostcenter.Items.Clear()
        ddlLoc.Items.Clear()
        ddlTower1.Items.Clear()
        'ddlVertical.Items.Clear()
        ddlVertical.SelectedIndex = 0
        ddlCostcenter.SelectedIndex = 0
        txtFromdate.Text = ""
        txtTodate.Text = ""
        Try
            If ddlCity.SelectedIndex <> 0 Then
                
                strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
                BindCombo(strSQL, ddlLoc, "LCM_name", "lcm_code")
                If ddlLoc.Items.Count = 1 Then
                    lblMsg.Text = "No Locations Available"
                    lblMsg.Visible = True
                End If
               
                
            Else
                lblMsg.Text = "Please Select City"
                lblMsg.Visible = True
            End If
         
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Requisition", "Load", ex)

        End Try

    End Sub

    Protected Sub ddlLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLoc.SelectedIndexChanged
        ddlTower1.Items.Clear()
        txtCity1.Text = ""
        txtCountry1.Text = ""
        txtLocation1.Text = ""
        Try
            If ddlLoc.SelectedIndex <> 0 Then
                obj.loadtower1(ddlTower1, ddlCity.SelectedValue.ToString().Trim(), ddlLoc.SelectedValue.ToString().Trim())
                HiddenField1.Value = ddlLoc.SelectedValue
                If ddlLoc.Items.Count = 2 Then
                    ddlLoc.SelectedIndex = 1
                    ddlTower1_SelectedIndexChanged(sender, e)
                End If
                If ddlTower1.Items.Count = 1 Then
                    lblMsg.Text = "No towers in this Location"
                    lblMsg.Visible = True
                End If
                If (ddlVertical.SelectedIndex > 0 And ddlLoc.SelectedIndex > 0) Then
                    loadgrid()
                End If
            Else
                lblMsg.Text = "Please Select Location"
                lblMsg.Visible = True
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Requisition", "Load", ex)

        End Try

    End Sub
    Protected Sub ddlTower1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower1.SelectedIndexChanged
        Try
            Dim loc_code As String = String.Empty
            If ddlTower1.SelectedIndex <> 0 Then
                loc_code = obj.tower1_selectedindexchanged(ddlTower1, txtCountry1, txtCity1, txtLocation1)
            Else
                lblMsg.Text = "Please Select Tower"
                lblMsg.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Requisition", "Load", ex)

        End Try


    End Sub

    <WebMethod()> _
     <System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GetProjectsList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 120
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            'strSQL = "select AUR_ID from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID like '" & prefixText & "%' order by AUR_ID"
            'dtEle = SqlHelper.ExecuteDatatable(CommandType.Text, strSQL)
            Dim sp1 As New SqlParameter("@VC_PRJ_NAME", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SP_GET_PROJECTS", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            'count = dtcount
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "GetEmployeeList", exp)
        End Try
    End Function

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        If (ddlVertical.SelectedIndex > 0) Then

            Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
            sp1.Value = ddlVertical.SelectedValue
            ddlCostcenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvscostcenter", sp1)
            ddlCostcenter.DataTextField = "cost_center_name"
            ddlCostcenter.DataValueField = "cost_center_code"
            ddlCostcenter.DataBind()
            ddlCostcenter.Items.Insert(0, "--Select--")
            If (ddlVertical.SelectedIndex > 0 And ddlLoc.SelectedIndex > 0 And txtFromdate.Text <> "" And txtTodate.Text <> "") Then
                loadgrid()
            End If

        Else
            'ddlProject.SelectedIndex = 0
            ddlCostcenter.SelectedIndex = 0
            ddlCostcenter.Items.Clear()
            'ddlProject.Items.Clear()
        End If


    End Sub

    Protected Sub gvCount_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvCount.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(0).Text = Session("Parent") + " Name"
        End If
    End Sub
End Class
