﻿Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports cls_OLEDB_postgresSQL
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class WorkSpace_SMS_Webfiles_ViewExpiryLeases
    Inherits System.Web.UI.Page
    Dim filepath As String
    Dim strtxt As String
    Dim Subsonic As clsSubSonicCommonFunctions = New clsSubSonicCommonFunctions
    Dim strResponse As String = ""
    Dim strpoints As String = ""
    Dim strResponse1 As String = ""
    Dim fdate As DateTime
    Dim tdate As DateTime

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            
            Bindmap(" ", " ")
        End If
        txtFdate.Attributes.Add("readonly", "readonly")
        txtTdate.Attributes.Add("readonly", "readonly")

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
       
        fdate = CDate(txtFdate.Text)
        tdate = CDate(txtTdate.Text)
        If fdate > tdate Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('From Date should be less than to date');", True)
        Else
            Bindmap(fdate, tdate)


        End If

    End Sub


    Private Sub Bindmap(ByVal fdate As String, ByVal tdate As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "get_expirylease_grid")
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtTdate.Text, DbType.String)
        Dim dr As SqlDataReader = sp.GetReader
        Dim ds As New DataSet
        ds = sp.GetDataSet

        'gvitems.DataSource = ds
        'gvitems.DataBind()
        'ds = sp.GetDataSet()
        Dim rds As New ReportDataSource()
        rds.Name = "ViewExpiryLeases"

        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/ViewExpiryLeases.rdlc")
        'Dim cur As String = CultureInfo.GetCultureInfo(Session("userculture"))
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("Currencyparam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()

        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True


        'Dim lat As Double = "0"
        'Dim lon As Double = "0"
        'Dim icnt As Integer = 1
        'Dim Propertyname As String = ""
        'Dim Propertyaddress As String = ""
        'Dim propertycity As String = ""
        'Dim propertydate As String = ""
        'Dim propertyamount As String = ""
        'Dim arr1 As Array
        'Dim latlon As String = ""
        'Dim propertytype As Int16 = 0
        'Dim strText As String = ""

        'While dr.Read()
        '    lat = CDbl(dr("LATITUDE"))
        '    lon = CDbl(dr("LONGITUDE"))
        '    Propertyname = dr("PN_NAME")
        '    Propertyaddress = dr("PROPERTY_DESCRIPTION")

        '    propertycity = dr("CTY_NAME")

        '    propertydate = dr("EXPIRY_DATE")
        '    propertyamount = dr("RENT_AMOUNT")
        '    propertytype = dr("PROPERTY_TYPE")

        '    strText = " <b>Address:</b> " & CStr(Propertyname) & "," & CStr(Propertyaddress) & " ," & CStr(propertycity) & "<b></b>"


        '    If propertytype = 1 Or propertytype = 2 Then
        '        strText = strText + "<br><b>Purchase price(Rs):</b> " & CStr(propertyamount) & " <br><b>Purchase date:</b>" & CStr(propertydate) & "<b></b>"
        '    Else
        '        strText = strText + "<br><b>Rent Amount(Rs):</b> " & CStr(propertyamount) & " <br><b>Expiry date:</b>" & CStr(propertydate) & "<b></b>"

        '    End If



        '    Dim str As String = ""
        '    str = "<item>"
        '    str = str + "<title>" & CStr(dr("PN_NAME")) & "</title><link></link><guid></guid>"
        '    str = str + "<description>"
        '    str = str + "<![CDATA["
        '    str = str + strText

        '    str = str + "]]>"
        '    str = str + "</description>"
        '    str = str + "<georss:line>" & lat & " " & lon & "</georss:line>"
        '    str = str + "</item>"


        '    strpoints = strpoints + str
        '    latlon = latlon + " " & lon & "," & lat & ""

        '    icnt += 1
        'End While

        'If latlon = "" Then
        '    lblmsg.Text = "No Expiry Leases found"
        'Else
        '    lblmsg.Text = ""
        '    arr1 = Split(latlon, " ")

        '    If File.Exists(MapPath("MapXMLFormat1.txt")) Then
        '        strResponse = File.ReadAllText(MapPath("MapXMLFormat1.txt"))
        '        strResponse = strResponse.Replace("@@item", strpoints)
        '    End If

        '    File.Delete(MapPath("stpl-png_routes_uid1.xml"))
        '    File.WriteAllText(MapPath("stpl-png_routes_uid1.xml"), strResponse)


        '    If File.Exists(MapPath("index1.html")) Then
        '        strResponse1 = File.ReadAllText(MapPath("index1.html"))
        '        Dim strcity As String = ""
        '        Dim strMapCenter As String = ""
        '        For i As Integer = 0 To arr1.Length - 1
        '            strMapCenter = "map.setCenter(new OpenLayers.LonLat(" & arr1(i) & "),12);"
        '        Next
        '        strResponse1 = strResponse1.Replace("@@Zoom", strMapCenter)

        '        if1.Attributes.Add("src", "index1.html?id=" & Now.Millisecond)
        '        'Literal1.Text = strResponse1
        '    End If


        'End If
    End Sub

    'Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
    '    gvitems.PageIndex = e.NewPageIndex
    '    Bindmap(txtFdate.Text, txtTdate.Text)
    'End Sub
End Class
