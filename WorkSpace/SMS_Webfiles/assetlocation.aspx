﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="assetlocation.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_assetlocation" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Capital Asset Allocation Report 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-left:30px">
  

    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%-- <div class="row">--%>
            <label>Asset Category</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>

    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--     <div class="row">--%>
            <label>Asset Sub Category</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlAstSubCat"
                Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--">
            </asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Sub Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
         <div class="col-md-1 col-sm-12 col-xs-12"></div>

        <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--       <div class="row">--%>
            <label>Asset Brand/Make</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlAstBrand"
                Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
            </asp:DropDownList>
            <%-- </div>--%>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
        <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>
            Asset Model</label>

            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>


            <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model"
               >
            </asp:DropDownList>



        </div>
    </div>
</div>
<div class="row" style="padding-left:30px">

     <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Location</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlLocation"
                Display="none" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>


            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model" >
            </asp:DropDownList>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
 <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Duration</label><br />


        
                                        <select id="ddlRange"  class="selectpicker" onchange="getDate(this)">
                                            <option value="">Select Range</option>
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    


        </div>
    </div><div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>From Date</label>

            <%--<asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="FromDate"
                ErrorMessage="Please From Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>

            <div class='input-group date' id='Div1'>
                <asp:TextBox ID="FromDate" runat="server" CssClass="form-control"  placeholder="mm/dd/yyyy" MaxLength="10" > </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                </span>
            </div>


        </div>
    </div> <div class="col-md-1 col-sm-12 col-xs-12"></div>
      <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

         <label>To Date</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ToDate"
                ErrorMessage="Please Enter To Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>

            <div class='input-group date' id='Div4'>
                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                </span>
            </div>


        </div>
    </div><%--<div class="col-md-1 col-sm-12 col-xs-12"></div>--%>
      <div class="col-md-1 col-sm-12 col-xs-12 text-right" style="padding-left:30px">
            <div class="form-group"><br />
                <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                <%--<asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary custom-button-color" />--%>
            </div>
        </div>
    
</div>
                        <div class="row table table table-condensed table-responsive">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script src="../../Scripts/moment.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddlRange").val('');
            rangeChanged();
        });

        function rangeChanged() {
            var selVal = $("#ddlRange").val();
            switch (selVal) {
                case 'TODAY':
                    $("#FromDate").val(moment().format('DD-MMM-YYYY'));
                    $("#txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case 'YESTERDAY':
                    $("#FromDate").val(moment().subtract(1, 'days').format('DD-MMM-YYYY'));
                    $("#txtToDate").val(moment().subtract(1, 'days').format('DD-MMM-YYYY'));
                    break;
                case '7':
                    $("#FromDate").val(moment().subtract(6, 'days').format('DD-MMM-YYYY'));
                    $("#txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case '30':
                    $("#FromDate").val(moment().subtract(29, 'days').format('DD-MMM-YYYY'));
                    $("#txtToDate").val(moment().format('DD-MMM-YYYY'));
                    break;
                case 'THISMONTH':
                    $("#FromDate").val(moment().startOf('month').format('DD-MMM-YYYY'));
                    $("#txtToDate").val(moment().endOf('month').format('DD-MMM-YYYY'));
                    break;
                case 'LASTMONTH':
                    $("#FromDate").val(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY'));
                    $("#txtToDate").val(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY'));
                    break;
                case '':
                    $("#FromDate").val('');
                    $("#txtToDate").val('');
                    break;
            }
        }

        $("#ddlRange").change(function () {
            rangeChanged();
        });
    </script>
