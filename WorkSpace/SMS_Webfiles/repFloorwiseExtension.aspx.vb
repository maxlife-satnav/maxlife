Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repFloorwiseExtension
    Inherits System.Web.UI.Page
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""

        If Not Page.IsPostBack Then
            'bindreport()
            obj.bindTower(ddlTower)
            ddlTower.Items(0).Text = "--All Towers--"
            ddlFloor.Items.Insert(0, "--All Floors--")
            ddlTower.SelectedIndex = 0
            bindreport()
            btnViewReport.Visible = True
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        bindreport()
    End Sub
    Public Sub bindreport()
        Dim spTowerID As New SqlParameter("@VC_TWRID", SqlDbType.NVarChar, 50)
        Dim spFloorID As New SqlParameter("@VC_FLRID", SqlDbType.NVarChar, 50)
        Dim spsortexp As New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 50)
        Dim spsortdir As New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 50)

        Dim MTower As String = ""
        Dim MFloor As String = ""

        If ddlTower.SelectedItem.Value = "--All Towers--" Then
            MTower = ""
        Else
            MTower = ddlTower.SelectedItem.Value
        End If
        If ddlFloor.SelectedItem.Value = "--All Floors--" Then
            MFloor = ""
        Else
            MFloor = ddlFloor.SelectedItem.Value
        End If

        spTowerID.Value = MTower
        spFloorID.Value = MFloor
        spsortexp.Value = "SE.ssa_spc_id"
        spsortdir.Value = "ASC"

        Dim dtFloorE As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_REPORTS_FLOOR_EXTENSION", spTowerID, spFloorID, spsortexp, spsortdir)

        Dim rds As New ReportDataSource()
        rds.Name = "FloorExtensionDS"
        rds.Value = dtFloorE
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/FloorExtensionReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
        ddlFloor.Items(0).Text = "--All Floors--"
        ddlFloor.SelectedIndex = 0
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class
