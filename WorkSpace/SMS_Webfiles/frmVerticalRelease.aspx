<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmVerticalRelease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmVerticalRelease" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal)
        {
            re = new RegExp(aspCheckBoxID)
            
            var theForm = document.forms['aspnetForm'];
           
            for(i = 0; i < theForm.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox')
                {
                    if (re.test(elm.name))
                    elm.checked = checkVal
                }
           }
        }
        function CheckDataGrid()
        {
        document.getElementById("lblMsg").innerText ='';
            var k=0;
            for(i = 0; i < form1.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox')
                {
                    if (elm.checked == true)
                    {
                            k=k+1;
                    }
                }
           }
           if (k==0)
           {
               document.getElementById("lblMsg").innerText ='Please check at least one space to Release';
               return false;
           }
           else
           {
               
               var con =confirm("Are you sure you want to release the space?");
               if (con== true)
               {
                    return true;
               }
               else
               {
                    return false;
               }   
            
           }
        }
    </script>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Vertical wise Release Details
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="100%"
                align="center" border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 27px">
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Vertical wise Release Details</strong></td>
                    <td style="height: 27px">
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left" style="width: 962px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" />
                        &nbsp;&nbsp; &nbsp; &nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <br />
                        <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                            border="1">
                            <tr visible="false">
                                <td align="left" width="50%">
                                    &nbsp;Select Vertical <strong><span style="font-size: 8pt; color: #ff0000">*<asp:RequiredFieldValidator
                                        ID="rfvVertical" runat="server" ControlToValidate="ddlVertical" Display="None"
                                        ErrorMessage="Please select Vertical" InitialValue="--Select--"></asp:RequiredFieldValidator></span></strong></td>
                                <td align="center" width="50%">
                                    <asp:DropDownList ID="ddlVertical" runat="server" Width="99%" AutoPostBack="True"
                                        CssClass="clsComboBox" OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    &nbsp;Select Location <strong><span style="font-size: 8pt; color: #ff0000">*<asp:RequiredFieldValidator
                                        ID="rfvLoc" runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please select Location"
                                        InitialValue="--Select--"></asp:RequiredFieldValidator></span></strong></td>
                                <td align="center" width="50%">
                                    <asp:DropDownList ID="ddlLocation" runat="server" Width="99%" AutoPostBack="True"
                                        CssClass="clsComboBox" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    &nbsp;Select Tower<font class="clsNote"><span style="font-size: 8pt">*<asp:RequiredFieldValidator
                                        ID="rfvTower" runat="server" ControlToValidate="ddlTower" Display="None" ErrorMessage="Please select Tower"
                                        InitialValue="--Select--"></asp:RequiredFieldValidator></span></font></td>
                                <td align="center" width="50%">
                                    <asp:DropDownList ID="ddlTower" runat="server" Width="99%" CssClass="clsComboBox"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    &nbsp;Select Floor <font class="clsNote"><span style="font-size: 8pt">*<asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFloor" Display="None"
                                        ErrorMessage="Please select Floor" InitialValue="--Select--"></asp:RequiredFieldValidator></span></font></td>
                                <td align="center" width="50%">
                                    <asp:DropDownList ID="ddlFloor" runat="server" Width="99%" CssClass="clsComboBox"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlFloor_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    &nbsp;Select Wing <font class="clsNote"><span style="font-size: 8pt">*<asp:RequiredFieldValidator
                                        ID="rfvWing" runat="server" ControlToValidate="ddlWing" Display="None" ErrorMessage="Please select Wing"
                                        InitialValue="--Select--"></asp:RequiredFieldValidator></span></font></td>
                                <td align="center" width="50%">
                                    <asp:DropDownList ID="ddlWing" runat="server" Width="99%" CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="height: 26px">
                                    <asp:Button ID="btnView" runat="server" CssClass="clsButton" OnClick="btnView_Click"
                                        Text="View" /></td>
                            </tr>
                        </table>
                        <table id="tblGrid" runat="server" cellspacing="0" cellpadding="0" style="width: 98%"  align="center" border="1">
                            <tr>
                                <td align="left" colspan="4" style="height: 11px">
                                    <asp:GridView ID="gvSpaceExtend" runat="server" AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="4">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkEmp" runat="server" />
                                                    <asp:Label runat="server" ID="lblSpaceID" Visible="false" Text='<%#bind("SpaceReq") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkEmp',this.checked)">
                                                </HeaderTemplate>
                                                <ItemStyle Width="1px" />
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Tower" DataField="Tower" />
                                            <asp:BoundField HeaderText="Floor" DataField="Floor" />
                                            <asp:BoundField HeaderText="Wing" DataField="Wing" />
                                            <asp:BoundField HeaderText="Space" DataField="Space" >
                                                <ItemStyle Width="150px" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="From Date" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblFromDT" Text='<%#bind("FROMDATE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To Date" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblToDT" Text='<%#bind("TODATE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="From Date" DataField="FROMDATE" />
                                            <asp:BoundField HeaderText="To Date" DataField="TODATE" />
                                        </Columns>
                                        <PagerSettings Mode="NextPrevious" NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4" headers="24px" style="height: 24px">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Release" CssClass="clsButton" OnClientClick="return CheckDataGrid()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    </td>
                </tr>
                <tr>
                    <td style="height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px; width: 962px;" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
                <tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
