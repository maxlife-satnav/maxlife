Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repTWROccupancy
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""

        If Not Page.IsPostBack Then
            ReportViewer1.Visible = False
            objsubsonic.Binddropdown(ddlverticals, "GET_ALLCOSTCENTER_AURID_ALLOC", "COST_CENTER_NAME", "COST_CENTER_CODE")
            ddlverticals.Items(0).Text = "--All " & Session("Parent") & "(s)"
            ddlTower.Items.Insert(0, "--All " & Session("Child") & "(s)")
            bindreport()
            lblSelChild.Text = Session("Child")
            lblSelChild1.Text = Session("Child")
            lblSelParent.Text = Session("Parent")
            'lbl1.Text = " Select " & Session("Parent")
            'RequiredFieldValidator2.ErrorMessage = "Please Select " + lblSelParent.Text
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        If ddlTower.SelectedIndex = 0 Then
            lbl1.Visible = False
            'Exit Sub
        End If
        bindreport()
    End Sub
    Public Sub bindreport()
        Dim towerid As String = ""
        Dim verid As String = ""
        If ddlTower.SelectedItem.Text = "--All " & Session("Child") & "(s)" Then
            towerid = ""
        Else
            towerid = ddlTower.SelectedValue
        End If

        If ddlverticals.SelectedItem.Text = "--All " & Session("Parent") & "(s)" Then
            verid = ""
        Else
            verid = ddlverticals.SelectedItem.Value
        End If

        Try
            Dim sp1 As New SqlParameter("@prj_name", SqlDbType.NVarChar, 50)
            sp1.Value = towerid
            Dim sp2 As New SqlParameter("@ver_name", SqlDbType.NVarChar, 50)
            sp2.Value = verid

            Dim dtProjectA As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_get_project_Wise_ALLOC_Report", sp1, sp2)

            Dim rds As New ReportDataSource()
            rds.Name = "CostCenterAllocationDS"
            rds.Value = dtProjectA
            'This refers to the dataset name in the RDLC file
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/CostCenterAllocationReport.rdlc")

            'Setting Header Column value dynamically
            Dim p1 As New ReportParameter("dynamicParam1", Session("Child").ToString())
            ReportViewer1.LocalReport.SetParameters(p1)

            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub
    Protected Sub ddlverticals_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlverticals.SelectedIndexChanged
        ReportViewer1.Visible = False
        If ddlverticals.SelectedIndex <> 0 Then
            lbl1.Visible = False
        Else
            lbl1.Visible = True
        End If

        Dim sp1 As New SqlParameter("@vc_VerticalName", SqlDbType.NVarChar, 50)
        If ddlverticals.SelectedIndex = 1 Then
            sp1.Value = ""
        Else
            sp1.Value = ddlverticals.SelectedValue
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_get_bu_Projects", sp1)
        BindCombo(ds, ddlTower, "PRJ_NAME", "PROJ_NAME")
        ddlTower.Items(0).Text = "--All " & Session("Child") & "(s)"
        ddlTower.Items(0).Value = "1"
        ddlTower.SelectedIndex = 0
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class
