<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AllocateSeatToEmployeeVertical_bkp21May2015.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_AllocateSeatToEmployeeVertical" %>

<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>

<%@ Register Src="~/Controls/MappedAssetDetails.ascx" TagName="MappedAssetDetails"
    TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/jquery-ui.min.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">



        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setDropDownList(elementRef, valueToSetTo) {
            var isFound = false;


            for (var i = 0; i < elementRef.options.length; i++) {
                if (elementRef.options[i].value == valueToSetTo) {
                    elementRef.options[i].selected = true;
                    isFound = true;
                }
            }


            if (isFound == false)
                elementRef.options[0].selected = true;
        }

    </script>

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Allocation </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div id="PNLCONTAINER" runat="server">

                            <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                                ForeColor="red" />

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate" Display="None" ErrorMessage="Please Enter From Date ">

                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtFrmDate" Display="None" ErrorMessage="Please Enter Valid From Date " Operator="DataTypeCheck" Type="Date">
                                            </asp:CompareValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtFrmDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvtodate" runat="server" ControlToValidate="txtToDate" Display="None" ErrorMessage="Please Enter To Date ">
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtToDate" Display="None" ErrorMessage="Please Enter Valid To Date " Operator="DataTypeCheck" Type="Date">
                                            </asp:CompareValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="trshift" runat="server" class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Shift<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlshift"
                                                Display="None" ErrorMessage="Please Select Shift" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlshift" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="trTimeSlot" runat="server" visible="false" class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">From:<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr" Display="None" ErrorMessage="Please Enter From Time " InitialValue="Hr">
                                            </asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimemin" Display="None" ErrorMessage="Please Enter From Minutes" InitialValue="Min">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="starttimehr" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="False">
                                                    <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                                    <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                    <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="starttimemin" runat="server" Enabled="False" CssClass="selectpicker" data-live-search="true">
                                                    <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                                    <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">To:<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr" Display="None" ErrorMessage="Please Enter To Time " InitialValue="Hr">
                                            </asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimemin" Display="None" ErrorMessage="Please Enter To Minutes" InitialValue="Min">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="endtimehr" runat="server" Enabled="False" CssClass="selectpicker" data-live-search="true">
                                                    <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                                    <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                    <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                    <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                    <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                    <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                    <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                    <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                    <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                    <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                    <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="endtimemin" runat="server" Enabled="False" CssClass="selectpicker" data-live-search="true">
                                                    <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                                    <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                    <asp:ListItem>45</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblSelVertical" runat="server" class="col-md-5 control-label" Text=""><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVertical" Display="None" InitialValue="--Select--">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlVertical" runat="server" AutoPostBack="true" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Select Employee </label>
                                            <div class="col-md-7">

                                                <%--<editable:EditableDropDownList ID="EditableDropDownList3" runat="server" ClientIDMode="Static" Sorted="true"
                                                    CssClass="form-control">
                                                </editable:EditableDropDownList>--%>
                                                <asp:TextBox runat="server" ID="txtEmpName" CssClass="form-control"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Label1" runat="server" class="col-md-5 control-label" Text=""></asp:Label>

                                            <div class="col-md-7 text-right">

                                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" Text="Allocate to Vertical" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label"></label>
                                            <div class="col-md-7 text-right">
                                                <asp:Button ID="btnallocate" runat="server" CssClass="btn btn-primary custom-button-color" Text="Check Employee Details" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div runat="Server" id="empdetails" visible="false">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Employee Id</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblEmpSearchId" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Employee Name</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblEmpName" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Email Id</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblEmpEmailId" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Reporting To</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblReportingTo" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Designation</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblDesignation" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Department</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Employee Extension</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblEmpExt" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Contact Number</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblResNumber" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    City</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblCity" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">State</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblState" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">                                                
                                                <asp:Label ID="lblSelProject" class="col-md-5 control-label" runat="server" Text=""></asp:Label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblPrjCode" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblSelVertical1" class="col-md-5 control-label" runat="server" Text=""></asp:Label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblAUR_VERT_CODE" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Building</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblAUR_BDG_ID" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Tower</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblEmpTower" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Floor</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblEmpFloor" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Date of Joining</label>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblDoj" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Space</label>

                                                <asp:HyperLink ID="hypSpaceId" runat="server"></asp:HyperLink>
                                                <div class="col-md-7">
                                                    <asp:Label ID="lblSpaceAllocated" runat="server" Font-Bold="True"
                                                        Font-Names="Arial" Font-Size="Medium" Font-Underline="True" ForeColor="Red"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row pull-right">
                                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Allocate to Employee" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gdavail" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Select">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" Checked="true" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SPC_BDG_ID" HeaderText="Location Name" SortExpression="SPC_BDG_ID" />
                                        <asp:TemplateField HeaderText="Spaces Available">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspcid" runat="server" Text='<%# Eval("spc_id").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSpacelyr" runat="server" Text='<%# Eval("spc_layer").ToString()%>'>&nbsp;&nbsp;  </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Seat Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSpaceType" runat="server" Text='<%# Eval("SPC_BDG_ID").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Carved status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcarved" runat="server" Text='<%# Eval("SPACE_CARVED").ToString()%>'>&nbsp;&nbsp;  </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Port Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblportstatus" runat="server" Text='<%# Eval("SPACE_PORT_STATUS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space Port Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblporttype" runat="server" Text='<%# Eval("SPACE_PORT_TYPE").ToString()%>'>&nbsp;&nbsp;  </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BCP Seat Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblbcpseattype" runat="server" Text='<%# Eval("SPACE_BCP_SEAT_TYPE").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MCP ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmcp" runat="server" Text='<%# Eval("SPACE_MCP_SEAT").ToString()%>'>&nbsp;&nbsp;  </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PORT Number">
                                            <ItemTemplate>
                                                <asp:Label ID="lblportnumber" runat="server" Text='<%# Eval("SPACE_PORT_NUMBER").ToString()%>'>&nbsp;&nbsp;  </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Area Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblareatype" runat="server" Text='<%# Eval("SPACE_AREA_TYPE").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSeatStatus" class="col-md-5 control-label" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvSharedEmployee" runat="server" PageSize="20" EmptyDataText="Not Occupied By Any Employee."
                                    AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Employee Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAUR_ID1" runat="server" Text='<%#Eval("AUR_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("AUR_FIRST_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblAUR_ID" runat="server" Text='<%#Eval("AUR_ID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblemail" runat="server" Text='<%#Eval("AUR_EMAIL") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspaceID" runat="server" Text='<%#Eval("AUR_LAST_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSSA_FROM_DATE" runat="server" Text='<%#Eval("SSA_FROM_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSSA_TO_DT" runat="server" Text='<%#Eval("SSA_TO_DT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFROM_TIME" runat="server" Text='<%#Eval("FROM_TIME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTO_TIME" runat="server" Text='<%#Eval("TO_TIME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSpaceRelease" CausesValidation="false" CommandArgument='<%#Eval("AUR_ID") %>'
                                                    CommandName="SpaceRelease" runat="server">Space Release</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvAllocatedSeats" runat="server" PageSize="20" EmptyDataText="Not Allocated To Any Vertical."
                                    AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vertical">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSSA_VERTICAL" runat="server" Text='<%#Eval("SSA_VERTICAL") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspaceID" runat="server" Text='<%#Eval("SSA_SPC_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSSA_FROM_DATE" runat="server" Text='<%#Eval("SSA_FROM_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSSA_TO_DT" runat="server" Text='<%#Eval("SSA_TO_DT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFROM_TIME" runat="server" Text='<%#Eval("FROM_TIME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTO_TIME" runat="server" Text='<%#Eval("TO_TIME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSpaceRelease" CausesValidation="false" CommandName="Release"
                                                    runat="server">Release</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset>
                                    <legend>Asset Details</legend>
                                    <uc1:MappedAssetDetails ID="MappedAssetDetails1" runat="server" />
                                </fieldset>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>    
    <link href="../../BootStrapCSS/jquery-ui.min.css" rel="stylesheet" />    
    <script src="../../BootStrapCSS/Scripts/jquery-ui.min.js"></script>

    <script lang="javascript" type="text/javascript">

        $(document).ready(function () {
            $('#txtFrmDate').datepicker({
                format: 'mm/dd/yyyy',
                changeMonth: true,
                changeYear: true,
                //showOn: "button",
                //buttonImage: "../../BootStrapCSS/images/glyphicons_045_calendar.png",
                //buttonImageOnly: true,
                //buttonText: "Select From Date"
            });
            $('#txtToDate').datepicker({
                format: 'mm/dd/yyyy',
                changeMonth: true,
                changeYear: true,
                //showOn: "button",
                //buttonImage: "../../BootStrapCSS/images/glyphicons_045_calendar.png",
                //buttonImageOnly: true,
                //buttonText: "Select To Date"
            });
            SearchText();
        });
        function SearchText() {
            $("#txtEmpName").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "AllocateSeatToEmployeeVertical.aspx/SearchCustomers",
                        data: "{'prefixText':'" + document.getElementById('txtEmpName').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert(result.data);
                        }
                    });
                }
            });
        }
        function setup(id) {
            //$("#txtToDate").datepicker({ format: 'mm/dd/yyyy' });            
        };
    </script>

</body>
</html>
