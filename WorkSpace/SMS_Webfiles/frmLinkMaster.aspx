<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmLinkMaster.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmLinkMaster"
    Title="Link Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Link 
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Link Master </strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="height: 158px">
                        &nbsp;</td>
                    <td align="left" style="height: 158px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                            <tr id="trLname" runat="server">
                                <td align="left" class="label" style="height: 11px; width: 50%;">
                                    <asp:Label ID="lblLinkName" runat="server" Text="Select Link " Font-Bold="False"></asp:Label>
                                    <span style="font-size: 8pt; color: #ff0000">*</span>
                                    <asp:RequiredFieldValidator ID="rfvlink" runat="server" InitialValue="0" Display="none"
                                        ControlToValidate="ddlLinkName" ErrorMessage="Please select Link"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 43%; height: 11px">
                                    <asp:DropDownList ID="ddlLinkName" runat="server" Width="97%" CssClass="clsComboBox"
                                        ToolTip="Select Wing to modify">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    &nbsp;Enter Link Name You Want to Replace <font class="clsNote">*<asp:RequiredFieldValidator
                                        ID="rfvCode" runat="server" ErrorMessage="Please Enter Link Name " ControlToValidate="txtLinkName"
                                        Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revLinkName" runat="server" ControlToValidate="txtLinkName"
                                            Display="None" ErrorMessage="Please Enter Valid Link Name" ValidationExpression="^[0-9a-zA-Z ]+"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator></font></td>
                                <td align="left" style="width: 43%; height: 26px;">
                                    <div onmouseover="Tip('Enter LinkName in alphabets and numbers, upto 15 characters allowed')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtLinkName" MaxLength="15" runat="server" Width="97%" CssClass="clsTextField"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table class="table" style="height: 22px" width="100%" border="1">
                            <tr>
                                <td align="center" style="height: 20px">
                                    &nbsp;
                                    <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit"
                                        ValidationGroup="Val1"></asp:Button>&nbsp;
                                    <asp:Button ID="btnback" runat="server" Width="76px" CssClass="button" Text="Back"></asp:Button></td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 158px;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
