﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports clsReports
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Imports System.Web.Services

Partial Class WorkSpace_SMS_Webfiles_EmpOccupiedUnoccupiedSeatCost
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim obj As New clsReports
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            obj.bindLocation(ddlLocation)
            ddlLocation.Items(0).Text = "--All--"
            'BindFloor()
            ddlFloor.Items.Insert(0, "--All--")
            ddlFloor.SelectedIndex = 0
            BindGridSummary()
        End If
    End Sub

    Private Sub BindFloor()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_FLOOR_BY_LOCTION")
        sp3.Command.AddParameter("@LOC_CODE", ddlLocation.SelectedValue, DbType.String)
        ddlFloor.DataSource = sp3.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
    End Sub

    Private Sub BindGridSummary()

        Dim location As String
        Dim floor As String

        If ddlLocation.SelectedItem.Text = "--All--" Then
            location = ""
        Else
            location = ddlLocation.SelectedItem.Text
        End If

        If ddlFloor.SelectedItem.Text = "--All--" Then
            floor = ""
        Else
            floor = ddlFloor.SelectedItem.Text
        End If
        Dim ds As DataSet

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_OCCUPIED_UNOCCUPIED_SEAT_COST")
        sp.Command.AddParameter("@VC_LOCNAME", location, DbType.String)
        sp.Command.AddParameter("@VC_FLOORNAME", floor, DbType.String)
        ds = sp.GetDataSet()
        gvitems.DataSource = ds
        gvitems.DataBind()
        If (ds.Tables(0).Rows.Count > 0) Then
            btnexcel.Visible = True
        Else
            btnexcel.Visible = False
        End If
    End Sub
    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGridSummary()
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        ddlFloor.Items.Clear()
        If ddlLocation.SelectedIndex <> 0 Then
            BindFloor()
            End If
        ddlFloor.Items.Insert(0, "--All--")
        ddlFloor.SelectedIndex = 0
        BindGridSummary()

        If ddlLocation.SelectedItem.Text = "--All--" And ddlFloor.SelectedItem.Text = "--All--" Then
            BindGridSummary()
        End If

    End Sub


    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        BindGridSummary()
    End Sub

    Protected Sub btnexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ExportToExcelEmployeeAllocationReport()
    End Sub

    Private Sub ExportToExcelEmployeeAllocationReport()
        Export("OccupanySeatCostReport.xls", gvitems)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
End Class
