﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_repshiftavailability
    Inherits System.Web.UI.Page
    Dim objMasters As clsMasters
    Dim obj As New clsReports
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False
        Try
            If Not Page.IsPostBack Then
                ReportViewer1.Visible = False
                obj.bindLocation(ddllocation)
                ddlTower.SelectedIndex = 0
                ddlFloor.SelectedIndex = 0
                ddlshift.SelectedIndex = 0

            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Dim dtReport, dtTemp As New DataTable
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        Try

            Dim rds As New ReportDataSource()
            rds.Name = "ShiftAvailableDS"

            'This refers to the dataset name in the RDLC file
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/ShiftAvailabilityReport.rdlc")
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True


            If ddllocation.SelectedIndex = 0 Then
                lblMsg.Visible = True
                Exit Sub
            End If
            If ddlTower.SelectedIndex = 0 Then
                lblMsg.Visible = True
                Exit Sub
            End If
            If ddlFloor.SelectedIndex = 0 Then
                lblMsg.Visible = True
                Exit Sub
            End If
            If ddllocation.SelectedIndex <> 0 Then
                lblMsg.Visible = False
            End If
            If ddlTower.SelectedIndex <> 0 Then
                lblMsg.Visible = False
            End If
            If ddlFloor.SelectedIndex <> 0 Then
                lblMsg.Visible = False
            End If
            If ddlshift.SelectedIndex <> 0 Then
                lblMsg.Visible = False
            End If


            If ddllocation.SelectedIndex > 0 And ddlTower.SelectedIndex > 0 And ddlFloor.SelectedIndex > 0 And ddlshift.SelectedIndex > 0 Then

                Dim MTowerId, MBdgId, MFlrid, MshiftId As String
                If ddlTower.SelectedValue = "--All Towers--" Then
                    MTowerId = ""
                Else
                    MTowerId = ddlTower.SelectedValue
                End If

                If ddllocation.SelectedValue = "--All Sites--" Then
                    MBdgId = ""
                Else
                    MBdgId = ddllocation.SelectedValue
                End If

                If ddlFloor.SelectedValue = "--All Floors--" Then
                    MFlrid = ""
                Else
                    MFlrid = ddlFloor.SelectedValue
                End If
                If ddlshift.SelectedValue = "--All Shifts--" Then
                    MshiftId = ""
                Else
                    MshiftId = ddlshift.SelectedItem.Value
                End If

                Dim sp3 As New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
                sp3.Value = MBdgId
                Dim sp4 As New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 50)
                sp4.Value = MTowerId
                Dim sp5 As New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 50)
                sp5.Value = MFlrid
                Dim sp6 As New SqlParameter("@SHIFT_CODE", SqlDbType.NVarChar, 50)
                sp6.Value = MshiftId
                'Dim sp7 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
                'sp7.Value = "Locationname"
                'Dim sp8 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
                'sp8.Value = "ASC"
                dtTemp = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETSEATAVAILABILITY_BYSHIFT", sp3, sp4, sp5, sp6)

                rds.Value = dtTemp

            Else
                lblMsg.Text = "Please select all"
            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub


    Public Sub bindshift()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@loc_code", SqlDbType.NVarChar, 200)
        param(0).Value = ddllocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlshift, "GET_ALLSHIFTS_BYLOCATION", "Name", "Code", param)

    End Sub
    Protected Sub ddllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddllocation.SelectedIndexChanged
        lblMsg.Visible = False
        ddlshift.Items.Clear()
        ReportViewer1.Visible = False
        If ddllocation.SelectedIndex <> 0 Then
            lblMsg.Visible = False
        End If

        obj.bindTower_Locationwise(ddlTower, ddllocation.SelectedValue)
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)

        bindshift()

        If ddlshift.Items.Count = 0 Then
            lblMsg.Text = "No Shifts Available for Selected Location"
            lblMsg.Visible = True
        Else
            ddlshift.Items.Insert(1, "--All Shifts--")
            lblMsg.Visible = False
        End If

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
        Dim lcm_code As String = ""
        Dim twr_code As String = ""

        If ddlTower.SelectedValue = "--All Towers--" Then
            twr_code = ""
        Else
            twr_code = ddlTower.SelectedValue
        End If

        If ddllocation.SelectedValue = "--All Sites--" Then
            lcm_code = ""
        Else
            lcm_code = ddllocation.SelectedValue
        End If


        If ddllocation.SelectedIndex <> 0 Then
            lblMsg.Visible = False
        End If
        If ddlTower.SelectedIndex <> 0 Then
            lblMsg.Visible = False
        End If
        
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)

    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        ReportViewer1.Visible = False
        Dim lcm_code As String = ""
        Dim twr_code As String = ""
        Dim flr_code As String = ""
        If ddlTower.SelectedValue = "--All Towers--" Then
            twr_code = ""
        Else
            twr_code = ddlTower.SelectedValue
        End If

        If ddllocation.SelectedValue = "--All Sites--" Then
            lcm_code = ""
        Else
            lcm_code = ddllocation.SelectedValue
        End If
        If ddlFloor.SelectedValue = "--All Floors--" Then
            flr_code = ""
        Else
            flr_code = ddlFloor.SelectedValue
        End If

        If ddllocation.SelectedIndex <> 0 Then
            lblMsg.Visible = False
        End If
        If ddlTower.SelectedIndex <> 0 Then
            lblMsg.Visible = False
        End If
        If ddlFloor.SelectedIndex <> 0 Then
            lblMsg.Visible = False
        End If
        
    End Sub
End Class
