'***********************
' 5   - Requsted
' 6   - Allocated
' 166 - Partially Allocated
' 8   - Cancelled  
'***********************

Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Data.SqlClient
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports System.Net
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_ReservedBlockedSeatsDetails
    Inherits System.Web.UI.Page

    Dim objclsViewCancelChange As clsViewCancelChange
    Dim verBll As New VerticalBLL()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim obj As clsMasters
    Dim REQID As String
    Dim strStatus As String
    Dim RIDDS As String
    Dim dtVerReqDetails As DataTable
    Dim strRedirect As String = String.Empty

    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""
        Label1.Text = Session("Parent") + " Requisition ID "
        If Not Page.IsPostBack Then
            Try
                Session("AcrdIndex") = "1"
                obj = New clsMasters()
                Dim strReqId1 As String
                lblSelVertical.Text = Session("Parent")
                rfvVTl.ErrorMessage = "Please Select " + lblSelVertical.Text
                strReqId1 = Request.QueryString("ReqID").ToString().Trim()
                Dim strReqID As String = strReqId1
                Dim strMonth As String = CType(Request.QueryString("Month").ToString().Trim(), String)
                Dim strSatusID As String = Request.QueryString("StaID").ToString().Trim()
                Dim dt1 As DateTime = Convert.ToDateTime(strMonth)

                RIDDS = obj.RIDGENARATION("VerticalReq")

                Dim strYear As String = CType(Request.QueryString("Year").ToString().Trim(), String)
                Dim dtYear As DateTime = Convert.ToDateTime(strYear)
                lblVerReqID.Text = strReqID
                loadlocation()
                LoadCity()
                loaddepartment()
                ObjSubSonic.Binddropdown(ddlspacetype, "GETSPACETYPE_NOTBCP", "SPACE_TYPE", "SNO")


                obj = New clsMasters

                Dim dt As New DataTable
                Dim i As Integer
                dt.Columns.Add("sno")
                For i = 0 To 0
                    Dim dr As DataRow = dt.NewRow
                    dr(0) = i + 1
                    dt.Rows.Add(dr)
                Next


                gvEnter.DataSource = dt
                gvEnter.DataBind()
                Fillyears()


                gvEnter.Visible = True

                Dim sp1 As SqlParameter = New SqlParameter("@VerReqID", SqlDbType.NVarChar, 50)
                sp1.Value = strReqID
                Dim sp2 As SqlParameter = New SqlParameter("@mon", SqlDbType.DateTime)
                sp2.Value = dt1
                Dim sp3 As SqlParameter = New SqlParameter("@yer", SqlDbType.DateTime)
                sp3.Value = dtYear


                dtVerReqDetails = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_get_VerticalDetails", sp1, sp2, sp3)
                ViewState("dtVerReqDetails") = dtVerReqDetails
                ddlSelectLocation.SelectedIndex = ddlSelectLocation.Items.IndexOf(ddlSelectLocation.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_LOC_ID").ToString().Trim()))
                ddlSelectLocation.Enabled = False
                ddlVertical.Enabled = False
                ddlCity.Enabled = False
                'ddlbcp.Enabled = False
                ddlspacetype.Enabled = False

                ddlVertical.SelectedIndex = ddlVertical.Items.IndexOf(ddlVertical.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_VERTICAL").ToString().Trim()))
                ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_CTY_ID").ToString().Trim()))

                txtRemarks.Text = dtVerReqDetails.Rows(0)("SVR_REM").ToString().Trim()


                Dim txtMonth As TextBox = CType(gvEnter.Rows(0).FindControl("txtMonth"), TextBox)
                Dim txtYear As TextBox = CType(gvEnter.Rows(0).FindControl("txtYear"), TextBox)

                Dim txtWorkstations As TextBox = CType(gvEnter.Rows(0).FindControl("txtWorkstations"), TextBox)
                Dim txtHalfcabinsrequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtHalfcabinsrequired"), TextBox)
                Dim txtFullCabinsRequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtFullCabinsRequired"), TextBox)
                txtMonth.Text = Convert.ToDateTime(dtVerReqDetails.Rows(0)("SVD_FROM").ToString().Trim()).ToShortDateString()
                txtYear.Text = Convert.ToDateTime(dtVerReqDetails.Rows(0)("SVD_TO").ToString().Trim()).ToShortDateString()
                txtWorkstations.Text = dtVerReqDetails.Rows(0)("SVD_WSTNO").ToString().Trim()
                txtHalfcabinsrequired.Text = dtVerReqDetails.Rows(0)("SVD_HCNO").ToString().Trim()
                txtFullCabinsRequired.Text = dtVerReqDetails.Rows(0)("SVD_FCNO").ToString().Trim()
                lblReqStatus.Text = dtVerReqDetails.Rows(0)("status").ToString().Trim()
                txtLabSpace.Text = dtVerReqDetails.Rows(0)("SVR_LABSPACE").ToString().Trim()

                'ddlbcp.Items.FindByValue(dtVerReqDetails.Rows(0)("BCPSTATUS")).Selected = True
                ddlspacetype.Items.FindByValue(dtVerReqDetails.Rows(0)("SPACETYPEID")).Selected = True

                'If strSatusID = 166 Then
                '    btnUpdate.Enabled = False
                'ElseIf strSatusID = 6 Or strSatusID = 8 Then
                '    btnUpdate.Enabled = False
                '    'btnCancel.Enabled = False

                'End If

                txtMonth.Enabled = False

                '--------- Bind Seats ----------'
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@SSA_SRNCC_ID", SqlDbType.NVarChar, 200)
                param(0).Value = Request.QueryString("ReqID").ToString().Trim()
                ObjSubSonic.BindGridView(gdavail, "GETBLOCKEDSEATS_allocate", param)
                '---------- Allocated to other vertical ---------- '
                Dim param1(0) As SqlParameter
                param1(0) = New SqlParameter("@SSA_SRNCC_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = Request.QueryString("ReqID").ToString().Trim()
                ObjSubSonic.BindGridView(gvSpaceAllocatedtoOtherVertical, "GETBLOCKEDSEATS_OTHERVERTICAL", param1)

If gdavail.Rows.Count > 0 Then
                    btnUpdate.Visible = True
                Else
                    btnUpdate.Visible = False

                End If
            Catch ex As Exception

                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Vertical Requisition", "Load", ex)

            End Try

        End If
    End Sub

    Public Sub LoadCity()
        obj.BindVerticalCity(ddlCity)
    End Sub
    Public Sub loadlocation()
        obj.Bindlocation(ddlSelectLocation)

    End Sub
    Public Sub loaddepartment()


        'Dim UID As String = ""
        'UID = Session("uid")

        'Dim param(0) As SqlParameter

        'param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        'param(0).Value = UID

        'ObjSubSonic.Binddropdown(ddlVertical, "GET_COSTCENTER_AURID", "COST_CENTER_NAME", "COST_CENTER_CODE", param)
        ObjSubSonic.Binddropdown(ddlVertical, "GET_ALL_VERTICALS", "Ver_Name", "Ver_Code")

    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim cntChk As Integer = 0
        For Each row As GridViewRow In gdavail.Rows

            Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                Dim param1(1) As SqlParameter
                param1(0) = New SqlParameter("@SVR_REQ_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = Request.QueryString("ReqID").ToString().Trim()
                param1(1) = New SqlParameter("@SPACEID", SqlDbType.NVarChar, 200)
                param1(1).Value = lblspcid.Text
                ObjSubSonic.GetSubSonicExecute("UPDATERESEVERBLOCKEDSEATS_allocateSeat", param1)

                UpdateRecord(LTrim(RTrim(lblspcid.Text)), 6, LTrim(RTrim(lblspcid.Text)))
                cntChk += 1
            End If

        Next
        If cntChk = gdavail.Rows.Count Then
            Dim param(2) As SqlParameter

            param(0) = New SqlParameter("@SVR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("ReqID").ToString().Trim()
            param(1) = New SqlParameter("@REMARKS", SqlDbType.NVarChar, 200)
            param(1).Value = txtRemarks.Text
            param(2) = New SqlParameter("@UPDATED_BY", SqlDbType.NVarChar, 200)
            param(2).Value = Session("uid")

            ObjSubSonic.GetSubSonicExecute("UPDATERESEVERBLOCKEDSEATS_allocate", param)

        End If





        'Response.Redirect("ViewReservedBlockedSeats.aspx")
        'UPDATERESEVERBLOCKEDSEATS
        SendMail(lblVerReqID.Text.Trim())
        Response.Redirect("finalpage.aspx?sta=6A&" & "&rid=" & clsSecurity.Encrypt(lblVerReqID.Text))
        'Response.Redirect("ReserveBlockedSeatsRequisitions.aspx")
        'UPDATERESEVERBLOCKEDSEATS
    End Sub
    Private Sub SendMail(ByVal REQ_ID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@strReqID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ_ID
        ObjSubSonic.GetSubSonicExecute("SEND_MAIL_VERTCALREQUISITIONALLOCATION", param)
    End Sub


    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function
    Private Sub Fillyears()
        For i As Integer = 0 To gvEnter.Rows.Count - 1
            Dim ddlYear As DropDownList = CType(gvEnter.Rows(i).FindControl("ddlYear"), DropDownList)
            For iYear As Integer = getoffsetdatetime(DateTime.Now).Year() To getoffsetdatetime(DateTime.Now).Year + 3
                ddlYear.Items.Add(iYear.ToString())
            Next
        Next
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If Session("View") = "DashBoard" Then
                Session("View") = ""
                strRedirect = "../../WebFiles/frmDetails.aspx"
            ElseIf Session("View") = "ViewVertical" Then
                Session("AcrdIndex") = ""
                Session("View") = ""
                strRedirect = "ViewReservedBlockedSeats.aspx"
            ElseIf Session("View") = "" Then
                strRedirect = "ViewReservedBlockedSeats.aspx"

            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while redirecting", "Dashboard view Vertical Requisition", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub

    Protected Sub gdavail_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gdavail.PageIndexChanging
        gdavail.PageIndex = e.NewPageIndex
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SSA_SRNCC_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("ReqID").ToString().Trim()
        ObjSubSonic.BindGridView(gdavail, "GETBLOCKEDSEATS_allocate", param)

    End Sub
End Class


