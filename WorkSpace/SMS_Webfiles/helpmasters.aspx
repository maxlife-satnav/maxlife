<%@ Page Language="VB" AutoEventWireup="false" CodeFile="helpmasters.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_helpmasters" Title="Help Desk Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
           <style>
        .btn {
            border-radius: 4px;   
            background-color : #3A618F; 
        }

    </style>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Help Desk Masters</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        
                        <div class="clearfix">
                            <div class="col-md-4 col-sm-12 col-xs-12 ">
                               
                                    <asp:HyperLink ID="HyperLink4" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHDMMainCategoryMaster.aspx">HD Main Category</asp:HyperLink>
                              
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                               
                                    <asp:HyperLink ID="HyperLink11" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmSubCategory.aspx">HD Sub Category</asp:HyperLink>
                                
                            </div>
                              <div class="col-md-4 col-sm-12 col-xs-12">
                                
                                    <asp:HyperLink ID="HyperLink12" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmChildCategory.aspx">HD Child Category</asp:HyperLink>
                                
                            </div>

                        </div><br />

                        <div class="clearfix">
                           <%-- <div class="col-md-4 col-sm-12 col-xs-12">
                         

                                    <asp:HyperLink ID="hpl" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmAddServiceCategory.aspx">Service Category</asp:HyperLink>

                              
                            </div>--%>


           <%--                 <div class="col-md-4 col-sm-12 col-xs-12">
                              

                                    <asp:HyperLink ID="hplreqtype" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmAddServiceType.aspx">Service Type</asp:HyperLink>

                             
                            </div>--%>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                              
                                    <asp:HyperLink ID="HyperLink5" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHDMAssetLocation.aspx">Asset Location</asp:HyperLink>
                            
                            </div>
                              <div class="col-md-4 col-sm-12 col-xs-12">
                             
                                    <asp:HyperLink ID="hplServiceEscalation" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmServiceEscalationMapping.aspx">Service Escalation Mapping</asp:HyperLink>
                               
                            </div>
                             <div class="col-md-4 col-sm-12 col-xs-12">
                             
                                    <asp:HyperLink ID="hplviewreqtypes" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmSLA.aspx">SLA Time Definition</asp:HyperLink>
                             
                            </div>
                           <%--   <div class="col-md-4 col-sm-12 col-xs-12 ">
                             
                                    <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmMasLocationSerivceEmpMapping.aspx">Location Service Incharge</asp:HyperLink>
                               
                            </div>--%> 
                        </div><br />
          

                        <div class="clearfix">
                             
                          
                                 <div class="col-md-4 col-sm-12 col-xs-12">
                                
                                    <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHDMRepeatCallMaster.aspx">Repeat Call</asp:HyperLink>
                               
                            </div>
                             <div class="col-md-4 col-sm-12 col-xs-12">
                           
                                    <asp:HyperLink ID="HyperLink9" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmImpact.aspx">Impact</asp:HyperLink>
                               
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                       
                                    <asp:HyperLink ID="HyperLink10" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmUrgency.aspx">Urgency</asp:HyperLink>
                               
                            </div>
                        

                            <%-- <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                                <div class="form-group">
                                    <asp:HyperLink ID="hplspctype" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCatVendorMaster.aspx">Category Vendor Master</asp:HyperLink>
                                </div>
                            </div>--%>
                        </div>
                        <br />

                        <%--<div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                                <div class="form-group">
                                    <asp:HyperLink ID="hplLoc" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmAddLocationMaster.aspx">Service Location Master(Optional)</asp:HyperLink>
                                </div>
                            </div>
                        </div>--%>
                        <div class="clearfix">
                            
                            <div class="col-md-4 col-sm-12 col-xs-12 ">
                          
                                    <asp:HyperLink ID="HyperLink8" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHolidayMaster.aspx">Holiday </asp:HyperLink>
                             
                            </div>
                               <div class="col-md-4 col-sm-12 col-xs-12">
                         
                                    <asp:HyperLink ID="HyperLink7" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmFeedBackMaster.aspx">Feedback </asp:HyperLink>
                           
                            </div>
                        </div><br />

           
                        <div class="clearfix">
                           <%-- <div class="col-md-4 col-sm-12 col-xs-12 ">
                             
                                    <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmPopulationClassifcation.aspx">Population Classification Master</asp:HyperLink>
                             
                            </div>--%>
                                 

                         
                        </div><br />
                        <div class="clearfix">
                      
                          
                            <%-- <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink9" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmFeedBackMaster.aspx">Feedback</asp:HyperLink>
                                </div>
                            </div>--%>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
