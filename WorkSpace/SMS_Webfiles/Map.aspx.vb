Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient


Partial Class WorkSpace_SMS_Webfiles_Map
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetLocationURL()
        End If

    End Sub

    Private Sub GetLocationURL()
        Dim LCM_CODE As String = ""
        Dim TWR_CODE As String = ""
        Dim FLR_CODE As String = ""
        Dim FLR_USR_MAP As String = ""

        LCM_CODE = Request.QueryString("lcm_code")
        TWR_CODE = Request.QueryString("twr_code")
        FLR_CODE = Request.QueryString("flr_code")


        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = LCM_CODE
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = TWR_CODE
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = FLR_CODE

        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_URL_LOCATION", param)
        If ds.Tables(0).Rows.Count > 0 Then
            FLR_USR_MAP = ds.Tables(0).Rows(0).Item("FLR_USR_MAP")
        End If

        'midFrame.Attributes("src") = FLR_USR_MAP
        midFrame.Attributes("src") = "../gis/UserLocationMap.aspx?lcm_code=" & LCM_CODE & "&twr_code=" & TWR_CODE & "&flr_code=" & FLR_CODE
    End Sub
End Class
