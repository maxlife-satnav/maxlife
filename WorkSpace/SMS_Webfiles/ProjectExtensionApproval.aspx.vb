﻿Imports System.Net
Imports System.Text
Imports Amantra.RequisitionDTON
Imports Amantra.RequisitionDALN
Imports Amantra.RequisitionBLLN
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_ProjectExtensionApproval
    Inherits System.Web.UI.Page
    Dim reqBll As New RequisitionBLL()
    Dim rDTO As New RequisitionDTO()
    Dim objExtendRelease As New clsExtenedRelease()
    Dim dt As New DataTable
    Dim dtRej As New DataTable
    Dim obj As New clsMasters()
    Dim strRedirect As String = String.Empty
    Dim Email As String = String.Empty
    Dim strEmail1 As String = String.Empty
    Dim strRM As String = String.Empty
    Dim strRR As String = String.Empty
    Dim strFMG As String = String.Empty
    Dim strBUHead As String = String.Empty
    Dim strVertical As String = String.Empty
    Dim strsql As String = String.Empty
    Dim stVrm As String = String.Empty
    Dim dr As SqlDataReader
    Dim dr1 As SqlDataReader
    Dim iQry As Integer = 0
    Dim strVRM As String = String.Empty
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Visible = False
        If Not Page.IsPostBack Then
            Try
                BindCity()
                'obj.Bindlocation(ddlLocation)
                ddlTower.Items.Insert(0, "--Select--")
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Extend", "Load", ex)
            End Try

        End If
    End Sub
    Private Sub BindCity()
        ObjSubSonic.Binddropdown(ddlCity, "GETALLCITIES", "CTY_NAME", "CTY_CODE")
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'obj.Bindlocation(ddlLocation)
        Dim cty_code As String = ""
        If ddlCity.SelectedItem.Text = "--Select--" Then
            lblMsg.Text = "Please select city."
            Exit Sub
        Else
            cty_code = ddlCity.SelectedItem.Value
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = cty_code

        ObjSubSonic.Binddropdown(ddlLocation, "GETACTIVELOCATIONBY_CCODE", "LCM_NAME", "LCM_CODE", param)

    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex > 0 Then
                obj.BindTowerLoc(ddlTower, ddlLocation.SelectedValue.Trim())
            Else
                gvSpaceExtension.Visible = False
                btnApprove.Visible = False
                btnReject.Visible = False
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Extend", "Load", ex)
        End Try

    End Sub

    Dim verBll As New VerticalBLL()
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
                ' ddlWing.Items.Clear()
                ' ddlWing.Items.Insert(0, "--Select--")
                ' ddlVertical.Items.Clear()
                'ddlVertical.Items.Insert(0, "--Select--")
                'ddlCostCenter.Items.Clear()
                'ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")
                ' ddlWing.Items.Clear()
                ' ddlWing.Items.Insert(0, "--Select--")
                ' ddlVertical.Items.Clear()
                'ddlVertical.Items.Insert(0, "--Select--")
                'ddlCostCenter.Items.Clear()
                'ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
        End Try

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try

            objExtendRelease.getProjectidforExtension(gvSpaceExtension, ddlTower.SelectedValue, ddlFloor.SelectedValue)
            If gvSpaceExtension.Rows.Count = 0 Then
                btnApprove.Visible = False
                btnReject.Visible = False
                lblMsg.Text = "No requistion for Approval"
                lblMsg.Visible = True
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Approval for Space Extend", "Load", ex)
        End Try

    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

        Try
            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)
                If chk.Checked = True Then
                    intCheckCount += 1
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    If txtRemarks.Text.Trim.Length > 500 Then
                        lblMsg.Text = "Please enter remarks in less than or equal to 500 characters"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If txtRemarks.Text.Trim.Length = 0 Then
                        lblMsg.Text = "Please enter remarks"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atlease one"
                lblMsg.Visible = True
                Exit Sub
            End If

            dt.Columns.Add("Sno", GetType(Integer))
            dt.Columns.Add("ProjectID", GetType(String))
            dt.Columns.Add("ExtDate", GetType(Date))
            dt.Columns.Add("FromDate", GetType(Date))
            dt.Columns.Add("ToDate", GetType(Date))
            dt.Columns.Add("Remarks", GetType(String))
            Dim drNew As DataRow
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)

                If chk.Checked = True Then
                    Dim lblReqID As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblReqID"), Label)
                    Dim lblExt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblextensionDate"), Label)
                    Dim lblToDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblToDate"), Label)
                    Dim lblFDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label9"), Label)
                    Dim lblTower As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label3"), Label)
                    Dim lblFloor As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label4"), Label)
                    Dim lblWing As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label5"), Label)
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    Dim lblVertical As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblVercode"), Label)



                    objExtendRelease.updateProjectExtensionApprove(txtRemarks.Text.Trim().Replace("'", "''"), lblExt.Text, lblReqID.Text, 4, ddlFloor.SelectedValue)

                    drNew = dt.NewRow
                    drNew(0) = dt.Rows.Count + 1
                    drNew(1) = lblReqID.Text
                    drNew(2) = lblExt.Text.Trim()
                    drNew(3) = lblFDt.Text.Trim()
                    drNew(4) = lblToDt.Text.Trim()
                    drNew(5) = txtRemarks.Text.Trim()
                    dt.Rows.Add(drNew)
                    ' Email = lblMail.Text

                    'sendMail(dt, lblTower.Text.Trim, lblFloor.Text.Trim(), lblWing.Text.Trim(), lblMail.Text, "Approve", strVertical)

                End If

            Next

            If dt.Rows.Count > 0 Then
                Session("ReleaseData") = dt
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("172") & ""
            End If

            If gvSpaceExtension.Rows.Count = 0 Then
                btnApprove.Visible = False
                btnReject.Visible = False
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Approving data.", "Default", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)
                If chk.Checked = True Then
                    intCheckCount += 1
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    If txtRemarks.Text.Trim.Length > 500 Then
                        lblMsg.Text = "Please enter remarks in less than or equal to 500 characters"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If txtRemarks.Text.Trim.Length = 0 Then
                        lblMsg.Text = "Please enter remarks"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atleast one space"
                lblMsg.Visible = True
                Exit Sub
            End If
            dtRej.Columns.Add("Sno", GetType(Integer))
            dtRej.Columns.Add("ProjectID", GetType(String))
            dtRej.Columns.Add("ExtDate", GetType(Date))
            dtRej.Columns.Add("FromDate", GetType(Date))
            dtRej.Columns.Add("ToDate", GetType(Date))
            dtRej.Columns.Add("Remarks", GetType(String))
            Dim drNew As DataRow
            For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtension.Rows(i).FindControl("chk"), CheckBox)
                If chk.Checked = True Then
                    Dim lblReqID As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblReqID"), Label)
                    Dim txtRemarks As TextBox = CType(gvSpaceExtension.Rows(i).FindControl("txtRem"), TextBox)
                    Dim lblTower As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label3"), Label)
                    Dim lblFloor As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label4"), Label)
                    Dim lblWing As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label5"), Label)
                    Dim lblVertical As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblVercode"), Label)
                    Dim lblExt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblextensionDate"), Label)
                    Dim lblToDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("lblToDate"), Label)
                    Dim lblFDt As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label9"), Label)

                    objExtendRelease.updateProjectExtensionApprove(txtRemarks.Text.Trim().Replace("'", "''"), CType(gvSpaceExtension.Rows(i).FindControl("lblToDate"), Label).Text, lblReqID.Text, 7, ddlFloor.SelectedValue)

                    drNew = dtRej.NewRow
                    drNew(0) = dtRej.Rows.Count + 1
                    drNew(1) = lblReqID.Text
                    drNew(2) = lblExt.Text.Trim()
                    drNew(3) = lblFDt.Text.Trim()
                    drNew(4) = lblToDt.Text.Trim()
                    drNew(5) = txtRemarks.Text.Trim()
                    dtRej.Rows.Add(drNew)
                    'sendMail(dtRej, lblTower.Text, lblFloor.Text, lblWing.Text, lblMail.Text, "Reject", strVertical)


                    'strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & lblVertical.Text & "'"
                    'iQry = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL1))
                    'If iQry > 0 Then
                    '    strSQL1 = "Select VER_VRM from VERTICAL where VER_CODE='" & lblVertical.Text & "'"
                    '    dr1 = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
                    '    While dr1.Read
                    '        stVrm = dr1("VER_VRM").ToString()
                    '    End While
                    '    strsql = "Select aur_email from amantra_user where aur_id='" & stVrm & "'"
                    '    dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
                    '    While dr.Read
                    '        strVRM = dr("aur_email").ToString()
                    '    End While
                    'Else
                    '    strVRM = ConfigurationManager.AppSettings("AmantraEmailId").ToString
                    'End If

                    'If (strVertical = String.Empty) Then

                    '    strVertical = strVRM
                    'Else
                    '    strVertical = strVertical + "," + strVRM
                    'End If

                    'sendMail(lblSpaceID.Text, gvSpaceExtension.Rows(i).Cells(8).Text, gvSpaceExtension.Rows(i).Cells(7).Text, gvSpaceExtension.Rows(i).Cells(3).Text, gvSpaceExtension.Rows(i).Cells(4).Text, gvSpaceExtension.Rows(i).Cells(5).Text, Email)
                End If
            Next
            'For i As Integer = 0 To gvSpaceExtension.Rows.Count - 1
            '    Dim lblTower As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label3"), Label)
            '    Dim lblFloor As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label4"), Label)
            '    Dim lblWing As Label = CType(gvSpaceExtension.Rows(i).FindControl("Label5"), Label)
            '    sendMail(dt, lblTower.Text.Trim, lblFloor.Text.Trim(), lblWing.Text.Trim(), Email, "Reject", strVertical)

            'Next
            If dtRej.Rows.Count > 0 Then
                Session("ReleaseData") = dtRej
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("203") & ""
            End If
            If gvSpaceExtension.Rows.Count = 0 Then
                btnApprove.Visible = False
                btnReject.Visible = False
                ddlTower.SelectedIndex = 0
            Else
                btnApprove.Visible = True
                btnReject.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Rejecting data.", "Approval for Space Extend", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub
End Class
