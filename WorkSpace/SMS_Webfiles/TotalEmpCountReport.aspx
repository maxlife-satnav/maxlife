<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="TotalEmpCountReport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_TotalEmpCountReport"
    Title="Consolidated Report" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <contenttemplate>
<TABLE id="table2" cellSpacing=0 cellPadding=0 width="95%" align=center border=0><TBODY><TR><TD align=center width="100%"><asp:Label id="lblHead" runat="server" ForeColor="Black" Font-Underline="False" Width="95%" CssClass="clsHead">Wipro Consolidated Report 
             <hr align="center" width="60%" /></asp:Label></TD></TR></TBODY></TABLE><TABLE id="table3" cellSpacing=0 cellPadding=0 width="95%" align=center border=0><TBODY><TR><TD align=left width="100%" colSpan=3>(*) Mandatory Fields. </TD></TR><TR><TD><IMG height=27 alt="" src="../../Images/table_left_top_corner.gif" width=9 /></TD><TD style="WIDTH: 1058px" class="tableHEADER" align=left><STRONG>&nbsp; Wipro Consolidated Report </STRONG></TD><TD><IMG height=27 alt="" src="../../Images/table_right_top_corner.gif" width=16 /></TD></TR><TR><TD background="../../Images/table_left_mid_bg.gif">&nbsp;</TD><TD style="WIDTH: 1058px" align=left><asp:ValidationSummary id="ValidationSummary1" runat="server" ForeColor="" CssClass="clsMessage" ValidationGroup="Val1"></asp:ValidationSummary> <BR />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label id="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><BR />
                 
                 <TABLE id="tab1" cellSpacing=0 cellPadding=1 width="100%" border=1 ><TBODY><TR><TD style="WIDTH: 50%; HEIGHT: 26px" align=left>Select City <asp:RequiredFieldValidator id="rfvcity" runat="server" InitialValue="0" ErrorMessage="Please select City" ControlToValidate="ddlcity" Display="Dynamic">
                            </asp:RequiredFieldValidator> </TD><TD style="WIDTH: 50%; HEIGHT: 26px" align=left><asp:DropDownList id="ddlcity" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged">
                            </asp:DropDownList> </TD></TR><TR><TD align=right colSpan=2><asp:Button id="btnExport" runat="server" Text="Export To Excel" Width="136px" CssClass="clsButton" Visible="false"></asp:Button> </TD></TR><TR><TD style="HEIGHT: 20px" colSpan=2><cc1:ExportPanel id="exp_CityWise" runat="server"><asp:GridView id="gv_report" runat="server" Width="100%" BorderColor="Gray" EmptyDataText="No records found." AutoGenerateColumns="False" AllowPaging="True"><Columns>
<asp:BoundField DataField="CTY_NAME" HeaderText="Circle / City"></asp:BoundField>
<asp:BoundField DataField="LCM_NAME" HeaderText="Branch"></asp:BoundField>
<asp:BoundField DataField="AVAILABLE_WT" HeaderText="AVAILABLE WT"></asp:BoundField>
<asp:BoundField DataField="OCCUPIED_WT" HeaderText="AVAILABLE WT"></asp:BoundField>
<asp:BoundField DataField="AVAILABLE_WI" HeaderText="AVAILABLE WI"></asp:BoundField>
<asp:BoundField DataField="OCCUPIED_WI" HeaderText="AVAILABLE WI"></asp:BoundField>
<asp:BoundField DataField="AVAILABLE_WBPO" HeaderText="AVAILABLE WBPO"></asp:BoundField>
<asp:BoundField DataField="OCCUPIED_WBPO" HeaderText="AVAILABLE WBPO"></asp:BoundField>
</Columns>

<PagerStyle BackColor="#CCCCCC"></PagerStyle>
</asp:GridView> </cc1:ExportPanel> </TD></TR></TBODY></TABLE></TD><TD style="WIDTH: 17px; HEIGHT: 100%" background="../../Images/table_right_mid_bg.gif">&nbsp;</TD></TR><TR><TD style="WIDTH: 10px; HEIGHT: 17px"><IMG height=17 alt="" src="../../Images/table_left_bot_corner.gif" width=9 /></TD><TD style="WIDTH: 1058px; HEIGHT: 17px" background="../../Images/table_bot_mid_bg.gif"><IMG height=17 alt="" src="../../Images/table_bot_mid_bg.gif" width=25 /></TD><TD style="WIDTH: 17px; HEIGHT: 17px"><IMG height=17 alt="" src="../../Images/table_right_bot_corner.gif" width=16 /></TD></TR></TBODY></TABLE>
</contenttemplate>
        <triggers>
          <asp:PostBackTrigger ControlID="btnExport" />
      </triggers>
    </asp:UpdatePanel>
</asp:Content>
