Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class WorkSpace_SMS_Webfiles_Projectcostreport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            'txtdate.Attributes.Add("onClick", "displayDatePicker('" + txtdate.ClientID + "')")
            'txtdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            Get_Project()
            ddlproject.Items(0).Text = "--All " & Session("Child") & "(s)--"
            BindGrid()
            'rfvloc.ErrorMessage = "Please Select " + Session("Child")
            'btnExport.Visible = False
        End If
    End Sub
    Private Sub Get_Project()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROJECT")
        ddlproject.DataSource = sp.GetDataSet()
        ddlproject.DataTextField = "PROJECT_NAME"
        ddlproject.DataValueField = "PROJECT_CODE"
        ddlproject.DataBind()
        ddlproject.Items(0).Text = "--All " & Session("Child") & "(s)--"
        ddlproject.Items(0).Value = ""
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        BindGrid()

    End Sub
    Private Sub BindGrid()

        Dim rds As New ReportDataSource()
        rds.Name = "ProjectCostDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/ProjectCostReport.rdlc")
        Dim p1 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)
        Dim p3 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p3)
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p2 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p2)

        'Setting Header Column value dynamically
        'Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        'ReportViewer1.LocalReport.SetParameters(p1)

        'Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        'ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim project As String = ""
        Dim tilldate As String = ""

        If ddlproject.SelectedValue = "--All " & Session("Child") & "(s)--" Then
            project = ""
        Else
            project = ddlproject.SelectedValue
        End If


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROJECT_WISE_COST")
        sp.Command.AddParameter("@PROJECT", project, DbType.String)
        sp.Command.AddParameter("@DATE", tilldate, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        rds.Value = ds.Tables(0)

    End Sub

   
    Protected Sub ddlproject_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproject.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class
