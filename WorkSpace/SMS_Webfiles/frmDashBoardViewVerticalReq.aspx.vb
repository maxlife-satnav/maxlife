'***********************
' 5   - Requsted
' 6   - Allocated
' 166 - Partially Allocated
' 8   - Cancelled  
'***********************

Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Data.SqlClient
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports System.Net
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_frmDashBoardViewVerticalReq

    Inherits System.Web.UI.Page
    Dim objclsViewCancelChange As clsViewCancelChange
    Dim verBll As New VerticalBLL()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim obj As clsMasters
    Dim REQID As String
    Dim strStatus As String
    Dim RIDDS As String
    Dim dtVerReqDetails As DataTable
    Dim strRedirect As String = String.Empty

    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""
        Label1.Text = Session("Parent") + " Requisition ID  "
        If Not Page.IsPostBack Then
            Try
                Session("AcrdIndex") = "1"
                obj = New clsMasters()
                Dim strReqId1 As String
                
                strReqId1 = Request.QueryString("ReqID").ToString().Trim()
                lblSelVertical.Text = Session("Parent")
                rfvVTl.ErrorMessage = "Please Select " + lblSelVertical.Text
                Dim strReqID As String = strReqId1
                Dim strMonth As String = CType(Request.QueryString("Month").ToString().Trim(), String)
                Dim strSatusID As String = Request.QueryString("StaID").ToString().Trim()
                Dim dt1 As DateTime = Convert.ToDateTime(strMonth)
 
                RIDDS = obj.RIDGENARATION("VerticalReq")

                Dim strYear As String = CType(Request.QueryString("Year").ToString().Trim(), String)
                Dim dtYear As DateTime = Convert.ToDateTime(strYear)
                lblVerReqID.Text = strReqID
                loadlocation()
                LoadCity()
                loaddepartment()
                ObjSubSonic.Binddropdown(ddlspacetype, "GETSPACETYPE_NOTBCP", "SPACE_TYPE", "SNO")
 

                obj = New clsMasters

                Dim dt As New DataTable
                Dim i As Integer
                dt.Columns.Add("sno")
                For i = 0 To 0
                    Dim dr As DataRow = dt.NewRow
                    dr(0) = i + 1
                    dt.Rows.Add(dr)
                Next


                gvEnter.DataSource = dt
                gvEnter.DataBind()
                Fillyears()


                gvEnter.Visible = True

                Dim sp1 As SqlParameter = New SqlParameter("@VerReqID", SqlDbType.NVarChar, 50)
                sp1.Value = strReqID
                Dim sp2 As SqlParameter = New SqlParameter("@mon", SqlDbType.DateTime)
                sp2.Value = dt1
                Dim sp3 As SqlParameter = New SqlParameter("@yer", SqlDbType.DateTime)
                sp3.Value = dtYear
 

                dtVerReqDetails = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_get_VerticalDetails", sp1, sp2, sp3)
                ViewState("dtVerReqDetails") = dtVerReqDetails
                ddlSelectLocation.SelectedIndex = ddlSelectLocation.Items.IndexOf(ddlSelectLocation.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_LOC_ID").ToString().Trim()))
                ddlSelectLocation.Enabled = False
                ddlVertical.Enabled = False
                ddlCity.Enabled = False
                'ddlbcp.Enabled = False
                ddlspacetype.Enabled = False

                ddlVertical.SelectedIndex = ddlVertical.Items.IndexOf(ddlVertical.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_VERTICAL").ToString().Trim()))
                ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(dtVerReqDetails.Rows(0)("SVR_CTY_ID").ToString().Trim()))

                txtRemarks.Text = dtVerReqDetails.Rows(0)("SVR_REM").ToString().Trim()
                

                Dim txtMonth As TextBox = CType(gvEnter.Rows(0).FindControl("txtMonth"), TextBox)
                Dim txtYear As TextBox = CType(gvEnter.Rows(0).FindControl("txtYear"), TextBox)

                Dim txtWorkstations As TextBox = CType(gvEnter.Rows(0).FindControl("txtWorkstations"), TextBox)
                Dim txtHalfcabinsrequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtHalfcabinsrequired"), TextBox)
                Dim txtFullCabinsRequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtFullCabinsRequired"), TextBox)
                    txtMonth.Text = Convert.ToDateTime(dtVerReqDetails.Rows(0)("SVD_FROM").ToString().Trim()).ToShortDateString()
                txtYear.Text = Convert.ToDateTime(dtVerReqDetails.Rows(0)("SVD_TO").ToString().Trim()).ToShortDateString()
                txtWorkstations.Text = dtVerReqDetails.Rows(0)("SVD_WSTNO").ToString().Trim()
                txtHalfcabinsrequired.Text = dtVerReqDetails.Rows(0)("SVD_HCNO").ToString().Trim()
                txtFullCabinsRequired.Text = dtVerReqDetails.Rows(0)("SVD_FCNO").ToString().Trim()
                lblReqStatus.Text = dtVerReqDetails.Rows(0)("status").ToString().Trim()
                txtLabSpace.Text = dtVerReqDetails.Rows(0)("SVR_LABSPACE").ToString().Trim()

                'ddlbcp.Items.FindByValue(dtVerReqDetails.Rows(0)("BCPSTATUS")).Selected = True
                ddlspacetype.Items.FindByValue(dtVerReqDetails.Rows(0)("SPACETYPEID")).Selected = True

                If strSatusID = 166 Then
                    btnUpdate.Enabled = False
                ElseIf strSatusID = 6 Or strSatusID = 8 Then
                    btnUpdate.Enabled = False
                    btnCancel.Enabled = False

                End If

                txtMonth.Enabled = False

                '--------- Bind Seats ----------'
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@SSA_SRNCC_ID", SqlDbType.NVarChar, 200)
                param(0).Value = Request.QueryString("ReqID").ToString().Trim()
                ObjSubSonic.BindGridView(gdavail, "GETBLOCKEDSEATS", param)
                '---------- Allocated to other vertical ---------- '
                Dim param1(0) As SqlParameter
                param1(0) = New SqlParameter("@SSA_SRNCC_ID", SqlDbType.NVarChar, 200)
                param1(0).Value = Request.QueryString("ReqID").ToString().Trim()
                ObjSubSonic.BindGridView(gvSpaceAllocatedtoOtherVertical, "GETBLOCKEDSEATS_OTHERVERTICAL", param1)


            Catch ex As Exception

                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Vertical Requisition", "Load", ex)
 
            End Try

        End If
    End Sub

    Public Sub LoadCity()
        obj.BindVerticalCity(ddlCity)
    End Sub
    Public Sub loadlocation()
        obj.Bindlocation(ddlSelectLocation)
       
    End Sub
    Public Sub loaddepartment()
        

        'Dim UID As String = ""
        'UID = Session("uid")

        'Dim param(0) As SqlParameter

        'param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        'param(0).Value = UID

        'ObjSubSonic.Binddropdown(ddlVertical, "GET_COSTCENTER_AURID", "COST_CENTER_NAME", "COST_CENTER_CODE", param)
        ObjSubSonic.Binddropdown(ddlVertical, "GET_ALL_VERTICALS", "Ver_Name", "Ver_Code")
 
    End Sub
    
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click


        dtVerReqDetails = ViewState("dtVerReqDetails")
        Dim dtTemp As DataTable = New DataTable
        dtTemp.Columns.Add("Sno")
        dtTemp.Columns.Add("Month")
        dtTemp.Columns.Add("Year")
        dtTemp.Columns.Add("WS")
        dtTemp.Columns.Add("HC")
        dtTemp.Columns.Add("FC")
        Dim drNew As DataRow
        Dim txtMonth As TextBox = CType(gvEnter.Rows(0).FindControl("txtMonth"), TextBox)
        Dim txtYear As TextBox = CType(gvEnter.Rows(0).FindControl("txtYear"), TextBox)
        Dim txtWorkstations As TextBox = CType(gvEnter.Rows(0).FindControl("txtWorkstations"), TextBox)
        Dim txtHalfcabinsrequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtHalfcabinsrequired"), TextBox)
        Dim txtFullCabinsRequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtFullCabinsRequired"), TextBox)
        If txtMonth.Text = String.Empty Then
            lblMsg.Text = "Please select Month"
            Exit Sub
        End If
        If txtYear.Text = String.Empty Then
            lblMsg.Text = "Please select Year"
            Exit Sub
        End If

        If txtWorkstations.Text.Trim() = "" And txtHalfcabinsrequired.Text.Trim() = "" And txtFullCabinsRequired.Text.Trim() = "" Then
            lblMsg.Text = "Please Enter Spaces Required"
            Exit Sub
        End If
        If txtRemarks.Text.Trim().Length > 500 Then
            lblMsg.Text = "Remarks allows only 500 Characters"
            Exit Sub
        End If
        If Convert.ToDateTime(txtYear.Text) < Convert.ToDateTime(txtMonth.Text) Then
            lblMsg.Text = "Please Select Valid Date To Date cannot be less than From Date"
            Exit Sub
        End If

        If Convert.ToDateTime(txtYear.Text).Month < getoffsetdatetime(DateTime.Now).Month And Convert.ToDateTime(txtYear.Text).Year = getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "Month should be greater than or equal to present month in the same year"
            Exit Sub
        End If
        If Convert.ToDateTime(txtYear.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "Year Cannot be less Than Present Year"
            Exit Sub
        End If

        Dim iWS, iHC, iFC As Integer
        Try
            If txtWorkstations.Text = String.Empty Then
                iWS = 0
            Else
                iWS = Convert.ToInt32(txtWorkstations.Text.Trim())
            End If
        Catch ex As Exception
            lblMsg.Text = "Please enter numerics only in Work stations Required"
            Exit Sub
        End Try

        Try
            If txtHalfcabinsrequired.Text = String.Empty Then
                iHC = 0
            Else
                iHC = Convert.ToInt32(txtHalfcabinsrequired.Text.Trim())
            End If
        Catch ex As Exception
            lblMsg.Text = "Please enter numerics only in Half cabins Required"
            Exit Sub
        End Try
        Try
            If txtFullCabinsRequired.Text = String.Empty Then
                iFC = 0
            Else
                iFC = Convert.ToInt32(txtFullCabinsRequired.Text)
            End If
        Catch ex As Exception
            lblMsg.Text = "Please enter numerics only in Full cabins Required"
            Exit Sub
        End Try

        For iGrdVal As Integer = 0 To gvEnter.Rows.Count - 1

            Dim txtMonth1 As String = CType(gvEnter.Rows(iGrdVal).FindControl("txtMonth"), TextBox).Text
            Dim txtYear1 As String = CType(gvEnter.Rows(iGrdVal).FindControl("txtYear"), TextBox).Text
            Dim dtMonth As DateTime = Convert.ToDateTime(txtMonth1).ToShortDateString()
            Dim dtYear As DateTime = Convert.ToDateTime(txtYear1).ToShortDateString()

            If CType(gvEnter.Rows(iGrdVal).FindControl("txtWorkstations"), TextBox).Text = String.Empty Then
                iWS = 0
            Else
                iWS = Convert.ToInt32(CType(gvEnter.Rows(iGrdVal).FindControl("txtWorkstations"), TextBox).Text)
            End If
            If CType(gvEnter.Rows(iGrdVal).FindControl("txtHalfcabinsrequired"), TextBox).Text = String.Empty Then
                iHC = 0
            Else
                iHC = Convert.ToInt32(CType(gvEnter.Rows(iGrdVal).FindControl("txtHalfcabinsrequired"), TextBox).Text)
            End If
            If CType(gvEnter.Rows(iGrdVal).FindControl("txtFullCabinsRequired"), TextBox).Text = String.Empty Then
                iFC = 0
            Else
                iFC = Convert.ToInt32(CType(gvEnter.Rows(iGrdVal).FindControl("txtFullCabinsRequired"), TextBox).Text)
            End If

            strSQL = " exec " & Session("TENANT") & "."  & "usp_vertical_requisition '" & ddlSelectLocation.SelectedValue & "', '" & dtMonth & "','" & dtYear & "'"
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            While reader.Read()
                If iWS > 0 Then
                    If iWS > Convert.ToInt32(reader("work station")) Then
                        lblMsg.Text = "Available Workstations for " & dtMonth.Month & "-" & dtYear.Year & " is " & Convert.ToInt32(reader("work station"))
                        Exit Sub
                    End If
                End If
                If iHC > 0 Then
                    If iHC > Convert.ToInt32(reader("Half Cabin")) Then
                        lblMsg.Text = "Available HalfCabins for " & dtMonth.Month & "-" & dtYear.Year & "  is " & Convert.ToInt32(reader("Half Cabin"))
                        Exit Sub
                    End If
                End If
                If iFC > 0 Then
                    If iFC > Convert.ToInt32(reader("Full Cabin")) Then
                        '                        lblMsg.Text = "Available FullCabins for " & ddlMonth1.SelectedItem.Text & "-" & iYear1 & " is " & Convert.ToInt32(reader("FullCabins"))
                        lblMsg.Text = "Available FullCabins for " & dtMonth.Month & "-" & dtYear.Year & " is " & Convert.ToInt32(reader("Full Cabin"))
                        Exit Sub
                    End If
                End If
            End While
        Next


        Dim intCount As Int16 = 0
        Try
            Dim spReqID As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
            spReqID.Value = lblVerReqID.Text.Trim()
            Dim spWsNo As New SqlParameter("@VC_WSTNO", SqlDbType.NVarChar, 50)
            Dim spHcNo As New SqlParameter("@VC_HCNO", SqlDbType.NVarChar, 50)
            Dim spFcNo As New SqlParameter("@VC_FCNO", SqlDbType.NVarChar, 50)
            Dim spMon As New SqlParameter("@VC_MON", SqlDbType.DateTime)
            Dim spYear As New SqlParameter("@VC_YEAR", SqlDbType.DateTime)
            Dim spStatusID As New SqlParameter("@VC_STA_ID", SqlDbType.NVarChar, 50)
            spWsNo.Value = txtWorkstations.Text.Trim()
            spHcNo.Value = txtHalfcabinsrequired.Text.Trim()
            spFcNo.Value = txtFullCabinsRequired.Text.Trim()
            spMon.Value = Convert.ToDateTime(txtMonth.Text).ToShortDateString()
            spYear.Value = Convert.ToDateTime(txtYear.Text).ToShortDateString()
            spStatusID.Value = 5
            Dim spStatus As New SqlParameter("i_Status", SqlDbType.Int)
            spStatus.Value = 1
            intCount = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_VERTICALREQ", spReqID, spWsNo, spHcNo, spFcNo, spMon, spYear, spStatusID, spStatus)
            Dim spReqID1 As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
            spReqID1.Value = lblVerReqID.Text
            Dim spReqDs As New SqlParameter("@VC_REQ_DS", SqlDbType.NVarChar, 50)
            spReqDs.Value = lblVerReqID.Text
            Dim spReqDate As New SqlParameter("@VC_REQ_DATE", SqlDbType.DateTime, 50)
            spReqDate.Value = getoffsetdatetime(DateTime.Now)
            Dim spReqStaID As New SqlParameter("@VC_STA_ID", SqlDbType.NVarChar, 50)
            spReqStaID.Value = 5
            Dim spReqBy As New SqlParameter("@VC_REQ_BY", SqlDbType.NVarChar, 50)
            spReqBy.Value = Session("uid").ToString().Trim()
            Dim spReqWsNo As New SqlParameter("@VC_WST_REQ", SqlDbType.NVarChar, 50)
            spReqWsNo.Value = 0
            Dim spReqFcNo As New SqlParameter("@VC_CAB_REQ", SqlDbType.NVarChar, 50)
            spReqFcNo.Value = 0
            Dim spReqHcNo As New SqlParameter("@VC_CUB_REQ", SqlDbType.NVarChar, 50)
            spReqHcNo.Value = 0
            Dim spReqRem As New SqlParameter("@VC_REM", SqlDbType.NVarChar, 50)
            spReqRem.Value = Replace(Trim(txtRemarks.Text), "'", "''")
            Dim spReqDepID As New SqlParameter("@VC_DEP_ID", SqlDbType.NVarChar, 50)
            spReqDepID.Value = ddlVertical.SelectedItem.Value
            Dim spReqLocID As New SqlParameter("@VC_LOC_ID", SqlDbType.NVarChar, 50)
            spReqLocID.Value = ddlSelectLocation.SelectedItem.Value
            Dim spReqFromDate As New SqlParameter("@VC_FROM_DATE", SqlDbType.DateTime, 50)
            spReqFromDate.Value = getoffsetdatetime(DateTime.Now)
            Dim spReqToDate As New SqlParameter("@VC_TO_DATE", SqlDbType.DateTime, 50)
            spReqToDate.Value = getoffsetdatetime(DateTime.Now)
            Dim spReqVertical As New SqlParameter("@VC_VERTICAL", SqlDbType.NVarChar, 50)
            spReqVertical.Value = ddlVertical.SelectedValue
            Dim spReqLabSpace As New SqlParameter("@VC_LABSPACE", SqlDbType.NVarChar, 50)
            spReqLabSpace.Value = txtLabSpace.Text
            Dim spSt As New SqlParameter("@i_Status =0", SqlDbType.Int)
            spSt.Value = 1
            intCount = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_VERTICALREQUISITION", spReqID1, spReqDs, spReqDate, spReqStaID, spReqBy, spReqWsNo, spReqFcNo, spReqHcNo, spReqRem, spReqDepID, spReqLocID, spReqFromDate, spReqToDate, spReqVertical, spReqLabSpace, spStatus)
            strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("5") & "&rid=" & clsSecurity.Encrypt(lblVerReqID.Text)
            lblMsg.Text = "Request has been Updated"
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Updating data.", "Dashboard view Vertical Requisition", "Load", ex)

        End Try
        strStatus = 1
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_VER_REQ_MAIL")
        'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
        'sp.Command.AddParameter("@REQID", lblVerReqID.Text.Trim(), DbType.String)
        'sp.Command.AddParameter("@MAILSTATUS", 2, DbType.Int32)
        'sp.ExecuteScalar()
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim dtTemp As DataTable = New DataTable
        Try

            dtVerReqDetails = ViewState("dtVerReqDetails")

            dtTemp.Columns.Add("Sno")
            dtTemp.Columns.Add("Month")
            dtTemp.Columns.Add("Year")
            dtTemp.Columns.Add("WS")
            dtTemp.Columns.Add("HC")
            dtTemp.Columns.Add("FC")
            Dim drNew As DataRow
            drNew = dtTemp.NewRow
            drNew(0) = dtTemp.Rows.Count + 1

            Dim txtMonth As TextBox = CType(gvEnter.Rows(0).FindControl("txtMonth"), TextBox)
            Dim txtYear As TextBox = CType(gvEnter.Rows(0).FindControl("txtYear"), TextBox)
            Dim txtWorkstations As TextBox = CType(gvEnter.Rows(0).FindControl("txtWorkstations"), TextBox)
            Dim txtHalfcabinsrequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtHalfcabinsrequired"), TextBox)
            Dim txtFullCabinsRequired As TextBox = CType(gvEnter.Rows(0).FindControl("txtFullCabinsRequired"), TextBox)
            If Convert.ToDateTime(txtYear.Text) < Convert.ToDateTime(txtMonth.Text) Then
                lblMsg.Text = "Please Select Valid Date To Date cannot be less than From Date"
                Exit Sub
            End If

            drNew("Month") = txtMonth.Text
            drNew("Year") = txtYear.Text
            drNew("WS") = txtWorkstations.Text.Trim
            drNew("HC") = txtHalfcabinsrequired.Text.Trim
            drNew("FC") = txtFullCabinsRequired.Text
            dtTemp.Rows.Add(drNew)
            Dim spReqID As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
            spReqID.Value = lblVerReqID.Text.Trim()
            Dim spWsNo As New SqlParameter("@VC_WSTNO", SqlDbType.NVarChar, 50)
            Dim spHcNo As New SqlParameter("@VC_HCNO", SqlDbType.NVarChar, 50)
            Dim spFcNo As New SqlParameter("@VC_FCNO", SqlDbType.NVarChar, 50)
            Dim spMon As New SqlParameter("@VC_MON", SqlDbType.DateTime)
            Dim spYear As New SqlParameter("@VC_YEAR", SqlDbType.DateTime)
            Dim spStatusID As New SqlParameter("@VC_STA_ID", SqlDbType.NVarChar, 50)
            spWsNo.Value = txtWorkstations.Text.Trim()
            spHcNo.Value = txtHalfcabinsrequired.Text.Trim()
            spFcNo.Value = txtFullCabinsRequired.Text.Trim()
            spMon.Value = Convert.ToDateTime(txtMonth.Text).ToShortDateString()
            spYear.Value = Convert.ToDateTime(txtYear.Text).ToShortDateString()


            spStatusID.Value = 10

            Dim spStatus As New SqlParameter("i_Status", SqlDbType.Int)
            spStatus.Value = 2

            Dim intCount As Int16 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_VERTICALREQ", spReqID, spWsNo, spHcNo, spFcNo, spMon, spYear, spStatusID, spStatus)
            If intCount >= 1 Then
                btnUpdate.Enabled = False

                intCount = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_Req_count", spReqID)
                If intCount <= 0 Then
                    Dim spReqID1 As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
                    spReqID1.Value = lblVerReqID.Text
                    Dim spReqDs As New SqlParameter("@VC_REQ_DS", SqlDbType.NVarChar, 50)
                    spReqDs.Value = RIDDS
                    Dim spReqDate As New SqlParameter("@VC_REQ_DATE", SqlDbType.DateTime, 50)
                    spReqDate.Value = getoffsetdatetime(DateTime.Now)
                    Dim spReqStaID As New SqlParameter("@VC_STA_ID", SqlDbType.NVarChar, 50)
                    spReqStaID.Value = 10
                    Dim spReqBy As New SqlParameter("@VC_REQ_BY", SqlDbType.NVarChar, 50)
                    spReqBy.Value = Session("uid").ToString().Trim()
                    Dim spReqWsNo As New SqlParameter("@VC_WST_REQ", SqlDbType.NVarChar, 50)
                    spReqWsNo.Value = 0
                    Dim spReqFcNo As New SqlParameter("@VC_CAB_REQ", SqlDbType.NVarChar, 50)
                    spReqFcNo.Value = 0
                    Dim spReqHcNo As New SqlParameter("@VC_CUB_REQ", SqlDbType.NVarChar, 50)
                    spReqHcNo.Value = 0
                    Dim spReqRem As New SqlParameter("@VC_REM", SqlDbType.NVarChar, 50)
                    spReqRem.Value = Replace(Trim(txtRemarks.Text), "'", "''")
                    Dim spReqDepID As New SqlParameter("@VC_DEP_ID", SqlDbType.NVarChar, 50)
                    spReqDepID.Value = ddlVertical.SelectedItem.Value
                    Dim spReqLocID As New SqlParameter("@VC_LOC_ID", SqlDbType.NVarChar, 50)
                    spReqLocID.Value = ddlSelectLocation.SelectedItem.Value
                    Dim spReqFromDate As New SqlParameter("@VC_FROM_DATE", SqlDbType.DateTime, 50)
                    spReqFromDate.Value = getoffsetdatetime(DateTime.Now)
                    Dim spReqToDate As New SqlParameter("@VC_TO_DATE", SqlDbType.DateTime, 50)
                    spReqToDate.Value = getoffsetdatetime(DateTime.Now)
                    Dim spReqVertical As New SqlParameter("@VC_VERTICAL", SqlDbType.NVarChar, 50)
                    spReqVertical.Value = ddlVertical.SelectedValue
                    Dim spReqLabSpace As New SqlParameter("@VC_LABSPACE", SqlDbType.NVarChar, 50)
                    spReqLabSpace.Value = txtLabSpace.Text
                    Dim spSt As New SqlParameter("@i_Status", SqlDbType.Int)
                    spSt.Value = 2
                    ' strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_VERTICAL_REQUISITION SET SVR_STA_ID ='8' where SVR_REQ_ID ='" & lblVerReqID.Text.Trim() & "' "
                    intCount = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_VERTICALREQUISITION", spReqID1, spReqDs, spReqDate, spReqStaID, spReqBy, spReqWsNo, spReqFcNo, spReqHcNo, spReqRem, spReqDepID, spReqLocID, spReqFromDate, spReqToDate, spReqVertical, spReqLabSpace, spSt)
                End If
                SendMail(lblVerReqID.Text.Trim())
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("10") & "&rid=" & clsSecurity.Encrypt(lblVerReqID.Text)
                lblMsg.Text = "Request has been Cancelled"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Cancelling data.", "Dashboard view Vertical Requisition", "Load", ex)
        End Try
        strStatus = 2
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_VER_REQ_MAIL")
        'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
        'sp.Command.AddParameter("@REQID", REQID, DbType.String)
        'sp.Command.AddParameter("@MAILSTATUS", 3, DbType.Int32)
        'sp.ExecuteScalar()
        'sendMail(lblVerReqID.Text.Trim(), dtTemp, ddlSelectLocation.SelectedItem.Text, ddlVertical.SelectedItem.Text, "Cancelled")

        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub
    Private Sub SendMail(ByVal REQ_ID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@strReqID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ_ID
        ObjSubSonic.GetSubSonicExecute("SEND_MAIL_VERTCALREQUISITIONCANCEL", param)
    End Sub
    'Private Sub sendMail(ByVal strReqId As String, ByVal dt As DataTable, ByVal strLoc As String, ByVal strVertical As String, ByVal strStatus As String)
    '    Dim to_mail As String = String.Empty
    '    'Dim cc_mail As String = String.Empty
    '    Dim body As String = String.Empty
    '    to_mail = Session("uemail")
    '    Dim strCC As String = String.Empty
    '    Dim strEmail As String = String.Empty
    '    Dim strRM As String = String.Empty
    '    Dim strKnownas As String = String.Empty
    '    Dim strRR As String = String.Empty
    '    Dim strFMG As String = String.Empty
    '    Dim strBUHead As String = String.Empty
    '    Dim objData As SqlDataReader
    '    'strCC = "select aur_known_as,aur_reporting_to from amantra_user where aur_id= '" & Session("uid") & "'"
    '    'objData = SqlHelper.ExecuteReader(CommandType.Text, strCC)
    '    'While objData.Read
    '    '    strRR = objData("aur_reporting_to")
    '    '    strKnownas = objData("aur_known_as")
    '    'End While
    '    'strEmail = " select aur_email from amantra_user where aur_id='" & strRR & "'"
    '    'objData = SqlHelper.ExecuteReader(CommandType.Text, strEmail)

    '    'While objData.Read
    '    '    strRM = objData("aur_email")
    '    'End While
    '    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '    sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '    objData = sp1.GetReader()
    '    While objData.Read
    '        strRR = objData("aur_reporting_to").ToString
    '        strRM = objData("aur_reporting_email").ToString
    '        strKnownas = objData("aur_known_as").ToString
    '        to_mail = objData("aur_email").ToString
    '        'BCC = objData("BCC").ToString
    '    End While


    '    Dim txtMonth_mail As TextBox
    '    Dim txtYear_mail As TextBox
    '    Dim txtWS_mail As TextBox
    '    Dim txtHC_mail As TextBox
    '    Dim txtFC_mail As TextBox

    '    For j As Integer = 0 To gvEnter.Rows.Count - 1
    '        txtMonth_mail = CType(gvEnter.Rows(j).FindControl("txtMonth"), TextBox)
    '        txtYear_mail = CType(gvEnter.Rows(j).FindControl("txtYear"), TextBox)
    '        txtWS_mail = CType(gvEnter.Rows(j).FindControl("txtWorkstations"), TextBox)
    '        txtHC_mail = CType(gvEnter.Rows(j).FindControl("txtHalfcabinsrequired"), TextBox)
    '        txtFC_mail = CType(gvEnter.Rows(j).FindControl("txtFullCabinsRequired"), TextBox)
    '    Next
    '    body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "

    '    If strStatus = "1" Then
    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam , </td></tr><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Vertical space request has been Updated by  " & strKnownas & " and is pending  for allocation. The details are as follows </td></tr></table>&nbsp;<br /> "
    '    Else
    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam , </td></tr><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Vertical space request has been Canceled by  " & strKnownas & ". The details are as follows </td></tr></table>&nbsp;<br /> "
    '    End If



    '    body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '    body = body & "<table align='center'>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical Requisition Id </td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> " & lblVerReqID.Text.Trim() & " </td></tr> "
    '    body = body & " <tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> " & strLoc & "</tr> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>City</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> " & ddlCity.SelectedItem.Text & "</td></tr> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> " & ddlVertical.SelectedItem.Text & "</td></tr> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtMonth_mail.Text.Trim() & "</td></tr> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtYear_mail.Text.Trim() & "</td></tr> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtWS_mail.Text.Trim() & "</td></tr> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Stations</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtHC_mail.Text.Trim() & "</td></tr> "
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Full Cabins</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtFC_mail.Text.Trim() & "</td></tr>"
    '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtRemarks.Text.Trim() & "</td></tr>"
    '    body = body & "</table>"
    '    body = body & "</td></tr></table>"
    '    body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"

    '    body = body & "<br/><br/><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"

    '    body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "

    '    body = body & "</table>"


    '    Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '    Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '    Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '    Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '    Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '    Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '    Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '    Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '    parms10.Value = "Abcd"
    '    parms11.Value = body
    '    If strStatus = "1" Then
    '        parms12.Value = strRM
    '    Else
    '        parms12.Value = to_mail
    '    End If

    '    parms13.Value = "Vertical Requisition Details"
    '    parms14.Value = getoffsetdatetime(DateTime.Now)
    '    parms15.Value = "Request Submitted"
    '    parms16.Value = "Normal Mail"
    '    If strStatus = "1" Then
    '        parms17.Value = to_mail
    '    Else
    '        parms17.Value = strRM
    '    End If

    '    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)

    '    'End If
    '    'Dim parms18 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
    '    'parms18(0).Value = 3
    '    'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms18)
    '    'If objData.HasRows Then
    '    '    While objData.Read
    '    '        strBUHead = objData("aur_email")

    '    '        Dim parms19 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '    '        Dim parms20 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
    '    '        Dim parms21 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '    '        Dim parms22 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '    '        Dim parms23 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '    '        Dim parms24 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '    '        Dim parms25 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '    '        Dim parms26 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '    '        parms19.Value = "Abcd"
    '    '        parms20.Value = body
    '    '        parms21.Value = strFMG
    '    '        parms22.Value = "Vertical Requisition Details"
    '    '        parms23.Value = getoffsetdatetime(DateTime.Now)
    '    '        parms24.Value = "Request Submitted"
    '    '        parms25.Value = "Normal Mail"
    '    '        parms26.Value = strBUHead

    '    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms19, parms20, parms21, parms22, parms23, parms24, parms25, parms26)


    '    '    End While
    '    'End If
    'End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function
    Private Sub Fillyears()
        For i As Integer = 0 To gvEnter.Rows.Count - 1
            Dim ddlYear As DropDownList = CType(gvEnter.Rows(i).FindControl("ddlYear"), DropDownList)
            For iYear As Integer = getoffsetdatetime(DateTime.Now).Year() To getoffsetdatetime(DateTime.Now).Year + 3
                ddlYear.Items.Add(iYear.ToString())
            Next
        Next
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If Session("View") = "DashBoard" Then
                Session("View") = ""
                strRedirect = "../../WorkSpace/SMS_WebFiles/Dashboard.aspx"
            ElseIf Session("View") = "ViewVertical" Then
                Session("AcrdIndex") = ""
                Session("View") = ""
                strRedirect = "frmViewVerticalRequisition.aspx"
            ElseIf Session("View") = "" Then
                strRedirect = "frmViewVerticalRequisition.aspx"

            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while redirecting", "Dashboard view Vertical Requisition", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub
	 Protected Sub gdavail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdavail.PageIndexChanging
        gdavail.PageIndex = e.NewPageIndex
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SSA_SRNCC_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("ReqID").ToString().Trim()
        ObjSubSonic.BindGridView(gdavail, "GETBLOCKEDSEATS", param)
    End Sub
End Class
