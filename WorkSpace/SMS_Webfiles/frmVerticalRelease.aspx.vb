Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail

Partial Class WorkSpace_SMS_Webfiles_frmVerticalRelease
    Inherits System.Web.UI.Page

    Dim obj As New clsRelease
    Dim ds As DataSet
    Dim dt As New DataTable
    Dim strRedirect As String = String.Empty
    Dim objMaster As New clsMasters()
    Dim objEmp As New clsEmpMapping()
    Dim objExtedSpace As New clsExtenedRelease()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(AppSettings("logout"))
            End If
            lblMsg.Text = ""
            If Not Page.IsPostBack Then
                Try
                    btnSubmit.Visible = False
                    Dim obj1 As New clsRelease
                    Dim sta As Integer = 6
                    objMaster.Bindlocation(ddlLocation)
                    '   ddlTower.Items.Insert(0, "--Select--")
                    objMaster.BindAllocVertical(ddlVertical)
                    ddlFloor.Items.Insert(0, "--Select--")
                    ddlWing.Items.Insert(0, "--Select--")
                    objMaster.BindTower(ddlTower)
                    'btnSubmit.Enabled = False
                Catch ex As Exception
                    Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Release", "Load", ex)

                End Try

            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex > 0 Then
                If ddlLocation.SelectedIndex > 0 Then
                    If ddlVertical.SelectedIndex > 0 Then
                        objMaster.BindAllocFloor(ddlFloor, ddlTower.SelectedValue.Trim(), ddlLocation.SelectedValue.Trim(), ddlVertical.SelectedValue.Trim())
                        gvSpaceExtend.Visible = False
                        btnSubmit.Visible = False
                    Else
                        lblMsg.Text = "Please Select Vertical"
                        gvSpaceExtend.Visible = False
                        btnSubmit.Visible = False
                    End If

                Else
                    lblMsg.Text = "Please Select Location"
                    gvSpaceExtend.Visible = False
                    btnSubmit.Visible = False
                End If

                Else
                    gvSpaceExtend.Visible = False
                    btnSubmit.Visible = False
                    ddlFloor.Items.Clear()
                    ddlFloor.Items.Insert(0, "--Select--")
                ddlWing.Items.Insert(0, "--Select--")
                End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
        End Try

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try

            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chkEmp As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)
                If chkEmp.Checked = True Then
                    intCheckCount += 1
                    Exit For
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atleast one Space to Release"
                Exit Sub
            End If

            dt.Columns.Add("Sno", GetType(Integer))
            dt.Columns.Add("SpaceID", GetType(String))
            dt.Columns.Add("FromDate", GetType(Date))
            dt.Columns.Add("ToDate", GetType(Date))
            Dim drNew As DataRow
            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chkEmp As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)
                Dim lblSpaceReqID As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblSpaceID"), Label)
                Dim txtfromDT As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblFromDT"), Label)
                Dim txtToDT As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblToDT"), Label)
                If chkEmp.Checked = True Then
                    Try
                        drNew = dt.NewRow
                        obj.updateSpace_vertical(gvSpaceExtend.Rows(i).Cells(4).Text, lblSpaceReqID.Text)
                        drNew(0) = dt.Rows.Count + 1
                        drNew(1) = gvSpaceExtend.Rows(i).Cells(4).Text
                        drNew(2) = gvSpaceExtend.Rows(i).Cells(7).Text
                        drNew(3) = gvSpaceExtend.Rows(i).Cells(8).Text
                        dt.Rows.Add(drNew)
                        Session("ReleaseData") = dt
                        'sendMail(String.Empty, dt, ddlVertical.SelectedItem.Text, ddlLocation.SelectedItem.Text, ddlTower.SelectedItem.Text, ddlFloor.SelectedItem.Text, ddlWing.SelectedItem.Text)
                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_MAIL")
                        'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                        'sp.Command.AddParameter("@REQID", REQID, DbType.String)
                        'sp.Command.AddParameter("@MAILSTATUS", 1, DbType.Int32)
                        'sp.ExecuteScalar()
                    Catch ex As Exception
                        Throw New Amantra.Exception.DataException("This error has been occured while Releasing Space.", "Space Allocation", "Load", ex)
                        lblMsg.Text = "Unable to Release"
                        Exit Sub
                    End Try
                End If
            Next
            If dt.Rows.Count > 0 Then
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("4") & ""
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
        If (strRedirect <> String.Empty) Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    'Private Sub sendMail(ByVal strReqId As String, ByVal dt As DataTable, ByVal strVertical As String, ByVal strLocation As String, ByVal strTower As String, ByVal strFloor As String, ByVal strWing As String)
    '    Try
    '        Dim to_mail As String = Session("uemail")
    '        Dim cc_mail As String = Session("uemail")
    '        Dim body As String = String.Empty
    '        Dim strCC As String = String.Empty
    '        Dim strKnownas As String = String.Empty
    '        Dim strEmail As String = String.Empty
    '        Dim strRM As String = String.Empty
    '        Dim strRR As String = String.Empty
    '        Dim strFMG As String = String.Empty
    '        Dim strBUHead As String = String.Empty
    '        Dim objData As SqlDataReader
    '        dt = CType(Session("ReleaseData"), DataTable)
    '        Dim j As Integer = 5
    '        Dim to_email As String = ""
    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '        sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '        objData = sp1.GetReader()
    '        While objData.Read
    '            strRR = objData("aur_reporting_to").ToString
    '            strRM = objData("aur_reporting_email").ToString
    '            strKnownas = objData("aur_known_as").ToString
    '            to_mail = objData("aur_email").ToString
    '            'BCC = objData("BCC").ToString
    '        End While

    '        'strCC = "select aur_known_as,aur_reporting_to from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strCC)
    '        'While objData.Read
    '        '    strRR = objData("aur_reporting_to")
    '        '    strKnownas = objData("aur_known_as")
    '        'End While

    '        body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "

    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b> , </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Vertical spaces have been released by " & strKnownas & "</td></tr></table>&nbsp;<br />"
    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '        body = body & "<table align='center'>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> " & strLocation & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strVertical & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'> Tower Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Floor Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strFloor & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Wing Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strWing & "</td></tr>"
    '        'For i As Integer = 0 To dt.Rows.Count - 1
    '        '    body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space ID</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows(i)(1).ToString() & "<br /></td></tr>"
    '        'Next
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>No.Of WS, HC, FC Released</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows.Count & "</td></tr> "
    '        body = body & "</table>"
    '        body = body & "</td></tr></table>"
    '        body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"

    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"

    '        body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "

    '        body = body & "</table>"
    '        'If to_mail = String.Empty Then
    '        '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If
    '        'Dim mail As New MailMessage
    '        'mail.From = New MailAddress(ConfigurationManager.AppSettings("AmantraEmailId").ToString)
    '        'mail.To.Add(to_mail)
    '        'mail.Subject = "Vertical Release Details"
    '        'mail.IsBodyHtml = True
    '        'mail.Body = body
    '        'Dim strVRM As String = String.Empty

    '        'Dim strsql As String = String.Empty
    '        'Dim stVrm As String = String.Empty
    '        'Dim stVrm1 As String = String.Empty
    '        'Dim dr As SqlDataReader
    '        'Dim dr1 As SqlDataReader
    '        'Dim iQry As Integer = 0

    '        'strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
    '        'iQry = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL1))
    '        'If iQry > 0 Then
    '        '    strSQL1 = "Select VER_VRM from VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
    '        '    dr1 = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
    '        '    While dr1.Read
    '        '        stVrm = dr1("VER_VRM").ToString()
    '        '    End While
    '        '    strsql = "Select aur_email from amantra_user where aur_id='" & stVrm & "'"
    '        '    dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
    '        '    While dr.Read
    '        '        stVrm1 = dr("aur_email").ToString()
    '        '    End While
    '        'Else
    '        '    stVrm1 = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If

    '        'Dim parms1 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
    '        'parms1(0).Value = 1
    '        'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms1)
    '        'If objData.HasRows Then
    '        '    While objData.Read
    '        '        strFMG = objData("aur_email")
    '        '        If (strVRM = String.Empty) Then

    '        '            strVRM = strFMG
    '        '        Else
    '        '            strVRM = strVRM + "," + strFMG
    '        '        End If
    '        '    End While
    '        Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        Dim parms2 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
    '        Dim parms3 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        Dim parms4 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        Dim parms5 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        Dim parms6 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        Dim parms7 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        Dim parms8 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)


    '        parms.Value = "Abcd"
    '        parms2.Value = body
    '        parms3.Value = to_email
    '        parms4.Value = "Vertical Release Details"
    '        parms5.Value = getoffsetdatetime(DateTime.Now)
    '        parms6.Value = "Request Submitted"
    '        parms7.Value = "Normal Mail"
    '        parms8.Value = strRM
    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms2, parms3, parms4, parms5, parms6, parms7, parms8)

    '        'End If
    '        'Dim parms9 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
    '        'parms9(0).Value = 3
    '        'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms9)
    '        'If objData.HasRows Then
    '        '    While objData.Read
    '        '        strBUHead = objData("aur_email")
    '        '        Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        '        Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
    '        '        Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        '        Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        '        Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        '        Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        '        Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        '        Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)


    '        '        parms10.Value = "Abcd"
    '        '        parms11.Value = body
    '        '        parms12.Value = to_mail
    '        '        parms13.Value = "Vertical Release Details"
    '        '        parms14.Value = getoffsetdatetime(DateTime.Now)
    '        '        parms15.Value = "Request Submitted"
    '        '        parms16.Value = "Normal Mail"
    '        '        parms17.Value = strBUHead
    '        '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)

    '        '    End While
    '        'End If
    '    Catch ex As Exception
    '        ' Throw New Amantra.Exception.DataException("This error has been occured while Triggering the mail", "Space Vertical Requisition", "Load", ex)
    '        Throw (ex)
    '    End Try

    'End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlLocation.SelectedIndex > 0 Then
                If ddlVertical.SelectedIndex > 0 Then
                    objMaster.BindAllocTower(ddlTower, ddlLocation.SelectedValue.Trim(), ddlVertical.SelectedValue.Trim())
                    ddlFloor.Items.Clear()
                    ddlFloor.Items.Insert(0, "--Select--")
                    gvSpaceExtend.Visible = False
                    btnSubmit.Visible = False
                Else
                    lblMsg.Text = "Please Select Vertical"
                    gvSpaceExtend.Visible = False
                    btnSubmit.Visible = False
                End If

            Else
                gvSpaceExtend.Visible = False
                btnSubmit.Visible = False
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, "--Select--")
                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, "--Select--")
                ddlWing.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
        End Try

    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            If ddlFloor.SelectedIndex > 0 Then
                If ddlTower.SelectedIndex > 0 Then
                    If ddlVertical.SelectedIndex > 0 Then
                        objExtedSpace.bindData1(ddlVertical.SelectedValue, ddlTower.SelectedValue, ddlFloor.SelectedValue, gvSpaceExtend, 7, ddlWing.SelectedValue)
                        If gvSpaceExtend.Rows.Count > 0 Then
                            gvSpaceExtend.Visible = True
                            btnSubmit.Visible = True
                        Else
                            btnSubmit.Visible = False
                            gvSpaceExtend.Visible = False
                            lblMsg.Text = "No Spaces to Release in this Tower"
                        End If
                    Else
                        lblMsg.Text = "Please Select Vertical"
                        gvSpaceExtend.Visible = False
                        btnSubmit.Visible = False
                    End If

                Else
                    lblMsg.Text = "Please Select Tower"
                    gvSpaceExtend.Visible = False
                    btnSubmit.Visible = False
                End If

            Else
                lblMsg.Text = "Please Select Floor"
                gvSpaceExtend.Visible = False
                btnSubmit.Visible = False
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
        End Try


    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        If (ddlVertical.SelectedIndex > 0) Then
            Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)

            sp1.Value = ddlVertical.SelectedValue

            ddlLocation.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getAllocatedLocs", sp1)
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, "--Select--")
            ddlTower.Items.Clear()
            ddlFloor.Items.Clear()
            ddlTower.Items.Insert(0, "--Select--")
            ddlFloor.Items.Insert(0, "--Select--")
            ddlWing.Items.Insert(0, "--Select--")
            gvSpaceExtend.Visible = False
            btnSubmit.Visible = False

        Else
            gvSpaceExtend.Visible = False
            btnSubmit.Visible = False
            ddlTower.Items.Clear()
            ddlLocation.Items.Clear()
            ddlFloor.Items.Clear()
            ddlTower.Items.Insert(0, "--Select--")
            ddlLocation.Items.Insert(0, "--Select--")
            ddlFloor.Items.Insert(0, "--Select--")
            ddlWing.Items.Insert(0, "--Select--")
        End If


    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddlTower.SelectedIndex > 0 Then
                If ddlLocation.SelectedIndex > 0 Then
                    If ddlVertical.SelectedIndex > 0 Then
                        If ddlFloor.SelectedIndex > 0 Then
                            objMaster.BindAllocWing(ddlWing, ddlTower.SelectedValue.Trim(), ddlLocation.SelectedValue.Trim(), ddlVertical.SelectedValue.Trim(), ddlFloor.SelectedValue.Trim())
                            gvSpaceExtend.Visible = False
                            btnSubmit.Visible = False
                        Else
                            lblMsg.Text = "Please Select Floor"
                            gvSpaceExtend.Visible = False
                            btnSubmit.Visible = False
                        End If
                    Else

                        lblMsg.Text = "Please Select Vertical"
                        gvSpaceExtend.Visible = False
                        btnSubmit.Visible = False
                    End If
                Else
                    lblMsg.Text = "Please Select Location"
                    gvSpaceExtend.Visible = False
                    btnSubmit.Visible = False

                End If
            Else
                gvSpaceExtend.Visible = False
                btnSubmit.Visible = False
                ddlWing.Items.Clear()
                ddlWing.Items.Insert(0, "--Select--")

            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
        End Try
    End Sub

    Protected Sub gvSpaceExtend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpaceExtend.PageIndexChanging
        gvSpaceExtend.PageIndex = e.NewPageIndex
        objExtedSpace.bindData1(ddlVertical.SelectedValue, ddlTower.SelectedValue, ddlFloor.SelectedValue, gvSpaceExtend, 7, ddlWing.SelectedValue)
    End Sub
End Class
