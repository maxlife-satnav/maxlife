
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports Google.Apis.Calendar.v3.Data


Partial Class WorkSpace_SMS_Webfiles_conference_request
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty
    Dim Conf_REQ_ID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        lblmsg.Text = " "
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If

        If Not Page.IsPostBack Then
            If Not Request.QueryString("FROM_DATE") Is Nothing Then
                txtFrmDate.Text = Request.QueryString("FROM_DATE")
                LoadCity()
                BindMail()
                ddlCity.ClearSelection()
                ddlCity.Items.FindByValue(Request.QueryString("CTY_CODE")).Selected = True
                Bindlocation(ddlCity.SelectedItem.Value)
                ddlSelectLocation.ClearSelection()
                ddlSelectLocation.Items.FindByValue(Request.QueryString("LCM_CODE")).Selected = True
                BindTower(ddlSelectLocation.SelectedItem.Value)
                ddlTower.ClearSelection()
                ddlTower.Items.FindByValue(Request.QueryString("TWR_CODE")).Selected = True
                BindFloor(ddlTower.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value.ToString().Trim())
                ddlFloor.ClearSelection()
                ddlFloor.Items.FindByValue(Request.QueryString("FLR_CODE")).Selected = True
                BindCapacity()
                ddlCapacity.ClearSelection()
                ddlCapacity.Items.FindByValue(Request.QueryString("CAPACITY")).Selected = True
                BindConference()
                ddlConf.ClearSelection()
                ddlConf.Items.FindByValue(Request.QueryString("CONF_CODE")).Selected = True

                ddlCity.Enabled = False
                ddlSelectLocation.Enabled = False
                ddlTower.Enabled = False
                ddlFloor.Enabled = False
                ddlCapacity.Enabled = False
                ddlConf.Enabled = False
                txtFrmDate.Enabled = False
                starttimehr.Enabled = False
                endtimehr.Enabled = False

                If Not Request.QueryString("FROM_TIME") Is Nothing Then
                    starttimehr.SelectedValue = (CInt(Request.QueryString("FROM_TIME"))).ToString("d2")
                    If starttimehr.SelectedValue = 23 Then
                        endtimehr.SelectedValue = "00"
                    Else
                        endtimehr.SelectedValue = (CInt(Request.QueryString("FROM_TIME")) + 1).ToString("d2")
                    End If
                Else
                    starttimehr.Enabled = True
                    endtimehr.Enabled = True
                End If
            Else
                LoadCity()
            End If
        End If
    End Sub

    Private Sub BindMail()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ALL_MAIL_IDS")
        lstInternal.DataSource = sp.GetDataSet()
        lstInternal.DataTextField = "AUR_ID"
        lstInternal.DataValueField = "AUR_EMAIL"
        lstInternal.DataBind()
    End Sub

    Public Sub LoadCity()
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub

    Public Sub Bindlocation(ByVal strcity As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = strcity
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindTower(ByVal strLoc As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
        param(0).Value = strLoc
        ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)

    End Sub

    Private Sub BindFloor(ByVal tower As String, ByVal loc As String)
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforFloors(tower, loc.Trim())
        ddlFloor.DataSource = verDTO
        ddlFloor.DataTextField = "Floorname"
        ddlFloor.DataValueField = "Floorcode"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "-- Select --")
    End Sub

    Private Sub BindCapacity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)

        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

    End Sub

    Public Sub BindConference()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONFERENCE_BOOKED_SLOTS")
        sp.Command.AddParameter("@REQ_ID", "", DbType.String)
        sp.Command.AddParameter("@SPC_ID", ddlConf.SelectedValue, DbType.String)
        sp.Command.AddParameter("@FROM_DATE", txtFrmDate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtFrmDate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", starttimehr.SelectedValue, DbType.String)
        sp.Command.AddParameter("@TO_TIME", endtimehr.SelectedValue, DbType.String)

        'sp.Command.AddParameter("@S", 1, DbType.Int16)
        'sp.Command.AddParameter("@E", 10, DbType.Int16)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()

        If (ds.Tables(0).Rows.Count > 0) Then
            gvItem.Visible = True
            lblconfalert.Visible = True
            lblconfalert.Text = "Note: selected time interval is already booked, please select other available slots."
            Exit Sub
        Else

            If (txtFrmDate.Text >= getoffsetdate(Date.Today)) Then
                If starttimehr.SelectedValue < endtimehr.SelectedValue Or (starttimehr.SelectedValue > endtimehr.SelectedValue And starttimehr.SelectedValue = "23") Then

                    Dim cntWst As Integer = 0
                    Dim cntHCB As Integer = 0
                    Dim cntFCB As Integer = 0
                    Dim twr As String = ""
                    Dim flr As String = ""
                    Dim wng As String = ""
                    Dim intCount As Int16 = 0
                    Dim ftime As String = ""
                    Dim ttime As String = ""
                    Dim StartHr As String = "00"
                    Dim EndHr As String = "00"
                    Dim StartMM As String = "00"
                    Dim EndMM As String = "00"

                    If starttimehr.SelectedItem.Value <> "Hr" Then
                        StartHr = starttimehr.SelectedItem.Text
                    End If

                    If endtimehr.SelectedItem.Value <> "Hr" Then
                        If endtimehr.SelectedItem.Value = "24" Then
                            EndHr = "00"
                        Else
                            EndHr = endtimehr.SelectedItem.Text
                        End If

                    End If

                    If StartHr = "23" And EndHr <> "00" Then
                        lblmsg.Text = "To date Should not be more than 00 hrs"
                        Exit Sub
                    End If

                    Dim InternalCount As Integer = 0
                    Dim InternalAttendees As String = ""
                    For Each li As ListItem In lstInternal.Items
                        If li.Selected = True Then
                            InternalCount = 1
                        End If
                    Next

                    If txtAttendees.Text = "" And InternalCount = 0 Then
                        lblmsg.Text = "Please enter or select at least one Attendees Email"
                        Exit Sub
                    End If

                    ftime = StartHr + ":" + StartMM
                    ttime = EndHr + ":" + EndMM

                    Dim param(7) As SqlParameter
                    param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
                    param(0).Value = ddlSelectLocation.SelectedItem.Value
                    param(1) = New SqlParameter("@TOWER", SqlDbType.NVarChar, 200)
                    param(1).Value = ddlTower.SelectedItem.Value
                    param(2) = New SqlParameter("@FLOOR", SqlDbType.NVarChar, 200)
                    param(2).Value = ddlFloor.SelectedItem.Value
                    param(3) = New SqlParameter("@FROM_TIME", SqlDbType.DateTime)
                    param(3).Value = ftime
                    param(4) = New SqlParameter("@TO_TIME", SqlDbType.DateTime)
                    param(4).Value = ttime
                    param(5) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
                    param(5).Value = Convert.ToDateTime(txtFrmDate.Text)
                    param(6) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
                    param(6).Value = Convert.ToDateTime(txtFrmDate.Text)
                    param(7) = New SqlParameter("@CONF_CODE", SqlDbType.DateTime)
                    param(7).Value = ddlConf.SelectedValue

                    'ObjSubSonic.Binddropdown(ddlConference, "usp_get_CONFERENCE_Seats_PART1", "spc_name", "spc_id", param)
                    'confroom.Visible = True

                    'If Request.QueryString("mode") = 2 Then
                    '    emp.Visible = True
                    'Else
                    '    emp.Visible = False
                    'End If
                    If CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtFrmDate.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
                        lblmsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                        Exit Sub
                    End If

                    'added by praveen on 13/02/2014
                    Dim flag As Integer
                    flag = ObjSubSonic.GetSubSonicExecuteScalar("usp_get_CONFERENCE_Seats_PART1", param)
                    If flag = 1 Then 'added a new flag case.
                        SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 2)
                        lblmsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                        cleardata()
                    Else
                        PopUpMessage("Already conference is alloted in this interval of time.Please select another interval ", Me)
                        Exit Sub
                    End If



                    'Else
                    '    lblMsg.Text = "Please enter employee code."
                    'End If
                Else
                    lblmsg.Text = "Start Time Must Be Less Than End Time"
                End If
            Else
                lblmsg.Text = "Date Must Be Greater than Today's date"
            End If
        End If

    End Sub


    Private Sub cleardata()
        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        'ddlConference.Items.Clear()
        ddlCapacity.Items.Clear()
        ddlConf.Items.Clear()
        txtFrmDate.Text = String.Empty
        starttimehr.ClearSelection()
        endtimehr.ClearSelection()
        btnsubmit.Visible = False
        ' gvDaily.Visible = False
        'gvassets.Visible = False
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)

        REQID = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "SVR_ID", Session("TENANT") & ".", "SMS_VERTICAL_REQUISITION")
        Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        'If starttimemin.SelectedItem.Value <> "Min" Then
        '    StartMM = starttimemin.SelectedItem.Text
        'End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If

        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM

        Dim sta As Integer = status

        RIDDS = RIDGENARATION("VerticalReq")

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubSonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblmsg.Text = "Request is already raised "
            Exit Sub
        Else
            cntWst = 1

            Dim param2(15) As SqlParameter
            param2(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param2(0).Value = REQID
            param2(1) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param2(1).Value = strVerticalCode
            param2(2) = New SqlParameter("@WSTREQCOUNT", SqlDbType.Int)
            param2(2).Value = cntWst
            param2(3) = New SqlParameter("@FCREQCOUNT", SqlDbType.Int)
            param2(3).Value = cntFCB
            param2(4) = New SqlParameter("@HCREQCOUNT", SqlDbType.Int)
            param2(4).Value = cntHCB
            param2(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param2(5).Value = txtFrmDate.Text
            param2(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param2(6).Value = txtFrmDate.Text
            param2(7) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param2(7).Value = ftime
            param2(8) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param2(8).Value = ttime
            param2(9) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
            param2(9).Value = strAurId
            param2(10) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 50)
            param2(10).Value = ddlSelectLocation.SelectedItem.Value 'GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
            param2(11) = New SqlParameter("@CITY", SqlDbType.NVarChar, 50)
            param2(11).Value = ddlCity.SelectedValue
            param2(12) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param2(12).Value = "2" 'GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param2(13) = New SqlParameter("@BCPTYPE", SqlDbType.NVarChar, 50)
            param2(13).Value = "2"
            param2(14) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param2(14).Value = sta
            param2(15) = New SqlParameter("@REMARKS", SqlDbType.NVarChar, 200)
            param2(15).Value = remarks
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART1", param2)
            Dim cnt As Int32 = 0
            ' For Each row As GridViewRow In gdavail.Rows
            Dim lblspcid As String   ' DirectCast(row.FindControl("lblspcid"), Label)
            lblspcid = ddlConf.SelectedItem.Value
            'Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblmsg.Text = ""
            ' If chkSelect.Checked = True Then
            verticalreqid = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "ssa_id", Session("TENANT") & ".", "SMS_space_allocation")
            Conf_REQ_ID = verticalreqid
            Dim param3(10) As SqlParameter

            param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param3(0).Value = verticalreqid
            param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
            param3(1).Value = REQID
            param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param3(2).Value = strVerticalCode
            param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
            param3(3).Value = lblspcid
            param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
            param3(4).Value = strAurId
            param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param3(5).Value = txtFrmDate.Text
            param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param3(6).Value = txtFrmDate.Text
            param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param3(7).Value = "2" ' GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param3(8).Value = ftime
            param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param3(9).Value = ttime
            param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param3(10).Value = sta
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART3", param3)
            'Addusers.Occupiedspace(Trim(strAurId), LTrim(RTrim(lblspcid)))
            ' UpdateRecord(LTrim(RTrim(lblspcid)), sta, Trim(strAurId) & "/" & lblDepartment.Text)
            'End If
            'Next
            '-------------------SEND MAIL-----------------

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_CONF_BOOKING_REQUEST")
            sp.Command.AddParameter("@REQID", verticalreqid, DbType.String)
            sp.ExecuteScalar()

        End If

        strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("2") & "&rid=" & clsSecurity.Encrypt(verticalreqid)
GVColor:

        Try
            BookEventOnGoogleCalendar()
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try

        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect, False)
        End If


    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        'GET_SPACE_CONFERENCE
        If ddlFloor.SelectedIndex <> 0 Then
            BindConference()
        End If

    End Sub

    'Private Sub newEvent()
    '    Dim Summary As String = "Google I/O 2015"
    '    Dim Location As String = "Hyderabad"
    'End Sub

    Private Function BookEventOnGoogleCalendar() As Boolean
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If

        If EndHr = "00" And StartHr = "23" Then
            EndHr = "23:59"
        End If

        ftime = txtFrmDate.Text + " " + StartHr + ":" + StartMM
        ttime = txtFrmDate.Text + " " + EndHr + ":" + EndMM

        Dim calendarservice As New clsCalendarService
        With calendarservice
            .Summary = txtDescription.Text
            .Location = ddlSelectLocation.SelectedItem.Text + "," + ddlTower.SelectedItem.Text + "," + ddlFloor.SelectedItem.Text + "," + ddlConf.SelectedItem.Text
            .Description = txtDescription.Text
            .Start = New EventDateTime
            With calendarservice.Start
                .DateTime = ftime
                '.TimeZone = "Asia/Kolkata"
                .TimeZone = Session("useroffset")
            End With

            .End = New EventDateTime
            With .End
                .DateTime = ttime
                .TimeZone = Session("useroffset")
            End With

            Dim ndx As Integer = 0

            Dim attendees() = txtAttendees.Text.Split(",")
            .Attendees = New EventAttendee(attendees.Length - 1) {}
            For i = 0 To attendees.Length - 1
                .Attendees(ndx) = New EventAttendee
                With .Attendees(ndx)
                    .Email = (attendees(ndx)).Trim()
                End With
                ndx = ndx + 1
            Next
            For i = 0 To lstInternal.Items.Count - 1
                If lstInternal.Items(i).Selected Then
                    ReDim Preserve .Attendees(ndx + 1)
                    .Attendees(ndx) = New EventAttendee
                    With .Attendees(ndx)
                        .Email = lstInternal.Items(i).Value
                    End With
                    ndx = ndx + 1
                End If
            Next
            .Recurrence = New String() {"RRULE:FREQ=DAILY;COUNT=1"}
            .Reminders = New Google.Apis.Calendar.v3.Data.Event.RemindersData()
            With .Reminders
                .UseDefault = False
                .Overrides = New EventReminder() {
                    New EventReminder() With {.Method = "email", .Minutes = 24 * 60},
                    New EventReminder() With {.Method = "popup", .Minutes = 10}
                }
            End With
        End With

        Dim Event_ID As String = calendarservice.PushEvent()
        Dim InternalAttendees As String = ""
        For Each li As ListItem In lstInternal.Items
            If li.Selected = True Then
                InternalAttendees = InternalAttendees + li.Value + ","
            End If
        Next

        InternalAttendees = InternalAttendees.TrimEnd(",")

        Dim param_Event(6) As SqlParameter
        param_Event(0) = New SqlParameter("@CONF_REQ_ID", SqlDbType.NVarChar, 1000)
        param_Event(0).Value = Conf_REQ_ID
        param_Event(1) = New SqlParameter("@CONF_EVENT_ID", SqlDbType.NVarChar, 1000)
        param_Event(1).Value = Event_ID
        param_Event(2) = New SqlParameter("@CONF_EVENT_STATUS", SqlDbType.Int)
        param_Event(2).Value = 1
        param_Event(3) = New SqlParameter("@CONF_DESCRIPTION", SqlDbType.NVarChar, 1000)
        param_Event(3).Value = txtDescription.Text
        param_Event(4) = New SqlParameter("@CREATED_BY", SqlDbType.NVarChar, 100)
        param_Event(4).Value = Session("UID")
        param_Event(5) = New SqlParameter("@EXTERNAL_ATTENDEES", txtAttendees.Text)
        'param_Event(5).Value = txtAttendees.Text
        param_Event(6) = New SqlParameter("@INTERNAL_ATTENDEES", InternalAttendees)
        ObjSubSonic.GetSubSonicExecute("CONF_GOOGLE_NEW_EVENT_CREATE", param_Event)

    End Function
End Class
