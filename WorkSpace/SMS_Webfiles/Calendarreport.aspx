﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Calendarreport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Calendarreport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>
   
     <script language="javascript" type="text/javascript">
         

         function CheckDate() {
             var dtFrom = document.getElementById("txtFromdate").Value;
             var dtTo = document.getElementById("txtTodate").Value;
             if (dtFrom < dtTo) {
                 alert("Invalid Dates");
             }
         }
    </script>

    <div>
        <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">
             <hr align="center" width="60%" />Calendar Report</asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Calendar Report </strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val2" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />

                        <table id="tab" runat="server" cellpadding="1" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">Select From Date <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtfromdate"
                                        ErrorMessage="Please select From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtfromdate" runat="server" CssClass="clsTextField" Width="65%"></asp:TextBox>
                                     <%-- <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfromdate"
                                        Format="dd-MMM-yyyy">
                                    </cc1:CalendarExtender>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">Select To Date <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttodate"
                                        ErrorMessage="Please select To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txttodate" runat="server" CssClass="clsTextField" Width="65%"></asp:TextBox>
                                     <%--   <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txttodate"
                                        Format="dd-MMM-yyyy">
                                    </cc1:CalendarExtender>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height:26px;width:50%">
                                    Select <%= Session("Parent")%> <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlvertical" 
                                        ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                 <td align="left" style="height:26px;width:50%">
                                   <asp:DropDownList ID="ddlvertical" runat="server" CssClass="clsComboBox" Width="97%"></asp:DropDownList>
                                </td>

                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="button" CausesValidation="true" />

                                </td>
                               

                            </tr>


                            <tr>
                                <td align="center" colspan="2">
                                  
                                 <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="819px">
                                     <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                                     <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                     <OtherMonthDayStyle ForeColor="#999999" />
                                     <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                     <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                     <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                     <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                     <WeekendDayStyle BackColor="#CCCCFF" />
                                    </asp:Calendar>
                                </td>

                            </tr>


                        </table>

                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>




</asp:Content>

