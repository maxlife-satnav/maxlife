﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_conferenceRelease
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            btnsubmit.Visible = False
            emp.Visible = False
            gvassets.Visible = False
            LoadCity()
            lblMsg.Text = ""
        End If
    End Sub
    Public Sub LoadCity()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectLocation.SelectedIndexChanged

        Try
            'usp_getActiveTower_LOC

            If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
                ' objMsater.BindTowerLoc(ddlTower, ddlSelectLocation.SelectedItem.Value)
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
                param(0).Value = ddlSelectLocation.SelectedItem.Value
                ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)


            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try

            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
        param(0).Value = ddlSelectLocation.SelectedItem.Value
        param(1) = New SqlParameter("@TWR", SqlDbType.NVarChar, 200)
        param(1).Value = ddlTower.SelectedItem.Value
        param(2) = New SqlParameter("@FLR", SqlDbType.NVarChar, 200)
        param(2).Value = ddlFloor.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlConference, "GET_CONFERENCE_SEATS", "spc_name", "spc_id", param)


    End Sub

    Protected Sub btncheck_Click(sender As Object, e As EventArgs) Handles btncheck.Click

        If ddlConference.SelectedIndex > 0 Then
            BindassetGrid()

        End If
        gvassets.Visible = True

        If gvassets.Rows.Count > 0 Then
            btnsubmit.Visible = True
        Else
            btnsubmit.Visible = False
        End If

    End Sub

    Private Sub BindassetGrid()
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
        param(0).Value = ddlSelectLocation.SelectedItem.Value
        param(1) = New SqlParameter("@TWR", SqlDbType.NVarChar, 200)
        param(1).Value = ddlTower.SelectedItem.Value
        param(2) = New SqlParameter("@FLR", SqlDbType.NVarChar, 200)
        param(2).Value = ddlFloor.SelectedItem.Value
        param(3) = New SqlParameter("@CONF_ID", SqlDbType.NVarChar, 200)
        param(3).Value = ddlConference.SelectedItem.Value

        ObjSubSonic.BindGridView(gvassets, "GETCONFERENCE_ASSETS_APPROVED", param)

    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click

        Dim count1 As Integer = 0




        If gvassets.Rows.Count > 0 Then

            For Each row As GridViewRow In gvassets.Rows
                Dim lblconfcode As Label = DirectCast(row.FindControl("lblconfcode"), Label)
                Dim lblassetcode As Label = DirectCast(row.FindControl("lblassetcode"), Label)
                Dim lblconfdate As Label = DirectCast(row.FindControl("lblconfdate"), Label)
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then

                    Dim param4(2) As SqlParameter

                    param4(0) = New SqlParameter("@CONF_ID", SqlDbType.NVarChar, 200)
                    param4(0).Value = lblconfcode.Text
                    param4(1) = New SqlParameter("@ASSET_ID", SqlDbType.NVarChar, 200)
                    param4(1).Value = lblassetcode.Text
                    param4(2) = New SqlParameter("@CONF_DATE", SqlDbType.DateTime)
                    param4(2).Value = lblconfdate.Text
                    ObjSubSonic.GetSubSonicExecute("RELEASE_ASSETS", param4)
                End If

            Next


        End If

        lblMsg.Text = "Assets Released Succesfully"
        ' End If

    End Sub
End Class
