Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_GIS_AllocateSeatsByVertical
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers


    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    'Meta tags
    '    '************************
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/tabcontent.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/tabcontent.css")))
    '    'Meta tags
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))

    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))

    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    '    txtfromdate.Attributes.Add("onClick", "displayDatePicker('" + txtfromdate.ClientID + "')")
    '    txtfromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    '    txttodate.Attributes.Add("onClick", "displayDatePicker('" + txttodate.ClientID + "')")
    '    txttodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    '    '************************
    'End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindSeatType()
                BindShifts()
                BindVerticals()
            End If
        End If
    End Sub

    Private Sub BindSeatType()
        ObjSubSonic.Binddropdown(ddlSeatType, "getSeatType", "SPACE_TYPE", "SNO")
    End Sub

    Private Sub BindShifts()
        ObjSubSonic.Binddropdown(ddlShifts, "getshifts", "shift", "id")
    End Sub

    Private Sub BindVerticals()
        ObjSubSonic.Binddropdown(ddlVerticals, "get_costcenter_vertical", "Ver_Name", "Ver_Code")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If ddlSeatType.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Please select seat type."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If
        If ddlShifts.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Please select shifts."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If
        If ddlVerticals.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Please select Vertical."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If

        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@SEATTYPE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlSeatType.SelectedItem.Value
        param(1) = New SqlParameter("@SHIFT", SqlDbType.NVarChar, 200)
        param(1).Value = ddlSeatType.SelectedItem.Value
        param(2) = New SqlParameter("@EDATE", SqlDbType.NVarChar, 200)
        param(2).Value = txttodate.Text
        param(3) = New SqlParameter("@SDATE", SqlDbType.NVarChar, 200)
        param(3).Value = txtfromdate.Text
        param(4) = New SqlParameter("@NOOFSEATS", SqlDbType.NVarChar, 200)
        param(4).Value = txtNumberofSeats.Text
        param(5) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
        param(5).Value = ddlVerticals.SelectedItem.Value
        ObjSubSonic.BindGridView(gvitems, "GET_AVLBL_SEATS", param)
        If gvitems.Rows.Count > 0 Then
            btnAllocate.Visible = True
        Else
            btnAllocate.Visible = False
        End If
    End Sub

    Protected Sub btnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllocate.Click
        'Dim REQID As String = ""
        'REQID = ObjSubSonic.REQGENARATION_REQ(ddlVerticals.SelectedItem.Value.Trim(), "SVR_ID", Session("TENANT") & "." , "SMS_VERTICAL_REQUISITION")

        'Dim param2(15) As SqlParameter
        'param2(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
        'param2(0).Value = REQID
        'param2(1) = New SqlParameter("@VC_REQ_DS", SqlDbType.NVarChar, 50)
        'param2(1).Value = REQID
        'param2(2) = New SqlParameter("@VC_REQ_DATE", SqlDbType.NVarChar, 100)
        'param2(2).Value = txtfromdate.Text
        'param2(3) = New SqlParameter("@VC_STA_ID", SqlDbType.NVarChar, 50)
        'param2(3).Value = 5
        'param2(4) = New SqlParameter("@VC_REQ_BY", SqlDbType.NVarChar, 50)
        'param2(4).Value = Session("uid").ToString().Trim()
        'param2(5) = New SqlParameter("@VC_WST_REQ", SqlDbType.NVarChar, 50)
        'param2(5).Value = gvitems.Rows.Count
        'param2(6) = New SqlParameter("@VC_CAB_REQ", SqlDbType.NVarChar, 50)
        'param2(6).Value = 0
        'param2(7) = New SqlParameter("@VC_CUB_REQ", SqlDbType.NVarChar, 50)
        'param2(7).Value = 0
        'param2(8) = New SqlParameter("@VC_REM", SqlDbType.NVarChar, 50)
        'param2(8).Value = ""
        'param2(9) = New SqlParameter("@VC_DEP_ID", SqlDbType.NVarChar, 50)
        'param2(9).Value = ddlVerticals.SelectedItem.Value
        'param2(10) = New SqlParameter("@VC_LOC_ID", SqlDbType.NVarChar, 50)
        'param2(10).Value = "PSN"
        'param2(11) = New SqlParameter("@VC_FROM_DATE", SqlDbType.NVarChar, 50)
        'param2(11).Value = txtfromdate.Text
        'param2(12) = New SqlParameter("@VC_TO_DATE", SqlDbType.NVarChar, 50)
        'param2(12).Value = txttodate.Text
        'param2(13) = New SqlParameter("@VC_VERTICAL", SqlDbType.NVarChar, 50)
        'param2(13).Value = ddlVerticals.SelectedValue
        'param2(14) = New SqlParameter("@VC_LABSPACE", SqlDbType.NVarChar, 50)
        'param2(14).Value = "0"
        'param2(15) = New SqlParameter("@VC_CTY_ID", SqlDbType.NVarChar, 50)
        'param2(15).Value = "BLR"
        'ObjSubSonic.GetSubSonicExecute("USP_SPACE_INSERT_VERTICALREQUISITION", param2)
    End Sub
End Class
