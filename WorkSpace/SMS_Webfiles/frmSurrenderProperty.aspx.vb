Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmSurrenderProperty
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session("CurrentPageIndex") = 0
            BindGrid()
            panel1.Visible = False
            txtSdate.Text = getoffsetdate(Date.Today)
            
            BindCity()
        End If
        txtSdate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindCity()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlcity.DataSource = sp.GetDataSet()
            ddlcity.DataTextField = "CTY_NAME"
            ddlcity.DataValueField = "CTY_CODE"
            ddlcity.DataBind()
            ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETPROPTEN")
            sp.Command.AddParameter("@user", Session("uid"), DbType.String)
            gvitems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            gvItems.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
            gvItems.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        Session("CurrentPageIndex") = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        Try
            If e.CommandName = "Surrender" Then
                panel1.Visible = True

                Dim currentpageindex As Integer = 0
                ddlcity.SelectedIndex = 0
                If Session("CurrentPageIndex") <> Nothing Then
                    currentpageindex = (Integer.Parse(Session("CurrentPageIndex")))
                End If
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString()) - (currentpageindex * 5)
                Dim lblid As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblsno"), Label)
                Dim lblPropertyCode As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblpropcode"), Label)
                hfsno.Value = lblid.Text
                txtpropertycode.Text = lblPropertyCode.Text
                'Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
                'Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
                'Dim lblpropcode As Label = DirectCast(gvRow.FindControl("lblpropcode"), Label)
                'UPDATE_PNSTATUS(lblpropcode.Text)
                'update_userstatus()

            Else
                panel1.Visible = False
            End If
            'BindGrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim validatedat As Integer
            validatedat = ValidateDate()
            If validatedat = 0 Then
                lblMsg.Text = "Surrendered Date Should be Less than Joining Date"
                lblMsg.Visible = True
            Else
                If Not String.IsNullOrEmpty(hfsno.Value) Then
                    UpdateRecord()
                    Session("CITY") = ddlcity.SelectedItem.Value
                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_TNTSTATUS1")
                    'sp2.Command.AddParameter("@user", Session("uid"), DbType.String)
                    sp2.Command.AddParameter("@user", hfsno.Value, DbType.String)
                    sp2.Command.AddParameter("@city", ddlcity.SelectedItem.Value, DbType.String)
                    sp2.ExecuteScalar()
                    Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=40")
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdateRecord()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_SURRENDER_DATE")
        'sp.Command.AddParameter("@user", Session("uid"), DbType.String)
        sp.Command.AddParameter("@sno", hfsno.Value, DbType.String)
        sp.Command.AddParameter("@sdate", txtSdate.Text, DbType.Date)
        sp.Command.AddParameter("@City", ddlcity.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub UPDATE_PNSTATUS(ByVal propertycode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_PN_STATUS")
        sp.Command.AddParameter("@propcode", propertycode, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub update_userstatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_USR_STATUS")
        sp.Command.AddParameter("@user", Session("uid"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Function ValidateDate()
        Dim validatedat As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Validate_Surrender_Date")
        sp.Command.AddParameter("@SDATE", txtSdate.Text, DbType.Date)
        sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
        validatedat = sp.ExecuteScalar()
        Return validatedat
    End Function
End Class
