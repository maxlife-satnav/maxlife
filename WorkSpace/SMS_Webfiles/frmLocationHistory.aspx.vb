Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports System.IO
Partial Class WorkSpace_SMS_Webfiles_frmLocationHistory
    Inherits System.Web.UI.Page
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtFdate.Text = "" And ddlcity.SelectedIndex = 0 Then
            lblMsg.Text = "Please Enter Date or select city"
        Else
            BindGrid()
            pnlgrid.Visible = True
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_GET_LOCATION_HISTORY")
            If txtFdate.Text = "" Then
                sp.Command.AddParameter("@FROM_DATE", DBNull.Value, DbType.DateTime)
            Else
                sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.DateTime)
            End If

            sp.Command.AddParameter("@CITY", ddlcity.SelectedItem.Value, DbType.String)
         
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
            If gvitems.Rows.Count > 0 Then
                btnexport.Visible = True
            Else
                btnexport.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            BindCity()
            btnexport.Visible = False
            pnlgrid.Visible = False
            txtFdate.Attributes.Add("onClick", "displayDatePicker('" + txtFdate.ClientID + "')")
            txtFdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
    End Sub
    Private Sub BindCity()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_BIND_CITY_LOCATION_HISTORY")
            ddlcity.DataSource = sp.GetDataSet()
            ddlcity.DataTextField = "CTY_NAME"
            ddlcity.DataValueField = "CTY_CODE"
            ddlcity.DataBind()
            ddlcity.Items.Insert(0, New ListItem("--Select--", ""))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim gv As New GridView
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_GET_LOCATION_HISTORY")
        If txtFdate.Text = "" Then
            sp.Command.AddParameter("@FROM_DATE", DBNull.Value, DbType.DateTime)
        Else
            sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.DateTime)
        End If
        sp.Command.AddParameter("@CITY", ddlcity.SelectedItem.Value, DbType.String)
        gv.DataSource = sp.GetDataSet()
        gv.DataBind()
        Export("Available & Occupied Locations History" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
        
    End Sub
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblloc As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblloc"), Label)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_DELETE_LOCATION_HISTORY")
            sp.Command.AddParameter("@ID", lblloc.Text, DbType.Int32)
            sp.ExecuteScalar()
        End If
        BindGrid()
    End Sub

    Protected Sub gvitems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvitems.RowDeleting

    End Sub
End Class
