﻿Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient
Partial Class WorkSpace_SMS_Webfiles_seatmovement
    Inherits System.Web.UI.Page

    Protected Sub btnrefresh_Click(sender As Object, e As EventArgs) Handles btnrefresh.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_SEAT_MOVEMENT_REQUEST")
        sp.ExecuteScalar()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                lblMsg.Text = ""
                
            End If
        End If
    End Sub
End Class
