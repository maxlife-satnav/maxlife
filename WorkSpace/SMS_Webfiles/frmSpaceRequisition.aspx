<%@ Page Language="VB" AutoEventWireup="false" ValidateRequest="true" CodeFile="frmSpaceRequisition.aspx.vb"
    Inherits="frmSpaceRequisition" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function CheckDate() {
            var dtFrom = document.getElementById("txtFromdate").Value;
            var dtTo = document.getElementById("txtTodate").Value;
            if (dtFrom < dtTo) {
                alert("Invalid Dates");
            }
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Requisition
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="vsSM" runat="server" CssClass="alert alert-danger" ForeColor="red" ValidationGroup="submit" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" Font-Bold="True" Visible="False" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">City <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="rfvCity" runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City"
                                            InitialValue="--Select--" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLoc" Display="None"
                                            ErrorMessage="Please Select Location" InitialValue="--Select--" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">

                                            <asp:DropDownList ID="ddlLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlLoc_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Tower <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvT1"
                                            runat="server" ErrorMessage="Please Select Tower" ControlToValidate="ddlTower1"
                                            Display="None" InitialValue="--Select--" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">

                                            <asp:DropDownList ID="ddlTower1" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div visible="FALSE" runat="server" class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label" runat="server" visible="false"></label>
                                        Country
                                        <asp:TextBox ID="txtCity1" runat="server" ReadOnly="True" CssClass="form-control"
                                            Visible="False"></asp:TextBox>
                                        <div>
                                            <asp:TextBox ID="txtCountry1" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div align="left" visible="FALSE">
                                            Preferred Location
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtLocation1" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvFD" runat="server" ErrorMessage="Please Select From Date"
                                            ControlToValidate="txtFromdate" Display="None" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFromdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTD" runat="server" ErrorMessage="Please Select To Date"
                                            ControlToValidate="txtTodate" Display="None" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelVertical" runat="server" Text="" class="col-md-5 control-label">
                                        <span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                            runat="server" ControlToValidate="ddlVertical"
                                            Display="None" InitialValue="--Select--" ValidationGroup="submit"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlVertical" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblSelCostcenter" runat="server" Text="" class="col-md-5 control-label"><span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                            runat="server" ControlToValidate="ddlCostcenter"
                                            Display="None" InitialValue="--Select--" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCostcenter" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <h5>
                                            <label class="col-md-12 control-label" style="text-align: left;">Full Cabins / Half Cabins / Work Stations Required</label></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="trDep" runat="server" visible="false" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Department<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            runat="server" ErrorMessage="Please Select Department " ControlToValidate="ddlDept"
                                            Display="None" InitialValue="--Select--" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Work Stations<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="rfvWS" runat="server" ErrorMessage="Please Enter Work Stations in Numbers"
                                            ControlToValidate="txtWstations" Display="None" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reg" runat="server" ControlToValidate="txtWstations"
                                            Display="None" ErrorMessage="Only Numerics are allowed in Work stations field"
                                            ValidationExpression="^[0-9]*" ValidationGroup="submit"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtWstations" runat="server" CssClass="form-control"
                                                MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Half Cabins<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please Enter Half Cabins in Numbers"
                                            ControlToValidate="txtHcabins" Display="None" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regHC" runat="server" ControlToValidate="txtHcabins"
                                            Display="None" ErrorMessage="Only Numerics are allowed in Half Cabins field"
                                            ValidationExpression="^[0-9]*" ValidationGroup="submit"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtHcabins" runat="server" CssClass="form-control"
                                                MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Full Cabins<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Enter Full Cabins in Numbers"
                                            ControlToValidate="txtFcabins" Display="None" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regFc" runat="server" ControlToValidate="txtFcabins"
                                            Display="None" ErrorMessage="Only Numerics are allowed in Full Cabins field"
                                            ValidationExpression="^[0-9]*" ValidationGroup="submit"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtFcabins" runat="server" CssClass="form-control"
                                                MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                        <asp:CustomValidator ID="cvRemarks"
                                            runat="server" ClientValidationFunction="maxLength" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Remarks Shoud be less than 500 characters "></asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvRem" runat="server" ErrorMessage="Please Enter Remarks"
                                            Display="None" ControlToValidate="txtRemarks" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter remarks in 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                                    MaxLength="500" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtFromdate"
                                        Display="None" ErrorMessage="Please enter valid from date !" Operator="DataTypeCheck"
                                        Type="Date" ValidationGroup="submit"></asp:CompareValidator>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="submit" />
                                    <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" Visible="False"
                                        BorderWidth="0px"></asp:TextBox>
                                    <asp:CompareValidator ID="cvToDate" runat="server" ControlToValidate="txtTodate"
                                        Display="None" ErrorMessage="Please enter valid to date !" Operator="DataTypeCheck"
                                        Type="Date" ValidationGroup="submit"></asp:CompareValidator>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />

                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-12">
                                <asp:GridView ID="gvCount" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:BoundField DataField="COST_CENTER_NAME" HeaderText="Vertical Name" />
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblWS" runat="server" Text='<%# Eval("ws")%>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHC" runat="server" Text='<%# Eval("HC")%>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFC" runat="server" Text='<%# Eval("FC")%>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Work Stations Available" DataField="ws" />
                                        <asp:BoundField HeaderText="Half Cabins Available" DataField="HC" />
                                        <asp:BoundField HeaderText="Full Cabins Available" DataField="FC" />
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
