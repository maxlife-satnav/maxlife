Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Partial Class WorkSpace_SMS_Webfiles_frmViewVerticalReq
    Inherits System.Web.UI.Page
    Dim obj As clsViewCancelChange
    Dim verBll As New VerticalBLL()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Public Sub BindReqidtoDropDown()
        Dim reqDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        reqDTO = verBll.GetRequIds(5)
        ddlReqID.DataSource = reqDTO
        ddlReqID.DataTextField = "RequestID"
        ddlReqID.DataValueField = "RequestID"
        ddlReqID.DataBind()
        ddlReqID.Items.Insert(0, "-- Select --")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("uid") = "" Then
        '    Response.Redirect(AppSettings("logout"))
        'End If
        If Not Page.IsPostBack Then
            ' BindReqidtoDropDown()
            Try
                loadlocation()
                loaddepartment()
                loadrm()
                If ddlRM.Items.Count = 2 Then
                    ddlRM.SelectedIndex = 1
                End If

                txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
                txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
                txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Vertical Requisition", "Load", ex)

            End Try
        End If
    End Sub
    Public Sub loadlocation()
        strSQL = "select lcm_code,lcm_name from " & Session("TENANT") & "."  & "location where lcm_sta_id=1 order by lcm_name"
        BindCombo(strSQL, ddlSelectLocation, "LCM_NAME", "LCM_CODE")
    End Sub
    Public Sub loaddepartment()
        strSQL = "select VER_code,VER_NAME FROM " & Session("TENANT") & "."  & "VERTICAL WHERE VER_STA_ID=1 ORDER BY VER_NAME"
        BindCombo(strSQL, ddlVertical, "VER_NAME", "VER_code")
    End Sub
    Public Sub loadrm()
        strSQL = "SELECT AUR_REPORTING_TO,USR_NAME FROM " & _
                " " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW," & Session("TENANT") & "."  & "[User] WHERE AUR_ID= '1' and usr_id=AUR_REPORTING_TO"
        BindCombo(strSQL, ddlRM, "usr_name", "aur_reporting_to")
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

    End Sub

    Protected Sub ddlReqID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqID.SelectedIndexChanged

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

    End Sub
End Class
