<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSurrenderLease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApprovalLease" Title="Surrender Lease" %>

<%@ Register Src="../../Controls/SurrenderLease.ascx" TagName="SurrenderLease" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Surrender Lease</legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">                                                  
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />
                            <uc1:SurrenderLease ID="SurrenderLease1" runat="server"></uc1:SurrenderLease>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>






