Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_TotalEmpCountReport
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindCity()
            gv_report.Visible = False
            btnExport.Visible = False
        End If
    End Sub
    Private Sub BindCity()
        Try
            ObjSubSonic.Binddropdown(ddlcity, "GET_CONSOLIDATE_CITY", "CTY_NAME", "CTY_CODE")
            ddlcity.Items.Insert(1, New ListItem("--All--", "All"))
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CONSOLIDATE_CITY")
            'ddlcity.DataSource = sp.GetDataSet()
            'ddlcity.DataTextField = "CTY_NAME"
            'ddlcity.DataValueField = "CTY_CODE"
            'ddlcity.DataBind()
            'ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
            'ddlcity.Items.Insert(1, New ListItem("All", "All"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindGridSummary()
        'ObjSubSonic.BindGridView(gv_report, "GET_Outsourced_Solid", ddlcity.SelectedItem.Value)

        Dim strctycode As String = ""
        If ddlcity.SelectedItem.Text <> "--All--" Then
            strctycode = ddlcity.SelectedItem.Value
        End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CONSOLIDATE_REPORT")
        sp.Command.AddParameter("@CTY_CODE", strctycode, DbType.String)
        gv_report.DataSource = sp.GetDataSet()
        gv_report.DataBind()
        If gV_REPORT.Rows.Count > 0 Then
            gV_REPORT.Visible = True
            btnExport.Visible = True
        Else
            gV_REPORT.Visible = False
            btnExport.Visible = False
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportToExcelCityWise()
    End Sub


    Private Sub ExportToExcelCityWise()
        Dim strCity As String = ""

        If ddlcity.SelectedItem.Text <> "--All--" Then
            strCity = ddlcity.SelectedItem.Value
        Else
            strCity = ""
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = strCity

        Dim dsCityWise As New DataSet
        dsCityWise = objsubsonic.GetSubSonicDataSet("GET_CONSOLIDATE_REPORT", param)

        dsCityWise.Tables(0).Columns(0).ColumnName = "City"
        dsCityWise.Tables(0).Columns(1).ColumnName = "Location"
        dsCityWise.Tables(0).Columns(2).ColumnName = "Location Code"
        dsCityWise.Tables(0).Columns(3).ColumnName = "Available (WT)"
        dsCityWise.Tables(0).Columns(4).ColumnName = "Occupied (WT)"
        dsCityWise.Tables(0).Columns(5).ColumnName = "Available (WI)"
        dsCityWise.Tables(0).Columns(6).ColumnName = "Occupied (WI)"
        dsCityWise.Tables(0).Columns(7).ColumnName = "Available (WBPO)"
        dsCityWise.Tables(0).Columns(8).ColumnName = "Occupied (WBPO)"
        dsCityWise.Tables(0).Columns(9).ColumnName = "Available (WSTL)"
        dsCityWise.Tables(0).Columns(10).ColumnName = "Occupied (WSTL)"


        'dsCityWise.Tables(0).Columns.RemoveAt(0)
        'dsCityWise.Tables(0).Columns.RemoveAt(3)


        Dim gv As New GridView
        gv.DataSource = dsCityWise
        gv.DataBind()
        Export("ConsolidateReport.xls", gv)




    End Sub


    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlcity.SelectedIndex > 0 Then
            BindGridSummary()
        End If

    End Sub

    Protected Sub gv_report_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gv_report.PageIndexChanging
        gv_report.PageIndex = e.NewPageIndex
        BindGridSummary()
    End Sub
End Class
