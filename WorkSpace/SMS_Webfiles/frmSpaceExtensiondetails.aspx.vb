Imports System
Imports System.Net
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic

Imports clsSubSonicCommonFunctions


Partial Class SpaceManagement_frmSpaceVerticalAllocations
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters()
    Dim objExtenedRelease As New clsExtenedRelease
    Dim Email As String = String.Empty
    Dim strRedirect As String = String.Empty
    Dim dt As New DataTable

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Visible = False
        If Not Page.IsPostBack Then
            Try
                BindCity()
                ddlTower.Items.Insert(0, "--Select--")
                'obj.BindTower(ddlTower)
                lblSelVertical.Text = Session("Parent")
                btnSubmit.Visible = False
                rfvVertical.ErrorMessage = "Please Select " + Session("Parent")
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
            End Try
        End If
    End Sub

    Private Sub BindCity()
        ObjSubSonic.Binddropdown(ddlCity, "GETALLCITIES", "CTY_NAME", "CTY_CODE")
    End Sub

    Dim verBll As New VerticalBLL()
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
                ddlWing.Items.Clear()
                ddlWing.Items.Insert(0, "--Select--")
                ddlVertical.Items.Clear()
                ddlVertical.Items.Insert(0, "--Select--")
                'ddlCostCenter.Items.Clear()
                'ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")
                ddlWing.Items.Clear()
                ddlWing.Items.Insert(0, "--Select--")
                ddlVertical.Items.Clear()
                ddlVertical.Items.Insert(0, "--Select--")
                'ddlCostCenter.Items.Clear()
                'ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
        End Try
    
    End Sub
    Private Function CheckExtens(ByVal dtdate As Date, ByVal space As String, ByVal req As String, ByVal mode As Integer)
        Dim extend As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECK_EXTEND_DATE")
        sp.Command.AddParameter("@DATE", dtdate, DbType.Date)
        sp.Command.AddParameter("@SPACE", space, DbType.String)
        sp.Command.AddParameter("@REQ", req, DbType.String)
        sp.Command.AddParameter("@MODE", mode, DbType.Int32)
        extend = sp.ExecuteScalar
        Return extend
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)
                If chk.Checked = True Then
                    Dim txtExtendDate As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendDate"), TextBox)
                    Dim lblspace As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblspace"), Label)
                    Dim lblReqID As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblReqID"), Label)
                    Try
                        Dim dtDate As Date = txtExtendDate.Text
                    Catch ex As Exception
                        lblMsg.Text = "Please Select valid Date"
                        lblMsg.Visible = True
                        Exit Sub
                    End Try


                    If CType(gvSpaceExtend.Rows(i).Cells(6).Text, Date) >= CType(txtExtendDate.Text, Date) Then
                        lblMsg.Text = "Date should not be less than are equal to FromDate"
                        lblMsg.Visible = True
                        Exit Sub
                    End If


                    ' Check Condition for Extension 
                    If CType(gvSpaceExtend.Rows(i).Cells(7).Text, Date) >= CType(txtExtendDate.Text, Date) Then
                        Dim extend As Integer
                        extend = CheckExtens(txtExtendDate.Text, lblspace.Text, lblReqID.Text, 2)
                        If extend = 1 Then
                            lblMsg.Text = "Space already Allocated for this Date"
                            lblMsg.Visible = True
                            Exit Sub

                        End If

                    End If


                    If CType(gvSpaceExtend.Rows(i).Cells(7).Text, Date) <= CType(txtExtendDate.Text, Date) Then
                        Dim extend As Integer
                        extend = CheckExtens(txtExtendDate.Text, lblspace.Text, lblReqID.Text, 1)
                        If extend = 1 Then
                            lblMsg.Text = "Space already Allocated for this Date"
                            lblMsg.Visible = True
                            Exit Sub

                        End If

                    End If

                    


                   
                    
                    Dim txtExtendReason As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendReason"), TextBox)
                    intCheckCount += 1
                    If txtExtendDate.Text = "" Then
                        lblMsg.Text = "Please Select Extend Date"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If txtExtendReason.Text = "" Then
                        lblMsg.Text = "Please enter Reason to Extend"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If txtExtendReason.Text.Length() > 500 Then
                        lblMsg.Text = "Reason not more than 500 letters"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atleast one checkbox before Submit"
                lblMsg.Visible = True
                Exit Sub
            End If

            dt.Columns.Add("Sno", GetType(Integer))
            dt.Columns.Add("SpaceID", GetType(String))
            dt.Columns.Add("ExtDate", GetType(String))
            dt.Columns.Add("FromDate", GetType(Date))
            dt.Columns.Add("ToDate", GetType(Date))
            dt.Columns.Add("Remarks", GetType(String))
            Dim drNew As DataRow
            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chkEmp As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)
                Dim lblMail As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblAllocEmpMail"), Label)
                Dim lblempid As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblempid"), Label)

                If chkEmp.Checked = True Then
                    Dim txtExtendReason As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendReason"), TextBox)
                    Dim lblReqID As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblReqID"), Label)
                    Dim lblspace As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblspace"), Label)
                    Dim txtDate As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendDate"), TextBox)
                    Try

                        objExtenedRelease.updateSpace(lblspace.Text, txtExtendReason.Text.Trim().Replace("'", "''"), lblReqID.Text, txtDate.Text, Session("uid"))

                        drNew = dt.NewRow
                        drNew(0) = dt.Rows.Count + 1
                        drNew(1) = lblspace.Text
                        drNew(2) = txtDate.Text.Trim()
                        drNew(3) = gvSpaceExtend.Rows(i).Cells(6).Text
                        drNew(4) = gvSpaceExtend.Rows(i).Cells(7).Text
                        drNew(5) = txtExtendReason.Text.Trim()
                        dt.Rows.Add(drNew)
                        Email = lblMail.Text
                        sendmail(lblReqID.Text)
                        'sendMail(dt, gvSpaceExtend.Rows(i).Cells(1).Text, gvSpaceExtend.Rows(i).Cells(2).Text, gvSpaceExtend.Rows(i).Cells(3).Text, Email, lblempid.Text)

                    Catch ex As Exception
                        Throw New Amantra.Exception.DataException("This error has been occured while Updating data to Change Timeline.", "Space Allocation", "Load", ex)
                        lblMsg.Text = "unable to extend"
                        lblMsg.Visible = True
                        Exit Sub
                    End Try
                End If
            Next
            If dt.Rows.Count > 0 Then
                Session("ExtendData") = dt
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("3") & ""
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Allocation Details", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub
    Private Sub sendmail(reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEEXTENSIONREQ")
        sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@REQID", reqid, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex = 0 Then
                gvSpaceExtend.Visible = False
                ddlTower.SelectedIndex = 0
                btnSubmit.Visible = False
            Else
                obj.BindTowerLoc(ddlTower, ddlLocation.SelectedValue.Trim())
            End If



        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
        End Try


    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            If ddlTower.SelectedItem.Text = "--Select--" Then
                gvSpaceExtend.DataSource = Nothing
                gvSpaceExtend.DataBind()
                btnSubmit.Visible = False
                Exit Sub
            End If
            '  objExtenedRelease.bindData(ddlTower.SelectedValue, gvSpaceExtend, 7)
            objExtenedRelease.bindData(ddlLocation.SelectedValue, gvSpaceExtend, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue, ddlVertical.SelectedItem.Value, 7)
            If gvSpaceExtend.Rows.Count > 0 Then
                btnSubmit.Visible = True
                gvSpaceExtend.Visible = True
            Else
                ddlTower.SelectedIndex = 0
                btnSubmit.Visible = False

                lblMsg.Text = "No Spaces to Extend in this Tower"
                lblMsg.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
        End Try

    End Sub

    Private Sub sendMail(ByVal dt As DataTable, ByVal strTower As String, ByVal strFloor As String, ByVal strWing As String, ByVal strEmail As String, ByVal empid As String)

        Try
            Dim to_mail As String = strEmail
            Dim cc_mail As String = Session("uemail")
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail1 As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim objData As SqlDataReader
            Dim user As String = String.Empty
           
            Dim strKnownas As String = String.Empty
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
            sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            objData = sp1.GetReader()
            While objData.Read
                strRR = objData("aur_reporting_to").ToString
                strRM = objData("aur_reporting_email").ToString
                strKnownas = objData("aur_known_as").ToString
                cc_mail = objData("aur_email").ToString

            End While

            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
            sp2.Command.AddParameter("@AUR_ID", empid, DbType.String)
            Dim dr As SqlDataReader
            dr = sp2.GetReader()
            While (dr.Read)
                user = dr("aur_known_as").ToString
                to_mail = dr("aur_email").ToString
            End While

            body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
            body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'> Dear <b> Sir/Madam&nbsp;</b> , </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Project timeline extension request has been raised by " + user + " And is pending for your approval. The details are as follows.</td></tr></table>&nbsp;<br /> "
            body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
            body = body & "<table align='center'>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Floor Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strFloor & "</td></tr>"
            body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Wing Name</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strWing & "</td></tr>"

            body = body & "</table>"
            body = body & "</td></tr></table><br>"
            body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
            body = body & "<table align='center'>"
            Dim j As Integer = 3
            For i As Integer = 0 To dt.Rows.Count - 1
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Space Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows(i)(1).ToString() & "</td></tr> "
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Extended Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows(i)(2).ToString() & "</td></tr>"
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dt.Rows(i)(3).ToString()).ToShortDateString() & "</td></tr>"
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & Convert.ToDateTime(dt.Rows(i)(4).ToString()).ToShortDateString() & "</td></tr>"
                body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & dt.Rows(i)(5).ToString() & "</td></tr>"
                j = j + 4
            Next
            body = body & "</table>"
            body = body & "</td></tr></table>"
            body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"

            body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
            body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
            body = body & "</table>"

            
            Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
            Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
            Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
            Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
            Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
            Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
            Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)


            parms10.Value = "Abcd"
            parms11.Value = body
            parms12.Value = to_mail
            parms13.Value = "Space Extension Details"
            parms14.Value = getoffsetdatetime(DateTime.Now)
            parms15.Value = "Request Submitted"
            parms16.Value = "Normal Mail"
            parms17.Value = cc_mail
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)
           
            Dim parms19 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
            Dim parms20 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
            Dim parms21 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
            Dim parms22 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
            Dim parms23 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
            Dim parms24 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
            Dim parms25 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim parms26 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

            parms19.Value = "Abcd"
            parms20.Value = body
            parms21.Value = strRM
            parms22.Value = "Space Extension Details"
            parms23.Value = getoffsetdatetime(DateTime.Now)
            parms24.Value = "Request Submitted"
            parms25.Value = "Normal Mail"
            parms26.Value = String.Empty

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms19, parms20, parms21, parms22, parms23, parms24, parms25, parms26)


            
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
        End Try


    End Sub

    Protected Sub gvSpaceExtend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpaceExtend.PageIndexChanging
        gvSpaceExtend.PageIndex = e.NewPageIndex
        objExtenedRelease.bindData(ddlLocation.SelectedValue, gvSpaceExtend, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue, ddlVertical.SelectedItem.Value, 7)
    End Sub



    Protected Sub gvSpaceExtend_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSpaceExtend.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim txtMonth As TextBox = CType(e.Row.Cells(0).FindControl("txtExtendDate"), TextBox)
            txtMonth.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtMonth.Attributes.Add("onClick", "displayDatePicker('" + txtMonth.ClientID + "')")

        End If
    End Sub


    Dim obj1 As New clsReports
    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        Try
            obj1.bindwing(ddlWing, ddlFloor.SelectedValue, ddlLocation.SelectedValue, ddlTower.SelectedValue)
            ddlWing.Items(0).Text = "--Select--"
            
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseOccupancy", "ddlFloor_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub ddlWing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWing.SelectedIndexChanged

        Dim sp1 As New SqlParameter("@BDG_ID", SqlDbType.VarChar, 250)
        Dim sp2 As New SqlParameter("@TWR_ID", SqlDbType.VarChar, 250)
        Dim sp3 As New SqlParameter("@FLR_ID", SqlDbType.VarChar, 250)
        Dim sp4 As New SqlParameter("@WNG_ID", SqlDbType.VarChar, 250)
        Dim sp5 As New SqlParameter("@AUR_ID", SqlDbType.VarChar, 250)

        sp1.Value = ddlLocation.SelectedValue
        sp2.Value = ddlTower.SelectedValue
        sp3.Value = ddlFloor.SelectedValue
        sp4.Value = ddlWing.SelectedValue
        sp5.Value = Session("UID")

        ddlVertical.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GETCOSTCENTERBYDETAILS", sp1, sp2, sp3, sp4, sp5)
        ddlVertical.DataTextField = "COST_CENTER_NAME"
        ddlVertical.DataValueField = "COST_CENTER_CODE"
        ddlVertical.DataBind()
        ddlVertical.Items.Insert(0, "--Select--")

        If ddlWing.SelectedItem.Text = "--Select--" Then
            ddlVertical.Items.Clear()
            ddlVertical.Items.Insert(0, "--Select--")



            'ddlCostCenter.Items.Clear()
            'ddlCostCenter.Items.Insert(0, "--Select--")
            'ddlProject.Items.Clear()
            'ddlProject.Items.Insert(0, "--Select--")
        Else

            'ddlCostCenter.Items.Clear()
            'ddlCostCenter.Items.Insert(0, "--Select--")
            'ddlProject.Items.Clear()
            'ddlProject.Items.Insert(0, "--Select--")
        End If
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        Try
            If (ddlVertical.SelectedIndex > 0) Then
                Dim sp1 As New SqlParameter("@TWR_ID", SqlDbType.VarChar, 250)
                Dim sp2 As New SqlParameter("@FLR_ID", SqlDbType.VarChar, 250)
                Dim sp3 As New SqlParameter("@WNG_ID", SqlDbType.VarChar, 250)
                Dim sp4 As New SqlParameter("@BDG_ID", SqlDbType.VarChar, 250)
                Dim sp5 As New SqlParameter("@COSTCENTER", SqlDbType.VarChar, 250)

                sp1.Value = ddlTower.SelectedValue
                sp2.Value = ddlFloor.SelectedValue
                sp3.Value = ddlWing.SelectedValue
                sp4.Value = ddlLocation.SelectedValue
                sp5.Value = ddlVertical.SelectedValue

                'ddlCostCenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_PROJECTCOSTCENTER", sp1, sp2, sp3, sp4, sp5)
                'ddlCostCenter.DataTextField = "cost_center_name"
                'ddlCostCenter.DataValueField = "cost_center_code"
                'ddlCostCenter.DataBind()
                'ddlCostCenter.Items.Insert(0, "--Select--")


                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            Else
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")



                'ddlCostCenter.Items.Clear()
                'ddlCostCenter.Items.Insert(0, "--Select--")


              
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
        End Try

       

    End Sub

    'Protected Sub ddlCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCenter.SelectedIndexChanged
    '    Try





    '        If ddlCostCenter.SelectedIndex > 0 Then
    '            Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
    '            sp1.Value = ddlVertical.SelectedValue
    '            Dim sp2 As New SqlParameter("@vc_costcenter", SqlDbType.VarChar, 250)
    '            sp2.Value = ddlCostCenter.SelectedValue
    '            ddlProject.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvcproject", sp1, sp2)
    '            ddlProject.DataTextField = "prj_name"
    '            ddlProject.DataValueField = "prj_code"
    '            ddlProject.DataBind()
    '            ddlProject.Items.Insert(0, "--Select--")
    '        Else
    '            ddlProject.Items.Clear()
    '        End If


    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
    '    End Try
    'End Sub


    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlFloor.Items.Clear()
        ddlTower.Items.Clear()
        ddlWing.Items.Clear()

        ddlVertical.Items.Clear()
        Dim cty_code As String = ""
        If ddlCity.SelectedItem.Text = "--Select--" Then
            lblMsg.Text = "Please select city."
            Exit Sub
        Else
            cty_code = ddlCity.SelectedItem.Value
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = cty_code

        ObjSubSonic.Binddropdown(ddlLocation, "GETACTIVELOCATIONBY_CCODE", "LCM_NAME", "LCM_CODE", param)

    End Sub




End Class
