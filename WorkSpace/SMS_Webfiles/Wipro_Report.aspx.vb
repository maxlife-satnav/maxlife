Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail

Partial Class WorkSpace_SMS_Webfiles_AmantraAxis_Report
    Inherits System.Web.UI.Page
    Dim objMaster As New clsMasters()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("uid") = "" Then
        '    Response.Redirect(AppSettings("logout"))
        'End If
        If Not Page.IsPostBack Then
            Try

                getRptDocList_Normal()

            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
            End Try

        End If
    End Sub

    Public Sub getRptDocList(ByVal ds As DataSet)

        gvReportList.DataSource = ds
        gvReportList.DataBind()

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        lblReport.Visible = True
        ExportPanel1.FileName = "ConsolidatedSpaceReport"
    End Sub





    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim spCount1 As New SqlParameter("@RPT_FOR", SqlDbType.VarChar, 500)
            Dim spCount2 As New SqlParameter("@RPT_DATE", SqlDbType.VarChar, 500)
            Dim spCount3 As New SqlParameter("@RPT_TITLE", SqlDbType.VarChar, 500)
            Dim spCount4 As New SqlParameter("@RPT_DOC", SqlDbType.VarChar, 500)
            spCount1.Value = ddlReportList.SelectedItem.Text
            spCount2.Value = getoffsetdatetime(DateTime.Now)
            spCount3.Value = txtTitle.Text
            spCount4.Value = FU_Doc.FileName

            Dim MCount As Integer = 0
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "INSERT_REPORT_DOCS", spCount1, spCount2, spCount3, spCount4)
            UploadDocs()
            lblError.Text = "Successfully added report document."
            getRptDocList_Normal()
        Catch ex As Exception
            lblError.Text = ""
        End Try
    End Sub

    Public Sub getRptDocList_Normal()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GET_REPORT_DOCS")
        getRptDocList(ds)
    End Sub

    Private Sub UploadDocs()
        If Not FU_Doc.PostedFile Is Nothing Then
            'Dim strpath As String = "..\Docs\" & 
            Dim filePath As String = Request.PhysicalApplicationPath.ToString & "Docs\" & FU_Doc.FileName
            Try
                FU_Doc.PostedFile.SaveAs(filePath)
            Catch ex As Exception
                Exit Sub
            End Try
        Else
        End If
        dtProcess = Nothing
    End Sub

    Protected Sub gvReportList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReportList.PageIndexChanging
        gvReportList.PageIndex = e.NewPageIndex
        If String.IsNullOrEmpty(txtSearch.Text) Then
            getRptDocList_Normal()
        Else
            getRptDocList_Search()
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        getRptDocList_Search()
        lblError.Text = ""
    End Sub

    Public Sub getRptDocList_Search()
        Dim ds As New DataSet
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@SearchQry", txtSearch.Text)
        param(1) = New SqlParameter("@fromdate", txtFdate.Text)
        param(2) = New SqlParameter("@todate", txtEDate.Text)

        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GET_REPORT_DOCS_Search", param)
        getRptDocList(ds)
    End Sub
End Class


