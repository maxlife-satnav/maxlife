<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmSurrenderProperty.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSurrenderProperty"
    Title="Surrender Property" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            
                
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Surrender Property
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-bordered table-hover table-striped"
                                        SelectedRowStyle-VerticalAlign="Top" AllowPaging="True" Width="100%" PageSize="5" EmptyDataText="No Surrender Property Found.">
                                        <Columns>
                                            <asp:TemplateField Visible="false" HeaderText="SNO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpropcode" runat="server" Text='<%#Eval("PN_CODE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpropname" runat="server" Text='<%#Eval("PN_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Code" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltenantcode" runat="server" Text='<%#Eval("TEN_CODE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltenant" runat="server" Text='<%#Eval("TEN_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Surrender">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnksurrender" runat="server" Text="Surrender" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Surrender"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="View Details">
                                                <ItemTemplate>
                                                    <a href="#" onclick="showPopWin('<%# Eval("TEN_CODE") %>')">
                                                        <asp:Label ID="lbltencode" runat="server" Text="View Details"></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="panel1" runat="server">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Property Code <span style="color: red;">*</span></label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtpropertycode" ReadOnly="true" runat="server" Width="97%" CssClass="form-control"></asp:TextBox>
                                                    <asp:HiddenField ID="hfsno" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Enter Surrendered Date <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvDesgcode" runat="server" ControlToValidate="txtSdate" Display="None" ErrorMessage="Please Pick Date"
                                                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <div class='input-group date' id='fromdate'>
                                                        <asp:TextBox ID="txtSdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <%-- <asp:TextBox ID="txtSdate" runat="server" Width="97%" CssClass="form-control"></asp:TextBox>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">
                                                    Select City to Move <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlcity"
                                                    Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlcity" runat="server" Width="97%" CssClass="selectpicker" data-live-search="true"
                                                        ToolTip="Select City">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">

                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"
                                                CausesValidation="true" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    </div>
        
      <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Tenant Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" src="#" width="100%" height="450px" frameborder="0"></iframe>                   
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <script>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "frmTenantDetails.aspx?tenant=" + id);
            $("#myModal").modal().fadeIn();
        }       
    </script>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
