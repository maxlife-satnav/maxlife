﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repshiftwiseoccupancy
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False

        If Not Page.IsPostBack Then
            ReportViewer1.Visible = False
            bindcity()
            BindLocation("")
            bindshift()

            ddllocation.Items.Insert(0, "--All--")
            ddllocation.SelectedIndex = 0
            ddlshift.SelectedIndex = 0
            binddata_wingwise()
        End If
    End Sub
    Public Sub bindcity()
        ObjSubsonic.Binddropdown(ddlcity, "BINDALLCITIES", "CTY_NAME", "CTY_CODE")
        ddlcity.Items(0).Text = "--All--"

    End Sub
    Public Sub bindshift()

        ObjSubsonic.Binddropdown(ddlshift, "GET_ALL_SHIFTS", "Name", "Code")

        If ddlshift.Items.Count > 0 Then

            ddlshift.Items.Insert(0, "--All--")
            ddlshift.Items.RemoveAt(1)
        End If
    End Sub
    Public Sub BindLocation(ByVal strCity As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strCity
        ObjSubsonic.Binddropdown(ddllocation, "GET_LOC_BY_CITY", "LCM_NAME", "LCM_CODE", param)
        If ddllocation.Items.Count > 0 Then
            ' ReportViewer1.Visible = False

            ddllocation.Items.Insert(0, "--All--")
            ddllocation.Items.RemoveAt(1)
        End If
    End Sub
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click


        binddata_wingwise()

    End Sub

    Public Sub binddata_wingwise()

        Dim rds As New ReportDataSource()
        rds.Name = "ShiftOccupancyDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/ShiftOccupancyReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim Mcity As String = ""
        Dim Mlocation As String = ""
        Dim Mshift As String = ""

        If ddlcity.SelectedItem.Value = "--All--" Then
            Mcity = ""
        Else
            Mcity = ddlcity.SelectedItem.Value
        End If

        If ddllocation.SelectedItem.Value = "--All--" Then
            Mlocation = ""
        Else
            Mlocation = ddllocation.SelectedItem.Value
        End If


        If ddlshift.SelectedItem.Value = "--All--" Then
            Mshift = ""
        Else
            Mshift = ddlshift.SelectedItem.Value
        End If


        Dim sp1 As New SqlParameter("@VC_CTY_CODE", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = Mcity
        Dim sp2 As New SqlParameter("@VC_LOC_CODE", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = Mlocation
        Dim sp3 As New SqlParameter("@VC_SHIFTID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = Mshift

        Dim sp4 As New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 50)
        sp4.Value = "SSA_SPC_ID"

        Dim sp5 As New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 50)
        sp5.Value = "ASC"

        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_SHIFT_OCCUPANCY_REPORT", sp1, sp2, sp3, sp4, sp5)

        rds.Value = dt

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMPLOYEE_SHIFT_COUNT")
        sp.Command.AddParameter("@CTY_CODE", Mcity, DbType.String)
        sp.Command.AddParameter("@LOC_CODE", Mlocation, DbType.String)
        sp.Command.AddParameter("@SHIFT_ID", Mshift, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltot.Visible = True
            lbltot.Text = "Total Employees : " & ds.Tables(0).Rows(0).Item("EMPLOYEE_ID")
            lbltot.ForeColor = Drawing.Color.Black
            lbltot.Font.Bold = True

        End If
        lbltot.Visible = True
        t2.Visible = True

    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlcity.SelectedIndexChanged
        ReportViewer1.Visible = False
        t2.Visible = False
        BindLocation(ddlcity.SelectedItem.Value)
        ddllocation.Items(0).Text = "--All--"
        If ddllocation.Items.Count = 2 Then
            ddllocation.SelectedIndex = 0
            'ddlFloor_SelectedIndexChanged(sender, e)
        End If
        bindshift()
        'ddlshift.Items(0).Text = "--All--"
        'If ddlshift.Items.Count = 2 Then
        '    ddlshift.SelectedIndex = 0
        '    bindshift("", "")
        '    'ddlWing_SelectedIndexChanged(sender, e)
        'End If
    End Sub

    Protected Sub ddllocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllocation.SelectedIndexChanged
        ReportViewer1.Visible = False
        t2.Visible = False
        bindshift()
        'ddlshift.Items(0).Text = "--All--"
        'If ddlshift.Items.Count = 2 Then
        '    ddlshift.SelectedIndex = 0
        'End If
    End Sub
End Class
