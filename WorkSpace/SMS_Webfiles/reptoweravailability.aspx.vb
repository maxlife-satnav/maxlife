﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_reptoweravailability
    Inherits System.Web.UI.Page
    Dim objMasters As clsMasters
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False
        Try
            If Not Page.IsPostBack Then
                'ReportViewer1.Visible = False
                obj.bindLocation(ddllocation)
                ddllocation.Items(0).Text = "--All Locations--"
                ddlTower.Items.Insert(0, "--All Towers--")
                ddlTower.SelectedIndex = 0
                binddata()

            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Dim dtReport, dtTemp As New DataTable
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        binddata()

    End Sub

    Public Sub binddata()
        Try

            Dim rds As New ReportDataSource()
            rds.Name = "TowerAvailabilityDS"

            'This refers to the dataset name in the RDLC file
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/TowerAvailabilityReport.rdlc")
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True


            Dim MTowerId As String = ""
            Dim MBdgId As String = ""

            If ddlTower.SelectedValue = "--All Towers--" Then
                MTowerId = ""
            Else
                MTowerId = ddlTower.SelectedValue
            End If

            If ddllocation.SelectedValue = "--All Locations--" Then
                MBdgId = ""
            Else
                MBdgId = ddllocation.SelectedValue
            End If

            Dim sp3 As New SqlParameter("@vc_LocationName", SqlDbType.NVarChar, 50)
            sp3.Value = MBdgId
            Dim sp4 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50)
            sp4.Value = MTowerId

            Dim sp5 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
            sp5.Value = "Locationname"

            Dim sp6 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
            sp6.Value = "ASC"
            dtTemp = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Availability_Towerwise", sp3, sp4, sp5, sp6)

            rds.Value = dtTemp


        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddllocation.SelectedIndexChanged
        ReportViewer1.Visible = False
        obj.bindTower_Locationwise(ddlTower, ddllocation.SelectedValue)
        ddlTower.Items(0).Text = "--All Towers--"
        If ddlTower.Items.Count = 2 Then
            ddlTower.SelectedIndex = 0
        End If

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
        If ddlTower.SelectedIndex > 1 Then

        End If
        If ddlTower.SelectedValue = "--All Towers--" Then

        End If
    End Sub

End Class
