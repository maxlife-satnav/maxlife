Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repTWROccupancy
    Inherits System.Web.UI.Page
    Dim objMasters As clsMasters
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False
        Try
            If Not Page.IsPostBack Then
                binddata()
                'ReportViewer1.Visible = False
                objMasters = New clsMasters()
                objMasters.Bindlocation(ddlbdg)
                ddlbdg.Items(0).Text = "--All Locations--"
                ddlTower.Items.Insert(0, "--All Towers--")
                ddlTower.SelectedIndex = 0
                lbltotal_util.ForeColor = Drawing.Color.Black
                lbltotal_util.Font.Bold = True
            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Dim dtReport, dtTemp As New DataTable
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click



        If ddlTower.SelectedIndex = 0 Then
            lbl2.Visible = False
            'Exit Sub
        End If
        binddata()
        tutility.Visible = True

     
    End Sub

    Public Sub binddata()
        Try

            Dim rds As New ReportDataSource()
            rds.Name = "TowerUtilizationDS"
            'This refers to the dataset name in the RDLC file
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/TowerUtilizationReport.rdlc")
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True

           
                Dim MTowerId As String = ""
                Dim MBdgId As String = ""

                If ddlTower.SelectedValue = "--All Towers--" Then
                    MTowerId = ""
                Else
                    MTowerId = ddlTower.SelectedValue
                End If

                If ddlbdg.SelectedValue = "--All Locations--" Then
                    MBdgId = ""
                Else
                    MBdgId = ddlbdg.SelectedValue
                End If

                Dim sp3 As New SqlParameter("@vc_twrID", SqlDbType.NVarChar, 50)
                sp3.Value = MTowerId
                Dim sp4 As New SqlParameter("@bdg_id", SqlDbType.NVarChar, 50)
                sp4.Value = MBdgId

                Dim sp5 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
                sp5.Value = "BUILDING"

                Dim sp6 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
                sp6.Value = "ASC"
                dtTemp = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETTOTALWORKSTATIONS_REPORT", sp3, sp4, sp5, sp6)

          

            rds.Value = dtTemp


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWER_UTI_COUNT")
            sp.Command.AddParameter("@BDG_ID", MBdgId, DbType.String)
            sp.Command.AddParameter("@TWR_ID", MTowerId, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()

            If ds.Tables(0).Rows.Count > 0 Then
                lbltotal_util.Text = ds.Tables(0).Rows(0).Item("util") & "%"
                tutility.Visible = True
            Else
                tutility.Visible = False
            End If



        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddlbdg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlbdg.SelectedIndexChanged
        obj = New clsReports()
        obj.bindTower_Locationwise(ddlTower, ddlbdg.SelectedValue)
        'ddlTower.Items.Insert(1, "--All Towers--")
        ddlTower.Items(0).Text = "--All Towers--"
        ReportViewer1.Visible = False
        If ddlTower.Items.Count = 2 Then
            ddlTower.SelectedIndex = 0
            'ddlTower_SelectedIndexChanged(sender, e)
        End If
        tutility.Visible = False
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
        If ddlTower.SelectedIndex > 1 Then
            lbl2.Visible = False
        End If
        If ddlTower.SelectedValue = "--All Towers--" Then
            lbl2.Visible = False
        End If
    End Sub

End Class