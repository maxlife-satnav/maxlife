<%@ Page Language="VB"  MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="repTWRAllocation.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_repTWRAllocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>
    <script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>
<script language="javascript" type="text/javascript"> 
    function noway(go)
    {
        if(document.all)
        {
            if (event.button == 2)
            {
                alert('For Securtity reasons Right click option was disabled,Inconvenience regreted!');
                return false;
            }
        }
        if (document.layers) 
        {
            if (go.which == 3)
            {
                alert('For Securtity reasons Right click option was disabled,,Inconvenience regreted!');
                return false;
            }
        }
    } 
     if (document.layers) 
     {
        document.captureEvents(Event.MOUSEDOWN); 
     }
     document.onmousedown=noway;
    </script>
    <script language="javascript" type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal)
        {
            re = new RegExp(aspCheckBoxID)
            for(i = 0; i < form1.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox')
                {
                    if (re.test(elm.name))
                    elm.checked = checkVal
                }
           }
        }
    </script>

        <div>
        <table id="table4" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">  <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
            
                   <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="100%"
                    align="center" border="0">
                    <tr>
                        <td  colspan="3" align="left">
             <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
          </td>
                        </tr>
                    <tr>
                        <td style="height: 27px">
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td class="tableHEADER" style="height: 27px;" align="left">
                            <strong>&nbsp;Tower wise Allocation Report</strong></td>
                        <td style="height: 27px">
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td  colspan="3" align="left">
             <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                    ShowSummary="False" />
          </td>
                        </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                            &nbsp;</td>
                        <td align="center" style="width: 962px">
                            <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                border="1">
                                <tr>
                                </tr>
                                <tr>
                                    <td align="left" width="50%" style="height: 21px">
                                        Select Tower<font class="clsNote">*</font></td>
                                    <td align="center" width="50%" style="height: 21px">
                                        <asp:DropDownList ID="ddlReqID" runat="server" Width="99%" AutoPostBack="True" CssClass="clsComboBox">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="left" width="50%">
                                        Select Criteria<font class="clsNote">*</font></td>
                                    <td align="center" width="50%">
                                        <asp:DropDownList ID="DropDownList1" runat="server" Width="99%" AutoPostBack="True" CssClass="clsComboBox">
                                            <asp:ListItem Value="0">--Select Criteria--</asp:ListItem>
                                            <asp:ListItem Value="1">On Date</asp:ListItem>
                                            <asp:ListItem Value="2">Between Dates</asp:ListItem>
                                        
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                 <tr id ="trOnDate" runat ="server">
                                    <td align="left" width="50%">
                                        Select Date<font class="clsNote">*</font></td>
                                    <td align="center" width="50%">
                                        <asp:TextBox ID="txtOnDate" runat="server" MaxLength="10"></asp:TextBox>&nbsp;
                                    </td>
                                </tr>
                                 <tr id ="trFromDate" runat ="server">
                                    <td align="left" width="50%">
                                        Select From Date<font class="clsNote">*</font></td>
                                    <td align="center" width="50%">
                                        <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10"></asp:TextBox>&nbsp;
                                    </td>
                                </tr>
                               <tr id ="trToDate" runat ="server">
                                    <td align="left" width="50%">
                                        Select To Date<font class="clsNote">*</font></td>
                                    <td align="center" width="50%">
                                        <asp:TextBox ID="txtToDate" runat="server" MaxLength="10"></asp:TextBox>&nbsp;
                                    </td>
                                </tr>
                              </table>
                               <table id="Table2" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                border="1">
                                <tr>
                                <td style="width:25%; height: 24px;">
                                 <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="clsButton" Width="136px" />
                                </td>
                                   <td style="width:25%; height: 24px;">
                                 <asp:Button ID="btnViewCriteria" runat="server" Text="View Criteria" CssClass="clsButton" />
                                </td>
                                <td style="width:25%; height: 24px;">
                                 <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="clsButton" Width="120px" />
                                </td>
                                </tr>
                                </table>
                               <table id="Table1" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                border="0">
                                
                                <tr>
                                    <td align="left" colspan="4" style="height: 11px">
                                        <asp:GridView ID="gvSpaceExtend" runat="server" AutoGenerateColumns="False" Width="100%"
                                                    AllowPaging="True" PageSize="25">
                                                    <Columns>
                                                      
                                                          <asp:BoundField HeaderText="Tower" DataField="Tower" />
                                                        <asp:BoundField HeaderText="Floor" DataField="Floor" />
                                                        <asp:BoundField HeaderText="Wing" DataField="Wing" />
                                                        <asp:BoundField HeaderText="Space ID" DataField="Space" />
                                                        <asp:BoundField HeaderText="Space Type" DataField="Space" />
                                                        <asp:BoundField HeaderText="Employee Name" DataField="FROMDATE" />
                                                        <asp:BoundField HeaderText="From Date" DataField="FROMDATE" />
                                                        <asp:BoundField HeaderText="To Date" DataField="TODATE" />
                                                        <asp:BoundField HeaderText="Vertical" DataField="TODATE" />
                                                        <asp:BoundField HeaderText="Project" DataField="TODATE" />
                                                      
                                                    </Columns>
                                                    
                                                </asp:GridView></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" headers="24px" style="height: 24px">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4" headers="24px" style="height: 24px">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px; width: 962px;" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                </table>
            
            </asp:Panel>
   
         
            
            
        </div>
 </asp:Content>