'StatusID's For vertical ***********************
' 5   - Requsted
' 6   - Allocated
' 166 - Partially Allocated
' 8   - Cancelled  
'***********************

Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class frmViewSpaceRequisition
    Inherits System.Web.UI.Page


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If
        If Not Page.IsPostBack Then
            Try
                BindGrid()
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
            Finally
            End Try
        End If
    End Sub
    Dim strRedirect As String = String.Empty

    Public Sub BindGrid()
        Dim sp As SqlParameter = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
        sp.Value = Session("uid").ToString().Trim()
        Dim dt As DataSet
        dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_SPACE_REQ_COUNT", sp)
        dvSpaceRequistions.DataSource = dt
        dvSpaceRequistions.DataBind()
        If dvSpaceRequistions.Rows.Count = 0 Then
            lblSpace.Text = "No Requisitions in this Month"
        Else
            lblSpace.Text = ""
            Session("View") = "View"
        End If
    End Sub


    Protected Sub lnkSpaceReq_click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkSender As LinkButton = sender
            For i As Int16 = 0 To dvSpaceRequistions.Rows.Count - 1
                Dim lnk As LinkButton = CType(dvSpaceRequistions.Rows(i).FindControl("lnkSpaceReqId"), LinkButton)
                If lnk.ClientID = lnkSender.ClientID Then
                    Dim lblStatus As Label = CType(dvSpaceRequistions.Rows(i).FindControl("lblStatusID"), Label)

                    'If lblStatus.Text = "6" Then
                    '    PopUpMessage("Request has been allocated", Me)
                    '    Exit Sub
                    'ElseIf lblStatus.Text = "166" Then
                    '    PopUpMessage("Request has been Partially allocated", Me)
                    '    Exit Sub
                    'Else
                    ' If lblStatus.Text = "5" Then
                    'Or lblStatus.Text = "166" Or lblStatus.Text = "8" Then
                    PopUpMessage("Request has been Requested", Me)
                    'http://localhost/amantraAmantraAxis/WorkSpace/SMS_Webfiles/frmDashBoardViewVerticalReq.aspx
                    Dim str As String = Server.MapPath("frmDashBoardViewVerticalReq.aspx")
                    strRedirect = "frmDashBoardViewSpaceRequisition.aspx?ReqID=" & lnk.Text.Trim() & "&StaID=" & lblStatus.Text.Trim()

                    'Exit Sub
                    'Else
                    'PopUpMessage("Request -1", Me)
                    'Exit Sub
                    'End If
                End If
            Next
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)

        End Try

        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    Protected Sub dvSpaceRequistions_OnRowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'If e.Row.RowState = DataControlRowState.Alternate Then
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='White';")
            'Else
            '    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            '    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            'End If
        End If
    End Sub

    Protected Sub dvSpaceRequistions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles dvSpaceRequistions.PageIndexChanging
        dvSpaceRequistions.PageIndex = e.NewPageIndex
        BindGrid()

    End Sub
End Class