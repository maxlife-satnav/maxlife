﻿Imports System
Imports System.Net
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_frmEmployeeSwap
    Inherits System.Web.UI.Page
    Dim obj As New clsRelease
    Dim clsObj As New clsMasters
    Dim ds As DataSet
    Dim objMaster As New clsMasters()
    Dim objEmp As New clsEmpMapping()
    Dim objExtedSpace As New clsExtenedRelease()
    Dim strRedirect As String = String.Empty
    Dim Email As String = String.Empty
    Dim dt As New DataTable
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            Dim obj1 As New clsRelease
            Dim sta As Integer = 7
            LoadCountry()
            'LoadCity()
            'LoadLocation()
            'LoadTowers()
            'LoadFloors()
            LoadVertical()
            ddlDept.Items.Insert(0, "--Select--")
            btnswap.Visible = False
            ReqVertical.ErrorMessage = "Please select " & Session("Parent")
            ReqDept.ErrorMessage = "Please select " & Session("Parent")
            lblvert.Text = Session("Parent") & "<span style='color: red;'>*</span>"
            lbldept.Text = Session("Child") & "<span style='color: red;'>*</span>"
        End If
    End Sub

    Public Sub LoadCountry()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@MODE", SqlDbType.NVarChar, 50)
        param(1).Value = 2
        ObjSubSonic.Binddropdown(ddlCountry, "Get_Countries_ADM", "CNY_NAME", "CNY_CODE", param)
    End Sub
    Private Sub GetZone()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ZONE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlZone.DataSource = sp.GetDataSet()
            ddlZone.DataTextField = "ZN_NAME"
            ddlZone.DataValueField = "ZN_CODE"
            ddlZone.DataBind()
            ddlZone.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub GetState()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_STATE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@MODE", 1, DbType.Int32)
            ddlState.DataSource = sp.GetDataSet()
            ddlState.DataTextField = "STE_NAME"
            ddlState.DataValueField = "STE_CODE"
            ddlState.DataBind()
            ddlState.Items.Insert(0, "--Select--")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Private Sub LoadCity()
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
    '    param(0).Value = Session("Uid").ToString().Trim()
    '    ObjSubSonic.Binddropdown(ddlCity, "GET_CITY_ADM", "CTY_NAME", "CTY_CODE", param)
    '    LoadLocation()
    'End Sub

    'Public Sub LoadLocation()
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
    '    param(0).Value = Session("Uid").ToString().Trim()
    '    ObjSubSonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    'End Sub

    'Public Sub LoadTowers()
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
    '    param(0).Value = Session("Uid").ToString().Trim()
    '    ObjSubSonic.Binddropdown(ddlTower, "GET_TOWERS_BY_USRID", "TWR_NAME", "TWR_CODE", param)
    'End Sub

    'Public Sub LoadFloors()
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
    '    param(0).Value = Session("Uid").ToString().Trim()
    '    ObjSubSonic.Binddropdown(ddlFloor, "GET_FLOORS_BY_USRID", "FLR_NAME", "FLR_CODE", param)
    'End Sub

    Public Sub LoadVertical()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@MODE", SqlDbType.NVarChar, 50)
        param(1).Value = 2
        ObjSubSonic.Binddropdown(ddlVertical, "Get_Vertical_ADM", "VER_NAME", "VER_CODE", param)
    End Sub

    Public Sub BindGrid()
        Try
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50)
            param(0).Value = ddlTower.SelectedItem.Value
            param(1) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 50)
            param(1).Value = ddlFloor.SelectedItem.Value
            param(2) = New SqlParameter("@AUR_VERT_CODE", SqlDbType.NVarChar, 50)
            param(2).Value = ddlVertical.SelectedItem.Value
            param(3) = New SqlParameter("@AUR_PRJ_CODE", SqlDbType.NVarChar, 50)
            param(3).Value = ddlDept.SelectedItem.Value
            param(4) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)
            param(4).Value = Session("UID")

            Dim dsgvswap As New Data.DataSet
            dsgvswap = ObjSubSonic.GetSubSonicDataSet("BIND_EMP", param) 'SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "BIND_EMP", param)
            gvswap.DataSource = dsgvswap
            gvswap.DataBind()
            If gvswap.Rows.Count > 0 Then
                btnView.Visible = True
                gvswap.Visible = True
                btnswap.Visible = True
            Else
                'ddlCountry.SelectedIndex = 0
                'ddlCity.SelectedIndex = 0
                'ddlLocation.SelectedIndex = 0
                'ddlTower.SelectedIndex = 0
                'ddlFloor.SelectedIndex = 0
                'ddlVertical.SelectedIndex = 0
                'ddlDept.SelectedIndex = 0
                btnView.Visible = True
                lblMsg.Text = "No Records Found"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Try
            ddlZone.Items.Clear()
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
            param(0).Value = Session("Uid").ToString().Trim()
            param(1) = New SqlParameter("@CNYCODE", SqlDbType.NVarChar, 100)
            param(1).Value = ddlCountry.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlZone, "GET_ZONE_BY_CNY", "ZN_NAME", "ZN_CODE", param)
            If ddlZone.Items.Count = 1 Then
                lblMsg.Text = "No Zones Available"
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            ddlLocation.Items.Clear()
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Session("Uid").ToString().Trim()
            param(1) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlCity.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
            If ddlLocation.Items.Count = 1 Then
                lblMsg.Text = "No Locations Available"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            ddlTower.Items.Clear()
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Session("Uid").ToString().Trim()
            param(1) = New SqlParameter("@LCMID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlLocation.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlTower, "GET_TOWERSBYLCMID", "TWR_NAME", "TWR_CODE", param)
            If ddlTower.Items.Count = 1 Then
                lblMsg.Text = "No Towers Available"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Dim verBll As New VerticalBLL()
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            ddlFloor.Items.Clear()
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Session("Uid").ToString().Trim()
            param(1) = New SqlParameter("@TWRID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlTower.SelectedItem.Value
            param(2) = New SqlParameter("@LOCID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlLocation.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlFloor, "GET_FLRS_BY_TWR_LOC", "FLR_NAME", "FLR_CODE", param)
            If ddlFloor.Items.Count = 1 Then
                lblMsg.Text = "No Floors Available"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Dim obj1 As New clsReports
    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        Try
            ddlVertical.Items.Clear()
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BDG_ID", SqlDbType.VarChar, 250)
            param(0).Value = ddlLocation.SelectedValue
            param(1) = New SqlParameter("@TWR_ID", SqlDbType.VarChar, 250)
            param(1).Value = ddlTower.SelectedValue
            param(2) = New SqlParameter("@FLR_ID", SqlDbType.VarChar, 250)
            param(2).Value = ddlFloor.SelectedValue
            param(3) = New SqlParameter("@AUR_ID", SqlDbType.VarChar, 250)
            param(3).Value = Session("Uid").ToString().Trim()

            ddlVertical.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GETCOSTCENTERBYDETAILS", param)
            ddlVertical.DataTextField = "COST_CENTER_NAME"
            ddlVertical.DataValueField = "COST_CENTER_CODE"
            ddlVertical.DataBind()
            ddlVertical.Items.Insert(0, "--Select--")

            If ddlVertical.Items.Count = 1 Then
                lblMsg.Text = "No Business Units Available"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        Try
            If (ddlVertical.SelectedIndex > 0) Then
                Dim param(3) As SqlParameter
                param(0) = New SqlParameter("@TWR_ID", SqlDbType.VarChar, 250)
                param(0).Value = ddlTower.SelectedValue
                param(1) = New SqlParameter("@FLR_ID", SqlDbType.VarChar, 250)
                param(1).Value = ddlFloor.SelectedValue
                param(2) = New SqlParameter("@BDG_ID", SqlDbType.VarChar, 250)
                param(2).Value = ddlLocation.SelectedValue
                param(3) = New SqlParameter("@COSTCENTER", SqlDbType.VarChar, 250)
                param(3).Value = ddlVertical.SelectedValue
                ddlDept.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_PROJECTCOSTCENTER", param)
                ddlDept.DataTextField = "cost_center_name"
                ddlDept.DataValueField = "cost_center_code"
                ddlDept.DataBind()
                ddlDept.Items.Insert(0, "--Select--")
                If ddlDept.Items.Count = 1 Then
                    lblMsg.Text = "No Functions Available"
                End If
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
        End Try
    End Sub

    'Protected Sub ddlDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDept.SelectedIndexChanged
    '    Try
    '        If ddlDept.SelectedIndex > 0 Then
    '            Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
    '            sp1.Value = ddlVertical.SelectedValue
    '            Dim sp2 As New SqlParameter("@vc_costcenter", SqlDbType.VarChar, 250)
    '            sp2.Value = ddlDept.SelectedValue
    '        Else
    '        End If
    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
    '    End Try
    'End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            If ddlVertical.SelectedIndex = 0 Or ddlDept.SelectedIndex = 0 Then
                gvswap.DataSource = Nothing
                gvswap.DataBind()
            End If
            BindGrid()
            For i As Integer = 0 To gvswap.Rows.Count - 1
                Dim ddlEmp As DropDownList = CType(gvswap.Rows(i).FindControl("ddlEmp"), DropDownList)
                Dim lbldept As Label = CType(gvswap.Rows(i).FindControl("lblDept"), Label)
                Dim lblbempid As Label = CType(gvswap.Rows(i).FindControl("lblbempid"), Label)
                Dim param(4) As SqlParameter
                param(0) = New SqlParameter("@TWR_CODE", SqlDbType.VarChar, 250)
                param(0).Value = ddlTower.SelectedValue
                param(1) = New SqlParameter("@FLR_CODE", SqlDbType.VarChar, 250)
                param(1).Value = ddlFloor.SelectedValue
                param(2) = New SqlParameter("@LCM_CODE", SqlDbType.VarChar, 250)
                param(2).Value = ddlLocation.SelectedValue
                param(3) = New SqlParameter("@VER_CODE", SqlDbType.VarChar, 250)
                param(3).Value = ddlVertical.SelectedValue
                param(4) = New SqlParameter("@CST_CODE", SqlDbType.VarChar, 250)
                param(4).Value = ddlDept.SelectedValue
                ObjSubSonic.Binddropdown(ddlEmp, "GET_EMP_ACTIVE_USERS", "NAME", "Aur_Id", param)
                'ddlEmp.Items.Remove(ddlEmp.Items.FindByValue(lblbempid.Text))
            Next
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Release Details", "Load", ex)
        End Try
    End Sub

    Protected Sub ddlEmp_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim currentRow As GridViewRow = DirectCast(DirectCast(sender, DropDownList).Parent.Parent, GridViewRow)
            Dim ddlEmp As DropDownList = CType(currentRow.FindControl("ddlEmp"), DropDownList)
            Dim lbldept As Label = CType(currentRow.FindControl("lblDept"), Label)
            Dim lblspc As Label = CType(currentRow.FindControl("lblSpc"), Label)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EMP_GRIDDATA")
            If (ddlEmp.SelectedIndex > 0) Then
                sp.Command.AddParameter("@DD_ID", ddlEmp.SelectedValue, DbType.String)
                Dim dr As SqlDataReader = sp.GetReader()
                If (dr.Read()) Then
                    lbldept.Text = dr("DEPARTMENT_NAME").ToString()
                    lblspc.Text = dr("SPACE_ID").ToString()
                    dr.Close()
                End If
            ElseIf (ddlEmp.SelectedValue = "--Select--") Then
                lbldept.Text = ""
                lblspc.Text = ""
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Details", "Load", ex)
        End Try

    End Sub

    Protected Sub gvswap_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvswap.PageIndexChanging
        Try
            gvswap.PageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Protected Sub gvswap_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvswap.RowCommand
        Try
            For i As Integer = 0 To gvswap.Rows.Count
                Dim Emp_1 As String
                Dim emp1 As Array

                Dim Emp_2 As String
                Dim emp2 As Array

                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim lbldata As Label = DirectCast(row.FindControl("EmpId"), Label)
                emp1 = lbldata.Text.Split("/")
                Emp_1 = Trim(emp1(0))

                Dim ddlswapdata As DropDownList = DirectCast(row.FindControl("ddlEmp"), DropDownList)
                emp2 = ddlswapdata.SelectedValue.Split("/")
                Emp_2 = Trim(emp2(0))

                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SWAP_EMPLOYEE1")
                sp.Command.AddParameter("OEMPID", Emp_1, DbType.String)
                sp.Command.AddParameter("@NEMPID", Emp_2, DbType.String)
                sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
                sp.ExecuteScalar()
            Next
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnswap_Click(sender As Object, e As EventArgs) Handles btnswap.Click
        Try
            For Each row As GridViewRow In gvswap.Rows
                Dim Emp_1 As String
                Dim emp1 As Array
                Dim Emp_2 As String
                Dim emp2 As Array
                Dim chkbox As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)

                If chkbox.Checked = True Then
                    Dim lbldata As Label = DirectCast(row.FindControl("EmpId"), Label)
                    emp1 = lbldata.Text.Split("/")
                    Emp_1 = Trim(emp1(0))

                    Dim ddlswapdata As DropDownList = DirectCast(row.FindControl("ddlEmp"), DropDownList)
                    If ddlswapdata.SelectedItem.Text = "--Select--" Then
                        lblMsg.Text = "Please select employee to swap with"
                    Else
                        emp2 = ddlswapdata.SelectedValue.Split
                        Emp_2 = Trim(emp2(0))
                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SWAP_EMPLOYEE1")
                        sp.Command.AddParameter("OEMPID", Emp_1, DbType.String)
                        sp.Command.AddParameter("@NEMPID", Emp_2, DbType.String)
                        sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
                        Dim flag As Integer = sp.ExecuteScalar()
                        If flag = 1 Then
                            lblMsg.Text = "Error Occcured while updating, Please Try Again."
                            Exit For
                        Else
                            lbldata.Text = ddlswapdata.SelectedItem.Text
                            ddlswapdata.SelectedValue = Emp_1
                            lblMsg.Text = "Swapping Done Successfully"
                        End If

                    End If
                End If
            Next
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlZone_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlZone.SelectedIndexChanged
        Try
            ddlState.Items.Clear()
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
            param(0).Value = Session("Uid").ToString().Trim()
            param(1) = New SqlParameter("@CNYCODE", SqlDbType.NVarChar, 100)
            param(1).Value = ddlCountry.SelectedItem.Value
            param(2) = New SqlParameter("@ZNCODE", SqlDbType.NVarChar, 100)
            param(2).Value = ddlZone.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlState, "GET_STATE_BY_ZONE", "STE_NAME", "STE_CODE", param)
            If ddlState.Items.Count = 1 Then
                lblMsg.Text = "No States Available"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try
    End Sub

    Protected Sub ddlState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlState.SelectedIndexChanged
        Try
            ddlCity.Items.Clear()
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Session("Uid").ToString().Trim()
            param(1) = New SqlParameter("@CNYID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlCountry.SelectedItem.Value
            param(2) = New SqlParameter("@ZNID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlZone.SelectedItem.Value
            param(3) = New SqlParameter("@STEID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlState.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlCity, "GET_CITIES_BY_STATEID", "CTY_NAME", "CTY_CODE", param)
            If ddlCity.Items.Count = 1 Then
                lblMsg.Text = "No Cities Available"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try
    End Sub
End Class
