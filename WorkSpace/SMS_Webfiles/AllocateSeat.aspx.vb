
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_GIS_AllocateSeat
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    'Meta tags

    '    '************************
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/tabcontent.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/tabcontent.css")))
    '    'Meta tags
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))

    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    '    '************************


    'End Sub

   


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""

        txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")


        If Not Page.IsPostBack Then

            btnSubmit.Visible = False
            Dim sp(0) As SqlParameter
            sp(0) = New SqlParameter("@VC_SESSION", SqlDbType.NVarChar, 50)
            sp(0).Value = Session("Uid").ToString().Trim()
            Dim intCount As Integer = ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_LA_ROLE_COUNT", sp)

            Dim myList As New System.Collections.ArrayList()
            myList.Add("Group C1")
            myList.Add("Group C2")
            myList.Add("Group D1")
            myList.Add("Group D2")
            myList.Add("Group E")

            If intCount = 0 Then
                Dim k As Integer = myList.IndexOf(ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_GRADE", sp))
                If k = -1 Then
                    PopUpMessage("You are not Authorized to Request for Vertical", Me)
                    btnSubmit.Enabled = False

                End If
            End If
            If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 2 Then
                trTimeSlot.Visible = True
            Else
                trTimeSlot.Visible = False
            End If
            'loadVertical()
            'LoadCity()

            'ObjSubsonic.Binddropdown(ddlspacetype, "GETSPACETYPE_NOTBCP", "SPACE_TYPE", "SNO")
            
            RIDDS = ObjSubsonic.RIDGENARATION("VerticalReq")
         
        End If
    End Sub

    Private Function GETSPACETYPE(ByVal strSpaceId As String) As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETSPACETYPE")
        sp.Command.AddParameter("@SPC_ID", strSpaceId, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Return ds
    End Function

    Public Sub cleardata()
        'ddlSelectLocation.SelectedItem.Value = -1
        'ddlVertical.SelectedItem.Value = -1
        'txtRemarks.Text = String.Empty
    End Sub
 

    Private Sub loadgrid()
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"


        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value = "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value = "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If






        If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 1 Then
            ftime = "00:00"
            ttime = "23:30"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If




        Dim param(10) As SqlParameter

        param(0) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = lblCity.Text
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
        param(2) = New SqlParameter("@frmdate", SqlDbType.DateTime)
        param(2).Value = Convert.ToDateTime(txtFrmDate.Text)
        param(3) = New SqlParameter("@toDate", SqlDbType.DateTime)
        param(3).Value = Convert.ToDateTime(txtToDate.Text)
        param(4) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        param(4).Value = ftime
        param(5) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        param(5).Value = ttime
        param(6) = New SqlParameter("@bcp", SqlDbType.Int)
        param(6).Value = "2"
        param(7) = New SqlParameter("@shared", SqlDbType.NVarChar, 50)
        param(7).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
        param(8) = New SqlParameter("@vertical", SqlDbType.NVarChar, 50)
        param(8).Value = lblAUR_VERT_CODE.Text
        param(9) = New SqlParameter("@numberofseatsreq", SqlDbType.Int)
        param(9).Value = 1
        param(10) = New SqlParameter("@space_id", SqlDbType.NVarChar, 200)
        param(10).Value = Request.QueryString("id")
        
        ObjSubSonic.BindGridView(gdavail, "usp_getseatdetails", param)


        If gdavail.Rows.Count > 0 Then
            btnSubmit.Visible = True
        End If
        gdavail.Visible = False
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click




        REQID = ObjSubsonic.REQGENARATION_REQ( "Vertical", "SVR_ID", Session("TENANT") & "." , "SMS_VERTICAL_REQUISITION")

        Dim verticalreqid As String = REQID

        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0

        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""

        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"


        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value = "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value = "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If




        If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 1 Then
            ftime = "00:00"
            ttime = "23:59"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If

        If CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "You cant request for the past month"
            Exit Sub
        ElseIf CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year = getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "You cant request for the past month"
            Exit Sub
        ElseIf CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now) Then
            lblMsg.Text = "Please Select Valid Date From Date Cannot be less than Current Date"
            Exit Sub
        ElseIf CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
            lblMsg.Text = "Selected To Date Cannot be less than From Date"
            Exit Sub
        End If

        If CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now) Or CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
            lblMsg.Text = "Please Select Valid Date From Date Cannot be less than Current Date/Todate Cannot be less than From Date"
            Exit Sub
        End If

        Dim sta As Integer = 7



        RIDDS = RIDGENARATION("VerticalReq")

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubsonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblMsg.Text = "Request is already raised "
            Exit Sub
        Else





            For Each row As GridViewRow In gdavail.Rows
                Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
                Dim lblSpaceType As Label = DirectCast(row.FindControl("lblSpaceType"), Label)
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then
                  

                    If lblSpaceType.Text = "WS" Then
                        cntWst += 1

                    End If
                    If lblSpaceType.Text = "HC" Then
                        cntHCB += 1

                    End If
                    If lblSpaceType.Text = "FC" Then
                        cntFCB += 1
                    End If

                End If

            Next








            Dim param2(14) As SqlParameter

            param2(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param2(0).Value = REQID
            param2(1) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param2(1).Value = "Vertical"
            param2(2) = New SqlParameter("@WSTREQCOUNT", SqlDbType.Int)
            param2(2).Value = cntWst
            param2(3) = New SqlParameter("@FCREQCOUNT", SqlDbType.Int)
            param2(3).Value = cntFCB
            param2(4) = New SqlParameter("@HCREQCOUNT", SqlDbType.Int)
            param2(4).Value = cntHCB
            param2(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param2(5).Value = txtFrmDate.Text
            param2(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param2(6).Value = txtToDate.Text
            param2(7) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param2(7).Value = ftime
            param2(8) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param2(8).Value = ttime
            param2(9) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
            param2(9).Value = txtEmployeeCode.Text
            param2(10) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 50)
            param2(10).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
            param2(11) = New SqlParameter("@CITY", SqlDbType.NVarChar, 50)
            param2(11).Value = lblCity.Text
            param2(12) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param2(12).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param2(13) = New SqlParameter("@BCPTYPE", SqlDbType.NVarChar, 50)
            param2(13).Value = "2"
            param2(14) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param2(14).Value = sta
            ObjSubsonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART1", param2)
            Dim cnt As Int32 = 0
            For Each row As GridViewRow In gdavail.Rows
                Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)

                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then
                    verticalreqid = ObjSubSonic.REQGENARATION_REQ(lblAUR_VERT_CODE.Text, "ssa_id", Session("TENANT") & "." , "SMS_space_allocation")

                    Dim param3(10) As SqlParameter

                    param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
                    param3(0).Value = verticalreqid
                    param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
                    param3(1).Value = REQID
                    param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
                    param3(2).Value = lblAUR_VERT_CODE.Text
                    
                    param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
                    param3(3).Value = lblspcid.Text
                    param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
                    param3(4).Value = txtEmployeeCode.Text
                    param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
                    param3(5).Value = txtFrmDate.Text
                    param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
                    param3(6).Value = txtToDate.Text
                    param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
                    param3(7).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
                    param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
                    param3(8).Value = ftime
                    param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
                    param3(9).Value = ttime
                    param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
                    param3(10).Value = sta
                    ObjSubsonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART2", param3)
                End If
            Next
        End If


        Addusers.Allocatespace(Trim(txtEmployeeCode.Text), LTrim(RTrim(Request.QueryString("ID"))))
        UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), sta, Trim(txtEmployeeCode.Text) & "/" & lblDepartment.Text)


       ' strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("6") & "&rid=" & clsSecurity.Encrypt(REQID)
GVColor:


        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If




    End Sub




    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function


    'Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
    '    param(0).Value = ddlCity.SelectedItem.Value
    '    ObjSubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    'End Sub


    'Private Sub CheckSpaceType()
    '    '        loadgrid()
    '    If gdavail.Rows.Count > 0 Then
    '        btnSubmit.Visible = True
    '    End If
    'End Sub

   

    Protected Sub gdavail_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gdavail.PageIndexChanging
        gdavail.PageIndex = e.NewPageIndex
        loadgrid()
    End Sub

    Protected Sub btnallocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnallocate.Click
        Dim StrSearchQry As String = ""
        If String.IsNullOrEmpty(txtEmployeeCode.Text) = False Then
            lblMsg.Text = ""
            StrSearchQry = txtEmployeeCode.Text
            empdetails.Visible = True
            GetEmployeeSearchDetails(StrSearchQry)
            loadgrid()
        Else
            lblMsg.Text = "Please enter employee code to search."
        End If
    End Sub
    Private Sub GetEmployeeSearchDetails(ByVal EmpCode As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = EmpCode
        Dim dsEmpDetails As New DataSet
        dsEmpDetails = objsubsonic.GetSubSonicDataSet("GETEMPLOYEEDETAILS", param)
        If dsEmpDetails.Tables(0).Rows.Count > 0 Then
            lblEmpSearchId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_ID")
            lblEmpName.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
            lblEmpEmailId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_EMAIL")
            lblReportingTo.Text = dsEmpDetails.Tables(0).Rows(0).Item("REPORTTO")
            lblAUR_BDG_ID.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_BDG_ID")
            lblAUR_VERT_CODE.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_VERT_CODE")
            lblCity.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_CITY")
            lblDepartment.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DEP_ID")
            lblDesignation.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            lblEmpExt.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_EXTENSION")
            lblEmpFloor.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_FLOOR")
            lblPrjCode.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_PRJ_CODE")
            lblResNumber.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            lblState.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_STATE")
            lblEmpTower.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_TOWER")
            lblDoj.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DOJ")
            hypSpaceId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_LAST_NAME")
            If hypSpaceId.Text = "" Then
                lblSpaceAllocated.Text = "Space not allocated to " & lblEmpName.Text
                hypSpaceId.Text = ""
                hypSpaceId.Visible = False

            Else
                lblSpaceAllocated.Text = "Space ID "
                hypSpaceId.Visible = True

            End If
        Else
            lblmsg.Text = "Employee not found."
            empdetails.Visible = False
        End If

    End Sub
End Class



