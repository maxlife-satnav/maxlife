Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmViewAllocatedRequisition
    Inherits System.Web.UI.Page

    Dim dt As DataSet
    Dim dv As DetailsView
    Dim sp As SqlParameter
    Dim dtVerReqDetails As DataTable
    Dim strRedirect As String = String.Empty
    Dim i As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = "View Allocated Requisition"
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If
        If (Not Page.IsPostBack) Then
            Try
                GetData()
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
            Finally
            End Try
        End If
    End Sub

    Protected Sub GetData()

        Dim sp As SqlParameter = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
        sp.Value = Session("Uid").ToString()
        dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "" & Session("TENANT") & "."  & "USP_VERTICAL_REQ_ALLOCATEPARTIAL", sp)



        Dim intWsCount As Integer
        Dim intHCCount As Integer
        Dim intFCCount As Integer
        For i As Integer = 0 To dvVerticalReq.Rows.Count - 1
            Dim lnkVerReq As LinkButton = CType(dvVerticalReq.Rows(i).FindControl("lnkVerReq"), LinkButton)
            strSQL = "SELECT count(*) FROM " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE SSA_SRNREQ_ID = '" & lnkVerReq.Text.ToString().Trim() & "' AND SSA_SPC_TYPE = 'WORK STATION' and SSA_STA_ID='167'"
            intWsCount = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)

            strSQL = "SELECT count(*) FROM " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE SSA_SRNREQ_ID = '" & lnkVerReq.Text.ToString().Trim() & "' AND SSA_SPC_TYPE = 'HALF CABIN' and SSA_STA_ID='167'"
            intHCCount = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)

            strSQL = "SELECT count(*) FROM " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION WHERE SSA_SRNREQ_ID = '" & lnkVerReq.Text.ToString().Trim() & "' AND SSA_SPC_TYPE = 'FULL CABIN' and SSA_STA_ID='167'"
            intFCCount = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)

            If intWsCount = 0 And intHCCount = 0 And intFCCount = 0 Then

                lnkVerReq.Enabled = False
                dvVerticalReq.Rows(i).Visible = False
            End If

        Next
        dvVerticalReq.DataSource = dt
        dvVerticalReq.DataBind()
        dvVerticalReq.Columns(7).Visible = False
        dvVerticalReq.Columns(8).Visible = False
        If dvVerticalReq.Rows.Count = 0 Then
            lblVertical.Text = "No Requisitions are available"
            lblVertical.Visible = True
            dvVerticalReq.Visible = False
        Else
            lblVertical.Text = ""
            dvVerticalReq.Visible = True
        End If
        dt.Clear()
    End Sub


    Protected Sub dvVerticalReq_OnRowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='White';")
        End If
    End Sub
    Protected Sub lnk_view(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnkSender As LinkButton = sender
            For i As Int16 = 0 To dvVerticalReq.Rows.Count - 1
                Dim lnk As LinkButton = CType(dvVerticalReq.Rows(i).FindControl("lnkVerReq"), LinkButton)
                If lnk.ClientID = lnkSender.ClientID Then
                    Dim lblStatus As Label = CType(dvVerticalReq.Rows(i).FindControl("lblStatusID"), Label)
                    Dim lblMon As Label = CType(dvVerticalReq.Rows(i).FindControl("lblMon"), Label)
                    Dim lblYear As Label = CType(dvVerticalReq.Rows(i).FindControl("lblYear"), Label)

                    strRedirect = "frmViewAllocatedReqDetails.aspx?ReqID=" & lnk.Text.Trim() & "&Month=" & lblMon.Text.Trim() & "&Year=" & lblYear.Text.Trim() & "&StaID=" & lblStatus.Text.Trim()
                End If
            Next

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If

    End Sub
End Class
