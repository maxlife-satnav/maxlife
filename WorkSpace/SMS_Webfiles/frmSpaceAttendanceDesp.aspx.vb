﻿#Region " Document Details "
'**************************************************************************************
'Class Name                 : frmSmsViewMap.aspx.vb
'Purpose                    : Using this page, User can View the map
'Developer                  : Phanindra
'Creation Date              : 27th August 2008
'Last Revised               : 27th August 2008
'**************************************************************************************
'Revision History
'Modified By             Date Modified          Reason for Modification
'**************************************************************************************
#End Region

Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.IO
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_frmSpaceAttendanceDesp
    Inherits System.Web.UI.Page
    Dim obj As New clsWorkSpace
    Dim objMsater As New clsMasters

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("Logout"))
        End If
        If Not Page.IsPostBack Then
            LoadCity()
            ddlCity.ClearSelection()
            ddlLocation.Items.Add("--Select--")
            'objMsater.Bindlocation(ddlLocation)
            ddlTower.Items.Add("--Select--")
            ddlFloor.Items.Add("--Select--")
            btnExport.Visible = False
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex <> -1 And ddlLocation.SelectedIndex <> 0 Then
                objMsater.BindTowerLoc(ddlTower, ddlLocation.SelectedItem.Value)
            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub
    Dim verBll As New VerticalBLL()
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try

            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub
    Public Sub cleardata()
        ddlTower.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0

    End Sub


    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        'If Page.IsValid Then

        '    Dim BDG_ID As String = String.Empty
        '    Dim FLR_CODE As String = String.Empty


        '    'Dim spFlrID As New SqlParameter("@VCFLR_CODE", SqlDbType.NVarChar, 50)
        '    'Dim spTwrID As New SqlParameter("@VCTWR_CODE", SqlDbType.NVarChar, 50)
        '    'Dim spLocID As New SqlParameter("@VCBDG_CODE", SqlDbType.NVarChar, 50)
        '    'spFlrID.Value = ddlFloor.SelectedItem.Value
        '    'spTwrID.Value = ddlTower.SelectedValue
        '    'spLocID.Value = ddlLocation.SelectedValue

        '    'ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, Session("TENANT") & "."  & "USP_GET_FLOOR_BDGCODE", spFlrID, spTwrID, spLocID)

        '    'If ObjDR.Read Then
        '    '    BDG_ID = ObjDR("FLR_BDG_ID").ToString
        '    '    FLR_CODE = ObjDR("FLR_Code").ToString
        '    'End If
        If ddlStatus.SelectedIndex <> 0 Then
            gvCat.Visible = True
            bindgrid()
            btnExport.Visible = True
        Else
            gvCat.Visible = False
            btnExport.Visible = False
        End If
    End Sub
    Private Sub bindgrid()
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SPC_BDG_ID", SqlDbType.NVarChar, 50)
        param(0).Value = ddlLocation.SelectedItem.Value
        param(1) = New SqlParameter("@SPC_TWR_ID", SqlDbType.NVarChar, 50)
        param(1).Value = ddlTower.SelectedItem.Value
        param(2) = New SqlParameter("@SPC_FLR_ID", SqlDbType.NVarChar, 50)
        param(2).Value = ddlFloor.SelectedItem.Value
        param(3) = New SqlParameter("@AD_ID", SqlDbType.NVarChar, 50)
        param(3).Value = ddlStatus.SelectedItem.Value
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_SPACE_ATNDNCE", param)
        If ds.Tables(0).Rows.Count > 0 Then
            gvCat.DataSource = ds
            gvCat.DataBind()
        End If
        'For i As Integer = 0 To gvCat.Rows.Count - 1
        '    Dim lblstatus As Label = CType(gvCat.Rows(i).FindControl("lblstatus"), Label)
        '    Dim lblSpcId As Label = CType(gvCat.Rows(i).FindControl("lblSpcId"), Label)
        '    If lblstatus.Text = "" And lblSpcId.Text = "" Then
        '        lblstatus.Text = "Not Swipped and Not Allocated"
        '    ElseIf lblstatus.Text <> "" And lblSpcId.Text = "" Then
        '        lblstatus.Text = "Allocated but not swipped"
        '    ElseIf lblstatus.Text = "" And lblSpcId.Text <> "" Then
        '        lblstatus.Text = "Swipped but not Allocated"
        '    ElseIf lblstatus.Text <> "" And lblSpcId.Text <> "" Then
        '        lblstatus.Text = "Allocated and Swipped "
        '    End If
        'Next
    End Sub
    Public Sub LoadCity()
        objMsater.BindVerticalCity(ddlCity)
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            If Session("uid") = "" Then
                Response.Redirect(AppSettings("logout"))
            End If
            strSQL = "select lcm_code,LCM_name +'('+ Lcm_code +')' LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
            BindCombo(strSQL, ddlLocation, "LCM_name", "lcm_code")
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvCat_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SPC_BDG_ID", SqlDbType.NVarChar, 50)
        param(0).Value = ddlLocation.SelectedItem.Value
        param(1) = New SqlParameter("@SPC_TWR_ID", SqlDbType.NVarChar, 50)
        param(1).Value = ddlTower.SelectedItem.Value
        param(2) = New SqlParameter("@SPC_FLR_ID", SqlDbType.NVarChar, 50)
        param(2).Value = ddlFloor.SelectedItem.Value
        param(3) = New SqlParameter("@AD_ID", SqlDbType.NVarChar, 50)
        param(3).Value = ddlStatus.SelectedItem.Value
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_SPACE_ATNDNCE", param)
        Dim gv As New GridView
        If ds.Tables(0).Rows.Count > 0 Then

            gv.DataSource = ds
            gv.DataBind()
        End If
        Export("SpaceAttendanceDiscrepancy.xls", gv)
    End Sub
End Class
