
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports System.Web.Security
Partial Class WorkSpace_SMS_Webfiles_AllocateSeatToEmployeeVertical
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Meta tags

        '************************
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/tabcontent.js")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/tabcontent.css")))
        ''Meta tags
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


        '' Styles
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))

        '' Styles
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
        'Me.Button2.Attributes.Add("onclick", "JavaScript:window.close(); return false;")
        '************************


    End Sub




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblSelVertical.Text = Session("Parent")
        If Session("uid") = "" Then

            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""
        RequiredFieldValidator1.ErrorMessage = "Please Select " + Session("Parent")

        'txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        'txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'Me.Button2.Attributes.Add("OnClick", "self.close()")
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")

        If Not Page.IsPostBack Then

            btnSubmit.Visible = False
            Dim sp(0) As SqlParameter
            sp(0) = New SqlParameter("@VC_SESSION", SqlDbType.NVarChar, 50)
            sp(0).Value = Session("Uid").ToString().Trim()
            Dim intCount As Integer = ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_LA_ROLE_COUNT", sp)

            Dim myList As New System.Collections.ArrayList()
            myList.Add("Group C1")
            myList.Add("Group C2")
            myList.Add("Group D1")
            myList.Add("Group D2")
            myList.Add("Group E")

            If intCount = 0 Then
                Dim k As Integer = myList.IndexOf(ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_GRADE", sp))
                If k = -1 Then
                    PopUpMessage("You are not Authorized to Request for Vertical", Me)
                    btnSubmit.Enabled = False

                End If
            End If
            If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 2 Then
                trTimeSlot.Visible = True
                trshift.Visible = True
            Else
                trTimeSlot.Visible = False
                trshift.Visible = False

            End If
            loadVertical()
            SharedEmployee()
            loadshifts()
            'ObjSubsonic.Binddropdown(ddlspacetype, "GETSPACETYPE_NOTBCP", "SPACE_TYPE", "SNO")

            RIDDS = ObjSubsonic.RIDGENARATION("VerticalReq")

        End If
    End Sub

    Protected Sub gvSharedEmployee_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSharedEmployee.RowCommand
        If e.CommandName = "SpaceRelease" Then
            Dim lblAUR_ID As String = (e.CommandArgument.ToString())
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@VC_SPACEID", SqlDbType.NVarChar, 200)
            param(0).Value = LTrim(RTrim(Request.QueryString("ID")))
            param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
            param(1).Value = lblAUR_ID
            ObjSubSonic.GetSubSonicExecute("VACANTSEAT_BYEMPID", param)
            Dim spcstatus As Integer = 1
            If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 1 Then
                If GETALLOCATIONCOUNT() = 0 Then
                    spcstatus = 1
                End If
            ElseIf GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 2 Then
                If GETALLOCATIONCOUNT() > 0 Then
                    Dim param4(0) As SqlParameter
                    param4(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
                    param4(0).Value = LTrim(RTrim(Request.QueryString("ID")))
                    Dim count As Integer = 0
                    count = ObjSubSonic.GetSubSonicExecuteScalar("Shift_SeatRequsition", param4)
                    'If count = 1 Then
                    '    spcstatus = 11 ' completely allocated
                    'Else
                    '    spcstatus = 10 ' partially allocated
                    'End If
                    If count = 1 Then
                        spcstatus = 11  ' completely allocated
                    ElseIf count = -1 Then
                        spcstatus = 1 'vacant
                    Else
                        spcstatus = 10 ' partially allocated
                    End If
                Else
                    spcstatus = 1
                End If
            End If
            Dim empcode As String
            Dim a1
            a1 = txtEmpName.Text.Split(" ")
            empcode = a1(0).ToString()
            UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), spcstatus, Trim(empcode) & "/" & lblDepartment.Text)

        Response.Redirect("AllocateSeatToEmployeeVertical.aspx?id=" & Request.QueryString("id"))
        End If
    End Sub


    Private Function GETALLOCATIONCOUNT() As Integer
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SPC_ID", SqlDbType.NVarChar, 200)
        param(0).Value = LTrim(RTrim(Request.QueryString("ID")))
        Dim dsEmpDetails As New DataSet
        dsEmpDetails = objsubsonic.GetSubSonicDataSet("GETALLOCATIONCOUNT", param)
        Dim space_cnt As Integer = 0

        If dsEmpDetails.Tables(0).Rows.Count > 0 Then
            space_cnt = dsEmpDetails.Tables(0).Rows(0).Item("cnt")
        End If
        Return space_cnt
    End Function

    Protected Function PreventUnlistedValueError(li As DropDownList, val As String) As String
        If li.Items.FindByValue(val) Is Nothing Then
            'option 1: add the value to the list and display
            Dim arr(3) As String

            arr = val.Split("/")
            If arr.Length > 0 Then

                Dim lit As New ListItem()
                lit.Text = arr(0)
                lit.Value = val
                'option 2: set a default e.g.
                'val="";
                'val = "--Select--"
                li.Items.Insert(li.Items.Count, lit)
            End If

        End If
        Return val
    End Function

    Private Sub SharedEmployee()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SPC_ID", SqlDbType.VarChar, 50)
        param(0).Value = LTrim(RTrim(Request.QueryString("ID")))

        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GetSharedSeats", param)
        gvSharedEmployee.DataSource = ds
        gvSharedEmployee.DataBind()

        If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 1 Then

            If gvSharedEmployee.Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("SSA_STA_ID").ToString = "7" Then
                    lblSeatStatus.Text = "(" & Request.QueryString("id") & ") Occupied status."
                ElseIf ds.Tables(0).Rows(0).Item("SSA_STA_ID").ToString = "6" Then
                    lblSeatStatus.Text = "(" & Request.QueryString("id") & ")  Allocated status."
                    gvSharedEmployee.Visible = False

                End If

                PNLCONTAINER.Visible = False
            End If
        ElseIf GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 2 Then
            If gvSharedEmployee.Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("SSA_STA_ID").ToString = "7" Then
                    lblSeatStatus.Text = "(" & Request.QueryString("id") & ") Occupied status."
                ElseIf ds.Tables(0).Rows(0).Item("SSA_STA_ID").ToString = "6" Then
                    lblSeatStatus.Text = "(" & Request.QueryString("id") & ")  Allocated status."
                    gvSharedEmployee.Visible = False

                End If

                'PNLCONTAINER.Visible = False
            End If
        Else

        End If


      '  Dim ds1 As New DataSet
      '  ds1 = objsubsonic.GetSubSonicDataSet("GetSharedSeats_allocated", param)
      '  gvAllocatedSeats.DataSource = ds1
      '  gvAllocatedSeats.DataBind()
        BindSharedSeats()
    End Sub

    Private Sub BindSharedSeats()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SPC_ID", SqlDbType.VarChar, 50)
        param(0).Value = LTrim(RTrim(Request.QueryString("ID")))
        Dim ds1 As New DataSet
        ds1 = objsubsonic.GetSubSonicDataSet("GetSharedSeats_allocated", param)
        gvAllocatedSeats.DataSource = ds1
        gvAllocatedSeats.DataBind()
    End Sub

    Public Sub loadVertical()
        ObjSubSonic.Binddropdown(ddlVertical, "GET_ALL_VERTICALS", "Ver_Name", "Ver_Code")
    End Sub

    Private Function GETSPACETYPE(ByVal strSpaceId As String) As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETSPACETYPE")
        sp.Command.AddParameter("@SPC_ID", strSpaceId, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Return ds
    End Function

    Public Sub cleardata()
        'ddlSelectLocation.SelectedItem.Value = -1
        'ddlVertical.SelectedItem.Value = -1
        'txtRemarks.Text = String.Empty
    End Sub


    Private Sub loadgrid()
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"


        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If






        If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 1 Then
            ftime = "00:00"
            ttime = "23:30"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If




        Dim param(10) As SqlParameter

        param(0) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = lblCity.Text
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
        param(2) = New SqlParameter("@frmdate", SqlDbType.DateTime)
        param(2).Value = Convert.ToDateTime(txtFrmDate.Text)
        param(3) = New SqlParameter("@toDate", SqlDbType.DateTime)
        param(3).Value = Convert.ToDateTime(txtToDate.Text)
        param(4) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        param(4).Value = ftime
        param(5) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        param(5).Value = ttime
        param(6) = New SqlParameter("@bcp", SqlDbType.Int)
        param(6).Value = "2"
        param(7) = New SqlParameter("@shared", SqlDbType.NVarChar, 50)
        param(7).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
        param(8) = New SqlParameter("@vertical", SqlDbType.NVarChar, 50)
        param(8).Value = lblAUR_VERT_CODE.Text
        param(9) = New SqlParameter("@numberofseatsreq", SqlDbType.Int)
        param(9).Value = 1
        param(10) = New SqlParameter("@space_id", SqlDbType.NVarChar, 200)
        param(10).Value = Request.QueryString("id")

        ObjSubSonic.BindGridView(gdavail, "getSeattoAllocate_Occupied", param)


        If gdavail.Rows.Count > 0 Then
            btnSubmit.Visible = True
            lblMsg.Text = ""
        Else

            lblMsg.Text = " Time slot not available. "

        End If
        gdavail.Visible = False
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        VacantEmpToAllocateNewSeat()
        Dim empcode As String
        Dim a1
        a1 = txtEmpName.Text.Split(" ")
        empcode = a1(0).ToString()
        SubmitAllocationOccupied(lblAUR_VERT_CODE.Text, empcode, 7)
        If lblMsg.Text = "" Then
            Response.Redirect("AllocateSeatToEmployeeVertical.aspx?id=" & Request.QueryString("id"))
        End If
    End Sub


    Private Sub VacantEmpToAllocateNewSeat()
        Dim empcode As String
        Dim a1
        a1 = txtEmpName.Text.Split(" ")
        empcode = a1(0).ToString()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = empcode
        ObjSubSonic.GetSubSonicExecute("VACANTSEAT_BYEMPIDExisting", param)
        UpdateRecord(LTrim(RTrim(hypSpaceId.Text)), 1, Trim(empcode) & "/" & lblDepartment.Text)
    End Sub


    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)
        loadgrid()
        REQID = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "SVR_ID", Session("TENANT") & "." , "SMS_VERTICAL_REQUISITION")
        Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If




        If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 1 Then
            ftime = "00:00"
            ttime = "23:59"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If

        If CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "You cant request for the past month"
            Exit Sub
        ElseIf CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year = getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "You cant request for the past month"
            Exit Sub
        ElseIf CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now).Date Then
            lblMsg.Text = "Please Select Valid Date From Date Cannot be less than Current Date"
            Exit Sub
        ElseIf CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
            lblMsg.Text = "Selected To Date Cannot be less than From Date"
            Exit Sub
        End If

        If CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now).Date Or CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
            lblMsg.Text = "Please Select Valid Date From Date Cannot be less than Current Date/Todate Cannot be less than From Date"
            Exit Sub
        End If

        Dim sta As Integer = status



        RIDDS = RIDGENARATION("VerticalReq")

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubSonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblMsg.Text = "Request is already raised "
            Exit Sub
        Else

            For Each row As GridViewRow In gdavail.Rows
                Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
                Dim lblSpaceType As Label = DirectCast(row.FindControl("lblSpaceType"), Label)
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then

                    If lblSpaceType.Text = "WORK STATION" Then
                        cntWst += 1
                    End If
                    If lblSpaceType.Text = "HALF CABIN" Then
                        cntHCB += 1
                    End If
                    If lblSpaceType.Text = "FULL CABIN" Then
                        cntFCB += 1
                    End If
                   
                End If
            Next

            Dim param2(16) As SqlParameter

            param2(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param2(0).Value = REQID
            param2(1) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param2(1).Value = strVerticalCode
            param2(2) = New SqlParameter("@WSTREQCOUNT", SqlDbType.Int)
            param2(2).Value = cntWst
            param2(3) = New SqlParameter("@FCREQCOUNT", SqlDbType.Int)
            param2(3).Value = cntFCB
            param2(4) = New SqlParameter("@HCREQCOUNT", SqlDbType.Int)
            param2(4).Value = cntHCB
            param2(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param2(5).Value = txtFrmDate.Text
            param2(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param2(6).Value = txtToDate.Text
            param2(7) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param2(7).Value = ftime
            param2(8) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param2(8).Value = ttime
            param2(9) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
            param2(9).Value = strAurId
            param2(10) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 50)
            param2(10).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
            param2(11) = New SqlParameter("@CITY", SqlDbType.NVarChar, 50)
            param2(11).Value = lblCity.Text
            param2(12) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param2(12).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param2(13) = New SqlParameter("@BCPTYPE", SqlDbType.NVarChar, 50)
            param2(13).Value = "2"
            param2(14) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param2(14).Value = sta
			param2(15) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
            param2(15).Value = Request.QueryString("id")
            If sta = "7" Then
                param2(16) = New SqlParameter("@costcenter", SqlDbType.NVarChar, 200)
                param2(16).Value = lblPrjCode.Text
            Else
                param2(16) = New SqlParameter("@costcenter", SqlDbType.NVarChar, 200)
                param2(16).Value = ""
            End If
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART1_updated", param2)
            Dim cnt As Int32 = 0
            For Each row As GridViewRow In gdavail.Rows
                Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)

                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then
                    verticalreqid = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "ssa_id", Session("TENANT") & "." , "SMS_space_allocation")

                    Dim param3(12) As SqlParameter

                    param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
                    param3(0).Value = verticalreqid
                    param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
                    param3(1).Value = REQID
                    param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
                    param3(2).Value = strVerticalCode

                    param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
                    param3(3).Value = lblspcid.Text
                    param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
                    param3(4).Value = Session("Uid")
                    param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
                    param3(5).Value = txtFrmDate.Text
                    param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
                    param3(6).Value = txtToDate.Text
                    param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
                    param3(7).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
                    param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
                    param3(8).Value = ftime
                    param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
                    param3(9).Value = ttime
                    param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
                    param3(10).Value = sta
                    param3(11) = New SqlParameter("@shiftid", SqlDbType.NVarChar, 200)
                    param3(11).Value = ddlshift.SelectedItem.Value

                    If sta = "7" Then
                        param3(12) = New SqlParameter("@costcenter", SqlDbType.NVarChar, 200)
                        param3(12).Value = lblPrjCode.Text
                    Else
                        param3(12) = New SqlParameter("@costcenter", SqlDbType.NVarChar, 200)
                        param3(12).Value = ""
                    End If

                    ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART2", param3)
                    If sta = "7" Then
                        Addusers.Occupiedspace(Trim(strAurId), LTrim(RTrim(Request.QueryString("ID"))))
                        UpdateSpace_EmployeeDetails(verticalreqid, strAurId, lblspcid.Text)
                    Else
                        UpdateSpace_EmployeeDetails(verticalreqid, strAurId, lblspcid.Text)
                    End If

                    If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") <> 2 Then
                        UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), sta, Trim(strAurId) & "/" & lblDepartment.Text)
                    Else
                        Dim param4(0) As SqlParameter
                        param4(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
                        param4(0).Value = lblspcid.Text
                        Dim count As Integer = 0
                        count = ObjSubSonic.GetSubSonicExecuteScalar("Shift_SeatRequsition", param4)
                        If count = 1 Then
                            UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), 11, Trim(strAurId) & "/" & lblDepartment.Text) ' completely allocated
                        Else
                            UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), 10, Trim(strAurId) & "/" & lblDepartment.Text) ' partially allocated
                        End If
                    End If

                End If
            Next
            End If





            ' strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("6") & "&rid=" & clsSecurity.Encrypt(REQID)
GVColor:


            If strRedirect <> String.Empty Then
                Response.Redirect(strRedirect)
            End If


    End Sub

    Private Sub UpdateSpace_EmployeeDetails(ByVal REQ_ID As String, ByVal AUR_ID As String, ByVal Space_id As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@strReqID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ_ID
        param(1) = New SqlParameter("@intEmpID", SqlDbType.NVarChar, 200)
        param(1).Value = AUR_ID
        param(2) = New SqlParameter("@strSpaceID", SqlDbType.NVarChar, 200)
        param(2).Value = Space_id

        ObjSubSonic.GetSubSonicExecute("UPDATE_SPACE_EmployeebyMap", param)




    End Sub
    'Private Function checkspacetype(space_type As String) As String
    '    Dim sta As String = ""
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECKSPACETYPE")
    '    sp.Command.AddParameter("@SPACE_TYPE", space_type)
    '    sta = sp.ExecuteScalar()
    '    Return sta
    'End Function



    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function


    'Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
    '    param(0).Value = ddlCity.SelectedItem.Value
    '    ObjSubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    'End Sub


    'Private Sub CheckSpaceType()
    '    '        loadgrid()
    '    If gdavail.Rows.Count > 0 Then
    '        btnSubmit.Visible = True
    '    End If
    'End Sub



    Protected Sub gdavail_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles gdavail.PageIndexChanging
        gdavail.PageIndex = e.NewPageIndex
        loadgrid()
    End Sub

    Protected Sub btnallocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnallocate.Click
        Dim StrSearchQry As String = ""


        If String.IsNullOrEmpty(txtEmpName.Text) = False Then
            lblMsg.Text = ""
            Dim a1
            a1 = txtEmpName.Text.Split(" ")
            StrSearchQry = a1(0).ToString() 'txtEmpName.Text
            empdetails.Visible = True
            lblSelProject.Text = Session("Child")
            lblSelVertical1.Text = Session("Parent")
            GetEmployeeSearchDetails(StrSearchQry)

            loadgrid()
        Else
            lblMsg.Text = "Please select employee to search."
        End If
    End Sub
    Private Sub GetEmployeeSearchDetails(ByVal EmpCode As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = EmpCode
        Dim dsEmpDetails As New DataSet
        dsEmpDetails = objsubsonic.GetSubSonicDataSet("GETEMPLOYEEDETAILS", param)
        If dsEmpDetails.Tables(0).Rows.Count > 0 Then
            lblEmpSearchId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_ID")
            lblEmpName.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
            lblEmpEmailId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_EMAIL")
            lblReportingTo.Text = dsEmpDetails.Tables(0).Rows(0).Item("REPORTTO")
            lblAUR_BDG_ID.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_BDG_ID")
            lblAUR_VERT_CODE.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_VERT_CODE")
            lblCity.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_CITY")
            lblDepartment.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DEP_ID")
            lblDesignation.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            lblEmpExt.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_EXTENSION")
            lblEmpFloor.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_FLOOR")
            lblPrjCode.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_PRJ_CODE")
            lblResNumber.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            lblState.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_STATE")
            lblEmpTower.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_TOWER")
            lblDoj.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_DOJ")
            hypSpaceId.Text = dsEmpDetails.Tables(0).Rows(0).Item("AUR_LAST_NAME")
            ddlVertical.ClearSelection()
            ddlVertical.SelectedValue = PreventUnlistedValueError(ddlVertical, lblAUR_VERT_CODE.Text)
            ' ddlVertical.Items.FindByValue(lblAUR_VERT_CODE.Text).Selected = True
            ' ddlVertical.SelectedItem(lblAUR_VERT_CODE.Text).Selected = 
            If hypSpaceId.Text = "" Then
                lblSpaceAllocated.Text = "Space not allocated to " & lblEmpName.Text
                hypSpaceId.Text = ""
                hypSpaceId.Visible = False

            Else
                lblSpaceAllocated.Text = "Space ID "
                hypSpaceId.Visible = True

            End If
        Else
            lblmsg.Text = "Employee not found."
            empdetails.Visible = False
        End If

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ddlVertical.SelectedItem.Text <> "--Select--" Then

            loadgrid()
            If gdavail.Rows.Count > 0 Then

                lblMsg.Text = ""
                SubmitAllocationOccupied(ddlVertical.SelectedItem.Value, Session("uid"), 6)
                If lblMsg.Text = "" Then
                    Response.Redirect("AllocateSeatToEmployeeVertical.aspx?id=" & Request.QueryString("id"))
                Else
                    Exit Sub
                End If

            Else

                lblMsg.Text = " Time slot not available. "

            End If
            SharedEmployee()


        Else
            lblMsg.Visible = True
            lblMsg.Text = "Please select " + Session("Parent")

        End If
    End Sub

	
    Protected Sub gvAllocatedSeats_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAllocatedSeats.RowCommand

        If e.CommandName = "Release" Then
            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            'Dim lblsno As Label = DirectCast(row.FindControl("lblsno"), Label)
            Dim lblSSA_VERTICAL As Label = CType(gvAllocatedSeats.Rows(row.RowIndex).FindControl("lblSSA_VERTICAL"), Label)
            Dim lblspaceID As Label = CType(gvAllocatedSeats.Rows(row.RowIndex).FindControl("lblspaceID"), Label)
            Dim lblSSA_FROM_DATE As Label = CType(gvAllocatedSeats.Rows(row.RowIndex).FindControl("lblSSA_FROM_DATE"), Label)
            Dim lblSSA_TO_DT As Label = CType(gvAllocatedSeats.Rows(row.RowIndex).FindControl("lblSSA_TO_DT"), Label)
            Dim lblFROM_TIME As Label = CType(gvAllocatedSeats.Rows(row.RowIndex).FindControl("lblFROM_TIME"), Label)
            Dim lblTO_TIME As Label = CType(gvAllocatedSeats.Rows(row.RowIndex).FindControl("lblTO_TIME"), Label)
            Dim ds As New DataSet

            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param(0).Value = lblSSA_VERTICAL.Text
            param(1) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
            param(1).Value = lblspaceID.Text
            param(2) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param(2).Value = lblSSA_FROM_DATE.Text
            param(3) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param(3).Value = lblSSA_TO_DT.Text
            param(4) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param(4).Value = lblFROM_TIME.Text
            param(5) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param(5).Value = lblTO_TIME.Text
            'ds = objsubsonic.GetSubSonicDataSet("RELEASE_VERTICAL_SPACEID", param)
            ObjSubSonic.GetSubSonicExecute("RELEASE_VERTICAL_SPACEID", param)

            BindSharedSeats()
            Dim spcstatus As Integer
            If GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 1 Then
                If GETALLOCATIONCOUNT() = 0 Then
                    spcstatus = 1
                End If
            ElseIf GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE") = 2 Then
                If GETALLOCATIONCOUNT() > 0 Then
                    Dim param4(0) As SqlParameter
                    param4(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
                    param4(0).Value = LTrim(RTrim(Request.QueryString("ID")))
                    Dim count As Integer = 0
                    count = ObjSubSonic.GetSubSonicExecuteScalar("Shift_SeatRequsition", param4)
                    'If count = 1 Then
                    '    spcstatus = 11 ' completely allocated
                    'Else
                    '    spcstatus = 10 ' partially allocated
                    'End If
                    If count = 1 Then
                        spcstatus = 11  ' completely allocated
                    ElseIf count = -1 Then
                        spcstatus = 1 'vacant
                    Else
                        spcstatus = 10 ' partially allocated
                    End If
                Else
                    spcstatus = 1
                End If
            End If
            Dim empcode As String
            Dim a1
            a1 = txtEmpName.Text.Split(" ")
            empcode = a1(0).ToString()
            UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), spcstatus, Trim(empcode) & "/" & lblDepartment.Text)





            'UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), 1, LTrim(RTrim(Request.QueryString("ID"))))

            Response.Redirect("AllocateSeatToEmployeeVertical.aspx?id=" & Request.QueryString("id"))
        End If



    End Sub

   ' Protected Sub gvAllocatedSeats_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAllocatedSeats.RowDataBound
   '     Dim i As Integer = 0
   '     If e.Row.RowType = DataControlRowType.DataRow Then
   '         Dim lblsno As Label = CType(e.Row.FindControl("lblsno"), Label)
   '         lblsno.Text = e.Row.RowIndex
   '     End If
    ' End Sub
    Public Sub loadshifts()
        Dim param(0) As SqlParameter

        param(0) = New SqlParameter("@location", SqlDbType.NVarChar, 50)
        param(0).Value = GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPC_BDG_ID")
        ObjSubSonic.Binddropdown(ddlshift, "usp_getActiveshiftsbylocation", "sh_name", "sh_code", param)

        If (ddlshift.Items.Count < 1) Then
            lblMsg.Text = "You have No Shifts Alloted for this Location"
            ddlshift.Items.Insert(0, "--Select--")
        End If

        'ObjSubSonic.Binddropdown(ddlshift, "usp_getActiveshiftsbylocation", "sh_name", "sh_code")
    End Sub
    Public Sub loadShifttimings()
        Dim cmd As New SqlCommand
        Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        Dim dr As SqlDataReader
        cmd.Connection = cn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = Session("TENANT") & "." & "usp_getActiveShifttiming"
        cmd.Parameters.AddWithValue("@sh_code", ddlshift.SelectedItem.Value)
        cn.Open()
        dr = cmd.ExecuteReader()
        If dr.Read() Then

            starttimehr.SelectedValue = dr("SH_FRM_HRS").ToString()
            starttimemin.SelectedValue = dr("SH_FRM_MINS").ToString()
            endtimehr.SelectedValue = dr("SH_TO_HRS").ToString()
            endtimemin.SelectedValue = dr("SH_TO_MINS").ToString()
        End If
        cn.Close()

    End Sub

    Protected Sub ddlshift_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlshift.SelectedIndexChanged
        If ddlshift.SelectedIndex <> 0 Then
            loadShifttimings()
        Else
            starttimehr.SelectedIndex = 0
            starttimemin.SelectedIndex = 0
            endtimehr.SelectedIndex = 0
            endtimemin.SelectedIndex = 0
        End If
    End Sub

    Protected Sub gvAllocatedSeats_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvAllocatedSeats.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(0).Text = Session("Parent")
        End If
    End Sub

    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()> _
    Public Shared Function SearchCustomers(ByVal prefixText As String, ByVal vert As String) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
         .ConnectionStrings("CSAmantraFAM").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandType = Data.CommandType.StoredProcedure
        cmd.CommandText = HttpContext.Current.Session("TENANT") & "." & "getsearchEmpName"
        'cmd.CommandText = "getsearchEmpName"
        cmd.Parameters.AddWithValue("@EmpName", prefixText)
        cmd.Parameters.AddWithValue("@VERTICAL", vert)
        cmd.Connection = conn
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("EMP_NAME").ToString)
        End While

        conn.Close()
        Return customers
    End Function

    'Public Sub GetAutoComplete()
    '    Dim param(1) As SqlParameter
    '    param(0) = New SqlParameter("@EmpName", "")
    '    param(1) = New SqlParameter("@functn", ddlVertical.SelectedValue)

    '    Dim sdr As SqlDataReader = ObjSubSonic.GetSubSonicDataReader("getsearchEmpName_byfunction", param)


    '    Dim customers As List(Of ListItem) = New List(Of ListItem)

    '    While sdr.Read
    '        customers.Add(New ListItem(sdr("EMP_NAME").ToString, sdr("EMP_NAME").ToString()))
    '    End While
    '    EditableDropDownList3.DataSource = customers
    '    EditableDropDownList3.DataTextField = "Text"
    '    EditableDropDownList3.DataValueField = "Value"
    '    EditableDropDownList3.AutoselectFirstItem = True
    '    EditableDropDownList3.DataBind()

    'End Sub

    Protected Sub ddlVertical_SelectedIndexChanged1(sender As Object, e As EventArgs) Handles ddlVertical.SelectedIndexChanged
        'GetAutoComplete()

    End Sub
End Class