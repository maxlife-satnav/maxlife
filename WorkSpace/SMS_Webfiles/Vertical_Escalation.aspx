<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Vertical_Escalation.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Vertical_Escalation" Title="Escalation Matrix for Vertical" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>
                                <asp:Label ID="lblHeader" runat="server" />
                                   Matrix
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-md-6">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true" />
                                        Add</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblVertical" class="col-md-5 control-label" runat="server">Vertical Block Requisition By<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvrm" runat="server" ControlToValidate="ddlVerticalBlockRequest" InitialValue="--Select--" Display="None" ErrorMessage="Please Select Vertical Block Requisition By"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlVerticalBlockRequest" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblVerticalconfirm" class="col-md-5 control-label" runat="server">Vertical Block Confirmation By<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvhr" runat="server" ControlToValidate="ddlVerticalBlockConfirmation" InitialValue="--Select--" Display="None" ErrorMessage="Please Select Vertical Block Confirmation By"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlVerticalBlockConfirmation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblVerticalrelse" class="col-md-5 control-label" runat="server">Vertical Release By<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvre" runat="server" ControlToValidate="ddlVerticalRelease" InitialValue="--Select--" Display="None" ErrorMessage="Please Select Vertical Release By"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlVerticalRelease" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Label1" class="col-md-5 control-label" runat="server">Status<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="--Select--" ControlToValidate="ddlStatus" Display="None" ErrorMessage="Please Select Status"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                                    <asp:ListItem Value="0">InActive</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Label2" class="col-md-5 control-label" runat="server">Remarks</asp:Label>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRemarks" Display="None"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" MaxLength="15"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true"
                                            ValidationGroup="Val1" />
                                        <asp:Button ID="btnModify" runat="server" CssClass="btn btn-primary custom-button-color" Text="Modify" CausesValidation="true"
                                            ValidationGroup="Val1" />
                                        <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" CausesValidation="true" />

                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvitems" runat="server" AllowSorting="true" AllowPaging="true"
                                        AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>

                                               <asp:BoundField DataField="VM_SPC_BLK_REQ" HeaderText="Vertica" ItemStyle-HorizontalAlign="Left" />
                                              <asp:BoundField DataField="VM_APP_SPC_BLK_REQ" HeaderText="Vertica" ItemStyle-HorizontalAlign="Left" />
                                              <asp:BoundField DataField="VM_APP_VER_REL" HeaderText="Vertica" ItemStyle-HorizontalAlign="Left" />
                                              <asp:BoundField DataField="STA_ID" HeaderText="Vertica" ItemStyle-HorizontalAlign="Left" />

                                      
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
