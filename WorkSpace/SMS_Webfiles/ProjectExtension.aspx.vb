﻿Imports System
Imports System.Net
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic

Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_ProjectExtension
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters()
    Dim objExtenedRelease As New clsExtenedRelease
    Dim Email As String = String.Empty
    Dim strRedirect As String = String.Empty
    Dim dt As New DataTable

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Visible = False
        If Not Page.IsPostBack Then
            Try
                BindCity()
                ddlTower.Items.Insert(0, "--Select--")
                'obj.BindTower(ddlTower)
                btnSubmit.Visible = False
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
            End Try
        End If
    End Sub

    Private Sub BindCity()
        ObjSubSonic.Binddropdown(ddlCity, "GETALLCITIES", "CTY_NAME", "CTY_CODE")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim cty_code As String = ""
        If ddlCity.SelectedItem.Text = "--Select--" Then
            lblMsg.Text = "Please select city."
            Exit Sub
        Else
            cty_code = ddlCity.SelectedItem.Value
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = cty_code

        ObjSubSonic.Binddropdown(ddlLocation, "GETACTIVELOCATIONBY_CCODE", "LCM_NAME", "LCM_CODE", param)

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex = 0 Then
                gvSpaceExtend.Visible = False
                ddlTower.SelectedIndex = 0
                btnSubmit.Visible = False
            Else
                obj.BindTowerLoc(ddlTower, ddlLocation.SelectedValue.Trim())
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
        End Try
    End Sub

    Dim verBll As New VerticalBLL()
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
                ' ddlWing.Items.Clear()
                ' ddlWing.Items.Insert(0, "--Select--")
                ' ddlVertical.Items.Clear()
                'ddlVertical.Items.Insert(0, "--Select--")
                'ddlCostCenter.Items.Clear()
                'ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")
                ' ddlWing.Items.Clear()
                ' ddlWing.Items.Insert(0, "--Select--")
                ' ddlVertical.Items.Clear()
                'ddlVertical.Items.Insert(0, "--Select--")
                'ddlCostCenter.Items.Clear()
                'ddlCostCenter.Items.Insert(0, "--Select--")
                'ddlProject.Items.Clear()
                'ddlProject.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
        End Try

    End Sub
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            If ddlTower.SelectedItem.Text = "--Select--" Then
                gvSpaceExtend.DataSource = Nothing
                gvSpaceExtend.DataBind()
                btnSubmit.Visible = False
                Exit Sub
            End If
            '  objExtenedRelease.bindData(ddlTower.SelectedValue, gvSpaceExtend, 7)
            objExtenedRelease.bindData(ddlLocation.SelectedValue, gvSpaceExtend, ddlTower.SelectedValue, ddlFloor.SelectedValue, 7)
            If gvSpaceExtend.Rows.Count > 0 Then
                btnSubmit.Visible = True
                gvSpaceExtend.Visible = True
            Else
                ddlTower.SelectedIndex = 0
                btnSubmit.Visible = False

                lblMsg.Text = "No Projects to Extend in this Tower"
                lblMsg.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Extension Details", "Load", ex)
        End Try

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim intCheckCount As Integer = 0
            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chk As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)
                If chk.Checked = True Then
                    Dim txtExtendDate As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendDate"), TextBox)
                    Dim lblReqID As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblReqID"), Label)
                    Try
                        Dim dtDate As Date = txtExtendDate.Text
                    Catch ex As Exception
                        lblMsg.Text = "Please enter valid Date"
                        lblMsg.Visible = True
                        Exit Sub
                    End Try


                    If CType(gvSpaceExtend.Rows(i).Cells(4).Text, Date) >= CType(txtExtendDate.Text, Date) Then
                        lblMsg.Text = "Date should not be less than are equal to FromDate"
                        lblMsg.Visible = True
                        Exit Sub
                    End If


                    ' Check Condition for Extension 
                    If CType(gvSpaceExtend.Rows(i).Cells(5).Text, Date) >= CType(txtExtendDate.Text, Date) Then
                        Dim extend As Integer
                        extend = CheckExtens(txtExtendDate.Text, lblReqID.Text, ddlLocation.SelectedValue, ddlFloor.SelectedValue)
                        If extend = 1 Then
                            lblMsg.Text = "Space already Allocated for this Date"
                            lblMsg.Visible = True
                            Exit Sub

                        End If

                    End If


                    If CType(gvSpaceExtend.Rows(i).Cells(5).Text, Date) <= CType(txtExtendDate.Text, Date) Then
                        Dim extend As Integer
                        extend = CheckExtens(txtExtendDate.Text, lblReqID.Text, ddlLocation.SelectedValue, ddlFloor.SelectedValue)
                        If extend = 1 Then
                            lblMsg.Text = "Space already Allocated for this Date"
                            lblMsg.Visible = True
                            Exit Sub

                        End If

                    End If






                    Dim txtExtendReason As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendReason"), TextBox)
                    intCheckCount += 1
                    If txtExtendDate.Text = "" Then
                        lblMsg.Text = "Please enter Extend Date"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If txtExtendReason.Text = "" Then
                        lblMsg.Text = "Please enter Reason to Extend"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If txtExtendReason.Text.Length() > 500 Then
                        lblMsg.Text = "Reason not more than 500 letters"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                End If
            Next
            If intCheckCount = 0 Then
                lblMsg.Text = "Please select atleast one before Submit"
                lblMsg.Visible = True
                Exit Sub
            End If

            dt.Columns.Add("Sno", GetType(Integer))
            dt.Columns.Add("ProjectID", GetType(String))
            dt.Columns.Add("ExtDate", GetType(String))
            dt.Columns.Add("FromDate", GetType(Date))
            dt.Columns.Add("ToDate", GetType(Date))
            dt.Columns.Add("Remarks", GetType(String))
            Dim drNew As DataRow
            For i As Integer = 0 To gvSpaceExtend.Rows.Count - 1
                Dim chkEmp As CheckBox = CType(gvSpaceExtend.Rows(i).FindControl("chkEmp"), CheckBox)

                If chkEmp.Checked = True Then
                    Dim txtExtendReason As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendReason"), TextBox)
                    Dim lblReqID As Label = CType(gvSpaceExtend.Rows(i).FindControl("lblReqID"), Label)
                    Dim txtDate As TextBox = CType(gvSpaceExtend.Rows(i).FindControl("txtExtendDate"), TextBox)
                    Try
                        objExtenedRelease.updateProject(txtExtendReason.Text.Trim().Replace("'", "''"), lblReqID.Text, txtDate.Text, ddlLocation.SelectedValue, ddlFloor.SelectedValue)

                        drNew = dt.NewRow
                        drNew(0) = dt.Rows.Count + 1
                        drNew(1) = lblReqID.Text
                        drNew(2) = txtDate.Text.Trim()
                        drNew(3) = gvSpaceExtend.Rows(i).Cells(4).Text
                        drNew(4) = gvSpaceExtend.Rows(i).Cells(5).Text
                        drNew(5) = txtExtendReason.Text.Trim()
                        dt.Rows.Add(drNew)
                        'sendMail(dt, gvSpaceExtend.Rows(i).Cells(1).Text, gvSpaceExtend.Rows(i).Cells(2).Text, gvSpaceExtend.Rows(i).Cells(3).Text, Email, lblempid.Text)

                    Catch ex As Exception
                        Throw New Amantra.Exception.DataException("This error has been occured while Updating data to Change Timeline.", "Space Allocation", "Load", ex)
                        lblMsg.Text = "unable to extend"
                        lblMsg.Visible = True
                        Exit Sub
                    End Try
                End If
            Next
            If dt.Rows.Count > 0 Then
                Session("ExtendData") = dt
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("11") & ""
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Allocation Details", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    Private Function CheckExtens(ByVal dtdate As Date, ByVal req As String, ByVal ddlloc As String, ByVal ddlflr As String)
        Dim extend As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECK_PRJEXTEND_DATE")
        sp.Command.AddParameter("@DATE", dtdate, DbType.Date)
        sp.Command.AddParameter("@REQ", req, DbType.String)
        sp.Command.AddParameter("@BDG_ID", req, DbType.String)
        sp.Command.AddParameter("@flr_id", req, DbType.String)
        extend = sp.ExecuteScalar
        Return extend
    End Function

End Class
