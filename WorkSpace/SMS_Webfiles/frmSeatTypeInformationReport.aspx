﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSeatTypeInformationReport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSeatTypeInformationReport" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Seat Type Info Report
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select City</label>
                                        <asp:RequiredFieldValidator ID="rfvcty" runat="server"
                                            ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Location"
                                            ControlToValidate="ddlLocation" Display="None" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Seat Type</label>
                                        <asp:RequiredFieldValidator ID="rfvCode12" runat="server" ControlToValidate="ddlseattype"
                                            Display="None" ErrorMessage="Please Select Seat Type " ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlseattype" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />
                                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="btn btn-primary custom-button-color" OnClick="btnExport_Click" />
                                   
                                </div>
                            </div>
                        </div>
                        <div id="pnlsummary" runat="server" visible="false" class="row">
                             <div class="col-md-6">
                                 <div class="form-group">
                                     <div class="row">
                                         <fieldset>
                                             <legend>Business Entity Occupied Summary </legend>
                                             <asp:Label ID="lblTotalWST" runat="server" class="col-md-4 control-label"></asp:Label>
                                             <asp:Label ID="lblTotalHCB" runat="server" class="col-md-4 control-label"></asp:Label>
                                             <asp:Label ID="lblTotalFCB" runat="server" class="col-md-4 control-label"></asp:Label>
                                         </fieldset>
                                     </div>
                                 </div>
                             </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <fieldset>
                                        <legend>Total Vacant Summary </legend>
                                        <asp:Label ID="lblVacant_WST" runat="server" class="col-md-4 control-label"></asp:Label>
                                        <asp:Label ID="lblVacant_HCB" runat="server" class="col-md-4 control-label"></asp:Label>
                                        <asp:Label ID="lblVacant_FCB" runat="server" class="col-md-4 control-label"></asp:Label>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                            </div>

                        <cc1:ExportPanel ID="ExportPanel1" runat="server">
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <div style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">
                                        <asp:GridView ID="gvVerticalReport" runat="server"
                                            RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" Width="100%"
                                            AutoGenerateColumns="true" EmptyDataText="No Records Found" ShowFooter="True" CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />  
                                        <PagerStyle CssClass="pagination-ys" />  
                                        </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </cc1:ExportPanel>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery('#txtNumberofReq').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });
        });
    </script>
</body>
