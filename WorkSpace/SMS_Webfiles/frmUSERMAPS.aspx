<%@ Page Language="VB" MasterPageFile="~/MapMasterPage.master" AutoEventWireup="false"
    CodeFile="frmUSERMAPS.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmUSERMAPS"
    Title="View Map" %>

<%@ Register Src="Controls/query.ascx" TagName="query" TagPrefix="uc1" %>
<%@ Register Src="Controls/Map_Departments.ascx" TagName="Map_Departments" TagPrefix="uc2" %>
<%@ Register Src="Controls/ConsolidateReport.ascx" TagName="ConsolidateReport" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
 function querySt(ji) {
hu = window.location.search.substring(1);
gy = hu.split("&");
for (i=0;i<gy.length;i++) {
ft = gy[i].split("=");
if (ft[0] == ji) {
return ft[1];
}
}
} 

var flr_id = querySt("flr_id");
var box = querySt("box");
 var spc_id = querySt("spc_id");
 
 
    </script>

    <script type="text/javascript" src="../../PGMap/MAP_GIS/ol/OpenLayers.js"></script>

    <script type="text/javascript" src="../../PGMap/MAP_GIS/map_files/wms-getfeatureinfo.js"></script>

    <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
        <tr>
            <td colspan="2" align="left">
                <table cellspacing="0" cellpadding="0" width="58" border="0">
                    <tr valign="top">
                        <td width="58">
                            <table cellspacing="2" cellpadding="2" border="0">
                                <tr>
                                    <td>
                                        <b>Legend</b></td>
                                </tr>
                            </table>
                            <table cellspacing="1" cellpadding="1" border="0" align="left">
                                <tr>
                                    <td>
                                        Occupied</td>
                                    <td width="50">
                                        &nbsp;</td>
                                    <td bgcolor="#FF0000">
                                        &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        Vacant</td>
                                    <td width="50">
                                        &nbsp;</td>
                                    <td bgcolor="#00FF00">
                                        &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        Download</td>
                                    <td width="50">
                                        &nbsp;</td>
                                    <td>
                                        <a href='<%=Page.ResolveUrl("~/WorkSpace/Uploads/MGControl65.zip")%>' target="_blank"
                                            title="download ActiveXControl">Download ActiveX</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="70%" valign="top" style="height: 260px">
                <iframe id="midFrame" height="500px" width="97%" frameborder="0" scrolling="no" runat="server">
                </iframe>
            </td>
            <td width="30%" align="left" valign="top" style="height: 260px">
                <table width="100%" border="1" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <uc1:query ID="Query1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <uc2:Map_Departments ID="Map_Departments1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            &nbsp;
                            <uc3:ConsolidateReport ID="ConsolidateReport1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
