Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Net.Mail
Imports System.Web
Imports System.Web.Services
Imports System.Collections.Generic

Partial Class frmDashBoardViewSpaceRequisition
    Inherits System.Web.UI.Page
    Dim obj As New clsWorkSpace
    Dim dtSpaceReqDetails As DataTable
    Dim clsObj As New clsMasters
    Dim dtProject As DataTable
    Dim strRedirect As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Visible = False
        txtFromdate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            Try
                Session("AcrdIndex") = "2"
                'ddlProject.Visible = False
                Dim strReqID As String = Request.QueryString("ReqID").ToString().Trim()
                Dim strSatusID As String = Request.QueryString("StaID").ToString().Trim()

                lblSelVertical.Text = Session("Parent")
                lblSelCostcenter.Text = Session("Child")
                If strSatusID = 5 Then
                    btnSubmit.Enabled = True
                    btnCancel.Enabled = True
                ElseIf strSatusID = 8 Then
                    EnableFalse()

                ElseIf strSatusID = 147 Or strSatusID = 148 Or strSatusID = 163 Or strSatusID = 164 Then
                    EnableFalse()
                Else
                    EnableFalse()
                End If
                lblSpaceReqID.Text = strReqID.Trim()
                strSQL = "usp_getCtyDetailsforddl"
                BindCombo(strSQL, ddlCity, "CTY_NAME", "CTY_CODE")
                btnSubmit.Attributes.Add("onclick", "CheckDate()")
                txtDate.Text = getoffsetdatetime(DateTime.Now)
                Dim sp As SqlParameter = New SqlParameter("@SpaceReqID", SqlDbType.NVarChar, 50)
                sp.Value = strReqID
                dtSpaceReqDetails = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_SpaceReqDetails", sp)
                ViewState("dtSpaceReqDetails") = dtSpaceReqDetails
                txtWstations.Text = dtSpaceReqDetails.Rows(0)("Work_Stations").ToString().Trim()
                txtHcabins.Text = dtSpaceReqDetails.Rows(0)("Cubicals").ToString().Trim()
                txtFcabins.Text = dtSpaceReqDetails.Rows(0)("Cabins").ToString().Trim()
                txtFromdate.Text = dtSpaceReqDetails.Rows(0)("SRN_FRM_DT").ToString().Trim()
                txtTodate.Text = dtSpaceReqDetails.Rows(0)("SRN_TO_DT").ToString().Trim()
                txtRemarks.Text = dtSpaceReqDetails.Rows(0)("SRN_REM").ToString().Trim()
                ddlDept.SelectedIndex = ddlDept.Items.IndexOf(ddlDept.Items.FindByValue(dtSpaceReqDetails.Rows(0)("SRN_DEP_ID").ToString().Trim()))
                lblReqStatus.Text = dtSpaceReqDetails.Rows(0)("STATUS").ToString().Trim()
                hdnPrjName.Value = dtSpaceReqDetails.Rows(0)("prjname").ToString().Trim()
                Hidden1.Value = dtSpaceReqDetails.Rows(0)("SRN_PROJ_ID").ToString().Trim()
                lblVertcode.Text = dtSpaceReqDetails.Rows(0)("SRN_BRANCH_ID").ToString().Trim()
                loadCostcenter()
                lblVertical.Text = dtSpaceReqDetails.Rows(0)("vertical").ToString().Trim()
                ' lblProjectCode.Text = dtSpaceReqDetails.Rows(0)("PrjCode").ToString().Trim()
                ddlCostcenter.SelectedIndex = ddlCostcenter.Items.IndexOf(ddlCostcenter.Items.FindByValue(dtSpaceReqDetails.Rows(0)("SRN_COST_CENTER").ToString().Trim()))
                Dim sp1 As New SqlParameter("@vc_twrCode", SqlDbType.NVarChar, 50)
                sp1.Value = dtSpaceReqDetails.Rows(0)("SRN_BDG_ONE").ToString().Trim()
                Dim sp2 As New SqlParameter("@vc_bdg_code", SqlDbType.NVarChar, 50)
                sp2.Value = dtSpaceReqDetails.Rows(0)("SRN_BGT_ID").ToString().Trim()
                ddlCity.SelectedIndex = ddlCity.Items.IndexOf(ddlCity.Items.FindByValue(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "usp_getCityforTower", sp1, sp2)))
                ddlCity_SelectedIndexChanged(sender, e)
                ddlLoc.SelectedIndex = ddlLoc.Items.IndexOf(ddlLoc.Items.FindByValue(dtSpaceReqDetails.Rows(0)("SRN_BGT_ID").ToString().Trim()))
                ddlLoc_SelectedIndexChanged(sender, e)
                ddlTower1.SelectedIndex = ddlTower1.Items.IndexOf(ddlTower1.Items.FindByValue(dtSpaceReqDetails.Rows(0)("SRN_BDG_ONE").ToString().Trim()))
                ddlTower1_SelectedIndexChanged(sender, e)
                txtFromdate.Attributes.Add("onClick", "displayDatePicker('" + txtFromdate.ClientID + "')")
                txtFromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                txtTodate.Attributes.Add("onClick", "displayDatePicker('" + txtTodate.ClientID + "')")
                txtTodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Space Requisition", "Load", ex)
            End Try
        End If
    End Sub

    Public Sub EnableFalse()
        btnSubmit.Enabled = False
        btnCancel.Enabled = False
        ddlCity.Enabled = False
        ddlLoc.Enabled = False
        ddlTower1.Enabled = False
        txtFromdate.Enabled = False
        txtTodate.Enabled = False
        txtWstations.Enabled = False
        txtHcabins.Enabled = False
        txtFcabins.Enabled = False
        txtRemarks.Enabled = False
        ddlCostcenter.Enabled = False
    End Sub

    Public Sub loadProject()
        'clsObj.BindProject(ddlProject)
    End Sub

    Public Sub loadCostcenter()
        Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 50)
        sp1.Value = lblVertcode.Text
        ddlCostcenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_GET_PROJCOSTCENTER_PROJECT_DETAILS", sp1)
        ddlCostcenter.DataTextField = "cost_center_name"
        ddlCostcenter.DataValueField = "cost_center_code"
        ddlCostcenter.DataBind()
        ddlCostcenter.Items.Insert(0, "--Select--")
        ddlCostcenter.Enabled = True
    End Sub

    'Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If (ddlVertical.SelectedIndex > 0) Then
    '        Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
    '        sp1.Value = ddlVertical.SelectedValue
    '        ddlCostcenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvscostcenter", sp1)
    '        ddlCostcenter.DataTextField = "cost_center_name"
    '        ddlCostcenter.DataValueField = "cost_center_code"
    '        ddlCostcenter.DataBind()
    '        ddlCostcenter.Items.Insert(0, "--Select--")
    '        ddlCostcenter.Enabled = True

    '    Else
    '        ddlProject.SelectedIndex = 0
    '        ddlCostcenter.SelectedIndex = 0
    '        ddlCostcenter.Items.Clear()
    '        ddlProject.Items.Clear()
    '    End If


    'End Sub

    'Protected Sub ddlCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostcenter.SelectedIndexChanged
    '    If ddlCostcenter.SelectedIndex > 0 Then
    '        Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
    '        sp1.Value = lblVertcode.Text
    '        Dim sp2 As New SqlParameter("@vc_costcenter", SqlDbType.VarChar, 250)
    '        sp2.Value = ddlCostcenter.SelectedValue
    '        ddlProject.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvcproject", sp1, sp2)
    '        ddlProject.DataTextField = "prj_name"
    '        ddlProject.DataValueField = "prj_code"
    '        ddlProject.DataBind()
    '        ddlProject.Items.Insert(0, "--Select--")
    '        ddlProject.Visible = True
    '        lblProjectCode.Visible = False
    '    Else
    '        ddlProject.Items.Clear()
    '        lblProjectCode.Visible = True
    '    End If
    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If
        Try
            If txtHcabins.Text = "" Then
                lblMsg.Text = "Please Enter Half Cabins Required "
                lblMsg.Visible = True
                Exit Sub
            End If
            Dim intHC As Integer = CType(txtHcabins.Text, Integer)
        Catch ex As Exception
            lblMsg.Text = "Only numbers are allowed in Half Cabins "
            lblMsg.Visible = True
            Exit Sub
        End Try

        Try
            If txtFcabins.Text = "" Then
                lblMsg.Text = "Please Enter Full Cabins Required "
                lblMsg.Visible = True
                Exit Sub
            End If
            Dim intFC As Integer = CType(txtFcabins.Text, Integer)
        Catch ex As Exception
            lblMsg.Text = "Only numbers are allowed in Full Cabins "
            lblMsg.Visible = True
            Exit Sub
        End Try


        Try
            If txtWstations.Text = "" Then
                lblMsg.Text = "Please Enter Work Stations Required "
                lblMsg.Visible = True
                Exit Sub
            End If
            Dim intWS As Integer = CType(txtWstations.Text, Integer)
        Catch ex As Exception
            lblMsg.Text = "Only numbers are allowed in Work Stations "
            lblMsg.Visible = True
            Exit Sub
        End Try

        Dim spVC_Code As New SqlParameter("@VC_PRJ_CODE", SqlDbType.NVarChar, 50)
        'If ddlProject.Visible = False Then
        '    spVC_Code.Value = Hidden1.Value
        'Else
        '    spVC_Code.Value = ddlProject.SelectedValue
        'End If
        dtProject = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SP_GET_PROJECT_DETAILS", spVC_Code)
        Try
            Dim intFromDate As Date = CType(txtFromdate.Text, Date)
        Catch ex As Exception
            lblMsg.Text = "Please enter vaild From Date "
            lblMsg.Visible = True
            Exit Sub
        End Try

        Try
            Dim intToDate As Date = CType(txtTodate.Text, Date)
        Catch ex As Exception
            lblMsg.Text = "Please enter vaild To Date "
            lblMsg.Visible = True
            Exit Sub
        End Try
        If ddlCity.SelectedIndex = 0 Then
            lblMsg.Text = "Please select City "
            lblMsg.Visible = True
            Exit Sub
        End If
        If txtRemarks.Text.Length > 500 Then
            lblMsg.Text = "Remarks Shoud be less than 500 characters "
            lblMsg.Visible = True
            Exit Sub
        End If
        If ddlLoc.SelectedIndex = 0 Then
            lblMsg.Text = "Please select Location "
            lblMsg.Visible = True
            Exit Sub
        End If
        If ddlTower1.SelectedIndex = 0 Then
            lblMsg.Text = "Please select Tower "
            lblMsg.Visible = True
            Exit Sub
        End If
        If CDate(txtTodate.Text) < CDate(txtFromdate.Text) Then
            lblMsg.Text = "To date should be greater than from date "
            lblMsg.Visible = True
            Exit Sub
        ElseIf CDate(txtFromdate.Text) < CDate(txtDate.Text) Then
            lblMsg.Text = " Please enter valid from date that has to be greater than today date "
            lblMsg.Visible = True
            Exit Sub
        ElseIf CDate(txtTodate.Text) < CDate(txtDate.Text) Then
            lblMsg.Text = "Please enter valid To date that has to be greater than today date"
            lblMsg.Visible = True
            Exit Sub
        Else
            If txtHcabins.Text = "" Then
                txtHcabins.Text = "0"
            End If
            If txtFromdate.Text = String.Empty Or txtTodate.Text = String.Empty Or txtRemarks.Text = String.Empty Then
                lblMsg.Text = " Please Enter the Mandatory Fields"
                lblMsg.Visible = True
                Exit Sub
            End If
            If txtFcabins.Text = String.Empty Then
                txtFcabins.Text = 0
            End If
            If txtWstations.Text = String.Empty Then
                txtWstations.Text = 0
            End If

            Dim pcount As SqlParameter() = {New SqlParameter("@VC_VERTICAL", SqlDbType.VarChar, 150), New SqlParameter("@VC_LOC", SqlDbType.VarChar, 150), New SqlParameter("@vc_Tower", SqlDbType.VarChar, 150), New SqlParameter("@from_date", SqlDbType.Date), New SqlParameter("@to_date", SqlDbType.Date)}
            pcount(0).Value = lblVertcode.Text
            pcount(1).Value = ddlLoc.SelectedItem.Value
            pcount(2).Value = ddlTower1.SelectedValue
            pcount(3).Value = CDate(txtFromdate.Text)
            pcount(4).Value = CDate(txtTodate.Text)


            Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"USP_project_requisition_space", pcount)

            Dim wscnt As Integer
            Dim fccnt As Integer
            Dim hccnt As Integer

            Dim i As Integer = 0
            If ds.Tables(0).Rows.Count > 0 Then
                wscnt = CInt(ds.Tables(0).Rows(0).Item("WS"))
                fccnt = CInt(ds.Tables(0).Rows(0).Item("FC"))
                hccnt = CInt(ds.Tables(0).Rows(0).Item("HC"))
            Else
                Exit Sub
            End If

            If CInt(txtWstations.Text) > wscnt Then
                lblMsg.Visible = True
                lblMsg.Text = "Please enter the available no.of work station(s)."
                Exit Sub
            End If
            If CInt(txtHcabins.Text) > hccnt Then
                lblMsg.Visible = True
                lblMsg.Text = "Please enter the available no.of Half cabins(s)."
                Exit Sub
            End If
            If CInt(txtFcabins.Text) > fccnt Then
                lblMsg.Visible = True
                lblMsg.Text = "Please enter the available no.of Full cabin(s)."
                Exit Sub
            End If

            If txtFromdate.Text = String.Empty Or txtTodate.Text = String.Empty Or txtRemarks.Text = String.Empty Then
                lblMsg.Text = " Please Enter the Mandatory Fields"
                lblMsg.Visible = True
                Exit Sub
            Else
                Try
                    Dim sp1 As New SqlParameter("@vc_reqid", SqlDbType.NVarChar, 50)
                    sp1.Value = lblSpaceReqID.Text.Trim()
                    Dim sp2 As New SqlParameter("@vc_fromdate", SqlDbType.DateTime)
                    sp2.Value = txtFromdate.Text
                    Dim sp3 As New SqlParameter("@vc_todate", SqlDbType.DateTime)
                    sp3.Value = txtTodate.Text
                    Dim sp4 As New SqlParameter("@vc_tower", SqlDbType.NVarChar, 50)
                    sp4.Value = ddlTower1.SelectedValue
                    Dim sp5 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
                    sp5.Value = txtRemarks.Text.Trim().Replace("'", "")
                    Dim sp6 As New SqlParameter("@vc_rm", SqlDbType.NVarChar, 250)
                    sp6.Value = ""
                    Dim sp7 As New SqlParameter("@vc_vertical", SqlDbType.NVarChar, 50)
                    sp7.Value = lblVertcode.Text
                    Dim sp8 As New SqlParameter("@vc_proj", SqlDbType.NVarChar, 50)
                    'If ddlProject.Visible = False Then
                    sp8.Value = ""
                    'Else
                    'sp8.Value = ddlProject.SelectedValue
                    'End If
                    Dim sp9 As New SqlParameter("@vc_dept", SqlDbType.NVarChar, 50)
                    sp9.Value = ""
                    Dim sp10 As New SqlParameter("@vc_bdg", SqlDbType.NVarChar, 50)
                    sp10.Value = ddlLoc.SelectedValue.ToString()
                    Dim sp11 As New SqlParameter("@cubicals", SqlDbType.Int)
                    sp11.Value = txtHcabins.Text.Trim()
                    Dim sp12 As New SqlParameter("@cabins", SqlDbType.Int)
                    sp12.Value = txtFcabins.Text.Trim()
                    Dim sp13 As New SqlParameter("@WorkStations", SqlDbType.Int)
                    sp13.Value = txtWstations.Text.Trim()
                    Dim sp14 As New SqlParameter("@vc_cost ", SqlDbType.NVarChar, 150)
                    sp14.Value = ddlCostcenter.SelectedValue
                    Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12, sp13, sp14}
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_updateSpaceDetails", parms)
                    Dim strPrjName As String = String.Empty
                    'If ddlProject.Visible = False Then
                    '    strPrjName = hdnPrjName.Value
                    'Else
                    '    strPrjName = ddlProject.SelectedItem.Text
                    'End If
                    'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_SPACE_REQ_MAIL")
                    'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                    'sp.Command.AddParameter("@REQID", lblSpaceReqID.Text.Trim, DbType.String)
                    'sp.Command.AddParameter("@MAILSTATUS", 8, DbType.Int32)
                    'sp.ExecuteScalar()
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEREQUISITIONUPDATE")
                    sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
                    sp.Command.AddParameter("@REQID", lblSpaceReqID.Text.Trim(), DbType.String)
                    sp.ExecuteScalar()
                    'sendMail_Update(lblSpaceReqID.Text.Trim, txtFromdate.Text, txtTodate.Text, dtProject.Rows(0)(0).ToString(), txtCity1.Text, ddlTower1.SelectedItem.Text, txtWstations.Text, txtHcabins.Text, txtFcabins.Text, strPrjName, "Updated")
                    lblMsg.Text = " Space Requistion has been Updated"
                    lblMsg.Visible = True

                    strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("1001") & "&rid=" & clsSecurity.Encrypt(lblSpaceReqID.Text)
                Catch ex As Exception
                    Throw New Amantra.Exception.DataException("This error has been occured while Updating data.", "Dashboard view Space Requisition", "Load", ex)

                End Try
            End If
        End If
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    'Private Sub sendMail_Update(ByVal strReqId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal strVertical As String, ByVal strCity As String, ByVal strTower As String, ByVal reqWs As Int16, ByVal reqHC As Int16, ByVal reqFC As Int16, ByVal strProject As String, ByVal strStatus As String)
    '    Try
    '        Dim to_mail As String = Session("uemail")
    '        'Dim cc_mail As String = Session("uemail")
    '        Dim body As String = String.Empty
    '        Dim strCC As String = String.Empty
    '        Dim strEmail As String = String.Empty
    '        Dim strRM As String = String.Empty
    '        Dim strRR As String = String.Empty
    '        Dim strUKnownas As String = String.Empty
    '        Dim strKnownas As String = String.Empty
    '        Dim objData As SqlDataReader
    '        'strCC = "select aur_reporting_to,aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strCC)
    '        'While objData.Read
    '        '    strRR = objData("aur_reporting_to")
    '        '    strUKnownas = objData("aur_known_as")
    '        'End While
    '        'strEmail = " select aur_email,aur_known_as from amantra_user where aur_id='" & strRR & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strEmail)

    '        'While objData.Read
    '        '    strRM = objData("aur_email")
    '        '    strKnownas = objData("aur_known_as")
    '        'End While
    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '        sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '        objData = sp1.GetReader()
    '        While objData.Read
    '            strRR = objData("aur_reporting_to").ToString
    '            strRM = objData("aur_reporting_email").ToString
    '            strKnownas = objData("aur_known_as").ToString
    '            to_mail = objData("aur_email").ToString
    '            'BCC = objData("BCC").ToString
    '        End While
    '        'body = "Dear <b> USER </b> <br /> <br />Space Requisition for the request has been " & strStatus & "  successfully. The details are as follows <br /> <br /> " & _
    '        '"Vertical Requisition Id : <b> " & lblSpaceReqID.Text.Trim() & "</b><br /> City : <b> " & strCity & "</b><br /> Tower : <b> " & strTower & "</b><br /> Vertical : <b> " & strVertical & "</b><br />Project : <b></b>" & strProject & "</br>"
    '        'body = body & "From Date        :    <b>" & FromDate & "</b></br>To Date        :     <b>" & ToDate & "</b></br>Required Work Stations :   <b>" & reqWs & "</b></br>" & _
    '        '        "Required Half Cabins   :    <b>" & reqHC & "</b></br> Full Cabins   :     <b>" & reqFC & "</b></br> "
    '        'body = body & "<br /> <br /> <br /><br /> Thanks & Regards <br /><b>myAmantraAxis Team</b>"
    '        If ddlProject.Visible = True Then
    '            strProject = ddlProject.SelectedItem.Text
    '        Else
    '            strProject = hdnPrjName.Value
    '        End If

    '        body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
    '        'body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> " + strKnownas + " </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Project space request has been updated by " + strUKnownas + " and pending for your Approval. The details are as follows</td></tr></table>&nbsp;<br />"
    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Project space request has been updated by " + strUKnownas + " and pending for your Approval. The details are as follows</td></tr></table>&nbsp;<br />"
    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '        body = body & "<table align='center'>"
    '        '<tr><td style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Sno</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Field</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Value</td></tr> "

    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project requisition ID</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblSpaceReqID.Text.Trim() & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>City</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strCity & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblVertical.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project Name</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strProject & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Cost Center Name</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlCostcenter.SelectedItem.Text & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & FromDate & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Date</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ToDate & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqWs & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqHC & "</td></tr>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Full Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks </td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtRemarks.Text.Trim & "</td></tr>"
    '        body = body & "</table>"
    '        body = body & "</td></tr></table>"
    '        body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
    '        body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
    '        body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
    '        body = body & "</table>"

    '        'If to_mail = String.Empty Then
    '        '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If

    '        Dim mail As New MailMessage
    '        'mail.From = New MailAddress(ConfigurationManager.AppSettings("AmantraEmailId").ToString)
    '        'mail.To.Add(to_mail)
    '        'mail.CC.Add(cc_mail)
    '        mail.Subject = "Space Requisition Details"
    '        mail.IsBodyHtml = True
    '        mail.Body = body

    '        Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        Dim parms1 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
    '        Dim parms2 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        Dim parms3 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        Dim parms4 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        Dim parms5 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        Dim parms6 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        Dim parms7 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        parms.Value = "Abcd"
    '        parms1.Value = body
    '        parms2.Value = strRM
    '        parms3.Value = "Space Requisition Details"
    '        parms4.Value = getoffsetdatetime(DateTime.Now)
    '        parms5.Value = "Request Submitted"
    '        parms6.Value = "Normal Mail"
    '        parms7.Value = to_mail

    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms1, parms2, parms3, parms4, parms5, parms6, parms7)

    '        ''Create the SMTP Client
    '        'Dim client As New SmtpClient()
    '        'client.Host = ConfigurationManager.AppSettings("MailIP").ToString()
    '        'Try
    '        '    client.Send(mail)
    '        'Catch ex As Exception
    '        'End Try
    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Space Requisition", "Load", ex)
    '    End Try


    'End Sub

    'Private Sub sendMail_Cancel(ByVal strReqId As String, ByVal FromDate As String, ByVal ToDate As String, ByVal strVertical As String, ByVal strCity As String, ByVal strTower As String, ByVal reqWs As Int16, ByVal reqHC As Int16, ByVal reqFC As Int16, ByVal strProject As String, ByVal strStatus As String)
    '    Try
    '        Dim to_mail As String = Session("uemail")
    '        'Dim cc_mail As String = Session("uemail")
    '        Dim body As String = String.Empty
    '        Dim strCC As String = String.Empty
    '        Dim strEmail As String = String.Empty
    '        Dim strRM As String = String.Empty
    '        Dim strRR As String = String.Empty
    '        Dim strUKnownas As String = String.Empty
    '        Dim strKnownas As String = String.Empty
    '        Dim objData As SqlDataReader
    '        'strCC = "select aur_reporting_to,aur_known_as from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strCC)
    '        'While objData.Read
    '        '    strRR = objData("aur_reporting_to")
    '        '    strUKnownas = objData("aur_known_as")
    '        'End While
    '        'strEmail = " select aur_email,aur_known_as from amantra_user where aur_id='" & strRR & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strEmail)

    '        'While objData.Read
    '        '    strRM = objData("aur_email")
    '        '    strKnownas = objData("aur_known_as")
    '        'End While
    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '        sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '        objData = sp1.GetReader()
    '        While objData.Read
    '            strRR = objData("aur_reporting_to").ToString
    '            strRM = objData("aur_reporting_email").ToString
    '            strKnownas = objData("aur_known_as").ToString
    '            to_mail = objData("aur_email").ToString
    '            'BCC = objData("BCC").ToString
    '        End While
    '        'body = "Dear <b> USER </b> <br /> <br />Space Requisition for the request has been " & strStatus & "  successfully. The details are as follows <br /> <br /> " & _
    '        '"Vertical Requisition Id : <b> " & lblSpaceReqID.Text.Trim() & "</b><br /> City : <b> " & strCity & "</b><br /> Tower : <b> " & strTower & "</b><br /> Vertical : <b> " & strVertical & "</b><br />Project : <b></b>" & strProject & "</br>"
    '        'body = body & "From Date        :    <b>" & FromDate & "</b></br>To Date        :     <b>" & ToDate & "</b></br>Required Work Stations :   <b>" & reqWs & "</b></br>" & _
    '        '        "Required Half Cabins   :    <b>" & reqHC & "</b></br> Full Cabins   :     <b>" & reqFC & "</b></br> "
    '        'body = body & "<br /> <br /> <br /><br /> Thanks & Regards <br /><b>myAmantraAxis Team</b>"
    '        If ddlProject.Visible = True Then
    '            strProject = ddlProject.SelectedItem.Text
    '        Else
    '            strProject = hdnPrjName.Value
    '        End If

    '        body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "
    '        'body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> " + strKnownas + " </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Project space request has been Canceled by " + strUKnownas + ". The details are as follows</td></tr></table>&nbsp;<br />"
    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b>, </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Project space request has been Canceled by " + strUKnownas + ". The details are as follows</td></tr></table>&nbsp;<br />"
    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '        body = body & "<table align='center'>"
    '        '<tr><td style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Sno</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Field</td><td  style='background-color:#3793B4;color:White;font-family:Bookman Old Style;font-size:10.5pt;font-weight:bold;padding-left:5px'>Value</td></tr> "

    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project requisition ID</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblSpaceReqID.Text.Trim() & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>City</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strCity & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strTower & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & lblVertical.Text.Trim() & "</td></tr>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Project Name</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strProject & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Cost Center Name</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlCostcenter.SelectedItem.Text & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Date</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & FromDate & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Date</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ToDate & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqWs & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqHC & "</td></tr>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Full Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqFC & "</td></tr>"
    '        body = body & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Remarks </td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtRemarks.Text.Trim & "</td></tr>"
    '        body = body & "</table>"
    '        body = body & "</td></tr></table>"
    '        body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table>"
    '        body = body & "<br /> <br /><table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"
    '        body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "
    '        body = body & "</table>"

    '        'If to_mail = String.Empty Then
    '        '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If

    '        Dim mail As New MailMessage
    '        ' mail.From = New MailAddress(ConfigurationManager.AppSettings("AmantraEmailId").ToString)
    '        'mail.To.Add(to_mail)
    '        'mail.CC.Add(cc_mail)
    '        mail.Subject = "Space Requisition Details"
    '        mail.IsBodyHtml = True
    '        mail.Body = body

    '        Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        Dim parms1 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
    '        Dim parms2 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        Dim parms3 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        Dim parms4 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        Dim parms5 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        Dim parms6 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        Dim parms7 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        parms.Value = "Abcd"
    '        parms1.Value = body
    '        parms2.Value = to_mail
    '        parms3.Value = "Space Requisition Cancellation Details"
    '        parms4.Value = getoffsetdatetime(DateTime.Now)
    '        parms5.Value = "Request Submitted"
    '        parms6.Value = "Normal Mail"
    '        parms7.Value = strRM

    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms1, parms2, parms3, parms4, parms5, parms6, parms7)

    '        ''Create the SMTP Client
    '        'Dim client As New SmtpClient()
    '        'client.Host = ConfigurationManager.AppSettings("MailIP").ToString()
    '        'Try
    '        '    client.Send(mail)
    '        'Catch ex As Exception
    '        'End Try
    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Space Requisition", "Load", ex)
    '    End Try


    'End Sub

    Sub cleardata()
        ddlTower1.SelectedIndex = -1
        txtCountry1.Text = String.Empty
        txtCity1.Text = String.Empty
        txtLocation1.Text = String.Empty

        txtFromdate.Text = String.Empty
        txtTodate.Text = String.Empty
        txtHcabins.Text = String.Empty
        txtFcabins.Text = String.Empty
        txtWstations.Text = String.Empty
        txtRemarks.Text = String.Empty
        ''txtProject.Text = String.Empty
        ddlDept.SelectedIndex = -1

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If txtRemarks.Text.Length > 500 Then
                lblMsg.Text = "Remarks Shoud be less than 500 characters "
                lblMsg.Visible = True
                Exit Sub
            End If
            dtSpaceReqDetails = ViewState("dtSpaceReqDetails")
            'strSQL = "UPDATE " & Session("TENANT") & "."  & "SMS_SPACEREQUISITION SET SRN_STA_ID='8' where SRN_REQ_ID='" & lblSpaceReqID.Text.Trim() & "' "
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = lblSpaceReqID.Text.Trim()
            Dim intCount As Int16 = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_cancelSpaceRequest", sp1)
            If intCount = 1 Then
                btnSubmit.Enabled = False
                'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_SPACE_REQ_MAIL")
                'sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
                'sp.Command.AddParameter("@REQID", lblSpaceReqID.Text.Trim(), DbType.String)
                'sp.Command.AddParameter("@MAILSTATUS", 9, DbType.Int32)
                'sp.ExecuteScalar()
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEREQUISITIONCANCEL")
                sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
                sp.Command.AddParameter("@REQID", lblSpaceReqID.Text.Trim(), DbType.String)
                sp.ExecuteScalar()
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("8") & "&rid=" & clsSecurity.Encrypt(lblSpaceReqID.Text)
                ' sendMail_Cancel(lblSpaceReqID.Text.Trim(), txtFromdate.Text, txtTodate.Text, lblVertical.Text.Trim, txtCity1.Text, ddlTower1.SelectedItem.Text, txtWstations.Text, txtHcabins.Text, txtFcabins.Text, ddlProject.SelectedValue, "Cancelled")
                'PopUpMessage("Request has been Cancelled", Me)
                lblMsg.Text = "Request has been Cancelled"
                lblMsg.Visible = True
                'Exit Sub
            End If
            If strRedirect <> String.Empty Then
                Response.Redirect(strRedirect)
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Cancelling data.", "Dashboard view Space Requisition", "Load", ex)
        End Try

    End Sub

    'Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    '    ddlTower1.Items.Clear()
    '    txtCity1.Text = ""
    '    txtCountry1.Text = ""
    '    txtLocation1.Text = ""

    '    If ddlCity.SelectedIndex <> 0 Then
    '        obj.loadtower1(ddlTower1, ddlCity.SelectedValue.ToString().Trim())
    '        If ddlTower1.Items.Count = 2 Then
    '            ddlTower1.SelectedIndex = 1
    '            ddlTower1_SelectedIndexChanged(sender, e)
    '        End If
    '        If ddlTower1.Items.Count = 1 Then
    '            PopUpMessage("No towers in this City", Me)
    '        End If
    '    Else
    '        PopUpMessage("Please Select City", Me)
    '    End If
    'End Sub
    'Protected Sub ddlTower1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower1.SelectedIndexChanged
    '    Dim loc_code As String = obj.tower1_selectedindexchanged(ddlTower1, txtCountry1, txtCity1, txtLocation1)
    'End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlLoc.Items.Clear()
        ddlTower1.Items.Clear()
        txtCity1.Text = ""
        txtCountry1.Text = ""
        txtLocation1.Text = ""
        Try

            If ddlCity.SelectedIndex <> 0 Then
                'obj.loadtower1(ddlTower1, ddlCity.SelectedValue.ToString().Trim())
                'obj.loadlocation_City(ddlLoc, ddlCity.SelectedValue.ToString().Trim())
                'If ddlLoc.Items.Count = 2 Then
                '    ddlLoc.SelectedIndex = 1
                '    ddlLoc_SelectedIndexChanged(sender, e)
                'End If
                'If ddlTower1.Items.Count = 1 Then
                '    'PopUpMessage("No towers in this City", Me)
                '    lblMsg.Text = "No towers in this City "
                '    lblMsg.Visible = True
                'End If

                strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
                BindCombo(strSQL, ddlLoc, "LCM_name", "lcm_code")
            Else
                'PopUpMessage("Please Select City", Me)
                lblMsg.Text = "Please Select City !"
                lblMsg.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Space Requisition", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlTower1.Items.Clear()
        txtCity1.Text = ""
        txtCountry1.Text = ""
        txtLocation1.Text = ""
        Try
            If ddlLoc.SelectedIndex <> 0 Then
                obj.loadtower1(ddlTower1, ddlCity.SelectedValue.ToString().Trim(), ddlLoc.SelectedValue.ToString().Trim())
                If ddlLoc.Items.Count = 2 Then
                    ddlLoc.SelectedIndex = 1
                    'ddlLoc_SelectedIndexChanged(sender, e)
                End If
                If ddlTower1.Items.Count = 1 Then
                    'PopUpMessage("No towers in this Location", Me)
                    lblMsg.Text = "No towers in this Location !"
                    lblMsg.Visible = True
                End If
            Else
                'PopUpMessage("Please Select Location", Me)
                lblMsg.Text = "Please Select Location !"
                lblMsg.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Space Requisition", "Load", ex)
        End Try

    End Sub

    Protected Sub ddlTower1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower1.SelectedIndexChanged
        Try
            Dim loc_code As String = String.Empty
            If ddlTower1.SelectedIndex <> 0 Then
                loc_code = obj.tower1_selectedindexchanged(ddlTower1, txtCountry1, txtCity1, txtLocation1)
            Else
                'PopUpMessage("Please Select Tower", Me)
                lblMsg.Text = "Please Select Tower "
                lblMsg.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Dashboard view Space Requisition", "Load", ex)
        End Try

    End Sub

    <WebMethod()> _
    <System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GetProjectsList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 12
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            'strSQL = "select AUR_ID from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID like '" & prefixText & "%' order by AUR_ID"
            'dtEle = SqlHelper.ExecuteDatatable(CommandType.Text, strSQL)
            Dim sp1 As New SqlParameter("@VC_PRJ_NAME", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SP_GET_PROJECTS", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            'count = dtcount
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "GetEmployeeList", exp)
        End Try
    End Function

    Protected Sub btnBack_Click1(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            If Session("View") = "DashBoard" Then
                Session("View") = ""
                strRedirect = "../../WorkSpace/SMS_WebFiles/Dashboard.aspx"
            ElseIf Session("View") = "View" Then
                Session("AcrdIndex") = ""
                Session("View") = ""
                strRedirect = "frmViewSpaceRequisition.aspx"
            ElseIf Session("View") = "" Then
                strRedirect = "frmViewSpaceRequisition.aspx"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while redirecting", "Dashboard view Space Requisition", "Load", ex)
        End Try
        If (strRedirect <> String.Empty) Then
            Response.Redirect(strRedirect, False)
        End If

    End Sub

End Class
