<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Wipro_Report.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_AmantraAxis_Report" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label id="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Space Demo Report</asp:Label>
                    <hr align="center" width="60%" />
                </td>
            </tr>
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td style="width: 36px">
                    <img height="27" alt="" src="../../Images/table_left_top_corner.gif" width="9" />
                </td>
                <td class="tableHEADER" align="left" width="100%">
                    &nbsp;<strong>Space Demo Report</strong>&nbsp;
                </td>
                <td>
                    <img height="27" alt="" src="../../Images/table_right_top_corner.gif" width="16" />
                </td>
            </tr>
            <tr>
                <td style="width: 36px" background="../../Images/table_left_mid_bg.gif">
                </td>
                <td align="left">
                    <asp:ValidationSummary id="Validationsummary1" runat="server" ValidationGroup="Search"
                        CssClass="clsMessage" ForeColor="">
                    </asp:ValidationSummary>
                    <asp:Label id="lblError" runat="server" CssClass="clsMessage"></asp:Label>
                    <asp:Button ID="Button1" Visible="false" runat="server" Text="Export To Excel" CssClass="clsButton"
                        Width="136px" />
                    <asp:Panel id="panl1" runat="Server" width="100%" GroupingText="New Document Entry">
                        <table id="tab1" runat="server" cellpadding="1" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Space Demo Report For
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" Display="Dynamic" ErrorMessage="Select Report For"
                                        ControlToValidate="ddlReportList" Operator="NotEqual" ValidationGroup="Entry"
                                        ValueToCompare="--Select--">
                                    </asp:CompareValidator><br />
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:DropDownList ID="ddlReportList" runat="server">
                                        <asp:ListItem Selected="True" Value="--Select--" Text="--Select--">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Seat Status" Value="0">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Demands" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Profits" Value="2">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Reviews" Value="3">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Title<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                                        Display="Dynamic" ErrorMessage="Title required" ValidationGroup="Entry">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtTitle" runat="server">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Upload Document
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This is a required field!"
                                        ControlToValidate="FU_Doc" ValidationGroup="Entry">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:FileUpload ID="FU_Doc" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnSubmit" runat="server" cssclass="button" Text="Submit" ValidationGroup="Entry" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel id="panl2" runat="Server" width="100%" GroupingText="Search">
                        <table id="tab2" runat="server" cellpadding="1" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Search
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtSearch" runat="server">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%" nowrap="nowrap">
                                    From Date<asp:CompareValidator ID="cvFdate" runat="server" ControlToValidate="txtFdate"
                                        Display="dynamic" ErrorMessage="Please enter valid from date !" Operator="DataTypeCheck"
                                        Type="Date" ValidationGroup="Search"></asp:CompareValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFdate" runat="server" CssClass="clsTextField" MaxLength="10">
                                    </asp:TextBox>(MM/DD/YYYY)
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    To Date
                                    <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtEDate" Display="dynamic"
                                        ErrorMessage="Please enter valid To date !" Operator="DataTypeCheck" Type="Date"
                                        ValidationGroup="Search">
                                    </asp:CompareValidator></td>
                                <td style="height: 26px; width: 50%" class="label" nowrap="nowrap">
                                    <asp:TextBox ID="txtEDate" runat="server" CssClass="clsTextField" MaxLength="10">
                                    </asp:TextBox>(MM/DD/YYYY) 
                               
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnSearch" runat="server" Text="Submit" cssclass="button" CausesValidation="true" ValidationGroup="Search" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel id="panl3" runat="server" width="100%" GroupingText="Report List">
                        <cc1:ExportPanel ID="ExportPanel1" runat="server">
                            &nbsp;<asp:Label ID="lblReport" runat="server"></asp:Label>
                            <br />
                            <asp:GridView ID="gvReportList" EmptyDataText="Records not found." runat="server"
                                AutoGenerateColumns="false" CellPadding="4" ForeColor="#333333" ShowFooter="FALSE"
                                GridLines="None" Width="100%" PageSize="10" AllowPaging="True">
                                <rowstyle backcolor="#F7F6F3" forecolor="#333333" />
                                <columns>
                                            <asp:BoundField DataField="RPT_FOR" HeaderText="REPORT FOR" SortExpression="RPT_FOR" />
                                            <asp:BoundField DataField="RPT_TITLE" HeaderText="REPORT TITLE" SortExpression="RPT_TITLE" />
                                            <asp:BoundField DataField="RPT_DOC" HeaderText="DOCUMENT NAME" SortExpression="RPT_DOC" />
                                            <asp:BoundField DataField="RPT_DATE" HeaderText="Date" SortExpression="RPT_DATE" />
                                            <asp:TemplateField HeaderText="Review">
                                                <ItemTemplate>
                                                    <a href='../../Docs/<%# trim(Eval("RPT_DOC")) %>'>Review</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </columns>
                                <footerstyle backcolor="#5D7B9D" font-bold="True" forecolor="White" />
                                <pagerstyle backcolor="#284775" forecolor="White" horizontalalign="Center" />
                                <selectedrowstyle backcolor="#E2DED6" font-bold="True" forecolor="#333333" />
                                <headerstyle backcolor="#5D7B9D" font-bold="True" forecolor="Black" horizontalalign="Left"
                                    verticalalign="Top" />
                                <editrowstyle backcolor="#999999" />
                                <alternatingrowstyle backcolor="White" forecolor="#284775" />
                            </asp:GridView>
                        </cc1:ExportPanel>
                    </asp:Panel>
                </td>
                <td style="width: 10px" background="../../Images/table_right_mid_bg.gif">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" alt="" src="../../Images/table_left_bot_corner.gif" /></td>
                <td background="../../Images/table_bot_mid_bg.gif">
                    <img height="17" alt="" src="../../Images/table_bot_mid_bg.gif" /></td>
                <td style="height: 17px">
                    <img height="17" alt="" src="../../Images/table_right_bot_corner.gif" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
