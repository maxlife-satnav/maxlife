﻿Imports System.Data.SqlClient
Imports System.Data
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_CheckStatus
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        bindgrid()
    End Sub

    Protected Sub gvResult_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvResult.PageIndexChanging
        gvResult.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub
    Protected Sub bindgrid()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TYPE_ID", SqlDbType.NVarChar, 50)
        param(0).Value = ddlType.SelectedItem.Value
        gvResult.Visible = True
        ObjSubsonic.BindGridView(gvResult, "GET_STATUS_RESULTS", param)
    End Sub
End Class
