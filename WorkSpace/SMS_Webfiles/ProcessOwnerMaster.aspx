﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="ProcessOwnerMaster.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_ProcessOwnerMaster" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>
                                <asp:Label ID="lblHeader" runat="server" />
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Upload Document  (Only Excel ) <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                                                ControlToValidate="fpBrowseDoc" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                                                ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                                                ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                                            </asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <div class="btn btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5  control-label">
                                                <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" />
                                            </div>
                                            <div class="col-md-7">
                                                <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/Cost_Center_Owner_Master.xls"></asp:HyperLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">
                                                Enter User ID<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvemp" runat="server" Display="None" ErrorMessage="Please Enter User ID"
                                                ValidationGroup="Val1" ControlToValidate="txtempid">
                                            </asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtempid" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblProcess" class="col-md-5 control-label" runat="server"></asp:Label>
                                            <div class="col-md-7">
                                                <asp:ListBox ID="liitems" runat="server" SelectionMode="Multiple" CssClass="form-control" Rows="5"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true"
                                            ValidationGroup="Val1" />
                                        <asp:Button ID="btnmodify" runat="server" CssClass="btn btn-primary custom-button-color" Text="Modify" CausesValidation="true"
                                            ValidationGroup="Val1" />
                                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Enter Employee ID</label>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtempsearch" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="col-md-2 text-right">
                                            <asp:Button ID="btnsearch" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="bs-docs-example">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="costsuccess" <%=If(ActiveTab = 0, "class='active'", "")%>><a href="#Success" aria-controls="Legend" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;Success</a></li>
                                                        <li <%=If(ActiveTab = 1, "class='active'", "")%> role="unsuccess"><a href="#unSuccess" aria-controls="Legend" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;Un-success</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tab-content">
                                                <div role="tabpanel" <%=If(ActiveTab = 0, "class='tab-pane active'", "class='tab-pane'")%> id="Success">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="gvitems" runat="server" AllowSorting="true" AllowPaging="true"
                                                                AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped" Width="98%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ID">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblpowid" runat="server" Text='<%#Eval("POW_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee ID">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempid" runat="server" Text='<%#Eval("POW_USR_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Cost Center">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("POW_PSY_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:ButtonField CommandName="Edit" Text="Edit" />
                                                                    <asp:ButtonField CommandName="Delete" Text="Delete" />
                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" <%=If(ActiveTab = 1, "class='tab-pane active'", "class='tab-pane'")%> id="unSuccess">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="unsuccessgridview" runat="server" AllowSorting="true" AllowPaging="true"
                                                                AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped" Width="98%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ID">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblpowid" runat="server" Text='<%#Eval("EMP_ID")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Employee ID">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempid" runat="server" Text='<%#Eval("EMP_COSTCENTER")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Cost Center">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("REMARK")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
