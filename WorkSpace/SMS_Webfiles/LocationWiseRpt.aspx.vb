Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_LocationWiseRpt
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblmsg.Text = ""
        If Not IsPostBack Then

            BindCity()
            BindLocation(ddlCity.SelectedItem.Value)
            ddlCity.Items(0).Text = "--All--"
            'ddlLocation.Items.Insert(0, "--All--")
            ddlLocation.SelectedIndex = 0
            bindgrid()
            t2.Visible = True
            lbltot.Visible = True
        End If
    End Sub
   
    Private Sub BindCity()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        ObjSubsonic.Binddropdown(ddlCity, "GETCITY_BYUSERS", "CTY_NAME", "CTY_CODE", param)
        ddlCity.Items(0).Text = "--All--"
        'ddlLocation.Items.Insert(0, "--All--")
        
    End Sub
    Private Sub BindLocation(ByVal strcity As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@city", SqlDbType.NVarChar, 200)
        param(0).Value = strcity

        If ddlLocation.Items.Count > 1 Then
            ddlLocation.Items.Clear()
        End If
        ObjSubsonic.Binddropdown(ddlLocation, "GET_ALL_AMANTRA_LOCATIONSBYCITY", "LCM_NAME", "LCM_VAL", param)
        If ddlLocation.Items.Count > 0 Then
            ddlLocation.Items.Insert(0, "--All--")
            ddlLocation.Items.RemoveAt(1)
        Else

        End If

    End Sub



#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    

    Public Sub bindgrid()

        Dim rds As New ReportDataSource()
        rds.Name = "EmployeeOccupancyDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/EmployeeOccupancyReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)
        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True



        Dim city As String = ""
        Dim location As String = ""

        If ddlCity.SelectedValue = "--All--" Then
            city = ""
        Else
            city = ddlCity.SelectedValue
        End If

        If ddlLocation.SelectedValue = "--All--" Then
            location = ""
        Else
            location = ddlLocation.SelectedValue
        End If

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = city
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = location
        param(2) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(2).Value = 1
        param(3) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(3).Value = "EMPLOYEE_ID"
        param(4) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(4).Value = "ASC"
        param(5) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(5).Value = 1000
        param(6) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(6).Value = 0
        param(6).Direction = ParameterDirection.Output
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_ALLAMANTRA_USERBYLOC_CODE", param)

        rds.Value = dt


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMPLOYEE_COUNT")
        sp.Command.AddParameter("@CTY_CODE", city, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", location, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltot.Visible = True
            lbltot.Text = "Total Employees : " & ds.Tables(0).Rows(0).Item("EMPLOYEE_ID")
            lbltot.ForeColor = Drawing.Color.Black
            lbltot.Font.Bold = True

        End If
        t2.Visible = True
        lbltot.Visible = True
    End Sub

 
    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click

        If ddlLocation.SelectedIndex = 0 Then

            'Exit Sub
        End If
        bindgrid()
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        lbltot.Visible = False
        t2.Visible = False

        BindLocation(ddlCity.SelectedItem.Value)
        'ddlLocation.Items(0).Text = "--All--"
        Dim i As Integer = ddlLocation.Items.Count
        'ddlLocation.Items.Insert(0, "--All--")
        ReportViewer1.Visible = False
        If ddlLocation.Items.Count = 2 Then
            ddlLocation.SelectedIndex = 0
        End If

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        ReportViewer1.Visible = False
        t2.Visible = False
    End Sub
End Class
