<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmCheckedListforApproval.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmCheckedListforApproval" title="Report Approval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">CHECKLIST FOR AVAILING LEASED RESIDENTIAL ACCOMMODATION 
FROM THE BANK ( INITIAL PROCESS)

             <hr align="center" width="60%" /></asp:Label></td>
        </tr>
    </table>
    <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                <strong>&nbsp; Add Lease</strong>
            </td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                        <td align="left" style="height: 26px; width: 100%" colspan="3">
                            <strong>I) FROM THE STAFF MEMBER :</strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%">
                            <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="5" Width="75%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl1" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Documents">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl2" runat="server" Text='<%#Eval("ItemString") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Submitted(Yes/No)">
                                        <ItemTemplate>
                                            <asp:RadioButtonList ID="rdbtn" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                                <asp:ListItem Value="N">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 100%" colspan="3">
                            <strong>II) FROM THE OWNER / LANDLORD :</strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%">
                            <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="5" Width="75%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl3" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Documents">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl4" runat="server" Text='<%#Eval("ItemString") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Submitted(Yes/No)">
                                        <ItemTemplate>
                                            <asp:RadioButtonList ID="rdbtn1" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                                <asp:ListItem Value="N">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 100%" colspan="3">
                            <strong>III) FROM BROKER / AGENT :</strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width: 100%">
                            <asp:GridView ID="gv2" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="5" Width="75%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl5" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Documents">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl6" runat="server" Text='<%#Eval("ItemString") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Submitted(Yes/No)">
                                        <ItemTemplate>
                                            <asp:RadioButtonList ID="rdbtn2" runat="server" RepeatDirection="Horizontal" Enabled="false" >
                                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                                <asp:ListItem Value="N">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    
                </table>
                <table id="t1" runat="server" cellpadding="1" border="0" width="100%">
                <tr>
                        <td align="left" style="width: 25%; height: 26px;">
                                        Remarks<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfComments" runat="server" ControlToValidate="txtComments"
                                            Display="None" ErrorMessage="Please enter Comments" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"  style="width: 25%; height: 26px;">
                                        <div onmouseover="Tip('Enter Comments with maximum 500 Characters')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtComments" runat="server" CssClass="clsTextField" Width="97%"
                                                TextMode="MultiLine" Rows="5" MaxLength="1000"></asp:TextBox>
                                    </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnApprove" Text="Approve" runat="server" CssClass="button" />
                            <asp:Button ID="btnReject" Text="Reject" runat="server" CssClass="button" />
                        </td>
                    </tr>
                    </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>

</asp:Content>

