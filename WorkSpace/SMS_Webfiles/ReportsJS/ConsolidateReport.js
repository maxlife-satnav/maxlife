﻿//$('#<%= btnSubmit.ClientID %>').click(function() {  
 
function dg_manage_stores(city,brn)
{ 
   $("#ConsolidateGrid").jqGrid(
   {    url: '../../Generics_Handler/ConsolidateReport.ashx?Mcity='+city+'&MLCM_CODE='+brn,
        datatype: 'json',
        height: 250,
        
        colNames: ['City','Location','Tower','Floor','Wing','Vertical','Cost Center','WST','FCB','HCB','TOTAL'],
        colModel: [
                           { name: 'City', width: 100, sortable: true },
                           { name: 'LOCATION_CODE', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true }, 
                           { name: 'Floor', index: 'Floor',  width: 75,  sortable: true },
                           { name: 'Wing', index: 'Wing', width: 50,   sortable: true },
                           { name: 'VERTICAL', index: 'VERTICAL',  width: 100,  sortable: true },
                           { name: 'Project', index: 'Project', width: 120,   sortable: true },
                           { name: 'WST', index: 'WST', width: 50,   sortable: true },
                           { name: 'FCB', index: 'FCB', width: 50,   sortable: true },
                           { name: 'HCB', index: 'HCB',  width: 50,  sortable: true },
                           { name: 'TOTAL', index: 'TOTAL',  width: 50,  sortable: true }
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#ConsolidateGridPager'),
        sortname: 'Project',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#ConsolidateGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#ConsolidateGrid").jqGrid('navButtonAdd','#ConsolidateGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/ConsolidateExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn + ''; jQuery("#ConsolidateGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 
 