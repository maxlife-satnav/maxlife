﻿// JScript File

function dg_manage_storesvertical(vertical)
{ 
   $("#CitySummaryGrid").jqGrid(
   {    url: '../../Generics_Handler/VerticalSummary.ashx?MVertical='+vertical,
        datatype: 'json',
        height: 250,
        
        colNames: ['VERTICAL NAME', 'ALLOCATEDSEATS'],
        colModel: [
                           { name: 'VERTICAL NAME', width: 200, sortable: true },
                           { name: 'ALLOCATEDSEATS', width: 150, sortable: true } 
                        
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridPager'),
        sortname: 'COST_CENTER_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGrid").jqGrid('navButtonAdd','#CitySummaryGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/VerticalSummaryExport.aspx?MVertical='+ vertical + ''; jQuery("#CitySummaryGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	