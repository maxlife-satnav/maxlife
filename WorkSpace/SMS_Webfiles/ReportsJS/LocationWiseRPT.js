﻿// JScript File

function dg_manage_stores(city,brn)
{ 
 
   $("#UsersGrid").jqGrid(
   {    url: '../../Generics_Handler/LocationWiseRPT.ashx?Mcity='+city+'&MLCM_CODE='+brn ,
        datatype: 'json',
        height: 250,
        
        colNames: ['Employee ID','Employee Name','Space ID','Department','Vertical','Cost Center'],
        colModel: [
                           { name: 'EMPLOYEE_ID', width: 100, sortable: true },
                           { name: 'EMPLOYEE_NAME', width: 200, sortable: true }, 
                           { name: 'SPACE_ID', width: 150, sortable: true }, 
                           { name: 'DEPARTMENT',  width: 150,   sortable: true },
                           { name: 'VERTICAL',  width: 150,   sortable: true },
                           { name: 'PROJECT',  width: 150,   sortable: true }
                       
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'EMPLOYEE_ID',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#UsersGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UsersGrid").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/LocationWiseRPTExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn + ''; jQuery("#UsersGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 