﻿// JScript File

function dg_manage_stores_City_SEZ(city,brn,lcmtype)
{ 
   $("#CitySummaryGrid").jqGrid(
   {    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn+'&MLCM_TYPE='+lcmtype,
        datatype: 'json',
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGrid").jqGrid('navButtonAdd','#CitySummaryGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn +'&MLCM_TYPE='+ lcmtype + ''; jQuery("#CitySummaryGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 	
};	 
function dg_manage_stores_City_STPI(city,brn,lcmtype)
{  
		
   $("#CitySummaryGridSTPI").jqGrid(
   {  
    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn+'&MLCM_TYPE='+lcmtype,
        datatype: 'json',
        
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                        
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridSTPIPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridSTPIPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGridSTPI").jqGrid('navButtonAdd','#CitySummaryGridSTPIPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn  +'&MLCM_TYPE='+ lcmtype + ''; jQuery("#CitySummaryGridSTPI").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 
		
			 
};	 
 
function dg_manage_stores(city,brn)
{ 
   $("#CitySummaryGrid").jqGrid(
   {    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn,
        datatype: 'json',
        height: 250,
        
        
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                          
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOTTED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGrid").jqGrid('navButtonAdd','#CitySummaryGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn + ''; jQuery("#CitySummaryGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 
 
function dg_manage_storesLOCATION_SEZ(city,brn,lcmtype)
{ 
   $("#CitySummaryGrid").jqGrid(
   {    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn+'&MLCM_TYPE='+lcmtype,
        datatype: 'json',
         
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                           
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGrid").jqGrid('navButtonAdd','#CitySummaryGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn +'&MLCM_TYPE='+ lcmtype +  ''; jQuery("#CitySummaryGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 
 
function dg_manage_storesLOCATION_STPI(city,brn,lcmtype)
{ 
   $("#CitySummaryGridSTPI").jqGrid(
   {    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn+'&MLCM_TYPE='+lcmtype,
        datatype: 'json',
         
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                          
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridSTPIPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridSTPIPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGridSTPI").jqGrid('navButtonAdd','#CitySummaryGridSTPIPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn +'&MLCM_TYPE='+ lcmtype +  ''; jQuery("#CitySummaryGridSTPI").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 
function dg_manage_stores_Tower(city,brn,twr)
{ 

   $("#CitySummaryGrid").jqGrid(
   {    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn+'&MTWR_CODE='+twr,
        datatype: 'json',
        height: 250,
        
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                           
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGrid").jqGrid('navButtonAdd','#CitySummaryGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn +'&MTWR_CODE='+ twr + ''; jQuery("#CitySummaryGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 
 
function dg_manage_stores_TowerSEZ(city,brn,twr,lcmtype)
{ 

   $("#CitySummaryGrid").jqGrid(
   {    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn+'&MTWR_CODE='+twr+'&MLCM_TYPE='+lcmtype,
        datatype: 'json',
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                           
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGrid").jqGrid('navButtonAdd','#CitySummaryGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn +'&MTWR_CODE='+ twr + ''; jQuery("#CitySummaryGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 
function dg_manage_stores_TowerSTPI(city,brn,twr,lcmtype)
{ 

   $("#CitySummaryGridSTPI").jqGrid(
   {    url: '../../Generics_Handler/CitySummary.ashx?Mcity='+city+'&MLCM_CODE='+brn+'&MTWR_CODE='+twr+'&MLCM_TYPE='+lcmtype,
        datatype: 'json',
        colNames: ['City','Location','Tower','Available Seats','Allocated Seats','Occupied Seats','Allocated but<BR> not occupied seats','Seats available <BR> for allocation'],
        colModel: [
                           { name: 'CTY_NAME', width: 100, sortable: true },
                           { name: 'LCM_NAME', width: 100, sortable: true }, 
                           { name: 'TWR_NAME', width: 100, sortable: true },  
                           
                           { name: 'AVAILABLE_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_SEATS',   width: 100,  sortable: true },
                           { name: 'OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'ALLOCATED_BUT_NOT_OCCUPIED_SEATS',  width: 100,   sortable: true },
                           { name: 'SEATS_AVAILABLE_FOR_ALLOCATION',  width: 100,   sortable: true }
                     
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#CitySummaryGridSTPIPager'),
        sortname: 'CTY_NAME',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#CitySummaryGridSTPIPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
				
					// add custom button to export the data to excel
			jQuery("#CitySummaryGridSTPI").jqGrid('navButtonAdd','#CitySummaryGridSTPIPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/CitySummaryExport.aspx?Mcity='+ city +'&MLCM_CODE='+ brn +'&MTWR_CODE='+ twr + ''; jQuery("#CitySummaryGridSTPI").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 