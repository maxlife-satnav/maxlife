﻿// JScript File

function dg_manage_stores_City_SEZ(city, location, fromdate, todate, fromtimehh, fromtimemm, totimehh, totimemm, bcp, shared, vertical, noofselected) {
    $("#spaceSummaryGrid").jqGrid(
    {
        url: '../../Generics_Handler/spacesummary.ashx?Mcity=' + city + '&Mlocation=' + location + '&Mfromdate=' + fromdate + '&Mtodate=' + todate + '&Mfromtimehh=' + fromtimehh + '&Mfromtimemm=' + fromtimemm + '&Mtotimehh=' + totimehh + '&Mtotimemm=' + totimemm + '&Mbcp=' + bcp + '&Mshared=' + shared + '&Mvertical=' + vertical + '&Mnoofselected=' + noofselected,
        datatype: 'json',
        colNames: ['Location', 'Available Seats', 'Seat Category', 'Seattype', 'Carved status', 'Port Status', 'Space Port Type', 'BCP Seat Type', 'MCP', 'PORT Number', 'Area Type'],
        colModel: [
                           { name: 'SPC_BDG_ID', width: 100, sortable: true },
                           { name: 'spc_id', width: 100, sortable: true },
                           { name: 'spc_layer', width: 100, sortable: true },
                           { name: 'SPACE_TYPE', width: 100, sortable: true },
                           { name: 'SPACE_CARVED', width: 100, sortable: true },
                           { name: 'SPACE_PORT_STATUS', width: 100, sortable: true },
                           { name: 'SPACE_PORT_TYPE', width: 100, sortable: true },
                           { name: 'SPACE_BCP_SEAT_TYPE', width: 100, sortable: true },
                            { name: 'SPACE_MCP_SEAT', width: 100, sortable: true },
                           { name: 'SPACE_PORT_NUMBER', width: 100, sortable: true },
                           { name: 'SPACE_AREA_TYPE', width: 100, sortable: true }
                        

        ],
        rowNum: 10,
        rowList: [10, 50, 100, 150, 200, 250, 300],
        pager: jQuery('#spaceSummaryGridPager'),
       // sortname: 'spc_id',
        viewrecords: true,
       // sortorder: 'asc'
        //caption: 'Report'
    }).navGrid('#spaceSummaryGridPager', { view: false, edit: false, del: false, search: false, add: false, refresh: true }
                     );

    // add custom button to export the data to excel
    jQuery("#spaceSummaryGrid").jqGrid('navButtonAdd', '#spaceSummaryGridPager',
    {
        caption: 'Export to Excel', buttonicon: "ui-icon-calculator", onClickButton: function () {
            var csv_url = 'Exports/space.aspx?Mcity=' + city + '&Mlocation=' + location + 'Mfromdate=' + fromdate + '&Mtodate=' + todate + 'Mfromtimehh=' + fromtimehh + '&Mfromtimemm=' + fromtimemm + 'Mtotimehh=' + totimehh + '&Mtotimemm=' + totimemm + 'Mbcp=' + bcp + '&Mshared=' + shared + 'Mvertical=' + vertical + '&Mnoofselected=' + noofselected + ''; jQuery("#CitySummaryGrid").jqGrid('excelExport', { url: csv_url });
        }

    });
};



