Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repWingwiseOccupancy
    Inherits System.Web.UI.Page
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Visible = False

        If Not Page.IsPostBack Then
            ReportViewer1.Visible = False
            obj.bindTower(ddlReqID)
            ddlReqID.Items(0).Text = "--All Towers--"
            ddlFloor.Items.Insert(0, "--All Floors--")
            ddlWing.Items.Insert(0, "--All Wings--")
            ddlFloor.SelectedIndex = 0
            ddlWing.SelectedIndex = 0
            binddata_wingwise()
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        
        binddata_wingwise()

    End Sub

    Public Sub binddata_wingwise()

        Dim rds As New ReportDataSource()
        rds.Name = "WingOccupancyDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/WingOccupancyReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim MTower As String = ""
        Dim MFloor As String = ""
        Dim MWing As String = ""

        If ddlReqID.SelectedValue = "--All Towers--" Then
            MTower = ""
        Else
            MTower = ddlReqID.SelectedItem.Value
        End If

        If ddlFloor.SelectedValue = "--All Floors--" Then
            MFloor = ""
        Else
            MFloor = ddlFloor.SelectedItem.Value
        End If


        If ddlWing.SelectedValue = "--All Wings--" Then
            MWing = ""
        Else
            MWing = ddlWing.SelectedItem.Value
        End If

        Dim sp1 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = MTower
        Dim sp2 As New SqlParameter("@vc_FloorName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = MFloor
        Dim sp3 As New SqlParameter("@vc_WingName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = MWing

        Dim sp4 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50)
        sp4.Value = "SSA_SPC_ID"

        Dim sp5 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50)
        sp5.Value = "ASC"

        Dim dt As New DataTable
        dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_TowerWise_FloorWise_WingWise", sp1, sp2, sp3, sp4, sp5)

        rds.Value = dt

    End Sub

    Protected Sub ddlReqID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqID.SelectedIndexChanged
        ReportViewer1.Visible = False
        obj.bindfloor(ddlFloor, ddlReqID.SelectedValue)
        ddlFloor.Items(0).Text = "--All Floors--"
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
            'ddlFloor_SelectedIndexChanged(sender, e)
        End If
        obj.bindwing(ddlWing, ddlFloor.SelectedValue)
        ddlWing.Items(0).Text = "--All Wings--"
        If ddlWing.Items.Count = 2 Then
            ddlWing.SelectedIndex = 0
            'ddlWing_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        obj.bindwing(ddlWing, ddlFloor.SelectedValue)
        ReportViewer1.Visible = False
        ddlWing.Items(0).Text = "--All Wings--"
        If ddlWing.Items.Count = 2 Then
            ddlWing.SelectedIndex = 0
            'ddlWing_SelectedIndexChanged(sender, e)
        End If
    End Sub

    Protected Sub ddlWing_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlWing.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class
