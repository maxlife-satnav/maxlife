Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repFloorwiseOccupancy
    Inherits System.Web.UI.Page

    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") Is Nothing Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If

        If Not Page.IsPostBack Then

            'ReportViewer1.Visible = True
            obj.bindTower(ddlReqID)
            ddlReqID.Items(0).Text = "--All Towers--"
            ddlFloor.Items.Insert(0, "--All Floors--")
            ddlFloor.SelectedIndex = 0
            binddata_floorwise()
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click

        
        binddata_floorwise()
    End Sub

    Public Sub binddata_floorwise()


        Dim rds As New ReportDataSource()
        rds.Name = "FloorOccupancyDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/FloorOccupancyReport.rdlc")

        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        Dim p2 As New ReportParameter("dynamicParam2", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        Dim MTower As String = ""
        Dim MFloor As String = ""

        If ddlReqID.SelectedItem.Value = "--All Towers--" Then
            MTower = ""
        Else
            MTower = ddlReqID.SelectedItem.Value
        End If

        If ddlFloor.SelectedItem.Value = "--All Floors--" Then
            MFloor = ""
        Else
            MFloor = ddlFloor.SelectedItem.Value
        End If
        Dim sp1 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = MTower
        Dim sp2 As New SqlParameter("@vc_FloorName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = MFloor

        Dim sp3 As New SqlParameter("@SortExp", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = "SSA.SSA_SPC_ID"

        Dim sp4 As New SqlParameter("@SortDir", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp4.Value = "Asc"

        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_TowerWise_FloorWise", sp1, sp2, sp3, sp4)

        rds.Value = dt

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMPLOYEE_FLOOR_COUNT")
        sp.Command.AddParameter("@TWR_CODE", MTower, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", MFloor, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lbltot.Visible = True
            lbltot.Text = "Total Employees : " & ds.Tables(0).Rows(0).Item("EMPLOYEE_ID")
            lbltot.ForeColor = Drawing.Color.Black
            lbltot.Font.Bold = True

        End If
        lbltot.Visible = True
        t2.Visible = True

    End Sub

    Protected Sub ddlReqID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        obj.bindfloor(ddlFloor, ddlReqID.SelectedValue)
        ReportViewer1.Visible = False
        t2.Visible = False
        ddlFloor.Items(0).Text = "--All Floors--"
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class