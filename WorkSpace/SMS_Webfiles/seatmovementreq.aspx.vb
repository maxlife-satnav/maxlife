﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_seatmovementreq
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim verticalreqid As String
    Dim strRedirect As String
    Dim cntwst As Int32 = 0
    Dim cnthcb As Int32 = 0
    Dim cntfcb As Int32 = 0

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")


     
        If Not IsPostBack Then
            trgrid.Visible = False
            trbutton.Visible = False
            loadVertical()
            objsubsonic.Binddropdown(ddlCity, "GET_ALLCITY", "CTY_NAME", "CTY_CODE")
            objsubsonic.Binddropdown(ddltocity, "GET_ALLCITY", "CTY_NAME", "CTY_CODE")
        End If
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCity.SelectedItem.Value
            objsubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
        End If
    End Sub

    Protected Sub btnavail_Click(sender As Object, e As EventArgs) Handles btnavail.Click
      

        If ddlCity.SelectedIndex > 0 And ddlSelectLocation.SelectedIndex > 0 Then

            If CDate(txtFrmDate.Text) > CDate(txtToDate.Text) Then
                lblMsg.Text = "To Date should be more than from date "
            Else
                BindAvailspaces()
                trgrid.Visible = True

            End If

          
        End If
    End Sub



    Public Sub loadVertical()


        Dim UID As String = ""
        UID = Session("uid")

        Dim param(0) As SqlParameter

        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = UID

        ObjSubsonic.Binddropdown(ddlVertical, "GET_COSTCENTER_AURID", "COST_CENTER_NAME", "COST_CENTER_CODE", param)


    End Sub
    Private Sub BindAvailspaces()

        Dim ftime As String = ""
        Dim ttime As String = ""

        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"


        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If

        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM


        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlSelectLocation.SelectedItem.Value
        param(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
        param(2).Value = ddlvertical.SelectedItem.Value
        param(3) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        param(3).Value = txtFrmDate.Text
        param(4) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        param(4).Value = txtToDate.Text
        param(5) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        param(5).Value = ftime
        param(6) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        param(6).Value = ttime
        objsubsonic.BindGridView(gdavail, "GET_AVAIL_SPACES", param)
        If gdavail.Rows.Count > 0 Then
            lblMsg.Text = ""
            trbutton.Visible = True
            For i As Integer = 0 To gdavail.Rows.Count - 1
                Dim ddlavail As DropDownList = CType(gdavail.Rows(i).FindControl("ddlavail"), DropDownList)
                Dim ds As New DataSet
                Dim sp1 As New SqlParameter("@LOC", SqlDbType.NVarChar, 50, ParameterDirection.Input)
                sp1.Value = ddltoloc.SelectedItem.Value
                'Dim sp2 As New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 50, ParameterDirection.Input)
                'sp2.Value = ddlvertical.SelectedItem.Value
                'Dim sp3 As New SqlParameter("@FROMDATE", SqlDbType.DateTime)
                'sp3.Value = txtFrmDate.Text
                'Dim sp4 As New SqlParameter("@TODATE", SqlDbType.DateTime)
                'sp4.Value = txtToDate.Text
                'Dim sp5 As New SqlParameter("@FROMTIME", SqlDbType.DateTime)
                'sp5.Value = ftime
                'Dim sp6 As New SqlParameter("@TOTIME", SqlDbType.DateTime)
                'sp6.Value = ttime

                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_VACANT_SPACES", sp1)
                ddlavail.DataSource = ds
                ddlavail.DataTextField = "SPC_ID"
                ddlavail.DataValueField = "SPC_ID"
                ddlavail.DataBind()
                ddlavail.Items.Insert(0, "--Select--")
            Next

        Else
            lblMsg.Text = "No seats found"
            trbutton.Visible = False
        End If




    End Sub

    Protected Sub ddlavail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try



            For i As Integer = 0 To gdavail.Rows.Count - 1
                Dim ddlavail As DropDownList = CType(gdavail.Rows(i).FindControl("ddlavail"), DropDownList)
                If ddlavail.SelectedIndex <= 0 Then
                    Exit Sub

                End If

                Dim ds As New DataSet
                Dim sp1 As New SqlParameter("@LOC", SqlDbType.NVarChar, 200, ParameterDirection.Input)
                sp1.Value = ddltoloc.SelectedItem.Value
                Dim sp2 As New SqlParameter("@SPACE_ID", SqlDbType.NVarChar, 200, ParameterDirection.Input)
                sp2.Value = ddlavail.SelectedItem.Value

                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_VACANT_SPACES_LOC", sp1)
                ddlavail.DataSource = ds
                ddlavail.DataTextField = "SPC_ID"
                ddlavail.DataValueField = "SPC_ID"
                ddlavail.DataBind()
                ddlavail.Items.Insert(0, "--Select--")

            Next

        Catch ex As Exception

        End Try
    End Sub



   

    Protected Sub ddltocity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltocity.SelectedIndexChanged
        If ddltocity.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
            param(0).Value = ddltocity.SelectedItem.Value
            objsubsonic.Binddropdown(ddltoloc, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        verticalreqid = objsubsonic.REQGENARATION_REQ(ddlSelectLocation.SelectedItem.Value, "sno", Session("TENANT") & "." , "Seat_movement_Req")



        For Each row As GridViewRow In gdavail.Rows
            Dim ddlavail As DropDownList = CType(row.FindControl("ddlavail"), DropDownList)
            Dim spc_layer As String = Getspacelayer(ddlavail.SelectedItem.Value)
            Dim chkSelect As CheckBox = CType(row.FindControl("chkSelect"), CheckBox)
            lblMsg.Text = ""
            If chkSelect.Checked = True Then
                If spc_layer = "WS" Then
                    cntwst += 1

                End If
                If spc_layer = "HC" Then
                    cnthcb += 1
                End If

                If spc_layer = "FC" Then
                    cntfcb += 1
                End If

            End If

        Next
        Raiseseatmovementreq(verticalreqid, cntwst, cntfcb, cnthcb)

        For i As Integer = 0 To gdavail.Rows.Count - 1
            Dim lblspcoccu As Label = CType(gdavail.Rows(i).FindControl("lblspcoccu"), Label)

            Dim ddlavail As DropDownList = CType(gdavail.Rows(i).FindControl("ddlavail"), DropDownList)
            Dim txtavailspace As TextBox = CType(gdavail.Rows(i).FindControl("txtavailspace"), TextBox)
            Dim chkSelect As CheckBox = CType(gdavail.Rows(i).FindControl("chkSelect"), CheckBox)
            Dim intCount As Int16 = 0
            If chkSelect.Checked = True Then
                intCount = 0
                For j As Integer = 0 To gdavail.Rows.Count - 1
                    Dim ddlavailspc As DropDownList = CType(gdavail.Rows(j).FindControl("ddlavail"), DropDownList)
                    Dim txtavailspacespc As TextBox = CType(gdavail.Rows(j).FindControl("txtavailspace"), TextBox)
                    Dim chkSelectspc As CheckBox = CType(gdavail.Rows(j).FindControl("chkSelect"), CheckBox)
                    If chkSelectspc.Checked = True Then
                        If (ddlavail.Visible = True) Then
                            If ddlavail.SelectedIndex = 0 Then
                                lblMsg.Text = "Please select Seat"
                                Exit Sub
                            End If
                            If ddlavail.SelectedValue = ddlavailspc.SelectedValue Then
                                intCount += 1
                            End If
                        End If
                    End If
                Next
                If intCount > 1 Then
                    lblMsg.Text = "One seat for only one space"
                    Exit Sub
                End If

                Raiseseatmovementreq(verticalreqid, lblspcoccu.Text, ddlavail.Text)

            End If
        Next
        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("1111") & "&rid=" & clsSecurity.Encrypt(verticalreqid)
GVColor:


        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub
    Private Function Getspacelayer(ByVal spc_id As String) As String
        Dim SPACELAYER As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_LAYER")
        sp.Command.AddParameter("@SPC_ID", spc_id, DbType.String)
        SPACELAYER = sp.ExecuteScalar()
        Return SPACELAYER
    End Function
    Private Sub Raiseseatmovementreq(ByVal verticalreqid As String, ByVal cntwst As Int32, ByVal cntfcb As Int32, ByVal cnthcb As Int32)
        Dim ftime As String = ""
        Dim ttime As String = ""

        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"


        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If

        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM

        Dim param3(13) As SqlParameter

        param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        param3(0).Value = verticalreqid
        param3(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param3(1).Value = Session("uid")
        param3(2) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param3(2).Value = ddlCity.SelectedItem.Value
        param3(3) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
        param3(3).Value = ddlSelectLocation.SelectedItem.Value
        param3(4) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        param3(4).Value = txtFrmDate.Text
        param3(5) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        param3(5).Value = txtToDate.Text
        param3(6) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        param3(6).Value = ftime
        param3(7) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        param3(7).Value = ttime
        param3(8) = New SqlParameter("@TOCITY", SqlDbType.NVarChar, 200)
        param3(8).Value = ddltocity.SelectedItem.Value
        param3(9) = New SqlParameter("@TOLOC", SqlDbType.NVarChar, 200)
        param3(9).Value = ddltoloc.SelectedItem.Value
        param3(10) = New SqlParameter("@vertical", SqlDbType.NVarChar, 200)
        param3(10).Value = ddlvertical.SelectedItem.Value

        param3(11) = New SqlParameter("@WST_COUNT", SqlDbType.Int)
        param3(11).Value = cntwst

        param3(12) = New SqlParameter("@HCB_COUNT", SqlDbType.Int)
        param3(12).Value = cnthcb

        param3(13) = New SqlParameter("@FCB_COUNT", SqlDbType.Int)
        param3(13).Value = cntfcb

        objsubsonic.GetSubSonicExecute("SPACE_MOVEMENT_REQUEST", param3)





    End Sub
    Private Sub Raiseseatmovementreq(ByVal verticalreqid As String, ByVal oldspcid As String, ByVal newspcid As String)




        Dim param3(2) As SqlParameter

        param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        param3(0).Value = verticalreqid
        param3(1) = New SqlParameter("@OLD_SPACE_ID", SqlDbType.NVarChar, 200)
        param3(1).Value = oldspcid
        param3(2) = New SqlParameter("@NEW_SPACE_ID", SqlDbType.NVarChar, 200)
        param3(2).Value = newspcid


        objsubsonic.GetSubSonicExecute("SPACE_MOVEMENT_REQUEST_DTLS", param3)



    End Sub




End Class
