<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmApprovalforSpaceExtend.aspx.vb" Inherits="SpaceManagement_frmSpaceVerticalAllocations" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function CheckDataGrid() {
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                window.alert('Please Select atleast one');
                return false;
            }
            else {
                var input = confirm("Are you sure you want to extend?");
                if (input == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Approval for Space Extension
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trCName" runat="server">
                                        <label class="col-md-5 control-label">Select City <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvLoc"
                                            runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Tower <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTower" runat="server" ControlToValidate="ddlTower" Display="None" ErrorMessage="Please Select Tower"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlTower" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="View" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvSpaceExtension" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="Server" ID="chk" />
                                                <asp:Label runat="server" ID="lblAllocEmpMail" Visible="false" Text='<%#bind("Email") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                                <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chk', this.checked)">
                                            </HeaderTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space ID">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblSpaceID" Text='<% #Bind("SPACE_ID")%>'></asp:Label>
                                                <asp:Label runat="server" ID="lblReqID" Text='<% #Bind("SRN_REQ_ID")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vertical">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblVercode" Text='<% #Bind("SSA_VERTICAL")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tower Name">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="Label3" Text='<% #Bind("Tower_Name")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Floor Name">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="Label4" Text='<% #Bind("FloorName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Wing Name">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="Label5" Text='<% #Bind("WingName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="Label7" Text='<% #Bind("EmpName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="Label9" Text='<% #Bind("From_Date")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblToDate" Text='<% #Bind("To_Date")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Extension Date">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblextensionDate" Text='<% #Bind("Extend_Date")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Extension Reason">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="Label11" Text='<% #Bind("SRN_EXT_REMARKS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Admin Remarks">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" MaxLength="500" ID="txtRem" CssClass="form-control"></asp:TextBox>
                                                <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                                    ControlToValidate="txtRem" Display="None" ErrorMessage="Remarks shoud be less than 500 characters !"></asp:CustomValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnApprove" OnClientClick="return CheckDataGrid()"
                                        Text="Approve" CssClass="btn btn-primary custom-button-color" OnClick="btnApprove_Click" Visible="False" />
                                    <asp:Button runat="server" ID="btnReject" Text="Reject" CssClass="btn btn-primary custom-button-color" OnClick="btnReject_Click"
                                        Visible="False" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
