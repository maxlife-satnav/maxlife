﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_frmLeaseSummaryReport
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        BindGrid()
        If Not IsPostBack Then
            'BindGrid()
            'btnsubmit.Visible = False
            'btnpaid.Visible = False
            'lblunpaid.Visible = False
            'lblpaid.Visible = False
            'txtFdate.Attributes.Add("onClick", "displayDatePicker('" + txtFdate.ClientID + "')")
            'txtFdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            'txtTdate.Attributes.Add("onClick", "displayDatePicker('" + txtTdate.ClientID + "')")
            'txtTdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
        'BindGrid()
        'txtFdate.Attributes.Add("readonly", "readonly")
        'txtTdate.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LEASE_SUMMARY_DETAILS")
        'sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.DateTime)
        'sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.DateTime)
        Dim ds As DataSet = sp.GetDataSet()

        gvLeaseDetails.DataSource = ds.Tables(0)
        gvLeaseDetails.DataBind()
        If gvLeaseDetails.Rows.Count > 0 Then
            'btnsubmit.Visible = True
            'lblunpaid.Visible = True

        Else
            'btnsubmit.Visible = False
            'lblunpaid.Visible = False
        End If
    End Sub

    Protected Sub gvLeaseDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvLeaseDetails.PageIndexChanging
        gvLeaseDetails.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Dim total As Integer = 0

    Protected Sub gvLeaseDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            total += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "TOTAL_RENT"))
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblamount As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            lblamount.Text = "Total Amount : " + total.ToString()
        End If
    End Sub

    Protected Sub btnexporttoexcel_Click(sender As Object, e As EventArgs) Handles btnexporttoexcel.Click
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel1.FileName = "Lease Summary Details Report"
    End Sub

End Class
