<%@ Page Language="VB"  AutoEventWireup="false" CodeFile="frmProperty_Details.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmProperty_Details"
    Title="Property Details" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
      <div id="page-wrapper">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                     <fieldset>
                        <legend>Property Details for Approval / Reject</legend>
                    </fieldset>                    
                    <form id="form1" class="form-horizontal well" runat="server">
                           <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Property Type</label>                                
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlPropertyType" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">City</label>                                
                                <div class="col-md-8">
                                     <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Property Code </label>                                
                                <div class="col-md-8">
                                    <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblpropcode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Property Name </label>                                
                                <div class="col-md-8">
                                     <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblPropIDName" runat="server" CssClass="form-control" Enabled="false" ></asp:TextBox></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Maximum Capacity</label>                                
                                <div class="col-md-8">
                                    <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblMaxCapacity" runat="server" CssClass="form-control" Enabled="false" MaxLength="6"></asp:TextBox></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Optimum Capacity </label>                                
                                <div class="col-md-8">
                                     <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblOptCapacity" runat="Server"  CssClass="form-control" Enabled="false" MaxLength="6"></asp:TextBox></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                        
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Carpet Area </label>                                
                                <div class="col-md-8">
                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblCarpetArea" runat="server"  CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Built up Area </label>                                
                                <div class="col-md-8">
                                     <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                 <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                    <asp:TextBox ID="lblBuiltupArea" runat="server" CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Common Area </label>                                
                                <div class="col-md-8">
                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblCommonArea" runat="server"  CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox></div>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Rentable Area </label>                                
                                <div class="col-md-8">
                                     <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblRentableArea" runat="server"  CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Usable Area </label>                                
                                <div class="col-md-8">
                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblUsableArea" runat="server"  CssClass="form-control" Enabled="false" MaxLength="14"></asp:TextBox></div>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">UOM CODE</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="lblUOM_CODE" runat="server"  CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div>
                
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Status</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="lblstatus"  CssClass="form-control" Enabled="false" runat="server" ></asp:TextBox>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Government Property Code </label>                                
                                <div class="col-md-8">
                                    <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblGovtPropCode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox></div>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div>

                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Owner Name </label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="lblownrname" runat="SERVER" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Phone Number</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="lblphno" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Email</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="lblemail" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Property Address</label>                                
                                <div class="col-md-8">
                                    <div onmouseover="Tip('Enter Address with maximum 250 characters')" onmouseout="UnTip()">
                                <asp:TextBox ID="lblPropDesc" runat="server" CssClass="form-control" Enabled="false" Rows="3"
                                    TextMode="Multiline" MaxLength="250"></asp:TextBox></div>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div>   
                        
                        <%--Insurance Details--%>
                        
                        <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Insurance Type </label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtInsType" runat="SERVER" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Insurance Vendor</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtInsVendor" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div> 

                        <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Insurance Policy Number </label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtInsPolNum" runat="SERVER" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Insurance Amount</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtInsAmt" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div>
                        <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Insurance Start Date</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtInsSdate" runat="SERVER" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                               </div> 
                            </div>
                        </div>                 
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Insurance End Date</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtInsEdate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                 </div>                    
                </div>

                        
                  <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-4 control-label">Comments</label>                                
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                               </div> 
                            </div>
                        </div>      
                     </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                  <div class="row">
                            <asp:Button ID="btnApproval" runat="server" CssClass="btn btn-primary custom-button-color" Text="Approval" CausesValidation="true" ValidationGroup="Val1" />
                            <asp:Button ID="btnReject" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reject" CausesValidation="true" ValidationGroup="Val1" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="false" />
                                </div>
                            </div>
                        </div>               
                        </form>
                      </div>
        
        </div>
    </div>
   </div>
   <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    
    </body>
    </html>