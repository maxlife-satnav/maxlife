<%@ Page Language="VB" MasterPageFile="~/LocationMapMaster.master" AutoEventWireup="false"
    CodeFile="Location.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Location" %>
<%@ Register Assembly="Artem.GoogleMap" Namespace="Artem.Web.UI.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   
    <!-- you can use tables or divs for the overall layout -->
    <table border="1" width="100%">
        <tr>
            <td colspan="2">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left" style="width: 50%; height: 16px">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="clslabel">
                            Select City :</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlcity" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            &nbsp;
                        </td>
                        <td valign="top" align="left">
                            <asp:Button ID="btnSubmit" CssClass="clsButton" runat="server" Text="Submit" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" valign="top">
                            <asp:Label ID="LBLMSG" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" width="100%" colspan="2">
                <cc1:GoogleMap ID="GoogleMap1" runat="server" Width="100%" Height="600px" Latitude="42.1229"
            Longitude="24.7879" Zoom="15" EnableScrollWheelZoom="true" EnableDoubleClickZoom="true">
        </cc1:GoogleMap>
            </td>
            <%--<td width="30%" valign="top" style="text-decoration: underline; color: #4444ff;">
                <asp:ListBox ID="lstItems" runat="server" Width="100%" Height="500px"></asp:ListBox>
            </td>--%>
        </tr>
    </table>
</asp:Content>
