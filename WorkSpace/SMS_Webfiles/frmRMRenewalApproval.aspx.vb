Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmRMRenewalApproval
    Inherits System.Web.UI.Page
    Protected Sub ddlLtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLtype.SelectedIndexChanged
        If ddlLtype.SelectedIndex > 0 Then
            BindGrid()
            table2.Visible = True
        Else
            table2.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindLease_Type()
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            BindGrid()
            table2.Visible = True

        End If
    End Sub
    
    Private Sub BindLease_Type()
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
            sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlLtype.DataSource = sp3.GetDataSet()
            ddlLtype.DataTextField = "PN_LEASE_TYPE"
            ddlLtype.DataValueField = "PN_LEASE_ID"
            ddlLtype.DataBind()
            ddlLtype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_LEASES_ALL_APPROVAL_GETDETAILS_RENEWAL")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            gvLDetails_Lease.DataSource = sp.GetDataSet()
            gvLDetails_Lease.DataBind()
            
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvLDetails_Lease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetails_Lease.PageIndexChanging
        gvLDetails_Lease.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

End Class
