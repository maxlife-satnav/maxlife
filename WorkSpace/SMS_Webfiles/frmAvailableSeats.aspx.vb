Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail

Partial Class WorkSpace_SMS_Webfiles_frmAvailableSeats
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim fdate As Date
    Dim tdate As Date
    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init




    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))


    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            
            fdate = Session("FROMDATE")
            tdate = Session("TODATE")
            GetSpaceIDs(fdate, tdate)
        End If
    End Sub

    Private Sub GetSpaceIDs(ByVal fdate As Date, ByVal tdate As Date)
        Dim strLCM_CODE As String
        Dim strSPC_Layer As String
        strLCM_CODE = Request.QueryString("lcm_code")
        strSPC_Layer = Request.QueryString("layer")
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SPC_BDG_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strLCM_CODE
        param(1) = New SqlParameter("@SPC_LAYER", SqlDbType.NVarChar, 200)
        param(1).Value = strSPC_Layer
        param(2) = New SqlParameter("@frmdate", SqlDbType.DateTime)
        param(2).Value = fdate
        param(3) = New SqlParameter("@toDate", SqlDbType.DateTime)
        param(3).Value = tdate
        ObjSubsonic.BindGridView(gvVerticalReport, "GETSPACE_IDLIST", param)
    End Sub

    Protected Sub gvVerticalReport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvVerticalReport.PageIndexChanging
        gvVerticalReport.PageIndex = e.NewPageIndex
        GetSpaceIDs(fdate, tdate)
    End Sub
End Class
