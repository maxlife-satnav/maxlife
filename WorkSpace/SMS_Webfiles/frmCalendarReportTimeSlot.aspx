<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmCalendarReportTimeSlot.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmCalendarReportTimeSlot" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <script language="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">Calendar Report
                </asp:Label>
                <hr align="center" width="60%" />
            </td>
        </tr>
        <tr>
            <td>
                <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                    align="center" border="0">
                    <tr>
                        <td colspan="3" align="left">
                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 27px">
                            <img height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                                width="9"></td>
                        <td width="100%" class="tableHEADER" style="height: 27px" align="left">
                            <strong>Calendar Report</strong>
                        </td>
                        <td style="height: 27px">
                            <img height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                                width="16"></td>
                    </tr>
                    <tr>
                        <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                            &nbsp;</td>
                        <td align="left">
                        	    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
							
							
							
							<table id="table2" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                                border="0">
                                <tr id="trLName" runat="server">
                                    <td align="left" class="clslabel" style="width: 25%; height: 26px;">
                                        City<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select City" InitialValue="--Select--">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td style="width: 25%; height: 26px;">
                                        <asp:DropDownList ID="ddlCity" runat="server" Width="98%" CssClass="clsComboBox"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Preferred Location<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                            Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td style="height: 26px">
                                        <asp:DropDownList ID="ddlSelectLocation" runat="server" Width="98%" CssClass="clsComboBox">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="Tr1" runat="server">
                                    <td align="left" class="clslabel" style="width: 25%; height: 17px">
                                        From Date<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                            Display="None" ErrorMessage="Please Enter From Date ">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                            Display="None" ErrorMessage="Please Enter Valid From Date " Operator="DataTypeCheck"
                                            Type="Date">
                                        </asp:CompareValidator></td>
                                    <td style="width: 25%; height: 17px">
                                        <asp:TextBox ID="txtFrmDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                                        </asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 17px">
                                        To Date<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvtodate" runat="server" ControlToValidate="txtToDate"
                                            Display="None" ErrorMessage="Please Enter To Date ">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtToDate"
                                            Display="None" ErrorMessage="Please Enter Valid To Date " Operator="DataTypeCheck"
                                            Type="Date">
                                        </asp:CompareValidator></td>
                                    <td style="height: 17px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="Tr2" runat="server">
                                    <td align="left" class="clslabel" style="width: 25%; height: 17px">
                                    </td>
                                    <td colspan="2" style="height: 17px; text-align: center">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                                    </td>
                                    <td style="height: 17px">
                                    </td>
                                </tr>
                                <tr id="Tr3" runat="server">
                                    <td align="left" class="clslabel" style="width: 25%; height: 17px">
                                    </td>
                                    <td colspan="2" style="height: 17px; text-align: center">
                                        <asp:Button ID="btnSubmit" CausesValidation="false" runat="server" CssClass="button" Text="Submit" /></td>
                                    <td style="height: 17px">
                                    </td>
                                </tr>
                                <tr id="Tr4" runat="server">
                                    <td align="left" class="clslabel" style="width: 25%; height: 17px">
                                    </td>
                                    <td colspan="2" style="height: 17px; text-align: center">
                                    </td>
                                    <td style="height: 17px">
                                    </td>
                                </tr>
                                <tr id="Tr5" runat="server">
                                    <td align="left" class="clslabel" colspan="4" style="height: 17px; text-align: center">
                                        <asp:Calendar ID="CalendarReport" Visible="false" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66"
                                            BorderWidth="1px" Font-Names="Verdana" Font-Size="8pt"
                                            ForeColor="#663399" Height="200px" ShowGridLines="True" Width="100%">
                                            <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                                            <SelectorStyle BackColor="#FFCC66" />
                                            <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                                            <OtherMonthDayStyle ForeColor="#CC9966" />
                                            <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                                            <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                                            <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
                                        </asp:Calendar>
                                    </td>
                                </tr>
                            </table>


	 </ContentTemplate>
                </asp:UpdatePanel>

                        </td>
                        <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 17px;">
                            <img height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                                width="9" /></td>
                        <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                            <img height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                                width="16" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

