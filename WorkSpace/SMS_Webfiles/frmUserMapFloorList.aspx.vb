Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient

Partial Class WorkSpace_SMS_Webfiles_frmUserMapFloorList
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim TArea As Decimal = 0
    Dim WArea As Decimal = 0
    Dim CArea As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                Dim UID As String = Session("uid")
                GetUserFloorList(UID)
            End If
        End If
    End Sub

    Private Sub GetUserFloorList(ByVal AurID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = AurID
        ObjSubsonic.BindGridView(gvSpaceReport, "GETUSERFLOORLIST", param)
        If gvSpaceReport.Rows.Count = 0 Then
            gvSpaceReport.DataSource = Nothing
            gvSpaceReport.DataBind()
        End If
    End Sub


    Protected Sub gvSpaceReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSpaceReport.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbllink As Label = DirectCast(e.Row.FindControl("lbllink"), Label)
            Dim lnkViewMap As LinkButton = DirectCast(e.Row.FindControl("lnkViewMap"), LinkButton)
           
            If lbllink.Text = "" Then
                lnkViewMap.Enabled = False
            Else
                lnkViewMap.Enabled = True
            End If
            Dim rowArea As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "FLR_AREA"))
            TArea = TArea + rowArea
           

        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTArea As Label = DirectCast(e.Row.FindControl("lblTArea"), Label)
            lblTArea.Text = TArea.ToString()
            lblTArea.ForeColor = Drawing.Color.Black
            lblTArea.Font.Bold = True
        End If
    End Sub

    Protected Sub gvSpaceReport_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSpaceReport.RowCommand
        If e.CommandName = "select" Then
            Dim strstring As String = ""
            strstring = e.CommandArgument

            Dim param
            param = strstring.Split("|")
            Session("FLR_CODE") = param(2)
            Session("BDG_ID") = param(0)
            Session("Twr_Code") = param(1)
            Session("LOC_ID") = param(0)
            Session("Twr_name") = param(1)
            Session("Loc_name") = param(0)
            Session("Flr_name") = param(2)

            Session("Cty_Id") = param(3)


            Response.Redirect(ResolveUrl("~/SMViews/Map/Maploader.aspx") & "?lcm_code=" & param(0) & "&twr_code=" & param(1) & "&flr_code=" & param(2), False)

            'Response.Redirect("frmusermaps.aspx?lcm_code=" & param(0) & "&twr_code=" & param(1) & "&flr_code=" & param(2) & "&spc_id=''")


            'http://localhost/AxisAmantra/WorkSpace/SMS_WebFiles/Map.aspx?lcm_code=COPA&twr_code=U6/COPA&flr_code=GF

        End If
    End Sub


   
    Protected Sub gvSpaceReport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpaceReport.PageIndexChanging
        Try
            gvSpaceReport.PageIndex = e.NewPageIndex
            Dim UID As String = Session("uid")
            GetUserFloorList(UID)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "gvSpaceReport_PageIndexChanging", exp)
        End Try
    End Sub
End Class
