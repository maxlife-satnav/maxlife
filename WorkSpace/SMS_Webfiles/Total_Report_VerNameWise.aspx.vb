Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail

Partial Class WorkSpace_SMS_Webfiles_Total_Report_VerNameWise
    Inherits System.Web.UI.Page
    Dim objMaster As New clsMasters()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("uid") = "" Then
        '    Response.Redirect(AppSettings("logout"))
        'End If
        If Not Page.IsPostBack Then
            Try
                objMaster.Bindlocation(ddlLocation)
                ddlLocation.Items.Insert(1, "--All Locations--")
                ddlLocation.SelectedIndex = 0
                LoadCity()

                strSQL = "GetVer_Name"
                BindCombo(strSQL, ddlVerNameList, "ver_name", "ver_value")
                ddlVerNameList.SelectedIndex = 0
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
            End Try

        End If
    End Sub


    Protected Sub bindData()
        'Dim strTable As String
        'Dim strTower1 As String = String.Empty
        'Dim strTower2 As String = String.Empty
        'Dim icap_ws As Int32 = 0
        'Dim icap_fc As Int32 = 0
        'Dim icap_hc As Int32 = 0
        'Dim icap_total As Int32 = 0
        'Dim iocc_ws As Int32 = 0
        'Dim iocc_fc As Int32 = 0
        'Dim iocc_hc As Int32 = 0
        'Dim iocc_total As Int32 = 0
        'Dim ivacant_ws As Int32 = 0
        'Dim ivacant_fc As Int32 = 0
        'Dim ivacant_hc As Int32 = 0
        'Dim ivacant_total As Int32 = 0

        'Dim iSno As Int32 = 0

        'Dim iGcap_ws As Int32 = 0
        'Dim iGcap_fc As Int32 = 0
        'Dim iGcap_hc As Int32 = 0
        'Dim iGcap_total As Int32 = 0
        'Dim iGocc_ws As Int32 = 0
        'Dim iGocc_fc As Int32 = 0
        'Dim iGocc_hc As Int32 = 0
        'Dim iGocc_total As Int32 = 0
        'Dim iGvacant_ws As Int32 = 0
        'Dim iGvacant_fc As Int32 = 0
        'Dim iGvacant_hc As Int32 = 0
        'Dim iGvacant_total As Int32 = 0
        'Dim iGTotal_ws As Int32 = 0
        'Dim iGTotal_fc As Int32 = 0
        'Dim iGTotal_hc As Int32 = 0
        'dim iGTotal_total as Int32=0

        'strTable = "<br /><br /> "
        'strTable = strTable & "<table border='1'><tr style='background-color:#EFC8F2'><td rowspan='2'><b>SNo</b></td><td rowspan='2'><b>Tower</b></td><td rowspan='2'><b>Floor</b></td><td rowspan='2'><b>Wing</b></td><td rowspan='2'><b>Project/Occupant</b></td><td rowspan='2'><b>Vertical</b></td><td colspan='4'><b>Total Available Seats</b></td><td colspan='4'><b>Allocated Seats</b></td><td colspan='4'><b>Occupied Seats</b></td><td colspan='4'><b>Allocated but not Occupied</b></tr>"
        'strTable = strTable & "<tr style='background-color:#EFC8F2'><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td></tr>"
        'Dim parms As SqlParameter() = {New SqlParameter("@VC_LOCATION", SqlDbType.NVarChar, 150), New SqlParameter("@VC_CHECK", SqlDbType.NVarChar, 10)}
        'If ddlLocation.SelectedItem.Text <> "--All Locations--" Then
        '    parms(0).Value = ddlLocation.SelectedValue
        '    parms(1).Value = 0
        'Else
        '    parms(0).Value = String.Empty
        '    parms(1).Value = 1
        'End If

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_TotalReport", parms)
        'For i As Integer = 0 To ds.Tables(0).Rows.Count

        '    If i <> ds.Tables(0).Rows.Count Then


        '        If (strTower1 = String.Empty) Then
        '            strTower1 = ds.Tables(0).Rows(i)("twr_name")

        '        End If
        '        strTower2 = ds.Tables(0).Rows(i)("twr_name")
        '        If (strTower1.Trim() <> strTower2.Trim()) Then

        '            iGcap_ws = iGcap_ws + icap_ws
        '            iGcap_fc = iGcap_fc + icap_fc
        '            iGcap_hc = iGcap_hc + icap_hc
        '            iGcap_total = iGcap_total + icap_total
        '            iGocc_ws = iGocc_ws + iocc_ws
        '            iGocc_fc = iGocc_fc + iocc_fc
        '            iGocc_hc = iGocc_hc + iocc_hc
        '            iGocc_total = iGocc_total + iocc_total
        '            iGvacant_ws = iGvacant_ws + ivacant_ws
        '            iGvacant_fc = iGvacant_fc + ivacant_fc
        '            iGvacant_hc = iGvacant_hc + ivacant_hc
        '            iGvacant_total = iGvacant_total + ivacant_total
        '            i = i - 1
        '            If (icap_fc = 0 And icap_hc = 0 And icap_ws = 0 And icap_total = 0 And iocc_fc = 0 And iocc_hc = 0 And iocc_ws = 0 And iocc_total = 0 And ivacant_fc = 0 And ivacant_hc = 0 And ivacant_ws = 0 And ivacant_total = 0) Then
        '            Else
        '                strTable = strTable & "<tr style='background-color:#FFFFC0' ><td colspan='6' align='center'> <b>Total</b> </td><td></td><td></td><td></td><td></td><td> " & icap_total.ToString() & "</td><td> " & icap_fc.ToString() & "</td><td> " & icap_hc.ToString() & "</td><td> " & icap_ws.ToString() & "</td><td> " & iocc_total.ToString() & "</td><td> " & iocc_fc.ToString() & "</td><td> " & iocc_hc.ToString() & "</td><td> " & iocc_ws.ToString() & "</td><td> " & ivacant_total.ToString() & "</td><td> " & ivacant_fc.ToString() & "</td><td> " & ivacant_hc.ToString() & "</td><td> " & ivacant_ws.ToString() & "</td></tr>"
        '            End If

        '            strTower1 = String.Empty
        '            icap_ws = 0
        '            icap_fc = 0
        '            icap_hc = 0
        '            icap_total = 0

        '            iocc_ws = 0
        '            iocc_fc = 0
        '            iocc_hc = 0
        '            iocc_total = 0

        '            ivacant_ws = 0
        '            ivacant_fc = 0
        '            ivacant_hc = 0
        '            ivacant_total = 0


        '        Else
        '            If (ds.Tables(0).Rows(i)("CAP_FC") = 0 And ds.Tables(0).Rows(i)("CAP_WS") = 0 And ds.Tables(0).Rows(i)("CAP_TOTAL") = 0 And ds.Tables(0).Rows(i)("OCC_FC") = 0 And ds.Tables(0).Rows(i)("OCC_HC") = 0 And ds.Tables(0).Rows(i)("OCC_WS") = 0 And ds.Tables(0).Rows(i)("OCC_TOTAL") = 0 And ds.Tables(0).Rows(i)("VACANT_FC") = 0 And ds.Tables(0).Rows(i)("VACANT_HC") = 0 And ds.Tables(0).Rows(i)("VACANT_WS") = 0 And ds.Tables(0).Rows(i)("VACANT_TOTAL") = 0) Then
        '            Else
        '                iSno = iSno + 1
        '                strTable = strTable & "<tr> <td> " & iSno & "</td><td>" & ds.Tables(0).Rows(i)("twr_name") & "</td><td>" & ds.Tables(0).Rows(i)("flr_name") & "</td><td>" & ds.Tables(0).Rows(i)("wng_name") & "</td><td>" & ds.Tables(0).Rows(i)("SSA_PROJECT") & "</td><td>" & ds.Tables(0).Rows(i)("ver_name") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_FC") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_HC") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_WS") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_FC") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_HC") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_WS") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_FC") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_HC") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_WS") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_FC") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_HC") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_WS") & "</td></tr>"

        '                icap_ws = icap_ws + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_WS"))
        '                icap_fc = icap_fc + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_FC"))
        '                icap_hc = icap_hc + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_HC"))
        '                icap_total = icap_total + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_TOTAL"))
        '                iocc_ws = iocc_ws + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_WS"))
        '                iocc_fc = iocc_fc + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_FC"))
        '                iocc_hc = iocc_hc + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_HC"))
        '                iocc_total = iocc_total + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_TOTAL"))
        '                ivacant_ws = ivacant_ws + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_WS"))
        '                ivacant_fc = ivacant_fc + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_FC"))
        '                ivacant_hc = ivacant_hc + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_HC"))
        '                ivacant_total = ivacant_total + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_TOTAL"))

        '            End If
        '        End If
        '    Else
        '        iGcap_ws = iGcap_ws + icap_ws
        '        iGcap_fc = iGcap_fc + icap_fc
        '        iGcap_hc = iGcap_hc + icap_hc
        '        iGcap_total = iGcap_total + icap_total
        '        iGocc_ws = iGocc_ws + iocc_ws
        '        iGocc_fc = iGocc_fc + iocc_fc
        '        iGocc_hc = iGocc_hc + iocc_hc
        '        iGocc_total = iGocc_total + iocc_total
        '        iGvacant_ws = iGvacant_ws + ivacant_ws
        '        iGvacant_fc = iGvacant_fc + ivacant_fc
        '        iGvacant_hc = iGvacant_hc + ivacant_hc
        '        iGvacant_total = iGvacant_total + ivacant_total
        '        If (icap_fc = 0 And icap_hc = 0 And icap_ws = 0 And icap_total = 0 And iocc_fc = 0 And iocc_hc = 0 And iocc_ws = 0 And iocc_total = 0 And ivacant_fc = 0 And ivacant_hc = 0 And ivacant_ws = 0 And ivacant_total = 0) Then
        '        Else
        '            strTable = strTable & "<tr style='background-color:#FFFFC0' ><td colspan='6' align='center'> <b>Total</b> </td><td></td><td></td><td></td><td></td><td> " & icap_total.ToString() & "</td><td> " & icap_fc.ToString() & "</td><td> " & icap_hc.ToString() & "</td><td> " & icap_ws.ToString() & "</td><td> " & iocc_total.ToString() & "</td><td> " & iocc_fc.ToString() & "</td><td> " & iocc_hc.ToString() & "</td><td> " & iocc_ws.ToString() & "</td><td> " & ivacant_total.ToString() & "</td><td> " & ivacant_fc.ToString() & "</td><td> " & ivacant_hc.ToString() & "</td><td> " & ivacant_ws.ToString() & "</td></tr>"
        '        End If


        '    End If
        'Next
        'strTable = strTable & "<tr style='background-color:#80FFBF' ><td colspan='6' align='center'> <b>Grand Total</b> </td><td></td><td></td><td></td><td></td><td> " & iGcap_total.ToString() & "</td><td> " & iGcap_fc.ToString() & "</td><td> " & iGcap_hc.ToString() & "</td><td> " & iGcap_ws.ToString() & "</td><td> " & iGocc_total.ToString() & "</td><td> " & iGocc_fc.ToString() & "</td><td> " & iGocc_hc.ToString() & "</td><td> " & iGocc_ws.ToString() & "</td><td> " & iGvacant_total.ToString() & "</td><td> " & iGvacant_fc.ToString() & "</td><td> " & iGvacant_hc.ToString() & "</td><td> " & iGvacant_ws.ToString() & "</td></tr>"

        'strTable = strTable & "</table>"
        ''Response.Write(strTable)
        'lblReport.Text = strTable
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        lblReport.Visible = True
        ExportPanel1.FileName = "ConsolidatedSpaceReport"
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        'If ddlLocation.SelectedIndex <= 0 Then
        '    lblReport.Visible = False
        'ElseIf (ddlCity.SelectedIndex <= 0) Then
        '    lblError.Text = "Please Select Valid City"
        '    lblError.Visible = True
        'Else
        '    bindData()
        '    lblReport.Visible = True
        '    lblError.Visible = False
        'End If

    End Sub

    Public Sub LoadCity()
        objMaster.BindVerticalCity(ddlCity)
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex <= 0 Then
            lblReport.Visible = False
            ddlLocation.SelectedIndex = 0
            lblReport.Visible = False
        Else
            strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
            BindCombo(strSQL, ddlLocation, "LCM_name", "lcm_code")
            lblReport.Visible = False
            lblError.Visible = False
        End If

    End Sub
    Private grdCAP_WS As Decimal = 0
    Private grdCAP_FC As Decimal = 0
    Private grdCAP_HC As Decimal = 0
    Private grdCAP_TOTAL As Decimal = 0
    Private grdOCC_WS As Decimal = 0
    Private grdOCC_FC As Decimal = 0
    Private grdOCC_HC As Decimal = 0
    Private grdOCC_TOTAL As Decimal = 0
    Private grdVACANT_WS As Decimal = 0
    Private grdVACANT_FC As Decimal = 0
    Private grdVACANT_HC As Decimal = 0
    Private grdVACANT_TOTAL As Decimal = 0


    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Try



                Dim rowCAP_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_WS"))
                grdCAP_WS = grdCAP_WS + rowCAP_WS

                Dim rowCAP_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_FC"))
                grdCAP_FC = grdCAP_FC + rowCAP_FC

                Dim rowCAP_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_HC"))
                grdCAP_HC = grdCAP_HC + rowCAP_HC

                Dim rowCAP_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_TOTAL"))
                grdCAP_TOTAL = grdCAP_TOTAL + rowCAP_TOTAL

                Dim rowOCC_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_WS"))
                grdOCC_WS = grdOCC_WS + rowOCC_WS

                Dim rowOCC_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_FC"))
                grdOCC_FC = grdOCC_FC + rowOCC_FC

                Dim rowOCC_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_HC"))
                grdOCC_HC = grdOCC_HC + rowOCC_HC
                Dim rowOCC_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_TOTAL"))
                grdOCC_TOTAL = grdOCC_TOTAL + rowOCC_TOTAL
                Dim rowVACANT_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_WS"))
                grdVACANT_WS = grdVACANT_WS + rowVACANT_WS
                Dim rowVACANT_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_FC"))
                grdVACANT_FC = grdVACANT_FC + rowVACANT_FC

                Dim rowVACANT_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_HC"))
                grdVACANT_HC = grdVACANT_HC + rowVACANT_HC

                Dim rowVACANT_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_TOTAL"))
                grdVACANT_TOTAL = grdVACANT_TOTAL + rowVACANT_TOTAL
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


        End If



        If e.Row.RowType = DataControlRowType.Footer Then
            Dim grdTotalTOTAL_TOTAL As Integer = 0
            Dim lblTotalTOTAL_WS As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_WS"), Label)
            lblTotalTOTAL_WS.Text = GetTotalWST_HCB_FCB("WST").ToString()
            lblTotalTOTAL_WS.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_WS.Font.Bold = True

            Dim lblTotalTOTAL_FC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_FC"), Label)
            lblTotalTOTAL_FC.Text = GetTotalWST_HCB_FCB("FCB").ToString()
            lblTotalTOTAL_FC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_FC.Font.Bold = True

            Dim lblTotalTOTAL_HC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_HC"), Label)
            lblTotalTOTAL_HC.Text = GetTotalWST_HCB_FCB("HCB").ToString()
            lblTotalTOTAL_HC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_HC.Font.Bold = True

            Dim lblTotalTOTAL_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_TOTAL"), Label)
            lblTotalTOTAL_TOTAL.Text = Integer.Parse(lblTotalTOTAL_HC.Text) + Integer.Parse(lblTotalTOTAL_FC.Text) + Integer.Parse(lblTotalTOTAL_WS.Text)
            lblTotalTOTAL_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_TOTAL.Font.Bold = True


            Dim lblTotalCAP_WS As Label = DirectCast(e.Row.FindControl("lblTotalCAP_WS"), Label)
            lblTotalCAP_WS.Text = grdCAP_WS.ToString()
            lblTotalCAP_WS.ForeColor = Drawing.Color.Black
            lblTotalCAP_WS.Font.Bold = True

            Dim lblTotalCAP_FC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_FC"), Label)
            lblTotalCAP_FC.Text = grdCAP_FC.ToString()
            lblTotalCAP_FC.ForeColor = Drawing.Color.Black
            lblTotalCAP_FC.Font.Bold = True

            Dim lblTotalCAP_HC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_HC"), Label)
            lblTotalCAP_HC.Text = grdCAP_HC.ToString()
            lblTotalCAP_HC.ForeColor = Drawing.Color.Black
            lblTotalCAP_HC.Font.Bold = True


            Dim lblTotalCAP_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalCAP_TOTAL"), Label)
            lblTotalCAP_TOTAL.Text = grdCAP_TOTAL.ToString()
            lblTotalCAP_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalCAP_TOTAL.Font.Bold = True

            Dim lblTotalOCC_WS As Label = DirectCast(e.Row.FindControl("lblTotalOCC_WS"), Label)
            lblTotalOCC_WS.Text = grdOCC_WS.ToString()
            lblTotalOCC_WS.ForeColor = Drawing.Color.Black
            lblTotalOCC_WS.Font.Bold = True


            Dim lblTotalOCC_FC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_FC"), Label)
            lblTotalOCC_FC.Text = grdOCC_FC.ToString()
            lblTotalOCC_FC.ForeColor = Drawing.Color.Black
            lblTotalOCC_FC.Font.Bold = True

            Dim lblTotalOCC_HC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_HC"), Label)
            lblTotalOCC_HC.Text = grdOCC_HC.ToString()
            lblTotalOCC_HC.ForeColor = Drawing.Color.Black
            lblTotalOCC_HC.Font.Bold = True

            Dim lblTotalOCC_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalOCC_TOTAL"), Label)
            lblTotalOCC_TOTAL.Text = grdOCC_TOTAL.ToString()
            lblTotalOCC_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalOCC_TOTAL.Font.Bold = True

            Dim lblTotalVACANT_WS As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_WS"), Label)
            lblTotalVACANT_WS.Text = grdVACANT_WS.ToString()
            lblTotalVACANT_WS.ForeColor = Drawing.Color.Black
            lblTotalVACANT_WS.Font.Bold = True

            Dim lblTotalVACANT_FC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_FC"), Label)
            lblTotalVACANT_FC.Text = grdVACANT_FC.ToString()
            lblTotalVACANT_FC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_FC.Font.Bold = True


            Dim lblTotalVACANT_HC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_HC"), Label)
            lblTotalVACANT_HC.Text = grdVACANT_HC.ToString()
            lblTotalVACANT_HC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_HC.Font.Bold = True

            Dim lblTotalVACANT_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_TOTAL"), Label)
            lblTotalVACANT_TOTAL.Text = grdVACANT_TOTAL.ToString()
            lblTotalVACANT_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalVACANT_TOTAL.Font.Bold = True
        End If
    End Sub


    Public Function GetTotalWST_HCB_FCB(ByVal SPC_LAYER As String) As Integer
        Dim spCount1 As New SqlParameter("@SPC_BDG_ID", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@LCM_CTY_ID", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@SPC_LAYER", SqlDbType.VarChar, 500)

        spCount1.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount2.Value = ddlCity.SelectedItem.Value.Trim()
        spCount3.Value = SPC_LAYER
        Dim MCount As Integer = 0
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GetTotalWST_HCB_FCB", spCount1, spCount2, spCount3)

        If ds.Tables(0).Rows.Count > 0 Then
            MCount = ds.Tables(0).Rows(0).Item("CNT")
        End If

        Return MCount
    End Function

    Protected Sub ddlVerNameList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVerNameList.SelectedIndexChanged
        If ddlVerNameList.SelectedIndex <= 0 Then
            lblReport.Visible = False
        ElseIf (ddlVerNameList.SelectedIndex <= 0) Then
            lblError.Text = "Please Select Valid City"
            lblError.Visible = True
        Else
            bindData()
            lblReport.Visible = True
            lblError.Visible = False
        End If
    End Sub
End Class

