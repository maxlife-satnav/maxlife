using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text; 
public partial class Result : System.Web.UI.Page
{
    string clientName;
    protected void Page_Load(object sender, EventArgs e)
    {
      clientName = Request["search"].ToString();
      Getresult();
        
    }

    //The Method Getresult will return (Response.Write) which contains search results seprated by character "~"

    // For E.G. "Ra~Rab~Racd~Raef~Raghi"   which will going to display in search suggest box 

  


    private void Getresult()
    {
        DataTable dt = new DataTable();

       // SqlConnection con = new SqlConnection("Data Source=192.168.5.234;Initial Catalog=AmantraSpace_StarTVDB;User ID=amantra;Password=Super@123");
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["CSAmantraFAM"].ToString());
         
        SqlCommand cmd=new SqlCommand();
        cmd.Connection=con;
        cmd.CommandType= CommandType.StoredProcedure;



        cmd.CommandText = "getsearchEmpName";

        //SFund is Stored Procedure Name which accecpts parameter as @Client_Name and give the search Result

        SqlParameter parClientName = new SqlParameter("@EmpName", SqlDbType.VarChar);
        parClientName.Value = clientName;
        cmd.Parameters.Add(parClientName);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        StringBuilder sb = new StringBuilder(); 
       
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            { 
                sb.Append(dt.Rows[i].ItemArray[0].ToString()  + "~");   //Create Con
            }
        }

           Response.Write(sb.ToString());   

    }
    
}
