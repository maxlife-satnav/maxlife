Imports clsMasters
Imports clsEmpMapping
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports cls_OLEDB_postgresSQL
Imports clsSubSonicCommonFunctions

Partial Class SpaceManagement_frmSpaceVerticalAllocations
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters()
    Dim objEmp As New clsEmpMapping()
    Dim ListWS As New System.Collections.ArrayList()
    Dim ListHC As New System.Collections.ArrayList()
    Dim ListFC As New System.Collections.ArrayList()
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("uid") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            Try

                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
                param(0).Value = Session("uid")
                ObjSubSonic.Binddropdown(ddlReq, "usp_bindSpaceData_Mapping", "SRN_REQ_ID1", "SRN_REQ_ID", param)
                If ddlReq.Items.Count = 0 Then
                    ddlReq.Items.Insert(0, "--Select--")
                End If
                gvEmp.Visible = False
                btnAssign.Visible = False
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
            End Try
        End If
    End Sub
   
    Private Sub UpdateSpace_EmployeeDetails(ByVal REQ_ID As String, ByVal AUR_ID As String, ByVal Space_id As String, ByVal Statusid As Integer, ByVal AUR_VERT As String, lblspcreqid As String)
        Dim param(7) As SqlParameter
        param(0) = New SqlParameter("@strReqID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ_ID
        param(1) = New SqlParameter("@intEmpID", SqlDbType.NVarChar, 200)
        param(1).Value = AUR_ID
        param(2) = New SqlParameter("@strSpaceID", SqlDbType.NVarChar, 200)
        param(2).Value = Space_id
        param(3) = New SqlParameter("@intStatusID", SqlDbType.NVarChar, 200)
        param(3).Value = Statusid
        param(4) = New SqlParameter("@aur_vert", SqlDbType.NVarChar, 200)
        param(4).Value = AUR_VERT
        param(5) = New SqlParameter("@Alloc_req", SqlDbType.NVarChar, 200)
        param(5).Value = ddlReq.SelectedItem.Value
        param(6) = New SqlParameter("@spcreqid", SqlDbType.NVarChar, 200)
        param(6).Value = lblspcreqid
        param(7) = New SqlParameter("@aurid", SqlDbType.NVarChar, 200)
        param(7).Value = Session("uid")





        ObjSubSonic.GetSubSonicExecute("UPDATE_SPACE_Employee", param)

        If GETSPACETYPE(Space_id).Tables(0).Rows(0).Item("SPACE_TYPE") <> 2 Then
            UpdateRecord(LTrim(RTrim(Request.QueryString("ID"))), 7, "")
        Else
            Dim param4(0) As SqlParameter
            param4(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
            param4(0).Value = Space_id
            Dim count As Integer = 0
            count = ObjSubSonic.GetSubSonicExecuteScalar("Shift_SeatRequsition", param4)
            If count = 1 Then
                UpdateRecord(LTrim(RTrim(Space_id)), 11, "") ' completely allocated
            Else
                UpdateRecord(LTrim(RTrim(Space_id)), 10, "") ' partially allocated
            End If
        End If


        'UpdateRecord(Space_id, 7, AUR_ID)


        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SEND_MAIL_SPACEREQUISITIONALLOCATION")
        'sp.Command.AddParameter("@UID", Session("uid"), DbType.String)
        'sp.Command.AddParameter("@REQID", ddlReq.SelectedValue, DbType.String)
        'sp.ExecuteScalar()



    End Sub
    Private Function GETSPACETYPE(ByVal strSpaceId As String) As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETSPACETYPE")
        sp.Command.AddParameter("@SPC_ID", strSpaceId, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Return ds
    End Function

    Protected Sub ddlEmp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim ddl As DropDownList = CType(sender, DropDownList)
            ListWS.Add("GROUP A")
            ListWS.Add("Group A1")
            ListWS.Add("Group A2")
            ListWS.Add("GROUP A3")
            ListWS.Add("GROUP B")
            ListWS.Add("Group B1")
            ListWS.Add("Group B2")
            ListWS.Add("GROUP C1")
            ListWS.Add("RETAINER")
            ListWS.Add("TEAMRBOW")
            ListWS.Add("TRAINEE")
            ListWS.Add("TRAINEES")
            ListWS.Add("WASE")

            ListHC.Add("GROUP C2")
            ListHC.Add("Group D1")

            ListFC.Add("Group D2")
            ListFC.Add("GROUP E")

            For i As Integer = 0 To gvEmp.Rows.Count - 1
                Dim ddlSEmp As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
                If ddlSEmp.SelectedIndex <= 0 Then
                    Exit Sub

                End If
                If ddlSEmp.ClientID = ddl.ClientID Then
                    Dim lblSpaceType As Label = CType(gvEmp.Rows(i).FindControl("lblSpaceType"), Label)
                    'strSQL = "select count(*) from " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION_FOR_PROJECT where SSA_EMP_MAP='" & ddlSEmp.SelectedValue.ToString().Trim() & "'"
                    Dim spAurID As New SqlParameter("@VC_AUR_ID", SqlDbType.NVarChar, 50)
                    spAurID.Value = ddlSEmp.SelectedValue.ToString().Trim()

                    Dim intCnt As Integer = CType(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "USP_EMP_GETEMP_COUNT", spAurID).ToString().Trim(), Int16)
                    If intCnt > 0 Then
                        lblMsg.Text = "Space Already mapped to selected Employee !"
                        ddlSEmp.SelectedIndex = 0
                        Exit Sub
                    End If

                    'strSQL = "select isnull(Aur_grade,'NA')as Grade from " & Session("TENANT") & "."  & "amantra_user where aur_id='" & ddlSEmp.SelectedValue.ToString().Trim() & "'"
                    Dim strGrade As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "USP_GET_GRADE", spAurID).ToString().Trim()

                    Dim k As Integer = ListWS.IndexOf(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "USP_GET_GRADE", spAurID).ToString().Trim())
                    If k = -1 Then
                        k = ListHC.IndexOf(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "USP_GET_GRADE", spAurID).ToString().Trim())
                        If k = -1 Then
                            k = ListFC.IndexOf(SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "USP_GET_GRADE", spAurID).ToString().Trim())
                            If k = -1 Then
                            Else
                                If lblSpaceType.Text.Trim() <> "FULL CABIN" Then
                                    lblMsg.Text = "Selected Employee is Applicable for Full Cabin only "
                                    Exit Sub
                                End If
                            End If
                        Else
                            If lblSpaceType.Text.Trim() <> "HALF CABIN" Then
                                lblMsg.Text = "Selected Employee is Applicable for Half Cabin only "
                                Exit Sub
                            End If
                        End If
                    Else
                        If lblSpaceType.Text.Trim() <> "Work Station" Then
                            lblMsg.Text = "Selected Employee is Applicable for Workstaion only "
                            Exit Sub
                        End If
                    End If
                    Exit Sub
                End If
            Next
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
        End Try


    End Sub

  

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            If ddlReq.SelectedIndex = 0 Then
                gvEmp.DataSource = Nothing
                gvEmp.DataBind()
                Exit Sub
            End If

            Dim parms As SqlParameter() = {New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 50)}
            parms(0).Value = ddlReq.SelectedItem.Value
            Dim dt As DataSet
            dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_GET_SPACES_TO_MAP_EMPLOYEE_MAPPING", parms)
            gvEmp.DataSource = dt
            gvEmp.DataBind()
            bindDetails(gvEmp, ddlReq.SelectedItem.Value)
            btnAssign.Visible = True
            If gvEmp.Rows.Count = 0 Then
                lblMsg.Text = "No Spaces for Employee Mapping"
                btnAssign.Visible = False
            Else
                gvEmp.Visible = True
            End If
            'Dim j As Integer
            For i As Integer = 0 To gvEmp.Rows.Count - 1
                Dim ddlEmp As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
                Dim lblempemail As Label = CType(gvEmp.Rows(i).FindControl("lblempemail"), Label)
                Dim ds As New DataSet
                Dim sp1 As New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
                sp1.Value = ddlReq.SelectedItem.Value

                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GETALLEMPLOYEE1", sp1)
                ddlEmp.DataSource = ds

                ' For j = 0 To ds.Tables(0).Rows.Count - 1
                ddlEmp.DataTextField = "AUR_KNOWN_AS"
                ddlEmp.DataValueField = "AUR_ID"
                ddlEmp.DataBind()
                ddlEmp.Items.Insert(0, "--Select--")
                'lblempemail.Text = "AUR_EMAIL"
                'Next

            Next

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Employee Mapping", "Load", ex)
        End Try
    End Sub
    Public Sub bindDetails(ByVal gvEmp As GridView, ByVal ddlreqid As String)

        Dim sp1 As New SqlParameter("@vc_proj_id", SqlDbType.VarChar, 50)
        sp1.Value = ddlreqid
        Dim dtUsers As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_USERS_TO_MAP", sp1)

        For i As Integer = 0 To gvEmp.Rows.Count - 1
            Dim spReqId As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
            spReqId.Value = CType(gvEmp.Rows(i).FindControl("lblReqID"), Label).Text
            ObjDR = SqlHelper.ExecuteReader(Data.CommandType.StoredProcedure, "USP_GET_FLRID_WINGID", spReqId)
            If ObjDR.Read() Then
                CType(gvEmp.Rows(i).FindControl("lblFloor"), Label).Text = ObjDR("flr_name").ToString() 'ObjDR(0).ToString()
                CType(gvEmp.Rows(i).FindControl("lblWing"), Label).Text = ObjDR("wng_name").ToString() 'ObjDR(1).ToString()
                ' CType(gvEmp.Rows(i).FindControl("lbltower"), Label).Text = ObjDR("twr_name").ToString() 'ObjDR(2).ToString()
            End If
            Dim ddl As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
            ddl.DataSource = dtUsers
            ddl.DataTextField = "NAME"
            ddl.DataValueField = "AUR_ID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select--")
        Next
    End Sub
    Protected Sub ddlEmpType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddleType As DropDownList = CType(sender, DropDownList)
        For i As Integer = 0 To gvEmp.Rows.Count - 1
            Dim ddlEmpType As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmpType"), DropDownList)
            If ddleType.ClientID = ddlEmpType.ClientID Then
                Dim txtEmpName As TextBox = CType(gvEmp.Rows(i).FindControl("txtEmpName"), TextBox)
                Dim ddlEmp As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
                If (ddlEmpType.SelectedValue = "C") Then
                    ddlEmp.Visible = False
                    txtEmpName.Visible = True
                Else
                    ddlEmp.Visible = True
                    txtEmpName.Visible = False
                End If
            End If

        Next

    End Sub

    Protected Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim strRedirect As String = String.Empty
        Try
            For i As Integer = 0 To gvEmp.Rows.Count - 1
                Dim ddlEmp As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
                Dim txtEmp As TextBox = CType(gvEmp.Rows(i).FindControl("txtEmpName"), TextBox)
                Dim chkEmp As CheckBox = CType(gvEmp.Rows(i).FindControl("chkEmp"), CheckBox)

                Dim intCount As Int16 = 0
                If chkEmp.Checked = True Then
                    intCount = 0
                    For j As Integer = 0 To gvEmp.Rows.Count - 1
                        Dim ddlEmpCheck As DropDownList = CType(gvEmp.Rows(j).FindControl("ddlEmp"), DropDownList)
                        Dim txtEmpName As TextBox = CType(gvEmp.Rows(j).FindControl("txtEmpName"), TextBox)
                        Dim chkEmpCheck As CheckBox = CType(gvEmp.Rows(j).FindControl("chkEmp"), CheckBox)
                        If chkEmpCheck.Checked = True Then
                            If (ddlEmp.Visible = True) Then
                                If ddlEmp.SelectedIndex = 0 Then
                                    lblMsg.Text = "Please select Employee"
                                    Exit Sub
                                End If
                                If ddlEmp.SelectedValue = ddlEmpCheck.SelectedValue Then
                                    intCount += 1
                                End If
                            End If

                            If txtEmp.Visible = True Then
                                If txtEmp.Text = String.Empty Then
                                    lblMsg.Text = "Please Enter Employee Name"
                                    Exit Sub
                                End If
                                If txtEmp.Text = txtEmpName.Text Then
                                    intCount += 1
                                End If
                            End If


                        End If
                    Next
                    If intCount > 1 Then
                        lblMsg.Text = "Employee can not be mapped to multiple spaces"
                        Exit Sub
                    End If
                End If
            Next
            Dim intCount1 As Int16 = 0
            Dim dt As New DataTable
            dt.Columns.Add("Sno", GetType(Integer))
            dt.Columns.Add("SpaceID", GetType(String))
            Dim drNew As DataRow
            Dim strEmp As String = String.Empty
            For i As Integer = 0 To gvEmp.Rows.Count - 1
                Dim ddlEmp As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
                Dim chkEmp As CheckBox = CType(gvEmp.Rows(i).FindControl("chkEmp"), CheckBox)
                Dim ddlEmpCheck As DropDownList = CType(gvEmp.Rows(i).FindControl("ddlEmp"), DropDownList)
                Dim txtEmpName As TextBox = CType(gvEmp.Rows(i).FindControl("txtEmpName"), TextBox)
                Dim txtEmp As TextBox = CType(gvEmp.Rows(i).FindControl("txtEmpName"), TextBox)
                Dim lblEmpEMail As Label = CType(gvEmp.Rows(i).FindControl("lblEmpEMail"), Label)
                Dim lblFloor As Label = CType(gvEmp.Rows(i).FindControl("lblFloor"), Label)
                Dim lblWing As Label = CType(gvEmp.Rows(i).FindControl("lblWing"), Label)
                Dim lbltower As Label = CType(gvEmp.Rows(i).FindControl("lbltower"), Label)

                If (txtEmpName.Visible = True) Then
                    strEmp = txtEmp.Text
                Else
                    strEmp = ddlEmp.SelectedValue
                End If

                If chkEmp.Checked = True And ddlEmpCheck.SelectedIndex <> 0 Then
                    'objEmp.updateSpace(CType(gvEmp.Rows(i).FindControl("lblReqID"), Label).Text, strEmp, CType(gvEmp.Rows(i).FindControl("lblSpace"), Label).Text, 7)

                    UpdateSpace_EmployeeDetails(CType(gvEmp.Rows(i).FindControl("lblReqID"), Label).Text, strEmp, CType(gvEmp.Rows(i).FindControl("lblSpace"), Label).Text, 7, CType(gvEmp.Rows(i).FindControl("lblvertical"), Label).Text, CType(gvEmp.Rows(i).FindControl("lblSpcReqid"), Label).Text)


                    intCount1 += 1
                    drNew = dt.NewRow
                    drNew(0) = dt.Rows.Count + 1
                    drNew(1) = CType(gvEmp.Rows(i).FindControl("lblSpace"), Label).Text
                    dt.Rows.Add(drNew)
                    'sendMail(dt, lbltower.Text, lblFloor.Text, lblWing.Text, strEmp, CType(gvEmp.Rows(i).FindControl("lblSpace"), Label).Text)

                End If
                If chkEmp.Checked = True And txtEmpName.Text <> String.Empty Then
                    objEmp.updateSpace(CType(gvEmp.Rows(i).FindControl("lblReqID"), Label).Text, strEmp, CType(gvEmp.Rows(i).FindControl("lblSpace"), Label).Text, 7)
                    intCount1 += 1
                    drNew = dt.NewRow
                    drNew(0) = dt.Rows.Count + 1
                    drNew(1) = CType(gvEmp.Rows(i).FindControl("lblSpace"), Label).Text
                    dt.Rows.Add(drNew)
                End If

            Next
            If dt.Rows.Count > 0 Then
                Session("ReleaseData") = dt
                strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("199") & ""
            End If
            If intCount1 > 0 Then

                lblMsg.Text = "Space mapped to employee successfully"
                'objEmp.BindData(Session("uid"), gvEmp, ddlTower.SelectedValue, ddlLocation.SelectedValue)
                objEmp.BindData(Session("uid"), gvEmp, "", "")
                objEmp.bindDetails(gvEmp, ddlReq.SelectedValue)
                If gvEmp.Rows.Count = 0 Then
                    btnAssign.Visible = False
                End If
            Else
                lblMsg.Text = "Please select at least one checkbox"
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Updating data.", "Employee Mapping", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub

  
End Class
