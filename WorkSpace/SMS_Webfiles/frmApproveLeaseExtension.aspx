<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApproveLeaseExtension.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApproveLeaseExtension"
    Title="Approve Lease Extension" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Approve Lease Extension</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">                        
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="Tr1" runat="server" visible="false">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12 control-label">Select Lease Type </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                                Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                                InitialValue="0"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-10 control-label">Search by Tenant Code/Tenant Name/Property Name<span style="color: red;">*</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <asp:RequiredFieldValidator ID="rfvEmpID" runat="server" ControlToValidate="txtempid"
                                                Display="None" ErrorMessage="Please Enter Tenant Code/Tenant Name/Property Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtempid" runat="Server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                           <div class="col-md-12">
                                <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" PageSize="5" EmptyDataText="No Approved Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                    Style="font-size: 12px;">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Employee No">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllEmpNo" runat="server" CssClass="clsLabel" Text='<%#Eval("LESSE_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CTS Number">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="clsLabel" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CITY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lessor">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLesseName" runat="server" CssClass="clsLabel" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Created By">
                                            <ItemTemplate>
                                                <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <a href='VPHRApprovalLeaseDetails.aspx?id=<%# HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>&ren=1'>View Details</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>

                        <fieldset>
                            <legend>Lease History Details</legend>
                        </fieldset>
                        <div class="row">
                           <div class="col-md-12">
                                <asp:GridView ID="gvitems1" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" PageSize="5" EmptyDataText="No History Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                    Style="font-size: 12px;">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lease">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllname" runat="server" CssClass="lblCODE" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CTS Number">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcity" runat="server" CssClass="lblcity" Text='<%#Eval("CITY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lessor">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLesseName" runat="server" CssClass="lblLesseName" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" CssClass="lblstartdate" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" CssClass="lblExpiryDate" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLstatus" runat="server" CssClass="lblLstatus" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Created By">
                                            <ItemTemplate>
                                                <asp:Label ID="Lbluser" runat="server" CssClass="lbluser" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <%--<a href="#" onclick="showPopWin('../SMS_Webfiles/frmViewLeaseDetailsuser.aspx?id=<%#Eval("LEASE_NAME")%>',750,580,null)">ViewDetails </a>--%>
                                                <%--<a href='frmViewLeaseDetailsuser.aspx?id=<%#Eval("LEASE_NAME")%>'>View Details</a>--%>
                                                <a href="#" onclick="showPopWin('<%# HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>')">View Details</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lease Application form</h4>
                </div>
                <div class="modal-body" id="modelcontainer"> 
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
