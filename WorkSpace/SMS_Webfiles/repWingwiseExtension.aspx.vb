Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_repWingwiseExtension
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(AppSettings("FMGLogout"))
            End If
            If Not Page.IsPostBack Then
                obj.bindTower(ddlTower)
                'ddlTower.Items.Insert(0, "--Select--")
                ddlTower.Items.Insert(1, "--All Towers--")
                txtOnDate.Text = getoffsetdatetime(DateTime.Now)
                trOnDate.Visible = False
                trFromDate.Visible = False
                trToDate.Visible = False
                btnViewReport.Visible = True
                btnExport.Visible = False
                btnViewCriteria.Visible = False
                btnPrint.Visible = False
                txtFromDate.Attributes.Add("onClick", "displayDatePicker('" + txtFromDate.ClientID + "')")
                txtFromDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
                txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                trOnDate.Attributes.Add("onClick", "displayDatePicker('" + trOnDate.ClientID + "')")
                trOnDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "Page_Load", exp)
        End Try
    End Sub
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        Try
            If ddlTower.SelectedIndex = 0 Then
                PopUpMessageNormal("Please select Tower", Me)
                Exit Sub
            End If
            If ddlFloor.SelectedIndex = 0 Then
                PopUpMessageNormal("Please select Floor", Me)
                Exit Sub
            End If
            If ddlWing.SelectedIndex = 0 Then
                PopUpMessageNormal("Please select Wing", Me)
                Exit Sub
            End If
            If ddlCriteria.SelectedValue = "2" Then
                If txtOnDate.Text = "" Then
                    PopUpMessageNormal("Please enter Date", Me)
                    Exit Sub
                Else
                    Try
                        Dim dt As Date = txtOnDate.Text
                    Catch ex As Exception
                        PopUpMessageNormal("Invalid Date,Please enter correct Date", Me)
                        Exit Sub
                    End Try
                End If
            Else
                Dim dtFrom, dtTODate As Date
                If txtFromDate.Text = "" Then
                    PopUpMessageNormal("Please enter From Date", Me)
                    Exit Sub
                Else
                    Try
                        dtFrom = txtFromDate.Text
                    Catch ex As Exception
                        PopUpMessageNormal("Invalid Date,Please enter correct From Date", Me)
                        Exit Sub
                    End Try
                End If
                If txtToDate.Text = "" Then
                    PopUpMessageNormal("Please enter To Date", Me)
                    Exit Sub
                Else
                    Try
                        dtTODate = txtToDate.Text
                    Catch ex As Exception
                        PopUpMessageNormal("Invalid Date,Please enter correct To Date", Me)
                        Exit Sub
                    End Try
                End If
                If dtFrom > dtTODate Then
                    PopUpMessageNormal("From Date should  be less than To Date", Me)
                    Exit Sub
                End If
            End If
            strSQL = "SELECT SMS_SPACE_ALLOCATION.SSA_SRNREQ_ID,convert(nvarchar,SRN_REQ_DT,107) as 'Requistion Date', " & _
                     "convert(nvarchar,SRN_FRM_DT,107) as 'From Date', convert(nvarchar,SRN_TO_DT,107) as 'To Date', " & _
                     "(CASE WHEN SMS_SPACE_ALLOCATION_FOR_PROJECT.SSA_STA_ID = '4' THEN 'Extension Approved' WHEN SMS_SPACE_ALLOCATION_FOR_PROJECT.SSA_STA_ID = '3' THEN 'Requested for Extend' ELSE 'Unknown' END) " & _
                     " AS 'Status', convert(nvarchar,SRN_EXTREQ_DATE,107) AS 'Extension Date'," & _
                     "convert(nvarchar,SRN_TO_DATE1,107) AS 'Actual To Date', convert(nvarchar,SRN_EXTREQ_DATE1,107) AS 'Extension Requested On',SMS_SPACE_ALLOCATION.SSA_SPC_ID , " & _
                     " SMS_SPACE_ALLOCATION_FOR_PROJECT.SSA_SRNREQ_ID ,SMS_SPACEREQUISITION.SRN_REQ_ID AS 'Requisition ID' , " & _
                     " SMS_SPACE_ALLOCATION_FOR_PROJECT.SSA_SPC_ID ,(SELECT DISTINCT WNG_NAME FROM " & Session("TENANT") & "." & "WING WHERE WNG_CODE =SMS_SPACE_ALLOCATION.SSA_WNG_ID and wng_flr_id= SMS_SPACE_ALLOCATION.SSA_FLR_ID and wng_twr_id=SMS_SPACE_ALLOCATION.SSA_TWR_ID)AS 'Wing'," & _
                     "(SELECT DISTINCT FLR_NAME FROM " & Session("TENANT") & "." & "FLOOR WHERE FLR_CODE =SMS_SPACE_ALLOCATION.SSA_FLR_ID and flr_twr_id= SMS_SPACE_ALLOCATION.SSA_TWR_ID ) AS 'Floor' , " & _
                     "(SELECT DISTINCT TWR_NAME FROM " & Session("TENANT") & "." & "TOWER WHERE TWR_CODE =SMS_SPACE_ALLOCATION.SSA_TWR_ID) AS 'Tower' " & _
                     "FROM " & Session("TENANT") & "." & "SMS_SPACEREQUISITION INNER JOIN  " & Session("TENANT") & "." & "SMS_SPACE_ALLOCATION_FOR_PROJECT ON " & _
                     "SMS_SPACEREQUISITION.SRN_REQ_ID = SMS_SPACE_ALLOCATION_FOR_PROJECT.SSA_SRNREQ_ID INNER JOIN " & _
                     "" & Session("TENANT") & "." & "SMS_SPACE_ALLOCATION ON SMS_SPACE_ALLOCATION_FOR_PROJECT.SSA_SPC_ID =SMS_SPACE_ALLOCATION.SSA_SPC_ID " & _
                     "WHERE(SRN_EXTREQ_DATE1 Is Not NULL) and  SMS_SPACE_ALLOCATION_FOR_PROJECT.SSA_STA_ID in('3','4')"

            If ddlTower.SelectedIndex > 1 Then
                strSQL = strSQL & " AND SMS_SPACE_ALLOCATION.SSA_TWR_ID = '" & ddlTower.SelectedValue & "'"
            End If

            If ddlFloor.SelectedIndex > 1 And ddlFloor.SelectedIndex <> -1 Then
                strSQL = strSQL & " AND SMS_SPACE_ALLOCATION.SSA_FLR_ID = '" & ddlFloor.SelectedValue & "'"
            End If

            If ddlWing.SelectedIndex > 1 And ddlWing.SelectedIndex <> -1 Then
                strSQL = strSQL & " AND SMS_SPACE_ALLOCATION.SSA_wng_ID = '" & ddlWing.SelectedValue & "'"
            End If

            If ddlCriteria.SelectedValue = 2 Then
                Try
                    Dim dtOnDate As Date = CType(txtOnDate.Text, DateTime)
                Catch ex As Exception
                    PopUpMessage("Please enter Date", Me)
                End Try
                strSQL = strSQL & " AND convert(nvarchar, cast(SMS_SPACEREQUISITION.SRN_REQ_DT as datetime),101) = convert(nvarchar,cast('" & txtOnDate.Text & "'  as datetime),101) "

            ElseIf ddlCriteria.SelectedValue = 1 Then
                Dim dtToDate, dtFromDate As Date
                Try
                    dtToDate = CType(txtToDate.Text, DateTime)
                Catch ex As Exception
                    PopUpMessageNormal("Please enter To Date", Me)
                End Try
                Try
                    dtFromDate = CType(txtFromDate.Text, DateTime)
                Catch ex As Exception
                    PopUpMessageNormal("Please enter From Date", Me)
                End Try
                If dtToDate < dtFromDate Then
                    PopUpMessageNormal("From Date should not be less than the To Date", Me)
                    Exit Sub
                End If
                'ondate
                strSQL = strSQL & " AND convert(nvarchar, cast(SMS_SPACEREQUISITION.SRN_REQ_DT as datetime),101) between convert(nvarchar,cast('" & txtFromDate.Text & "' as datetime),101)  and convert(nvarchar,cast('" & txtToDate.Text & "' as datetime),101) "
            End If
            BindGrid(strSQL, gvSpaceExtend)
            If gvSpaceExtend.Rows.Count = 0 Then
                PopUpMessageNormal("Data not found with the selection Criteria", Me)
                btnViewReport.Visible = True
                btnViewCriteria.Visible = False
                btnExport.Visible = False
                btnPrint.Visible = False
                tblDetails.Visible = True
            Else
                tblDetails.Visible = False
                btnViewCriteria.Visible = True
                btnExport.Visible = True
                btnPrint.Visible = True
                btnViewReport.Visible = False
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "btnViewReport_Click", exp)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "btnExport_Click", exp)
        End Try
    End Sub

    Protected Sub btnViewCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewCriteria.Click
        Try
            tblDetails.Visible = True
            btnExport.Visible = False
            btnViewCriteria.Visible = False
            btnPrint.Visible = False
            btnViewReport.Visible = True
            txtOnDate.Text = getoffsetdatetime(DateTime.Now)
            txtFromDate.Text = ""
            txtToDate.Text = ""
            ddlCriteria.SelectedIndex = 0
            trOnDate.Visible = False
            trFromDate.Visible = False
            trToDate.Visible = False
            gvSpaceExtend.DataSource = Nothing
            gvSpaceExtend.DataBind()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "btnViewCriteria_Click", exp)
        End Try
    End Sub

    Protected Sub ddlCriteria_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCriteria.SelectedIndexChanged
        Try
            If ddlCriteria.SelectedValue = 1 Then
                trOnDate.Visible = False
                trFromDate.Visible = True
                trToDate.Visible = True
                txtOnDate.Text = ""

            ElseIf ddlCriteria.SelectedValue = 2 Then
                trOnDate.Visible = True
                trFromDate.Visible = False
                trToDate.Visible = False
                txtFromDate.Text = ""
                txtToDate.Text = ""

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "ddlCriteria_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
            ddlFloor.Items.Insert(1, "--All Floors--")
            'ddlFloor.Items.Insert(0, "--Select--")
            If ddlFloor.Items.Count = 2 Then
                ddlFloor.SelectedIndex = 1
                ddlFloor_SelectedIndexChanged(sender, e)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "ddlTower_SelectedIndexChanged", exp)
        End Try
    End Sub


    Protected Sub ddlWing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            obj.bindwing(ddlWing, ddlFloor.SelectedValue)
            ddlWing.Items.Insert(0, "--All Wings--")

        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "ddlWing_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvSpaceExtend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpaceExtend.PageIndexChanging
        Try
            gvSpaceExtend.PageIndex = e.NewPageIndex
            btnViewReport_Click(sender, e)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "gvSpaceExtend_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        Try
            obj.bindwing(ddlWing, ddlFloor.SelectedValue)
            ddlWing.Items.Insert(1, "--All Wings--")
            'ddlWing.Items.Insert(0, "--Select--")
            If ddlWing.Items.Count = 2 Then
                ddlWing.SelectedIndex = 1
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repWingwiseExtension", "ddlFloor_SelectedIndexChanged", exp)
        End Try
    End Sub
End Class
