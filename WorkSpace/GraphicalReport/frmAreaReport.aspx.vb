Imports System.Data.SqlClient
Imports System.Data
Imports System.io
Imports ControlFreak
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web.Configuration
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_GraphicalReport_frmAreaReport
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            PieChart_CityAreaReport()
        End If
    End Sub

    Private Sub PieChart_CityAreaReport()

        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("cty_code")
        ds = objsubsonic.GetSubSonicDataSet("GET_FLOORAREA_DATA", param)
        Dim strPieFloorArea As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                '['Firefox',   45.0],
                strPieFloorArea = strPieFloorArea & "['" & ds.Tables(0).Rows(i).Item("FLR_BDG_ID") & "'," & ds.Tables(0).Rows(i).Item("FLR_AREA") & "],"
            Next
            If strPieFloorArea.EndsWith(",") = True Then
                strPieFloorArea = strPieFloorArea.Substring(0, strPieFloorArea.Length - 1)
            End If

        Else
            lblmsg.Text = "Area is not available for the selected city."
        End If

        Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/PieChart_CityAreaReport_legend.txt")
        Dim TextLine As String = ""
        If System.IO.File.Exists(FILE_NAME) = True Then
            Dim objReader As New System.IO.StreamReader(FILE_NAME)
            Do While objReader.Peek() <> -1
                TextLine = TextLine & objReader.ReadLine().Replace("<PieFloorArea>", strPieFloorArea.ToString)
            Loop
            litArea.Text = TextLine
            objReader.Close()
            objReader.Dispose()
        End If

       
    End Sub

End Class
