<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmLocationWiseReport.aspx.vb" Inherits="WorkSpace_GraphicalReport_frmLocationWiseReport"
    Title="Graphical Reports" %>

<%@ Register Src="Controls/GraphicalReport.ascx" TagName="LocationWiseReport" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
  <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/Scripts/highcharts.js"></script>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Floor Allocation Report 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <uc1:LocationWiseReport ID="LocationWiseReport1" runat="server" />
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>
  
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Report</h4>
                </div>
                <div id="modelcontainer">
                </div>
            </div>
        </div>
    </div>
    
     
    <%-- Modal popup block--%>
    <script>
        function showPopWin(ctrl) {
            $("#modelcontainer").load(ctrl.href, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal();
            });
            return false;
        }
        $(".close").click(function () {
            window.location = "../../Workspace/GraphicalReport/frmLocationWiseReport.aspx"
        });
    </script>

</body>
</html>
