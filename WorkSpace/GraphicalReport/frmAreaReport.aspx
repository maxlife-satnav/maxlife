<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAreaReport.aspx.vb" Inherits="WorkSpace_GraphicalReport_frmAreaReport" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)"> 
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <form id="form1" runat="server">
        

        <div>
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label text-center" ForeColor="Red">
                        </asp:Label>
                    </div>
                </div>
            </div>
        </div>
            <asp:Literal ID="litArea" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
