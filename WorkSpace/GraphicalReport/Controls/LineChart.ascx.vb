Imports System.Data.SqlClient
Imports System.Data
Imports System.io
Imports ControlFreak
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web.Configuration
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_GraphicalReport_Controls_LineChart
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim RandomClass As New Random()
    Dim intR, intG, intB As Integer

    Dim filepath As String
    Dim strtxt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            t1.Visible = False
            t2.Visible = False

            BindLocation()
            litBarChart.Text = ""
            txtFdate.Attributes.Add("onClick", "displayDatePicker('" + txtFdate.ClientID + "')")
            txtFdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtTdate.Attributes.Add("onClick", "displayDatePicker('" + txtTdate.ClientID + "')")
            txtTdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        End If
    End Sub
    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_BIND_LOCATION_HISTORY1")
            ddllocation.DataSource = sp.GetDataSet()
            ddllocation.DataTextField = "LCM_NAME"
            ddllocation.DataValueField = "LCM_CODE"
            ddllocation.DataBind()
            ddllocation.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Location_History_Total_OccupiedChart()
        Try
            Dim ds As New DataSet
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
            param(0).Value = txtFdate.Text
            param(1) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
            param(1).Value = txtTdate.Text
            param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar)
            param(2).Value = ddllocation.SelectedItem.Value
            ds = objsubsonic.GetSubSonicDataSet("GET_BARGRAPH_DATA_OCCUPIED_LOCATION", param)
            'Dim strdata As String = ""
            
            Dim strdata_lineAvailable As String = ""
            Dim strdata_lineOccupied As String = ""
            Dim strTotalAvailable As String = ""
            Dim strTotalOccupied As String = ""
            Dim strLineLocationArea As String = ""
            Dim strLineLocationArea1 As String = ""
    
            If ds.Tables(0).Rows.Count > 0 Then
              
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    strdata_lineAvailable = strdata_lineAvailable & "['" & ds.Tables(0).Rows(i).Item("MODIFIED_DATE") & "'," & ds.Tables(0).Rows(i).Item("TOTALVACANT") & "],"
                    strdata_lineOccupied = strdata_lineOccupied & "['" & ds.Tables(0).Rows(i).Item("MODIFIED_DATE") & "'," & ds.Tables(0).Rows(i).Item("TOTALOCCUPIED") & "],"
                Next
                If strdata_lineAvailable.EndsWith(",") = True Then
                    strdata_lineAvailable = strdata_lineAvailable.Substring(0, strdata_lineAvailable.Length - 1)
                End If
                If strdata_lineOccupied.EndsWith(",") = True Then
                    strdata_lineOccupied = strdata_lineOccupied.Substring(0, strdata_lineOccupied.Length - 1)
                End If
            End If
            Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/LineChart_DateWise.txt")
            Dim TextLine As String = ""
            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim objReader As New System.IO.StreamReader(FILE_NAME)
                Do While objReader.Peek() <> -1

                    TextLine = TextLine & objReader.ReadLine().Replace("<Data1>", strdata_lineAvailable.ToString).Replace("<Data2>", strdata_lineOccupied.ToString)
                Loop
                litBarChart.Text = TextLine
                If litBarChart.Text <> "" Then
                    t1.Visible = True
                    t2.Visible = True
                Else
                    t1.Visible = False
                    t2.Visible = False
                End If
                
                objReader.Close()
                objReader.Dispose()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If txtFdate.Text <> "" And txtTdate.Text <> "" And ddllocation.SelectedIndex > 0 Then
                If txtFdate.Text > txtTdate.Text Then
                    litBarChart.Text = ""
                    lblMsg.Text = "To Date Should be greater than from date"
                Else
                    lblMsg.Text = ""

                    Location_History_Total_OccupiedChart()
                End If

            Else
                litBarChart.Text = ""

                lblMsg.Text = "Pleas Fill all the mandatory Fields"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    
    

End Class
