<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LineChart.ascx.vb" Inherits="WorkSpace_GraphicalReport_Controls_LineChart" %>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

    <script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">  Graphical Report
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong> Graphical Report</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
              <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                 <table id="tab" runat="server" cellpadding="2" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    From Date(MM/DD/YYYY)<font class=clsNote>*</font>
                                    <asp:RequiredFieldValidator ID="rfvfdate" runat="Server" ControlToValidate="txtFdate" ErrorMessage="Please Select From Date" Display="None" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox ID="txtFdate" runat="server" CssClass="clsTextField" Width="97%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    To Date(MM/DD/YYYY)<font class=clsNote>*</font>
                                       <asp:RequiredFieldValidator ID="rfvtdate" runat="Server" ControlToValidate="txtTdate" ErrorMessage="Please Select To Date" Display="None" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox ID="txtTdate" runat="server" CssClass="clsTextField" Width="97%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Select Location<font class=clsNote>*</font>
                                    
                                     <asp:RequiredFieldValidator ID="rfvlocation" runat="Server" ControlToValidate="ddllocation" ErrorMessage="Please Select Location" InitialValue="--Select--" Display="None" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:DropDownList ID="ddllocation" runat="server" CssClass="clsComboBox" Width="97%">
                                    </asp:DropDownList>
                                    
                             <%--  <asp:Literal ID="litCtrl" runat="server"></asp:Literal>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnSubmit" runat="Server" CssClass="button" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                                </td>
                            </tr>
                                <tr runat="server" id="t1">
                                <td align="right" width="25%" colspan="2">
                                Green -Occupied
                              
                                </td>
                                </tr>
                                <tr runat="server" id="t2">
                                <td align="right" width="25%" colspan="2">
                                  Blue -Vacant
                                </td>
                                </tr>
                            <tr>
                                <td align="center" valign="top" colspan="2">
                                    <asp:Label ID="lblReport" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" colspan="2" style="height: 23px">
                                    <asp:Literal ID="litBarChart" runat="server"></asp:Literal>
                                    &nbsp;</td>
                            </tr>
                        </table>
                <%--<table cellspacing="2" cellpadding="2" width="100%" border="1" style="border-collapse: collapse">
                    <tr>
                        <td align="center" valign="top" colspan="2">
                            <asp:Label ID="lblmsg" CssClass="error" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <asp:Literal ID="litArea" runat="server"></asp:Literal>
                        </td>
                        <td align="center" valign="top">
                            
                        </td>
                    </tr>
                </table>--%>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</div>
