<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GraphicalReport.ascx.vb"
    Inherits="WorkSpace_GraphicalReport_Controls_GraphicalReport" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    City <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity"
                    Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:Literal ID="litCtrl" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvLoc" runat="server" ControlToValidate="ddlLocation"
                    Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnTowerWise" runat="server" Text="View Tower Wise Report" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />

            <asp:Button ID="btnFloorWise" runat="server" Text="View Floor Wise Report" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
        </div>
    </div>
</div>


<div>
    <asp:Literal ID="litBarChart" runat="server"></asp:Literal>
</div>
