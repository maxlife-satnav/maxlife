Imports System.Data.SqlClient
Imports System.Data
Imports System.io
Imports ControlFreak
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web.Configuration
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_GraphicalReport_Controls_GraphicalReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim RandomClass As New Random()
    Dim intR, intG, intB As Integer

    Dim strdata As String
     
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If ddlCity.SelectedItem IsNot Nothing Then
            'litCtrl.Text = "<a href='#' title='City Wise Area  Report' onclick=""showPopWin('frmAreaReport.aspx?cty_code=" & ddlCity.SelectedItem.Value & "',850,450,'')"">City Wise Area  Report</a> | <a href='#' title='City Wise Allocation Report' onclick=""showPopWin('CityReport.aspx?cty_code=" & ddlCity.SelectedItem.Value & "',850,470,'')"">City Wise Allocation Report</a> "
            litCtrl.Text = "<a href='frmAreaReport.aspx?cty_code=" & ddlCity.SelectedItem.Value & "' title='City Wise Area  Report' onclick=""return showPopWin(this)"">City Wise Area  Report</a> | <br> <a href='CityReport.aspx?cty_code=" & ddlCity.SelectedItem.Value & "' title='City Wise Allocation Report' onclick=""return showPopWin(this)"">City Wise Allocation Report</a> "
        Else
            litCtrl.Text = "<a href='#' onclick=""alert('Select City')"" >City Wise Area  Report</a> | <br> <a href='#'  onclick=""alert('Select City')"">City Wise Allocation Report</a>"
        End If

        If Not IsPostBack Then
            BindCities()
        End If

    End Sub

    Public Sub BindCities()
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")

        'ddlCity.Items.Insert(1, "--All--")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        lblmsg.Text = ""
        litBarChart.Text = ""

        ddlLocation.Items.Clear()
        BindLocation(ddlCity.SelectedItem.Value)
        If ddlLocation.Items.Count = 0 Then
            lblmsg.Text = "No Locations Available"
            ddlLocation.Items.Insert(0, "--Select--")
        End If
    End Sub

    Public Sub BindLocation(ByVal strCity As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strCity
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONBY_CTY", "LCM_NAME", "LCM_CODE", param)
        'ddlLocation.Items.Insert(1, "--All--")
    End Sub

    Private Function RandomColor() As String
        intR = RandomClass.Next(0, 256)
        intG = RandomClass.Next(0, 256)
        intB = RandomClass.Next(0, 256)
        Dim hexR, hexG, hexB As String
        hexR = intR.ToString("X").PadLeft(2, "0")
        hexG = intG.ToString("X").PadLeft(2, "0")
        hexB = intB.ToString("X").PadLeft(2, "0")
        Dim strColor As String
        strColor = "#" & hexR & hexG & hexB
        Return strColor
    End Function

    Protected Sub btnTowerWise_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTowerWise.Click
        If ddlCity.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Select City."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If

        If ddlLocation.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Select Location."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If


        TowerWise_Total_OccupiedChart()
    End Sub
 
    Private Sub TowerWise_Total_OccupiedChart()
        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        ds = objsubsonic.GetSubSonicDataSet("GET_BARGRAPH_DATA_OCCUPIED_TOWER", param)

        Dim strDataCategories As String = ""
        Dim strTotalAvailable As String = ""
        Dim strTotalOccupied As String = ""
        Dim strTotalAlloted As String = ""
        Dim strTotalVacant As String = ""

        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                strDataCategories = strDataCategories & "'" & ds.Tables(0).Rows(i).Item("FLR_NAME") & "',"
                strTotalAvailable = strTotalAvailable & ds.Tables(0).Rows(i).Item("TOTAL_TOTAL") & ","
                strTotalOccupied = strTotalOccupied & ds.Tables(0).Rows(i).Item("OCC_TOTAL") & ","
                strTotalAlloted = strTotalAlloted & ds.Tables(0).Rows(i).Item("ALLOT_TOTAL") & ","
                strTotalVacant = strTotalVacant & ds.Tables(0).Rows(i).Item("VACANT_TOTAL") & ","
            Next

            If strDataCategories.EndsWith(",") = True Then
                strDataCategories = strDataCategories.Substring(0, strDataCategories.Length - 1)
            End If

            If strTotalAvailable.EndsWith(",") = True Then
                strTotalAvailable = strTotalAvailable.Substring(0, strTotalAvailable.Length - 1)
            End If

            If strTotalOccupied.EndsWith(",") = True Then
                strTotalOccupied = strTotalOccupied.Substring(0, strTotalOccupied.Length - 1)
            End If

            If strTotalAlloted.EndsWith(",") = True Then
                strTotalAlloted = strTotalAlloted.Substring(0, strTotalAlloted.Length - 1)
            End If

            If strTotalVacant.EndsWith(",") = True Then
                strTotalVacant = strTotalVacant.Substring(0, strTotalVacant.Length - 1)
            End If

            Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/BarChart_TowerReport.txt")
            Dim TextLine As String = ""
            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim objReader As New System.IO.StreamReader(FILE_NAME)
                Do While objReader.Peek() <> -1
                    TextLine = TextLine & objReader.ReadLine().Replace("<Allocated>", strTotalAlloted.ToString).Replace("<Occupied>", strTotalOccupied.ToString).Replace("<Vacant>", strTotalVacant.ToString).Replace("<DataCategories>", strDataCategories.ToString)
                Loop
                litBarChart.Text = TextLine
                objReader.Close()
                objReader.Dispose()
            End If

        Else
            litBarChart.Text = "Data Not Found."



        End If



    End Sub

    Private Sub FloorWise_Total_OccupiedChart()
        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        ds = objsubsonic.GetSubSonicDataSet("GET_BARGRAPH_DATA_OCCUPIED_FLOOR", param)

        Dim strDataCategories As String = ""
        Dim strTotalAvailable As String = ""
        Dim strTotalOccupied As String = ""
        Dim strTotalAlloted As String = ""
        Dim strTotalVacant As String = ""
 
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                strDataCategories = strDataCategories & "'" & ds.Tables(0).Rows(i).Item("FLR_NAME") & "',"
                strTotalAvailable = strTotalAvailable & ds.Tables(0).Rows(i).Item("TOTAL_TOTAL") & ","
                strTotalOccupied = strTotalOccupied & ds.Tables(0).Rows(i).Item("OCC_TOTAL") & ","
                strTotalAlloted = strTotalAlloted & ds.Tables(0).Rows(i).Item("ALLOT_TOTAL") & ","
                strTotalVacant = strTotalVacant & ds.Tables(0).Rows(i).Item("VACANT_TOTAL") & ","
            Next

            If strDataCategories.EndsWith(",") = True Then
                strDataCategories = strDataCategories.Substring(0, strDataCategories.Length - 1)
            End If

            If strTotalAvailable.EndsWith(",") = True Then
                strTotalAvailable = strTotalAvailable.Substring(0, strTotalAvailable.Length - 1)
            End If

            If strTotalOccupied.EndsWith(",") = True Then
                strTotalOccupied = strTotalOccupied.Substring(0, strTotalOccupied.Length - 1)
            End If

            If strTotalAlloted.EndsWith(",") = True Then
                strTotalAlloted = strTotalAlloted.Substring(0, strTotalAlloted.Length - 1)
            End If

            If strTotalVacant.EndsWith(",") = True Then
                strTotalVacant = strTotalVacant.Substring(0, strTotalVacant.Length - 1)
            End If

            Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/BarChart_FloorReport.txt")
            Dim TextLine As String = ""
            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim objReader As New System.IO.StreamReader(FILE_NAME)
                Do While objReader.Peek() <> -1
                    TextLine = TextLine & objReader.ReadLine().Replace("<Allocated>", strTotalAlloted.ToString).Replace("<Occupied>", strTotalOccupied.ToString).Replace("<Vacant>", strTotalVacant.ToString).Replace("<DataCategories>", strDataCategories.ToString)
                Loop
                litBarChart.Text = TextLine
                objReader.Close()
                objReader.Dispose()
            End If

        Else
            litBarChart.Text = "Data Not Found."



        End If



    End Sub

    Protected Sub btnFloorWise_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFloorWise.Click
        FloorWise_Total_OccupiedChart()
    End Sub

End Class
