Imports System.Data.SqlClient
Imports System.Data
Imports System.io
Imports ControlFreak
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web.Configuration
Imports clsSubSonicCommonFunctions



Partial Class WorkSpace_GraphicalReport_Controls_LocationWiseReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim RandomClass As New Random()
    Dim intR, intG, intB As Integer

    Dim filepath As String
    Dim strtxt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindCities()
        End If
 
    End Sub

    Public Sub BindCities()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")
        'ddlCity.Items.Insert(1, "--All--")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        BindLocation(ddlCity.SelectedItem.Value)
    End Sub

    Public Sub BindLocation(ByVal strCity As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strCity
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONBY_CTY", "LCM_NAME", "LCM_CODE", param)
        'ddlLocation.Items.Insert(1, "--All--")
    End Sub

    Public Sub LocationFile()
        Try
            Dim ds As New DataSet
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCity.SelectedItem.Value
            param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
            param(1).Value = ddlLocation.SelectedItem.Value
            ds = objsubsonic.GetSubSonicDataSet("GET_BARGRAPH_DATA_OCCUPIED", param)
            Dim strdata As String = ""
            Dim strColor As String = "["
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    strdata = strdata & "['" & ds.Tables(0).Rows(i).Item("FLR_NAME") & "'," & ds.Tables(0).Rows(i).Item("TOTAL_TOTAL") & "," & ds.Tables(0).Rows(i).Item("OCC_TOTAL") & "],"

                    strColor = strColor & "'" & RandomColor() & "',"
                Next

                If strdata.EndsWith(",") = True Then
                    strdata = strdata.Substring(0, strdata.Length - 1)
                    strColor = strColor.Substring(0, strColor.Length - 1)
                    strColor = strColor & "]"
                End If
                Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/LocationWiseReport.txt")
                Dim TextLine As String = ""
                If System.IO.File.Exists(FILE_NAME) = True Then
                    Dim objReader As New System.IO.StreamReader(FILE_NAME)
                    Do While objReader.Peek() <> -1
                        'TextLine = TextLine & objReader.ReadLine().Replace("<data>", "['Ground Floor', 21], ['First Floor', 28], ['Second Floor', 12], ['Third Floor', 17], ['Fourth Floor', 47], ['Fifth Floor', 37]") & vbNewLine
                        TextLine = TextLine & objReader.ReadLine().Replace("<data>", strdata.ToString).Replace("<color>", strColor.ToString) & vbNewLine
                    Loop
                    litBarChart.Text = TextLine
                    objReader.Close()
                    objReader.Dispose()
                End If
            Else
                litBarChart.Text = ""


            End If


           

        Catch ex As Exception

        End Try
       
        
    End Sub

    Private Function RandomColor() As String
        intR = RandomClass.Next(0, 256)
        intG = RandomClass.Next(0, 256)
        intB = RandomClass.Next(0, 256)
        Dim hexR, hexG, hexB As String
        hexR = intR.ToString("X").PadLeft(2, "0")
        hexG = intG.ToString("X").PadLeft(2, "0")
        hexB = intB.ToString("X").PadLeft(2, "0")
        Dim strColor As String
        strColor = "#" & hexR & hexG & hexB
        Return strColor
    End Function





    Public Sub LocationFile_PieChart()
        Try

            Dim ds As New DataSet
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCity.SelectedItem.Value
            ds = objsubsonic.GetSubSonicDataSet("GET_CityGRAPH_DATA_OCCUPIED", param)
            Dim strdata As String = ""
            Dim strColor As String = "["

            Dim TOTAL_SEATS As String = ""
            Dim OCC_TOTAL As String = ""
            Dim VACANT_TOTAL As String = ""

            If ds.Tables(0).Rows.Count > 0 Then
                TOTAL_SEATS = ds.Tables(0).Rows(0).Item("TOTAL SEATS")
                OCC_TOTAL = ds.Tables(0).Rows(0).Item("TOTAL OCCUPIED")
                VACANT_TOTAL = ds.Tables(0).Rows(0).Item("TOTAL VACANT")

                If TOTAL_SEATS = "0" And OCC_TOTAL = "0" And VACANT_TOTAL = "0" Then
                    litPieChart.Text = ""
                Else


                    strdata = "['" & ds.Tables(0).Columns(0).ColumnName & "'," & TOTAL_SEATS & "]," & "['" & ds.Tables(0).Columns(1).ColumnName & "'," & OCC_TOTAL & "]," & "['" & ds.Tables(0).Columns(2).ColumnName & "'," & VACANT_TOTAL & "],"
                    RandomColor()
                    strColor = strColor & "'" & RandomColor() & "'," & "'" & RandomColor() & "'," & "'" & RandomColor() & "',"

                    If strdata.EndsWith(",") = True Then
                        strdata = strdata.Substring(0, strdata.Length - 1)
                        strColor = strColor.Substring(0, strColor.Length - 1)
                        strColor = strColor & "]"
                    End If


                    Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/LocationWiseReport_Pie.txt")
                    Dim TextLine As String = ""
                    If System.IO.File.Exists(FILE_NAME) = True Then
                        Dim objReader As New System.IO.StreamReader(FILE_NAME)
                        Do While objReader.Peek() <> -1
                            'TextLine = TextLine & objReader.ReadLine() & vbNewLine
                            TextLine = TextLine & objReader.ReadLine().Replace("<data>", strdata.ToString).Replace("<color>", strColor.ToString) & vbNewLine
                        Loop
                        litPieChart.Text = TextLine
                        objReader.Close()
                        objReader.Dispose()
                    End If
                End If
 
            End If


        Catch ex As Exception

        End Try

    End Sub



    Public Sub LocationFileArea_PieChart()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ds = objsubsonic.GetSubSonicDataSet("GET_FLOORAREA_DATA", param)
        Dim strdata As String = ""
        Dim strColor As String = "["
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                strdata = strdata & "['" & ds.Tables(0).Rows(i).Item("FLR_BDG_ID") & "'," & ds.Tables(0).Rows(i).Item("FLR_AREA") & "],"
                RandomColor()
                strColor = strColor & "'" & RandomColor() & "',"
            Next

            If strdata.EndsWith(",") = True Then
                strdata = strdata.Substring(0, strdata.Length - 1)
                strColor = strColor.Substring(0, strColor.Length - 1)
                strColor = strColor & "]"
            End If


            Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/CityWiseAreaReport_Pie.txt")
            Dim TextLine As String = ""
            If System.IO.File.Exists(FILE_NAME) = True Then
                Dim objReader As New System.IO.StreamReader(FILE_NAME)
                Do While objReader.Peek() <> -1

                    TextLine = TextLine & objReader.ReadLine().Replace("<data>", strdata.ToString).Replace("<color>", strColor.ToString) & vbNewLine
                Loop
                litArea.Text = TextLine
                objReader.Close()
                objReader.Dispose()
                bindgridFloorArea()
            End If
        Else
            litArea.Text = ""
        End If
    End Sub


    Private Sub bindgridFloorArea()

        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubSonic.BindGridView(gvFloorArea, "GET_FLOORAREA_DATA", param)


    End Sub

    Protected Sub btnSubmit_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click


        If ddlCity.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Select City."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If


        If ddlLocation.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Select Location."
            Exit Sub
        Else
            lblmsg.Text = ""
        End If

        LocationFile()
        LocationFile_PieChart()
        LocationFileArea_PieChart()

        lblReport.Text = "<h3>Graphical report for <b>" & ddlCity.SelectedItem.Text & "</b> city &  <b>" & ddlLocation.SelectedItem.Text & "</b> location.</h3>"


    End Sub

    Protected Sub lnkPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPopup.Click
        Response.Write("<script language=javascript>javascript:window.open('frmAreaReport.aspx','Window','toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=790,height=550')</script>")
    End Sub

    Protected Sub gvFloorArea_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFloorArea.PageIndexChanging
        gvFloorArea.PageIndex = e.NewPageIndex
        bindgridFloorArea()
    End Sub
End Class
