<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LocationWiseReport.ascx.vb"
    Inherits="WorkSpace_GraphicalReport_Controls_LocationWiseReport" %>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">  Graphical Report
            </asp:Label>
            <hr align="center" width="60%" />
            <br />
        </td>
    </tr>
</table>
<table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
    border="0">
    <tr>
        <td>
            <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                width="9" /></td>
        <td width="100%" class="tableHEADER" align="left">
            &nbsp;<strong> Graphical Report</strong>
        </td>
        <td>
            <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                width="16" /></td>
    </tr>
    <tr>
        <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
            &nbsp;</td>
        <td align="left">
            <table cellspacing="2" cellpadding="2" width="100%" border="1" style="border-collapse: collapse">
                <tr>
                    <td valign="top" colspan="2" align="left" class="bodytext">
                        City <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="true">
                        </asp:DropDownList><asp:LinkButton ID="lnkPopup"   Text="City Wise Report" runat="server"></asp:LinkButton>
                    </td>
                   
                </tr>
                <tr>
                    <td valign="top"  colspan="2" align="left" class="bodytext">
                        Location <asp:DropDownList ID="ddlLocation" runat="server">
                        </asp:DropDownList>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsButton" />
                        
                    </td>
                    
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="2">
                        <asp:Label ID="lblmsg" CssClass="error" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="2">
                        <asp:Label ID="lblReport" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="width: 50%">
                        <asp:Literal ID="litBarChart" runat="server"></asp:Literal>
                    </td>
                    <td align="center" valign="top" style="width: 50%">
                        <asp:Literal ID="litPieChart" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Literal ID="litArea" runat="server"></asp:Literal>
                    </td>
                    <td align="center" valign="top">
                        <asp:GridView ID="gvFloorArea" runat="server"  PageSize="17" AllowPaging="true" AutoGenerateColumns="False"
                            EmptyDataText="No records found.">
                            <Columns>
                                <asp:BoundField DataField="FLR_BDG_ID" HeaderText="Sol ID"></asp:BoundField>
                                <asp:BoundField DataField="FLR_AREA" HeaderText="Floor Area (SQFT)"></asp:BoundField>
                            </Columns>
                            <PagerStyle BackColor="#CCCCCC" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
        <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
            height: 100%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width: 10px; height: 17px;">
            <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                width="9" /></td>
        <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
            <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                width="25" /></td>
        <td style="height: 17px; width: 17px;">
            <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                width="16" /></td>
    </tr>
</table>
