Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports ControlFreak
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web.Configuration
Imports clsSubSonicCommonFunctions



Partial Class WorkSpace_GraphicalReport_CityReport
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            PieChart_CityAllocationReport_SEZ("SEZ")
        End If
    End Sub

    Private Sub PieChart_CityAllocationReport_SEZ(ByVal lcm_type As String)


        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("cty_code")
        param(1) = New SqlParameter("@LCM_TYPE_ID", SqlDbType.NVarChar, 200)
        param(1).Value = lcm_type
        ds = ObjSubSonic.GetSubSonicDataSet("GET_CityGRAPH_DATA_OCCUPIED_STPI", param)
        Dim strdata As String = ""
        Dim strColor As String = "["


        Dim OCC_TOTAL As String = ""
        Dim VACANT_TOTAL As String = ""
        Dim TOTAL As String = ""
        Dim ALLOTED_TOTAL As String = ""
        Dim strPieFloorArea As String = ""

        If ds.Tables(0).Rows.Count > 0 Then
            OCC_TOTAL = ds.Tables(0).Rows(0).Item("OCCUPIED")
            VACANT_TOTAL = ds.Tables(0).Rows(0).Item("VACANT")
            TOTAL = ds.Tables(0).Rows(0).Item("TOTAL")
            ALLOTED_TOTAL = ds.Tables(0).Rows(0).Item("ALLOTED")

            If OCC_TOTAL = "0" And VACANT_TOTAL = "0" Then
                litArea.Text = "NO RECORD FOUND "
                Exit Sub
            Else
                strPieFloorArea = "['Allocated'," & ALLOTED_TOTAL & "],['Occupied'," & OCC_TOTAL & "],['Vacant'," & VACANT_TOTAL & "]"
            End If
            If strPieFloorArea.EndsWith(",") = True Then
                strPieFloorArea = strPieFloorArea.Substring(0, strPieFloorArea.Length - 1)
            End If

        Else
            lblmsg.Text = "No Allocation for the selected city"
        End If

        Dim FILE_NAME As String = Server.MapPath("~/WorkSpace/GraphicalReport/sources/PieChart_CityAllocation_SEZ.txt")
        Dim TextLine As String = ""
        If System.IO.File.Exists(FILE_NAME) = True Then
            Dim objReader As New System.IO.StreamReader(FILE_NAME)
            Do While objReader.Peek() <> -1
                TextLine = TextLine & objReader.ReadLine().Replace("<PieFloorArea>", strPieFloorArea.ToString)
            Loop
            litArea.Text = TextLine
            objReader.Close()
            objReader.Dispose()
        End If
    End Sub


End Class
