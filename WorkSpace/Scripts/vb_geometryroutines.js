function map_onViewChanged(map)
{
	onViewChanged(map)
}


Function map_onDigitizedPolyline( map, numPoints, points)
{
	onDigitizedPolyline map, numPoints, points
}

Function map_onDoubleClickObject(mapObj)
{
	mapObj.DoubleClickHandled = onDoubleClickObject(mapObj)
}


function map_onDigitizedPoint(Map, Point)
{
	onDigitizedPoint Map, Point
}

function map_onDigitizedPolygon( map, numPoints, points)
{
	onDigitizedPolygon map, numPoints, points
}