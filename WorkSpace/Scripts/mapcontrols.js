
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Get the map object...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
this.ts = 0;
function getMap()
{
    
    if (navigator.appName == "Netscape")
	{
        return top.main.document.map;
	}
    else
	{
        return parent.main.document.map;
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Get the overview map object...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function getOverviewMap()
{
    if (navigator.appName == "Netscape")
        return top.overview.document.map;
    else
        return top.overview.map;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Zoom to the selected map features...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function zoomSelected()
{
    var map = getMap();
	var selected = map.getSelection().getMapObjectsEx(null);
	if (selected.size()>-1)
		map.zoomSelected();
	else
		alert("nothing selected!");

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Zoom in...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function setZoomInMode()
	{
		var map = getMap();
		map.zoomInMode();
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Zoom out...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function setZoomOutMode()
	{
		var map = getMap();
		map.zoomOutMode();
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Zoom extents...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function setZoomExtents()
	{
		ClearAll();
		var map = getMap();
		map.zoomOut();
		
	}
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Zoom previous...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function setZoomPrevious()
	{
		var map = getMap();
		map.zoomPrevious();
	}
	
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Pan...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function setPanMode()
	{
		var map = getMap();
		map.panMode();
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Select Mode...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function setSelectMode()
	{
		var map = getMap();
		map.selectMode();
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//View Distance Mode...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function setViewDistanceMode()
	{
		var map = getMap();
		map.viewDistance("M");
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Print Map...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	function PrintMap()
	{
		var map = getMap();
		map.printDlg();
	}
	
	

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Clear  Selected...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	function ClearAll()
	{
		var map = getMap();
		var mapSel = map.getSelection();
		mapSel.clear();
	} 


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Reload Map...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	function ReloadMap()
	{	   	  
		var map =  getMap();		
		map.setAutoRefresh(false);		
		var layers =  map.getMapLayersEx();
				
		for (var i=0;i<layers.size();i++)
		{
			var thislayer = layers.item(i);
			thislayer.setRebuild(true);
		}
		
		map.zoomOut();
		map.zoomPrevious();
		map.setAutoRefresh(true);
		map.refresh();			
	}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Digitize Function...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	function digitizeLine()
	{
	    
	  	getMap().digitizePolyline();
	  	
	}


	function digitizePoint()
	{
		
		getMap().digitizePoint();
		
		
	}

	function digitizePolygon()
	{
      if (navigator.appName == "Netscape")
        getMap().digitizePolygonEx(parent.mapframe.document.obs);
    else
        
        getMap().digitizePolygon();
        

	}



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Digitize  Polyline...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	function onDigitizedPolyline (map, numPoints, points)
	{
	   
		var str_x="";
		var str_y="";
	    
		top.Line.document.form1.txtPointCount.value = numPoints;
		for (i = 0; i < numPoints; i++)
		{
			str_y = str_y + points.item(i).getY() + ",";
			str_x = str_x + points.item(i).getX() + ",";
		}
		
		top.Line.document.form1.txtXValues.value = str_y;
		top.Line.document.form1.txtYValues.value = str_x;
	}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Digitize  Polygon...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	function onDigitizedPolygon (map, numPoints, points)
	{
	    
		var str_x="";
		var str_y="";
	    
		top.Polygon.document.form1.txtPointCount.value = numPoints;
		alert(numPoints);
		for (i = 0; i < numPoints; i++)
		{
			str_y = str_y + points.item(i).getY() + ",";
			str_x = str_x + points.item(i).getX() + ",";
		}
		top.Polygon.document.form1.txtXValues.value = str_y;
		top.Polygon.document.form1.txtYValues.value = str_x;
		
	}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Digitize  Point...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//function onDigitizedPoint(map, points)
//{
//    alert("You just digitized a point. The coordinates are:\n\nLAT = " + points.getY()
//    + " , " + "LON = " + points.getX());
//}

	function onDigitizedPoint (map, points)
	{
	    
		top.Line.document.form1.txtId.value = points.getX();
				
//        top.Asset.document.form1.txtXValues.value = points.getX();
//        top.Asset.document.form1.txtYValues.value = points.getY();
        
        top.Point.document.form1.txtXValues.value = points.getX();
        top.Point.document.form1.txtYValues.value = points.getY();
       alert(top.Point.document.form1.txtXValues.value + ' and  ' + top.Point.document.form1.txtYValues.value);
        top.Point.document.form1.txtPointCount.value = 0;

      }
     

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Update Function...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	function updateLineMap() // Updates the map after an Add or Remove
	{
		getMap().getMapLayer("PAR").setRebuild(true);
		getMap().refresh();
	}

	function updatePolygonMap(Layer) // Updates the map after an Add or Remove
	{
		//alert("asdf");
		getMap().getMapLayer(Layer).setRebuild(true);
		getMap().refresh();
	}

	function updatePointMap(Layer) // Updates the map after an Add or Remove
	{
	    
		getMap().getMapLayer(Layer).setRebuild(true);
		getMap().refresh();
		
	}
   

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Get Selection Function...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	function GetSelection(Space,layer)
	{
		
		var map = getMap();
		var mapSel = map.getSelection();
		var mapLayer = map.getMapLayer(layer);
	
		var mapObjects = map.createObject("MGCollection");
		var spcKeys = Space.split(",");
		var sKey = 0;
		while (sKey < spcKeys.length)
		{
	
	var mapObj = mapLayer.getMapObject(spcKeys[sKey]);
    		if (mapObj != null)
			mapObjects.add(mapObj);
			sKey += 1;
		}
		mapSel.clear();
		if (mapObjects.size() > 0)
			mapSel.addObjectsEx(mapObjects, false);
			zoomSelected();

	}


// use this function for getting all the destination x, y coordinates one by one
// to use in the move feature functionality
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function onDigitizedPolygon1 (map, numPoints, points)
	{
//	    alert(numPoints);
	    var str_x="";
		var str_y="";
		var str_x1="";
		var str_y1="";
		var str_x2;
		var str_y2;
		// to hold the x,y coordinates values of the destination point
		var xVal;
		var yVal;
		
		top.Point.document.form1.txtPointCount.value = numPoints;
				
		//alert(top.Point.document.form1.txtPointCount.value);
		
		xVal = top.Point.document.form1.txtXValues.value;
		yVal = top.Point.document.form1.txtYValues.value;	
				
		//alert(xVal);
		//alert(yVal);
		
	// to hold all vertices x,y coordinates values of the selected object 
		var xVal1;
		var yVal1;
		
		xVal1 = points.item(0).getX();
		yVal1 = points.item(0).getY();	
			
		//alert(xVal1);
		//alert(yVal1);
		
		// calculate the difference in coordinates values		
		//str_x1 = xVal - xVal1;
		//str_y1= yVal - yVal1;			
		
		for (i = 0; i < numPoints; i++)
		{	
		       	
			str_y = str_y + points.item(i).getY() + ","; 
			str_x = str_x + points.item(i).getX() + ",";
			
			if (i > 0)
			{
			str_x2 = parseFloat(xVal) + parseFloat(points.item(i).getX()) - parseFloat(points.item(i-1).getX());
			str_x1 = str_x1 + str_x2 + ",";	
			xVal = str_x2;			
			
			str_y2 = parseFloat(yVal) + parseFloat(points.item(i).getY()) - parseFloat(points.item(i-1).getY());
			str_y1 = str_y1 + str_y2 + ",";	
			yVal = str_y2;			
			
			}    
		}
		
		top.Point.document.form1.txtXValues.value = yVal + "," + str_y1;
		top.Point.document.form1.txtYValues.value = xVal + "," + str_x1;
		
			//alert(top.Polygon.document.form1.txtXValues.value);
	        //alert(top.Polygon.document.form1.txtYValues.value);
	}

//----------------------
// This functions is for getting the selected objects Keys
//---------------------
	

	function doGetKey()  //pavan added this function on 25th april
			{		
//			alert('Entered');
				var map = getMap();	
				var sel = map.getSelection();
				var objs = sel.getMapObjectsEx(null);
				var cntObjects = objs.size();
				var objkeys = "";			
				var keys="";
			// select the workspace in decending order
				for (t=cntObjects;t>0;t--)
				{
					var obj=objs.item(t-1);
					var key=obj.getKey();
					if(keys!="")													
					keys=keys+","+key;
					else
					keys=key;
				}																			
					//keys=keys+"'";			
					if (cntObjects > 0)	
					{		
					   			
//						top.SelectFloors.document.form1.TxtKey1.value = key;
//alert(top.SelectFloors.document.form1.drpdwnFlm_Id.value);
//                        alert(keys);
						createCookie("WstID",keys,1);
						var fInfo = top.SelectFloors.document.form1.drpdwnFlm_Id.value + '/' + top.SelectFloors.document.form1.drpdwnTwm_Id.value;
//						alert(fInfo);
				        createCookie("FlrInfo",fInfo,1);
						//Don't Delete						
						//top.Polygon.document.form1.txtXValues.value = cntObjects + objkeys;	
						//alert(top.Polygon.document.form1.txtXValues.value);	
						//alert("Press 'Submit' button to Proceed");
						
					}
					else
					{
					 	alert("No Grapical Data Selected, Select at leat one polygon and Proceed");						
					}	

			}
function deletecook(name)
{
//    alert('cookie Name : ' + name);
      var d = new Date();
      d.setTime(d.getTime ()-(2*24*60*60*1000));
      
     document.cookie = name+";expires=" + d.toGMTString() + ";" + ";";
     
}

			
	function createCookie(name,value,days) 
	{
       deletecook(name);
    
    if (days) 
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
		
	}
	else var expires = "";
	
	document.cookie = name+"="+value+expires+"; path=/";
	
	
}


	function GetSelectedPorts()
	{ 
//	 alert('this.shval');
		var layer = "WST";
	var ca = document.cookie.split(';');	
		var Space=readCookie("AuZ");
//		alert(Space);
		var map = getMap();
		var mapSel = map.getSelection();
		var mapLayer = map.getMapLayer(layer);
	
		var mapObjects = map.createObject("MGCollection");
		var spcKeys = Space.split(",");
		var sKey = 0;
		while (sKey < spcKeys.length)
		{
		
	
	var mapObj = mapLayer.getMapObject(spcKeys[sKey]);
    		if (mapObj != null)
			mapObjects.add(mapObj);
			sKey += 1;
		}
		mapSel.clear();
		
		if (mapObjects.size() > 0)
			mapSel.addObjectsEx(mapObjects, false);
			zoomSelected();
			this.shval++;

	}

