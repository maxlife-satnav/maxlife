
// Determine browser...
	bName = navigator.appName;
	bVer = parseInt(navigator.appVersion);
	if (bName == "Netscape" && bVer >= 4)
		ver = "n4";
	else if (bName == "Microsoft Internet Explorer" && bVer >= 4)
		ver = "e4";
	else ver = "other";

// ...if Netscape, embed event observer
		if (ver == "n4")
		{
			document.write("<APPLET CODE=\"MapGuideObserver6.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
			document.write("</APPLET>");
		}

// Now create the event-handling function. The function updates the
// lat & lon text boxes with the coordinates of the point the user
// clicks. Note that the name of this function must be the same as
// the event name so that the MapGuideObserver5 observer can find it.

function getMap()
{
    
    if (navigator.appName == "Netscape")
        return document.map;
    else
        return document.map;
}

function initObs1()
{

    if (navigator.appName == "Netscape")
    {
        if (ver >= 1.2)
        {
            getMap().setViewChangedObserver(document.obs);
            getMap().setDoubleClickObserver(document.obs);
            getMap().setSelectionChangedObserver(document.obs);
        }
        if (ver > 1.2)
        {
            getMap().setBusyStateChangedObserver(document.obs);
            getMap().setMapLoadedObserver(document.obs);
            getMap().setViewChangingObserver(document.obs);
            getMap().setViewDistanceObserver(document.obs);
        }
    }
}

function initObs2()
{
    if (navigator.appName == "Netscape")
    {
        var ver = parseFloat(getMap().getApiVersion());
        if (ver <= 1.2)
        {
            document.write("<APPLET CODE=\"MapGuideObserver3.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
        if (ver >= 5.0)
        {
            document.write("<APPLET CODE=\"MapGuideObserver5.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
		  if (ver >= 6.0)
        {
            document.write("<APPLET CODE=\"MapGuideObserver6.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
        else
        {
            document.write("<APPLET CODE=\"MapGuideObserver4.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
    }
}


function init()
{
    ver = parseFloat(getMap().getApiVersion());

    if (ver < 1.2)
		{
        displayDownloadMsg();
        getMap().enableSelectionChangedEvent();
        }
    else
        initObs1();
}


function onSelectionChanged(map)
{
   
 var sel = map.getselection().getMapObjectsEx(null);
  
  var obj = sel.item(0);
  var key = obj.getKey();
  
  doGetKey(); 
}

function onBusyStateChanged(map, bIsBusy)
{
}

function onViewChanging(map)
{
}

function onViewChanged(map)
{

}

function onViewChanged(map)
{
//		setTimeout("show()",10000);
		initObs2();
}

function show()
{

}

function view()
{
	 map.zoomout();
}

function onViewChanging(map)
{
}

function onMapLoaded(map)
{
}

function onViewedDistance(map, totalDistance, distances, units)
{
}

function onDoubleClickObject(mapObj)  
{         
    
    var map = getMap();    
      
	var assetlayer=mapObj.getMapLayer().getName();
	var xx = mapObj.getKey();
	 
if(assetlayer=="HCB_T")
{
window.open("ShowDetails.aspx?id="+xx,"desktop","left=130,top=0,menubar=no,toolbar=no,location=no,resizable=no,scrollbars=yes,status=no,width=530,height=300");
}
if(assetlayer=="FCB_T")
{
window.open("ShowDetails.aspx?id="+xx,"desktop","left=130,top=0,menubar=no,toolbar=no,location=no,resizable=no,scrollbars=yes,status=no,width=530,height=300");
}
if(assetlayer=="WST_T")
{
window.open("ShowDetails.aspx?id="+xx,"desktop","left=130,top=0,menubar=no,toolbar=no,location=no,resizable=no,scrollbars=yes,status=no,width=800,height=400");
}
    // alert("This Layer Is Not Editable");

    //  return true;
}

function Theme()
{
	
	var map = getMap();
	var layer = map.getMapLayer("WST_T");
    
    	if (layer == null)
    	    alert("Layer Not Found");
	    else
        	layer.setVisibility(!layer.getVisibility());
	if(document.f1.setTheme.value=="Theme On")
		document.f1.setTheme.value="Theme Off"
	else
		document.f1.setTheme.value="Theme On"
		
        map.refresh();

}
