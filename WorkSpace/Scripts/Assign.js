// Determine browser...
	bName = navigator.appName;
	bVer = parseInt(navigator.appVersion);
	if (bName == "Netscape" && bVer >= 4)
		ver = "n4";
	else if (bName == "Microsoft Internet Explorer" && bVer >= 4)
		ver = "e4";
	else ver = "other";

// ...if Netscape, embed event observer
		if (ver == "n4")
		{
			document.write("<APPLET CODE=\"MapGuideObserver6.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
			document.write("</APPLET>");
		}

// Now create the event-handling function. The function updates the
// lat & lon text boxes with the coordinates of the point the user
// clicks. Note that the name of this function must be the same as
// the event name so that the MapGuideObserver5 observer can find it.

function getMap()
{
    if (navigator.appName == "Netscape")
        return parent.document.map;
    else
        return parent.window.map;
}

alert(getMap());

function initObs1()
{
    if (navigator.appName == "Netscape")
    {
        if (ver >= 1.2)
        {
            getMap().setViewChangedObserver(document.obs);
            getMap().setDoubleClickObserver(document.obs);
            getMap().setSelectionChangedObserver(document.obs);
        }
        if (ver > 1.2)
        {
            getMap().setBusyStateChangedObserver(document.obs);
            getMap().setMapLoadedObserver(document.obs);
            getMap().setViewChangingObserver(document.obs);
            getMap().setViewDistanceObserver(document.obs);
        }
    }
}

function initObs2()
{
    if (navigator.appName == "Netscape")
    {
        var ver = parseFloat(getMap().getApiVersion());
        if (ver <= 1.2)
        {
            document.write("<APPLET CODE=\"MapGuideObserver3.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
        if (ver >= 5.0)
        {
            document.write("<APPLET CODE=\"MapGuideObserver5.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
		  if (ver >= 6.0)
        {
            document.write("<APPLET CODE=\"MapGuideObserver6.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
        else
        {
            document.write("<APPLET CODE=\"MapGuideObserver4.class\" WIDTH=2 HEIGHT=2 NAME=\"obs\" MAYSCRIPT>");
            document.write("</APPLET>");
        }
    }
}


function init()
{
    ver = parseFloat(getMap().getApiVersion());

    if (ver < 1.2)
        displayDownloadMsg();
    else
        initObs1();
}


function onSelectionChanged(map)
{
  
}

function onBusyStateChanged(map, bIsBusy)
{
}

function onViewChanging(map)
{
}

function onViewChanged(map)
{
}

function onViewChanged(map)
{
		setTimeout("show()",10000);
		initObs2();
}

function show()
{

}

function view()
{
	 map.zoomout();
}

function onViewChanging(map)
{
}

function onMapLoaded(map)
{
}

function onViewedDistance(map, totalDistance, distances, units)
{
}

function onDoubleClickObject(mapObj)
{
	var assetlayer=mapObj.getMapLayer().getName();
	var xx = mapObj.getKey();
	if (assetlayer == "COM")
		window.open("AddCOM.aspx?id="+xx,"desktop","left=100,top=0,menubar=no,toolbar=no,location=no,resizable=no,scrollbars=yes,status=no,width=650,height=500");
	else if (assetlayer == "CHA")
		window.open("AddCHA.aspx?id="+xx,"desktop","left=100,top=0,menubar=no,toolbar=no,location=no,resizable=no,scrollbars=yes,status=no,width=650,height=500");
	else if (assetlayer == "TEL")
		window.open("AddTEL.aspx?id="+xx,"desktop","left=100,top=0,menubar=no,toolbar=no,location=no,resizable=no,scrollbars=yes,status=no,width=750,height=500");
return true;

}




