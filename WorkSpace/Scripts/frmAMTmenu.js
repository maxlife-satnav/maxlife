////////////////////////////////////////////////////////////////////////////
///
/// J a v a S c r i p t   H e i r a r c h i c a l   M e n u
/// Author: Rama Satyanarayana
/// Source: http://www.shastrynet.com/javascript/hmenu.asp
///
////////////////////////////////////////////////////////////////////////////

var canvas			/// Menu frame body object
var itemIndex = 0;	/// Index counter for the menus/items
var objCurrMenu;	/// Variable to hold the currently active Menu object

var bookmark = "top";
var index = 1;
//var imgFiller = new Image(); imgFiller.src = "images/filler.gif"; var intImgFiller = 0;
var imgFiller = new Image();  var intImgFiller = 0;
var imgOpen = new Image(); imgOpen.src = "../images/imgAMTpluse.gif"; var intImgOpen = 0;
var imgClosed = new Image(); imgClosed.src = "../images/imgAMTpluse.gif"; var intImgClosed = 0;
var imgOpen1 = new Image(); imgOpen1.src = "../images/imgAMTpluse.gif"; var intImgOpen1 = 0;
//var imgClosed1 = new Image(); imgClosed1.src = "images/arrow_submenu.gif"; var intImgClosed1 = 0;

function drawHMenu() {
	var intI;
	itemIndex = 0;
	intImgClosed = 0;
	intImgOpen = 0;
	intImgFiller = 0;	
	intImgOpen1 = 0;
	intImgClosed1 = 0
	
	canvas = window.frames["menuFrame"].document;
	
	canvas.open("text/html");
	
	canvas.writeln("<!doctype html public '-//w3c//dtd html 4.0 final//en'>");
	canvas.writeln("<html>");
	canvas.writeln("<head>");
	canvas.writeln("<title>Heirarchical Menu</title>");		
	canvas.writeln("<meta name='author' content='Subramanya Shastry Y V H'>");	
	canvas.writeln("<meta name='source' content='http://www.shastrynet.com/'>");		
	canvas.writeln("<link rel=stylesheet  href='../Scripts/styAMTamantramenu.css' type='text/css'>");	
	//canvas.writeln("<SCRIPT LANGUAGE='JavaScript' SRC='../images/slideshow_inner.js'>");
	//canvas.writeln("</SCRIPT>");
	fetchScript();
	canvas.writeln("</head>");
	canvas.write("<body  background='../Images/left_bg.gif' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>");
/*	
	if(document.body.cols == '231,*')			
		canvas.writeln('<table cellpadding=0 cellspacing=0 border=0 width=212><tr><td align=right><div><a href=#><IMG border=0 src=../Images/imgAMThidemenu.gif border=0 usemap="#ResizeCollapse" onClick="javascript:SetFrameSize()" onMouseOver=window.status="Resize"; return true; onMouseOut=window.status=""; return true;></img></a></td></tr></table>');		
	else
		canvas.writeln('<IMG border=0 height=30 src=images/imgAMTaexpand.gif >');				
	canvas.writeln("</div>");		
*/
//satyanand code start
//	canvas.writeln('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="231" height="178" VIEWASTEXT>');	
//    canvas.writeln('<param name="movie" value="../images/amantra.swf" >');
//    canvas.writeln('<param name="quality" value="high">');
//    canvas.writeln('<param name="wmode" value="transparent">');
//    canvas.writeln('<embed src="../images/amantra.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="231" height="178"></embed>');
//    canvas.writeln("</object> ");
canvas.writeln('<IMG border=0 src=../images/inner_top.jpg>');	
   
   //satyanand code end
	//Menu content
	canvas.writeln("	<DIV id='divCont'>");
	canvas.writeln("	<DIV class='clsCroll' id='divScroll1' align = 'left' >");
	//canvas.writeln("<br><br><br><br>");
	if(navigator.appVersion.indexOf('MSIE 6') > -1)
		canvas.writeln("	<table border='0' width='87%' cellpadding='0' cellspacing='0'>");	
	else
		canvas.writeln("	<table border='0' width='75%' cellpadding='0' cellspacing='0'>");	
	canvas.writeln("	<tr><td   valign='top'>");
	paintMenu();
	canvas.writeln("	</td></tr></table>");
	canvas.writeln("</div></div>")
	
	//up scroll	
	
	canvas.writeln("<DIV id='divControl'  align='right'>");
	canvas.writeln("<table  border='0' width='230' align='left' cellpadding='0' cellspacing='0'>");
	canvas.writeln("<td   align=right>");
	canvas.writeln("<A href='#' onmouseout='noScroll()' onmouseover='scroll(4)' title='Top'><IMG border=0 height=15 src =\"../Images/imgAMTuparrow.gif\" width='15' border=0></A>");		
	canvas.writeln("</td></tr></table>");
	if(navigator.appVersion.indexOf('MSIE 6') > -1)
	{
		canvas.writeln("<br><br><br><br><br><br><br><br>");
		//canvas.writeln("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>");
		//canvas.writeln("<br><br><br><br><br><br><br><br><br><br><br>");
		}
	else
	{
	canvas.writeln("<br><br><br><br><br><br><br>");	
	//canvas.writeln("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>");	
	//canvas.writeln("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>");	
	}
	
//	canvas.writeln("<table border='0' width='212' align='left' cellpadding='0' cellspacing='0'>");
//	canvas.writeln("<tr><br><td align=right>");
//	canvas.writeln("<A href='#' onmouseout='noScroll();' onmouseover='scroll(-5)' title='Bottom'><IMG border='0' height='15' src=\"../Images/imgAMTdownarrow.gif\" width='15'></A>");
//	canvas.writeln("</td></tr></table>");
//	canvas.writeln("</div>");
	
	
	//down scroll
	canvas.writeln("<table border='0' width='230' align='left' cellpadding='0' cellspacing='0'>");
	canvas.writeln("<tr><br><td   align=right>");
	canvas.writeln("<A href='#' onmouseout='noScroll()' onmouseover='scroll(-4)' title='Bottom'><IMG border='0' height='15' src=\"../Images/imgAMTdownarrow.gif\" width='15'></A>");
	canvas.writeln("</td></tr></table>");	
	if(navigator.appVersion.indexOf('MSIE 6') > -1)
	{
		//canvas.writeln("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>");
		//canvas.writeln("<br><br><br><br><br><br>");
		}
	else
	{
	//canvas.writeln("<br><br>");	
	//canvas.writeln("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><IMG border=0 src=../images/amantralogo_new.jpg>");	
	}
	canvas.writeln("</div>");
	canvas.writeln("<br><br>");
	canvas.writeln("<div>&nbsp;");
	canvas.writeln("</div>");

	
	
	printUseMap();
	
    canvas.writeln("</body>");
	canvas.writeln("</html>");
	canvas.close()
}
////////////////////////// Object: Menu //////////////////////////
/// Function to construct a Menu Object
///

function Menu(title, parent, tip) {
	this.type = "menu"
	this.menuIndex = itemIndex++;
	this.title = title;
	if (tip == null)
		this.tip = title;
	else
		this.tip = tip;
	this.parent = parent;
	
	// Assign the depth value
	if (parent == null)
		this.depth = 0;
	else {
		this.depth = parent.depth + 1;
		this.parent.items[this.parent.items.length] = this;
	}
	
	this.items = new Array();
	this.curindex = 0
	this.status = -1;
}
////////////////////////// Object: Menu //////////////////////////

////////////////////////// Object: MenuItem //////////////////////////
/// Function to construct a MenuItem Object
function MenuItem(title, url, target, parent, tip) {
	this.type = "item";
	this.title = title;		
	this.url = url;
	this.target = target;	
	this.parent = parent;
	this.parent.items[this.parent.items.length] = this;
	this.depth = parent.depth + 1;
	if (tip == null)
		this.tip = title;
	else
		this.tip = tip;
}
////////////////////////// Object: MenuItem //////////////////////////


////////////////////////// function: paintMenu() //////////////////////////
function paintMenu() {
	canvas.writeln('<table cellspacing="5" cellpadding="1" border="0">');
	
	paintSubMenus(hMenu,1);
	paintSubItems(hMenu);

	canvas.writeln('</table>');
}
////////////////////////// function: paintSubMenus() //////////////////////////
function paintSubMenus(objMenu,intLevel) {

	var intI;
	var quote = "'";
	
	for(intI = 0; intI < objMenu.items.length; intI++)
	
		if (objMenu.items[intI].type == "menu") {
			var subMenu = objMenu.items[intI];
		
			canvas.write('<tr><td>')
			
			canvas.write(trimFiller(subMenu.depth - 1))
		
			//Level 1
			
	      if(intLevel == 1) {
				if (subMenu.status == 1) {
				    canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to close ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;"><img width="13" height="15" border=0 src="../Images/imgAMTminus.gif" name="o' +( intImgOpen++ ) + '"> </a>')
					canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to close ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" class="clsMenu">' +'<font color=maroon>' +subMenu.title + '<font></a>')
				}else {
					canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;"><img width="13" height="15" border=0 src="../Images/imgAMTpluse.gif" name="c' + ( intImgClosed++ ) + '"> </a>')
					canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" class="clsMenu">' + subMenu.title + '</a>')
				}	
			} else {
			//Level 2
				if (subMenu.status == 1){
				canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to close ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" > <img width="13" height="15" border=0 src="../Images/imgAMTminus.gif" name="o1' + '<font color=red>'+( intImgOpen++ ) + '"></font> </a>');
				canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to close ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" class="clsMenu">' + '<font color=blue>'+subMenu.title + '</font></a></font>');
				}else if (subMenu.status == -1){
				    canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to Open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" > <img width="13" height="15" src="../Images/imgAMTpluse.gif" border=0 name="c1' +'<font color=red>'+ (intImgClosed++ ) + '"></font> </a>')
					canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to Open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" class="clsMenu">' + subMenu.title + '</a>')
				}
				else{
				    canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to Open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" > <img width="13" height="15" src="../Images/imgAMTpluse.gif" border=0 name="c1' + ( intImgClosed++ ) + '"> </a>')
					canvas.writeln('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to Open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" class="clsMenu">' + subMenu.title + '</a>')
				}
			}
			canvas.writeln('</td></tr>') ;
			
			if (subMenu.status == 1) {
				paintSubMenus(subMenu,-1);
				paintSubItems(subMenu);
			}
			}
		
}
////////////////////////// function: paintSubMenus() //////////////////////////



////////////////////////// function: paintSubItems() //////////////////////////
function paintSubItems(objMenu) {
	var intI;
	var quote = "'";

	for(intI = 0; intI < objMenu.items.length; intI++)
		if (objMenu.items[intI].type == "item") {
			var subItem = objMenu.items[intI];
			
			if (subItem.depth == 2)
				canvas.writeln('<tr><td  >' + trimFiller(subItem.depth - 1) + '<a href="' + subItem.url + '" target="' + subItem.target + '" onMouseOver="window.status=' + quote + subItem.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;"><img src="../Images/imgAMTdot.gif" border=0 width="13" height="15"><a href="' + subItem.url + '" class=clsMenu target="' + subItem.target + '" onMouseOver="window.status=' + quote + subItem.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;">')
			else
				canvas.writeln('<tr><td  >' + trimFiller(subItem.depth - 1) + '<a href="' + subItem.url + '" target="' + subItem.target + '" onMouseOver="window.status=' + quote + subItem.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;"><img src="../Images/imgAMTdot.gif" border=0 width="13" height="15"><a href="' + subItem.url + '" class=clsMenu target="' + subItem.target + '" onMouseOver="window.status=' + quote + subItem.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;">')

			if (subItem.depth == 3)
				//canvas.write('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;"><img width="13" height="15" border=0 src="../Images/imgAMTpluse.gif" name="c' + ( intImgClosed++ ) + '"> </a>')
				//canvas.write('<a href="javascript:parent.click(' + subMenu.menuIndex + ', ' + subMenu.status + ')" onMouseOver="window.status=' + quote + 'Click to open ' + subMenu.tip + quote + '; return true;" onMouseOut="window.status=' + quote + quote + '; return true;" class="clsMenu">' + subMenu.title + '</a>')
				//canvas.writeln('<a href='+ subItem.title+'>'+subItem.title+11+'</a>') 
				canvas.writeln(subItem.title) 
			else
				canvas.writeln(subItem.title) 

		    canvas.writeln('</a></td></tr>')
		}
}
////////////////////////// function: paintSubItems() //////////////////////////



////////////////////////// function: trimFiller() //////////////////////////
function trimFiller(amount) {
	if (amount > 0)
//		return '<img width="' + (9 * amount) + '" height="9" border="0" src="../Images/filler.gif">'
		return '<img width="' + (9 * amount) + '" height="9" border="0">'
	else
		return ""
}
////////////////////////// function: filler() //////////////////////////



////////////////////////// function: filler() //////////////////////////
function filler(amount) {
	if (amount > 0)
		return '<img width="' + (9 * amount) + '" height="9" border="0" name="f' + (intImgFiller++) + '"> '
	else
		return ""
}
////////////////////////// function: filler() //////////////////////////



////////////////////////// function: getCookie() //////////////////////////
function getCookie(name) {
	var search;

	search = name + "=";
	offset = document.cookie.indexOf(search) ;

	if (offset != -1) {
		offset += search.length;
		end = document.cookie.indexOf(";", offset);
		if (end == -1)
			end = document.cookie.length;
		return unescape(document.cookie.substring(offset, end));
	}
	else
		return "";
}
////////////////////////// function: getCookie() //////////////////////////



////////////////////////// function: click() //////////////////////////
function click(menuNumber, status) {

	status *= (-1);
	objMenu = searchChildren(hMenu, menuNumber, status);

	resetAll(hMenu);
	expandThread(objCurrMenu);
	objCurrMenu.status = status;
	
	setTimeout("drawHMenu()", 10);
	
	for (var intI = 0; intI < hMenu.items.length; intI++)
		if (hMenu.items[intI].menuIndex == menuNumber)
			hMenu.curindex= intI
	
	//setTimeout("drawHMenu("+menuNumber +")", 10);
}

////////////////////////// function: resetAll() //////////////////////////
function resetAll(parent) {

	parent.status = -1;
	for (var intI = 0; intI < parent.items.length; intI++) {
			if (parent.items[intI].type == "menu")
				resetAll(parent.items[intI]);
			
	}
}
////////////////////////// function: resetAll() //////////////////////////



////////////////////////// function: searchChildren() //////////////////////////
function searchChildren(parent, menuNumber, status) {
	for (var intI = 0; intI < parent.items.length; intI++)
		if (parent.items[intI].type == "menu")
			if (parent.items[intI].menuIndex == menuNumber)
				objCurrMenu = parent.items[intI];
			else
				searchChildren(parent.items[intI], menuNumber, status);
}
////////////////////////// function: searchChildren() //////////////////////////


////////////////////////// function: expandThread() //////////////////////////
function expandThread(subMenu) {
	subMenu.status = 1;

	if (subMenu.parent != null)
		expandThread(subMenu.parent);
}
////////////////////////// function: expandThread() //////////////////////////


////////////////////////// function: fetchScript() //////////////////////////
function fetchScript() {
canvas.writeln("<script>");
canvas.writeln("function checkBrowser(){");
        canvas.writeln("this.ver=navigator.appVersion");
        canvas.writeln("this.dom=document.getElementById?1:0");
        canvas.writeln("this.ie5=(this.ver.indexOf('MSIE 5')>-1 && this.dom)?1:0;");
        canvas.writeln("this.ie4=(document.all && !(this.dom))?1:0;");
        canvas.writeln("this.ns5=(this.dom && parseInt(this.ver) >= 5) ?1:0;");
        canvas.writeln("this.ns4=(document.layers && !(this.dom))?1:0;");
        canvas.writeln("this.bw=(this.ie5 || this.ie4 || this.ns4 || this.ns5)");
        canvas.writeln("return this");
canvas.writeln("}");
canvas.writeln("bw=new checkBrowser()");

canvas.writeln("timSpeed=50");
canvas.writeln("contHeight=100");

canvas.writeln("function makeScrollObj(obj,nest){");
canvas.writeln("        nest=(!nest) ? '':'document.'+nest+'.'");                                                                          
canvas.writeln("        this.el=bw.dom?document.getElementById(obj):bw.ie4?document.all[obj]:bw.ns4?eval(nest+'document.'+obj):0;");
canvas.writeln("        this.css=bw.dom?document.getElementById(obj).style:bw.ie4?document.all[obj].style:bw.ns4?eval(nest+'document.'+obj):0;");          
canvas.writeln("        this.height=bw.ns4?this.css.document.height:this.el.offsetHeight");
canvas.writeln("        this.top=b_gettop ");                                                                              
canvas.writeln("        return this");
canvas.writeln("}");

canvas.writeln("function b_gettop(){");
canvas.writeln("        var gleft=(bw.ns4 || bw.ns5) ? eval(this.css.top):eval(this.css.pixelTop);");
canvas.writeln("        return gleft;");
canvas.writeln("}");

canvas.writeln("var scrollTim;");
canvas.writeln("var active=0;");

canvas.writeln("function scroll(speed){");
        canvas.writeln("clearTimeout(scrollTim)");
        canvas.writeln("way=speed>0?1:0");
        canvas.writeln("if((!way && oScroll[active].top()>-oScroll[active].height+contHeight) || (oScroll[active].top()<0 && way)){");
        canvas.writeln("        oScroll[active].css.top=oScroll[active].top()+speed");
        canvas.writeln("        scrollTim=setTimeout('scroll('+speed+')',timSpeed)");
        canvas.writeln("}");
canvas.writeln("}");

////////////////////////// New function for scrolling fixed height
canvas.writeln("function scrollToIndex(index){");
        canvas.writeln("speed=(-5);");
        canvas.writeln("clearTimeout(scrollTim)");
        canvas.writeln("way=speed>0?1:0");
        canvas.writeln("for(var i=0;i<index;i++){");
		canvas.writeln("	if((!way && oScroll[active].top()>-oScroll[active].height+contHeight) || (oScroll[active].top()<0 && way))");
		canvas.writeln("		oScroll[active].css.top=oScroll[active].top()+speed");
	    canvas.writeln("   }");
          
canvas.writeln("}");

/////////////////////////////////////////////////////


canvas.writeln("function noScroll(){");
canvas.writeln("        clearTimeout(scrollTim)");
canvas.writeln("}");

canvas.writeln("function changeActive(num){");
canvas.writeln("        oScroll[active].css.visibility='hidden'");
canvas.writeln("        active=num");
canvas.writeln("        oScroll[active].css.top=0");
canvas.writeln("        oScroll[active].css.visibility='visible'");
canvas.writeln("}");
canvas.writeln("function scrollInit(){	");
canvas.writeln("        oScroll=new Array()");
canvas.writeln("        oScroll[0]=new makeScrollObj('divScroll1','divCont')");
canvas.writeln("        oScroll[0].css.visibility='visible'");
canvas.writeln("        oControl=new makeScrollObj('divControl')");
canvas.writeln("        oControl.css.visibility='visible'");
canvas.writeln("	var scrollTim;");
canvas.writeln("	var active=0;");
canvas.writeln("	var speed=(-5);");
canvas.writeln("	var way;");

// Scroll to selected index 
canvas.writeln("	for(var i=0;i<(parent.hMenu.curindex*5);i++){");
canvas.writeln("		if((!way && oScroll[active].top()>-oScroll[active].height+contHeight) || (oScroll[active].top()<0 && way))");
canvas.writeln("		oScroll[active].css.top=oScroll[active].top()+speed");
canvas.writeln("   }");
canvas.writeln("}");

canvas.writeln("function SetFrameSize()")
canvas.writeln("{")
canvas.writeln("	if(parent.document.body.cols == '231,*')");
canvas.writeln("	{");
canvas.writeln("		parent.document.body.cols = '231,*'");
canvas.writeln("		parent.drawHMenuNew()");
canvas.writeln("	}");
canvas.writeln("	else");
canvas.writeln("	{");
canvas.writeln("		parent.document.body.cols = '231,*'");
canvas.writeln("		parent.drawHMenu()");
canvas.writeln("	}");
canvas.writeln("}")

canvas.writeln("onload=scrollInit;");

canvas.writeln("</script>");
}

////////////////////////// function: printUseMap() //////////////////////////

function printUseMap() {
canvas.writeln('<map name="ResizeCollapse">');
canvas.writeln('<area shape="rect" alt="Decrease Frame Width" coords="1,1,15,15">');
canvas.writeln('</map>');

canvas.writeln('<map name="ResizeExpand">');
canvas.writeln('<area shape="rect" alt="Increase Frame Width" coords="1,1,15,15">');
canvas.writeln('</map>');

}

////////////////////////// function: drawHMenuNew() //////////////////////////
function drawHMenuNew() {
	var intI;

	itemIndex = 0;
	intImgClosed = 0;
	intImgOpen = 0;
	intImgFiller = 0;	
	intImgOpen1 = 0;
	intImgClosed1 = 0

	canvas1 = window.frames["menuFrame"].document;
	canvas1.open("text/html");
	
	canvas1.writeln("<!doctype html public '-//w3c//dtd html 4.0 final//en'>");
	canvas1.writeln("<html>");
	canvas1.writeln("<head>");
	canvas1.writeln("<title>Heirarchical Menu</title>");		
	canvas1.writeln("<meta name='author' content='Subramanya Shastry Y V H'>");	
	canvas1.writeln("<meta name='source' content='http://www.shastrynet.com/'>");		
	canvas.writeln("<link rel=stylesheet href='../Scripts/styAMTamantramenu.css' type='text/css'>");
		
	fetchScriptNew();
	canvas1.writeln("</head>");		
	/*canvas1.writeln("<body background='images/sidegray.jpg' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>");*/
	canvas.write("<body  background='../Images/left_bg.gif' leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' > ");
	
	canvas1.writeln("<DIV>");
	canvas1.writeln("<table border='1' width='100%' align='center' cellpadding='0' cellspacing='0'>");
		
	if(document.body.cols == '231,*')
		canvas1.writeln('<IMG border=0 height=30 src=../Images/hidemenu.jpg usemap="#ResizeCollapse" width=231 onClick="javascript:SetFrameSize()" onMouseOver=window.status="Resize"; return true; onMouseOut=window.status=""; return true;>');	
			
	else
	
		canvas1.writeln('<tr><td><a href=#><IMG border=0 height=15 src=../Images/imgAMTaexpand.gif usemap="#ResizeExpand" width=15 onClick="javascript:SetFrameSize()" onMouseOver=window.status="Resize"; return true; onMouseOut=window.status=""; return true;></a></td></tr>');				
	canvas1.writeln("</table>");		
	canvas1.writeln("</DIV>");
	
	canvas1.writeln("</body>");
	canvas1.writeln("</html>");
	printUseMap();
	canvas1.close()
}

////////////////////////// function: fetchScriptNew() //////////////////////////
function fetchScriptNew() {
canvas1.writeln("<script>");
canvas1.writeln("function SetFrameSize()")
canvas1.writeln("{")
canvas1.writeln("if(parent.document.body.cols == '231,*')");
canvas1.writeln("	{");
canvas1.writeln("	parent.document.body.cols = '231,*'");
canvas1.writeln("	parent.drawHMenuNew()");
canvas1.writeln("	}");
canvas1.writeln("else");
canvas1.writeln("	{");
canvas1.writeln("	parent.document.body.cols = '231,*'");
canvas1.writeln("	parent.drawHMenu()");
canvas1.writeln("	}");
canvas1.writeln("}")
canvas1.writeln("</script>");
}

////////////////////////// function: printUseMapNew() //////////////////////////

function printUseMapNew() {
canvas1.writeln('<map name="ResizeCollapse">');
canvas1.writeln('<area shape="rect" alt="Decrease Frame Width" coords="1,1,15,15">');
canvas1.writeln('</map>');

canvas1.writeln('<map name="ResizeExpand">');
canvas1.writeln('<area shape="rect" alt="Increase Frame Width" coords="1,1,15,15">');
canvas1.writeln('</map>');

}