Sub map_onViewChanged(map)
	onViewChanged(map)
End sub

Sub map_onDigitizedPolyline( map, numPoints, points)
    onDigitizedPolyline map, numPoints, points
End sub

Sub map_onDoubleClickObject(mapObj)
	mapObj.DoubleClickHandled = onDoubleClickObject(mapObj)
End sub

Sub map_onDigitizedPoint(map,points)
    onDigitizedPoint map, points
End Sub

Sub map_onDigitizedPolygon(map,numPoints,points)
    onDigitizedPolygon map, numPoints, points
End Sub
Sub map_onSelectionChanged(map)
    onSelectionChanged map
End Sub