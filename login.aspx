﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>QuickFMS :: Total Infrastructure Control</title>
    <%=ScriptCombiner.GetScriptTags("login_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 140px;
        }
    </style>
    <style>
        #txtUsrPwd {
            -webkit-text-security: disc;
        }
    </style>
</head>
<body>

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="login-div">
                        <img src="BootStrapCSS/images/quickfms_p_l_logo1.png" class="profile-img img " />

                        <div class="form-group text-center">
                            <asp:Label ID="lbl1" runat="server" CssClass="control-label" ForeColor="Red"></asp:Label>
                        </div>
                        <form id="loginform" class="form-horizontal" role="form" runat="server" defaultfocus="tenantID" autocomplete="off">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>

                            <asp:HiddenField ID="hidForModel" runat="server" />


                            <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="hidForModel"
                                CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none">
                                You have already Logged-In. Do you want to Clear that Session?<br />
                                <br />
                                <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary " OnClick="btnYes_Click" />
                                <asp:Button ID="btnClose" runat="server" Text="No" CssClass="btn" OnClick="btnClose_Click" />
                            </asp:Panel>
                            <asp:Panel ID="pnlMain" runat="server" onclick="FoolTheBrowser2()">
                                <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val1" />

                                <%-- for Enterprise edition enable below code & also handle in session --%>
                                <%--<div style="margin-bottom: 25px" class="input-group" runat="server" visible='<%$AppSettings:enterprise %>'>--%>
                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <asp:TextBox ID="tenantID" runat="server" type="text" class="form-control" placeholder="Company Id" autocomplete="off" oncopy="return false" onpaste="return false" oncut="return false"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtnt" runat="server" ControlToValidate="tenantID"
                                        Display="None" ErrorMessage="Please Enter Company Id" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                </div>

                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <asp:TextBox ID="txtUsrId" runat="server" type="text" class="form-control" placeholder="Username" autocomplete="off" oncopy="return false" onpaste="return false" oncut="return false"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvusr" runat="server" ControlToValidate="txtUsrId"
                                        Display="None" ErrorMessage="Please Enter Username " ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                </div>

                                <div class="input-group" onclick="FoolTheBrowser2()">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <asp:textbox id="txtUsrPwd" runat="server" maxlength="15" class="form-control" placeholder="Password" autocomplete="off" autocompletetype="Disabled" xmlns:asp="#unknown" oncopy="return false" onpaste="return false" oncut="return false" />
                                    <asp:RequiredFieldValidator ID="rfvpwd" runat="server" ControlToValidate="txtUsrPwd"
                                        Display="None" ErrorMessage="Please Enter Password" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                    <div style="color: red" id="err"></div>
                                </div>

                                <div class="form-group text-center">
                                </div>
                                <div class="form-group">
                                    <div class="checkbox col-sm-6">
                                        <div>
                                            <a href="#" onclick="showPopWin()">Forgot Password!</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 controls">
                                        <div>
                                            <asp:Button ID="btnSignIn" OnClientClick="Validate()" runat="server" class="btn login-btn pull-right" Text="Sign In" ValidationGroup="Val1" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <!--Start Licence key entering    -->
                            <asp:Panel ID="pnlLic" runat="server">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val3" />
                                <div class="form-group text-center">
                                    <%--  <asp:Label ID="lblSystemKey" runat="server" CssClass="control-label"></asp:Label>--%>
                                </div>
                                <div class="form-group text-center">
                                    <b>
                                        <asp:Label ID="lblKeyDesc" runat="server" CssClass="control-label" Text="License Code: "></asp:Label></b>
                                    <asp:Label ID="lblKey" runat="server" CssClass="control-label" Text="Key"></asp:Label>
                                </div>
                                <div style="margin-bottom: 25px" class="input-group">

                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <asp:TextBox ID="txtSerialKey" runat="server" type="text" class="form-control" placeholder="Enter Activation Code"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSerialKey"
                                        Display="None" ErrorMessage="Please Enter Valid Activation Code" ValidationGroup="Val3"> </asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 controls">
                                    </div>
                                    <div class="col-sm-6 controls">
                                        <asp:Button ID="btnNext" runat="server" class="btn login-btn pull-right" Text="Next" ValidationGroup="Val3" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <!--End License key entering-->

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;span></button>
                    <h4 class="modal-title">Forgot Password</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="250" frameborder="0" style="border: none"></iframe>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        function showPopWin() {

            $("#modalcontentframe").attr("src", "frmForgetPwd.aspx");
            $("#myModal").modal().fadeIn();
            return false;
            //$("#modelcontainer").load("frmForgetPwd.aspx", function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal().fadeIn();
            //});
        }

        function FoolTheBrowser1() {
            document.getElementById('<%= txtUsrPwd.ClientID%>').type = 'text';
        }
        function FoolTheBrowser2() {
            document.getElementById('<%= txtUsrPwd.ClientID%>').type = 'password';
        }
        function Validate() {
            $('#content').height($(window).height());
            var str = $("#<%= txtUsrPwd.ClientID %>").val();
            $("#<%= txtUsrPwd.ClientID %>").val(window.btoa(str));
            FoolTheBrowser2();
        }

        if (self != top) {
            window.top.location.href = "login.aspx"
        }
        $(document).keydown(function (event) {
            if (event.keyCode == 123) {
                return false;
            }
            else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                return false;
            }
        });
        $(document).on("contextmenu", function (e) {
            e.preventDefault();
        });

    </script>
</body>
</html>
