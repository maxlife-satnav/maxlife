Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.Web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Imports System.Security.Cryptography

Partial Class login
    Inherits System.Web.UI.Page

    Protected Sub btnSignIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSignIn.Click

        Dim UserStatus As String
        Session("UID") = ""
        Session("uname") = ""
        Session("TENANT") = tenantID.Text
        Dim staid As String = ""
        Dim security As String = ""
        Dim cnt As Integer
        Dim Tenant_Id As String = ""
        Session("UID") = txtUsrId.Text
        Tenant_Id = ValidateTenant(tenantID.Text)
        Session("TENANT") = Tenant_Id
        If Tenant_Id <> "0" Then
            Dim txtpwd As String = DecodeFrom64(txtUsrPwd.Text)
            staid = ValidateUser(txtUsrId.Text, txtpwd)

            If staid <> "0" And staid <> "1" And staid <> "2" Then
                SetInvalidLoginCountToZero()
                'cnt = SecureValidateUser()
                'If cnt >= "3" Then
                '    lbl1.Text = "Account Has Been Locked"
                '    Change_Pwd_For_Locked_Account()
                'Else

                FormsAuthentication.Initialize()
                Dim authTicket As New FormsAuthenticationTicket(txtUsrId.Text, True, 60)
                Dim encryptedticket As String = FormsAuthentication.Encrypt(authTicket)
                Dim authcookie As New HttpCookie(FormsAuthentication.FormsCookieName, encryptedticket)
                authcookie.Secure = True
                Response.Cookies.Add(authcookie)

                Session("uname") = staid
                displaybsmdata()
                GET_ASSET_MODULE_CHECK_FOR_USER()
                'SetInvalidLoginCountToZero()
                UserStatus = GetUserStatus(Session("UID"))
                If UserStatus <> "" Then
                    Panel1.Visible = True
                    mp1.Show()
                Else
                    Dim lastlogin As DateTime = savelastlogin(Session("UID"))
                    Session("Lastlogintime") = lastlogin.ToString("dd MMM yyyy HH:mm:ss")
                    Dim stl As String = Validatelicence(txtUsrId.Text, txtpwd)
                    If stl = "0" Then
                        Response.Redirect("~/frmAMTDefault.aspx")
                    Else
                        Response.Redirect("~/frmLicense.aspx")
                    End If
                End If
                'End If
            ElseIf staid = "2" Then
                cnt = SecureValidateUser()
                If cnt >= "3" Then
                    lbl1.Text = "Account Has Been Locked"
                    Change_Pwd_For_Locked_Account()
                Else
                    lbl1.Text = "Invalid Login!"
                End If
            Else
                lbl1.Text = "Invalid Login!"
            End If
        Else
            lbl1.Text = "Invalid Company Id!"
        End If

    End Sub

    Private Function Decrypt(cipherText As String) As String
        Dim EncryptionKey As String = "MAKV2SPBNI99212"
        Dim cipherBytes As Byte() = Convert.FromBase64String(cipherText)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                cipherText = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return cipherText
    End Function
    'Get the Parent, Child for business specific master
    Public Sub getuseroffsetandculture()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_OFFSET_CULTURE_BYID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("useroffset") = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            Session("userculture") = ds.Tables(0).Rows(0).Item("AUR_CULTURE")

            'Dim useroffsetcookie As HttpCookie = New HttpCookie("useroffset")
            'useroffsetcookie.Value = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            'Response.Cookies.Add(useroffsetcookie)
            Dim userculturecookie As HttpCookie = New HttpCookie("userculture")
            userculturecookie.Value = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Response.Cookies.Add(userculturecookie)

        End If
    End Sub
    Public Sub Change_Pwd_For_Locked_Account()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PWD_RESET")
        sp.Command.AddParameter("@USR_ID", txtUsrId.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Public Sub SetInvalidLoginCountToZero()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SET_INVALID_COUNT_TO_ZERO")
        sp.Command.AddParameter("@USR_ID", txtUsrId.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Public Sub displaybsmdata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_BSM_GETALL")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("Parent") = ds.Tables(0).Rows(0).Item("AMT_BSM_PARENT")
            Session("Child") = ds.Tables(0).Rows(0).Item("AMT_BSM_CHILD")
        End If
    End Sub
    Public Sub GET_ASSET_MODULE_CHECK_FOR_USER()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_MODULE_CHECK_FOR_USER")
        sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Dim asset = ds.Tables(0).Rows(0).Item("T_STA_ID")
            If asset = 1 Then
                Get_asset_sysp_value()
            End If
        End If
    End Sub
    Public Sub Get_asset_sysp_value()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AST_SYSP_VALUE")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("Procurement") = ds.Tables(0).Rows(0).Item("AST_SYSP_VAL1")
        End If
    End Sub
    Public Function ValidateTenant(ByVal Tenant_Name As String) As String
        Dim Valid_Tenant_Id As String = ""
        Dim sp As New SubSonic.StoredProcedure("CHECK_USER_TENANT_ISVALD")
        sp.Command.AddParameter("@TENANT_NAME", Tenant_Name, DbType.String)
        Session("useroffset") = "+05:30"
        Valid_Tenant_Id = sp.ExecuteScalar
        Return Valid_Tenant_Id
    End Function
    Public Function ValidateUser(ByVal UserName As String, ByVal Password As String) As String
        getuseroffsetandculture()
        Dim ValidateStatus As String = ""
        'ValidateStatus = SPs.ValidateUser(UserName, Password).ExecuteScalar
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_USER")
        sp.Command.AddParameter("@USR_ID", UserName, DbType.String)
        sp.Command.AddParameter("@USR_LOGIN_PASSWORD", Password, DbType.String)
        ValidateStatus = sp.ExecuteScalar
        Return ValidateStatus
    End Function

    Public Function SecureValidateUser() As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USR_INVALID_CNT")
        sp.Command.AddParameter("@USR_ID", txtUsrId.Text, DbType.String)
        Dim cnt As Integer
        cnt = sp.ExecuteScalar
        Return cnt
    End Function
    Public Function savelastlogin(ByVal UserName As String) As DateTime
        Dim ValidateStatus As DateTime = getoffsetdatetime(DateTime.Now)
        Dim Mode As String = "1"
        Dim ds As New DataSet

        Try

            Dim param As SqlParameter() = New SqlParameter(2) {}
            param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
            param(0).Value = UserName
            param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
            param(1).Value = Mode
            param(2) = New SqlParameter("@TIMEOUT", SqlDbType.NVarChar)
            param(2).Value = AppSettings("TIMEOUT").ToString()
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SAVE_USER_LASTLOGIN", param)
            Session("LoginUniqueID") = ds.Tables(0).Rows(0).Item("SNO")
            ValidateStatus = ds.Tables(0).Rows(0).Item("LASTLOGINTIME")
        Catch ex As Exception
        Finally

        End Try
        Return ValidateStatus
    End Function

    Public Function Validatelicence(ByVal UserName As String, ByVal Password As String) As String
        Dim ValidateStatus As String = ""
        Dim sp As New SubSonic.StoredProcedure("VALIDATE_USER_EXPIRY")
        sp.Command.AddParameter("@TID", Session("Tenant"), DbType.String)
        ValidateStatus = sp.ExecuteScalar
        Return ValidateStatus
    End Function
    Private tm As New SoftwareLocker.TrialMaker("Amantra", "Amantra", "1234")
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Start checking license & serial key existed or not
        'Dim strReturn As NameValueCollection = CType(System.Configuration.ConfigurationManager.GetSection("ProductLicense"), NameValueCollection)
        'Session.Clear()
        loginform.Attributes.Add("autocomplete", "off")
        tenantID.Attributes.Add("autocomplete", "off")
        txtUsrId.Attributes.Add("autocomplete", "off")
        txtUsrPwd.Attributes.Add("autocomplete", "off")

        If tm.CheckRegister() = False Then
            pnlLic.Visible = True
            lblKey.Text = tm.BaseString
            pnlMain.Visible = False
        Else
            pnlLic.Visible = False
            pnlMain.Visible = True
            Panel1.Visible = False
        End If
        'End checking license & serial key existed or not

        'If User.Identity.IsAuthenticated Then

        '    Response.Redirect("frmAMTDefault.aspx")
        'End If
    End Sub

    Protected Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        If tm.CheckSerial(txtSerialKey.Text) = True Then
            tm.RegisterPassword(txtSerialKey.Text)
            pnlLic.Visible = False
            pnlMain.Visible = True
            lbl1.Visible = False
        Else
            lbl1.Visible = True
            lbl1.Text = "Please enter valid Activation Code"
            pnlLic.Visible = True
            pnlMain.Visible = False

        End If
    End Sub
    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Response.Redirect("~/login.aspx")
    End Sub

    Protected Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        Dim Status As String = ""
        Dim Mode As String = "2"
        Dim ds As New DataSet
        Dim param As SqlParameter() = New SqlParameter(2) {}
        param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
        param(0).Value = txtUsrId.Text
        param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
        param(1).Value = Mode
        param(2) = New SqlParameter("@timeout", SqlDbType.NVarChar)
        param(2).Value = AppSettings("timeout")
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_USER_STATUS", param)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Status = ds.Tables(0).Rows(0).Item("SNO")
            Else
                Status = ""
            End If
        End If
        Response.Redirect("~/login.aspx")
    End Sub
    Public Function GetUserStatus(ByVal UserName As String) As String
        Dim Status As String = ""
        Dim Mode As String = "1"

        Dim ds As New DataSet

        Dim param As SqlParameter() = New SqlParameter(2) {}
        param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
        param(0).Value = UserName
        param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
        param(1).Value = Mode
        param(2) = New SqlParameter("@timeout", SqlDbType.NVarChar)
        param(2).Value = AppSettings("timeout")
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_USER_STATUS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Status = ds.Tables(0).Rows(0).Item("SNO")
        Else
            Status = ""
        End If
        Return Status
    End Function

    Public Sub Get_Property_Lease_Approval_Levels()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTY_APPROVAL_LEVELS")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("APPR_LEVELS") = ds.Tables(0).Rows(0).Item("SYSP_VAL1")
        End If
    End Sub


    Public Function DecodeFrom64(encodedData As String) As String
        Dim encodedDataAsBytes As Byte() = System.Convert.FromBase64String(encodedData)
        Dim returnValue As String = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes)
        Return returnValue
    End Function

End Class
