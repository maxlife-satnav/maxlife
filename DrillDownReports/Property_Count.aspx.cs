﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DrillDownReports_Property_Count : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }

        if (!IsPostBack)
        {
            LoadReport();
        }
    }
    public void LoadReport()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_TOTAL_PPTS_LOCWISE_DRILLDOWN");
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "SummaryPropDS";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/Property_DDR.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
}