﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DrillDownReports_Space_count : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }

        if (!IsPostBack)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_REPORT_DUP");
            sp.ExecuteScalar();

            LoadReport();
            
        }
    }

    public void LoadReport()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_SPACE_SUMMARY_DRILLDOWN");             
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "DataSet1";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/Space_DDR.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();            
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
}