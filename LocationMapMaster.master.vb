Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Partial Class LocationMapMaster
    Inherits System.Web.UI.MasterPage

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))


    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                Dim UID As String = Session("uid")
            End If

            Dim aurid As String = Session("uid")
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MENU_SCRIPT1")
            sp.Command.AddParameter("@AURID", aurid, Data.DbType.String)
            litMenu.Text = sp.ExecuteScalar
        End If
    End Sub
    Protected Sub lbtnLogOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLogOut.Click
        'Session.Abandon()
        'Session("UID") = ""
        Dim strQuery As String = ""
        strQuery = "Update " & Session("TENANT") & "."  & "[user] set Usr_logged='N' WHERE USR_ID='" & Session("uid") & "'"
        SqlHelper.ExecuteScalar(CommandType.Text, strQuery)

        Session.Abandon()
        Session("UID") = ""

        Response.Redirect("~/login.aspx")

    End Sub
End Class



