Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Configuration.ConfigurationManager
Partial Class frmForgetPwd
    Inherits System.Web.UI.Page

#Region "Variable Declaration"
    Dim strSQL As String
    Dim objDR As SqlDataReader
    Dim strMessage As String = String.Empty
    Dim aur_name As String = String.Empty
    Dim aur_Email As String = String.Empty
    Dim strPassword As String = String.Empty
    Dim msg As String = String.Empty
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Session("TENANT") = tenantID.Text
        Try
            Dim Tenant_Id As String = ""
            If Tenant_Id <> "0" Then
                txtUsrId.Text = txtUsrId.Text.Replace("'", "")
                If txtUsrId.Text <> "" Then
                    Tenant_Id = ValidateTenant(tenantID.Text)
                    If Tenant_Id <> "0" Then
                        Session("TENANT") = Tenant_Id

                        Dim sp As New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 50)
                        sp.Value = txtUsrId.Text.Trim()
                        objDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "USP_SPACE_FPWD_CHECKPW", sp)
                        If objDR.Read() Then
                            aur_Email = objDR("AUR_EMAIL").ToString '("dpavan@gmrwebteam.com")
                            aur_name = objDR("aur_known_as").ToString
                            strPassword = objDR("USR_LOGIN_PASSWORD").ToString
                            getuseroffsetandculture()
                        End If
                        objDR.Close()

                        If aur_Email = "" Then
                            strMessage = "Record not found"
                            lblMsg.Text = strMessage
                        Else
                            If strPassword <> "" Then
                                ' msg = "Dear " & aur_name & ",<br><br> Your  password is: " & clsEDSecurity.Decrypt(strPassword) & "<br><br>Regards,<br>A-mantra Admin."
                                msg = "<table align=center width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align=left>Dear " + aur_name + ",<br><br> Your  password is: " + strPassword + "<br><br>Regards,<br>" + tenantID.Text + "-QuickFMS.</td></table>"
                                Send_Mail(aur_Email, txtUsrId.Text, msg)
                                strMessage = "Password has been sent to your mail ID"
                                lblMsg.Text = strMessage
                            End If
                        End If
                    Else
                        strMessage = "This is not a valid Company ID"
                        lblMsg.Text = strMessage
                    End If
                Else
                    strMessage = "This is not a valid user ID"
                    lblMsg.Text = strMessage
                End If
            Else
                lblMsg.Text = "Invalid Company ID!"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Updating data", "Forgot password", "Load", ex)
        End Try
    End Sub

    Public Function ValidateTenant(ByVal Tenant_Name As String) As String
        Dim Valid_Tenant_Id As String = ""
        Dim sp As New SubSonic.StoredProcedure("CHECK_USER_TENANT_ISVALD")
        sp.Command.AddParameter("@TENANT_NAME", Tenant_Name, DbType.String)
        Session("useroffset") = "+05:30"
        Valid_Tenant_Id = sp.ExecuteScalar
        Return Valid_Tenant_Id
    End Function

    Public Sub Send_Mail(ByVal MailTo As String, ByVal Id As String, ByVal msg As String)
        Try
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strRR As String = String.Empty
            'Dim objData As SqlDataReader
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_FORGET_PASSWORD_DETAILS")

            'sp.Command.AddParameter("@aurid", Convert.ToString(txtUsrId.Text), SqlDbType.NVarChar)
            'Dim ds As DataSet = sp.GetDataSet()
            'If ds.Tables(0).Rows.Count > 0 Then
            '    strCC = Convert.ToString(ds.Tables(0).Rows(0).Item("aur_reporting_to"))
            '    strEmail = Convert.ToString(ds.Tables(0).Rows(0).Item("aur_email"))
            'End If
            strRM = strEmail

            Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
            Dim parms1 As New SqlParameter("@VC_MSG", SqlDbType.Text, 20000)
            Dim parms2 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
            Dim parms3 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
            Dim parms4 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
            Dim parms5 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
            Dim parms6 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
            Dim parms7 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)
            parms.Value = Id
            parms1.Value = msg
            parms2.Value = MailTo
            parms3.Value = "User Password"
            parms4.Value = getoffsetdatetime(DateTime.Now)
            parms5.Value = "NOT SENT"
            parms6.Value = "Normal Mail"
            parms7.Value = strRM
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms1, parms2, parms3, parms4, parms5, parms6, parms7)
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Updating data from database", "Forgot Password", "Load", ex)
        End Try
    End Sub

    Public Sub getuseroffsetandculture()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_OFFSET_CULTURE_BYID")
        sp.Command.AddParameter("@AUR_ID", txtUsrId.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("useroffset") = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            Session("userculture") = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            'Dim useroffsetcookie As HttpCookie = New HttpCookie("useroffset")
            'useroffsetcookie.Value = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            'Response.Cookies.Add(useroffsetcookie)
            Dim userculturecookie As HttpCookie = New HttpCookie("userculture")
            userculturecookie.Value = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Response.Cookies.Add(userculturecookie)
        End If
    End Sub
    'Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
    '    Response.Redirect("~/login.aspx")
    'End Sub
End Class
