Imports System
Imports System.IO
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.Data


Partial Class frmChangePassword
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strPwd As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            FillCustomerInfo()
        End If
    End Sub

    Private Sub FillCustomerInfo()
        Try

        
            Dim dsCustomerInfo As New DataSet

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 200)
            param(0).Value = Session("uid")

            dsCustomerInfo = objsubsonic.GetSubSonicDataSet("USP_SPACE_CHNGPWD_GETCHNGPWD", param)
            If dsCustomerInfo.Tables(0).Rows.Count > 0 Then
                txtUsrId.Text = dsCustomerInfo.Tables(0).Rows(0).Item("AUR_ID")
                txtEmpName.Text = dsCustomerInfo.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtmailid.Text = dsCustomerInfo.Tables(0).Rows(0).Item("aur_email")
            End If

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Change Password", "Load", ex)
        End Try

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If txtpwd.Text.Trim().Length < 8 Then
                lblMsg.Text = "Password should be minimum of 8 Chars."
                Exit Sub
            End If
            Dim bol As Boolean = clsSecurity.checkPassword(txtpwd.Text.Trim())
            If bol = False Then
                lblMsg.Text = "Password should be minimum of 8 Chars, 2 Numerics and 1 Specail Char."
                Exit Sub
            End If

            Dim param(0) As SqlParameter

            param(0) = New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 200)
            param(0).Value = Session("uid")
            Dim dsGetOldPWD As New DataSet

            dsGetOldPWD = objsubsonic.GetSubSonicDataSet("USP_SPACE_VIEW_GETVIEWBTN", param)
            If dsGetOldPWD.Tables(0).Rows.Count > 0 Then
                strPwd = dsGetOldPWD.Tables(0).Rows(0).Item("USR_LOGIN_PASSWORD")
            End If

            If txtOldPwd.Text = "" Then
                lblMsg.Visible = True
                lblMsg.Text = "Please Enter Old Password "
                Exit Sub
            End If
            If txtpwd.Text = "" Then
                lblMsg.Visible = True
                lblMsg.Text = "Please Enter New Password "
                Exit Sub
            End If
            If Trim(txtOldPwd.Text) = Trim(strPwd) Then
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please Enter Valid Old Password "
                Exit Sub
            End If

            If Trim(txtpwd.Text) = Trim(txtcpwd.Text) Then

                Dim paramPWD(5) As SqlParameter
                paramPWD(0) = New SqlParameter("@vc_LGNPWD", SqlDbType.NVarChar, 200)
                paramPWD(0).Value = txtpwd.Text.Trim()
                paramPWD(1) = New SqlParameter("@VC_ACTIVE", SqlDbType.NVarChar, 200)
                paramPWD(1).Value = "Y"
                paramPWD(2) = New SqlParameter("@VC_UPDTBY", SqlDbType.NVarChar, 200)
                paramPWD(2).Value = Session("uid").ToString().Trim()
                paramPWD(3) = New SqlParameter("@VC_UPDTON", SqlDbType.DateTime)
                paramPWD(3).Value = getoffsetdatetime(DateTime.Now)
                paramPWD(4) = New SqlParameter("@VC_LOGGED", SqlDbType.NVarChar, 200)
                paramPWD(4).Value = "N"
                paramPWD(5) = New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 200)
                paramPWD(5).Value = Session("uid").ToString().Trim()

                ObjSubsonic.GetSubSonicExecute("USP_SPACE_CHNGPWD_UPDATEPWD", paramPWD)
                lblMsg.Text = "Password Has been Changed and Sent Your Mail ID"
                lblPwd.Visible = False
                btnSubmit.Enabled = True
                Dim strMsg As String
                strMsg = "Dear " & txtEmpName.Text & "," & vbCrLf & vbCrLf & _
                    "<br><br>This is to inform you that your password has been changed to  '" & txtpwd.Text & "'. " & vbCrLf & vbCrLf & _
                    "<br><br>Thanking You," & vbCrLf & _
                    "<br>Amantra Admin."

                'Send_Mail(txtmailid.Text, txtUsrId.Text, "New Password -- a-mantra tool", strMsg)
                Response.Redirect("frmFinalPage.aspx?staid=ChangePassword")
            End If


        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Change Password", "Load", ex)
        End Try
    End Sub
End Class
