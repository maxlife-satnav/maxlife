﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmLicense.aspx.vb" Inherits="frmLicence" %>


<!DOCTYPE html>
<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="~/BootStrapCSS/images/favicon.ico" />
    <title>a-mantra :: Facilities Management Solutions</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="BootStrapCSS/menu/css/metismenu.min.css" rel="stylesheet" />
    <link href="BootStrapCSS/menu/css/menu.css" rel="stylesheet" />

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <a class="brand" href='#'>
                    <img src='<%=Page.ResolveUrl("~/BootStrapCSS/images/logo_Quick.gif")%>' class="ie8-pngcss" alt="" style="margin-top: 2px; height: 60px; width: 300px;" />
                </a>
            </div>
        </nav>

        <div style="clear: both; min-height: 425px; width: 100%; position: inherit;">
            <div id="wrapper">
                <div id="page-wrapper" class="row">
                    <div class="row form-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>
                                <asp:Label ID="lbl1" runat="server" Text="Your trial license has expired, Please contact  helpdesk@quickfms.com" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <asp:Literal ID="litMenu" runat="server"></asp:Literal>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-3 pull-left">
                    Other Links : 
                <a href="http://www.quickfms.com/" target="_blank" style="color: white">www.quickfms.com</a>
                </div>
                <div class="col-md-3 pull-right">
                    &copy; 2015 QuickFMS
                </div>
            </div>
        </div>

    </form>
</body>
</html>


