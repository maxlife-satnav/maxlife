﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

Partial Class HDM_HDM_Webfiles_frmHDMRMRequisitionDetails

    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim filePath As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        filePath = HttpContext.Current.Request.Url.Authority & "/UploadFiles/" & Session("TENANT")
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                'BindLocations()
                Dim UID As String = Session("uid")
                Session("reqid") = Request.QueryString("rid").ToString()
                'BindUsers(UID)
                BindRequisition()
                divExpenses.Visible = False
                txtAC.Text = "0"
                txtLBC.Text = "0"
                txtSC.Text = "0"
            End If
        End If
    End Sub

    Private Sub BindRequisition()
        Try
            Dim ds As New DataSet
            Dim param As SqlParameter() = New SqlParameter(0) {}
            param(0) = New SqlParameter("@REQID", Session("reqid").ToString())
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQUISITION_BY_REQ_ID", param)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblReqId.Text = ds.Tables(0).Rows(0)("REQUEST_ID").ToString()
                    'Dim UID As String = ds.Tables(0).Rows(0).Item("CREATED_BY")
                    'BindUsers(UID)
                    'If ds.Tables(0).Rows(0)("REQUESTED_BY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("REQUESTED_BY").ToString() IsNot Nothing Then
                    '    ddlEmp.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQUESTED_BY")).Selected = True
                    'End If
                    txtEmp.Text = ds.Tables(0).Rows(0).Item("REQUESTED_BY")
                    txtDepartment.Text = ds.Tables(0).Rows(0)("DEPARTMENT").ToString()
                    txtSpaceID.Text = ds.Tables(0).Rows(0)("SPACE_ID").ToString()

                    BindLocations()
                    ddlLocation.ClearSelection()
                    If ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() IsNot Nothing Then
                        ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString()).Selected = True
                    End If
                    BindMainCategory()
                    ddlMainCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() IsNot Nothing Then
                        ddlMainCategory.Items.FindByValue(ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString()).Selected = True
                    End If
                    BindSubCategory()
                    ddlSubCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() IsNot Nothing Then
                        ddlSubCategory.Items.FindByValue(ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString()).Selected = True
                    End If
                    BindChildCategory()
                    ddlChildCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() IsNot Nothing Then
                        ddlChildCategory.Items.FindByValue(ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString()).Selected = True
                    End If
                    BindAssetLocations()
                    ddlAssetLoaction.ClearSelection()
                    If ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() IsNot Nothing Then
                        ddlAssetLoaction.Items.FindByValue(ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString()).Selected = True
                    End If
                    txtMobile.Text = ds.Tables(0).Rows(0)("MOBILE").ToString()
                    BindRepeatCalls()
                    ddlRepeatCalls.ClearSelection()
                    If ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() IsNot Nothing Then
                        ddlRepeatCalls.Items.FindByValue(ds.Tables(0).Rows(0)("REPEAT_CALL").ToString()).Selected = True
                    End If
                    BindImpact()
                    ddlImpact.ClearSelection()
                    If ds.Tables(0).Rows(0)("IMPACT").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("IMPACT").ToString() IsNot Nothing Then
                        ddlImpact.Items.FindByValue(ds.Tables(0).Rows(0)("IMPACT").ToString()).Selected = True
                    End If
                    BindUrgency()
                    ddlUrgency.ClearSelection()
                    If ds.Tables(0).Rows(0)("URGENCY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("URGENCY").ToString() IsNot Nothing Then
                        ddlUrgency.Items.FindByValue(ds.Tables(0).Rows(0)("URGENCY").ToString()).Selected = True
                    End If
                    BindStatus()
                    ddlStatus.ClearSelection()
                    If ds.Tables(0).Rows(0)("STATUS_TITLE").ToString <> "" AndAlso ds.Tables(0).Rows(0)("STATUS_TITLE").ToString() IsNot Nothing Then
                        ' ddlStatus.Items.FindByText(ds.Tables(0).Rows(0)("STATUS_TITLE").ToString()).Selected = True
                    End If

                    txtPB.Text = ds.Tables(0).Rows(0)("REMARKS").ToString()
                    'hrfDoc.Text = ds.Tables(0).Rows(0)("DOC").ToString()
                    'hrfDoc.NavigateUrl = "../../UploadFiles/" + Session("TENANT") + "/" + ds.Tables(0).Rows(0)("DOC").ToString()

                    Dim list As List(Of fileInfo)
                    list = GetFiles()
                    Dim jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(list)
                    jsonstringhdn.Value = jsonstring
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Class fileInfo
        Public Property Name As String
        Public Property Path As String
        Public Property UplTimeName As String
    End Class

    Public Function GetFiles() As List(Of fileInfo)
      Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_FILES")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, Data.DbType.String)
        Dim fileslist As List(Of fileInfo) = New List(Of fileInfo)()

        Dim dr As SqlDataReader
        dr = sp.GetReader()
        While (dr.Read())
            fileslist.Add(New fileInfo() With {.Name = dr.GetString(0), .Path = "/UploadFiles/" & Session("TENANT") & "/" & dr.GetString(1), .UplTimeName = dr.GetString(1)})
        End While
        dr.Close()
        Return fileslist
    End Function

    'Private Sub BindUsers(ByVal AUR_ID As String)

    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '    param(0).Value = AUR_ID
    '    ObjSubsonic.Binddropdown(ddlEmp, "HDM_BINDUSERS_NAME", "NAME", "AUR_ID", param)
    '    Dim li As ListItem = ddlEmp.Items.FindByValue(AUR_ID)
    '    If Not li Is Nothing Then
    '        li.Selected = True
    '    End If
    'End Sub

    Private Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindMainCategory()
        ddlMainCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE")
    End Sub

    Public Sub BindSubCategory()
        ddlSubCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub

    Public Sub BindAssetLocations()
        ddlAssetLoaction.Items.Clear()
        ObjSubsonic.Binddropdown(ddlAssetLoaction, "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS", "HAL_LOC_NAME", "HAL_LOC_CODE")
    End Sub

    Public Sub BindRepeatCalls()
        ddlRepeatCalls.Items.Clear()
        ObjSubsonic.Binddropdown(ddlRepeatCalls, "HDM_GET_ALL_ACTIVE_REPEATCALLS", "RPT_NAME", "RPT_CODE")
    End Sub
    Public Sub BindStatus()
        ddlStatus.Items.Clear()
        Dim ds1 As New DataSet
        'ObjSubsonic.Binddropdown(ddlStatus, "HDM_STATUS_BIND", "STA_TITLE", "STA_ID")
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        'ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_BIND_STATUS")
        ddlStatus.DataSource = ds1
        ddlStatus.DataTextField = "STA_TITLE"
        ddlStatus.DataValueField = "STA_ID"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, "--Select--")
    End Sub

    Public Sub BindImpact()
        ddlImpact.Items.Clear()
        ObjSubsonic.Binddropdown(ddlImpact, "HDM_GET_ALL_ACTIVE_IMPACT", "IMP_NAME", "IMP_CODE")
    End Sub

    Public Sub BindUrgency()
        ddlUrgency.Items.Clear()
        ObjSubsonic.Binddropdown(ddlUrgency, "HDM_GET_ALL_ACTIVE_URGENCY", "UGC_NAME", "UGC_CODE")
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            If ddlStatus.SelectedValue <> "--Select--" Then

                Dim param As SqlParameter() = New SqlParameter(6) {}
                param(0) = New SqlParameter("@REQ_ID", lblReqId.Text)
                param(1) = New SqlParameter("@STA_ID", ddlStatus.SelectedValue)
                param(2) = New SqlParameter("@UPDATE_REMARKS", txtProbDesc.Text)
                param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                param(4) = New SqlParameter("@ADDN_COST", txtAC.Text)
                param(5) = New SqlParameter("@LBR_COST", txtLBC.Text)
                param(6) = New SqlParameter("@SPR_COST", txtSC.Text)

                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQ_UPDATE", param)
                lblMessage.Visible = True
                lblMessage.Text = "Request " + lblReqId.Text + " Updated Successfully"
            Else
                lblMessage.Text = "Please select Status and Submit"
            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try

        Response.Redirect("frmRMViewRequisitions.aspx?Updated=1")
        'Response.Redirect("frmhelpthanks.aspx?id=2" & "&Reqid=" & lblReqId.Text)
        'Response.Redirect("frmhelpthanks.aspx?id=" & ddlStatus.SelectedItem.Value & "&Reqid=" & lblReqId.Text)
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmRMViewRequisitions.aspx")

    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        If ddlStatus.SelectedValue = "9" Then
            divExpenses.Visible = True
        Else
            divExpenses.Visible = False
        End If
    End Sub

End Class
