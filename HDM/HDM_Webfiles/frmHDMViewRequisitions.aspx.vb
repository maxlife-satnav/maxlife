﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class HDM_HDM_Webfiles_frmHDMViewRequisitions
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            BindLocations()
            BindChildCategory()
            FillStatus()
            If Request.QueryString("Reopened") = 1 Then
                lblMsg.Text = " Request " + Session("reqid") + " Reopened Successfully"
            End If
        End If
    End Sub

    Private Sub FillStatus()
        Dim dsStatus As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_HDMSTATUS_FILTERS")
        sp.Command.AddParameter("@TYPE", 1, Data.DbType.String)
        dsStatus = sp.GetDataSet()
        ddlStatus.DataSource = dsStatus
        ddlStatus.DataSource = dsStatus
        ddlStatus.DataTextField = "STA_TITLE"
        ddlStatus.DataValueField = "STA_ID"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindGrid()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_USP_RM_REQUISITIONS_GetByUserId")
        'sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        'gvViewRequisitions.DataSource = sp.GetDataSet
        'gvViewRequisitions.DataBind()

        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_VIEW_REQUISITIONS")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        ds = sp.GetDataSet
        gvViewRequisitions.DataSource = ds
        gvViewRequisitions.DataBind()
        'Dim ds1 As New DataSet
        'ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_BIND_STATUS")
        'For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
        '    Dim ddlstat As DropDownList = CType(gvViewRequisitions.Rows(i).FindControl("ddlStatus"), DropDownList)
        '    ddlstat.DataSource = ds1
        '    ddlstat.DataTextField = "STA_TITLE"
        '    ddlstat.DataValueField = "STA_ID"
        '    ddlstat.DataBind()
        '    'ddlstat.Items.Insert(0, "--Select--")
        '    ddlstat.ClearSelection()
        '    ddlstat.Items.FindByValue(ds.Tables(0).Rows(i).Item("SER_STATUS")).Selected = True
        'Next

    End Sub
    Public Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)


    End Sub
    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")

    End Sub
    Protected Sub gvViewRequisitions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_VIEW_REQUISITIONS")
            sp.Command.AddParameter("@REQID", txtReqId.Text, Data.DbType.String)
            sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, Data.DbType.String)
            sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, Data.DbType.String)
            sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String)
            sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String)
            sp.Command.AddParameter("@STATUS", ddlStatus.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
            gvViewRequisitions.DataSource = sp.GetDataSet
            gvViewRequisitions.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

End Class
