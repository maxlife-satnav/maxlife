﻿app.service("HDMUtilityService", function ($http, $q) {

    var path = window.location.origin;
    //+ "/Amantra_Multi_UI";

    var deferred = $q.defer();

    this.getMainCategories = function () {
        deferred = $q.defer();
        return $http.get(path + '/api/HDMUtility/GetMainCategories')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.getSubCatbyMainCategories = function (main) {
        var deferred = $q.defer();
        return $http.post(path + '/api/HDMUtility/GetSubCatByMainCategories', main)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.getChildCatBySubCategories = function (sub) {
        var deferred = $q.defer();
        return $http.post(path + '/api/HDMUtility/GetChildCatBySubCategories', sub)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.getStatus = function () {
        var deferred = $q.defer();
        return $http.get(path + '/api/HDMUtility/GetStatus')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.getRequestedUsers = function () {
        var deferred = $q.defer();
        return $http.get(path + '/api/HDMUtility/GetRequestedUsers')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

  

});