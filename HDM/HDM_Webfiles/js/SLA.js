﻿//app.directive('validSubmit', ['$parse', function ($parse) {
//    return {
//        // we need a form controller to be on the same element as this directive
//        // in other words: this directive can only be used on a &lt;form&gt;
//        require: 'form',
//        // one time action per form
//        link: function (scope, element, iAttrs, form) {
//            form.$submitted = false;
//            // get a hold of the function that handles submission when form is valid
//            var fn = $parse(iAttrs.validSubmit);

//            // register DOM event handler and wire into Angular's lifecycle with scope.$apply
//            element.on('submit', function (event) {
//                scope.$apply(function () {
//                    // on submit event, set submitted to true (like the previous trick)
//                    form.$submitted = true;
//                    // if form is valid, execute the submission handler function and reset form submission state
//                    if (form.$valid) {
//                        fn(scope, { $event: event });
//                        form.$submitted = false;
//                    }
//                });
//            });
//        }
//    };
//}
//])
app.service("SLAService", function ($http, $q) {
    var deferred = $q.defer();

    //to save
    this.SaveSLADetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post('../../../api/SLA/SaveDetails', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };
    //update
    this.UpdateSLADetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post('../../../api/SLA/UpdateDetails', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };
    //edit
    this.EditSLADetails = function (dt) {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/EditSLA/' + dt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetMainCategory = function () {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetMaincategory')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetCountries = function () {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetCountries')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetCityByCountry = function (SLA_CNY_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetCityByCountry/' + SLA_CNY_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetLocationByCity = function (SLA_CTY_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetLocationByCity/' + SLA_CTY_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    ///Amantra_Multi_UI/api/SLA/GetSubCategoryByMain/4444
    this.GetSubCategoryByMain = function (SLA_MNC_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetSubCategoryByMain/' + SLA_MNC_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //to get child by sub category
    this.GetChildCategoryBySub = function (SLA_SUBC_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetChildCategoryBySubCat/' + SLA_SUBC_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //For Timer View
    this.GetSLATime = function () {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetSLATime')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {

              deferred.reject(response);
              return deferred.promise;

          });
    };

    //For Grid
    this.GetSLAList = function () {
        deferred = $q.defer();
        return $http.get('../../../api/SLA/GetSLAGrid')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('SLAcontroller', function ($scope, $q, SLAService, $timeout, UtilityService) {
    $scope.SLAMaster = {};
    $scope.ActionStatus = 0;
    $scope.countrylist = [];
    $scope.cny = [];
    $scope.Citylist = [];
    $scope.locationlist = [];
    $scope.maincategorylist = [];
    $scope.SubCategorylist = [];

    $scope.ChildCategorylist = [];
    $scope.populationlist = [];
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    $scope.timetype = [{ value: "1", Name: "Minute(s)" }, { value: "60", Name: "Hour(s)" }, { value: "1440", Name: "Day(s)" }];

    //main category drop down bind

    //to bind city on country change
    $scope.CountryChanged = function () {
        $scope.cny.push($scope.SLAMaster.SLA_CNY_CODE);
        SLAService.GetCityByCountry($scope.SLAMaster.SLA_CNY_CODE).then(function (data) {
            $scope.Citylist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //location by city
    $scope.CityChanged = function () {
        SLAService.GetLocationByCity($scope.SLAMaster.SLA_CTY_CODE).then(function (data) {
            $scope.locationlist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //to bind subcategory on main category change
    $scope.MainCategoryChanged = function () {
        SLAService.GetSubCategoryByMain($scope.SLAMaster.SLA_MNC_CODE).then(function (data) {
            $scope.SubCategorylist = data;
            // $scope.SubCategoryChanged(data.SLA_SUBC_CODE);
        }, function (error) {
            console.log(error);
        });
    }

    //to bind childcategory on sub category change
    $scope.SubCategoryChanged = function () {
        SLAService.GetChildCategoryBySub($scope.SLAMaster.SLA_SUBC_CODE).then(function (data) {
            $scope.ChildCategorylist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //For Grid column Headers
    var columnDefs = [
         { headerName: "Country", field: "CNY_NAME", cellClass: "grid-align", filter: 'set' },
        { headerName: "City", field: "CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
        { headerName: "Main Category", field: "MNC_NAME", cellClass: "grid-align" },
        { headerName: "Sub Category", field: "SUBC_NAME", cellClass: "grid-align" },
        { headerName: "Child Category", field: "CHC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Edit", template: '<a data-ng-click ="EditSLA(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        SLAService.GetSLAList().then(function (data) {
            if (data != null) {
                $scope.gridata = data;
               // $scope.createNewDatasource();
                $scope.gridOptions.api.setRowData(data);
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Data Found');
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };
    //$scope.pageSize = '10';

    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };
        $scope.gridOptions.api.setDatasource(dataSource);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1500);

    //To save a record
    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            $scope.dataobject = { SLA: $scope.SLAMaster, SLADET: $scope.SLADetails, UPLSTA: $scope.RetStatus };
            SLAService.UpdateSLADetails($scope.dataobject).then(function (dataobj) {
                progress(0, '', false);
                $scope.clear();
                showNotification('success', 8, 'bottom-right', 'Data Updated Successfully');

            }, function (response) {
                progress(0, '', false);
            });
        }
        else {
            var count = 0;
            var keepGoing = true;
            angular.forEach($scope.SLADetails, function (value) {
                count = 0;
                if (keepGoing) {
                    angular.forEach(value.Value, function (innerval) {
                        if (innerval.Value.SLAD_ESC_TIME != 0) {
                            count = count + 1;
                        }
                    });
                    if (count == 0) {
                        keepGoing = false;
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Please enter at least one value for the each Status');
                    }
                }
            });

            if (keepGoing) {
                $scope.dataobject = { SLA: $scope.SLAMaster, SLADET: $scope.SLADetails, selected: $scope.SLA };
                SLAService.SaveSLADetails($scope.dataobject).then(function (dataobj) {
                    $scope.gridata.unshift(dataobj);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    $scope.clear();
                    progress(0, '', false);
                    showNotification('success', 8, 'bottom-right', 'Data Inserted Successfully');
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
    }

    //For Timer view
    $scope.GetSLA = function () {
        SLAService.GetSLATime().then(function (response) {
            $scope.Roles = response.ROLELST;
            $scope.SLADetails = response.SLADET;            
        }, function (error) {
        });
    }

    //clear 
    $scope.clear = function () {
        $scope.SLAMaster = {};
        $scope.SLADetails = false;
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
    }

    //Edit
    $scope.EditSLA = function (data) {
        progress(0, 'Loading...', true);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        var dt = data.SLA_ID;
        SLAService.EditSLADetails(dt).then(function (response) {
            SLAService.GetSubCategoryByMain(data.SLA_MNC_CODE).then(function (subcatData) {     //sub cat ddl
                $scope.SubCategorylist = subcatData;
                SLAService.GetChildCategoryBySub(data.SLA_SUBC_CODE).then(function (childcatData) {     //child cat ddl
                    $scope.ChildCategorylist = childcatData;
                    SLAService.GetCityByCountry(data.SLA_CNY_CODE).then(function (cityData) {        //city ddl
                        $scope.Citylist = cityData;
                        SLAService.GetLocationByCity(data.SLA_CTY_CODE).then(function (locData) {  //location ddl                            
                                $scope.locationlist = locData;                            
                                $scope.SLAMaster = data;
                                $scope.Roles = response.ROLELST;
                                $scope.SLADetails = response.SLADET;
                        }, function (error) {
                            console.log(error);
                        });
                    }, function (error) {
                        console.log(error);
                    });
                }, function (error) {
                    console.log(error);
                });
            }, function (error) {
                console.log(error);
            });
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });
    }

    UtilityService.getCountires(1).then(function (response) {
        $scope.countrylist = response.data;
    }, function (error) {
        console.log(error);
    });

    function getmaincat() {
        SLAService.GetMainCategory().then(function (data) {
            $scope.maincategorylist = data;
        }, function (error) {
            console.log(error);
        });
    }

    $timeout($scope.getcountires, 200)
    $timeout(getmaincat(), 500)

});
