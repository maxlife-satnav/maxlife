﻿app.service("ServiceEscalationService", function ($http, $q) {
    var deferred = $q.defer();
    //to save
    this.SaveSEMDetails = function (dataobject) {
        console.log(dataobject);
        deferred = $q.defer();
        return $http.post('../../../api/ServiceEscalationMapping/SaveDetails', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //update
    this.UpdateSEMDetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post('../../../api/ServiceEscalationMapping/UpdateDetails', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //edit
    this.EditSEMDetails = function (dt) {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/EditSEMDetails/' + dt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetMainCategory = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetMaincategory')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetCountries = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetCountries')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    ///Amantra_Multi_UI/api/SLA/GetSubCategoryByMain/4444
    this.GetSubCategoryByMain = function (SEM_MNC_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetSubCategoryByMain/' + SEM_MNC_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetCityByCountry = function (SEM_CNY_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetCityByCountry/' + SEM_CNY_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetLocationByCity = function (SEM_CTY_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetLocationByCity/' + SEM_CTY_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetTowerByLocation = function (SEM_LOC_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetTowerByLocation/' + SEM_LOC_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //to get child by sub category
    this.GetChildCategoryBySub = function (SEM_SUBC_CODE) {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetChildCategoryBySubCat/' + SEM_SUBC_CODE)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //For Grid
    this.GetSEMList = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetSEMDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetRoleUserList = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceEscalationMapping/GetRoleUserDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('ServiceController', function ($scope, $q, ServiceEscalationService) {
    $scope.SEMMaster = {};
    $scope.ActionStatus = 0;
    $scope.countrylist = [];
    $scope.Citylist = [];
    $scope.locationlist = [];
    $scope.towerlist = [];
    $scope.maincategorylist = [];
    $scope.SubCategorylist = [];
    $scope.ChildCategorylist = [];
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.roleuserspageSize = '10';
    //$scope.semdetailspageSize = '10';

    //main category drop down bind
    ServiceEscalationService.GetMainCategory().then(function (data) {
        $scope.maincategorylist = data;
        //country
        ServiceEscalationService.GetCountries().then(function (data) {
            $scope.countrylist = data;
        }, function (error) {
            console.log(error);
        });
    }, function (error) {
        console.log(error);
    });

    //to bind city on country change
    $scope.CountryChanged = function () {
        ServiceEscalationService.GetCityByCountry($scope.SEMMaster.SEM_CNY_CODE).then(function (data) {
            $scope.Citylist = data;
            $scope.CityChanged(data.SEM_CTY_CODE);
        }, function (error) {
            console.log(error);
        });
    }

    //location by city
    $scope.CityChanged = function () {
        ServiceEscalationService.GetLocationByCity($scope.SEMMaster.SEM_CTY_CODE).then(function (data) {
            $scope.locationlist = data;
            $scope.LocationChanged(data.SEM_LOC_CODE);
        }, function (error) {
            console.log(error);
        });
    }

    //tower by location
    $scope.LocationChanged = function () {
        ServiceEscalationService.GetTowerByLocation($scope.SEMMaster.SEM_LOC_CODE).then(function (data) {
            $scope.towerlist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //to bind subcategory on main category change
    $scope.MainCategoryChanged = function () {
        ServiceEscalationService.GetSubCategoryByMain($scope.SEMMaster.SEM_MNC_CODE).then(function (data) {
            $scope.SubCategorylist = data;
            $scope.SubCategoryChanged(data.SEM_SUBC_CODE);
        }, function (error) {
            console.log(error);
        });
    }
    //to bind childcategory on sub category change

    $scope.SubCategoryChanged = function () {
        ServiceEscalationService.GetChildCategoryBySub($scope.SEMMaster.SEM_SUBC_CODE).then(function (data) {
            $scope.ChildCategorylist = data;
            $scope.CountryChanged(data.SEM_CNY_CODE);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.createNewDatasource = function (data, pagesize) {
        var dataSource = {
            pageSize: parseInt(pagesize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = data.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if (data.length <= params.endRow) {
                        lastRow = data.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };
        return dataSource;
    }

    //For Grid column Headers
    $scope.roleusersoptionscolumnDefs = [
          { headerName: "Role ", field: "ROL_ACRONYM", cellClass: "grid-align", rowGroupIndex: 0 },
    ];

    $scope.roleusersoptions = {
        columnDefs: $scope.roleusersoptionscolumnDefs,
        rowData: null,
        angularCompileRows: true,
        enableScrollbars: false,
        rowSelection: 'multiple',
        groupSelectsChildren: true,
        width: 50,
        suppressRowClickSelection: true,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Role", field: "AUR_KNOWN_AS",
            cellRenderer: {
                renderer: "group",
                checkbox: true
            }
        },
        onReady: function () {
            $scope.roleusersoptions.api.sizeColumnsToFit()
        },
    };

    //To save a record
    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);
        var count = 0;
        var keep = true;
        if ($scope.IsInEdit) {
            if (keep) {
                $scope.roleusersoptions.api.getSelectedRows().forEach(function (node) {
                    if (node.ROL_ORDER == 1) {
                        count++;
                    }
                });
                if (count == 0) {
                    keep = false;
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Please Select One Value For Role HD INCHARGE');
                }
            }
            if (keep) {
                $scope.dataobject = { SEM: $scope.SEMMaster, SEMD: $scope.roleusersoptions.api.getSelectedRows() };
                ServiceEscalationService.UpdateSEMDetails($scope.dataobject).then(function (dataobj) {
                    //$scope.semdetailsOptions.api.setRowData($scope.gridata);
                    $scope.LoadSemDetails();
                    $scope.clear();
                    progress(0, '', false);
                    showNotification('success', 8, 'bottom-right', 'Data Updated Succesfully');
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
        else {
            if (keep) {
                $scope.roleusersoptions.api.getSelectedRows().forEach(function (node) {
                    if (node.ROL_ORDER == 1) {
                        count++;
                    }
                });
                if (count == 0) {
                    keep = false;
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Please Select One Value For Role HD INCHARGE');
                }
            }
            if (keep) {
                $scope.dataobject = { SEM: $scope.SEMMaster, SEMD: $scope.roleusersoptions.api.getSelectedRows() };
                ServiceEscalationService.SaveSEMDetails($scope.dataobject).then(function (dataobj) {
                    $scope.LoadSemDetails();
                    //console.log(dataobj);
                    //$scope.gridata.unshift(dataobj);
                    //$scope.semdetailsOptions.api.setRowData($scope.gridata);
                    $scope.clear();
                    progress(0, '', false);
                    showNotification('success', 8, 'bottom-right', 'Data Inserted Succesfully');
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
    }

    //clear 
    $scope.clear = function () {
        // && $scope.roleusersoptions.api.getBestCostNodeSelection()[0].parent.id == -1
        //if ($scope.roleusersoptions.api.getBestCostNodeSelection().length == 0 || $scope.roleusersoptions.api.getBestCostNodeSelection()[0].parent == null){
        //    alert("yahhooo");
        //}
        $scope.SEMMaster = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.roleusersoptions.api.deselectAll();
    }

    //Edit
    $scope.EditSEMDetails = function (data) {
        progress(0, 'Loading...', true);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        var dt = data.SEM_ID;
        ServiceEscalationService.EditSEMDetails(dt).then(function (response) {
            $scope.SEMMaster = data;
            $scope.roleusersoptions.api.forEachNode(function (node) {
                if (node.data != null) {
                        $scope.roleusersoptions.api.selectNode(node, false)
                }
            });
            for (i = 0; i < response.length; i++) {
                var user = response[i];                
                $scope.roleusersoptions.api.forEachNode(function (node) {
                    if (node.data != null) {
                        if (user.AD_ID == node.data.AD_ID & user.ROL_ID == node.data.ROL_ID) {
                            //console.log(user.AD_ID + '/' + user.ROL_ID + ' - ' + node.data.AD_ID+'/'+node.data.ROL_ID);
                            $scope.roleusersoptions.api.selectNode(node, true)
                        }
                    }
                });
            }

            setTimeout(function () {
                // $scope.MainCategoryChanged(data.SEM_MNC_CODE);
                ServiceEscalationService.GetSubCategoryByMain(data.SEM_MNC_CODE).then(function (Maindata) {
                    $scope.SubCategorylist = Maindata;
                    ServiceEscalationService.GetChildCategoryBySub(data.SEM_SUBC_CODE).then(function (Subdata) {
                        $scope.ChildCategorylist = Subdata;
                        //$scope.CountryChanged(data.SEM_CNY_CODE);
                        ServiceEscalationService.GetCityByCountry(data.SEM_CNY_CODE).then(function (Citydata) {
                            $scope.Citylist = Citydata;
                            //$scope.CityChanged(data.SEM_CTY_CODE);
                            ServiceEscalationService.GetLocationByCity(data.SEM_CTY_CODE).then(function (Locdata) {
                                $scope.locationlist = Locdata;
                                //$scope.LocationChanged(data.SEM_LOC_CODE);
                                ServiceEscalationService.GetTowerByLocation($scope.SEMMaster.SEM_LOC_CODE).then(function (Twrdata) {
                                    $scope.towerlist = Twrdata;
                                }, function (error) {
                                    console.log(error);
                                });
                            }, function (error) {
                                console.log(error);
                            });

                        }, function (error) {
                            console.log(error);
                        });

                    }, function (error) {
                        console.log(error);
                    });
                }, function (error) {
                    console.log(error);
                });

                //$scope.SubCategoryChanged(data.SEM_SUBC_CODE);
                //$scope.CityChanged(data.SEM_CTY_CODE);
                //$scope.LocationChanged(data.SEM_LOC_CODE);
                progress(0, '', false);
            }, 1000);

        }, function (response) {
            progress(0, '', false);
        });
    }

    ////////////////////////////////

    $scope.semdetailscolumnDefs = [
       { headerName: "Country", field: "CNY_NAME", cellClass: "grid-align" },
       { headerName: "City", field: "CTY_NAME", cellClass: "grid-align" },
       { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
       { headerName: "Tower", field: "TWR_NAME", cellClass: "grid-align" },
       { headerName: "Main Category", field: "MNC_NAME", cellClass: "grid-align" },
       { headerName: "Sub Category", field: "SUBC_NAME", cellClass: "grid-align" },
       { headerName: "Child Category", field: "CHC_TYPE_NAME", cellClass: "grid-align" },
       { headerName: "Edit", template: '<a data-ng-click ="EditSEMDetails(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" }
    ];

    $scope.semdetailsOptions = {
        columnDefs: $scope.semdetailscolumnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        onReady: function () {
            $scope.semdetailsOptions.api.sizeColumnsToFit()
        },
    };

    $scope.LoadRoleUsers = function () {
        progress(0, 'Loading...', true);
        ServiceEscalationService.GetRoleUserList().then(function (data) {
            $scope.gridata = data;
            // $scope.roleusersoptions.api.setDatasource($scope.createNewDatasource($scope.gridata, $scope.roleuserspageSize));
            $scope.roleusersoptions.api.setRowData($scope.gridata);
            setTimeout(function () { $scope.LoadSemDetails() }, 1000);
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });
    }


    // To display grid row data
    $scope.LoadSemDetails = function () {
        progress(0, 'Loading...', true);
        ServiceEscalationService.GetSEMList().then(function (data) {
            $scope.gridata = data;
            //$scope.semdetailsOptions.api.setDatasource($scope.createNewDatasource($scope.gridata, $scope.semdetailspageSize));
            $scope.semdetailsOptions.api.setRowData($scope.gridata);
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.semdetailsOptions.api.setQuickFilter(value);
    }

    setTimeout(function () {
        $scope.LoadRoleUsers();
    }, 2000);
});
