﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmReqHistoryDetails.aspx.vb" Inherits="HDM_HDM_Webfiles_frmReqHistoryDetails" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="row table table table-condensed table-responsive">
            <div class="form-group">
                <div class="col-md-9">
                    <h4>Request History Details</h4>
                    <asp:GridView ID="gvHist" runat="server" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                        <Columns>
                            <asp:BoundField DataField="CREATEDBY" HeaderText="Requested By" ItemStyle-Width="20px"></asp:BoundField>
                            <asp:BoundField DataField="ASSIGNTO" HeaderText="Assigned To" ItemStyle-Width="20px"></asp:BoundField>
                            <asp:BoundField DataField="UPDATEDDATE" HeaderText="Updated Date" ItemStyle-Width="18px"></asp:BoundField>
                            <asp:BoundField DataField="UPDATEDBY" HeaderText="Updated By" ItemStyle-Width="20px"></asp:BoundField>
                            <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ItemStyle-Width="20px"></asp:BoundField>
                            <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-Width="10px"></asp:BoundField>
                        </Columns>
                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
