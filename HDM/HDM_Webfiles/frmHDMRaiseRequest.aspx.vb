﻿Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports System.Web
Imports System.Web.Services
Imports System.Web.Script.Services


Partial Class HDM_HDM_Webfiles_frmHDMRaiseRequest
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim MaintenanceReq As String = ""

    Private Sub ClearAll()
        'txtDepartment.Text = ""
        'txtSpaceID.Text = ""
        'txtEmpId.Text = ""
        ddlLocation.SelectedIndex = 0
        ddlMainCategory.SelectedIndex = 0
        ddlSubCategory.SelectedIndex = 0
        ddlChildCategory.SelectedIndex = 0
        txtMobile.Text = ""
        ddlImpact.SelectedIndex = 1
        ddlUrgency.SelectedIndex = 1
        ddlAssetLoaction.SelectedIndex = 1
        ddlRepeatCalls.SelectedIndex = 1
        txtProbDesc.Text = ""
        ancReqid.Visible = False
        btnDraft.Visible = True
        btnSubmit.Text = "Submit Request"
    End Sub

    Private Sub ClearFileds_Rbt()
        txtEmpId.Text = ""
        txtEmpId.Enabled = True
        txtName.Text = ""
        txtDepartment.Text = ""
        txtSpaceID.Text = ""
        ddlLocation.ClearSelection()
        'ddlTower.ClearSelection()
        ddlMainCategory.ClearSelection()
        ddlSubCategory.ClearSelection()
        ddlChildCategory.ClearSelection()
        ddlAssetLoaction.ClearSelection()
        txtMobile.Text = ""
        ddlRepeatCalls.ClearSelection()
        ddlImpact.ClearSelection()
        ddlUrgency.ClearSelection()
        txtProbDesc.Text = ""
    End Sub

    Public Function validateempid(empid As String) As String
        Dim validatecode As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_AUR_ID")
        sp.Command.AddParameter("@AUR_ID", empid, DbType.String)
        validatecode = sp.ExecuteScalar
        Return validatecode
    End Function

    Public Sub GetUserData(aur_id As String)
        Dim validatecode As String = validateempid(txtEmpId.Text)
        If validatecode = "0" Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HRDGET_USER_DETAILS")
            sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtName.Text = ds.Tables(0).Rows(0).Item("NAME")
                txtDepartment.Text = ds.Tables(0).Rows(0).Item("DEPARTMENT")
                txtSpaceID.Text = ds.Tables(0).Rows(0).Item("SPACEID")
                txtMobile.Text = ds.Tables(0).Rows(0).Item("PHONE_NUBER")
                'If ds.Tables(1).Rows.Count > 0 Then
                '    txtSpaceID.Text = ds.Tables(1).Rows(0).Item("SSA_SPC_ID")
                'Else
                '    txtSpaceID.Text = "--"
                'End If
            Else
                lblMsg.Text = "Department Code not exist..."
                txtName.Text = ""
                ClearAll()
            End If
        Else
            lblMsg.Text = "Employee Id not existed...Please enter valid empoyee Id"
            txtName.Text = ""
            ClearAll()
        End If
    End Sub

    Public Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindMainCategory()
        ddlMainCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE")
    End Sub

    Public Sub BindSubCategory()
        ddlSubCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub

    Public Sub BindAssetLocations()
        ddlAssetLoaction.Items.Clear()
        ObjSubsonic.Binddropdown(ddlAssetLoaction, "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS", "HAL_LOC_NAME", "HAL_LOC_CODE")
    End Sub

    Public Sub BindRepeatCalls()
        ddlRepeatCalls.Items.Clear()
        ObjSubsonic.Binddropdown(ddlRepeatCalls, "HDM_GET_ALL_ACTIVE_REPEATCALLS", "RPT_NAME", "RPT_CODE")
    End Sub

    Public Sub BindImpact()
        ddlImpact.Items.Clear()
        ObjSubsonic.Binddropdown(ddlImpact, "HDM_BIND_IMPACT", "IMP_NAME", "IMP_CODE")
    End Sub

    Public Sub BindUrgency()
        ddlUrgency.Items.Clear()
        ObjSubsonic.Binddropdown(ddlUrgency, "HDM_BIND_URGENCY", "UGC_NAME", "UGC_CODE")
    End Sub

    Public Sub BindSubCategory_By_MainCategory(Main_Category As String)
        ddlSubCategory.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = Main_Category
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_SUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
    End Sub

    Public Sub BindChildCategory_By_SubCategory(Main_Category As String, Sub_Category As String)
        ddlChildCategory.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = Main_Category
        param(1) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = Sub_Category
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
    End Sub

    Private Sub RaiseRequest(ByVal MaintenanceReq As String, ByVal STATUS As Integer)

        Dim projectPath As String = Server.MapPath("~/")
        Dim folderName As String = Path.Combine(projectPath, "UploadFiles\" & Session("TENANT"))
        If Not Directory.Exists(folderName) Then
            System.IO.Directory.CreateDirectory(folderName)
        End If

        Dim RepeatCall As String
        Dim Impact As String
        Dim Urgency As String
        Dim REQ_TYPE As Integer
        Dim CALL_LOG_BY As String

        If ddlRepeatCalls.SelectedIndex > 0 Then
            RepeatCall = ddlRepeatCalls.SelectedItem.Value
        Else
            RepeatCall = ""
        End If
        If ddlImpact.SelectedIndex > 0 Then
            Impact = ddlImpact.SelectedItem.Value
        Else
            Impact = ""
        End If
        If ddlUrgency.SelectedIndex > 0 Then
            Urgency = ddlUrgency.SelectedItem.Value
        Else
            Urgency = ""
        End If
        If fu1.PostedFiles IsNot Nothing Then
            Dim selectedfiles = Request.Form("selectedfiles")
            If selectedfiles <> Nothing Then
                Dim selectedfilesarray = selectedfiles.Split(",")
                For fucount As Integer = 0 To selectedfilesarray.Length - 1
                    'Dim intsixe As Long = fu1.PostedFiles(fucount).ContentLength
                    For Each File In fu1.PostedFiles
                        If selectedfilesarray(fucount).Equals(File.FileName) Then
                            Dim intsize As Long = CInt(File.ContentLength)
                            Dim strFileName As String
                            Dim strFileExt As String
                            If intsize <= 20971520 Then
                                Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmmss")
                                strFileName = System.IO.Path.GetFileName(File.FileName)
                                strFileExt = System.IO.Path.GetExtension(File.FileName)
                                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Session("TENANT") & "\" & Upload_Time & "_" & strFileName '& "." & strFileExt
                                File.SaveAs(filePath)

                                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_INSERT_UPLOADED_FILES")
                                sp2.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
                                sp2.Command.AddParameter("@STATUS_ID", STATUS, DbType.Int32)
                                sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String)
                                sp2.Command.AddParameter("@UPLOAD_PATH", Upload_Time + "_" + strFileName, DbType.String)
                                sp2.Command.AddParameter("@REMARKS", txtProbDesc.Text, DbType.String)
                                sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
                                sp2.ExecuteScalar()
                            End If
                        End If
                    Next
                Next
            End If
        Else
            lblMsg.Text = "Select Uploadable file by clicking the Browse button"
        End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_RAISE_HELP_DESK_REQUEST")
        sp.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
        sp.Command.AddParameter("@STATUS", STATUS, DbType.Int32)
        sp.Command.AddParameter("@LOC_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_ID", txtSpaceID.Text, DbType.String)
        sp.Command.AddParameter("@MNC_CODE", ddlMainCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SUB_CAT", ddlSubCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CHILD_CAT", ddlChildCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_LOC", ddlAssetLoaction.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REPEATCALL", RepeatCall, DbType.String)
        sp.Command.AddParameter("@MOBILE", txtMobile.Text, DbType.String)
        sp.Command.AddParameter("@PROB_DESC", txtProbDesc.Text, DbType.String)
        sp.Command.AddParameter("@IMPACT", Impact, DbType.String)
        sp.Command.AddParameter("@URGENCY", Urgency, DbType.String)
        If rbActions.Checked = True Then
            REQ_TYPE = 0
            CALL_LOG_BY = Session("UID")
        Else
            REQ_TYPE = 1
            CALL_LOG_BY = txtEmpId.Text
        End If
        sp.Command.AddParameter("@SER_REQ_TYPE", REQ_TYPE, DbType.Int32)
        sp.Command.AddParameter("@SER_CALL_LOG_BY", CALL_LOG_BY, DbType.String)
        sp.Command.AddParameter("@RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@EMP_NAME", txtName.Text, DbType.String)
        sp.ExecuteScalar()
        lblMsg.Text = "Service Requisition (" + MaintenanceReq + ") raised successfully."
    End Sub

    Function GetTodaysReqCount() As String
        Dim cnt As String
        cnt = 0
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TODAYS_REQUEST_ID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ds = sp.GetDataSet()
        cnt = CInt(ds.Tables(0).Rows(0).Item("cnt"))
        Return cnt
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack() Then
            lnkShowEscaltion.Visible = False
            BindLocations()
            'BindTowers()
          
            BindAssetLocations()
            BindRepeatCalls()
            BindImpact()
            BindUrgency()
            ddlImpact.SelectedIndex = 1
            ddlUrgency.SelectedIndex = 1
            ddlAssetLoaction.SelectedIndex = 1
            ddlRepeatCalls.SelectedIndex = 1
            If rbActions.Checked = True Then
                txtEmpId.Text = Session("Uid")
                txtEmpId.Enabled = False
                GetUserData(txtEmpId.Text)
                ddlImpact.SelectedIndex = 1
                ddlUrgency.SelectedIndex = 1
                ddlRepeatCalls.SelectedIndex = 1
            Else
                txtEmpId.Text = ""
                txtEmpId.Enabled = True
                ddlLocation.ClearSelection()

            End If
        End If
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            txtEmpId.Text = Session("Uid")

            GetUserData(txtEmpId.Text)
            txtEmpId.Enabled = False
            lblMsg.Text = ""
            lnkShowEscaltion.Visible = False
        Else
            'txtEmpId.Text = Session("Uid")

            lnkShowEscaltion.Visible = False
            ClearFileds_Rbt()
        End If
        ' BindLocations()
        'BindMainCategory()
        'BindSubCategory()
        'BindChildCategory()
        'BindAssetLocations()
        'BindRepeatCalls()
        'BindImpact()
        'BindUrgency()
        ddlImpact.SelectedIndex = 1
        ddlUrgency.SelectedIndex = 1
        ddlRepeatCalls.SelectedIndex = 1
    End Sub

    Protected Sub txtEmpId_TextChanged(sender As Object, e As EventArgs) Handles txtEmpId.TextChanged
        GetUserData(txtEmpId.Text)
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            lblMsg.Text = ""
            If ddlLocation.SelectedIndex <> 0 Then
                BindMainCategory()
                ddlMainCategory.ClearSelection()
                ddlSubCategory.ClearSelection()
                ddlChildCategory.ClearSelection()
                If ddlMainCategory.SelectedIndex <> 0 And ddlSubCategory.SelectedIndex <> 0 And ddlChildCategory.SelectedIndex <> 0 Then
                    lnkShowEscaltion.Visible = True
                Else
                    lnkShowEscaltion.Visible = False
                End If
            Else
                lnkShowEscaltion.Visible = False
                ddlMainCategory.ClearSelection()
                ddlSubCategory.ClearSelection()
                ddlChildCategory.ClearSelection()
            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlRepeatCalls_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRepeatCalls.SelectedIndexChanged
        Try
            If ddlRepeatCalls.SelectedValue = "Y" Then
                Dim ds As New DataSet
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LASTSERVED_REQID")
                sp.Command.AddParameter("@LOC", ddlLocation.SelectedValue, DbType.String)
                sp.Command.AddParameter("@MNCCAT", ddlMainCategory.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SUBCAT", ddlSubCategory.SelectedValue, DbType.String)
                sp.Command.AddParameter("@CHILDCAT", ddlChildCategory.SelectedValue, DbType.String)
                sp.Command.AddParameter("@AURID", Session("uid"), DbType.String)
                ds = sp.GetDataSet()

                If (ds.Tables(0).Rows.Count > 0) Then
                    hdnReqid.Value = Convert.ToString(ds.Tables(0)(0)(0))
                    ancReqid.Visible = True
                    btnDraft.Visible = False
                    btnSubmit.Text = "Reopen and Submit"
                Else
                    ancReqid.Visible = False
                    btnDraft.Visible = True
                    btnSubmit.Text = "Submit Request"
                End If

            else
            ancReqid.Visible = False
            btnDraft.Visible = True
            btnSubmit.Text = "Submit Request"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlMainCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMainCategory.SelectedIndexChanged
        Try
            ddlRepeatCalls.ClearSelection()
            ddlRepeatCalls.SelectedIndex = 1
            hdnReqid.Value = ""
            lblMsg.Text = ""
            lnkShowEscaltion.Visible = False
            If ddlMainCategory.SelectedIndex <> 0 Then
                ddlSubCategory.Items.Clear()
                BindSubCategory_By_MainCategory(ddlMainCategory.SelectedItem.Value)
            Else
                ddlSubCategory.Items.Clear()
                ddlSubCategory.Items.Insert(0, "--Select--")
                ddlChildCategory.Items.Clear()
                ddlChildCategory.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlSubCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubCategory.SelectedIndexChanged
        Try
            ddlRepeatCalls.ClearSelection()
            ddlRepeatCalls.SelectedIndex = 1
            hdnReqid.Value = ""
            lblMsg.Text = ""
            lnkShowEscaltion.Visible = False
            If ddlMainCategory.SelectedIndex <> 0 And ddlSubCategory.SelectedIndex <> 0 Then
                ddlChildCategory.Items.Clear()
                BindChildCategory_By_SubCategory(ddlMainCategory.SelectedItem.Value, ddlSubCategory.SelectedItem.Value)
            Else
                ddlChildCategory.Items.Clear()
                ddlChildCategory.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlChildCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChildCategory.SelectedIndexChanged
        ddlRepeatCalls.ClearSelection()
        ddlRepeatCalls.SelectedIndex = 1
        hdnReqid.Value = ""
        lblMsg.Text = ""
        If ddlLocation.SelectedIndex > 0 And ddlMainCategory.SelectedIndex > 0 And ddlSubCategory.SelectedIndex > 0 And ddlChildCategory.SelectedIndex > 0 Then
            lnkShowEscaltion.Visible = True

        Else
            lnkShowEscaltion.Visible = False
        End If

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_SERVICE_INCHARGE")
        sp1.Command.AddParameter("@LOC", ddlLocation.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@MAIN_CATEGORY", ddlMainCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SUB_CATEGORY", ddlSubCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@CHILD_CATEGORY", ddlChildCategory.SelectedItem.Value, DbType.String)
        Dim flag1 As Integer = sp1.ExecuteScalar()
        Dim Ser_Inch_errmsg As String = String.Empty
        If flag1 = 0 Then
            lblMsg.Text = "Service Incharges are Not mapped to the selected service."
            btnDraft.Visible = False
            btnSubmit.Visible = False
        Else
            btnDraft.Visible = True
            btnSubmit.Visible = True

            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_ESCALTIONS")
            sp2.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@MAIN_CATEGORY", ddlMainCategory.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@SUB_CATEGORY", ddlSubCategory.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@CHILD_CATEGORY", ddlChildCategory.SelectedItem.Value, DbType.String)
            Dim flag2 As Integer = sp2.ExecuteScalar()
            If flag2 = 0 Then
                lblMsg.Text = Ser_Inch_errmsg + "<br/> SLA was not Defined to the selected service."
                btnDraft.Visible = False
                btnSubmit.Visible = False
                Exit Sub
            Else
                btnDraft.Visible = True
                btnSubmit.Visible = True
            End If
        End If
    End Sub

    Private Sub RaiseReopenRequest(Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_MODIFY_REQ_REOPEN_TICKET")
        sp.Command.AddParameter("@REQID", hdnReqid.Value, DbType.String)
        sp.Command.AddParameter("@AURID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@SER_CALL_TYPE", ddlRepeatCalls.SelectedValue, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim Main As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
            MaintenanceReq = Main + "/" + GetTodaysReqCount().ToString()
            If hdnReqid.Value = "" Then
                RaiseRequest(MaintenanceReq, 1)
            Else
                RaiseReopenRequest(hdnReqid.Value) 'reopen
                lblMsg.Text = "Service Requisition (" + hdnReqid.Value + ") reopened successfully."
            End If
            ClearAll()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try

    End Sub

    Protected Sub btnDraft_Click(sender As Object, e As EventArgs) Handles btnDraft.Click
        Try
            Dim Main As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
            MaintenanceReq = Main + "/" + GetTodaysReqCount().ToString()
            RaiseRequest(MaintenanceReq, 13)
            ClearAll()
            lblMsg.Text = "Request Saved Successfully"
            'Response.Redirect("frmhelpthanks.aspx?id=10&Reqid=" & MaintenanceReq)
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()> _
    Public Shared Function SearchCustomers(ByVal prefixText As String) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
         .ConnectionStrings("CSAmantraFAM").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandType = Data.CommandType.StoredProcedure
        cmd.CommandText = HttpContext.Current.Session("TENANT") & "." & "AUTOCOMPLETE_SEARCH"
        'cmd.CommandText = "getsearchEmpName"
        cmd.Parameters.AddWithValue("@TERM", prefixText)
        cmd.Connection = conn
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("AUR_ID").ToString)
        End While

        conn.Close()
        Return customers
    End Function

    <System.Web.Script.Services.ScriptMethod(), _
   System.Web.Services.WebMethod()> _
    Public Shared Function GetUserDataClient(ByVal aur_id As String) As Object

        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "HRDGET_USER_DETAILS")
        sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            Return New With {.Name = ds.Tables(0).Rows(0).Item("NAME"), .DEPARTMENT = ds.Tables(0).Rows(0).Item("DEPARTMENT"), .SPACEID = ds.Tables(0).Rows(0).Item("SPACEID"), .PHONE_NUBER = ds.Tables(0).Rows(0).Item("PHONE_NUBER")}
        End If
        Return Nothing
    End Function
End Class


