﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmHDMRaiseRequest.aspx.vb" Inherits="HDM_HDM_Webfiles_frmHDMRaiseRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../BootStrapCSS/datepicker.css">
    <link href="../../BootStrapCSS/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/amantra.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/form-style.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <script type="text/javascript">
        function checkDate(sender, args) {
            var toDate = new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if (sender._selectedDate < toDate) {
                document.getElementById("<%= txtconvdate.ClientID %>").value = "";
                alert("You can't select day earlier than today!");


                sender._selectedDate = toDate;
                //set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Raise Request  </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="col-md-12 control-label"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:RadioButtonList ID="rbActions" runat="server" AutoPostBack="True"
                                            Font-Bold="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbActions_SelectedIndexChanged">
                                            <asp:ListItem Value="Add" Selected="true">Self</asp:ListItem>
                                            <asp:ListItem Value="Modify">On behalf of</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Employee Id <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtEmpId"
                                            Display="none" ErrorMessage="Please enter Employee Id!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtEmpId"
                                            ErrorMessage="Please enter Employee Id in numbers" Display="None" ValidationExpression="^[0-9 ]+"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter code in numbers')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control" OnTextChanged="txtEmpId_TextChanged" AutoPostBack="True"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Location<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                            ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<tr>
                                    <td align="left" width="50%" style="height: 26px">Service Raised For <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlType"
                                            Display="None" ErrorMessage="Select Location " ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:DropDownList ID="ddlType" runat="server" Width="97%" CssClass="clsComboBox"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>--%>
                        <%--  <tr>
                                    <td align="center" colspan="2">--%>

                        <%-- <tr id="loc" runat="server">
                                                <td style="width: 50%; height: 26px" align="left">Select Building<font class="clsNote"> *</font>
                                                    <asp:RequiredFieldValidator ID="rfvloc" runat="server" ValidationGroup="Val1" ControlToValidate="ddlloc"
                                                        Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                                <td style="width: 50%; height: 26px" align="left">
                                                    <asp:DropDownList ID="ddlloc" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                        Width="99%" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%>
                        <%-- <tr id="Twr" runat="server">
                                                <td style="width: 50%; height: 26px" align="left">Select Tower<font class="clsNote"> *</font>
                                                    <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ValidationGroup="Val1" ControlToValidate="ddltwr"
                                                        Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                                <td style="width: 50%; height: 26px" align="left">
                                                    <asp:DropDownList ID="ddltwr" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                        Width="99%" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%>
                        <%-- <tr id="flr" runat="Server">
                                                <td align="left" style="height: 26px; width: 50%">Floor<font class="clsNote"> *</font>
                                                    <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlfloor"
                                                        Display="NONE" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--"
                                                        Enabled="true"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:DropDownList ID="ddlfloor" runat="server" TabIndex="9" CssClass="clsComboBox"
                                                        AutoPostBack="true" Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%>
                        <%-- <tr id="wng" runat="server">
                                                <td align="left" style="height: 26px; width: 50%">Wing<font class="clsNote"> *</font>
                                                    <asp:RequiredFieldValidator ID="rfvwing" runat="server" ControlToValidate="ddlwing"
                                                        Display="NONE" ErrorMessage="Please Select Wing" ValidationGroup="Val1" InitialValue="--Select--"
                                                        Enabled="true"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:DropDownList ID="ddlwing" runat="server" TabIndex="10" CssClass="clsComboBox"
                                                        AutoPostBack="true" Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%>
                        <%-- <tr runat="Server" id="room">
                                                <td style="width: 50%; height: 26px" align="left">Location
                                            <font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Val1"
                                                        ControlToValidate="ddlSpace" Display="NONE" ErrorMessage="Please Select SubLocation"
                                                        Enabled="true" InitialValue="0"></asp:RequiredFieldValidator></td>
                                                <td style="width: 50%; height: 26px" align="left">
                                                    <asp:DropDownList ID="ddlSpace" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                        Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%>

                        <%--   </td>
                                </tr>--%>
                        <%--    <tr id="tabcomm" runat="server">
                                    <td colspan="4" style="height: 252px">
                                        <table id="common" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">--%>
                        <tr>
                            <td width="50%">
                                <p>
                                    <asp:Label ID="lblAssetBrand" runat="server">Select Service Category <font class="clsNote">*</font></asp:Label>
                                    <asp:Label ID="lblTemp" runat="server" Visible="false"></asp:Label>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlServiceCat"
                                        ErrorMessage="Please Select Service Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                </p>
                            </td>
                            <td width="50%">
                                <asp:DropDownList ID="ddlServiceCat" runat="server" CssClass="clsComboBox" Width="100%"
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td width="50%">
                                <p>
                                    <asp:Label ID="Label2" runat="server">Select Service Type <font class="clsNote">*</font></asp:Label>
                                    <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlSerType"
                                        ErrorMessage="Please Select Service Type" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                </p>
                            </td>
                            <td width="50%">
                                <asp:DropDownList ID="ddlSerType" runat="server" CssClass="clsComboBox" Width="100%"
                                    AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">Problem Description<font class="clsNote"> *</font>
                                <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtProbDesc"
                                    ValidationGroup="Val1" Display="NONE" ErrorMessage="Problem Description Required"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:TextBox ID="txtProbDesc" runat="server" Width="97%" Rows="3" TabIndex="15" TextMode="MultiLine"
                                    MaxLength="10000"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">Convenient date<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvconvdate" runat="server" ControlToValidate="txtconvdate"
                                    Display="NONE" ErrorMessage="Please Select Convenient date" ValidationGroup="Val1"
                                    Enabled="true"></asp:RequiredFieldValidator></td>
                            <td align="left" style="height: 26px; width: 50%; z-index: 900; position: inherit;">
                                <asp:TextBox ID="txtconvdate" runat="server" CssClass="form-control" Width="40%"></asp:TextBox>
                                <asp:Button ID="btnreload" CssClass="button" runat="server" Text="Reload" />
                                <%-- <asp:Image ID="Image1" runat="server" Height="21px" ImageUrl="~/images/calen1.gif.PNG" />--%>
                                <%--<cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                                TargetControlID="txtconvdate" OnClientDateSelectionChanged="checkDate" PopupButtonID="txtconvdate">
                                            </cc1:CalendarExtender>--%>
                                <%-- <asp:LinkButton ID="LinkButton2" runat="server">Refresh convinient time</asp:LinkButton>--%>
                            </td>
                        </tr>
                        <tr id="tr1" runat="server">
                            <td align="left" style="height: 26px; width: 50%">Convenient time
                                           <%--<font class="clsNote">
                                                *</font>
                                            <asp:RequiredFieldValidator ID="cfvHr" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select Convenient time!"
                                                Display="None" ControlToValidate="cboHr" InitialValue="-HH--"></asp:RequiredFieldValidator>
                                           --%>
                            </td>
                            <td align="left" style="height: 26px; width: 50%;">
                                <asp:DropDownList ID="cboHr" runat="server" Width="48%" CssClass="clsComboBox">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <%--<td align="left" style="height: 26px; width: 50%">An email will be sent to EmailID.If you wish to receive SMS as well,Please Click
                                            Yes<font class="clsNote">*</font>
                                                </td>--%>
                            <%-- <td align="left" style="height: 26px; width: 50%">
                                                    <asp:RadioButtonList AutoPostBack="True" OnSelectedIndexChanged="radlstactions_SelectedIndexChanged"
                                                        ID="radlstactions" runat="server" CssClass="clsRadioButton">
                                                        <asp:ListItem Value="0">Yes</asp:ListItem>
                                                        <asp:ListItem Value="1" Selected="True">No</asp:ListItem>
                                                    </asp:RadioButtonList>--%>
                            <%--    <asp:CheckBoxList ID="chklist" runat="server" TabIndex="16" RepeatDirection="Horizontal"
                                                AutoPostBack="True" OnSelectedIndexChanged="chklist_SelectedIndexChanged">
                                                <asp:ListItem Value="SMS">SMS</asp:ListItem>
                                              <asp:ListItem Value="Email">Email</asp:ListItem>
                                          </asp:CheckBoxList>--%>
                                                </td>
                        </tr>
                        <%-- <tr runat="server" id="Mobile">
                                                <td align="left" style="height: 26px; width: 50%">Enter Mobile Number<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvSMSMobile" runat="server" ControlToValidate="txtSMSMobile"
                                                        ValidationGroup="Val1" Display="NONE" ErrorMessage="Enter Mobile Number to send SMS"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rgvmobile" runat="server" ErrorMessage="Please Enter Numerics Only"
                                                        Display="None" ControlToValidate="txtSMSMobile" ValidationGroup="Val1" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:TextBox ID="txtSMSMobile" runat="server" CssClass="clsTextField" MaxLength="10"
                                                        Width="97%"></asp:TextBox>
                                                </td>
                                            </tr>--%>
                        <tr runat="server" id="Email" visible="false">
                            <td align="left" style="height: 26px; width: 50%">Enter Email ID<font class="clsNote">*</font>
                                <%--<asp:RequiredFieldValidator ID="rfvSMSEmail" runat="server" ControlToValidate="txtSMSEmail"
                                                ValidationGroup="Val1" Display="NONE" ErrorMessage="Enter Email id "></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSMSEmail"
                                    Display="None" ErrorMessage="Please Ente Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ValidationGroup="Val1"></asp:RegularExpressionValidator></td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:TextBox ID="txtSMSEmail" runat="server" TextMode="MultiLine" Rows="3" MaxLength="1000"
                                    Width="97%"></asp:TextBox>
                            </td>
                        </tr>
                        <%--      </table>
                                    </td>
                                </tr>--%>
                        <tr runat="server" id="btn">
                            <td colspan="6" align="center">
                                <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="Val1"
                                    CausesValidation="true" TabIndex="17" />&nbsp;

                               
                            </td>
                        </tr>
                        </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
                        </tr>
                    <tr>
                        <td>
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td background="../../images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td>
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                    </form>
                </div>
            </div>
        </div>


    </div>
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/bootstrap.min.js"></script>
    <script src='../../BootStrapCSS/Scripts/bootstrap-datepicker.js'></script>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

</body>
</html>


