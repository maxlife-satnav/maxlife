﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddServiceCategory.aspx.vb" Inherits="HDM_HDM_Webfiles_Masters_frmAddServiceCategory" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Service Category Master</legend>
                    </fieldset>

                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Upload Document  (Only Excel )<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                                            ControlToValidate="fpBrowseDoc" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                                            ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                                            ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                                        </asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <div class="btn btn-default">
                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5  control-label">
                                            <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" />
                                        </div>
                                        <div class="col-md-7">
                                            <asp:HyperLink ID="hyp" runat="server" Text=" Click here to view the template" NavigateUrl="~/Masters/Mas_Webfiles/ServiceCategory.xls"></asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-md-2 btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                        ToolTip="Please Select Add to add new Service Category and Select Modify to modify the existing Service Category" />
                                    Add
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                        ToolTip="Please Select Add to add new Service Category and Select Modify to modify the existing Service Category" />
                                    Modify
                                </label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trLName" runat="server">
                                        <asp:Label ID="lblSerCatSelect" runat="server" class="col-md-5 control-label" Visible="False">Select Service Category<span style="color: red;">*</span></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlSerCat"
                                            ErrorMessage="Please Select Service Category" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlSerCat" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Service Category"
                                                AutoPostBack="True" Visible="False">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="Label1" runat="server" class="col-md-5 control-label">Select Location<span style="color: red;">*</span></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                            ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Location"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service Category Code <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtSerCatCode"
                                            Display="none" ErrorMessage="Please Enter Service Category Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtSerCatCode" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service Category Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSerCatName"
                                            Display="none" ErrorMessage="Please Enter Service Category Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtSerCatName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                            Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Status">
                                                <asp:ListItem>--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                        <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                            ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters  " ValidationGroup="Val1"></asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please Enter Remarks  " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                                    MaxLength="500" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/Workspace/SMS_WEBfiles/helpmasters.aspx" CausesValidation="False" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvLocation" runat="server" AllowPaging="True" AllowSorting="False"
                                    RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                    PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Service Category Found" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>

                                                <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("SER_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLcmName" runat="server" CssClass="lblLcmName" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("SER_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Category Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("SER_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("SER_STA_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
