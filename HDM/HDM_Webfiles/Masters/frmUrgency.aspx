﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmUrgency.aspx.vb" Inherits="Masters_Mas_Webfiles_frmUrgency" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
    <style>
        .grid-align {text-align: center;}
        a:hover { cursor: pointer; }
    </style>
</head>
<body data-ng-controller="UrgencyController">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Urgency Master</legend>
                    </fieldset>
                    <form role="form" id="form1" class="form-horizontal well" name="frm" data-valid-submit="Save()" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Code.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Code<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <input type="text" id="txtcode" name="Code" data-ng-pattern="codepattern" data-ng-model="Urgency.Code" maxlength="15" class="form-control" required="required" data-ng-readonly="ActionStatus==1" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.Code.$invalid" style="color: red">Please enter valid code </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Name.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Name<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <input id="txtCName" type="text" name="Name" data-ng-model="Urgency.Name" maxlength="50" class="form-control" required="required" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.Name.$invalid" style="color: red">Please enter valid name</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <textarea id="txtremarks" data-ng-model="Urgency.Remarks" class="form-control" maxlength="500"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.Status.$invalid}">
                                        <label class="col-md-4 control-label">Status<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div>
                                                <select id="ddlsta" name="Status" data-ng-model="Urgency.Status" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.Status.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()">
                                    <%--<input type="button" value="Back" class='btn btn-primary' onclick="window.location = 'frmMASMasters.aspx'" />--%>
                                     <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="height: 320px;">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 329px;width:100%"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/aggrid/ag-grid.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
        app.directive('validSubmit', ['$parse', function ($parse) {
            return {
                require: 'form',
                link: function (scope, element, iAttrs, form) {
                    form.$submitted = false;
                    var fn = $parse(iAttrs.validSubmit);
                    element.on('submit', function (event) {
                        scope.$apply(function () {
                            form.$submitted = true;
                            if (form.$valid) {
                                fn(scope, { $event: event });
                                form.$submitted = false;
                            }
                        });
                    });
                }
            };
        }
        ])
        app.service("UrgencyService", function ($http, $q) {
            var deferred = $q.defer();
            this.getUrgency = function () {
                return $http.get('../../../api/Urgency')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.CreateUrgency = function (urgency) {
                deferred = $q.defer();
                return $http.post('../../../api/Urgency/Create', urgency)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.UpdateUrgency = function (urgency) {
                deferred = $q.defer();
                return $http.post('../../../api/Urgency/UpdateUrgencyMaster', urgency)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.GetGriddata = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Urgency/GetGridData')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });

        app.controller('UrgencyController', function ($scope, $q, UrgencyService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.Urgency = {};
            $scope.urgencydata = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    UrgencyService.UpdateUrgency($scope.Urgency).then(function (urgency) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";
                        $scope.IsInEdit = false;
                        $scope.EraseData();
                        //setTimeout(function () {
                        //    $scope.$apply(function () {
                        //        $scope.ShowMessage = false;
                        //    });
                        //}, 700);
                        $scope.LoadData();
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.Urgency.Status = "1";
                    UrgencyService.CreateUrgency($scope.Urgency).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        angular.copy($scope.Urgency, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        //setTimeout(function () {
                        //    $scope.$apply(function () {
                        //        $scope.ShowMessage = false;
                        //    });
                        //}, 700);
                        $scope.Urgency = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }

            var columnDefs = [
                { headerName: "Urgency Code", field: "Code", width: 290, cellClass: 'grid-align' },
                { headerName: "Urgency Name", field: "Name", width: 380, cellClass: 'grid-align' },
                { headerName: "Status", template: "{{ShowStatus(data.Status)}}", width: 270, cellClass: 'grid-align' },
                { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];

            $scope.LoadData = function () {
                UrgencyService.GetGriddata().then(function (data) {
                    $scope.gridata = data;
                    $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });
            }

            $scope.onPageSizeChanged = function () {
                $scope.createNewDatasource();
            };
            $scope.pageSize = '10';
            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        console.log('asking for ' + params.startRow + ' to ' + params.endRow);
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }
            $scope.gridOptions = {
                columnDefs: columnDefs,
                enableCellSelection: false,
                rowData: null,
                enableSorting: true,
                angularCompileRows: true,
                enableFilter: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };

            $scope.LoadData();

            $scope.EditData = function (data) {
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                angular.copy(data, $scope.Urgency);
            }

            $scope.EraseData = function () {
                $scope.Urgency = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

        });

    </script>
</body>
</html>

