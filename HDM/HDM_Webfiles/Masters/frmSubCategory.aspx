﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSubCategory.aspx.vb" Inherits="HDM_HDM_Webfiles_Masters_frmSubCategory" %>
<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="SubCategoryController">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>HD Sub Category</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frm" class="form-horizontal well" data-valid-submit="Save()" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.SUBC_CODE.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Sub Category Code<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <input id="txtcode" name="SUBC_CODE" type="text" data-ng-model="SubCategory.SUBC_CODE" data-ng-pattern="codepattern" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.SUBC_CODE.$invalid" style="color: red">Please enter valid code </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.SUBC_NAME.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Sub Category Name<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <input id="txtCName" name="SUBC_NAME" type="text" data-ng-model="SubCategory.SUBC_NAME" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.SUBC_NAME.$invalid" style="color: red">Please enter valid name</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.MNC_NAME.$invalid}">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Main Category<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div data-ng-class="{'has-error': frm.$submitted && frm.MNC_NAME.$invalid}">
                                                <select id="ddlMain" name="MNC_NAME" class="form-control" data-ng-model="SubCategory.MNC_CODE" required>
                                                    <option id="ddlsub" value=''>--Select--</option>
                                                    <option data-ng-repeat="maincategory in maincategorylist" value="{{maincategory.MNC_CODE}}">{{maincategory.MNC_NAME}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.MNC_NAME.$invalid" style="color: red">Please select main category </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <textarea id="txtremarks" data-ng-model="SubCategory.SUBC_REM" class="form-control" maxlength="500"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.SUBC_STA_ID.$invalid}">
                                        <label class="col-md-4 control-label">Status<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div>
                                                <select id="ddlsta" name="SUBC_STA_ID" data-ng-model="SubCategory.SUBC_STA_ID" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.SUBC_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                                    <%--<input type="button" value="Back" class='btn btn-primary' onclick="window.location = 'frmMASMasters.aspx'" />--%>
                                     <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="height: 320px">
                                 <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 329px;width:100%"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../../../SMViews/Utility.js"></script>
    <script>
       
        app.service("SubCategoryService", function ($http, $q) {
            var deferred = $q.defer();
            this.getSubCategory = function () {
                return $http.get('../../../api/SubCategory')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.CreateSubCategory = function (category) {
                deferred = $q.defer();
                return $http.post('../../../api/SubCategory/Create', category)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.UpdateSubCategory = function (category) {
                deferred = $q.defer();
                return $http.post('../../../api/SubCategory/UpdateSubcatData', category)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.GetMainCategory = function () {
                deferred = $q.defer();
                return $http.get('../../../api/SubCategory/GetMaincategory')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            }

            this.GetGriddata = function () {
                deferred = $q.defer();
                return $http.get('../../../api/SubCategory/GetGridData')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });
        app.controller('SubCategoryController', function ($scope, $q, SubCategoryService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.SubCategory = {};
            $scope.categorydata = [];
            $scope.maincategorylist = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;

            SubCategoryService.GetMainCategory().then(function (data) {
                $scope.maincategorylist = data;
                $scope.LoadData();
            }, function (error) {
                console.log(error);
            });

            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    SubCategoryService.UpdateSubCategory($scope.SubCategory).then(function (category) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";
                        var savedobj = {};                        
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.IsInEdit = false;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                            });
                        }, 700);
                        $scope.LoadData();
                        $scope.SubCategory = {};
                        $scope.ActionStatus = 0;
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.SubCategory.SUBC_STA_ID = "1";
                    SubCategoryService.CreateSubCategory($scope.SubCategory).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};                        
                        $scope.SubCategory.MNC_NAME = $scope.GetMainName($scope.SubCategory.MNC_CODE);
                        angular.copy($scope.SubCategory, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.SubCategory = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            var columnDefs = [
                { headerName: "Sub Category Code", field: "SUBC_CODE", width: 190, cellClass: 'grid-align' },
                { headerName: "Sub Category Name", field: "SUBC_NAME", width: 180, cellClass: 'grid-align' },
                { headerName: "Main Category", field: "MNC_NAME", width: 180, cellClass: 'grid-align' },
                { headerName: "Status", template: "{{ShowStatus(data.SUBC_STA_ID)}}", width: 270, cellClass: 'grid-align' },
                { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];

            $scope.LoadData = function () {                
                SubCategoryService.GetGriddata().then(function (data) {
                    $scope.gridata = data;
                    //$scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);                    
                }, function (error) {
                    console.log(error);
                });
            }

            //$scope.pageSize = '10';

            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                enableCellSelection: false,
                rowData: null,
                enableSorting: true,
                enableFilter: true,
                angularCompileRows: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit();
                }
            };
            //$scope.LoadData();

            $scope.EditData = function (data) {
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                angular.copy(data, $scope.SubCategory);
              }

            $scope.EraseData = function () {
                $scope.SubCategory = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            //get main cat name to display in grid after submit
            $scope.GetMainName = function (mncCode) {
                //var fruitId = $scope.maincategorylist;
                for (var i = 0; $scope.maincategorylist != null && i < $scope.maincategorylist.length; i += 1) {
                    var result = $scope.maincategorylist[i];
                    if (result.MNC_CODE === mncCode) {                        
                        return result.MNC_NAME;
                    }
                }
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
        });
    </script>
</body>
</html>
