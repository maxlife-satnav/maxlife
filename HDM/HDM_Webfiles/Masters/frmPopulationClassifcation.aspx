﻿<%@ Page Language="C#" AutoEventWireup="true"  %>


<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;

        }
        a:hover {
            cursor:pointer;
        }
    </style>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="PopulationClassificationController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Population Classification Master</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frmPopulationClassification" class="form-horizontal well" data-valid-submit="Save()" novalidate>
                        <div>
                 <%--           <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>                      
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Population Classification Code<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmPopulationClassification.$submitted && frmPopulationClassification.PCN_Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                    <input id="PCN_Code" type="text" name="PCN_Code" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="PopulationClassification.PCN_Code" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmPopulationClassification.$submitted && frmPopulationClassification.PCN_Code.$invalid" style="color: red">Please enter valid code </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Population Classification Name<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmPopulationClassification.$submitted && frmPopulationClassification.PCN_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                                    <input id="PCN_Name" type="text" name="PCN_Name" maxlength="50" data-ng-model="PopulationClassification.PCN_Name" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmPopulationClassification.$submitted && frmPopulationClassification.PCN_Name.$invalid" style="color: red">Please enter valid Name </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Remarks</label>
                                            <div class="col-md-8">
                                                <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                    <textarea id="Textarea1" runat="server" data-ng-model="PopulationClassification.PCN_REM" class="form-control" maxlength="500"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" >
                                        <div class="row" data-ng-show="ActionStatus==1">
                                            <label class="col-md-4 control-label" >Status<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div >
                                                    <select id="PCN_Status_Id" name="PCN_Status_Id" data-ng-model="PopulationClassification.PCN_Status_Id"  class="form-control" >                                                   
                                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                    </select>                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <input type="submit"  value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0"/>
                                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1"  />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />                         
                                         <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>   
                           <div class="row">
                            <div class="col-md-12">
                                <div data-ag-grid="gridOptions" style="height: 329px; width: 100%;" class="ag-blue"></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        //agGrid.initialiseAgGridWithAngular1(angular);
        var app = angular.module('QuickFMS', ["agGrid"]);
        //app.directive('validSubmit', ['$parse', function ($parse) {
        //    return {              
        //        require: 'form',             
        //        link: function (scope, element, iAttrs, form) {
        //            form.$submitted = false;                
        //            var fn = $parse(iAttrs.validSubmit);
        //            element.on('submit', function (event) {
        //                scope.$apply(function () {
        //                    form.$submitted = true;
        //                    if (form.$valid) {
        //                        fn(scope, { $event: event });
        //                        form.$submitted = false;
        //                    }
        //                });
        //            });
        //        }
        //    };
        //}
        //])

        app.service("PopulationClassificationService", function ($http, $q) {
            var deferred = $q.defer();
            this.getPopulationClassification = function () {
                deferred = $q.defer();
                return $http.get('../../../api/PopulationClassification')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            //SAVE
            this.savePopulationClassification = function (popclas) {
                deferred = $q.defer();
                return $http.post('../../../api/PopulationClassification/Create', popclas)
                  .then(function (response) {
                      deferred.resolve(response);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };



            //UPDATE BY ID
            this.updatePopulationClassification = function (pcn) {
                deferred = $q.defer();
                return $http.post('../../../api/PopulationClassification/UpdatePopulationClassificationData/', pcn)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //Bind Grid
            this.GetPopulationClassificationBindGrid = function () {
                deferred = $q.defer();
                return $http.get('../../../api/PopulationClassification/GetPopulationClassificationBindGrid')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });

        app.controller('PopulationClassificationController', function ($scope, $q, PopulationClassificationService,$timeout,$http) {

            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.PopulationClassification = {};
            $scope.repeatCategorylist = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;

            //to Save the data
            $scope.Save = function () {
                if ($scope.IsInEdit) {

                    PopulationClassificationService.updatePopulationClassification($scope.PopulationClassification).then(function (repeat) {
                        var updatedobj = {};
                        angular.copy($scope.PopulationClassification, updatedobj)
                        $scope.gridata.unshift(updatedobj);                 
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";
                        $scope.LoadData();
                        $scope.IsInEdit = false;
                        $scope.ClearData();

                        setTimeout(function () {
                            $scope.$apply(function () {

                                showNotification('success', 8, 'bottom-right', $scope.Success);
                                //$scope.ShowMessage = false;
                            });
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.PopulationClassification.PCN_Status_Id = "1";
                    PopulationClassificationService.savePopulationClassification($scope.PopulationClassification).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        angular.copy($scope.PopulationClassification, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);

                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.PopulationClassification = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            //for GridView
            var columnDefs = [
               { headerName: "Population Classification Code", field: "PCN_Code", width: 120, cellClass: 'grid-align' },
               { headerName: "Population Classification Name", field: "PCN_Name", width: 160, cellClass: 'grid-align' },
               { headerName: "Status", template: "{{ShowStatus(data.PCN_Status_Id)}}", width: 100, cellClass: 'grid-align' },
               { headerName: "Action", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" }
            ];

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                PopulationClassificationService.GetPopulationClassificationBindGrid().then(function (data) {
                    $scope.gridata = data;
                    $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                    progress(0, '', false);
                }, function (error) {
                    console.log(error);
                });
            }

            $scope.pageSize = '10';

            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableFilter: true,
                enableCellSelection: false,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };


            $timeout($scope.LoadData, 1000);

            $scope.EditFunction = function (data) {             
                $scope.PopulationClassification = {};
                angular.copy(data, $scope.PopulationClassification);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;

            }

            $scope.ClearData = function () {
                $scope.PopulationClassification = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
            }
            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

        });

    </script>
    <script src="../../../SMViews/Utility.js"></script>


</body>
</html>
