﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <style>
        .grid-align
        {
            text-align: center;
        }

        a:hover
        {
            cursor: pointer;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }
    </style>

</head>

<body data-ng-controller="ServiceController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Service Escalation Mapping Master</legend>
                    </fieldset>

                    <form role="form" id="form2" name="frmService" class="form-horizontal well" data-valid-submit="SaveDetails()" novalidate>
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5  control-label">
                                                <input id="btnbrowse" type="button" id="btnBrowse" class='btn btn-primary custom-button-color' value="Upload" />
                                            </div>
                                            <div class="col-md-7">
                                                <a href="">Click here to view the template</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>--%>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Country<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmService.$submitted && frmService.SEM_CNY_CODE.$invalid}">
                                                    <select id="ddlCountry" name="SEM_CNY_CODE" class="form-control" data-ng-model="SEMMaster.SEM_CNY_CODE" required data-live-search="true" data-ng-change="CountryChanged()">
                                                         <option value="">--Select-- </option>
                                                        <option data-ng-repeat="countrycategory in countrylist" value="{{countrycategory.CNY_CODE}}">{{countrycategory.CNY_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SEM_CNY_CODE.$invalid" style="color: red">Please Select Country </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">City<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmService.$submitted && frmService.SEM_CTY_CODE.$invalid}">
                                                    <select id="ddlCity" name="SEM_CTY_CODE" class="form-control" data-ng-model="SEMMaster.SEM_CTY_CODE" required data-live-search="true" data-ng-change="CityChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="citycategory in Citylist" value="{{citycategory.CTY_CODE}}">{{citycategory.CTY_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SEM_CTY_CODE.$invalid" style="color: red">Please Select City</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Location<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmService.$submitted && frmService.SEM_LOC_CODE.$invalid}">
                                                    <select id="ddlLocation" name="SEM_LOC_CODE" class="form-control" data-ng-model="SEMMaster.SEM_LOC_CODE" required data-live-search="true" data-ng-change="LocationChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="locationcategory in locationlist" value="{{locationcategory.LCM_CODE}}">{{locationcategory.LCM_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SEM_LOC_CODE.$invalid" style="color: red">Please Select Location</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Tower<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmService.$submitted && frmService.SEM_TWR_CODE.$invalid}">
                                                    <select id="ddlTower" name="SEM_TWR_CODE" class="form-control" data-ng-model="SEMMaster.SEM_TWR_CODE" required data-live-search="true" data-ng-change="TowerChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="towercategory in towerlist" value="{{towercategory.TWR_CODE}}">{{towercategory.TWR_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SEM_TWR_CODE.$invalid" style="color: red">Please Select Tower </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Main Category<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmService.$submitted && frmService.SEM_MNC_CODE.$invalid}">
                                                    <select id="ddlMain" name="SEM_MNC_CODE" class="form-control" data-ng-model="SEMMaster.SEM_MNC_CODE" required data-live-search="true" data-ng-change="MainCategoryChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="maincategory in maincategorylist" value="{{maincategory.MNC_CODE}}">{{maincategory.MNC_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SEM_MNC_CODE.$invalid" style="color: red">Please Select Main Category </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Sub Category<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmService.$submitted && frmService.SEM_SUBC_CODE.$invalid}">
                                                    <select id="ddlSub" name="SEM_SUBC_CODE" class="form-control" data-ng-model="SEMMaster.SEM_SUBC_CODE" required data-live-search="true" data-ng-change="SubCategoryChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="subcategory in SubCategorylist" value="{{subcategory.SUBC_CODE}}">{{subcategory.SUBC_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SEM_SUBC_CODE.$invalid" style="color: red">Please Select Sub Category </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Child Category<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmService.$submitted && frmService.SEM_CHC_CODE.$invalid}">
                                                    <select id="ddlChild" name="SEM_CHC_CODE" class="form-control" data-ng-model="SEMMaster.SEM_CHC_CODE" required data-live-search="true">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="childcategory in ChildCategorylist" value="{{childcategory.CHC_TYPE_CODE}}">{{childcategory.CHC_TYPE_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SEM_CHC_CODE.$invalid" style="color: red">Please Select Child Category </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="height: 250px; width: 300px;">
                            <div data-ag-grid="roleusersoptions" style="height: 100%;" class="ag-blue"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" id="btnSubmit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" id="btnmodify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input id="btnClear" type="button" class='btn btn-primary custom-button-color' value="Clear" data-ng-click="clear()" />
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div style="height: 250px">
                             <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="semdetailsOptions" style="height: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../../../SMViews/Utility.js"></script>
    <script src="../js/ServiceEscalationMapping.js"></script>

</body>
</html>



