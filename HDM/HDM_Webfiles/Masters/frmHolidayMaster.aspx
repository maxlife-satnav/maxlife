﻿<%@ Page Language="C#" AutoEventWireup="true"  %>
<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'>   
    <![endif]-->

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="HolidayController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Holiday Master</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frm" class="form-horizontal well" data-valid-submit="Save()" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <div class="row">
                                        <label class="col-md-4 control-label">Holiday Name<span style="color: red;">*</span></label>
                                        <div class="col-md-8" data-ng-class="{'has-error': frm.$submitted && frm.HOL_REASON.$invalid}">
                                            <div onmouseover="Tip('Enter Holiday Name')" onmouseout="UnTip()">
                                                <input id="txtcode" name="HOL_REASON" type="text" data-ng-model="Holiday.HOL_REASON" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" maxlength="15" class="form-control" autofocus required="" />
                                                <span class="error" data-ng-show="frm.$submitted && frm.HOL_REASON.$invalid" style="color: red">Please enter Holiday Name </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <div class="row">
                                        <label class="col-md-4 control-label">Holiday Date<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group date" id='fromdate' data-ng-class="{'has-error': frm.$submitted && frm.HOL_DATE.$invalid}">
                                                <div class="input-group-addon">
                                                    <i class="btn btn-mini dropdown-toggle" onclick="setup('fromdate')"></i>
                                                </div>
                                                <div onmouseover="Tip('Enter Holiday date')"
                                                    onmouseout="UnTip()">
                                                    <input class="form-control input-sm" type="text" id="Text2" name="HOL_DATE" data-ng-model="Holiday.HOL_DATE" required="" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                                                </div>
                                            </div>
                                            <span class="error" data-ng-show="frm.$submitted && frm.HOL_DATE.$invalid" style="color: red">Please Enter Holiday Date</span>
                                        </div>
                                    </div>
                                </div>
                            </div>                 
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <div class="row">
                                        <label class="col-md-4 control-label">City Name<span style="color: red;">*</span></label>
                                        <div class="col-md-8" data-ng-class="{'has-error': frm.$submitted && frm.HOL_CITY_CODE.$invalid}">
                                            <select id="ddlMain" name="HOL_CITY_CODE" class="form-control" data-ng-model="Holiday.HOL_CITY_CODE" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" required data-ng-readonly="ActionStatus==1">
                                                <option id="ddlsub" value=''>--Select--</option>
                                                <option data-ng-repeat="city in CityList" value="{{city.CTY_CODE}}">{{city.CTY_NAME}}</option>
                                            </select>
                                            <span class="error" data-ng-show="frm.$submitted && frm.HOL_CITY_CODE.$invalid" style="color: red">Please select City </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <textarea id="txtremarks" data-ng-model="Holiday.HOL_REM" class="form-control" maxlength="500"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.HOL_STA_ID.$invalid}">
                                        <label class="col-md-4 control-label">Status<span style="color: red;">*</span></label>
                                        <div class="col-md-8">
                                            <div>
                                                <select id="ddlsta" name="Status" data-ng-model="Holiday.HOL_STA_ID" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.HOL_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class='btn btn-primary custom-button-color' data-ng-click="EraseData()" />                           
                                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" >
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 329px;width:100%"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>        
        var app = angular.module('QuickFMS', ["agGrid"]);
 
        app.service("HolidayService", function ($http, $q) {
            var deferred = $q.defer();
            this.getHoliday = function () {
                return $http.get('../../../api/Holiday')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.CreateHoliday = function (crt) {
                deferred = $q.defer();
                return $http.post('../../../api/Holiday/Create', crt)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.UpdateHoliday = function (upd) {
                deferred = $q.defer();
                return $http.post('../../../api/Holiday/UpdateHolidays', upd)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            this.GetCitiesList = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Holiday/GetCities')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            }

            this.GetGriddata = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Holiday/GetGridData')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });
        app.controller('HolidayController', function ($scope, $q, HolidayService, $http, $timeout, $filter) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.Holiday = {};
            $scope.categorydata = [];
            $scope.CityList = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;

            HolidayService.GetCitiesList().then(function (data) {
                $scope.CityList = data;
                $scope.LoadData();
            }, function (error) {
                console.log(error);
            });

            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    console.log($scope.Holiday);
                    HolidayService.UpdateHoliday($scope.Holiday).then(function (category) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";
                        var savedobj = {};
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.IsInEdit = false;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                showNotification('success', 8, 'bottom-right', $scope.Success);                             
                            });
                        }, 700);
                        $scope.LoadData();
                        $scope.Holiday = {};
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.Holiday.HOL_STA_ID = "1";
                    HolidayService.CreateHoliday($scope.Holiday).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        $scope.Holiday.HOL_CITY_NAME = $scope.GetCityName($scope.Holiday.HOL_CITY_CODE);
                        angular.copy($scope.Holiday, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);

                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.Holiday = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                       
                        setTimeout(function () {
                            $scope.$apply(function () {

                                showNotification('error', 8, 'bottom-right', $scope.Success);
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            var columnDefs = [
                { headerName: "Holiday", field: "HOL_REASON", width: 200, cellClass: 'grid-align' },
                 { headerName: "Holiday Date", template: '<span>{{data.HOL_DATE | date:"dd MMM, yyyy"}}</span>', width: 150, cellClass: 'grid-align' },
                { headerName: "City", field: "HOL_CITY_NAME", width: 180, cellClass: 'grid-align' },
                { headerName: "Status", template: "{{ShowStatus(data.HOL_STA_ID)}}", width: 100, cellClass: 'grid-align' },
                { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 100 }];

            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                HolidayService.GetGriddata().then(function (gddata) {              
                    $scope.gridata = gddata;
                   // $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(gddata);
                    progress(0, '', false);
                }, function (error) {
                    console.log(error);
                });
            }

            //$scope.pageSize = '10';

            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                enableCellSelection: false,
                rowData: null,
                enableSorting: true,
                enableFilter: true,
                angularCompileRows: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };
            $timeout($scope.LoadData, 1000);

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

            $scope.EditData = function (data) {
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;                
                angular.copy(data, $scope.Holiday);                
                $scope.Holiday.HOL_PREVDATE = $filter('date')($scope.Holiday.HOL_DATE, 'MM/dd/yyyy');
                $scope.Holiday.HOL_DATE = $filter('date')($scope.Holiday.HOL_DATE, 'MM/dd/yyyy');
            }

            $scope.EraseData = function () {
                $scope.Holiday = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            //get city cat name to display in grid after submit
            $scope.GetCityName = function (ctyCode) {                
                for (var i = 0; $scope.CityList != null && i < $scope.CityList.length; i += 1) {
                    var result = $scope.CityList[i];
                    if (result.CTY_CODE === ctyCode) {
                        return result.CTY_NAME;
                    }
                }
            }
        });



    </script>
    <script src="../../../SMViews/Utility.js"></script>
</body>
</html>
