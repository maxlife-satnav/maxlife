﻿<%@ Page Language="VB" AutoEventWireup="false" %>
<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/FMS_UI/css/bootstrap2.css" rel="stylesheet" />
    <style>
        .grid-align
        {
            text-align: center;
        }
        a:hover
        {
            cursor: pointer;
        }
        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }
    </style>
</head>

<body data-ng-controller="SLAcontroller" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>SLA</legend>
                    </fieldset>
                    <form role="form" id="form2" name="frmSLA" class="form-horizontal well" data-valid-submit="SaveDetails()" novalidate>
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>

                            <div>&nbsp;</div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Country<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_CNY_CODE.$invalid}">
                                                    <select id="ddlCountry" name="SLA_CNY_CODE" class="form-control" data-ng-model="SLAMaster.SLA_CNY_CODE" required data-live-search="true" data-ng-change="CountryChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="countrycategory in countrylist" value="{{countrycategory.CNY_CODE}}">{{countrycategory.CNY_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_CNY_CODE.$invalid" style="color: red">Please Select Country </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">City<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_CTY_CODE.$invalid}">
                                                    <select id="ddlCity" name="SLA_CTY_CODE" class="form-control" data-ng-model="SLAMaster.SLA_CTY_CODE" required data-live-search="true" data-ng-change="CityChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="citycategory in Citylist" value="{{citycategory.CTY_CODE}}">{{citycategory.CTY_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_CTY_CODE.$invalid" style="color: red">Please Select City</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Location<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_LOC_CODE.$invalid}">
                                                    <select id="ddlLocation" name="SLA_LOC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_LOC_CODE" required data-live-search="true">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="locationcategory in locationlist" value="{{locationcategory.LCM_CODE}}">{{locationcategory.LCM_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_LOC_CODE.$invalid" style="color: red">Please Select Location</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Main Category<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_MNC_CODE.$invalid}">
                                                    <select id="ddlMain" name="SLA_MNC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_MNC_CODE" required data-live-search="true" data-ng-change="MainCategoryChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="maincategory in maincategorylist" value="{{maincategory.MNC_CODE}}">{{maincategory.MNC_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_MNC_CODE.$invalid" style="color: red">Please Select Main Category </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Sub Category<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_SUBC_CODE.$invalid}">
                                                    <select id="ddlSub" name="SLA_SUBC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_SUBC_CODE" required data-live-search="true" data-ng-change="SubCategoryChanged()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="subcategory in SubCategorylist" value="{{subcategory.SUBC_CODE}}">{{subcategory.SUBC_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_SUBC_CODE.$invalid" style="color: red">Please Select Sub Category </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Child Category<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_CHC_CODE.$invalid}">
                                                    <select id="ddlChild" name="SLA_CHC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_CHC_CODE" required data-live-search="true" data-ng-change="GetSLA()">
                                                        <option value="">--Select-- </option>
                                                        <option data-ng-repeat="childcategory in ChildCategorylist" value="{{childcategory.CHC_TYPE_CODE}}">{{childcategory.CHC_TYPE_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_CHC_CODE.$invalid" style="color: red">Please Select Child Category </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />

                        <div class="col-md-12" data-ng-show="SLADetails">
                            <div class="box">
                                <div class="box-body">
                                    <table data-ng-table="tableParams" class="table ng-table-responsive">
                                        <tr>
                                            <th>Status </th>
                                            <th>Email Escalation</th>
                                            <th data-ng-repeat="rol in Roles">{{rol.ROL_DESCRIPTION}}</th>
                                        </tr>
                                        <tr data-ng-repeat="SLA in SLADetails">
                                            <td style="vertical-align: central !important;">
                                                <label>{{SLA.Key.SLAD_DESC}}</label>
                                            </td>
                                            <td>
                                                <input type="checkbox" id="chk" data-ng-model="SLA.Key.selected" value="{{SLA.Key.selected}}" data-ng-init="SLA.Key.selected = true"/>
                                            </td>
                                            <td data-ng-repeat="SLARoles in SLA.Value" data-ng-form="innerForm">
                                                <div class="input-group margin">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                    <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.SLAD_ESC_TIME.$invalid}">
                                                        <input class="form-control input-sm" placeholder="" data-ng-model="SLARoles.Value.SLAD_ESC_TIME" data-ng-pattern="/^[0-9]{1,5}$/" name="SLAD_ESC_TIME" required="" type="text">
                                                    </div>
                                                    <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.SLAD_ESC_TIME_TYPE.$invalid}">
                                                        <select class="form-control input-sm" data-ng-model="SLARoles.Value.SLAD_ESC_TIME_TYPE" name="SLAD_ESC_TIME_TYPE" required="">
                                                            <option data-ng-repeat="type in timetype" data-ng-selected="type.value==SLARoles.Value.SLAD_ESC_TIME_TYPE" value="{{type.value}}">{{type.Name}}</option>
                                                        </select>
                                                    </div>
                                                    <span class="error" data-ng-show="innerForm.SLAD_ESC_TIME.$error.pattern">Enter Valid Number</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input id="btnClear" type="button" class="btn btn-primary custom-button-color" value="Clear" data-ng-click="clear()" />
                                    <%--<input id="btnBack" type="button" class='btn btn-primary' value="Back"  data-ng-click="goBack()" >--%>
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div style="height: 320px">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/aggrid/ag-grid.min.js"></script>

    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>

    <script src="../js/SLA.js"></script>
    <script src="../../../SMViews/Utility.js"></script>
</html>