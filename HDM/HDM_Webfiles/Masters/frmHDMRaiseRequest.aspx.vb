﻿Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration

Partial Class HDM_HDM_Webfiles_frmHDMRaiseRequest
    Inherits System.Web.UI.Page
    Dim MaintenanceReq As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            'Mobile.Visible = False
            'Email.Visible = False
            'personal.Visible = False
            'BindType()
            'BindCity()
            'BindDetails()
            'loc.Visible = False
            'Twr.Visible = False
            'flr.Visible = False
            'wng.Visible = False
            getlocation()
            Dim iLoop As Integer
            For iLoop = 0 To rbActions.Items.Count - 1S
                If rbActions.Items(iLoop).Selected = True Then
                    If rbActions.SelectedItem.Value = "Add" Then
                        txtEmpId.Text = Session("Uid")
                        txtEmpId.Enabled = False
                        getuserdata(txtEmpId.Text)
                        ddlLocation.Enabled = False
                        lblMsg.Visible = False
                    ElseIf rbActions.SelectedItem.Value = "Modify" Then
                        txtEmpId.Text = ""
                        txtEmpId.Enabled = True

                        ddlLocation.ClearSelection()
                        ddlLocation.Enabled = False
                        lblMsg.Visible = False
                    End If
                End If
            Next
            txtconvdate.Text = getoffsetdatetime(DateTime.Now).ToString("dd-MMM-yyyy")
        End If
        txtconvdate.Attributes.Add("onClick", "displayDatePicker('" + txtconvdate.ClientID + "')")
        txtconvdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    End Sub

    Public Sub getuserdata(aur_id As String)
        Dim validatecode As String = validateempid(txtEmpId.Text)
        If validatecode = "0" Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HRDGET_USER_DETAILS")
            sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlLocation.ClearSelection()
                ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("LCM_CODE")).Selected = True
                bindrequests(ds.Tables(0).Rows(0).Item("LCM_CODE"))
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Location Code not exist..."
            End If
        Else
            lblMsg.Visible = True
            lblMsg.Text = "Employee Id not existed...Please enter valid empoyee Id"
        End If
       

    End Sub
    Public Function validateempid(empid As String) As String
        Dim validatecode As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_AUR_ID")
        sp.Command.AddParameter("@AUR_ID", empid, DbType.String)
        validatecode = sp.ExecuteScalar
        Return validatecode
    End Function
    Public Sub getlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub
    Public Sub bindretypes(lcm_code As String, ser_cat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_SERTYPEBYCATLOCATION")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        sp.Command.AddParameter("@SER_CODE", ser_cat, DbType.String)
        ddlSerType.DataSource = sp.GetDataSet()
        ddlSerType.DataTextField = "SER_TYPE_NAME"
        ddlSerType.DataValueField = "SER_TYPE_CODE"
        ddlSerType.DataBind()
        ddlSerType.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.SelectedIndexChanged
        Dim iLoop As Integer
        For iLoop = 0 To rbActions.Items.Count - 1S
            If rbActions.Items(iLoop).Selected = True Then
                If rbActions.SelectedItem.Value = "Add" Then
                    txtEmpId.Text = Session("Uid")
                    txtEmpId.Enabled = False
                    getuserdata(txtEmpId.Text)
                    ddlLocation.Enabled = False
                    lblMsg.Visible = False
                ElseIf rbActions.SelectedItem.Value = "Modify" Then
                    txtEmpId.Text = ""
                    txtEmpId.Enabled = True

                    ddlLocation.ClearSelection()
                    ddlLocation.Enabled = False
                    lblMsg.Visible = False
                End If
            End If
        Next
    End Sub

 

    Protected Sub txtEmpId_TextChanged(sender As Object, e As EventArgs) Handles txtEmpId.TextChanged

        getuserdata(txtEmpId.Text)
    End Sub
    Public Sub bindrequests(lcm_code As String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        ddlServiceCat.DataSource = sp.GetDataSet()
        ddlServiceCat.DataTextField = "SER_NAME"
        ddlServiceCat.DataValueField = "SER_CODE"
        ddlServiceCat.DataBind()
        ddlServiceCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlServiceCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceCat.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex <> 0 And ddlServiceCat.SelectedIndex <> 0 Then
                bindretypes(ddlLocation.SelectedItem.Value, ddlServiceCat.SelectedItem.Value)
            Else
                ddlSerType.Items.Clear()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnreload_Click(sender As Object, e As EventArgs) Handles btnreload.Click
        BindTimings()
    End Sub
    Private Sub BindTimings()
        If ddlLocation.SelectedIndex > 0 And ddlSerType.SelectedIndex > 0 And ddlServiceCat.SelectedIndex > 0 And txtconvdate.Text <> "" Then
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDMGET_CMS_TIMINGS")
            sp1.Command.AddParameter("@SER_TYPE_LCMID", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_TYPE_CODE", ddlSerType.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_TYPE_CATID", ddlServiceCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@date", txtconvdate.Text, DbType.Date)
            cboHr.DataSource = sp1.GetDataSet()
            cboHr.DataTextField = "TIMINGS"
            cboHr.DataValueField = "VALUE"
            cboHr.DataBind()
            cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))

        End If
    End Sub

    Protected Sub ddlSerType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSerType.SelectedIndexChanged
        BindTimings()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            If txtconvdate.Text <> "" Then
                If CDate(txtconvdate.Text) < getoffsetdate(Date.Today) Then
                    lblMsg.Text = "Convenient Date Should be greater than or equal to  Todays Date"
                    Exit Sub
                End If
            End If
            lblMsg.Text = ""
            Dim Main As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
            MaintenanceReq = Main + "/" + GetTodaysReqCount().ToString()
            Session("reqid") = MaintenanceReq
            'If radlstactions.Items("0").Selected = False Then
            '    ' RaiseRequest("", MaintenanceReq)

            'Else
            RaiseRequest("", MaintenanceReq)
            'End If

            ClearAll()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks1.aspx?id=1&Reqid=" & MaintenanceReq)
    End Sub
    Private Sub ClearAll()
        txtEmpId.Text = ""
        ddlLocation.SelectedIndex = 0
        ddlServiceCat.SelectedIndex = 0
        ddlSerType.SelectedIndex = 0
        txtProbDesc.Text = ""
        txtconvdate.Text = ""
        cboHr.SelectedIndex = 0
    End Sub
    Private Sub RaiseRequest(ByVal mobile As String, ByVal MaintenanceReq As String)
        Dim count As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDMRASIE_HELP_DESK_REQUEST1")
        sp.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
        sp.Command.AddParameter("@AUR_TYPE", "", DbType.String)
        sp.Command.AddParameter("@AUR_ID", txtEmpId.Text, DbType.String)
        sp.Command.AddParameter("@SER_TYPE", ddlSerType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@Name", "", DbType.String)
        sp.Command.AddParameter("@Email", "", DbType.String)
        sp.Command.AddParameter("@Contact", "", DbType.String)
        sp.Command.AddParameter("@SER_BDG_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_RAISED_FOR", ddlSerType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_TYPE", ddlServiceCat.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@SER_SPC_ID", "", DbType.String)
        sp.Command.AddParameter("@DESCRIPTION", txtProbDesc.Text, DbType.String)
        sp.Command.AddParameter("@MOBILE", "", DbType.String)
        sp.Command.AddParameter("@RESPONSE_BY", "", DbType.String)
        sp.Command.AddParameter("@RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ", "", DbType.String)
        sp.Command.AddParameter("@SMS", "", DbType.String)
        sp.Command.AddParameter("@AUR_EMAIL", "", DbType.String)
        If txtconvdate.Text = "" Then
            sp.Command.AddParameter("@CONV_DATE", DBNull.Value, DbType.DateTime)
        Else
            sp.Command.AddParameter("@CONV_DATE", txtconvdate.Text, DbType.DateTime)
        End If
        sp.Command.AddParameter("@CONV_TIME", cboHr.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CONV_TIME1", cboHr.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@CONREMARKS", "NA", DbType.String)
        sp.ExecuteScalar()
        ClearAll()
        lblMsg.Text = "Request Raised Succesfully"
    End Sub
    Function GetTodaysReqCount() As String
        Dim cnt As String
        cnt = 0
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TODAYS_REQUEST_ID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ds = sp.GetDataSet()
        cnt = CInt(ds.Tables(0).Rows(0).Item("cnt")) + 1
        Return cnt
    End Function

    Protected Sub cboHr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboHr.SelectedIndexChanged

    End Sub
End Class
