﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class HDM_HDM_Webfiles_frmhelpthanks
    Inherits System.Web.UI.Page
    Dim message As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_HDM_MESSAGE")
            sp.Command.AddParameter("@id", Request.QueryString("id"), DbType.Int32)
            sp.Command.AddParameter("@cuser", Session("UID"), DbType.String)
            'sp.Command.AddParameter("@creq", Session("reqid"), DbType.String)
            Dim StatusId As Integer = Request.QueryString("id")
            Dim message As String = ""
            Select Case StatusId
                Case 1
                    message = "Service Requisition (" + Request.QueryString("Reqid") + ") raised successfully."
                    'Case 1002
                    '    Message = "Asset Requisition (" + ReqId + ") updated successfully."
                    'Case 1003
                    '    Message = "Asset Requisition (" + ReqId + ") cancelled successfully."
                    'Case 1004
                    '    Message = "Asset Requisition (" + ReqId + ") approved by Admin/IT."
                    'Case 1005
                    '    Message = "Asset Requisition (" + ReqId + ") rejected by Admin/IT."
                    'Case 1006
                    '    Message = "Asset Requisition (" + ReqId + ") approved by Admin."
                    'Case 1007
                    '    Message = "Asset Requisition (" + ReqId + ") rejected by Admin."
                    'Case 1008
                    '    Message = "Asset Requisition (" + ReqId + ") approved by Budget."
                    'Case 1009
                    '    Message = "Asset Requisition (" + ReqId + ") rejected by Budget."
                    'Case 1010
                    '    Message = "Asset Requisition (" + ReqId + ") approved by Co-ordinator Check."
                    'Case 1011
                    '    Message = "Asset Requisition (" + ReqId + ") rejected by Co-ordinator Check."
                    'Case 1012
                    '    Message = "Asset Requisition (" + ReqId + ") approved for procurement."
                    'Case 1013
                    '    Message = "Asset Requisition (" + ReqId + ") rejected for procurement."
                    'Case 1014
                    '    Message = "PO (" + Request("PO") + ")Generated For Asset Requisition (" + ReqId + ")."
                    'Case 1015
                    '    Message = "Asset Requisition (" + ReqId + ") updated successfully."
                    'Case 1016
                    '    Message = "Asset Requisition (" + ReqId + ") updated successfully."
                    'Case 1501
                    '    Message = "Asset Requisition (" + ReqId + ") raised successfully."
                    'Case 1502
                    '    Message = "Asset Requisition (" + ReqId + ") updated successfully."
                    'Case 1503
                    '    Message = "Asset Requisition (" + ReqId + ") cancelled successfully."
                    'Case 1504
                    '    Message = "Asset Requisition (" + ReqId + ") approved by RM."
                    'Case 1505
                    '    Message = "Asset Requisition (" + ReqId + ") rejected by RM."
                    'Case 1506
                    '    Message = "Asset Requisition (" + ReqId + ") approved by Admin."
                    'Case 1507
                    '    Message = "Asset Requisition (" + ReqId + ") rejected by Admin."
            End Select
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                lbl1.Text = ds.Tables(0).Rows(0).Item("Message1") + " (" + Request.QueryString("Reqid") + " )"
                'lbl1.Text = lbl1.Text + " " + message
            End If
            sp.ExecuteScalar()
        End If
    End Sub

End Class
