﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports clsSubSonicCommonFunctions
Partial Class HDM_HDM_Webfiles_frmHDMAdminViewRequisitions
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

     Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            BindLocations()
            BindChildCategory()
            FillStatus()
            lbls.Visible = False
            If gvViewRequisitions.Rows.Count = 0 Then
                showDiv.Visible = False
                btnSubmit.Visible = False
            Else
                showDiv.Visible = True
                btnSubmit.Visible = True
            End If
        End If
    End Sub

    Private Sub FillStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_HDMSTATUS_FILTERS")
        sp.Command.AddParameter("@TYPE", 2, Data.DbType.String)
        ddlStatus.DataSource = sp.GetDataSet
        ddlStatus.DataTextField = "STA_TITLE"
        ddlStatus.DataValueField = "STA_ID"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindGrid()
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_VIEW_REQUISITIONS_TO_ASSIGN")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        ds = sp.GetDataSet
        Session("Reqs") = ds
        gvViewRequisitions.DataSource = ds
        gvViewRequisitions.DataBind()
    End Sub

    Public Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub

    Protected Sub OnRowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvViewRequisitions.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            'Select the Country of Customer in DropDownList
            Dim reqid As String = CType(e.Row.FindControl("lblreqid"), Label).Text
            Dim loc As String = CType(e.Row.FindControl("lblLocation"), Label).Text
            Dim mainCat As String = CType(e.Row.FindControl("lblMain"), Label).Text
            Dim subCat As String = CType(e.Row.FindControl("lblSub"), Label).Text
            Dim childCat As String = CType(e.Row.FindControl("lblChild"), Label).Text
            Dim ds As New DataSet
            Dim param As SqlParameter() = New SqlParameter(3) {}
            param(0) = New SqlParameter("@SEM_LOC_CODE", loc)
            param(1) = New SqlParameter("@SEM_MNC_CODE", mainCat)
            param(2) = New SqlParameter("@SEM_SUBC_CODE", subCat)
            param(3) = New SqlParameter("@SEM_CHC_CODE", childCat)
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_BIND_INCHARGES", param)

            Dim ddlAssign As DropDownList = CType(e.Row.FindControl("ddlAssignTo"), DropDownList)
            ddlAssign.DataSource = ds
            'ddlAssignTo.DataSource = GetData("SELECT DISTINCT Country FROM Customers")
            ddlAssign.DataTextField = "ASSIGNED_TO"
            ddlAssign.DataValueField = "AUR_ID"
            ddlAssign.ClearSelection()
            ddlAssign.DataBind()
            ddlAssign.Items.Insert(0, "--Select--")
        End If
    End Sub
  
    Protected Sub gvViewRequisitions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex()
        gvViewRequisitions.DataSource = Session("Reqs")
        gvViewRequisitions.DataBind()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_REQUISITIONS_VIEW_ASSIGN")
        sp.Command.AddParameter("@REQID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String)
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        gvViewRequisitions.DataSource = sp.GetDataSet
        gvViewRequisitions.DataBind()
        Session("Reqs") = sp.GetDataSet
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim lblreqid As Label
        Dim ddlAssign As DropDownList
        Dim reqids As String = String.Empty

        Dim grdCount As Integer = gvViewRequisitions.Rows.Count
        Dim chkCnt As Integer = 0
        For Each row As GridViewRow In gvViewRequisitions.Rows
            Dim ddlAsn As DropDownList
            ddlAsn = DirectCast(row.FindControl("ddlAssignTo"), DropDownList)
            If ddlAsn.SelectedValue = "--Select--" Then
                chkCnt = chkCnt + 1
            End If
        Next

        If grdCount = chkCnt Then
            lblMessage.Text = "Please Re-Assign for Atleast One HD Request"
            Exit Sub
        End If

        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvViewRequisitions.Rows
                'Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                ddlAssign = DirectCast(row.FindControl("ddlAssignTo"), DropDownList)
                If ddlAssign.SelectedValue <> "--Select--" Then
                    lblreqid = DirectCast(row.FindControl("lblreqid"), Label)
                    Dim rems As TextBox = DirectCast(row.FindControl("txtUpdateRemarks"), TextBox)
                    Dim param As SqlParameter() = New SqlParameter(3) {}
                    param(0) = New SqlParameter("@REQ_ID", lblreqid.Text)
                    param(1) = New SqlParameter("@ASSIGN_TO", ddlAssign.SelectedValue)
                    param(2) = New SqlParameter("@UPDATE_REMARKS", rems.Text)
                    param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_ASSIGN_REQUEST", param)
                End If

            Next
            BindGrid()
            txtUpdateRemarks.Text = ""
            lblMessage.Visible = True
            lblMessage.Text = "Assigned to incharge Successfully"

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
 
End Class
