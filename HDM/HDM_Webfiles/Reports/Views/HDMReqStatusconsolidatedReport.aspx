﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <style>
        .grid-align
        {
            text-align: center;
        }

        a:hover
        {
            cursor: pointer;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }

        .modal-header-primary
        {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word
        {
            color: #4813CA;
        }

        #pdf
        {
            color: #FF0023;
        }

        #excel
        {
            color: #2AE214;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button
        {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMReqStatusconsolidatedReportController" class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Consolidated Request Status Report</legend>
                    </fieldset>
                    <div class="well">
                        <div class="row" style="padding-left: 18px">
                            <div class="col-md-6">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small"
                                    data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />
                            </div>
                            <div class="col-md-6" id="table2">
                                <br />
                                <a data-ng-click="GenReport('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                        <br />
                        <form id="form2">
                            <div id="Tabular">
                                <div class="row" style="padding-left: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                            <div id="Graphicaldiv">
                                <div id="HDMconsolidatedGraph">&nbsp</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../../../Scripts/jspdf.min.js"></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script>
        //agGrid.initialiseAgGridWithAngular1(angular);
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../Js/HDMReqStatusconsolidatedReport.js"></script>
    <script src="../../../../SMViews/Utility.js"></script>
    <script src="../../../../Scripts/moment.min.js"></script>
</body>
</html>
