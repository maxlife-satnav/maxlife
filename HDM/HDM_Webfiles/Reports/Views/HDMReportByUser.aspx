﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
   <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
   <%-- <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
    <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>--%>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
      <script type="text/javascript">
          function maxLength(s, args) {
              if (args.Value.length >= 500)
                  args.IsValid = false;
          }
          function setup(id) {
              $('#' + id).datepicker({
                  format: 'mm/dd/yyyy',
                  autoclose: true,
                  todayHighlight: true
              });
          };

    </script>

</head>
<body data-ng-controller="HDMReportByUserController">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">

                    <fieldset>
                        <legend>HDM Report by User</legend>
                    </fieldset>
                    <div class="row">
                        <div class="col-md-4">
                            <label id="lblSuccessHeader" for="lblMsg" data-ng-show="ShowMessageHeader" class="col-md-12 control-label" style="color: red">{{SuccessHeader}}</label>
                        </div>
                    </div>

                    <div class="well">
                        <form role="form" id="HDMRptByUser" name="frmHDMRptByUser" data-valid-submit="GetSpacesToRelease()" novalidate>
                                <div class="clearfix">

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.CNY_NAME.$invalid}">
                                            <label for="txtcode">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Getcountry" data-output-model="RptByUsr.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                                data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.CTY_NAME.$invalid}">
                                            <label for="txtcode">City <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Citylst" data-output-model="RptByUsr.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                                data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.LCM_NAME.$invalid}">
                                            <label for="txtcode">Location <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Locationlst" data-output-model="RptByUsr.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.LCM_NAME.$invalid" style="color: red">Please select Location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.TWR_NAME.$invalid}">
                                            <label for="txtcode">Tower <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Towerlist" data-output-model="RptByUsr.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                                data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.TWR_NAME.$invalid" style="color: red">Please select Tower </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.MC.$invalid}">
                                            <label for="txtcode">Main Category <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="maincategorylist" data-output-model="RptByUsr.selectedMC" data-button-label="icon MNC_NAME" data-item-label="icon MNC_NAME MNC_CODE"
                                                data-on-item-click="MCChange()" data-on-select-all="MCChangeAll()" data-on-select-none="MCSelectNone()" data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedMC" name="MC" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.MC.$invalid" style="color: red">Please select Main Category </span>
                                        </div>
                                    </div>



                                     <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.SC.$invalid}">
                                            <label for="txtcode">Sub Category <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="SubCategorylist" data-output-model="RptByUsr.selectedSC" data-button-label="icon SUBC_NAME" data-item-label="icon SUBC_NAME SUBC_CODE"
                                                data-on-item-click="SCChange()" data-on-select-all="SCChangeAlll()" data-on-select-none="SCNone()" data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedSC" name="SC" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.SC.$invalid" style="color: red">Please select Sub Category </span>
                                        </div>
                                    </div>

                                      <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.CC.$invalid}">
                                            <label for="txtcode">Child Category <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="ChildCategorylist" data-output-model="RptByUsr.selectedCC" data-button-label="icon CHC_TYPE_NAME" data-item-label="icon CHC_TYPE_NAME CHC_TYPE_CODE"
                                               data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedCC" name="CC" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.CC.$invalid" style="color: red">Please select Child Category </span>
                                        </div>
                                    </div>



                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.ReqBy.$invalid}">
                                            <label for="txtcode">Requested By<span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="ReqBylist" data-output-model="RptByUsr.selectedReqBy" data-button-label="icon AUR_KNOWN_AS" data-item-label="icon AUR_KNOWN_AS "
                                               data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedReqBy" name="ReqBy" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.ReqBy.$invalid" style="color: red">Please select Requested By </span>
                                        </div>
                                    </div>


                                                                 
                                   
                                </div>
                                <div class="clearfix">


                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.FROM_DATE.$invalid}">
                                            <label for="txtcode">From Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='fromdate'>
                                                <input type="text" class="form-control" data-ng-model="RptByUsr.FROM_DATE" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                </span>
                                            </div>

                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.FROM_DATE.$invalid" style="color: red">Please select From Date </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.TO_DATE.$invalid}">
                                            <label for="txtcode">To Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='todate'>
                                                <input type="text" class="form-control" data-ng-model="RptByUsr.TO_DATE" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.TO_DATE.$invalid" style="color: red">Please select To Date </span>
                                        </div>
                                    </div>

                                  
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMRptByUser.$submitted && frmHDMRptByUser.ReqSTA.$invalid}">
                                            <label for="txtcode">Requested Status<span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="ReqSTAlist" data-output-model="RptByUsr.selectedReqSTA" data-button-label="icon STA_DESC" data-item-label="icon STA_DESC"
                                               data-tick-property="ticked" max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.selectedReqSTA" name="ReqSTA" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMRptByUser.$submitted && frmHDMRptByUser.ReqSTA.$invalid" style="color: red">Please select Requested Status </span>
                                        </div>
                                    </div>
                                     
                                </div>
                                <div class="clearfix">
                                    <div class="box-footer text-right">
                                        <button type="submit" value="Search" class="btn btn-primary custom-button-color">Search</button>
                                        <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                         </form>
                        <br />

                        <form role="form" id="HDMRptByUserDetails" name="frmHDMRptByUserDetails">
                            <div data-ng-show="SRShowGrid">
                             
                               <div class="row">
                                    <div class="col-md-12" style="height: 320px">
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <textarea rows="2" data-ng-model="SSR_REQ_REM" class="form-control" placeholder="Remarks"></textarea>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <input type="submit" data-ng-click="ReleaseSelectedSpaces()" value="Release" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../js/HDMUtility.js"></script>
    <script src="../../../../SMViews/Utility.js"></script>
    <script src="../Js/HDMReportByUser.js"></script>
</body>
</html>