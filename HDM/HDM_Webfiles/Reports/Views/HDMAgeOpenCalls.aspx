﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align
        {
            text-align: center;
        }

        a:hover
        {
            cursor: pointer;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }

        .modal-header-primary
        {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word
        {
            color: #4813CA;
        }

        #pdf
        {
            color: #FF0023;
        }

        #excel
        {
            color: #2AE214;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button
        {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMAgeOpenCallsController" onload="setDateVals()" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Age of Open Calls Report</legend>
                    </fieldset>
                    <div class="well">
                        <form id="form1" name="frmopencalls" data-valid-submit="LoadData()">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date</label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="OpenCallsRpt.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmopencalls.$submitted && frmopencalls.FromDate.$invalid" style="color: red;">Please From Date</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date</label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="OpenCallsRpt.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmopencalls.$submitted && frmopencalls.ToDate.$invalid" style="color: red;">Please To Date</span>
                                    </div>
                                </div>
                                  <div class="col-md-3 col-sm-6 col-xs-12">
                                      <br />
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                        </div>
                            </div>
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small"
                                    data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>
                        </form>

                        <form id="form2" data-ng-show="GridVisiblity">
                            <div id="Tabular">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <a data-ng-click="GenReport(OpenCallsRpt,'doc')"><i id="word" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(OpenCallsRpt,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(OpenCallsRpt,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div></div>
                                </div>
                                <div class="row">
                                    <div >
                                        <div>
                                            <div data-ag-grid="gridOptions" class="ag-blue" style=" overflow-x:hidden !important; overflow-y:hidden !important; width:385px; height:134px;"></div>
                                        </div>
                                    </div>
                                </div>
                            
                             <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                                <div id="OccupGraph">&nbsp</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../../../Scripts/jspdf.min.js"></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../Js/HDMAgeOpenCalls.js"></script>
    <script src="../../../../SMViews/Utility.js"></script>
    <script src="../../../../Scripts/moment.min.js"></script>
    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY')));
        }
    </script>

</body>

</html>
