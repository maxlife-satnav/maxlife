﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmHDMReportViewDetails.aspx.vb"
    Inherits="HDM_HDM_Webfiles_Reports_frmHDMReportViewDetails" Title="Report by user" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>
    <script type="text/javascript">
        function checkDate(sender, args) {
            var toDate = new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if (sender._selectedDate < toDate) {
                document.getElementById("<%= txtconvdate.ClientID %>").value = "";
                alert("You can't select day earlier than today!");

                sender._selectedDate = toDate;
                //set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Report by user requisition details</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Requisition Id</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="lblReqId" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Employee ID</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="true" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Raised By</label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlEmp" runat="server" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Location</label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                            ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Service Category</label>
                                        <asp:Label ID="lblTemp" runat="server" Visible="false"></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlServiceCategory"
                                            ErrorMessage="Please Select Service Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                        </asp:CompareValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlServiceCategory" runat="server" AutoPostBack="True" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Service Type</label>
                                        <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlServiceType"
                                            ErrorMessage="Please Select Service Type" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlServiceType" runat="server" AutoPostBack="True" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Problem Description</label>
                                        <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtRemarks"
                                            ValidationGroup="Val1" Display="NONE" ErrorMessage="Problem Description Required"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Service Required At</label>
                                        <asp:RequiredFieldValidator ID="rfvSerReqAt" runat="server" ControlToValidate="txtServReqAt"
                                            ValidationGroup="Val1" Display="NONE" ErrorMessage="Please Enter Service Required At"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtServReqAt" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine"
                                                MaxLength="10000" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Convenient Date</label>
                                        <asp:RequiredFieldValidator ID="rfvconvdate" runat="server" ControlToValidate="txtconvdate"
                                            Display="NONE" ErrorMessage="Please Select Convenient date" ValidationGroup="Val1"
                                            Enabled="true"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtconvdate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Convenient Time<span style="color: red;"></span></label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="cboHr" runat="server" AutoPostBack="True" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Text="--HH--" Value="--HH--"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlMins" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false" AutoPostBack="True">
                                                <asp:ListItem Text="--MIN--" Value="--MIN--"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                                <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                                <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                                <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                                <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                                <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                                <asp:ListItem Text="32" Value="32"></asp:ListItem>
                                                <asp:ListItem Text="33" Value="33"></asp:ListItem>
                                                <asp:ListItem Text="34" Value="34"></asp:ListItem>
                                                <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                                <asp:ListItem Text="36" Value="36"></asp:ListItem>
                                                <asp:ListItem Text="37" Value="37"></asp:ListItem>
                                                <asp:ListItem Text="38" Value="38"></asp:ListItem>
                                                <asp:ListItem Text="39" Value="39"></asp:ListItem>
                                                <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                                <asp:ListItem Text="41" Value="41"></asp:ListItem>
                                                <asp:ListItem Text="42" Value="42"></asp:ListItem>
                                                <asp:ListItem Text="43" Value="43"></asp:ListItem>
                                                <asp:ListItem Text="44" Value="44"></asp:ListItem>
                                                <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                                <asp:ListItem Text="46" Value="46"></asp:ListItem>
                                                <asp:ListItem Text="47" Value="47"></asp:ListItem>
                                                <asp:ListItem Text="48" Value="48"></asp:ListItem>
                                                <asp:ListItem Text="49" Value="49"></asp:ListItem>
                                                <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                <asp:ListItem Text="51" Value="51"></asp:ListItem>
                                                <asp:ListItem Text="52" Value="52"></asp:ListItem>
                                                <asp:ListItem Text="53" Value="53"></asp:ListItem>
                                                <asp:ListItem Text="54" Value="54"></asp:ListItem>
                                                <asp:ListItem Text="55" Value="55"></asp:ListItem>
                                                <asp:ListItem Text="56" Value="56"></asp:ListItem>
                                                <asp:ListItem Text="57" Value="57"></asp:ListItem>
                                                <asp:ListItem Text="58" Value="58"></asp:ListItem>
                                                <asp:ListItem Text="59" Value="59"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Admin Remarks</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Service In-charge(RM) Remarks</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
