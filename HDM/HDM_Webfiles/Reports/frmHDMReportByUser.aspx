﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmHDMReportByUser.aspx.vb"
    Inherits="HDM_HDM_Webfiles_Reports_frmHDMReportByUser" Title="Report by user" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Report by user</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:HyperLink ID="hyOpen" runat="server"
                                                NavigateUrl="~/HDM/HDM_Webfiles/Reports/frmHDMStatusDetailsReport.aspx?RID=1">                                
                                            </asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:HyperLink ID="hyProgress" runat="server"
                                                NavigateUrl="~/HDM/HDM_Webfiles/Reports/frmHDMStatusDetailsReport.aspx?RID=2"> 
                                            </asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:HyperLink ID="hyClosed" runat="server"
                                                NavigateUrl="~/HDM/HDM_Webfiles/Reports/frmHDMStatusDetailsReport.aspx?RID=3">
                                            </asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <%--<div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select User <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvassigned" runat="server" ValidationGroup="Val1"
                                            InitialValue="--Select--" ErrorMessage="Please Select User" Display="None" ControlToValidate="ddlassigned">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlassigned" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Employee">Employee</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Enter Employee ID <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvempid" runat="server" ValidationGroup="Val1" ErrorMessage="Please Enter Employee ID" Display="NONE" ControlToValidate="txtempid" Enabled="true"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">

                                            <asp:TextBox ID="txtempid" runat="server" CssClass="form-control" AutoCompleteType="Disabled" AutoComplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select From Date" Display="None" ControlToValidate="txtfromdate">

                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select To Date" Display="None" ControlToValidate="txttodate"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" runat="server" Text="Submit" CausesValidation="true" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"></asp:Button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row table table table-condensed table-responsive">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        EmptyDataText="No User Report Found." CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10">
                                        <Columns>
                                            <asp:BoundField DataField="SERVICE_CATEGORY" HeaderText="Service Category" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="SERVICE_TYPE" HeaderText="Service Type" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="REQUESTED_BY" HeaderStyle-Width="10%" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="REQUESITION_ID" HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="CLOSED_TIME" HeaderText="Closed Time" ItemStyle-HorizontalAlign="left" Visible="false" />
                                            <asp:BoundField DataField="CLOSED_TIME" HeaderText="Updated Time" ItemStyle-HorizontalAlign="left" Visible="false" />
                                            <asp:BoundField DataField="TOTAL_TIME" HeaderText="Total Time(Min)" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="DEFINED_TAT" HeaderText="Defined TAT" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="DELAYED_TIME" HeaderText="Delayed Time(Min)" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" />
                                            <%--<asp:BoundField DataField="SER_STATUS" HeaderText="Status" ItemStyle-HorizontalAlign="left" />--%>
                                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("REQUESITION_ID", "~/HDM/HDM_Webfiles/Reports/frmHDMReportViewDetails.aspx?RID={0}")%>'
                                                        Text="View Details"></asp:HyperLink>
                                                    <%--<asp:HyperLink ID="hLinkFeedBack" runat="server" NavigateUrl='<%#Eval("REQUISITION_ID", "~/HDM/HDM_Webfiles/frmHDMFeedback.aspx?RID={0}")%>'
                                            Text="Give Feedback"></asp:HyperLink>--%>
                                                    <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("SER_STATUS") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
