﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmHDMViewCategoryDetailReport.aspx.vb"
     Inherits="HDM_HDM_Webfiles_Reports_frmHDMViewCategoryDetailReport" title="Report By Category Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <script type="text/javascript">
        function checkDate(sender, args) {
            var toDate = new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if (sender._selectedDate < toDate) {
                document.getElementById("<%= txtconvdate.ClientID %>").value = "";
                alert("You can't select day earlier than today!");

                sender._selectedDate = toDate;
                //set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Report by category requisition details
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>Report by category requisition details</strong></td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                <td align="center" valign="top" height="100%">
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label></td>

            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table width="100%" cellpadding="2px" border="1" cellspacing="0">
                        <tr>
                            <td align="left">Requisition Id :</td>
                            <td align="left">
                                <asp:Label ID="lblReqId" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Employee ID :</td>
                            <td align="left">
                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="true" Width="97%">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Raised By :</td>
                            <td align="left">
                                <div>
                                    <asp:DropDownList ID="ddlEmp" runat="server" Width="97%" Enabled="false" CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Location :<font class="clsNote"> *</font>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                    ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                            </td>
                            <td align="left">
                                <div>
                                    <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" Width="97%" Enabled="false" CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td align="left">Service Category :<font class="clsNote"> *</font>
                                <asp:Label ID="lblTemp" runat="server" Visible="false"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlServiceCategory"
                                    ErrorMessage="Please Select Service Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                </asp:CompareValidator>
                            </td>
                            <td align="left">
                                <div>
                                    <asp:DropDownList ID="ddlServiceCategory" runat="server" AutoPostBack="True" Width="97%" Enabled="false" CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px">Service Type :<font class="clsNote"> *</font>
                                <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlServiceType"
                                    ErrorMessage="Please Select Service Type" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                            </td>
                            <td align="left" style="height: 26px">
                                <div>
                                    <asp:DropDownList ID="ddlServiceType" runat="server" AutoPostBack="True" Width="97%" Enabled="false" CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td align="left">Problem Description :<font class="clsNote"> *</font>
                                <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtRemarks"
                                    ValidationGroup="Val1" Display="NONE" ErrorMessage="Problem Description Required"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left">
                                <div>
                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="97%" Enabled="false">
                                    </asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">Service Required At<font class="clsNote"> *</font>
                                <asp:RequiredFieldValidator ID="rfvSerReqAt" runat="server" ControlToValidate="txtServReqAt"
                                    ValidationGroup="Val1" Display="NONE" ErrorMessage="Please Enter Service Required At"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <div>
                                    <asp:TextBox ID="txtServReqAt" runat="server" Width="97%" Rows="3" TabIndex="15" TextMode="MultiLine"
                                        MaxLength="10000" Enabled="false"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Convenient Date :<font class="clsNote"> *</font>
                                <asp:RequiredFieldValidator ID="rfvconvdate" runat="server" ControlToValidate="txtconvdate"
                                    Display="NONE" ErrorMessage="Please Select Convenient date" ValidationGroup="Val1"
                                    Enabled="true"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; z-index: 900; position: inherit;">
                                <asp:TextBox ID="txtconvdate" runat="server" CssClass="clsTextField" Width="50%" Enabled="false"></asp:TextBox>
                                <%--<asp:Button ID="btnReload" Visible="false" CssClass="button" runat="server" Text="Reload" Enabled="false" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Convenient Time :
                            </td>
                            <td align="left" style="height: 26px; width: 50%;">
                                <asp:DropDownList ID="cboHr" runat="server" Width="25%" CssClass="clsComboBox" AutoPostBack="True" Enabled="false">
                                    <asp:ListItem Text="--HH--" Value="--HH--"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                    <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                    <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                    <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                    <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                    <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                    <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                    <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                    <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlMins" runat="server" Width="25%" CssClass="clsComboBox" Enabled="false" AutoPostBack="True">
                                    <asp:ListItem Text="--MIN--" Value="--MIN--"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                    <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                    <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                    <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                    <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                    <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                    <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                    <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                    <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                    <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                    <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                    <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                    <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                    <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                    <asp:ListItem Text="32" Value="32"></asp:ListItem>
                                    <asp:ListItem Text="33" Value="33"></asp:ListItem>
                                    <asp:ListItem Text="34" Value="34"></asp:ListItem>
                                    <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                    <asp:ListItem Text="36" Value="36"></asp:ListItem>
                                    <asp:ListItem Text="37" Value="37"></asp:ListItem>
                                    <asp:ListItem Text="38" Value="38"></asp:ListItem>
                                    <asp:ListItem Text="39" Value="39"></asp:ListItem>
                                    <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                    <asp:ListItem Text="41" Value="41"></asp:ListItem>
                                    <asp:ListItem Text="42" Value="42"></asp:ListItem>
                                    <asp:ListItem Text="43" Value="43"></asp:ListItem>
                                    <asp:ListItem Text="44" Value="44"></asp:ListItem>
                                    <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                    <asp:ListItem Text="46" Value="46"></asp:ListItem>
                                    <asp:ListItem Text="47" Value="47"></asp:ListItem>
                                    <asp:ListItem Text="48" Value="48"></asp:ListItem>
                                    <asp:ListItem Text="49" Value="49"></asp:ListItem>
                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                    <asp:ListItem Text="51" Value="51"></asp:ListItem>
                                    <asp:ListItem Text="52" Value="52"></asp:ListItem>
                                    <asp:ListItem Text="53" Value="53"></asp:ListItem>
                                    <asp:ListItem Text="54" Value="54"></asp:ListItem>
                                    <asp:ListItem Text="55" Value="55"></asp:ListItem>
                                    <asp:ListItem Text="56" Value="56"></asp:ListItem>
                                    <asp:ListItem Text="57" Value="57"></asp:ListItem>
                                    <asp:ListItem Text="58" Value="58"></asp:ListItem>
                                    <asp:ListItem Text="59" Value="59"></asp:ListItem>
                                </asp:DropDownList>                                
                            </td>
                        </tr>
                        
                        <tr id="trAdminRemarks" runat="server">
                            <td align="left">Admin Remarks :
                            </td>
                            <td align="left">
                                <div>
                                    <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" Width="97%" Enabled="false">
                                    </asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr id="trRMRemarks" runat="server">
                            <td align="left">Service Incharge(RM) Remarks :
                            </td>
                            <td align="left">
                                <div>
                                    <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" Width="97%" Enabled="false">
                                    </asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="button" OnClick="btnBack_Click" />
                               <%-- <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" />
                                <asp:Button ID="btnModify" Text="Modify" runat="server" CssClass="button" OnClientClick="return checkDate();" />&nbsp;--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>