﻿app.service("HDMTATReportService", function ($http, $q, UtilityService) {
    this.LoadGrid = function (HDMTAT) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMTATReport/GetGrid', HDMTAT)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMTATReport/GetDetailsOnSelection/', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.getRequestTypes = function () {
        deferred = $q.defer();
        $http.get(UtilityService.path + '/api/HDMTATReport/GetRequestTypes')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };
});

app.controller('HDMTATReportController', function ($scope, $q, $http, HDMTATReportService, UtilityService, $timeout) {
    $scope.HDMTAT = {};
    $scope.GridVisiblity = true;
    $scope.Spcdata = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.REQUEST_TYPE_For_PopUP = "";
    $scope.RequestTypes = [];
           
    $scope.columnDefs = [
              {
                  headerName: "Request ID", field: "REQ_ID", width: 200, cellClass: 'grid-align',
                  template: '<a ng-click="ShowPopup(data)">{{data.REQ_ID}}</a>', filter: 'text', pinned: 'left', suppressMenu: true
              },
              { headerName: "Description", field: "DESCRIPTION", cellClass: 'grid-align', width: 500 },
              { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 300 },
    ];

    $scope.LoadData = function () {
        var params = {
            FromDate: $scope.HDMTAT.FromDate,
            ToDate: $scope.HDMTAT.ToDate,
            Request_Type: $scope.HDMTAT.Request_Type
        };

        var fromdate = moment($scope.HDMTAT.FromDate);
        var todate = moment($scope.HDMTAT.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            progress(0, 'Loading...', false);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            HDMTATReportService.LoadGrid(params).then(function (data) {
                $scope.gridata = data;
                if ($scope.gridata == null) {
                    progress(0, 'Loading...', false);
                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                //$scope.SpaceChartDetails(params);
                //chart.unload();
                //chart.load({ columns: data });
                //setTimeout(function () {
                //    $("#SpcGraph").append(chart.element);
                //}, 700);
                progress(0, 'Loading...', false);
            }, function (error) {
                console.log(error);
            });
        }
    }    HDMTATReportService.getRequestTypes().then(function (response) {
        console.log(response.data);
        if (response.data != null) {
            $scope.RequestTypes = response.data;
        }
    });    $scope.GetTAT = function () {
        progress(0, 'Loading...', true);
        $scope.rptDateRanges();
        $scope.Pageload = {
            FromDate: $scope.HDMTAT.FromDate,
            ToDate: $scope.HDMTAT.ToDate,
            Request_Type: $scope.HDMTAT.Request_Type
        };
        //console.log($scope.Pageload);
        HDMTATReportService.LoadGrid($scope.Pageload).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
        }, function (error) {
            console.log(error);
            progress(0, '', false);
        });
    }    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    var PopDefs = [
          { headerName: "Request ID", field: "REQ_ID", width: 190, cellClass: 'grid-align', suppressMenu: true, },
          { headerName: "Requested By", field: "REQUESTED_BY", width: 190, cellClass: 'grid-align' },
          { headerName: "Requested Date", field: "REQUESTED_DATE", width: 190, cellClass: 'grid-align', suppressMenu: true, },
          { headerName: "Assigend To", field: "ASSIGNED_TO", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },
          { headerName: "Comments", field: "DESCRIPTION", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },          
          { headerName: "Status", field: "REQ_STATUS", cellClass: 'grid-align', width: 200 },
          { headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align', hide: false, suppressMenu: true, },
          { headerName: "Total Time", field: "TOTAL_TIME", width: 100, cellClass: 'grid-align', hide: false, suppressMenu: true, },
          { headerName: "Delayed Time", field: "DELAYED_TIME", width: 110, cellClass: 'grid-align', hide: false, suppressMenu: true, },          
          { headerName: "Closed Time", field: "CLOSED_TIME", width: 150, cellClass: 'grid-align', suppressMenu: true, },
    ];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        onReady: function () {
            $scope.PopOptions.api.sizeColumnsToFit()
        }
    }

    $scope.ShowPopup = function (data) {
        $scope.SelValue = data;
        $scope.REQUEST_TYPE_For_PopUP = data.REQUEST_TYPE;
        $scope.CurrentProfile = [];
        $("#historymodal").modal('show');
    }

    $('#historymodal').on('shown.bs.modal', function () {
        HDMTATReportService.GetDetailsOnSelection($scope.SelValue).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.popdata = response.data;
            $scope.PopOptions.api.setRowData($scope.popdata);
            progress(0, '', false);
        });
    });

    //$("#Tabular").fadeIn();
    //$("#Graphicaldiv").fadeOut();
    //$("#table2").fadeIn();
    //var chart;
    //chart = c3.generate({
    //    data: {
    //        columns: [],
    //        cache: false,
    //        type: 'bar',
    //        empty: { label: { text: "Sorry, No Data Found" } },
    //    },
    //    legend: {
    //        position: 'top'
    //    },
    //    axis: {
    //        x: {
    //            type: 'category',
    //            categories: ['Location'],
    //            height: 130,
    //            show: true,
    //        },
    //        y: {
    //            show: true,
    //            label: {
    //                text: 'Requisition Count',
    //                position: 'outer-middle'
    //            }
    //        }
    //    },
    //    width:
    //    {
    //        ratio: 0.5
    //    }
    //});

    //$scope.SpaceChartDetails = function (spcData) {
    //    $http({
    //        url: UtilityService.path + '/api/HDMTATReport/GetSpaceChartData',
    //        method: 'POST',
    //        data: spcData
    //    }).success(function (result) {
    //        chart.unload();
    //        chart.load({ columns: result });
    //    });
    //    setTimeout(function () {
    //        $("#SpcGraph").append(chart.element);
    //    }, 700);
    //}

    //$('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
    //    if (state) {
    //        $("#Graphicaldiv").fadeOut(function () {
    //            $("#Tabular").fadeIn();
    //            $("#table2").fadeIn();
    //        });
    //    }
    //    else {
    //        $("#Tabular").fadeOut(function () {
    //            $("#Graphicaldiv").fadeIn();
    //            $("#table2").fadeOut();
    //        });
    //    }
    //});

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Request ID", key: "REQ_ID" }, { title: "Description", key: "DESCRIPTION" }, { title: "Status", key: "STA_TITLE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("HDMTATReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDMTATReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (spcdata, Type) {
        progress(0, 'Loading...', true);
        spcdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (spcdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/HDMTATReport/GetSpaceRequisitionReportdata',
                method: 'POST',
                data: spcdata,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'HDMTATReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

   
    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {       
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.HDMTAT.FromDate = moment().format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.HDMTAT.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.HDMTAT.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.HDMTAT.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.HDMTAT.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.HDMTAT.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.HDMTAT.Request_Type = "ALL";
    $timeout($scope.GetTAT, 500);
});