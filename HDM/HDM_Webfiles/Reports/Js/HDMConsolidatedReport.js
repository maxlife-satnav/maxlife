﻿app.service("HDMConsolidatedReportService", function ($http, $q, UtilityService) {
    this.GetConsReport = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMConsolidatedReport/GetHDMConsolidateDetails', searchObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});

app.controller('HDMConsolidatedReportController', function ($scope, $q, UtilityService, HDMConsolidatedReportService, $timeout, $http) {
    $scope.ConsReport = {};
    $scope.ConsReportGrid = true;
    $scope.ActionStatus = 0;
    $scope.DocTypeVisible = 0;
    $scope.GetConsReport = function () {
        $scope.rptDateRanges();
        $scope.HDMConsReport = {
            FromDate: $scope.ConsReport.FromDate,
            ToDate: $scope.ConsReport.ToDate
        };
        var fromdate = moment($scope.ConsReport.FromDate);
        var todate = moment($scope.ConsReport.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            HDMConsolidatedReportService.GetConsReport($scope.HDMConsReport).then(function (response) {
                $scope.ConsReportGrid = true;
                $scope.gridata = response;                
                if (response.data == null) {                    
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, 'Loading...', false);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.gridOptions.api.setRowData(response.data);
                    progress(0, 'Loading...', false);
                }
            }, function (error) {
                console.log(error);
            });
        };
    }

    var columnDefs = [
         { headerName: "Requisition Id", field: "SER_REQ_ID", width: 130, cellClass: 'grid-align', },
         { headerName: "Main Category", field: "MNC_NAME", width: 130, cellClass: 'grid-align', },
         { headerName: "Sub Category", field: "SUBC_NAME", width: 140, cellClass: 'grid-align', },
         { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 150, cellClass: 'grid-align', },
         { headerName: "Location", field: "LCM_NAME", width: 120, cellClass: 'grid-align', },
         { headerName: "Problem Description", field: "SER_PROB_DESC", width: 150, cellClass: 'grid-align', },
         { headerName: "Requested By", field: "REQUESTED_BY", width: 150, cellClass: 'grid-align', },
         { headerName: "Requested Date", field: "SER_CREATED_DT", width: 130, cellClass: 'grid-align', },
         { headerName: "Updated Date", field: "SER_UPDATED_DT", width: 130, cellClass: 'grid-align', },
         { headerName: "Assigned Date", field: "ASSIGNED_DATE", width: 130, cellClass: 'grid-align', },
         { headerName: "Assigned To", field: "SERH_ASSIGN_TO", width: 100, cellClass: 'grid-align', },
         { headerName: "Status", field: "STA_TITLE", width: 100, cellClass: 'grid-align', },
         { headerName: "FeedBack", field: "FEEBACK", width: 100, cellClass: 'grid-align', },
         { headerName: "Labour Cost", field: "LABOUR_COST", width: 100, cellClass: 'grid-align', },
         { headerName: "Spare Cost", field: "SPARE_COST", width: 100, cellClass: 'grid-align', },
         { headerName: "Additional Cost", field: "ADDITIONAL_COST", width: 120, cellClass: 'grid-align', },
         { headerName: "Closed Time", field: "CLOSED_TIME", width: 130, cellClass: 'grid-align', },
         { headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align', },
         { headerName: "Response TAT", field: "RESPONSE_TAT", width: 140, cellClass: 'grid-align', },
         { headerName: "Delayed TAT", field: "DELAYED_TAT", width: 140, cellClass: 'grid-align', },
    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function onUpdateFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }

    $("#UpdteFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    $scope.ConsReport.selVal = "THISMONTH";

    $scope.rptDateRanges = function () {
        switch ($scope.ConsReport.selVal) {
            case 'TODAY':
                $scope.ConsReport.FromDate = moment().format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.ConsReport.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break;
            case '7':
                $scope.ConsReport.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.ConsReport.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.ConsReport.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.ConsReport.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }

    function getLastMonthData() {
        $scope.GetConsReport();
    }

    // $timeout(getLastMonthData, 200);

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [            
            { title: "Requisition Id", key: "SER_REQ_ID" }, { title: "Main Category", key: "MNC_NAME" }, { title: "Sub Category", key: "SUBC_NAME" }, 
            { title: "Child Category", key: "CHC_TYPE_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Prbolem Description", key: "SER_PROB_DESC" }, 
            { title: "Requested By", key: "REQUESTED_BY" }, { title: "Requested Date", key: "SER_CREATED_DT" }, { title: "Updated Date", key: "SER_UPDATED_DT" },
            { title: "Assingned Date", key: "ASSIGNED_DATE" },  { title: "Assingned To", key: "SERH_ASSIGN_TO" }, { title: "Closed Time", key: "CLOSED_TIME" }, 
            { title: "Defined TAT", key: "DEFINED_TAT" }, { title: "Response TAT(in Min)", key: "RESPONSE_TAT" }, { title: "Delayed TAT(in Min)", key: "DELAYED_TAT" }
                    ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("ConsolidatedReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "ReportByCategory.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (ConsReport, Type) {
        progress(0, 'Loading...', true);
        ConsReport.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (ConsReport.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        } else {
            $http({
                url: UtilityService.path + '/api/HDMConsolidatedReport/GetConsolidatedReportDoc',
                method: 'POST',
                data: ConsReport,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Consolidated Report.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, 'Loading...', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $timeout($scope.GetConsReport, 500);

});

