﻿app.service("HDMReportByUserService", function ($http, $q) {
    this.getHDMReportByUser = function (searchObj) {
        var deferred = $q.defer();
        return $http.post('../../../../api/HDMReportByUser/GetHDMReportByUser', searchObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.GetHistory = function (data) {
        console.log(data);
        deferred = $q.defer();
        return $http.post('../../../../api/HDMReportByUser/GetHistory/', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('HDMReportByUserController', function ($scope, $q, HDMReportByUserService, UtilityService, HDMUtilityService, $timeout, $http) {
    $scope.RptByUsr = {};
    $scope.Getcountry = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];

    $scope.maincategorylist = [];
    $scope.SubCategorylist = [];
    $scope.ChildCategorylist = [];
    $scope.ReqBylist = [];
    $scope.ReqSTAlist = [];

    $scope.RptByUsrGrid = true;
    sendCheckedValsObj = [];
    $scope.tempspace = {};
    $scope.DocTypeVisible = 0;

    UtilityService.getCountires(2).then(function (response) {
        //$scope.Getcountry = angular.forEach(response.data, function (data) {
        //    data.ticked = true;
        //});
        $scope.Getcountry = response.data;
    }, function (error) {
        console.log(error);
    });

    $scope.CountryChanged = function () {
        UtilityService.getCitiesbyCny($scope.RptByUsr.selectedCountries, 2).then(function (response) {
            $scope.Citylst = response.data
        }, function (error) {
            console.log(error);
        });
    }
    $scope.CnyChangeAll = function () {
        UtilityService.getCitiesbyCny($scope.Getcountry, 2).then(function (response) {
            $scope.Citylst = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectNone = function () {
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.RptByUsr.selectedCities, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.CtyChangeAll = function () {
        UtilityService.getLocationsByCity($scope.Citylst, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.ctySelectNone = function () {
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.RptByUsr.selectedLocations, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.LCMChangeAll = function () {
        UtilityService.getTowerByLocation($scope.Locationlst, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    HDMUtilityService.getMainCategories().then(function (data) {
        $scope.maincategorylist = data.data;
    }, function (error) {
        console.log(error);
    });

    $scope.MCChange = function () {
        HDMUtilityService.getSubCatbyMainCategories($scope.RptByUsr.selectedMC).then(function (data) {
            $scope.SubCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.MCChangeAll = function () {
        HDMUtilityService.getSubCatbyMainCategories($scope.maincategorylist).then(function (data) {
            $scope.SubCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.MCSelectNone = function () {
        $scope.SubCategorylist = [];
    }

    $scope.SCChange = function () {
        HDMUtilityService.getChildCatBySubCategories($scope.RptByUsr.selectedSC).then(function (data) {
            $scope.ChildCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.SCChangeAlll = function () {
        HDMUtilityService.getChildCatBySubCategories($scope.SubCategorylist).then(function (data) {
            $scope.ChildCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.SCSelectNone = function () {
        $scope.maincategorylist = [];
    }

    HDMUtilityService.getRequestedUsers().then(function (response) {
        $scope.ReqBylist = response.data;
    }, function (error) {
        console.log(error);
    });

    HDMUtilityService.getStatus().then(function (response) {
        $scope.ReqSTAlist = response.data;
    }, function (error) {
        console.log(error);
    });

    $scope.getHDMReportByUser = function () {
        $scope.rptDateRanges();
        $scope.HDMRptByUser = {
            FromDate: $scope.RptByUsr.FromDate,
            ToDate: $scope.RptByUsr.ToDate
        };
        var fromdate = moment($scope.RptByUsr.FromDate);
        var todate = moment($scope.RptByUsr.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);

            HDMReportByUserService.getHDMReportByUser($scope.HDMRptByUser).then(function (response) {
                $scope.RptByUsrGrid = true;
                $scope.gridata = response;
                if (response == null) {
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, 'Loading...', false);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.gridOptions.api.setRowData(response);
                    progress(0, 'Loading...', false);
                }
                function bindLocChart() {
                    $scope.LocWiseBarChart($scope.HDMRptByUser);
                }
                function bindSubCatGraph() {
                    $scope.subCatGraph($scope.HDMRptByUser);
                }
                $timeout(bindLocChart, 1000);
                $timeout(bindSubCatGraph, 1000);
                console.log(response);
            }, function (error) {
                console.log(error);
            });
        }
    };

    var columnDefs = [
         { headerName: "Req ID", field: "SER_REQ_ID", width: 120, cellClass: 'grid-align', filter: 'set', template: '<a ng-click="ShowPopup(data)">{{data.SER_REQ_ID}}</a>',pinned:'left' },
         { headerName: "Requested By", field: "AUR_KNOWN_AS", width: 150, cellClass: 'grid-align', },
         { headerName: "Location", field: "LCM_NAME", width: 130, cellClass: 'grid-align', },
         { headerName: "Main Category ", field: "MNC_NAME", width: 120, cellClass: 'grid-align', },
         { headerName: "Sub Category", field: "SUBC_NAME", width: 130, cellClass: 'grid-align', },
         { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 200, cellClass: 'grid-align', },
         { headerName: "No Of Escalations", field: "NoOfEscalations", width: 150, cellClass: 'grid-align', },
         { headerName: "Status", field: "STATUS", width: 120, cellClass: 'grid-align', },
         { headerName: "SLA", field: "SLA", width: 100, cellClass: 'grid-align', },
         { headerName: "DEFINED TAT", field: "DEFINED_TAT", width: 150, cellClass: 'grid-align', },
         { headerName: "TAT", field: "TAT", width: 100, cellClass: 'grid-align', },         
         { headerName: "TAT_SLA", field: "TAT_SLA", width: 120, cellClass: 'grid-align', },
         { headerName: "DELAYED_TIME", field: "DELAYED_TIME", width: 150, cellClass: 'grid-align', },
         { headerName: "DELAYED_SLA", field: "DELAYED_SLA", width: 150, cellClass: 'grid-align', },
         { headerName: "TOTAL", field: "TOTAL", width: 110, cellClass: 'grid-align', },
    ];

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Req ID", key: "SER_REQ_ID" }, { title: "Requested By", key: "AUR_KNOWN_AS" }, { title: "Location", key: "LCM_NAME" }, { title: "Location", key: "LOCATION" }, { title: "Main Category ", key: "MNC_NAME" }, { title: "Sub Category", key: "SUBC_NAME" }, { title: "Child Category", key: "CHC_TYPE_NAME" }, { title: "NoOfEscalations", key: "NoOfEscalations" }, { title: "DEFINED TAT", key: "DEFINED_TAT" },
        { title: "TAT", key: "TAT" }, { title: "SLA", key: "SLA" }, { title: "TAT_SLA", key: "TAT_SLA" }, { title: "Delayed Time(in Minutes)", key: "DELAYED_TIME" },
        { title: "DELAYED_SLA", key: "DELAYED_SLA" }, { title: "TOTAL", key: "TOTAL" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("ReportByUser.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "ReportByUser.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function onUpdateFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }
    $("#UpdteFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    $scope.RptByUsr.selVal = "THISMONTH";

    $scope.rptDateRanges = function () {
        switch ($scope.RptByUsr.selVal) {
            case 'TODAY':
                $scope.RptByUsr.FromDate = moment().format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.RptByUsr.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break;
            case '7':
                $scope.RptByUsr.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.RptByUsr.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.RptByUsr.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.RptByUsr.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }


    function getLastMonthData(){
        $scope.getHDMReportByUser();
        //$scope.LocWiseBarChart($scope.HDMRptByUser);
        //$scope.subCatGraph($scope.HDMRptByUser);
    }

    function getLastMonthChartData() {
        $scope.LocWiseBarChart($scope.HDMRptByUser);
    }  

    $timeout(getLastMonthData, 500);
   
    var PopDefs = [
       { headerName: "Req Id", field: "SERH_SER_ID", width: 120, cellClass: 'grid-align',pinned:'left' },
       { headerName: "Updated On", field: "CREATEDON", template: '<span>{{data.CREATEDON | date:"MMM dd yyyy - HH:mm"}}</span>', width: 190, cellClass: 'grid-align' },
       { headerName: "Updated By", field: "CREATEDBY", width: 190, cellClass: 'grid-align' },
       { headerName: "Status", field: "SERH_STA_TITLE", width: 190, cellClass: 'grid-align' },
       { headerName: "Remarks", field: "SERH_COMMENTS", width: 190, cellClass: 'grid-align' },
      ];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
    }

    $scope.ShowPopup = function (data) {
        $scope.SelValue = data;
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        HDMReportByUserService.GetHistory($scope.SelValue).then(function (response) {
            $scope.popdata = response.data;
            console.log($scope.popdata);
            //$scope.PopOptions.api.setDatasource($scope.createNewDatasource($scope.popdata, $scope.ApprvlPageSize));
            $scope.PopOptions.api.setRowData($scope.popdata);
         });
    });

    $scope.GenReport = function (rptByUser, Type) {
        progress(0, 'Loading...', true);
        rptByUser.DocType = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (rptByUser.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        } else {
            $http({
                url: '../../../../api/HDMReportByUser/GetReportByUser',
                method: 'POST',
                data: rptByUser,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                //var popupwin = window.open(fileURL, "SpaceRequisitionReport", "toolbar=no,width=500,height=500");
                //setTimeout(function () {
                //    popupwin.focus();
                //}, 500);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'ReportByUser.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, 'Loading...', false);
            }).error(function (data, status, headers, config) {
            });
        };
    }

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    var chartSubCat;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Requisition Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });


    chartSubCat = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Sub Category'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Requisition Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });

    $scope.LocWiseBarChart = function (data) {
        $http({
            url: '../../../../api/HDMReportByUser/GetLocationWiseCount',
            method: 'POST',
            data: data
        }).success( function (result) {
                chart.unload();
                console.log(result);
                chart.load({ columns: result });
            });
        setTimeout(function () {
            $("#LocationGraph").append(chart.element);
        }, 700);
    }

    $scope.subCatGraph = function (spcData) {
        $http({
            url: '../../../../api/HDMReportByUser/GetSubCatWiseCount',
            method: 'POST',
            data: spcData
        }).success(function (result) {
                console.log(result);
                chartSubCat.unload();
                chartSubCat.load({ columns: result });
        });
        setTimeout(function () {
            $("#SubCatGraph").append(chartSubCat.element);
        }, 700);
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
            });
        }
    });
    $timeout($scope.getHDMReportByUser, 500);
});

