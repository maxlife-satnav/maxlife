﻿app.service("HDMAgeOpenCallsService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (parameters) {
        var deferred = $q.defer();
        //return $http.post('../../../../api/HDMAgeOpenCalls/GetHDMOpenCallsRpt', parameters)
        return $http.post(UtilityService.path + '/api/HDMAgeOpenCalls/GetHDMOpenCallsRpt', parameters)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('HDMAgeOpenCallsController', function ($scope, $q, $http, UtilityService, $timeout, HDMAgeOpenCallsService) {
    $scope.OpenCallsRpt = {};
    $scope.Type = [];
    $scope.GridVisiblity = true;

    $scope.columnDefs = [
               { headerName: "Age of Incident", field: "COL1", cellClass: 'grid-align', width: 130 },
               { headerName: "", field: "COL2", cellClass: 'grid-align', width: 110 },
               { headerName: "No of Tickets", field: "REQCOUNT", suppressMenu: true, cellClass: 'grid-align', width: 140 }],

    $scope.LoadData = function () {
        var params = {
            FromDate: $scope.OpenCallsRpt.FromDate,
            ToDate: $scope.OpenCallsRpt.ToDate,
        };
        var fromdate = moment($scope.OpenCallsRpt.FromDate);
        var todate = moment($scope.OpenCallsRpt.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            $scope.gridata = [];
            HDMAgeOpenCallsService.GetGriddata(params).then(function (response) {
                if (response.VMlist != null) {
                    $scope.gridata = response.VMlist;
                    if ($scope.gridata.length != 0) {
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        //$scope.OccupChartDetails(params);
                        $scope.OccupChartDetails(response.graphData);
                    }
                }
                progress(0, '', false);
            }, function (error) {
                console.log(error);
            });
        }
    };

    $scope.ColumnNames = [];
    $scope.opencallsdataLoad = function () {
        progress(0, 'Loading...', true);
        $scope.Pageload = {
            FromDate: $scope.OpenCallsRpt.FromDate,
            ToDate: $scope.OpenCallsRpt.ToDate,
        };
        HDMAgeOpenCallsService.GetGriddata($scope.Pageload).then(function (data) {
            $scope.gridata = data.VMlist;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            }
            $scope.OccupChartDetails(data.graphData);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
    };

    //graph binding
    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    chart = c3.generate({
        data: {
            columns: [],
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Categories'],
                height: 130,
                show: true,
            },
            y: {
                show: true,
                label: {
                    text: 'Total Requests',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    $scope.OccupChartDetails = function (spcData) {
        chart.unload({
            ids: ['OccupGraph']
        });
        chart.load({ columns: spcData });        
        setTimeout(function () {
            $("#OccupGraph").append(chart.element);
        }, 700);
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        var columns = [{ title: "Age of Incident", key: "COL3" }, { title: "No of Tickets", key: "REQCOUNT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("OpenCallsRptReport.pdf");
    }

    $scope.GenerateFilterExcel = function () {
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "OpenCallsRptReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
    }

    $scope.GenReport = function (opencallsdata, Type) {
        opencallsdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (opencallsdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/HDMAgeOpenCalls/GetOpenCallsAgeReportdata',
                //url: '../../../../api/HDMAgeOpenCalls/GetOpenCallsAgeReportdata',
                method: 'POST',
                data: opencallsdata,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'OpenCallsRptReport.' + Type;
                document.body.appendChild(a);
                a.click();
            }).error(function (data, status, headers, config) {
            });
        };
    }

    $timeout($scope.opencallsdataLoad, 500);
});