﻿app.service("HDMReqStatusconsolidatedReportService", function ($http, $q, UtilityService) {
    this.BindGrid = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMReqStatusconsolidatedReport/BindGrid')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.HDMconsolidatedChart = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMReqStatusconsolidatedReport/HDMconsolidatedChart')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});

app.controller('HDMReqStatusconsolidatedReportController', function ($scope, $q, HDMReqStatusconsolidatedReportService, UtilityService, $timeout, $http) {
    $scope.Viewstatus = 0;
    $scope.rptArea = {};
    $scope.GridVisiblity = true;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;

    $scope.BindGrid = function () {
        HDMReqStatusconsolidatedReportService.BindGrid().then(function (response) {
            progress(0, 'Loading...', true);
            $scope.RptByUsrGrid = true;
            $scope.gridata = response.data;
            if (response == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
           $scope.HDMconsolidatedChart();  
        }, function (error) {
            console.log(error);
        });
    }


    var columnDefs = [
         { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, suppressMenu: true },
         { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 1, hide: true, suppressMenu: true },
         { headerName: "Location", field: "LCM_NAME", width: 200, cellClass: 'grid-align', rowGroupIndex: 2, hide: true },
         { headerName: "Total Requests", field: "Total_Requests", width: 180, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Pending Requests", field: "Pending_Requests", width: 130, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Requests In-Progress", field: "In_Progress_Requests", width: 250, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Closed Requests", field: "Closed_Requests", width: 130, cellClass: 'grid-align', suppressMenu: true },
         { headerName: "Canceled Requests", field: "Canceled_Requests", width: 130, cellClass: 'grid-align', suppressMenu: true }
    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableCellSelection: false,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "CNY_NAME",
            cellRenderer: {
                renderer: "group"
            }
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            Total_Requests: 0,
            Pending_Requests: 0,
            In_Progress_Requests: 0,
            Closed_Requests: 0,
            Canceled_Requests: 0
        };

        rows.forEach(function (row) {
            var data = row.data;
            sums.Total_Requests += parseInt(data.Total_Requests);
            sums.Pending_Requests += parseInt(data.Pending_Requests);
            sums.In_Progress_Requests += parseInt(data.In_Progress_Requests);
            sums.Closed_Requests += parseInt(data.Closed_Requests);
            sums.Canceled_Requests += parseInt(data.Canceled_Requests);
        });
        return sums;
    }

    $scope.BindGrid();

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();

    var chart;
    chart = c3.generate({
        data: {
            columns: [],
            cache: false,
            type: 'pie',
            empty: { label: { text: "Sorry, No Data Found" } }
        },
        tooltip: {
            contents: tooltip_contents
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return (value);
                }
            }
        }
    });

    function tooltip_contents(d, defaultTitleFormat, defaultValueFormat, color) {
        var $$ = this, config = $$.config, CLASS = $$.CLASS,
            titleFormat = config.tooltip_format_title || defaultTitleFormat,
            nameFormat = config.tooltip_format_name || function (name) { return name; },
            valueFormat = config.tooltip_format_value || defaultValueFormat,
            text, i, title, value, name, bgcolor;

        for (i = 0; i < d.length; i++) {
            if (!(d[i] && (d[i].value || d[i].value === 0))) { continue; }

            if (!text) {
                text = "<table class='" + CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
            }

            name = nameFormat(d[i].name);
            value = d[i].value;
            bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

            text += "<tr class='" + CLASS.tooltipName + "-" + d[i].id + "'>";
            text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
            text += "<td class='value'>" + value + "</td>";
            text += "</tr>";
        }
        return text + "</table>";
    }

    //GetCostChartData
    $scope.HDMconsolidatedChart = function () {        
        HDMReqStatusconsolidatedReportService.HDMconsolidatedChart().then(function (response) {
            console.log(response.data);
            if (response.data != null) {
                chart.unload();
                chart.load({ columns: response.data });
                setTimeout(function () {
                    $("#HDMconsolidatedGraph").append(chart.element);
                }, 700);
            }
        }, function (error) {
            console.log(error);
        });
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "Total Requests", key: "ALLOCATED_SEATS" }, { title: "Pending Requests", key: "OCCUPIED_SEATS" }, { title: "Requests In-Progress", key: "ALLOCATED_VACANT" }, { title: "Closed Requests", key: "VACANT_SEATS" }, { title: "Canceled Requests", key: "TOTAL_SEATS" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log(jsondata);
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("HDMReqStatusconsolidatedReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDMReqStatusconsolidatedReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        console.log(Type);

        $scope.rptArea.DocType = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            progress(0, 'Loading...', true);
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/HDMReqStatusconsolidatedReport/Export_HDMconsolidatedRpt',
                method: 'POST',
                data: $scope.rptArea,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'HDMReqStatusconsolidatedReport.' + Type;
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }).error(function (data, status, headers, config) {

            });
        }
    };
});

