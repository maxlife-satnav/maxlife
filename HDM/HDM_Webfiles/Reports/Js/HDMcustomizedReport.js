﻿app.service("HDMcustomizedReportService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        //return $http.post('../../../../api/HDMcustomizedReport/GetCustomizedDetails', Customized)
        return $http.post(UtilityService.path + '/api/HDMcustomizedReport/GetCustomizedDetails', Customized)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('HDMcustomizedReportController', function ($scope, $q, $http, HDMcustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.HDMcustomized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.columnDefs = [           
    ]

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
       // rowWidth: 150,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Main Category", field: "Main_Category",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.HDMcustomized.selVal = "THISMONTH";

    $scope.rptDateRanges = function () {
        switch ($scope.HDMcustomized.selVal) {
            case 'TODAY':
                $scope.HDMcustomized.FromDate = moment().format('DD-MMM-YYYY');
                $scope.HDMcustomized.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.HDMcustomized.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.HDMcustomized.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break
            case '7':
                $scope.HDMcustomized.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.HDMcustomized.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.HDMcustomized.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.HDMcustomized.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.HDMcustomized.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.HDMcustomized.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.HDMcustomized.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.HDMcustomized.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }

    //function onFilterChanged(value) {
    //    $scope.gridOptions.api.setQuickFilter(value);
    //    if (value) {
    //        $scope.DocTypeVisible = 1
    //    }
    //    else { $scope.DocTypeVisible = 0 }
    //}
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    var clk = true;
    var ExportColumns;
    $scope.LoadData = function () {
        var unticked = _.filter($scope.Cols, function (item) {
            return item.ticked == false;
        });
        var ticked = _.filter($scope.Cols, function (item) {
            return item.ticked == true;
        });

        if ($scope.HDMcustomized.FromDate == undefined && $scope.HDMcustomized.ToDate == undefined) {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1)) + '/' + firstDay.getDate() + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1)) + '/' + lastDay.getDate() + '/' + lastDay.getFullYear();

            $scope.HDMcustomized.FromDate = firstDayWithSlashes;
            $scope.HDMcustomized.ToDate = lastDayWithSlashes;
        }
        var params = {
            Request_Type: $scope.HDMcustomizedReport.Request_Type,
            FromDate: $scope.HDMcustomized.FromDate,
            ToDate: $scope.HDMcustomized.ToDate
        };
        HDMcustomizedReportService.GetGriddata(params).then(function (response) {
            if (clk) {
                $scope.Cols = response.data.coldata;
            }
            clk = false;
            ExportColumns = response.data.lst.exportCols;
            $scope.gridOptions.api.setColumnDefs(response.data.lst.Coldef);
            $scope.gridata = response.data.lst.griddata;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];

                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        })
    }, function (error) {
        console.log(error);
    }

    $scope.CustmPageLoad = function () {
    }, function (error) {
        console.log(error);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns; //[{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("HDMcustomizationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDMcustomizationReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (HDMcustomized, Type) {
        progress(0, 'Loading...', true);
        $scope.HDMcustomized.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            if(Type == 'xls')
                JSONToCSVConvertor($scope.gridata, "Customized Report", true, "CustomizedReport");
            else // if(Type == 'pdf')
                $scope.GenerateFilterPdf();
        };
        progress(0, '', false);
    }
    $timeout($scope.LoadData, 500);
});
