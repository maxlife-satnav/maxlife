﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HDMViewModifyRequesitions.aspx.vb" Inherits="HDM_HDM_Webfiles_HDMViewModifyRequesitions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align
        {
            text-align: center;
        }

        a:hover
        {
            cursor: pointer;
        }
    </style>
    <script src="../../Scripts/aggrid/ag-grid-2.js"></script>
    <script>
        var gridLengthInitial;
        var columnDefs = [
                { headerName: "Uploaded Files", field: "Name", width: 350 },
                //{
                //    headerName: "File", width: 250, field: "Path",  cellRenderer: function (params) {
                //        var str = "../.." + params.data.Path;
                //        //return '<input type="image" src="../../UploadFiles/[Oliva_UAT].dbo/06092016023844_Chrysanthemum.jpg" alt="Submit" width="48" height="48">'
                //        var Extn = params.data.Path.substr((params.data.Path.lastIndexOf('.') + 1)).toLowerCase();
                //        var ExtArray = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];
                //        if ($.inArray(Extn, ExtArray) > -1) {
                //            return '<input type="image" src="' + str + '" alt="Submit" width="50" height="50">'
                //        }
                //        else {
                //            return params.data.Name;
                //        }
                //    }
                //},
                {
                    headerName: "Image/File Name", field: "",  cellRenderer: ghimages, suppressMenu: true, width: 250
                },

                //{ headerName: "Remove", width: 250, template: '<a href="#"  onclick="Remove(this.parentNode.parentNode)"><span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align'},
                {
                    headerName: "Download", field: "Path", width: 250, cellRenderer: function (params) {
                        return '<a download="' + params.data.UplTimeName + '" href="' + params.data.Path + '"><span class="glyphicon glyphicon-download"></span></a>';
                    }
                },
                 { headerName: "Remove", template: '<a href="#" onclick="Remove(this.parentNode.parentNode)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 150 }
        ];

        function createImageSpan(image) {
            var resultElement = document.createElement("span");
            for (var i = 0; i < 1; i++) {
                var imageElement = document.createElement("img");
                imageElement.src = "../.." + image;
                imageElement.height = 50;
                imageElement.width = 50;
                resultElement.appendChild(imageElement);
            }
            return resultElement;
        }

        function ghimages(params) {
            var Extn = params.data.Path.substr((params.data.Path.lastIndexOf('.') + 1)).toLowerCase();
            var ExtArray = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];
            if ($.inArray(Extn, ExtArray) > -1) {
                //return '<input type="image" src="' + str + '" alt="Submit" width="50" height="50">'
                return createImageSpan(params.data.Path);
            }
            else {
                return params.data.Name;
            }
            //return createImageSpan(params.data.Path);
        }

        var rowData = [];
        //var newRowData = [];

        var gridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            rowHeight: 70,          //image size in ag grid
            enableColResize: true,
            suppressHorizontalScroll: true,
            onGridReady: function sizeToFit() {
                gridOptions.api.sizeColumnsToFit();
            }
        };

        function resetselectedfiles() {
            for (i = 0; i < rowData.length; i++) {
                var selectedfiles = document.createElement("input");
                selectedfiles.setAttribute("type", "hidden");
                selectedfiles.setAttribute("name", "selectedfiles");
                selectedfiles.setAttribute("value", rowData[i].Name);
                document.getElementById("form1").appendChild(selectedfiles);
            }
        }

        function showselectedfiles(fu) {
            if (gridLengthInitial < gridOptions.rowData.length) {
                for (j = gridLengthInitial; j <= gridOptions.rowData.length; j++) {
                    rowData.splice(gridLengthInitial, 1);
                    gridOptions.api.setRowData(rowData);
                }
            }
            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
            }
            gridOptions.api.setRowData(rowData);
        }

        function Remove(node) {
            //if sts ! closed
            console.log(jQuery("#lblStatus").val);
            if ($("#lblStatus").val != "9") {
                var ndx = parseInt(node.getAttribute("row"));
                rowData.splice(ndx, 1);
                gridOptions.api.setRowData(rowData);
            }
        }
        //function Remove(node) {
        //    var ndx = parseInt(node.getAttribute("row"));
        //    rowData.splice(ndx, 1);
        //    gridOptions.api.setRowData(rowData);
        //}

        document.addEventListener("DOMContentLoaded", function () {
            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
            var jsonstringhdn = document.getElementById("jsonstringhdn");
            var jsonstring = jsonstringhdn.value;
            var jsonobj = JSON.parse(jsonstring);
            //console.log(jsonobj);
            for (i = 0; i < jsonobj.length; i++) {
                rowData.push(jsonobj[i]);
            }
            gridOptions.api.setRowData(rowData);
            gridLengthInitial = gridOptions.rowData.length;
        });
    </script>

    <style>
        #user, #dept, #id
        {
            color: deepskyblue;
        }
    </style>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View Requisition</legend>
                    </fieldset>
                    <form id="form1" class="well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i id="id" class="fa fa-tag"></i></div>

                                        <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i id="user" class="fa fa-user"></i></div>
                                        <asp:TextBox ID="txtEmp" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i id="dept" class="fa fa-bookmark"></i></div>
                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <img src="../../images/Chair_Blue.gif" />
                                        </div>
                                        <asp:TextBox ID="txtSpaceID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Location<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                        ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>
                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Main Category<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlMainCategory"
                                        ErrorMessage="Please Select Main Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>
                                    <asp:DropDownList ID="ddlMainCategory" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Sub Category<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlSubCategory"
                                        ErrorMessage="Please Select Sub Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>
                                    <asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Child Category<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator4" runat="server" Display="None" ControlToValidate="ddlChildCategory"
                                        ErrorMessage="Please Select Child Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>
                                    <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Asset Location</label>
                                    <asp:DropDownList ID="ddlAssetLoaction" runat="server" CssClass="selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Mobile Number</label>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMobile"
                                        ErrorMessage="Please Enter Mobile Number In Digits Only" Display="None" ValidationExpression="^[0-9 ]+"
                                        ValidationGroup="Val1">
                                    </asp:RegularExpressionValidator>
                                    <asp:TextBox ID="txtMobile" MaxLength="13" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Repeat Calls</label>
                                    <asp:DropDownList ID="ddlRepeatCalls" AutoPostBack="true" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Impact</label>
                                    <asp:DropDownList ID="ddlImpact" runat="server" CssClass="selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Urgency</label>
                                    <asp:DropDownList ID="ddlUrgency" runat="server" CssClass="selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Upload Images/Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                    <div class="btn btn-default">
                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                        <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" Enabled="false" onchange="showselectedfiles(this)" ClientIDMode="Static" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Problem Description<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="txtProbDesc" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine">                               
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <asp:Label ID="txtcode" runat="server" Text="Status"></asp:Label>
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" Enabled="false" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div id="ddimages" runat="server"></div>
                        </div>

                        <div class="row" id="showFeedback" runat="server">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Feedback<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select feedback"
                                        ControlToValidate="ddlFeedback" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlFeedback" CssClass="selectpicker" runat="server" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Remarks</label>
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine">                               
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <asp:Label ID="lblStatus" runat="server" Visible="false" Text="Label"></asp:Label>

                        <div class="row">
                            <div class="col-md-6">
                                <a id="lnkShowEscaltion" href="#" onclick="showPopWin()" enabled="false" runat="server">Click here to view SLA & Escalation</a>
                            </div>
                            <%-- <div>
                                <div class="col-md-6">
                                    <asp:CheckBox ID="Chkreopen" runat="server"/>
                                    <label for="Lblreopen">Check to Reopen this Ticket</label>                                    
                                </div>
                            </div>--%>

                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Button ID="btnApprove" Text="Submit Feedback" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                                        <asp:Button ID="btnModify" Text="Modify" runat="server" CssClass="btn btn-primary custom-button-color" OnClientClick="resetselectedfiles()" ValidationGroup="Val1" />
                                        <asp:Button ID="btnCancel" Text="Cancel" Visible="false" runat="server" CssClass="btn btn-primary custom-button-color" />
                                        <asp:Button ID="btnReopen" Text="Submit and Reopen" Visible="false" runat="server" CssClass="btn btn-primary custom-button-color " />
                                        <%-- <button id="btnReopen" type="button" class="btn btn-primary custom-button-color" visible="false" runat="server">
          <span class="glyphicon glyphicon-repeat"></span> Submit and Reopen
        </button>--%>
                                        <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4>Request History Details </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row table table table-responsive " style="overflow-x:hidden;">
                                    <asp:GridView ID="gvReqHistory" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:BoundField DataField="CREATEDBY" HeaderText="Requsted By" ItemStyle-Width="12px"></asp:BoundField>
                                            <asp:BoundField DataField="ASSIGNTO" HeaderText="Assigned To" ItemStyle-Width="12px"></asp:BoundField>
                                            <asp:BoundField DataField="UPDATEDDATE" HeaderText="Updated Date" ItemStyle-Width="13px"></asp:BoundField>
                                            <asp:BoundField DataField="UPDATEDBY" HeaderText="Updated By" ItemStyle-Width="13px"></asp:BoundField>
                                            <asp:BoundField DataField="REMARKS" HeaderText="Remarks"  ItemStyle-Width="13px"></asp:BoundField>                                            
                                            <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-Width="10px"></asp:BoundField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <asp:HiddenField ID="jsonstringhdn" runat="server" />
                        <div class="row">
                            <div class="col-md-12">
                                <div id="myGrid" style="height: 232px; width: 520px;" visible="false" class="ag-blue"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View SLA & Escalation Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="450px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin() {
            $("#modalcontentframe").attr("src", "frmSLADetails.aspx?LOC_CODE=" + document.getElementById('ddlLocation').value + "&MAIN_CAT_CODE=" + document.getElementById('ddlMainCategory').value
                + "&SUB_CAT_CODE=" + document.getElementById('ddlSubCategory').value + "&CHILD_CAT_CODE=" + document.getElementById('ddlChildCategory').value);
            $("#myModal").modal().fadeIn();
            return false;
        }
    </script>
</body>
</html>


