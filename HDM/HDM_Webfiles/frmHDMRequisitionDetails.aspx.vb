﻿#Region "NameSpaces"

Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

#End Region

Partial Class HDM_HDM_Webfiles_frmHDMRequisitionDetails
    Inherits System.Web.UI.Page

#Region "Variable Declarations"

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

#End Region

#Region "User Defined Methods"

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function

    Private Sub BindRequisition()
        'If txtconvdate.Text <> "" Then
        '    If CDate(txtconvdate.Text) < getoffsetdate(Date.Today) Then
        '        lblMsg.Visible = True
        '        lblMsg.Text = "Convenient date should be greater than or equal to  Todays Date"
        '        Exit Sub
        '    End If
        'End If
        'lblMsg.Text = ""
        Dim ReqId As String = Request("RID")
        Dim RaisedBy As Integer = 0
        If String.IsNullOrEmpty(ReqId) Then
            ' lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            'sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()

            Dim UID As String = ds.Tables(0).Rows(0).Item("SER_UP_BY")
            BindUsers(UID)

            lblReqId.Text = ReqId

            BindLocations()
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_BDG_ID")).Selected = True

            BindCategories(ddlLocation.SelectedItem.Value)
            ddlServiceCategory.ClearSelection()
            ddlServiceCategory.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_TYPE")).Selected = True

            BindServiceType(ddlLocation.SelectedItem.Value, ddlServiceCategory.SelectedItem.Value)
            ddlServiceType.ClearSelection()
            ddlServiceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_REQ_FOR")).Selected = True
            txtRemarks.Text = ds.Tables(0).Rows(0).Item("SER_DESCRIPTION")
            txtconvdate.Text = ds.Tables(0).Rows(0).Item("CONVINIENT_DATE")
            txtEmployee.Text = ds.Tables(0).Rows(0).Item("SER_AUR_ID")
            txtServReqAt.Text = ds.Tables(0).Rows(0).Item("SER_REQUIRED_AT")
            cboHr.ClearSelection()
            cboHr.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONV_HOURS")).Selected = True

            ddlMins.ClearSelection()
            ddlMins.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONV_MINS")).Selected = True
            Dim Status As Integer
            Status = ds.Tables(0).Rows(0).Item("SER_STATUS")
            If Status > 2 Then
                btnCancel.Visible = False
                btnModify.Visible = False
            Else
                btnCancel.Visible = True
                ddlServiceCategory.Enabled = True
                ddlServiceType.Enabled = True
                txtServReqAt.Enabled = True
                cboHr.Enabled = True
                txtRemarks.Enabled = True
                txtconvdate.Enabled = True
                btnModify.Visible = True
            End If

        End If
    End Sub

    Private Sub BindUsers(ByVal AUR_ID As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = AUR_ID
        ObjSubsonic.Binddropdown(ddlEmp, "HDM_BINDUSERS_NAME", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(AUR_ID)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub

    Private Sub BindCategories(lcm_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GETACTIVESERREQUESTTYPE")
        'sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        ddlServiceCategory.DataSource = sp.GetDataSet()
        ddlServiceCategory.DataTextField = "SER_NAME"
        ddlServiceCategory.DataValueField = "SER_CODE"
        ddlServiceCategory.DataBind()
        ddlServiceCategory.Items.Insert(0, "--Select--")

    End Sub

    Private Sub BindServiceType(lcm_code As String, ser_cat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_SERTYPEBYCATLOCATION")
        'sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@SER_CODE", ddlServiceCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        sp.Command.AddParameter("@SER_CODE", ser_cat, DbType.String)
        ddlServiceType.DataSource = sp.GetDataSet()
        ddlServiceType.DataTextField = "SER_TYPE_NAME"
        ddlServiceType.DataValueField = "SER_TYPE_CODE"
        ddlServiceType.DataBind()
        ddlServiceType.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")

    End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Private Sub DeleteRequistionItems(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_DeleteByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As Integer, ByVal Qty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_update")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Private Sub BindTimings()
        If ddlLocation.SelectedIndex > 0 And ddlServiceType.SelectedIndex > 0 And ddlServiceCategory.SelectedIndex > 0 And txtconvdate.Text <> "" Then
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDMGET_CMS_TIMINGS")
            sp1.Command.AddParameter("@SER_TYPE_LCMID", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_TYPE_CODE", ddlServiceType.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_TYPE_CATID", ddlServiceCategory.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@date", txtconvdate.Text, DbType.Date)
            cboHr.DataSource = sp1.GetDataSet()
            cboHr.DataTextField = "TIMINGS"
            cboHr.DataValueField = "VALUE"
            cboHr.DataBind()
            cboHr.Items.Insert(0, New ListItem("--HH--", "--HH--"))
        End If
    End Sub

#End Region

#Region "Page Event Handlers"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
            txtconvdate.Attributes.Add("readonly", "readonly")
        Else
            If Not IsPostBack Then
                BindLocations()
                BindRequisition()
            End If
        End If
        txtconvdate.Attributes.Add("onClick", "displayDatePicker('" + txtconvdate.ClientID + "')")
        txtconvdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'Response.Write(getoffsetdatetime(DateTime.Now).ToString("HH:mm:ss tt"))
        'Response.Write(getoffsetdatetime(DateTime.Now).ToString("HH"))
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmHDMViewRequisitions.aspx")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindCategories(ddlLocation.SelectedItem.Value)
    End Sub

    Protected Sub ddlServiceCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceCategory.SelectedIndexChanged
        BindServiceType(ddlLocation.SelectedItem.Value, ddlServiceCategory.SelectedItem.Value)
    End Sub

    Protected Sub ddlServiceType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceType.SelectedIndexChanged
        'BindTimings()
    End Sub

    Protected Sub cboHr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboHr.SelectedIndexChanged
        If cboHr.SelectedIndex = 0 Then
            ddlMins.Enabled = False
            ddlMins.SelectedIndex = 0
        Else
            ddlMins.Enabled = True
            ddlMins.SelectedIndex = 1
        End If
    End Sub

    Protected Sub ddlMins_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMins.SelectedIndexChanged
        If ddlMins.SelectedIndex = 0 Then
            cboHr.SelectedIndex = 0
            ddlMins.Enabled = False
        End If
    End Sub

    'Protected Sub btnreload_Click(sender As Object, e As EventArgs) Handles btnReload.Click
    '    'BindTimings()
    '    ddlMins.Enabled = False
    '    ddlMins.SelectedIndex = 0
    'End Sub

    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        If Not IsValid() Then
            Exit Sub
        End If
        If txtconvdate.Text <> "" Then
            If CDate(txtconvdate.Text) < getoffsetdate(Date.Today) Then
                lblMsg.Visible = True
                lblMsg.Text = "Convenient date should be greater than or equal to  Todays Date"
                Exit Sub
            End If
        End If
        'If CInt(cboHr.SelectedItem.Value) < CInt(getoffsetdatetime(DateTime.Now).ToString("HH")) And CDate(txtconvdate.Text) = getoffsetdate(Date.Today) Then
        '    lblMsg.Visible = True
        '    lblMsg.Text = "Convenient hour should be greater than current hour..."
        '    Exit Sub

        'End If

        If cboHr.SelectedItem.Text <> "--HH--" Then
            Dim ConvTime1 As String
            Dim ConvTimet As String
            ConvTime1 = cboHr.SelectedItem.Text + ddlMins.SelectedItem.Text
            ConvTimet = CInt(ConvTime1)
            Dim curtime As String = DateTime.Now.ToString("HHmm")
            If txtconvdate.Text = getoffsetdatetime(DateTime.Now).Date Then
                If CInt(ConvTimet) < CInt(curtime) Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Selected time should be greater than current time"
                    Exit Sub
                End If
                '
                'CommonModules.PopUpMessage(curtime, Page)
            End If
        End If

        Dim Holidaydate As String
        Holidaydate = txtconvdate.Text
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Check_holiday")
        sp2.Command.AddParameter("@HOLIDAY_DATE", Holidaydate, DbType.String)
        Dim flag As Integer = sp2.ExecuteScalar()

        If flag = 1 Then
            lblMsg.Text = "The selected date is Holiday, so please select another date."
            lblMsg.Visible = True
            Exit Sub
        End If

        lblMsg.Text = ""
        'Dim dstime1 As DataSet
        'dstime1 = GetServiceStart()

        'If dstime1.Tables(0).Rows.Count > 0 Then
        '    If cboHr.SelectedItem.Value <> dstime1.Tables(0).Rows(0).Item("VALUE") Then
        '        lblMsg.Text = "You can't raise request for before time, Please select the current Hour... "
        '        Exit Sub
        '    End If

        'Else
        '    cboHr.Items.Clear()
        '    ddlMins.Items.Clear()
        '    cboHr.Items.Insert(0, "--HH--")
        '    ddlMins.Items.Insert(0, "--MIN--")
        '    lblMsg.Text = "Please select another date...  "
        '    Exit Sub
        'End If
        'Dim ReqId As String = Request("RID")
        Dim ConvTime As String
        ConvTime = cboHr.SelectedItem.Value + ":" + ddlMins.SelectedItem.Value
        Dim count As Integer = 0

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_MODIFY_REQUEST_BY_REQID")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, DbType.String)
        sp.Command.AddParameter("@AUR_TYPE", "", DbType.String)
        sp.Command.AddParameter("@AUR_ID", txtEmployee.Text, DbType.String)
        sp.Command.AddParameter("@SER_TYPE", ddlServiceType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@Name", "", DbType.String)
        sp.Command.AddParameter("@Email", "", DbType.String)
        sp.Command.AddParameter("@Contact", "", DbType.String)
        sp.Command.AddParameter("@SER_BDG_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_RAISED_FOR", ddlServiceType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_TYPE", ddlServiceCategory.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@SER_SPC_ID", "", DbType.String)
        sp.Command.AddParameter("@DESCRIPTION", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@MOBILE", "", DbType.String)
        sp.Command.AddParameter("@RESPONSE_BY", "", DbType.String)
        sp.Command.AddParameter("@RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ", "", DbType.String)
        sp.Command.AddParameter("@SMS", "", DbType.String)
        sp.Command.AddParameter("@AUR_EMAIL", "", DbType.String)
        sp.Command.AddParameter("@SER_REQUIRED_AT", txtServReqAt.Text, DbType.String)

        If txtconvdate.Text = "" Then
            sp.Command.AddParameter("@CONV_DATE", DBNull.Value, DbType.DateTime)
        Else
            sp.Command.AddParameter("@CONV_DATE", txtconvdate.Text, DbType.DateTime)
        End If
        sp.Command.AddParameter("@CONV_TIME", ConvTime, DbType.String)
        sp.Command.AddParameter("@CONV_TIME1", ConvTime, DbType.String)
        sp.Command.AddParameter("@CONREMARKS", "NA", DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("frmhelpthanks.aspx?id=2&Reqid=" & lblReqId.Text)
    End Sub

    Public Function GetServiceStart() As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_SERSTARTTIME")
        sp.Command.AddParameter("@SER_TYPE_LCMID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_TYPE_CODE", ddlServiceType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_TYPE_CATID", ddlServiceCategory.SelectedItem.Value, DbType.String)
        Dim dsSTime As New DataSet()
        dsSTime = sp.GetDataSet()
        Return dsSTime
    End Function


    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_CANCEL_REQUEST_BY_REQID")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("frmhelpthanks.aspx?id=3&Reqid=" & lblReqId.Text)
    End Sub


#End Region

End Class
