﻿<%@ Page Title="View Requisitions" Language="VB" AutoEventWireup="false" CodeFile="frmHDMViewRequisitions.aspx.vb"
    Inherits="HDM_HDM_Webfiles_frmHDMViewRequisitions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script lang="javascript" type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View Requisitions
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row" style="padding-left: 25px">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtid">Enter Request Id </label>
                                            <asp:TextBox ID="txtReqId" runat="server" TabIndex="1" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtloc">Location</label>
                                            <asp:DropDownList ID="ddlLocation" runat="server" TabIndex="2" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtcat">Child Category</label>
                                            <asp:DropDownList ID="ddlChildCategory" runat="server" TabIndex="3" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 ">
                                </div>
                            </div>

                            <div class="row" style="padding-left: 25px">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtid">Enter From Date</label>
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtfromDt" runat="server" TabIndex="4" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtloc">Enter To Date</label>
                                              <div class='input-group date' id='toDt'>
                                                <asp:TextBox ID="txtToDt" runat="server" TabIndex="5" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txtcat">Select Status</label>
                                            <asp:DropDownList ID="ddlStatus" runat="server" TabIndex="6" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 ">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" TabIndex="7" runat="server" Text="Search" ValidationGroup="Val1"
                                                CausesValidation="true" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <div class="row table table table-condensed table-responsive">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                    EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Request Id">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HDM/HDM_Webfiles/HDMViewModifyRequesitions.aspx?RID={0}")%>'>
                                                                    <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                                     
                                                        <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="LOCATION" HeaderText="Location" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="REQ_STATUS" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="ESC_COUNT" HeaderText="No.Of Escalations" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken (HOURS)" ItemStyle-HorizontalAlign="left" />
                                                    </Columns>
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


