﻿<%@ Page Title="Admin View Requisitions" Language="VB" AutoEventWireup="false" CodeFile="frmHDMAdminViewRequisitions.aspx.vb"
    Inherits="HDM_HDM_Webfiles_frmHDMAdminViewRequisitions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
           function setup(id) {
           $('#' + id).datepicker({
               format: 'mm/dd/yyyy',
               autoclose: true
           });
       };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View & Assign
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">                       
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="padding-left: 25px">
                            <div id="showDiv" runat="server">
                                <div class="row">

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="txtid">Enter Request Id </label>                                              
                                                <asp:TextBox ID="txtReqId" runat="server" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="txtloc">Location</label>
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="txtcat">Child Category</label>
                                                <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="txtid">Enter From Date</label>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="txtloc">Enter To Date</label>
                                                <div class='input-group date' id='toDt'>
                                                    <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="txtcat">Select Status</label>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 ">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <br />
                                               <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                    CausesValidation="true" TabIndex="2" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row table table table-condensed table-responsive">
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped" OnRowDataBound="OnRowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Request Id">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HDM/HDM_Webfiles/frmHDMViewAssignRequisition.aspx?RID={0}")%>'>
                                                        <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                          
                                            <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="LOCATION" HeaderText="Location" ItemStyle-HorizontalAlign="left" />

                                            <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="STATUS" HeaderText="Current Status" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left" />
                                            <asp:TemplateField HeaderText="Re-Assign">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlAssignTo" runat="server" CssClass="selectpicker" Width="70px">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                            
                                               <asp:TemplateField HeaderText="Remarks">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtUpdateRemarks" runat="server" TextMode="MultiLine" />
                                                                        </ItemTemplate>
                                            </asp:TemplateField>                                            
                                            <asp:BoundField DataField="ESC_COUNT" HeaderText="No. Of Escalations" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left" />                                          
                                            <asp:TemplateField HeaderText="Location" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("LCM_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Main" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMain" runat="server" Text='<%# Eval("MNC_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sub" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSub" runat="server" Text='<%# Eval("SUBC_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Child" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblChild" runat="server" Text='<%# Eval("CHC_TYPE_CODE")%>'></asp:Label>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>                                          
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="txtcode" class="col-md-5 control-label">Remarks</label>
                                    <div class="col-md-7">                                      
                                        <asp:TextBox ID="txtUpdateRemarks" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine">                               
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div id="lbls" runat="server">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" runat="server" Text="Bulk Assign"
                                        CausesValidation="true" TabIndex="17" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


