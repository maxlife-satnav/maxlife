﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports clsSubSonicCommonFunctions
Partial Class HDM_HDM_Webfiles_frmRMViewRequisitions
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            BindUnassignedGrid()
            BindLocations()
            BindChildCategory()
            FillStatus()
            If gvViewRequisitions.Rows.Count = 0 Then
                showDiv.Visible = False
                btnUpdate1.Visible = False
            Else
                showDiv.Visible = True
                btnUpdate1.Visible = True
            End If
            If gvUnassigned.Rows.Count = 0 Then
                showDiv2.Visible = False
                btnUpdate2.Visible = False
            Else
                showDiv2.Visible = True
                btnUpdate2.Visible = True
            End If
            If Request.QueryString("Updated") = 1 Then
                lblMessage.Text = "Request Updated Successfully"
            Else
                lblMessage.Text = ""
            End If
        End If
    End Sub

    Private Sub FillStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_HDMSTATUS_FILTERS")
        sp.Command.AddParameter("@TYPE", 2, Data.DbType.String)
        ddlStatus.DataSource = sp.GetDataSet
        ddlStatus.DataTextField = "STA_TITLE"
        ddlStatus.DataValueField = "STA_ID"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindGrid()
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_PENDING_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        ds = sp.GetDataSet
        Session("MYReq") = ds
        gvViewRequisitions.DataSource = ds
        gvViewRequisitions.DataBind()
        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)

        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
            Dim ddlstat As DropDownList = CType(gvViewRequisitions.Rows(i).FindControl("ddlModifyStatus"), DropDownList)
            ddlstat.DataSource = ds1
            ddlstat.DataTextField = "STA_TITLE"
            ddlstat.DataValueField = "STA_ID"
            ddlstat.DataBind()
            ddlstat.ClearSelection()
        Next
    End Sub

    Private Sub BindUnassignedGrid()
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_PENDING_UNASSIGNED_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        ds = sp.GetDataSet
        Session("UnAssReq") = ds
        gvUnassigned.DataSource = ds
        gvUnassigned.DataBind()
        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        'ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_BIND_STATUS")
        For i As Integer = 0 To gvUnassigned.Rows.Count - 1
            Dim ddlAssignStatus As DropDownList = CType(gvUnassigned.Rows(i).FindControl("ddlAssignStatus"), DropDownList)
            ddlAssignStatus.DataSource = ds1
            ddlAssignStatus.DataTextField = "STA_TITLE"
            ddlAssignStatus.DataValueField = "STA_ID"
            ddlAssignStatus.DataBind()
            ddlAssignStatus.ClearSelection()
        Next
    End Sub

    Protected Sub gvViewRequisitions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex()
        gvViewRequisitions.DataSource = Session("MYReq")
        gvViewRequisitions.DataBind()

        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)

        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
            Dim ddlstat As DropDownList = CType(gvViewRequisitions.Rows(i).FindControl("ddlModifyStatus"), DropDownList)
            ddlstat.DataSource = ds1
            ddlstat.DataTextField = "STA_TITLE"
            ddlstat.DataValueField = "STA_ID"
            ddlstat.DataBind()
            ddlstat.ClearSelection()
        Next
    End Sub

    Protected Sub btnUpdate1_Click(sender As Object, e As EventArgs) Handles btnUpdate1.Click
        ' Dim reqids As String = String.Empty
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvViewRequisitions.Rows
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblreqid"), Label)
                Dim txtUpdateRemarks As TextBox = DirectCast(row.FindControl("txtUpdateRemarks"), TextBox)
                Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlModifyStatus"), DropDownList)
                Dim txtAC As TextBox = DirectCast(row.FindControl("txtADC"), TextBox)
                Dim txtLBC As TextBox = DirectCast(row.FindControl("txtLBC"), TextBox)
                Dim txtSC As TextBox = DirectCast(row.FindControl("txtSPC"), TextBox)
                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(6) {}
                    param(0) = New SqlParameter("@REQ_ID", lblreqid.Text)
                    param(1) = New SqlParameter("@STA_ID", ddlStatus.SelectedValue)
                    param(2) = New SqlParameter("@UPDATE_REMARKS", txtUpdateRemarks.Text)
                    param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                    param(4) = New SqlParameter("@ADDN_COST", txtAC.Text)
                    param(5) = New SqlParameter("@LBR_COST", txtLBC.Text)
                    param(6) = New SqlParameter("@SPR_COST", txtSC.Text)
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQ_UPDATE", param)
                End If
            Next
            lblMessage.Text = "Request Updated Successfully"
            BindGrid()
        Catch ex As Exception

        End Try
        lblMessage.Visible = True
        lblMessage.Text = "Request Updated Successfully"
    End Sub

    Protected Sub btnUpdate2_Click(sender As Object, e As EventArgs) Handles btnUpdate2.Click
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvUnassigned.Rows
                Dim chkSelect1 As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblreqid"), Label)
                Dim txtUpdate2Remarks As TextBox = DirectCast(row.FindControl("txtUpdate2Remarks"), TextBox)
                Dim ddlStatus2 As DropDownList = DirectCast(row.FindControl("ddlAssignStatus"), DropDownList)

                Dim txtUpdateRemarks As TextBox = DirectCast(row.FindControl("txtUpdateRemarks"), TextBox)
                Dim txtAC As TextBox = DirectCast(row.FindControl("txtADC"), TextBox)
                Dim txtLBC As TextBox = DirectCast(row.FindControl("txtLBC"), TextBox)
                Dim txtSC As TextBox = DirectCast(row.FindControl("txtSPC"), TextBox)

                If chkSelect1.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(6) {}
                    param(0) = New SqlParameter("@REQ_ID", lblreqid.Text)
                    param(1) = New SqlParameter("@STA_ID", ddlStatus2.SelectedValue)
                    param(2) = New SqlParameter("@UPDATE_REMARKS", txtUpdate2Remarks.Text)
                    param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                    param(4) = New SqlParameter("@ADDN_COST", txtAC.Text)
                    param(5) = New SqlParameter("@LBR_COST", txtLBC.Text)
                    param(6) = New SqlParameter("@SPR_COST", txtSC.Text)
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_UPDATE_UNASSIGNED_REQUESTS", param)
                End If
            Next
            lblMessage.Visible = True
            lblMessage.Text = "Request Updated Successfully"
        Catch ex As Exception

        End Try
        BindUnassignedGrid()
    End Sub

    Public Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
        ObjSubsonic.Binddropdown(ddlLocaton1, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
        ObjSubsonic.Binddropdown(ddlChildCategroy1, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub

    Protected Sub txtReqIdFilter_Click(sender As Object, e As EventArgs) Handles txtReqIdFilter.Click
        Dim DS As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_PENDING_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@REQID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@LOCATION", ddlLocaton1.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@CATEGORY", ddlChildCategroy1.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String)
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        DS = sp.GetDataSet
        Session("MYReq") = DS
        gvViewRequisitions.DataSource = DS
        gvViewRequisitions.DataBind()

        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
            Dim ddlstat As DropDownList = CType(gvViewRequisitions.Rows(i).FindControl("ddlModifyStatus"), DropDownList)
            ddlstat.DataSource = ds1
            ddlstat.DataTextField = "STA_TITLE"
            ddlstat.DataValueField = "STA_ID"
            ddlstat.DataBind()
            ddlstat.ClearSelection()
        Next
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim DS As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_PENDING_UNASSIGNED_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@REQID", textReqID2.Text, Data.DbType.String)
        sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtUAfromDt.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtUAtoDt.Text, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        DS = sp.GetDataSet
        Session("UnAssReq") = DS
        gvUnassigned.DataSource = DS
        gvUnassigned.DataBind()

        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvUnassigned.Rows.Count - 1
            Dim ddlAssignStatus As DropDownList = CType(gvUnassigned.Rows(i).FindControl("ddlAssignStatus"), DropDownList)
            ddlAssignStatus.DataSource = ds1
            ddlAssignStatus.DataTextField = "STA_TITLE"
            ddlAssignStatus.DataValueField = "STA_ID"
            ddlAssignStatus.DataBind()
            ddlAssignStatus.ClearSelection()
        Next
    End Sub

    Protected Sub gvUnassigned_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvUnassigned.PageIndexChanging
        gvUnassigned.PageIndex = e.NewPageIndex()
        gvUnassigned.DataSource = Session("UnAssReq")
        gvUnassigned.DataBind()

        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvUnassigned.Rows.Count - 1
            Dim ddlAssignStatus As DropDownList = CType(gvUnassigned.Rows(i).FindControl("ddlAssignStatus"), DropDownList)
            ddlAssignStatus.DataSource = ds1
            ddlAssignStatus.DataTextField = "STA_TITLE"
            ddlAssignStatus.DataValueField = "STA_ID"
            ddlAssignStatus.DataBind()
            ddlAssignStatus.ClearSelection()
        Next
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            Dim ddl_status As DropDownList = DirectCast(sender, DropDownList)
            Dim row As GridViewRow = DirectCast(ddl_status.Parent.Parent, GridViewRow)
            Dim txtlbcost As TextBox = DirectCast(row.FindControl("txtLBC"), TextBox)
            Dim txtspcost As TextBox = DirectCast(row.FindControl("txtSPC"), TextBox)
            Dim txtadncost As TextBox = DirectCast(row.FindControl("txtADC"), TextBox)

            Dim lbllbcost As Label = DirectCast(row.FindControl("lblLBC"), Label)
            Dim lblspcost As Label = DirectCast(row.FindControl("lblSPC"), Label)
            Dim lbladncost As Label = DirectCast(row.FindControl("lblADC"), Label)

            If ddl_status.SelectedValue = "9" Then
                txtlbcost.Visible = True
                txtspcost.Visible = True
                txtadncost.Visible = True
                lbllbcost.Visible = False
                lblspcost.Visible = False
                lbladncost.Visible = False
            Else
                txtlbcost.Visible = False
                txtspcost.Visible = False
                txtadncost.Visible = False
                lbllbcost.Visible = True
                lblspcost.Visible = True
                lbladncost.Visible = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    '=======================================================
    'Service provided by Telerik (www.telerik.com)
    'Conversion powered by NRefactory.
    'Twitter: @telerik
    'Facebook: facebook.com/telerik
    '=======================================================

End Class
