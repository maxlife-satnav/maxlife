﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmRMViewRequisitions.aspx.vb"
    Inherits="HDM_HDM_Webfiles_frmRMViewRequisitions" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../Dashboard/CSS/ProfileStyle.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        #ddlAssignStatus1
        {
            height: 600px;
            max-height: 600px;
            overflow-y: scroll;
        }

        #ddlAssignStatus
        {
            height: auto;
            max-height: 200px;
            overflow-y: scroll;
        }
    </style>
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }
         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvViewRequisitions.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }

        function validateCheckBoxesUnA() {
            var gridView = document.getElementById("<%=gvUnassigned.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View & Update
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="accordion">
                                    <div visible="true">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">My Requisitions</h4>
                                        </div>
                                        <br />
                                        <div style="padding-left: 25px">
                                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                            <div id="showDiv" runat="server">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtid">Enter Request Id </label>
                                                                <asp:TextBox ID="txtReqId" runat="server" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtloc">Location</label>
                                                                <asp:DropDownList ID="ddlLocaton1" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtcat">Child Category</label>
                                                                <asp:DropDownList ID="ddlChildCategroy1" runat="server" CssClass="selectpicker" data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtid">Enter From Date</label>
                                                                <div class='input-group date' id='fromdate'>
                                                                    <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtloc">Enter To Date</label>
                                                                <div class='input-group date' id='toDt'>
                                                                    <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtcat">Select Status</label>
                                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 ">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <br />
                                                                <asp:Button ID="txtReqIdFilter" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                                    CausesValidation="true" TabIndex="2" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row table table table-responsive ">
                                                            <asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                                EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Request Id">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HDM/HDM_Webfiles/frmHDMRMRequisitionDetails.aspx?RID={0}")%>'>
                                                                                <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                            </asp:HyperLink>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="LOCATION" HeaderText="Location Name" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Main Category" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REQ_STATUS" HeaderText="Current Status" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:TemplateField HeaderText="Update Status">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlModifyStatus" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="REMARKS" HeaderText="Current Remarks" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                     <asp:TemplateField HeaderText="Update Remarks">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtUpdateRemarks" runat="server" TextMode="MultiLine" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                                   
                                                                    <asp:BoundField DataField="ESC_COUNT" HeaderText="No. Of Escalations" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>                                                                    
                                                                     <asp:BoundField DataField="IMP_NAME" HeaderText="Impact" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="UGC_NAME" HeaderText="Urgency" ItemStyle-HorizontalAlign="left">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="Labour Cost">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="revTR" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtLBC"
                                                                                ErrorMessage="Please Enter Labour cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtLBC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtLBC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtLBC">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblLBC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Spare Cost">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="rfvSC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtSPC"
                                                                                ErrorMessage="Please Enter Spare cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtSPC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtSPC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtSPC">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblSPC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Additional Cost">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="revAC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtADC"
                                                                                ErrorMessage="Please Enter Additional cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtADC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtADC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtADC">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblADC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            Select All
                                                                        <input id="chkSelect1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="txtReqIdFilter" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <asp:Button ID="btnUpdate1" runat="server" Text="Update" CssClass="btn btn-primary custom-button-color"
                                                    OnClientClick="javascript:return validateCheckBoxesMyReq();" ValidationGroup="Val2" />                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr style="border-color: black" />
                        <div class="row">
                            <div class="col-md-12">
                                <div id="accordion1">
                                    <div runat="server" id="divhd" visible="true">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                            UnAssigned Requisitions   </h3>                      
                                        </div>
                                        <br />
                                        <div style="padding-left: 25px">
                                            <div id="showDiv2" runat="server">                                                
                                                <div class="row">

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtid">Enter Request Id </label>                                                                
                                                                <asp:TextBox ID="textReqID2" runat="server" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtloc">Location</label>
                                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtcat">Child Category</label>
                                                                <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="selectpicker" data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtUAid">Enter From Date</label>
                                                                <div class='input-group date' id='UAfromDt'>
                                                                    <asp:TextBox ID="txtUAfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('UAfromDt')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="txtUAtodt">Enter To Date</label>
                                                                <div class='input-group date' id='UAtoDt'>
                                                                    <asp:TextBox ID="txtUAtoDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('UAtoDt')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <br />
                                                                <asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                                    CausesValidation="true" TabIndex="2" />                                                              
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 ">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row table table table-responsive">
                                                            <asp:GridView ID="gvUnassigned" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                                EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Request Id" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>                                                                           
                                                                            <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="LOCATION" HeaderText="Location Name" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="REQ_STATUS" HeaderText="Current Status" ItemStyle-HorizontalAlign="left" />
                                                                     <asp:TemplateField HeaderText="Update Status">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlAssignStatus" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" runat="server">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:BoundField DataField="REMARKS" HeaderText="Current Remarks" ItemStyle-HorizontalAlign="left" />
                                                                      <asp:TemplateField HeaderText="Update Remarks">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtUpdate2Remarks" runat="server" TextMode="MultiLine" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>                                                                     
                                                                    <asp:BoundField DataField="ESC_COUNT" HeaderText="No. Of Escalations" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="IMP_NAME" HeaderText="Impact" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="UGC_NAME" HeaderText="Urgency" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:TemplateField HeaderText="Labour Cost">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="revTR" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtLBC"
                                                                                ErrorMessage="Please Enter Labour cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtLBC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtLBC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtLBC">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblLBC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Spare Cost">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="rfvSC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtSPC"
                                                                                ErrorMessage="Please Enter Spare cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtSPC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtSPC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtSPC">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblSPC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Additional Cost">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="revAC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtADC"
                                                                                ErrorMessage="Please Enter Additional cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtADC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtADC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtADC">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblADC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            Select All
                                                                        <input id="chkSelect2" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkSelect1', this.checked)">
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect1" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <div class="form-group">
                                                    <asp:Button ID="btnUpdate2" runat="server" Text="Update" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesUnA();" />                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                  </form>
                        </div>          
                </div>
            </div>
        </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


