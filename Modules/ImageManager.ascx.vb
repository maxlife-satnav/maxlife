#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

''' <summary>
''' Summary description for ImageManager
''' </summary>
Public Partial Class ImageManager
	Inherits System.Web.UI.UserControl
	Public Sub New()


	End Sub
	Private _imageFolder As String = "images/productimages"

	Public Property ImageFolder() As String
		Get
			Return _imageFolder
		End Get
		Set
			_imageFolder = Value
		End Set
	End Property

	Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	End Sub
	Public Function GetSelectedImage() As String
		Dim imgName As String = Request.Form(ClientID & "_imgHolder").ToString()
		Return imgName
		'return System.IO.Path.GetFileName(imgName);
	End Function
	Public Sub SetImage(ByVal imageUrl As String)
		If (Not String.IsNullOrEmpty(imageUrl)) Then
			imgPic.ImageUrl = imageUrl
		Else
			imgPic.ImageUrl = Page.ResolveUrl("~/images/1pix.gif")
		End If
	End Sub
	Protected Function GetImage() As String
		Dim sImage As String = imgPic.ImageUrl
		sImage = System.IO.Path.GetFileName(sImage)
		Return sImage

	End Function
End Class
