#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Public Partial Class ResultMessage
	Inherits System.Web.UI.UserControl
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	End Sub
	Public Sub ShowSuccess(ByVal message As String)
		tblResult.Visible = True
		trSuccess.Visible = True
		trFail.Visible = False
				lblSuccess.Text = message & " - " & DateTime.UtcNow.ToString()
	End Sub
	Public Sub ShowFail(ByVal message As String)
		tblResult.Visible = True
		trSuccess.Visible = False
		trFail.Visible = True
		lblFail.Text = message & " - " & DateTime.UtcNow.ToString()

	End Sub
	Protected Function GetPath() As String
		Dim sPath As String = Request.ApplicationPath
		If sPath = "/" Then
			sPath = ""
		End If
		Return sPath
	End Function
End Class
