#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Public Partial Class Modules_Admin_ProductCategories
	Inherits System.Web.UI.UserControl
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	End Sub
	Private catLevel As Integer = 0
	Private Sub BuildCategoryList(ByVal ds As DataSet)

		ds.Relations.Add("NodeRelation", ds.Tables(0).Columns("categoryID"), ds.Tables(0).Columns("parentID"), False)

		For Each dbRow As DataRow In ds.Tables(0).Rows
					If Integer.Parse(dbRow("parentID").ToString()) = 0 Then
				catLevel = 0
				ddlCats.Items.Add(New ListItem(dbRow("categoryName").ToString(), dbRow("categoryID").ToString()))
				PopulateSubTree(dbRow)
					End If
		Next dbRow

	End Sub
	Private Sub PopulateSubTree(ByVal dbRow As DataRow)
		catLevel += 1
		For Each childRow As DataRow In dbRow.GetChildRows("NodeRelation")
			ddlCats.Items.Add(New ListItem("---" & childRow("categoryName").ToString(), childRow("categoryID").ToString()))
			PopulateSubTree(childRow)
		Next childRow
	End Sub
	Public Sub LoadCategories(ByVal productID As Integer)
		lblID.Text = productID.ToString()
		Dim ds As DataSet = CategoryController.GetDataSetList()

		BuildCategoryList(ds)
		LoadCatList()

	End Sub
	Private Sub LoadCatList()
		dgCats.DataSource = CategoryController.GetByProductID(Integer.Parse(lblID.Text))
		dgCats.DataBind()
	End Sub

	Protected Sub DeleteCat(ByVal source As Object, ByVal e As DataGridCommandEventArgs)
		Dim sCatID As String = e.Item.Cells(0).Text
		ProductController.RemoveFromCategory(Integer.Parse(lblID.Text),Integer.Parse(sCatID))
		LoadCatList()
	End Sub
	Protected Sub btnCats_Click(ByVal sender As Object, ByVal e As EventArgs)
		ProductController.AddToCategory(Integer.Parse(lblID.Text),Integer.Parse(ddlCats.SelectedValue))
		LoadCatList()

	End Sub
End Class
