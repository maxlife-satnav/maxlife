#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common

Public Partial Class Modules_Admin_ProductAttributes
	Inherits System.Web.UI.UserControl
	Protected thisProduct As Product
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	End Sub
	Public Sub LoadAttributes(ByVal product As Commerce.Common.Product)

		'get the attributes for this product
		lblID.Text = product.ProductID.ToString()

		'get the atts, if they exist for this product
		ViewState("atts")=product.Attributes
		BindAttList(product.Attributes)

		LoadAttributeTemplates()

	End Sub

	Protected Sub btnAddAttribute_Click(ByVal sender As Object, ByVal e As EventArgs)

		Dim att As Commerce.Common.Attribute = New Commerce.Common.Attribute()
		att.Name = txtAttributeNew.Text.Trim()
		att.Description = txtAttNewDesc.Text.Trim()
		att.SelectionType = CType(Integer.Parse(ddlAttNewSelectionType.SelectedValue), AttributeType)

		'get the selections from the viewstate
		Dim aList As ArrayList = GetSelections()
		att.Selections = New System.Collections.Generic.List(Of AttributeSelection)()

		If att.SelectionType <> AttributeType.UserInput Then
			Dim i As Integer = 0
			Do While i < aList.Count
				att.Selections.Add(CType(aList(i), AttributeSelection))
				i += 1
			Loop

		Else
			Dim sel As AttributeSelection = New AttributeSelection()
			sel.Value = ""
			sel.PriceAdjustment = 0
			att.Selections.Add(sel)
		End If


		'a product can have one or more attribute selections
		'like "size" and "color"
		'store these into the viewstate as they are saved
		'and synch them with the product bits as well
		Dim atts As Commerce.Common.Attributes = Nothing
		If ViewState("atts") Is Nothing Then
			atts = New Attributes()
		Else
			atts = CType(ViewState("atts"), Attributes)
		End If
		atts.Add(att)


		'put it back the ViewState
		ViewState("atts") = atts

		'and set it to the product, which will serialize it down
		'to XML
		ProductController.UpdateProductAttributes(Integer.Parse(lblID.Text), atts)
		'bind up the grid
		BindAttList(atts)

	End Sub
    Protected Sub btnSaveAttTemplate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAttTemplate.Click
        Dim att As Commerce.Common.Attribute = New Commerce.Common.Attribute()
        Dim atts As Attributes = New Attributes()
        Dim attributeTemplate As AttributeTemplate = New AttributeTemplate()
        attributeTemplate.AttributeName = txtAttributeNew.Text.Trim()
        attributeTemplate.AttributeTypeID = Integer.Parse(ddlAttNewSelectionType.SelectedValue)
        Dim aList As ArrayList = GetSelections()
        If Not aList Is Nothing Then
            If aList.Count > 0 Then
                att.Selections = New System.Collections.Generic.List(Of AttributeSelection)()
                Dim i As Integer = 0
                Do While i < aList.Count
                    att.Selections.Add(CType(aList(i), AttributeSelection))
                    i += 1
                Loop
            End If
        End If
        atts.Add(att)
        attributeTemplate.SelectionList = Utility.ObjectToXML(GetType(Attributes), atts)
        attributeTemplate.Description = txtAttNewDesc.Text.Trim()
        attributeTemplate.Save(Page.User.Identity.Name)
        lblTemplateSaved.Text = "&nbsp;Template Saved"
        LoadAttributeTemplates()
    End Sub
	Protected Sub DeleteAtt(ByVal source As Object, ByVal e As DataGridCommandEventArgs)
		'pull the attributes out of the ViewState
		If Not ViewState("atts") Is Nothing Then
			Dim atts As Attributes = CType(ViewState("atts"), Attributes)
			Dim itemIndex As Integer = e.Item.ItemIndex
			atts.RemoveAt(itemIndex)
            'save it back and rebind
			ViewState("atts") = atts
            'update the product
			ProductController.UpdateProductAttributes(Integer.Parse(lblID.Text), atts)
            BindAttList(atts)
		End If
    End Sub
	Private Sub BindAttList(ByVal atts As Attributes)
		dgAtts.DataSource = atts
		dgAtts.DataBind()
	End Sub
	Protected Sub btnSetTemplate_Click(ByVal sender As Object, ByVal e As EventArgs)
		Dim sTemplate As String = ddlAttTemplates.SelectedValue
		Dim template As AttributeTemplate = New AttributeTemplate(Integer.Parse(sTemplate))
		txtAttributeNew.Text = template.AttributeName
		txtAttNewDesc.Text = template.Description
		ddlAttNewSelectionType.SelectedValue = template.AttributeTypeID.ToString()
		Dim attributes As Commerce.Common.Attributes = CType(Utility.XmlToObject(GetType(Attributes), template.SelectionList), Attributes)
		Dim aList As ArrayList = New ArrayList()
		For Each selection As AttributeSelection In attributes(0).Selections
			aList.Add(selection)
		Next selection
		BindSells(aList)
		ViewState("aList") = aList
	End Sub
	Private Function GetSelections() As ArrayList
		Dim aList As ArrayList = Nothing
		If Not ViewState("aList") Is Nothing Then
			aList = CType(ViewState("aList"), ArrayList)
		End If
		Return aList
	End Function
	Private Sub LoadAttributeTemplates()
        ddlAttTemplates.DataSource = AttributeTemplate.FetchAll()
		ddlAttTemplates.DataTextField = "attributeName"
		ddlAttTemplates.DataValueField = "templateID"
		ddlAttTemplates.DataBind()
	End Sub
    Protected Sub btnAddAtt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddAtt.Click
        'create an attribute list
        Dim aList As ArrayList = GetSelections()
        If aList Is Nothing Then
            aList = New ArrayList()
        End If
        'create a new selection and add it
        Dim sel As Commerce.Common.AttributeSelection = New AttributeSelection()
        sel.PriceAdjustment = Decimal.Parse(txtPriceAdjustment.Text)
        sel.Value = txtSelectionAdd.Text
        aList.Add(sel)
        BindSells(aList)
        ViewState("aList") = aList
    End Sub
	Private Sub BindSells(ByVal aList As ArrayList)
		dgSells.DataSource = aList
		dgSells.DataBind()
    End Sub
	Protected Sub removeSelection(ByVal source As Object, ByVal e As DataGridCommandEventArgs)
		Dim aList As ArrayList = GetSelections()
		Dim itemIndex As Integer = e.Item.ItemIndex
		If Not aList Is Nothing Then
			aList.RemoveAt(itemIndex)
		End If
		BindSells(aList)
    End Sub
	Protected Sub ddlAttNewSelectionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
		pnlSelections.Visible = ddlAttNewSelectionType.SelectedValue <> "2"
	End Sub
End Class
