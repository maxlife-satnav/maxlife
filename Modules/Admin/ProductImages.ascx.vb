#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common

Public Partial Class Modules_Admin_ProductImages
	Inherits System.Web.UI.UserControl

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	End Sub
	Protected Sub btnSaveImage_Click(ByVal sender As Object, ByVal e As EventArgs)
		Dim imageName As String = ImagePicker1.GetSelectedImage()
		'a little cleanup here
        'want to remove the path to the images folder
        lblMessage.Text = ""
        If Not String.IsNullOrEmpty(imageName) Then
            If Not imageName.Contains("1pix.gif") Then
                Dim appRoot As String = Request.ApplicationPath
                'if on the root, appRoot will be "/"
                'if it's a virtual, appRoot will be "/virtual so we need to scrape off the virtual path";
                If appRoot.Length > 1 Then
                    imageName = imageName.Replace(appRoot & "/", "")
                End If
                Dim image As Commerce.Common.Image = New Commerce.Common.Image()
                image.ImageFile = imageName
                image.ListOrder = Integer.Parse(txtNewImageListOrder.Text)
                image.Caption = txtNewImageCaption.Text
                image.ProductID = Integer.Parse(lblProductID.Text)

                image.Save(Page.User.Identity.Name)
                LoadImages(Integer.Parse(lblProductID.Text))
                If rptImages.Items.Count = 1 Then
                    'this is the first image - 
                    'set the defaultImage of the product
                    SetProductDefault(image.ImageFile)
                End If
            Else
                lblMessage.Text = "Please select an image"
            End If
        Else
            lblMessage.Text = "Please select an image"
        End If
    End Sub
	Private Sub SetProductDefault(ByVal imgFile As String)
		ProductController.SetProductDefaultImage(Integer.Parse(lblProductID.Text),imgFile)
	End Sub
	Public Sub LoadImages(ByVal productID As Integer)
		lblProductID.Text = productID.ToString()
		rptImages.DataSource = ProductController.GetImages(productID)
		rptImages.DataBind()
	End Sub
	Protected Sub DeleteImage(ByVal source As Object, ByVal e As RepeaterCommandEventArgs)
		Dim lbl As Label = CType(e.Item.FindControl("lblImageID"), Label)
		If Not lbl Is Nothing Then
			Commerce.Common.Image.Delete(Integer.Parse(lbl.Text))
			LoadImages(Integer.Parse(lblProductID.Text))

		End If

		'reset the defaultImage property
		If rptImages.Items.Count = 0 Then
			'no images, so set the default to 0
			SetProductDefault("")
		End If
	End Sub
End Class
