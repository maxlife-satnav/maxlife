<%@ Control Language="vb" AutoEventWireup="true" CodeFile="ProductImages.ascx.vb"
    Inherits="Modules_Admin_ProductImages" %>
<%@ Register Src="../ImageManager.ascx" TagName="ImagePicker" TagPrefix="uc2" %>
<table class="admintable" width="675px">
    <tr>
        <td class="tableHEADER" colspan="2">
            <strong>Images</strong></td>
    </tr>
    <tr>
        <td class="adminlabel" align="left">
            <b>Select an Image:</b></td>
        <td class="adminitem" align="left">
            <uc2:ImagePicker ID="ImagePicker1" runat="server" ImageFolder="images/productimages" />
        </td>
    </tr>
    <tr>
        <td class="adminlabel" align="left">
            Image Caption
        </td>
        <td class="adminitem" align="left">
            <asp:TextBox ID="txtNewImageCaption" runat="server" Width="350px" Height="60px" TextMode="MultiLine"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="adminlabel" align="left">
            Image List Order</td>
        <td class="adminitem" align="left">
            <asp:TextBox ID="txtNewImageListOrder" runat="server" Width="34px" Text="0"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="btnSaveImage" runat="server" OnClick="btnSaveImage_Click" Text="Add Image"
                CssClass="button" />
            <br />
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>
<br />
<asp:Label ID="lblProductID" runat="server" Visible="False"></asp:Label><br />
<table width="650" cellspacing="0">
    <asp:Repeater ID="rptImages" runat="server" OnItemCommand="DeleteImage">
        <ItemTemplate>
            <tr>
                <td class="adminlabel" width="15">
                    <b>
                        <%#Eval("listorder")%>
                    </b>
                    <asp:Label ID="lblImageID" runat="server" Text='<%#Eval("imageID")%>' Visible="false"></asp:Label></td>
                <td align="center">
                    &nbsp;&nbsp;&nbsp;<img src="<%=Utility.GetSiteRoot()%>/<%#Eval("imagefile")%>" alt="<%#Eval("imagefile")%>" /><br />
                    <i>
                        <%#Eval("caption")%>
                    </i>
                </td>
                <td>
                    <asp:Button ID="btnDelImage" runat="server" Text="Delete" CssClass="button" /></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
</table>
