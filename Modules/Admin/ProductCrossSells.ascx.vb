#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Promotions
Imports Commerce.Common

Public Partial Class Modules_Admin_ProductCrossSells
	Inherits System.Web.UI.UserControl
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	End Sub

	Private Sub LoadProductList(ByVal productID As Integer)

		'grab all the products
		Dim prods As ProductCollection = ProductController.GetAll()
		Dim item As ListItem
		For Each prod As Product In prods
					If (prod.DefaultImage Is Nothing) OrElse (prod.DefaultImage.Trim().Length = 0) Then
						item = New ListItem("<img src='" & Page.ResolveUrl("~/images/productimages/no_image_available.gif") & "'  width=50><br><b>" & prod.Sku & "</b>", prod.ProductID.ToString())
					Else
						item = New ListItem("<img src='" & Page.ResolveUrl("../" & prod.DefaultImage) & "' width=50><br><b>" & prod.Sku & "</b>", prod.ProductID.ToString())
					End If
			chkProducts.Items.Add(item)
		Next prod

		'now load the cross-products for this product, and check off the
		'bits
		Dim crosses As ProductCollection = PromotionService.GetCrossSells(productID)
		For Each cross As Product In crosses
			For Each l As ListItem In chkProducts.Items
				If Integer.Parse(l.Value) = cross.ProductID Then
					l.Selected = True
				End If
			Next l
		Next cross

	End Sub


	Public Sub LoadCrossProducts(ByVal productID As Integer)
		lblID.Text = productID.ToString()
		LoadProductList(productID)
	End Sub
	Protected Sub SaveCrossList(ByVal sender As Object, ByVal e As EventArgs)
		'first, remove all the cross-sell bits
		Dim productID As Integer=Utility.GetIntParameter("id")
		Dim prod As Product = New Product(productID)
		Try
			'prod.SaveManyToMany("CSK_Promo_Product_CrossSell_Map", "crossProductID", chkProducts.Items);
			'Utility.SaveManyToMany("CSK_Store_Product", "productID", "CSK_Promo_Product_CrossSell_Map", "crossProductID", chkProducts.Items);
			Utility.SaveManyToMany("productID", productID, "CSK_Promo_Product_CrossSell_Map", "crossProductID", chkProducts.Items)
			ResultMessage1.ShowSuccess("Cross-Sells saved")
		Catch x As Exception
			ResultMessage1.ShowFail(x.Message)

		End Try
	End Sub
End Class
