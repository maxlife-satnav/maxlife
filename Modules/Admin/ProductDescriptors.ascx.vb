#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common

Public Partial Class Modules_Admin_ProductDescriptors
	Inherits System.Web.UI.UserControl
	Private productID_Renamed As Integer

	Public Property ProductID() As Integer
		Get

			Return productID_Renamed
		End Get
		Set

			productID_Renamed = Value
		End Set
	End Property


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		If (Not Page.IsPostBack) Then
			lblProductID.Text = productID_Renamed.ToString()
			LoadDescriptors()
		End If
	End Sub
	Private Sub LoadDescriptors()
		rptDescriptors.DataSource = ProductController.GetDescriptors(Integer.Parse(lblProductID.Text))
		rptDescriptors.DataBind()

	End Sub
	Protected Function GetDescriptorList(ByVal descriptor As Object, ByVal isBulletedList As Object) As String
		Dim sOut As String = descriptor.ToString()

		If CBool(isBulletedList) Then
			Dim sList As String() = sOut.Split(ControlChars.Cr)
			sOut = "<ul>"
			For Each s As String In sList
				sOut &= "<li>" & s & "</li>"
			Next s
			sOut &= "</ul>"
		End If

		Return sOut
	End Function
	Protected Sub DeleteDescriptor(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
		If Not e.CommandArgument Is Nothing Then

			Dim descriptorID As Integer = Convert.ToInt16(e.CommandArgument.ToString())
			'delete it out
			If e.CommandName = "Save" Then
				'update the text and checkbox
				Dim chkIsBulleted As CheckBox = CType(e.Item.FindControl("chkIsBulletedList"), CheckBox)
				Dim txtDesc As TextBox = CType(e.Item.FindControl("txtDescriptor"), TextBox)
				Dim txtListOrder As TextBox = CType(e.Item.FindControl("txtDescriptorListOrder"), TextBox)
				If Not chkIsBulletedList Is Nothing AndAlso Not txtDescriptor Is Nothing Then
					Dim pd As ProductDescriptor = New ProductDescriptor(descriptorID)
					pd.IsBulletedList = chkIsBulleted.Checked
					pd.Descriptor = txtDesc.Text

					Try
						pd.ListOrder = Integer.Parse(txtListOrder.Text)
					Catch

					End Try
					pd.Save(Utility.GetUserName())
				End If
			Else
				ProductDescriptor.Delete(descriptorID)

			End If
			LoadDescriptors()

		End If
	End Sub
	Protected Sub btnSaveDescriptor_Click(ByVal sender As Object, ByVal e As EventArgs)
		'add in the new desc list
		Dim pd As ProductDescriptor = New ProductDescriptor()
		Try
			pd.ListOrder = Integer.Parse(txtDescriptorListOrder.Text)
		Catch

		End Try
		pd.Title = txtDescriptorTitle.Text
		pd.Descriptor = txtDescriptor.Text
		pd.IsBulletedList = chkIsBulletedList.Checked
		pd.ProductID = Integer.Parse(lblProductID.Text)
		pd.Save(Utility.GetUserName())
		LoadDescriptors()
	End Sub

End Class
