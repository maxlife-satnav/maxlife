﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Logo.aspx.vb" Inherits="FAM_FAM_Webfiles_Logo" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="form-wrapper">
        <form id="form1" runat="server">
            <div class="row" style="margin-bottom: 30px;">
                <label class="col-sm-3 control-label">Select Image<span style="color: red;"> *</span></label>
                <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Select Image"
                    ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-sm-4 btn btn-default">
                    <i class="fa fa-folder-open-o fa-lg"></i>
                    <asp:FileUpload ID="fpBrowseDoc" runat="Server" />
                </div>
                <div class="col-sm-5">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color pull-right" Text="Submit" ValidationGroup="Val1" />
                    <asp:Label ID="lblMsg" runat="server" CssClass="control-label" ForeColor="Red"></asp:Label>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger pull-left"  Style="top: 15px;position:relative;color:red" ShowSummary="true" ValidationGroup="Val1" />
                </div>
            </div>
            
        </form>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script type="text/javascript">
        $("#btnSubmit").click(function (e) {
            $("#lblMsg").text(' ');
        })

        $("#fpBrowseDoc").click(function (e) {
            $("#lblMsg").text(' ');
        })

    </script>


</body>
</html>
