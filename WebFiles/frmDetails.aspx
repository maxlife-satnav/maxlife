<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmDetails.aspx.vb" Inherits="WebFiles_frmDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="row">
        <aside class="sidebar" style="margin-left: 15px;">
            <nav class="sidebar-nav">
                <ul id="menu">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </ul>
            </nav>
        </aside>

        <div class="row custom-page-wrapper" style="background-image: url('../BootStrapCSS/images/loading_2.gif'); background-position: center; background-repeat: no-repeat;">
            <div id="framediv">
                <iframe id="container" src="../Dashboard/DashboardV2.aspx" width="100%" frameborder="0"></iframe>
            </div>
        </div>
    </div>

    <script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../Scripts/Intercom/Intercom.js"></script>
    <script type="text/javascript">
       
        function Clear() {
            var Backlen = history.length;
            if (Backlen > 0) history.go(-Backlen);
        }
   
        window.onload =  function clearListCookies() {
            var cookies = document.cookie.split(";");
            for (var i = 0; i < cookies.length; i++) {
                var spcook = cookies[i].split("=");
                deleteCookie(spcook[0]);
            }
            function deleteCookie(cookiename) {
                var d = new Date();
                d.setDate(d.getDate() - 1);
                var expires = ";expires=" + d;
                var name = cookiename;
                //alert(name);
                var value = "";
                document.cookie = name + "=" + value + expires + "; path=/acc/html";
            }

            Clear();
            //window.location = ""; // TO REFRESH THE PAGE
        }
    </script>
    <script type="text/javascript">

        //TO AVOID FADE IN EFFECT IN IE
        var ms_ie = false;
        var ua = window.navigator.userAgent;
        var old_ie = ua.indexOf('MSIE ');
        var new_ie = ua.indexOf('Trident/');
        var mozilla = ua.indexOf('Firefox/');

        if ((old_ie > -1) || (new_ie > -1)) {
            ms_ie = true;
        } else if (mozilla > -1) {
            mozilla = true;
        }

        function setFrameSource(src) {        
            //if (src == '/SMViews/Map/MapMarker.aspx' || src == '/SMViews/Map/ProposedLocations.aspx') {
             if (src == '/SMViews/Map/MapMarker.aspx') {
                window.open(src,"_blank");
            } else {
                if (!(ms_ie || mozilla)) {
                    $("#container").hide();
                }
                $("#container").attr("src", src);
                Intercom('update', { "Link": src });
                return false;
            }
        }

        $(document).ready(function () {
            //$(window).on('blur', function () { $('.dropdown-toggle').parent().removeClass('open'); });            
            $('#container').css('height', $(window).height() - 100 + 'px');
            $('.sidebar').css('height', $(window).height() - 100 + 'px');
            $("#page-wrapper").css('min-height', $(window).height() + 'px');

            //if (!ms_ie) {
            $("#container").load(function (e) {
                $("#container").fadeIn("slow");
            });
            //}
        });


        window.intercomSettings = {
            app_id: "qy20hva4",
            name: '<%=Session("uid")%>', // Full name
            email: '<%=Session("uemail")%>', // Email address
            created_at: '<%=Session("LoginTime")%>',// Signup date as a Unix timestamp
            "Link": 'frmDetails.aspx',
            tenant_id: '<%=Session("TENANT")%>'
        };

    </script>
</asp:Content>
