﻿Imports System.Data
Imports System.IO

Partial Class FAM_FAM_Webfiles_Logo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                Dim UID As String = Session("uid")
            End If
        End If
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        lblMsg.Text = String.Empty
        If fpBrowseDoc.PostedFile IsNot Nothing Then
            Dim valid As Integer = 0
            Dim FileName As String = System.IO.Path.GetFileName(fpBrowseDoc.PostedFile.FileName)
            Dim ext = Path.GetExtension(FileName)
            If ext.Equals(".jpg") Then
                fpBrowseDoc.SaveAs(Server.MapPath("~/BootStrapCSS/images/" & FileName))
                valid = 1
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
                sp1.Command.AddParameter("@path", "~/BootStrapCSS/images/" & FileName, DbType.String)
                sp1.Command.AddParameter("@filename", FileName, DbType.String)
                sp1.Command.AddParameter("@type", "1", DbType.String)
                sp1.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
                sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
                sp1.ExecuteScalar()
                lblMsg.Text = "Logo uploaded Successfully"
            Else
                lblMsg.Text = "Please Select JPG/JPEG Image File"
            End If
            Exit Sub
        End If
    End Sub
End Class
