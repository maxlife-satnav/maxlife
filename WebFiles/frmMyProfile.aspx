﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMyProfile.aspx.vb" Inherits="WebFiles_frmMyProfile" %>

<html>
<head>
    <title></title>
    <link href="../Dashboard/CSS/ProfileStyle.css" rel="stylesheet" />
</head>
<style>
    .glyphiconcol {
        color: #5bc0de;
    }
</style>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <form id="form1" class="form-horizontal" runat="server">
                <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                    <legend style="padding-top: 0px; width: 13%">User Profile</legend>
                    <div class="row">

                        <div class="col-md-5">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-primary" runat="server" id="divspace" visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">My Space Management</a>
                                            <button type="button" class="btn" id="spacePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                                data-content="<%=popupVal%>"
                                                style="clear: both; float: right; margin-top: -5px">
                                                <i class="fa fa-bell" style="color: #5bc0de;"></i>
                                            </button>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Allocated Space Id:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblSpaceId"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Location:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblLocation"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Floor:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblfloor"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Business Unit:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblvertical"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Function:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblcostcenter"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Shift Timings:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblshifttime"></asp:Label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-info" runat="server" id="propertydiv" visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">My Property Management</a>
                                            <button type="button" class="btn" id="propertyPopOver" data-container="body" data-placement="right" data-trigger="hover"
                                                data-content="<%=propertypopup%>"
                                                style="clear: both; float: right; margin-top: -5px">
                                                <i class="fa fa-bell" style="color: #5bc0de;"></i>
                                            </button>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Property Types:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblpt"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>No of Properties:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblpptscount"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Lease Properties:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblleasecount"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Tenant Properties:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lbltenantcount"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-warning" runat="server" id="assetdiv" visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">My Asset Management</a>
                                            <button type="button" class="btn" id="assetPopOver" data-container="body" data-placement="right" data-trigger="hover"
                                                data-content="<%=Assetpopup%>"
                                                style="clear: both; float: right; margin-top: -5px">
                                                <i class="fa fa-bell" style="color: #5bc0de;"></i>
                                            </button>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <strong>No of Assets Mapped(Capital):</strong>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Label runat="server" ID="lblastcount"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-success" runat="server" id="maintenancediv" visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">My Maintenance Management</a>
                                            <button type="button" class="btn" id="maintenancePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                                data-content="<%=maintenancepopup%>"
                                                style="clear: both; float: right; margin-top: -5px">
                                                <i class="fa fa-bell" style="color: #5bc0de;"></i>
                                            </button>
                                        </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Assets under AMC:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblamcassets"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default" runat="server" id="helpdeskdiv" visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">My Helpdesk Management</a>
                                            <button type="button" class="btn" id="helpDeskPopOver" data-container="body" data-placement="right" data-trigger="hover"
                                                data-content="<%=HelpDeskpopup%>"
                                                style="clear: both; float: right; margin-top: -5px">
                                                <i class="fa fa-bell" style="color: #5bc0de;"></i>
                                            </button>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <strong>Total Requests Raised: </strong>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Label runat="server" ID="lblreqraised"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <strong>Total Requests In-progress:</strong>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Label runat="server" ID="lblinprogreq"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <strong>Total Requests On Hold:</strong>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Label runat="server" ID="lblonhold"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <strong>Total Requests Closed:</strong>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Label runat="server" ID="lblclosed"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <strong>Total Requests Rejected:</strong>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Label runat="server" ID="lblrejected"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-danger" runat="server" id="conferencediv" visible="false">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">My Conference Management</a>
                                            <button type="button" class="btn" id="conferencePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                                data-content="<%=Conferencepopup%>"
                                                style="clear: both; float: right; margin-top: -5px">
                                                <i class="fa fa-bell" style="color: #5bc0de;"></i>
                                            </button>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Conference Name:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblconfname"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Location:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblconfloc"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Floor:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblconsfloor"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Booked Date:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblbkdate"></asp:Label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <strong>Booked Time:</strong>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:Label runat="server" ID="lblbktime"></asp:Label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="profile">
                                <div class="col-sm-12">
                                    <div class="col-xs-12 col-sm-8">
                                        <h2><span class="glyphicon glyphicon-user glyphiconcol"></span>
                                            <asp:Label ID="lblempname" runat="server"></asp:Label></h2>
                                        <br />
                                        <h5><span class="glyphicon glyphicon-tag glyphiconcol"></span><strong>&nbsp;&nbsp;Employee ID:&nbsp;</strong><asp:Label ID="lblemployeeID" runat="server"></asp:Label></h5>
                                        <br />
                                        <h5><span class="glyphicon glyphicon-envelope glyphiconcol"></span><strong>&nbsp;&nbsp;Email:&nbsp;</strong><asp:Label ID="lblemail" runat="server"></asp:Label></h5>
                                        <br />
                                        <h5><span class="glyphicon glyphicon-certificate glyphiconcol"></span><strong>&nbsp;&nbsp;Designation:</strong><asp:Label ID="lbldesig" runat="server"></asp:Label></h5>
                                        <br />
                                        <h5><span class="glyphicon glyphicon-bookmark glyphiconcol"></span><strong>&nbsp;&nbsp;Department:&nbsp;</strong><asp:Label ID="lbldept" runat="server"></asp:Label></h5>
                                        <br />
                                        <h5><span class="glyphicon glyphicon-hand-right glyphiconcol"></span><strong>&nbsp;&nbsp;Reporting To:&nbsp;</strong><asp:Label ID="lblreporting" runat="server"></asp:Label></h5>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 text-center">
                                        <a href="#">                                                                               
                                        <asp:Image ID="img" ToolTip="Click here to change profile picture" runat="server" ImageUrl="../Userprofiles/default-user-icon-profile.jpg" AlternateText="" CssClass="img-circle img-responsive" />                                                                       
                                        <input type="file" id="fup" name="fileUp" class="hidden" accept="image/*">
                                        </a>
                                    </div>
                                </div>
                                <%--<div class="col-xs-12 divider text-center">
                                    <div class="col-xs-12 col-sm-4 emphasis">                          
                                        <h2><strong>20,7K </strong></h2>
                                        <p><small>Followers</small></p>
                                        <button class="btn btn-success btn-block"><span class="fa fa-plus-circle"></span>Follow </button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 emphasis">
                                        <h2><strong>245</strong></h2>
                                        <p><small>Following</small></p>
                                        <button class="btn btn-info btn-block"><span class="fa fa-user"></span>View Profile </button>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 emphasis">
                                        <h2><strong>43</strong></h2>
                                        <p><small>Snippets</small></p>
                                        <div class="btn-group dropup btn-block">
                                            <button type="button" class="btn btn-primary"><span class="fa fa-gear"></span>Options </button>
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu text-left" role="menu">
                                                <li><a href="#"><span class="fa fa-envelope pull-right"></span>Send an email </a></li>
                                                <li><a href="#"><span class="fa fa-list pull-right"></span>Add or remove from a list  </a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><span class="fa fa-warning pull-right"></span>Report this user for spam</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#" class="btn disabled" role="button">Unfollow </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                        </div>

                        <%-- <div class="modal fade" id="myModal" tabindex='-1'>
                             <div class="modal-dialog modal-md">
                             <div class="modal-content">
                             <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                             <h4 class="modal-title">Upload Image</h4>
                        </div>
                       <div class="modal-body" id="modelcontainer">
                             Content loads here 
                       <iframe id="modalcontentframe" width="100%" frameborder="0" style="border: none"></iframe>
                    </div>
            </div>
        </div>
    </div>--%>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <script>
        $(function () {
            //$("#spacePopOver").popover({ title: 'Twitter Bootstrap Popover', content: "It's so simple to create a tooltop for my website!" });
            $("#spacePopOver").popover();
            $("#propertyPopOver").popover();
            $("#assetPopOver").popover();
            $("#conferencePopOver").popover();
            $("#maintenancePopOver").popover();
            $("#helpDeskPopOver").popover();
        });
       
        $("#img").click(function () {           
            var ofd = document.getElementById("fup");
            ofd.click();       
            return false;
        });
        $(":file").change(function (e) {
            var fileUpload = $("#fup").get(0);
            var files = fileUpload.files; 
            var data = new FormData();            
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
            } 
            $.ajax({
                url: "../api/MyprofileAPI/SaveImagetoDB",
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function (result) { alert(result); },
                error: function (err) {
                alert(err.statusText)
                }
            }); 
            e.preventDefault();
            $("#img").attr("src", "../Userprofiles/" + files[0].name);
        });       
    </script>
</body>
</html>
