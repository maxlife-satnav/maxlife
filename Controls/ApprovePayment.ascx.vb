Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ApprovePayment
    Inherits System.Web.UI.UserControl

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
     
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADDWR")
        sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        'sp1.Command.AddParameter("@PAYMENT_TYPE", "Approved", DbType.String)
        sp1.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp1.ExecuteScalar()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_WRDETAILS")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=14")
        'lblMsg.Text = "Payment Approved Succesfully"
        Cleardata()

    End Sub
    'Public Sub fillgrid()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETWDETAIL")
    '    sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
    '    gvwrequest.DataSource = sp.GetDataSet()
    '    gvwrequest.DataBind()
    '    For i As Integer = 0 To gvwrequest.Rows.Count - 1
    '        Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblstatus"), Label)
    '        If lblstatus.Text = "0" Then
    '            lblstatus.Text = "Pending"
    '        ElseIf lblstatus.Text = "1" Then
    '            lblstatus.Text = "InProgress"
    '        Else
    '            lblstatus.Text = "2"
    '            lblstatus.Text = "Completed"
    '        End If

    '        Dim lblworktype As Label = CType(gvwrequest.Rows(i).FindControl("lblworktype"), Label)
    '        If lblworktype.Text = "0" Then
    '            lblworktype.Text = "Rejected"
    '        Else
    '            lblworktype.Text = "Approved"
    '        End If
    '    Next
    '    btnApprove.Enabled = True
    '    btnReject.Enabled = True
    'End Sub
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        txtStartDate.Text = ""
        txtExpiryDate.Text = ""
        txtpamount.Text = ""
        txtoamount.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtRemarks.Text = ""
        ddlstatus.SelectedIndex = -1
        ddlwstatus.SelectedIndex = -1
        btnApprove.Enabled = False
        btnReject.Enabled = False
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
       
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADDWR1")
        sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        'sp1.Command.AddParameter("@REQUEST_STATUS", 1, DbType.Int32)
        'sp1.Command.AddParameter("@PAYMENT_TYPE", "Pending", DbType.String)
        sp1.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp1.ExecuteScalar()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_WRDETAILS")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=22")
        'lblMsg.Text = "Payment Rejected"
        Cleardata()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getrequest()         
        End If
        txtStartDate.Attributes.Add("readonly", "readonly")
        txtExpiryDate.Attributes.Add("readonly", "readonly")
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETWORK1REQ")
        sp.Command.AddParameter("@User", Session("UID"), DbType.String)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select WorkRequest--", "0"))
    End Sub

    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        If ddlWorkRequest.SelectedIndex > 0 Then



            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETWORKDETAILS")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT")
                Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                If status = 0 Then
                    ddlwstatus.SelectedValue = 0
                ElseIf status = 1 Then
                    ddlwstatus.SelectedValue = 1
                Else
                    ddlwstatus.SelectedValue = 2
                End If

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETWORKREDETAILS")
                sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
                Dim ds1 As New DataSet()
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    txtStartDate.Text = ds1.Tables(0).Rows(0).Item("START_DATE")
                    txtExpiryDate.Text = ds1.Tables(0).Rows(0).Item("END_DATE")
                    txtpamount.Text = ds1.Tables(0).Rows(0).Item("PAID_AMOUNT")
                    txtoamount.Text = ds1.Tables(0).Rows(0).Item("OUTSTANDING_AMOUNT")
                    txtRemarks.Text = ds1.Tables(0).Rows(0).Item("Remarks")
                    Dim status1 As Integer = ds1.Tables(0).Rows(0).Item("STATUS")
                    If status1 = 0 Then
                        ddlstatus.SelectedValue = 0
                    ElseIf status1 = 1 Then
                        ddlstatus.SelectedValue = 1
                    Else
                        ddlstatus.SelectedValue = 2
                    End If
                End If
            End If
            btnApprove.Visible = True
            btnReject.Visible = True
        Else
            txtStartDate.Text = ""
            txtExpiryDate.Text = ""
            txtpamount.Text = ""
            txtoamount.Text = ""
            txtWorkTitle.Text = ""
            txtWorkSpec.Text = ""
            txtamount.Text = ""
            txtRemarks.Text = ""
            ddlstatus.SelectedIndex = -1
            ddlwstatus.SelectedIndex = -1
            
        End If
    End Sub
End Class
