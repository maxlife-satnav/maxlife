Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_UpdateSurrenderRequisitions
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim Req_id As String
    Dim lblAstCode As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Req_id = Request.QueryString("Req_id")
            GetDetailsByRequistion(Req_id)
        End If
    End Sub

    Private Sub GetDetailsByRequistion(ByVal Req_id As String)
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        ObjSubsonic.BindGridView(SurReqDetItems, "GET_SURRENDER_REQ", param)
        ds = ObjSubsonic.GetSubSonicDataSet("GET_SURRENDER_REQ", param)
        'If ds.Tables(0).Rows.Count > 0 Then
        lblAstCode = ds.Tables(0).Rows(0).Item("AAT_AST_CODE")
        '    lblAstName.Text = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
        '    lblLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
        '    'lblTower.Text = ds.Tables(0).Rows(0).Item("TWR_NAME")
        '    'lblFloor.Text = ds.Tables(0).Rows(0).Item("FLR_NAME")
        '    lblAstAllocDt.Text = ds.Tables(0).Rows(0).Item("AAT_ALLOCATED_DATE")
        '    lblAstSurDt.Text = ds.Tables(0).Rows(0).Item("AAT_SURRENDERED_DATE")
        '    lblSurBy.Text = ds.Tables(0).Rows(0).Item("AUR_NAME")
        '    lblSurReq_id.Text = Req_id
        txtRemarks.Text = ds.Tables(0).Rows(0).Item("SREQ_REQ_REMARKS")
        txtRemarks.Enabled = False

        If ds.Tables(0).Rows(0).Item("SREQ_STATUS") = 1037 Or ds.Tables(0).Rows(0).Item("SREQ_STATUS") = 1038 Then
            btnApprov.Visible = False
            btnReject.Visible = False
            txtRMRemarks.Text = ds.Tables(0).Rows(0).Item("SREQ_RM_REMARKS")
            txtRMRemarks.Enabled = False
        Else
            btnApprov.Visible = True
            btnReject.Visible = True

        End If

    End Sub

    Protected Sub btnApprov_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprov.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("REQ_ID")
        param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("UID")
        param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
        param(2).Value = txtRMRemarks.Text
        param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
        param(3).Value = 1037
        ObjSubsonic.GetSubSonicDataSet("UPDATESURRENDER_REQUISTION_byRM", param)

        For Each row As GridViewRow In SurReqDetItems.Rows
            Dim lblAstCod As Label = DirectCast(row.FindControl("lblAstCode"), Label)
            send_mail(Request.QueryString("REQ_ID"), lblAstCod.Text, Session("TENANT") & "." & "SEND_MAIL_ASSET_SURRENDER_REQUISITION_ITAPPROVAL")
        Next

        ' Response.Redirect("frmSurrenderRequisitions.aspx")
        Response.Redirect("frmAssetThanks.aspx?RID=surrenderreqRMApprove&MReqId=" + Request.QueryString("REQ_ID"))

    End Sub

    Public Sub send_mail(ByVal reqid As String, ByVal astcode As String, ByVal proc As String)
        Dim sp1 As New SubSonic.StoredProcedure(proc)
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@AAT_CODE", astcode, DbType.String)
        sp1.Execute()
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("REQ_ID")
        param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("UID")
        param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
        param(2).Value = txtRMRemarks.Text
        param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
        param(3).Value = 1038
        ObjSubsonic.GetSubSonicDataSet("UPDATESURRENDER_REQUISTION_byRM", param)

        For Each row As GridViewRow In SurReqDetItems.Rows
            Dim lblAstCod As Label = DirectCast(row.FindControl("lblAstCode"), Label)
            send_mail(Request.QueryString("REQ_ID"), lblAstCod.Text, Session("TENANT") & "." & "SEND_MAIL_ASSET_SURRENDER_REQUISITION_ITREJECT")
        Next


        Response.Redirect("frmAssetThanks.aspx?RID=surrenderreqRMReject&MReqId=" + Request.QueryString("REQ_ID"))
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmSurrenderRequisitions.aspx")
    End Sub
End Class
