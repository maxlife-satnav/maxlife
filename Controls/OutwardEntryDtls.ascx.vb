Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_OutwardEntryDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim strstat As String


    Dim FBDG_ID, SBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            strstat = Request.QueryString("stat")
            getDetailsbyReqId(strReqId)
            BindLocations()

        End If
    End Sub

    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)
        If strstat = "INTRA" Then

            Dim dr As SqlDataReader
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strREQ_id
            dr = ObjSubsonic.GetSubSonicDataReader("GET_INTRA_MVMT_REQID", param)
            If dr.Read Then
                FBDG_ID = dr.Item("SLOC_CODE")
                SBDG_ID = dr.Item("SLOC_CODE")
                'FTower = dr.Item("MMR_FROMBDG_ID")
                'FFloor = dr.Item("MMR_FROMFLR_ID")
                TBDG_ID = dr.Item("FLOC_CODE")
                'TTower = dr.Item("MMR_TOBDG_ID")
                'TFloor = dr.Item("MMR_TOFLR_ID")
                receiveAst = dr.Item("MMR_RECVD_BY")
                strremarks = dr.Item("MMR_COMMENTS")
            End If
            If dr.IsClosed = False Then
                dr.Close()
            End If
            BindRequestAssets(strReqId)
            txtPersonName.Text = receiveAst
        Else
            Dim dr As SqlDataReader
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strREQ_id
            dr = ObjSubsonic.GetSubSonicDataReader("GET_INTER_MVMT_REQID", param)
            If dr.Read Then
                FBDG_ID = dr.Item("MMR_FROMBDG_ID")
                SBDG_ID = dr.Item("MMR_FROMBDG_ID")
                'FTower = dr.Item("MMR_FROMBDG_ID")
                'FFloor = dr.Item("MMR_FROMFLR_ID")
                TBDG_ID = dr.Item("MMR_TOBDG_ID")
                'TTower = dr.Item("MMR_TOBDG_ID")
                'TFloor = dr.Item("MMR_TOFLR_ID")
                receiveAst = dr.Item("MMR_RECVD_BY")
                strremarks = dr.Item("MMR_COMMENTS")
            End If
            If dr.IsClosed = False Then
                dr.Close()
            End If
            BindInterRequestAssets(strReqId)
            txtPersonName.Text = receiveAst
        End If
    End Sub

    Private Sub BindLocations()
        '----------- Binding Location-----------------------
        ObjSubsonic.Binddropdown(ddlSLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")
        ObjSubsonic.Binddropdown(ddlDLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")

        '------------ From Building/Tower/Floor -----------------------------
        If FBDG_ID = "" Or FBDG_ID Is Nothing Then

        Else
            ddlSLoc.Items.FindByValue(FBDG_ID).Selected = True

            ''----------- Binding Tower -----------------------
            'If ddlSLoc.SelectedIndex > 0 Then
            '    Dim LocCode As String = ddlSLoc.SelectedItem.Value
            '    BindTowersByLocation(LocCode, ddlSTower)
            'End If
            'ddlSTower.Items.FindByValue(FTower).Selected = True
            ''-------------------------------------------------

            'If ddlSTower.SelectedIndex > 0 Then
            '    Dim TwrCode As String = ddlSTower.SelectedItem.Value
            '    Dim LocCode As String = ddlSLoc.SelectedItem.Value
            '    BindFloorsByTower(TwrCode, LocCode, ddlSFloor)
            'End If
            'ddlSFloor.Items.FindByValue(FFloor).Selected = True

        End If
        '------------ From Building/Tower/Floor -------------------------
        If TBDG_ID = "" Or TBDG_ID Is Nothing Then

        Else
            ddlDLoc.Items.FindByValue(TBDG_ID).Selected = True

            '----------- Binding Tower -----------------------
            'If ddlDLoc.SelectedIndex > 0 Then
            '    Dim LocCode As String = ddlDLoc.SelectedItem.Value
            '    BindTowersByLocation(LocCode, ddlDTower)
            'End If
            'ddlDTower.Items.FindByValue(TTower).Selected = True
            ''-------------------------------------------------

            'If ddlDTower.SelectedIndex > 0 Then
            '    Dim TwrCode As String = ddlDTower.SelectedItem.Value
            '    Dim LocCode As String = ddlDLoc.SelectedItem.Value
            '    BindFloorsByTower(TwrCode, LocCode, ddlDFloor)
            'End If
            'ddlDFloor.Items.FindByValue(TFloor).Selected = True

        End If
    End Sub

    Public Sub BindInterRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "GET_INTER_MVMT_ASTS", param)
    End Sub

    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "GET_MVMT_ASTS", param)
    End Sub


    'Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
    '    If ddlSLoc.SelectedIndex > 0 Then
    '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
    '        BindTowersByLocation(LocCode, ddlSTower)
    '        getDetailsbyReqId(Request.QueryString("Req_id"))
    '    End If
    'End Sub

    'Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
    '    param(0).Value = LocCode
    '    ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    'End Sub

    'Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
    '    If ddlDLoc.SelectedIndex > 0 Then
    '        Dim LocCode As String = ddlDLoc.SelectedItem.Value
    '        BindTowersByLocation(LocCode, ddlDTower)
    '    End If
    'End Sub

    'Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
    '    If ddlSTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlSTower.SelectedItem.Value
    '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, LocCode, ddlSFloor)
    '    End If
    'End Sub
    'Private Sub BindFloorsByTower(ByVal TwrCode As String, ByVal loccode As String, ByRef ddl As DropDownList)
    '    Dim param(1) As SqlParameter
    '    param(0) = New SqlParameter("@FLR_LOC_ID", DbType.String)
    '    param(0).Value = Loccode
    '    param(1) = New SqlParameter("@FLR_TWR_ID", DbType.String)
    '    param(1).Value = TwrCode
    '    ObjSubsonic.Binddropdown(ddl, "GET_FLOOR_BYLOCTWR", "FLR_NAME", "FLR_CODE", param)
    'End Sub

    'Protected Sub ddlDTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTower.SelectedIndexChanged
    '    If ddlDTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlDTower.SelectedItem.Value
    '        Dim LocCode As String = ddlDLoc.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, LocCode, ddlDFloor)
    '    End If
    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strASSET_LIST As New ArrayList
        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            'strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text)
        Next

        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_Outwardentrydetails_approve"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)
        If Request.QueryString("stat") = "INTRA" Then

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1019
            ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_OUTWARDSTATUS", param)
            'lblMsg.Text = "Request succesfully approved."
        Else
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1019
            ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_MVMT_OUTWARDSTATUS", param)
        End If

        Response.Redirect("frmAssetThanks.aspx?RID=outwardapp")
    End Sub



    Private Sub getRequestDetails(ByVal ReqId As String, ByVal strAst As ArrayList, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        'Dim ReqId As String = GetRequestId()
        'Dim strASSET_LIST As New ArrayList
        'Dim i As Integer = 0
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        '    If chkSelect.Checked Then
        '        strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
        '    End If
        '    i += 1
        'Next
        SendMail(ReqId, strAst, Session("uid"), txtRemarks.Text, MailStatus, App_Rej_status)
    End Sub


    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_INTRA_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty


            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = ObjSubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If

            'strAstList = "<table width='200' border='1'><tr><td><strong>Asset Name </strong></td><td><strong>Quantity</strong></td></tr>"
            'For i As Integer = 0 To AstList.Count - 1
            '    Dim p1
            '    p1 = AstList.Item(i).Split(",")

            '    strAstList = strAstList & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            'Next
            'strAstList = strAstList & "</table> "


            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(3) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(4) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(5) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(6) & "</td></tr>"



            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtPersonName.Text & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&rl=rm&ty=imr> Click to Approve </a></td></tr></table>"

            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&rl=rm&ty=imr>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)



            '1. IT Request Approved copy goes to request raised person.
            '2. One copy for IT ADMIN to approve.

            'If App_Rej_status = True Then
            Insert_AmtMail(body, strRR, strSubject, strRM)
            'Else
            'Insert_AmtMail(body, strRM, strSubject, "")
            'End If






        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub


    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "IntraMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = DateTime.Now
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub



    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmOutwardEntry.aspx")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click



        Dim strASSET_LIST As New ArrayList

        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)

            'strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text)
        Next




        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("Req_id")
        param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Session("uid")
        param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
        param(3).Value = 1020

        ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_OUTWARDSTATUS", param)





        Dim MailTemplateId As Integer
        MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_Outwardentrydetails_Reject"))
        getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)





        'lblMsg.Text = "Succesfully Updated information for request id " & Request.QueryString("Req_id") & "."
        'lblMsg.Text = "Rejected successfully."
        Response.Redirect("frmAssetThanks.aspx?RID=outwardrej")
    End Sub


End Class
