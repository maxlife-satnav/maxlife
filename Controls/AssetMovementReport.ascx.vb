Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_AssetMovementReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FillReqIds("")
            FillInterMVMTRequests("")
        End If
    End Sub

    Public Sub FillReqIds(ByVal strSearch As String)
        Dim UID As String = ""
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            UID = Session("uid")
        End If
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
        param(0).Value = UID
        param(1) = New SqlParameter("@MMR_AST_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = strSearch
        ObjSubsonic.BindGridView(gvReqIds, "GET_AST_MVMT_RPT", param)
    End Sub



    Public Sub FillInterMVMTRequests(ByVal strSearch As String)
        Dim UID As String = ""
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            UID = Session("uid")
        End If
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
        param(0).Value = UID
        param(1) = New SqlParameter("@MMR_AST_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = strSearch
        ObjSubsonic.BindGridView(gvInterMVMT, "GET_AST_INTER_MVMT_RPT", param)
    End Sub

    Protected Sub gvReqIds_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReqIds.PageIndexChanging
        gvReqIds.PageIndex = e.NewPageIndex
        FillReqIds(txtSearch.Text)
    End Sub

    Protected Sub gvReqIds_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqIds.RowCommand
        If e.CommandName = "Details" Then
            Response.Redirect("AssetMovementReportDtls.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        FillReqIds(txtSearch.Text)
    End Sub

    Protected Sub btnInterSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInterSearch.Click
        FillInterMVMTRequests(txtInterSearch.Text)

    End Sub

    Protected Sub gvInterMVMT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvInterMVMT.PageIndexChanging
        gvInterMVMT.PageIndex = e.NewPageIndex
        FillInterMVMTRequests(txtInterSearch.Text)
    End Sub

    Protected Sub gvInterMVMT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvInterMVMT.RowCommand
        If e.CommandName = "Details" Then
            Response.Redirect("InterAssetMovementReportDtls.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub
End Class
