<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Mapped_Unmapped_AstDisposeDTLS.ascx.vb" Inherits="Controls_Mapped_Unmapped_AstDisposeDTLS" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="col-sm-3 control-label"><strong>Asset Name</strong></label>
            <asp:Label ID="lblAstName" CssClass="col-sm-3 control-label " runat="server"></asp:Label>
            <asp:Label ID="lblAstCode" Visible="false" CssClass="control-label" runat="server"></asp:Label>

            <%-- <label class="col-sm-3 control-label"><strong>Remarks</strong></label>
            <div class="col-sm-3">
                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
            </div>--%>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="col-sm-3 control-label"><strong>Model Name</strong></label>
            <asp:Label ID="lblModelName" CssClass="col-sm-3 control-label" runat="server"></asp:Label>

            <label class="col-sm-3 control-label"><strong>Location</strong></label>
            <asp:Label ID="lblLocation" CssClass="col-sm-3 control-label" runat="server"></asp:Label>
        </div>
    </div>
</div>

<div id="trAstMapped" runat="server">

    <%--    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-sm-3 control-label"><strong>Employee Id</strong></label>
                <asp:Label ID="lblEmpAstTaggedId" Visible="false" CssClass="control-label" runat="server"></asp:Label>
                <asp:Label ID="lblEmpId" CssClass="control-label" runat="server"></asp:Label>

                <label class="col-sm-3 control-label"><strong>Employee Name</strong></label>
                <asp:Label ID="lblEmpName" CssClass="control-label" runat="server"></asp:Label>
            </div>
        </div>
    </div>--%>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-sm-3 control-label"><strong>Employee Id</strong></label>
                <asp:Label ID="lblEmpAstTaggedId" Visible="false" CssClass="col-sm-3 control-label" runat="server"></asp:Label>
                <asp:Label ID="lblEmpId" CssClass="control-label" runat="server"></asp:Label>

                 <label class="col-sm-3 control-label"><strong>Employee Name</strong></label>
                <asp:Label ID="lblEmpName" CssClass="col-sm-3 control-label" runat="server"></asp:Label>
              
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
               
                  <label class="col-sm-3 control-label"><strong>Email</strong></label>
                <asp:Label ID="lblEmpEmail" CssClass="control-label" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">

            <div class="col-sm-6"></div>
            <label class="col-sm-3 control-label"><strong>Remarks</strong></label>
            <div class="col-sm-3">
                <asp:TextBox ID="txtRemarks" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <asp:Button ID="btnDispose" OnClientClick="javascript:return confirm('Do you want to dispose this Asset ?');" runat="server" Text="Dispose" CssClass="btn btn-primary custom-button-color" />
        <asp:Button ID="btnCancel" Visible="false" runat="server" Text="Cancel" CssClass="btn btn-primary custom-button-color" />
    </div>
</div>
