<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewSurrenderRequisitions_admin.ascx.vb" Inherits="Controls_ViewSurrenderRequisitions_admin" %>
<div class="row" style="margin-top: 10px">
   <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvSurrenderAstReq" runat="server" EmptyDataText="No Approved Surrender Requisition Found." AllowPaging="true" PageSize="10"
                CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Requesition ID" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                             <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("SREQ_ID", "~/FAM/FAM_WebFiles/frmUpdateAdminSurrenderRequisitions.aspx?Req_id={0}")%>'
                                 Text='<%# Eval("SREQ_ID")%> '></asp:HyperLink>
                            <asp:Label ID="lblSREQ_ID" runat="server" Text='<%#Eval("SREQ_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                   <%-- <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAAT_AST_CODE" runat="server" Text='<%#Eval("AAT_AST_CODE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender Date" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSurrenderDate" runat="server" Text='<%#Eval("AAT_SURRENDERED_DATE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender Status" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSurStatus" runat="server" Text='<%#Eval("STA_TITLE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
              <%--      <asp:TemplateField HeaderText="View" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkView" runat="server" CommandArgument='<%#Eval("SREQ_ID") %>'
                                CommandName="View"> View</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
