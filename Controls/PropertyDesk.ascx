<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PropertyDesk.ascx.vb"
    Inherits="Controls_PropertyDesk" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Search by Tenant Code/Tenant Name/Property Name <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvCode"
                    runat="server" ErrorMessage="Search by Tenant Code/Tenant Name/Property Name " ControlToValidate="txtEMPID"
                    Display="none" ValidationGroup="Val2"></asp:RequiredFieldValidator>

                <div class="col-md-7">
                    <asp:TextBox ID="txtEMPID" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="row">
                    <div class="col-md-5 control-label">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val2"></asp:Button>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="panel1" runat="server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Tenant Found." AutoGenerateColumns="false"
                CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" PageSize="5">
                <Columns>
                    <asp:TemplateField Visible="false" HeaderText="SNO">
                        <ItemTemplate>
                            <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Property Code">
                        <ItemTemplate>
                            <asp:Label ID="lblpropcode" runat="server" Text='<%#Eval("PN_CODE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Property Name">
                        <ItemTemplate>
                            <asp:Label ID="lblpropname" runat="server" Text='<%#Eval("PN_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tenant Code" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbltenantcode" runat="server" Text='<%#Eval("TEN_CODE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tenant Name">
                        <ItemTemplate>
                            <asp:Label ID="lbltenant" runat="server" Text='<%#Eval("TEN_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnksurrender" runat="server" Text="Surrender" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Surrender"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="View Details">
                        <ItemTemplate>
                            <a href="#" onclick="showPopWin('<%# Eval("TEN_CODE") %>')">
                                <asp:Label ID="lbltencode" runat="server" Text="View Details"></asp:Label>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div id="panel2" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Property Code
                    </label>
                    <asp:RequiredFieldValidator ID="rfvprpcode" runat="server" ControlToValidate="txtpropertycode" Display="None" ErrorMessage="Please Enter Property Code"
                        ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtpropertycode" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:HiddenField ID="hfsno" runat="server" />
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Property Name</label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPropertyName" Display="None" ErrorMessage="Please Enter Property Code"
                        ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtPropertyName" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>

                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Surrendered Date </label>
                    <asp:RequiredFieldValidator ID="rfvDesgcode" runat="server" ControlToValidate="txtSdate" Display="None" ErrorMessage="Please Pick Date"
                        ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtSdate" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label" runat="server" id="Label1">
                        Rent Amount
                    </label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtpropertycode" Display="None" ErrorMessage="Please Enter Property Code"
                        ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtRent" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label" runat="server" id="Label2">
                        Rent Amount Paid</label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtpropertycode" Display="None" ErrorMessage="Please Enter Property Code"
                        ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtRentPaid" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label" runat="server" id="Label3">
                        Security Deposit</label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtpropertycode" Display="None" ErrorMessage="Please Enter Property Code"
                        ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtSecurityDeposit" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label" runat="server" id="lblClosingBalMsg">
                        Clearance Amount 
                    </label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpropertycode" Display="None" ErrorMessage="Please Enter Property Code"
                        ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtTenClosingBal" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>



    </div>
    <div class="row">
        <label class="col-md-6 control-label" runat="server" id="Label4">
            Clearance Amount = Security Deposit - (Total Rent - Rent Paid Till Date)
        </label>

    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnApprove" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"
                    CausesValidation="true" />

            </div>
        </div>
    </div>
</div>
