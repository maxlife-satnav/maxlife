<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DisposableAssetsRequisitionDTLS.ascx.vb"
    Inherits="Controls_DisposableAssetsRequisitionDTLS" %>


<div class="row">
   
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Name:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstName" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
     <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Model Name:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblModelName" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Allocated to Employee Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstAllocDt" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Surrender Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstSurDt" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Surrender Requesition Id:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblSurReq_id" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <fieldset>
            <legend>Admin Comments </legend>
        </fieldset>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Admin Approval Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAdminDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Approved By (Admin):</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAdminName" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Admin Remarks:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAdminRemarks" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Salvage Value:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstSalvage" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnApprov" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
