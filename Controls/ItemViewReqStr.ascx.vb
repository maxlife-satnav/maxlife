Imports System.Data

Partial Class Controls_ItemViewReqStr
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillgrid()
        End If
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_SMAPPROVAL")
        sp.Command.AddParameter("@Cuser", Session("Uid"), Data.DbType.String)
        gvItemStock.DataSource = sp.GetDataSet()
        gvItemStock.DataBind()
    End Sub

    Protected Sub gvItemStock_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemStock.PageIndexChanging
        gvItemStock.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqIdSM")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", 1032, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    'send_mail_reject(Request.QueryString("RID"))
    '   For Each row As GridViewRow In gvItemStock.Rows
    '        If row.RowType = DataControlRowType.DataRow Then
    '            Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
    '            Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text

    '            If bf.Checked Then
    '                RejectRequest(id, txtSTRem.Text)
    '            End If
    '        End If
    '    Next


    'End Sub
    'Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
    '    For Each row As GridViewRow In gvItemStock.Rows
    '        If row.RowType = DataControlRowType.DataRow Then
    '            Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
    '            Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text

    '            If bf.Checked Then
    '                UpdateData(id, txtSTRem.Text)
    '            End If
    '        End If
    '    Next
    'End Sub
    Private Sub RejectRequest(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqIdRMCANCEL")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", 1033, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
End Class
