Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.IO
Imports System.Threading
Imports System.Data.OleDb


Partial Class Controls_AddAsset
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtIncDate.Attributes.Add("readonly", "readonly")
        txtAMCDate.Attributes.Add("readonly", "readonly")
        txtPDate.Attributes.Add("readonly", "readonly")
        txtMfgDate.Attributes.Add("readonly", "readonly")
        txtWarDate.Attributes.Add("readonly", "readonly")

        If Not IsPostBack Then
            getassetcategories()
            'getassetbrand()
            getlocation()
            'getassetcategory()
            getvendors()
            getAssetTypes()
            ddlAssetType.SelectedIndex = 1
            gvBrand.Visible = True
            fillgrid()
            rbtamc()
            rbtIns()
            ''txtIncDate.Attributes.Add("onClick", "displayDatePicker('" + txtIncDate.ClientID + "')")
            ''txtIncDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtAMCDate.Attributes.Add("onClick", "displayDatePicker('" + txtAMCDate.ClientID + "')")
            ''txtAMCDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtPDate.Attributes.Add("onClick", "displayDatePicker('" + txtPDate.ClientID + "')")
            ''txtPDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtMfgDate.Attributes.Add("onClick", "displayDatePicker('" + txtMfgDate.ClientID + "')")
            ''txtMfgDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtWarDate.Attributes.Add("onClick", "displayDatePicker('" + txtWarDate.ClientID + "')")
            ''txtWarDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ddlAstBrand.Items.Insert(0, "--Select--")
            ddlAstSubCat.Items.Insert(0, "--Select--")
            ddlModel.Items.Insert(0, "--Select--")
            ddlLocation.Items.Insert(0, "--Select--")
            ddlTower.Items.Insert(0, "--Select--")
            ddlFloor.Items.Insert(0, "--Select--")
            'pnlAst.Visible = True
            ' getassettaxtype()
        End If
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSET_DETAILS")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int16)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvBrand.DataSource = ds
        gvBrand.DataBind()


        'For i As Integer = 0 To gvBrand.Rows.Count - 1
        '    Dim lblstatus As Label = CType(gvBrand.Rows(i).FindControl("lblstatus"), Label)
        '    If lblstatus.Text = "1" Then
        '        lblstatus.Text = "Active"
        '    Else
        '        lblstatus.Text = "InActive"
        '    End If
        'Next

    End Sub
    Protected Sub gvBrand_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvBrand.PageIndexChanging
        gvBrand.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    'Private Sub getassetbrand()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETBRAND")
    '    sp.Command.AddParameter("@dummy", 1, DbType.Int16)
    '    ddlBrand.DataSource = sp.GetDataSet()
    '    ddlBrand.DataTextField = "manufacturer"
    '    ddlBrand.DataValueField = "manufacturerID"
    '    ddlBrand.DataBind()
    '    ddlBrand.Items.Insert(0, New ListItem("--Select--", "0"))
    'End Sub
    Private Sub getassetcategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
    End Sub
    Private Sub getassetcategory(ven_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CATBYVENDORS_NOTCONS")
        sp.Command.AddParameter("@VT_CODE", ven_code, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
    End Sub
    'Private Sub getassettaxtype()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETTAXTYPE")
    '    sp.Command.AddParameter("@dummy", 1, DbType.Int16)
    '    ddlTaxTypeID.DataSource = sp.GetDataSet()
    '    ddlTaxTypeID.DataTextField = "taxtype"
    '    ddlTaxTypeID.DataValueField = "taxtypeid"
    '    ddlTaxTypeID.DataBind()
    '    ddlTaxTypeID.Items.Insert(0, New ListItem("--Select--", "0"))

    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text = "Add" Then
            ltrAMC.Visible = False
            litINC.Visible = False
            Dim orgfilenameAMC As String = Replace(Replace(fpBrowseAMCDoc.FileName, " ", "_"), "&", "_")
            Dim orgfilenameINC As String = Replace(Replace(fpBrowseIncDoc.FileName, " ", "_"), "&", "_")
            Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
            Dim repdocdatetimeAMC As String = ""
            Dim repdocdatetimeINC As String = ""
            Try
                If (fpBrowseAMCDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseAMCDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_AMC_" & orgfilenameAMC
                    fpBrowseAMCDoc.PostedFile.SaveAs(filePath)
                    'lblMsg.Visible = True
                    'lblMsg.Text = "File upload successfully"
                    repdocdatetimeAMC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "AMC_" & orgfilenameAMC

                End If
                If (fpBrowseIncDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseIncDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_INC_" & orgfilenameINC
                    fpBrowseIncDoc.PostedFile.SaveAs(filePath)
                    'lblMsg.Visible = True
                    'lblMsg.Text = "File upload successfully"
                    repdocdatetimeINC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "INC_" & orgfilenameINC
                    'Else

                    '    lblMsg.Visible = True
                    '    lblMsg.Text = "Select the file"
                End If

                'If (fpBrowseAMCDoc.HasFile) And (fpBrowseIncDoc.HasFile) Then
                ' getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
                Dim asttype As Integer = checkassettype()
                If asttype = 1 Then
                    'Consumable
                    pnlAst.Visible = False
                Else
                    'Not
                    insertnewrecord(repdocdatetimeAMC, repdocdatetimeINC)
                    InsertAssetSpace(txtAssetCode.Text, ddlLocation.SelectedItem.Value, ddlTower.SelectedValue, ddlTower.SelectedValue, 0, "MANUAL_UPLOAD_MASTER")

                End If

                lblMsg.Visible = True
                lblMsg.Text = "New Asset Added Successfully..."
                ' cleardata()
                'Else
                '    lblMsg.Visible = True
                '    lblMsg.Text = "Select the file"
                'End If


            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            'Dim ValidateCode As Integer
            'ValidateCode = ValidateAsset(txtAssetCode.Text)
            'If ValidateCode = 0 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Asset Code already exist please enter another Asset Code..."
            'ElseIf ValidateCode = 1 Then
            'insertnewrecord()
            'fillgrid()

            'End If
        Else
            Dim orgfilenameAMC As String = Replace(Replace(fpBrowseAMCDoc.FileName, " ", "_"), "&", "_")
            Dim orgfilenameINC As String = Replace(Replace(fpBrowseIncDoc.FileName, " ", "_"), "&", "_")
            Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
            Dim repdocdatetimeAMC As String = ""
            Dim repdocdatetimeINC As String = ""
            If ddlAsset.SelectedIndex <> 0 Then
                If (fpBrowseAMCDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseAMCDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_AMC_" & orgfilenameAMC

                    fpBrowseAMCDoc.PostedFile.SaveAs(filePath)
                    'lblMsg.Visible = True
                    'lblMsg.Text = "File upload successfully"
                    repdocdatetimeAMC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "AMC_" & orgfilenameAMC
                End If
                If (fpBrowseIncDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseIncDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_INC_" & orgfilenameINC
                    fpBrowseIncDoc.PostedFile.SaveAs(filePath)
                    'lblMsg.Visible = True
                    'lblMsg.Text = "File upload successfully"
                    repdocdatetimeINC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "INC_" & orgfilenameINC
                    'Else

                    '    lblMsg.Visible = True
                    '    lblMsg.Text = "Select the file"
                End If
                modifydata(repdocdatetimeAMC, repdocdatetimeINC)

            End If

        End If
    End Sub

    Private Sub modifydata(fpamcdocfilename As String, fpincdocfilename As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_AMG_ASSET_UPDATE_Master")
            sp.Command.AddParameter("@AAT_CODE", txtAssetCode.Text, DbType.String)
            sp.Command.AddParameter("@AAT_NAME", txtAssetName.Text, DbType.String)
            sp.Command.AddParameter("@AAT_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_MODEL_ID", ddlModel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_MODEL_NAME", ddlModel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AAG_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_SUB_CODE", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AAB_CODE", ddlAstBrand.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AMC_REQD", rdAMC.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AAT_AMC_DATE", txtAMCDate.Text, DbType.Date)
            sp.Command.AddParameter("@AAT_AMC_DOCPATH", fpamcdocfilename, DbType.String)
            sp.Command.AddParameter("@AAT_INS_REQD", rdIns.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AAT_INS_DATE", txtIncDate.Text, DbType.Date)
            sp.Command.AddParameter("@AAT_INS_DOCPATH", fpincdocfilename, DbType.String)
            sp.Command.AddParameter("@AAT_AST_COST", txtAssetPrice.Text, DbType.Double)
            sp.Command.AddParameter("@AAT_AST_SERIALNO", txtAssetSerialNo.Text, DbType.String)
            sp.Command.AddParameter("@AAT_AST_SLVGVAL", txtSalvgValue.Text, DbType.Double)
            sp.Command.AddParameter("@AAT_UPT_DT", getoffsetdatetime(DateTime.Now), DbType.DateTime)
            sp.Command.AddParameter("@AAT_UPT_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@AAT_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AAT_OWNED", 1, DbType.Int32)
            sp.Command.AddParameter("@AAT_PURCHASED_STATUS", 1, DbType.Int32)
            sp.Command.AddParameter("@AAT_SPC_FIXED", 1, DbType.Int32)
            sp.Command.AddParameter("@AAT_USR_MOVABLE", 0, DbType.Int32)
            sp.Command.AddParameter("@AAT_AST_CONS", 0, DbType.Int32)
            sp.Command.AddParameter("@AAT_DESC", txtAssetDescription.Text, DbType.String)
            sp.Command.AddParameter("@CONF_TYPE", "", DbType.String)
            sp.Command.AddParameter("@AAT_MFG_DATE", txtMfgDate.Text, DbType.Date)
            sp.Command.AddParameter("@AAT_WRNTY_DATE", txtWarDate.Text, DbType.Date)
            sp.Command.AddParameter("@PURCHASEDDATE", txtPDate.Text, DbType.Date)
            sp.Command.AddParameter("@LIFESPAN", txtLifeSpan.Text, DbType.String)
            sp.Command.AddParameter("@PONUMBER", txtPONumber.Text, DbType.String)
            sp.Command.AddParameter("@DEPRECIATION", txtDepriciation.Text, DbType.Double)
            sp.Command.AddParameter("@ASTYPE", ddlAssetType.SelectedValue, DbType.Int32)
            sp.Command.AddParameter("@AAT_TOWER_CODE", ddlTower.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_FLOOR_CODE", ddlFloor.SelectedItem.Value, DbType.String)

            sp.ExecuteScalar()
            lblMsg.Visible = True
            lblMsg.Text = "Asset Modified Successfully..."
            fillgrid()
            cleardata()
            ddlAsset.SelectedIndex = 0

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub InsertAssetSpace(ByVal AstCode As String, ByVal location As String, ByVal Tower As String, ByVal Floor As String, ByVal ProcType As Integer, ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMT_ASSET_SPACE_INSERT")
        sp.Command.AddParameter("@AAS_AAT_CODE", AstCode, DbType.String)
        sp.Command.AddParameter("@AAS_LOC_ID", location, DbType.String)
        sp.Command.AddParameter("@AAS_TWR_ID", Tower, DbType.String)
        sp.Command.AddParameter("@AAS_FLR_ID", Floor, DbType.String)
        sp.Command.AddParameter("@AAS_BDG_ID", "", DbType.String)
        sp.Command.AddParameter("@AAS_PROC_TYPE", ProcType, DbType.String)
        sp.Command.AddParameter("@AAS_REMARKS", "MANUAL_UPLOAD_MASTER", DbType.String)
        sp.Command.AddParameter("@AAS_REQ_ID", reqid, DbType.String)
        sp.ExecuteScalar()
    End Sub
    'Private Sub modifydata()
    '    Try
    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MODIFY_ASSETDATA")
    '        sp1.Command.AddParameter("@productid", ddlAsset.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@sku", txtAssetCode.Text, DbType.String)
    '        sp1.Command.AddParameter("@productName", txtAssetName.Text, DbType.String)
    '        sp1.Command.AddParameter("@shortDescription", txtAssetCode.Text, DbType.String)
    '        sp1.Command.AddParameter("@ourprice", txtSalvgValue.Text, DbType.Currency)
    '        sp1.Command.AddParameter("@retailPrice", txtAssetPrice.Text, DbType.Currency)
    '        sp1.Command.AddParameter("@manufacturerID", ddlBrand.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@statusid", ddlStatus.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@productTypeID", ddlAssetCategory.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@taxTypeID", ddlTaxTypeID.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@stockLocation", txtStkLocation.Text, DbType.String)
    '        sp1.Command.AddParameter("@adminComments", txtAdmCmnts.Text, DbType.String)
    '        sp1.Command.AddParameter("@createdBy", Session("Uid"), DbType.String)
    '        sp1.ExecuteScalar()
    '        ' fillgrid()
    '        lblMsg.Visible = True
    '        lblMsg.Text = "Asset Modified Successfully..."
    '        cleardata()
    '        ddlAsset.SelectedIndex = 0
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    Public Function ValidateAsset(ByVal assetcode As String)
        Dim ValidateCode As Integer
        'Dim PN_PROPERTYTYPE As String = brandcode
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_VALIDATE_ASSET")
        sp1.Command.AddParameter("@sku", assetcode, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function

    Public Sub insertnewrecord(fpamcdocfilename As String, fpincdocfilename As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_AMG_ASSET_Insert_Master")
            sp.Command.AddParameter("@AAT_CODE", txtAssetCode.Text, DbType.String)
            ' sp.Command.AddParameter("@AAT_NAME", txtAssetName.Text, DbType.String)
            sp.Command.AddParameter("@AAT_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_MODEL_ID", ddlModel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_MODEL_NAME", ddlModel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AAG_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_SUB_CODE", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AAB_CODE", ddlAstBrand.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_AMC_REQD", rdAMC.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AAT_AMC_DATE", txtAMCDate.Text, DbType.String)
            sp.Command.AddParameter("@AAT_AMC_DOCPATH", fpamcdocfilename, DbType.String)
            sp.Command.AddParameter("@AAT_INS_REQD", rdIns.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AAT_INS_DATE", txtIncDate.Text, DbType.String)
            sp.Command.AddParameter("@AAT_INS_DOCPATH", fpincdocfilename, DbType.String)
            sp.Command.AddParameter("@AAT_AST_COST", txtAssetPrice.Text, DbType.Double)
            sp.Command.AddParameter("@AAT_AST_SERIALNO", txtAssetSerialNo.Text, DbType.String)
            sp.Command.AddParameter("@AAT_AST_SLVGVAL", txtSalvgValue.Text, DbType.Double)
            sp.Command.AddParameter("@AAT_UPT_DT", getoffsetdatetime(DateTime.Now), DbType.DateTime)
            sp.Command.AddParameter("@AAT_UPT_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@AAT_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AAT_OWNED", 1, DbType.Int32)
            sp.Command.AddParameter("@AAT_PURCHASED_STATUS", 1, DbType.Int32)
            sp.Command.AddParameter("@AAT_SPC_FIXED", 1, DbType.Int32)
            sp.Command.AddParameter("@AAT_USR_MOVABLE", 0, DbType.Int32)
            sp.Command.AddParameter("@AAT_AST_CONS", 0, DbType.Int32)
            sp.Command.AddParameter("@AAT_DESC", txtAssetDescription.Text, DbType.String)
            sp.Command.AddParameter("@CONF_TYPE", "", DbType.String)
            sp.Command.AddParameter("@AAT_MFG_DATE", txtMfgDate.Text, DbType.Date)
            sp.Command.AddParameter("@AAT_WRNTY_DATE", txtWarDate.Text, DbType.Date)
            sp.Command.AddParameter("@PURCHASEDDATE", txtPDate.Text, DbType.Date)
            sp.Command.AddParameter("@LIFESPAN", txtLifeSpan.Text, DbType.String)
            sp.Command.AddParameter("@PONUMBER", txtPONumber.Text, DbType.String)
            sp.Command.AddParameter("@DEPRECIATION", txtDepriciation.Text, DbType.Double)
            sp.Command.AddParameter("@ASTYPE", ddlAssetType.SelectedValue, DbType.Int32)
            sp.Command.AddParameter("@AAT_TOWER_CODE", ddlTower.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AAT_FLOOR_CODE", ddlFloor.SelectedItem.Value, DbType.String)

            sp.ExecuteScalar()
            'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_INSERT_ASSETDATA")
            'sp1.Command.AddParameter("@sku", txtAssetCode.Text, DbType.String)
            'sp1.Command.AddParameter("@productName", txtAssetName.Text, DbType.String)
            'sp1.Command.AddParameter("@shortDescription", txtAssetCode.Text, DbType.String)
            'sp1.Command.AddParameter("@ourprice", txtSalvgValue.Text, DbType.Currency)
            'sp1.Command.AddParameter("@retailPrice", txtAssetPrice.Text, DbType.Currency)
            'sp1.Command.AddParameter("@manufacturerID", ddlBrand.SelectedItem.Value, DbType.Int32)
            'sp1.Command.AddParameter("@statusid", ddlStatus.SelectedItem.Value, DbType.Int32)
            'sp1.Command.AddParameter("@productTypeID", ddlAssetCategory.SelectedItem.Value, DbType.Int32)
            'sp1.Command.AddParameter("@taxTypeID", ddlTaxTypeID.SelectedItem.Value, DbType.Int32)
            'sp1.Command.AddParameter("@stockLocation", txtStkLocation.Text, DbType.String)
            'sp1.Command.AddParameter("@adminComments", txtAdmCmnts.Text, DbType.String)
            'sp1.Command.AddParameter("@createdBy", Session("Uid"), DbType.String)
            'sp1.ExecuteScalar()
            fillgrid()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
                    ddlAsset.Visible = False
                    lblAsset.Visible = False
                    cleardata()
            btnSubmit.Text = "Add"
            getassetcategories()
            ddlAstBrand.Items.Insert(0, "--Select--")
            ddlAstSubCat.Items.Insert(0, "--Select--")
            ddlModel.Items.Insert(0, "--Select--")
            '  ddlLocation.Items.Insert(0, "--Select--")
                    lblMsg.Visible = False
                    txtAssetCode.Enabled = True
                    ddlVendor.Enabled = True
                    ddlAssetCategory.Enabled = True
                    ddlAstSubCat.Enabled = True
                    ddlAstBrand.Enabled = True
                    ddlModel.Enabled = True
            ddlLocation.Enabled = True
            ddlTower.Enabled = True
            ddlFloor.Enabled = True
            txtAssetPrice.Enabled = True
            txtAssetName.Enabled = True
            ddlAssetType.Enabled = True
                    ' fillgrid()
        Else
            ddlAsset.Visible = True
            lblAsset.Visible = True
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            txtAssetCode.Enabled = False
            getasset()
            ddlVendor.Enabled = False
            ddlAssetCategory.Enabled = False
            ddlAstSubCat.Enabled = False
            ddlAstBrand.Enabled = False
            ddlModel.Enabled = False
            ddlLocation.Enabled = False
            ddlTower.Enabled = False
            ddlFloor.Enabled = False
            txtAssetPrice.Enabled = False
            txtAssetName.Enabled = False
            ddlAssetType.Enabled = False
            'rbn1.Visible = True
            'rbn2.Visible = True

            ddlAssetType.SelectedIndex = 1

            'fillgrid()
                End If
            
    End Sub

    Private Sub getasset()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GETALL_ASSET")
        sp.Command.AddParameter("@dummy", 1, DbType.Int16)
        ddlAsset.DataSource = sp.GetDataSet()
        ddlAsset.DataTextField = "AAT_NAME"
        ddlAsset.DataValueField = "AAT_CODE"
        ddlAsset.DataBind()
        ddlAsset.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAsset.SelectedIndexChanged
        Try
            If ddlAsset.SelectedIndex <> 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_ALLASSETDATA")
                sp.Command.AddParameter("@AAT_ID", ddlAsset.SelectedItem.Value, DbType.String)
                Dim ds As New DataSet
                ds = sp.GetDataSet
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim cat_code As String = ds.Tables(0).Rows(0).Item("AAT_AAG_CODE")
                    Dim subcat_code As String = ds.Tables(0).Rows(0).Item("AAT_SUB_CODE")
                    Dim brand_code As String = ds.Tables(0).Rows(0).Item("AAT_AAB_CODE")
                    Dim model_code As String = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
                    Dim lcmCode As String = ds.Tables(0).Rows(0).Item("AAS_LOC_ID")
                    Dim twrCode As String = ds.Tables(0).Rows(0).Item("AAT_TOWER_CODE")
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_STA_ID")).Selected = True
                    ddlVendor.ClearSelection()
                    ddlVendor.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AVR_CODE")).Selected = True
                    ' getassetcategory(ds.Tables(0).Rows(0).Item("AAT_AVR_CODE"))
                    getassetcategories()
                    ddlAssetCategory.ClearSelection()
                    ddlAssetCategory.Items.FindByValue(cat_code).Selected = True
                    getsubcategorybycat(cat_code)
                    ddlAstSubCat.ClearSelection()
                    ddlAstSubCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_SUB_CODE")).Selected = True
                    getbrandbycatsubcat(cat_code, subcat_code)
                    ddlAstBrand.ClearSelection()
                    ddlAstBrand.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AAB_CODE")).Selected = True
                    getmakebycatsubcat(cat_code, subcat_code, brand_code)
                    ddlModel.ClearSelection()
                    ddlModel.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")).Selected = True
                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAS_LOC_ID")).Selected = True                   
                    txtAssetName.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
                    txtAssetCode.Text = ds.Tables(0).Rows(0).Item("AAT_CODE")
                    txtAssetDescription.Text = ds.Tables(0).Rows(0).Item("AAT_DESC")
                    txtSalvgValue.Text = ds.Tables(0).Rows(0).Item("AAT_AST_SLVGVAL")
                    txtAssetPrice.Text = ds.Tables(0).Rows(0).Item("AAT_AST_COST")
                    txtAssetSerialNo.Text = ds.Tables(0).Rows(0).Item("AAT_AST_SERIALNO")
                    txtAMCDate.Text = ds.Tables(0).Rows(0).Item("AAT_AMC_DATE")
                    txtIncDate.Text = ds.Tables(0).Rows(0).Item("AAT_INS_DATE")
                    txtMfgDate.Text = ds.Tables(0).Rows(0).Item("AAT_MFG_DATE")
                    txtPDate.Text = ds.Tables(0).Rows(0).Item("PURCHASEDDATE")
                    txtWarDate.Text = ds.Tables(0).Rows(0).Item("AAT_WRNTY_DATE")
                    txtLifeSpan.Text = ds.Tables(0).Rows(0).Item("AAT_LIFE_SPAN")
                    txtDepriciation.Text = ds.Tables(0).Rows(0).Item("AAT_DEPRICIATION")
                    txtPONumber.Text = ds.Tables(0).Rows(0).Item("AAT_PO_NUMBER")
                    ddlAssetType.ClearSelection()
                    ddlAssetType.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AST_STATUS")).Selected = True
                    getTowersByLocations(lcmCode)
                    ddlTower.ClearSelection()
                    ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_TOWER_CODE")).Selected = True
                    getFloorsByLocationTower(lcmCode, twrCode)
                    ddlFloor.ClearSelection()
                    ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_FLOOR_CODE")).Selected = True
                    rdAMC.ClearSelection()
                    rdAMC.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AMC_REQD")).Selected = True
                    rbtamc()
                    rdIns.ClearSelection()
                    rdIns.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_INS_REQD")).Selected = True
                    rbtIns()
                    If rdIns.SelectedValue = 0 Then     'ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH") = "" Or 
                        hplINC.Visible = False
                        rbnInsDate.Visible = False
                        rbnInsUpload.Visible = False
                    Else
                        rbnAmcDate.Visible = False
                        rbnAmcUpload.Visible = False
                        hplINC.Visible = False
                        hplINC.Text = "Click to view document"
                        ' hplINC.NavigateUrl = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH")
                        hplINC.Visible = False
                        litINC.Visible = True
                        litINC.Text = "<a download href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH") & "' >Click to download document</a>"
                    End If
                    If rdAMC.SelectedValue = 0 Then     'ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") = "" Or
                        hplAMC.Visible = False
                        rbnAmcDate.Visible = False
                        rbnAmcUpload.Visible = False
                    Else
                        rbnAmcDate.Visible = True
                        rbnAmcUpload.Visible = True
                        hplAMC.Visible = False
                        hplAMC.Text = "Click to view Image"
                        ' hplAMC.NavigateUrl = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH")
                        Dim filename As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH")
                        ' hplAMC.Target = "_blank"
                        ltrAMC.Visible = True
                        'ltrAMC.Text = "<a href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") & "' >Click to view document</a>"
                        ltrAMC.Text = "<a download href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") & "' >Click to download image</a>"
                        'fileDownload(ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH"), Server.MapPath("~/UploadFiles/"))
                    End If
                    ' txtAdmCmnts.Text = ds.Tables(0).Rows(0).Item("adminComments")AAS_LOC_ID
                    'txtStkLocation.Text = ds.Tables(0).Rows(0).Item("stockLocation")
                    'ddlBrand.ClearSelection()
                    'ddlBrand.Items.FindByValue(ds.Tables(0).Rows(0).Item("manufacturerID")).Selected = True
                    'ddlAssetCategory.ClearSelection()
                    'ddlAssetCategory.Items.FindByValue(ds.Tables(0).Rows(0).Item("productTypeID")).Selected = True
                    'ddlTaxTypeID.ClearSelection()
                    'ddlTaxTypeID.Items.FindByValue(ds.Tables(0).Rows(0).Item("taxTypeID")).Selected = True
                End If
            Else
                cleardata()

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub fileDownload(fileName As String, fileUrl As String)
        Try
            Page.Response.Clear()
            Dim success As Boolean = ResponseFile(Page.Request, Page.Response, fileName, fileUrl, 1024000)
            If Not success Then
                Response.Write("Downloading Error!")
            End If
            Page.Response.[End]()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Shared Function ResponseFile(_Request As HttpRequest, _Response As HttpResponse, _fileName As String, _fullPath As String, _speed As Long) As Boolean
        Try
            Dim myFile As New FileStream(_fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            Dim br As New BinaryReader(myFile)
            Try
                _Response.AddHeader("Accept-Ranges", "bytes")
                _Response.Buffer = False
                Dim fileLength As Long = myFile.Length
                Dim startBytes As Long = 0

                Dim pack As Integer = 10240
                '10K bytes
                Dim sleep As Integer = CInt(Math.Floor(CDbl(1000 * pack / _speed))) + 1
                If _Request.Headers("Range") IsNot Nothing Then
                    _Response.StatusCode = 206
                    Dim range As String() = _Request.Headers("Range").Split(New Char() {"="c, "-"c})
                    startBytes = Convert.ToInt64(range(1))
                End If
                _Response.AddHeader("Content-Length", (fileLength - startBytes).ToString())
                If startBytes <> 0 Then
                    _Response.AddHeader("Content-Range", String.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength))
                End If
                _Response.AddHeader("Connection", "Keep-Alive")
                _Response.ContentType = "application/octet-stream"
                _Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(_fileName, System.Text.Encoding.UTF8))

                br.BaseStream.Seek(startBytes, SeekOrigin.Begin)
                Dim maxCount As Integer = CInt(Math.Floor(CDbl((fileLength - startBytes) / pack))) + 1

                For i As Integer = 0 To maxCount - 1
                    If _Response.IsClientConnected Then
                        _Response.BinaryWrite(br.ReadBytes(pack))
                        Thread.Sleep(sleep)
                    Else
                        i = maxCount
                    End If
                Next
            Catch
                Return False
            Finally
                br.Close()
                myFile.Close()
            End Try
        Catch
            Return False
        End Try
        Return True
    End Function

    Private Sub cleardata()
        ' txtAdmCmnts.Text = ""
        txtAssetCode.Text = ""
        txtAssetDescription.Text = ""
        txtAssetName.Text = ""
        txtAssetPrice.Text = ""
        txtSalvgValue.Text = ""
        txtMfgDate.Text = ""
        txtWarDate.Text = ""
        txtAMCDate.Text = ""
        txtPDate.Text = ""
        txtIncDate.Text = ""
        ddlVendor.SelectedIndex = 0
        ddlAssetCategory.Items.Clear()
        ddlAstSubCat.Items.Clear()
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        ddlLocation.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        ltrAMC.Visible = False
        litINC.Visible = False
        ltrAMC.Text = ""
        litINC.Text = ""
        txtAssetSerialNo.Text = ""
        txtLifeSpan.Text = ""
        txtDepriciation.Text = ""
        txtPONumber.Text = ""
        ddlAssetType.SelectedIndex = 0
        rdIns.SelectedValue = 1
        rdAMC.SelectedValue = 1
        'txtAMCDate.Visible = True
        'txtIncDate.Visible = True
        'fpBrowseAMCDoc.Visible = True
        'fpBrowseIncDoc.Visible = True
        rbtamc()
        rbtIns()



        'ddlAsset.SelectedIndex = 0
        'ddlAssetCategory.SelectedIndex = 0
        'ddlBrand.SelectedIndex = 0
        'ddlStatus.SelectedIndex = 0
        'ddlTaxTypeID.SelectedIndex = 0


    End Sub

    Private Sub getlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
        sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ' ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub getAssetTypes()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSET_TYPES")
        ddlAssetType.DataSource = sp.GetReader
        ddlAssetType.DataTextField = "TAG_NAME"
        ddlAssetType.DataValueField = "TAG_CODE"
        ddlAssetType.DataBind()
        ddlAssetType.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        If ddlAssetCategory.SelectedItem.Value <> "--Select--" Then
            getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
            'Dim asttype As Integer = checkassettype()
            'If asttype = 1 Then
            '    'Consumable
            '    pnlAst.Visible = False
            'Else
            '    'Not
            '    pnlAst.Visible = True

            'End If
            pnlAst.Visible = True

        End If
    End Sub
    'Checking the asset type if it is consumable or non consumable
    Public Function checkassettype() As Integer
        Dim astconstatus As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_ASSETTYPE")
        sp.Command.AddParameter("@VT_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
        astconstatus = sp.ExecuteScalar()
        Return astconstatus
    End Function

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        If ddlAstSubCat.SelectedIndex <> 0 Then
            getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        End If
    End Sub

    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub

    Private Sub getTowersByLocations(ByVal lcmCode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_TOWER_BY_LOCATION")
        sp.Command.AddParameter("@LCM_CODE", lcmCode, DbType.String)
        'sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlTower.DataSource = sp.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub
    Private Sub getFloorsByLocationTower(ByVal lcmCode As String, ByVal twrCode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FLOOR_BY_LOC_TWR")
        sp.Command.AddParameter("@LCM_CODE", lcmCode, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrCode, DbType.String)
        ddlFloor.DataSource = sp.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex <> 0 Then
            getmakebycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value, ddlAstBrand.SelectedItem.Value)
        End If
    End Sub

    Private Sub getmakebycatsubcat(cat_code As String, subcat_code As String, brand_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", cat_code, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", subcat_code, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", brand_code, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        getTowersByLocations(ddlLocation.SelectedItem.Value)
        If ddlLocation.SelectedIndex <> 0 And ddlAssetCategory.SelectedIndex <> 0 And ddlAstSubCat.SelectedIndex <> 0 And ddlAstBrand.SelectedIndex <> 0 And ddlModel.SelectedIndex <> 0 Then
            Dim AstCode As String = ddlLocation.SelectedItem.Value + "/" + ddlAssetCategory.Text + "/" + ddlAstSubCat.Text + "/" + ddlAstBrand.Text + "/" + ddlModel.Text + "/" + CStr(GetMaxAssetId())
            txtAssetCode.Text = AstCode

        End If

    End Sub

    Private Function GetMaxAssetId() As Integer
        Dim AstId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ASSET_GetMaxAAT_ID")
        If Integer.TryParse(sp.ExecuteScalar, AstId) Then
        Else
            AstId = 0
        End If
        Return AstId
    End Function

    'Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendor.SelectedIndexChanged
    '    If ddlVendor.SelectedItem.Value <> "--Select--" Then
    '        getassetcategory(ddlVendor.SelectedItem.Value)
    '    End If
    'End Sub

    Private Sub getvendors()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_VENDORS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
        getassetprice()
    End Sub
    'Checking the asset type if it is consumable or non consumable
    Public Function getassetprice() As Decimal
        Dim getassetp As Decimal
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_VENDERRATE")
        sp.Command.AddParameter("@AMG_VENCAN_VENID", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCOT_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_BRID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_MDLID", ddlModel.SelectedItem.Value, DbType.String)
        getassetp = sp.ExecuteScalar()
        'Dim ds As New DataSet
        'ds = sp.ExecuteScalar()
        'If ds.Tables(0).Rows.Count - 1 > 0 Then
        '    getassetp = ds.Tables(0).Rows(0).Item("AMG_VENCATN_RT")
        'End If
        If getassetp = 0.0 Then
            getassetp = 0.0
            lblMsg.Visible = False
            txtAssetPrice.Text = ""
            lblMsg.Text = "There is no price added for this model, Please contact administrator..."
        Else
            txtAssetPrice.Text = getassetp
        End If

        Return getassetp
    End Function

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx")
    End Sub

    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1
                Dim locationid_cnt As Integer = 1
                Dim category_codecnt As Integer = 1
                Dim category_namecnt As Integer = 1
                Dim status_cnt As Integer = 1
                Dim remarks_cnt As Integer = 1
                'Dim dmn_idcnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "aat_avr_code" Then
                            locationid_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AVR_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "aat_aag_code" Then

                            category_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AAG_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "aat_sub_code" Then
                            category_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_SUB_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "aat_model_name" Then
                            status_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_MODEL_NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "aat_aab_code" Then
                            remarks_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AAB_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "aas_loc_id" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAS_LOC_ID"
                            Exit Sub
                    
                        ElseIf LCase(ds.Tables(0).Columns(7).ToString) <> "aat_ast_cost" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AST_COST"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(8).ToString) <> "purchaseddate" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be PURCHASEDDATE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(9).ToString) <> "aat_mfg_date" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_MFG_DATE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(10).ToString) <> "aat_name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(11).ToString) <> "aat_desc" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_DESC"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(12).ToString) <> "aat_ast_slvgval" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AST_SLVGVAL"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(13).ToString) <> "aat_ins_reqd" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_INS_REQD"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(14).ToString) <> "aat_amc_reqd" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AMC_REQD"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(15).ToString) <> "aat_amc_date" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AMC_DATE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(16).ToString) <> "aat_ins_date" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_INS_DATE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(17).ToString) <> "aat_wrnty_date" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_WRNTY_DATE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(18).ToString) <> "aat_sta_id" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_STA_ID"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(19).ToString) <> "aat_ast_serialno" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AST_SERIALNO"
                            Exit Sub

                        ElseIf LCase(ds.Tables(0).Columns(20).ToString) <> "aat_life_span" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_LIFE_SPAN"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(21).ToString) <> "aat_po_number" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_PO_NUMBER"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(22).ToString) <> "aat_depriciation" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_DEPRICIATION"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(23).ToString) <> "aat_ast_status" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_AST_STATUS"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(24).ToString) <> "aat_tower_code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_TOWER_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(25).ToString) <> "aat_floor_code" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be AAT_FLOOR_CODE"
                            Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "domain_head_id" Then
                            '    dmn_idcnt = 0
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be DOMAIN_HEAD_ID"
                            '    Exit Sub
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'Start inseting excel data
                    Dim ValidateData As Integer
                    'ValidateData = ValidateCategory(ds.Tables(0).Rows(i).Item("CATEGORY_CODE").ToString, ds.Tables(0).Rows(i).Item("LOCATION_ID").ToString)
                    'If ValidateData = 1 Then
                    Dim AstCode As String = ds.Tables(0).Rows(i).Item("AAS_LOC_ID").ToString + "/" + ds.Tables(0).Rows(i).Item("AAT_AAG_CODE").ToString + "/" + ds.Tables(0).Rows(i).Item("AAT_SUB_CODE").ToString + "/" + ds.Tables(0).Rows(i).Item("AAT_AAB_CODE").ToString + "/" + ds.Tables(0).Rows(i).Item("AAT_MODEL_NAME").ToString + "/" + CStr(GetMaxAssetId())
                    'txtAssetCode.Text = AstCode
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_AMG_ASSET_Insert_Master")
                    sp.Command.AddParameter("@AAT_CODE", AstCode, DbType.String)
                    'sp.Command.AddParameter("@AAT_NAME", ds.Tables(0).Rows(i).Item("AAT_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_MODEL_ID", ds.Tables(0).Rows(i).Item("AAT_MODEL_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_MODEL_NAME", ds.Tables(0).Rows(i).Item("AAT_MODEL_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_AAG_CODE", ds.Tables(0).Rows(i).Item("AAT_AAG_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_SUB_CODE", ds.Tables(0).Rows(i).Item("AAT_SUB_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_AVR_CODE", ds.Tables(0).Rows(i).Item("AAT_AVR_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_AAB_CODE", ds.Tables(0).Rows(i).Item("AAT_AAB_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_AMC_REQD", ds.Tables(0).Rows(i).Item("AAT_AMC_REQD").ToString, DbType.Int32)
                    sp.Command.AddParameter("@AAT_AMC_DATE", ds.Tables(0).Rows(i).Item("AAT_AMC_DATE").ToString, DbType.Date)
                    sp.Command.AddParameter("@AAT_AMC_DOCPATH", "", DbType.String)
                    sp.Command.AddParameter("@AAT_INS_REQD", ds.Tables(0).Rows(i).Item("AAT_INS_REQD").ToString, DbType.Int32)
                    sp.Command.AddParameter("@AAT_INS_DATE", ds.Tables(0).Rows(i).Item("AAT_INS_DATE").ToString, DbType.Date)
                    sp.Command.AddParameter("@AAT_INS_DOCPATH", "", DbType.String)
                    sp.Command.AddParameter("@AAT_AST_COST", ds.Tables(0).Rows(i).Item("AAT_AST_COST").ToString, DbType.Double)
                    sp.Command.AddParameter("@AAT_AST_SLVGVAL", ds.Tables(0).Rows(i).Item("AAT_AST_SLVGVAL").ToString, DbType.Double)
                    sp.Command.AddParameter("@AAT_UPT_DT", getoffsetdatetime(DateTime.Now), DbType.DateTime)
                    sp.Command.AddParameter("@AAT_UPT_BY", Session("UID"), DbType.String)
                    sp.Command.AddParameter("@AAT_STA_ID", ds.Tables(0).Rows(i).Item("AAT_STA_ID").ToString, DbType.Int32)
                    sp.Command.AddParameter("@AAT_OWNED", 1, DbType.Int32)
                    sp.Command.AddParameter("@AAT_PURCHASED_STATUS", 1, DbType.Int32)
                    sp.Command.AddParameter("@AAT_SPC_FIXED", 1, DbType.Int32)
                    sp.Command.AddParameter("@AAT_USR_MOVABLE", 0, DbType.Int32)
                    sp.Command.AddParameter("@AAT_AST_CONS", 0, DbType.Int32)
                    sp.Command.AddParameter("@AAT_DESC", ds.Tables(0).Rows(i).Item("AAT_DESC").ToString, DbType.String)
                    sp.Command.AddParameter("@CONF_TYPE", "", DbType.String)
                    sp.Command.AddParameter("@AAT_MFG_DATE", ds.Tables(0).Rows(i).Item("AAT_MFG_DATE").ToString, DbType.Date)
                    sp.Command.AddParameter("@AAT_WRNTY_DATE", ds.Tables(0).Rows(i).Item("AAT_WRNTY_DATE").ToString, DbType.Date)
                    sp.Command.AddParameter("@PURCHASEDDATE", ds.Tables(0).Rows(i).Item("PURCHASEDDATE").ToString, DbType.Date)
                    sp.Command.AddParameter("@AAT_AST_SERIALNO", ds.Tables(0).Rows(i).Item("AAT_AST_SERIALNO").ToString, DbType.String)
                    sp.Command.AddParameter("@LIFESPAN", ds.Tables(0).Rows(i).Item("AAT_LIFE_SPAN").ToString, DbType.String)
                    sp.Command.AddParameter("@PONUMBER", ds.Tables(0).Rows(i).Item("AAT_PO_NUMBER").ToString, DbType.String)
                    sp.Command.AddParameter("@DEPRECIATION", ds.Tables(0).Rows(i).Item("AAT_DEPRICIATION").ToString, DbType.Double)
                    sp.Command.AddParameter("@ASTYPE", ds.Tables(0).Rows(i).Item("AAT_AST_STATUS").ToString, DbType.Int32)
                    sp.Command.AddParameter("@AAT_TOWER_CODE", ds.Tables(0).Rows(i).Item("AAT_TOWER_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_FLOOR_CODE", ds.Tables(0).Rows(i).Item("AAT_FLOOR_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_LOC_ID", ds.Tables(0).Rows(i).Item("AAS_LOC_ID").ToString, DbType.String)
                    sp.ExecuteScalar()
                    'End If
                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_ASSET_SPACE_INSERT")
                    sp1.Command.AddParameter("@AAS_AAT_CODE", AstCode, DbType.String)
                    sp1.Command.AddParameter("@AAS_LOC_ID", ds.Tables(0).Rows(i).Item("AAS_LOC_ID").ToString, DbType.String)
                    sp1.Command.AddParameter("@AAS_FLR_ID", "", DbType.String)
                    sp1.Command.AddParameter("@AAS_BDG_ID", "", DbType.String)
                    'sp.Command.AddParameter("@AAS_FLR_ID", Floor, DbType.String)
                    sp1.Command.AddParameter("@AAS_PROC_TYPE", 0, DbType.String)
                    sp1.Command.AddParameter("@AAS_REMARKS", "EXCEL_UPLOAD_MASTER", DbType.String)
                    sp1.Command.AddParameter("@AAS_REQ_ID", "EXCEL_UPLOAD_MASTER", DbType.String)
                    sp1.ExecuteScalar()
                Next
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                'fillgrid()
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please choose file..."
            End If
        Catch ex As System.IO.IOException
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub rbtamc()
        If Not rdAMC.SelectedValue = 1 Then
            rbnAmcDate.Visible = False
            rbnAmcUpload.Visible = False
        Else
            rbnAmcDate.Visible = True
            rbnAmcUpload.Visible = True
        End If
    End Sub
   
    Protected Sub rdAMC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdAMC.SelectedIndexChanged
        rbtamc()
    End Sub

    Private Sub rbtIns()
        If Not rdIns.SelectedValue = 1 Then
            rbnInsDate.Visible = False
            rbnInsUpload.Visible = False
        Else
            rbnInsDate.Visible = True
            rbnInsUpload.Visible = True


        End If
    End Sub
    Protected Sub rdIns_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdIns.SelectedIndexChanged
        rbtIns()

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlLocation.SelectedIndex <> 0 And ddlTower.SelectedIndex <> 0 Then
            getFloorsByLocationTower(ddlLocation.SelectedItem.Value, ddlTower.SelectedItem.Value)
        End If

    End Sub
End Class
