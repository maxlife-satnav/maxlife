Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Controls_RejectAsset
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Dim intAstID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            intAstID = Request.QueryString("Ast_id")
            BindAssetDetails(intAstID, Session("uid"))
        End If
    End Sub

    Private Sub BindAssetDetails(ByVal AstId As Integer, ByVal strAur_id As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AST_ID", SqlDbType.NVarChar, 200)
        param(0).Value = AstId
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = strAur_id

        Dim dsdetails As New DataSet
        dsdetails = ObjSubSonic.GetSubSonicDataSet("GET_UNCLAIMEDAST_DETAILS", param)
        If dsdetails.Tables(0).Rows.Count > 0 Then
            lblAstCode.Text = dsdetails.Tables(0).Rows(0).Item("AAT_AST_CODE")
            lblAstName.Text = dsdetails.Tables(0).Rows(0).Item("AAT_NAME")
            lblAllocatedDate.Text = dsdetails.Tables(0).Rows(0).Item("AAT_SURRENDERED_DATE")


        End If

    End Sub


 

    Protected Sub btnAssetReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssetReject.Click
 
        Dim STATUS As Integer = 1043

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_AMT_STATUS")
        'sp.Command.AddParameter("@acode", lblAstCode.Text, DbType.String)
        'sp.ExecuteScalar()

        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_TAG_STATUS")
        sp2.Command.AddParameter("@acode", lblAstCode.Text, DbType.String)
        sp2.Command.AddParameter("@EMPID", Session("uid"), DbType.String)
        sp2.Command.AddParameter("@AAT_SURRENDERED_DATE", getoffsetdatetime(DateTime.Now), DbType.Date)
        sp2.Command.AddParameter("@AAT_TAG_STATUS", STATUS, DbType.Int32)
        sp2.ExecuteScalar()

        Dim Req_id As String
        Req_id = ObjSubSonic.RIDGENARATION("AST/")


        'Dim lblID As Label = DirectCast(gvRow.FindControl("lblID"), Label)
        intAstID = Request.QueryString("Ast_id")
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@SREQ_TS", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        param(1) = New SqlParameter("@SREQ_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Req_id
        param(2) = New SqlParameter("@SREQ_REQUEST_DATE", SqlDbType.DateTime)
        param(2).Value = Today.Date
        param(3) = New SqlParameter("@SREQ_REQUEST_BY", SqlDbType.NVarChar, 200)
        param(3).Value = Session("uid")
        param(4) = New SqlParameter("@SREQ_REQ_REMARKS", SqlDbType.NVarChar, 2000)
        param(4).Value = txtRemarks.Text
        param(5) = New SqlParameter("@SREQ_STATUS", SqlDbType.Int)
        param(5).Value = 1043
        param(6) = New SqlParameter("@AST_TAGGED_ID", SqlDbType.Int)
        param(6).Value = intAstID
        ObjSubSonic.GetSubSonicExecute("INSERT_SURRENDER_REQ", param)

        Response.Redirect("frmAssetThanks.aspx?RID=AstReject&Req_id=" & Req_id)
    End Sub
End Class

