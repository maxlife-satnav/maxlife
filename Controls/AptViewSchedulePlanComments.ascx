﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptViewSchedulePlanComments.ascx.vb" Inherits="Controls_AptViewSchedulePlanComments" %>
<asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Request ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:Label ID="lblreqid" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Status<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:Label ID="lblstatus" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDep" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlRM" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                    <%--<asp:TextBox ID="txtRM" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>--%>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Requested Date<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:Label ID="lblreqdate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <asp:GridView ID="gvItems" runat="server" EmptyDataText="Sorry! No Available Records..."
            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
            AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="ID" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("STARTDATE")%>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="End Date">
                    <ItemTemplate>
                        <asp:Label ID="lblenddate" runat="server" Text='<%#Eval("ENDDATE")%>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="From">
                    <ItemTemplate>
                        <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ORIGIN")%>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To">
                    <ItemTemplate>
                        <asp:Label ID="lblTo" runat="server" Text='<%#Eval("DESTINATION")%>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mode of Travel">
                    <ItemTemplate>
                        <asp:Label ID="lblMode" runat="server" Text='<%#Eval("MODEOFTRAVEL")%>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Time">
                    <ItemTemplate>
                        <asp:Label ID="lbltime" runat="server" Text='<%#Eval("TIME")%>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Travel Assistance">
                    <ItemTemplate>
                        <asp:Label ID="lbltravelassistance" runat="server" Text='<%#Eval("TRAVELASSISTANCE")%>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:ButtonField CommandName="View" Text="View" HeaderText="Comments" />
                <asp:ButtonField CommandName="Add" Text="Add" HeaderText="Comments" />
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblsno" runat="server" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div id="pnlview" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <h4>
                        <label class="col-md-12 control-label" style="text-align: center;">View Comments </label>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvviewcomments" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Comments">
                        <ItemTemplate>
                            <asp:Label ID="lblcommnets" runat="server" Text='<%#Eval("COMMENT")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Commented By">
                        <ItemTemplate>
                            <asp:Label ID="lblcommentedby" runat="server" Text='<%#Eval("AUR_KNOWN_AS")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Commented on">
                        <ItemTemplate>
                            <asp:Label ID="lblcomenteddate" runat="server" Text='<%#Eval("COMMENTED_ON")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div id="pnladdcomments" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <h4>
                        <label class="col-md-12 control-label" style="text-align: left;">Add Comments </label>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Comment<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcomment"
                        Display="None" ErrorMessage="Please Enter Comments " ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtcomment" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
                <div class="form-group">
                    <asp:Button ID="btnaddcommnet" runat="server" Text="Add Comment" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnsubmit" runat="server" Text="View Details" CssClass="btn btn-primary custom-button-color"
                CausesValidation="false" />
        </div>
    </div>
</div>



