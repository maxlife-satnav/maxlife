Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Controls_AddAssetModel
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getquantityUnits()
            getassetcategory()
            fillgrid()
            btnSubmit.Text = "Add"
            'getconsumbles()
            pnlMinQtyCon.Visible = False
            pnlmodelcode.Visible = False
        End If
    End Sub

    Public Sub getquantityUnits()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_QTY_UNITS")
        ddlunit.DataSource = sp.GetDataSet()
        ddlunit.DataTextField = "AST_UT_NAME"
        ddlunit.DataValueField = "AST_UT_ID"
        ddlunit.DataBind()
        ddlunit.Items.Insert(0, "--Select--")
    End Sub
    Private Sub getconsumbles()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GETCHECK_CONSUMABLES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        Dim con_code As String = ""
        Dim con_name As String = ""
        For Each row As DataRow In ds.Tables(0).Rows
            con_code = row.Item("VT_CODE")
            con_code = row.Item("VT_CODE")
            If ddlAssetCategory.SelectedItem.Value = con_code Then
                pnlMinQtyCon.Visible = True
                Exit For
            Else
                pnlMinQtyCon.Visible = False
                txtMinOrdQty.Text = 0
                txtMinEscQty.Text = 0
            End If
        Next
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            cleardata()
            btnSubmit.Text = "Add"
            pnlmodelcode.Visible = False
            txtModel.Enabled = True
            lblMsg.Visible = False
            ddlAssetCategory.SelectedIndex = 0
            'ddlAstSubCat.SelectedIndex = 0
            'ddlAstBrand.SelectedIndex = 0
            ddlAstSubCat.Items.Clear()
            ddlAstBrand.Items.Clear()
            ddlunit.ClearSelection()
            ddlAssetCategory.Enabled = True
            ddlAstSubCat.Enabled = True
            ddlAstBrand.Enabled = True
        Else
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            txtModel.Enabled = False
            pnlmodelcode.Visible = True
            ddlAssetCategory.Enabled = False
            ddlAstSubCat.Enabled = False
            ddlAstBrand.Enabled = False
            ddlAssetCategory.SelectedIndex = 0
            ddlAstSubCat.Items.Clear()
            ddlAstBrand.Items.Clear()
            txtRemarks.Text = ""
            ddlunit.SelectedIndex = 0
            'ddlAstSubCat.SelectedIndex = 0
            'ddlAstBrand.SelectedIndex = 0
            'ddlModel.SelectedIndex = 0

            getmodel()
        End If
            
    End Sub
    Private Sub getmodel()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MODELDRP")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_CODE"
        ddlModel.DataValueField = "AST_MD_ID"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChangeds(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        If ddlAssetCategory.SelectedIndex <> 0 Then
            getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)

        End If
        pnlMinQtyCon.Visible = False
    End Sub
    Private Sub cleardata()
        txtModel.Text = ""
        txtModelName.Text = ""
        'ddlAssetCategory.SelectedIndex = 0
        'ddlAstSubCat.SelectedIndex = 0
        'ddlAstBrand.SelectedIndex = 0
        'ddlModel.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        txtMinEscQty.Text = ""
        txtMinOrdQty.Text = ""
        txtRemarks.Text = ""
        ' ddlModel.SelectedIndex = 0
    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        If ddlAstSubCat.SelectedIndex <> 0 Then
            getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        End If
        pnlMinQtyCon.Visible = False
    End Sub
    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")
        getconsumbles()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text = "Add" Then

            Dim ValidateCode As Integer
            ValidateCode = ValidateBrand(txtModel.Text)
            If ValidateCode = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Brand/Make already exist please enter another Brand/Make..."
            ElseIf ValidateCode = 1 Then
                If txtMinOrdQty.Text = "" Then
                    txtMinOrdQty.Text = 0
                End If
                If txtMinEscQty.Text = "" Then
                    txtMinEscQty.Text = 0
                End If
                insertnewrecord()
                fillgrid()

            End If
        Else
            txtModel.Enabled = False

            If ddlModel.SelectedIndex <> 0 Then
                '    Dim ValidateCode As Integer
                '    ValidateCode = ValidateBrand(txtBrand.Text)
                '    If ValidateCode = 0 Then
                '        lblMsg.Visible = True
                '        lblMsg.Text = "Brand/Make already exist please enter another Brand/Make..."
                '    ElseIf ValidateCode = 1 Then
                If txtMinOrdQty.Text = "" Then
                    txtMinOrdQty.Text = 0
                End If
                If txtMinEscQty.Text = "" Then
                    txtMinEscQty.Text = 0
                End If
                modifydata()
                '        fillgrid()

                '    End If

            End If
        End If
        'cleardata()
    End Sub
    Private Sub modifydata()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_MODIFY_MODEL")
            sp1.Command.AddParameter("@AST_MD_CODE", ddlModel.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AST_MD_NAME", txtModelName.Text, DbType.String)
            sp1.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AST_MD_STAID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AST_MD_UPDATEDBY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AST_MD_MINQTY", txtMinOrdQty.Text, DbType.Int32)
            sp1.Command.AddParameter("@AST_MD_STKQTY", txtMinEscQty.Text, DbType.Int32)
            sp1.Command.AddParameter("@AST_MD_QTYUNIT", ddlunit.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@AST_MD_REM", txtRemarks.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            cleardata()
            lblMsg.Visible = True
            lblMsg.Text = "Model Modified Successfully..."
            ddlAssetCategory.SelectedIndex = 0
            ddlAstSubCat.SelectedIndex = 0
            ddlAstBrand.SelectedIndex = 0
            ddlModel.SelectedIndex = 0
            ddlunit.ClearSelection()

        Catch ex As Exception
            Response.Redirect(ex.Message)
        End Try
    End Sub
    Public Function ValidateBrand(ByVal modelcode As String)
        Dim ValidateCode As Integer
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_VALIDATE_MODEL")
        sp1.Command.AddParameter("@AST_MD_CODE", modelcode, DbType.String)
        sp1.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_INSERT_MODEL")
            sp1.Command.AddParameter("@AST_MD_CODE", txtModel.Text, DbType.String)
            sp1.Command.AddParameter("@AST_MD_NAME", txtModelName.Text, DbType.String)
            sp1.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AST_MD_STAID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AST_MD_CREATEDBY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AST_MD_MINQTY", txtMinOrdQty.Text, DbType.Int32)
            sp1.Command.AddParameter("@AST_MD_STKQTY", txtMinEscQty.Text, DbType.Int32)
            sp1.Command.AddParameter("@AST_MD_QTYUNIT", ddlunit.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@AST_MD_REM", txtRemarks.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "New Model Added Successfully..."
            cleardata()
            ddlAssetCategory.SelectedIndex = 0
            ddlAstSubCat.SelectedIndex = 0
            ddlAstBrand.SelectedIndex = 0
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSETMODEL")
        sp.Command.AddParameter("@dummy", 1, DbType.Int16)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvBrand.DataSource = ds
        gvBrand.DataBind()


        For i As Integer = 0 To gvBrand.Rows.Count - 1
            Dim lblstatus As Label = CType(gvBrand.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next

    End Sub

    'Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Response.Redirect("~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx")
    'End Sub

    Protected Sub ddlModel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModel.SelectedIndexChanged
        lblMsg.Text = " "
        If ddlModel.SelectedIndex <> 0 Then

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MODELBYCATSUBCATBRD")
            sp.Command.AddParameter("@AST_MD_ID", ddlModel.SelectedItem.Value, DbType.Int32)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            ddlAstSubCat.Enabled = False
            Dim categorycode As String
            Dim subcategory As String
            If ds.Tables(0).Rows.Count > 0 Then

                txtModel.Text = ds.Tables(0).Rows(0).Item("AST_MD_CODE")
                txtModelName.Text = ds.Tables(0).Rows(0).Item("AST_MD_NAME")
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AST_MD_STAID")).Selected = True
                ddlAssetCategory.ClearSelection()
                ddlAssetCategory.Items.FindByValue(ds.Tables(0).Rows(0).Item("VT_CODE")).Selected = True
                categorycode = ds.Tables(0).Rows(0).Item("VT_CODE")
                getsubcategorybycat(categorycode)
                ddlAstSubCat.ClearSelection()
                ddlAstSubCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("AST_SUBCAT_CODE")).Selected = True
                subcategory = ds.Tables(0).Rows(0).Item("AST_SUBCAT_CODE")
                getbrandbycatsubcat(categorycode, subcategory)
                ddlAstBrand.ClearSelection()
                ddlAstBrand.Items.FindByValue(ds.Tables(0).Rows(0).Item("manufactuer_code")).Selected = True
                txtMinOrdQty.Text = ds.Tables(0).Rows(0).Item("AST_MD_MINQTY")
                txtMinEscQty.Text = ds.Tables(0).Rows(0).Item("AST_MD_STKQTY")
                txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("AST_MD_REM"))
                getquantityUnits()
                If ds.Tables(0).Rows(0).Item("AST_MD_QTYUNITS") <> "0" Then
                    ddlunit.Items.FindByValue(ds.Tables(0).Rows(0).Item("AST_MD_QTYUNITS")).Selected = True
                End If
                getconsumbles()
            End If
        Else
            txtModel.Text = ""
            txtModelName.Text = ""
            ddlAssetCategory.SelectedIndex = 0
            ddlAstSubCat.SelectedIndex = 0
            ddlAstBrand.SelectedIndex = 0
            ddlunit.ClearSelection()
            txtRemarks.Text = ""
            txtMinOrdQty.Text = ""
            txtMinEscQty.Text = ""

        End If

    End Sub
    Private Sub ValidateAssetModel()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSETCODE_VALIDATE")
        sp.Command.AddParameter("@ASSETCODE", txtModel.Text, DbType.String)
        Dim str As DataSet
        str = sp.GetDataSet()
        Dim cnt As Integer
        cnt = str.Tables(0).Rows.Count
        If cnt > 1 Then
            txtModel.Text = ""
            lblMsg.Text = "Asset Model Code already exists"
        Else : lblMsg.Text = ""
        End If
    End Sub


    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getconsumbles()
        Else
            pnlMinQtyCon.Visible = False
        End If
    End Sub

    Protected Sub gvBrand_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvBrand.PageIndexChanging
        gvBrand.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub txtModel_TextChanged(sender As Object, e As EventArgs) Handles txtModel.TextChanged
        ValidateAssetModel()
    End Sub
End Class
