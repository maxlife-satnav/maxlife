<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetReq.ascx.vb" Inherits="Controls_AssetReq" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }
        <%--function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }--%>

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>

</head>
<body>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">                  
                    </asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Employee Id</label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Asset Category </label>
                    <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                        Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Sub Category </label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                        Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Brand/Make </label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                        Display="none" ErrorMessage="Please Select Asset Brand/Make !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Model </label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                        Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Location </label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                        ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="true" CssClass="btn btn-primary custom-button-color" Text="Search" />
                     <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" CausesValidation="False" />
            </div>
        </div>
    </div>
    <div id="pnlItems" runat="server">
        <div class="row">
            <fieldset>
                <legend>Assets List</legend>
            </fieldset>
        </div>
        <div class="row">


            <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false"
                    EmptyDataText="No Asset Requisition Found." ShowHeader="True" CssClass="table table-condensed table-bordered table-hover table-striped">
                    <Columns>
                        <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblproductname" Text='<%#Eval("AST_MD_NAME") %>' runat="server"></asp:Label>
                                <asp:Label ID="lblmdcode" Text='<%#Eval("AST_MD_CODE") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
                                <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"></asp:Label>


                                   <asp:Label ID="lbl_vt_code" Text='<%#Eval("VT_CODE") %>' runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lbl_ast_subcat_code" Text='<%#Eval("AST_SUBCAT_CODE") %>' runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lbl_manufactuer_code" Text='<%#Eval("manufactuer_code") %>' runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lbl_ast_md_code" Text='<%#Eval("AST_MD_CODE") %>' runat="server" Visible="false"></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                     <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAll" ItemStyle-HorizontalAlign="Center" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                ToolTip="Click to check all" />
                            </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                        </ItemTemplate>
                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                    </asp:TemplateField>


                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row">
        <br />
    </div>
    <div class="row" id="remarksAndSubmitBtn" runat="server">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Remarks </label>
                    <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                        Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"
                        Enabled="true"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSubmit" Text="Submit" runat="server" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();"  />
            </div>
        </div>
    </div>
</body>
</html>

