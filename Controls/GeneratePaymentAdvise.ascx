<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GeneratePaymentAdvise.ascx.vb"
    Inherits="Controls_GeneratePaymentAdvise" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Work Request <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                    Display="None" ErrorMessage="Please Select Work Request" ValidationGroup="Val1"
                    InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    City
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtBuilding" runat="server" CssClass="form-control"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Property
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtproperty" runat="server" CssClass="form-control"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Title
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                        MaxLength="50" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Work Specifications
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control"
                        Rows="3" TextMode="MultiLine" MaxLength="1000" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Estimated Amount
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Vendor Name
                </label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control"
                        Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Work Status
                </label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"
                        Enabled="False">
                        <asp:ListItem>--Select Status--</asp:ListItem>
                        <asp:ListItem Value="0">Pending</asp:ListItem>
                        <asp:ListItem Value="1">InProgress</asp:ListItem>
                        <asp:ListItem Value="2">Completed</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Payment Mode <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="ddlpay"
                    Display="None" ErrorMessage="Please Select Payment Mode" ValidationGroup="Val1"
                    InitialValue="--Select Payment Mode--" Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlpay" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                        <asp:ListItem>--Select Payment Mode--</asp:ListItem>
                        <asp:ListItem Value="1">Cheque</asp:ListItem>
                        <asp:ListItem Value="2">Cash</asp:ListItem>
                        <asp:ListItem Value="3">Bank Transfer</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Remarks<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                        Rows="3" MaxLength="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="panel1" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Cheque No<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvCheque" runat="server" ControlToValidate="txtCheque"
                        Display="None" ErrorMessage="Please Enter Cheque Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rvcheque" Display="None" runat="server" ControlToValidate="txtCheque"
                        ErrorMessage="Invalid Cheque Number" ValidationExpression="^[A-Za-z0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                            <asp:TextBox ID="txtCheque" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Bank Name<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Account Number<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                        ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter  Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtAccNo" runat="Server" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="panel2" runat="Server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Issuing Bank Name<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                        ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtIBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Deposited Bank<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                        Display="None" ErrorMessage="Please Enter Deposited Bank" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtDeposited"
                        ErrorMessage="Enter Valid Bank Branch" ValidationExpression="^[A-Za-z0-9 ]*$"
                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtDeposited" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        IFSC Code<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvIFCB" runat="server" ControlToValidate="txtIFCB"
                        Display="none" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="REVIFCB" Display="None" runat="server" ControlToValidate="txtIFCB"
                        ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtIFCB" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnsubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                CausesValidation="true" />

        </div>
    </div>
</div>
