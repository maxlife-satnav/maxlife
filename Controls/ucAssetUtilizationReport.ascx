<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucAssetUtilizationReport.ascx.vb"
    Inherits="Controls_ucAssetUtilizationReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset</label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Asset" ControlToValidate="ddlassetid"
                    InitialValue="--Select--" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlassetid" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="row">
                    <div class="col-md-5 control-label">
                        <%--<asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary custom-button-color" />--%>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
                            <div class="form-group">
                            </div>
                        </div>


<div id="pnllbl" runat="server" visible="false">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Assets  :</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbltot" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Allocated  :</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbltotalloc" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Total Available     :</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbltotvacnt" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="form-group">
    </div>
</div>
<div>
    <div class="form-group">
        <div class="col-md-12">
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
        </div>
    </div>
</div>
