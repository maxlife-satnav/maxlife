
Partial Class Controls_CoCheck
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByStatusId")
        sp.Command.AddParameter("@StatusId", 1004, Data.DbType.Int32)
		        sp.Command.AddParameter("@Cuser", SESSION("UID"), Data.DbType.STRING)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
