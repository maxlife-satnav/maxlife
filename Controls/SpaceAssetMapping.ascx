<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SpaceAssetMapping.ascx.vb" Inherits="Controls_SpaceAssetMapping" EnableViewState="true" %>

<%--<script type="text/javascript">
    function noBack() { window.history.forward() }
    noBack();
    window.onload = noBack;
    window.onpageshow = function (evt) { if (evt.persisted) noBack() }
    window.onunload = function () { void (0) }
</script>--%>
<script type="text/javascript">
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID)
        for (i = 0; i < form1.elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'checkbox') {
                if (re.test(elm.name)) {
                    if (elm.disabled == false)
                        elm.checked = checkVal
                }
            }
        }
    }
    function ChildClick(CheckBox) {
        //Get target base & child control.
        var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
        var TargetChildControl = "chkSelect";
        //Get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        // check to see if all other checkboxes are checked
        for (var n = 0; n < Inputs.length; ++n)
            if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                // Whoops, there is an unchecked checkbox, make sure
                // that the header checkbox is unchecked
                if (!Inputs[n].checked) {
                    Inputs[0].checked = false;
                    return;
                }
            }
        // If we reach here, ALL GridView checkboxes are checked
        Inputs[0].checked = true;
    }
</script>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <label class="col-md-3 btn btn-default pull-right">
            <asp:CheckBox ID="CheckBox1" runat="server" Checked="true" GroupName="rbActions" Text="Space Mapping"></asp:CheckBox>

        </label>
    </div>
    <div class="col-md-6">
        <label class="btn btn-default" style="margin-left: 25px">
            <asp:CheckBox ID="CheckBox2" runat="server" GroupName="rbActions" Text="Employee Mapping"></asp:CheckBox>

        </label>
    </div>
</div>
<br />
<br />
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                    Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" Style="width: 77px">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Sub Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                    Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Model<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                <div class="col-md-7">
                    <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location</label>
                <asp:RequiredFieldValidator ID="cvLocation" runat="server" ControlToValidate="ddlLocation"
                    Display="Dynamic" ErrorMessage="Please Select Location" ValidationGroup="Val1"
                    InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="ReqId">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label" id="lbreq" runat="server">Request ID</label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlReqId"
                    Display="Dynamic" ErrorMessage="Please Select Request ID" ValidationGroup="Val1"
                    InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlReqId" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1" CausesValidation="true" />
            <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" ValidationGroup="Val1" CausesValidation="False" />
        </div>
    </div>
</div>
<div id="panel1" runat="server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
         <div class="row" style= width: 100%; min-height: 20px; max-height: 500px">
            <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:TemplateField HeaderText="Asset Name">

                        <ItemTemplate>
                            <asp:Label ID="lblassetName" runat="server" Text='<%#Eval("Asset Code")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AssetId" Visible="false">

                        <ItemTemplate>
                            <%--Modelid--%>
                            <asp:Label ID="lblAssetId" runat="server" Text='<%#Eval("Asset Model Code")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">

                        <ItemTemplate>
                            <asp:Label ID="lbllocation" Visible="false" runat="server" Text='<%#Eval("Location Code")%>'></asp:Label>
                            <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("Location Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tower">

                        <ItemTemplate>
                            <asp:DropDownList ID="ddlTower" runat="server" CssClass="selectpicker" data-live-search="true"
                                OnSelectedIndexChanged="ddlTower_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Floor">

                        <ItemTemplate>
                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true"
                                OnSelectedIndexChanged="ddlFloor_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Space">
                        <ItemStyle Width="10%" />
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlSpace" runat="server" CssClass="selectpicker" data-live-search="true"
                                OnSelectedIndexChanged="ddlSpace_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Employee" Visible="false">
                        <ItemStyle Width="10%" />
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="selectpicker" data-live-search="true"
                                OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- <asp:TemplateField HeaderText="Asset ID">
                        <ItemStyle Width="15%" />
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlAssetspace" runat="server" CssClass="selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <%--      <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                        <ItemStyle Width="5%" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                ToolTip="Click to check all" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
        </div>
    </div>
</div>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="GridView1" runat="server" EmptyDataText="Sorry! No Available Records..."
                CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" PageSize="10 " AutoGenerateColumns="true">
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
