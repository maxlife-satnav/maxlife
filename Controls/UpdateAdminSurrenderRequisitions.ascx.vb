Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_UpdateAdminSurrenderRequisitions
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim Req_id As String
    Dim lblAstCode As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Req_id = Request.QueryString("Req_id")
            GetDetailsByRequistion(Req_id)
        End If
    End Sub

    Private Sub GetDetailsByRequistion(ByVal Req_id As String)
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        ObjSubsonic.BindGridView(SurReqDetItems, "GET_AdminALLSURRENDERREQ_byReq", param)
        ds = ObjSubsonic.GetSubSonicDataSet("GET_AdminALLSURRENDERREQ_byReq", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblAstCode = ds.Tables(0).Rows(0).Item("AAT_AST_CODE")
            ' lblAstName.Text = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
            'lblLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            ' lblTower.Text = ds.Tables(0).Rows(0).Item("TWR_NAME")
            'lblFloor.Text = ds.Tables(0).Rows(0).Item("FLR_NAME")
            'lblAstAllocDt.Text = ds.Tables(0).Rows(0).Item("AAT_ALLOCATED_DATE")
            ' lblAstSurDt.Text = ds.Tables(0).Rows(0).Item("AAT_SURRENDERED_DATE")
            'lblSREQ_RMAPPROVAL_DATE.Text = ds.Tables(0).Rows(0).Item("SREQ_RMAPPROVAL_DATE")
            'lblRM_NAME.Text = ds.Tables(0).Rows(0).Item("RM_NAME")
            lblRMRemarks.Text = ds.Tables(0).Rows(0).Item("SREQ_RM_REMARKS")

            'lblSurReq_id.Text = Req_id
            'lblAstDate.Text = ds.Tables(0).Rows(0).Item("AAT_UPT_DT")

        End If
        lblRMRemarks.Enabled = False
    End Sub

    Protected Sub btnApprov_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprov.Click

        For Each gvRow As GridViewRow In SurReqDetItems.Rows
            Dim lblAstCde As Label = DirectCast(gvRow.FindControl("lblAstCode"), Label)
            lblAstCode = lblAstCde.Text
        Next
        'REUSE
        If rdbList.Items(0).Selected = False Then
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("REQ_ID")
            param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
            param(1).Value = Session("UID")
            param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
            param(2).Value = txtRMRemarks.Text
            param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
            param(3).Value = 13
            param(4) = New SqlParameter("@AST_SALAVAGE_VALUE", SqlDbType.NVarChar, 2000)
            param(4).Value = 0
            param(5) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
            param(5).Value = False
            ObjSubsonic.GetSubSonicDataSet("UPDATESURRENDER_REQUISTION_byAdmin", param)
            send_mail(Request.QueryString("REQ_ID"), lblAstCode)
            Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))

            'DISPOSE
        Else
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("REQ_ID")
            param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
            param(1).Value = Session("UID")
            param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
            param(2).Value = txtRMRemarks.Text
            param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
            param(3).Value = 1040
            param(4) = New SqlParameter("@AST_SALAVAGE_VALUE", SqlDbType.NVarChar, 2000)
            param(4).Value = CDbl(txtSalvageVal.Text)
            param(5) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
            param(5).Value = True
            ObjSubsonic.GetSubSonicDataSet("UPDATESURRENDER_REQUISTION_byAdmin", param)
            ' Response.Redirect("frmViewadminSurrenderRequisitions.aspx")
            send_mail(Request.QueryString("REQ_ID"), lblAstCode)
            dispose_asset(lblAstCode)
            Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))
        End If

    End Sub

    Public Sub dispose_asset(ByVal ast As String)
        Dim Req_id As String
        Req_id = ObjSubsonic.RIDGENARATION("Admin/Ast/Dispose/")

        Dim Salvage As Integer

        '************ Calculate Asset Salvage Value ***********************
        Dim param_Sal(0) As SqlParameter
        param_Sal(0) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
        param_Sal(0).Value = ast
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_DEP_VALUE", param_Sal)
        If ds.Tables(0).Rows.Count > 0 Then
            Salvage = FormatNumber(ds.Tables(0).Rows(0).Item("ASTVALUE"), 2)
        End If

        '*********************************
        Dim AstTaggedId As Integer = 0

        Dim param(12) As SqlParameter
        param(0) = New SqlParameter("@DREQ_TS", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        param(1) = New SqlParameter("@DREQ_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Req_id
        param(2) = New SqlParameter("@AST_TAGGED_ID", SqlDbType.Int)
        param(2).Value = AstTaggedId
        param(3) = New SqlParameter("@DREQ_REQUESITION_DT", SqlDbType.DateTime)
        param(3).Value = Now.Date
        param(4) = New SqlParameter("@DREQ_REQUESTED_BY", SqlDbType.NVarChar, 200)
        param(4).Value = Session("uid")
        param(5) = New SqlParameter("@DREQ_REQUESTED_REMARKS", SqlDbType.NVarChar, 2000)
        param(5).Value = txtRMRemarks.Text
        param(6) = New SqlParameter("@DREQ_STATUS", SqlDbType.Int)
        param(6).Value = 1040
        param(7) = New SqlParameter("@AST_SALAVAGE_VALUE", SqlDbType.Int)
        param(7).Value = Salvage
        param(8) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
        param(8).Value = True
        param(9) = New SqlParameter("@DREQ_ADMIN_DT", SqlDbType.Int)
        param(9).Value = ""
        param(10) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.Int)
        param(10).Value = ""
        param(11) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.Int)
        param(11).Value = ""
        param(12) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
        param(12).Value = ast
        ' send_mail_dispose(Req_id, ast)
        ObjSubsonic.GetSubSonicExecute("INSERT_DISPOSE_REQ", param)


    End Sub
    Public Sub send_mail(ByVal reqid As String, ByVal astcode As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_SURRENDER_REQUISITION_APPROVE_INSTOCKDISPOSE")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@AAT_CODE", astcode, DbType.String)
        sp1.Execute()
    End Sub
    Public Sub send_mail_dispose(ByVal Req_id As String, ByVal AST_CODE As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION")
        sp1.Command.AddParameter("@REQ_ID", Req_id, DbType.String)
        sp1.Command.AddParameter("@AST_CODE", AST_CODE, DbType.String)
        sp1.Execute()
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmViewadminSurrenderRequisitions.aspx")
    End Sub


    Protected Sub rdbList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbList.SelectedIndexChanged

        For Each gvRow As GridViewRow In SurReqDetItems.Rows
            Dim lblAstCde As Label = DirectCast(gvRow.FindControl("lblAstCode"), Label)
            lblAstCode = lblAstCde.Text
        Next

        If rdbList.Items(0).Selected = True Then
            pnlDispose.Visible = True
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = lblAstCode
            Dim ds As New DataSet
            ds = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_DEP_VALUE", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtSalvageVal.Text = FormatNumber(ds.Tables(0).Rows(0).Item("ASTVALUE"), 2)
            End If
        Else
            pnlDispose.Visible = False
            txtSalvageVal.Text = ""
        End If
    End Sub
End Class
