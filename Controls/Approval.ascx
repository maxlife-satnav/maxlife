<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Approval.ascx.vb" Inherits="Controls_Approval" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-bordered table-hover table-striped"
            AllowPaging="True" PageSize="5" EmptyDataText="No Surrender Request Found.">
            <Columns>
                <asp:TemplateField Visible="false" HeaderText="Property Name">
                    <ItemTemplate>
                        <asp:Label ID="lblUniqueNo" runat="server" Text='<%#Eval("SNO")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Property Name">
                    <ItemTemplate>
                        <asp:Label ID="lblpropertyname" runat="server" Text='<%#Eval("PN_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tenant Code">
                    <ItemTemplate>
                        <asp:Label ID="lbltenantcode" runat="server" Text='<%#Eval("TEN_CODE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tenant Name">
                    <ItemTemplate>
                        <asp:Label ID="lbltenant" runat="server" Text='<%#Eval("TEN_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tenant Email">
                    <ItemTemplate>
                        <asp:Label ID="lblemail" runat="server" Text='<%#Eval("TEN_EMAIL") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Approve">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkApprove" runat="server" Text="Approve" CommandName="Approve"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reject">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkReject" runat="server" Text="Reject" CommandName="Reject"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>

            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
        <asp:HiddenField ID="hdnPN" runat="server" />
        <asp:HiddenField ID="hdnSNO" runat="server" />
    </div>
</div>
<div id="panel1" runat="server">
    <div class="row">
        <div class="col-sm-12">


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Tenant Code<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Tenant Name<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Select Property Type<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Select City<span style="color: red;">*</span></label>

                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                                    AutoPostBack="True" Enabled="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Select Location<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                                    AutoPostBack="True" Enabled="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Select Property<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                    <asp:ListItem>--Select Property--</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Tenant Occupied Area<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control"
                                    MaxLength="5" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Tenant Rent<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtRent" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Joining Date<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <div class='input-group date' id='fromdate'>
                                    <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Next Payable Date<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <div class='input-group date' id='todate'>
                                    <asp:TextBox ID="txtPayableDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Security Deposit<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control"
                                    MaxLength="5" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Select Payment Terms<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                    <asp:ListItem>--Select Payment Terms--</asp:ListItem>
                                    <asp:ListItem>Weekly</asp:ListItem>
                                    <asp:ListItem>Monthly</asp:ListItem>
                                    <asp:ListItem>Half-Yearly</asp:ListItem>
                                    <asp:ListItem>Annual</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <%-- <div class="col-sm-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-5 control-label">
                                    Tenant Status<span style="color: red;">*</span></label>
                                <div class="col-sm-7">
                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                        <asp:ListItem>--Select Status--</asp:ListItem>
                                        <asp:ListItem Value="0">Active</asp:ListItem>
                                        <asp:ListItem Value="1">InActive</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Number of Parking<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Maintenance Fees<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtfees" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">


                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Outstanding amount<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Select User<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:DropDownList ID="ddluser" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">



                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Remarks<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                                    Rows="3" MaxLength="500" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-5 control-label">
                                Surrender Date<span style="color: red;">*</span></label>
                            <div class="col-sm-7">
                                <div class='input-group date' id='Div1'>
                                    <asp:TextBox ID="txtSurrenderDt" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"> </asp:TextBox>
                                    <%-- <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>--%>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label" runat="server" id="Label1">
                                Rent Amount
                            </label>


                            <div class="col-md-7">
                                <asp:TextBox ID="txtTotalRent" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label" runat="server" id="Label2">
                                Rent Amount Paid</label>


                            <div class="col-md-7">
                                <asp:TextBox ID="txtRentPaid" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>





            </div>



            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label" runat="server" id="lblClosingBalMsg">
                                Clearance Amount 
                            </label>


                            <div class="col-md-7">
                                <asp:TextBox ID="txtTenClosingBal" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>



            </div>


            <div class="row">
                <label class="col-md-6 control-label" runat="server" id="Label4">
                    Clearance Amount = Security Deposit - (Total Rent - Rent Paid Till Date)
                </label>

            </div>



        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">

                <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />&nbsp;
           
            </div>
        </div>
    </div>
</div>
