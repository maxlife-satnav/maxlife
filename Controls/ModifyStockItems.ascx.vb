Imports System.Data
Partial Class Controls_ModifyStockItems
    Inherits System.Web.UI.UserControl
   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            btnModify.Visible = True
            Dim id As Integer = CInt(Request.QueryString("id"))
            lblTemp.Text = id
            dispdata()
        End If
    End Sub
    Private Sub modifydata()
        Try
            Dim validatecode1 As Integer
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_MODIFY_STOCKITEMS")
            sp.Command.AddParameter("@id1", CInt(lblTemp.Text), DbType.Int32)
            sp.Command.AddParameter("@AIM_CODE", txtAIMC.Text, DbType.String)
            sp.Command.AddParameter("@AIM_UNIT_RATE", txtUntPrc.Text, DbType.Double)
            sp.Command.AddParameter("@AIM_RT1", txtESC1.Text, DbType.Double)
            sp.Command.AddParameter("@AIM_RT1Email", txtEmail1.Text, DbType.String)
            sp.Command.AddParameter("@AIM_RT2", txtESC2.Text, DbType.Double)
            sp.Command.AddParameter("@AIM_RT2Email", txtEmail2.Text, DbType.String)
            sp.Command.AddParameter("@AIM_RT3", txtESC3.Text, DbType.String)
            sp.Command.AddParameter("@AIM_RT3Email", txtEmail3.Text, DbType.String)
            sp.Command.AddParameter("@AIM_REM", txtRem.Text, DbType.String)
            sp.Command.AddParameter("@AIM_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AIM_MINORDQTY", txtMinOrdr.Text, DbType.Int32)
            sp.Command.AddParameter("@AIM_NAME", txtName.Text, DbType.String)
            sp.Command.AddParameter("@AIM_UP_BY", Session("Uid"), DbType.String)
            sp.ExecuteScalar()
            validatecode1 = sp.ExecuteScalar()
            If validatecode1 = "0" Then
                Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?ID=10")
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Code already exist, Please enter another Code"
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub dispdata()
      Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_EDIT_STOCKITEMS")
        sp.Command.AddParameter("@id1", CInt(lblTemp.Text), DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
           txtAIMC.Text = ds.Tables(0).Rows(0).Item("AIM_CODE")
            txtUntPrc.Text = ds.Tables(0).Rows(0).Item("AIM_UNIT_RATE")
            txtESC1.Text = ds.Tables(0).Rows(0).Item("AIM_RT1")
            txtESC2.Text = ds.Tables(0).Rows(0).Item("AIM_RT2")
            txtESC3.Text = ds.Tables(0).Rows(0).Item("AIM_RT3")
            txtEmail1.Text = ds.Tables(0).Rows(0).Item("AIM_RT1Email")
            txtEmail2.Text = ds.Tables(0).Rows(0).Item("AIM_RT2Email")
            txtEmail3.Text = ds.Tables(0).Rows(0).Item("AIM_RT3Email")
            txtRem.Text = ds.Tables(0).Rows(0).Item("AIM_REM")
            txtMinOrdr.Text = ds.Tables(0).Rows(0).Item("AIM_MINORDQTY")
            txtName.Text = ds.Tables(0).Rows(0).Item("AIM_NAME")
            ddlStatus.ClearSelection()
            ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AIM_STA_ID")).Selected = True
        End If
    End Sub
    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        modifydata()
    End Sub
	 Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAddStockItems.aspx")
    End Sub
End Class

