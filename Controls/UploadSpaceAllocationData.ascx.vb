﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports System.IO
Imports System.Data.OleDb
Imports System.Collections.Generic
Partial Class Controls_UploadSpaceAllocationData
    Inherits System.Web.UI.UserControl
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
            Else
                'lblMsg.Text = "Please Select File !"
                'lblMsg.Visible = True
            End If
            Dim fs As System.IO.FileStream
            Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
            If strFileType <> "" Then
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                'Dim filename As String = s(0).ToString() & Date.Now.ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filename As String = Path.GetFileNameWithoutExtension(fpBrowseDoc.FileName) & Date.Now.ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                'Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename
                Dim filepath As String = Replace(Server.MapPath("~\UploadFiles\"), "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                'fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "~\UploadFiles\" + filename)
                fpBrowseDoc.SaveAs(filepath)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                'For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                'If Trim(LCase(ds.Tables(0).Columns(0).ToString)) <> "space id" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 1: Column name should be SPACE ID"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(1).ToString)) <> "space type" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 2: Column name should be SPACE TYPE"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(2).ToString)) <> "vertical" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 3: Column name should be VERTICAL"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(3).ToString)) <> "costcenter" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 4: Column name should be COSTCENTER"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(4).ToString)) <> "shift code" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 5: Column name should be SHIFT CODE"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(5).ToString)) <> "employee id" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 6: Column name should be EMPLOYEE ID"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(6).ToString)) <> "from date(mm/dd/yyyy)" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 7: Column name should be FROM DATE(MM/DD/YYYY)"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(7).ToString)) <> "to date(mm/dd/yyyy)" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 8: Column name should be TO DATE(MM/DD/YYYY)"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(8).ToString)) <> "from time(hh:mm)" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 9: Column name should be FROM TIME(HH:MM)"
                '    Exit Sub
                'ElseIf Trim(LCase(ds.Tables(0).Columns(9).ToString)) <> "to time(hh:mm)" Then
                '    lblMsg.Visible = True
                '    lblMsg.Text = "At Column 10: Column name should be TO TIME(HH:MM)"
                '    Exit Sub

                'End If

                'Next
                'Next
                Dim empid_eidcnt As Integer = 1
                Dim vert_cnt As Integer = 1
                Dim cost_cnt As Integer = 1
                Dim spaceid_sidcnt As Integer = 1
                Dim spctype_cnt As Integer = 1


                Dim empid_remarks As String = ""
                Dim spaceid_remarks As String = ""
                Dim fromdate_remarks As String = ""
                Dim todate_remarks As String = ""
                Dim fromtime_remarks As String = ""
                Dim totime_remarks As String = ""
                Dim remarks As String = ""


                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "space id" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'name_cnt = 0
                                remarks = remarks + "Spaceid is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "vertical" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'id_cnt = 0
                                remarks = remarks + "Vertical is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "space type" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'id_cnt = 0
                                remarks = remarks + "Space Type is null or empty, "
                            End If
                        End If

                       
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "from date" Then
                            'Dim fromdate As DateTime = (ds.Tables(0).Rows(i).Item(j))
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'id_cnt = 0

                                remarks = remarks + "Fromdate is null or empty, "


                                'ElseIf 
                                '      fromdate.ToString( "mm/dd/yyyy") Then
                                '    remarks = remarks + "Date format should be mm/dd/yy ,"
                                'End If

                            End If

                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "to date" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'id_cnt = 0
                                remarks = remarks + "Todate is null or empty, "

                            ElseIf (ds.Tables(0).Rows(i).Item(j)) < DateTime.Now Then
                                remarks = remarks + "Todate should be greater than today's date,"
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "from time" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'id_cnt = 0
                                'remarks = remarks + "Fromtime is null or empty, "
                                ds.Tables(0).Rows(i).Item(j) = "00:00"

                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "to time" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'id_cnt = 0
                                'remarks = remarks + "Totime is null or empty, "
                                ds.Tables(0).Rows(i).Item(j) = "23:59"
                            End If
                        End If

                    Next

                    If vert_cnt <> 0 And spaceid_sidcnt <> 0 And spctype_cnt <> 0 Then

                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPLOAD_SPACEALLOCATION_DATA")
                        sp.Command.AddParameter("@SPACE_ID", ds.Tables(0).Rows(i).Item(0).ToString, DbType.String)
                        sp.Command.AddParameter("@SPC_TYPE", ds.Tables(0).Rows(i).Item(1).ToString, DbType.String)
                        sp.Command.AddParameter("@VERTICAL", ds.Tables(0).Rows(i).Item(2).ToString, DbType.String)
                        sp.Command.AddParameter("@COSTCENTER", ds.Tables(0).Rows(i).Item(3).ToString, DbType.String)
                        sp.Command.AddParameter("@SH_CODE", ds.Tables(0).Rows(i).Item(4).ToString, DbType.String)
                        sp.Command.AddParameter("@EMP_ID", ds.Tables(0).Rows(i).Item(5).ToString, DbType.String)
                        sp.Command.AddParameter("@FROM_DATE", ds.Tables(0).Rows(i).Item(6).ToString, DbType.String)
                        sp.Command.AddParameter("@TO_DATE", ds.Tables(0).Rows(i).Item(7).ToString, DbType.String)
                        sp.Command.AddParameter("@FROM_TIME", ds.Tables(0).Rows(i).Item(8).ToString, DbType.DateTime)
                        sp.Command.AddParameter("@TO_TIME", ds.Tables(0).Rows(i).Item(9).ToString, DbType.DateTime)
                        sp.Command.AddParameter("@REMARKS", remarks, DbType.String)
                        sp.Command.AddParameter("@UID", Session("Uid"), DbType.String)
                        sp.ExecuteScalar()
                    End If
                    remarks = ""
                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_SPACEALLOCATION_DATA")
                sp1.Command.AddParameter("@dummy", 1, DbType.Int32)
                Dim ds1 As New DataSet
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    GridView1.DataSource = ds1.Tables(0)
                    GridView1.DataBind()
                    btnExport.Visible = True
                    lblMsg.Visible = True
                    lblMsg.Text = "Data uploaded successfully..."

                    For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                        For j As Integer = 0 To ds1.Tables(0).Columns.Count - 1

                            Dim space_id As String = ds1.Tables(0).Rows(i).Item(1).ToString
                            ' Dim vertical As String = ds1.Tables(0).Rows(i).Item(1).ToString
                            'Dim costcenter As String = ds1.Tables(0).Rows(i).Item(2).ToString
                            Dim emp_id As String = ds1.Tables(0).Rows(i).Item(1).ToString

                            Dim emp_name As String
                            If Trim(LCase(ds1.Tables(0).Columns(j).ToString)) = "remarks" Then
                                If (ds1.Tables(0).Rows(i).Item(6).ToString = "Success, ") Then
                                    Dim sp As New SubSonic.StoredProcedure("GET_EMPID")
                                    sp.Command.AddParameter("@EMP_ID", ds.Tables(0).Rows(i).Item(0).ToString, DbType.String)
                                    emp_name = sp.ExecuteScalar()
                                    UpdateRecord(space_id, 7, emp_name)

                                End If

                            End If

                        Next
                    Next

                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "TRUNCATE_TEMP_SPACEALLOCATION_DATA")
                    sp2.Execute()


                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Please select file..."
                End If
                End If
        Catch ex As Exception
            CommonModules.PopUpMessage(ex.Message, Page)

        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack Then
            lblMsg.Text = ""
            btnExport.Visible = False
        End If
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        'ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        'GridView1.Visible = True
        'ExportPanel1.FileName = "UPLOAD_HRMS_DATA.xls"
        Export("Upload SpaceAllocation Data.xls", GridView1)
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table



        For Each row As GridViewRow In gv.Rows
            row.Cells(0).Style.Add("mso-number-format", "\@")
            row.Cells(5).Style.Add("mso-number-format", "\@")


            PrepareControlForExport(row)
            table.Rows.Add(row)

        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)

        '  render the htmlwriter into the response
        'style to format numbers to string
        Dim style As String = "<style>.textmode{mso-number-format:\@;}</style>"
        'HttpContext.Current.Response.Write(style)
        HttpContext.Current.Response.Write(sw.ToString)
        'HttpContext.Current.Response.Write(style + sw.ToString())
        HttpContext.Current.Response.End()

        ' Response.Write(style)
        'Response.Output.Write(sw.ToString())


    End Sub
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx")

    End Sub

    
End Class
