Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Partial Class Controls_AssetReq
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                BindUsers(UID)
                'BindCategories()
                getassetcategory()
                getassetsubcategory()
                BindLocation()
                ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
                ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
                pnlItems.Visible = True
                lblMsg.Visible = False
                gvItems.Visible = True
                remarksAndSubmitBtn.Visible = True
                fillgrid()

            End If
        End If
    End Sub
    Private Sub BindUsers(ByVal aur_id As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = aur_id
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP", "NAME", "AUR_ID", param)

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_bindUsers_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 0, 0)
    End Sub
    Private Sub BindCategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
    End Sub
    
    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged

        pnlItems.Visible = False
        remarksAndSubmitBtn.Visible = False
        getassetsubcategory()
    End Sub
   Private Sub getbrandbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        pnlItems.Visible = False
    End Sub
    Private Function GenerateRequestId() As String
        Dim ReqId As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetMaxAIR_SNO")
        Dim Id As Integer = CInt(sp.ExecuteScalar())
        Return Session("UID") + "/AST/REQ/" + CStr(Id)
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim ReqId As String = GenerateRequestId()
        Validate(ReqId)
    End Sub
    Private Sub Validate(ByVal ReqId As String)
        Dim VT_Code As String
        Dim Ast_SubCat_Code As String
        Dim manufactuer_code As String
        Dim Ast_Md_code As String

        Dim count As Integer = 0
        Dim Message As String = String.Empty
        Dim mdcode As String = String.Empty
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim lblmdcode As Label = DirectCast(row.FindControl("lblmdcode"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            mdcode = lblmdcode.Text
            Dim n As Integer

            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                    Exit Sub
                ElseIf IsNumeric(txtQty.Text) = False Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Please enter Quantity in numbers..."
                    Exit Sub
                ElseIf CInt(txtQty.Text) > 0 Then
                    count = count + 1
                    If count = 0 Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Please Enter Required Quantity And Select The Product"
                        Exit Sub
                    End If
                Else
                    lblMsg.Text = "Please enter more than Zero"
                End If
            End If
        Next
        If count > 0 Then
            'InsertData(ReqId, txtRemarks.Text)
            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)

             


                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Please enter Quantity for Selected Checkbox"


                    ElseIf CInt(txtQty.Text) > 0 Then
                        count = count + 1
                        If count > 0 Then

                            Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
                            Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
                            Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
                            Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)

                            VT_Code = lbl_vt_code.Text
                            Ast_SubCat_Code = lbl_ast_subcat_code.Text
                            manufactuer_code = lbl_manufactuer_code.Text
                            Ast_Md_code = lbl_ast_md_code.Text

                            InsertDetails(ReqId, lblProductId.Text, CInt(Trim(txtQty.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code)
                        End If
                    End If

                End If
            Next
            InsertData(ReqId, txtRemarks.Text, ddlAstCat.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value, ddlAstBrand.SelectedItem.Value, ddlAstModel.SelectedItem.Value, ddlLocation.SelectedItem.Value)
            ' send_mail(ReqId)
            Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
        ElseIf count = 0 Then
            lblMsg.Visible = True
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        End If
    End Sub

    'Private Function IsNumeric(ByVal txt As String)
    '    Dim value As String = Convert.ToInt32(txt.ToString())
    '    Return value
    'End Function
    Private Sub InsertData(ByVal ReqId As String, ByVal Remarks As String, ByVal cat1 As String, ByVal subcat1 As String, ByVal brand1 As String, ByVal Model1 As String, ByVal reqLocation As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisition_AddNew")
        sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp1.Command.AddParameter("@AurId", Session("uid"), DbType.String)
        sp1.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp1.Command.AddParameter("@CatId", cat1, DbType.String)
        sp1.Command.AddParameter("@AIR_ITEM_SUBCAT", subcat1, DbType.String)
        sp1.Command.AddParameter("@AIR_ITEM_BRD", brand1, DbType.String)
        sp1.Command.AddParameter("@AIR_ITEM_MOD", Model1, DbType.String)
        sp1.Command.AddParameter("@AIR_REQ_LOCATION", reqLocation, DbType.String)
        sp1.ExecuteScalar()
    End Sub
    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_AddNew")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.Command.AddParameter("@ITEM_TYPE", VT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", AST_SUBCAT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", manufactuer_code, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", AST_MD_CODE, DbType.String)
        sp.ExecuteScalar()
    End Sub

  

    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged

        getmakebycatsubcat()
        remarksAndSubmitBtn.Visible = False
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKE_FOR_ASSETGRID")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            remarksAndSubmitBtn.Visible = False
        End If

    End Sub

    Public Sub send_mail(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Execute()
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, "--All--")
    End Sub
    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        '    If ddlAstCat.SelectedIndex > 0 Then
        getbrandbycatsubcat()
        remarksAndSubmitBtn.Visible = False
        '   End If
    End Sub
    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 0, 0)
    End Sub
    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        'ddlAstSubCat.Items.Insert(0, "--All--")
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
        'pnlItems.Visible = False
        'remarksAndSubmitBtn.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        gvItems.Visible = True
        pnlItems.Visible = True
        remarksAndSubmitBtn.Visible = True
       fillgrid()
    End Sub
    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()
        pnlItems.Visible = False
        remarksAndSubmitBtn.Visible = False
        getassetsubcategory()
        ddlAstBrand.Items.Clear()
        ddlAstModel.Items.Clear()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
   
End Class
