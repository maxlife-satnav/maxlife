Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptViewRequisition
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            DEL_TEMP_CONV()
            BindDep()
            Dept()
            BindRM()
            BindDetails()
            txtJrDate.Text = getoffsetdate(Date.Today)
            txtJrDate.Attributes.Add("onClick", "displayDatePicker('" + txtJrDate.ClientID + "')")
            txtJrDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            btnsubmit.Visible = False
            btnModify.Visible = False
            lblTotalAmount.Visible = False
            lblTamount.Visible = False
            tr1.Visible = False
            table4.Visible = False
            txtAssociateName.ReadOnly = True
            ddlDep.Enabled = False
            txtAssociateID.ReadOnly = True
            ddlRM.Enabled = False
            txtDesig.ReadOnly = True
        End If
        txtJrDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub DEL_TEMP_CONV()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LCONV_DEL")
            sp.Command.AddParameter("@ESP_AUR_ID", Session("UID"), DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GETASSOCIATE_DETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("AUR_NO")
                ddlDep.ClearSelection()
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                ddlRM.ClearSelection()
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMode.SelectedIndexChanged
        If ddlMode.SelectedValue = "CR" Then
            tr1.Visible = True
            If txtKM.Text = "" Then
                lblMsg.Text = "Please enter Approximate Kilometers "
            Else
                lblMsg.Text = ""
                txtAmount.Text = 5.8 * (txtKM.Text)
                txtAmount.ReadOnly = True
            End If
        ElseIf ddlMode.SelectedValue = "BKE" Then
            tr1.Visible = True
            If txtKM.Text = "" Then
                lblMsg.Text = "Please enter Approximate Kilometers "
            Else
                lblMsg.Text = ""
                txtAmount.Text = 2.3 * (txtKM.Text)
                txtAmount.ReadOnly = True
            End If
        Else
            tr1.Visible = False
            lblMsg.Text = ""
            txtAmount.Text = ""
            txtAmount.ReadOnly = False
        End If
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblMsg.Text = ""
            'Dim Reqid As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("UID")
            'txtStore.Text = Reqid
            Add_Temp_Local_Conveyance()
            BindGrid()
            If gvItems.Rows.Count > 0 Then
                btnsubmit.Visible = True
                lblTotalAmount.Visible = True
                lblTamount.Visible = True
                table4.Visible = True
            Else
                btnsubmit.Visible = False
                lblTotalAmount.Visible = False
                lblTamount.Visible = False
                table4.Visible = False
            End If
            ClearConveyance()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try  
    End Sub
    Private Sub Add_Temp_Local_Conveyance()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_ADD_LOCAL_CONVEYANCE_TEMP")
        sp.Command.AddParameter("@ESP_AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@ESP_JRNY_DATE", txtJrDate.Text, DbType.Date)
        sp.Command.AddParameter("@ESP_FROM", txtFrom.Text, DbType.String)
        sp.Command.AddParameter("@ESP_TO", txtTp.Text, DbType.String)
        sp.Command.AddParameter("@ESP_MODE_TRVL", ddlMode.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@ESP_PURPOSE_TRVL", txtPurpose.Text, DbType.String)
        sp.Command.AddParameter("@ESP_AMOUNT", txtAmount.Text, DbType.Decimal)
        If txtKM.Text = "" Then
            txtKM.Text = 0
        End If
        sp.Command.AddParameter("@ESP_KM", txtKM.Text, DbType.Decimal)
        sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub ClearConveyance()
        txtJrDate.Text = getoffsetdate(Date.Today)
        txtFrom.Text = ""
        txtTp.Text = ""
        ddlMode.SelectedIndex = 0
        txtPurpose.Text = ""
        txtAmount.Text = ""
        txtKM.Text = ""
        txtRemarks.Text = ""
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_BINDDETAILS")
            sp.Command.AddParameter("@ESP_AUR_ID", Session("UID"), DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim lblMode As Label = CType(gvItems.Rows(i).FindControl("lblMode"), Label)
                If lblMode.Text = "FLT" Then
                    lblMode.Text = "Flight"
                ElseIf lblMode.Text = "TRN" Then
                    lblMode.Text = "Train"
                ElseIf lblMode.Text = "BS" Then
                    lblMode.Text = "Bus"
                ElseIf lblMode.Text = "CR" Then
                    lblMode.Text = "Car"
                ElseIf lblMode.Text = "ATO" Then
                    lblMode.Text = "AUTO"
                ElseIf lblMode.Text = "BKE" Then
                    lblMode.Text = "Bike"
                End If
            Next
            Total_Amount()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Total_Amount()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_TOTAL_EXPENSE")
        sp.Command.AddParameter("@ESP_AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblTotalAmount.Visible = True
            lblTamount.Visible = True
            lblTotalAmount.Text = "Rs." & "" & ds.Tables(0).Rows(0).Item("Amount")
        End If

    End Sub
    Private Sub Add_Local_Conveyance(ByVal ReqId As String)
        Try
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_HISTORY_ADD")
                sp.Command.AddParameter("@ESP_REQID", ReqId, DbType.String)
                sp.Command.AddParameter("@ESP_AUR_ID", txtAssociateID.Text, DbType.String)
                sp.Command.AddParameter("@ESP_AUR_RM", ddlRM.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@ESP_AUR_DEPT", ddlDep.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@ESP_AUR_DESGN", txtDesig.Text, DbType.String)
                sp.Command.AddParameter("@ESP_JRNY_DATE", CType(gvItems.Rows(i).FindControl("lbljrdate"), Label).Text, DbType.Date)
                sp.Command.AddParameter("@ESP_FROM", CType(gvItems.Rows(i).FindControl("lblFrom"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_TO", CType(gvItems.Rows(i).FindControl("lblTo"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_MODE_TRVL", CType(gvItems.Rows(i).FindControl("lblMode"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_PURPOSE_TRVL", CType(gvItems.Rows(i).FindControl("lblPurpose"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_AMOUNT", CType(gvItems.Rows(i).FindControl("lblAmount"), Label).Text, DbType.Decimal)
                sp.Command.AddParameter("@ESP_KM", CType(gvItems.Rows(i).FindControl("lblkms"), Label).Text, DbType.Decimal)
                sp.Command.AddParameter("@REMARKS", CType(gvItems.Rows(i).FindControl("lblRem"), Label).Text, DbType.String)
                sp.ExecuteScalar()
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "EDIT" Then
            btnAdd.Visible = False
            lblMsg.Text = ""
            btnModify.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As Integer = Integer.Parse(lblID.Text)
            Show_Data(id)
            txtstore1.Text = id
        ElseIf e.CommandName = "DELETE" Then
            DEL_CONV(txtstore1.Text)
            BindGrid()
        End If
    End Sub
    Private Sub DEL_CONV(ByVal id1 As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_HISTORY_DELETE")
            sp.Command.AddParameter("@id", id1, DbType.Int32)
            sp.ExecuteScalar()
            If gvItems.Rows.Count > 0 Then
                lblTotalAmount.Visible = True
                lblTamount.Visible = True
                btnsubmit.Visible = True
            Else
                lblTotalAmount.Visible = False
                lblTamount.Visible = False
                btnsubmit.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Show_Data(ByVal id As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_GETDETAILS")
            sp.Command.AddParameter("@id", id, DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtJrDate.Text = ds.Tables(0).Rows(0).Item("ESP_JRNY_DATE")
                txtFrom.Text = ds.Tables(0).Rows(0).Item("ESP_FROM")
                txtTp.Text = ds.Tables(0).Rows(0).Item("ESP_TO")
                txtPurpose.Text = ds.Tables(0).Rows(0).Item("ESP_PURPOSE_TRVL")
                ddlMode.ClearSelection()
                ddlMode.Items.FindByValue(ds.Tables(0).Rows(0).Item("ESP_MODE_TRVL")).Selected = True
                txtAmount.Text = ds.Tables(0).Rows(0).Item("ESP_AMOUNT")
                Dim km As Decimal = ds.Tables(0).Rows(0).Item("ESP_APPRX_KM")
                If km <> 0 Then
                    tr1.Visible = True
                    txtKM.Text = km
                Else
                    tr1.Visible = False

                End If
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("REMARKS")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvItems.RowEditing

    End Sub

    Protected Sub gvItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItems.RowDeleting

    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        Try
            lblMsg.Text = ""
            Modify_Conveyance()
            ClearConveyance()
            BindGrid()
            btnAdd.Visible = True
            btnModify.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Modify_Conveyance()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_HISTORY_MODIFY")
            sp.Command.AddParameter("@ID", txtstore1.Text, DbType.Int32)
            sp.Command.AddParameter("@ESP_JRNY_DATE", txtJrDate.Text, DbType.Date)
            sp.Command.AddParameter("@ESP_FROM", txtFrom.Text, DbType.String)
            sp.Command.AddParameter("@ESP_TO", txtTp.Text, DbType.String)
            sp.Command.AddParameter("@ESP_MODE_TRVL", ddlMode.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@ESP_PURPOSE_TRVL", txtPurpose.Text, DbType.String)
            sp.Command.AddParameter("@ESP_AMOUNT", txtAmount.Text, DbType.Decimal)
            sp.Command.AddParameter("@ESP_KM", txtKM.Text, DbType.Decimal)
            sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If gvItems.Rows.Count > 0 Then
            Dim Reqid As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("UID")
            Add_Local_Conveyance_Request(ReqId)
            Add_Local_Conveyance(Reqid)
            ClearConveyance()
            btnModify.Visible = False
            btnAdd.Visible = True
            table4.Visible = False
            lblMsg.Text = "Your Request Has been sent for Approval"
            mail_CNVReq(Reqid)
        End If
    End Sub
    Private Sub Add_Local_Conveyance_Request(ByVal id As String)
        Try
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_ADD_LOCAL_CONVEYANCE")
                sp.Command.AddParameter("@ESP_REQID", id, DbType.String)
                sp.Command.AddParameter("@ESP_AUR_ID", Session("UID"), DbType.String)
                sp.Command.AddParameter("@ESP_JRNY_DATE", CType(gvItems.Rows(i).FindControl("lbljrdate"), Label).Text, DbType.Date)
                sp.Command.AddParameter("@ESP_FROM", CType(gvItems.Rows(i).FindControl("lblFrom"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_TO", CType(gvItems.Rows(i).FindControl("lblTo"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_MODE_TRVL", CType(gvItems.Rows(i).FindControl("lblMode"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_PURPOSE_TRVL", CType(gvItems.Rows(i).FindControl("lblPurpose"), Label).Text, DbType.String)
                sp.Command.AddParameter("@ESP_AMOUNT", CType(gvItems.Rows(i).FindControl("lblAmount"), Label).Text, DbType.Decimal)
                sp.Command.AddParameter("@ESP_KM", CType(gvItems.Rows(i).FindControl("lblkms"), Label).Text, DbType.Decimal)
                sp.Command.AddParameter("@REMARKS", CType(gvItems.Rows(i).FindControl("lblRem"), Label).Text, DbType.String)
                sp.ExecuteScalar()
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    
    Private Sub mail_CNVReq(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strHrEmail As String                              'To hold the HR Email
        Dim Amount As Decimal
        Dim strPurpose As String


        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_REQUISITIONS_MAIL")
        sp1.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        gvdetails.Datasource = sp1.GetDataSet()
        gvdetails.Databind()


        strSubj = " Local Conveyance Requisition."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONEVYANCE_BINDDETAILS_MAIL")
        sp.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        Dim strfrommail, strfromname As String

       
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"

        strSubj = "Local Conveyance Request - by " & strEmpName & "."


        'Mail message for the Requisitioner. 

        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
        "This is to inform your that these are the Details of Your  Local Conveyance Requisitions" & _
        "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
        "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
        "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
         "<td  align=left width=12.5%><U>From Place: </U></td>" & _
        "<td  align=left width=12.5%><U>To Place: </U></td>" & _
        "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
        "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
           "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
        "<td  align=left width=12.5%><U>Amount </U></td>" & _
        "<td  align=left width=12.5%><U>Remarks </U></td></tr></table>"

        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrem"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
       
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE")
        sp2.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")

        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
 "This is to inform your that these are the Details of  " & strEmpName & "'s  Local Conveyance Requisitions" & _
 "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
        "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
        "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
         "<td  align=left width=12.5%><U>From Place: </U></td>" & _
        "<td  align=left width=12.5%><U>To Place: </U></td>" & _
        "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
        "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
         "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
        "<td  align=left width=12.5%><U>Amount </U></td>" & _
        "<td  align=left width=12.5%><U>Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrem"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "NOT SENT", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub txtKM_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKM.TextChanged
        If ddlMode.SelectedValue = "CR" Then
            tr1.Visible = True
            If txtKM.Text = "" Then
                lblMsg.Text = "Please enter Approximate Kilometers "
            Else
                lblMsg.Text = ""
                txtAmount.Text = 5.8 * (txtKM.Text)
                txtAmount.ReadOnly = True
            End If
        ElseIf ddlMode.SelectedValue = "BKE" Then
            tr1.Visible = True
            If txtKM.Text = "" Then
                lblMsg.Text = "Please enter Approximate Kilometers "
            Else
                lblMsg.Text = ""
                txtAmount.Text = 2.3 * (txtKM.Text)
                txtAmount.ReadOnly = True
            End If
        Else
            tr1.Visible = False
            lblMsg.Text = ""
            txtAmount.Text = ""
            txtAmount.ReadOnly = False
        End If
    End Sub
End Class
