Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_HRSurrenderLease
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            BindPropType()
            BindCity()
            pnltermination.Visible = False
            ddlproptype.Enabled = False
            ddlCity.Enabled = False
            txtlease.ReadOnly = True
            panel2.Visible = False
            panel3.Visible = False
            panel4.Visible = False
            gvempdetails.Visible = False
            btnModify.Visible = False
            ddlproptype.SelectedValue = "1"
            ddlproptype.Enabled = False
            txtTedate.Text = Date.Today.ToString("MM/dd/yyyy")
        End If
        txtTdate.Attributes.Add("readonly", "readonly")
        txtTedate.Attributes.Add("readonly", "readonly")
        txtrdzdate.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_LEASE_DESK_HR1")
        sp.Command.AddParameter("@EMPID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
    End Sub

    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindCity()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTCTY_ENTITLE")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlCity.DataSource = sp.GetDataSet()
            ddlCity.DataTextField = "CTY_NAME"
            ddlCity.DataValueField = "CTY_CODE"
            ddlCity.DataBind()
            ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "Surrender" Then
            pnltermination.Visible = True
            Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
            Dim lblLseName As Label = DirectCast(gvRow.FindControl("lblLseName"), Label)
            Dim code As String = lblLseName.Text
            txtstore.Text = code
            BindDetails()
            BindEmpDetails()
            gvempdetails.Visible = True
            If txtapay.Text = 0 Then
                txtamt.ReadOnly = True
                txtamt.Text = 0
                btnSubmit.Visible = False
                btnModify.Visible = True
                'btnterminatepay.Visible = False
            Else
                btnSubmit.Visible = True
                btnModify.Visible = False
                'btnterminatepay.Visible = True
            End If
        Else
            txtedate.Text = ""
            txtapay.Text = ""
            txtlcost.Text = ""
            txtSdate.Text = ""
        End If
    End Sub

    Private Sub BindEmpDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AXIS_SP_GETEMPDETAILS_LEASE")
        sp.Command.AddParameter("@LEASE_ID", txtstore.Text, DbType.String)
        gvempdetails.DataSource = sp.GetDataSet()
        gvempdetails.DataBind()
    End Sub

    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_LEASES_ALL_DETAILS")
            sp.Command.AddParameter("@LeaseCode", txtstore.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlproptype.ClearSelection()
                ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("PROPERTY_TYPE")).Selected = True
                ddlCity.ClearSelection()
                ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CITY")).Selected = True
                txtlease.Text = txtstore.Text
                txtedate.Text = ds.Tables(0).Rows(0).Item("LEASE_EXPIRY_DATE")
                txtSdate.Text = ds.Tables(0).Rows(0).Item("LEASE_START_DATE")
                txtlcost.Text = ds.Tables(0).Rows(0).Item("LEASE_INVESTED_AREA")
                txtapay.Text = ds.Tables(0).Rows(0).Item("ADVANCE")
                txtamt.Text = ds.Tables(0).Rows(0).Item("PAID_AMOUNT1")
                txtTdate.Text = ds.Tables(0).Rows(0).Item("PAID_DATE1")
            End If
        Catch ex As Exception
            Response.Redirect(ex.Message)
        End Try
    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim amt As Decimal = 0
        If txtamt.Text <> "" Then
            amt = CDbl(txtamt.Text)
        End If

        If CDate(txtTdate.Text) < getoffsetdatetime(DateTime.Now) Or CDate(txtTedate.Text) < getoffsetdatetime(DateTime.Now) Or CDate(txtrdzdate.Text) < getoffsetdatetime(DateTime.Now) Then
            lblMsg.Text = "Past date Not allowed"
            Exit Sub
        End If

        If txtTdate.Text <> "" Then
            If amt > CDbl(txtapay.Text) Then
                lblMsg.Text = "Amount should not be more than Security Deposit"
            Else
                lblMsg.Text = ""
                UpdateStatus()
                UpdateRecord()
            End If
        Else
            If amt > CDbl(txtapay.Text) Then
                lblMsg.Text = "Amount should not be more than Security Deposit"
            Else
                lblMsg.Text = ""
                UpdateStatus()
                UpdateRecord()
            End If
        End If
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=43")
    End Sub

    Private Function validateTDate()
        Dim Validatedate As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_TERMINATE_LEASES_TDATE")
        sp.Command.AddParameter("@LEASE_CODE", txtstore.Text, DbType.String)
        sp.Command.AddParameter("@tdate", txtTdate.Text, DbType.Date)
        Validatedate = sp.ExecuteScalar()
        Return Validatedate
    End Function

    Private Sub UpdateStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_LEASES_STATUS")
        sp.Command.AddParameter("@LEASE_CODE", txtstore.Text, DbType.String)
        sp.Command.AddParameter("@TERMINATED_BY", Session("uid"), DbType.String)
        If txtTdate.Text = "" Then
            sp.Command.AddParameter("@TERMINATED_DATE", DBNull.Value, DbType.Date)
        Else
            sp.Command.AddParameter("@TERMINATED_DATE", txtTedate.Text, DbType.Date)
        End If
        sp.ExecuteScalar()
    End Sub

    Private Sub UpdateRecord()
        Dim sd As Decimal = 0
        If txtamt.Text <> "" Then
            sd = CDbl(txtamt.Text)
        End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_TERMINATE_LEASE_DETAILS_ADD")
        sp.Command.AddParameter("@LEASE_CODE", txtstore.Text, DbType.String)
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@PROPERTY_TYPE", ddlproptype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LEASE_EXPIRY_DATE", txtedate.Text, DbType.Date)
        sp.Command.AddParameter("@LEASE_START_DATE", txtSdate.Text, DbType.Date)
        sp.Command.AddParameter("@ADVANCE", txtapay.Text, DbType.Decimal)
        sp.Command.AddParameter("@LEASECOST", txtlcost.Text, DbType.Decimal)
        sp.Command.AddParameter("@PAIDAMOUNT", sd, DbType.Decimal)
        sp.Command.AddParameter("@PAY_MODE", ddlpaymentmode.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        If txtTdate.Text = "" Then
            sp.Command.AddParameter("@PAIDDATE", DBNull.Value, DbType.Date)
        Else
            sp.Command.AddParameter("@PAIDDATE", txtTdate.Text, DbType.Date)
        End If

        If txtrdzdate.Text = "" Then
            sp.Command.AddParameter("@REALIZATIONDATE", DBNull.Value, DbType.Date)
        Else
            sp.Command.AddParameter("@REALIZATIONDATE", txtrdzdate.Text, DbType.Date)
        End If

        sp.Command.AddParameter("@TerminatedBy", Session("uid"), DbType.String)
        sp.Command.AddParameter("@TerminatedDate", txtTedate.Text, DbType.Date)

        If ddlpaymentmode.SelectedItem.Value = "1" Then
            sp.Command.AddParameter("@Cheque", "", DbType.String)
            sp.Command.AddParameter("@IssuingBank", "", DbType.String)
            sp.Command.AddParameter("@DepositedBank", txtBankName.Text, DbType.String)
            sp.Command.AddParameter("@AccNo", txtAccNo.Text, DbType.String)
            sp.Command.AddParameter("@IFSC", "", DbType.String)
            sp.Command.AddParameter("@Branch_Name", "", DbType.String)
        ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then
            sp.Command.AddParameter("@Cheque", "", DbType.String)
            sp.Command.AddParameter("@IssuingBank", "", DbType.String)
            sp.Command.AddParameter("@DepositedBank", "", DbType.String)
            sp.Command.AddParameter("@AccNo", TXTacccredit.Text, DbType.String)
            sp.Command.AddParameter("@IFSC", "", DbType.String)
            sp.Command.AddParameter("@Branch_Name", "", DbType.String)

        ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
            sp.Command.AddParameter("@Cheque", "", DbType.String)
            sp.Command.AddParameter("@IssuingBank", "AXIS", DbType.String)
            sp.Command.AddParameter("@DepositedBank", txtDeposited.Text, DbType.String)
            sp.Command.AddParameter("@AccNo", txtIBankName.Text, DbType.String)
            sp.Command.AddParameter("@IFSC", txtIFSC.Text, DbType.String)
            sp.Command.AddParameter("@Branch_Name", txtbrnch.Text, DbType.String)
        End If
        sp.ExecuteScalar()

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_REFUND_AMOUNT")
        sp1.Command.AddParameter("@LEASE_CODE", txtstore.Text, DbType.String)
        sp1.ExecuteScalar()
    End Sub

    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "1" Then
                panel2.Visible = True
                panel3.Visible = False
                panel4.visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then
                panel2.Visible = False
                panel3.Visible = False
                panel4.visible = True
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel2.Visible = False
                panel3.Visible = True
                panel4.visible = False
            End If
        End If
        GET_TOTAMT()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CDate(txtTdate.Text) < getoffsetdatetime(DateTime.Now) Or CDate(txtTedate.Text) < getoffsetdatetime(DateTime.Now) Or CDate(txtrdzdate.Text) < getoffsetdatetime(DateTime.Now) Then
                lblMsg.Text = "Past date Not allowed"
                Exit Sub
            End If

            Dim amt As Decimal = 0
            If txtamt.Text <> "" Then
                amt = CDbl(txtamt.Text)
            End If

            If txtTdate.Text <> "" Then
                If amt > CDbl(txtapay.Text) Then
                    lblMsg.Text = "Amount should not be more than Security Deposit"
                Else
                    lblMsg.Text = ""
                    UpdateSaveStatus()
                    UpdateRecord()
                End If
            Else
                If amt > CDbl(txtapay.Text) Then
                    lblMsg.Text = "Amount should not be more than Security Deposit"
                Else
                    lblMsg.Text = ""
                    UpdateSaveStatus()
                    UpdateRecord()
                End If
            End If
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=39")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub UpdateSaveStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_LEASES_ALL_STATUS_SAVE")
        sp.Command.AddParameter("@LEASE_CODE", txtstore.Text, DbType.String)
        sp.Command.AddParameter("@TERMINATED_BY", Session("uid"), DbType.String)
        If txtTdate.Text = "" Then
            sp.Command.AddParameter("@TERMINATED_DATE", DBNull.Value, DbType.Date)
        Else
            sp.Command.AddParameter("@TERMINATED_DATE", txtTedate.Text, DbType.Date)
        End If
        sp.ExecuteScalar()
    End Sub

    Protected Sub txtamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            GET_TOTAMT()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GET_TOTAMT()
        If txtamt.Text = "" Then
            txtamt.Text = 0
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_TERMINATE_LEASES_ALL_AMOUNT")
        sp.Command.AddParameter("@LEASE_CODE", txtstore.Text, DbType.String)
        sp.Command.AddParameter("@AMOUNT", txtamt.Text, DbType.Decimal)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            Dim val As Decimal = ds.Tables(0).Rows(0).Item("TOT")
            If val = 0 Then
                btnSubmit.Visible = False
                btnModify.Visible = True
                ' btnterminatepay.Visible = False
            Else
                btnSubmit.Visible = True
                btnModify.Visible = False
                'btnterminatepay.Visible = True
            End If
        End If
    End Sub

    Protected Sub btnterminatepay_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim validatedate As Integer
        validatedate = validateTDate()
        If validatedate = 0 Then
            lblMsg.Text = "Paid Date Should be greater than Effective Date of Agreement"
        ElseIf CDbl(txtamt.Text) > CDbl(txtapay.Text) Then
            lblMsg.Text = "Amount should not be more than Security Deposit"
        Else
            lblMsg.Text = ""
            UpdateSaveStatus()
            UpdateRecord()
        End If
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=44")
    End Sub
End Class