<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Update_Maintain_Schedule.ascx.vb"
    Inherits="Controls_Update_Maintain_Schedule" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript" language="javascript" src="../scripts/frmamtcal.js"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Update Plan Details
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Update Plan Details</strong>
            </td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <table width="100%" cellpadding="2px">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>&nbsp;<asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:DataGrid ID="PM_REQ_DATA" runat="server" Width="100%" OnPageIndexChanged="PM_REQ_DATA_Paged"
                                AutoGenerateColumns="False" HorizontalAlign="Center" AllowSorting="True" AllowPaging="True"
                                PageSize="15">
                                <Columns>
                                    <asp:ButtonColumn Text="Update" HeaderText="Action" CommandName="Edit">
                                        <HeaderStyle Width="8%" CssClass="LABEL"></HeaderStyle>
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn DataField="PVD_ID" HeaderText="S.No.">
                                        <HeaderStyle Width="8%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AAT_CODE" HeaderText="Asset Code">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AAT_NAME" HeaderText="Asset Name">
                                        <HeaderStyle Width="12%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PVD_PLANSCHD_DT" HeaderText="Schedule Date" DataFormatString="{0:d}">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="STA_TITLE" HeaderText="Status">
                                        <HeaderStyle Width="5%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="stime" HeaderText="Schedule Time">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="EXEC_DT" HeaderText="Executed Date">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ETIME" HeaderText="Executed Time">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SCOST" HeaderText="Spare Cost">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCOST" HeaderText="Labour Cost">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="REMARKS" HeaderText="Remarks">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="brkStart" HeaderText="Breakdown Startup Time">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="brkEnd" HeaderText="Breakdown Endup Time">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="brkExp" HeaderText="Breakdown Expenses">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="brkRem" HeaderText="Breakdown Remarks">
                                        <HeaderStyle Width="10%" CssClass="LABEL"></HeaderStyle>
                                    </asp:BoundColumn>
                                </Columns>
                                <PagerStyle NextPageText=" Next" PrevPageText="Previous " HorizontalAlign="Left"
                                    Position="Top"></PagerStyle>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlButton" runat="server" Width="100%" Visible="False">
                                <table id="tblSubmit1" border="1" cellspacing="1" cellpadding="1" width="100%">
                                    <tr>
                                        <td style="height: 21px" class="label" width="50%">
                                            <asp:Label ID="lblComp" runat="server" CssClass="label" Text="Executed Date">Executed Date<font class="clsNote">
									</font></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtExeDate"
                                                ErrorMessage="Required Executed Date " SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtExeDate"
                                                Display="None" ErrorMessage="Invalid Date" Operator="DataTypeCheck" SetFocusOnError="True"
                                                Type="Date"></asp:CompareValidator></td>
                                        <td style="height: 21px" width="50%" align="left">
                                            <asp:TextBox ID="txtExeDate" runat="server" CssClass="clsTextField" ></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtExeDate" runat="server">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="50%">
                                            <asp:Label ID="Label4" runat="server" CssClass="label" Text="Executed Time">Executed Time<font class="clsNote">
									</font></asp:Label>
                                            <asp:RegularExpressionValidator ID="revTime" runat="server" ErrorMessage="Incorrect Time Format !  (Please follow 24Hr Format)"
                                                Display="None" ControlToValidate="txtExeTime" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"></asp:RegularExpressionValidator>
                                            &nbsp;(Format: 00:00 to 23:59)
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtExeTime"
                                                ErrorMessage="Required Executed Time" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator></td>
                                        <td width="50%" align="left">
                                            <asp:TextBox ID="txtExeTime" CssClass="clsTextField" Width="100%" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="50%">
                                            <asp:Label ID="Label1" runat="server" CssClass="label" Text="Spared Cost">Spared Cost<FONT class="clsNote">
									</FONT></asp:Label>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSpr"
                                                ErrorMessage="Invalid Digits" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)"  SetFocusOnError="True" Display="None"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSpr"
                                                Display="None" ErrorMessage="Required Spared Cost" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                                        <td width="50%" align="left">
                                            <asp:TextBox ID="txtSpr" CssClass="clsTextField" Width="100%" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="50%">
                                            <asp:Label ID="Label2" runat="server" CssClass="label" Text="Labour Cost">Labour Cost<FONT class="clsNote">
									</FONT></asp:Label>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtLbr"
                                                Display="None" ErrorMessage="Invalid Digits" SetFocusOnError="True" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLbr"
                                                Display="None" ErrorMessage="Required Labour Cost" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                                        <td width="50%" align="left">
                                            <asp:TextBox ID="txtLbr" CssClass="clsTextField" Width="100%" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="50%">
                                            Remarks<font class="clsNote"></font>
                                        </td>
                                        <td width="50%" align="left">
                                            <asp:TextBox ID="txtRmks" CssClass="clsTextField" Width="100%" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td class="label" width="50%">
                                            Want to Enter Breakdown Info?&nbsp;
                                        </td>
                                        <td width="50%" align="left">
                                            <asp:CheckBox ID="chkBrk" runat="server"   ></asp:CheckBox></td>
                                    </tr>
                                </table>
                                <div id="divtest" style="display:none;">
                               <table id="tblSubmit" border="1" cellspacing="1" cellpadding="1" width="100%">
                                        <tr>
                                            <td style="height: 18px" class="label" width="50%">
                                                <asp:Label ID="Label3" runat="server" CssClass="label" Text="Executed Date">Startup Time & Date:</asp:Label>
                                            </td>
                                            <td style="height: 18px" width="50%" align="left">
                                                <asp:DropDownList ID="cboBRKStHr" runat="server" CssClass="clsComboBox" Width="15%">
                                                    <asp:ListItem Value="00">00</asp:ListItem>
                                                    <asp:ListItem Value="01">01</asp:ListItem>
                                                    <asp:ListItem Value="02">02</asp:ListItem>
                                                    <asp:ListItem Value="03">03</asp:ListItem>
                                                    <asp:ListItem Value="04">04</asp:ListItem>
                                                    <asp:ListItem Value="05">05</asp:ListItem>
                                                    <asp:ListItem Value="06">06</asp:ListItem>
                                                    <asp:ListItem Value="07">07</asp:ListItem>
                                                    <asp:ListItem Value="08">08</asp:ListItem>
                                                    <asp:ListItem Value="09">09</asp:ListItem>
                                                    <asp:ListItem Value="10">10</asp:ListItem>
                                                    <asp:ListItem Value="11">11</asp:ListItem>
                                                    <asp:ListItem Value="12">12</asp:ListItem>
                                                    <asp:ListItem Value="13">13</asp:ListItem>
                                                    <asp:ListItem Value="14">14</asp:ListItem>
                                                    <asp:ListItem Value="15">15</asp:ListItem>
                                                    <asp:ListItem Value="16">16</asp:ListItem>
                                                    <asp:ListItem Value="17">17</asp:ListItem>
                                                    <asp:ListItem Value="18">18</asp:ListItem>
                                                    <asp:ListItem Value="19">19</asp:ListItem>
                                                    <asp:ListItem Value="20">20</asp:ListItem>
                                                    <asp:ListItem Value="21">21</asp:ListItem>
                                                    <asp:ListItem Value="22">22</asp:ListItem>
                                                    <asp:ListItem Value="23">23</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="cboBRKStMin" runat="server" CssClass="clsComboBox" Width="15%">
                                                    <asp:ListItem Value="00">00</asp:ListItem>
                                                    <asp:ListItem Value="15">15</asp:ListItem>
                                                    <asp:ListItem Value="30">30</asp:ListItem>
                                                    <asp:ListItem Value="45">45</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtBRKStartDt" runat="server" CssClass="clsTextField"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="txtBRKStartDt" runat="server">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" width="50%">
                                                <asp:Label ID="Label5" runat="server" CssClass="label" Text="Executed Time">Expenditure Incurred for the Services(in INR):</asp:Label>
                                            </td>
                                            <td width="50%" align="left">
                                                <asp:TextBox ID="txtBrkExp" CssClass="clsTextField" Width="100%"
                                                    runat="server" MaxLength="10"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 24px" class="label" width="50%">
                                                <asp:Label ID="Label6" runat="server" CssClass="label" Text="Spared Cost">Endup Time & Date:</asp:Label>
                                            </td>
                                            <td style="height: 24px" width="50%" align="left">
                                                <asp:DropDownList ID="cboBRKEndHr" runat="server" CssClass="clsComboBox" Width="15%">
                                                    <asp:ListItem Value="00">00</asp:ListItem>
                                                    <asp:ListItem Value="01">01</asp:ListItem>
                                                    <asp:ListItem Value="02">02</asp:ListItem>
                                                    <asp:ListItem Value="03">03</asp:ListItem>
                                                    <asp:ListItem Value="04">04</asp:ListItem>
                                                    <asp:ListItem Value="05">05</asp:ListItem>
                                                    <asp:ListItem Value="06">06</asp:ListItem>
                                                    <asp:ListItem Value="07">07</asp:ListItem>
                                                    <asp:ListItem Value="08">08</asp:ListItem>
                                                    <asp:ListItem Value="09">09</asp:ListItem>
                                                    <asp:ListItem Value="10">10</asp:ListItem>
                                                    <asp:ListItem Value="11">11</asp:ListItem>
                                                    <asp:ListItem Value="12">12</asp:ListItem>
                                                    <asp:ListItem Value="13">13</asp:ListItem>
                                                    <asp:ListItem Value="14">14</asp:ListItem>
                                                    <asp:ListItem Value="15">15</asp:ListItem>
                                                    <asp:ListItem Value="16">16</asp:ListItem>
                                                    <asp:ListItem Value="17">17</asp:ListItem>
                                                    <asp:ListItem Value="18">18</asp:ListItem>
                                                    <asp:ListItem Value="19">19</asp:ListItem>
                                                    <asp:ListItem Value="20">20</asp:ListItem>
                                                    <asp:ListItem Value="21">21</asp:ListItem>
                                                    <asp:ListItem Value="22">22</asp:ListItem>
                                                    <asp:ListItem Value="23">23</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="cboBRKEndMin" runat="server" CssClass="clsComboBox" Width="15%">
                                                    <asp:ListItem Value="00">00</asp:ListItem>
                                                    <asp:ListItem Value="15">15</asp:ListItem>
                                                    <asp:ListItem Value="30">30</asp:ListItem>
                                                    <asp:ListItem Value="45">45</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtBRKEndDt" runat="server" CssClass="clsTextField"  ></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender3" TargetControlID="txtBRKEndDt" runat="server">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label" width="50%">
                                                Breakdown Remarks<font class="clsNote">*</font>
                                            </td>
                                            <td width="50%" align="left">
                                                <asp:TextBox ID="txtBRKRem" CssClass="clsTextField" Width="100%" runat="server"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:Panel ID="pnlBrkDwn" Width="100%" runat="server">
                                   
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <table id="tblSubmit2" border="1" cellspacing="1" cellpadding="1" width="100%">
                    <tr>
                        <td width="100%" colspan="4" align="center">
                            <asp:Button ID="cmdSubmit" runat="server" CssClass="clsButton" Text="Submit"></asp:Button></td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>

<script type="text/javascript" language="javascript">
function test()
{
 //alert('Hi');
 var tt=document.getElementById("divtest");
 if (tt.style.display=='none')
 {tt.style.display = 'block' ;
 }
 else
 {
tt.style.display = 'none';
 }
}
</script>

