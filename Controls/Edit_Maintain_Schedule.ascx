<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Edit_Maintain_Schedule.ascx.vb"
    Inherits="Controls_Edit_Maintain_Schedule" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Edit Maintenance Schedule 
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Edit Maintenance Schedule </strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <center>
                            <asp:CompareValidator ID="cvdates" runat="server" ErrorMessage="please select to date greater than from date!"
                                Display="none" ControlToCompare="txttodate" ControlToValidate="txtfromdate" Operator="lessthan"
                                Type="date"></asp:CompareValidator>
                            <asp:ValidationSummary ID="validationsummary1" runat="server" ShowMessageBox="true"
                                ShowSummary="false" DisplayMode="list"></asp:ValidationSummary>
                        </center>
                        <br />
                        <asp:Panel ID="pnlcontainer" runat="server" Width="100%">
                            <table id="tbloption" border="0" width="100%" align="center">
                                <tr>
                                    <td class="clstblnew" width="50%" colspan="6">
                                        <b>Change plan</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="17%">
                                        <asp:RadioButton ID="raddaily" runat="server" CssClass="clsradiobutton" AutoPostBack="true"
                                            Text="daily"></asp:RadioButton></td>
                                    <td width="17%">
                                        <asp:RadioButton ID="radweekly" runat="server" CssClass="clsradiobutton" AutoPostBack="true"
                                            Text="weekly"></asp:RadioButton></td>
                                    <td width="17%">
                                        <asp:RadioButton ID="radmonthly" runat="server" CssClass="clsradiobutton" AutoPostBack="true"
                                            Text="monthly"></asp:RadioButton></td>
                                    <td width="17%">
                                        <asp:RadioButton ID="radbimonthly" runat="server" CssClass="clsradiobutton" AutoPostBack="true"
                                            Text="bimonthly"></asp:RadioButton></td>
                                    <td width="17%">
                                        <asp:RadioButton ID="radquarterly" runat="server" CssClass="clsradiobutton" AutoPostBack="true"
                                            Text="quarterly"></asp:RadioButton></td>
                                    <td width="15%">
                                        <asp:RadioButton ID="radyearly" runat="server" CssClass="clsradiobutton" AutoPostBack="true"
                                            Text="yearly"></asp:RadioButton></td>
                                </tr>
                            </table>
                            <asp:Panel ID="panel1" runat="server" Width="100%">
                                <asp:Panel ID="panweekly" runat="server" Width="100%" Height="30px">
                                    <table border="1" width="100%" align="center">
                                        <tr>
                                            <td class="clslabel" width="50%">
                                                Day of the week</td>
                                            <td width="50%">
                                                <asp:DropDownList ID="w_drpdwnweek" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="2">Monday</asp:ListItem>
                                                    <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                                    <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                                    <asp:ListItem Value="5">Thursday</asp:ListItem>
                                                    <asp:ListItem Value="6">Friday</asp:ListItem>
                                                    <asp:ListItem Value="7">Saturday</asp:ListItem>
                                                    <asp:ListItem Value="1">Sunday</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="panmonthly" runat="server" Width="100%">
                                    <table style="height: 10px" border="1" width="100%" align="center">
                                        <tr>
                                            <td width="50%">
                                                Day of the month</td>
                                            <td width="50%">
                                                <asp:DropDownList ID="m_drpdwndate" runat="server" Width="100%" CssClass="clscombobox">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="panbimonthly" runat="server" Width="100%">
                                    <table style="height: 14px" border="1" width="100%" align="center">
                                        <tr>
                                            <td class="clslabel" width="50%">
                                                Month</td>
                                            <td width="50%">
                                                <asp:DropDownList ID="b_drpdwnmonth" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="1">January</asp:ListItem>
                                                    <asp:ListItem Value="2">Febuary</asp:ListItem>
                                                    <asp:ListItem Value="3">March</asp:ListItem>
                                                    <asp:ListItem Value="4">April</asp:ListItem>
                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                    <asp:ListItem Value="6">June</asp:ListItem>
                                                    <asp:ListItem Value="7">July</asp:ListItem>
                                                    <asp:ListItem Value="8">August</asp:ListItem>
                                                    <asp:ListItem Value="9">September</asp:ListItem>
                                                    <asp:ListItem Value="10">October</asp:ListItem>
                                                    <asp:ListItem Value="11">November</asp:ListItem>
                                                    <asp:ListItem Value="12">December</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="clslabel" width="50%">
                                                day</td>
                                            <td width="50%">
                                                <asp:DropDownList ID="b_drpdwndate" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="panyearly" runat="server" Width="100%">
                                    <table border="1" width="100%" align="center">
                                        <tr>
                                            <td class="clslabel" width="25%">
                                                Month</td>
                                            <td width="25%">
                                                <asp:DropDownList ID="y_drpdwnmonth" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="1">January</asp:ListItem>
                                                    <asp:ListItem Value="2">Febuary</asp:ListItem>
                                                    <asp:ListItem Value="3">March</asp:ListItem>
                                                    <asp:ListItem Value="4">April</asp:ListItem>
                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                    <asp:ListItem Value="6">June</asp:ListItem>
                                                    <asp:ListItem Value="7">July</asp:ListItem>
                                                    <asp:ListItem Value="8">August</asp:ListItem>
                                                    <asp:ListItem Value="9">September</asp:ListItem>
                                                    <asp:ListItem Value="10">October</asp:ListItem>
                                                    <asp:ListItem Value="11">November</asp:ListItem>
                                                    <asp:ListItem Value="12">December</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td class="clslabel" width="25%">
                                                day</td>
                                            <td width="25%">
                                                <asp:DropDownList ID="y_drpdwndate" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="panquarterly" runat="server" Width="100%">
                                    <table border="1" width="100%" align="center">
                                        <tr>
                                            <td class="clstblhead" width="25%">
                                                First Quarter</td>
                                            <td class="clstblhead" width="25%">
                                                Second Quarter</td>
                                            <td class="clstblhead">
                                                Third Quarter</td>
                                            <td class="clstblhead">
                                                Fourth Quarter</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 40px" width="25%">
                                                <asp:DropDownList ID="q1_drpdwnmonth" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="4">April</asp:ListItem>
                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                    <asp:ListItem Value="6">June</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td style="height: 40px" width="25%">
                                                <asp:DropDownList ID="q2_drpdwnmonth" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="7">July</asp:ListItem>
                                                    <asp:ListItem Value="8">August</asp:ListItem>
                                                    <asp:ListItem Value="9">September</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td style="height: 40px">
                                                <asp:DropDownList ID="q3_drpdwnmonth" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="10">October</asp:ListItem>
                                                    <asp:ListItem Value="11">November</asp:ListItem>
                                                    <asp:ListItem Value="12">December</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td style="height: 40px">
                                                <asp:DropDownList ID="q4_drpdwnmonth" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Value="1">January</asp:ListItem>
                                                    <asp:ListItem Value="2">Febuary</asp:ListItem>
                                                    <asp:ListItem Value="3">March</asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td width="25%">
                                                <asp:DropDownList ID="q1_drpdwndate" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                            <td width="25%">
                                                <asp:DropDownList ID="q2_drpdwndate" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                            <td>
                                                <asp:DropDownList ID="q3_drpdwndate" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                            <td>
                                                <asp:DropDownList ID="q4_drpdwndate" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pandaily" runat="server" Width="100%">
                                    <table border="1" width="100%" align="center">
                                        <tr>
                                            <td class="clslabel" width="50%">
                                                Time</td>
                                            <td width="50%">
                                                <asp:DropDownList ID="d_drpdwnhour" runat="server" Width="100%" CssClass="clscombobox"
                                                    AutoPostBack="true">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <table style="height: 10px" border="1" width="100%" align="center">
                                    <tr>
                                        <td class="clslabel" width="25%">
                                            From Date</td>
                                        <td width="25%">
                                            <ew:CalendarPopup ID="txtfromdate" runat="server">
                                            </ew:CalendarPopup>
                                        </td>
                                        <td class="clslabel" width="25%">
                                            To Date</td>
                                        <td class="label">
                                            <ew:CalendarPopup ID="txttodate" runat="server">
                                            </ew:CalendarPopup>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="submit"></asp:Button>
                                            <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Back" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnsubmit" />
    </Triggers>
</asp:UpdatePanel>
