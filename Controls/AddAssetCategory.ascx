<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAssetCategory.ascx.vb" Inherits="Controls_AddAssetCategory" %>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>



<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6">
        <label class="col-md-2 btn btn-default pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Asset Category and Select Modify to modify the existing Asset Category" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="btn btn-default" style="margin-left: 25px">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Asset Category and Select Modify to modify the existing Asset Category" />
            Modify
        </label>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblAssetBrand" runat="server" CssClass="col-md-5 control-label" Visible="False">Asset Category<span style="color: red;">*</span></asp:Label>
                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlBrand"
                    ErrorMessage="Please Select The Asset Category" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Category"
                        AutoPostBack="True" Visible="False">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Category Code<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtBrand"
                    Display="none" ErrorMessage="Please Enter Asset Category Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtBrand"
                    ErrorMessage="Please Enter Asset Category Code" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Category  Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBrandName"
                    Display="none" ErrorMessage="Please Enter Asset Category Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtBrandName"
                    ErrorMessage="Please Enter Asset Category Name" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>--%>
                <div class="col-md-7">
                    <%-- <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">--%>
                    <div>
                        <asp:TextBox ID="txtBrandName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Type<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlasttype"
                    Display="none" ErrorMessage="Please Select Asset Type" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlasttype" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Asset Type">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem Value="0">Capital Asset</asp:ListItem>
                        <asp:ListItem Value="1">Consumable Asset</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Status</label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                    Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Status">
                        <%-- <asp:ListItem>--Select--</asp:ListItem>--%>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks</label>
                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtBrand"
                    Display="none" ErrorMessage="Please Enter Asset Category Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtBrand"
                    ErrorMessage="Please Enter Asset Category Code" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>--%>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-6" id="rbnInsUpload" runat="server">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Checklist Document</label>

                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" ControlToValidate="fpBrowseIncDoc"
                    ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                    ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([pP][nN][gG])|([tT][iI][fF]))$"> 
                            
                </asp:RegularExpressionValidator>

                <div class="col-md-7">
                    <div class="btn btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseIncDoc" runat="Server" onchange="show(this)" Width="90%" />

                    </div>
                    <div id="image-holder"></div>

                    <asp:LinkButton ID="hplINC" runat="server" Visible="false"></asp:LinkButton>
                    <asp:Label ID="lblINC" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:Literal ID="litINC" Text="" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx" CausesValidation="False" />

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvCat" runat="server" AllowPaging="True" AllowSorting="False"
            RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Asset Category Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <%-- <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Category Code">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("VT_CODE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Category Name">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Type">
                    <ItemTemplate>
                        <asp:Label ID="lblAssetType" runat="server" CssClass="lblAssetType" Text='<%#Bind("ASSET_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("VT_STATUS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
