<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddStock.ascx.vb" Inherits="Controls_AddStock" %>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Document  (Only Excel ) <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                    ControlToValidate="fpBrowseDoc" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div class="btn btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5  control-label">
                    <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" />
                </div>
                <div class="col-md-7">
                    <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/Add_Consumable_Stock.xls"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <label class="col-md-2 btn btn-default pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Stock and Select Modify to modify the existing Stock" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="btn btn-default" style="margin-left: 25px">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Stock and Select Modify to modify the existing Stock" />
            Modify
        </label>
    </div>
</div>
<div class="row">&nbsp</div>


<div id="trselVendor" runat="server" visible="false">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblAsset" class="col-md-5 control-label" runat="server">Select Asset</asp:Label>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlAsset"
                        ErrorMessage="Please Select the Asset" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>

                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAsset" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Asset"
                            AutoPostBack="True">
                        </asp:DropDownList>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
       <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAssetCategory"
                    Display="none" ErrorMessage="Please Select Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                         AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
        <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Sub Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlAstSubCat"
                    Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>

                </div>
            </div>
        </div>
    </div>

 
</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                         AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
      <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Asset Model<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                <div class="col-md-7">
                    <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                      
                        >
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
      <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Vendor<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlVendor"
                    Display="none" ErrorMessage="Please Select Vendor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Vendor" >
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Location<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlLocation"
                    Display="none" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Location" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Tower<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTower"
                    Display="none" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                <div class="col-md-7">
                    <asp:DropDownList ID="ddlTower" runat="server" CssClass="selectpicker" data-live-search="true"
                       
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Floor<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlFloor"
                    Display="none" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true">
                        
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Quantity Unit</label>
                <div class="col-md-7">
                    <asp:TextBox ID="lblunit" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>

                </div>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Min Order Quantity</label>
                <div class="col-md-7">
                    <%--   <asp:Label ID="lblMinordqty" runat="server"></asp:Label>--%>
                    <asp:Label ID="lblMinordqty" runat="server" CssClass="form-control"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Min Stock Quantity</label>
                <div class="col-md-7">
                    <asp:Label ID="lblstockqty" runat="server" CssClass="form-control"></asp:Label>
                    <%--<asp:Label ID="lblstockqty" runat="server"></asp:Label>--%>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Quantity<span style="color: red;">*</span></label>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAstQty" Display="None" ErrorMessage="Enter Quantity in Numbers" SetFocusOnError="True" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAstQty" Display="None" ErrorMessage="Please Enter Asset Quantity" SetFocusOnError="True" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAstQty" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Manufacturing Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtmnfdate" Display="None" ErrorMessage="Please Enter Manufacturing Date" SetFocusOnError="True" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtmnfdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Expiry Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtExpdate" Display="None" ErrorMessage="Please Enter Expiry Date" SetFocusOnError="True" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='Div1'>
                        <asp:TextBox ID="txtExpdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CausesValidation="true" CssClass="btn btn-primary custom-button-color" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" ValidationGroup="Val1" CausesValidation="False" />
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx" />

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvitems" runat="server" AllowSorting="true" AllowPaging="true" 
            AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>               
                <asp:TemplateField HeaderText="Vendor">
                    <ItemTemplate>
                        <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("VENDOR")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>       
                 <asp:TemplateField HeaderText="Asset Category">
                    <ItemTemplate>
                        <asp:Label ID="lblempid" runat="server" Text='<%#Eval("ASSET_CATEGORY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Sub Category">
                    <ItemTemplate>
                        <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("ASSET_SUB_CATEGORY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>   
                 <asp:TemplateField HeaderText="Asset Brand">
                    <ItemTemplate>
                        <asp:Label ID="lblempid" runat="server" Text='<%#Eval("ASSET_BRAND")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Model">
                    <ItemTemplate>
                        <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("ASSET_MODEL")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>   
                 <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                        <asp:Label ID="lblempid" runat="server" Text='<%#Eval("LOC")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Qty">
                    <ItemTemplate>
                        <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("ASSET_QTY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField> 
                 <asp:TemplateField HeaderText="Asset Mrf">
                    <ItemTemplate>
                        <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("ASSET_MRF")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>    
                <asp:TemplateField HeaderText="Asset Exp">
                    <ItemTemplate>
                        <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("ASSET_EXP")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField> 
                 <asp:TemplateField HeaderText="Asset Remark">
                    <ItemTemplate>
                        <asp:Label ID="lblcostcenter" runat="server" Text='<%#Eval("ASSET_REMARK")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>         
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
