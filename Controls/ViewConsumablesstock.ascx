<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewConsumablesstock.ascx.vb"
    Inherits="Controls_ViewConsumablesstock" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Category</label>
                <asp:RequiredFieldValidator ID="reqastcat" runat="server" ErrorMessage="Please Enter Asset Category" InitialValue="--Select--"
                    ControlToValidate="ddlastCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Sub Category  </label>
                <asp:RequiredFieldValidator ID="rersubastcat" runat="server" ErrorMessage="Please Enter Asset sub Category" InitialValue="--Select--"
                    ControlToValidate="ddlastsubCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastsubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
      <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand/Make </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

     <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Model </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

   
</div>
<div class="row">
     <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Location </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                    ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CausesValidation="true" CssClass="btn btn-primary custom-button-color" Text="Search" ValidationGroup="Val1" />
            <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" ValidationGroup="Val1" CausesValidation="False" />
            <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary custom-button-color"  Text="Export To Excel" CausesValidation="False" Visible="false" />
        </div>
    </div>
</div>
<div class="row" id="divGrid" runat="server" style="margin-top: 10px">
  <div class="col-md-12">       
            <asp:GridView ID="gvCusomableAstStock" runat="server" AutoGenerateColumns="False"
                AllowSorting="True" AllowPaging="True" EmptyDataText="No Assets Found" CssClass="table table-condensed table-bordered table-hover table-striped">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>

                     <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLCM_CODE" runat="server" Text='<%#Eval("Location Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                      <asp:TemplateField HeaderText="Category Name">
                        <ItemTemplate>
                            <asp:Label ID="lblBarnd" runat="server" Text='<%#Eval("Category")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                      <asp:TemplateField HeaderText="Sub Category Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("Subcategory Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Brand Name">
                        <ItemTemplate>
                            <asp:Label ID="lblBarnd" runat="server" Text='<%#Eval("Brand")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                   
                    <asp:TemplateField HeaderText="Asset Model Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("Asset Model Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                  
                    <asp:TemplateField HeaderText="Asset Code" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblStock_id" Visible="false" runat="server" Text='<%#Eval("Asset Model Id")%>'></asp:Label>
                            <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("Asset Model Code")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Available Stock">
                        <ItemTemplate>
                            <asp:Label ID="lblQty" runat="server" Text='<%#Eval("Available Stock")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>

