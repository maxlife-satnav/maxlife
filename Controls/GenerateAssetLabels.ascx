<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GenerateAssetLabels.ascx.vb"
    Inherits="Controls_GenerateAssetLabels" %>
<script type="text/javascript">
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID)
        for (i = 0; i < form1.elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'checkbox') {
                if (re.test(elm.name)) {
                    if (elm.disabled == false)
                        elm.checked = checkVal
                }
            }
        }
    }
    function ChildClick(CheckBox) {
        //Get target base & child control.
        var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
        var TargetChildControl = "chkSelect";
        //Get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        // check to see if all other checkboxes are checked
        for (var n = 0; n < Inputs.length; ++n)
            if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                // Whoops, there is an unchecked checkbox, make sure
                // that the header checkbox is unchecked
                if (!Inputs[n].checked) {
                    Inputs[0].checked = false;
                    return;
                }
            }
        // If we reach here, ALL GridView checkboxes are checked
        Inputs[0].checked = true;
    }
</script>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div id="mainPanel" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Vendor<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVendor"
                        Display="none" ErrorMessage="Please Select Vendor !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select PO<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlPO"
                        Display="none" ErrorMessage="Please Select PO !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlPO" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>


   
        <%--<div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row text-center">
                        <strong>ASSETS LIST</strong>
                    </div>
                </div>
            </div>
        </div>--%>
        <div class="row" style="margin-top: 10px" id="pnlItems" runat="server">
            <div class="col-md-12 pull-right">
               
                 <div class="row" style= width: 100%; min-height: 20px; max-height: 500px"> 
                <fieldset>
                    <legend>Assets List</legend>
                     </fieldset>
                    <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                        EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                        <Columns>
                            <asp:BoundField DataField="AST_MD_CODE" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" />
                            <asp:BoundField DataField="AST_MD_NAME" HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" />
                            <asp:TemplateField HeaderText="Category" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("VT_TYPE") %>'></asp:Label>
                                    <asp:Label ID="lblCatCode" runat="server" Text='<%#Eval("VT_CODE") %>' Visible="FALSE"></asp:Label>
                                    <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AST_MD_NAME")%>' Visible="FALSE"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sub-Category" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubCategory" runat="server" Text='<%#Eval("AST_SUBCAT_NAME") %>'></asp:Label>
                                    <asp:Label ID="lblSubCategoryCode" runat="server" Text='<%#Eval("AST_SUBCAT_CODE") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Brand" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" runat="server" Text='<%#Eval("manufacturer") %>'></asp:Label>
                                    <asp:Label ID="lblBrandCode" runat="server" Text='<%#Eval("manufactuer_code") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Request Id" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblReq" runat="server" Text='<%#Eval("AIPD_ITMREQ_ID") %>' Visible="TRUE"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. Of Assets" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" MaxLength="5" ReadOnly="true" Text='<%#Eval("AIPD_RCD_QTY") %>'></asp:TextBox>
                                    <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_ID")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblModel" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlLocation"
                                        Display="none" ErrorMessage="Please Select Location !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                         <%--   <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="center">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                        ToolTip="Click to check all" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                </ItemTemplate>
                                <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
               </div>
            </div>
        </div>

   

    <div class="row" id="submitbtn" runat="server">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 10px">
    <div class="col-md-12 pull-right">
        <asp:GridView ID="GridView1" runat="server" EmptyDataText="Sorry! No Available Records..."
            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10 " AutoGenerateColumns="true">
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnExport1" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export to Excel" Visible="false" />
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/FAM/Masters/Mas_WebFiles/frmGenerateAssetLabels.aspx" Visible="false" />
        </div>
    </div>
</div>
