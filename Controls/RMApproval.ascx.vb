Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_RMApproval
    Inherits System.Web.UI.UserControl
    Dim UID As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                UID = Session("uid")
                BindGrid(UID)
            End If
        End If
    End Sub
    Private Sub BindGrid(ByVal aur_id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GETREQBYSTATUSID1")
        sp.Command.AddParameter("@Cuser", Session("uid"), Data.DbType.String)
        'Dim ds As New dataset()
        'ds = sp.GetDataSet()
        'If ds.Tables(0).Rows.Count > 0 Then
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
        'End If
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid(UID)
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim ReqIDsForMsg = ""
        For Each row As GridViewRow In gvItems.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
                Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text
                If bf.Checked Then
                    UpdateData(id, txtRM.Text)
                    ReqIDsForMsg = ReqIDsForMsg + ", " + id
                End If
            End If
        Next
        BindGrid(Session("UID"))
        txtRM.Text = ""
        lblMsg.Text = "Requisition(s) Approved Successfully ( " + ReqIDsForMsg.Remove(0, 1) + " ) "

    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Dim ReqIDsForMsg = ""
        For Each row As GridViewRow In gvItems.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
                Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text
                If bf.Checked Then
                    CancelData(id, Trim(txtRM.Text))
                    ReqIDsForMsg = ReqIDsForMsg + ", " + id
                End If
            End If
        Next
        BindGrid(Session("UID"))
        txtRM.Text = ""
        lblMsg.Text = "Requisition(s) Rejected Successfully ( " + ReqIDsForMsg.Remove(0, 1) + " ) "

    End Sub



    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisition_UpdateByRM")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", 1004, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Private Sub CancelData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisition_UpdateByRM")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", 1005, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

End Class
