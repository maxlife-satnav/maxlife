﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ApprovalJourneyPlan
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            gvItems.Visible = False

            btnAdd.Visible = False
            btnModify.Visible = False

        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_REQDETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvitems1.DataSource = sp.GetDataSet()
            gvitems1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems1.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvitems1.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As String = lblID.Text
            txtStore.Text = id
            pnlview.Visible = False
            pnladdcomments.Visible = False
            BindDetails(txtStore.Text)

            If gvItems.Rows.Count > 0 Then
                btnAdd.Visible = True
                btnModify.Visible = True



            Else
                btnAdd.Visible = False
                btnModify.Visible = False

            End If

            gvItems.Visible = True
        End If
    End Sub
    Private Sub BindDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_DETAILS_BYREQ")
            sp.Command.AddParameter("@REQUESTID", id, DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindDetails(txtStore.Text)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_FIRSTLEVELAPPROVAL")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@aur_id", Session("UID"), DbType.String)
            sp.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=63")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_FIRSTLEVELREJECT")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@aur_id", Session("UID"), DbType.String)
            sp.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=64")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindComments()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_COMMENTS_SCHEDULEJOURNEY")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@DETAILSID", Convert.ToInt32(lblsno.Text), DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                gvviewcomments.DataSource = ds.Tables(0)
                gvviewcomments.DataBind()
            Else
                gvviewcomments.EmptyDataText = "Sorry! No Available Records..."
                gvviewcomments.DataBind()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        Response.Redirect("~/ESP/ESP_Webfiles/frmAptApprovalJourneyPlan.aspx")
    End Sub

    Protected Sub btnaddcommnet_Click(sender As Object, e As EventArgs) Handles btnaddcommnet.Click
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_ADD_SCHEDULECOMMENT")
            sp.Command.AddParameter("@REQUESTID", txtStore.Text, DbType.String)
            sp.Command.AddParameter("@COMMENTED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@DETAILS_ID", Convert.ToInt32(lblsno.Text), DbType.Int32)
            If (txtcomment.Text <> "") Then
                sp.Command.AddParameter("@COMMENT", txtcomment.Text, DbType.String)
            Else
                sp.Command.AddParameter("@COMMENT", "NA", DbType.String)
            End If
            sp.ExecuteScalar()
            lblMsg.Text = "Comments Added Successfully."
            txtcomment.Text = ""
            ' Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=46")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID1"), Label)
            Dim id As String = lblID.Text
            lblsno.Text = lblID.Text
            pnlview.Visible = True
            pnladdcomments.Visible = False
            BindComments()
            lblMsg.Text = ""
        ElseIf e.CommandName = "Add" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID1"), Label)
            pnlview.Visible = False
            pnladdcomments.Visible = True
            lblsno.Text = lblID.Text
        End If
    End Sub

    Protected Sub gvviewcomments_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvviewcomments.PageIndexChanging
        gvviewcomments.PageIndex = e.NewPageIndex
        BindComments()
    End Sub
End Class
