<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SendRentRemainder.ascx.vb"
    Inherits="Controls_SendRentRemainder" %>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvTenantRent" runat="server" EmptyDataText="No Rent Reminder Found."
            AutoGenerateColumns="false" AllowPaging="true"
            CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField HeaderText="Tenant Code">
                    <ItemTemplate>
                        <asp:Label ID="lblcode" runat="server" Text='<%# Eval("TEN_CODE") %>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tenant Name">
                    <ItemTemplate>
                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("TEN_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Next-Payable Date">
                    <ItemTemplate>
                        <asp:Label ID="lblnpdate" runat="server" Text='<%# Eval("TEN_NEXTPAYABLEDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment-Terms">
                    <ItemTemplate>
                        <asp:Label ID="lblpterm" runat="server" Text='<%# Eval("TEN_PAYMENTTERMS") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Location Name">
                    <ItemTemplate>
                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("PN_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reminder Sent Date">
                    <ItemTemplate>
                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("SENTDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" HorizontalAlign="Center" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>


