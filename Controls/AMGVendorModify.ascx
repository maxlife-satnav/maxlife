<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGVendorModify.ascx.vb"
    Inherits="Controls_AMGVendorModify" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                    Display="None" ErrorMessage="Please Enter Name !" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>



    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Address<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                    Display="None" ErrorMessage="Please Enter Address !" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" MaxLength="500"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
      <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Vendor Category<span style="color: red;">*</span></label>
        <%--         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" InitialValue="0" ControlToValidate="chkVendorCat"
                    Display="None" ErrorMessage="Please Select Vendor Category !" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                <div class="col-md-7">
                    <asp:CheckBoxList ID="chkVendorCat" runat="server">
                                            <asp:ListItem Text="Supplier" Value="Supplier"></asp:ListItem>
                                            <asp:ListItem Text="Service" Value="Service"></asp:ListItem>
                                            </asp:CheckBoxList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvcity" runat="server" InitialValue="0" ControlToValidate="ddlCity"
                    Display="None" ErrorMessage="Please Select City !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select City">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Phone Number<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvPhoneNo" runat="server" ControlToValidate="txtPhone"
                    Display="None" ErrorMessage="Please Enter Phone Number !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Mobile<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfMobile" runat="server" ControlToValidate="txtMobile"
                    Display="None" ErrorMessage="Please Enter Mobile Number !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="none" runat="server" ControlToValidate="txtMobile"
                    ValidationGroup="Val1" ErrorMessage="Please Enter Valid Mobile Number" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Email<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfEmail" runat="server" ControlToValidate="txtEmail"
                    Display="None" ErrorMessage="Please Enter Email !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="none"
                    ValidationGroup="Val1" ErrorMessage="Please Enter Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:RadioButtonList ID="rdbtnStatus" runat="server" CssClass="clsRadioButton"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                        <asp:ListItem Value="0">Inactive</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Bank Name<span style="color: red;">*</span></label>



                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtBankName"
                    Display="None" ErrorMessage="Please Enter  Bank Name !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Bank A/C No
                </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBankAcNo"
                    Display="None" ErrorMessage="Please Enter Bank A/C No !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtBankAcNo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Branch Name</label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBranchName"
                    Display="None" ErrorMessage="Please Enter Branch Name !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtBranchName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">IFSC Code</label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtIFSCCode"
                    Display="None" ErrorMessage="Please Enter IFSC Code !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtIFSCCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfRemarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks !" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" MaxLength="250" Rows="3"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">PAN No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPANno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">GIR No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtGIRno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">TAN No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtTANno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">LST No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtlstno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">WCT No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtwctno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">CST No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtcstno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">ST. Assessment Circle</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtstcircle" runat="server" CssClass="form-control"
                        MaxLength="75"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color"
                ValidationGroup="Val1" Text="Submit" />

            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
        </div>
    </div>
</div>
