<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ItemRequisitionDetails.ascx.vb"
    Inherits="Controls_ItemRequisitionDetails" %>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Item Pending Requisition Details
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="TABLE1"
        onclick="return TABLE1_onclick()">
        <tr>
            <td align="left" colspan="3">
                <asp:Label align="left" ID="Label9" runat="server" CssClass="note" Text="(*) Mandatory fields"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Item Pending Requisition Details</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                &nbsp;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblMsg" runat="server" CssClass="error"></asp:Label><br />
                <br />
                <table id="table4" cellspacing="2" cellpadding="2" width="100%" border="1">
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label1" runat="server" CssClass="clslabel" Text="Requisition ID"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left" width="50%">
                            <asp:Label ID="lblReqId" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblTemp" runat="server" CssClass="clslabel" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label4" runat="server" CssClass="clslabel" Text="Location"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblLoc" runat="server" CssClass="clslabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblItemCode" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblLocName" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblTwrName" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblFlrName" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="Label5" runat="server" CssClass="clslabel" Text="Tower"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblTwr" runat="server" CssClass="clslabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label6" runat="server" CssClass="clslabel" Text="Floor"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblFlr" runat="server" CssClass="clslabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <fieldset>
                                <legend>Item List</legend>
                                <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    EmptyDataText="No Asset(s) Found." Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="AIM_CODE" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" />
                                        <asp:BoundField DataField="AIM_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left" />
                                        <asp:TemplateField HeaderText="Available Qty" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAvailQty" runat="server" Text='<%#Eval("AID_AVLBL_QTY") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pending Qty" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQty" runat="server" Text='<%#Eval("AID_QTY") %>'></asp:Label>
                                                <asp:Label ID="lblAIM_CODE" runat="server" Text='<%#Eval("AIM_CODE") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" />&nbsp;
                        </td>
                    </tr>
                </table>
                <br />
                <br />
            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
