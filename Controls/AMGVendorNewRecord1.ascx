<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGVendorNewRecord1.ascx.vb"
    Inherits="Controls_AMGVendorNewRecord1" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Vendor Code<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfCode" runat="server" ControlToValidate="txtCode" Display="nONE" ErrorMessage="Please Enter Vendor Code" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>

                <div class="col-md-7">
                    <asp:TextBox ID="txtCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Vendor Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                    Display="None" ErrorMessage="Please Enter Vendor Name" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Vendor Type<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:ListBox ID="lstTypes" CssClass="form-control" SelectionMode="Multiple" runat="server"></asp:ListBox>
                </div>
            </div>
        </div>
    </div>

     <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Vendor Category<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:CheckBoxList ID="chkVendorCat" runat="server">
                                            <asp:ListItem Text="Supplier" Value="Supplier"></asp:ListItem>
                                            <asp:ListItem Text="Service" Value="Service"></asp:ListItem>
                                            </asp:CheckBoxList>
                </div>
            </div>
        </div>
    </div>
   
</div>
<div class="row">
     <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Address<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                    Display="None" ErrorMessage="Please Enter Address" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAddress" runat="server"
                        TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvcity" runat="server" InitialValue="--Select--"
                    ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City"
                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select City">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Phone Number<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvPhoneNo" runat="server" ControlToValidate="txtPhone"
                    Display="None" ErrorMessage="Please Enter A Valid Phone Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPhone" MaxLength="20" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Mobile Number<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfMobile" runat="server" ControlToValidate="txtMobile"
                    Display="None" ErrorMessage="Please Enter A Valid 10-Digit Mobile Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMobile" ValidationGroup="Val1"
                    Display="None" ErrorMessage="Please Enter A Valid 10-Digit Mobile Number" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtMobile" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Email Address<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfEmail" runat="server" ControlToValidate="txtEmail"
                    Display="None" ErrorMessage="Please Enter Email Address" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="Val1"
                    Display="None" ErrorMessage="Please Enter Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
<div class="col-md-6">
    <div class="form-group">
        <div class="row">
            <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
            <div class="col-md-7">
                <asp:RadioButtonList ID="rdbtnStatus" runat="server" CssClass="col-md-9 control-label"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                    <asp:ListItem Value="0">InActive</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <div class="row">
            <label class="col-md-5 control-label">Bank Name<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtBankName"
                Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <div class="col-md-7">
                <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
</div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Bank A/C No<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBankAcNo"
                    Display="None" ErrorMessage="Please Enter  Bank A/C No" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtBankAcNo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Branch Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBranchName"
                    Display="None" ErrorMessage="Please Enter  Branch Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtBranchName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">IFSC Code<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtIFSCCode"
                    Display="None" ErrorMessage="Please Enter  IFSC Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtIFSCCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks</label>
              <%--  <asp:RequiredFieldValidator ID="rfRemarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>--%>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">PAN No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPANno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">GIR No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtGIRno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">TAN No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtTANno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">LST No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtlstno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">WCT No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtwctno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">CST No.</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtcstno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">ST. Assessment Circle</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtstcircle" runat="server" CssClass="form-control"
                        MaxLength="75"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" TabIndex="18" ValidationGroup="Val1" Text="Submit" />
            <%--<asp:Button ID="btnview" runat="server" CssClass="btn btn-primary custom-button-color" Text="View" />--%>
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
        </div>
    </div>

</div>

<div class="row table table table-condensed table-responsive">
    <div class="form-group">
        <div class="col-md-12">
            <asp:GridView ID="gvDetails_AVR" runat="server" AutoGenerateColumns="False" AllowSorting="True" 
                AllowPaging="True" EmptyDataText="No Asset Vendor Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <PagerSettings Mode="NumericFirstLast"/>
                <Columns>
                   <%-- <asp:TemplateField HeaderText="Vendor Details">
                        <ItemTemplate>--%>
                            <%-- <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">ID</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="lblID" runat="server" CssClass="lblAssetID" Text='<%#Eval("AVR_ID")%>'></asp:Label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Code</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="lblCode_AVR" runat="server" CssClass="lblVendorCODE" Text='<%#Eval("AVR_CODE")%>'></asp:Label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Name</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblCodeName_AVR" runat="server" CssClass="lblVendorCODENAME" Text='<%#Eval("AVR_CO_NAME")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Phone No</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblPHNO_AVR" runat="server" CssClass="lblVendorPHNO" Text='<%#Eval("AVR_PHNO")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Mobile</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblMobile_AVR" runat="server" CssClass="lblVendorMobile" Text='<%#Eval("AVR_MOBILE_PHNO")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Email</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblEmail_AVR" runat="server" CssClass="lblVendorEmail" Text='<%#Eval("AVR_EMAIL")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">A/C No.</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="Label1" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_ACCNO")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Bank Name</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="Label2" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BANK_NAME")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Branch Name</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="Label3" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BRN_NAME")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">IFSC Code</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label4" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_IFSC_CODE")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Date</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblDate_AVR" runat="server" CssClass="lblVendorUpdateDate" Text='<%#Eval("AVR_UPT_DT")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <%--    <table width="100%" border="1" cellpadding="1" cellspacing="1">
                                <tr>
                                    <td align="left">ID
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblID" runat="server" CssClass="lblAssetID" Text='<%#Eval("AVR_ID")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Code</td>
                                    <td align="left">
                                        <asp:Label ID="lblCode_AVR" runat="server" CssClass="lblVendorCODE" Text='<%#Eval("AVR_CODE")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="lefT">Name</td>
                                    <td align="left">
                                        <asp:Label ID="lblCodeName_AVR" runat="server" CssClass="lblVendorCODENAME" Text='<%#Eval("AVR_CO_NAME")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="LEFT">Phone No</td>
                                    <td align="left">
                                        <asp:Label ID="lblPHNO_AVR" runat="server" CssClass="lblVendorPHNO" Text='<%#Eval("AVR_PHNO")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Mobile</td>
                                    <td align="left">
                                        <asp:Label ID="lblMobile_AVR" runat="server" CssClass="lblVendorMobile" Text='<%#Eval("AVR_MOBILE_PHNO")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Email</td>
                                    <td align="left">
                                        <asp:Label ID="lblEmail_AVR" runat="server" CssClass="lblVendorEmail" Text='<%#Eval("AVR_EMAIL")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">A/C No.</td>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_ACCNO")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Bank Name</td>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BANK_NAME")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Branch Name</td>
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BRN_NAME")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">IFSC Code</td>
                                    <td align="left">
                                        <asp:Label ID="Label4" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_IFSC_CODE")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Date</td>
                                    <td align="left">
                                        <asp:Label ID="lblDate_AVR" runat="server" CssClass="lblVendorUpdateDate" Text='<%#Eval("AVR_UPT_DT")%>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Vendor Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName_AVR" runat="server" CssClass="lblVendorName" Text='<%#Eval("AVR_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Vendor Grade">
                        <ItemTemplate>
                            <asp:Label ID="lblGrade_AVR" runat="server" CssClass="lblVendorGrade" Text='<%#Eval("AVR_GRADE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Vendor Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress_AVR" runat="server" CssClass="lblVendorName" Text='<%#Eval("AVR_ADDR")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor City">
                        <ItemTemplate>
                            <asp:Label ID="lblCity_AVR" runat="server" CssClass="lblVendorCity" Text='<%#Eval("AVR_CITY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Vendor Country">
                        <ItemTemplate>
                            <asp:Label ID="lblCountry_AVR" runat="server" CssClass="lblVendorCountry" Text='<%#Eval("AVR_COUNTRY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus_AVR" runat="server" CssClass="lblVendorStatus" Text='<%#Eval("AVR_STATUS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Updated By">
                        <ItemTemplate>
                            <asp:Label ID="lblUpdatedBy_AVR" runat="server" CssClass="lblVendorUpdatedBy" Text='<%#Eval("AVR_UPT_BY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks_AVR" runat="server" CssClass="lblRemarks" Text='<%#Eval("AVR_REMARKS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <a href='frmAMGVendorModify.aspx?code=<%#Eval("AVR_CODE")%>'>EDIT</a>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
