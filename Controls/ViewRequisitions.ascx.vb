Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_ViewRequisitions
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                FillInterMVMTRequests()
            End If
        End If
    End Sub

    Public Sub FillInterMVMTRequests()

        Try
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(0).Value = Session("UID")
            ObjSubsonic.BindGridView(gvInterMVMT, "GET_REQID_InterMOVEMENTS_NP", param)
        Catch ex As Exception
            gvInterMVMT.DataSource = Nothing
            gvInterMVMT.DataBind()
        End Try

    End Sub

    Protected Sub gvInterMVMT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvInterMVMT.PageIndexChanging
        gvInterMVMT.PageIndex = e.NewPageIndex
        FillInterMVMTRequests()
    End Sub

    Protected Sub gvInterMVMT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvInterMVMT.RowCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("EditInterMovementReq.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub
End Class
