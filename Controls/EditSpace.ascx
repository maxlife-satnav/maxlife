<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditSpace.ascx.vb" Inherits="Controls_EditSpace" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Edit Space
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%"  cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Edit Space</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
             <asp:ValidationSummary ID="ValidationSummary1" runat="server"  CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" /><br />
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label2" runat="server" CssClass="clsMessage"></asp:Label><br />
            
              <table id="table4" cellspacing="0" cellpadding="1" width="100%" border="1">
            
            
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblBDG" runat="server" CssClass="bodytext">Select Building <font class="clsNote">*</font></asp:Label>
                
                            
                            <asp:RequiredFieldValidator ID="rfvBDG" runat="server" ControlToValidate="ddlBDG"
                                Display="None" ErrorMessage="Please Select Building" ValidationGroup="Val1"
                                InitialValue="--Select Building--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlBDG" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                 <asp:ListItem>--Select Building--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblTwr" runat="server" CssClass="bodytext" >Select Tower <font class="clsNote">*</font></asp:Label>
                
                            
                            <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ControlToValidate="ddlTWR"
                                Display="None" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="--Select Tower--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlTWR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                <asp:ListItem>--Select Tower--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblFLR" runat="server" CssClass="bodytext" >Select Floor <font class="clsNote">*</font></asp:Label>
                
                            
                            <asp:RequiredFieldValidator ID="rfvFLR" runat="server" ControlToValidate="ddlFLR"
                                Display="None" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select Floor--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlFLR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                <asp:ListItem>--Select Floor--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%" style="height: 26px">
                            <asp:Label ID="lblWNG" runat="server" CssClass="bodytext">Select Wing <font class="clsNote">*</font>
                
                            </asp:Label>
                            <asp:RequiredFieldValidator ID="frvWNG" runat="server" ControlToValidate="ddlWNG"
                                Display="None" ErrorMessage="Please Select Wing" ValidationGroup="Val1" InitialValue="--Select Wing--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%; height: 26px;">
                            <asp:DropDownList ID="ddlWNG" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                <asp:ListItem>--Select Wing--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                
                
                </table>
                
               <asp:Label ID="lblTotal" runat="server" Visible="false" cssClass="bodytext">Total Seats <font class="clsNote">*</font></asp:Label>
                 <asp:Label ID="lblTotalM" runat="server" Visible="false" cssClass="bodytext"></asp:Label>
      
      
      
      
                     <br />
                     
                 <asp:GridView ID="gvSpcLyrTot" TabIndex="5" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" EmptyDataText="No Records Found">
                    <Columns>
                        <asp:TemplateField HeaderText="Space Name">
                            <ItemTemplate>
                                <asp:Label ID="lblLayer" runat="Server" Text='<%#Eval("LAYER_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Count">
                            <ItemTemplate>
                                <asp:Label ID="lblIndTotal" runat="Server" Text='<%#Eval("TOTAL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                    </Columns>
                </asp:GridView>
                     
                     
                     
                     
                     
                     
                     
                     
                     <br />
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                <asp:GridView ID="gvSpcDetails" TabIndex="9" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" EmptyDataText="No Records Found">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SPC_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Space">
                            <ItemTemplate>
                                <asp:Label ID="lblSpace" runat="Server" Text='<%#Eval("SPC_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Space View Name">
                            <ItemTemplate>
                                <asp:Label ID="lblSpaceViewName" runat="Server" Text='<%#Eval("SPC_VIEW_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                          <%-- <asp:TemplateField HeaderText="Total" Visible="false">
                        <ItemTemplate>
                        <asp:Label ID="lblTotal" runat="server" Text='<%#Eval("TOTAL") %>'></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>--%>
                             <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkStatus" runat="server" text='<%#BIND("spc_smssta_id")%>' CommandArgument='<%#Bind("spc_smssta_id")%>' CommandName="Status"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField ShowHeader="false">
                        <ItemTemplate>
                            <a href='frmModifySpace.aspx?id=<%#Eval("SPC_ID")%>'>EDIT</a>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                  
                    </Columns>
                </asp:GridView>
      
      
                
      
      
      
      
      
      




            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
