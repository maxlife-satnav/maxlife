<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LveViewRequisition.ascx.vb" Inherits="Controls_LveViewRequisition" %>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvItemsC" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="Sorry No Compensatory Leaves Records Found" OnPageIndexChanging="gvItemsC_PageIndexChanging">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblCReqId" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_COMREQ_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Requested By">
                    <ItemTemplate>
                        <asp:Label ID="lblCReqBy" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_COMAUR_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From Date">
                    <ItemTemplate>
                        <asp:Label ID="lblCFromDate" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_COM_FDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Date">
                    <ItemTemplate>
                        <asp:Label ID="lblCToDate" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_COM_TDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Number Of Days">
                    <ItemTemplate>
                        <asp:Label ID="lblCTotDays" runat="Server" CssClass="bodyText" Text='<%#Eval("LMS_COM_DAYS") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested On">
                    <ItemTemplate>
                        <asp:Label ID="lblCReqOn" runat="Server" CssClass="bodyText" Text='<%#Eval("LMS_COMREQ_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View">
                    <ItemTemplate>
                        <a href='frmLveRMApprovalCompOff.aspx?ReqID=<%#Eval("LMS_COMREQ_ID") %>'>View Details</a>

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
