<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HRApprovalSurrenderLease.ascx.vb" Inherits="Controls_HRSurrenderLease" %>
<script type="text/javascript">
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };

    function showPopWin(id) {
        $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
            $("#myModal").modal().fadeIn();
        });
    }
</script>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label"></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">

        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
            AllowPaging="True" PageSize="5" EmptyDataText=" No Surrender Lease Found" CssClass="table table-condensed table-bordered table-hover table-striped"
            Style="font-size: 12px;">
            <Columns>
                <asp:TemplateField HeaderText="Lease">
                    <ItemTemplate>
                        <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("LEASE_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lessor">
                    <ItemTemplate>
                        <asp:Label ID="lblLesseName" runat="server" Text='<%#Eval("LESSE_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("LEASE_BDG_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Effective Agreement">
                    <ItemTemplate>
                        <asp:Label ID="lbllsdate" runat="server" Text='<%#Eval("LEASE_START_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Expiry Date of Agreement">
                    <ItemTemplate>
                        <asp:Label ID="LBLEXPIRY" runat="server" Text='<%#Eval("LEASE_EXPIRY_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View Surrender Details">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkLseSurrender" runat="server" Text="View Details" CommandName="Surrender"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="#" onclick="showPopWin('<%# HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>')">View Lease Details</a>
                        <%--<a href='frmViewLeaseDetailsUser.aspx?id=<%#Eval("LEASE_NAME")%>'>View Details</a>--%>
                        <%--<a href="#" onclick="showPopWin('frmViewLeaseDetailsUser.aspx?id=<%#Eval("LEASE_NAME")%>',700,550,null)">View Lease Details</a>--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <%--CssClass="accordionContent" --%>
        <asp:GridView ID="gvempdetails" runat="server" EmptyDataText="No Records Found"
            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
            AllowPaging="false" AllowSorting="false" PageSize="1" AutoGenerateColumns="false"
            ShowHeader="false" CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12 control-label">Employee ID</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12 text-left">
                                            <asp:Label ID="lblempid" runat="server" CssClass="col-md-12 control-label" Text='<%#Eval("EMP_ID")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12 control-label">Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12 text-left">
                                            <asp:Label ID="lblempname" runat="server" CssClass="col-md-12 control-label" Text='<%#Eval("EMP_NAME")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12 control-label">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12 text-left">
                                            <asp:Label ID="lblempemail" runat="server" CssClass="col-md-12 control-label" Text='<%#Eval("EMP_EMAIL")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12 control-label">Designation</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12 text-left">
                                            <asp:Label ID="lbldesig" runat="server" CssClass="col-md-12 control-label" Text='<%#Eval("EMP_DESIG")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div id="pnltermination" runat="Server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Property Type<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select City<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Lease<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtlease" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Effective Date of Agreement</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtSdate" runat="server" CssClass="form-control" Enabled="false" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Expiry Date of Agreement<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtedate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Pending Security deposit</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtapay" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Lease Rent<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtlcost" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Received Security Deposit Amount</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtamt" runat="server" CssClass="form-control" AutoPostBack="True" OnTextChanged="txtamt_TextChanged" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Receiving Mode <span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:RequiredFieldValidator ID="cvmode" runat="server" ControlToValidate="ddlpaymentmode"
                            Display="None" ErrorMessage="Please Select Receiving Mode" ValidationGroup="Val1"
                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <asp:DropDownList ID="ddlpaymentmode" runat="server" CssClass="selectpicker" data-live-search="true"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlpaymentmode_SelectedIndexChanged">
                            <asp:ListItem Value="--Select--">--Select ReceivingMode--</asp:ListItem>
                            <asp:ListItem Value="1">CHEQUE</asp:ListItem>
                            <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                            <asp:ListItem Value="3">NEFT/RTGS</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        </div>
    </div>
    <div id="panel2" runat="Server">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9, ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                ErrorMessage="Enter Valid Account Number" ValidationExpression="^[a-zA-Z0-9, ]*$"
                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="panel4" runat="Server">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TXTacccredit"
                                Display="None" ErrorMessage="Account Number Required" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                ControlToValidate="TXTacccredit" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[A-Za-z0-9 ]*$"
                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="TXTacccredit" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="panel3" runat="Server">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                                Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[A-Za-z0-9 ]*$"
                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtIBankName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                                Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtDeposited"
                                ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtDeposited" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Branch Name <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvl3brnch" runat="server" ControlToValidate="txtbrnch"
                                Display="None" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revl3brnch" Display="None" runat="server" ControlToValidate="txtbrnch"
                                ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtbrnch" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">IFSC Code <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvIFsc" runat="server" ControlToValidate="txtIFSC"
                                Display="None" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="REVIFsc" Display="None" runat="server" ControlToValidate="txtIFSC"
                                ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtIFSC" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Paid Date</label>
                    <div class="col-md-7">
                        <div class='input-group date' id='effdate'>
                            <div onmouseover="Tip('Please Pick Date')" onmouseout="UnTip()">
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTdate"
                            Display="None" ErrorMessage="Please Select Paid Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtTdate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Lease Terminated Date <span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:RequiredFieldValidator ID="rfvtedate" runat="server" ControlToValidate="txtTedate"
                            Display="None" ErrorMessage="Please Select Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div class='input-group date' id='fromdate'>
                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtTedate" runat="server" CssClass="form-control"> </asp:TextBox>
                            </div>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Date of Realization <span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <div class='input-group date' id='realdt'>
                            <div onmouseover="Tip('Please Pick Date')" onmouseout="UnTip()">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtrdzdate"
                            Display="None" ErrorMessage="Please Select Date of Realization" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtrdzdate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('realdt')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Payment Remarks <span style="color: red;">*</span></label>
                    <div class="col-md-7">
                        <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div onmouseover="Tip('Enter Remarks with Maximum 250 characters and No special characters allowed ')" onmouseout="UnTip()">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="250" Rows="3" TextMode="MultiLine"> </asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            
                <div class="row">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" CausesValidation="true" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnModify" runat="server" Text="Received Security Deposit Amount" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" CausesValidation="true" OnClick="btnModify_Click" />
                    <asp:Button ID="btnterminatepay" runat="server" Text="Terminated Advance Not Yet Received" ValidationGroup="Val1" Visible="false" CausesValidation="true" CssClass="btn btn-primary custom-button-color" OnClick="btnterminatepay_Click" />
                </div>
            
        </div>
    </div>
</div>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lease Application form</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>





