<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucAssetMovementReport.ascx.vb"
    Inherits="Controls_ucAssetMovementReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div>
    <%--<div class="col-md-8 pull-right">
        <fieldset>
            <legend>Inter Asset Movement Report 
            </legend>
        </fieldset>
    </div>--%>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Search by Asset Code</label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Asset Code" ControlToValidate="txtsearchInter"
                        Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtsearchInter" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="row">
                        <div class="col-md-5 control-label">
                            <asp:Button ID="btnsearchInter" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><div class="row" style="padding-left:30px">
  
  


    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%-- <div class="row">--%>
            <label>Asset Category</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>

    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--     <div class="row">--%>
            <label>Asset Sub Category</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlAstSubCat"
                Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--">
            </asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Sub Category" AutoPostBack="True">
            </asp:DropDownList>
            <%--</div>--%>
        </div>
    </div>
         <div class="col-md-1 col-sm-12 col-xs-12"></div>

        <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">
            <%--       <div class="row">--%>
            <label>Asset Brand/Make</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlAstBrand"
                Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
            </asp:DropDownList>
            <%-- </div>--%>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
        <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>
            Asset Model</label>

            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>


            <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model"
                >
            </asp:DropDownList>



        </div>
    </div>
</div>
<div class="row" style="padding-left:30px">

     <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Location</label>
     
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlLocation"
                Display="none" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>


            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                ToolTip="Select Asset Model" >
            </asp:DropDownList>
        </div>
    </div>
     <div class="col-md-1 col-sm-12 col-xs-12"></div>
 <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

           <label>Duration</label> <br />


        
                                        <select id="ddlRange"  class="selectpicker" onhange="getDate(this)">
                                            <option value="">Select Range</option>
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    


        </div>
    </div><div class="col-md-1 col-sm-12 col-xs-12"></div>
    <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>From Date</label>

            <%--<asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="FromDate"
                ErrorMessage="Please From Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>

            <div class='input-group date' id='Div1'>
                <asp:TextBox ID="FromDate" runat="server" CssClass="form-control"  placeholder="mm/dd/yyyy" MaxLength="10" > </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                </span>
            </div>


        </div>
    </div> <div class="col-md-1 col-sm-12 col-xs-12"></div>
      <div class="col-md-2 col-sm-12 col-xs-12">
        <div class="form-group">

         <label>To Date</label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ToDate"
                ErrorMessage="Please Enter To Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>

            <div class='input-group date' id='Div4'>
                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                </span>
            </div>


        </div>
    </div><%--<div class="col-md-1 col-sm-12 col-xs-12"></div>--%>
      <div class="col-md-1 col-sm-12 col-xs-12 text-right" style="padding-left:30px">
            <div class="form-group"><br />
                <asp:Button ID="btnSubmit" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                <%--<asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary custom-button-color" />--%>
            </div>
        </div>
    
</div>
<div>
    <div class="row table table table-condensed table-responsive">
        <div class="row">
            <div class="col-md-12">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
            </div>
        </div>
    </div>
</div>

    <%-- <div class="col-md-8 pull-right">
        <fieldset>
            <legend>Intra Asset Movement Report 
            </legend>
        </fieldset>
    </div>--%>
    <%-- <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Search by Asset Code: <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter Asset Code" ControlToValidate="txtsearchIntra"
                        Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtsearchIntra" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="row">
                        <div class="col-md-5 control-label">
                            <asp:Button ID="btnsearchIntra" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    <%--<div class="row" id="pnl2" runat="server">
        <div class="row table table table-condensed table-responsive">
                <div class="form-group">
                    <div class="col-md-12">
                        <rsweb:ReportViewer ID="ReportViewer2" runat="server" Width="100%"></rsweb:ReportViewer>
                    </div>
                </div>
            </div>
    </div>--%>
</div>

