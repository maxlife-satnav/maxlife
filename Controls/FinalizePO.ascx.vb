Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common

Partial Class Controls_FinalizePO
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            lblMsg.Text = ""
        End If
    End Sub

    Private Sub BindGrid()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_GetByStatusId")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_GetByStatusId_BYAURID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            divRemarks.Visible = False
        Else
            divRemarks.Visible = True
        End If


    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnApproveAll_Click(sender As Object, e As EventArgs) Handles btnApproveAll.Click
        Dim count As Integer = 0
        For Each gvRow As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                count = count + 1
                Exit For
            End If
        Next
        If count > 0 Then
            For Each gvRow As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)


                lblMsg.Text = ""
                If chkSelect.Checked = True Then

                    Dim lblPoId As Label = DirectCast(gvRow.FindControl("lblPoId"), Label)
                    Dim lblAdvance As Label = DirectCast(gvRow.FindControl("lblAdvance"), Label)


                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_UpdateStatusFinal")
                    sp.Command.AddParameter("@AIP_PO_ID", lblPoId.Text, DbType.String)
                    sp.Command.AddParameter("@StatusId", 2, DbType.Int32)
                    sp.Command.AddParameter("@AIP_PO_REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@ADVANCE", lblAdvance.Text, DbType.String)
                    sp.ExecuteScalar()
                    send_mail_Finalize_PO(lblPoId.Text)
                End If
            Next
            lblMsg.Visible = True
            lblMsg.Text = "PO(s) has been Approved Successfully"
        Else
            lblMsg.Text = "Please select PO(s) to Approve"
        End If
        BindGrid()

    End Sub
    Public Sub send_mail_Finalize_PO(ByVal POnum As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_FINALIZE")
        sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
        sp.Execute()
    End Sub

End Class
