<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VPHRApprovalLease.ascx.vb" Inherits="Controls_VPHRApprovalLease" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row" runat="server" visible="false">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-4 control-label">Select Lease Type <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype" Display="None" ErrorMessage="Please Select Lease Type"
                    InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-8">
                    <asp:DropDownList ID="ddlLtype" runat="Server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label text-left">Search by Tenant Code/Tenant Name/Property Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvTxtEmpId" runat="server" ControlToValidate="txtempid" Display="None" ErrorMessage="Please Enter Tenant Code/Tenant Name/Property Name"
                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtempid" runat="Server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">
            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="false" CssClass="btn btn-primary custom-button-color" Text="Reset" />
        </div>
    </div>
</div>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
            AllowPaging="True" PageSize="5" EmptyDataText="No Competent Authority Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField HeaderText="Employee No">
                    <ItemTemplate>
                        <asp:Label ID="lbllEmpNo" runat="server" CssClass="clsLabel" Text='<%#Eval("LESSE_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease">
                    <ItemTemplate>
                        <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CTS Number">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="clsLabel" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CITY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Lessor">
                    <ItemTemplate>
                        <asp:Label ID="lblLesseName" runat="server" CssClass="clsLabel" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Expiry Date">
                    <ItemTemplate>
                        <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Status">
                    <ItemTemplate>
                        <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Created By">
                    <ItemTemplate>
                        <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <a href='VPHRApprovalLeaseDetails.aspx?id=<%#HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>'>View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<h4>Lease History</h4>
<div class="row" runat="server" id="panel1" style="margin-top: 10px">
    <div class="col-md-12">
        <%--<asp:Panel ID="panel1" runat="server" GroupingText="Lease History Details">--%>
        <asp:GridView ID="gvitems1" runat="server" AutoGenerateColumns="False" AllowSorting="True"
            AllowPaging="True" PageSize="5" EmptyDataText="No Competent Authority Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField HeaderText="Lease">
                    <ItemTemplate>
                        <asp:Label ID="lbllname" runat="server" CssClass="lblCODE" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CTS Number">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblcity" runat="server" CssClass="lblcity" Text='<%#Eval("CITY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lessor">
                    <ItemTemplate>
                        <asp:Label ID="lblLesseName" runat="server" CssClass="lblLesseName" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lblsdate" runat="server" CssClass="lblstartdate" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Expiry Date">
                    <ItemTemplate>
                        <asp:Label ID="lblEdate" runat="server" CssClass="lblExpiryDate" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Status">
                    <ItemTemplate>
                        <asp:Label ID="lblLstatus" runat="server" CssClass="lblLstatus" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Created By">
                    <ItemTemplate>
                        <asp:Label ID="Lbluser" runat="server" CssClass="lbluser" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <%--<a href="#" onclick="showPopWin('../SMS_Webfiles/frmViewLeaseDetailsuser.aspx?id=<%#Eval("LEASE_NAME")%>',750,580,null)">ViewDetails</a>--%>
                        <%--<a href="#" onclick="showPopWin('<%#Eval("LEASE_NAME")%>')">ViewDetails</a> --%>
                        <%--<a href='frmViewLeaseDetailsuser.aspx?id=<%#Eval("LEASE_NAME")%>'>View Details</a>--%>
                        <a href="#" onclick="showPopWin('<%# HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>')">View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>

    </div>
</div>


<%-- Modal popup block --%>

<div class="modal fade" id="myModal" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lease Application form</h4>
            </div>
            <div class="modal-body" id="modelcontainer">
                <%-- Content loads here --%>
            </div>
        </div>
    </div>
</div>

<%-- Modal popup block--%>

<script>
    function showPopWin(id) {
        $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
            $("#myModal").modal().fadeIn();
        });
    }
</script>
