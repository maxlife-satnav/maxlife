<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SurrenderLease.ascx.vb" Inherits="Controls_SurrenderLease" %>

<script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>

<script type="text/javascript">

    //$(document).ready(function () {        
    //    var nowDate = new Date();
    //    var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
    //    console.log(today);
    //    $("#effdate").datepicker({
    //        startDate: today,
    //        format: 'mm/dd/yyyy',
    //        autoclose: true
    //    });
    //    $("#refdt").datepicker({
    //        startDate: today,
    //        format: 'mm/dd/yyyy',
    //        autoclose: true
    //    });
    //});


    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };

</script>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<strong>
    <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel"></asp:Label></strong>
<div class="row">
   <div class="col-md-12">
        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
            AllowPaging="True" PageSize="5" EmptyDataText="No Surrender Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
            Style="font-size: 12px;">
            <Columns>
                <asp:TemplateField HeaderText="Lease Name">
                    <ItemTemplate>
                        <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("LEASE_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee ID">
                    <ItemTemplate>
                        <asp:Label ID="lblempid" runat="server" Text='<%#Eval("EMP_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lessor">
                    <ItemTemplate>
                        <asp:Label ID="lblLesseName" runat="server" Text='<%#Eval("LESSE_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("LEASE_BDG_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lblLsdate" runat="server" Text='<%#Eval("LEASE_START_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Expiry Date">
                    <ItemTemplate>
                        <asp:Label ID="lblLedate" runat="server" Text='<%#Eval("LEASE_EXPIRY_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkLseSurrender" runat="server" Text="Surrender" CommandName="Surrender"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="#" onclick="showPopWin('<%# HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>')">View</a>
                        <%--<a href='frmViewLeaseDetailsuser.aspx?id=<%#Eval("LEASE_NAME")%>'>View Details</a>--%>
                        <%--<a href="#" onclick="showPopWin('frmViewLeaseDetailsUser.aspx?id=<%#Eval("LEASE_NAME")%>',750,580,null)">View Details</a>--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>

<div id="pnl1" runat="Server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">Lease Name</label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">Enter Surrender Date<span style="color: red;">*</span></label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <asp:RequiredFieldValidator ID="rfvsdate" runat="Server" ControlToValidate="txtsurrender"
                            ErrorMessage="Please Enter Surrender Date" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div class='input-group date' id='effdate'>
                            <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtsurrender" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">Enter Refund Date<span style="color: red;">*</span></label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="Server" ControlToValidate="txtrefund"
                            ErrorMessage="Please Enter Refund Date" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div class='input-group date' id='refdt'>
                            <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtrefund" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('refdt')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Enter Refund Amount<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtrefundamount"
                            ErrorMessage="Please Enter Valid Refund Amount " Display="None" ValidationGroup="Val1"
                            ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="Server" ControlToValidate="txtrefundamount"
                            ErrorMessage="Please Enter Refund Amount" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtrefundamount" runat="server" CssClass="form-control">0</asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                    <div class="col-md-7">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="Server" ControlToValidate="txtremarks"
                            ErrorMessage="Please Enter Remarks" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div onmouseover="Tip('Please Enter Remarks')" onmouseout="UnTip()">
                            <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" Rows="5" TextMode="multiLine" MaxLength="1000"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lease Application form</h4>
            </div>
            <div class="modal-body" id="modelcontainer">
                <%-- Content loads here --%>
            </div>
        </div>
    </div>
</div>

<script>
    function showPopWin(id) {
        $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
            $("#myModal").modal().fadeIn();
        });
    }
</script>
