<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditAssetPrices.ascx.vb"
    Inherits="Controls_EditAssetPrices" %>
       <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
 <ContentTemplate>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Edit Asset Cost
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong> Edit Asset Cost</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table width="100%" cellpadding="2px">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" CssClass="bodytext" Text="Select Location"></asp:Label></td>
                        <td align="left">
                            <div>
                                <asp:DropDownList ID="ddlLocation" AutoPostBack="true" CssClass="bodytext" runat="server" Width="50%">
                                </asp:DropDownList>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label4" runat="server" CssClass="bodytext" Text="Select Tower"></asp:Label></td>
                        <td align="left">
                            <div>
                                <asp:DropDownList ID="ddlTower" AutoPostBack="true" CssClass="bodytext" runat="server" Width="50%">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label5" runat="server" CssClass="bodytext" Text="Select Floor"></asp:Label></td>
                        <td align="left">
                            <div>                                
                                <asp:DropDownList ID="ddlFloor" AutoPostBack="true" CssClass="bodytext" runat="server" Width="50%"> 
                                </asp:DropDownList> 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td style="clear:both;float:left;">
                            <asp:Button ID="btnView" runat="server" CssClass="button" Text="View" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <fieldset id="Fieldset1" visible="false" runat="server">
                                <legend>Asset List</legend>
                                <asp:GridView ID="gvAstList" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" EmptyDataText="No Assets Found" Width="100%">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tower">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTWR_NAME" runat="server" Text='<%#Eval("TWR_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Floor">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFLR_NAME" runat="server" Text='<%#Eval("FLR_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAIM_NAME" runat="server" Text='<%#Eval("AIM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Cost">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAstPrice" CssClass="bodytext" Text='<%# FORMATNUMBER(Eval("AAT_AST_COST"),2) %>'
                                                    runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Salvage Value">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSalvageValue" runat="server" Text='<%# FORMATNUMBER(Eval("OurPrice"),2)%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                <asp:Label ID="lblAAT_ID" Visible="false" runat="server" Text='<%#Eval("AAT_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:Button ID="btnSubmit" Visible="false" runat="server" CssClass="button" Text="Submit" /></td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</div>
</ContentTemplate>
           <Triggers>
               <asp:AsyncPostBackTrigger ControlID="btnView" EventName="Click" />
           </Triggers>
</asp:UpdatePanel>
