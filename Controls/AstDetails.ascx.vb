Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Promotions
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.IO
Partial Class Controls_AstDetails
    Inherits System.Web.UI.UserControl

    Public product As Commerce.Common.Product
    Protected productID As Integer
    Protected productSku, ProductName As String
    Protected productGUID As Guid
    Public ImageFile As String
    Dim ds As New DataSet
    Private selectedAttributes_Renamed As Commerce.Common.Attributes
    Dim oBjMiscfunctions As New MiscFunctions

    'Public ReadOnly Property SelectedAttributes() As Commerce.Common.Attributes
    '    Get
    '        Return attList.SelectedAttributes
    '    End Get
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            'If Not IsPostBack Then
            Dim sProductGUID As String = Utility.GetParameter("guid")
            productID = Utility.GetIntParameter("id")
            productSku = Utility.GetParameter("n")
            ProductName = Utility.GetParameter("prdname")

            If sProductGUID <> String.Empty Then
                productGUID = New Guid(sProductGUID)
                product = ProductController.GetProductDeepByGUID(productGUID)
            ElseIf productID <> 0 Then
                ds = ProductController.GetProductDeepProductId(productID)
                If ds.Tables(0).Rows.Count > 0 Then
                    productID = ds.Tables(0).Rows(0).Item("ProductId")
                    product = ProductController.GetProductDeep(productID)
                    'ImageFile = ds.Tables(0).Rows(0).Item("ImageFile")
                End If
            ElseIf productSku <> String.Empty Then
                product = ProductController.GetProductDeep(productSku)
            ElseIf ProductName <> String.Empty Then
                ds = ProductController.GetProductDeepProductName(ProductName)
                If ds.Tables(0).Rows.Count > 0 Then
                    productID = ds.Tables(0).Rows(0).Item("ProductId")
                    product = ProductController.GetProductDeep(productID)
                    'ImageFile = ds.Tables(0).Rows(0).Item("ImageFile")

                End If
            End If
            'End If
            ' pRating.DisplayValue = CDbl(product.Rating)
            'pRating.DisplayValue = 5

            'If Not product Is Nothing Then
            '    discount = PromotionService.SetProductPricing(product)
            'AstAttributeSelection1.Product = product

            'End If
            'attList.Product = product
            AstDescriptorDisplay1.DescriptorList = product.Descriptors
            'ReviewDisplay1.product = product
        Catch ex As Exception

        End Try
    End Sub

  
End Class

