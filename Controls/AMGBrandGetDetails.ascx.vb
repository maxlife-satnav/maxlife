Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGBrandGetDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvDetails_AAB.PageIndex = 0
            fillgrid()
            lbtn1.Visible = False
        End If
    End Sub
    Public Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMG_BRAND")
        sp.Command.AddParameter("@AAB_CODE", txtfindcode.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvDetails_AAB.DataSource = ds
        gvDetails_AAB.DataBind()
        For i As Integer = 0 To gvDetails_AAB.Rows.Count - 1
            Dim lblStaID_AAB As Label = CType(gvDetails_AAB.Rows(i).FindControl("lblStaID_AAB"), Label)
            If lblStaID_AAB.Text = "0" Then
                lblStaID_AAB.Text = "Inactive"
            Else
                lblStaID_AAB.Text = "Active"
            End If
        Next
    End Sub
    Protected Sub gvDetails_AAB_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails_AAB.PageIndexChanging
        gvDetails_AAB.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Protected Sub gvDetails_AAB_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_AAB.SelectedIndexChanged    
    End Sub
    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click      
        fillgrid()
        lbtn1.Visible = True
    End Sub
    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
        txtfindcode.Text = ""
        fillgrid()
        lbtn1.Visible = False
    End Sub
    Protected Sub gvDetails_AAB_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails_AAB.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblCode_AAB As Label = DirectCast(gvDetails_AAB.Rows(rowIndex).FindControl("lblCode_AAB"), Label)
            Dim id As String = lblCode_AAB.Text
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMG_AAB_DEL")
            SP1.Command.AddParameter("@AAB_CODE", id, DbType.String)
            SP1.ExecuteScalar()
        End If
        fillgrid()
    End Sub

    Protected Sub gvDetails_AAB_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDetails_AAB.RowDeleting

    End Sub
End Class
