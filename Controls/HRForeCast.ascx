<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HRForeCast.ascx.vb" Inherits="Controls_HRForeCast" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>



<div>
    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">HR ForeCast
             <hr align="center" width="60%" /></asp:Label></td>
        </tr>
    </table>
    <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
        <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left" width="100%" colspan="3">
                    <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong>&nbsp; HR ForeCast</strong>
                </td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table id="table1" cellspacing="0" cellpadding="2" width="100%" border="1">
                        <tr>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                Select Location<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlLocation"
                                    Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="clsComboBox" Width="99%">
                                    
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                Select Department <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvDept" runat="server" ControlToValidate="ddlDept"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="--Select City--"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                <asp:DropDownList ID="ddlDept" runat="server" CssClass="clsComboBox" Width="99%">
                                  
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                Select Designation <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvDesig" runat="server" ControlToValidate="ddlDesig"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Designation"
                                    InitialValue="--Select City--"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                <asp:DropDownList ID="ddlDesig" runat="server" CssClass="clsComboBox" Width="99%">
                                   
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                ForeCasted Number<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvFNumber" runat="server" ControlToValidate="txtFnumber"
                                    Display="None" ErrorMessage="Please enter Forecasted Number" ValidationGroup="Val1">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revFNumber" runat="server" ControlToValidate="txtFnumber"
                                    ErrorMessage="Please enter Forecasted Number" Display="none" ValidationExpression="^[0-9]*$"
                                    ValidationGroup="Val1">
                                </asp:RegularExpressionValidator>
                            </td>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                <asp:TextBox ID="txtFnumber" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                From Month <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvFMonth" runat="server" ControlToValidate="ddlFMonth"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select From Month"
                                    InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:DropDownList ID="ddlFMonth" runat="server" CssClass="clsComboBox" Width="99%">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                    <asp:ListItem Value="1">JAN</asp:ListItem>
                                    <asp:ListItem Value="2">FEB</asp:ListItem>
                                    <asp:ListItem Value="3">MAR</asp:ListItem>
                                    <asp:ListItem Value="4">APRIL</asp:ListItem>
                                    <asp:ListItem Value="5">MAY</asp:ListItem>
                                    <asp:ListItem Value="6">JUNE</asp:ListItem>
                                    <asp:ListItem Value="7">JULY</asp:ListItem>
                                    <asp:ListItem Value="8">AUG</asp:ListItem>
                                    <asp:ListItem Value="9">SEP</asp:ListItem>
                                    <asp:ListItem Value="10">OCT</asp:ListItem>
                                    <asp:ListItem Value="11">NOV</asp:ListItem>
                                    <asp:ListItem Value="12">DEC</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                From Year <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvFYear" runat="server" ControlToValidate="ddlFYear"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select From Year"
                                    InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:DropDownList ID="ddlFYear" runat="server" CssClass="clsComboBox" Width="99%">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                    <asp:ListItem Value="1">2011</asp:ListItem>
                                    <asp:ListItem Value="2">2012</asp:ListItem>
                                    <asp:ListItem Value="3">2013</asp:ListItem>
                                    <asp:ListItem Value="4">2014</asp:ListItem>
                                    <asp:ListItem Value="5">2015</asp:ListItem>
                                    <asp:ListItem Value="6">2016</asp:ListItem>
                                    <asp:ListItem Value="7">2017</asp:ListItem>
                                    <asp:ListItem Value="8">2018</asp:ListItem>
                                    <asp:ListItem Value="9">2019</asp:ListItem>
                                    <asp:ListItem Value="10">2020</asp:ListItem>
                                    <asp:ListItem Value="11">2021</asp:ListItem>
                                    <asp:ListItem Value="12">2022</asp:ListItem>
                                    <asp:ListItem Value="13">2023</asp:ListItem>
                                    <asp:ListItem Value="14">2024</asp:ListItem>
                                    <asp:ListItem Value="15">2025</asp:ListItem>
                                    <asp:ListItem Value="16">2026</asp:ListItem>
                                    <asp:ListItem Value="17">2027</asp:ListItem>
                                    <asp:ListItem Value="18">2028</asp:ListItem>
                                    <asp:ListItem Value="19">2029</asp:ListItem>
                                    <asp:ListItem Value="20">2030</asp:ListItem>
                                    <asp:ListItem Value="21">2031</asp:ListItem>
                                    <asp:ListItem Value="22">2032</asp:ListItem>
                                    <asp:ListItem Value="23">2033</asp:ListItem>
                                    <asp:ListItem Value="24">2034</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                To Month <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvTMonth" runat="server" ControlToValidate="ddlTMonth"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select To Month" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:DropDownList ID="ddlTMonth" runat="server" CssClass="clsComboBox" Width="99%">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                    <asp:ListItem Value="1">JAN</asp:ListItem>
                                    <asp:ListItem Value="2">FEB</asp:ListItem>
                                    <asp:ListItem Value="3">MAR</asp:ListItem>
                                    <asp:ListItem Value="4">APRIL</asp:ListItem>
                                    <asp:ListItem Value="5">MAY</asp:ListItem>
                                    <asp:ListItem Value="6">JUNE</asp:ListItem>
                                    <asp:ListItem Value="7">JULY</asp:ListItem>
                                    <asp:ListItem Value="8">AUG</asp:ListItem>
                                    <asp:ListItem Value="9">SEP</asp:ListItem>
                                    <asp:ListItem Value="10">OCT</asp:ListItem>
                                    <asp:ListItem Value="11">NOV</asp:ListItem>
                                    <asp:ListItem Value="12">DEC</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                To Year <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvTyear" runat="server" ControlToValidate="ddlTYear"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select To Year" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:DropDownList ID="ddlTYear" runat="server" CssClass="clsComboBox" Width="99%">
                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                 
                                    <asp:ListItem Value="1">2011</asp:ListItem>
                                    <asp:ListItem Value="2">2012</asp:ListItem>
                                    <asp:ListItem Value="3">2013</asp:ListItem>
                                    <asp:ListItem Value="4">2014</asp:ListItem>
                                    <asp:ListItem Value="5">2015</asp:ListItem>
                                    <asp:ListItem Value="6">2016</asp:ListItem>
                                    <asp:ListItem Value="7">2017</asp:ListItem>
                                    <asp:ListItem Value="8">2018</asp:ListItem>
                                    <asp:ListItem Value="9">2019</asp:ListItem>
                                    <asp:ListItem Value="10">2020</asp:ListItem>
                                    <asp:ListItem Value="11">2021</asp:ListItem>
                                    <asp:ListItem Value="12">2022</asp:ListItem>
                                    <asp:ListItem Value="13">2023</asp:ListItem>
                                    <asp:ListItem Value="14">2024</asp:ListItem>
                                    <asp:ListItem Value="15">2025</asp:ListItem>
                                    <asp:ListItem Value="16">2026</asp:ListItem>
                                    <asp:ListItem Value="17">2027</asp:ListItem>
                                    <asp:ListItem Value="18">2028</asp:ListItem>
                                    <asp:ListItem Value="19">2029</asp:ListItem>
                                    <asp:ListItem Value="20">2030</asp:ListItem>
                                    <asp:ListItem Value="21">2031</asp:ListItem>
                                    <asp:ListItem Value="22">2032</asp:ListItem>
                                    <asp:ListItem Value="23">2033</asp:ListItem>
                                    <asp:ListItem Value="24">2034</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnSubmit" runat="Server" Text="Submit" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </asp:Panel>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
    </Triggers>
</asp:UpdatePanel>
