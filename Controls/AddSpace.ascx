<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddSpace.ascx.vb" Inherits="Controls_AddSpace" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Excel<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvspacedata" runat="server" ControlToValidate="fpBrowseDoc" Display="None" ErrorMessage="Please Select File" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revfubrowse" runat="Server" ControlToValidate="fpBrowseDoc" Display="None" ErrorMessage="Only Excel file allowed" ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$" ValidationGroup="Val2"> 
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div class="btn btn-default" style="display: block!important;">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <asp:Button ID="btnbrowse" runat="Server" CausesValidation="true" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" />
                        </div>
                        <div class="col-md-4">
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary custom-button-color" Visible="False" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <asp:HyperLink ID="hyp" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/Space Data Master.xls" Text=" Download Template"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</div>

<cc1:ExportPanel ID="ExportPanel1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvspace" runat="server" EmptyDataText="No Space Found."
                CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="SNo" HeaderText="SNo">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Location Code" HeaderText="Location Code">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Tower Code" HeaderText="Tower Code">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Floor Code" HeaderText="Floor Code">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Wing Code" HeaderText="Wing Code">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="City Code" HeaderText="City Code">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Space Type" HeaderText="Space Type">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Space Name" HeaderText="Space Name">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Seat Type" HeaderText="Seat Type">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>

                    <asp:BoundField DataField="Space Area" HeaderText="Space Area">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Space Cost" HeaderText="Space Cost" DataFormatString="{0:c2}">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Remarks" HeaderText="Remarks">
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</cc1:ExportPanel>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblBDG" runat="server" class="col-md-5 control-label" Text="Select Location">Select Location <span style="color: red;">*</span>                
                </asp:Label>
                <asp:RequiredFieldValidator ID="rfvBDG" runat="server" ControlToValidate="ddlBDG" Display="None" ErrorMessage="Please Select Location" InitialValue="--Select Location--" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlBDG" runat="server" AutoPostBack="True" CssClass="selectpicker" TabIndex="1" data-live-search="true" ToolTip="Select Location">
                        <asp:ListItem>--Select Location--</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblTwr" runat="server" class="col-md-5 control-label" Text="Select Tower">Select Tower <span style="color: red;">*</span>  </asp:Label>

                <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ControlToValidate="ddlTWR" Display="None" ErrorMessage="Please Select Tower" InitialValue="--Select Tower--" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlTWR" runat="server" AutoPostBack="True" CssClass="selectpicker" TabIndex="2" data-live-search="true" ToolTip="Select Tower">
                        <asp:ListItem>--Select Tower--</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblFLR" runat="server" class="col-md-5 control-label" Text="Select Floor">Select Floor <span style="color: red;">*</span>                
                </asp:Label>
                <asp:RequiredFieldValidator ID="rfvFLR" runat="server" ControlToValidate="ddlFLR" Display="None" ErrorMessage="Please Select Floor" InitialValue="--Select Floor--" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlFLR" runat="server" AutoPostBack="True" CssClass="selectpicker" TabIndex="3" data-live-search="true" ToolTip="Select Floor">
                        <asp:ListItem>--Select Floor--</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblWNG" runat="server" class="col-md-5 control-label" Text="Select Wing">Select Wing <span style="color: red;">*</span>                
                </asp:Label>
                <asp:RequiredFieldValidator ID="frvWNG" runat="server" ControlToValidate="ddlWNG" Display="None" ErrorMessage="Please Select Wing" InitialValue="--Select Wing--" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlWNG" runat="server" CssClass="selectpicker" TabIndex="4" data-live-search="true" ToolTip="Select Wing">
                        <asp:ListItem>--Select Wing--</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="Label1" runat="server" class="col-md-5 control-label" Text="Select Space Type">Select Space Type <span style="color: red;">*</span>                
                </asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlSPCType" Display="None" ErrorMessage="Please Select Space Type" InitialValue="--Select Space Type--" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlSPCType" runat="server" AutoPostBack="True" CssClass="selectpicker" TabIndex="5" data-live-search="true" ToolTip="Select Space Type">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="Label8" runat="server" class="col-md-5 control-label" Text="Select Seat Type">Select Seat Type <span style="color: red;">*</span>                
                </asp:Label>
                <asp:RequiredFieldValidator ID="rfvseattype" runat="server" ControlToValidate="ddlseattype" Display="None" ErrorMessage="Please Select Seat Type" InitialValue="--Select--" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlseattype" runat="server" CssClass="selectpicker" TabIndex="6" data-live-search="true" ToolTip="Select Seat Type">
                        <asp:ListItem>--Select--</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="Label5" runat="server" class="col-md-5 control-label" Text="Space Name">Space Name(0000-9999)<span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtSpcName" Display="None" ErrorMessage="Please Enter Space Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtSpcName" Display="None" ErrorMessage="Space Name Should Be From 0000 To 9999" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtSpcName" runat="server" CssClass="form-control" MaxLength="4" TabIndex="7"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="Label4" runat="server" class="col-md-5 control-label" Text="Space Area">Space Area <span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtArea" Display="None" ErrorMessage="Please Enter Space Area Value" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtArea" Display="None" ErrorMessage="Invalid Area Value" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtArea" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="Label3" runat="server" class="col-md-5 control-label" Text="Space Cost">Space Cost <span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCost" Display="None" ErrorMessage="Please Enter Space Cost Value" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCost" Display="None" ErrorMessage="Invalid Cost Value" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblDesc" runat="server" class="col-md-5 control-label" Text="Description">Description <span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDesc" Display="None" ErrorMessage="Please Enter Description" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control" MaxLength="50" Rows="3" TabIndex="10" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblRem" runat="server" class="col-md-5 control-label" Text="Remarks">Remarks  <span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRem" Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRem" runat="server" CssClass="form-control" MaxLength="50" Rows="3" TabIndex="11" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblSatus" runat="server" class="col-md-5 control-label" Text="Status">Status <span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus" Display="None" ErrorMessage="Please Select Status" InitialValue="--Select Status--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" TabIndex="12" data-live-search="true" ToolTip="Select Status">
                        <asp:ListItem>--Select Status--</asp:ListItem>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" TabIndex="13" Text="Submit" ValidationGroup="Val1" />
            <asp:Button ID="btnback" runat="server" CausesValidation="false" CssClass="btn btn-primary custom-button-color" TabIndex="14" Text="Back" />

        </div>
    </div>
</div>
