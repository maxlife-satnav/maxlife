Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_UpdateMaintenanceRequest
    Inherits System.Web.UI.UserControl
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim orgfilename As String = ""
            Dim filedatetime As String = ""
            If (fuBrowseFile.HasFile) Then
                orgfilename = fuBrowseFile.FileName
                filedatetime = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & orgfilename
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filedatetime
                fuBrowseFile.PostedFile.SaveAs(filePath)
            End If
            insertnewrecord(filedatetime)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub insertnewrecord(ByVal filedatetime As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_STATUS")
        sp1.Command.AddParameter("@PN_MAINTENANCE_REQ", ddlMRequest.SelectedItem.Text, DbType.String)
        sp1.Command.AddParameter("@REQUEST_STATUS", ddlwstatus.SelectedItem.Value, DbType.String)
        sp1.ExecuteScalar()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_MAINTENANCE_DETAILS")
        sp.Command.AddParameter("@PN_REQUEST_ID", ddlMRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@PN_UPDATED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@PN_COMMENTS", txtComments.Text, DbType.String)
        sp.Command.AddParameter("@PN_DOCUMENT", filedatetime, DbType.String)
        sp.Command.AddParameter("PN_STATUS", ddlstatus.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=21")
        'lblMsg.Text = "Request Updated Succesfully"
        fillgrid()
        Cleardata()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim stat As String = Nothing
            stat = Request.QueryString("_flag")
            If stat Is Nothing Then
                getrequest()
            ElseIf stat = "0" Then
                BindReq()
                BindDetails()
            End If
            
        End If
    End Sub
    Private Sub BindReq()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"getreqs")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlMRequest.DataSource = sp.GetDataSet()
            ddlMRequest.DataTextField = "PN_MAINTENANCE_REQ"
            ddlMRequest.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Private Sub BindDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MAIN1_DETAILS")
        sp.Command.AddParameter("@PN_MAINTENANCE_REQ", Request.QueryString("_id"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then

            Dim li As ListItem = Nothing
            li = ddlMRequest.Items.FindByText(ds.Tables(0).Rows(0).Item("PN_MAINTENANCE_REQ"))
            If Not li Is Nothing Then
                li.Selected = True

            End If

            txtCity.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
            txtProperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
            txtTenant.Text = ds.Tables(0).Rows(0).Item("PN_TENANT_ID")
            txtReqType.Text = ds.Tables(0).Rows(0).Item("REQUEST_TYPE")
            Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
            If status = 0 Then
                ddlwstatus.SelectedValue = 0
            ElseIf status = 1 Then
                ddlwstatus.SelectedValue = 1
            Else
                ddlwstatus.SelectedValue = 2
            End If
        End If
        fillgrid()
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MAINIDS")
        sp.Command.AddParameter("@CUSER", Session("UID"), DbType.String)
        ddlMRequest.DataSource = sp.GetDataSet()
        ddlMRequest.DataTextField = "PN_MAINTENANCE_REQ"
        ddlMRequest.DataBind()
        ddlMRequest.Items.Insert(0, New ListItem("--Select MaintenanceRequest--", "0"))
    End Sub
    Public Sub Cleardata()
        getrequest()
        ddlMRequest.SelectedValue = 0
        txtComments.Text = ""    
        txtTenant.Text = ""
        txtReqType.Text = ""
        ddlwstatus.SelectedIndex = -1
        ddlstatus.SelectedIndex = -1
    End Sub
    Public Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MAINTENANCEREQ_DETAILS")
        sp.Command.AddParameter("@PN_MAINTENANCE_REQ", ddlMRequest.SelectedItem.Text, DbType.String)
        gvMrequest.DataSource = sp.GetDataSet()
        gvMrequest.DataBind()
    End Sub

    Protected Sub gvMrequest_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMrequest.PageIndexChanging
        gvMrequest.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Protected Sub ddlMRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMRequest.SelectedIndexChanged
        If ddlMRequest.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MAIN_DETAILS")
            sp.Command.AddParameter("@PN_MAINTENANCE_REQ", ddlMRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                txtCity.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
                txtProperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
                txtTenant.Text = ds.Tables(0).Rows(0).Item("PN_TENANT_ID")
                txtReqType.Text = ds.Tables(0).Rows(0).Item("REQUEST_TYPE")
                Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                If status = 0 Then
                    ddlwstatus.SelectedValue = 0
                ElseIf status = 1 Then
                    ddlwstatus.SelectedValue = 1
                Else
                    ddlwstatus.SelectedValue = 2
                End If
            End If
            fillgrid()
        Else
            txtComments.Text = ""
            txtCity.Text = ""
            txtProperty.Text = ""
            txtTenant.Text = ""
            txtReqType.Text = ""
            ddlwstatus.SelectedIndex = -1
            ddlstatus.SelectedIndex = -1
        End If
    End Sub
End Class
