<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HRAddLease.ascx.vb" Inherits="Controls_HRAddLease" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div id="UpdatePanel1" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                    </asp:Label>
                </div>
            </div>
        </div>
    </div>
    <fieldset>
        <legend>
            <asp:Label ID="lblHead" runat="server"></asp:Label></legend>
    </fieldset>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label"></label>
                    <asp:TextBox ID="txtstore1" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="step1heading" runat="server">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">
                        Step1: Lease Details</label>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        Property, Complete Address, State, Region, No of Landlords(Lessors), Power of Attorney, Date
                                                of Lease Agreement, One time charges(Stamp duty, Registration charges, Professional
                                                Fee (TSR), Consultancy / Brokerage, Total Rent per month, Basic Rent, Maintenance charges, DG
                                                Back up charges, Service Tax, Property Tax, Tenure of Agreement, Rent Revision(Escalation), Security
                                                Deposit, Interior Details(Leasehold Improvements, Furniture & Fixtures, Office Equipments))
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="step2heading" runat="server">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">
                        Step2: Brokerage Details & Recovery Amount</label>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        Brokerage Details(Broker Name, PAN Number, Email, Contact Details), Recovery Amount(
                                                Amount, Recovery From Date, Recovery To Date, Recovery Amount, User Account Number)
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="step3heading" runat="server">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">
                        Step3: Power of Attorney & Landlord details</label>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        Power of Attorney Details(POA Name, Address, Email, Contact Details), Landlord Amount(Landlord
                                                Name, Address, Email, Contact Details, PAN Number, Rent Amount, Security Deposit,
                                                Payment mode, User Account Number)
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <asp:GridView ID="gvEmpDetails" runat="server" AllowPaging="true" AllowSorting="true" EmptyDataText="No Lease Details Found."
                AutoGenerateColumns="false" Width="100%" CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:TemplateField HeaderText="Employee No">
                        <ItemTemplate>
                            <asp:Label ID="lblempno" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Name">
                        <ItemTemplate>
                            <asp:Label ID="lblname" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_fIRST_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                        <ItemTemplate>
                            <asp:Label ID="lblemail" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_EMAIL") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Department">
                        <ItemTemplate>
                            <asp:Label ID="lblDept" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_DEP_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Designation">
                        <ItemTemplate>
                            <asp:Label ID="lbldesg" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_DESGN_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DOJ">
                        <ItemTemplate>
                            <asp:Label ID="lblDoj" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_DOJ") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contact Details">
                        <ItemTemplate>
                            <asp:Label ID="lblext" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_EXTENSION") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mobile No">
                        <ItemTemplate>
                            <asp:Label ID="lblresidence" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_RES_NUMBER") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            </asp:GridView>
        </div>
    </div>
    <div id="panwiz1" runat="server">
        <h4>Lease Details</h4>        
        <div id="Tr1" runat="server" visible="false" class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select Status</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="Selectpicker" data-live-search="true">
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">Terminated</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select Lesse</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlLesse" runat="server" CssClass="Selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="Tr6" runat="server" visible="false">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Lease Type</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlLeaseType" runat="server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Lease Escalation Type</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True">
                                <asp:ListItem Value="PER">Percentage</asp:ListItem>
                                <asp:ListItem Value="FLT">Flat Amount</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="Tr7" runat="server" visible="false">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select Property Type<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Property Type"
                                InitialValue="0"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlproptype" runat="server" CssClass="Selectpicker" data-live-search="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select City <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True" TabIndex="1">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="Tr8" runat="server" visible="False">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">PinCode<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvPincode" runat="server" ControlToValidate="txtpincode"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter PinCode"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revpincode" runat="server" ControlToValidate="txtpincode"
                                ErrorMessage="Please enter valid PinCode" Display="None" ValidationGroup="Val1"
                                ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter Numerics only')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control" TabIndex="5"
                                    MaxLength="10">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Select Property <span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvpropaddr1" runat="server" ControlToValidate="ddlproperty"
                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Property "
                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlproperty" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">CTS Number </label>
                        <div class="col-md-7">
                            <div onmouseover="Tip('Enter Alphabets,Numbers and some special characters like /-\ with maximum length 50')"
                                onmouseout="UnTip()">
                                <asp:TextBox ID="txtLnumber" runat="server" CssClass="form-control" MaxLength="50"
                                    TabIndex="2"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="Tr9" runat="server">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Complete Address<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvpropaddr2" runat="server" ControlToValidate="txtBuilding"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Property Address2"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtBuilding" runat="server" CssClass="form-control" MaxLength="500" TabIndex="3"
                                TextMode="MultiLine" Rows="5">.</asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">State<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvpropaddr3" runat="server" ControlToValidate="txtprop3"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter State"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtprop3" runat="server" CssClass="form-control" MaxLength="50"
                                TabIndex="4">.</asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Region<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtregion"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Region"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtregion" runat="server" CssClass="form-control" MaxLength="50"
                                TabIndex="5">.</asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Stamp duty <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtsduty"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Stamp Duty"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                ControlToValidate="txtsduty" ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Stamp duty"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtsduty" runat="server" CssClass="form-control" MaxLength="12"
                                TabIndex="6"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Registration Charges <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtregcharges"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Registration Charges"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                ControlToValidate="txtregcharges" ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Registration Charges"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtregcharges" runat="server" CssClass="form-control" TabIndex="7"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Professional Fees <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtpfees"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Professional Fees"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                ControlToValidate="txtpfees" ValidationGroup="Val1" Display="None" ErrorMessage="Invalid  Professional Fees"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtpfees" runat="server" CssClass="form-control" MaxLength="12"
                                TabIndex="8"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Consultancy / Brokerage <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtbrokerage"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Brokerage Fees"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                ControlToValidate="txtbrokerage" ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Brokerage Fees"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtbrokerage" runat="server" CssClass="form-control" TabIndex="9"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Entitled Lease Amount</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtentitle" runat="server" CssClass="form-control" TabIndex="12">0</asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">
                            Basic Rent(Max.
                                                    <asp:Label ID="lblmaxrent" runat="server" CssClass="bodytext"></asp:Label>)<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Rent Amount with out Escalation "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtInvestedArea"
                                ErrorMessage="Please Enter Valid Rent Amount with out Escalation" Display="None"
                                ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtInvestedArea" runat="server" CssClass="form-control" MaxLength="15" TabIndex="13"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Built Up Area (sqft.)<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvOccupiedArea" runat="server" ControlToValidate="txtOccupiedArea"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Lease Built Up Area "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revbuilt" runat="server" ControlToValidate="txtOccupiedArea"
                                ErrorMessage="Please Enter Valid Lease BuiltUp Area" Display="None" ValidationGroup="Val1"
                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtOccupiedArea" runat="server" CssClass="form-control" MaxLength="15" TabIndex="14" AutoPostBack="true"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">
                            Security Deposit (Max.
                                                    <asp:Label ID="lblmaxsd" runat="server" CssClass="bodytext"></asp:Label>)<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="txtpay"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Security Deposit "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revsec" runat="server" ControlToValidate="txtpay"
                                ErrorMessage="Please Enter valid Security Deposit" Display="None" ValidationGroup="Val1"
                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtpay" runat="server" CssClass="form-control" TabIndex="15"
                                    MaxLength="26"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">DG Back up Charges<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtdg"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter DG Back Up Charges "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                ControlToValidate="txtdg" ErrorMessage="Please Enter Valid DG Back up charges"
                                Display="None" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtdg" runat="server" CssClass="form-control" MaxLength="15"
                                TabIndex="16"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Maintenance Charges <span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtmain1"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Maintenance Charges "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                ControlToValidate="txtmain" ErrorMessage="Please Enter Valid  Maintenance charges"
                                Display="None" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtmain1" runat="server" CssClass="form-control" MaxLength="15"
                                    ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div runat="server" id="backup2">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Furniture & Fixtures<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="txtfurniture"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Furniture & Fixtures cost"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                    ControlToValidate="txtfurniture" ErrorMessage="Please Enter Valid Furniture & Fixtures cost"
                                    Display="None" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtfurniture" runat="server" TabIndex="20" MaxLength="17"
                                    CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Office Equipments <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ControlToValidate="txtofcequip"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Office Equipments cost"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                                    ControlToValidate="txtofcequip" ErrorMessage="Please Enter Valid Office Equipments cost"
                                    Display="None" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtofcequip" runat="server" TabIndex="18" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div runat="server" id="backup1" class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Service Tax<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtservicetax"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Service Tax "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                ControlToValidate="txtservicetax" ErrorMessage="Please Enter Valid Service Tax"
                                Display="None" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtservicetax" runat="server" CssClass="form-control" MaxLength="15" TabIndex="21"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Property Tax<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ControlToValidate="txtproptax"
                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Property Tax "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                ControlToValidate="txtproptax" ErrorMessage="Please Enter Valid Property Tax"
                                Display="None" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtproptax" runat="server" CssClass="form-control" MaxLength="15" TabIndex="22"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <div class="form-group">
                    <div class="row">
                        <asp:Button ID="btntotal" runat="server" CssClass="btn btn-primary custom-button-color" Text="Calculate"
                            OnClick="btntotal_Click" TabIndex="19" ValidationGroup="Val1" />
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">
                            One Time cost (Stamp Duty + Registration Charges + Professional Fees + Brokergare Fees)</label>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                            ControlToValidate="txtbasic" ErrorMessage="Please Enter Valid Basic Rent" Display="None"
                            ValidationGroup="val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:TextBox ID="txtbasic" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">
                            Maintenance Cost (Maintenance Charge + DGBackup Charges + Furniture&fixtures + Office Equipments)</label>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                            ControlToValidate="txtmain" ErrorMessage="Please Enter Valid Maintenance Cost"
                            Display="None" ValidationGroup="val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:TextBox ID="txtmain" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">
                            Total Rent (Service Tax + Maintenance Cost + Basic Rent)</label>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server"
                            ControlToValidate="txtmain" ErrorMessage="Please Enter Valid Maintenance Cost"
                            Display="None" ValidationGroup="val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:TextBox ID="txttotalrent" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Effective Date of Agreement<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvsdate" runat="server" ControlToValidate="txtsdate"
                                Display="None" ValidationGroup="val1" ErrorMessage="Please Select Effective Date of Agreement"></asp:RequiredFieldValidator>
                            <div class='input-group date' id='effdate'>
                                <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtsdate" runat="server" CssClass="form-control" TabIndex="25"></asp:TextBox>
                                </div>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Expiry Date of Agreement<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvedate" runat="server" ControlToValidate="txtedate"
                                Display="None" ValidationGroup="val1" ErrorMessage="Please Select Expiry Date of Agreement"></asp:RequiredFieldValidator>
                            <div class='input-group date' id='fromdate'>
                                <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtedate" runat="server" CssClass="form-control" TabIndex="26"> </asp:TextBox>
                                </div>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Rent Revision<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ControlToValidate="txtrentrev"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Rent Revision"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                ControlToValidate="txtrentrev" ErrorMessage="Please Enter Valid Rent Revision"
                                Display="None" ValidationGroup="val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtrentrev" runat="server" CssClass="form-control" MaxLength="15" TabIndex="27"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">No. of Landlords<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvlandlord" runat="server" ControlToValidate="ddlleaseld"
                                Display="None" ValidationGroup="val1" ErrorMessage="Please Select No. of Landlords"
                                InitialValue="--Select--"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlleaseld" runat="server" CssClass="selectpicker" data-live-search="true"
                                TabIndex="28" AutoPostBack="True">
                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Agreement to be signed by POA<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvpoa" runat="server" ControlToValidate="ddlpoa"
                                Display="None" ValidationGroup="val1" ErrorMessage="Please Select Agreement to be signed by POA"
                                InitialValue="--Select--"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlpoa" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="29">
                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Do You wish to Enter Lease Escalation<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvesc" runat="server" ControlToValidate="ddlesc"
                                Display="None" ValidationGroup="val1" InitialValue="--Select--" ErrorMessage="Please Select Lease Escalation"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlesc" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="30"
                                AutoPostBack="True">
                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row" visible="false">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Lease Cost Per</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlMode" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="29">
                                <asp:ListItem>--Select--</asp:ListItem>
                                <asp:ListItem Value="1">Weekly</asp:ListItem>
                                <asp:ListItem Value="2">Monthly</asp:ListItem>
                                <asp:ListItem Value="3">Half-Yearly</asp:ListItem>
                                <asp:ListItem Value="4">Yearly</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Lease Escalation Type</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlesctype" runat="server" CssClass="selectpicker" data-live-search="true"
                                AutoPostBack="True" TabIndex="33">
                                <asp:ListItem Value="PER">Percentage</asp:ListItem>
                                <asp:ListItem Value="FLT">Flat Amount</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="pnlesc1" runat="server">
            <fieldset>
                <legend>Escalation1</legend>
            </fieldset>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <div class='input-group date' id='fd'>
                                    <asp:RequiredFieldValidator ID="rfvedcdate" runat="server" ControlToValidate="txtEscalationDate"
                                        ErrorMessage="Please Enter Escalation Date" Display="None"></asp:RequiredFieldValidator>
                                    <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtEscalationDate" runat="server" TabIndex="34" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fd')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <div class='input-group date' id='txtesctodate22'>
                                    <asp:RequiredFieldValidator ID="rfcesctodate" runat="server" ControlToValidate="txtesctodate1"
                                        ErrorMessage="Please Enter Escalation Date" Display="None"></asp:RequiredFieldValidator>
                                    <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtesctodate1" runat="server" TabIndex="35" CssClass="form-control"
                                            Width="97%"></asp:TextBox>
                                    </div>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('txtesctodate22')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Rent Escalation<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvfirstesc" runat="server" ControlToValidate="txtfirstesc"
                                    Display="None" ErrorMessage="Please Enter Amount for First Escalation"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revfirstescalation" runat="server" ControlToValidate="txtfirstesc"
                                    ErrorMessage="Please Enter valid Amount for First Escalation" Display="None"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtfirstesc" runat="server" TabIndex="36" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="pnlesc2" runat="server">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <div class='input-group date' id='fdd'>
                                    <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtescfromdate2" runat="server" TabIndex="37" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fdd')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <div class='input-group date' id='txtesctodate2c'>
                                    <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtesctodate2" runat="server" TabIndex="38" CssClass="form-control"
                                            Width="97%"></asp:TextBox>
                                    </div>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('txtesctodate2c')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Rent Escalation</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revsecondesc" runat="server" ControlToValidate="txtsecondesc"
                                    ErrorMessage="Please Enter valid Amount for Second Escalation" Display="None"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtsecondesc" runat="server" TabIndex="39" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div runat="server" visible="false">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Tenure of agreement<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <div class='input-group date' id='toa'>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ControlToValidate="txttenure"
                                        Display="None" ValidationGroup="val1" ErrorMessage="Please Enter Tenure of Agreement "></asp:RequiredFieldValidator>
                                    <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txttenure" runat="server" CssClass="form-control" MaxLength="15" TabIndex="40"></asp:TextBox>
                                    </div>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('toa')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Leasehold Improvements<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ControlToValidate="txtimp"
                                ValidationGroup="val1" Display="None" ErrorMessage="Please Enter Leasehold Improvements"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtimp" runat="server" Rows="5" CssClass="form-control" TextMode="Multiline" MaxLength="500" TabIndex="41"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Lease Comments<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfComments" runat="server" ControlToValidate="txtComments"
                                Display="None" ValidationGroup="val1" ErrorMessage="Please Enter Comments"></asp:RequiredFieldValidator>
                            <div onmouseover="Tip('Enter Comments with maximum 750 Characters')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" MaxLength="1000" TabIndex="42"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btn1Next" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="true"
                    Text="Next" TabIndex="43" ValidationGroup="val1" />
            </div>
        </div>
    </div>


    <div id="panwiz3" runat="server">
        <fieldset>
            <legend>Agreement Details</legend>
        </fieldset>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Tentative Execution Date<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <div class='input-group date' id='texdt'>
                                <asp:RequiredFieldValidator ID="rfvagreedate" runat="server" ControlToValidate="txtAgreedate"
                                    Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Agreement Execution Date"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtAgreedate" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('texdt')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Amount of stampDuty paid<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvagreeamt" runat="server" ControlToValidate="txtagreeamt"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Amount of Stamp Duty paid"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revagreeamt" runat="server" ControlToValidate="txtagreeamt"
                                ErrorMessage="Please Enter valid Amount of stampDuty paid" Display="None" ValidationGroup="Val2"
                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtagreeamt" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Amount paid towards Registraion<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvregamt" runat="server" ControlToValidate="txtregamt"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter  Amount paid towards Registraion"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revregamt" runat="server" ControlToValidate="txtregamt"
                                ErrorMessage="Please Enter valid Amount paid towards Registraion" Display="None"
                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtregamt" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Agreement Registered<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvagreeregis" runat="server" ControlToValidate="ddlagreeres"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Select Agreement Registered"
                                InitialValue="--Select--"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlagreeres" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="No" Selected="True">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="trregagree" runat="server">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Date of Registration<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <div class='input-group date' id='doreg'>
                                <asp:RequiredFieldValidator ID="rfvAgreeregdate" runat="server" ControlToValidate="txtagreeregdate"
                                    Display="None" ValidationGroup="Val2" ErrorMessage="Please Select Date of Registration"></asp:RequiredFieldValidator>
                                <div onmouseover="Tip('Please click on the Textbox to select Date')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtagreeregdate" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('doreg')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Sub-Regristrarís office name<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvagreesub" runat="server" ControlToValidate="txtagreesub"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Sub-Regristrarís office name"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtagreesub" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="Tr3" runat="server">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Termination Notice (in days)<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvnotice" runat="server" ControlToValidate="txtnotice"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Termination Notice"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revnotice" runat="server" ControlToValidate="txtnotice"
                                ErrorMessage="Please Enter Termination Notice in No of Days" Display="None" ValidationGroup="Val2"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            <div onmouseover="Tip('Please Enter No of days')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtnotice" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Lock-In Period of Agreement(in months)<span style="color: red;">*</span></label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvlock" runat="server" ControlToValidate="txtlock"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Lock-In Period of Agreement"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revlock" runat="server" ControlToValidate="txtlock"
                                ErrorMessage="Please Enter Lock-In Period of Agreement in No of Months" Display="None"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtlock" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="panbrk" runat="server">        
            <h4>Brokerage Details</h4>       
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Amount of Brokerage Paid</label>
                        <div class="col-md-7">
                            <asp:RegularExpressionValidator
                                ID="revbrkamount" runat="server" ControlToValidate="txtbrkamount" ErrorMessage="Please enter valid Amount of Brokerage Paid"
                                Display="None" ValidationGroup="Val2" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtbrkamount" runat="server" CssClass="form-control" MaxLength="12" TabIndex="24"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Broker Name</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtbrkname" runat="server" CssClass="form-control" TabIndex="25" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Broker Address</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtbrkaddr" runat="server" CssClass="form-control" MaxLength="1000"
                                TextMode="MultiLine" TabIndex="26" Rows="5"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Broker Pan Number</label>
                        <div class="col-md-7">
                            <asp:RegularExpressionValidator ID="regpanbrk" runat="server" ControlToValidate="txtbrkpan"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Broker Pan number in Alphanumerics only"
                                ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="regExTextBox1" runat="server" ControlToValidate="txtbrkpan"
                                Display="None" ValidationGroup="Val2" ErrorMessage="Broker Pan card Minimum length is 10"
                                ValidationExpression=".{10}.*" />
                            <asp:TextBox ID="txtbrkpan" runat="server" TabIndex="27" CssClass="form-control" MaxLength="10"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="Tr5" runat="server">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Broker Email</label>
                        <div class="col-md-7">
                            <asp:RegularExpressionValidator ID="revbrkremail" runat="server" ControlToValidate="txtbrkremail"
                                ErrorMessage="Please Enter valid Email" Display="None" ValidationGroup="Val2"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtbrkremail" runat="server" CssClass="form-control" MaxLength="50" TabIndex="28"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Contact Details</label>
                        <div class="col-md-7">
                            <asp:RegularExpressionValidator ID="revbrkmob" runat="server" ControlToValidate="txtbrkmob"
                                ErrorMessage="Please Enter valid Contact Details" Display="None" ValidationGroup="Val2"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                            <asp:TextBox ID="txtbrkmob" runat="server" CssClass="form-control" TabIndex="29"
                                Width="97%" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="panrecwiz" runat="server">
        <fieldset>
            <legend>Recovery Details Before Escalation</legend>
        </fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Employee's Account Number</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtEmpAccNo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Employee's Bank Name</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtEmpBankName" runat="server" CssClass="form-control"
                                MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Employee's Bank Branch Name</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtEmpBranch" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery Amount</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtEmpRcryAmt" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery From Date</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcryfromdate" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery To Date</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcrytodate" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="panafteresc1" runat="server">
        <fieldset>
            <legend>First Escalation</legend>
        </fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery Amount</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcramt1" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery From Date</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcrfrmdate1" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery To Date</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcrtodate1" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="panafteresc2" runat="server">
        <fieldset>
            <legend>Second Escalation</legend>
        </fieldset>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery Amount (Per Month)</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcramt2" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery From Date</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcrfrmdate2" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Recovery To Date</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtrcrtodate2" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btn2prev" runat="server" Text="Previous" TabIndex="30" CausesValidation="false" CssClass="btn btn-primary custom-button-color" />
                <asp:Button ID="btn2Next" runat="server" Text="Next" CausesValidation="true" TabIndex="31" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color" />
            </div>
        </div>
    </div>


    <div id="landlord" runat="server">

        <div id="panPOA" runat="Server">            
                <h4>Power of Attorney Details</h4>
            

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Name<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvPOAName" runat="server" ControlToValidate="txtPOAName"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Name of Power of Attorney"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtPOAName" runat="server" CssClass="form-control" TabIndex="32" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvPOAAddress" runat="server" ControlToValidate="txtPOAAddress"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Address of Power of Attorney"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtPOAAddress" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" Rows="4" TabIndex="33"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Contact Details<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Contact Details of Power of Attorney"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                    ErrorMessage="Please enter valid Contact Details of Power of Attorney" Display="None"
                                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtPOAMobile" runat="server" CssClass="form-control" MaxLength="12" TabIndex="34"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Email-ID</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revPOAEmail" runat="server" ControlToValidate="txtPOAEmail"
                                    ErrorMessage="Please Enter valid Email of Power of Attorney" Display="None" ValidationGroup="Val3"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtPOAEmail" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="35"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div id="panld1" runat="server">
            <fieldset>
                <legend>Landlord1 Details</legend>
            </fieldset>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Name<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtldname"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 name"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtldname" runat="server" TabIndex="36" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 1<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvldaddr" runat="server" ControlToValidate="txtldaddr"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 Address1"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtldaddr" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine" TabIndex="37" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 2</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtld1addr2" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine" TabIndex="38" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 3</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtld1addr3" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine" TabIndex="38" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">State<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlstate"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Select Landlord1 State"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlstate" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="39">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlld1city"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 City"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="ddlld1city" runat="server" CssClass="form-control" TabIndex="40"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">PIN CODE<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld1pin" runat="server" ControlToValidate="txtld1Pin"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 Pin number"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtld1Pin"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 Pin number in numerics only"
                                    ValidationExpression="^[0-9 ]*"></asp:RegularExpressionValidator>
                                <div onmouseover="Tip('Enter PIN No with maximum length 10')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtld1Pin" runat="server" CssClass="form-control" TabIndex="41"
                                        MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">PAN No <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="revpan" runat="server" ControlToValidate="txtPAN"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 PAN No!"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="REVl1pan" runat="server" ControlToValidate="txtPAN"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 Pan number in Alphanumerics only"
                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                    ControlToValidate="txtPAN" Display="None" ValidationGroup="Val3" ErrorMessage="Landlord1 Pan card Minimum length should be 10"
                                    ValidationExpression=".{10}.*" />
                                <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtPAN" runat="server" CssClass="form-control" TabIndex="42"
                                        MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Email</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revldemail" runat="server" ControlToValidate="txtldemail"
                                    ErrorMessage="Please Enter valid Email of Landlord1" Display="None" ValidationGroup="Val3"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtldemail" runat="server" CssClass="form-control" TabIndex="43" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Contact Details <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvmob" runat="server" ControlToValidate="txtmob"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord1 Contact Details"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server" ControlToValidate="txtmob"
                                    ErrorMessage="Please Enter valid Contact Details of Landlord1" Display="None"
                                    ValidationGroup="Val3" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtmob" runat="server" CssClass="form-control" TabIndex="44" MaxLength="12"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Monthly Rent Payable <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvMrent" runat="server" ControlToValidate="txtpmonthrent"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter  Monthly Rent Payable"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revmrent" runat="server" ControlToValidate="txtpmonthrent"
                                    ErrorMessage="Please Enter Valid Monthly Rent Payable" Display="None" ValidationGroup="Val3"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtpmonthrent" runat="server" CssClass="form-control" MaxLength="15" TabIndex="45"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Security Deposit <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvpsec" runat="server" ControlToValidate="txtpsecdep"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Security Deposit"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revpsec" runat="server" ControlToValidate="txtpsecdep"
                                    ErrorMessage="Please enter Valid Security Deposit" Display="None" ValidationGroup="Val3"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtpsecdep" runat="server" TabIndex="46" CssClass="form-control" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvPFromdate" runat="server" ControlToValidate="txtpfromdate"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter From Date"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtpfromdate" runat="Server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvptodate" runat="server" ControlToValidate="txtptodate"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter To Date"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtptodate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Select Payment Mode <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="cvmode" runat="server" ControlToValidate="ddlpaymentmode"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Select Payment Mode"
                                    InitialValue="--Select PaymentMode--"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlpaymentmode" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" TabIndex="47">
                                    <asp:ListItem Value="--Select PaymentMode--">--Select PaymentMode--</asp:ListItem>
                                    <asp:ListItem Value="1">DD</asp:ListItem>
                                    <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                    <asp:ListItem Value="3">NEFT / RTGS</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label"></label>
                            <div class="col-md-7">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="panel1" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revBankName" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtBankName" ErrorMessage="Enter Valid Bank Name"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtBankName" runat="server" TabIndex="48" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number<span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revAccno" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtAccNo" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtAccNo" runat="server" TabIndex="49" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="panelL12" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtL12Accno"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtL12Accno" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtL12Accno" runat="server" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="panel2" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtIBankName" runat="server" TabIndex="51" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revDeposited" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtDeposited" ErrorMessage="Enter Valid Bank Name"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtDeposited" runat="server" TabIndex="52" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Branch Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvBrnch" runat="server" ControlToValidate="txtbrnch"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revbrnch" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtbrnch" ErrorMessage="Enter Valid Branch Name"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtbrnch" runat="server" TabIndex="53" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">IFSC Code <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvIFsc" runat="server" ControlToValidate="txtIFSC"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter IFSC Code"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="REVIFsc" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtIFSC" ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtIFSC" runat="server" TabIndex="54" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <asp:Button ID="btnAddlandlord1" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add Landlord1"
                            Visible="false" />
                    </div>
                </div>
            </div>
        </div>

        <div id="pnlld2" runat="server">
            <fieldset>
                <legend>Landlord2 Details</legend>
            </fieldset>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Name<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld2name" runat="server" ControlToValidate="txtld1name"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 name"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld1name" runat="server" TabIndex="55" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 1<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld2address" runat="server" ControlToValidate="txtld2addr"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 Address"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld2addr" runat="server" TabIndex="56" CssClass="form-control" MaxLength="500" TextMode="MultiLine" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 2<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtld2addr2" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine" TabIndex="57" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 3<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtld2addr3" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine" TabIndex="58" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">State<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlld2state"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Select Landlord2 State"
                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlld2state" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" TabIndex="59"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlld2city"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Select Landlord2 City"
                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="ddlld2city" runat="server" CssClass="form-control" TabIndex="60"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">PIN CODE<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtld2Pin"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 PIN No"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtld2Pin"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 Pin number in numerics only"
                                    ValidationExpression="^[0-9 ]*"></asp:RegularExpressionValidator>
                                <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtld2Pin" runat="server" CssClass="form-control" TabIndex="61"
                                        MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">PAN No <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld2pan" runat="server" ControlToValidate="txtld2pan"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 PAN"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revld2pan" runat="server" ControlToValidate="txtld2pan"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 Pan number in Alphanumerics only"
                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtld2pan"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Landlord2 Pan card Minimum length should be 10"
                                    ValidationExpression=".{10}.*" />
                                <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtld2pan" runat="server" TabIndex="62" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Email</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revld2email" runat="server" ControlToValidate="txtld2email"
                                    ErrorMessage="Please Enter valid  Email of Landlord2" Display="None" ValidationGroup="Val3"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld2email" runat="server" TabIndex="63" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Contact Details <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld2mobile" runat="server" ControlToValidate="txtld2mob"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 Contact Details"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revld2mobile" runat="server" ControlToValidate="txtld2mob"
                                    ErrorMessage="Please Enter valid Landlord2 Contact Details" Display="None" ValidationGroup="Val3"
                                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld2mob" runat="server" TabIndex="64" CssClass="form-control" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld2fdate" runat="server" ControlToValidate="txtld2frmdate"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 From Date"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld2frmdate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld2todate" runat="server" ControlToValidate="txtld2todate"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord2 To Date"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld2todate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Monthly Rent Payable</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revld2rent" runat="server" ControlToValidate="txtld2rent"
                                    ErrorMessage="Please Enter Valid Monthly Rent Payable of Landlord2" Display="None"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld2rent" runat="server" CssClass="form-control" TabIndex="65"
                                    Width="97%" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Security Deposit</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revld2sd" runat="server" ControlToValidate="txtld2sd"
                                    ErrorMessage="Please enter Valid Security Deposit of landlord2" Display="None"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld2sd" runat="server" TabIndex="66" CssClass="form-control" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Select Payment Mode <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld2paymode" runat="server" ControlToValidate="ddlld2mode"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Select Payment Mode of Landlord2"
                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlld2mode" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" TabIndex="67">
                                    <asp:ListItem Value="--Select--">--Select PaymentMode--</asp:ListItem>
                                    <asp:ListItem Value="1">DD</asp:ListItem>
                                    <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                    <asp:ListItem Value="3">NEFT / RTGS</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-8 control-label"></label>
                            <div class="col-md-12">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="panel3" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld2bankname" runat="server" ControlToValidate="txtld2bankname"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revld2bankname" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld2bankname" ErrorMessage="Enter Valid Bank Name"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld2bankname" runat="server" TabIndex="68" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <font class="clsNote">*</font></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld2accno" runat="server" ControlToValidate="txtld2accno"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revld2accno" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld2accno" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld2accno" runat="server" TabIndex="69" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="pnll22" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtl22accno"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtl22accno" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtl22accno" runat="server" TabIndex="70" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="panel4" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvl2accnumber" runat="server" ControlToValidate="txtld2IBankName"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revl2accnumber" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld2IBankName" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld2IBankName" runat="server" TabIndex="71" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld2Deposited" runat="server" ControlToValidate="txtld2Deposited"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Deposited Bank Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revld2Deposited" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld2Deposited" ErrorMessage="Enter Valid Deposited Bank "
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld2Deposited" runat="server" TabIndex="72" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Branch Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvl2brnch" runat="server" ControlToValidate="txtl2brnchname"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revl2brnch" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtl2brnchname" ErrorMessage="Enter Valid Branch Name"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtl2brnchname" runat="server" TabIndex="73" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">IFSC Code <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld2IFsc" runat="server" ControlToValidate="txtld2IFSC"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter IFSC Code"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="REVld2IFsc" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld2IFSC" ErrorMessage="Enter Valid IFSC"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld2IFSC" runat="server" TabIndex="74" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <asp:Button ID="btnAddLandLord2" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add Landlord2" Visible="false" />
                    </div>
                </div>
            </div>
        </div>


        <div id="pnlld3" runat="server">
            <fieldset>
                <legend>Recovery Landlord3 Details</legend>
            </fieldset>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Name<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld3name" runat="server" ControlToValidate="txtld3name"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 Name"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld3name" runat="server" TabIndex="75" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 1<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld3address" runat="server" ControlToValidate="txtld3addr"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 Address"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld3addr" runat="server" TabIndex="76" CssClass="form-control" MaxLength="500" TextMode="MultiLine" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 2</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtld3addr2" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine" TabIndex="57" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Address 3</label>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtld3addr3" runat="server" CssClass="form-control" MaxLength="500" TextMode="MultiLine" TabIndex="78" Rows="5"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">State<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="ddlld3state"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Select Landlord3 State"
                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlld3state" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" TabIndex="79">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="ddlld3city"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 City"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="ddlld3city" runat="server" CssClass="form-control" TabIndex="80"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">PIN CODE<span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtld3Pin"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 PIN No"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtld3Pin"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 Pin number in numerics only"
                                    ValidationExpression="^[0-9 ]*"></asp:RegularExpressionValidator>
                                <div onmouseover="Tip('Enter PIN No with maximum length 10')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtld3Pin" runat="server" CssClass="form-control" TabIndex="81" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">PAN No <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld3pan" runat="server" ControlToValidate="txtld3pan"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 PAN"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revld3pan" runat="server" ControlToValidate="txtld3pan"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 Pan number in Alphanumerics only"
                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtld3pan"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Landlord3 Pan card Minimum length should be 10"
                                    ValidationExpression=".{10}.*" />
                                <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                    <asp:TextBox ID="txtld3pan" runat="server" CssClass="form-control" TabIndex="82" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Email</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revld3email" runat="server" ControlToValidate="txtld3email"
                                    ErrorMessage="Please Enter valid  Email of Landlord3" Display="None" ValidationGroup="Val3"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld3email" runat="server" CssClass="form-control" MaxLength="50" TabIndex="83"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Contact Details <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld3mobile" runat="server" ControlToValidate="txtld3mob"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 Contact Details"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revld3mob" runat="server" ControlToValidate="txtld3mob"
                                    ErrorMessage="Please Enter valid Contact Details of Landlord3" Display="None"
                                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld3mob" runat="server" CssClass="form-control" TabIndex="84" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" runat="server" visible="False">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">From Date</label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld3fdate" runat="server" ControlToValidate="txtld3fromdate"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 From Date"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld3fromdate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">To Date</label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvtdate" runat="server" ControlToValidate="txtld3todate"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Landlord3 To Date"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtld3todate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Monthly Rent Payable</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revld3rent" runat="server" ControlToValidate="txtld3rent"
                                    ErrorMessage="Please Enter Valid Monthly Rent Payable of Landlord3" Display="None"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld3rent" runat="server" TabIndex="85" CssClass="form-control" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Security Deposit</label>
                            <div class="col-md-7">
                                <asp:RegularExpressionValidator ID="revld3sd" runat="server" ControlToValidate="txtld3sd"
                                    ErrorMessage="Please enter Valid Security Deposit of landlord3" Display="None"
                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtld3sd" runat="server" CssClass="form-control" TabIndex="86" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Select Payment Mode <span style="color: red;">*</span></label>
                            <div class="col-md-7">
                                <asp:RequiredFieldValidator ID="rfvld3paymode" runat="server" ControlToValidate="ddlld3mode"
                                    Display="None" ValidationGroup="Val3" ErrorMessage="Please Select Payment Mode for Landlord3"
                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlld3mode" runat="server" TabIndex="87" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                    <asp:ListItem Value="--Select--">--Select PaymentMode--</asp:ListItem>
                                    <asp:ListItem Value="1">DD</asp:ListItem>
                                    <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                    <asp:ListItem Value="3"> NEFT / RTGS </asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label"></label>
                            <div class="col-md-7">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="panel5" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld3bank" runat="server" ControlToValidate="txtld3bankname"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator25" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld3bankname" ErrorMessage="Enter Valid Bank Name"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld3bankname" runat="server" CssClass="form-control" MaxLength="50" TabIndex="88"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <font class="clsNote">*</font></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld3acc" runat="server" ControlToValidate="txtLd3acc"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number "></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator27" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtLd3acc" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtLd3acc" runat="server" TabIndex="89" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="pnll32" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtl32accno"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtl32accno" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtl32accno" runat="server" TabIndex="90" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="panel6" runat="server">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Account Number <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld3ibankname" runat="server" ControlToValidate="txtld3IBankName"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revld3ibankname" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld3IBankName" ErrorMessage="Enter Valid Account Number"
                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld3IBankName" runat="server" TabIndex="91" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Bank Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld3deposited" runat="server" ControlToValidate="txtld3Deposited"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revld3deposited" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld3Deposited" ErrorMessage="Enter Valid Bank Name "
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtld3Deposited" runat="server" TabIndex="92" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Branch Name <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvl3brnch" runat="server" ControlToValidate="txtl3brnch"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revl3brnch" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtl3brnch" ErrorMessage="Enter Valid Branch Name"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtl3brnch" runat="server" TabIndex="93" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">IFSC Code <span style="color: red;">*</span></label>
                                <div class="col-md-7">
                                    <asp:RequiredFieldValidator ID="rfvld3ifsc" runat="server" ControlToValidate="txtld3ifsc"
                                        Display="None" ValidationGroup="Val3" ErrorMessage="Please Enter IFSC Code"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revld3ifsc" Display="None" ValidationGroup="Val3"
                                        runat="server" ControlToValidate="txtld3ifsc" ErrorMessage="Enter Valid IFSC"
                                        ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                        onmouseout="UnTip()">
                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                            onmouseout="UnTip()">
                                            <asp:TextBox ID="txtld3ifsc" runat="server" TabIndex="94" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">

                        <asp:Button ID="btnAddlandlord3" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add LandLord3"
                            Visible="false" />

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btn3Prev" runat="server" CssClass="btn btn-primary custom-button-color" TabIndex="95" Text="Previous" CausesValidation="false" />
                <asp:Button ID="btn3Finish" runat="server" CssClass="btn btn-primary custom-button-color" TabIndex="96" Text="Finish" CausesValidation="true"
                    ValidationGroup="Val3" />
            </div>
        </div>
    </div>
</div>

