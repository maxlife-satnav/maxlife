Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_GenGatePassDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim strStat As String

    Dim FBDG_ID, SBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            strStat = Request.QueryString("stat")
            getDetailsbyReqId(strReqId, strStat)
            'BindRequestAssets(strReqId)
            btnPrint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
        End If
    End Sub


    Public Sub getDetailsbyReqId(ByVal strREQ_id As String, ByVal strstat As String)
        If strstat = "INTRA" Then

            Dim dr As SqlDataReader
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strREQ_id
            dr = ObjSubsonic.GetSubSonicDataReader("GET_INTRA_MVMT_REQID", param)
            If dr.Read Then
                FBDG_ID = dr.Item("SLOC_CODE")
                SBDG_ID = dr.Item("FLOC_CODE")
                'FTower = dr.Item("STWR_NAME")
                'FFloor = dr.Item("SFLR_NAME")
                'TBDG_ID = dr.Item("FLOC_NAME")
                'TTower = dr.Item("DTWR_NAME")
                'TFloor = dr.Item("DFLR_NAME")
                receiveAst = dr.Item("MMR_RECVD_BY")
                strremarks = dr.Item("MMR_COMMENTS")

                lblRaisedBy.Text = dr.Item("MMR_RAISEDBY")
                lblapprovedby.Text = dr.Item("MMR_APPROVED_BY")
                lblreceivedby.Text = dr.Item("RECVD_BY")
            End If
            If dr.IsClosed = False Then
                dr.Close()
            End If
            lblSTower.Text = FBDG_ID
            lblSFloor.Text = SBDG_ID

            'lblDTower.Text = TTower
            'lblDFloor.Text = TFloor
            lblReqId.Text = strREQ_id
            BindRequestAssets(strREQ_id)
        Else

            Dim dr As SqlDataReader
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strREQ_id
            dr = ObjSubsonic.GetSubSonicDataReader("GET_INTER_MVMT_REQID", param)
            If dr.Read Then
                FBDG_ID = dr.Item("MMR_FROMBDG_ID")
                SBDG_ID = dr.Item("MMR_TOBDG_ID")
                'FTower = dr.Item("MMR_FROMBDG_ID")
                'FFloor = dr.Item("MMR_FROMFLR_ID")
                TBDG_ID = dr.Item("MMR_TOBDG_ID")
                'TTower = dr.Item("MMR_TOBDG_ID")
                'TFloor = dr.Item("MMR_TOFLR_ID")
                receiveAst = dr.Item("MMR_RECVD_BY")
                strremarks = dr.Item("MMR_COMMENTS")

                lblRaisedBy.Text = dr.Item("MMR_RAISEDBY")
                lblapprovedby.Text = dr.Item("MMR_APPROVED_BY")
                lblreceivedby.Text = dr.Item("RECVD_BY")
            End If
            If dr.IsClosed = False Then
                dr.Close()
            End If
            lblSTower.Text = FBDG_ID
            lblSFloor.Text = SBDG_ID

            'lblDTower.Text = TTower
            'lblDFloor.Text = TFloor
            lblReqId.Text = strREQ_id
            BindInterRequestAssets(strReqId)
        End If


    End Sub


    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "GET_MVMT_ASTS", param)
    End Sub
    Public Sub BindInterRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "GET_INTER_MVMT_ASTS", param)
    End Sub


    Protected Sub btnGetPass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetPass.Click
        Dim strASSET_LIST As New ArrayList
        If Request.QueryString("stat") = "INTRA" Then

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_GETPASS_approved_by", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_GETPASS_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = ""
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1021
            ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_GETPASS", param)
        Else
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_GETPASS_approved_by", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_GETPASS_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = ""
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1021
            ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_MVMT_getpass", param)
        End If
        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & lblAAT_NAME.Text)
        Next


        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_gatepassgeneration"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)





        'lblMsg.Text = "Request succesfully approved."
        Response.Redirect("frmAssetThanks.aspx?RID=gengatepass")
    End Sub



    Private Sub getRequestDetails(ByVal ReqId As String, ByVal strAst As ArrayList, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        'Dim ReqId As String = GetRequestId()
        'Dim strASSET_LIST As New ArrayList
        'Dim i As Integer = 0
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        '    If chkSelect.Checked Then
        '        strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
        '    End If
        '    i += 1
        'Next
        SendMail(ReqId, strAst, Session("uid"), MailStatus, App_Rej_status)
    End Sub


    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_INTRA_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty


            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = ObjSubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If






            strAstList = "<table cellspacing='2' cellpadding='2' width='100%' border='0'>"
            'strAstList = strAstList & "<tr><td colspan='2' >The following assets are being moved from <b>" & lblSTower.Text & "</b>, <b>" & lblSFloor.Text & "</b> to <b>" & lblDTower.Text & "</b>, <b>" & lblDFloor.Text & "</b></td>"
            strAstList = strAstList & "</tr>"
            strAstList = strAstList & "<tr><td colspan='2' align='center'>"
            'strAstList = strAstList & "<table><tr><td align='left'></td><td align='left'></td></tr></table>"

            strAstList = strAstList & "<table width='200' border='1'><tr><td><strong>Asset Code </strong></td><td><strong>Asset Name</strong></td></tr>"
            For i As Integer = 0 To AstList.Count - 1
                Dim p1
                p1 = AstList.Item(i).Split(",")

                strAstList = strAstList & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            Next
            strAstList = strAstList & "</table> "

            strAstList = strAstList & "</td>"
            strAstList = strAstList & "</tr>"
            strAstList = strAstList & "<tr><td align='left' colspan='2'><br /><br />The Request <b>" & lblReqId.Text & "</b>"
            strAstList = strAstList & "has been raised by <b>" & lblRaisedBy.Text & "</b>, approved by <b>" & lblapprovedby.Text & "</b>"
            strAstList = strAstList & "<br /><hr />This is to inform that these assets are now being shifted from this premise and will be accepted by <br /><b>" & lblreceivedby.Text & "</b> at the new premise.</td></tr>"
            strAstList = strAstList & "</table>"


            'strAstList = "<table width='100%' border='1'>"
            'strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td></tr>"
            'strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            'strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            'strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(3) & "</td></tr>"


            'Dim strApproveList As String = String.Empty
            'Dim strRejectList As String = String.Empty

            'Dim AppLink As String = String.Empty
            'AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            'strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&rl=rm&ty=imr> Click to Approve </a></td></tr></table>"

            'strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&rl=rm&ty=imr>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strremarks)
            body = body.Replace("@@RM", strKnownas)



            '1. IT Request Approved copy goes to request raised person.
            '2. One copy for IT ADMIN to approve.

            'If App_Rej_status = True Then
            Insert_AmtMail(body, strRR, strSubject, strRM)
            'Else
            'Insert_AmtMail(body, strRM, strSubject, "")
            'End If






        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "IntraMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = DateTime.Now
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub



End Class
