<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateMaintenanceRequest.ascx.vb"
    Inherits="Controls_UpdateMaintenanceRequest" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Update Maintenance Request
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="85%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Update Maintenance Request</strong></td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Select Maintenance Request<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="cvReqid" runat="server" ControlToValidate="ddlMRequest"
                                        Display="none" ErrorMessage="Please Select Request" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:DropDownList ID="ddlMRequest" runat="server" CssClass="clsComboBox" AutoPostBack="true"
                                        Width="99%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Select WorkStatus<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="cvworkstatus" runat="server" ControlToValidate="ddlwstatus"
                                        Display="none" ErrorMessage="Please Select Work status" ValidationGroup="Val1"
                                        InitialValue="--Select Status--"></asp:RequiredFieldValidator></td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="clsComboBox" Width="99%">
                                        <asp:ListItem>--Select Status--</asp:ListItem>
                                        <asp:ListItem Value="0">Pending</asp:ListItem>
                                        <asp:ListItem Value="1">InProgress</asp:ListItem>
                                        <asp:ListItem Value="2">Completed</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Property
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtProperty" runat="server" CssClass="clsTextField" Width="97%"
                                        Enabled="false"> 
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    City
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtCity" runat="server" CssClass="clsTextField" Width="97%" Enabled="false"> 
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Tenant
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtTenant" runat="server" CssClass="clsTextField" Width="97%" Enabled="false"> 
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Request Type
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtReqType" runat="server" CssClass="clsTextField" Width="97%" Enabled="false"> 
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Select ItemStatus<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvstatus1" runat="server" ControlToValidate="ddlstatus"
                                        Display="none" ErrorMessage="Please Select Item status" ValidationGroup="Val1"
                                        InitialValue="--Select Status--"></asp:RequiredFieldValidator></td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="clsComboBox" Width="99%">
                                        <asp:ListItem>--Select Status--</asp:ListItem>
                                        <asp:ListItem Value="0">Pending</asp:ListItem>
                                        <asp:ListItem Value="1">InProgress</asp:ListItem>
                                        <asp:ListItem Value="2">Completed</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Comments<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvComments" runat="server" ControlToValidate="txtComments"
                                        Display="none" ErrorMessage="Comments Required" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtComments" runat="server" CssClass="clsTextField" Width="97%"
                                        Rows="3" TextMode="MultiLine" MaxLength="250"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Upload Document (if Required )
                                    <asp:RegularExpressionValidator ID="revfubrowse" Display="none" ControlToValidate="fuBrowseFile"
                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt files allowed"
                                        ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT]))$"></asp:RegularExpressionValidator>
                                    <td align="left" style="height: 26px; width: 50%">
                                        <asp:FileUpload ID="fuBrowseFile" runat="Server" Width="97%" />
                                    </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="center">
                                    <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="Val1"
                                        CausesValidation="true" />&nbsp;
                                </td>
                            </tr>
                        </table>
                        <table id="table1" runat="server" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td align="center" style="height: 20px">
                                    <asp:GridView ID="gvMrequest" runat="server" EmptyDataText="There are no data records to display."
                                        Width="100%" AutoGenerateColumns="false" AllowPaging="true">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblid" runat="Server" Text='<%# Eval("PN_DETAIL_ID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Request ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblrid" runat="server" Text='<%# Eval("PN_REQUEST_ID") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblupdated" runat="server" Text='<%# Eval("PN_UPDATED_BY") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldate" runat="server" Text='<%# Eval("PN_UPDATED_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Comments">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomments" runat="server" Text='<%# Eval("PN_COMMENTS") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Document">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblViewFilename" Visible="false" runat="server" Text='<%#Eval("PN_DETAIL_ID")%>'>
                                                    </asp:Label>
                                                    <asp:HyperLink ID="hypbtnDocs" Visible="false" runat="server" Text='View Document'>
                                                    </asp:HyperLink>
                                                    <a target="_blank" href='<%=Request.ApplicationPath %>/UploadFiles/<%#Eval("PN_DOCUMENT")%>'>
                                                        <%#Eval("PN_DOCUMENT")%>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
        <asp:PostBackTrigger ControlID="gvMrequest" />
    </Triggers>
</asp:UpdatePanel>
