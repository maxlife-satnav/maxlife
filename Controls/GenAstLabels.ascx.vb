Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class Controls_GenAstLabels
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ' BindConftype()
            BindAssetCategories()
            BindVendors()
            BindBrands()
            BindLocations()
            BindProCtype()
            pnlItems.Visible = False
        End If
    End Sub
    'Private Sub BindConfType()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CONF_TYPE")
    '    ddlconftype.DataSource = sp.GetDataSet()
    '    ddlconftype.DataTextField = "name"
    '    ddlconftype.DataValueField = "code"
    '    ddlconftype.DataBind()
    '    ddlconftype.Items.Insert(0, New ListItem("--Select--", "0"))
    'End Sub
    Private Sub BindProCtype()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_amg_asset_status_GetByTagStatus")
        ddlProcType.DataSource = sp.GetReader
        ddlProcType.DataTextField = "Tag_Name"
        ddlProcType.DataValueField = "Tag_code"
        ddlProcType.DataBind()
        ddlProcType.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindBrands()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CSK_Store_Manufacturer_GetAll")
        ddlBrand.DataSource = sp.GetReader
        ddlBrand.DataTextField = "Manufacturer"
        ddlBrand.DataValueField = "ManufacturerId"
        ddlBrand.DataBind()
        ddlBrand.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindAssetCategories()
        GetChildRows("0")
        ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindVendors()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_VENDOR_GETALL")
        ddlVendor.DataSource = sp.GetReader
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub GetChildRows(ByVal i As String)
        Dim str As String = ""
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If
        Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id & " Order by CategoryName ", objConn)
        Dim ds As New DataSet
        da.Fill(ds)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim j As Integer = CType(dr("CategoryId"), Integer)
                If id = 0 Then
                    str = ""
                End If
                Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
                ddlAstCat.Items.Add(li)
                GetChildRows(str & "~" & j)
            Next
        End If
    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        If ddlAstCat.SelectedIndex > 0 Then
            Dim CatId As Integer = 0
            Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
            BindAssetsByCategory(CatId)
        Else
            CLEAR()
        End If
    End Sub
    Private Sub CLEAR()
        pnlItems.Visible = False
        ddlAsset.Items.Clear()
        ddlAsset.Items.Insert(0, New ListItem("--Select--", "0"))
        ddlAsset.SelectedValue = 0
        ddlVendor.SelectedIndex = 0
        ddlProcType.SelectedIndex = 0
        ddlBrand.SelectedIndex = 0
        txtModel.Text = ""
        txtNoOfAssetIds.Text = ""
        ddlLocation.SelectedIndex = 0
        'ddlTower.Items.Clear()
        'ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
        'ddlTower.SelectedIndex = 0
        'ddlFloor.Items.Clear()
        'ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
        'ddlFloor.SelectedIndex = 0
        txtRemarks.Text = ""
    End Sub
    Private Sub BindAssetsByCategory(ByVal CatId As Integer)
        ddlAsset.DataSource = ProductController.GetByCategoryID(CatId)
        ddlAsset.DataValueField = "ProductId"
        ddlAsset.DataTextField = "ProductName"
        ddlAsset.DataBind()
        ddlAsset.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Protected Sub ddlAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAsset.SelectedIndexChanged
        If ddlAsset.SelectedIndex > 0 Then
            Dim ProductId As Integer = 0
            Integer.TryParse(ddlAsset.SelectedItem.Value, ProductId)
            BindPos(ProductId)
            pnlItems.Visible = True
        Else
            pnlItems.Visible = False
        End If

    End Sub
    Private Sub BindPos(ByVal ProductId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEMPO_DETAILS_GetByAsset")
        sp.Command.AddParameter("@AssetId", ProductId, DbType.Int32)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub

    'Private Sub BindTowersByLocation(ByVal LocCode As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Tower_GetByLocation")
    '    sp.Command.AddParameter("@LocId", Trim(LocCode), DbType.String)
    '    ddlTower.DataSource = sp.GetReader
    '    ddlTower.DataTextField = "TWR_NAME"
    '    ddlTower.DataValueField = "TWR_CODE"
    '    ddlTower.DataBind()
    '    ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
    'End Sub
    'Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
    '    If ddlLocation.SelectedIndex > 0 Then
    '        Dim LocCode As String = ddlLocation.SelectedItem.Value
    '        'Integer.Parse(ddlLocation.SelectedItem.Value, LocId)
    '        BindTowersByLocation(LocCode)
    '    Else
    '        ddlTower.Items.Clear()
    '        ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '        ddlTower.SelectedIndex = 0
    '        ddlFloor.Items.Clear()
    '        ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '        ddlFloor.SelectedIndex = 0
    '    End If
    'End Sub
   
    'Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
    '    If ddlTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlTower.SelectedItem.Value
    '        Dim loccode As String = ddlLocation.SelectedItem.Value
    '        'Integer.Parse(ddlTower.SelectedItem.Value, FlrId)
    '        BindFloorsByTower(TwrCode, loccode)
    '    Else
    '        ddlFloor.Items.Clear()
    '        ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '        ddlFloor.SelectedIndex = 0
    '    End If
    'End Sub
    'Private Sub BindFloorsByTower(ByVal TwrCode As String, ByVal loccode As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Floor_GetByTowerLOC")
    '    sp.Command.AddParameter("@TwrCode", Trim(TwrCode), DbType.String)
    '    sp.Command.AddParameter("@loc", Trim(loccode), DbType.String)
    '    ddlFloor.DataSource = sp.GetReader
    '    ddlFloor.DataTextField = "FLR_NAME"
    '    ddlFloor.DataValueField = "FLR_CODE"
    '    ddlFloor.DataBind()
    '    ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
    'End Sub
    Private Function GetMaxAssetId() As Integer
        Dim AstId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ASSET_GetMaxAAT_ID")
        If Integer.TryParse(sp.ExecuteScalar, AstId) Then
        Else
            AstId = 0
        End If
        Return AstId
    End Function
    'Private Function GenerateAssetCode(ByVal sku As String, ByVal Location As String) As String
    '    Dim AstCode As String = Location + "/" + sku + "/" + CStr(GetMaxAssetId())
    '    Return AstCode
    'End Function
    Private Function GenerateAssetCode(ByVal sku As String, ByVal Location As String) As String
        Dim AstCode As String = Location + "/" + ddlAstCat.SelectedItem.Text + "/" + sku + "/" + ddlBrand.SelectedItem.Value + "/" + Trim(txtModel.Text) + "/" + CStr(GetMaxAssetId())
        'DC/IT/TEL/PAN/A123
        Return AstCode
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim AstCode As String = ""
        Dim AstName As String = ""
        Dim sku As String = ""
        Dim BrandId As Integer = 0
        Dim ProductId As Integer = 0
        Dim VendorCode As String = ""
        Dim Vendor As String = ""
        Dim Brand As String = ""
        Dim Model As String = ""
        Dim Location As String = ""
        Dim Tower As String = ""
        Dim Floor As String = ""
        Dim ProcType As Integer = 0

        'If gvItems.Rows.Count > 0 Then
        '    Dim lblsku As Label = DirectCast(gvItems.Rows(0).FindControl("lblsku"), Label)
        '    sku = lblsku.Text
        'End If

        If ddlAsset.SelectedIndex > 0 Then
            Integer.TryParse(ddlAsset.SelectedItem.Value, ProductId)
            If ProductId > 0 Then
                Dim prd As Commerce.Common.Product = ProductController.GetProduct(ProductId)
                If Not prd Is Nothing Then
                    sku = prd.Sku
                End If
            End If

            AstName = ddlAsset.SelectedItem.Text
        Else
            lblMsg.Text = "Please select an asset."
        End If

        If ddlProcType.SelectedIndex > 0 Then
            Integer.TryParse(ddlProcType.SelectedItem.Value, ProcType)
        Else
            lblMsg.Text = "Please select a procurement type."
        End If

        If ddlBrand.SelectedIndex > 0 Then
            Integer.TryParse(ddlBrand.SelectedItem.Value, BrandId)
            Brand = ddlBrand.SelectedItem.Text
        Else
            lblMsg.Text = "Please select a brand."
        End If

        If ddlVendor.SelectedIndex > 0 Then
            VendorCode = ddlVendor.SelectedItem.Value
            Vendor = ddlVendor.SelectedItem.Text
        Else
            lblMsg.Text = "Please select a vendor."
        End If

        If ddlLocation.SelectedIndex > 0 Then
            Location = ddlLocation.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a location."
        End If

        'If ddlTower.SelectedIndex > 0 Then
        '    Tower = ddlTower.SelectedItem.Value
        'Else
        '    lblMsg.Text = "Please select a tower."
        'End If

        'If ddlFloor.SelectedIndex > 0 Then
        '    Floor = ddlFloor.SelectedItem.Value
        'Else
        '    lblMsg.Text = "Please select a floor."
        'End If

        Dim AssetGeneratedCount As Integer = 0
        If Integer.TryParse(txtNoOfAssetIds.Text, AssetGeneratedCount) Then
        Else
            AssetGeneratedCount = 0
            lblMsg.Text = "Invalid value entered for No. Of Asset Id to be generated."
        End If

        Model = Trim(txtModel.Text)




        While AssetGeneratedCount > 0
            AstCode = GenerateAssetCode(sku, Location)
            InsertAsset(AstCode, AstName, ProductId, VendorCode, CStr(BrandId), Model)
            InsertAssetSpace(AstCode, Tower, Floor, ProcType)
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim chkSelect As CheckBox = CType(gvItems.Rows(i).FindControl("chkSelect"), CheckBox)
                Dim lblPO As Label = CType(gvItems.Rows(i).FindControl("lblPO"), Label)
                If chkSelect.Checked = True Then
                    UpdatePOStatus(lblPO.Text, ProductId)
                End If
            Next



            AssetGeneratedCount -= 1
        End While
        pnlall.visible = False
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASTLOCTWRFLR_SPACE")
        sp3.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)

        GridView1.DataSource = sp3.GetDataSet()
        GridView1.DataBind()
        btnExport1.Visible = True
        'Response.Redirect("frmAssetThanks.aspx?RID=AssetLabels")
    End Sub

    Private Sub UpdatePOStatus(ByVal PoNumber As String, ByVal Productid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_UPDATESTATUSORDER_STATUS")
        sp.Command.AddParameter("@AIP_PO_ID", PoNumber, DbType.String)
        sp.Command.AddParameter("@AIP_PRDID", Productid, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertAsset(ByVal AstCode As String, ByVal AstName As String, ByVal ProductId As Integer, ByVal Vendor As String, ByVal Brand As String, ByVal Model As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ASSET_Insert")
        sp.Command.AddParameter("@AAT_CODE", AstCode, DbType.String)
        sp.Command.AddParameter("@AAT_NAME", AstName, DbType.String)
        sp.Command.AddParameter("@AAT_MODEL_NAME", Model, DbType.String)
        sp.Command.AddParameter("@AAT_AAG_CODE", ProductId, DbType.Int32)
        sp.Command.AddParameter("@AAT_AVR_CODE", Vendor, DbType.String)
        sp.Command.AddParameter("@AAT_AAB_CODE", Brand, DbType.String)
        sp.Command.AddParameter("@AAT_AMC_REQD", 0, DbType.Int32)
        sp.Command.AddParameter("@AAT_UPT_DT", getoffsetdatetime(DateTime.Now), DbType.DateTime)
        sp.Command.AddParameter("@AAT_UPT_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@AAT_STA_ID", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_OWNED", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_PURCHASED_STATUS", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_SPC_FIXED", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_USR_MOVABLE", 0, DbType.Int32)
        sp.Command.AddParameter("@AAT_AST_CONS", 0, DbType.Int32)
        sp.Command.AddParameter("@AAT_DESC", "", DbType.String)
        sp.Command.AddParameter("@CONF_TYPE", "", DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertAssetSpace(ByVal AstCode As String, ByVal Tower As String, ByVal Floor As String, ByVal ProcType As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_ASSET_SPACE_INSERT")
        sp.Command.AddParameter("@AAS_AAT_CODE", AstCode, DbType.String)
        sp.Command.AddParameter("@AAS_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAS_FLR_ID", "", DbType.String)
        sp.Command.AddParameter("@AAS_BDG_ID", "", DbType.String)
        'sp.Command.AddParameter("@AAS_FLR_ID", Floor, DbType.String)
        sp.Command.AddParameter("@AAS_PROC_TYPE", ProcType, DbType.String)
        sp.Command.AddParameter("@AAS_REMARKS", Trim(txtRemarks.Text), DbType.String)
        sp.ExecuteScalar()
    End Sub
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub btnExport1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport1.Click
        Export("AssetLables.xls", GridView1)
    End Sub
End Class
