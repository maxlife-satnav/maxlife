﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HDMViewReqisitions.ascx.vb"
    Inherits="Controls_HDMViewReqisitions" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Search by Requisition ID <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                    Display="none" ErrorMessage="Please Enter Requisition Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row col-md-6">
        <div class="col-md-12">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                CausesValidation="true" TabIndex="2" />
        </div>
    </div>
</div>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
            EmptyDataText="No Service Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:BoundField DataField="REQUISITION_ID" HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" />
                <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Date" ItemStyle-HorizontalAlign="left" />
                <asp:BoundField DataField="REQ_STATUS" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("REQUISITION_ID", "~/HDM/HDM_Webfiles/frmHDMRequisitionDetails.aspx?RID={0}")%>'
                            Text="View Details"></asp:HyperLink>
                        <asp:HyperLink ID="hLinkFeedBack" runat="server" NavigateUrl='<%#Eval("REQUISITION_ID", "~/HDM/HDM_Webfiles/frmHDMFeedback.aspx?RID={0}")%>'
                            Text="Give Feedback"></asp:HyperLink>
                        <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("SER_STATUS") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>

