Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_frmAGNewRecord
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim AAB_CODE As String = Request.QueryString("code")
        Dim AAB_UPT_BY As String = Session("Uid")
        Dim AAB_UPT_DT As Date = getoffsetdatetime(DateTime.Now).ToString()
        Dim AAB_REM As String = txtRemarks.Text
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDT_AMG_BRAND")
            sp1.Command.AddParameter("@AAB_CODE", AAB_CODE, DbType.String)
            sp1.Command.AddParameter("@AAB_NAME", txtName.Text, DbType.String)
            sp1.Command.AddParameter("@AAB_STA_ID", rdbtnBrandID.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AAB_UPT_BY", AAB_UPT_BY, DbType.String)
            sp1.Command.AddParameter("@AAB_UPT_DT", AAB_UPT_DT, DbType.Date)
            sp1.Command.AddParameter("@AAB_REM", AAB_REM, DbType.String)
            sp1.ExecuteScalar()
            lblMsg.Text = "Data Modified Succesfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    
    
    Public Sub Cleardata()
        'ddlUser.SelectedItem.Value = 0
        txtRemarks.Text = ""
		txtName.Text=""
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim AAB_CODE As String = Request.QueryString("code")
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSETBRAND_DETAILS")
            sp3.Command.AddParameter("@AAB_CODE", AAB_CODE, DbType.String)
            Dim DS3 As New DataSet
            DS3 = sp3.GetDataSet
            If DS3.Tables(0).Rows.Count > 0 Then
                txtName.Text = DS3.Tables(0).Rows(0).Item("AAB_NAME")
                Dim rdbtnstaid As Integer = DS3.Tables(0).Rows(0).Item("AAB_STA_ID")
                If rdbtnstaid = 0 Then
                    rdbtnBrandID.SelectedValue = "0"
                    'rdbtnBrandID.SelectedItem.Value = "0"
                Else
                    rdbtnBrandID.SelectedValue = "1"
                    'rdbtnBrandID.SelectedItem.Value = "1"
                End If

                'ddlUser.SelectedItem.Text = DS3.Tables(0).Rows(0).Item("AAB_UPT_BY")
                txtRemarks.Text = DS3.Tables(0).Rows(0).Item("AAB_REM")
            End If


        End If

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAMGBrandGetDetails.aspx")
    End Sub
End Class
