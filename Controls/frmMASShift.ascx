﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="frmMASShift.ascx.vb" Inherits="Controls_frmMASShift" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6">
        <label class="col-md-2 btn btn-default pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Shift Master and Select Modify to modify the existing Shift Master" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="btn btn-default" style="margin-left: 25px">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Shift Master and Select Modify to modify the existing Shift Master" />
            Modify
        </label>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Country<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvcountry" runat="server" ControlToValidate="ddlCountry"
                    Display="none" ErrorMessage="Please Select Country" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlLocation"
                    Display="none" ErrorMessage="Please Select Location" InitialValue="" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row" id="trshift" runat="server">
                <label class="col-md-5 control-label">Shift<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvshift" runat="server" ControlToValidate="ddlBrand"
                    Display="none" ErrorMessage="Please Select Shift" InitialValue="" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlBrand"
                    ErrorMessage="Please Select Shift" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Shift Code <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtBrand"
                    Display="none" ErrorMessage="Please Enter Shift  Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtBrand"
                    ErrorMessage="Please enter  Shift Code" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Shift Name <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBrandName"
                    Display="none" ErrorMessage="Please Enter Shift Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtBrandName"
                    ErrorMessage="Please Enter Shift Name" Display="None" ValidationExpression="^[-_0-9a-zA-Z ()-:]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">

                        <asp:TextBox ID="txtBrandName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr"
                    Display="None" ErrorMessage="Please Select From Time " InitialValue="HH" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimemin"
                    Display="None" ErrorMessage="Please Select From Minutes" InitialValue="MM" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="starttimehr" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="starttimemin" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem Text="MM" Value="MM"></asp:ListItem>
                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                        <asp:ListItem Text="45" Value="45"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr"
                    Display="None" ErrorMessage="Please Select To Time " InitialValue="HH" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimemin"
                    Display="None" ErrorMessage="Please Select To Minutes" InitialValue="MM" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="endtimehr" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="endtimemin" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem Text="MM" Value="MM"></asp:ListItem>
                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                        <asp:ListItem Text="45" Value="45"></asp:ListItem>
                        <asp:ListItem Text="59" Value="59"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Shift Type<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvddlShiftType" runat="server" ControlToValidate="ddlShiftType"
                    Display="none" ErrorMessage="Please Select Shift Type" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlShiftType" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                    Display="none" ErrorMessage="Please Select Status " ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" CausesValidation="false" PostBackUrl="~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvCat" runat="server" AllowPaging="True" AllowSorting="False"
            RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Shift Found.">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("SH_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Country">
                    <ItemTemplate>
                        <asp:Label ID="lblCountry" runat="server" Text='<%#Bind("COUNTRY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Location">
                    <ItemTemplate>
                        <asp:Label ID="lblLocation" runat="server" Text='<%#Bind("LOCATION")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Shift Code">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("SH_CODE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Shift Name">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("SH_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From Time(HRS)">
                    <ItemTemplate>
                        <asp:Label ID="lblhrs" runat="server" CssClass="lblStatus" Text='<%#Bind("SH_FRM_TIME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Time(HRS)">
                    <ItemTemplate>
                        <asp:Label ID="lblTTime" runat="server" CssClass="lblStatus" Text='<%#Bind("SH_TO_TIME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("SH_STATUS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>


