<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptViewApartment.ascx.vb"
    Inherits="Controls_AptViewApartment" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">           
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>           
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvitems1" runat="server" EmptyDataText="No More Local Conveyance To Approve."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Requisition ID">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("ESP_REQ_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested By">
                        <ItemTemplate>
                            <asp:Label ID="lblAur_id" runat="server" Text='<%#Eval("AUR_NAME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField Text="View Details" CommandName="View" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div id="tr1" runat="server">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblTamount" runat="server" Text="Total Amount"></asp:Label>
                    <asp:Label ID="lblTotalAmount" runat="server"> </asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvItems" runat="server"
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkbx" runat="server" />

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("ESP_JRNY_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From">
                        <ItemTemplate>
                            <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ESP_FROM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To">
                        <ItemTemplate>
                            <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ESP_TO")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("ESP_MODE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purpose of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblPurpose" runat="server" Text='<%#Eval("ESP_PURPOSE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appox. Kms.">
                        <ItemTemplate>
                            <asp:Label ID="lblkms" runat="server" Text='<%#Eval("ESP_APPRX_KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("AMOUNT","{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblRem" runat="server" Text='<%#Eval("REMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<div id="tr2" runat="server">

    <div class="row">
        <div class="col-md-6 pull-center">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                        Display="None" ErrorMessage="Please Enter Remarks " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Rows="3" MaxLength="500"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnAdd" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" />
            <asp:Button ID="btnModify" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" />
            <asp:TextBox ID="txtStore" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>

        </div>
    </div>
</div>
<div id="t1" runat="server" visible="false">
    <div class="row">
      <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvdetails" runat="server"
                    RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="FALSE">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Journey Date">
                            <ItemTemplate>
                                <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("ESP_JRNY_DATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="From">
                            <ItemTemplate>
                                <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ESP_FROM")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To">
                            <ItemTemplate>
                                <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ESP_TO")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblMode" runat="server" Text='<%#Eval("ESP_MODE_TRVL")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Purpose of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblPurpose" runat="server" Text='<%#Eval("ESP_PURPOSE_TRVL")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Appox. Kms.">
                            <ItemTemplate>
                                <asp:Label ID="lblkms" runat="server" Text='<%#Eval("ESP_APPRX_KM")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("AMOUNT","{0:c2}")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRem" runat="server" Text='<%#Eval("REMARKS")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RM Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRMRem" runat="server" Text='<%#Eval("RM_REMARKS")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>

</div>



