Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AddTenants
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If CDate(txtPayableDate.Text) < CDate(txtDate.Text) Then
                lblMsg.Text = "The payable date should not be less than the joining date"
                lblMsg.Visible = True
            Else

                Dim Validatecode As Integer
                Validatecode = ValidateTenantCode()
                If Validatecode = 0 Then
                    lblMsg.Text = "Tenant Code already exist please enter another code"
                    lblMsg.Visible = True
                Else
                    AddTenant()
                End If


            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateTenantCode()
        Dim ValidateCode As Integer
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_TENANT_CODE")
        sp3.Command.AddParameter("@TEN_CODE", txttcode.Text, DbType.String)
        ValidateCode = sp3.ExecuteScalar()
        Return ValidateCode
    End Function

    Public Sub AddTenant()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_TENANTS")
            ' Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_STA_AU")
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_STA_PNL")
            sp1.Command.AddParameter("@PN_NAME", ddlBuilding.SelectedItem.Value, DbType.String)
            ' sp.Command.AddParameter("@AUR_ID", ddluser.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@BDG_PROP_TYPE", ddlproptype.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@TEN_CODE", txttcode.Text, DbType.String)
            sp2.Command.AddParameter("@PN_NAME", ddlBuilding.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@TEN_OCCUPIEDAREA", txtTenantOccupiedArea.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_RENT_PER_SFT", txtRent.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_COMMENCE_DATE", txtDate.Text, DbType.Date)
            sp2.Command.AddParameter("@TEN_SECURITY_DEPOSIT", txtSecurityDeposit.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_PAYMENTTERMS", ddlPaymentTerms.SelectedItem.Text, DbType.String)
            sp2.Command.AddParameter("@TEN_NEXTPAYABLEDATE", txtPayableDate.Text, DbType.Date)
            sp2.Command.AddParameter("@TEN_STATUS", ddlstatus.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@TEN_NO_PARKING", txtNoofparking.Text, DbType.Int32)
            sp2.Command.AddParameter("@TEN_MAINT_FEE", txtfees.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_OUTSTANDING_AMOUNT", txtamount.Text, DbType.Decimal)
            sp2.Command.AddParameter("@AUR_ID", ddluser.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@TEN_REMARKS", txtRemarks.Text, DbType.String)
            sp2.Command.AddParameter("@CreatedBy", Session("uid"), DbType.String)
            sp1.ExecuteScalar()
            'sp.ExecuteScalar()
            sp2.ExecuteScalar()

            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=7")
            'lblMsg.Text = "New Tenant Added Succesfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub Cleardata()
        ddlproptype.SelectedIndex = 0

        ddlBuilding.Items.Clear()
        ddlBuilding.Items.Insert(0, New ListItem("--Select Property--", "0"))
        ddlBuilding.SelectedIndex = 0

        txtTenantOccupiedArea.Text = ""
        txtRent.Text = ""
        txtDate.Text = getoffsetdate(Date.Today)
        txtSecurityDeposit.Text = ""

        ddlPaymentTerms.SelectedIndex = -1
        txtPayableDate.Text = ""
        ddlstatus.SelectedIndex = -1

        txtNoofparking.Text = ""
        txtfees.Text = ""
        txtamount.Text = ""

        ddluser.SelectedValue = 0

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 10)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = path
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
            BindPropertyType()
            BindCity()

            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_DETAILS_FOR_PI")
            'sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            'Dim ds As New DataSet()
            'ds = sp.GetDataSet()
            'ddlCity.SelectedValue = ds.Tables(0).Rows(0).Item("CITY_ID")
            'ddlCity.Enabled = False

            BindUser()
            txtDate.Text = getoffsetdate(Date.Today)

        End If
        txtDate.Attributes.Add("readonly", "readonly")
        txtPayableDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindUser()
        Try


            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETTENUSER")
            sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select User--", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindCity()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTCTY")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddlCity.DataSource = sp.GetDataSet()
            ddlCity.DataTextField = "CTY_NAME"
            ddlCity.DataValueField = "CTY_CODE"
            ddlCity.DataBind()
            ddlCity.Items.Insert(0, New ListItem("--Select City--", "--Select City--"))
        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindPropertyType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlproptype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlproptype.SelectedIndexChanged
       
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        BindCityLoc()
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            BindProp()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select Property--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub

    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROP_tenant")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()

            ddlBuilding.Items.Insert(0, New ListItem("--Select Property--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub txtfees_TextChanged(sender As Object, e As EventArgs) Handles txtfees.TextChanged
        If txtRent.Text <> Nothing And txtfees.Text <> Nothing Then
            txtamount.ReadOnly = False
            txtamount.Text = Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtfees.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub

    Protected Sub txtRent_TextChanged(sender As Object, e As EventArgs) Handles txtRent.TextChanged
        If txtfees.Text <> Nothing And txtRent.Text <> Nothing Then
            txtamount.ReadOnly = False
            txtamount.Text = Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub
End Class
