<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssignMaintenanceRequest.ascx.vb"
    Inherits="Controls_AssignMaintenanceRequest" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>


<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Assign Maintenance Request
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
       <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Assign Maintenance Request</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
             <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <tr>
                        <td align="left" style="height:26px;width:50%">
                           Select RequestID <font class="clsNote">*</font>
                         <asp:RequiredFieldValidator ID="rfvid" runat="server" ControlToValidate="ddlid" Enabled="true" 
                                            Display="none" ErrorMessage="Please Select Request ID" InitialValue="0"  ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                           
                        </td>
                        <td align="left" style="height:26px;width:50%">
                            <asp:DropDownList ID="ddlid" runat="server" CssClass="clsComboBox" Width="99%">
                                
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:26px;width:50%">
                            Select User<font class="clsNote">*</font>
                             <asp:RequiredFieldValidator ID="rfvuser" runat="server" ControlToValidate="ddlUser" Enabled="true" 
                                            Display="none" ErrorMessage="Select User" InitialValue="0"  ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height:26px;width:50%">
                            <asp:DropDownList ID="ddlUser" runat="server" CssClass="clsComboBox" Width="99%">
                              
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" ValidationGroup="Val1" CausesValidation="true"  />
                       <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back"  />
                        </td>
                    </tr>
                    
                </table>
               
                <table id="table1" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td align="LEFT" style="height: 20px">
                            <asp:GridView ID="gvmainreq" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="5" EmptyDataText="No Records Found" Width="100%">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Request">
                                        <ItemTemplate>
                                            <asp:Label ID="lblreq" runat="server" CssClass="lblreq" Text='<%#Eval("PN_MAINTENANCE_REQ")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltype" runat="server" CssClass="lbltype" Text='<%#Eval("REQUEST_TYPE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Title">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltitle" runat="server" CssClass="lbltitle" Text='<%#Eval("REQUEST_TITLE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" CssClass="lblstatus" Text='<%#Eval("REQUEST_STATUS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Raised By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblraisedby" runat="server" CssClass="lblraisedby" Text='<%#Eval("REQUEST_RAISED_BY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Requested Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrdate" runat="server" CssClass="lblreqDate" Text='<%#Eval("REQUESTED_DATE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assigned To">
                                        <ItemTemplate>
                                            <asp:Label ID="Lbluser" runat="server" CssClass="lbluser" Text='<%#Eval("REQUEST_ASSIGNED_TO")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnsubmit" />
    </Triggers>
</asp:UpdatePanel>
