Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_Approval
    Inherits System.Web.UI.UserControl
    Dim code As String
    Dim TotalRent As Decimal
    Dim TotalRentPaid As Decimal
    Dim ClosingBalance As Decimal
    Dim SecurityDeposit As Decimal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            panel1.Visible = False
            BindGrid()         
            txtDate.Text = getoffsetdate(Date.Today)
            'txtDate.Attributes.Add("onClick", "displayDatePicker('" + txtDate.ClientID + "')")
            'txtDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            'txtPayableDate.Attributes.Add("onClick", "displayDatePicker('" + txtPayableDate.ClientID + "')")
            'txtPayableDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
        txtDate.Attributes.Add("readonly", "readonly")
        txtPayableDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_TENANT_MASTER_DETAILS")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindUser()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETUSER")
            sp1.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlproptype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlproptype.SelectedIndexChanged
        If ddlproptype.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            BindProp()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROP1")
            sp.Command.AddParameter("@PROPERTYTYPE", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()

            ddlBuilding.Items.Insert(0, New ListItem("--Select Property--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            'BindProp()
            BindCityLoc()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub

    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        BindUser()
        BindPropType()
        BindCity()
        Dim lnkApprove As LinkButton = DirectCast(e.CommandSource, LinkButton)
        Dim gvRow As GridViewRow = DirectCast(lnkApprove.NamingContainer, GridViewRow)
        Dim lbltenantcode As Label = DirectCast(gvRow.FindControl("lbltenantcode"), Label)
        Dim pname As Label = DirectCast(gvRow.FindControl("lblpropertyname"), Label)

        Dim SNO As Label = DirectCast(gvRow.FindControl("lblUniqueNo"), Label)

        hdnPN.Value = pname.Text
        hdnSNO.Value = SNO.Text

        Dim code As String = lbltenantcode.Text
        If e.CommandName = "Approve" Then
            panel1.Visible = True
            BindDetails()
            GetTenantClosingBalance()
            txtTotalRent.Text = TotalRent
            txtRentPaid.Text = TotalRentPaid

            txtTenClosingBal.Text = ClosingBalance
            txtSecurityDeposit.Text = SecurityDeposit

            If txtTenClosingBal.Text = "" Then
                txtTenClosingBal.Text = 0
            End If
        Else
            panel1.Visible = False
            UpdateStatus()
            lblmsg.Visible = True
            lblmsg.Text = "Surrender Request Rejected Successfully"
            BindGrid()
        End If

    End Sub
    Private Sub UpdateStatus()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_PN_TENANT_MASTER_REJECT")
            'sp.Command.AddParameter("@TEN_CODE", hdnSNO.Value, DbType.String)
            sp.Command.AddParameter("@SNO", hdnSNO.Value, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_TENANT_MASTER_TENCODE")
            'sp.Command.AddParameter("@CODE", code, DbType.String)
            'Dim ds As New DataSet()
            'ds = sp.GetDataSet()
            'If ds.Tables(0).Rows.Count > 0 Then

            '    Dim li As ListItem = Nothing
            '    li = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("MOV_CITY"))
            '    If Not li Is Nothing Then
            '        li.Selected = True
            '    End If

            '    Dim li1 As ListItem = Nothing
            '    li1 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_ID"))
            '    If Not li1 Is Nothing Then
            '        li1.Selected = True
            '    End If

            '    txtcode.Text = ds.Tables(0).Rows(0).Item("TEN_CODE")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETTNTDTL")
            sp.Command.AddParameter("@dummy", hdnSNO.Value, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                Dim li4 As ListItem = Nothing
                li4 = ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_PROP_TYPE"))
                If Not li4 Is Nothing Then
                    li4.Selected = True
                End If
                Dim li2 As ListItem = Nothing
                li2 = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_ADM_CODE"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                End If


                BindCityLoc()


                Dim liLoc As ListItem = Nothing
                liLoc = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_ID"))
                If Not liLoc Is Nothing Then
                    liLoc.Selected = True
                End If


                BindProp()
                Dim li As ListItem = Nothing
                li = ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PN_NAME"))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim LI5 As ListItem = Nothing
                'LI5 = ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("TEN_STATUS"))
                'If Not LI5 Is Nothing Then
                '    LI5.Selected = True
                'End If

                Dim li3 As ListItem = Nothing
                li3 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("Aur_Id"))
                If Not li3 Is Nothing Then
                    li3.Selected = True
                End If

                txtcode.Text = ds.Tables(0).Rows(0).Item("TEN_CODE")
                txtname.Text = ds.Tables(0).Rows(0).Item("TEN_NAME")
                txtTenantOccupiedArea.Text = ds.Tables(0).Rows(0).Item("TEN_OCCUPIEDAREA")
                txtRent.Text = ds.Tables(0).Rows(0).Item("TEN_RENT_PER_SFT")
                txtDate.Text = ds.Tables(0).Rows(0).Item("TEN_COMMENCE_DATE")
                txtSecurityDeposit.Text = ds.Tables(0).Rows(0).Item("TEN_SECURITY_DEPOSIT")

                ddlPaymentTerms.SelectedValue = ds.Tables(0).Rows(0).Item("TEN_PAYMENTTERMS")
                txtPayableDate.Text = ds.Tables(0).Rows(0).Item("TEN_NEXTPAYABLEDATE")

                txtNoofparking.Text = ds.Tables(0).Rows(0).Item("TEN_NO_PARKING")
                txtfees.Text = ds.Tables(0).Rows(0).Item("TEN_MAINT_FEE")
                txtamount.Text = ds.Tables(0).Rows(0).Item("TEN_OUTSTANDING_AMOUNT")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("TEN_REMARKS")
                txtSurrenderDt.Text = ds.Tables(0).Rows(0).Item("SurrenderDate")

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try

            UpdateUserStat()
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_TENANT_MASTER_PROPERTY")
            sp.Command.AddParameter("@SNO", hdnSNO.Value, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                Dim PNAME As String = ds.Tables(0).Rows(0).Item("PN_NAME")
                UpdateSurenderProperty(PNAME)
            End If
            UpdatePropertyStat()
            ApproveRequest()
            'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=41")
            lblmsg.Visible = True
            lblmsg.Text = "Request has been approved successfully."
            panel1.Visible = False
            BindGrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdateSurenderProperty(ByVal PNAME As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_LOCATIONS_PROPERTY")
        sp.Command.AddParameter("@PN_NAME", PNAME, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub UpdateUserStat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_STA_AU")
        sp.Command.AddParameter("@AUR_ID", ddluser.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub UpdatePropertyStat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_STA_PNL")
        sp.Command.AddParameter("@PN_NAME", ddlBuilding.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@Status", hdnPN.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub ApproveRequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_TENANT_MASTER_APPROVE")
        sp.Command.AddParameter("@BDG_PROP_TYPE", ddlproptype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TEN_CODE", txtcode.Text, DbType.String)
        sp.Command.AddParameter("@PN_NAME", ddlBuilding.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TEN_OCCUPIEDAREA", Convert.ToDecimal(txtTenantOccupiedArea.Text), DbType.Decimal)
        sp.Command.AddParameter("@TEN_RENT_PER_SFT", Convert.ToDecimal(txtRent.Text), DbType.Decimal)
        sp.Command.AddParameter("@TEN_COMMENCE_DATE", txtDate.Text, DbType.Date)
        sp.Command.AddParameter("@TEN_SECURITY_DEPOSIT", Convert.ToDecimal(txtSecurityDeposit.Text), DbType.Decimal)
        sp.Command.AddParameter("@TEN_PAYMENTTERMS", ddlPaymentTerms.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@TEN_NEXTPAYABLEDATE", txtPayableDate.Text, DbType.Date)
        'sp.Command.AddParameter("@TEN_STATUS", ddlstatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TEN_NO_PARKING", Convert.ToDecimal(txtNoofparking.Text), DbType.Decimal)
        sp.Command.AddParameter("@TEN_MAINT_FEE", Convert.ToDecimal(txtfees.Text), DbType.Decimal)
        sp.Command.AddParameter("@TEN_OUTSTANDING_AMOUNT", Convert.ToDecimal(txtamount.Text), DbType.Decimal)
        sp.Command.AddParameter("@AUR_ID", ddluser.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TEN_REMARKS", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@SNO", hdnSNO.Value, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Public Sub GetTenantClosingBalance()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_CLOSING_BALANCE")
        sp.Command.AddParameter("@SNO", hdnSNO.Value, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            TotalRent = ds.Tables(0).Rows(0).Item("TotalRent")
            TotalRentPaid = ds.Tables(0).Rows(0).Item("TotalRentPaid")
            ClosingBalance = ds.Tables(0).Rows(0).Item("ClosingBalance")
            SecurityDeposit = ds.Tables(0).Rows(0).Item("TEN_SECURITY_DEPOSIT")
        End If
    End Sub
End Class

