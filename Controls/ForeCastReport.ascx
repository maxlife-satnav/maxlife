<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ForeCastReport.ascx.vb"
    Inherits="Controls_ForeCastReport" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>

<div>
    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">ForeCast Report
             <hr align="center" width="60%" /></asp:Label></td>
        </tr>
    </table>
    <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
        <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left" width="100%" colspan="3">
                    <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong>&nbsp;ForeCast Report</strong>
                </td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table id="tab2" runat="server" cellpadding="2" cellspacing="0" width="100%" border="1">
                        <tr>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                Select Location<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlLocation"
                                    Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="clsComboBox" Width="99%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 25%">
                                Select From Date<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFdate"
                                    Display="None" ErrorMessage="Please Select From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 25%">
                                <asp:TextBox ID="txtFdate" runat="server" CssClass="clsTextField"></asp:TextBox>
                            </td>
                            <td align="left" style="height: 26px; width: 25%">
                                Select To Date<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                                    Display="None" ErrorMessage="Please Select To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 25%">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="clsTextField"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnSubmit" runat="Server" Text="Submit" CssClass="button" CausesValidation="true"
                                    ValidationGroup="Val1" />
                                <asp:Button ID="btnCancel" runat="Server" Text="Cancel" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                    <table id="tab1" runat="server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="height: 20px">
                                <asp:GridView ID="gvitems" runat="Server" Width="100%" AllowPaging="true" AutoGenerateColumns="false"
                                    EmptyDataText="Sorry! No Records Found">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Department">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepartment" runat="server" Text='<%#Eval("DEPARTMENT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Designation">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDesignation" runat="server" Text='<%#Eval("DESIGNATION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Forecasted Number">
                                            <ItemTemplate>
                                                <asp:Label ID="lblForecastedNumber" runat="server" Text='<%#Eval("FORECASTED_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Budgeted Number">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBudgetedNumber" runat="server" Text='<%#Eval("BUDGETED_NO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Actual Number">
                                            <ItemTemplate>
                                                <asp:Label ID="lblActual" runat="server" Text='<%#Eval("ACTUAL") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </asp:Panel>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
        <asp:PostBackTrigger ControlID="gvitems" />
    </Triggers>
</asp:UpdatePanel>
