Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Controls_EmployeeClaimedNMappedAssets
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid(Session("uid"))
            BindUnclaimedAssets(Session("uid"))
        End If

    End Sub


    Private Sub BindUnclaimedAssets(ByVal EmpId As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_UnclaimedASSETS")
            sp.Command.AddParameter("@AAT_EMP_ID", EmpId, DbType.String)
            gvClaimedAsts.DataSource = sp.GetDataSet()
            gvClaimedAsts.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid(ByVal EmpId As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TAGDTLS")
            sp.Command.AddParameter("@AAT_EMP_ID", EmpId, DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid(Session("Uid"))
    End Sub
    Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim txtSurrendered_TAG As TextBox = CType(e.Row.Cells(0).FindControl("txtSurrendered_TAG"), TextBox)
            txtSurrendered_TAG.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtSurrendered_TAG.Attributes.Add("onClick", "displayDatePicker('" + txtSurrendered_TAG.ClientID + "')")
        End If
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "Surrender" Then
                Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
                Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
                Dim txtSurrendered_TAG As TextBox = DirectCast(gvRow.FindControl("txtSurrendered_TAG"), TextBox)
                Dim sdate As Date = txtSurrendered_TAG.Text
                Dim lblCode As Label = DirectCast(gvRow.FindControl("lblCode"), Label)
                Dim stat As Integer = 15
                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_TAG_STATUS")
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_AMT_STATUS")
                sp.Command.AddParameter("@acode", lblCode.Text, DbType.String)
                sp2.Command.AddParameter("@acode", lblCode.Text, DbType.String)
                sp2.Command.AddParameter("@EMPID", Session("uid"), DbType.String)
                sp2.Command.AddParameter("@AAT_SURRENDERED_DATE", sdate, DbType.Date)
                sp2.Command.AddParameter("@AAT_TAG_STATUS", stat, DbType.Int32)
                sp.ExecuteScalar()
                sp2.ExecuteScalar()

                Dim Req_id As String
                Req_id = ObjSubSonic.RIDGENARATION("Surr/Ast/")


                Dim lblID As Label = DirectCast(gvRow.FindControl("lblID"), Label)

                Dim param(6) As SqlParameter
                param(0) = New SqlParameter("@SREQ_TS", SqlDbType.NVarChar, 200)
                param(0).Value = Req_id
                param(1) = New SqlParameter("@SREQ_ID", SqlDbType.NVarChar, 200)
                param(1).Value = Req_id
                param(2) = New SqlParameter("@SREQ_REQUEST_DATE", SqlDbType.DateTime)
                param(2).Value = getoffsetdatetime(DateTime.Now)
                param(3) = New SqlParameter("@SREQ_REQUEST_BY", SqlDbType.NVarChar, 200)
                param(3).Value = Session("uid")
                param(4) = New SqlParameter("@SREQ_REQ_REMARKS", SqlDbType.NVarChar, 2000)
                param(4).Value = ""
                param(5) = New SqlParameter("@SREQ_STATUS", SqlDbType.Int)
                param(5).Value = 15
                param(6) = New SqlParameter("@AST_TAGGED_ID", SqlDbType.Int)
                param(6).Value = Val(lblID.Text)
                ObjSubSonic.GetSubSonicExecute("INSERT_SURRENDER_REQ", param)




            End If

            BindGrid(Session("uid"))

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvClaimedAsts_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvClaimedAsts.PageIndexChanging
        gvClaimedAsts.PageIndex = e.NewPageIndex
        BindUnclaimedAssets(Session("uid"))
    End Sub

    Protected Sub gvClaimedAsts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvClaimedAsts.RowCommand
        Try
            If e.CommandName = "Claim" Then
                Dim lnkClaim As LinkButton = DirectCast(e.CommandSource, LinkButton)
                Dim gvRow As GridViewRow = DirectCast(lnkClaim.NamingContainer, GridViewRow)
                Dim lbldate As Label = DirectCast(gvRow.FindControl("lbldate"), Label)
                Dim sdate As Date = lbldate.Text

                'Dim lblID As Label = DirectCast(gvRow.FindControl("lblID"), Label)
                Dim lblCode As Label = DirectCast(gvRow.FindControl("lblCode"), Label)
                'Dim id As Integer = lblID.Text
                Dim stat As Integer = 3
                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_TAG_ClaimedAstSTATUS")
                'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_AMT_STATUS")
                'sp.Command.AddParameter("@acode", lblCode.Text, DbType.String)

                sp2.Command.AddParameter("@acode", lblCode.Text, DbType.String)
                sp2.Command.AddParameter("@EMPID", Session("uid"), DbType.String)
                sp2.Command.AddParameter("@AAT_SURRENDERED_DATE", sdate, DbType.Date)
                sp2.Command.AddParameter("@AAT_TAG_STATUS", stat, DbType.Int32)
                'sp.ExecuteScalar()
                sp2.ExecuteScalar()
            End If

            BindGrid(Session("uid"))
            BindUnclaimedAssets(Session("uid"))

        Catch ex As Exception

        End Try
    End Sub

 
End Class
