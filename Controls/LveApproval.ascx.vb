Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_LveApproval
    Inherits System.Web.UI.UserControl

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_APPROVAL")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvItem.DataSource = sp.GetDataSet()
            gvItem.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
