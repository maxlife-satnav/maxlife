<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetGetDetails.ascx.vb"
    Inherits="Controls_AssetGetDetails" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>


<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<table width="98%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">View Asset Details
             <hr align="center" width="60%" /></asp:Label>
            &nbsp;
            <br />
        </td>
    </tr>
</table>
<asp:Panel ID="PanelGridview" runat="server" Width="95%">
    <table cellspacing="0" cellpadding="0" style="vertical-align: top;"
    width="95%" align="center" border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Asset Details</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table width="80%" cellpadding="3" cellspacing="0" align="center" border="0">
                    <tr>
                        <td align="left" style="height: 26px" width="25%">
                            Enter Code to find Asset
                        </td>
                        <td align="left" style="height: 26px" width="25%">
                            <asp:TextBox ID="txtfindcode" runat="server" CssClass="clsTextField"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvcode" runat="server" ControlToValidate="txtfindcode"
                                ErrorMessage="Please enter Asset Code !" ValidationGroup="Val1" SetFocusOnError="True"
                                Display="none"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px" width="5%" valign="top">
                            <asp:Button ID="btnfincode" runat="server" CssClass="button" Text="GO" ValidationGroup="Val1" />
                        </td>
                        <td align="LEFT" style="height: 26px" width="25%" valign="top">
                            <asp:LinkButton ID="lbtn1" runat="Server" Text="Get All Records"></asp:LinkButton>
                        </td>
                    </tr>
                    
                </table>
                <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td align="center" style="height: 20px">
                            <asp:GridView ID="gvDetails_AAT" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="5" EmptyDataText="No Records Found" OnRowDeleting="gvDetails_AAT_RowDeleting">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    
                                    <asp:TemplateField HeaderText="AssetGroup Details">
                                        <ItemTemplate>
                                            <table width="100%" border="1" cellpadding="1" cellspacing="1">
                                                <tr align="left">
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        ID
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblID" runat="server" CssClass="lblAssetID" Text='<%#Eval("AAT_ID")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Code
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblCode_AAT" runat="server" CssClass="lblAssetCODE" Text='<%#Eval("AAT_CODE")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="Left">
                                                        Name
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblName_AAT" runat="server" CssClass="lblAssetName" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Model Name</td>
                                                    <td align="left">
                                                        <asp:Label ID="lblModelName_AAT" runat="server" CssClass="lblAssetModelName" Text='<%#Eval("AAT_MODEL_NAME")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Group Code</td>
                                                    <td align="left">
                                                        <asp:Label ID="lblGrpCode_AAT" runat="server" CssClass="lblAsseTGrpCODE" Text='<%#Eval("AAT_AAG_CODE")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Date</td>
                                                    <td align="left">
                                                        <asp:Label ID="lblDate_AAT" runat="server" CssClass="lblAssetUpdateDate" Text='<%#Eval("AAT_UPT_DT")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVendorCode_AAT" runat="server" CssClass="lblVendorCode" Text='<%#Eval("AAT_AVR_CODE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Brand Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBrandCode_AAT" runat="server" CssClass="lblBrandCode" Text='<%#Eval("AAT_AAB_CODE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AMC Required">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReq_AAT" runat="server" CssClass="lblAssetReq" Text='<%#Eval("AAT_AMC_REQD")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUpdatedBy_AAT" runat="server" CssClass="lblAssetUpdatedBy" Text='<%#Eval("AAT_UPT_BY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStaID_AAT" runat="server" CssClass="lblStaID" Text='<%#Eval("AAT_STA_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Owned Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOwned_AAT" runat="server" CssClass="lblAssetOwned" Text='<%#Eval("AAT_OWNED")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Purchased Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPurchasedStatus_AAT" runat="server" CssClass="lblPurchasedStatus"
                                                Text='<%#Eval("AAT_PURCHASED_STATUS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Space Status">
                                        <ItemTemplate>
                                            <asp:Label ID="LblAAT_SPC_FIXED" runat="server" CssClass="lblSpcFixed" Text='<%#Eval("AAT_SPC_FIXED")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAAT_USR_MOVABLE" runat="server" CssClass="lblSpcFixed" Text='<%#Eval("AAT_USR_MOVABLE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Conditions">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAAT_AST_CONS" runat="server" CssClass="lblConditions" Text='<%#Eval("AAT_AST_CONS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblremarks_AAT" runat="server" CssClass="lblRemarks" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <a href='frmAMGAssetModify.aspx?code=<%#Eval("AAT_CODE")%>'>EDIT</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                                </Columns>
                                <FooterStyle CssClass="GVFixedFooter" />
                                <HeaderStyle CssClass="GVFixedHeader" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
    </table>
</asp:Panel>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="gvDetails_AAT" />
    </Triggers>
</asp:UpdatePanel>
