﻿
Partial Class Controls_HDMViewReqisitions
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_AMG_ITEM_REQUISITION_GetByUserId")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        gvViewRequisitions.DataSource = sp.GetDataSet
        gvViewRequisitions.DataBind()
        If gvViewRequisitions.Rows.Count > 0 Then
            For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
                Dim lblstatus As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatusId"), Label)
                Dim hLinkFeedBack As HyperLink = CType(gvViewRequisitions.Rows(i).FindControl("hLinkFeedBack"), HyperLink)
                Dim hLinkDetails As HyperLink = CType(gvViewRequisitions.Rows(i).FindControl("hLinkDetails"), HyperLink)
                If lblstatus.Text = "8" Then
                    hLinkFeedBack.Visible = True
                    hLinkDetails.Visible = False
                Else
                    hLinkFeedBack.Visible = False
                    hLinkDetails.Visible = True
                End If
            Next
        Else
            gvViewRequisitions.DataSource = Nothing
            gvViewRequisitions.DataBind()
        End If

    End Sub

    Protected Sub gvViewRequisitions_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_AMG_ITEM_REQUISITION_GetBy_REQID")
        sp.Command.AddParameter("@REQ_ID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        gvViewRequisitions.DataSource = sp.GetDataSet
        gvViewRequisitions.DataBind()
        If gvViewRequisitions.Rows.Count > 0 Then
            For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
                Dim lblstatus As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatusId"), Label)
                Dim hLinkFeedBack As HyperLink = CType(gvViewRequisitions.Rows(i).FindControl("hLinkFeedBack"), HyperLink)
                Dim hLinkDetails As HyperLink = CType(gvViewRequisitions.Rows(i).FindControl("hLinkDetails"), HyperLink)
                If lblstatus.Text = "8" Then
                    hLinkFeedBack.Visible = True
                    hLinkDetails.Visible = False
                Else
                    hLinkFeedBack.Visible = False
                    hLinkDetails.Visible = True
                End If
            Next
        Else
            gvViewRequisitions.DataSource = Nothing
            gvViewRequisitions.DataBind()
        End If
    End Sub
End Class
