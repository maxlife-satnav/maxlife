Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGTaggedGetDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvDetails_TAG.PageIndex = 0
        End If
        fillgrid()
    End Sub
    Private Sub fillgrid() 
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_AMG_TAG")
        sp.Command.AddParameter("@AAT_AST_CODE", Trim(txtfindcode.Text), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvDetails_TAG.DataSource = ds
        gvDetails_TAG.DataBind()
        For i As Integer = 0 To gvDetails_TAG.Rows.Count - 1
            Dim lnkStatus As LinkButton = CType(gvDetails_TAG.Rows(i).FindControl("lnkStatus"), LinkButton)
            If lnkStatus.Text = 0 Then
                lnkStatus.Text = "Tagged"
            End If
        Next
        lbtn1.Visible = False
    End Sub
    Protected Sub gvDetails_TAG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_TAG.SelectedIndexChanged
        Dim rowIndex1 As Integer = gvDetails_TAG.SelectedIndex
        Dim lblID_TAG As Label = DirectCast(gvDetails_TAG.Rows(rowIndex1).FindControl("lblID_TAG"), Label)
    End Sub
    Protected Sub gvDetails_TAG_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails_TAG.PageIndexChanging
        gvDetails_TAG.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Protected Sub gvDetails_TAG_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails_TAG.RowCommand
        If e.CommandName = "UpdateStatus" Then
            Dim lnkStatus As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkStatus.NamingContainer, GridViewRow)
            Dim txtSurrendered_TAG As TextBox = DirectCast(gvRow.FindControl("txtSurrendered_TAG"), TextBox)
            Dim sdate As Date = txtSurrendered_TAG.Text
            Dim lblAllocated_TAG As Label = DirectCast(gvRow.FindControl("lblAllocated_TAG"), Label)
            Dim aDate As Date = lblAllocated_TAG.Text
            If CType(aDate, Date) > CType(sdate, Date) Then
                lblMsg.Text = "Date should not be less To date"
                lblMsg.Visible = True
                Exit Sub
            End If
            Dim status As Integer = CInt(e.CommandArgument)
            Dim lblID_TAG As Label = DirectCast(gvRow.FindControl("lblID_TAG"), Label)
            Dim id As Integer = lblID_TAG.Text
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_AMG_TAG")
            sp2.Command.AddParameter("@AST_SNO", id, DbType.Int32)
            sp2.Command.AddParameter("@AAT_SURRENDERED_DATE", sdate, DbType.Date)
            sp2.Command.AddParameter("@AAT_TAG_STATUS", 1 - status, DbType.Int32)
            sp2.ExecuteScalar()
        End If
        'Next
        fillgrid()
    End Sub
    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click 
        'Dim AAT_AST_CODE As String = txtfindcode.Text
        'Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TAGRECORD")
        'sp2.Command.AddParameter("@AAT_AST_CODE", AAT_AST_CODE, DbType.String)
        'gvDetails_TAG.DataSource = sp2.GetDataSet()
        'gvDetails_TAG.DataBind()
        'For i As Integer = 0 To gvDetails_TAG.Rows.Count - 1
        '    Dim lnkStatus As LinkButton = CType(gvDetails_TAG.Rows(i).FindControl("lnkStatus"), LinkButton)
        '    If lnkStatus.Text = 0 Then
        '        lnkStatus.Text = "Tagged"
        '    End If
        'Next
        fillgrid()
        lbtn1.Visible = True
    End Sub
    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
        txtfindcode.Text = ""
        fillgrid()
    End Sub

    Protected Sub gvDetails_TAG_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails_TAG.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim txtMonth As TextBox = CType(e.Row.Cells(0).FindControl("txtSurrendered_TAG"), TextBox)
            txtMonth.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtMonth.Attributes.Add("onClick", "displayDatePicker('" + txtMonth.ClientID + "')")
        End If
    End Sub
End Class
