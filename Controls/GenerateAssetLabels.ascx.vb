Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Controls_GenerateAssetLabels
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim ast_no As String
    Dim num As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Not IsPostBack Then
            bindvendor()
            'lblMsg.Visible = False
            pnlItems.Visible = False
        End If
    End Sub
    Private Sub bindvendor()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VENDORGENASTLALES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, "--Select--")
        If ddlVendor.Items.Count <= 1 Then
            lblMsg.Text = "No Requisitions Found !"
        End If
    End Sub

    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        lblMsg.Text = ""
        If ddlVendor.SelectedIndex > 0 Then
            bindpos()
            gvItems.Visible = False
            pnlItems.Visible = False
        Else
            gvItems.Visible = False
            ddlPO.SelectedIndex = 0
        End If
    End Sub
    Private Sub bindpos()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_POSFORASTLABELS")
        sp.Command.AddParameter("@AIP_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
        ddlPO.DataSource = sp.GetDataSet()
        ddlPO.DataTextField = "CODE1"
        ddlPO.DataValueField = "CODE2"
        ddlPO.DataBind()
        ddlPO.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPO.SelectedIndexChanged
        lblMsg.Text = ""
        If ddlPO.SelectedIndex > 0 Then
            pnlItems.Visible = True
            gvItems.Visible = True
            bindgrid()

        Else
            pnlItems.Visible = False
            gvItems.Visible = False
            lblMsg.Visible = False
            submitbtn.Visible = False

        End If
    End Sub
    Private Sub bindgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_DATAASTLBLS")
        sp.Command.AddParameter("@AIP_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIPD_PO_ID", ddlPO.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet()
        gvItems.DataSource = ds
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            submitbtn.Visible = False
        End If
    End Sub


    Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlLocation As DropDownList = DirectCast(e.Row.FindControl("ddlLocation"), DropDownList)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
         
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim count As Integer = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductId1 As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim lblCatCode As Label = DirectCast(row.FindControl("lblCatCode"), Label)
            Dim lblAstName As Label = DirectCast(row.FindControl("lblAstName"), Label)
            Dim lblSubCatCode As Label = DirectCast(row.FindControl("lblSubCategoryCode"), Label)
            Dim lblBrandCode As Label = DirectCast(row.FindControl("lblBrandCode"), Label)
            Dim lblModelCode As Label = DirectCast(row.FindControl("lblModel"), Label)
            Dim lblReq As Label = DirectCast(row.FindControl("lblReq"), Label)

            Dim ddlLocation As DropDownList = DirectCast(row.FindControl("ddlLocation"), DropDownList)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim i As Integer = 0
            If chkSelect.Checked Then
                If ddlLocation.SelectedIndex <= 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Please Select Location"
                    Exit Sub
                    'ElseIf lblCatCode.Text = "CON" Then
                    '    count = count + 1
                    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_UPDATE_CONS_STOCK")
                    '    sp.Command.AddParameter("@AST_MD_CODE", lblModelCode.Text, DbType.String)
                    '    sp.Command.AddParameter("@AST_MD_TOTAVBL", CInt(txtQty.Text), DbType.Int32)
                    '    sp.Command.AddParameter("@REQ_ID", lblReq.Text, DbType.String)
                    '    sp.Command.AddParameter("@AST_MD_LOCID", ddlLocation.SelectedItem.Value, DbType.String)
                    '    sp.ExecuteScalar()
                Else
                    For i = 0 To CInt(txtQty.Text) - 1
                        ' When the request is IT it not updating the AST_MODEL.This code added for updating AST_MODEL table.
                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_UPDATE_CONS_STOCK")
                        'sp.Command.AddParameter("@AST_MD_ID", CInt(lblProductId1.Text), DbType.Int32)
                        'sp.Command.AddParameter("@AST_MD_TOTAVBL", CInt(txtQty.Text), DbType.Int32)
                        'sp.Command.AddParameter("@REQ_ID", lblReq.Text, DbType.String)
                        'sp.Command.AddParameter("@AST_MD_LOCID", ddlLocation.SelectedItem.Value, DbType.String)
                        'sp.ExecuteScalar()
                        Dim AstCode As String = ddlLocation.SelectedItem.Value + "/" + lblCatCode.Text + "/" + lblSubCatCode.Text + "/" + lblBrandCode.Text + "/" + lblModelCode.Text + "/" + CStr(GetMaxAssetId())
                        InsertAsset(AstCode, lblCatCode.Text, ddlVendor.SelectedItem.Value, lblBrandCode.Text, lblModelCode.Text, lblSubCatCode.Text, lblProductId1.Text, ddlLocation.SelectedItem.Value)
                        InsertAssetSpace(AstCode, ddlLocation.SelectedItem.Value, "", "", 0, lblReq.Text)
                        ' space asset mapping code added on 31 dec 2014
                        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_AMT_ASSET_SPACE_STAT")
                        'sp1.Command.AddParameter("@LOC_ID", ddlLocation.SelectedValue, DbType.String)
                        'sp1.Command.AddParameter("@TWR_ID", "", DbType.String)
                        'sp1.Command.AddParameter("@FLR_ID", "", DbType.String)
                        'sp1.Command.AddParameter("@AAT_AST_CODE", AstCode, DbType.String)
                        'sp1.ExecuteScalar()
                        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ALLOCATE_SPACEASSET_INTRA")
                        'sp.Command.AddParameter("@AAT_AST_CODE", AstCode, DbType.String)
                        'sp.Command.AddParameter("@AAT_SPC_ID", "", DbType.String)
                        'sp.Command.AddParameter("@AAT_EMP_ID", Session("uid"), DbType.String)
                        'sp.Command.AddParameter("@AAT_ITEM_REQUISITION", lblReq.Text, DbType.String)
                        'sp.ExecuteScalar()
                        'Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_UPDATE_ASTLBLGENERATED")
                        'sp3.Command.AddParameter("@REQ_ID", lblReq.Text, DbType.String)
                        'sp3.Command.AddParameter("@AIPD_AST_CODE", AstCode, DbType.String)
                        'sp3.Command.AddParameter("@AIPD_ASTLBL_QTY", count, DbType.Int32)
                        'sp3.ExecuteScalar()
                        ' Ends here
                        'AstCode
                        'UpdatePODetails(Integer.Parse(lblProductId1.Text), ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text))
                    Next
                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_UPDATE_CONS_STOCK")
                    sp2.Command.AddParameter("@AST_MD_CODE", lblModelCode.Text, DbType.String)
                    sp2.Command.AddParameter("@AST_MD_TOTAVBL", CInt(txtQty.Text), DbType.Int32)
                    sp2.Command.AddParameter("@REQ_ID", lblReq.Text, DbType.String)
                    sp2.Command.AddParameter("@AST_MD_LOCID", ddlLocation.SelectedItem.Value, DbType.String)
                    sp2.ExecuteScalar()
                    UpdatePOStatus(ddlPO.SelectedItem.Value, lblModelCode.Text)
                    count = count + 1
                End If

            End If

        Next

        If count > 0 Then
            mainPanel.Visible = False


            bindgrid1(ddlPO.SelectedItem.Value, ast_no)
        End If
        If count = 0 Then
            lblMsg.Visible = True
            lblMsg.Text = "Please select the Asset(S)..."
            Exit Sub
        End If

    End Sub
    Private Sub bindgrid1(ByVal POCode As String, ByVal AstCode As String)
        AstCode = AstCode.Remove(0, 1)
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LABLE_ASSETS")
        sp3.Command.AddParameter("@PO_CODE", POCode, DbType.String)
        sp3.Command.AddParameter("@AST_CODE", AstCode, DbType.String)
        GridView1.DataSource = sp3.GetDataSet()
        GridView1.DataBind()
        btnExport1.Visible = True
        btnBack.Visible = True
    End Sub


    Private Sub InsertAssetSpace(ByVal AstCode As String, ByVal location As String, ByVal Tower As String, ByVal Floor As String, ByVal ProcType As Integer, ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMT_ASSET_SPACE_INSERT")
        sp.Command.AddParameter("@AAS_AAT_CODE", AstCode, DbType.String)
        sp.Command.AddParameter("@AAS_LOC_ID", location, DbType.String)
        sp.Command.AddParameter("@AAS_FLR_ID", "", DbType.String)
        sp.Command.AddParameter("@AAS_BDG_ID", "", DbType.String)
        'sp.Command.AddParameter("@AAS_FLR_ID", Floor, DbType.String)
        sp.Command.AddParameter("@AAS_PROC_TYPE", ProcType, DbType.String)
        sp.Command.AddParameter("@AAS_REMARKS", "", DbType.String)
        sp.Command.AddParameter("@AAS_REQ_ID", reqid, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Function GetMaxAssetId() As Integer
        Dim AstId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ASSET_GetMaxAAT_ID")
        If Integer.TryParse(sp.ExecuteScalar, AstId) Then
        Else
            AstId = 0
        End If
        Return AstId
    End Function
    Private Sub InsertAsset(ByVal AstCode As String, ByVal cat_id As String, ByVal Vendor As String, ByVal Brand As String, ByVal Model As String, subcat As String, ByVal model_id As String, ByVal loc_id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_AMG_ASSET_Insert")
        sp.Command.AddParameter("@AAT_CODE", AstCode, DbType.String)
        'sp.Command.AddParameter("@AAT_NAME", AstCode, DbType.String)
        sp.Command.AddParameter("@AAT_MODEL_ID", model_id, DbType.String)
        sp.Command.AddParameter("@AAT_MODEL_NAME", Model, DbType.String)
        sp.Command.AddParameter("@AAT_AAG_CODE", cat_id, DbType.String)
        sp.Command.AddParameter("@AAT_SUB_CODE", subcat, DbType.String)
        sp.Command.AddParameter("@AAT_AVR_CODE", Vendor, DbType.String)
        sp.Command.AddParameter("@AAT_AAB_CODE", Brand, DbType.String)
        sp.Command.AddParameter("@AAT_AMC_REQD", 0, DbType.Int32)
        sp.Command.AddParameter("@AAT_UPT_DT", DateTime.Now, DbType.DateTime)
        sp.Command.AddParameter("@AAT_UPT_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@AAT_STA_ID", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_OWNED", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_PURCHASED_STATUS", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_SPC_FIXED", 1, DbType.Int32)
        sp.Command.AddParameter("@AAT_USR_MOVABLE", 0, DbType.Int32)
        sp.Command.AddParameter("@AAT_AST_CONS", 0, DbType.Int32)
        sp.Command.AddParameter("@AAT_DESC", "", DbType.String)
        sp.Command.AddParameter("@CONF_TYPE", "", DbType.String)
        sp.Command.AddParameter("@LOC_ID", loc_id, DbType.String)
        sp.ExecuteScalar()

        ast_no = ast_no + "," + AstCode

    End Sub
    Private Sub UpdatePOStatus(ByVal PoNumber As String, ByVal Productid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_POINWARD_UPDATESTATUSORDER_STATUS")
        sp.Command.AddParameter("@AIP_PO_ID", PoNumber, DbType.String)
        sp.Command.AddParameter("@AIP_PRDID", Productid, DbType.String)
        sp.ExecuteScalar()
    End Sub
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        'bindgrid1()
    End Sub

    Protected Sub btnExport1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport1.Click
        Export("AssetLables.xls", GridView1)
    End Sub
End Class
