Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.ComponentModel

Partial Class Controls_ucAssetLocationReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocation()
            getassetcategory()
            TryCast(ddlAssetCategory, IPostBackDataHandler).RaisePostDataChangedEvent()
            BindGridView(Nothing)
            'ddlAstSubCat.Items.Insert(0, "--All--")
            'ddlAstBrand.Items.Insert(0, "--All--")
            'ddlModel.Items.Insert(0, "--All--")
            'btnExport.Visible = False
        End If
 
    End Sub

   
    'Public Sub bindlocation()
    '    Dim param(0) As SqlParameter

    '    param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 200)
    '    param(0).Value = 1
    '    'top(100) problem in query
    '    ObjSubsonic.Binddropdown(ddllocation, "GET_LOCTION_V", "LCM_NAME", "LCM_CODE", param)
    '    ddllocation.Items.Insert(0, New ListItem("--All--"))
    '    ddllocation.Items.RemoveAt(1)
    'End Sub

    Protected Sub ddllocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllocation.SelectedIndexChanged
        'BindGridView(Nothing)
        ' btnExport.Visible = True
    End Sub

    Public Sub BindGridView(ByVal gv As GridView)
        'Dim param(5) As SqlParameter
        'param(0) = New SqlParameter("@LOCCODE", SqlDbType.NVarChar, 200)
        'param(0).Value = ddllocation.SelectedValue
        'param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        'param(1).Value = 1
        'param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        'param(2).Value = "Location"
        'param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        'param(3).Value = "DESC"
        'param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        'param(4).Value = 100
        'param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        'param(5).Value = 0
        'ObjSubsonic.BindGridView(gv, "GET_ASSETS_BY_lOCATION_V", param)
        Dim AstCat As String = ""
        If ddlAssetCategory.SelectedValue = "--All--" Then
            AstCat = ""
        Else
            AstCat = ddlAssetCategory.SelectedValue
        End If

        Dim AstSubCat As String = ""
        If ddlAstSubCat.SelectedValue = "--All--" Then
            AstSubCat = ""
        Else
            AstSubCat = ddlAstSubCat.SelectedValue
        End If

        Dim AstBrand As String = ""
        If ddlAstBrand.SelectedValue = "--All--" Then
            AstBrand = ""
        Else
            AstBrand = ddlAstBrand.SelectedValue
        End If

        Dim AstModel As String = ""
        If ddlModel.SelectedValue = "--All--" Then
            AstModel = ""
        Else
            AstModel = ddlModel.SelectedValue
        End If

        Dim Location As String = ""
        If ddllocation.SelectedValue = "--All--" Then
            Location = ""
        Else
            Location = ddllocation.SelectedValue
        End If
        Dim frmDate As String = FromDate.Text


        Dim toDate As String = ""
        toDate = txtToDate.Text


        Dim param(7) As SqlParameter

        param(0) = New SqlParameter("@Category", SqlDbType.NVarChar, 200)
        param(0).Value = AstCat
        param(1) = New SqlParameter("@SubCategory", SqlDbType.NVarChar, 200)
        param(1).Value = AstSubCat
        param(2) = New SqlParameter("@Brand", SqlDbType.NVarChar, 200)
        param(2).Value = AstBrand
        param(3) = New SqlParameter("@Model", SqlDbType.NVarChar, 200)
        param(3).Value = AstModel
        param(4) = New SqlParameter("@LOCCODE", SqlDbType.NVarChar, 200)
        param(4).Value = Location
        param(5) = New SqlParameter("@FromDate", SqlDbType.DateTime, 200)
        param(5).Value = FromDate.Text
        param(6) = New SqlParameter("@ToDate", SqlDbType.DateTime, 200)
        param(6).Value = txtToDate.Text
        param(7) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(7).Value = Session("Uid").ToString


        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASSETS_BY_lOCATION_V", param)

        Dim rds As New ReportDataSource()
        rds.Name = "AssetLocationRptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetLocationReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        ReportViewer1.LocalReport.EnableHyperlinks = True
    End Sub

    'Protected Sub gvAssetLoc_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gvAssetLoc.Sorting
    '    Dim ds As DataSet = TryCast(Session("dataset"), DataSet)
    '    Dim dt = ds.Tables(0)

    '    If dt IsNot Nothing Then

    '        'Sort the data.
    '        dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
    '        gvAssetLoc.DataSource = dt
    '        gvAssetLoc.DataBind()

    '    End If



    'End Sub

    'Private Function GetSortDirection(ByVal column As String) As String

    '    ' By default, set the sort direction to ascending.
    '    Dim sortDirection = "ASC"

    '    ' Retrieve the last column that was sorted.
    '    Dim sortExpression = TryCast(ViewState("SortExpression"), String)

    '    If sortExpression IsNot Nothing Then
    '        ' Check if the same column is being sorted.
    '        ' Otherwise, the default value can be returned.
    '        If sortExpression = column Then
    '            Dim lastDirection = TryCast(ViewState("SortDirection"), String)
    '            If lastDirection IsNot Nothing _
    '              AndAlso lastDirection = "ASC" Then

    '                sortDirection = "DESC"

    '            End If
    '        End If
    '    End If

    '    ' Save new values in ViewState.
    '    ViewState("SortDirection") = sortDirection
    '    ViewState("SortExpression") = column

    '    Return sortDirection

    'End Function

    'Protected Sub gvAssetLoc_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvAssetLoc.PageIndexChanging
    '    gvAssetLoc.PageIndex = e.NewPageIndex
    '    BindGridView(gvAssetLoc)
    'End Sub

    '    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
    '        Dim gv As New GridView
    '        BindGridView(gv)
    '        Export("PODetails_Report" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
    '    End Sub

    '#Region "Export"
    '    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
    '        HttpContext.Current.Response.ContentType = "application/ms-excel"
    '        Dim sw As StringWriter = New StringWriter
    '        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '        '  Create a form to contain the grid
    '        Dim table As Table = New Table
    '        table.GridLines = gv.GridLines
    '        '  add the header row to the table
    '        If (Not (gv.HeaderRow) Is Nothing) Then
    '            PrepareControlForExport(gv.HeaderRow)
    '            table.Rows.Add(gv.HeaderRow)
    '        End If
    '        '  add each of the data rows to the table
    '        For Each row As GridViewRow In gv.Rows
    '            PrepareControlForExport(row)
    '            table.Rows.Add(row)
    '        Next
    '        '  add the footer row to the table
    '        If (Not (gv.FooterRow) Is Nothing) Then
    '            PrepareControlForExport(gv.FooterRow)
    '            table.Rows.Add(gv.FooterRow)
    '        End If
    '        '  render the table into the htmlwriter
    '        table.RenderControl(htw)
    '        '  render the htmlwriter into the response
    '        HttpContext.Current.Response.Write(sw.ToString)
    '        HttpContext.Current.Response.End()
    '    End Sub
    '    ' Replace any of the contained controls with literals
    '    Private Shared Sub PrepareControlForExport(ByVal control As Control)
    '        Dim i As Integer = 0
    '        Do While (i < control.Controls.Count)
    '            Dim current As Control = control.Controls(i)
    '            If (TypeOf current Is LinkButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
    '            ElseIf (TypeOf current Is ImageButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
    '            ElseIf (TypeOf current Is HyperLink) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
    '            ElseIf (TypeOf current Is DropDownList) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
    '            ElseIf (TypeOf current Is TextBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
    '            ElseIf (TypeOf current Is CheckBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
    '                'TODO: Warning!!!, inline IF is not supported ?
    '            End If
    '            If current.HasControls Then
    '                PrepareControlForExport(current)
    '            End If
    '            i = (i + 1)
    '        Loop
    '    End Sub
    '#End Region
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        'ddlAssetCategory.Items.Insert(0, "--All--")
        ddlAssetCategory.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        'ddlAstSubCat.Items.Insert(0, "--All--")
        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        'getconsumbles()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        TryCast(ddlAstBrand, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        'ddlModel.Items.Insert(0, "--All--")
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))

    End Sub
    Private Sub BindLocation()
        'Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        'param(0).Value = 1
        'param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 100)
        'param(0).Value = Session("Uid").ToString
        'ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
        sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        'ddlLocation.Items.Insert(0, "--All--")
        ' ddlLocation.Items.Insert(0, New ListItem("--All--", "0"))
        ddllocation.Items.Insert(0, New ListItem("--All--", "ALL"))


    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()

        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        TryCast(ddlAstSubCat, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub



    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        'If ddlAstSubCat.SelectedIndex <> 0 Then
        '    'ddlModel.Items.Clear()
        '    'ddlModel.SelectedIndex = 0
        '    'ddlLocation.SelectedIndex = 0

        '    getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        'End If  ddlModel.Items.Clear()
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)

    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        'If ddlAstBrand.SelectedIndex <> 0 Then
        '    'ddlLocation.SelectedIndex = 0

        '    getmakebycatsubcat()
        'End If
        getmakebycatsubcat()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        BindGridView(Nothing)
    End Sub
End Class
