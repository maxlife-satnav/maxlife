Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_Update_Maintain_Schedule
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    End Sub
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            
            chkBrk.Attributes.Add("OnClick", "test();")

            ''            pnlBrkDwn.Visible = False
            Dim ReqId As String
            ReqId = Request.QueryString("Req_id")
            BindMainSchedules(ReqId)
        End If
    End Sub

    Public Sub BindMainSchedules(ByVal ReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ReqId
        ObjSubsonic.BindDataGridGET_PLAN_DETAILS(PM_REQ_DATA, "GET_PLAN_DETAILS", param)
        If PM_REQ_DATA.Items.Count = 0 Then
            PM_REQ_DATA.DataSource = Nothing
            PM_REQ_DATA.DataBind()
        Else
            Dim cnt As Integer
            For cnt = 0 To PM_REQ_DATA.Items.Count() - 1
                

                Dim btn1 As New LinkButton
                btn1 = CType(PM_REQ_DATA.Items(cnt).Cells(0).Controls(0), LinkButton)

                If Convert.ToDateTime(PM_REQ_DATA.Items(cnt).Cells(7).Text) <= getoffsetdatetime(DateTime.Now) Then
                    btn1.Enabled = True
                Else
                    btn1.Enabled = False
                End If
                If PM_REQ_DATA.Items(cnt).Cells(5).Text = "Closed" Then
                    Dim btn As New LinkButton
                    btn = CType(PM_REQ_DATA.Items(cnt).Cells(0).Controls(0), LinkButton)
                    btn.Enabled = False
                End If



            Next

        End If

    End Sub

    
    Sub PM_REQ_DATA_Paged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

        PM_REQ_DATA.CurrentPageIndex = e.NewPageIndex
        Dim ReqId As String
        ReqId = Request.QueryString("Req_id")
        BindMainSchedules(ReqId)

    End Sub
    Private Sub PM_REQ_DATA_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles PM_REQ_DATA.ItemCommand

        Dim btn As LinkButton = CType(e.CommandSource, LinkButton)

        Dim gdSno, LnkVal
        Dim icnt As Integer

        Dim gdLnkbtn As New LinkButton
        gdLnkbtn = CType(PM_REQ_DATA.Items(icnt).Cells(0).Controls(0), LinkButton)
        LnkVal = gdLnkbtn.Text
        Select Case Trim(btn.Text)
            Case "Update"

               
                Dim exeDate As String = e.Item.Cells(7).Text
                Dim exeTime As String = e.Item.Cells(8).Text
                Dim sprCost As String = e.Item.Cells(9).Text
                Dim lbrCost As String = e.Item.Cells(10).Text
                Dim rmks As String = e.Item.Cells(11).Text

                'e.Item.BackColor = System.Drawing.Color.AliceBlue
                'e.Item.ForeColor = System.Drawing.Color.Blue

                e.Item.BackColor = System.Drawing.Color.LightGray
                e.Item.ForeColor = System.Drawing.Color.Blue
                pnlButton.Visible = True
                txtExeDate.Text = ""
                txtExeTime.Text = ""
                txtSpr.Text = ""
                txtLbr.Text = ""
                txtRmks.Text = ""
                gdSno = PM_REQ_DATA.Items(icnt).Cells(1).Text

                sus(gdSno)

                btn.Text = "Cancel"
                chkBrk.Checked = False

            Case "Cancel"
                e.Item.BackColor = System.Drawing.Color.Empty
                e.Item.ForeColor = System.Drawing.Color.Empty
                pnlButton.Visible = False
                btn.Text = "Update"
            Case Else
        End Select

    End Sub

    'Private Sub chkBrk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBrk.CheckedChanged

    '    If chkBrk.Checked = True Then
    '        pnlBrkDwn.Visible = True
    '    Else
    '        pnlBrkDwn.Visible = False
    '        '            pnlBrkDwn.Controls.Clear()
    '    End If

    'End Sub

    Private Function sus(ByVal i)
        Dim j As String = i
        Return j
    End Function

    Protected Sub cmdSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click
        If Page.IsValid Then

            If Val(txtSpr.Text) >= 0 Then
                If Val(txtLbr.Text) >= 0 Then
                    If Val(txtExeTime.Text) >= 0 Then
                        Dim icnt As Integer
                        Dim a As Integer
                        Dim LnkVal As String
                        Dim gdLnkbtn As New LinkButton
                        For icnt = 0 To PM_REQ_DATA.Items.Count - 1

                            gdLnkbtn = CType(PM_REQ_DATA.Items(icnt).Cells(0).Controls(0), LinkButton)
                            LnkVal = gdLnkbtn.Text

                            If LnkVal = "Cancel" Then
                                a = PM_REQ_DATA.Items(icnt).Cells(1).Text
                                If txtExeDate.Text = String.Empty Then
                                    txtExeDate.Text = getoffsetdate(Date.Today)
                                End If
                                If txtExeTime.Text = String.Empty Then
                                    txtExeTime.Text = "00:00"
                                End If
                                If txtSpr.Text = String.Empty Then
                                    txtSpr.Text = "0"
                                End If
                                If txtLbr.Text = String.Empty Then
                                    txtLbr.Text = "0"
                                End If
                                If txtRmks.Text = String.Empty Then
                                    txtRmks.Text = "0"
                                End If
                                If chkBrk.Checked = False Then

                                    Dim param(7) As SqlParameter
                                    param(0) = New SqlParameter("@PVD_PLANEND_TIME", SqlDbType.NVarChar, 200)
                                    param(0).Value = Replace(Trim(txtExeTime.Text), "'", "''")
                                    param(1) = New SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.DateTime)
                                    param(1).Value = Replace(Trim(txtExeDate.Text), "'", "''")
                                    param(2) = New SqlParameter("@PVD_PLANSPARES_COST", SqlDbType.NVarChar, 200)
                                    param(2).Value = Replace(Trim(txtSpr.Text), "'", "''")
                                    param(3) = New SqlParameter("@PVD_PLANLABOUR_COST", SqlDbType.NVarChar, 200)
                                    param(3).Value = Replace(Trim(txtLbr.Text), "'", "''")
                                    param(4) = New SqlParameter("@PVD_PLAN_REMARKS", SqlDbType.NVarChar, 200)
                                    param(4).Value = Replace(Trim(txtRmks.Text), "'", "''")
                                    param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                    param(5).Value = 1026
                                    param(6) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                    param(6).Value = Request.QueryString("Req_id")
                                    param(7) = New SqlParameter("@PVD_ID", SqlDbType.Int)
                                    param(7).Value = PM_REQ_DATA.Items(icnt).Cells(1).Text
                                    ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PLAN_DTLS", param)
                                Else
                                    Dim strStTimeDt, strEndTimeDt As String

                                    'strStTimeDt = cboBRKStHr.SelectedItem.Text & ":" & cboBRKStMin.SelectedItem.Text & " " & txtBRKStartDt.Text.ToString("MMDDYYYY")
                                    'strEndTimeDt = cboBRKEndHr.SelectedItem.Text & ":" & cboBRKEndMin.SelectedItem.Text & " " & txtBRKEndDt.Text.ToString("MMDDYYYY")

                                    If txtBRKStartDt.Text = String.Empty Then
                                        PopUpMessageNormal("", Me.Page)
                                        Exit Sub
                                    End If


                                    strStTimeDt = txtBRKStartDt.Text & " " & cboBRKStHr.SelectedItem.Text & ":" & cboBRKStMin.SelectedItem.Text & ":00.000"
                                    strEndTimeDt = txtBRKEndDt.Text & " " & cboBRKEndHr.SelectedItem.Text & ":" & cboBRKEndMin.SelectedItem.Text & ":00.000"

                                    If Trim(strStTimeDt) = "00:00:00.000" Then
                                        strStTimeDt = "00:00"
                                    End If
                                    If Trim(strEndTimeDt) = "00:00:00.000" Then
                                        strEndTimeDt = "00:00"
                                    End If

                                    Dim param(11) As SqlParameter
                                    param(0) = New SqlParameter("@PVD_PLANEND_TIME", SqlDbType.NVarChar, 200)
                                    param(0).Value = Replace(Trim(txtExeTime.Text), "'", "''")
                                    param(1) = New SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.DateTime)
                                    param(1).Value = Replace(Trim(txtExeDate.Text), "'", "''")
                                    param(2) = New SqlParameter("@PVD_PLANSPARES_COST", SqlDbType.Float)
                                    param(2).Value = CDbl(txtSpr.Text) 'Replace(Trim(txtSpr.Text), "'", "''")
                                    param(3) = New SqlParameter("@PVD_PLANLABOUR_COST", SqlDbType.NVarChar, 200)
                                    param(3).Value = CDbl(txtLbr.Text) 'Replace(Trim(txtLbr.Text), "'", "''")
                                    param(4) = New SqlParameter("@PVD_PLAN_REMARKS", SqlDbType.NVarChar, 200)
                                    param(4).Value = Replace(Trim(txtRmks.Text), "'", "''")
                                    param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                    param(5).Value = 1026
                                    param(6) = New SqlParameter("@PVD_BRK_EXP", SqlDbType.NVarChar, 200)
                                    param(6).Value = txtBrkExp.Text
                                    param(7) = New SqlParameter("@PVD_BRK_REM", SqlDbType.NVarChar, 200)
                                    param(7).Value = Replace(Trim(txtBRKRem.Text), "'", "''")
                                    param(8) = New SqlParameter("@PVD_BRK_ONDT", SqlDbType.NVarChar, 200)
                                    param(8).Value = strStTimeDt
                                    param(9) = New SqlParameter("@PVD_BRK_ENDDT", SqlDbType.NVarChar, 200)
                                    param(9).Value = strEndTimeDt
                                    param(10) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                    param(10).Value = Request.QueryString("Req_id")
                                    param(11) = New SqlParameter("@PVD_ID", SqlDbType.Int)
                                    param(11).Value = PM_REQ_DATA.Items(icnt).Cells(1).Text
                                    ObjSubsonic.GetSubSonicExecute("FAM_UPDATE_PLAN_DTLS_WTH_BREAKGE", param)

                                End If
                                gdLnkbtn.Enabled = False
                            End If
                        Next
                        lblMsg.Text = ""
                        pnlButton.Visible = False

                        Dim ReqId As String
                        ReqId = Request.QueryString("Req_id")
                        BindMainSchedules(ReqId)


                    Else
                        lblMsg.Text = "Enter Only Digits"
                    End If
                Else
                    lblMsg.Text = "Enter Only Digits"
                End If
            Else
                lblMsg.Text = "Enter Only Digits"
            End If
        End If
    End Sub
End Class
