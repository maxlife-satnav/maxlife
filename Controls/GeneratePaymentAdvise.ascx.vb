Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_GeneratePaymentAdvise
    Inherits System.Web.UI.UserControl

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GEN_PAY")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@PAYMENT_MODE", ddlpay.SelectedItem.Value, DbType.String)

        If ddlpay.SelectedItem.Value = "1" Then
            sp.Command.AddParameter("@ChequeNo", txtCheque.Text, DbType.String)
            sp.Command.AddParameter("@IssuingBank", txtBankName.Text, DbType.String)
            sp.Command.AddParameter("@DepositedBank", "", DbType.String)
            sp.Command.AddParameter("@AccountNumber", txtAccNo.Text, DbType.String)
            sp.Command.AddParameter("@IFSC", "", DbType.String)
        ElseIf ddlpay.SelectedItem.Value = "3" Then
            sp.Command.AddParameter("@ChequeNo", "", DbType.String)
            sp.Command.AddParameter("@IssuingBank", txtIBankName.Text, DbType.String)
            sp.Command.AddParameter("@DepositedBank", txtDeposited.Text, DbType.String)
            sp.Command.AddParameter("@AccountNumber", "", DbType.String)
            sp.Command.AddParameter("@IFSC", txtIFCB.Text, DbType.String)
        Else
            sp.Command.AddParameter("@ChequeNo", "", DbType.String)
            sp.Command.AddParameter("@IssuingBank", "", DbType.String)
            sp.Command.AddParameter("@DepositedBank", "", DbType.String)
            sp.Command.AddParameter("@AccountNumber", "", DbType.String)
            sp.Command.AddParameter("@IFSC", "", DbType.String)
        End If
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=15")
        'lblMsg.Text = "Payment Advise Generated"
        Cleardata()

    End Sub
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        txtBuilding.Text = ""
        txtproperty.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        ddlpay.SelectedIndex = -1
        txtamount.Text = ""
        txtVendorName.Text = ""
        txtRemarks.Text = ""
        ddlwstatus.SelectedIndex = -1
        txtCheque.Text = ""
        txtBankName.Text = ""
        txtAccNo.Text = ""
        txtIBankName.Text = ""
        txtDeposited.Text = ""
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getrequest()
            
            panel1.Visible = False
            panel2.Visible = False
        End If
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PAY_APP")
        sp.Command.AddParameter("@User", Session("UID"), DbType.String)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select Request--", "0"))
    End Sub

    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        If ddlWorkRequest.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_WR")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtBuilding.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
                txtproperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT")
                txtVendorName.Text = ds.Tables(0).Rows(0).Item("VENDOR_NAME")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("Remarks")
                Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                If status = 0 Then
                    ddlwstatus.SelectedValue = 0
                ElseIf status = 1 Then
                    ddlwstatus.SelectedValue = 1
                Else
                    ddlwstatus.SelectedValue = 2
                End If
            End If
        Else
            txtBuilding.Text = ""
            txtproperty.Text = ""
            txtWorkTitle.Text = ""
            txtWorkSpec.Text = ""
            txtamount.Text = ""
            txtVendorName.Text = ""
            txtRemarks.Text = ""
            ddlwstatus.SelectedIndex = -1
            txtCheque.Text = ""
            txtBankName.Text = ""
            txtAccNo.Text = ""
            txtIBankName.Text = ""
            txtDeposited.Text = ""
        End If
    End Sub

    Protected Sub ddlpay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpay.SelectedIndexChanged
        If ddlpay.SelectedIndex > 0 Then
            If ddlpay.SelectedItem.Value = "1" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpay.SelectedItem.Value = "2" Then
                panel1.Visible = False
                panel2.Visible = False
            ElseIf ddlpay.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            End If
        End If
    End Sub
End Class
