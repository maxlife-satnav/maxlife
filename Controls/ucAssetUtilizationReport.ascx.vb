Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Security
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms


Partial Class Controls_ucAssetUtilizationReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnllbl.Visible = False
            Binddrop()


            bindgridview()
            ' getassetcategory()


        End If
    End Sub

    Public Sub Binddrop()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        param(0).Value = 1
        ObjSubsonic.Binddropdown(ddlassetid, "USP_GET_ASSETCATEGORIESSALL", "VT_TYPE", "VT_CODE", param)
        ddlassetid.Items.Insert(0, "--All--")
        ddlassetid.Items.RemoveAt(1)
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        bindgridview()
        'BindLabels()
        'pnllbl.Visible = True

    End Sub
    'Public Sub BindLabels()
    '    Dim asstid As String = ""
    '    If ddlassetid.SelectedValue = "--All--" Then
    '        asstid = ""
    '    Else
    '        asstid = ddlassetid.SelectedValue
    '    End If


    '    Dim dr As SqlDataReader
    '    Dim param(5) As SqlParameter
    '    param(0) = New SqlParameter("@assetcat", SqlDbType.NVarChar, 200)
    '    param(0).Value = asstid
    '    param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
    '    param(1).Value = 1
    '    param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
    '    param(2).Value = "TOTAL"
    '    param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
    '    param(3).Value = "ASC"
    '    param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
    '    param(4).Value = 100
    '    param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '    param(5).Value = 0
    '    dr = ObjSubsonic.GetSubSonicDataReader("GET_ASSET_UTILIZATION_REPORT_V", param)
    '    If dr.Read() Then
    '        lbltotvacnt.Text = dr("AVAILABLE").ToString()
    '        lbltotalloc.Text = dr("ALLOCATED").ToString()
    '        lbltot.Text = dr("TOTAL").ToString()
    '        pnllbl.Visible = True
    '    Else
    '        pnllbl.Visible = False
    '    End If
    'End Sub
    Public Sub bindgridview()


        Dim assetid As String = ""

        If ddlassetid.SelectedItem.Text = "--All--" Then
            assetid = ""
        Else
            assetid = ddlassetid.SelectedItem.Text
        End If

        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@assetcat", SqlDbType.NVarChar, 200)
        param(0).Value = assetid
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = 1
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(2).Value = "PRODUCTNAME"
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(3).Value = "ASC"
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = 100
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = 0
        'ObjSubsonic.BindGridView(gvasset, "GET_ASSET_STATUSREPORT_V", param)


        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_STATUSREPORT_V", param)

        Dim rds As New ReportDataSource()
        rds.Name = "AssetUtilRptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetUtilReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

    End Sub
    '    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
    '        Export("AssetUtilization_Report" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gvasset)
    '    End Sub

    '#Region "Export"
    '    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
    '        HttpContext.Current.Response.ContentType = "application/ms-excel"
    '        Dim sw As StringWriter = New StringWriter
    '        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '        '  Create a form to contain the grid
    '        Dim table As Table = New Table
    '        table.GridLines = gv.GridLines
    '        '  add the header row to the table
    '        If (Not (gv.HeaderRow) Is Nothing) Then
    '            PrepareControlForExport(gv.HeaderRow)
    '            table.Rows.Add(gv.HeaderRow)
    '        End If
    '        '  add each of the data rows to the table
    '        For Each row As GridViewRow In gv.Rows
    '            PrepareControlForExport(row)
    '            table.Rows.Add(row)
    '        Next
    '        '  add the footer row to the table
    '        If (Not (gv.FooterRow) Is Nothing) Then
    '            PrepareControlForExport(gv.FooterRow)
    '            table.Rows.Add(gv.FooterRow)
    '        End If
    '        '  render the table into the htmlwriter
    '        table.RenderControl(htw)
    '        '  render the htmlwriter into the response
    '        HttpContext.Current.Response.Write(sw.ToString)
    '        HttpContext.Current.Response.End()
    '    End Sub
    '    ' Replace any of the contained controls with literals
    '    Private Shared Sub PrepareControlForExport(ByVal control As Control)
    '        Dim i As Integer = 0
    '        Do While (i < control.Controls.Count)
    '            Dim current As Control = control.Controls(i)
    '            If (TypeOf current Is LinkButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
    '            ElseIf (TypeOf current Is ImageButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
    '            ElseIf (TypeOf current Is HyperLink) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
    '            ElseIf (TypeOf current Is DropDownList) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
    '            ElseIf (TypeOf current Is TextBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
    '            ElseIf (TypeOf current Is CheckBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
    '                'TODO: Warning!!!, inline IF is not supported ?
    '            End If
    '            If current.HasControls Then
    '                PrepareControlForExport(current)
    '            End If
    '            i = (i + 1)
    '        Loop
    '    End Sub
    'Private Sub getassetcategory()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
    '    sp.Command.AddParameter("@dummy", 1, DbType.String)
    '    ddlAssetCategory.DataSource = sp.GetDataSet()
    '    ddlAssetCategory.DataTextField = "VT_TYPE"
    '    ddlAssetCategory.DataValueField = "VT_CODE"
    '    ddlAssetCategory.DataBind()
    '    ddlAssetCategory.Items.Insert(0, "--All--")
    'End Sub
    'Private Sub getsubcategorybycat(ByVal categorycode As String)
    '    ' ddlAstSubCat.Enabled = True
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
    '    sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
    '    ddlAstSubCat.DataSource = sp.GetDataSet()
    '    ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
    '    ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
    '    ddlAstSubCat.DataBind()
    '    ddlAstSubCat.Items.Insert(0, "--All--")
    '    'ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    'End Sub
    'Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
    '    sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
    '    sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
    '    ddlAstBrand.DataSource = sp.GetDataSet()
    '    ddlAstBrand.DataTextField = "manufacturer"
    '    ddlAstBrand.DataValueField = "manufactuer_code"
    '    ddlAstBrand.DataBind()
    '    ddlAstBrand.Items.Insert(0, "--All--")
    '    'getconsumbles()
    'End Sub
    'Private Sub getmakebycatsubcat()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
    '    sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
    '    ddlModel.DataSource = sp.GetDataSet()
    '    ddlModel.DataTextField = "AST_MD_NAME"
    '    ddlModel.DataValueField = "AST_MD_CODE"
    '    ddlModel.DataBind()
    '    ddlModel.Items.Insert(0, "--All--")

    'End Sub
    'Private Sub BindLocation()
    '    'Dim param(1) As SqlParameter
    '    'param(0) = New SqlParameter("@dummy", SqlDbType.Int)
    '    'param(0).Value = 1
    '    'param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 100)
    '    'param(0).Value = Session("Uid").ToString
    '    'ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)

    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
    '    sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
    '    ddlLocation.DataSource = sp.GetReader
    '    ddlLocation.DataTextField = "LCM_NAME"
    '    ddlLocation.DataValueField = "LCM_CODE"
    '    ddlLocation.DataBind()
    '    ddlLocation.Items.Insert(0, "--All--")
    '    ' ddlLocation.Items.Insert(0, New ListItem("--All--", "0"))


    'End Sub

    'Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
    '    If ddlAssetCategory.SelectedIndex <> 0 Then
    '        'ddlAstBrand.Items.Clear()
    '        'ddlModel.Items.Clear()
    '        'ddlAstBrand.SelectedIndex = 0
    '        'ddlModel.SelectedIndex = 0
    '        'ddlLocation.SelectedIndex = 0
    '        ' ddlAstSubCat.Items.Insert(0, "--All--")

    '        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)

    '    End If
    'End Sub



    'Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
    '    'If ddlAstSubCat.SelectedIndex <> 0 Then
    '    'ddlModel.Items.Clear()
    '    'ddlModel.SelectedIndex = 0
    '    'ddlLocation.SelectedIndex = 0

    '    getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
    '    'End If

    'End Sub

    'Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
    '    If ddlAstBrand.SelectedIndex <> 0 Then
    '        'ddlLocation.SelectedIndex = 0

    '        getmakebycatsubcat()
    '    End If

    'End Sub
End Class

