Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_InwardEntry
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FillReqIds()
        End If
    End Sub

    Public Sub FillReqIds()
        Dim UID As String = ""
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            UID = Session("uid")
        End If
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
        param(0).Value = UID
        ObjSubsonic.BindGridView(gvReqIds, "GET_INWARD_MOVEMENTS_NP", param)
    End Sub

    Protected Sub gvReqIds_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReqIds.PageIndexChanging
        gvReqIds.PageIndex = e.NewPageIndex
        FillReqIds()
    End Sub

    Protected Sub gvReqIds_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqIds.RowCommand
        If e.CommandName = "Details" Then
            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            Dim lblstat As Label = DirectCast(row.FindControl("lblstat"), Label)
            Dim stat As String = lblstat.Text
            Response.Redirect("InwardEntryDtls.aspx?Req_id=" & e.CommandArgument & "&stat=" & stat)
        End If
    End Sub
End Class
