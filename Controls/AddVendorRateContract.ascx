<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddVendorRateContract.ascx.vb" Inherits="Controls_AddVendorRateContract" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-2 btn btn-default pull-right">
                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                    ToolTip="Please Select Add to add new Vendor Rate Contract and Select Modify to modify the existing Vendor Rate Contract" />
                Add
            </label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="btn btn-default" style="margin-left: 25px">
                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                    ToolTip="Please Select Add to add new Vendor Rate Contract and Select Modify to modify the existing Vendor Rate Contract" />
                Modify
            </label>
        </div>
    </div>
</div>

<div id="mainpanel" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Vendor<span style="color: red;">*</span></label>

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVendor"
                        Display="none" ErrorMessage="Please Select Vendor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Vendor" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Asset Category<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                        Display="none" ErrorMessage="Please Select Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Asset Category" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Asset Sub Category<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                        Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Asset Category" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Asset Brand/Make<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                        Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Asset Brand/Make" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblAssetModel" CssClass="col-md-5 control-label" runat="server">Select Asset Model<span style="color: red;">*</span></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlModel"
                        Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Asset Model"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblHYDate" runat="server" CssClass="col-md-5 control-label">Contract From Date<span style="color: red;">*</span>
                    </asp:Label>
                    <asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="txtFDate"
                        ErrorMessage="Please Select From Date" ValidationGroup="Val1" SetFocusOnError="True"
                        Display="None"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtFDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="Label1" runat="server" CssClass="col-md-5 control-label">Contract To Date<span style="color: red;">*</span>
                    </asp:Label>


                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtTDate"
                        ErrorMessage="Please Select To Date" ValidationGroup="Val1" SetFocusOnError="True"
                        Display="None"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <div class='input-group date' id='Div1'>
                            <asp:TextBox ID="txtTDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="subpanel" runat="server" visible="false">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">

                    <asp:Label ID="lblVenRate" runat="server" CssClass="col-md-5 control-label">Select Vendor Rate Contract</asp:Label>


                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlVenRate" runat="server" CssClass="selectpicker" data-live-search="true"
                            ToolTip="Select Asset Model"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblVendorName" runat="server" CssClass="col-md-5 control-label">Vendor Name</asp:Label>

                    <div class="col-md-7">
                        <asp:Label ID="lblVendorNameData" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblCat" runat="server" CssClass="col-md-5 control-label">Category Name</asp:Label>

                    <div class="col-md-7">
                        <asp:Label ID="lblCatData" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblSubCat" runat="server" CssClass="col-md-5 control-label">Sub Category Name</asp:Label>

                    <div class="col-md-7">
                        <asp:Label ID="lblSubCatData" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblBrand" runat="server" CssClass="col-md-5 control-label">Brand/Make</asp:Label>

                    <div class="col-md-7">
                        <asp:Label ID="lblBrandData" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblModel" runat="server" CssClass="col-md-5 control-label">Model</asp:Label>

                    <div class="col-md-7">
                        <asp:Label ID="lblModelData" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="Label2" runat="server" CssClass="col-md-5 control-label">From Date</asp:Label>

                    <div class="col-md-7">
                        <div class='input-group date' id='Div2'>
                            <asp:TextBox ID="lblFDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('lblFDate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="Label4" runat="server" CssClass="col-md-5 control-label">To Date</asp:Label>

                    <div class="col-md-7">
                        <div class='input-group date' id='Div3'>
                            <asp:TextBox ID="lblTDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('lblTDate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Actual Cost. <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtActualCost"
                    Display="none" ErrorMessage="Please Enter Asset Actual Cost" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPrice"
                    ErrorMessage="Please enter price in numbers" Display="None" ValidationExpression="^[0-9]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <cc1:filteredtextboxextender id="FilteredTextBoxExtender1" runat="server" targetcontrolid="txtActualCost"
                    filtermode="ValidChars" validchars="0123456789">
                    </cc1:filteredtextboxextender>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter price in numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtActualCost" runat="server" CssClass="form-control" MaxLength="10" AutoPostBack="true"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Discount(%) <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtDiscount"
                    Display="none" ErrorMessage="Please Enter Discount(%)" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPrice"
                    ErrorMessage="Please enter price in numbers" Display="None" ValidationExpression="^[0-9]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <cc1:filteredtextboxextender id="FilteredTextBoxExtender2" runat="server" targetcontrolid="txtDiscount"
                    filtermode="ValidChars" validchars="0123456789">
                    </cc1:filteredtextboxextender>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter price in numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="true"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Cost per unit Qty<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtPrice"
                    Display="none" ErrorMessage="Please Enter Price" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtPrice"
                    ErrorMessage="Please enter price in numbers" Display="None" ValidationExpression="^[0-9]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter price in numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks</label>
            <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtBrand"
                    Display="none" ErrorMessage="Please Enter Asset Category Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtBrand"
                    ErrorMessage="Please Enter Asset Category Code" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>--%>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1"
                CausesValidation="true" />
            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowSorting="False"
            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Vendor Rate Contract Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%#Eval("AMG_VENCON_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Asset Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCat" runat="server" CssClass="lblCat" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Sub Category">
                    <ItemTemplate>
                        <asp:Label ID="lblSubCat" runat="server" CssClass="lblCat" Text='<%#BIND("AST_SUBCAT_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Brand/Make">
                    <ItemTemplate>
                        <asp:Label ID="lblAstBrnd" runat="server" CssClass="lblCat" Text='<%#BIND("manufacturer")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Model">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="lblCode" Text='<%#BIND("AST_MD_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vendor Name">
                    <ItemTemplate>
                        <asp:Label ID="lblvENnAME" runat="server" Text='<%#Eval("AVR_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Asset Actual Cost">
                    <ItemTemplate>
                        <asp:Label ID="lblAstActCost" runat="server" CssClass="lblCode" Text='<%# Eval("AMG_ACT_AMT", "{0:c2}")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Discount (%)">
                    <ItemTemplate>
                        <asp:Label ID="lblAstDiscCost" runat="server" CssClass="lblCode" Text='<%# Eval("AMG_DISC_AMT")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Cost">
                    <ItemTemplate>
                        <asp:Label ID="lblAstCost" runat="server" CssClass="lblCode" Text='<%# Eval("AMG_VENCAN_RT","{0:c2}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From Date">
                    <ItemTemplate>
                        <asp:Label ID="lblFromDate" runat="server" CssClass="lblFDate" Text='<%#Bind("AMG_VEN_FDATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Date">
                    <ItemTemplate>
                        <asp:Label ID="lblToDate" runat="server" CssClass="lblToDate" Text='<%#Bind("AMG_VEN_TDATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>

            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />

        </asp:GridView>
    </div>
</div>


