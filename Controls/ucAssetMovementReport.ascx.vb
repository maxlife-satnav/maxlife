Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Security
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class Controls_ucAssetMovementReport
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocation()

            getassetcategory()
            TryCast(ddlAssetCategory, IPostBackDataHandler).RaisePostDataChangedEvent()
            ReportViewer1.Visible = False
            BindGrid()
        End If

    End Sub

    Protected Sub btnsearchInter_Click(sender As Object, e As EventArgs) Handles btnsearchInter.Click

        BindGrid()

    End Sub

    Public Sub BindGrid()

        Dim param(1) As SqlParameter

        param(0) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
        param(0).Value = ""
        param(1) = New SqlParameter("@MMR_AST_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = txtsearchInter.Text
        'ObjSubsonic.BindGridView(gvInterMvmt, "GET_AST_INTER_MVMT_RPT", param)

        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_AST_INTER_MVMT_RPT", param)

        Dim rds As New ReportDataSource()
        rds.Name = "InterMvmtRptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/InterMvmtReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

    End Sub
    'Protected Sub btnsearchIntra_Click(sender As Object, e As EventArgs) Handles btnsearchIntra.Click

    '    Dim param(1) As SqlParameter

    '    param(0) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
    '    param(0).Value = ""
    '    param(1) = New SqlParameter("@MMR_AST_CODE", SqlDbType.NVarChar, 200)
    '    param(1).Value = txtsearchIntra.Text
    '    'ObjSubsonic.BindGridView(gvIntraMvmt, "GET_AST_INTRA_MVMT_RPT", param)

    '    Dim ds As New DataSet
    '    ds = ObjSubsonic.GetSubSonicDataSet("GET_AST_INTRA_MVMT_RPT", param)

    '    Dim rds As New ReportDataSource()
    '    rds.Name = "IntraMvmtRptDS"
    '    'This refers to the dataset name in the RDLC file
    '    rds.Value = ds.Tables(0)
    '    ReportViewer2.Reset()
    '    ReportViewer2.LocalReport.DataSources.Add(rds)
    '    ReportViewer2.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/IntraMvmtReport.rdlc")
    '    ReportViewer2.LocalReport.Refresh()
    '    ReportViewer2.SizeToReportContent = True
    '    ReportViewer2.Visible = True
    '    ReportViewer1.Visible = False
    'End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        'ddlAssetCategory.Items.Insert(0, "--All--")
        ddlAssetCategory.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        'ddlAstSubCat.Items.Insert(0, "--All--")
        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        'getconsumbles()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        TryCast(ddlAstBrand, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ' ddlModel.Items.Insert(0, "--All--")
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))

    End Sub
    Private Sub BindLocation()
        'Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        'param(0).Value = 1
        'param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 100)
        'param(0).Value = Session("Uid").ToString
        'ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
        sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        'ddlLocation.Items.Insert(0, "--All--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "ALL"))


    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        'If ddlAssetCategory.SelectedIndex <> 0 Then
        'ddlAstBrand.Items.Clear()
        'ddlModel.Items.Clear()
        'ddlAstBrand.SelectedIndex = 0
        'ddlModel.SelectedIndex = 0
        'ddlLocation.SelectedIndex = 0
        ' ddlAstSubCat.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()

        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        TryCast(ddlAstSubCat, IPostBackDataHandler).RaisePostDataChangedEvent()

        'End If
    End Sub



    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        'If ddlAstSubCat.SelectedIndex <> 0 Then
        'ddlModel.Items.Clear()
        'ddlModel.SelectedIndex = 0
        'ddlLocation.SelectedIndex = 0
        ddlModel.Items.Clear()
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        'End If

    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        'If ddlAstBrand.SelectedIndex <> 0 Then
        'ddlLocation.SelectedIndex = 0

        getmakebycatsubcat()
        'End If

    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        BindGrid()
    End Sub
End Class
