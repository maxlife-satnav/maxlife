Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ApproveRequest
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getrequest()
        End If
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_APPRREQ")
        sp.Command.AddParameter("@User", Session("uid"), DbType.String)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select WorkRequest--", "0"))
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Approve")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=11")
        'lblMsg.Text = "Approved Succesfully"
        Cleardata()

    End Sub

    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        If ddlWorkRequest.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_WR")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtCity.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
                txtproperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
                txtproptype.Text = ds.Tables(0).Rows(0).Item("PROP_TYPE")
                txtLocation.Text = ds.Tables(0).Rows(0).Item("PN_CTY_CODE")
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT")
                txtVendorName.Text = ds.Tables(0).Rows(0).Item("VENDOR_NAME")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("Remarks")
                Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                If status = 0 Then
                    txtstatus.Text = "Pending"
                ElseIf status = 1 Then
                    txtstatus.Text = "InProgress"
                Else
                    txtstatus.Text = "Completed"
                End If
            End If
            btnApprove.Enabled = True
            btnReject.Enabled = True
        Else
            txtCity.Text = ""
            txtproperty.Text = ""
            txtWorkTitle.Text = ""
            txtWorkSpec.Text = ""
            txtamount.Text = ""
            txtVendorName.Text = ""
            txtRemarks.Text = ""
            txtstatus.Text = ""
            txtproptype.Text = ""
            'btnApprove.Enabled = False
            'btnReject.Enabled = False
        End If
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Reject")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=12")
        'lblMsg.Text = "Request Rejected"
        Cleardata()

    End Sub
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        ' txtBuilding.Text = ""
        txtproperty.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtVendorName.Text = ""
        txtstatus.Text = ""
        txtRemarks.Text = ""
        btnApprove.Enabled = False
        btnReject.Enabled = False
    End Sub
End Class
