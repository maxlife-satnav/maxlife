<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RaiseMaintenanceRequest.ascx.vb"
    Inherits="Controls_RaiseMaintenanceRequest" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Raise Maintenance Request
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="85%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Raise Maintenance Request</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Select Type of Request<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="cvReq" runat="server" ControlToValidate="ddlReq"
                                Display="none" ErrorMessage="Please Select Type of Request" ValidationGroup="Val1"
                                InitialValue="0"></asp:RequiredFieldValidator></td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:DropDownList ID="ddlReq" runat="server" CssClass="clsComboBox" Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Problem Title<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfTitle" runat="server" ControlToValidate="txtProblemTitle"
                                Display="none" ErrorMessage="Problem Title" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:TextBox ID="txtProblemTitle" runat="server" CssClass="clsTextField" Width="97%"
                                MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Problem Description<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtProbDesc"
                                Display="none" ErrorMessage="Problem Description Required" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:TextBox ID="txtProbDesc" runat="server" CssClass="clsTextField" Width="97%"
                                Rows="3" TextMode="MultiLine" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Your Available Time<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                ValidationGroup="Val1" Display="NONE" ErrorMessage="Please Enter Your Available Time"></asp:RequiredFieldValidator>
                            <%-- <asp:RegularExpressionValidator ID="revPropdesc" Display="Dynamic" runat="server"
                                ControlToValidate="txtPropDesc" ErrorMessage="Invalid Property Description" ValidationExpression="[A-Za-z0-9\s]{1,1000}"></asp:RegularExpressionValidator>--%>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" Width="97%" Rows="3"
                                TextMode="MultiLine" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Upload Document (If Required)
                            <asp:RegularExpressionValidator ID="revfubrowse" Display="Dynamic" ControlToValidate="fuBrowseFile"
                                ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt files allowed"
                                ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT]))$"> 
                            </asp:RegularExpressionValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:FileUpload ID="fuBrowseFile" runat="Server" Width="97%" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="button" ValidationGroup="Val1"
                                CausesValidation="true" />
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnsubmit" />
    </Triggers>
</asp:UpdatePanel>
