Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_Mapped_Unmapped_AstDisposeDTLS
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnCancel.Attributes.Add("onclick", "hidePopWin(false);")
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                GetAstDetails()


            End If
        End If
    End Sub


    Public Sub GetAstDetails()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AAT_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("astid")
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASTDETAILS_AATCODE", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblLocation.Text = ds.Tables(0).Rows(0).Item("LCM_Name")
            lblAstName.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
            lblModelName.Text = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
            lblAstCode.Text = ds.Tables(0).Rows(0).Item("AAT_CODE")
        End If

        Dim dsEmpMappedDetails As New DataSet
        dsEmpMappedDetails = ObjSubsonic.GetSubSonicDataSet("GET_ASTDETAILS_AATCODE", param)
        If dsEmpMappedDetails.Tables(1).Rows.Count > 0 Then
            lblEmpId.Text = dsEmpMappedDetails.Tables(1).Rows(0).Item("AUR_ID")
            lblEmpName.Text = dsEmpMappedDetails.Tables(1).Rows(0).Item("AUR_KNOWN_AS")
            lblEmpEmail.Text = dsEmpMappedDetails.Tables(1).Rows(0).Item("AUR_EMAIL")
            trAstMapped.Visible = True
            lblEmpAstTaggedId.Text = dsEmpMappedDetails.Tables(1).Rows(0).Item("AST_SNO")
        Else
            lblEmpId.Text = ""
            lblEmpName.Text = ""
            lblEmpEmail.Text = ""
            trAstMapped.Visible = False
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

    End Sub

    Protected Sub btnDispose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDispose.Click
        Try

            Dim Req_id As String
            Req_id = ObjSubsonic.RIDGENARATION("Admin/Ast/Dispose/")

            Dim Salvage As Integer

            '************ Calculate Asset Salvage Value ***********************
            Dim param_Sal(0) As SqlParameter
            param_Sal(0) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
            param_Sal(0).Value = lblAstCode.Text
            Dim ds As New DataSet
            ds = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_DEP_VALUE", param_Sal)
            If ds.Tables(0).Rows.Count > 0 Then
                Salvage = FormatNumber(ds.Tables(0).Rows(0).Item("ASTVALUE"), 2)
            End If

            '*********************************
            Dim AstTaggedId As Integer = 0

            If String.IsNullOrEmpty(lblEmpAstTaggedId.Text) = True Then
            Else
                AstTaggedId = lblEmpAstTaggedId.Text
            End If


            Dim param(12) As SqlParameter
            param(0) = New SqlParameter("@DREQ_TS", SqlDbType.NVarChar, 200)
            param(0).Value = Req_id
            param(1) = New SqlParameter("@DREQ_ID", SqlDbType.NVarChar, 200)
            param(1).Value = Req_id
            param(2) = New SqlParameter("@AST_TAGGED_ID", SqlDbType.Int)
            param(2).Value = AstTaggedId
            param(3) = New SqlParameter("@DREQ_REQUESITION_DT", SqlDbType.DateTime)
            param(3).Value = getoffsetdatetime(DateTime.Now)
            param(4) = New SqlParameter("@DREQ_REQUESTED_BY", SqlDbType.NVarChar, 200)
            param(4).Value = Session("uid")
            param(5) = New SqlParameter("@DREQ_REQUESTED_REMARKS", SqlDbType.NVarChar, 2000)
            param(5).Value = txtRemarks.Text
            param(6) = New SqlParameter("@DREQ_STATUS", SqlDbType.Int)
            param(6).Value = 1040
            param(7) = New SqlParameter("@AST_SALAVAGE_VALUE", SqlDbType.Int)
            param(7).Value = Salvage
            param(8) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
            param(8).Value = True
            param(9) = New SqlParameter("@DREQ_ADMIN_DT", SqlDbType.Int)
            param(9).Value = ""
            param(10) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.Int)
            param(10).Value = ""
            param(11) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.Int)
            param(11).Value = ""
            param(12) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
            param(12).Value = lblAstCode.Text
            ObjSubsonic.GetSubSonicExecute("INSERT_DISPOSE_REQ", param)
            'btnDispose.Enabled = False


            'Response.Redirect("FAM/FAM_Webfiles/frmMapped_Unmapped_AstDispose.aspx")
            'Response.Write("<script type='text/javascript'>hidePopWin(); </script> ")
            lblMsg.Text = "Dispose request has been raised with Request Id : " & Req_id
        Catch ex As Exception

        End Try
    End Sub
End Class
