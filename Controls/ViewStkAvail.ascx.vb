Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Imports System.Data.OleDb

Partial Class Controls_ViewStkAvail
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If Not IsPostBack Then
                panel1.Visible = False
                Panel2.Visible = False
                Uploadfile.Visible = False
                BindLocation()
                getassetcategory()
                getsubcategorybycat(ddlastCat.SelectedItem.Value)
                ddlastsubCat.SelectedIndex = If(ddlastsubCat.Items.Count > 1, 1, 0)
                getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
                ddlAstBrand.SelectedIndex = If(ddlAstBrand.Items.Count > 1, 1, 0)
                getModelbycatsubcat()
                ddlAstModel.SelectedIndex = If(ddlAstModel.Items.Count > 1, 1, 0)

                BindGrid()
                If gvItems.Rows.Count > 0 Then
                    panel1.Visible = True
                    Panel2.Visible = False
                    btnExport.Visible = True
                    Uploadfile.Visible = True
                Else
                    panel1.Visible = True
                    Panel2.Visible = False
                    btnExport.Visible = False
                    Uploadfile.Visible = False


                End If
            End If

        Catch exp As System.Exception
            Response.Write(exp.Message)
        End Try
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            BindGrid()
            If gvItems.Rows.Count > 0 Then
                panel1.Visible = True
                Panel2.Visible = False
                btnExport.Visible = True
                Uploadfile.Visible = True
            Else
                panel1.Visible = True
                Panel2.Visible = False
                btnExport.Visible = False
                Uploadfile.Visible = False


            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = ddlLocation.SelectedItem.Value
            param(1) = New SqlParameter("@SubcategoryID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlastsubCat.SelectedItem.Value
            param(2) = New SqlParameter("@categoryID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlastCat.SelectedItem.Value
            param(3) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
            param(3).Value = Session("UID")
            param(4) = New SqlParameter("@brand", SqlDbType.NVarChar, 200)
            param(4).Value = ddlAstBrand.SelectedItem.Value
            param(5) = New SqlParameter("@Model", SqlDbType.NVarChar, 200)
            param(5).Value = ddlAstModel.SelectedItem.Value
            ObjSubsonic.BindGridView(gvItems, "GET_ASTLOCTWRFLR", param)
            If gvItems.Rows.Count = 0 Then
                'btnExport.Visible = False
                gvItems.DataSource = Nothing
                gvItems.DataBind()
            Else
                'btnExport.Visible = True
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "All"))
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)

        'ddlLocation.Items.Remove("--All--")
    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlastsubCat.DataSource = sp.GetDataSet()
        ddlastsubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlastsubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlastsubCat.DataBind()
        ' ddlastsubCat.Items.Insert(0, "--Select--")
        ddlastsubCat.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        'ddlastCat.Items.Insert(0, "--Select--")
        ddlastCat.Items.Remove("--Select--")
        ddlastCat.Items.Insert(0, New ListItem("--All--", "All"))
        ddlastCat.SelectedIndex = If(ddlastCat.Items.Count > 1, 1, 0)

    End Sub
    Protected Sub ddlastCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastCat.SelectedIndexChanged
        getsubcategorybycat(ddlastCat.SelectedItem.Value)
        getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
        getModelbycatsubcat()
        BindGrid()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()

        ddlAstBrand.ClearSelection()
        ddlAstModel.ClearSelection()
        btnExport.Visible = False
        Uploadfile.Visible = False
        gvItems.DataSource = Nothing
        gvItems.DataBind()
    End Sub

    Private Sub getbrandbycatsubcat(ByVal astcatcode As String, ByVal astsubcatcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlastsubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, New ListItem("--All--", ""))


    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getModelbycatsubcat()
            BindGrid()
        End If

    End Sub

    Private Sub getModelbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastsubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))


    End Sub
    'Private Sub getmodel()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MODELDRP")
    '    sp.Command.AddParameter("@dummy", 1, DbType.Int32)
    '    ddlAstModel.DataSource = sp.GetDataSet()
    '    ddlAstModel.DataTextField = "AST_MD_CODE"
    '    ddlAstModel.DataValueField = "AST_MD_ID"
    '    ddlAstModel.DataBind()
    '    ddlAstModel.Items.Insert(0, "--All--")
    'End Sub

    Protected Sub ddlastsubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlastsubCat.SelectedIndexChanged
        If ddlastsubCat.SelectedIndex > 0 Then
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
            getModelbycatsubcat()
            BindGrid()
        End If

    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Export("VIEW_AVAILABLE_CAPITAL_ASSETS.xls", gvItems)
        Uploadfile.Visible = True
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        If gv.Columns.Count > 0 Then
            gv.Columns(11).Visible = True
            gv.Columns(12).Visible = True
        End If

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            row.Cells(0).Style.Add("mso-number-format", "\@")
            row.Cells(5).Style.Add("mso-number-format", "\@")
            PrepareControlForExport(row)
            table.Rows.Add(row)

        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)

        '  render the htmlwriter into the response
        'style to format numbers to string
        Dim style As String = "<style>.textmode{mso-number-format:\@;}</style>"
        'HttpContext.Current.Response.Write(style)
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub



    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click

        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""

                msheet = listSheet(0).ToString()
                mfilename = msheet
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                End If
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Code is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Name is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                      
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "space id" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Space Id is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                Next

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EXCEL_CONSUMABLE_ASSET_SPC_EMP_MAP")
                    sp.Command.AddParameter("@ASSET_ID", ds.Tables(0).Rows(i).Item("Asset Code").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_NAME", ds.Tables(0).Rows(i).Item("Asset Name").ToString, DbType.String)
                    sp.Command.AddParameter("@SPACE_ID", ds.Tables(0).Rows(i).Item("Space Id").ToString, DbType.String)
                    sp.Command.AddParameter("@EMP_ID", ds.Tables(0).Rows(i).Item("Employee Id").ToString, DbType.String)
                    sp.ExecuteScalar()

                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_CONSUMABLE_ASSET_SPC_EMP_MAP")
                sp1.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
              
                Dim rst As New DataSet
                rst = sp1.GetDataSet()
                GridView2.DataSource = rst
                GridView2.DataBind()
                panel1.Visible = False
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
            Else
                lblMsg.Text = ""
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


    End Sub

    Protected Sub GridView2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView2.SelectedIndexChanged
        getsubcategorybycat(ddlastCat.SelectedItem.Value)
    End Sub

   

    Protected Sub ddlAstModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstModel.SelectedIndexChanged
        BindGrid()
    End Sub

   
    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindGrid()
    End Sub
End Class
