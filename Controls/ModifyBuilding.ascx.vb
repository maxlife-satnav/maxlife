Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ModifyBuilding
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim ValidateCode As Integer
        ValidateCode = ValidateBuildingCode()
        If ValidateCode = 0 Then
            lblMsg.Text = "Building Code already exist please select another code"
        ElseIf ValidateCode = 1 Then
            Modifyrecord()
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BDG_LOCATION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

            Dim sp7 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CATEGORY_TYPE")
            sp7.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlcategory.DataSource = sp7.GetDataSet()
            ddlcategory.DataTextField = "PN_PROPERTYTYPE"
            ddlcategory.DataValueField = "PN_TYPEID"
            ddlcategory.DataBind()
            ddlcategory.Items.Insert(0, New ListItem("--Select Category--", "0"))




            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BUILDING_DETAILS")
            sp4.Command.AddParameter("@BDG_ID", Request.QueryString("ID"), DbType.Int32)
            Dim ds As New DataSet
            ds = sp4.GetDataSet()

            If ds.Tables(0).Rows.Count > 0 Then
                Dim li As ListItem = Nothing
                li = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_LOC_ID"))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                txtBuildingCode.Text = ds.Tables(0).Rows(0).Item("BDG_ADM_CODE")
                txtBuilding.Text = ds.Tables(0).Rows(0).Item("BDG_NAME")
                TXTPREQID.Text = ds.Tables(0).Rows(0).Item("BDG_PREQ_ID")
                txtGSCID.Text = ds.Tables(0).Rows(0).Item("BDG_GSC_ID")
                
                txtAddress.Text = ds.Tables(0).Rows(0).Item("BDG_ADDRESS")

                Dim li2 As ListItem = Nothing
                li2 = ddlcategory.Items.FindByText(ds.Tables(0).Rows(0).Item("BDG_CATEGORY"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                End If

                Dim li1 As ListItem = Nothing
                li1 = ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_STA_ID"))
                If Not li1 Is Nothing Then
                    li1.Selected = True
                End If


                txtcparea.Text = ds.Tables(0).Rows(0).Item("BDG_CPAREA")
                txtBuiltuparea.Text = ds.Tables(0).Rows(0).Item("BDG_BUAREA")
                txtPMSID.Text = ds.Tables(0).Rows(0).Item("BDG_PMS_ID")
                txtLat.Text = ds.Tables(0).Rows(0).Item("X_LAT")
                txtLon.Text = ds.Tables(0).Rows(0).Item("Y_LON")
                txtBldgDesc.Text = ds.Tables(0).Rows(0).Item("BDG_DESC")
            End If
            End If
    End Sub
    Public Function ValidateBuildingCode()
        Dim ValidateCode As Integer
        Dim BDG_ADM_CODE As String = txtBuildingCode.Text
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_BDG1_CODE")
        sp1.Command.AddParameter("@BDG_ID", Request.QueryString("id"), DbType.Int32)
        sp1.Command.AddParameter("@BDG_ADM_CODE", BDG_ADM_CODE, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub Cleardata()
        ddlLocation.SelectedValue = 0
        txtBuildingCode.Text = ""
        txtBuilding.Text = ""
        TXTPREQID.Text = ""
        txtGSCID.Text = ""
        txtAddress.Text = ""

        ddlcategory.SelectedValue = 0
        ddlstatus.SelectedIndex = -1
       
        txtcparea.Text = ""
        txtBuiltuparea.Text = ""
        txtPMSID.Text = ""
        txtLat.Text = ""
        txtLon.Text = ""
        txtBldgDesc.Text = ""
    End Sub
    Public Sub Modifyrecord()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_BUILDING")
            sp2.Command.AddParameter("@BDG_ID", Request.QueryString("ID"), DbType.Int32)
            sp2.Command.AddParameter("@BDG_ADM_CODE", txtBuildingCode.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_NAME", txtBuilding.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_DESC", txtBldgDesc.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_PREQ_ID", TXTPREQID.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
            ' sp2.Command.AddParameter("@BDG_CTY_ID", ddlcity.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@BDG_GSC_ID", txtGSCID.Text, DbType.Int32)
            sp2.Command.AddParameter("@BDG_ADDRESS", txtAddress.Text, DbType.String)
            ' sp2.Command.AddParameter("@BDG_ACQ_TYPE", ddltype.SelectedItem.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_CATEGORY", ddlcategory.SelectedItem.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_STA_ID", ddlstatus.SelectedItem.Value, DbType.Int32)
            'sp2.Command.AddParameter("@BDG_SUB_CATEGORY", txtsubcategory.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_UPT_BY", Session("UID"), DbType.String)
            'sp2.Command.AddParameter("@BDG_PURPOSE", ddlpurpose.SelectedItem.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_CPAREA", txtcparea.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_BUAREA", txtBuiltuparea.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_PMS_ID", txtPMSID.Text, DbType.Int32)
            sp2.Command.AddParameter("@X_LAT", txtLat.Text, DbType.Decimal)
            sp2.Command.AddParameter("@Y_LON", txtLon.Text, DbType.Decimal)
            sp2.ExecuteScalar()
            'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=3")
            lblMsg.Text = "Property Modified Succesfully"
            lblMsg.Visible = True
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    
    Private Sub BindCategoryByID(ByVal ID As Integer)
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CATEGORYID")
            sp3.Command.AddParameter("@BDG_ID", ID, DbType.Int32)
            ddlcategory.DataSource = sp3.GetDataSet()
            ddlcategory.DataTextField = "BDG_CATEGORY"

            ddlcategory.DataBind()
            ddlcategory.Items.Insert(0, New ListItem("--Select Category--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    

    Protected Sub txtBuiltuparea_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBuiltuparea.TextChanged
        Dim carpetarea As Decimal = Convert.ToDecimal(txtcparea.Text)
        Dim builtuparea As Decimal = Convert.ToDecimal(txtBuiltuparea.Text)
        If (carpetarea > builtuparea) Then
            lblMsg.Text = "Carpet Area should not be greater than Builup Area"
        Else
            lblMsg.Text = ""
        End If
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmEditBuilding.aspx")
    End Sub
End Class
