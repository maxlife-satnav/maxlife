<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GenAstLabels.ascx.vb"
    Inherits="Controls_GenAstLabels" %>
 <div>
  
				
    
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Generate Asset Labels
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="100%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Generate Asset Labels</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td align="center" width="100%" colspan="2">
                <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="red"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>


		 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

            <td align="left">
            <asp:Panel ID="pnlAll" runat="server" >
        
                <table width="100%" cellpadding="1" cellspacing="0" border="1">
                   
              <%--       <tr>
                        <td align="left" width="50%">
                            Select Conference Room Type:
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlconftype"
                                Display="Dynamic" ErrorMessage="Please Select Conference Room Type" ValidationGroup="Val1"
                                InitialValue="0" Visible="false" Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlconftype" runat="server"  Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>

                     <tr>
                        <td align="left" width="50%">
                            Asset Category:
                            <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                                Display="Dynamic" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                                InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            Asset:
                            <asp:RequiredFieldValidator ID="rfvasset" runat="server" ControlToValidate="ddlAsset"
                                Display="Dynamic" ErrorMessage="Please Select Asset" ValidationGroup="Val1" InitialValue="0"
                                Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlAsset" runat="server" Width="99%" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%" style="height: 26px;">
                            Vendor:
                            <asp:RequiredFieldValidator ID="rfvddlvendor" runat="server" ControlToValidate="ddlVendor"
                                Display="Dynamic" ErrorMessage="Please Select Vendor" ValidationGroup="Val1"
                                InitialValue="--Select--" Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%" style="height: 26px">
                            <asp:DropDownList ID="ddlVendor" runat="server" Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left" valign="top">
                            <asp:Panel ID="pnlItems" runat="server" Width="100%">
                                PO List
                                <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    EmptyDataText="No Asset(s) Found." Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="PO" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPO" runat="server" Width="50px" Text='<%#Eval("AIPD_PO_ID") %>'></asp:Label>
                                                <asp:Label ID="lblsku" runat="server" Text='<%#Eval("SKU") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="sku" HeaderText="sku" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-HorizontalAlign="left" />
                                        <asp:BoundField DataField="productname" HeaderText="Name" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-HorizontalAlign="left" />
                                        <asp:TemplateField HeaderText="Requested Qty" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AIPD_QTY") %>'
                                                    ReadOnly="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Purchase Qty" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AIPD_ACT_QTY") %>'
                                                    ReadOnly="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rate (Rs.)" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtRate" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AIPD_RATE") %>'
                                                    ReadOnly="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OrderReceived" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Button ID="btnOrderReceived" runat="server" Text="Order Received" CssClass="button"
                                                    Visible="false" />
                                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            Procurement Type:
                            <asp:RequiredFieldValidator ID="rfvprocurment" runat="server" ControlToValidate="ddlProcType"
                                Display="Dynamic" ErrorMessage="Please Select Procurment Type" ValidationGroup="Val1"
                                InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlProcType" runat="server" Width="99%">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            Brand:
                            <asp:RequiredFieldValidator ID="rfvbrand" runat="server" ControlToValidate="ddlBrand"
                                Display="Dynamic" ErrorMessage="Please Select Brand" ValidationGroup="Val1" InitialValue="0"
                                Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlBrand" runat="server" Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            Model:
                            <asp:RequiredFieldValidator ID="rfvmodel" runat="server" ControlToValidate="txtModel"
                                Display="Dynamic" ErrorMessage="Please Enter Brand" ValidationGroup="Val1" Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtModel" runat="server" Width="99%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            No. Of Asset Id's To Be Generated:
                            <asp:RequiredFieldValidator ID="rfvno" runat="server" ControlToValidate="txtNoOfAssetIds"
                                Display="Dynamic" ErrorMessage="Please Enter No.Of Asset Id's" ValidationGroup="Val1"
                                Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtNoOfAssetIds" runat="server" Width="99%" MaxLength="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%" style="height: 26px">
                            Location:
                            <asp:RequiredFieldValidator ID="rfvlocation" runat="server" ControlToValidate="ddlLocation"
                                Display="Dynamic" ErrorMessage="Please Select Location" ValidationGroup="Val1"
                                InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%" style="height: 26px">
                            <asp:DropDownList ID="ddlLocation" runat="server" Width="99%" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                 <%--   <tr>
                        <td align="left" width="50%">
                            Tower:
                            <asp:RequiredFieldValidator ID="rfvtower" runat="server" ControlToValidate="ddlTower"
                                Display="Dynamic" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="0"
                                Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlTower" runat="server" Width="99%" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            Floor:
                            <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlFloor"
                                Display="Dynamic" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="0"
                                Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlFloor" runat="server" Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="50%">
                            Remarks:
                            <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                                Display="Dynamic" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"
                                Enabled="true"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="99%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" Text="Generate Asset Id's" runat="server" CssClass="button"
                                ValidationGroup="Val1" />
                        </td>
                    </tr>
                </table>
         </asp:Panel>
		  <asp:GridView ID="GridView1" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left" Width="100%"
                                        AllowPaging="True"  PageSize="10 " AutoGenerateColumns="true">
                                 <%--    <Columns>
                                  
                                    <asp:TemplateField HeaderText="Assets">--%>
                                        <%--<ItemTemplate>
                                            <asp:DropDownList ID="ddlassets" runat="server" CssClass="dropDown" Width="100%">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblAssets" runat="server" TEXT='<%#Eval("AAT_NAME") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Space">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlEmp" runat="server"  CssClass="dropDown" Width="100%">
                                            </asp:DropDownList>
                                            <%--<asp:Label ID="lblEmp" runat="server" TEXT='<%#Eval("AUR_FIRST_NAME") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Allocate">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAllocate" runat="server" CommandName="Allocate" ValidationGroup="Val1">Allocate</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>--%>
                                
                                
                                
                                    </asp:GridView>
                                    
                                    
                                     <asp:Button ID="btnExport1" runat="server" CssClass="button" Text="Export to Excel" Visible="false" /> 
                                    <br/>

            </td>
	    
				 </ContentTemplate>
                </asp:UpdatePanel>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
