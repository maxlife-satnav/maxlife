Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_SendRentRemainder
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvTenantRent.PageIndex = 0
        End If
        fillgrid()
    End Sub
    Public Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TENANTRENT_DETAILS")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@User", Session("UID"), DbType.String)
        gvTenantRent.DataSource = sp.GetDataSet()
        gvTenantRent.DataBind()
        'lbtn1.Visible = False
    End Sub

    Protected Sub gvTenantRent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTenantRent.PageIndexChanging
        gvTenantRent.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
End Class
