<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Mapped_Unmapped_AstDispose.ascx.vb"
    Inherits="Controls_Mapped_Unmapped_AstDispose" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="reqastcat" runat="server" ErrorMessage="Please Enter Asset Category" InitialValue="--Select--"
                    ControlToValidate="ddlastCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True" Style="width: 77px">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Sub Category <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rersubastcat" runat="server" ErrorMessage="Please Enter Asset sub Category" InitialValue="--Select--"
                    ControlToValidate="ddlastsubCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastsubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Model<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Location<span style="color: red;">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Location" Display="None" ControlToValidate="ddlLocation"
                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                </label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" AutoPostBack="true" CssClass="selectpicker" data-live-search="true" runat="server"></asp:DropDownList>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnView" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" ValidationGroup="Val1" CausesValidation="False" />
        </div>
    </div>
</div>
<div class="row" id="panel1" runat="server">
    <asp:GridView ID="gvAstList" runat="server" AutoGenerateColumns="False" AllowSorting="True"
        AllowPaging="True" EmptyDataText="No Asset Dispose Found." CssClass="table table-condensed table-bordered table-hover table-striped">
        <PagerSettings Mode="NumericFirstLast" />
        <Columns>

            <asp:TemplateField HeaderText="Asset Name">
                <ItemTemplate>
                    <asp:Label ID="lblAIM_NAME" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Asset Code" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("AAT_CODE")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Asset Model ">
                <ItemTemplate>
                    <asp:Label ID="lblModel_NAME" runat="server" Text='<%#Eval("AAT_MODEL_NAME")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location">
                <ItemTemplate>
                    <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Asset Age ">
                <ItemTemplate>
                    <asp:Label ID="lbl_age" runat="server" Text='<%#Eval("ASSET_AGE")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Asset Cost ">
                <ItemTemplate>
                    <asp:Label ID="lbl_cost" runat="server" Text='<%#Eval("COST")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Asset Salvage Value ">
                <ItemTemplate>
                    <asp:Label ID="lbl_salvge" runat="server" Text='<%#Eval("AAT_AST_SLVGVAL")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Asset Depreciation Cost ">
                <ItemTemplate>
                    <asp:Label ID="lbl_Dep_val" runat="server" Text='<%#Eval("DEPVALUE")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="Asset Tag Id " Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lbl_emp_tag_id" runat="server" Text='<%#Eval("EMP_TAG_ID")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Select">
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <%-- <asp:TemplateField>
                <ItemTemplate>
                    <%--<a href="#" onclick="showPopWin('frmMapped_Unmapped_AstDisposeDTLS.aspx?astid=<%# Eval("AAT_CODE") %>',550,480,null)">Dispose </a>--%>
            <%--<a href="#" onclick="showPopWin('<%#Eval("AAT_CODE")%>')">Dispose</a>--%>
            <%--                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
        <PagerStyle CssClass="pagination-ys" />
    </asp:GridView>
</div>
<div class="row" id="remarks" runat="server">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-3 control-label">Remarks</label>
            <span style="color: Red; display: none;"></span>
            <div class="col-md-7">              
                <asp:TextBox id="txtRemarks" TextMode="multiline" Columns="20" Rows="3" runat="server" class="form-control"/>
            </div>
        </div>
    </div>

    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="DisposeSelected" runat="server" Text="Dispose" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
<script>

<%-- Modal popup block --%>
    function showPopWin(id) {
        $("#modalcontentframe").attr("src", "frmMapped_Unmapped_AstDisposeDTLS.aspx?astid=" + id);
        $("#myModal").modal().fadeIn();
        return false;
    }
</script>

<div class="modal fade" id="myModal" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Mapped & Unmapped Asset Dispose</h4>
            </div>
            <div class="modal-body" id="modelcontainer">
                <%-- Content loads here --%>
                <iframe id="modalcontentframe" src="#" width="100%" height="250px" style="border: none"></iframe>
            </div>
        </div>
    </div>
</div>
