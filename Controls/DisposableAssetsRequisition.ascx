<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DisposableAssetsRequisition.ascx.vb"
    Inherits="Controls_DisposableAssetsRequisition" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <fieldset>
            <legend>Surrendered Disposable Asset List</legend>
             <br />
            <asp:GridView ID="gvSurrenderAstReq" runat="server" EmptyDataText="No Surrender Disposable Asset Found." AllowPaging="true" PageSize="10"
                AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="SurrchkSelect" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requesition ID" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("SREQ_ID", "~/FAM/FAM_WebFiles/frmDisposableAssetsRequisitionDTLS.aspx?Req_id={0}")%>'
                                Text='<%# Eval("SREQ_ID")%> '></asp:HyperLink>
                            <asp:Label ID="lblSREQ_ID" runat="server" Text='<%#Eval("SREQ_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAAT_AST_CODE" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Model Name" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_MODEL_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Surrender Date" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSurrenderDate" runat="server" Text='<%#Eval("AAT_SURRENDERED_DATE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender Status" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSurStatus" runat="server" Text='<%#Eval("STA_TITLE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Salvage Value" ItemStyle-HorizontalAlign="left" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblSalvage" runat="server" Text='<%#Eval("AST_SALAVAGE_VALUE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <%--  <asp:TemplateField ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("SREQ_ID") %>'
                                CommandName="Edit">Details</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </fieldset>
    </div>
</div>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <div class="row table table table-condensed table-responsive">
            <fieldset>
                <legend>Admin Disposable Asset List</legend>
                <br />
                <asp:GridView ID="gvAstDispoReq" runat="server" EmptyDataText="No Admin Disposable Asset Found." AllowPaging="true" PageSize="10"
                    CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="DispchkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Requesition ID" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("DREQ_ID", "~/FAM/FAM_WebFiles/frmAdminAstDisposableDTLS.aspx?Req_id={0}")%>'
                                    Text='<%# Eval("DREQ_ID")%> '></asp:HyperLink>
                                <asp:Label ID="lblDREQ_ID" runat="server" Text='<%#Eval("DREQ_ID") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblAST_CODE" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Model Name" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_MODEL_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Requisition Date" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblDREQ_REQUESITION_DT" runat="server" Text='<%#Eval("DREQ_REQUESITION_DT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblSurStatus" runat="server" Text='<%#Eval("STA_TITLE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <%-- <asp:TemplateField ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("DREQ_ID") %>'
                                    CommandName="Edit"> Details</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </fieldset>
        </div>
    </div>
</div>
<div class="row" id="divremarks" runat="server">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks:<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
<%--</div>
<div class="row">--%>
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnApprovAll" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
